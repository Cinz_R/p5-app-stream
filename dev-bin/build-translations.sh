#!/bin/bash

echo "BUILDING APP TRANSLATIONS"
perl ./share/translations/assets/s2-compile-translations.pl -t share/translations/templates/ -d app-stream2 -p share/translations/compiled/ -j public/static/translations/

echo "BUILDING TEMPLATE TRANSLATIONS"
./dev-bin/translate-compile-template-translation-mo.sh

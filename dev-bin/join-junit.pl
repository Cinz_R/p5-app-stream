#!/usr/bin/perl

use strict; use warnings;

#http://stackoverflow.com/a/1074975/2668265

use XML::LibXML;
my $parser = XML::LibXML->new();

my $junit_xml = XML::LibXML::Element->new('testsuites');

foreach my $file ( @ARGV ){
    my $test_xml = $parser->parse_file($file);
    my $tests = $test_xml->getElementsByTagName( 'testsuite' );
    while ( my $chunk = $tests->shift ){
        $junit_xml->appendWellBalancedChunk( $chunk->toString() );
    }
}

print $junit_xml->toString(1);

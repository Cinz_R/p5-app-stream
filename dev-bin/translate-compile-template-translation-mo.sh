#!/bin/bash

domain="stream-templates"
dir="share/translations/stream-templates"
for entry in $dir/*.po
do
    lang=`echo "$entry" | perl -pe  's/.*\/(.+)\.po/$1/g'`
    msgfmt $entry -o "share/translations/compiled/$lang/LC_MESSAGES/$domain.mo"
done

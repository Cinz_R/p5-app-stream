#!/bin/sh
GVF=share/VERSION-FILE
VERSION_FILE_FRONTEND=share/VERSION-FILE-FRONTEND
LF='
'

## Run this from the application working directory.

## Freely inspired by https://github.com/git/git/blob/master/GIT-VERSION-GEN
## and by http://git.661346.n2.nabble.com/Generate-version-file-by-hooks-td2492028.html

#
# First see if there is a version file (included in release tarballs),
# then try git-describe, then default.
if test -d ${GIT_DIR:-.git} -o -f .git &&
    VN=$(git rev-parse HEAD 2>/dev/null) &&
    case "$VN" in
        *$LF*) (exit 1) ;;
        [a-z0-9]*)
            git update-index -q --refresh
            test -z "$(git diff-index HEAD --)" ||
            VN="$VN-dirty" ;;
    esac
then
    VN=$(echo "$VN" | sed -e 's/-/./g');
else
    echo "Could not find version. Aborting"
    exit 1
fi
VN=$(expr "$VN" : '\(.*\)')

if test -r $GVF
then
    VC=$(sed -e 's/^GIT_VERSION = //' <$GVF)
else
    VC=unset
fi

test "$VN" = "$VC" || {
    echo >&2 "$VN"
    echo "$VN" >$GVF
}

echo "Version=$VN"

## Application specific version
APP_VERSION=`git log  -n 1 --format=format:%H  app/`
echo "Web application version=$APP_VERSION"
echo "$APP_VERSION" > $VERSION_FILE_FRONTEND

#!/usr/bin/env perl
use strict; use warnings;
use Hash::Diff qw/left_diff/;
use File::Temp qw/tempfile/;
use Data::Dumper;
use Stream2;
use Data::Compare q{Compare};
use Hash::Merge;

my $file_to_convert = $ARGV[0]
    or die "first argument should be config_file to convert";

my ( $empty_fh, $empty_fn ) = tempfile();
print $empty_fh q|{
  include_config => [
    'conf/api.conf',
    'conf/settings.conf',
    'conf/dev.conf',
  ]
}|;
close $empty_fh;

my $empty_s2 = Stream2->new({ config_file => $empty_fn });
my $convert_s2 = Stream2->new({ config_file => $file_to_convert });

my $diff = left_diff ( $convert_s2->config, $empty_s2->config );
foreach my $key ( keys %$diff ){
    if ( Compare( $empty_s2->config->{$key}, $convert_s2->config->{$key} ) ) {
        delete ( $diff->{$key} );
    }
}

$Data::Dumper::Sortkeys = 1;
$Data::Dumper::Indent = 1;
my $merge = Hash::Merge->new( 'LEFT_PRECEDENT' );
my $string = Dumper( $merge->merge( $diff, { 'include_config' => [ 'conf/api.conf', 'conf/settings.conf', 'conf/dev.conf' ] } ) );
$string =~ s/\A\$VAR1 = //;

File::Slurp::write_file($file_to_convert, { binmode => ':utf8' }, $string);

unlink( $empty_fn );

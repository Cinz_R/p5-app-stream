#!/usr/bin/env bash

set -e
set -x

BB_DOCKER_REPO=${BB_DOCKER_REPO-docker.steamcourier.com}

HOST_IP=$(ifconfig eth0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')

sudo docker pull $BB_DOCKER_REPO/broadbean/stream2-mysql
sudo docker pull $BB_DOCKER_REPO/broadbean/stream2-web
sudo docker pull $BB_DOCKER_REPO/broadbean/stream2-worker
sudo docker pull johncosta/redis

MYSQL_ID=$(sudo docker run -d -p 3306 $BB_DOCKER_REPO/broadbean/stream2-mysql)
MYSQL_PORT=$(sudo docker port $MYSQL_ID 3306)

REDIS_ID=$(sudo docker run -d -p 6379 -t johncosta/redis)
REDIS_PORT=$(sudo docker port $REDIS_ID 6379)

ADC_DSN="dbi:mysql:database=apli2;host=$HOST_IP;port=$MYSQL_PORT"
STREAM_DSN="dbi:mysql:database=cvsearch;host=$HOST_IP;port=$MYSQL_PORT"
REDIS="$HOST_IP:$REDIS_PORT"

WEB_ID=$(sudo docker run \
    -e STREAM2_CONFIG_MYSQL_ADC_DSN="$ADC_DSN" \
    -e STREAM2_CONFIG_MYSQL_ADC_USERNAME="apli2" \
    -e STREAM2_CONFIG_MYSQL_ADC_PASSWORD="password" \
    -e STREAM2_CONFIG_MYSQL_STREAM_DSN="$STREAM_DSN" \
    -e STREAM2_CONFIG_MYSQL_STREAM_USERNAME="apli2" \
    -e STREAM2_CONFIG_MYSQL_STREAM_PASSWORD="password" \
    -e STREAM2_CONFIG_REDIS="$REDIS" \
    -d -p 3000 $BB_DOCKER_REPO/broadbean/stream2-web)

sudo docker run -d \
    -e STREAM2_CONFIG_MYSQL_ADC_DSN="$ADC_DSN" \
    -e STREAM2_CONFIG_MYSQL_ADC_USERNAME="apli2" \
    -e STREAM2_CONFIG_MYSQL_ADC_PASSWORD="password" \
    -e STREAM2_CONFIG_MYSQL_STREAM_DSN="$STREAM_DSN" \
    -e STREAM2_CONFIG_MYSQL_STREAM_USERNAME="apli2" \
    -e STREAM2_CONFIG_MYSQL_STREAM_PASSWORD="password" \
    -e STREAM2_CONFIG_REDIS="$REDIS" \
    -e STREAM2_WORKER_MANIFEST='{"workers":[{"queues":["Stream2ActionSearch","Stream2ActionDownloadCV"] }]}' \
    $BB_DOCKER_REPO/broadbean/stream2-worker

echo "WEB AVAILABLE ON: $(sudo docker port $WEB_ID 3000)"

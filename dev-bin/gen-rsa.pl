#!/usr/bin/env perl
use strict; use warnings;

use Crypt::OpenSSL::RSA;
my $rsa = Crypt::OpenSSL::RSA->generate_key(2048);

print "\nKeys to be used with the OAuth stuff.";
print "\nThe private key needs to be stored and listed in the config, the public key is used to register your app with the service";
print "\n\n";

print $rsa->get_public_key_string();

print "\n\n".$rsa->get_private_key_string() . "\n\n" ;

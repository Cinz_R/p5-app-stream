
#set -xe

if [ "$1" == "" ]; then
    echo "Output filename required as first variable";
    exit 1;
fi

# handlebars files
./local-node/bin/node node_modules/.bin/xgettext-template --no-location=true -D app/templates/ -o /tmp/translate-hbs.pot --force-po -k __ -k __n:1,2 -k __x -k __nx:1,2


# Will create share/translations/templates/translate-app.pot which is the template file for back end translations
find lib/ -name "*.pm" -follow > /tmp/appfilelist


xgettext --files-from=/tmp/appfilelist -L Perl -kt9n -kt9np:1c,2 -k__ -k\$__ -k%__ -k__x -k__n:1,2 -k__nx:1,2 -k__xn:1,2 -kN__ -k__p:1c,2 -k -o /tmp/translate-app.pot --from-code=UTF-8 --package-name=app-stream2 --package-version=0.01 --force-po --no-location

perl -p -i -e 's/charset=CHARSET/charset=UTF-8/' /tmp/translate-app.pot

# static assets to be served to the front end will be better if they are pre-compiled and cached
find share/translations/assets/ -name "*.pl" -follow > /tmp/assetfilelist
xgettext --files-from=/tmp/assetfilelist -L Perl -k__ -k\$__ -k%__ -k__x -k__n:1,2 -k__nx:1,2 -k__xn:1,2 -kN__ -k -o /tmp/translate-asset.pot --from-code=UTF-8 --package-name=app-stream2 --package-version=0.01 --force-po --no-location

# app javascript files
find app/scripts/ -name "*.js" -follow > /tmp/jsfilelist
xgettext --files-from=/tmp/jsfilelist -L Python -k__ -k\$__ -k%__ -k__x -k__n:1,2 -k__nx:1,2 -k__xn:1,2 -kN__ -k__p:1c,2 -k -o /tmp/translate-js.pot --from-code=UTF-8 --package-name=app-stream2 --package-version=0.01 --force-po --no-location

# Template Toolkit files
perl dev-bin/s2-translate-compile-tt -o /tmp/translate-tt.pot --no-location

# merge them all
msgcat --use-first -o $1 /tmp/translate-app.pot /tmp/translate-tt.pot /tmp/translate-hbs.pot /tmp/translate-js.pot /tmp/translate-asset.pot share/translations/templates/static-template.pot --sort-output

rm /tmp/translate-hbs.pot /tmp/translate-app.pot /tmp/translate-tt.pot /tmp/translate-js.pot /tmp/appfilelist /tmp/jsfilelist /tmp/translate-asset.pot /tmp/assetfilelist

echo
echo
echo "Keys gathered, template file is $1"
echo
echo

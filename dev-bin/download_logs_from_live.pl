#!/usr/bin/perl
use strict;
use warnings;
use feature 'say';
use Net::SCP;


# This  script assummes you already have key access to the boxes you want access to

my $config = {
    "user"        => "sskelton",
    "remote_file" => "/var/log/broadbean/search.log",
    "local_dir"   => "./",
    "servers"     => {
        "prod" => {
            "local_filename" => "search_prod-%s.log",
            "server"         => "prod-aws-search-%s.steamcourier.com",
            "boxes"          => [ 6, 7, 10, 14, 15, 16 ],
        },
        "worker" => {
            "local_filename" => "search_worker-%s.log",
            "server"         => "prod-aws-searchworker-%s.steamcourier.com",
            "boxes"          => [
                27,14,18,24,23,30,15,31,28,32,17,21,20,25,16,26,10,33,9
            ],
        }
    }
};

while (my ($type,$servers) = each %{$config->{servers}}) {

    say "Getting $type";

    for my $box (@{$servers->{boxes}}) {

        my $server = sprintf($servers->{server},$box);
        my $local_filename = sprintf($servers->{local_filename},$box);

        say $box;
		say $server;
		say $local_filename;

 		my $scp = Net::SCP->new(
            {
                'host'=> $server,
                'user' => $config->{user}
            }
        );
		$scp->get(
            $config->{remote_file},
            $config->{local_dir} . $local_filename)
            or die $scp->{errstr};
	}
}



#!/bin/bash

# Merge any new keys in the translate.pot file into existing .po files

echo

function usage {
    echo
    echo "Merge any new keys in the translate.pot file into existing .po files"
    echo
    echo "./dev-bin/translate-deploy-languages.sh -t share/translations/templates/translate.pot  -d share/translations/templates/"
    echo
    echo "-t | --template-filepath :- to define where to find the template (.pot) file"
    echo "-d | --template-directory :- where to find/save/merge the new .po files"
    echo "-h | --help :- show this stuff"
    echo
}

while [ "$1" != "" ]; do
    case $1 in
        -t | --template-filepath ) shift
                                    template=$1
                                    ;;
        -d | --template-directory ) shift
                                    directory=$1
                                    ;;
        -h | --help )               usage
                                    exit
                                    ;;
        * )                         usage
                                    exit 1
    esac
    shift
done
if [ "$template" == "" ]; then
    echo ".pot file location recruired '-t'"
    usage
    exit 1
fi
if [ "$directory" == "" ]; then
    echo "template (.po) directory required '-d'"
    usage
    exit 1
fi
if [ ! -e "$template" ]; then
    echo "template file $template doesn't exist"
    exit 1
fi

for entry in $directory/*.po
do
    lang=`echo "$entry" | perl -pe  's/.*\/(.+)\.po/$1/g'`
    echo "doing $entry / $lang"
    msginit -i $template --locale=$lang --no-translator
    msgcat --use-first -o /tmp/_merged.po $entry "$lang.po"
    msgmerge /tmp/_merged.po $template | msgattrib --no-obsolete > $entry
    rm /tmp/_merged.po "$lang.po"
done

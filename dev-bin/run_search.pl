#!/usr/bin/env perl

use strict; use warnings;
use Log::Any::Adapter;
use Log::Log4perl qw/:easy/;

Log::Any::Adapter->set('Log4perl');

use Log::Any qw/$log/;

use Data::Dumper;
use Stream2::Action::Search;

my $s2 = Stream2->new({ config_file => 'app-stream2.pat.conf' });
$s2->config();

my $job = MyJob->new(
    class => 'Stream2::Action::Search',
    parameters => {
        criteria_id => q{1BTdnPhMp_9NnIUaw-NWLxShClQ},
        template    => q{google_plus},
        options     => {},
        user        => {
            user_id         => 96,
            group_identity  => 'pat',
            auth_tokens     => {}
        },
        remote_ip   => '127.0.0.1',
        stream2_base_url => 'https://192.168.1.213:3001/'
    }
);

$job->{stream2} = $s2;

DB::enable_profile();
for ( 1..10 ) {
    Stream2::Action::Search->perform( $job );
}
DB::disable_profile();

{
    package MyJob;
    use base 'Qurious::Job';

    sub complete_job {
        1;
    }
}

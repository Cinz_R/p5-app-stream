#! /usr/bin/env perl
use Digest::SHA qw(hmac_sha256_hex);

# Version 2
my $secret_key = q{26da45fe8b2aaba4d06a009858b37758ae6c138db64b4b5217dce4c158a9a10b};
my $username = q{pat@pat.pat.pat};
my $api_key = "1097103005";
my $time_ms = time()  * 1000;
my $auth2 = _generate_digest( $secret_key, join( '|', $username, $time_ms, $api_key ) );

warn $username;
warn $api_key;
warn $time_ms;
warn $auth2;

sub _generate_digest {
    my ($key, $data) = @_;

    my $digest = hmac_sha256_hex( $data, $key );

    return $digest;
}

#!/bin/bash

dir="share/translations/stream-templates"
template="$dir/translate.pot"

./dev-bin/extract_keys_from_template -o /tmp/stream-templates.pot
msguniq --use-first /tmp/stream-templates.pot > share/translations/stream-templates/translate.pot

for entry in $dir/*.po
do
    lang=`echo "$entry" | perl -pe  's/.*\/(.+)\.po/$1/g'`
    echo "doing $entry / $lang"
    msginit -i $template --locale=$lang --no-translator
    msgcat --use-first -o /tmp/_merged.po $entry "$lang.po"
    msgmerge /tmp/_merged.po $template | msgattrib --no-obsolete > $entry
    rm /tmp/_merged.po "$lang.po"
    perl ./dev-bin/maketext-to-gettext.pl $entry $lang
done

echo "DONE"

# for t in `ls ../templates/*.po | perl -pe '$_ =~ s/.+\/(.+?)\.po/$1/'`; do msginit -i translate.pot --locale=$t --no-translator; done

#!/usr/bin/env perl
use strict; use warnings;

use local::lib 'local';
use lib q{./lib};

use Encode;
use Locale::PO;
use Bean::Locale;
use Data::Dumper;

my $filename = $ARGV[0] or die "first argument is filepath to .po file";
my $lang = $ARGV[1] or die "second argument is lang";
my $keys_ref = Locale::PO->load_file_asarray( $filename ) or die "file not found";

foreach my $entry ( @$keys_ref ){

    my $key = $entry->msgid;
    $key =~ s/\A"//;
    $key =~ s/"\z//;

    my $translation = Bean::Locale::localise_in( $lang, $key ); # apparently returns bytes

    if ( $key ne $translation ){
        $entry->msgstr( $translation );
    }
}

Locale::PO->save_file_fromarray( $filename, $keys_ref, 'UTF-8' );

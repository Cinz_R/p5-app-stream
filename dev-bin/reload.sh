#! /bin/sh

## This is DEPRECATED as of 25th 03 2015.
#
# Remove at some time in the future.

if [ -f /etc/init.d/deploy_aws_search ];  then
    /etc/init.d/deploy_aws_search restart
fi

if [ -f /etc/init.d/deploy_aws_searchworker ]; then
    /etc/init.d/deploy_aws_searchworker restart
fi

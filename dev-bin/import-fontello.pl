#! /usr/bin/env perl

use strict;
use warnings;

use autodie qw(:all);

use Archive::Extract;

my $fontello_zip = shift or die "Usage: $0 path/to/fontello/file-384jd.zip\n";

my $destination = './public/static/fontello/';
my $template_file = './share/stream2_templates/layouts/default.html.tt';
my $test_file = './qunit/index.html';

unless( -d $destination ){
    die "Destination $destination does not exists";
}

print "Extracting $fontello_zip to $destination\n";

my $extractor = Archive::Extract->new( archive => $fontello_zip );

my $ok = $extractor->extract( to => $destination ) or die "Error extracting $fontello_zip : ".$extractor->error(1);

my $extract_path = $extractor->extract_path();

print "Successfully extracted to ".$extract_path."\n";

my ( $new_local_fontello ) = ( $extract_path =~ /(fontello-\w+)$/ );


print "New fontello dir: $new_local_fontello\n";

print "Adding to git\n";
system('git add -f '.$extract_path );

opendir(my $dh , $destination ) || die "Cannot open $destination: $!";

my @old_ones = grep { $_ !~ /\./ }  grep{ $_ ne $new_local_fontello } readdir( $dh );

print "GIT removing old fontello: ".join(', ' , @old_ones)."\n";

foreach my $old_fontello ( @old_ones ){
    system('git rm -rf '.$destination.'/'.$old_fontello);
}

print "Patching $template_file\n";

my $template_content = '';
{
    open( my $th , '<' , $template_file) or die "Cannot open $template_file: $!";
    {
        local $/ = undef;
        $template_content = <$th>;
    }
    close $th;
}

$template_content =~ s/fontello=\'[^']+\'/fontello='$new_local_fontello'/;

{
    open( my $th , '>' , $template_file ) or die "Cannot open $template_file: $!";
    binmode $th, ':encoding(UTF8)';
    print $th  $template_content;
    close($th);
}

print "Git adding $template_file\n";

system('git add '.$template_file);


my $test_file_content = '';
print "Patching $test_file\n";
{
    open( my $tfh , '<', $test_file );
    { local $/= undef; $test_file_content = <$tfh>; }
    close( $tfh );
}

$test_file_content =~ s/fontello-.+\/css/$new_local_fontello\/css/;
{
    open( my $tfh, '>', $test_file );
    binmode $tfh, ':encoding(UTF8)';
    print $tfh $test_file_content;
    close $tfh;
}

system('git add '.$test_file);

print "All done!\n";
exit(0);

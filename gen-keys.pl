use strict;
use warnings;
use Crypt::OpenSSL::RSA;

my $public_key_file = 'oauth_rsa/id_rsa.pub';
my $private_key_file = 'oauth_rsa/id_rsa';

my $rsa = Crypt::OpenSSL::RSA->generate_key(2048);

open ( PUBLIC_KEY , '>' ,  $public_key_file ) or die "Cannot open $public_key_file for writing: $!\n";
open ( PRIVATE_KEY , '>' , $private_key_file ) or die "Cannot open $private_key_file for writing: $!\n";
print PUBLIC_KEY $rsa->get_public_key_string();
print PRIVATE_KEY $rsa->get_private_key_string();

close PRIVATE_KEY;
close PUBLIC_KEY;

print "Done. Have a look in oauth_rsa\n";
exit(0);

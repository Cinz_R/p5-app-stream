#!./bin/s2-shell

=head1 Pre-compiled Translations!

=head2

    Lets do some pre-compiling, this is somewhat optional, not sure if it is entirely necessary
    but I'm also pretty sure it can't have a negative affect.
    It generally means that the translation lookup time is detracted from the web app frontent.
    Hopefully speeding up render times for people using potato computers

=cut

use utf8;

use Log::Any qw/$log/;
use JSON;
use Getopt::Long;
use File::Slurp qw/write_file read_file/;
use File::Path qw/make_path/;

my $NODE = $ENV{SYSTEM_NODE} || "./local-node/bin/node";
my $NPM = $ENV{SYSTEM_NPM} || "./local-node/bin/npm";

my ( $directory, $perl_dir, $json_dir, $domain );
GetOptions(
    "t=s" => \$directory,
    "p=s" => \$perl_dir,
    "j=s" => \$json_dir,
    "d=s" => \$domain
);

if ( !$directory ){
    die "'-t' template (.po) dir is required";
}
if ( !$perl_dir ){
    die "'-p' compiled .mo file directory required";
}
if ( !$json_dir ){
    die "'-j' compiled .json file directory required";
}
if ( !$domain ){
    die "'-d' domain required";
}


opendir( DIR, $directory ) or die $!;
while ( my $file = readdir(DIR) ){
    next if ( $file !~ /\.po\z/ );
    my ( $lang ) = $file =~ m/(.+)\.po\z/;
    if ( $lang ) {
        my $entry = "$directory/$file";
        warn "compiling $entry: $lang";
        make_path( "$perl_dir/$lang/LC_MESSAGES/" );
        make_path( "$json_dir" );

        # convert .po to .mo
        system(qq{msgfmt $entry -o "$perl_dir/$lang/LC_MESSAGES/$domain.mo"});

        # convert .po to .json
        #
        # The latest and alledgedly coolest version of node-gettext and its dependency
        # i18next-conv have introduced some lower casing of the 'domain' (aka lang).
        # See https://github.com/andris9/node-gettext/blob/master/lib/gettext.js#L248
        #
        system(qq{$NODE node_modules/.bin/i18next-conv -s "$entry" -t "$json_dir/$lang.json" -l }.lc($lang));

        # add extra pre-compiled keys to .json for use by front end
        # These lists are hard coded values which exist in the front end, they are not derived from templates 
        # so we need to pre-translate what them for performance reasons

        # the output of get_assets will be appended to each frontend json translation file
        # -m should point to the appropriate i18next file

        $s2->translations->set_language( $lang );
        $log->info( "Compiling assets for $lang" );

        my $i18next_content = read_file( "$json_dir/$lang.json", binmode => ':utf8' );
        my $i18next_keys = JSON::from_json( $i18next_content );

        my $extra_keys = get_assets( $s2 );
        $extra_keys->{i18nextKeys}->{$lang}->{translation} = $i18next_keys;

        my $json_string = JSON::to_json($extra_keys);
        write_file( "$json_dir/$lang.json", {binmode => ':utf8'}, $json_string );
    }
}

sub get_assets {
    my $s2 = shift;
    return {
        criteria => {
            cv_updated_within_options => [
                ['ALL',$s2->__('Any time')],
                ['TODAY',$s2->__('Today')],
                ['YESTERDAY',$s2->__('Since yesterday')],
                ['3D',$s2->__nx('in the last {n} day', 'in the last {n} days', 3, n => 3)],
                ['1W',$s2->__nx('in the last {n} week', 'in the last {n} weeks', 1, n => 1)],
                ['2W',$s2->__nx('in the last {n} week', 'in the last {n} weeks', 2, n => 2)],
                ['1M',$s2->__nx('in the last {n} month', 'in the last {n} months', 1, n => 1)],
                ['2M',$s2->__nx('in the last {n} month', 'in the last {n} months', 2, n => 2)],
                ['3M',$s2->__nx('in the last {n} month', 'in the last {n} months', 3, n => 3)],
                ['6M',$s2->__nx('in the last {n} month', 'in the last {n} months', 6, n => 6)],
                ['1Y',$s2->__nx('in the last {n} year', 'in the last {n} years', 1, n => 1)],
                ['2Y',$s2->__nx('in the last {n} year', 'in the last {n} years', 2, n => 2)],
                ['3Y',$s2->__nx('in the last {n} year', 'in the last {n} years', 3, n => 3)]
            ],

            industry_options => [
                ['',$s2->__('Not specified')],
                ['Accountancy',$s2->__('Accountancy')],
                ['Admin and Secretarial',$s2->__('Admin and Secretarial')],
                ['Advertising and PR',$s2->__('Advertising and PR')],
                ['Aerospace',$s2->__('Aerospace')],
                ['Agriculture Fishing and Forestry',$s2->__('Agriculture Fishing and Forestry')],
                ['Arts',$s2->__('Arts')],
                ['Automotive',$s2->__('Automotive')],
                ['Banking',$s2->__('Banking')],
                ['Building and Construction',$s2->__('Building and Construction')],
                ['Call Centre and Customer Service',$s2->__('Call Centre and Customer Service')],
                ['Consultancy',$s2->__('Consultancy')],
                ['Defence and Military',$s2->__('Defence and Military')],
                ['Design and Creative',$s2->__('Design and Creative')],
                ['Education and Training',$s2->__('Education and Training')],
                ['Electronics',$s2->__('Electronics')],
                ['Engineering',$s2->__('Engineering')],
                ['FMCG',$s2->__('FMCG')],
                ['Fashion',$s2->__('Fashion')],
                ['Financial Services',$s2->__('Financial Services')],
                ['Graduates and Trainees',$s2->__('Graduates and Trainees')],
                ['Health and Safety',$s2->__('Health and Safety')],
                ['Hospitality and Catering',$s2->__('Hospitality and Catering')],
                ['Human Resources and Personnel',$s2->__('Human Resources and Personnel')],
                ['IT',$s2->__('IT')],
                ['Insurance',$s2->__('Insurance')],
                ['Legal',$s2->__('Legal')],
                ['Leisure and Sport',$s2->__('Leisure and Sport')],
                ['Logistics Distribution and Supply Chain',$s2->__('Logistics Distribution and Supply Chain')],
                ['Manufacturing and Production',$s2->__('Manufacturing and Production')],
                ['Marketing',$s2->__('Marketing')],
                ['Media',$s2->__('Media')],
                ['Medical and Nursing',$s2->__('Medical and Nursing')],
                ['New Media and Internet',$s2->__('New Media and Internet')],
                ['Not for Profit and Charities',$s2->__('Not for Profit and Charities')],
                ['Pharmaceuticals',$s2->__('Pharmaceuticals')],
                ['Property and Housing',$s2->__('Property and Housing')],
                ['Public Sector and Government',$s2->__('Public Sector and Government')],
                ['Purchasing and Procurement',$s2->__('Purchasing and Procurement')],
                ['Recruitment Consultancy',$s2->__('Recruitment Consultancy')],
                ['Retail',$s2->__('Retail')],
                ['Sales',$s2->__('Sales')],
                ['Science and Research',$s2->__('Science and Research')],
                ['Senior Appointments',$s2->__('Senior Appointments')],
                ['Social Care',$s2->__('Social Care')],
                ['Telecommunications',$s2->__('Telecommunications')],
                ['Transport and Rail',$s2->__('Transport and Rail')],
                ['Travel and Tourism',$s2->__('Travel and Tourism')],
                ['Utilities',$s2->__('Utilities')]
            ],

            jobtype_options => [
                ['',$s2->__('Any')],
                ['permanent',$s2->__('Permanent/Employee')],
                ['contract',$s2->__('Contract')],
                ['temporary',$s2->__('Temporary')]
            ],

            # talentsearch is slightly different in its values
            talentsearch_jobtype_options => [
                ['',$s2->__('Any')],
                ['Permanent',$s2->__('Permanent/Employee')],
                ['Contract',$s2->__('Contract')],
                ['Temporary',$s2->__('Temporary')]
            ],

            notice_options => [
                ['',$s2->__('Not specified')],
                ['1',$s2->__('Immediate')],
                ['2',$s2->__nx('{n} week', '{n} weeks', 1, n => 1)],
                ['3',$s2->__nx('{n} week', '{n} weeks', 2, n => 2)],
                ['4',$s2->__nx('{n} week', '{n} weeks', 3, n => 3)],
                ['5',$s2->__nx('{n} week', '{n} weeks', 5, n => 5)],
                ['6',$s2->__nx('{n} week', '{n} weeks', 8, n => 8)],
                ['7',$s2->__nx('{n} week', '{n} weeks', 16, n => 16)],
                ['8',$s2->__nx('{n} month+', '{n} months+', 3, n => 3)]
            ],

            recruitment_status_options => [
                ['',$s2->__('Not specified')],
                ['Available',$s2->__('Available')],
                ['In Progress',$s2->__('In Progress')],
                ['On Assignment',$s2->__('On Assignment')]
            ],

            salary_per_options => [
                ['annum',$s2->__('year')],
                ['month',$s2->__('month')],
                ['week',$s2->__('week')],
                ['day',$s2->__('day')],
                ['hour',$s2->__('hour')]
            ],

            talentsearch_salary_per_options => [
                ['',''],
                ['annum',$s2->__('year')],
                ['month',$s2->__('month')],
                ['week',$s2->__('week')],
                ['day',$s2->__('day')],
                ['hour',$s2->__('hour')]
            ]
        },
        placeholders => {
            titleCloseButton                => $s2->__('Close'),
            titleEmailSelected              => $s2->__('Email selected'),
            titleForwardSelected            => $s2->__('Forward selected'),
            titlePreviousButton             => $s2->__('Prev'),
            titleNextButton                 => $s2->__('Next'),
            titleTagHotlistSelected         => $s2->__('Tag Hotlist selected'),
            titleWatchdogsSummaryLink       => $s2->__('View your watchdogs details'),

            placeholderCandidateEnhancement => $s2->__('More free information'),
            placeholderAddToLongList        => $s2->__('Add to Longlist'),
            placeholderAdvert               => $s2->__('Advert?'),
            placeholderBackToSearch         => $s2->__('Back to search'),
            placeholderBoardMightCharge     => $s2->__('Board might charge'),
            placeholderCancelThisAction     => $s2->__('Cancel this action'),
            placeholderDeleteThisNote       => $s2->__('Delete this note'),
            placeholderDeleteThisSearch     => $s2->__('Delete this search'),
            placeholderDeleteThisWatchdog   => $s2->__('Delete this watchdog'),
            placeholderDeleteThisWatchdog   => $s2->__('Delete this watchdog'),
            placeholderDownloadToDesktop    => $s2->__('Download to desktop'),
            placeholderEmail                => $s2->__('Email'),
            placeholderExternalProfile      => $s2->__('External profile'),
            placeholderFavouriteReviewLater => $s2->__('Favourite to review later'),
            placeholderFilterUser           => $s2->__('Type username to filter'),
            placeholderFlaggingHistory      => $s2->__('Flagging History'),
            placeholderForward              => $s2->__('Forward'),
            placeholderKeywords             => $s2->__('Keywords?'),
            placeholderLoadingSearchResults => $s2->__('Loading search results...'),
            placeholderMessage              => $s2->__('Message...'),
            placeholderNameThisTemplate     => $s2->__('Name this template'),
            placeholderNameThisSearch       => $s2->__('Name this search?'),
            placeholderNameYourWatchdog     => $s2->__('Name your watchdog'),
            placeholderNewLongList          => $s2->__('New Longlist Name'),
            placeholderNewWatchdogResults   => $s2->__('New watchdog results'),
            placeholderNoLocationTerm       => $s2->__('Please type a location'),
            placeholderOFCCPContext         => $s2->__('Required OFCCP - Reference Number or Job Title'),
            placeholderPleaseSelect         => $s2->__('Please Select'),
            placeholderPleaseTypeJobTitle   => $s2->__('Please type in a job title'),
            placeholderPreview              => $s2->__('Preview'),
            placeholderProfile              => $s2->__('Profile'),
            placeholderRemoveFromLongList   => $s2->__('Remove from Longlist'),
            placeholderReplyTo              => $s2->__('Reply To...'),
            placeholderResearchOnDuedil     => $s2->__('Research on Duedil'),
            placeholderResearchOnFacebook   => $s2->__('Research on Facebook'),
            placeholderResearchOnGooglePlus => $s2->__('Research on Google Plus'),
            placeholderResearchOnLinkedIn   => $s2->__('Research on LinkedIn'),
            placeholderResearchOnTwitter    => $s2->__('Research on Twitter'),
            placeholderResearchOnXing       => $s2->__('Research on XING'),
            placeholderSaveTalentSearch     => $s2->__('Save to TalentSearch'),
            placeholderSaveYourDatabase     => $s2->__('Save to your database'),
            placeholderSavedSearchName      => $s2->__('User or saved search name...'),
            placeholderSearchEmails         => $s2->__('Search emails'),
            placeholderSelectRecipients     => $s2->__('Select recipients'),
            placeholderSubject              => $s2->__('Subject...'),
            placeholderTagHotlist           => $s2->__('Tag/Hotlist'),
            placeholderTagName              => $s2->__('Tag Name...'),
            placeholderTalentSearch         => $s2->__('TalentSearch'),
            placeholderThisCandidateHistory => $s2->__('This candidate has history'),
            placeholderThisCandidateNotes   => $s2->__('This candidate has notes'),
            placeholderWatchdogName         => $s2->__('User or watchdog name...'),
            placeholderWatchdogsRunDaily    => $s2->__('Watchdogs run daily and new candidates are emailed to you.'),
            placeholderWhat                 => $s2->__('What?'),
            placeholderWhere                => $s2->__('Where?'),

            listAdminSettings               => [
                ['Make everything pink', 'setting1'],
                ['Hire all candidates', 'setting2']
            ],
        }
    };
}

;; #-*- mode: Lisp -*-
(ns s2
    ( . require Stream2::Lisp::Core )

    ;; Returns a string about this thing.
    (defn about []
      (.Stream2::Lisp::Core about))

    ;; Dies in misery, just to try what happens when things die
    (defn do-crash []
      (.Stream2::Lisp::Core do_crash))

    ;; Execute the given code in the same context in the given amount of minutes.
    (defmacro do-later [ minutes & body]
      `(.Stream2::Lisp::Core do_later ^{:return "scalar"} ~minutes (clj->string (quote ~@body )))) 
    
    ;;Is the user in the macro_context in the given company.
    (defn user-in-company [company]
      ( .Stream2::Lisp::Core user_in_company ^{:return "raw"} company ))
    
    ;; Is the user in the macro_context in the given office
    (defn user-in-office [office]
      ( .Stream2::Lisp::Core user_in_office ^{:return "raw"} office ))
    
    ;; Is the user in the macro_context in the given team
    (defn user-in-team [team]
      ( .Stream2::Lisp::Core user_in_team ^{:return "raw"} team ))

    ;; Is the candidate property updated with the given value?
    (defn candidate-updated-with-value [property value]
      ( .Stream2::Lisp::Core candidate_is_updated_with_value ^{:return "raw"} property value ))

    ;; Is the response being flagged with the given flag ID
    (defn response-is-being-flagged [flagID]
      ( s2#candidate-updated-with-value "broadbean_adcresponse_rank" flagID ))

    ;; Is candidate in the macro_context in the given destination
    (defn candidate-in-destination [destination]
      ( .Stream2::Lisp::Core candidate_in_destination ^{:return "raw"} destination ))


    ;; Is the candidate in the macro_context a response
    (defn candidate-is-response []
      ( s2#candidate-in-destination "adcresponses" ))

    ;; Send the candidate info to a url
    (defn send-candidate [url]
      ( .Stream2::Lisp::Core send_candidate ^{:return "raw"} url ))

    ;; Send the candidate info to a url
    (defn send-email-template [templateID]
      ( .Stream2::Lisp::Core send_email_template_candidate ^{:return "raw"} templateID ))

    ;; Forward the candidate to an email address
    (defn forward-candidate [email_address]
      ( .Stream2::Lisp::Core forward_candidate ^{:return "raw"} email_address ))

    ;; Do the specified action in the current action_context
    (defn do-action [action]
      ( .Stream2::Lisp::Core do_action ^{:return "raw"} action ))

    )
;;; End of ns namespace s2

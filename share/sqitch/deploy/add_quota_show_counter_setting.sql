-- Deploy sqitch:add_quota_show_counter_setting to mysql

BEGIN;

INSERT INTO setting SET
    setting_mnemonic = "quota-show_credit_counter",
    setting_description = "Show the quota credit counter",
    default_boolean_value = 1,
    setting_type = "boolean";

COMMIT;

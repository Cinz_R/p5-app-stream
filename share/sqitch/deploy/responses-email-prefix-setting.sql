-- Deploy sqitch:responses-email-prefix-setting to mysql

BEGIN;

INSERT INTO setting SET
    setting_mnemonic = 'responses-email_subject_prefix',
    setting_description = 'Responses Email Subject Prefix',
    setting_type = 'string',
    default_string_value = '',
    setting_meta_data = '{"information":"The subject prefix used when messaging a candidate. default is blank"}';
COMMIT;
-- Deploy sqitch:cv_users-new-ui_settings-col to mysql

BEGIN;

ALTER TABLE cv_users
ADD COLUMN ui_settings VARCHAR(1024) NULL AFTER settings,
ALGORITHM=INPLACE, LOCK=NONE;

COMMIT;

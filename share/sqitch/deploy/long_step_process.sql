-- Deploy sqitch:long_step_process to mysql

BEGIN;

CREATE TABLE longstepprocess(
       id BIGINT NOT NULL  AUTO_INCREMENT PRIMARY KEY,
       process_class VARCHAR(255) NOT NULL,
       what          VARCHAR(255) DEFAULT NULL,
       status        VARCHAR(50)  NOT NULL DEFAULT 'pending',
       run_at        DATETIME,
       run_id        VARCHAR(36),
       state         BLOB NOT NULL,
       error         TEXT,
       KEY idx_longstepprocess_run_id (run_id),
       KEY idx_longstepprocess_run_at (run_at)
);


COMMIT;

-- Deploy sqitch:add_progressed_flag to mysql

BEGIN;

INSERT INTO setting SET
    setting_mnemonic = "responses-flagging-enable_progressed_flag",
    setting_description = "Responses Progressed Flag Enabled",
    default_boolean_value = 1,
    setting_type = "boolean";

COMMIT;

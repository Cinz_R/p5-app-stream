-- Deploy sqitch:candidate_delegate_action_log to mysql

BEGIN;

INSERT INTO candidate_actions (action) VALUES ('delegate'), ('delegate_fail');

COMMIT;

-- Deploy sqitch:candidate_action_result_enhancement to mysql

BEGIN;

INSERT INTO candidate_actions (action) VALUES ('enhancement'), ('enhancement_fail');

COMMIT;

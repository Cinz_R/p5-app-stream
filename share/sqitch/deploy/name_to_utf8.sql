-- Deploy sqitch:name_to_utf8 to mysql

BEGIN;

alter table email_template modify name varchar(255) character set utf8;

COMMIT;

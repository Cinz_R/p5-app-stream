-- Deploy sqitch:searchrecord_remote_ip_address to mysql

BEGIN;

ALTER TABLE search_record ADD COLUMN remote_ip VARCHAR(32) DEFAULT NULL, ALGORITHM=INPLACE, LOCK=NONE;

COMMIT;

-- Deploy sqitch:custom-flag-table to mysql

BEGIN;

CREATE TABLE adcresponses_custom_flag (
    flag_id INT NOT NULL,
    group_identity VARCHAR(50),
    description VARCHAR(100) NOT NULL,
    colour_hex_code VARCHAR(7) DEFAULT '#000000',
    active TINYINT(1) DEFAULT 1,
    PRIMARY KEY( flag_id, group_identity )
);

COMMIT;

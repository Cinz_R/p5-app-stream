-- Deploy sqitch:new-standard-flag-options to mysql

BEGIN;

INSERT INTO setting SET
    setting_mnemonic = "responses-flagging-enable_red_flag",
    setting_description = "Responses Red Flag Enabled",
    default_boolean_value = 1,
    setting_type = "boolean";

INSERT INTO setting SET
    setting_mnemonic = "responses-flagging-enable_amber_flag",
    setting_description = "Responses Amber Flag Enabled",
    default_boolean_value = 1,
    setting_type = "boolean";

INSERT INTO setting SET
    setting_mnemonic = "responses-flagging-enable_green_flag",
    setting_description = "Responses Green Flag Enabled",
    default_boolean_value = 1,
    setting_type = "boolean";

COMMIT;

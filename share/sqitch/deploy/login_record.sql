-- Deploy sqitch:login_record to mysql

BEGIN;

CREATE TABLE login_record(
       id BIGINT AUTO_INCREMENT PRIMARY KEY,
       `insert_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
       user_id INT(11) NOT NULL REFERENCES cv_users(id) ON DELETE CASCADE,
       remote_ip VARCHAR(32) DEFAULT NULL,
       login_provider VARCHAR(40) DEFAULT NULL,
       hostname VARCHAR(1024) DEFAULT NULL
);

COMMIT;

-- Deploy sqitch:watchdog-destructid to mysql

BEGIN;

ALTER TABLE watchdog
      ADD COLUMN destruction_process_id  BIGINT DEFAULT NULL, algorithm=inplace, lock=none;

ALTER TABLE watchdog  ADD CONSTRAINT fk_watchdog_destruction_process
          FOREIGN KEY (destruction_process_id) REFERENCES longstepprocess(id);
COMMIT;

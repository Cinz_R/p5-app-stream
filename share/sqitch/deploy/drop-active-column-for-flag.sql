-- Deploy sqitch:drop-active-column-for-flag to mysql

BEGIN;

ALTER TABLE adcresponses_custom_flag DROP COLUMN active;

COMMIT;

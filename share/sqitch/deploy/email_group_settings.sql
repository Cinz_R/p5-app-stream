-- Deploy sqitch:email_group_settings to mysql

BEGIN;

INSERT IGNORE INTO groupsetting (group_identity,setting_mnemonic,setting_description,default_boolean_value)
SELECT group_identity, concat ('email_template-',id,'-active'), concat ('email template setting ',id), 1 FROM email_template;


delimiter //
CREATE TRIGGER create_email_template_restriction AFTER INSERT ON email_template
    FOR EACH ROW
        BEGIN
            INSERT INTO groupsetting (group_identity,setting_mnemonic,setting_description,default_boolean_value) VALUES (NEW.group_identity, CONCAT('email_template-', NEW.id , '-active'), concat ('email template setting ',NEW.id),1);
        END;//
delimiter ;


delimiter //
CREATE TRIGGER delete_email_template_restriction AFTER DELETE ON email_template
    FOR EACH ROW
        BEGIN
            DELETE FROM groupsetting WHERE group_identity = OLD.group_identity  AND setting_mnemonic = CONCAT('email_template-', OLD.id , '-active');
        END;//
delimiter ;

ALTER TABLE user_groupsetting DROP FOREIGN KEY `user_groupsetting_fk_setting_id`;
ALTER TABLE user_groupsetting ADD CONSTRAINT  `user_groupsetting_fk_setting_id` FOREIGN KEY (`setting_id`) REFERENCES `groupsetting` (`id`) ON DELETE CASCADE;


COMMIT;

-- Deploy sqitch:watchodog-lastview to mysql

BEGIN;

ALTER TABLE watchdog ADD COLUMN last_viewed_datetime datetime DEFAULT NULL,  algorithm=inplace, lock=none;

CREATE INDEX idx_watchdog_lastviewed ON watchdog( last_viewed_datetime ) algorithm=inplace lock=none;

COMMIT;

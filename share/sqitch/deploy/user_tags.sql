-- Deploy sqitch:user_tags to mysql

BEGIN;

CREATE TABLE `usertag` (
       id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
       `user_id` int(11) NOT NULL,
       `tag_name` varchar(100) COLLATE utf8_bin NOT NULL,
       `insert_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
       `tag_name_ci` varchar(100) CHARACTER SET utf8 NOT NULL,
       UNIQUE KEY ( user_id, tag_name),
       KEY usertag_name_ci_index ( user_id, tag_name_ci),
       FOREIGN KEY (user_id) REFERENCES cv_users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

DELIMITER ;;
CREATE  TRIGGER insert_usertag BEFORE INSERT ON usertag FOR EACH ROW SET NEW.tag_name_ci = LOWER(NEW.tag_name);;
CREATE  TRIGGER update_usertag BEFORE UPDATE ON usertag FOR EACH ROW SET NEW.tag_name_ci = LOWER(NEW.tag_name);;
DELIMITER ;

COMMIT;

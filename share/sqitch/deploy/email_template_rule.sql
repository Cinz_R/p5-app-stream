-- Deploy sqitch:email_template_rule to mysql

BEGIN;

CREATE TABLE email_template_rule(
       `id` int(25) NOT NULL  AUTO_INCREMENT PRIMARY KEY,
        role_name VARCHAR(255) NOT NULL,
        email_template_id int(25) NOT NULL,
        group_macro_id int(25),
        FOREIGN KEY (email_template_id) REFERENCES email_template(id) ON DELETE CASCADE,
        FOREIGN KEY( group_macro_id)  REFERENCES group_macro(id) ON DELETE RESTRICT
);

COMMIT;

-- Deploy sqitch:macro_blockly_xml to mysql

BEGIN;

ALTER TABLE group_macro ADD COLUMN blockly_xml LONGTEXT CHARACTER SET utf8 DEFAULT NULL;

COMMIT;

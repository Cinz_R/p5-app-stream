-- Deploy sqitch:search-record-data to mysql

BEGIN;

ALTER TABLE search_record ADD COLUMN data TEXT DEFAULT NULL, ALGORITHM=INPLACE, LOCK=NONE;
-- XXX Add DDLs here.

COMMIT;

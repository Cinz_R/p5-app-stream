-- Deploy sqitch:email-template-restriction to mysql

BEGIN;

CREATE TABLE email_template_restriction (
    email_template_id INT(25) NOT NULL,
    user_id INT(11) NOT NULL,
    UNIQUE KEY user__email_template (email_template_id,user_id),
    CONSTRAINT email_template_restriction_fk_email_template FOREIGN KEY (`email_template_id`) REFERENCES `email_template` (`id`) ON DELETE CASCADE,
    CONSTRAINT email_template_restriction_fk_cv_users FOREIGN KEY (`user_id`) REFERENCES `cv_users` (`id`) ON DELETE CASCADE
);

COMMIT;

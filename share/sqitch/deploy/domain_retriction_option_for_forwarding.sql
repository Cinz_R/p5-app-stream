-- Deploy sqitch:domain_retriction_option_for_forwarding to mysql

BEGIN;

INSERT INTO setting SET
    setting_mnemonic = "forwarding-restrict_recipient_domain",
    setting_description = "Restrict recipient domain on forward",
    setting_type = "string",
    default_string_value = "",
    setting_meta_data = '{"information":"A comma or space delimited list of domains a candidate can be forwarded to."}';

COMMIT;

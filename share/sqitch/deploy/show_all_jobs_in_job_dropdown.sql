-- Deploy sqitch:show_all_jobs_in_job_dropdown to mysql

BEGIN;

INSERT INTO setting SET
    setting_mnemonic = "show-all-companies-adverts",
    setting_description = "Show all the companies adverts in the jobs dropdown",
    default_boolean_value = 0,
    setting_type = "boolean";

COMMIT;

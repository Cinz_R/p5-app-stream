-- Deploy sqitch:forwarding-candidate_email_subject to mysql

BEGIN;

INSERT INTO setting SET
    setting_mnemonic = 'forwarding-candidate_email_subject',
    setting_description = 'Forward Candidate Email Subject',
    setting_type = 'string',
    default_string_value = 'Response to [jobref] / [advert_title]: [candidate_name]',
    setting_meta_data = '{"information":"The subject used when forwaring a candidate. default: Response to [jobref] / [advert_title]: [candidate_name]"}';
COMMIT;

-- Deploy sqitch:setting_email_template_duplicate_check to mysql

BEGIN;

INSERT INTO setting SET
    setting_mnemonic = 'candidate-actions-message-template-resend-enabled',
    setting_description = 'Candidate Message Resend Same Template Enabled',
    default_boolean_value = 0,
    setting_type = "boolean";

COMMIT;

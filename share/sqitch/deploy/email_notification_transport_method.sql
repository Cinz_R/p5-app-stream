-- Deploy sqitch:email_notification_transport_method to mysql

BEGIN;

ALTER TABLE email_notification ADD COLUMN transport_method VARCHAR(100) DEFAULT NULL, ALGORITHM=INPLACE, LOCK=NONE;

COMMIT;

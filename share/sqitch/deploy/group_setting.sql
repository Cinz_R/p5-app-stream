-- Deploy sqitch:group_setting to mysql

BEGIN;

CREATE TABLE `groupsetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  group_identity varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `setting_mnemonic` varchar(255) COLLATE utf8_bin NOT NULL,
  `setting_description` text CHARACTER SET utf8 NOT NULL,
  `setting_type` varchar(100) COLLATE utf8_bin NOT NULL DEFAULT 'boolean',
  `default_boolean_value` tinyint(4) DEFAULT NULL,
  `default_string_value` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `setting_meta_data` text COLLATE utf8_bin,
   PRIMARY KEY (`id`),
   UNIQUE KEY `groupsetting_mnemonic` (group_identity, `setting_mnemonic`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ;

ALTER TABLE groupsetting  ADD CONSTRAINT fk_groupsetting_groupclient FOREIGN KEY(group_identity) REFERENCES groupclient(`identity`);

CREATE TABLE `user_groupsetting` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `setting_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `admin_ip` varchar(40) NOT NULL DEFAULT '127.0.0.1',
  `insert_datetime` datetime DEFAULT NULL,
  `update_datetime` datetime DEFAULT NULL,
  `boolean_value` tinyint(4) DEFAULT NULL,
  `string_value` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_groupsetting_setting_id_user_id` (`setting_id`,`user_id`),
  KEY `user_groupsetting_idx_admin_user_id` (`admin_user_id`),
  KEY `user_groupsetting_idx_setting_id` (`setting_id`),
  KEY `user_groupsetting_idx_user_id` (`user_id`),
  CONSTRAINT `user_groupsetting_fk_admin_user_id` FOREIGN KEY (`admin_user_id`) REFERENCES `cv_users` (`id`),
  CONSTRAINT `user_groupsetting_fk_setting_id` FOREIGN KEY (`setting_id`) REFERENCES `groupsetting` (`id`),
  CONSTRAINT `user_groupsetting_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `cv_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=latin1;

COMMIT;

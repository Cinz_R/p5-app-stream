-- Deploy sqitch:search_record_nresults to mysql

BEGIN;

ALTER TABLE search_record ADD COLUMN n_results INTEGER DEFAULT NULL, ALGORITHM=INPLACE, LOCK=NONE;

COMMIT;

-- Deploy sqitch:email-template-attachment to mysql

BEGIN;

CREATE TABLE email_template_attachment (
    email_template_id INT(25) NOT NULL,
    group_file_id INT(25) NOT NULL,
    PRIMARY KEY (email_template_id,group_file_id),
    CONSTRAINT email_template_attachment_fk_email_template FOREIGN KEY (`email_template_id`) REFERENCES `email_template` (`id`) ON DELETE CASCADE,
    CONSTRAINT email_template_attachment_fk_group_file FOREIGN KEY (`group_file_id`) REFERENCES `group_file` (`id`) ON DELETE RESTRICT
);

COMMIT;

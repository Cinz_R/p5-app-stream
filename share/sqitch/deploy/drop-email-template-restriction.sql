-- Deploy sqitch:drop-email-template-restriction to mysql

BEGIN;

DROP TABLE email_template_restriction;

COMMIT;

-- Deploy sqitch:flag-name-utf8 to mysql

BEGIN;

ALTER TABLE adcresponses_custom_flag MODIFY COLUMN description VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL;

COMMIT;

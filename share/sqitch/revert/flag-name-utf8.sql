-- Revert sqitch:flag-name-utf8 from mysql

BEGIN;

ALTER TABLE adcresponses_custom_flag MODIFY COLUMN description VARCHAR(100) NOT NULL;

COMMIT;

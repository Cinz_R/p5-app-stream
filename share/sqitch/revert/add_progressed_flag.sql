-- Revert sqitch:add_progressed_flag from mysql

BEGIN;

DELETE FROM setting WHERE setting_mnemonic="responses-flagging-enable_progressed_flag";

COMMIT;

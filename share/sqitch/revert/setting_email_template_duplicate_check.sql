-- Revert sqitch:setting_email_template_duplicate_check from mysql

BEGIN;

DELETE FROM setting WHERE setting_mnemonic = 'candidate-actions-message-template-duplicate-check';

COMMIT;

-- Revert sqitch:search_record_nresults from mysql

BEGIN;

ALTER TABLE search_record DROP COLUMN n_results, ALGORITHM=INPLACE, LOCK=NONE;

COMMIT;

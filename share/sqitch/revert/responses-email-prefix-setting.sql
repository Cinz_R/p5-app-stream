-- Revert sqitch:responses-email-prefix-setting from mysql

BEGIN;

DELETE FROM setting WHERE setting_mnemonic = 'responses-email_subject_prefix';

COMMIT;

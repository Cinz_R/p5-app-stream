-- Revert sqitch:add_quota_show_counter_setting from mysql

BEGIN;

DELETE user_setting
    FROM user_setting
    INNER JOIN setting ON setting.id = user_setting.setting_id
    WHERE setting_mnemonic='quota-show_credit_counter';

DELETE FROM setting WHERE setting_mnemonic="quota-show_credit_counter";

COMMIT;

-- Revert sqitch:action_candidatefuture from mysql

BEGIN;

DELETE FROM candidate_actions WHERE action = 'candidate_future';

COMMIT;

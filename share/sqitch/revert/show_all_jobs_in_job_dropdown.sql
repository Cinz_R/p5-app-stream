-- Revert sqitch:show_all_jobs_in_job_dropdown from mysql

BEGIN;

DELETE user_setting
    FROM user_setting
    INNER JOIN setting ON setting.id = user_setting.setting_id
    WHERE setting_mnemonic='show-all-companies-adverts';

DELETE FROM setting WHERE setting_mnemonic="show-all-companies-adverts";

COMMIT;

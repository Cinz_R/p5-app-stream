-- Revert sqitch:domain_retriction_option_for_forwarding from mysql

BEGIN;

DELETE FROM setting WHERE setting_mnemonic="forwarding-restrict_recipient_domain";

COMMIT;

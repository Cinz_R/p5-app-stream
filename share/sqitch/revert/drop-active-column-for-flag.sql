-- Revert sqitch:drop-active-column-for-flag from mysql

BEGIN;

ALTER TABLE adcresponses_custom_flag ADD COLUMN active TINYINT(1) DEFAULT 1;

COMMIT;

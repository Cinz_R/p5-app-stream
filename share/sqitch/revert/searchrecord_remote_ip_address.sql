-- Revert sqitch:searchrecord_remote_ip_address from mysql

BEGIN;

ALTER TABLE search_record DROP COLUMN remote_ip, ALGORITHM=INPLACE, LOCK=NONE;

COMMIT;

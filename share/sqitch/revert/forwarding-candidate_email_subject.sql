-- Revert sqitch:forwarding-candidate_email_subject from mysql

BEGIN;

DELETE FROM setting WHERE setting_mnemonic = 'forwarding-candidate_email_subject';

COMMIT;

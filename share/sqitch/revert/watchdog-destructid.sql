-- Revert sqitch:watchdog-destructid from mysql

BEGIN;

ALTER TABLE watchdog DROP FOREIGN KEY fk_watchdog_destruction_process;

ALTER TABLE watchdog
      DROP COLUMN destruction_process_id;

COMMIT;

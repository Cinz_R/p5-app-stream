-- Revert sqitch:candidate_delegate_action_log from mysql

BEGIN;

DELETE FROM candidate_actions WHERE action='delegate';
DELETE FROM candidate_actions WHERE action='delegate_fail';

COMMIT;

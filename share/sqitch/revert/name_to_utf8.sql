-- Revert sqitch:name_to_utf8 from mysql

BEGIN;

alter table email_template modify name varchar(255) character set latin1;

COMMIT;

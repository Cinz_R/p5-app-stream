-- Revert sqitch:watchodog-lastview from mysql

BEGIN;

ALTER TABLE watchdog DROP COLUMN last_viewed_datetime;

COMMIT;

-- Revert sqitch:candidate_action_phonecall from mysql

BEGIN;

DELETE FROM candidate_actions WHERE action = 'phonecall';

COMMIT;

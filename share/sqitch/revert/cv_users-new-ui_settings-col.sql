-- Revert sqitch:cv_users-new-ui_settings-col from mysql

BEGIN;

ALTER TABLE cv_users DROP COLUMN ui_settings;

COMMIT;

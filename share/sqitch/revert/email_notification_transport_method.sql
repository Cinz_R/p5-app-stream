-- Revert sqitch:email_notification_transport_method from mysql

BEGIN;

ALTER TABLE email_notification DROP COLUMN transport_method, ALGORITHM=INPLACE, LOCK=NONE;

COMMIT;

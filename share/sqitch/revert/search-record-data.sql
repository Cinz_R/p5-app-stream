-- Revert sqitch:search-record-data from mysql

BEGIN;

ALTER TABLE search_record DROP COLUMN data, ALGORITHM=INPLACE, LOCK=NONE;
-- XXX Add DDLs here.

COMMIT;

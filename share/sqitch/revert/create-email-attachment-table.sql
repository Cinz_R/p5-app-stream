-- Revert sqitch:email-template-attachment from mysql

BEGIN;

SET foreign_key_checks=0;

DROP TABLE email_template_attachment;

SET foreign_key_checks=1;

COMMIT;

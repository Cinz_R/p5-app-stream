-- Revert sqitch:candidate_action_future_message from mysql

BEGIN;

DELETE FROM candidate_actions WHERE action = 'candidate_future_message';

COMMIT;

-- Revert sqitch:email_group_settings from mysql

BEGIN;


DROP TRIGGER create_email_template_restriction;
DROP TRIGGER delete_email_template_restriction;

ALTER TABLE user_groupsetting DROP FOREIGN KEY `user_groupsetting_fk_setting_id`;
ALTER TABLE user_groupsetting ADD CONSTRAINT  `user_groupsetting_fk_setting_id` FOREIGN KEY (`setting_id`) REFERENCES `groupsetting` (`id`) ON DELETE RESTRICT;


COMMIT;

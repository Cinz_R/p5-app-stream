-- Revert sqitch:action_auto_message from mysql

BEGIN;

DELETE FROM candidate_actions WHERE action = 'message_auto';

COMMIT;

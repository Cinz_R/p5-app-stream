-- Revert sqitch:email-template-restriction from mysql

BEGIN;

SET foreign_key_checks=0;

DROP TABLE email_template_restriction;

SET foreign_key_checks=1;

COMMIT;

-- Revert sqitch:candidate_action_result_enhancement from mysql

BEGIN;

DELETE FROM candidate_actions WHERE action='enhancement';
DELETE FROM candidate_actions WHERE action='enhancement_fail';

COMMIT;

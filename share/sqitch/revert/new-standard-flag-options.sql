-- Revert sqitch:new-standard-flag-options from mysql

BEGIN;

DELETE FROM setting WHERE setting_mnemonic="responses-flagging-enable_red_flag";
DELETE FROM setting WHERE setting_mnemonic="responses-flagging-enable_amber_flag";
DELETE FROM setting WHERE setting_mnemonic="responses-flagging-enable_green_flag";

COMMIT;

-- Revert sqitch:group_setting from mysql

BEGIN;

DROP TABLE user_groupsetting;
DROP TABLE groupsetting;

COMMIT;

-- Verify sqitch:watchdog-destructid on mysql

BEGIN;

SELECT destruction_process_id FROM watchdog LIMIT 1;

ROLLBACK;

-- Verify sqitch:domain_retriction_option_for_forwarding on mysql

BEGIN;

SELECT *
    FROM setting
    WHERE setting_mnemonic="forwarding-restrict_recipient_domain"
    LIMIT 1;

ROLLBACK;

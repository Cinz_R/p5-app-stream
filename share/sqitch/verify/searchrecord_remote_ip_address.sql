-- Verify sqitch:searchrecord_remote_ip_address on mysql

BEGIN;

SELECT remote_ip FROM search_record LIMIT 1;

ROLLBACK;

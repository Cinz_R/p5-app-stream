-- Verify sqitch:add_progressed_flag on mysql

BEGIN;

SELECT *
    FROM setting
    WHERE setting_mnemonic="responses-flagging-enable_progressed_flag"
    LIMIT 1;

ROLLBACK;

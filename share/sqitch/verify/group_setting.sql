-- Verify sqitch:group_setting on mysql

BEGIN;

SELECT * FROM groupsetting LIMIT 1;
SELECT * FROM user_groupsetting LIMIT 1;

ROLLBACK;

-- Verify sqitch:login_record on mysql

BEGIN;

SELECT id, insert_datetime, user_id, remote_ip , login_provider, hostname FROM login_record LIMIT 1;

ROLLBACK;

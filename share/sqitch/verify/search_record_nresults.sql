-- Verify sqitch:search_record_nresults on mysql

BEGIN;

SELECT n_results FROM search_record LIMIT 1;

ROLLBACK;

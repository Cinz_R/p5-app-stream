-- Verify sqitch:watchodog-lastview on mysql

BEGIN;

SELECT last_viewed_datetime FROM watchdog LIMIT 1;

ROLLBACK;

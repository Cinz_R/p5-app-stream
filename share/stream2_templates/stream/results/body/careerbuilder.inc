﻿<!-- start: [% component.name %] //-->
    <div class="bd">
    [% IF candidate.snippet %]
        <p>[% candidate.snippet %]</p>
    [% ELSE %]
        <p>[% candidate.resume_title | html %]</p>
    [% END %]
        <p><strong>[% L('Location') | html %]:</strong> [% candidate.location_text FILTER html %]</p>
    [% IF candidate.salary %]
        <p><strong>[% L('Salary') | html %]:</strong> [% candidate.salary FILTER html %]</p>
    [% END %]

    [% IF candidate.employer %]
        <p><strong>[% L('Recent Employer') | html %]:</strong> [% candidate.employer | html %]</p>
    [% END %]

    [% IF candidate.jobtitle %]
        <p><strong>[% L('Recent Jobtitle') | html %]:</strong> [% candidate.jobtitle | html %]</p>
    [% END %]

    [% IF candidate.highest_degree %]
        <p><strong>[% L('Highest Degree Level') | html %]:</strong> [% candidate.highest_degree | html %]</p>
    [% END %]

    <p><strong>[% L('Last Updated') | html %]:</strong> [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.cv_last_updated) FILTER html %]</p>
    [% IF candidate.viewed %]
        <p><strong>[% L('Viewed Status') | html %]:</strong> [% candidate.viewed %] </p>
    [% END %]

    [% IF candidate.raw_last_activity %]
        <p><strong>[% L('Last Activity') | html %]:</strong> [% candidate.raw_last_activity | html %]</p>
    [% END %]
    </div>
<!-- end: [% component.name %] //-->

﻿<!-- start: [% component.name %] //-->
    <div class="bd line">
        <div class="unit size4of5">
            
            [% IF candidate.profile_link OR candidate.profile_url %] 
                <p><strong> [% L('Profile Link')%]: </strong>
                <a target="_blank" href= 
                [% IF no_actions AND candidate.original_profile_link %]
                    "[% candidate.original_profile_link %]">[% candidate.original_profile_link %]
                [% ELSE %]                
                    [% IF candidate.profile_link %] "[% candidate.profile_link %]">[% candidate.profile_link %] [% END %]
                    [% IF candidate.profile_url AND candidate.profile_link == "" %] "[% candidate.profile_url %]" >[% candidate.profile_url %] [% END %]
                [% END %]
                </a>     
            [% END %]
            <p>[% candidate.snippet %]</p>
            [% IF candidate.industry %]
                <p>[% L('Industry') | html %]: [% candidate.industry | html %]</p>            
            [% END %]

            [% IF candidate.specialties %]
                <p style="margin-top: 4px; margin-bottom: 4px;"><strong>[% L('Specialities') | html %]</strong>: [% candidate.specialties %]</p>
            [% END %]

            [% SET current_positions = []; SET previous_positions = []; %]
            [% FOREACH position = candidate.positions %]
                [% IF position.is_current == 'true' %]
                    [% current_positions.push( position ) %]
                [% ELSE %]
                    [% previous_positions.push( position ) %]
                [% END %]
            [% END %]

            [% IF current_positions.size || previous_positions.size %]
                <div class="line" style="padding: 5px; border-top: 1px solid #ccc;">
                    <p><strong>[% L('Work Experience') | html %]</strong></p>
            [% END %]

            [% IF current_positions.size %]
                <strong>[% L('Current') | html %]:</strong>
                [% FOREACH position = current_positions %]
                    [% position.title FILTER html %] at [% position.company_name FILTER html %][% IF !loop.last %]; [% END %]
                [% END %]
                <br>
            [% END %]

            [% IF previous_positions.size %]
                [% FOREACH position = previous_positions %]
                    [% IF position.title %]
                        [% position.title FILTER html %] at [% position.company_name FILTER html %][% IF !loop.last %]; [% END %]
                    [% ELSE %]
                        [% position.company_name FILTER html %][% IF !loop.last %]; [% END %]
                    [% END %]
                [% END %]
            [% END %]
           
            [% IF current_positions.size || previous_positions.size %]
                </div>
            [% END %]

            [% IF candidate.location %]
                <p style="margin-top: 10px;">
                    <img src="https://maps.googleapis.com/maps/api/staticmap?center=[% candidate.location | uri %]&zoom=10&size=500x100&maptype=roadmap&sensor=false">
                </p>
            [% END %]
            [% IF candidate.twitter_handle %]
                <p style="margin-top: 10px;">
                    <iframe allowtransparency="true" frameborder="0" scrolling="no"          src="//platform.twitter.com/widgets/follow_button.html?screen_name=[% candidate.twitter_handle %]" style="width:300px; height:20px;"></iframe>
                </p>
            [% END %]
            [% IF candidate.google_plus_profile_url %]
                <br />
                <div class="g-follow" data-annotation="bubble" data-height="24" data-href="[% candidate.google_plus_profile_url %]" data-rel="author"></div>

                <script type="text/javascript">
                  (function() {
                    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                    po.src = 'https://apis.google.com/js/plusone.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                  })();
                </script>
            [% END %]

        </div>
        <div class="unit size1of5 lastUnit">
            [% IF candidate.picture_url %]
                <img src="[% candidate.picture_url %]" style="padding: 2px; border: 1px solid #E6E6E6; margin: 2px; height: 100px; width: 100px;"> <br />
            [% END %]
            [% IF candidate.num_connections %]
                <p>[% L('Connections') | html %]: <strong>[% candidate.num_connections || 0 | html %]</strong></p>
            [% END %]
            [% IF candidate.num_recommendations %]
                <p>[% L('Recommenders') | html %]: <strong>[% candidate.num_recommendations || 0 | html%]</strong></p>
            [% END %]

            [% IF candidate.distance == 1 %]
                <img src="/images/social/networks/icon_degree_1_24x13_v2.png">
            [% ELSIF candidate.distance == 2 %]
                <img src="/images/social/networks/icon_degree_2_24x13_v2.png">
            [% ELSIF candidate.distance == 3 %]
                <img src="/images/social/networks/icon_degree_3_24x13_v2.png">
            [% ELSIF candidate.distance == 100 %]
                <img src="/images/social/networks/icon_degree_g_v2.png">
            [% ELSIF candidate.distance == '-1' %]
                <img src="/images/social/networks/outofnetwork.png">
            [% END %]
            <!-- [% IF candidate.cache_profile_link %]
                <br />

            [% END %] //-->
        </div>
    </div>
[% IF no_actions %]
    <div class="ft">
        <p>View [% candidate.name || 'this candidate' %] <a href="[% candidate.profile_link %]">Profile</a></p>
    </div>
[% END %]

<!-- end: [% component.name %] //-->

﻿<!-- start: [% component.name %] //-->
<style type="text/css">
    td.indent
    {
        width: 80px;
        vertical-align:text-top;
    }
</style>



<div class="bd">

<p>
    <a class="btn btn-primary" target="_blank" href="[% candidate.contact %]">
        [% L('Contact on') | html_entity %] <b style="font-family: arial">indeed</b>
    </a>
</p>

[% IF candidate.date_modified %]
    <p>
        <strong>[% L('Date Modified') %]:</strong>
        [% candidate.date_modified | html%]
    </p>
[% END %]

<p><strong>[% L('Location') | html %]:</strong> [% candidate.location FILTER html %]</p>

[% IF candidate.experience.size %]
<table>
    <tr>
        <td class="indent"><strong>[% L('Experience') %]:&nbsp;</strong></td>
        <td>
        [% FOREACH experience IN candidate.experience %]
            [% IF experience.title AND experience.company %]
               [% experience.title %] - [% experience.company %]
               [% LAST IF loop.count >= 3 %]
               [% IF not loop.last %] <br/> [% END %]
            [% END %]
        [% END %]
        </td>
    </tr>
</table>
[% END %]

[% IF candidate.education.size %]
<table>
    <tr>
        <td class="indent"><strong>[% L('Education') %]:&nbsp;</strong></td>
        <td>

        [% FOREACH education IN candidate.education %]
            [% IF education.degree AND education.school %]
                [% education.school %] - [% education.degree %]
            [% ELSIF education.school %]
                [% education.school %]
            [% ELSE %]
                [% NEXT %]
            [% END %]
            [% LAST IF loop.count >= 3 %]
            [% IF not loop.last %] <br/> [% END %]
        [% END %]
        </td>
    </tr>
</table>
[% END %]
</div><!-- .bd -->


<!-- end: [% component.name %] //-->

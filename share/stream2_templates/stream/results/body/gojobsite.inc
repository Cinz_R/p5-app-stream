﻿<!-- start: [% component.name %] //-->
    <div class="line">
        [% IF candidate.unlocked != 'N' %]
            [% IF candidate.telephone || candidate.mobile_telephone %]
                <p>
                [% IF candidate.telephone %]<strong>[% L('Telephone') | html %]:</strong> [% candidate.telephone FILTER html %],[% END %]
                [% IF candidate.mobile_telephone %]<strong>[% L('Mobile') | html %]:</strong> [% candidate.mobile_telephone FILTER html %][% END %]
                </p>
            [% END %]
        [% END %]

        [% IF candidate.location_text %] 
        <p><strong>[% L('Preferred Work Location') | html %]:</strong> [% candidate.location_text FILTER html %] ([% L('currently lives in [_1]', candidate.city) | html %])</p>
        [% END %]

        <!-- jobsite return a html snippet //-->
        <p><strong>[% L('Snippet') | html %]:</strong>  [% candidate.snippet | html | em_highlight %]</p>

        <p><strong>[% L('Skills') | html %]:</strong>  [% candidate.skills.split(',').join(', ') %]</p>

        [% IF candidate.roles %]
        <p><strong>[% L('Roles') | html %]:</strong>  [% candidate.roles %]</p>
        [% END %]

        [% IF candidate.salary %]
        <p><strong>[% L('Salary') | html %]:</strong> [% candidate.salary FILTER html %]</p>
        [% END %]

        <p><strong>[% L('Work Permit') | html %]:</strong> [% candidate.work_permit FILTER html %]</p>

        <p><strong>[% L('Last Updated') | html %]:</strong>[% L('[dateformat,_1,_DATEFORMAT_HTML_DATETIME]', candidate.cv_last_updated) %]</p>

        [% IF candidate.online_days && candidate.online_days != 'Never' %]
        <p><strong>[% L('Last Viewed') | html %]:</strong> [% candidate.online_days | html %]</p>
        [% END %]

    </div>
<!-- end: [% component.name %] //-->

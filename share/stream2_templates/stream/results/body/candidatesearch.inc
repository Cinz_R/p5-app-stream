    <div class="line">
        <div class="unit size1of2">

            [% IF candidate.address %]<strong>[% L('Address') | html %]:</strong> [% candidate.address | html %][% END %]
            [% IF candidate.city %]<strong>[% L('City') | html %]:</strong> [% candidate.city | html %][% END %]
            [% IF candidate.country %]<strong>[% L('Country') | html %]:</strong> [% candidate.country | html %][% END %]
            [% IF candidate.phone %]<strong>[% L('Phone') | html %]:</strong> [% candidate.phone | html %][% END %]

            <br />

            [% IF candidate.broadbean_employer_org_name %]<strong>[% L('Employer') | html %]:</strong> [% candidate.broadbean_employer_org_name | html %]<br />[% END %]
            [% IF candidate.broadbean_employer_org_job_title %]<strong>[% L('Job Title') | html %]:</strong> [% candidate.broadbean_employer_org_job_title | html %]<br />[% END %]

            [% IF candidate.current_position %]
                [% IF candidate.current_position.start_date_epoch %]<strong>[% L('Start Date') | html %]:</strong> [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.current_position.start_date_epoch) | html %]<br />[% END %]
                [% IF candidate.current_position.end_date_epoch %]<strong>[% L('End Date') | html %]:</strong> [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.current_position.end_date_epoch) | html %]<br />[% END %]
            [% END %]

            &nbsp;
        </div>
        <div class="unit size1of2 lastUnit">

            <p>
                [% IF candidate.broadbean_initial_source %]<strong>[% L('Original Source') | html %]:</strong> [% candidate.broadbean_initial_source | html %][% END %]
                [% IF candidate.broadbean_final_source %]<strong>[% L('Latest Source') | html %]:</strong> [% candidate.broadbean_final_source | html %][% END %]
            </p>

            [% IF candidate.education_list.size %]
                <strong>[% L('Education') | html %]</strong>
                <ul>
                [% FOREACH education IN candidate.education_list %]
                    <li>
                        [% IF education.degree_name %][% education.degree_name | html %], [% END %]
                        [% IF education.major_name %][% education.major_name | html %], [% ELSE %][% education.degree_code | html %], [% END %]
                        [% IF education.school_name %][% education.school_name | html %], [% END %]
                        [% education.graduation_date %]
                    </li>
                [% END %]
                </ul>
            [% END %]

            [% IF candidate.skills %]
                <p><strong>[% L('Skills') | html %]:</strong> [% candidate.skills | html %]</p>
            [% END %]

        </div>
    </div>
    <div class="line">
        [% IF candidate.current_position.description %]
            <p><strong>[% L('Job Description') | html %]:</strong> [% candidate.current_position.description | html %]</p>
        [% END %]
    </div>
    <div class="line">
        [% IF candidate.highlight_cv.size %]
            <p class="result-text">
                [% FOREACH highlight = candidate.highlight_cv %]
                    [% highlight | html | em_highlight %][% IF !loop.last %] ... [% END %]
                [% END %]
            </p>
        [% ELSE %]
            <p>[% candidate.cv_text | truncate(250) | html %]</p>
        [% END %]
    </div>

<div>
    <div class="line">
        [% IF candidate.location           %]<strong>[% L('Location') %]:</strong> <span>[% candidate.location | html_entity %]</span><br />[% END %]
    </div>
    <div class="line">
        [% SET fields = candidate.additional_fields.item(0) %]
        [% FOR label IN fields.keys %]
            [% IF fields.item(label) %]
                <strong>[% L(label) | html_entity %]:</strong> <br />
                <span>[% fields.item(label) | html_entity %]</span><br />
            [% END %]
        [% END %]
    </div>
    [% IF candidate.summary %]<strong>[% L('Summary') %]</strong><p>[% candidate.summary | html_entity %]</p>[% END %]
</div>

<!-- start: [% component.name %] //-->

<div class="bd line">
    <div class="size1of2 unit">
        [% IF candidate.email %] <p> <strong>[% i18nx('Email') %]:</strong> <span>[% candidate.email %] </span> </p> [% END %]
        [% IF candidate.phone %] <p> <strong>[% i18nx('Telephone Number') %]:</strong> <span>[% candidate.phone %] </span> </p> [% END %]
        [% IF candidate.country_of_residence %] <p> <strong>[% i18nx('Country') %]:</strong> <span>[% candidate.country_of_residence %] </span> </p> [% END %]
        [% IF candidate.county %] <p> <strong>[% i18nx('County') %]:</strong> <span>[% candidate.county %] </span> </p> [% END %]
        [% IF candidate.city %] <p> <strong>[% i18nx('City') %]:</strong> <span>[% candidate.city %] </span> </p> [% END %]
        [% IF candidate.postcode %] <p> <strong>[% i18nx('Postcode') %]:</strong> <span>[% candidate.postcode %] </span> </p> [% END %]
        [% IF candidate.preferred_role %] <p> <strong>[% i18nx('Preferred Role') %]:</strong> <span>[% candidate.preferred_role %] </span> </p> [% END %]
        [% IF candidate.preferred_loc %] <p> <strong>[% i18nx('Preferred Location') %]:</strong> <span>[% candidate.preferred_loc %] </span> </p> [% END %]
    </div>
    <div class="size2of2 unit">
        [% IF candidate.key_skills %] <p> <strong>[% i18nx('Key Skills') %]:</strong> <span>[% candidate.key_skills %] </span> </p> [% END %]
        [% IF candidate.industries %] <p> <strong>[% i18nx('Industries') %]:</strong> <span>[% candidate.industries.join(', ') %] </span> </p> [% END %]
        [% IF candidate.date_last_employed %] <p> <strong>[% i18nx('Last Employed') %]:</strong> <span>[% candidate.date_last_employed %] </span> </p> [% END %]
        [% IF candidate.employer %] <p> <strong>[% i18nx('Employer') %]:</strong> <span>[% candidate.employer %] </span> </p> [% END %]
        [% IF candidate.prev_jobtitle %] <p> <strong>[% i18nx('Previous Jobtitle') %]:</strong> <span>[% candidate.prev_jobtitle %] </span> </p> [% END %]
    </div>
</div>
<!-- end: [% component.name %] //-->
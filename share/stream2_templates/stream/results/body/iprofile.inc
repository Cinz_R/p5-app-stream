﻿<!-- start: [% component.name %] //-->
    <div class="bd">
        <div class="line">
            <div class="unit size1of2">
         [% IF candidate.telephone %]
         <p><strong>[% L('Telephone') | html %]:</strong> [% candidate.telephone | html %]</p>
         [% END %]

         [% IF candidate.raw_availability %]
         <p><strong>[% L('Availability') | html %]:</strong> [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.availability) || candidate.raw_availability | html %]</p>
         [% END %]

         [% IF candidate.cv_last_updated %]
         <p><strong>[% L('CV Date') | html %]:</strong> [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.cv_last_updated) | html %]</p>
         [% END %]

         [% IF candidate.profile_link %]
         <p><a href="[% candidate.profile_link %]" target="_blank">View their [% UNLESS candidate.destination == 'talentsearch' %]i[% END %]Profile</a></p>
         [% END %]
            </div>

            <div class="unit size1of2 lastUnit">
         [% IF candidate.job_title %]
         <p><strong>[% L('Job Title') | html %]:</strong> [% candidate.job_title | html %]</p>
         [% END %]

         [% IF candidate.employer %]
         <p><strong>[% L('Employer') | html %]:</strong> [% candidate.employer | html %]</p>
         [% END %]

         [% IF candidate.location_text || candidate.town %]
         <p><strong>[% L('Location') | html %]:</strong> [% candidate.location_text || candidate.town | html %]</p>
         [% END %]

         [% IF candidate.employment_type %]
         <p><strong>[% L('Job Type') | html %]:</strong> [% candidate.employment_type | html %]</p>
         [% END %]

         [% IF candidate.preferred_rate %]
         <p><strong>[% L('Rate') | html %]:</strong> [% candidate.preferred_rate | html %]</p>
         [% END %]

         [% IF candidate.preferred_salary %]
         <p><strong>[% L('Salary') | html %]:</strong> [% candidate.preferred_salary | html %]</p>
         [% END %]
            </div><!-- .unit -->
        </div><!-- .line -->
    </div><!-- .bd -->
<!-- end: [% component.name %] //-->

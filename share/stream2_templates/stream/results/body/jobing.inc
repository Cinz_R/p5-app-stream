<!-- start: [% component.name %] //-->

<img src="[% candidate.photo_url_50x50 | html_entity %]" style="float: left; padding:5px;"/>
<p>
    <div class="line bd">
        <div class="unit size1of2">
            [% levels = [candidate.career_level, candidate.education_level] %]
            [% levels = levels.grep('.') %]
            [% IF levels %]
                [% levels.join(' | ') | html_entity %]<br />
            [% END %]
        </div>
        <div class="unit size1of2">
            <div class="line bd">
                <div class="unit size1of3">
                    &nbsp;
                    [% salaries = [] %]
                    [% IF candidate.hourly_rate %]
                        [% salaries.push("\$${candidate.hourly_rate}/hr") %]
                    [% END %]
                    [% IF candidate.salary_per_year %]
                        [% annual = candidate.salary_per_year %]
                        [% annual = annual.replace('000$', 'k') %]
                        [% salaries.push("\$${annual}/yr") %]
                    [% END %]
                    [% salaries.join(' - ') | html_entity %]
                </div>
                <div class="unit size1of3">
                    &nbsp;
                    [% candidate.city_stateabbr | html_entity %]
                </div>
                <div class="unit size1of3">
                    &nbsp;
                    [% candidate.last_updated | html_entity %]
                </div>
            </div>
        </div>
    </div>
</p>
<p>[% candidate.snippet | html_entity %]</p>

<div style="clear: both"></div>
<div>
    [% IF candidate.last_viewed %]
        My Last View: [% candidate.last_viewed | html_entity %]
    [% ELSE %]
        New
    [% END %]
</div>

<!-- end: [% component.name %] //-->

<!-- start: [% component.name %] //-->
<div class="bd line">

    <div class="size1of2 unit">
        [% IF candidate.relevance %] <p> <strong> [% L('Rank') %]: </strong> [% candidate.relevance | html%] </p> [% END %]
        [% IF candidate.modified_date %] <p> <strong> [% L('Modified Date') %]: </strong> [% candidate.modified_date | html%] </p> [% END %]
        [% IF candidate.country_resid %] <p> <strong> [% L('Country of Residence') %]: </strong> [% candidate.country_resid | html%] </p> [% END %]
        [% IF candidate.nationality %] <p> <strong> [% L('Nationality') %]: </strong> [% candidate.nationality | html%] </p> [% END %]
    </div>

    <div class="size1of2 unitExt">
    </div>

    <div style="clear:both;">
        <br>
        [% IF candidate.profile %] <p> <strong> [% L('Profile') %]: </strong> [% candidate.profile | html | html_line_break %] </p> [% END %]
        [% IF candidate.experience %] <p> <strong> [% L('Years of Experience') %]: </strong> [% candidate.experience | html | html_line_break %] </p> [% END %]
        [% IF candidate.employment_history %] <p> <strong> [% L('Employment History') %]: </strong> [% candidate.employment_history | html | html_line_break %] </p> [% END %]
        [% IF candidate.qualifications %] <p> <strong> [% L('Qualifications') %]: </strong> [% candidate.qualifications | html%] </p> [% END %]
    </div>
</div>
<!-- end: [% component.name %] //-->

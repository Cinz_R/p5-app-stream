[% USE Math ; %]

<!-- start: [% component.name %] //-->
   <div class="line">
   [% IF candidate.snippet %]
       <p><strong>[% L('Skills') | html %]:</strong>  [% candidate.snippet | html %]</p>
   [% END %]
   </div>
   <div class="line">
       <div class="unit size1of2">
           [% IF candidate.telephone %]
              <p><strong>[% L('Phone') | html %]:</strong> [% candidate.telephone | html %]</p>
           [% END %]
           [% IF candidate.mobile_telephone %]
              <p><strong>[% L('Mobile') | html %]:</strong> [% candidate.mobile_telephone | html %]</p>
           [% END %]
              <p><strong>[% L('Location') | html %]:</strong> [% candidate.location_text | html %][% IF candidate.distance %] ([% L('[_1] miles away', candidate.distance) | html %])[% END %]
              [% IF candidate.idx == 0 %]
                   <img width="1" height="1" src="https://www.cv-library.co.uk/cgi-bin/umid.gif?sessionID=[% cvlibrary_session_id %]&cache_killer=[% Math.rand(1) %]" ></p>
              [% END %]
              <p><strong>[% L('Relevance') | html %]:</strong> [% candidate.relevance | html %]</p>
              <p><strong>[% L('Job Title') | html %]:</strong>  [% candidate.current_job_title | html %]</p>
              <p><strong>[% L('Desired Job Role') | html %]:</strong>  [% candidate.desired_job_title | html %]</p>
              <p><strong>[% L('Last Updated') | html %]:</strong>[% L('[dateformat,_1,_DATEFORMAT_HTML_DATETIME]', candidate.cv_last_updated) %]</p>
           </div>
           <div class="unit size1of2 lastUnit">
              <p><strong>[% L('Available') | html %]:</strong>  [% candidate.availability | html %]</p>
              [% IF candidate.jobtype && candidate.jobtype.size > 1 %]
                <table>
                    <tr>
                        <!-- TODO -->
                        <td style="vertical-align:text-top;"><strong>[% L('Job Type') | html %]:&nbsp;</strong></td>
                        <td>[% candidate.jobtype.join( '<br /> ' ) %]</td>
                    <tr>
                </table>
              [% ELSE %]
                  <p><strong>[% L('Job Type') | html %]:</strong>  [% candidate.jobtype | html %]</p>
              [% END %]
              <p><strong>[% L('Travel') | html %]:</strong>  [% candidate.travel | html %]</p>
              <p><strong>[% L('Expected Salary') | html %]:</strong> [% candidate.salary %]</p>
              <p><strong>[% L('Willing to Relocate') | html %]:</strong>  [% candidate.relocate | html %]</p>
        </div>
    </div>

<!-- end: [% component.name %] //-->

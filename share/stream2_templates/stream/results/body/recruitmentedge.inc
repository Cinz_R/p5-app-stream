<!-- start: [% component.name %] //-->
<style>
    .clear {
        content: "";
        display: table;
        clear: both;
    }
    .float-right {
        float: right;
    }
    .pill-candidate {
        background-color: #1b75bb;
        color: white;
    }
    .indent {
        margin-left: 18px;
    }
</style>
<div class="line">
    <div class="unit size1of3">
        [% IF candidate.last_viewed %] <p> <strong>[% i18n('Last Viewed') %]:</strong> <span>[% candidate.last_viewed %]</span> </p> [% END %]
        [% IF candidate.views %] <p> <strong>[% i18n('Times viewed') %]:</strong> <span>[% candidate.views %]</span> </p> [% END %]
        [% IF candidate.location %] <p> <strong>[% L('Location') %]:</strong> <span>[% candidate.location %]</span> </p> [% END %]
        [% IF candidate.years_exp %] <p> <strong>[% L('Experience') %]:</strong> <span>[% candidate.years_exp %] years</span> </p> [% END %]
        [% IF candidate.social_sources.size > 0 %] <p> <strong>[% i18n('Social Networks') %]:</strong> <span>[% candidate.social_sources.join(', ') %] </span> </p> [% END %]
        [% IF candidate.recent_education %]
            <p> <strong>[% i18n('Recent Education') %]:</strong> </p>
            [% IF candidate.recent_education.Title %] <p class="indent"> <strong>[% i18n('Degree') %]:</strong> <span>[% candidate.recent_education.Title %] </span> </p> [% END %]
            [% IF candidate.recent_education.Institution %] <p class="indent"> <strong>[% i18n('Institute') %]:</strong> <span>[% candidate.recent_education.Institution %] </span> </p> [% END %]
        [% END %]
    </div>

    [% IF candidate.photo %]
        <div class="float-right">
            <img src="[% candidate.photo %]" width="120" height="120" />
        </div>
    [% END %]

    [% IF candidate.keywords.keys.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Skills & Competencies') %]</h4>
            [% FOREACH keyword IN candidate.keywords.keys %]
                <span class="pill pill-candidate">
                    [% keyword %]
                </span>
            [% END %]
        </div>
    [% END %]
</div>
<!-- end: [% component.name %] //-->

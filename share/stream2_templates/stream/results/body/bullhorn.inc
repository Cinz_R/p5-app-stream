﻿<!-- start: [% component.name %] //-->
[% IF candidate.comments %]
<div class="line">
   <div class="unit size1of1 lastUnit">
       <p><strong>[% L('Comments') | html %]</strong>[% candidate.comments | html %]</p>
   </div>
</div>
[% END %]

<div class="line">
    <div class="unit size1of2">
            [% IF candidate.date_added %]
                <p>
                    <strong>[% L('Date Added') | html %]:</strong>
                    [% L('[dateformat,_1,_DATEFORMAT_HTML_DATETIME]', candidate.cv_last_updated) %]
                </p>
            [% END %]
            [% IF candidate.source.size %]<p><strong>[% L('Source') | html %]:</strong> [% candidate.source.join(",") | html %]</p>[% END %]
            [% IF candidate.status %]<p><strong>[% L('Status') | html %]:</strong> [% candidate.status | html %]</p>[% END %]
            [% IF candidate.preferredContact %]<p><strong>[% L('Prefered Contact Method') | html %]:</strong> [% candidate.preferred_contact | html %]</p>[% END %]
            [% IF candidate.phone %]<p><strong>[% L('Phone') | html %]:</strong> [% candidate.phone | html %]</p>[% END %]
            [% IF candidate.mobile %]<p><strong>[% L('Mobile') | html %]:</strong> [% candidate.mobile | html %]</p>[% END %]
            </div>

            <div class="unit size1of2 lastUnit">
            [% IF candidate.willRelocate %]<p><strong>[% L('Willing to relocate') | html %]:</strong> [% candidate.willRelocate | html %]</p>[% END %]
            [% IF candidate.travelLimit %]<p><strong>[% L('Travel Limit') | html %]:</strong> [% candidate.travelLimit | html %]</p>[% END %]
            [% IF candidate.workAuthorised %]<p><strong>[% L('Work Authorisation') | html %]:</strong> [% candidate.workAuthorised | html %]</p>[% END %]
            [% IF candidate.hourlyRate %]<p><strong>[% L('Hourly Rate') | html %]:</strong> [% candidate.hourlyRate | html %]</p>[% END %]
            [% IF candidate.dailyRate %]<p><strong>[% L('Daily Rate') | html %]:</strong> [% candidate.dailyRate | html %]</p>[% END %]
            [% IF candidate.salary %]<p><strong>[% L('Salary') | html %]:</strong> [% candidate.salary | html %]</p>[% END %]
            </div>
</div>

[% IF candidate.skills %]
<div class="line">
    <div class="unit size1of1 lastUnit">
                    <p><strong>[% ('Skills') | html %]</strong>[% candidate.skills | html %]</p>
    </div>
</div>
[% END %]
<!-- end: [% component.name %] //-->

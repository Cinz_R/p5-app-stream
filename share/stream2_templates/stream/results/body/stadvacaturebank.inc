[% IF candidate.last_updated %]
    <strong>[% L('Last Updated') | html_entity %]:</strong>
    [% candidate.last_updated | html_entity %]<br />
[% END %]
[% IF candidate.location %]
    <strong>[% L('Location') | html_entity %]:</strong>
    [% candidate.location | html_entity %]<br />
[% END %]
[% IF candidate.education %]
    <strong>[% L('Education') | html_entity %]:</strong>
    [% candidate.education | html_entity %]<br />
[% END %]
[% IF candidate.desired_job_title %]
    <strong>[% L('Desired job title') | html_entity %]:</strong>
    [% candidate.desired_job_title | html_entity %]<br />
[% END %]
[% IF candidate.seniority %]
    <strong>[% L('Seniority') | html_entity %]:</strong>
    [% candidate.seniority | html_entity %]<br />
[% END %]
[% IF candidate.job_types %]
    <strong>[% L('Job types') | html_entity %]:</strong>
    [% candidate.job_types.join(', ') | html_entity %]<br />
[% END %]
[% IF candidate.experience %]
    <strong>[% L('Experience') | html_entity %]:</strong>
    [% candidate.experience | html_entity %]<br />
[% END %]

<p>[% candidate.bio | html_entity %]</p>
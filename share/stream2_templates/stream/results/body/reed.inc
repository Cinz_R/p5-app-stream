<!-- start: [% component.name %] //-->
<div class="bd line">

    [% IF candidate.date_recently_viewed %]
        <p style="clear:both;"><i>[% L('Viewed on') %]: [% candidate.date_recently_viewed %] </i></p>
    [% END %]

    <div class="size1of2 unit">
        [% IF candidate.preferred_work_locations %]
            <p><strong>[% L('Preferred Location') | html %]:</strong> [% candidate.preferred_work_locations | html %]</p>
        [% END %]

        [% IF candidate.most_recent_job_title %]
            <p><strong>[% L('Recent Job Titles') | html %]:</strong> [% candidate.most_recent_job_title | html %]</p>
        [% END %]

        [% IF candidate.permanent_work OR candidate.temp_work OR candidate.contract_work %]
            <p>
                <strong>[% L('Work Type') %]:</strong>
                [% IF candidate.permanent_work  %] [% candidate.permanent_work | html %] [% END %]
                [% IF candidate.temp_work  %], [% candidate.temp_work | html %] [% END %]
                [% IF candidate.contract_work  %], [% candidate.contract_work | html %] [% END %]
            </p>
        [% END %]
    </div>

    <div class="unit size1of2 lastUnit">

        [% IF candidate.created_on %]
            <p><strong>[% L('Created On') | html %]:</strong> [% L('[dateformat,_1,_DATEFORMAT_HTML_DATETIME]', candidate.created_on ) %]</p>
        [% END %]

        [% IF candidate.last_candidate_login %]
            <p><strong>[% L('Last Login') | html %]:</strong> [% candidate.last_candidate_login | html %]</p>
        [% END %]

        [% IF candidate.most_recent_employer %]
            <p><strong>[% L('Recent Employer') | html %]:</strong> [% candidate.most_recent_employer | html %]</p>
        [% END %]

        [% IF candidate.minimum_salary %]
            <p><strong>[% L('Salary') | html %]:</strong> &pound;[% candidate.minimum_salary | html %] per annum</p>
        [% END %]

        [% IF candidate.minimum_temp_rate %]
            <p><strong>[% L('Rate') | html %]:</strong> &pound;[% candidate.minimum_temp_rate | html %] per hour </p>
        [% END %]

    </div>
</div>

<div class="line">
    [% IF candidate.workHistory.size > 0 %]
        <div> <strong> [% L('Work History') %]:</strong> </div>
        [% FOREACH workHistory IN candidate.workHistory %]
            <div class="unit size1of2">
                <p> <strong> [% workHistory.name %] </strong> </p>
                [% IF workHistory.jobTitle %]
                    <p> <strong> [% L('Job Title') %]: </strong> [% workHistory.jobTitle | html %] </p>
                [% END %]
                [% IF workHistory.jobDescription %]
                    <p> <strong> [% L('Job Description') %]: </strong> [% workHistory.jobDescription | html %] </p>
                [% END %]
                [% IF workHistory.companyName %]
                    <p> <strong> [% L('Company Name') %]: </strong> [% workHistory.companyName | html %] </p>
                [% END %]
                [% IF workHistory.dateFrom OR workHistory.dateTo %]
                    <p style="padding-bottom: 15px;">
                        <strong> [% L('Period') %]: </strong>
                        [% IF workHistory.dateFrom %]
                            [% workHistory.dateFrom | html %]
                        [% END %] -
                        [% IF workHistory.dateTo %]
                            [% workHistory.dateTo | html %]
                        [% END %]
                    </p>
                [% END %]
            </div>
        [% END %]
    [% END %]
</div>

<!-- end: [% component.name %] //-->

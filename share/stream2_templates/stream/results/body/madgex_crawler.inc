<!-- start: [% component.name %] //-->
<div>
    <strong>Last updated:</strong> [% candidate.last_updated | html_entity %]<br />
    <strong>last login:  </strong> [% candidate.last_login   | html_entity %]<br />

    [% IF candidate.summary %]
        <p>[% candidate.summary | html_entity %]</p>
    [% END %]
    <div class="line">
        [% SET print_count = -1 %]
        [% SET per_line = 3 #close and open new div.line per $per_line printed %]
        [% FOR summary_field IN candidate.summary_fields %]
            [% IF summary_field.label AND summary_field.value %]
                [% IF !((print_count = print_count + 1) % per_line) %]
                    </div>
                    <div class="line">
                [% END %]
                <div class="unit size1of3">
                    <strong>[% summary_field.label | html_entity %]</strong>
                    <br />
                    [% summary_field.value | html_entity %]
                </div>
            [% END %]
        [% END %]
    </div>
</div>
<!-- end: [% component.name %] //-->

﻿<!-- start: [% component.name %] //-->
<div class="bd line">
    <div class="unit size1of2"> 
        <p><strong>[% L('Job Title') | html %]:</strong> [% candidate.job_title FILTER html %]</p>
        <p><strong>[% L('Location') | html %]:</strong> [% candidate.location_text FILTER html %]</p>
        <p><strong>[% L('Last Updated') | html %]:</strong> [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.cv_last_updated) | html %]</p>
    </div>
    <div class="unit size1of2 lastUnit">
        <p><strong>[% L('Job Discipline') | html %]:</strong> [% candidate.job_discipline FILTER html %]</p>
        <p><strong>[% L('Job Type') | html %]:</strong> [% candidate.jobtype FILTER html %]</p>
        <p><strong>[% L('Salary') | html %]:</strong> [% candidate.salary FILTER html %]</p>
    </div>
</div><!-- .bd -->
<div class="bd">
    <p>[% candidate.snippet FILTER html %]</p>
</div>
<!-- end: [% component.name %] //-->

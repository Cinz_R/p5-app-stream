<!-- start: [% component.name %] //-->
<div class="bd">
    <p><strong>[% L('Location') | html %]:</strong> [% candidate.location_text FILTER html %]</p>
    <p><strong>[% L('Snippet') | html %]:</strong>  [% candidate.snippet FILTER html %]</p>

    <p><strong>[% L('Experience') | html %]:</strong> [% candidate.experience FILTER html %]</p>
    <p><strong>[% L('Last Updated') | html %]:</strong> [% candidate.cv_last_updated FILTER html %]</p>

    <p><strong>[% L('Salary') | html %]:</strong> [% candidate.salary FILTER html %]</p>
</div>
<!-- end: [% component.name %] //-->

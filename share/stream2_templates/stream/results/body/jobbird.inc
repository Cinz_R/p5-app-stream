[% SET profile = candidate.profile %]
[% MACRO pill(text) BLOCK %]
    <span class="pill" style="background: #1b75bb; color: white;">
        [% text | html_entity %]
    </span>
[% END %]
<div style="position: relative; min-height: 150px">
    <div style="
        background: #b8c3c9;
        height: 150px;
        width: 150px;
        position: absolute;
    ">
        [% IF profile.Picture %]
            <img height="150" width="150" src="[% profile.Picture | html_entity %]" />
        [% END %]
    </div>
    <div style="margin-left: 160px">
        [% IF candidate.last_modified %]
            <strong>[% L('Last modified') %]:</strong>
            [% candidate.last_modified | html_entity %]
        [% END %]
        <div>
            [% pill( "$profile.ViewCount views") %]

            [% FOR hours IN  profile.WorkingHourRangeCodes %]
                [% SET units = L('hours') %]
                [% pill("$hours $units") %]
            [% END %]
            [% IF candidate.location_text %]
                [% pill(candidate.location_text) %]
            [% END %]
            [% FOR education IN profile.EducationLevelCodes %]
                [% pill(education) %]
            [% END %]
            [% FOR salary IN profile.SalaryRangeCodes %]
                [% SET units = L('euros') %]
                [% pill("$salary $units") %]
            [% END %]
            <div style="clear:both"></div>
        </div>
        [% IF profile.CvContent || profile.CvFileContent %]
            <div style="height: 150px; position: relative; border: 1px solid rgb(232, 232, 232);">
                <div style="padding: 5px; overflow: hidden; height: 100%; box-sizing: border-box;">
                    [% IF profile.CvContent %]
                        [% profile.CvContent | html_sandbox %]
                    [% ELSE %]
                        [% IF profile.CvFileContent %]
                            <pre>[% profile.CvFileContent | html_entity %]</pre>
                        [% END %]
                    [% END %]
                </div>
                <div style="
                    position: absolute;
                    left: 50%;
                    bottom: 0;
                    transform: translate(-50%, 50%);
                    border: 1px solid rgb(232, 232, 232);
                    background: white;
                    padding: 2px 10px;">
                    [% L('More in profile') | html_entity %]
                </div>
            </div>
        [% END %]
    </div>
</div>
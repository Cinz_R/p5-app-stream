<!-- start: [% component.name %] //-->
<div class="line">

    [% IF candidate.location       %] <div><strong>[% L('Location')              %]:</strong> [% candidate.location     | html_entity %]</div> [% END %]
    [% IF candidate.availability   %] <div><strong>[% L('Availability')          %]:</strong> [% candidate.availability | html_entity %]</div> [% END %]
    [% IF candidate.education      %] <div><strong>[% L('Education')             %]:</strong> [% candidate.education    | html_entity %]</div> [% END %]
    [% IF candidate.work_auth      %] <div><strong>[% L('US Work Authorization') %]:</strong> [% candidate.work_auth    | html_entity %]</div> [% END %]

    [% IF candidate.work_types.size %]
        <div><strong>Work Types:</strong> [% candidate.work_types.join(', ') %]</div>
    [% END %]
</div>
<!-- end: [% component.name %] //-->

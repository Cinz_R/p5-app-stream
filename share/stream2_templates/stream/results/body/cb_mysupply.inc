[% MACRO textOrDefault(label, text, type) BLOCK %]
    <strong>[% i18n(label) %]:</strong>
    [% IF text %]
        [% IF type == 'date' %]
            <span>[% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', text) | html %]</span>
        [% ELSE %]
            <span>[% text | html %]</span>
        [% END %]
    [% ELSE %]
        <span class="font-grey" style="font-style: italic">Not available</span>
    [% END %]
[% END %]
<!-- start: [% component.name %] //-->
<div class="line">
    <div class="size1of2 unit">
        <div>[% textOrDefault('Application Job Title', candidate.application_jobtitle) %]</div>
        <div>[% textOrDefault('Application Source', candidate.application_source) %]</div>
        <div>[% textOrDefault('Job Title', candidate.job_title) %]</div>
        <div>[% textOrDefault('Education Level', candidate.education_level) %]</div>
        <div>[% textOrDefault('System Source', candidate.vendor_name) %]</div>
        <div>[% textOrDefault('Employer', candidate.employer) %]</div>
        <div>[% textOrDefault('Subsource', candidate.subsource) %]</div>
        [% IF candidate.candidate_status %]
            <div>[% textOrDefault('Candidate Status', candidate.candidate_status) %]</div>
        [% END %]
    </div>

    <div class="size1of2 unitExt">
        <div>[% textOrDefault('Telephone', candidate.telephone) %]</div>
        <div>[% textOrDefault('City', candidate.city) %]</div>
        <div>[% textOrDefault('State', candidate.state) %]</div>
        <div>[% textOrDefault('Zipcode', candidate.postcode) %]</div>
        <div>[% textOrDefault('Last Activity', candidate.raw_last_login) %]</div>
        <div>[% textOrDefault('Last Contacted On', candidate.last_contact_date_epoch, 'date') %]</div>
    </div>
</div>

[% IF candidate.campaign_attributes.size %]
    <br />
    <h4>[% L('Campaign Details') | html_entity %]</h4>
    <div class="line">
        [% FOR key IN candidate.campaign_attributes.keys %]
            <div class="unit size1of2">
                <div style="display: inline-block">
                    <strong>[% key | html_entity %]:</strong>
                </div>
                <div style="display: inline-block">
                    [% candidate.campaign_attributes.$key.join(', ') | html_entity %]
                </div>
            </div>
        [% END %]
    </div>
[% END %]

[% IF candidate.custom_fields.size %]
    <br />
    <h4>[% L('Custom Questions') | html_entity %]</h4>
    <div class="line">
        [% FOR key IN candidate.custom_fields.keys %]
            <div class="unit size1of2">
                <div style="display: inline-block">
                    <strong>[% key | html_entity %]:</strong>
                </div>
                <div style="display: inline-block">
                    [% candidate.custom_fields.$key.join(', ') | html_entity %]
                </div>
            </div>
        [% END %]
    </div>
[% END %]
<!-- end: [% component.name %] //-->

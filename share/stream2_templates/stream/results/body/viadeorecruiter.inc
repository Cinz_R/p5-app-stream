﻿<!-- start: [% component.name %] //-->
    <style type="text/css">
        td.dates {
            width: 95px;
            vertical-align: top;
        }
    </style>

    <div class="bd line">
        <div class="" style="float: right">
            [% IF candidate.has_picture == 'true' %]
                <img src="[% candidate.picture_medium %]" style="padding: 2px; border: 1px solid #E6E6E6; margin: 2px;"> <br />
            [% END %]
            <p>[% L('Connections') | html %]: <strong>[% candidate.num_connections || 0 | html %]</strong></p>
        </div>
        <div class="" style="">
            [% IF candidate.city %]<strong>[% L('Location') %]: </strong> [% candidate.city %][% IF candidate.country %], [% candidate.country %][%END%][%END%]
            <br /> 
        
            [% IF candidate.experience %] 
                <strong> [% L('Experience') %]: </strong> <br />
                <table>
                [% FOREACH experience IN candidate.experience %]
                    [% IF experience.company_name %]  
                        <tr>
                            [% IF experience.begin %]
                                [% SET begin = experience.begin %]
                            [% ELSE %]
                                [% SET begin = '' %]
                            [% END %]

                            [% IF experience.end %]
                                [% SET end = ' - ' _ experience.end %]
                            [% ELSIF loop.first () %]
                                [% SET end = ' - ' _ L('Current') %]
                            [% ELSIF NOT experience.end %]
                                [% SET end = '' %]
                            [% END %]
                            
                            <td class="dates"> [% begin %][% end %] </td>
                            [% IF experience.position %]
                                <td> [% experience.position %], [% experience.company_name %]</td>
                            [% ELSE %]
                                <td> [% experience.company_name %] </td>
                            [% END %]
                        </tr>
                    [% END %]
                    [% LAST IF loop.count > 2 %]
                [% END %]
                </table>
            [% END %]
            
            [% IF candidate.education.size %] 
                    <strong> [% L('Education') %]: </strong> <br />
                    <table>
                    [% FOREACH education IN candidate.education %]
                        <tr>
                        [% IF education.begin %]
                            [% SET begin = education.begin %]
                        [% ELSE %]
                            [% SET begin = '' %]
                        [% END %]

                        [% IF education.end %]
                            [% SET end = ' - ' _ education.end %]
                        [% ELSIF loop.first () %]
                            [% SET end = ' - ' _ L('Current') %]
                        [% ELSIF NOT education.end %]
                            [% SET end = '' %]
                        [% END %]
                    
                        <td class="dates"> [% begin %][% end %] </td>
                        [% IF education.field_of_expertise %]
                            <td> [% education.field_of_expertise %], [% education.school %]</td>
                        [% ELSIF education.degree %]
                            <td> [% education.degree %], [% education.school %], [% education.school_location %] </td>
                        [% ELSIF education.school %]
                            <td> [% education.school %], [% education.school_location %] </td>

                        [% END %]
                        </tr>
                        [% LAST IF loop.count > 2 %]
                    [% END %]
                    </table>
            [% END %]
        </div>

    </div>

<!-- end: [% component.name %] //-->

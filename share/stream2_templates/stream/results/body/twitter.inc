[% SET tuser = candidate.twitter_user %]
<div class="line" style="font-family: Helvetica">

    <div class="unit size1of2">

        <div style="
            width: 300px;
            overflow: hidden;
            border-radius: 5px;
            border: 1px solid rgb(225, 232, 237);
            margin: 0 auto;
        ">
            <div style="
                background-image: url('[% tuser.profile_banner_url | html_entity %]/300x100');
                background-size: 100% auto;
                background-color: #[% tuser.profile_link_color | html_entity %];
                height: 95px;
            "></div>
            <div style="padding: 0 15px;">
                <div style="
                    border-radius: 6px;
                    display: inline-block;
                    background: white;
                    padding: 3px;
                    margin-top: -50%;
                ">
                    <img
                        style="
                            border-radius: 5px;
                            width: 73px;
                            height: 73px;
                        "
                        src="[% tuser.profile_image_url_bigger_https | html_entity %]"
                    />
                </div>
                <br />
                <h1 style="font: 700 18px 'Helvetica Neue',Helvetica,Arial,sans-serif ">[% tuser.name | html_entity %]</h1>

                <h2 style="font: 400 12px 'Helvetica Neue',Helvetica,Arial,sans-serif;
                color: rgb(102, 117, 127) ; ">@[% tuser.screen_name | html_entity %]</h2>

                <p style="font: 400 14px 'Helvetica Neue',Helvetica,Arial,sans-serif;
                color: rgb(102, 117, 127) ; ">[% tuser.description_html %]</p>

                <div>
                    [% tuser.location | html_entity %]
                </div>
                <div>
                    Joined [% tuser.created_at_formatted | html_entity %]
                </div>
                <div>[% tuser.url_html %]</div>


            </div>
        </div>
    </div>

    <div class="unit size1of2">

        [%# Statistics row %]
        <div style="text-align: center; padding-bottom: 20px;">

            <div style="float: left; width: 25%;">
                <span style="font: 11px Helvetica">TWEETS</span>
                <br />
                <span style="color: rgb(41, 47, 51); font: 500 18px Helvetica">
                    [% tuser.statuses_count_formatted | html_entity %]
                </span>
            </div>

            <div style="float: left; width: 25%;">
                <span style="font: 11px Helvetica">FOLLOWING</span>
                <br />
                <span style="color: rgb(41, 47, 51); font: 500 18px Helvetica">
                    [% tuser.friends_count_formatted | html_entity %]
                </span>
            </div>

            <div style="float: left; width: 25%;">
                <span style="font: 11px Helvetica">FOLLOWERS</span>
                <br />
                <span style="color: rgb(41, 47, 51); font: 500 18px Helvetica">
                    [% tuser.followers_count_formatted | html_entity %]
                </span>
            </div>

            <div style="float: left; width: 25%;">
                <span style="font: 11px Helvetica">LIKES</span>
                <br />
                <span style="color: rgb(41, 47, 51); font: 500 18px Helvetica">
                    [% tuser.favourites_count_formatted | html_entity %]
                </span>
            </div>

            <!--
            <div style="float: left; margin: 0 5px">
                <span style="font: 11px Helvetica">Verified:</span>
                <br />
                <span style="font: 500 18px Helvetica">1</span>
            </div>
           -->

            <div style="clear:both;"></div>
        </div>

        [% SET tweet = tuser.status %]

        [% IF tweet %]

            [% SET author = tuser %]

            [% IF tweet.retweeted_status %]
                [% SET author = tweet.retweeted_status.user %]
                [% tweet = tweet.retweeted_status %]
                [% SET retweet = 1 %]
            [% END %]
            <!--<a style="display: block"
            href="[% author.screen_name %]/status/[% tweet.id_str %]">-->
            <article style="
            border: 1px solid rgb(170,184,194);
            border-radius: 2px;
            padding: 20px 20px 11.6px;
            max-width: 500px;
            min-width: 200px;
            font-family: Helvetica;
            color: black;
            ">
                [% IF retweet %]
                    <div style="
                    width: 15px;
                    height: 18px;
                    display: inline-block;
                    background-size: contain;
                    vertical-align: bottom;
                    background-image: url('data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20width%3D%2260%22%20height%3D%2272%22%20viewBox%3D%220%200%2060%2072%22%3E%3Cpath%20class%3D%22icon%22%20fill%3D%22%2319cf86%22%20d%3D%22M49%209H11c-4.418%200-8%203.582-8%208v38c0%204.418%203.582%208%208%208h38c4.418%200%208-3.582%208-8V17c0-4.418-3.582-8-8-8zM21%2044h10c1.657%200%203%201.343%203%203s-1.343%203-3%203H17c-1.657%200-3-1.343-3-3V36H9c-.77%200-1.47-.44-1.803-1.134-.333-.692-.24-1.516.24-2.115l8-10c.76-.947%202.365-.947%203.124%200l8%2010c.48.6.576%201.425.243%202.117C26.47%2035.56%2025.77%2036%2025%2036h-5v7c0%20.553.448%201%201%201zm31.562-4.75l-8%2010c-.38.474-.954.75-1.562.75s-1.182-.276-1.562-.75l-8-10c-.48-.6-.574-1.424-.24-2.116C33.53%2036.44%2034.23%2036%2035%2036h5v-7c0-.553-.447-1-1-1H29c-1.657%200-3-1.343-3-3s1.343-3%203-3h14c1.657%200%203%201.343%203%203v11h5c.77%200%201.47.44%201.803%201.134.333.692.24%201.515-.24%202.115z%22%2F%3E%3C%2Fsvg%3E')">

                    </div>
                    [% tuser.name %] Retweeted
                [% END %]
                <div>
                    <a target="_blank" href="http://twitter.com/[% author.screen_name %]"
                        style="
                            border-radius: 3px;
                            width: 48px;
                            height: 48px;
                            margin-right: 10px;
                        "
                    >
                    <img
                        src="[% author.profile_image_url_https | html_entity %]"
                    />
                    </a>
                    <div style="display: inline-block; vertical-align: top">
                        <a target="_blank" href="http://twitter.com/[% author.screen_name %]" style="color: black; font: 700 18px Helvetica">
                            [% author.name | html_entity %]
                        </a>
                        <br />
                        <a target="_blank" href="http://twitter.com/[% author.screen_name %]" style="font: 400 12px Helvetica;
                        color: grey">@[% author.screen_name |html_entity %]</a>
                    </div>
                    <a target="_blank" href="http://twitter.com/[% author.screen_name %]/status/[% tweet.id_str %]" style="float: right">
                        <img style="float: right" src="https://g.twimg.com/dev/documentation/image/Twitter_logo_blue_48.png" />
                        <div style="clear: both;"></div>
                    </a>
                </div>

                <!-- Tweet body -->
                <div>
                    <p style="margin: 5px 0; font: 400 16px Helvetica">
                        [% tweet.html_text %]
                    </p>
                    <span style="font: 400 12px Helvetica; color: grey">
                        [% tweet.created_at_formatted | html_entity %]
                    </span>
                </div>

            </article>
        [% ELSE %]
            <i>None</i>
        [% END %]
    </div>
</div>
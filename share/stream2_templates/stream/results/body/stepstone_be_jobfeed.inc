<div class="bd line">
    <div class="size1of2 unit">
        [% IF candidate.last_activity %] <p> <strong>[% i18n("Last activity") %]:</strong> <span>[% candidate.last_activity %]</span> </p> [% END %]
        [% IF candidate.location %] <p> <strong>[% i18n("Location") %]:</strong> <span>[% candidate.location %]</span> </p> [% END %]
        [% IF candidate.age %] <p> <strong>[% i18n("Age") %]:</strong> <span>[% candidate.age %]</span> </p> [% END %]
        [% IF candidate.education %] <p> <strong>[% i18n("Highest qualification") %]:</strong> <span>[% candidate.education %]</span> </p> [% END %]
        [% IF candidate.skills %] <p> <strong>[% i18n("Skills") %]:</strong> <span>[% candidate.skills %]</span> </p> [% END %]
    </div>
    <div class="size1of2 unit">
        [% IF candidate.last_exp_title %] <p> <strong>[% i18n("Last job title") %]:</strong> <span>[% candidate.last_exp_title %]</span> </p> [% END %]
        [% IF candidate.last_exp_years %] <p> <strong>[% i18n("Last job duration") %]:</strong> <span>[% candidate.last_exp_years %]</span> </p> [% END %]
        [% IF candidate.last_exp_duration %] <p> <strong>[% i18n("Last job from/to") %]:</strong> <span>[% candidate.last_exp_duration %]</span> </p> [% END %]
        [% IF candidate.preferred_location %] <p> <strong>[% i18n("Preferred location") %]:</strong> <span>[% candidate.preferred_location %]</span> </p> [% END %]
        [% IF candidate.preferred_position %] <p> <strong>[% i18n("Preferred position") %]:</strong> <span>[% candidate.preferred_position %]</span> </p> [% END %]
    </div>
</div>

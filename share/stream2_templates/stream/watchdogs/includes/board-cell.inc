[% IF APP.debug %]<!-- start: [% component.name %] //-->[% END %]

[% IF board && board.name %]
    [% WRAPPER 'stream/wrappers/cell.inc' id="div_${board.name}" %]
        <div class="bd">
            <p>
                <input [% IF preselected.${board.name} == 'mandatory' %]type="hidden"[% ELSE %]type="checkbox"[% END %] name="destination" id="destination_[% board.name %]" value="[% board.name %]" class="destination_cb [% IF board.cvsearch_keywords_required %]_keywords_mandatory[% END %]" [% IF !board.can_search || 1 %]disabled[% ELSIF preselected.${board.name} %]checked[% END %]>
                <label for="destination_[% board.name %]">[% board.stream_nice_name | html %][% IF board.blocked( APP.company ) %][% WRAPPER 'stream/wrappers/internal-only.inc' %] <span class="juice_bar">(internal only)</span>[% END %][% END %]</label>
            </p>
        </div>
        <div class="bd boardLogo">
            <p>[% IF board.logo %]<img src="http://adcourier.com/board_logos/[% board.logo %]" alt="[% board.stream_nice_name | html %]">[% ELSE %]&nbsp;[% END %]</p>
        </div>
        <div class="ft">
            [% IF board.can_search || 1 %]
                <p class="detailed-search-button">
                    <a href="[% APP.link_detailed_search( board.name, based_on.id ) %]" onclick="return false;" rel="destination" title="[% board.stream_nice_name | html %]" class="beanbox" id="detailed_search_[% board.name %]">
                        [% L('Search Options') | html %]
                    </a>
                </p>
            [% ELSE %]
                <p class="no_quota">[% IF board.quota %]([% board.quota %])[% ELSE %]&nbsp;[% END %]</p>
            [% END %]
        </div>
    [% END %]
    [% IF board.tokens %]
        <div id="div_[% board.name %]_tokens" class="boardTokens" style="display: none;">
            [% FOREACH token = board.tokens %]
                <div>
                    <p><label for="[% token.Board %]_[% token.Name | html %]">[% L(token.Label) | html %][% IF token.Mandatory %]<span class="required">([% L('required') | html %])</span>[% END %]</label></p>
                    [% INCLUDE 'stream/tokens/default.inc' %]
                </div>
            [% END %]
        </div>
    [% END %]
[% END %]

[% IF APP.debug %]<!-- end: [% component.name %] //-->[% END %]

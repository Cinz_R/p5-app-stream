<div class="bd line">

    <div class="size2of2">
        [% IF candidate.jobtitle %] <p> <strong> [% L('Job title') %]: </strong>[% candidate.jobtitle %] </p> [% END %]
        [% IF candidate.job_discipline %] <p> <strong> [% L('Job discipline') %]: </strong>[% candidate.job_discipline %] </p> [% END %]
        [% IF candidate.sectors %] <p> <strong> [% L('Sectors') %]: </strong>[% candidate.sectors %] </p> [% END %]
        [% IF candidate.work_type %] <p> <strong> [% L('Job type(s)') %]: </strong>[% candidate.work_type %] </p> [% END %]
        [% IF candidate.location %] <p> <strong> [% L('Home location') %]: </strong>[% candidate.location %] </p> [% END %]
        [% IF candidate.desired_salary %] <p> <strong> [% L('Desired salary') %]: </strong>[% candidate.desired_salary %] </p> [% END %]
    </div>

    [% IF candidate.summary %]
        <div class="clear"></div>
        <div>
            <p><strong>[% L('Summary') %]</strong></p>
            [% candidate.summary %]
        </div>
    [% END %]

    <div class="clear"></div>
    [% IF candidate.cv_preview %]
        <div class="cv_preview">
            <p><strong>[% L('CV Preview') %]</strong></p>
            [% candidate.cv_preview %]
        </div>
    [% ELSE %]
        <p>[% L('No CV preview available') | html %]</p>
    [% END %]

</div>

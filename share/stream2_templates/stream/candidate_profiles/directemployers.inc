<div class="line">
    <h4>Quick Facts</h4>
    <ul>
    [% FOREACH quick_fact IN candidate.quick_facts %]
        <li>[% quick_fact | html_entity %]</li>
    [% END %]
    </ul>
</div>
<br />
<div class="line">
    [% candidate.html_cv %]
</div>
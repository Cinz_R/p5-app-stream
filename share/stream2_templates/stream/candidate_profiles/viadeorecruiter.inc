﻿<!-- start: [% component.name %] //-->
    <style type="text/css">
        td.dates {
            width: 95px;
            vertical-align: top;
        }
        
        /*[%# NOTA BENE!!! Styles get re-applied via JS at the bottom. The reason
           for this is because IE won't interpret invalid style tags that come
           back as part of an AJAX request.
           The CSS is duplicated here as well, so that you don't get a
           'flicker' effect when the styles are applied after loading in Chrome,
           FF, et al.
           TODO FIXME %]*/
        .viadeo_preview {
            line-height: 1.5em;
        }
        .viadeo_preview hr {
            height: 1px;
            color: #cccccc;
            background-color: #cccccc;
            border: 0;
            margin-top: 0px;
        }
        .viadeo_preview h3 {
            font-size: larger;
            font-weight: bold;
        }
        .viadeo_preview td {
            padding-top: 10px;
        }
    </style>

    <div class="bd line">
        <div class="" style="float: right">
            [% IF candidate.has_picture == 'true' %]
                <img src="[% candidate.picture_medium %]" style="padding: 2px; border: 1px solid #E6E6E6; margin: 2px;"> <br />
            [% END %]
            <p>[% L('Connections') | html %]: <strong>[% candidate.num_connections || 0 | html %]</strong></p>

            [% IF candidate.distance == 1 %]
            [% END %]
        </div>
        <div class="viadeo_preview" style="">
            <br/>
            [% IF candidate.is_premium %]<h2><strong>[% L('Viadeo Recruiter') %]</strong></h2>[%END%]
            <p>
                <strong>[% L('Name') %]: </strong>
                [% IF candidate.name %]
                    [% candidate.name %]
                [% ELSIF candidate.first_name %]
                    [% candidate.first_name %] [% candidate.last_name %]
                [% END %]
            </p>
            [% IF candidate.headline %] <p> <strong> [% L('Headline') %]: </strong> [% candidate.headline %] </p> [% END %]
            [% IF candidate.city %]<p><strong>[% L('Location') %]: </strong> [% candidate.city %][% IF candidate.country %], [% candidate.country %]</p>[%END%][%END%]
            [% IF candidate.gender ~%]
                <p>
                    <strong>[% L('Gender') %]: </strong>
                    [% candidate.gender == 'M' ? L('Male') : L('Female') %]
                </p>
            [%~ END ~%]
            [% IF candidate.interests %]<p><strong>[% L('Interests') %]: </strong> [% candidate.interests%]</p>[%END%]
            [% IF candidate.language %]<p><strong>[% L('Language') %]: </strong> [% L(candidate.language) %]</p>[%END%]

            [% IF candidate.snippet.size %]
                <br/>
                [% FOREACH snippet_line IN candidate.snippet %]
                    <p> [% snippet_line %]</p>
                [% END %]
            [% END %]
            <br/>
            [% IF candidate.experience %]
            <h3> [% L('Experience') %]</h3>
            <hr />
            <table>
            [% FOREACH experience IN candidate.experience %]
                [% IF experience.company_name %] 
                    <tr>
                        [% IF experience.begin %]
                            [% SET begin = experience.begin %]
                        [% ELSE %]
                            [% SET begin = '' %]
                        [% END %]

                        [% IF experience.end %]
                            [% SET end = ' - ' _ experience.end %]
                        [% ELSIF loop.first () %]
                            [% SET end = ' - ' _ L('Current') %]
                        [% ELSIF NOT experience.end %]
                            [% SET end = '' %]
                        [% END %]
                        
                        <td class="dates"> [% begin %][% end %] </td>
                        <td>
                        [% IF experience.position %]
                            <strong> [% experience.position %], [% experience.company_name %] </strong>
                        [% ELSE %]
                            <strong> [% experience.company_name %] </strong>
                        [% END %]
                        [% FOREACH desc IN experience.description %]
                            <br />[% desc %]
                        [% END %]
                        </td>
                    </tr>
                [% END %]
            [% END %]
            </table>
            <br/>
             [% IF candidate.education.size %] 
             <h3> [% L('Education') %]</h3>
             <hr />
             <table>
                    [% FOREACH education IN candidate.education %]
                        <tr>
                        [% IF education.begin %]
                            [% SET begin = education.begin %]
                        [% ELSE %]
                            [% SET begin = '' %]
                        [% END %]

                        [% IF education.end %]
                            [% SET end = ' - ' _ education.end %]
                        [% ELSIF loop.first () %]
                            [% SET end = ' - ' _ L('Current') %]
                        [% ELSIF NOT education.end %]
                            [% SET end = '' %]
                        [% END %]

                        <td class="dates"> [% begin %][% end %] </td>
                        [% IF education.field_of_expertise %]
                            <td> <strong>[% education.school %]</strong><br />[% education.field_of_expertise %]</td>
                        [% ELSIF education.degree %]
                            <td> <strong>[% education.school %]</strong><br />[% education.degree %], [% education.school_location %] </td>
                        [% ELSIF education.school %]
                            <td> <strong>[% education.school %]</strong><br />[% education.school_location %] </td>

                        [% END %]
                        </tr>
                    [% END %]
            [% END %]
            </table>
            [% IF candidate.keywords.size %]
                <br/>
                <h3>[% L('Keywords') %]</h3>
                <hr />
                <p>[% candidate.keywords.join(', ') %]</p>
            [% END %]
            [% IF candidate.groups.size %]
                <br/>
                <h3>[% L('Communities') %]</h3>
                <hr />
                <p>[% candidate.groups.join(', ') %]</p>
            [% END %]
            <br/>
        [% END %]
        </div>

    </div>

    <script type="text/javascript">
        //[%# See NOTA BENE section at top! %]
        $$('.viadeo_preview').invoke('setStyle', 'line-height: 1.5em');
        $$('.viadeo_preview hr').invoke('setStyle',
            'height: 1px;' +
            'color: #cccccc;' +
            'background-color: #cccccc;' +
            'border: 0;' +
            'margin-top: 0px;');
        $$('.viadeo_preview h3').invoke('setStyle',
            'font-size: larger;' +
            'font-weight: bold;');
        $$('.viadeo_preview td').invoke('setStyle', 'padding-top: 10px');
    </script>

<!-- end: [% component.name %] //-->

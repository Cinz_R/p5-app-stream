[% SET profile = candidate.profile %]
[% MACRO simple_field(title, value) BLOCK %]
    [% IF value %]
        <div>
            <strong>[% L(title) | html_entity %]:</strong>
            [%# inline-block makes it word-wrap as a whole %]
            <div style="display: inline-block">
                [% value.join(', ') | html_entity %]
            </div>
        </div>
    [% END %]
[% END %]

[% MACRO print_codes(codeValues) BLOCK %]
[% END %]

[% IF candidate.last_modified %]
    <div>
        <strong>[% L('Last Modified') %]:</strong>
        [% candidate.last_modified | html_entity %]
    </div>
[% END %]

[% simple_field('Location', candidate.location_text) %]
[% simple_field('Regions', profile.RegionCodes) %]
[% simple_field('Working hours', profile.WorkingHourRangeCodes) %]
[% simple_field('Sectors', profile.IndustryCodes) %]
[% simple_field('Career Level', profile.CareerLevel) %]
[% simple_field('Job types', profile.JobTypeCodes) %]
[% simple_field('Availability', profile.AvailabilityCodes) %]
[% simple_field('Salary', profile.SalaryRangeCodes) %]
[% simple_field('Position Group', profile.PositionGroupCodes) %]
[% simple_field('Position', profile.PositionCodes) %]
[% simple_field('Drivers licence', profile.DriverLicenseTypeCodes) %]
[% simple_field('Languages', profile.LanguageKnowledgeCodes) %]

[% IF profile.Educations %]
    <h4>[% L('Education') %]</h4>
    [% simple_field('Education level', profile.EducationLevelCodes) %]
    [% simple_field('Education type', profile.EducationTypeCodes) %]

    [% FOR education IN profile.Educations %]
        <p>[% education.Value | html_entity %]</p>
    [% END %]
[% END %]

[% IF profile.Experiences %]
    <h4>[% L('Experience') %]</h4>
    [% FOR experience IN profile.Experience %]
        <p>[% experience.Value | html_entity %]</p>
    [% END %]
[% END %]


[% IF profile.CvFileContent %]
    <br />
    <h2>[% L('CV Preview') %]</h2>
    <br />
    <pre>[% profile.CvFileContent | html_entity %]</pre>
[% ELSE %]
    [% IF profile.CvContent %]
        [% profile.CvContent | html_sandbox %]
    [% END %]
[% END %]
[% SET profile = candidate.profile %]
[% SET desired = profile.desiredEmployment %]

<div style="background: #CCC; display: inline-block;
    border-radius: 100px; position: relative; width: 100px; height: 100px;">
    <img
        src="[% profile.imageUrl | html_entity %]"
        height="100px"
        width="100px"
        style="position: relative; z-index: 2"
        onerror="this.style.visibility = 'hidden';"
    />
    <div style="position: absolute; left: 50%; top: 50%;
        transform: translate(-50%, -50%); font-size: 25px;
        color: white">
        [% profile.initials.join('') | html_entity %]
    </div>
</div>

<div class="line">
    <div class="unit size1of2">
        <h4>Current</h4>
        [% SET job = profile.mostRecentEmployment %]
        [% IF job.jobTitle %]
            <strong>[% job.jobTitle | html_entity %]</strong><br />
        [% END %]
        [% IF job.employerName %]
            <div>
                <strong>[% L('Employer') | html_entity %]:</strong>
                [% job.employerName | html_entity %]
            </div>
        [% END %]
        [% IF profile.contact.email %]
            <div>
                <strong>[% L('Email') | html_entity %]:</strong>
                [% profile.contact.email | html_entity %]
            </div>
        [% END %]
        [% IF profile.contact_telephone %]
            <div>
                <strong>[% L('Telephone ') | html_entity %]:</strong>
                [% profile.contact_telephone | html_entity %]
            </div>
        [% END %]
        [% IF profile.location %]
            <div>
                <strong>[% L('Location') %]:</strong>
                [% profile.location | html_entity %]
            </div>
        [% END %]
        [% IF desired.yearsTechExperience %]
            <div>
                <strong>[% L('Experience') | html_entity %]:</strong>
                [% desired.yearsTechExperience | html_entity %] yrs
            </div>
        [% END %]

        [% IF profile.dateAvailable %]
            <strong>[% L('Available from') | html_entity %]:</strong>
            [% profile.dateAvailable | html_entity %]
        [% END %]

        [% IF desired.workAuthorization %]
            <div>
                <strong>[% L('Eligibility') | html_entity %]:</strong>
                [% desired.workAthorization | html_entity %]
            </div>
        [% END %]
        [% IF desired.veteranStatus %]
            <div>
                <strong>[% L('Veteran') | html_entity %]:</strong>
                [% desired.veteranStatus ? 'Yes' : 'No' %]
            </div>
        [% END %]
        [% IF desired.securityClearance %]
            <div>
                <strong>[% L('Security Clerance') | html_entity %]:</strong>
                [% desired.securityClearance ? 'Yes' : 'No' %]
            </div>
        [% END %]
        [% IF profile.datePosted %]
            <div>
                <strong>Profile was created on:</strong>
                [% profile.datePosted | html_entity %]
            </div>
        [% END %]
        [% IF profile.lastModified %]
            <div>
                <strong>Last modified</strong>
                [% profile.lastModified | html_entity %]
            </div>
        [% END %]
    </div>
    [% IF desired %]
        <div class="unit size1of2">
            <h4>Desired</h4>
            [% IF profile.desiredPosition %]
                <div<strong>[% profile.desiredPosition %]</strong></div>
            [% END %]
            [% IF desired.job_types.size %]
                <div>
                    <strong>[% L('Job Types') | html_entity %]:</strong>
                    [% desired.job_types.join(', ') | html_entity  %]
                </div>
            [% END %]
            [% IF desired.hourly %]
                <div>
                    <strong>[% L('Hourly pay rate') | html_entity %]:</strong>
                    [% desired.hourly | html_entity %]
                </div>
            [% END %]
            [% IF desired.annual %]
                <div>
                    <strong>[% L('Annual pay rate') | html_entity %]:</strong>
                    [% desired.annual | html_entity %]
                </div>
            [% END %]
            [% IF profile.preferredLocationList %]
                <div>
                    <strong>[% L('Preferred Locations') | html_entity %]:</strong>
                    [% profile.preferredLocationList.join(', ') | html_entity %]
                </div>
            [% END %]
            <div>
                <strong>[% L('Relocation') | html_entity %]:</strong>
                [% profile.willingToRelocate ? 'Yes' : 'No' %]
            </div>
            <div>
                <strong>[% L('Telecommute') | html_entity %]:</strong>
                [% profile.willingToTelecommute ? 'Yes' : 'No' %]
            </div>
        </div>
    [% END %]
</div>



[% IF profile.educationList.size %]
    <h4>Education</h4>
    [% FOR education IN profile.educationList %]
        [% SET parts = [
            education.type,
            education.institution,
            education.location
        ] %]
        <div>
            [% parts.grep('.').join(', ') | html_entity %]
        </div>
    [% END %]
[% END %]

[% IF profile.employmentHistoryList %]
    <h4>Work History</h4>
    [% FOR job IN profile.employmentHistoryList %]
        [% SET job_parts = [ job.jobTitle, job.employerName, job.dateRange] %]
        [% SET job_parts = job_parts.grep('.') %]
        <div>
            <strong>[% job_parts.shift %]</strong>
            [% job_parts.join(', ') %]
        </div>
    [% END %]
[% END %]
<hr />
<p>[% profile.bio | html_entity %]</p>

<h4>Skills</h4>
<div>
    [% FOREACH skill IN profile.skillList %]
        <span class="pill" style="background-color: #1b75bb; color: white">
            [% skill.name | html_entity %]
            [% IF skill.years %]
                ([% skill.years | html_entity %] yrs)
            [% END %]
        </span>
    [% END %]
    <div class="clearfix"></div>
</div>
[% IF profile.webProfiles.size %]
    <div style="margin: 10px 0;">
        <h4>Web Profiles</h4>
        [% FOR social IN profile.webProfiles %]
            <a
                target="_blank"
                href="[% social.url | html_entity %]"
                style="display: inline-block"
            >
                <img
                    alt="[% social.network | html_entity %]"
                    title="[% social.network | html_entity %]"
                    width="50px"
                    height="50px"
                    src="[% social.bigIconSecure | html_entity %]"
                />
            </a>
        [% END %]
    </div>
[% END %]
[% IF profile.resume.resumeText %]
    <h4>CV</h4>
    [% profile.resume.resumeText | html_sandbox %]
[% END %]
<!-- end: [% component.name %] //-->

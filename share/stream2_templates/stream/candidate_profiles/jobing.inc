<!-- start: [% component.name %] //-->

<h4>Summary</h4>

<strong>Last Viewed:</strong>
[% IF candidate.last_viewed %]
    My Last View: [% candidate.last_viewed | html_entity %]
[% ELSE %]
    New
[% END %]
<br />

<strong>Last Updated:</strong>
[% candidate.last_updated | html_entity %]
<br />

[% IF candidate.email %]
    <strong>Email Address:</strong>
    [% candidate.email | html_entity %]<br />
[% END %]

[% IF candidate.phones.size %]
    <strong>Phone Numbers:</strong><br />
    [% FOR phone_contact IN candidate.phones %]
        <i>[% phone_contact.phone_type %]</i>:
        [% phone_contact.phone_number %]
        <br />
    [% END %]
[% END %]

[% IF candidate.career_level %]
    <strong>Experience:</strong>
    [% candidate.career_level | html_entity %]
    <br />
[% END %]
[% IF candidate.education_level %]
    <strong>Education:</strong>
    [% candidate.education_level | html_entity %]
    <br />
[% END %]
[% IF candidate.job_statuses.size %]
    <strong>Job Types:</strong>
    [% candidate.job_statuses.join(', ') | html_entity %]
    <br />
[% END %]
[% IF candidate.socials.size %]
    <strong>Socials:</strong><br />
    [% FOR social IN candidate.socials %]
        <i>[% social.type | html_entity %]:</i>
        <a href="[% social.link | html_entity %]">[% social.link | html_entity %]</a>
        <br />
    [% END %]
    <br />
[% END %]

[% IF candidate.sectors.size %]
    <strong>Sectors:</strong><br />
    [% candidate.sectors.join(', ') | html_entity %]
    <br />
[% END %]

[% IF candidate.city_stateabbr %]
    <strong>Location: </strong>
    [% candidate.city_stateabbr | html_entity %]
    <br />
[% END %]

[% salaries = [] %]
[% IF candidate.hourly_rate %]
    [% salaries.push("\$${candidate.hourly_rate}/hr") %]
[% END %]
[% IF candidate.salary_per_year %]
    [% annual = candidate.salary_per_year %]
    [% annual = annual.replace('000$', 'k') %]
    [% salaries.push("\$${annual}/yr") %]
[% END %]
[% IF salaries.size %]
    <strong>Salary:</strong>
    [% salaries.join(' - ') | html_entity %]
    <br />
[% END %]

[% IF candidate.cv_preview_html %]
    <h4>CV</h4>
    <div style="position: relative; overflow: hidden";>
        [% candidate.cv_preview_html %]
    </div>
[% END %]

<!-- end: [% component.name %] //-->

<style>
    .clear {
        clear: both;
    }
</style>
<div class="bd line">
    <div class="size1of2 unit">
        [% IF candidate.email %] <p> <strong>[% i18n("Email") %]:</strong> <span>[% candidate.email %]</span> </p> [% END %]
        [% IF candidate.address %] <p> <strong>[% i18n("Address") %]:</strong> <span>[% candidate.address %]</span> </p> [% END %]
        [% IF candidate.phone_numbers %] <p> <strong>[% i18n("Contact number(s)") %]:</strong> <span>[% candidate.phone_numbers %]</span> </p> [% END %]
    </div>
    <div class="clear">
        [% IF candidate.profile_content %] <p> [% candidate.profile_content %] </p> [% END %]
    </div>
</div>

<!-- start: [% component.name %] //-->
 
<div class="bd line">

    <div class="size1of2 unit">
        [% IF candidate.title %] <p> <strong> [% L('Title') %]: </strong> [% candidate.title | html%] </p> [% END %]
        [% IF candidate.full_name %] <p> <strong> [% L('Full Name') %]: </strong> [% candidate.full_name | html%] </p> [% END %]
        [% IF candidate.email %] <p> <strong> [% L('Email') %]: </strong> [% candidate.email | html%] </p> [% END %]
        [% IF candidate.address1 %] <p> <strong> [% L('Address1') %]: </strong> [% candidate.address1 | html%] </p> [% END %]
        [% IF candidate.address2 %] <p> <strong> [% L('Address2') %]: </strong> [% candidate.address2 | html%] </p> [% END %]
        [% IF candidate.town %] <p> <strong> [% L('Town') %]: </strong> [% candidate.town | html%] </p> [% END %]
        [% IF candidate.postcode %] <p> <strong> [% L('Postcode') %]: </strong> [% candidate.postcode | html%] </p> [% END %]
        [% IF candidate.country %] <p> <strong> [% L('Country') %]: </strong> [% candidate.country | html%] </p> [% END %]
        [% IF candidate.region %] <p> <strong> [% L('Region') %]: </strong> [% candidate.region | html%] </p> [% END %]
        [% IF candidate.telephone %] <p> <strong> [% L('Telephone') %]: </strong> [% candidate.telephone | html%] </p> [% END %]
        [% IF candidate.medical_certificate_expiry_date %] <p> <strong> [% L('Medical Certificate Expiry Date') %]: </strong> [% candidate.medical_certificate_expiry_date | html%] </p> [% END %]
        [% IF candidate.survival_certificate_expiry_date %] <p> <strong> [% L('Survival Certificate Expiry Date') %]: </strong> [% candidate.survival_certificate_expiry_date | html%] </p> [% END %]
        [% IF candidate.quick_description %] <p> <strong> [% L('Quick Description') %]: </strong> [% candidate.quick_description | html %] </p> [% END %]
        [% IF candidate.date_created %] <p> <strong> [% L('Date Created') %]: </strong> [% candidate.date_created | html%] </p> [% END %]
        [% IF candidate.date_updated %] <p> <strong> [% L('Date Updated') %]: </strong> [% candidate.date_updated | html%] </p> [% END %]
        [% IF candidate.last_visited %] <p> <strong> [% L('Last Visited') %]: </strong> [% candidate.last_visited | html%] </p> [% END %]
        [% IF candidate.date_of_birth %] <p> <strong> [% L('Date Of Birth') %]: </strong> [% candidate.date_of_birth | html%] </p> [% END %]
        [% IF candidate.current_job_title %] <p> <strong> [% L('Current Job Title') %]: </strong> [% candidate.current_job_title | html%] </p> [% END %]
        [% IF candidate.minimum_salary_required %] <p> <strong> [% L('Minimum Salary Required') %]: </strong> [% candidate.minimum_salary_required | html%] </p> [% END %]
        [% IF candidate.minimum_hourly_rate_required %] <p> <strong> [% L('Minimum Hourly Rate Required') %]: </strong> [% candidate.minimum_hourly_rate_required | html%] </p> [% END %]
        [% IF candidate.locations.size %] <p> <strong> [% L('Preferred Job Locations') %]: </strong> [% candidate.locations.join(", ") | html%] </p> [% END %]
        [% IF candidate.skills %] <p> <strong> [% L('Skills') %]: </strong> [% candidate.skills | html%] </p> [% END %]
    </div>

    <div class="size1of2 unitExt">
        [% IF candidate.nationality %] <p> <strong> [% L('Nationality') %]: </strong> [% candidate.nationality | html%] </p> [% END %]
        [% IF candidate.qualifications %] <p> <strong> [% L('Qualifications') %]: </strong><br> [% candidate.qualifications | html%] </p> [% END %]
        [% IF candidate.employment_history %] <p> <strong> [% L('Employment History') %]: </strong><br> [% candidate.employment_history | html%] </p> [% END %]
     </div>

    <div style="clear:both;"></div>
</div>
<!-- end: [% component.name %] //-->

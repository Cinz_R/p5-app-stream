[% IF candidate.has_done_delete %]

    <strong>[% L('This candidate is awaiting for deletion from the source. It should disappear from these results shortly.') %]</strong>

[% ELSE %]

    <div class="cv_preview">
      [% candidate.cv_html %]
    </div>

[% END %]

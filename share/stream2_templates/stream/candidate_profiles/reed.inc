<!-- start: [% component.name %] //-->
<p><a class="btn btn-success" id="reed_cv" onclick="
var toScroll = $('#profile-content');
var reedTarget = $('#reed_full_profile');
toScroll.scrollTop(
    reedTarget.offset().top - toScroll.offset().top + toScroll.scrollTop()
);
">Reed Profile</a></p>
<h1>CV</h1>
[% IF candidate.cv_html %]
    [% candidate.cv_html %]
[% ELSE %]
    <p><i>CV unavailable.</i></p>
[% END %]
<hr />

<p><a class="btn btn-success" id="reed_full_profile"  onclick="
var toScroll = $('#profile-content');
var reedTarget = $('#reed_cv');
toScroll.scrollTop(
    reedTarget.offset().top - toScroll.offset().top + toScroll.scrollTop()
);
">[% L('Reed CV') | html_entity %]</a></p>
<h1>[% L('Reed Profile') | html_entity %] </h1>

[% IF candidate.name %]
    <p>
        <strong>[% L('Name') | html_entity %]:</strong>
        [% candidate.name | html_entity %]
    </p>
[% END %]
[% IF candidate.email %]
    <p>
        <strong>[% L('Email') | html_entity %]:</strong>
        [% candidate.email | html_entity %]
    </p>
[% END %]
[% IF candidate.phone %]
    <p>
        <strong>[% L('Phone') | html_entity %]:</strong>
        [% candidate.phone | html_entity %]
    </p>
[% END %]
[% IF candidate.address %]
    <p>
        <strong>[% L('Address') | html_entity %]:</strong>
        <br />
        [% candidate.address.join("\n") | html_entity | replace("\n", '<br />') %];
    </p>
[% END %]

<div class="line">
    <div class="unit size1of2">
        [% IF candidate.created_on %]
            <p>
                <strong>[% L('Created On') | html_entity %]:</strong>
                [% candidate.created_on | html_entity %]
            </p>
        [% END %]
    </div>
    <div class="unit size1of2">
        [% IF candidate.last_candidate_login %]
            <p>
                <strong>[% L('Last Login') | html_entity %]:</strong>
                [% candidate.last_candidate_login | html_entity %]
            </p>
        [% END %]
    </div>
</div>

<div class="bd line">
    <div class="unit size1of2">
        [% IF candidate.jobTitle %]
            <p>
                <strong>[% L('Job Titles') | html_entity %]:</strong>
                [% candidate.jobTitle | html_entity %]
            </p>
            [% END %]
        [% IF candidate.previousEmployer %]
            <p>
                <strong>[% L('Previous Employer') | html_entity %]:</strong>
                [% candidate.previousEmployer | html_entity %]
            </p>
        [% END %]
    </div>

    <div class="unit size1of2">
        [% IF candidate.highestQualification %]
            <p>
                <strong>[% L('Highest Qualification') | html_entity %]:</strong>
                [% candidate.highestQualification | html_entity %]
            </p>
        [% END %]
    </div>
</div>

<h4>Looking for</h4>
<div class="line">
    <div class="unit size1of2">
        <strong>[% L('Salary') | html_entity %]</strong><br />
        [% IF candidate.minimum_salary %]
            &pound;[% candidate.minimum_salary | html_entity %] [% L('per annum') | html_entity %]<br />
        [% END %]
        [% IF candidate.minimum_temp_rate %]
            &pound;[% candidate.minimum_temp_rate | html_entity %] [% L('per hour') | html_entity %]<br />
        [% END %]
    </div>
    <div class="unit size1of2">
        <strong>[% L('Job Types') | html_entity %]</strong><br />
        [% candidate.jobtypes.join(', ') | ucfirst | html_entity %]
    </div>
</div>
<div class="line">
    <div class="unit size1of2">
        <strong>[% L('Locations') | html_entity %]</strong><br />
        [% FOREACH preferredLocations IN candidate.preferredLocations %]
            [% preferredLocations | html_entity %]<br />
        [% END %]
    </div>
    <div class="unit size1of2">
        <strong>[% L('Sectors') %]</strong><br />
        [% FOR sector IN candidate.sectors %]
            [% IF sector.parentSecto %]
                <strong>[% sector.parentSector | html_entity %]</strong><br />
            [% END %]
            [% FOR subSector IN sector.subSectors %]
                [% subSector | html_entity %]<br />
            [% END %]
        [% END %]
    </div>
</div>

[% IF candidate.personalStatement %]
    <h4>[% L('Personal Statement') | html_entity %]</h4>
    <p>[% candidate.personalStatement | html_entity %]</p>
[% END %]
[% IF candidate.cvSnippets %]
    <h4>[% L('CV snippets') | html_entity %]</h4>
    [% FOR snippet IN candidate.cvSnippets %]
        <p>[% candidate.cvSnippets | html_entity %]</p>
    [% END %]
[% END %]

[% IF candidate.skills OR candidate.languages %]
    <h4>[% L('Skills') | html_entity %]</h4>
[% END %]
[% IF candidate.skills %]
    <ul style="list-style: none; padding: 0">
        [%- FOR skill IN candidate.skills -%]
[%# No whitespace to avoid word-spacing with inline-block %]
<li style="
    display: inline-block;
    background: #f5f5f5;
    padding: 2px 6px;
    margin: 1px;
">
    [% skill | html_entity %]
</li>
        [%- END -%]
    </ul>
[% END %]

[% IF candidate.languages%]
    <h5>[% L('Languages') | html_entity %]:</h5>
    <ul>
        [% FOREACH languages IN candidate.languages %]
            <li>[% languages.language | html_entity %] - [% languages.fluency | html_entity %]</li>
        [% END %]
    </ul>
[% END %]

[% IF candidate.workHistory %]
    <h4>[% L('Work History') | html_entity %]</h4>
    [% FOR workHistory IN candidate.workHistory %]
        <p>
            [% IF workHistory.jobTitle %]
                <strong>[% L('Job Title') | html_entity %]:</strong>
                [% workHistory.jobTitle | html_entity %]<br />
            [% END %]
            [% IF workHistory.companyName %]
                <strong>[% L('Company Name') | html_entity %]:</strong>
                [% workHistory.companyName | html_entity %]<br />
            [% END %]
            [% IF workHistory.dateFrom OR workHistory.dateTo %]
                <strong>[% L('Period') | html_entity %]:</strong>
                [% SET workDates = [workHistory.dateFrom, workHistory.dateTo] %]
                [% workDates.grep('.').join(' - ') | html_entity %]
                <br />
            [% END %]
            [% IF workHistory.jobDescription %]
                <strong>[% L('Job Description') | html_entity %]:</strong>
                [% workHistory.jobDescription | html_entity %]<br />
            [% END %]
        </p>
    [% END %]
[% END %]

<h4>Qualifications</h4>
[% FOR qualifications IN candidate.qualificationGroups %]
    <p>
        <strong>[% SET firstQualification = qualifications.first %]
        [% firstQualification.institutionName | html_entity %]<br />
        [% firstQualification.qualificationType | html_entity %]<br />
        [% SET period = [firstQualification.startedOn, firstQualification.finishedOn] %]
        [% period.grep('.').join(' - ') | html_entity %]
        <br />
        </strong>
        [% FOR qualification IN qualifications %]
            [% qualification.subject | html_entity %]
            [% IF qualification.degreeGrade %]
                ([% qualification.degreeGrade| html_entity %])
            [% END %]
            <br />
        [% END %]
    </p>
[% END %]
<!-- end: [% component.name %] //-->
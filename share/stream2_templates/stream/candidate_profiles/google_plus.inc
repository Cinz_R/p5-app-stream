<!-- start: [% component.name %] //-->
<style type="text/css">
    p.indent
    {
 	    text-indent: 40px;
 	}
    .google_plus_divs{
        border-style:solid;
        border-color:#F0F0F0;
        border-width:0.1em;
        border-top-width:thick;
        border-top-color:#66FF66;
        background-color:#F8F8F8; 
        width:95%;
        //float:left;
        display:inline-block;
        margin-left:1%;
        margin-bottom:1%;
        
    }
   

    .google_plus_divs div{
    margin-bottom:0.2em;
    border-bottom-width:0.2em;
    border-bottom-style:dotted;
    border-bottom-color:#F0F0F0; 
    }    
    

</style>
 
<div class="bd line">

	<div class="size1of2 unit">
            [% IF candidate.image_url %] <p><img src="[% candidate.image_url %]" /></p>[% END %]


         [% IF candidate.name %] <p><strong> [% L('Name') %]: </strong>[% candidate.name %]</p>[% END %]
            [% IF candidate.profile_url %] <p><strong> [% L('Profile link') %]: </strong><a href="[% candidate.profile_url %]" target="_blank">[% candidate.name %]</a></p>[% END %]

            [% IF candidate.currentlyWorks %] <p><strong> [% L('Works') %]: </strong>[% candidate.currentlyWorks %]</p>[% END %]
            [% IF candidate.previouslyWorked %] <p><strong> [% L('Worked') %]: </strong>[% candidate.previouslyWorked %]</p>[% END %]

            [% IF candidate.currentlyAttends %] <p><strong> [% L('Attends') %]: </strong>[% candidate.currentlyAttends %]</p>[% END %]
            [% IF candidate.previouslyAttended %] <p><strong> [% L('Attended') %]: </strong>[% candidate.previouslyAttended %]</p>[% END %]

            [% IF candidate.currentLocation %] <p><strong> [% L('Lives') %]: </strong> [% candidate.currentLocation %]</p>[% END %]
            [% IF candidate.previouslyLived %] <p><strong> [% L('Lived in') %]: </strong> [% candidate.previouslyLived %]</p>[% END %]


<br />

    </div>

	<div class="size1of2 unitExt">

   	 </div>

	<div style="clear:both;">
        
        <div class="size1of2 unit">

      [% IF candidate.work.size > 0 %] 
            
            <div class="google_plus_divs">
           <p><strong><u> [% L('Employment') %] </u> </strong></p>
            [% FOREACH work IN candidate.work %]
                <div>
                    <p><strong>[% work.name %]</strong> </p>
                    <p>[% work.title %] <i>[% work.startDate %] [%IF work.endDate OR work.primary %]- [% work.endDate %][%IF work.primary %]current[% END %][% END %]</i> </p>
                </div>
            [% END %]
            </div>
         [% END %]


         [% IF candidate.school.size > 0 %] 
            
            <div class="google_plus_divs">
           <p><strong><u> [% L('Education') %] </u> </strong></p>
            [% FOREACH school IN candidate.school %]
               <div> 
                    <p><strong>[% school.name %]</strong> </p>
                     <p>[% school.title %] <i>[% school.startDate %] [%IF school.endDate OR school.primary %]- [% school.endDate %][%IF school.primary %]current[% END %][% END %]</i> </p>
                </div>
            [% END %]
            </div>
         [% END %]




        [% IF candidate.aboutMe OR candidate.braggingRights OR candidate.tagline %]
        
            <div class="google_plus_divs">   
            
           <p><strong><u> [% L('Story') %] </u> </strong></p>
            
            [% IF candidate.tagline %] <p><strong> [% L('Tagline') %]: </strong> </p><p>[% candidate.tagline %]</p>[% END %]
            [% IF candidate.aboutMe %] <p><strong> [% L('About me') %]: </strong></p><p>[% candidate.aboutMe %]</p>[% END %]
            [% IF candidate.braggingRights %] <p><strong> [% L('Bragging rights') %]: </strong> </p><p>[% candidate.braggingRights %]</p>[% END %]

            </div>
        
        [% END %]
    


   


        </div>

        <div class="size1of2 unitExt">


     [% IF candidate.urls.size > 0 %]

        <div class="google_plus_divs"> 
           <p><strong><u>[% L('Links') %]</u></strong></p>
            
            [% FOREACH url IN candidate.urls %]
                <p><a href="[% url.value %]" target="_blank"">[% url.label %]</a></p>
            [% END %]
        
        </div>
        [% END %]


         [% IF candidate.placesLived.size > 0 %] 
            
            <div class="google_plus_divs">
           <p> <strong><u> [% L('Places') %] </u> </strong></p>
            [% FOREACH placesLived IN candidate.placesLived %]
               <div> 
                    <p><strong>[% placesLived.value %]</strong> 
                      [%IF placesLived.primary %]<i>- current</i>[% END %] </p>
                </div>
            [% END %]
            </div>
         [% END %]


        <div class="google_plus_divs">
             <p><strong><u>[% L('Basic information') %]</u></strong></p>
             [% IF candidate.gender %] <p> <strong> [% L('Gender') %]: </strong> [% candidate.gender | html%] </p> [% END %]
             [% IF candidate.relationshipStatus %] <p> <strong> [% L('Status') %]: </strong> [% candidate.relationshipStatus | html%] </p> [% END %]
             [% IF candidate.birthday %] <p> <strong> [% L('Birthday') %]: </strong> [% candidate.birthday | html%] </p> [% END %]

        </div>

       </div>


	</div>
</div>
<!-- end: [% component.name %] //-->

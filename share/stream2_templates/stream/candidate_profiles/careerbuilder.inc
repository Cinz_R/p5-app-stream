﻿<!-- start: [% component.name %] //-->
    <style type="text/css">
        td.indent
        {
            width: 40px;
        }
    </style>

<div class="bd line">
	<div class="size1of2 unit">
        <p><strong>[% L('Resume Title') | html %]:</strong> [% candidate.resume_title FILTER html %]</p>
        [% IF candidate.name %] <p> <strong> [% L('Name') %]: </strong> [% candidate.name | html%] </p> [% END %]
		[% IF candidate.email %]
        	<p><strong>[% L('Email') | html %]:</strong> <a href="mailto:[% candidate.email %]">[% candidate.email FILTER html %]</a></p>
		[% END %]
		[% IF candidate.phone %] <p> <strong> [% L('Phone') %]: </strong> [% candidate.phone | html%] </p> [% END %]
        <p><strong>[% L('Location') | html %]:</strong> [% candidate.location_text FILTER html %]</p>
		[% IF candidate.motivation_to_change_jobs %] 
	        <p><strong>[% L('Motivation to change jobs') | html %]:</strong> [% candidate.motivation_to_change_jobs FILTER html %]</p>
		[% END %]
        <!-- Education and Skills -->

        <p><strong>[% L('Highest degree') | html %]:</strong>
        [% candidate.highest_degree FILTER html %]</p>
		
		[% IF candidate.certifications %]
	        <p><strong>[% L('Certifications') | html %]:</strong>
    	    [% candidate.certifications FILTER html %]</p>
		[% END %]

		[% IF candidate.languages.size %]
	        <p><strong>[% L('Languages') | html %]:</strong>
    	    [% candidate.languages.join(', ') FILTER html %]</p>
		[% END %]

  </div>

  <div class="size1of2 unitExt">
        <p><strong>[% L('Management experience') | html %]:</strong>
        [% IF candidate.management_experience == 'Yes' %]
                [% L('[quant,_1,person]', candidate.management_number_managed) | html %]
            [% ELSE %]
                [% L('None') %]
            [% END %]
        </p>
		[% IF candidate.military_experience %]
	        <p><strong>[% L('Military Experience') | html %]:</strong>
    	    [% candidate.military_experience FILTER html %]</p>
		[% END %]

        <!-- Desired Job -->
		[% IF candidate.desired_salary_amount %]
	        <p><strong>[% L('Salary') | html %]:</strong>
    	    [% candidate.desired_salary_amount FILTER html %] per [% candidate.desired_salary_per FILTER html %]</p>
		[% END %]

		[% IF candidate.desired_shift_preferences %]
	        <p><strong>[% L('Shift preferences') | html %]:</strong>
    	    [% candidate.desired_shift_preferences.join(', ') FILTER html %]</p>
		[% END %]
         <!-- Previous Job History -->
		
		[% IF candidate.most_recent_title %]
	        <p><strong>[% IF candidate.currently_employed == 'Yes' %][% L('Current title') | html %][% ELSE %][% L('Most recent title') | html %][% END %]:</strong>
    	    [% candidate.most_recent_title FILTER html %]</p>
		[% END %]
		
		[% IF candidate.recent_salary_amount %]
        <p><strong>[% IF candidate.currently_employed == 'Yes' %][% L('Current salary') | html %][% ELSE %][% L('Recent salary') | html %][% END %]:</strong>
	        [% candidate.recent_salary_amount FILTER html %] per [% candidate.recent_salary_per FILTER html %]</p>
		[% END %]

        [% IF candidate.employer %] <p> <strong> [% L('Employer') %]: </strong> [% candidate.employer | html%] </p> [% END %]
        [% IF candidate.employment_type %] <p> <strong> [% L('Employment Type') %]: </strong> [% candidate.employment_type | html%] </p> [% END %]
        [% IF candidate.experience_months %] <p> <strong> [% L('Experience Months') %]: </strong> [% candidate.experience_months | html%] </p> [% END %]
        
        [% IF candidate.jobs_last_three_years %] <p> <strong> [% L('Jobs Last Three Years') %]: </strong> [% candidate.jobs_last_three_years | html%] </p> [% END %]
        [% IF candidate.travel_preference %] <p> <strong> [% L('Travel Preference') %]: </strong> [% candidate.travel_preference | html%] </p> [% END %]
        [% IF candidate.max_commute_miles %] <p> <strong> [% L('Max Commute Miles') %]: </strong> [% candidate.max_commute_miles | html%] </p> [% END %]
        <p><strong>[% L('Security Clearance') | html %]:</strong>
        [% candidate.security_clearance FILTER html %]</p>
        [% IF candidate.felony_convictions %] <p> <strong> [% L('Felony Convictions') %]: </strong> [% candidate.felony_convictions | html%] </p> [% END %]

	</div>
	<div style="clear:both;">
        [% IF candidate.relocations.size > 0 %]
            <br/>
            <p> <strong><u> [% L('Desired Relocation Locations') %] </u> </strong> </p>
            [% FOREACH relocations IN candidate.relocations %]
                [% IF relocations.city AND relocations.state AND relocations.country%] [% relocations.city | html%], [% relocations.state | html%], [% relocations.country | html%] </p> [% END %]
            [% END %]
        [% END %]
        [% IF candidate.desired_job_types.size %]
			<br/>
            <p><strong><u> [% L('Desired Job Types') | html %]:</u> </strong> </p>
            <p> [% candidate.desired_job_types.join(', ') FILTER html %]</p>
        [% END %]
        
        [% IF candidate.educationhistory.size > 0 %]
            <br/>
            <strong><u> [% L('Education History') %] </u> </strong>
            <table>
            [% FOREACH educationhistory IN candidate.educationhistory %]
                <tr> <td colspan=2> <strong> [% educationhistory.school %] </strong> </td> </tr>
                [% IF educationhistory.graduation_date %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Graduation Date') %]: </strong> [% educationhistory.graduation_date | html %] </td> </tr>
                [% END %]
                [% IF educationhistory.degree %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Degree') %]: </strong> [% educationhistory.degree | html %] </td> </tr>
                [% END %]
                [% IF educationhistory.major %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Major') %]: </strong> [% educationhistory.major | html %] </td> </tr>
                [% END %]
            [% END %]
            </table>
        [% END %]


        [% IF candidate.workhistory.size > 0 %]
            <br/>
            <strong><u> [% L('Work History') %] </u> </strong>
            <table>
            [% FOREACH workhistory IN candidate.workhistory %]
                <tr> <td colspan=2> <strong> [% workhistory.tenure %] </strong> </td> </tr>
                [% IF workhistory.jobtitle %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Jobtitle') %]: </strong> [% workhistory.jobtitle | html %] </td> </tr>
                [% END %]
                [% IF workhistory.company %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Company') %]: </strong> [% workhistory.company | html %] </td> </tr>
                [% END %]
            [% END %]
            </table>
        [% END %]

        [% IF candidate.interests.size > 0 %]
            <br/>
            <strong><u> [% L('Interests') %] </u> </strong>
            <table>
            [% FOREACH interests IN candidate.interests %]
                <tr> <td colspan=2> <strong> [% interests.interest %] </strong> </td> </tr>
                [% IF interests.experience_months %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Experience Months') %]: </strong> [% interests.experience_months | html %] </td> </tr>
                [% END %]
                [% IF interests.jobtitles %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Jobtitles') %]: </strong> [% interests.jobtitles | html %] </td> </tr>
                [% END %]
            [% END %]
            </table>
        [% END %]

		<br/>
        <p> <strong> <u> [% L('Resume Text') %] </u> </strong> </p>
        [% candidate.resume_text FILTER html FILTER html_line_break %]
	</div>
</div>

<!-- end: [% component.name %] //-->

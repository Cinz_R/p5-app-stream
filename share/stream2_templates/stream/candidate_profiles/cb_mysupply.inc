<!-- start: [% component.name %] //-->
<br/>
[% IF candidate.campaign_attributes.size %]
    <br />
    <h4>[% L('Campaign Details') | html_entity %]</h4>
    <div class="line">
        [% FOR key IN candidate.campaign_attributes.keys %]
            <div class="unit size1of2">
                <div style="display: inline-block">
                    <strong>[% key | html_entity %]:</strong>
                </div>
                <div style="display: inline-block">
                    [% candidate.campaign_attributes.$key.join(', ') | html_entity %]
                </div>
            </div>
        [% END %]
    </div>
[% END %]

[% IF candidate.custom_fields.size %]
    <br />
    <h4>[% L('Custom Questions') | html_entity %]</h4>
    <div class="line">
        [% FOR key IN candidate.custom_fields.keys %]
            <div class="unit size1of2">
                <div style="display: inline-block">
                    <strong>[% key | html_entity %]:</strong>
                </div>
                <div style="display: inline-block">
                    [% candidate.custom_fields.$key.join(', ') | html_entity %]
                </div>
            </div>
        [% END %]
    </div>
[% END %]

<h4>CV</h4>
<br />
[% candidate.profile | html_line_break %]

[% IF candidate.has_image == 'True' %]
<div>
    <br><br>
    <button type="button" class="btn" id="mysupply_resume_btn" name="mysupply_resume_btn" onclick="$('.mysupply_resume_image').toggle();">Resume Image</button>
    <br><br>
    <img class="mysupply_resume_image" style="display:none;" src="[% candidate.cv_base64 %]"/>
</div>
[% END %]
<!-- end: [% component.name %] //-->

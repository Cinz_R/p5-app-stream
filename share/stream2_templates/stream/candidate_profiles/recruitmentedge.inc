<style>
    .indent {
        text-indent: 20px;
    }
    i {
        text-indent: 0px;
    }
    .block {
        margin: 14px auto;
        border-bottom: 2px solid #f5f5f5;
    }
    .pill-candidate {
        background-color: #1b75bb;
        color: white;
        padding: 0px 3px;
    }
    span.pill-candidate > em.highlight {
        color: blue;
    }
    .line h4 {
        margin: 12px 0px;
    }
    .float-right {
        float: right;
    }
</style>

<div class="line">
    <div class="unit size1of2">
        [% IF candidate.last_viewed %] <p> <strong>[% i18n('Last Viewed') %]:</strong> <span>[% candidate.last_viewed %] </span> </p> [% END %]
        [% IF candidate.last_modified %] <p> <strong>[% i18n('Resume Last Modified') %]:</strong> <span>[% candidate.last_modified %] </span> </p> [% END %]
        [% IF candidate.email %] <p> <strong>[% i18n('Email (Primary)') %]:</strong> <span><a href="mailto:[% candidate.email %]">[% candidate.email %]</a></span> </p> [% END %]
        [% IF candidate.jobtitle %] <p> <strong>[% i18n('JobTitle') %]:</strong> <span>[% candidate.jobtitle %]</span> </p> [% END %]
        [% IF candidate.years_exp %] <p> <strong>[% i18n('Experience') %]:</strong> <span>[% candidate.years_exp %] years</span> </p> [% END %]
        [% IF candidate.spoken_languages.size > 0 %] <p> <strong>[% i18n('Spoken Languages') %]:</strong> <span>[% candidate.spoken_languages.join(', ') %] </span> </p> [% END %]
        [% IF candidate.industries.size > 0 %] <p> <strong>[% i18n('Industries') %]:</strong> <span>[% candidate.industries.join(', ') %] </span> </p> [% END %]

        [% IF candidate.phones.size > 0 %]
            <div>
                <p> <strong>[% i18n('Phone number(s)') %]</strong> </p>
                [% FOREACH telephone IN candidate.phones %]
                    [% IF telephone.Number %] <p> <span>- [% telephone.Number %] </span> </p> [% END %]
                [% END %]
            </div>
        [% END %]

        [% IF candidate.websites.size > 0 %]
            <div>
                <p> <strong>[% i18n('Website(s)') %]</strong> </p>
                [% FOREACH website IN candidate.websites %]
                    <p>- [% website %]</p>
                [% END %]
            </div>
        [% END %]
    </div>

    [% IF candidate.photo %]
        <div class="float-right">
            <img src="[% candidate.photo %]" width="120" height="120" />
        </div>
    [% END %]

    <div class="clear">
        [% IF candidate.emails %]
            <h4>[% i18n('Emails') %]</h4>
            [% FOREACH email IN candidate.emails %]
                <div class="block indent">
                    <span><i class="icon-mail"></i> <a href="mailto:[% email.ID %]">[% email.ID %]</a></span>
                    [% IF email.Metadata.CrowdSourcing.UpVotes %]
                        &#8226;
                        <span title="Validity Up-votes" class="text-success"><i class="icon-thumbs-up"></i> [% email.Metadata.CrowdSourcing.UpVotes %]</span>
                    [% END %]
                    [% IF email.Metadata.CrowdSourcing.DownVotes %]
                        <span title="Validity Down-votes" class="text-error"><i class="icon-thumbs-down"></i> [% email.Metadata.CrowdSourcing.DownVotes %]</span>
                    [% END %]
                </div>
            [% END %]
        [% END %]
    </div>

    <div>
        [% IF candidate.certificates.size > 0 %]
            <div class="clear">
            <h4>[% i18n('Certifications') %]</h4>
            <div>
                [% FOREACH certificate IN candidate.certificates %]
                <div class="block indent">
                    <span><strong>[% i18n('Title') %]:</strong> [% certificate.Title %]</span>
                </div>
                [% END %]
            </div>
        [% END %]

        [% IF candidate.publications.size > 0 %]
            <div class="clear">
            <h4>[% i18n('Publications') %]</h4>
            <div>
                [% FOREACH publication IN candidate.publications %]
                <div class="block indent">
                    [% IF publication.Title %] <p> <strong>[% i18n("Title") %]:</strong> <span>[% publication.Title %]</span> </p> [% END %]
                    [% IF publication.Publisher %] <p> <strong>[% i18n("Publisher") %]:</strong> <span>[% publication.Publisher %]</span> </p> [% END %]
                    [% IF publication.PublishedDate %] <p> <strong>[% i18n("Date Published") %]:</strong> <span>[% publication.PublishedDate %]</span> </p> [% END %]
                </div>
                [% END %]
            </div>
        [% END %]
    </div>

    [% IF candidate.about.size > 0 %]
        <div class="clear">
            <h4>[% i18n('About') %]</h4>
            <div>
                [% FOREACH role IN candidate.about %]
                    <p>[% role %]</p>
                [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.social_networks.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Available on these social networks') %]</h4>
            <div class="block indent">
                [% FOREACH network IN candidate.social_networks %]
                <div>
                    [% IF network.URL %] <i class="[% network.IconClass %]"></i> <a href="[% network.URL %]" target="_blank">[% network.Name %]</a> [% END %]
                </div>
                [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.linkedin_properties %]
        <div class="clear">
            <h4>[% i18n('LinkedIn') %]</h4>
            <div class="block indent">
                [% IF candidate.linkedin_properties.connections %] <p> <strong>[% i18n("Connections") %]:</strong> <span>[% candidate.linkedin_properties.connections %]</span> </p> [% END %]
                [% IF candidate.linkedin_properties.current_position %] <p> <strong>[% i18n("Current Position") %]:</strong> <span>[% candidate.linkedin_properties.current_position %]</span> </p> [% END %]
                [% IF candidate.linkedin_properties.groups %] <p> <strong>[% i18n("Groups") %]:</strong> <span>[% candidate.linkedin_properties.groups %]</span> </p> [% END %]
                [% IF candidate.linkedin_properties.interests %] <p> <strong>[% i18n("Interests") %]:</strong> <span>[% candidate.linkedin_properties.interests %]</span> </p> [% END %]
                [% IF candidate.linkedin_properties.projects %] <p> <strong>[% i18n("Projects") %]:</strong> <span>[% candidate.linkedin_properties.projects %]</span> </p> [% END %]
                [% IF candidate.linkedin_properties.recommendations %] <p> <strong>[% i18n("Recommendations") %]:</strong> <span>[% candidate.linkedin_properties.recommendations %]</span> </p> [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.recent_education %]
        <div class="clear">
            <h4>[% i18n('Recent Education') %]</h4>
            <div class="block indent">
                [% IF candidate.recent_education.Title %] <p> <strong>[% i18n('Degree') %]:</strong> <span>[% candidate.recent_education.Title %] </span> </p> [% END %]
                [% IF candidate.recent_education.Institution %] <p> <strong>[% i18n('Institute') %]:</strong> <span>[% candidate.recent_education.Institution %] </span> </p> [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.recent_position %]
        <div class="clear">
            <h4>[% i18n('Recent Position') %]</h4>
            <div class="block indent">
                [% IF candidate.recent_position.Position %] <p> <strong>[% i18n('Position') %]:</strong> <span>[% candidate.recent_position.Position %] </span> </p> [% END %]
                [% IF candidate.recent_position.Employer %] <p> <strong>[% i18n('Employer') %]:</strong> <span>[% candidate.recent_position.Employer %] </span> </p> [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.top_skills.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Top 3 Skill Details') %]</h4>
            <div class="block indent">
                [% FOREACH skill IN candidate.top_skills %]
                    <p> <strong>[% skill.name %]:</strong> [% skill.reason %] </p>
                [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.current_locations.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Current Locations') %]</h4>
            <div class="size2of2">
                [% FOREACH location IN candidate.current_locations %]
                <div class="block indent">
                    [% IF location.City %] <p> <strong>[% i18n('City') %]:</strong> [% location.City %] </p> [% END %]
                    [% IF location.County %] <p> <strong>[% i18n('County') %]:</strong> [% location.County %] </p> [% END %]
                    [% IF location.Country %] <p> <strong>[% i18n('Country') %]:</strong> [% location.Country %] </p> [% END %]
                    [% IF location.State %] <p> <strong>[% i18n('State') %]:</strong> [% location.State %] </p> [% END %]
                    [% IF location.ZipCode %] <p> <strong>[% i18n('Zipcode') %]:</strong> [% location.ZipCode %] </p> [% END %]
                </div>
                [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.desired_locations.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Desired Locations') %]</h4>
            <div class="size2of2">
                [% FOREACH location IN candidate.desired_locations %]
                <div class="block indent">
                    [% IF location.City %] <p> <strong>[% i18n('City') %]:</strong> [% location.City %] </p> [% END %]
                    [% IF location.County %] <p> <strong>[% i18n('County') %]:</strong> [% location.County %] </p> [% END %]
                    [% IF location.Country %] <p> <strong>[% i18n('Country') %]:</strong> [% location.Country %] </p> [% END %]
                    [% IF location.State %] <p> <strong>[% i18n('State') %]:</strong> [% location.State %] </p> [% END %]
                    [% IF location.ZipCode %] <p> <strong>[% i18n('Zipcode') %]:</strong> [% location.ZipCode %] </p> [% END %]
                </div>
                [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.education.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Education') %]</h4>
            <div class="size2of2">
                [% FOREACH education IN candidate.education %]
                    [% NEXT IF !education.Institution && !education.Title %]
                    <div class="block indent">
                        [% IF education.Title %] <p> <strong>[% i18n('Title') %]:</strong> <span>[% education.Title %] </span> </p> [% END %]
                        [% IF education.Institution %] <p> <strong>[% i18n('Institution') %]:</strong> <span>[% education.Institution %] </span> </p> [% END %]
                        [% IF education.Concentration %] <p> <strong>[% i18n('Concentration') %]:</strong> <span>[% education.Concentration %] </span> </p> [% END %]
                        [% IF education.DegreeLevel %] <p> <strong>[% i18n('Degree Level') %]:</strong> <span>[% education.DegreeLevel %] </span> </p> [% END %]
                        [% IF education.DateGraduated %] <p> <strong>[% i18n('Graduated on') %]:</strong> <span>[% education.DateGraduated %] </span> </p> [% END %]
                    </div>
                [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.employment.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Employment History') %]</h4>
            <div class="size2of2">
                [% FOREACH employer IN candidate.employment %]
                <div class="block indent">
                    [% IF employer.Employer %] <p> <strong>[% i18n('Company') %]:</strong> <span>[% employer.Employer %] </span> </p> [% END %]
                    [% IF employer.Position %] <p> <strong>[% i18n('Position') %]:</strong> <span>[% employer.Position %] </span> </p> [% END %]
                    [% IF employer.DateFrom %] <p> <strong>[% i18n('From') %]:</strong> <span>[% employer.DateFrom %] </span> </p> [% END %]
                    [% IF employer.DateTo %] <p> <strong>[% i18n('To') %]:</strong> <span>[% employer.DateTo %] </span> </p> [% ELSE %] <p> <strong>[% i18n('To') %]:</strong> <span> Present </span> </p> [% END %]
                    [% IF employer.Description %] <p> <strong>[% i18n('Description') %]</strong> <p>[% employer.Description %] </p> </p> [% END %]
                </div>
                [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.awards.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Awards') %]</h4>
            <div class="size2of2">
                [% FOREACH award IN candidate.awards %]
                    <div class="block indent">
                        [% IF award.Source %] <p> <strong>[% i18n('Source') %]:</strong> <span>[% award.Source %] </span> </p> [% END %]
                        [% IF award.Title %] <p> <strong>[% i18n('Title') %]:</strong> <span>[% award.Title %] </span> </p> [% END %]
                        [% IF award.Issuer %] <p> <strong>[% i18n('Issuer') %]:</strong> <span>[% award.Issuer %] </span> </p> [% END %]
                    </div>
                [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.memberships.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Memberships') %]</h4>
            <div class="size2of2">
                [% FOREACH membership IN candidate.memberships %]
                    <div class="block indent">
                        [% IF membership.Title %] <p> <strong>[% i18n('Title') %]:</strong> <span>[% membership.Title %] </span> </p> [% END %]
                        [% IF membership.Organization %] <p> <strong>[% i18n('Organization') %]:</strong> <span>[% membership.Organization %] </span> </p> [% END %]
                        [% IF membership.MemberSince %] <p> <strong>[% i18n('Member since') %]:</strong> <span>[% membership.MemberSince %] </span> </p> [% END %]
                    </div>
                [% END %]
            </div>
        </div>
    [% END %]

    [% IF candidate.keywords.keys.size > 0 %]
        <div class="clear">
            <h4>[% i18n('Skills & Competencies') %]</h4>
            [% FOREACH keyword IN candidate.keywords.keys %]
                <span class="pill pill-candidate">
                    [% keyword %]
                </span>
            [% END %]
        </div>
    [% END %]

</div>

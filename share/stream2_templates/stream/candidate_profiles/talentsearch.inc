<div class="line">
   <div class="bd listingBlock listingSkin">
        <div class="hd">
            <p>[% candidate.name %]</p>
        </div>
    </div>
</div>

    <div class="line">
        <div class="unit size1of2">
            [% IF candidate.application_time_epoch %]
                <p><strong>[% L('Application Time') | html %]:</strong>
                    [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.application_time_epoch) || candidate.application_time | html %]
                </p>
            [% END %]
            [% IF candidate.address %]
                <p><strong>[% L('Address') | html %]:</strong> [% candidate.address | html %]</p>
            [% END %]
            [% IF candidate.telephone %]
                <p><strong>[% L('Home') | html %]:</strong> [% candidate.telephone | html %]</p>
            [% END %]
            [% IF candidate.mobile %]
                <p><strong>[% L('Mobile') | html %]:</strong> [% candidate.mobile | html %]</p>
            [% END %]
            [% IF candidate.applicant_availability_date %]

               <p><strong>[% L('Availability Date') | html %]:</strong> [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.applicant_availability_date_epoch) || candidate.applicant_availability_date | html %]</p>
            [% END %]
            [% IF candidate.advert_job_type %]
                <p><strong>[% L('Jobtype') | html %]:</strong>
                [% candidate.advert_job_type %]
            [% END %]

        </div>
        <div class="unit size1of2 lastUnit">
            [% IF candidate.employer_org_name %]
                <p><strong>[% L('Employer') | html %]:</strong> [% candidate.employer_org_name | html %]</p>
            [% END %]
            [% IF candidate.employer_org_position_title %]
                <p><strong>[% L('Job Title') | html %]:</strong> [% candidate.employer_org_position_title | html %]</p>
            [% END %]

            [% IF candidate.employer_org_position_start_date %]
                <p><strong>[% L('Start Date') | html %]:</strong> 
                    [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.employer_org_position_start_date_epoch) || candidate.employer_org_position_start_date | html %]
                </p>
            [% END %]
            [% IF candidate.employer_org_position_end_date %]
                <p><strong>[% L('End Date') | html %]:</strong>
                    [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.employer_org_position_end_date_epoch) || candidate.employer_org_position_end_date | html %]
                </p>
            [% END %]
            [% IF candidate.advert_industry %]
                <p><strong>[% L('Industry') | html %]:</strong>
                [% candidate.advert_industry %]</p>
            [% END %]
            [% IF candidate.applicant_salary %]
                <p><strong>[% L('Salary') | html %]:</strong>
                [% candidate.applicant_salary | html %]</p>
            [% ELSIF candidate.advert_salary %]
                <p><strong>[% L('Advert Salary') | html %]:</strong>
                [% candidate.advert_salary %]</p>
            [% END %]
        </div>
    </div>


<div class="cv_preview">
  [% candidate.cv_html %]
</div>

<!-- start: [% component.name %] //-->
  <!-- styling for the cv preview to ensure it looks the same as on site -->  
   <style type="text/css">
   .cvPreviewBox p{
    white-space: pre-wrap;
    }
   .cvPreviewBox h2{
       font-weight: bold;
       
       } 

    </style>
  

<div class="bd line">



<div class="unit size1of2">
                [% IF candidate.job_title %]<p><strong>[% L('Job Title') | html %]:</strong> [% candidate.job_title | html %]</p>[% END %]

                [% IF candidate.salary %]<p><strong>[% L('Salary') | html %]:</strong> [% candidate.salary | html %]</p>[% END %]
                [% IF candidate.jobtype %]<p><strong>[% L('Job Type') | html %]:</strong> [% candidate.jobtype | html %]</p>[% END %]

                [% IF candidate.date_added_epoch %]<p><strong>[% L('Date Added') | html %]:</strong>
                    [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.date_added_epoch) || candidate.date_added_epoch | html %]
                </p>[% END %]
                [% IF candidate.availibility_epoch %]<p><strong>[% L('Availability') | html %]:</strong>
                    [% L('[dateformat,_1,_DATEFORMAT_DISPLAY_DATE]', candidate.availibility_epoch) || candidate.availibility_epoch | html %]
                </p>[% END %]
                [% IF candidate.eu_eligable %]<p><strong>[% L('EU Permit') | html %]:</strong> [% candidate.eu_eligable | html %]</p>[% END %]
                [% IF candidate.home_telephone %]<p><strong>[% L('Home') | html %]:</strong> [% candidate.home_telephone | html %]</p>[% END %]
                [% IF candidate.mobile_telephone %]<p><strong>[% L('Mobile') | html %]:</strong> [% candidate.mobile_telephone | html %]</p>[% END %]
            </div><!-- .unit -->
            <div class="size1of2 unitExt">
                [% IF candidate.town %]<p><strong>[% L('Town') | html %]:</strong> [% candidate.town | html %]</p>[% END %]
                [% IF candidate.country %]<p><strong>[% L('Country') | html %]:</strong> [% candidate.country | html %]</p>[% END %]
                [% IF candidate.postcode %]<p><strong>[% L('Postcode') | html %]:</strong> [% candidate.postcode | html %]</p>[% END %]
                [% IF candidate.will_relocate %]<p><strong>[% L('Willing to relocate') | html %]:</strong> [% candidate.will_relocate | html %]</p>[% END %]
                [% IF candidate.snippet %]<p><strong>[% L('Snippet') | html %]:</strong> [% candidate.snippet | html %]</p>[% END %]
            </div>

    </div>

	<div style="clear:both;">
		<br/>
        

        [% IF candidate.cv_preview %] <p> <strong><u> [% L('CV Preview') %]</u> </strong></p><p>[% candidate.cv_preview %] </p> [% END %]

        <!--
        [% IF candidate.cvpreview.size > 0 %] <p><strong>CV Preview:</strong> <br /> 
        
        [% FOREACH cvpreview IN candidate.cvpreview %]
            
           </p> [% cvpreview | html %] </p> 
        
        [% END %]
        </p>
        [% END %]
        -->
	</div>
</div>
<!-- end: [% component.name %] //-->

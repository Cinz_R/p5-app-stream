<h1>[% candidate.name | html_entity %]</h1>
[% FOR paragraph IN candidate.paragraphs %]
    [% IF paragraph.title AND paragraph.value %]
        <h3>[% L(paragraph.title) | html_entity%]</h3>
        <p>[% paragraph.value | html_entity %]</p>
    [% END %]
[% END %]


[% FOR summary_field IN candidate.summary_fields %]
    [% IF summary_field.label AND summary_field.value %]
        <div>
            <div style="display: inline-block;">
                <strong>
                    [% L(summary_field.label) | trim | html_entity %]:
                </strong>
            </div>
            <div style="display: inline-block;">
                [% summary_field.value | trim | html_entity %]
            </div>
        </div>
    [% END %]
[% END %]

[% IF candidate.cv_html %]
    <div class="clear">
        <h3 class="center">CV</h3>
        <div>[% candidate.cv_html %]</div>
    </div>
[% END %]

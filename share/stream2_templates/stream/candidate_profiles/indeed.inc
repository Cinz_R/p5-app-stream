<!-- start: [% component.name %] //-->
<style type="text/css">
    td.indent
    {
        width: 40px;
    }

    div.indeed {
        font-size: 24px;
        text-align: left;
        float: right;
        width: 60%;
    }
</style>

<div class="bd line">
    [% IF candidate.contact %]
        <a class="btn" href="[% candidate.contact %]" target="_blank" >
            [% L('Contact on') | html_entity %]<br />
            <img alt="indeed" src="/images/indeed_contact.png"  alt="Contact Candidate" border="0" style="height: 30px;"/>
        </a>
        <div class="indeed">
            <a target="_blank" href=[% candidate.contact %]>[% candidate.name %] </a>
        </div>
        <p>&nbsp; </p>
    [% END %]
    <div style="clear:both;">
        [% IF candidate.name %] <p> <strong> [% L('Name') %]: </strong> [% candidate.name | html%] </p> [% END %]
        [% IF candidate.date_created %] <p> <strong> [% L('Date Created') %]: </strong> [% candidate.date_created | html%] </p> [% END %]
        [% IF candidate.date_modified %] <p> <strong> [% L('Date Modified') %]: </strong> [% candidate.date_modified | html%] </p> [% END %]
        [% IF candidate.skills %] <p> <strong> [% L('Skills') %]: </strong> [% candidate.skills | html_line_break %] </p> [% END %]


        [% IF candidate.experience.size > 0 %]
            <br/>
            <strong><u> [% L('Experience') %] </u> </strong>
            <table>
            [% FOREACH experience IN candidate.experience %]
                <tr> <td colspan=2> <strong> [% experience.dateRange.displayDateRange %] </strong> </td> </tr>
                [% IF experience.location %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Location') %]: </strong> [% experience.location %] </td> </tr>
                [% END %]
                [% IF experience.title %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Title') %]: </strong> [% experience.title %] </td> </tr>
                [% END %]
                [% IF experience.company %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Company') %]: </strong> [% experience.company %] </td> </tr>
                [% END %]
                [% IF experience.description %]
                    <tr> <td class="indent"> </td> <td> [% experience.description | trim | html_line_break %] </td> </tr>
                [% END %]
            [% END %]
            </table>
        [% END %]

        [% IF candidate.awards.size > 0 %]
            <br/>
            <strong><u> [% L('Awards') %] </u> </strong>
            [% FOREACH awards IN candidate.awards %]
            [% END %]
        [% END %]

        [% IF candidate.military_experiences.size > 0 %]
            <br/>
            <strong><u> [% L('Military Experiences') %] </u> </strong>
            [% FOREACH military_experiences IN candidate.military_experiences %]
            [% END %]
        [% END %]

        [% IF candidate.publications.size > 0 %]
            <br/>
            <strong><u> [% L('Publications') %] </u> </strong>
            [% FOREACH publications IN candidate.publications %]
            [% END %]
        [% END %]

        [% IF candidate.education.size > 0 %]
            <br/>
            <strong><u> [% L('Education') %] </u> </strong>
            <table>
            [% FOREACH education IN candidate.education %]
                [% IF education.dateRange.displayDateRange %]
                    <tr> <td colspan=2> <strong> [% education.dateRange.displayDateRange %] </strong> </td> </tr>
                [% ELSE %]
                    <tr> <td> &nbsp; </td> </tr>
                [% END %]
                [% IF education.location %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Location') %]: </strong> [% education.location %] </td> </tr>
                [% END %]
                [% IF education.degree %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Degree') %]: </strong> [% education.degree %] </td> </tr>
                [% END %]
                [% IF education.school %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('School') %]: </strong> [% education.school %] </td> </tr>
                [% END %]
                [% IF education.field %]
                    <tr> <td class="indent"> </td> <td> <strong> [% L('Field') %]: </strong> [% education.field %] </td> </tr>
                [% END %]
            [% END %]
            </table>
        [% END %]


        [% IF candidate.certifications.size > 0 %]
            <br/>
            <strong><u> [% L('Certifications') %] </u> </strong>
            [% FOREACH certifications IN candidate.certifications %]
            [% END %]
        [% END %]

        [% IF candidate.additional_info %]
            <br/>
            <strong> <u> [% L('Additional Info') %]: </u> </strong> <p> [% candidate.additional_info | html_line_break %] </p> [% END %]

        [% IF candidate.patents.size > 0 %]
            <br/>
            <strong><u> [% L('Patents') %] </u> </strong>
            [% FOREACH patents IN candidate.patents %]
            [% END %]
        [% END %]

    </div>
</div>
<!-- end: [% component.name %] //-->

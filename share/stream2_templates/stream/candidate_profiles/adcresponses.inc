[% IF candidate.has_done_delete %]

    <strong>[% L('This candidate is awaiting for deletion from the source. It should disappear from these results shortly.') %]</strong>

[% ELSE %]

    [% IF candidate.broadbean_adcresponse_email_body_html %]
     <h4>[% i18n('Application Message:') | html %]</h4>
     <div class="cv_preview">
       [% candidate.broadbean_adcresponse_email_body_html %]
     </div>
     <hr/>
    [% END %]


    <div class="cv_preview">
      [% candidate.cv_html %]
    </div>



[% END %]

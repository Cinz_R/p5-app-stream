
<div style="background: rgb(237, 237, 237); padding: 10px; border: 1px solid rgb(207, 207, 207); margin-bottom: 10px">
    <strong>[% L('CV Data') %]</strong><br />
    <ul>
        [% IF candidate.location %]
            <li>
                <strong>[% L('Location') | html_entity %]:</strong>
                [% candidate.location | html_entity %]
            </li>
        [% END %]
        [% IF candidate.last_updated %]
            <li>
                <strong>[% L('Last updated') | html_entity %]:</strong>
                [% candidate.last_updated | html_entity %]
            </li>
        [% END %]
        [% IF candidate.education %]
            <li>
                <strong>[% L('Education') | html_entity %]:</strong>
                [% candidate.education | html_entity %]
            </li>
        [% END %]
        [% IF candidate.desired_jobtitle %]
            <li>
                <strong>[% L('Desired job title') | html_entity %]:</strong>
                [% candidate.desired_jobtitle | html_entity %]
            </li>
        [% END %]
        [% IF candidate.seniority %]
            <li>
                <strong>[% L('Seniority') | html_entity %]:</strong>
                [% candidate.seniority | html_entity %]
            </li>
        [% END %]
        [% IF candidate.job_types %]
            <li>
                <strong>[% L('Job types') | html_entity %]:</strong>
                [% candidate.job_types | html_entity %]
            </li>
        [% END %]
        [% IF candidate.experience %]
            <li>
                <strong>[% L('Experience') | html_entity %]:</strong>
                [% candidate.experience | html_entity %]
            </li>
        [% END %]
    </ul>
</div>

[% IF candidate.education %]
    <div>
        <strong>[% L('Education')  | html_entity %]:</strong>
        [% candidate.education | html_entity %]
    </div>
[% END %]

[% IF candidate.desired_jobtitle %]
    <div>
        <strong>[% L('Desired Job Title') | html_entity %]:</strong>
        [% candidate.desired_jobtitle | html_entity %]
    </div>
[% END %]

[% IF candidate.personal_motivation %]
    <div>
        <strong>[% L('Personal Motivation') | html_entity %]:</strong>
        [% candidate.personal_motivation | html_entity %]
    </div>
[% END %]
<!-- start: [% component.name %] //-->
[%#

Using tables to that the document parser picks up on formatting.
Forgive me.

%]

[% IF candidate.cvSnippets.size %]
    <h5>[% L('CV Snippet') | html_entity %]</h5>
    [% FOREACH snippet IN candidate.cvSnippets %]
        <p>[% snippet | html_entity %]</p>
    [% END %]
[% END %]

<h4>Personal Statement</h4>
[% IF candidate.personalStatement %]
    <p>[% candidate.personalStatement | html_entity %]</p>
[% END %]

[% IF candidate.email %]
    <p>[% L('Email') | html_entity %]: [% candidate.email %]</p>
[% END %]
[% IF candidate.phone %]
    <p>[% L('Phone') | html_entity %]: [% candidate.phone | html_entity %]</p>
[% END %]
[% IF candidate.address %]
    <p>[% L('Address') | html_entity %]: [% candidate.address | html_entity %]</p>
[% END %]
[% IF candidate.jobTitle %]
    <p>
        [% L('Job Titles') | html_entity %]:
        [% candidate.jobTitle | html_entity %]
    </p>
[% END %]
[% IF candidate.permanent_work OR candidate.temp_work OR candidate.contract_work %]
    <p>
        [% L('Work Type') | html_entity %]:
        [% candidate.permanent_work | html_entity %]
        [% candidate.temp_work | html_entity %]
        [% candidate.contract_work | html_entity %]
    </p>
[% END %]
[% IF candidate.previousEmployer %]
    <p>
        [% L('Previous Employer') | html_entity %]:
        [% candidate.previousEmployer | html_entity %]
    </p>
[% END %]
[% IF candidate.minimum_salary %]
    <p>
        [% L('Salary') | html_entity %]:
        &pound;[% candidate.minimum_salary | html_entity %] per annum
    </p>
[% END %]
[% IF candidate.minimum_temp_rate %]
    <p>
        [% L('Rate') | html_entity %]:
        &pound;[% candidate.minimum_temp_rate | html_entity %] per hour
    </p>
[% END %]

[% IF candidate.created_on %]
    <p>
        [% L('Created On') | html_entity %]:
        [% candidate.created_on | html_entity %]
    </p>
[% END %]   
[% IF candidate.last_candidate_login %]
    <p>
        [% L('Last Login') | html_entity %]:
        [% candidate.last_candidate_login | html_entity %]
    </p>
[% END %]   

[% IF candidate.preferredLocations %]
    <h5>[% L('Preferred Locations') %]:</h5>
    [% FOREACH preferredLocations IN candidate.preferredLocations %]
        <p> [% preferredLocations | html_entity %] </p>
    [% END %]
[% END %]

[% IF candidate.part_work OR candidate.full_work %]
        <h6>[% L('Work Hours') | html_entity %]:</h6>
        [% IF candidate.full_work %]
            <p>[% candidate.full_work | html_entity %]</p>
        [% END %]
        [% IF candidate.part_work  %]
            <p>[% candidate.part_work | html_entity %]</p>
        [% END %]
[% END %]

[% IF candidate.isukEligible OR candidate.drive_licence OR candidate.car_owner OR candidate.noticePeriod %]
    <h5>[% L('Eligibility') | html_entity %]</h5>
    [% IF candidate.isukEligible %]
        <p>
            [% L('Eligible to work in the UK') | html_entity %]:
            [% candidate.isukEligible | html_entity %]
        </p>
    [% END %]   
    [% IF candidate.drive_licence %]
        <p>
            [% L('Driving Licence') | html_entity %]:
            [% candidate.drive_licence | html_entity %]
        </p>
    [% END %]    
    [% IF candidate.car_owner %]
        <p>
            [% L('Car Owner') | html_entity %]:
            [% candidate.car_owner | html_entity %]
        </p>
    [% END %]  
    [% IF candidate.noticePeriod %]
        <p>
            [% L('Notice Period') | html_entity %]:
            [% candidate.noticePeriod | html_entity %]
        </p>
    [% END %] 
[% END %]
[% IF candidate.languages.size %]
    <h5> [% L('Languages') %]:</h5>
    [% FOREACH languages IN candidate.languages %]
         <p> [% languages.language | html_entity %] - [% languages.fluency | html_entity %]</p>
    [% END %]
[% END %]   


[% IF candidate.skills %]
    <h5>[% L('Skills') %]</h5>
    <p>[% candidate.skills.join(', ') | html_entity %]</p>
[% END %]

[% IF candidate.jobSectors %]
    <h5> [% L('Preferred Sectors') %]:</h5>
    [% FOREACH sector_group IN candidate.jobSectors %]
        [% IF sector_group.parentSector %]
        <p>[% sector_group.parentSector | html_entity %]</p>
        [% END %]
        [% IF sector_group.subSectors.size %]
            [% FOREACH subsector IN sector_group.subSectors %]
                <p>[% subsector | html_entity%]</p>
            [% END %]
        [% END %]
    [% END %]
[% END %]

[% IF candidate.education.size OR candidate.highestQualification %]
    <h4>Education</h4>
    [% IF candidate.highestQualification %]
        [% L('Highest Qualification') | html_entity %]: [% candidate.highestQualification | html_entity %]
    [% END %]

    [% FOREACH education IN candidate.education %]
        [% IF education.institutionName %]
            <p>[% L('Institution') %]: [% education.institutionName | html_entity %]</p>
        [% END %]
        [% IF education.startedOn OR education.finishedOn %]
            <p>
                [% L('Period') %]:
                [% IF education.startedOn %]
                    [% education.startedOn | html_entity %]
                [% END %] -
                [% IF education.finishedOn %]
                    [% education.finishedOn | html_entity %]
                [% END %]
            </p>
        [% END %]
        [% IF education.subject %]
            <p>[% L('Subject') | html_entity %]: [% education.subject | html_entity %]</p>
        [% END %]
        [% IF education.qualificationType %]
            <p>[% L('Qualification Type') | html_entity %]: [% education.qualificationType | html_entity %]</p>
        [% END %]
        [% IF education.degreeGrade %]
            <p>[% L('Degree Grade') | html_entity %]: [% education.degreeGrade | html_entity %]</p>
        [% END %]
        [% IF education.gradeDescription %]
            <p>[% L('Grade Description') | html_entity %]: [% education.gradeDescription | html_entity %]</p>
        [% END %]
    [% END %]
[% END %]

[% IF candidate.workHistory %]
    <h4>Work history</h4>
    [% FOR job IN candidate.workHistory %]
        [% IF job.jobTitle %]
            <h5>
                [% L('Job Title') | html_entity%]:
                [% job.jobTitle | html_entity %]
            </h5>
        [% END %]
        [% IF job.companyName %]
            <p>
                [% L('Company Name') | html_entity%]:
                [% job.companyName | html_entity %]
            </p>
        [% END %]
        [% IF job.jobDescription %]
            [% L('Job Description') | html_entity%]:
            <p>[% job.jobDescription | html_entity %]</p>
        [% END %]
        [% IF job.dateFrom %]
            <p>
                [% L('Start Date') | html_entity%]:
                [% job.dateFrom | html_entity %]
            </p>
        [% END %]
        [% IF job.dateTo %]
            <p>
                [% L('End Date') | html_entity%]:
                [% job.dateTo | html_entity %]
            </p>
        [% END %]
    [% END %]
[% END %]
﻿<!-- start: [% component.name %] //-->

[% SET candidate_cell = 'stream/candidate_profiles/'_ candidate.destination _'.inc' %]

[% TRY %]
    [% INCLUDE $candidate_cell %]
[% CATCH %]
    <p class="error">Unable to load [% candidate_cell %]: [% error.info FILTER html %] ([% error.type %])</p>
[% END %]
<!-- end: [% component.name %] //-->

[% IF APP.debug %]<!-- start: [% component.name %] //-->[% END %]

[% DEFAULT class="mod boardCell" %]

<div class="[% class %]"[% IF id %] id="[% id %]"[% END %]>
    <b class="top"><b class="tl"></b><b class="tr"></b></b>
    <div class="inner">
[% content %]
   </div>
    <b class="bottom"><b class="bl"></b><b class="br"></b></b>
</div><!-- .[% class %]-->

[% IF APP.debug %]<!-- end: [% component.name %] //-->[% END %]

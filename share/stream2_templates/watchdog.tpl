[% FOREACH result_set IN results %]
    <h4>[% result_set.subscription.subscription.board %]</h4>
    <ul>
        [% FOREACH result IN result_set.new_results %]
            <li>[% result.name %]</li>
        [% END %]
    </ul>
[% END %]

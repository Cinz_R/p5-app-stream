FORMAT: 1A
# JSON Ripple

JSON Ripple, or JRipple, is a JSON API for accessing the Broadbean Search product. This document will explain how to
create a user session and how to interact with the API's features.

## Login - Secret Key Authentication

If you are acting on behalf of an AdCourier user, you should have already recieved your *secret key* and *api key*.
These are used together with the AdCourier username to create a hash as explained below...

#### Required elements

 - Secret Key
 - API key
 - Username - the AdCourier username of the user for whom you are making the request
 - Time - the time in milliseconds since 1 January 1970 UTC ( epoch )

#### Process
 1. Get the current time (epoch) as milliseconds ( this needs to be accurate within 1 minute of the request )
 2. Join the username, time and API key into a single string using a pipe "|" separator:
 3. Encrypt the string using HMAC-SHA256 output as a hexidecimal string. Use your secret_key as the key
```
    #!/bin/bash
    TIME=`date +%s%N | cut -b1-13`
    TO_SIGN="$USERNAME|$TIME|$API_KEY"
    HASH=`echo -n "$TO_SIGN" | openssl dgst -sha256 -hmac "$SECRET_KEY" | awk '{print $2}'`

    #!perl
    my $time = time * 1000;
    my $to_sign = join( '|', $username, $time, $api_key );
    my $hash = Digest::SHA::hmac_sha256_hex( $to_sign , $secret_key );
```

For more code examples, please visit our Hybrid documentation - http://api.adcourier.com/docs/index.cgi?page=passwordless

## Login [/ripple/login]

Initiation a user session in Search. The resultant session ID allows access to features within Search without further need
to authenticate the user.

### Login [POST]

+ Request

    + Headers

            Content-Type: application/json
            Accept: application/json

    + Attributes

        + `version` (enum[string], required)
            + Members
                + `0.1`

        + `api_key` (string, required) - Aquired following your registration at http://api.adcourier.com/docs/register.cgi

        + `account` (object, required)

            + Third Party - User does not have an AdCourier account
                + `provider` (string, required)
                + `jwt_token` (string, required)

            + Standard - User has an AdCourier account
                + `username` (string, required) - AdCourier Username
                + `signature` (object, required) - See documentation above
                    + `hash` (string, required)
                    + `time` (number, required)

        + `config` (object, optional)

            + `stylesheet_url` (string, optional) - URL of hosted specific stylesheet to use when generating the interface

            + `no_adcourier_shortlist` (boolean, optional) - Do NOT show the standard AdCourier shortlist button in case you dont give any shortlist settings
                + Default: `false`

            + `shortlists` (array, optional) - Collection of shortlist buttons
                + `name` (string, required) - Formal identifier name of the button
                + `label` (string, required) - Free form label of the button
                + `options_url` (string, required) - URL to fetch the options from
                + `allowPreview` (optional) - Will those options have a previewURL?
                 + Default: `false`

            + `new_candidate_url` (string, optional) - URL search will send Saved and Shortlisted candidates

            + `tagged_doc_type` (enum[string], optional) - The type of document sent to new_candidate_url
                + Members
                    + `HR` - HR XML format
                    + `BGT` - Burning Glass XML format

            + `results_per_page` (number, optional) - Only works on a limited number of boards, mostly internal

            + `hide_tagging` (boolean, optional) - HIDE the Tagging button in general for this integration
                + Default: `false`

            + `hide_favourite` (boolean, optional) - HIDE the Favourite button. Deprecated
                + Default: `false`

            + `hide_save` (boolean, optional) - HIDE the Save button in general for this integration
                + Default: `false`

            + `hide_forward` (boolean, optional) - HIDE the Forward button in general for this integration
                + Default: `false`

            + `hide_download` (boolean, optional) - HIDE the Download button in general for this integration
                + Default: `false`

            + `hide_save_internal` (boolean, optional) - Hide the save button for internal boards
                + Default: `false`

            + `hide_message` (boolean, optional) - HIDE the Message button in general for this integration
                + Default: `false`

            + `hide_chargeable_message` (boolean, optional) - HIDE the Chargeable Message button in general for this integration
                + Default: `false`

            + `show_adcourier_navigation` (boolean, optional) - Will show the Navigation bar with links to other Broadbean products
                + Default: `false`

    + Body

            {
                "api_key": "1234567890",
                "account": {
                    "username": "user@team.office.company",
                    "signature": {
                        "hash": "df032de8789a7003b8b3dcf857049ffa857d6i2cb4ea6faa59671130c92597ff",
                        "time": "1234567890000"
                    }
                },
                "config": {
                    "new_candidate_url": "https://www.example.com/new_candidate"
                }
            }

    + Schema

            {
                "title": "Ripple JSON Schema - Login Request",
                "type": "object",
                "required": [ "version", "api_key", "account" ],
                "properties": {
                    "version": {
                        "type": "number",
                        "enum": [0.1]
                    },
                    "api_key": {
                        "type": "string"
                    },
                    "account": {
                        "type": "object",
                        "oneOf": [
                            {
                                "required": ["provider", "jwt_token"],
                                "properties": {
                                   "provider": { "type": "string" },
                                   "jwt_token": { "type": "string" }
                                }
                            },
                            {
                                "required": ["username", "password"],
                                "properties": {
                                    "username": {
                                        "type": "string"
                                    },
                                    "password": {
                                        "type": "string"
                                    }
                                }
                            },
                            {
                                "required": ["username", "signature"],
                                "properties": {
                                    "signature": {
                                        "type": "object",
                                        "description": "As detailed here: http://api.adcourier.com/docs/index.cgi?page=passwordless",
                                        "required": [ "hash", "time" ],
                                        "properties": {
                                            "hash": {
                                                "type": "string"
                                            },
                                            "time": {
                                                "type": "integer"
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    "config": {
                        "title": "Options for searching and generating the GUI",
                        "type": "object",
                        "properties": {
                            "stylesheet_url": {
                                "description": "URL of hosted specific stylesheet to use when generating the interface",
                                "type": "string"
                            },
                            "no_adcourier_shortlist": {
                                "description": "Do NOT show the standard AdCourier shortlist button in case you dont give any shortlist settings",
                                "type": "boolean"
                            },
                            "shortlists": {
                                "description": "Collection of shortlist buttons",
                                "type": "array",
                                "items": {
                                    "type": "object",
                                    "required": [ "name", "label", "items_url" ],
                                    "properties": {
                                        "name": { "type": "string", "description": "Formal identifier name of the button." },
                                        "label": { "type": "string" , "description": "Free form label of the button" },
                                        "options_url": { "type": "string", "description": "URL to fetch the options from" },
                                        "allowPreview": { "type": "boolean", "description": "Will those options have a previewURL?" }
                                    }
                                }
                            },
                            "new_candidate_url": {
                                "description": "URL search will send Saved and Shortlisted candidates",
                                "type": "string"
                            },
                            "tagged_doc_type": {
                                "description": "The type of document sent to new_candidate_url",
                                "type": "string"
                            },
                            "results_per_page": {
                                "description": "Only works on a limited number of boards, mostly internal",
                                "type": "number"
                            },
                            "hide_forward": {
                                "description": "HIDE the Forward button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_download": {
                                "description": "HIDE the Download button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_save": {
                                "description": "HIDE the Save button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_message": {
                                "description": "HIDE the Message button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_chargeable_message": {
                                "description": "HIDE the Chargeable Message button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_favourite": {
                                "description" : "HIDE the Favourite button. Deprecated",
                                "type": "boolean"
                            },
                            "hide_tagging": {
                                "description" : "HIDE the Tagging button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_save_internal": {
                                "description": "Hide the save button for internal boards",
                                "type": "boolean"
                            },
                            "show_adcourier_navigation": {
                                "description": "Will show AdCourier navigation, default 0",
                                "type": "boolean"
                            }
                        }
                    }
                }
            }

+ Response 200 (application/json)

    + Body

            {
                "status": "OK",
                "session_url": "https://search.adcourier.com/search?stok=xyz",
                "ripple_session_id": "abc123-432"
            }

    + Schema

            {
                "title": "Ripple JSON Schema - Login Response",
                "type": "object",
                "properties": {
                    "status": {
                        "type": "string",
                        "enum": ["OK","Unauthorised"]
                    },
                    "session_url": {
                        "type": "string",
                        "description": "A sessionised URL used for redirecting the user to an interactive Search session"
                    },
                    "ripple_session_id": {
                        "type": "string",
                        "description": "For authenticating subsequent ripple requests, is bound to a Search user"
                    }
                }
            }

## - Board Meta [/api/board/{board_name}{?locale}]

For retrieving meta information about a search destination such as board specific fields and their acceptable values.

**Requires an active session ID.**

### Fetch board meta [GET]

+ Parameters

    + board_name: `totaljobs` (string,required) - ID of board in question
    + locale: `fr` (string,optional) - For translating field data
        + Default: `en`
        + Members
            + `en`
            + `fr`
            + `de`
            + `en_US`
            + `es`
            + `ja`
            + `nl`
            + `pt_BR`
            + `pt`
            + `sv`
            + `th`

+ Request

    + Headers

            Content-Type: application/json
            x-ripple-session-id: abc123-432

+ Response 200 (application/json)

    + Body

            {
                "name": "totaljobs",
                "nice_name": "Total Jobs",
                "tokens": [
                    {
                        "Type" : "multilist",
                        "Options" : [
                           [
                              "Nul",
                              ""
                           ],
                           [
                              "EU",
                              "EU"
                           ],
                           [
                              "UK",
                              "UK"
                           ]
                        ],
                        "Id" : "totaljobs_eligibility",
                        "Board" : "totaljobs",
                        "SortMetric" : 90,
                        "Name" : "eligibility",
                        "Label" : "Work Eligibility"
                    },
                    ...
                ],
                "auth_tokens": [
                    ...
                ],
                "sort_by_token": {
                    ...
                }
            }

## - Saved Searches [/api/savedsearches{?page,rows,prefetch}]

A saved search is a named set of criteria which a user has *saved* for use at a later time.

**Requires an active session ID.**

### Fetch saved searches [GET]

+ Parameters

    + page: `1` (integer,optional) - To be used in conjunction with **rows** for paging through the saved searches.
        + Default: `1`
    + rows: `10` (integer,optional) - Limits the number of saved searches which are returned
        + Default: `100`
    + prefetch: `tokens` (enum[string],optional) - For expanding related data.
        + Members
            + `tokens`

+ Request

    + Headers

            Content-Type: application/json
            x-ripple-session-id: abc123-432

+ Response 200 (application/json)

    + Body

            {
                "searches": [
                    {
                        "insert_datetime": "2016-07-06 07:54:24",
                        "tokens": {
                            "salary_to": [
                                ""
                            ],
                            "salary_from": [
                                ""
                            ],
                            "include_unspecified_salaries": [
                                "1"
                            ],
                            "talentsearch_tags": [],
                            "distance_unit": [
                                "miles"
                            ],
                            "totaljobs_sort_by": [
                                "true~~Relevancy"
                            ],
                            "keywords": [
                                "perl"
                            ],
                            "talentsearch_advert_job_type": [],
                            "salary_per": [
                                "annum"
                            ],
                            "talentsearch_advert_industry": [],
                            "talentsearch_applicant_availability_date": [],
                            "default_jobtype": [
                                ""
                            ],
                            "location_within": [
                                30
                            ],
                            "cv_updated_within": [
                                "3M"
                            ],
                            "salary_cur": [
                                "GBP"
                            ]
                        },
                        "name": "a saved seach",
                        "active": "1",
                        "criteria_id": "VuTuELrGYahx4VApFS1UiWyF_2Q",
                        "flavour": "search",
                        "update_datetime": "2016-07-06 07:54:24",
                        "user_id": "2",
                        "id": "82"
                    }
                ]
            }


## Begin Search [/ripple/search]

Run a search against a single board, asynchronously. Any criteria can be specified under "query", even board specific criteria ( AKA channel extensions? ). For multiple values, use an array!

There are many ways to authenticate yourself to perform a search.

1. You are already logged in.
In this case, you can inject your ripple session id as headers (see Headers section below).

2. You are a third party with a third party api_key and secret_key who manages his own users and
board subscriptions details.
In this case, you can specify ```account: { provider: "third_party" , jwt_token: "abcde123456..." }```.
Contact Broadbean to get yourself a set of api_key/secret_key and know the recipe for baking the jwt_token.

3. You have a tight integration with broadbean's user system and hold a specific secret key
for authenticating users of a specific broadbean company. In this case, follow the example below.

### Run new search [POST]

+ Headers:
    + **x-api-key** : (optional) In case you are already logged in.
    + **x-ripple-session-id** : (optional) must contain the ripple_session_id, in case you are already logged in.

+ Request

    + Header

            Content-Type: application/json
            Accept: application/json

    + Body

            {
                "version": "0.1",
                "api_key": "XXXXXXXXXXXXX",
                "account": {
                    "username" : "pat@pat.pat.pat",
                    "signature" : {
                        "hash": "XXXXXXXXXXXXXX",
                        "time": "1423000000000"
                    }
                },
                "config": {
                    "results_per_page": "12"
                },
                "query": {
                    "keywords": "kittens",
                    "location_id": 12345,
                    "location": {
                        "country": "England",
                        "postcode": "E14 9TP",
                        "latitude": -1.24456,
                        "longitude": 14.2222
                    },
                    "radius": 30,
                    "job_type": "permanent",
                    "salary_cur": "GBP",
                    "salary_from": 30000,
                    "salary_to": 40000,
                    "salary_per": "week",
                    "cv_updated_within": "TODAY"
                },
                "channel": {
                    "type": "external",
                    "name": "monsterxml"
                },
                "candidate_action": {
                    "class": "Stream2::Action::ImportCandidate",
                    "options": {
                        "candidate_attributes": {
                            "job_requisition_ids" => [ "csp_job_id:12345" ]
                        }
                    }
                }
            }

    + Schema

            {
                "title": "Ripple JSON Schema",
                "type": "object",
                "required": [ "version", "api_key", "account", "channel" ],
                "properties": {
                    "version": {
                        "type": "number",
                        "enum": [0.1]
                    },
                    "api_key": {
                        "type": "string"
                    },
                    "account": {
                        "type": "object",
                        "oneOf": [
                            {
                                "required": ["provider", "jwt_token"],
                                "properties": {
                                   "provider": { "type": "string" },
                                   "jwt_token": { "type": "string" }
                                }
                            },
                            {
                                "required": ["username", "password"],
                                "properties": {
                                    "username": {
                                        "type": "string"
                                    },
                                    "password": {
                                        "type": "string"
                                    }
                                }
                            },
                            {
                                "required": ["username", "signature"],
                                "properties": {
                                    "signature": {
                                        "type": "object",
                                        "description": "As detailed here: http://api.adcourier.com/docs/index.cgi?page=passwordless",
                                        "required": [ "hash", "time" ],
                                        "properties": {
                                            "hash": {
                                                "type": "string"
                                            },
                                            "time": {
                                                "type": "integer"
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    "config": {
                        "title": "Options for searching and generating the GUI",
                        "type": "object",
                        "properties": {
                            "stylesheet_url": {
                                "description": "URL of hosted specific stylesheet to use when generating the interface",
                                "type": "string"
                            },
                            "no_adcourier_shortlist": {
                                "description": "Do NOT show the standard AdCourier shortlist button in case you dont give any shortlist settings",
                                "type": "boolean"
                            },
                            "shortlists": {
                                "description": "Collection of shortlist buttons",
                                "type": "array",
                                "items": {
                                    "type": "object",
                                    "required": [ "name", "label", "items_url" ],
                                    "properties": {
                                        "name": { "type": "string", "description": "Formal identifier name of the button." },
                                        "label": { "type": "string" , "description": "Free form label of the button" },
                                        "options_url": { "type": "string", "description": "URL to fetch the options from" }
                                    }
                                }
                            },
                            "new_candidate_url": {
                                "description": "URL search will send Saved and Shortlisted candidates",
                                "type": "string"
                            },
                            "tagged_doc_type": {
                                "description": "The type of document sent to new_candidate_url",
                                "type": "string"
                            },
                            "results_per_page": {
                                "description": "Only works on a limited number of boards, mostly internal",
                                "type": "number"
                            },
                            "hide_forward": {
                                "description": "HIDE the Forward button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_download": {
                                "description": "HIDE the Download button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_save": {
                                "description": "HIDE the Save button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_message": {
                                "description": "HIDE the Message button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_chargeable_message": {
                                "description": "HIDE the Chargeable Message button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_favourite": {
                                "description" : "HIDE the Favourite button. Deprecated",
                                "type": "boolean"
                            },
                            "hide_tagging": {
                                "description" : "HIDE the Tagging button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_save_internal": {
                                "description": "Hide the save button for internal boards",
                                "type": "boolean"
                            },
                            "show_adcourier_navigation": {
                                "description": "Will show AdCourier navigation, default 0",
                                "type": "boolean"
                            }

                        }
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "keywords": {
                                "type": "string"
                            },
                            "location_id": {
                                "type": "integer"
                            },
                            "location": {
                                "type": "object",
                                "properties": {
                                    "postcode": {
                                        "type": "string"
                                    },
                                    "country": {
                                        "type": "string",
                                        "description": "Any ISO 3166-1 alpha-2: see http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2, defaults to UK"
                                    },
                                    "latitude": {
                                        "type": "number"
                                    },
                                    "longitude": {
                                        "type": "number"
                                    }
                                },
                                "oneOf": [
                                    {
                                        "required": [
                                            "postcode",
                                            "country"
                                        ]
                                    },
                                    {
                                        "required": [
                                            "latitude",
                                            "longitude"
                                        ]
                                    }
                                ]
                            },
                            "radius": {
                                "type": "integer",
                                "default": 30,
                                "description": "Radius, in miles, to search from given location"
                            },
                            "job_type": {
                                "enum": [
                                    "",
                                    "permanent",
                                    "contract",
                                    "temporary"
                                ]
                            },
                            "salary_cur": {
                                "type": "string",
                                "description": "Any ISO 4217 currency code: see http://en.wikipedia.org/wiki/ISO_4217"
                            },
                            "salary_from": {
                                "type": "number"
                            },
                            "salary_to": {
                                "type": "number"
                            },
                            "salary_per": {
                                "enum": [
                                    "annum",
                                    "month",
                                    "week",
                                    "day",
                                    "hour"
                                ]
                            },
                            "cv_updated_within": {
                                "default": "3M",
                                "enum": [
                                    "ALL",
                                    "TODAY",
                                    "YESTERDAY",
                                    "3D",
                                    "1W",
                                    "2W",
                                    "1M",
                                    "2M",
                                    "3M",
                                    "6M",
                                    "1Y",
                                    "2Y",
                                    "3Y"
                                ]
                            },
                            "include_unspecified_salaries": {
                                "type": "boolean",
                                "default": true
                            }
                        }
                    },
                    "channel": {
                        "oneOf": [
                            {
                                "title": "Internal board search",
                                "type": "object",
                                "required": [ "type" ],
                                "properties": {
                                    "type": {
                                        "enum": [
                                            "internal", "all"
                                        ]
                                    }
                                }
                            },
                            {
                                "title": "External board search",
                                "type": "object",
                                "required": [ "type", "name" ],
                                "properties": {
                                    "type": {
                                        "enum": [
                                            "external"
                                        ]
                                    },
                                    "name": {
                                        "type": "string"
                                    }
                                }
                            }
                        ]
                    },
                    "candidate_action": {
                        "title": "Corollary action to be taken against the found candidates",
                        "type": "object",
                        "properties": {
                            "class": {
                                "type": "string"
                            },
                            "options": {
                                "type": "object",
                                "properties": {
                                    "candidate_attributes": {
                                        "type": "object"
                                    }
                                }
                            }
                        }
                    }
                }
            }


+ Response 200 (application/json)

    + Body

            {
                "status": "OK",
                "ripple_session_id": "XXXXXXXXXXXXXXXXXXXXXX"
            }

## - Search Status [/ripple/status]

Get current status of given job_id, can be "unstarted", "started", "completed".
Once a job is completed, this call will also return the result meta information ( such as total results ).

+ Headers:
    + **x-api-key** : (required) must be included in subsequent requests to authenticate against given job_id
    + **x-ripple-session-id** : (required) must contain the ripple_session_id, as given in search response

### Poll status of job [GET]
+ Request

    + Headers

            x-api-key: XXXXXXXXXXXXXXXX
            x-ripple-session-id: XXXXXXXXXXXXXXXX
            Content-Type: application/json


+ Response 200 (application/json)

    + Body

            {
                "status": "completed",
                "context": {
                    "result": {
                        "success": 1,
                        "max_pages": 1,
                        "destination": "talentsearch",
                        "current_page": 1,
                        "per_page": 50,
                        "results_per_scrape": 7,
                        "total_results": 7,
                        "expires_after": 10800,
                        "id": "33287134-B5D1-11E4-9C7E-88D1FE83DEAB"
                    }
                }
            }

## Search Results [/ripple/results{?render_results,url_lifetime}]

Retrieve search results for given job_id

+ Headers:
    + **x-api-key** : (required) must be included in subsequent requests to authenticate against given job_id
    + **x-ripple-session-id** : (required) must contain the ripple_session_id, as given in search response

+ Parameters

    + `render_results`: `true` (boolean,optional) - If true, will add an `html_result` attribute to each candidate, which contains the candidate information in HTML form
        **n.b. currently always true unless omitted**
        + Default: `false`
    + `url_lifetime`: `300` (integer,optional) - The amount of time (in seconds) to keep the urls returned in the response valid. Default is 15 minutes
        + Default: `360`

### Fetch results of job [GET]

+ Request

    + Header

            x-api-key: XXXXXXXXXXXXXXXX
            x-ripple-session-id: XXXXXXXXXXXXXXXX
            Content-Type: application/json

+ Response 200 (application/json)

    + Body

            {
                "results": [
                    {
                        "name": "John Smith",
                        "email": "john@example.com",
                        "mobile": 01234567890,
                        "telephone": 02345678901,
                        "address": "21 Example Street",
                        "latitude": -13.34874984,
                        "longitude": 45.34873487,

                        "candidate_id": 12345,
                        "idx": 0,
                        "results_id": "XXXXXXXXXXXXX",
                        "can_downloadcv": true,
                        "has_profile": 1,
                        "can_forward": 1,
                        "can_paymessage": true,
                        "has_cv": 1,
                        "destination": "talentsearch",
                        "profile_url": "https://search.adcourier.com/search/j6ApmKgooatC8SeSk5eSohK8gvc/talentsearch/page/1?search_id=4B09116A-2CB2-11E6-A601-7F2023614383&candidate_idx=2"
                    }
                ],
                "results_url": "https://search.adcourier.com/search/j6ApmKgooatC8SeSk5eSohK8gvc/talentsearch/page/1?search_id=0B56CAC4-2CB4-11E6-A601-7F2023614383"
            }

## - Create Search Criteria [/api/create_criteria]

Generate a new criteria ID with the criteria data provided.

### Create Search Criteria [POST]

+ Request

    + Header

            Content-Type: application/x-www-form-urlencoded
            Accept: application/x-www-form-urlencoded

    + Body

            q={criteria: { "keywords": "Project Manager", "location_id": 40654 }, "board": "totaljobs"}

+ Response 200 (application/json)


        {
            "data": {
                "id": EWJ490U25JR092J0F,
                "search_url": "https://search.adcourier.com/search/EWJ490U25JR092J0F/totaljobs/page/1"
            }
        }

## Blocking Search [/ripple/search-results]

Run a search against a single board, **synchronously**. Any criteria can be specified under "query", even board specific criteria ( AKA channel extensions? ). For multiple values, use an array!

There are many ways to authenticate yourself to perform a search.

1. You are already logged in.
In this case, you can inject your ripple session id as headers (see Headers section below).

2. You are a third party with a third party api_key and secret_key who manages his own users and
board subscriptions details.
In this case, you can specify ```account: { provider: "third_party" , jwt_token: "abcde123456..." }```.
Contact Broadbean to get yourself a set of api_key/secret_key and know the recipe for baking the jwt_token.

3. You have a tight integration with broadbean's user system and hold a specific secret key
for authenticating users of a specific broadbean company. In this case, follow the example below.

### Run new search [POST]

#### About the response

##### Canonical fields

In the results, all the fields starting with 'canonical' will have
the same name accross all boards (when available).

##### Facets

Along the results, you will also find a *facets* array. It contains further filtering options
that can be applied to the results.

Each item contains amongst other properties an *Id*, a *Type* and an array of *Options* when
the *Type* is ```list``` or ```multilist```.

Each option is an array of ```[ <Actual Value>, <Label>, <Count> ]```. The 'Count' is optional and
is only there when the facet is dynamically computed by the backend search engine.

For some legacy facets, it's possible that it will contain a simple array of possible *Values*.
In this case, use this instead of *Options*. This is set to go away in the mid-term future.

To send a value for one of these facets to the search in order to filter your results further,
use the Id and include it as a parameter in the 'query' part of the request. For multilist, you
can specify multiple values by giving an array of values instead of just one.

For instance:

```
    "query": {
        <example_text_field>: "Bla",
        <example_list_field>: "the_selected_value",
        <example_multilist_field>: [ "the" , "selected", "values" ]
    }
```

+ Headers:
    + **x-api-key** : (optional) In case you are already logged in.
    + **x-ripple-session-id** : (optional) must contain the ripple_session_id, in case you are already logged in.

+ Request

    + Header

            Content-Type: application/json
            Accept: application/json

    + Body

            {
                "version": "0.1",
                "api_key": "XXXXXXXXXXXXX",
                "account": {
                    "username" : "pat@pat.pat.pat",
                    "signature" : {
                        "hash": "XXXXXXXXXXXXXX",
                        "time": "1423000000000"
                    }
                },
                "config": {
                    "results_per_page": "12"
                },
                "query": {
                    "keywords": "kittens",
                    "location_id": 12345,
                    "location": {
                        "country": "England",
                        "postcode": "E14 9TP",
                        "latitude": -1.24456,
                        "longitude": 14.2222
                    },
                    "radius": 30,
                    "job_type": "permanent",
                    "salary_cur": "GBP",
                    "salary_from": 30000,
                    "salary_to": 40000,
                    "salary_per": "week",
                    "cv_updated_within": "TODAY"
                },
                "channel": {
                    "type": "external",
                    "name": "monsterxml"
                },
                "candidate_action": {
                    "class": "Stream2::Action::ImportCandidate",
                    "options": {
                        "candidate_attributes": {
                            "job_requisition_ids" => [ "csp_job_id:12345" ]
                        }
                    }
                }
            }

    + Schema

            {
                "title": "Ripple JSON Schema",
                "type": "object",
                "required": [ "version", "api_key", "account", "channel" ],
                "properties": {
                    "version": {
                        "type": "number",
                        "enum": [0.1]
                    },
                    "api_key": {
                        "type": "string"
                    },
                    "account": {
                        "type": "object",
                        "oneOf": [
                            {
                                "required": ["provider", "jwt_token"],
                                "properties": {
                                   "provider": { "type": "string" },
                                   "jwt_token": { "type": "string" }
                                }
                            },
                            {
                                "required": ["username", "password"],
                                "properties": {
                                    "username": {
                                        "type": "string"
                                    },
                                    "password": {
                                        "type": "string"
                                    }
                                }
                            },
                            {
                                "required": ["username", "signature"],
                                "properties": {
                                    "signature": {
                                        "type": "object",
                                        "description": "As detailed here: http://api.adcourier.com/docs/index.cgi?page=passwordless",
                                        "required": [ "hash", "time" ],
                                        "properties": {
                                            "hash": {
                                                "type": "string"
                                            },
                                            "time": {
                                                "type": "integer"
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    "config": {
                        "title": "Options for searching and generating the GUI",
                        "type": "object",
                        "properties": {
                            "stylesheet_url": {
                                "description": "URL of hosted specific stylesheet to use when generating the interface",
                                "type": "string"
                            },
                            "no_adcourier_shortlist": {
                                "description": "Do NOT show the standard AdCourier shortlist button in case you dont give any shortlist settings",
                                "type": "boolean"
                            },
                            "shortlists": {
                                "description": "Collection of shortlist buttons",
                                "type": "array",
                                "items": {
                                    "type": "object",
                                    "required": [ "name", "label", "items_url" ],
                                    "properties": {
                                        "name": { "type": "string", "description": "Formal identifier name of the button." },
                                        "label": { "type": "string" , "description": "Free form label of the button" },
                                        "options_url": { "type": "string", "description": "URL to fetch the options from" }
                                    }
                                }
                            },
                            "new_candidate_url": {
                                "description": "URL search will send Saved and Shortlisted candidates",
                                "type": "string"
                            },
                            "tagged_doc_type": {
                                "description": "The type of document sent to new_candidate_url",
                                "type": "string"
                            },
                            "results_per_page": {
                                "description": "Only works on a limited number of boards, mostly internal",
                                "type": "number"
                            },
                            "hide_forward": {
                                "description": "HIDE the Forward button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_download": {
                                "description": "HIDE the Download button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_save": {
                                "description": "HIDE the Save button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_message": {
                                "description": "HIDE the Message button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_chargeable_message": {
                                "description": "HIDE the Chargeable Message button in general for this integration",
                                "type": "boolean"
                            },
                            "hide_favourite": {
                                "description" : "HIDE the Favourite button. Deprecated",
                                "type": "boolean"
                            },
                            "hide_tagging": {
                                "description" : "HIDE the Tagging button in general for this integration",
                                "type": "boolean"
                            },
                            "show_adcourier_navigation": {
                                "description": "Will show AdCourier navigation, default 0",
                                "type": "boolean"
                            }
                        }
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "keywords": {
                                "type": "string"
                            },
                            "location_id": {
                                "type": "integer"
                            },
                            "location": {
                                "type": "object",
                                "properties": {
                                    "postcode": {
                                        "type": "string"
                                    },
                                    "country": {
                                        "type": "string",
                                        "description": "Any ISO 3166-1 alpha-2: see http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2, defaults to UK"
                                    },
                                    "latitude": {
                                        "type": "number"
                                    },
                                    "longitude": {
                                        "type": "number"
                                    }
                                },
                                "oneOf": [
                                    {
                                        "required": [
                                            "postcode",
                                            "country"
                                        ]
                                    },
                                    {
                                        "required": [
                                            "latitude",
                                            "longitude"
                                        ]
                                    }
                                ]
                            },
                            "radius": {
                                "type": "integer",
                                "default": 30,
                                "description": "Radius, in miles, to search from given location"
                            },
                            "job_type": {
                                "enum": [
                                    "",
                                    "permanent",
                                    "contract",
                                    "temporary"
                                ]
                            },
                            "salary_cur": {
                                "type": "string",
                                "description": "Any ISO 4217 currency code: see http://en.wikipedia.org/wiki/ISO_4217"
                            },
                            "salary_from": {
                                "type": "number"
                            },
                            "salary_to": {
                                "type": "number"
                            },
                            "salary_per": {
                                "enum": [
                                    "annum",
                                    "month",
                                    "week",
                                    "day",
                                    "hour"
                                ]
                            },
                            "cv_updated_within": {
                                "default": "3M",
                                "enum": [
                                    "ALL",
                                    "TODAY",
                                    "YESTERDAY",
                                    "3D",
                                    "1W",
                                    "2W",
                                    "1M",
                                    "2M",
                                    "3M",
                                    "6M",
                                    "1Y",
                                    "2Y",
                                    "3Y"
                                ]
                            },
                            "include_unspecified_salaries": {
                                "type": "boolean",
                                "default": true
                            }
                        }
                    },
                    "channel": {
                        "oneOf": [
                            {
                                "title": "Internal board search",
                                "type": "object",
                                "required": [ "type" ],
                                "properties": {
                                    "type": {
                                        "enum": [
                                            "internal", "all"
                                        ]
                                    }
                                }
                            },
                            {
                                "title": "External board search",
                                "type": "object",
                                "required": [ "type", "name" ],
                                "properties": {
                                    "type": {
                                        "enum": [
                                            "external"
                                        ]
                                    },
                                    "name": {
                                        "type": "string"
                                    }
                                }
                            }
                        ]
                    },
                    "candidate_action": {
                        "title": "Corollary action to be taken against the found candidates",
                        "type": "object",
                        "properties": {
                            "class": {
                                "type": "string"
                            },
                            "options": {
                                "type": "object",
                                "properties": {
                                    "candidate_attributes": {
                                        "type": "object"
                                    }
                                }
                            }
                        }
                    }
                }
            }


+ Response 200 (application/json)

    + Attributes

        + `ripple_session_id` (string) - Current ripple session ID.
        + `search_url` (string) - Sessionised URL, can be used to run a new search interactively.
        + `context` (object)
            + `max_pages` (number) - Total number of available pages.
            + `per_page` (number) - Number of results per page.
            + `destination` (string) - The external source of the results.
            + `current_page` (string) - Page from which current results are derrived.
            + `notices` (array)
                + (string) - Free text string which should act as warnings to the user.
            + `total_results` (number) - Total number of results accross all pages.
            + `expires_after` (number) - The number of seconds indicating how long these results are actionable.
            + `search_record_id` (number) - The ID of an internally logged search record.
            + `id` (string) - Identifier of the Search results cache.
        + `data` (object)
            + `results_url` (string) - Sessionised URL - Allows the viewing of these results within the Search product.
            + `facets` (array)  - Each is the description of a board specific filter including options, label (see documentation for more info)
                + (object)
                    + `Id` (string) - The name of the facet and to which it should be refered in search requests.
                    + `Label` (string) - The "nice name" or description of the facet ( for display purposes )
                    + `Type` (enum[string])
                        + Members
                            + `list` - Facet will have "Options" and accept one value
                            + `multilist` - Facet will have "Options" and accept multiple values
                            + `text` - Facet accepts a free-text value
                    + `Options` (array) - e.g. [ [ 123, 'My Option 1', 45 ], [ 456, 'My Option 2', 18 ] ]
                        + (array)
                            + (string) - Option value
                            + (string) - Option label
                            + (number) - Number of matching documents ( not always applicable ).
                    + `SortMetric` (string) - Suggested facet ordering metric (lower number means more important).
                    + `Name` (string) - Board unspecific name, use `Id` instead.
                    + `Size` (number) - Max characters for text type.

            + `results` (array)
                + (object)
                    + `name` (string)
                    + `email` (string)
                    + `mobile` (string)
                    + `telephone` (string)
                    + `address` (string)
                    + `results_id` (string)
                    + `actions` (object)
                        + `profile` (string) - Sessionised URL: fetches a JSON version of the candidates profile ( including HTML ).
                        + `cv` (string) - Sessionised URL: fetches the binary CV.
                        + `profile_history` (string) - Sessionised URL: fetches a JSON list of historical actions carried out against this candidate by this user.
                    + `html_result` (string) - A structured HTML snippet containing board specific attributes for a candidate.

    + Body

            {
                "ripple_session_id":"XXXXXXXXXXXXXXXXXXXXXX",
                "search_url":"https://long-url",
                "context":{
                    "max_pages": 4,
                    "per_page": 50,
                    "destination": "cvlibrary",
                    "current_page": 1,
                    "notices": [ "this is a notice" ],
                    "total_results": 176,
                    "expires_after": 10800,
                    "search_record_id": 142,
                    "id": "00FA4-00FA4-00FA4-00FA4",
                },
                "data":{
                    "results_url":"https://long-url",
                    "facets": [
                                {
                                    "Id" : "talentsearch_applicant_recruitment_status",
                                    "Type" : "list OR multilist",
                                    "Label" : "Recruitment Status",
                                    "Options" : [
                                                    [
                                                        "Any",
                                                         ""
                                                    ],
                                                    [
                                                        "Available",
                                                        "Available"
                                                    ]
                                                ],
                                    "Board" : "talentsearch",
                                    "SortMetric" : 700,
                                    "Name" : "applicant_recruitment_status",
                                },
                                {
                                    "Id" : "talentsearch_employer_org_name",
                                    "Type" : "text",
                                    "Label" : "Current Employer",
                                    "Size" : 60,
                                    "Essential" : 1,
                                    "Board" : "talentsearch",
                                    "Name" : "employer_org_name",
                                    "SortMetric" : 500
                                },
                                {
                                    "Essential" : 1,
                                    "Id" : "talentsearch_tags",
                                    "Type" : "tag"
                                    "Label" : "Tags/Hotlists",
                                    "Name" : "tags",
                                    "Options" : [
                                                    [
                                                        "such tag",
                                                        "such tag",
                                                        2
                                                    ],
                                                ],
                                    "Size" : 60,
                                    "SortMetric" : 100,
                                },
                               {
                                    "Id" : "talentsearch_job_type",
                                    "Type" : "multilist"
                                    "Label" : "Job Type",
                                    "Name" : "advert_job_type",
                                    "Options" : [
                                                    [
                                                        "Permanent",
                                                        "Permanent",
                                                        5
                                                    ],
                                                    [
                                                        "Contract",
                                                        "Contract",
                                                        1
                                                    ]
                                                ],
                                    "SortMetric" : 300,
                                }
                    ],
                    "results": [
                        {
                            "canonical_address": "12 rue de la pierre en bois",
                            "canonical_city": "Petaouchnok",
                            "canonical_country": "NL",
                            "canonical_cv_text": "Long .. CV .. Text",
                            "canonical_education": "Some .. Text",
                            "canonical_education_level": "SomeLevel",
                            "canonical_employer": "Big Company.plc",
                            "canonical_industry": "BigIndustry",
                            "canonical_job_description": "Some .. Text",
                            "canonical_job_title": "Big Cheese",
                            "canonical_last_activity": "2016/12",
                            "canonical_mobile": "077123456789",
                            "canonical_name": "John Smith",
                            "canonical_postal_code": "SW6 1AB",
                            "canonical_state": "NV",
                            "canonical_telephone": "020123456789",
                            "name": "John Smith",
                            "email": "john@example.com",
                            "mobile": 01234567890,
                            "telephone": 02345678901,
                            "address": "21 Example Street",
                            "latitude": -13.34874984,
                            "longitude": 45.34873487,
                            "candidate_id": 12345,
                            "idx": 0,
                            "results_id": "XXXXXXXXXXXXX",
                            "can_downloadcv": true,
                            "has_profile": 1,
                            "can_forward": 1,
                            "can_paymessage": true,
                            "has_cv": 1,
                            "destination": "talentsearch",
                            "profile_url": "https://long-url"
                            "actions": {
                                "profile": "<sessionised_url>",
                                "profile_history": "<sessionised_url>",
                                "cv": "<sessionised_url>"
                            }
                        }
                    ]
                }
            }

+ Response 400 (application/json)

    + Attributes

        + `error` (object)
            + `message` (string) - A friendly error message describing the error that occurred on the channel.
            + `log_id` (string) - Log ID of this search. Quote this with your error report to help us debug :)
            + `properties` (object)
                + `type` (string) - Identifier for the type of error that occurred. Can be one of the following: `loginerror|timeout|critical|internal|inactiveaccount|nocvsearch|lackofcredit|accountinuse|toomanyresults|invalidbooleansearch|keywordstoolong|unavailable|invalidemailaddress|cvunavailable|notsupported|profilenotsupported|cvnotready|nocv|watchdognotsupported|quotalimitexceeded|shortlistingerror|forwardingerror|messagingerror`

    + Body

        {
            "error": {
                "message": "No CV: This candidate does not have a CV",
                "log_id": "XXXXXXXXXXXXX",
                "properties": {
                    "type": "nocv"
                }
            }
        }

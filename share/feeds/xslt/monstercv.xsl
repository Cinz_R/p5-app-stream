<?xml version='1.0'?>
<!DOCTYPE xsl:stylesheet [
]>
<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:M="http://schemas.monster.com/Monster">
<xsl:output method="html" version="4.0" indent="yes"/>
<xsl:template match="/">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" type="text/css" href="http://media.ad-courier.com/css/oo/monstercv.css"/>

        <title><xsl:call-template name="candidate_name" /></title>
    </head>
        <body>
            <div class="page">
                <div class="head">

                    <xsl:choose>
                        <xsl:when test="//M:Account/M:Confidential/text()='1'">
                            <h1><xsl:call-template name="candidate_name" /></h1>
                            <p><xsl:value-of select="//M:PersonalData/M:Contact/M:E-mail"/></p>
                        </xsl:when>
                        <xsl:otherwise><!-- start of public candidate -->
                            <xsl:for-each select="//M:PersonalData/M:Contact[1]">
                                <h1>
                                    <xsl:call-template name="candidate_name" />

                                    (<xsl:value-of select="./M:E-mail[1]"/>)

                                </h1>

                                <p>
                                    <xsl:if test="./M:Address/M:StreetAddress/text()">
                                        <xsl:value-of select="./M:Address/M:StreetAddress"/>,
                                    </xsl:if>
                                    <xsl:if test="./M:Address/M:City/text()">
                                        <xsl:call-template name="nbsp" /><xsl:value-of select="./M:Address/M:City"/>,
                                    </xsl:if>
                                    <xsl:if test="./M:Address/M:State/text()">
                                        <xsl:call-template name="nbsp" /><xsl:value-of select="./M:Address/M:State"/>,
                                    </xsl:if>
                                    <xsl:if test="./M:Address/M:CountryCode/text()">
                                        <xsl:call-template name="nbsp" /><xsl:value-of select="./M:Address/M:CountryCode"/>,
                                    </xsl:if>
                                    <xsl:if test="./M:Address/M:PostalCode/text()">
                                        <xsl:call-template name="nbsp" /><xsl:value-of select="./M:Address/M:PostalCode"/>
                                    </xsl:if>
                                </p>

                                <p>
                                    <xsl:for-each select="./M:Phones/M:Phone[@phoneType='mobile']">
                                        <strong>Mobile:</strong><xsl:call-template name="nbsp" /><xsl:value-of select="text()"/><xsl:call-template name="nbsp" /><xsl:call-template name="nbsp" />
                                    </xsl:for-each>

                                    <xsl:for-each select="./M:Phones/M:Phone[@phoneType='home']">
                                        <strong>Home:</strong><xsl:call-template name="nbsp" /><xsl:value-of select="text()"/><xsl:call-template name="nbsp" /><xsl:call-template name="nbsp" />
                                    </xsl:for-each>
                                </p>

                                <xsl:if test="./M:WebSite">
                                    <p><xsl:value-of select="./M:WebSite"/></p>
                                </xsl:if>

                            </xsl:for-each>
                        </xsl:otherwise><!-- end of public candidate -->
                    </xsl:choose>

                </div><!-- .head -->

                <div class="body">
                    <div class="main">

        <xsl:for-each select="//M:Resumes/M:Resume">
            <h2><xsl:value-of select="./M:ResumeTitle"/></h2>

            <xsl:if test="./M:Objective/text()">
                <div class="hr"><hr /></div>

                <p class="career_objective"><xsl:value-of select="./M:Objective"/></p>
            </xsl:if>

            <div class="hr"><hr /></div>

            <div class="mod">
                <div class="hd"><h3>Personal Info</h3></div>
                <div class="bd">
                    <table class="facts">

                        <xsl:if test="//M:ContactPreference/text()">
                        <tr>
                            <th scope="row">Contact by</th>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="//M:ContactPreference/text()='Mobile Phone'">
                                        Mobile (<xsl:value-of select="//M:Phones/M:Phone[@phoneType='mobile' and @priority='primary']"/>)
                                    </xsl:when>
                                    <xsl:when test="//M:ContactPreference/text()='Email'">
                                        Email (<xsl:value-of select="//M:E-mail"/>)
                                    </xsl:when>
                                    <xsl:otherwise>
                                         <xsl:value-of select="//M:ContactPreference"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                    </xsl:if>

                        <tr>
                            <th scope="row">Career Level</th>
                            <td><xsl:value-of select="//M:CareerLevel"/></td>
                        </tr>
                        <tr>
                            <th scope="row">Availability</th>
                            <td>
                                <xsl:call-template name="datetime_to_date">
                                    <xsl:with-param name="datetime" select="//M:Availability"/>
                                </xsl:call-template>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">Job status</th>
                            <td><xsl:for-each select="//M:JobStatus">
                                    <xsl:value-of select="text()"/>
                                    <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                            </xsl:for-each></td>
                        </tr>

                        <tr>
                            <th scope="row">Job type</th>
                            <td>
                                <xsl:for-each select="//M:JobType">
                                    <xsl:choose>
                                        <xsl:when test="@monsterId=1">Permanent</xsl:when>
                                        <xsl:when test="@monsterId=2">Contract</xsl:when>
                                        <xsl:when test="@monsterId=3">Temporary</xsl:when>
                                        <xsl:when test="@monsterId=4">Intern</xsl:when>
                                        <xsl:when test="@monsterId=5">Seasonal</xsl:when>
                                        <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
                                    </xsl:choose>
                                    <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                                </xsl:for-each>
                            </td>
                        </tr>

                        <xsl:if test="//M:Shifts/M:Shift">
                            <tr>
                                <th scope="row">Shifts</th>
                                <td>
                                    <xsl:for-each select="//M:Shifts/M:Shift">
                                        <xsl:value-of select="."/>
                                        <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                                    </xsl:for-each>
                                </td>
                            </tr>
                        </xsl:if>

                        <tr>
                            <th scope="row">Total Years Experience</th>
                            <td><xsl:value-of select="//M:TotalYearsWorkExperience"/></td>
                        </tr>
                        <tr>
                            <th scope="row">Highest Education Degree</th>
                            <td><xsl:value-of select="//M:HighestEducationDegree"/></td>
                        </tr>

                        <xsl:if test="//M:YearsInLeadership">
                            <tr>
                                <th scope="row">Years in Leadership</th>
                                <td><xsl:value-of select="//M:YearsInLeadership"/></td>
                            </tr>
                        </xsl:if>

                        <xsl:if test="//M:YearsInLeadership">
                            <tr>
                                <th scope="row">Years in Management</th>
                                <td><xsl:value-of select="//M:YearsInManagement"/></td>
                            </tr>
                        </xsl:if>

                        <xsl:if test="//M:AuthStatus[@monsterId='1']/parent::M:WorkAuthorization">
                            <tr>
                                <th scope="row">Work Authorization</th>
                                <td>
                            <xsl:for-each select="//M:AuthStatus[@monsterId='1']/parent::M:WorkAuthorization">
                                <xsl:value-of select="./M:CountryCode"/>
                                <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                            </xsl:for-each>
                                </td>
                            </tr>
                        </xsl:if>
                    </table>
                </div>
            </div>

            <xsl:if test="./M:TextResume/text()">
                <div class="hr"><hr /></div>

                <div class="mod">
                    <div class="bd">
                        <xsl:value-of select="./M:TextResume" disable-output-escaping="yes"/>
                    </div><!-- .bd -->
                </div><!-- .mod -->
            </xsl:if>

            <div class="hr"><hr /></div>

            <div class="mod">
                <div class="hd"><h3>Skills</h3></div>
                <div class="bd">
            <p><xsl:for-each select="//M:Skill|//M:Language">
                    <xsl:choose>
                        <xsl:when test="./M:LanguageName"><xsl:value-of select="./M:LanguageProficiency"/><xsl:text> </xsl:text><xsl:value-of select="./M:LanguageName"/></xsl:when>
                        <xsl:when test="./@yearsOfExperience">
                            <xsl:value-of select="./M:SkillName"/> (<xsl:value-of select="@yearsOfExperience"/> years)
                        </xsl:when>
                        <xsl:otherwise><xsl:value-of select="./M:SkillName"/> (<xsl:value-of select="./M:SkillLevel"/>)</xsl:otherwise>
                    </xsl:choose>
                    <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                </xsl:for-each>
            </p>
                </div><!-- .bd -->
            </div><!-- .mod -->


            <div class="hr"><hr /></div>

            <!-- Experience //-->
            <div class="timeline">
                <div class="hd"><h3>Previous Experience</h3></div>

            <xsl:for-each select="./M:EmploymentHistory/M:Position">
                <xsl:sort select=".//M:DateRange/@isCurrent" order="descending"/>
                <xsl:sort select=".//M:EndDate/M:Year" order="descending" data-type="number"/>
                <xsl:sort select=".//M:EndDate/M:Month" order="descending" data-type="number"/>
                <xsl:sort select=".//M:StartDate/M:Year" order="ascending" data-type="number"/>
                <xsl:sort select=".//M:StartDate/M:Month" order="ascending" data-type="number"/>

                <div class="bd line">
                    <div class="unit sizeDateCell">
                        <span class="badge">
                            <xsl:choose>
                                <xsl:when test="./M:DateRange">
                                    <xsl:value-of select=".//M:StartDate/M:Month"/>/<xsl:value-of select=".//M:StartDate/M:Year"/>
                                    <xsl:call-template name="nbsp" />-<xsl:call-template name="nbsp" />
                                    <xsl:choose>
                                        <xsl:when test="./M:DateRange[@isCurrent='true']">
                                            Current
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select=".//M:EndDate/M:Month"/>
                                            /<xsl:value-of select=".//M:EndDate/M:Year"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="nbsp" />
                                </xsl:otherwise>
                            </xsl:choose>
                        </span>
                    </div>
                    <div class="unit lastUnit">
                        <h4>
                            <xsl:value-of select="./M:PositionTitle"/>
                            <xsl:call-template name="nbsp" />(<xsl:value-of select="./M:EmployerName"/>)
                        </h4>

                        <xsl:if test="./M:EmploymentLocation">
                            <p><xsl:value-of select="./M:EmploymentLocation"/></p>
                        </xsl:if>

                        <xsl:if test="./M:ExperienceDescription">
                            <p><xsl:value-of select="./M:ExperienceDescription"/></p>
                        </xsl:if>

                        <!-- UNUSED ELEMENTS: ExperienceIndustry, EmploymentLocation -->
                    </div>

                </div><!-- .bd //-->
            </xsl:for-each>
            </div><!-- .mod -->

            <div class="hr"><hr /></div>

            <!-- Education //-->
            <div class="timeline">
                <div class="hd"><h3>Education</h3></div>
                <xsl:for-each select=".//M:EducationalHistory/M:SchoolOrInstitution">
                    <xsl:sort select=".//M:DateRange/@isCurrent" order="descending"/>
                    <xsl:sort select=".//M:EndDate/M:Year" order="descending" data-type="number"/>
                    <xsl:sort select=".//M:EndDate/M:Month" order="descending" data-type="number"/>
                    <xsl:sort select=".//M:StartDate/M:Year" order="ascending" data-type="number"/>
                    <xsl:sort select=".//M:StartDate/M:Month" order="ascending" data-type="number"/>

                    <div class="bd line">
                        <div class="unit sizeDateCell">
                            <span class="badge">
                                <xsl:if test="./M:DateRange/M:StartDate">
                                    <xsl:value-of select="./M:DateRange/M:StartDate/M:Month"/>/<xsl:value-of select="./M:DateRange/M:StartDate/M:Year"/><xsl:text> - </xsl:text>
                                </xsl:if>

                                <xsl:value-of select="./M:DateRange/M:EndDate/M:Month"/>/<xsl:value-of select="./M:DateRange/M:EndDate/M:Year"/>
                            </span>
                        </div><!-- .sizeDateCell -->

                        <div class="unit lastUnit">

                            <h4>
                                <xsl:value-of select=".//M:EducationDegree"/><xsl:call-template name="nbsp" />-<xsl:call-template name="nbsp" /> 

                                <xsl:value-of select="./M:SchoolName"/>
                            </h4>

                            <xsl:if test=".//M:Location">
                                <p><xsl:value-of select=".//M:Location/M:City"/>, <xsl:value-of select=".//M:Location/M:State"/>, <xsl:value-of select=".//M:Location/M:CountryCode"/></p>
                            </xsl:if>

                            <xsl:if test="./M:EducationSummary">
                                <p><xsl:value-of select="./M:EducationSummary"/></p>
                            </xsl:if>

                        </div><!-- .unit .lastUnit -->

                    </div><!-- .bd //-->
                </xsl:for-each>
            </div><!-- .mod -->

        <div class="hr"><hr /></div>

        <!-- EDUCATION: Not using GPA -->


        <div class="mod">
            <div class="hd"><h3>Desired Job and Occupation</h3></div>
            <div class="bd">
                <table class="facts">
                    <xsl:if test="//M:DesiredCompensation/M:CompensationType">
                        <tr>
                            <th scope="row">Desired salary</th>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="//M:DesiredCompensation/M:RateMax/text()">
                                        <xsl:choose>
                                            <xsl:when test="//M:DesiredCompensation/M:RateMin/text()">
                                                <xsl:value-of select="//M:DesiredCompensation/M:Currency"/><xsl:text> </xsl:text>
                                                <xsl:value-of select="//M:DesiredCompensation/M:RateMin"/><xsl:call-template name="nbsp" />-<xsl:call-template name="nbsp" /><xsl:value-of select="//M:DesiredCompensation/M:RateMax"/><xsl:text> </xsl:text>
                                                <xsl:value-of select="//M:DesiredCompensation/M:CompensationType"/>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:value-of select="//M:DesiredCompensation/M:Currency"/><xsl:text> </xsl:text>
                                                <xsl:value-of select="//M:DesiredCompensation/M:RateMax"/><xsl:text> </xsl:text>
                                                <xsl:value-of select="//M:DesiredCompensation/M:CompensationType"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:when test="//M:DesiredCompensation/M:RateMin/text()">
                                        <xsl:value-of select="//M:DesiredCompensation/M:Currency"/><xsl:text> </xsl:text>
                                        <xsl:value-of select="//M:DesiredCompensation/M:RateMin"/><xsl:text> </xsl:text>
                                        <xsl:value-of select="//M:DesiredCompensation/M:CompensationType"/> +
                                    </xsl:when>
                                    <xsl:when test="//M:DesiredCompensation/M:Rate/text()">
                                        <xsl:value-of select="//M:DesiredCompensation/M:Currency"/><xsl:text> </xsl:text>
                                        <xsl:value-of select="//M:DesiredCompensation/M:Rate"/><xsl:text> </xsl:text>
                                        <xsl:value-of select="//M:DesiredCompensation/M:CompensationType"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        Unknown
                                    </xsl:otherwise>
                                </xsl:choose>
                            </td>
                        </tr>
                    </xsl:if>

                    <tr>
                        <th scope="row">Willing to work weekends</th>
                        <td><xsl:value-of select="//M:Weekend"/></td>
                    </tr>
                    <tr>
                        <th scope="row">Willing to do overtime</th>
                        <td><xsl:choose><xsl:when test="//M:Overtime/text()='true'">Yes</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
                    </tr>
                    <tr>
                        <th scope="row">Willing to relocate</th>
                        <td><xsl:choose><xsl:when test="//M:WillingToRelocate/text()='true'">Yes</xsl:when><xsl:otherwise>No</xsl:otherwise></xsl:choose></td>
                    </tr>

                    <xsl:if test="//M:WillingtoTravel">
                        <tr>
                            <th scope="row">Willing to travel</th>
                            <td><xsl:choose>
                                    <xsl:when test="//M:WillingtoTravel/@monsterId='1'">No travel required</xsl:when>
                                    <xsl:when test="//M:WillingtoTravel/@monsterId='2'">Up to 25% travel</xsl:when>
                                    <xsl:when test="//M:WillingtoTravel/@monsterId='3'">Up to 50% travel</xsl:when>
                                    <xsl:when test="//M:WillingtoTravel/@monsterId='4'">Up to 75% travel</xsl:when>
                                    <xsl:when test="//M:WillingtoTravel/@monsterId='5'">100% travel</xsl:when>
                                </xsl:choose></td>
                        </tr>
                    </xsl:if>


                    <xsl:if test="//M:IdealJobDescription">
                        <tr>
                            <th scope="row">Ideal Job</th>
                            <td><xsl:value-of select="//M:IdealJobDescription"/></td>
                        </tr>
                    </xsl:if>
                    <xsl:if test="//M:CompanyDescription">
                        <tr>
                            <th scope="row">Ideal Employer</th>
                            <td><xsl:value-of select="//M:CompanyDescription"/></td>
                        </tr>
                    </xsl:if>

                    <xsl:if test="//M:EmployerSize">
                        <tr>
                            <th scope="row">Employer size</th>
                            <td>
                                <xsl:choose>
                                    <xsl:when test="//M:EmployerSize/@monsterId='2'">No preference</xsl:when>
                                    <xsl:when test="//M:EmployerSize/@monsterId='3'">Small (1-99)</xsl:when>
                                    <xsl:when test="//M:EmployerSize/@monsterId='4'">Medium (100-999)</xsl:when>
                                    <xsl:when test="//M:EmployerSize/@monsterId='5'">Large (1000+)</xsl:when>
                                </xsl:choose>
                            </td>
                        </tr>
                    </xsl:if>

                    <tr>
                        <th scope="row">Desired category</th>
                        <td><xsl:for-each select="//M:TargetEmployerCategory">
                                <xsl:value-of select="."/>
                                <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                        </xsl:for-each></td>
                    </tr>
                    <tr>
                        <th scope="row">Desired industry</th>
                        <td><xsl:for-each select="//M:Industry">
                                <xsl:value-of select="./M:IndustryName"/>
                                <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                        </xsl:for-each></td>
                    </tr>
                    <tr>
                        <th scope="row">Desired occupation</th>
                        <td><xsl:for-each select="//M:TargetEmployerOccupation">
                                <xsl:value-of select="./M:Name"/>
                                <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                        </xsl:for-each></td>
                    </tr>
                    <tr>
                        <th scope="row">Desired job titles</th>

                        <td><xsl:for-each select="//M:TargetJobTitles/M:TargetJobTitle">
                                <xsl:value-of select="./M:Title"/>
                                <xsl:if test="@type='desired'"><xsl:call-template name="nbsp" />(preferred)</xsl:if>
                                <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                            </xsl:for-each>
                        </td>
                    </tr>

                    <tr>
                        <th scope="row">Desired location</th>
                        <td><xsl:for-each select="//M:GeographicPref">
                                <xsl:choose>
                                    <xsl:when test="./M:City">
                                        <xsl:value-of select="./M:City"/>
                                        <xsl:text> </xsl:text>(<xsl:value-of select="./M:State"/>)
                                    </xsl:when>
                                    <xsl:when test="./M:State">
                                        <xsl:value-of select="./M:State"/>
                                        <xsl:text> </xsl:text>(<xsl:value-of select="./M:CountryCode"/>)
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="./M:CountryCode"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:if test="not(position() = last())"><xsl:text>, </xsl:text></xsl:if>
                            </xsl:for-each>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="hr"><hr /></div>

        <xsl:if test="//M:AdditionalInformation">
            <div class="mod">
                <div class="hd"><h3>Additional Information</h3></div>
                <div class="bd"><p><xsl:value-of select="//M:AdditionalInformation"/></p></div>
            </div><!-- .mod -->

            <div class="hr"><hr /></div>
        </xsl:if>

        <xsl:if test="//M:EmploymentReferences">
            <div class="mod">
                <div class="hd"><h3>References</h3></div>
            <xsl:for-each select="//M:EmploymentReferences/M:Reference">
                <div class="bd">
                    <h4><xsl:value-of select=".//M:Name"/></h4>
                    <p><xsl:value-of select="./M:PositionTitle"/><br />
                        <xsl:value-of select=".//M:CompanyName"/><br />
                        p: <xsl:value-of select=".//M:Phone"/><br />
                        <xsl:if test=".//M:E-mail">e: <xsl:value-of select=".//M:E-mail"/></xsl:if>
                    </p> 
                </div><!-- .bd -->
            </xsl:for-each>
            </div><!-- .mod -->
        </xsl:if>

        </xsl:for-each>

                    </div><!-- .main -->
                    <div class="foot">
                        <p>Last updated:
                                <xsl:call-template name="datetime_to_date">
                                    <xsl:with-param name="datetime" select="//M:ResumeModDate"/>
                                </xsl:call-template>
                        </p>
                    </div>
                </div><!-- .body -->
            </div><!-- .page -->
        </body>
</html>
</xsl:template>

<xsl:template name="datetime_to_date">
    <xsl:param name="datetime"/>

    <xsl:choose>
        <xsl:when test="starts-with(translate(normalize-space($datetime), '0123456789', '9999999999'), '9')">
            <xsl:value-of select="substring(normalize-space($datetime),9,2)"/>/<xsl:value-of select="substring(normalize-space($datetime),6,2)"/>/<xsl:value-of select="substring(normalize-space($datetime),1,4)"/>
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="$datetime"/></xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="candidate_name">
    <xsl:choose>
        <xsl:when test="//M:Account/M:Confidential/text()='1'">
            <xsl:choose>
                <xsl:when test="//M:Resumes[0]/M:Resume/M:ResumeTitle">
                    <xsl:value-of select="//M:Resumes[0]/M:Resume/M:ResumeTitle"/>
                </xsl:when>
                <xsl:otherwise>
                    Anonymous Candidate
                </xsl:otherwise>
            </xsl:choose>
        </xsl:when>
        <xsl:otherwise><!-- start of public candidate -->
            <xsl:for-each select="//M:PersonalData/M:Contact[1]">
                <xsl:if test="./M:StructuredName[1]/M:NamePrefix[1]">
                    <xsl:value-of select="./M:StructuredName[1]/M:NamePrefix[1]"/>
                    <xsl:text> </xsl:text>
                </xsl:if>

                <xsl:value-of select="./M:StructuredName[1]/M:GivenName[1]"/>
                <xsl:text> </xsl:text>

                <xsl:if test="./M:StructuredName[1]/M:MiddleName[1]/text()">
                    <xsl:value-of select="./M:StructuredName[1]/M:MiddleName[1]"/>
                    <xsl:text> </xsl:text>
                </xsl:if>

                <xsl:value-of select="./M:StructuredName[1]/M:FamilyName[1]"/>
            </xsl:for-each>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="nbsp">
    <xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;]]></xsl:text>
</xsl:template>

</xsl:stylesheet>

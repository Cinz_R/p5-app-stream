// This is the brunch build config
// Based heavily on https://github.com/icholy/ember-brunch

var fs = require('fs')
var sysPath = require('path')

/*
  Common filter to filter out stuff we never want.
*/

function commonFilter(path){
    if ( !path.match(/^vendor/) ) {
            return false;
    }
    if ( path.match(/jquery\/src\/.+\.js$/) ){
        return false;
    }
    if ( path.match(/jquery\/test\/.+\.js$/) ){
        return false;
    }
    if ( path.match(/jquery.fileDownload\/src\/Scripts\/Support\/jquery.gritter/) ){
        return false
    }
    return true;
}


/*

    All css+scss files found in vendor + app/scss will be included in the main
    search css. Unless it is excluded
*/
var vendorCSS_Exclude = [
    /jquery-ui\/themes\/base/,
    /vendor\/parsleyjs\/tests/,
    /jquery\/test/,
    /jquery.fileDownload\/src\/Content\/jquery.gritter.css/,
    /jquery\-ui\//,
    /vendor\/perfect\-scrollbar\/src/,
    /hopscotch/,
    /select2/,
    /blockly/,
    /perfect\-scrollbar/,
    /tinymce/,
    /pdfjs\-gh\-pages/
];
// exceptions to the above exclusion rules
var vendorCSS_Include = [
    /jquery-ui\/themes\/base\/(datepicker|core|theme).css/,
    /hopscotch.min.css$/,
    /select2.css$/,
    /perfect\-scrollbar\.css$/,
    /spectrum\/spectrum.css$/,
];

var vendorJS_Include = [
    'vendor/jquery/dist/jquery.js',
    'handlebars.js',
    'ember.prod.js',
    'select2.js',
    'jquery.dropdown.js',
    'json2/json2.js',
    'jquery.fileDownload.js',
    'jquery.gritter.js',
    'jquery.pulse.js',
    'jquery-ui.js',
    'datepicker.js',
    'perfect-scrollbar.jquery.js',
    'i18n/datepicker-nl.js',
    'i18n/datepicker-fr.js',
    'i18n/datepicker-de.js',
    'i18n/datepicker-en-GB.js',
    'i18n/datepicker-en-AU.js',
    'jquery.sticky.js',
    'moment-with-langs.js',
    'moment-timezone-with-data.js',
    'numeral/numeral.js',
    'numeral/languages.js',
    'konami.js',
    '/hopscotch.js',
    'jquery.highlight.js',
    'jquery.fileupload.js',
    'i18next.js',
    'tinymce.js',
    'spectrum.js',
    'cb/openid/widget.js',
    'blockly/blockly_compressed.js',
    'blockly/blocks_compressed.js',
    'blockly/msg/js/en.js',
    'mark.js/dist/jquery.mark.js',
    'filesaver/FileSaver.js'
];

exports.config = {
  paths: {
    watched: ['app','vendor', 'vendor-changes']
  },
  overrides: {
      // brunch warns if it finds a file it doesn't use in vendor/ this override tells it to ignore that in a hacky way
      we_dont_use_all_files_in_vendor : {
          files: {
              javascripts: {
                  joinTo: {
                      'whatever' : /^vendor|node_modules/
                  }
              },
              stylesheets: {
                  joinTo: {
                      'whatever' : /^vendor|node_modules/
                  }
              }
          }
      }
  },
  plugins: {
    sass: {
        mode: 'native' // node-sass
    },
    autoReload: {
      enabled: false
    },
    jshint: {
      options: {
        es3: true
      }
    },
    cleancss: {
        keepSpecialComments: 0,
        removeEmpty: false
    }
  },

  files: {
    javascripts: {
        joinTo: {
        'static/javascripts/app.js': /^app/,
        'static/javascripts/html5shiv.js': function ( path ) {
            if ( !path.match(/vendor\/html5shiv/) ) return false;
            return path.endsWith( 'html5shiv.js' );
        },
        'static/javascripts/vendor.js': function (path) {
            return commonFilter(path) && vendorJS_Include.some(function (file) { return path.endsWith(file); });
        },
        'static/tinymce/plugins/paste/plugin.js' : 'vendor/tinymce/plugins/paste/plugin.js',
        'static/tinymce/plugins/searchreplace/plugin.js' : 'vendor/tinymce/plugins/searchreplace/plugin.js',
        'static/tinymce/plugins/spellchecker/plugin.js' : 'vendor/tinymce/plugins/spellchecker/plugin.js',
        'static/tinymce/plugins/nonbreaking/plugin.js' : 'vendor/tinymce/plugins/nonbreaking/plugin.js',
        'static/tinymce/plugins/code/plugin.js' : 'vendor/tinymce/plugins/code/plugin.js',
        'static/tinymce/plugins/table/plugin.js' : 'vendor/tinymce/plugins/table/plugin.js',
        'static/tinymce/plugins/link/plugin.js' : 'vendor/tinymce/plugins/link/plugin.js',
        'static/tinymce/plugins/hr/plugin.js' : 'vendor/tinymce/plugins/hr/plugin.js',
        'static/tinymce/plugins/textcolor/plugin.js' : 'vendor/tinymce/plugins/textcolor/plugin.js',
        'static/tinymce/plugins/image/plugin.js' : 'vendor/tinymce/plugins/image/plugin.js',

        'static/javascripts/testing.js': function (path) {
          if ( !path.match(/^vendor/) ) {
            return false;
          }

          return [
            'qunit-reporter-junit/qunit-reporter-junit.js',
            'qunit/qunit/qunit.js',
            'jquery-mockjax/jquery.mockjax.js',
            'ember/ember-testing.js'
          ].some(function(file) {
              return path.endsWith(file);
          });
        },

        'static/javascripts/jquery.js': function ( path ) {
            return path == 'vendor/jquery/dist/jquery.js';
        }

      },
      order: {
        before: [

          'vendor/qunit/qunit/qunit.js',

          // brunch 1.7 will add bower support. Then we can move vendor to components and this won't be needed
          'vendor/jquery/dist/jquery.js',
          'vendor/gritter/js/jquery.gritter.js',
          'vendor/jquery.pulse.js/jquery.pulse.js',
          'vendor/handlebars/handlebars.js',
          'vendor/jquery-ui/jquery-ui.js',
          'vendor/jquery-ui/ui/datepicker.js',
          'vendor/ember/ember.js',
          'vendor/ember/ember.prod.js',

          'vendor/moment/min/moment-with-langs.js',
          'vendor/moment/min/moment-with-langs.min.js',
          'vendor/numeral/numeral.js',
          'vendor/numeral/min/numeral.min.js',
          'vendor/tinymce/tinymce.js',

          'app/scripts/stream2/mixins/ajax.js',
          'app/scripts/stream2/mixins/jquery-ui.js',
          'app/scripts/stream2/mixins/errors.js',
          'app/scripts/stream2/mixins/analytics.js',
          'app/scripts/stream2.js',
          'app/scripts/stream2/components/autocomplete.js',
          'app/scripts/stream2/components/jquery-ui-datepicker.js',
          'app/scripts/stream2/components/facet.js',
          'app/scripts/stream2/model.js',
          'app/scripts/stream2/models/candidate.js',
          'app/scripts/stream2/models/mutableCandidate.js',
          'app/scripts/stream2/components/candidate-shortlist.js',
          'app/scripts/stream2/components/search-side-control.js',
          'app/scripts/stream2/components/select2.js',
          'app/scripts/stream2/components/default-profile-body.js'
        ],
      },
    },

    stylesheets: {
      joinTo: {
        'static/stylesheets/app.css': function (path) {
            if( path.match(/app\/scss\/simplify-v5.scss$/) ){
                return false;
            }
            if ( vendorCSS_Exclude.some( function ( e ) { return !! path.match( e ); } ) ){
                return vendorCSS_Include.some( function ( i ) { return !! path.match( i ); } );
            }
            return path.match(/^(app\/scss|vendor)/);
        },
        'static/stylesheets/app-v5.css': function (path) {
            if( path.match(/app\/scss\/simplify.scss$/) ){
                return false;
            }
            if ( vendorCSS_Exclude.some( function ( e ) { return !! path.match( e ); } ) ){
                return vendorCSS_Include.some( function ( i ) { return !! path.match( i ); } );
            }
            return !! path.match(/^(app\/scss|vendor)/);
        },
        'static/stylesheets/unsubscribe.css': function ( path ) {
            return path.match(/^app\/css\/unsubscribe/);
        },
        'static/stylesheets/information.css': function ( path ) {
            return path.match(/^app\/css\/information/);
        },
        // pdfjs viewer css.
       'static/pdfjs/web/viewer.css':[
          'vendor/pdfjs-gh-pages/web/viewer.css',
          'vendor-changes/pdfjs-gh-pages/web/viewer.css'
        ]
      }
    },

    templates: {
      precompile: true,
      root: 'app/templates/',
      joinTo: 'static/javascripts/templates.js',
    }
  },

  modules: {
    addSourceURLs: false,
    wrapper: false,
    definition: function (path, data) {
      return "console.log('"+path+" was built on " + new Date() + "');\n\n";
    }
  },

  conventions: {
    ignored: function (path) {
        // allow _ prefixed templates so partials work
      if ( path.indexOf( sysPath.join('app', 'templates') ) === 0 ) {
        return false;
      }

      // ignore all uncompiled css in vendor
      if ( path.match(/^vendor\/.*?\.scss$/) ){
        return true;
      }

      return sysPath.basename(path).startsWith('_');
    },
    assets: function (path) {
        return path.match(/jquery-ui\/themes\/smoothness/);
    },
  },

  server: {
    port: 3333,
    base: '/',
    run:  false
  }
}

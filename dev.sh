#!/bin/sh

echo "Generating version";
dev-bin/VERSION-GEN.sh

if [ ! $MOJO_CONFIG ]; then
    echo "Missing MOJO_CONFIG in the environment. Example:"
    echo "MOJO_CONFIG=app-stream2.conf $0"
    exit
fi

# Increase the number of files that brunch is allowed to open
# brunch opens up every file it watches so it needs a higher limit

## Need to be root for that.
# No warning please.
# ulimit -n 10000

echo "exit" | ./bin/redis

# Need to be root for that.
# No warning please.
# ulimit -n 10000

# Run webserver and compass at the same time
(
  echo "morbo bin/app.pl -l http://*:3000 -l https://*:3001"
  echo "./node_modules/.bin/brunch watch"
  echo "./bin/app.pl worker"
) | xargs -P 3 -IX bash -c "X"
# -P 3 = xargs in parallel mode, max 3 processes

QUnit.module("Stream v2 Ember Frontend - Facet Tests");
QUnit.test("FacetGroupedComponent", 6, function() {
    beginTest( {}, function () {
        var groupedFacet = Stream2.FacetGroupedComponent.create();
        var facet = Stream2.Facet.create();
        facet.set('Name', "Adverts");
        facet.set('Type', "advert");
        facet.set('isSorted', false);
        facet.set('SelectedValues', { 29334: 1 });
        facet.set('Options', {
            "default": {
                "SortOrder": 2,
                "Label": "Live",
                "Options": [
                    { "label": "Software Company", "value": 144881, "count": 3 },
                    { "label": "Penguin Engineer", "value": 144869, "count": 7 }
                ]
            },
            "archived": {
                "SortOrder": 3,
                "Label": "Archived",
                "Options": [
                    { "label": "Hardware Company", "value": 39498, "count": 5 },
                    { "label": "Panda Enthusiast", "value": 29334, "count": 3 }
                ]
            }
        });

        groupedFacet.set('facetType', 'advert');
        groupedFacet.set('facet', facet);

        var allOptionGroups = groupedFacet.get('getAllOptionGroups');
        equal(allOptionGroups.length, 2, 'read the option groups');
        equal(allOptionGroups[0].name, 'default', 'set the option groups name');
        equal(allOptionGroups[0].label, 'Live', 'set the option groups name');
        equal(allOptionGroups[0].count, 2, 'set the option groups count value based on length of Options');
        equal(allOptionGroups[0].options[0].get('label'), 'Penguin Engineer (7)', 'sorted options based on count descending');

        var optionGroups = groupedFacet.get('getOptionGroups');
        equal(optionGroups[1].options[0].get('label'), 'Panda Enthusiast (3)', 'selected options are always at the top');
    });
});
QUnit.test("getOptionGroups", 3, function() {
    beginTest( {}, function () {
        var groupedFacet = Stream2.FacetGroupedComponent.create();
        var facet = Stream2.Facet.create();
        facet.set('Name', "Adverts");
        facet.set('Type', "advert");
        facet.set('isSorted', false);
        facet.set('SelectedValues', { 144881: 1 });
        facet.set('Options', {
            "general_submissions": {
                "SortOrder": 1,
                "Label": "General Submissions",
                "Options": [
                    { "label": "Pony", "value": 143811, "count": 31 }
                ]
            },
            "default": {
                "SortOrder": 2,
                "Label": "Live",
                "Options": [
                    { "label": "Software Company", "value": 144881, "count": 3 },
                    { "label": "Penguin Engineer", "value": 144869, "count": 7 }
                ]
            },
            "archived": {
                "SortOrder": 3,
                "Label": "Archived",
                "Options": [
                    { "label": "Hardware Company", "value": 39498, "count": 5 },
                    { "label": "Panda Enthusiast", "value": 29334, "count": 3 }
                ]
            }
        });

        groupedFacet.set('facetType', 'advert');
        groupedFacet.set('facet', facet);

        var optionGroups = groupedFacet.get('getOptionGroups');
        equal(optionGroups[0].get('label'), 'General Submissions', 'general submissions is the first options');
        equal(optionGroups[0].get('showing'), true, 'general submissions group is always showing');
        equal(optionGroups[1].get('showing'), true, 'live advert group is also showing');
    });
});
QUnit.test("List facets", 1, function() {
    beginTest( {}, function () {
        var facet = Stream2.Facet.create();
        facet.set('Name', "My Lists");
        facet.set('Type', "list");
        facet.set('Options', [['Opt1', 'opt1'], ['Opt2', 'opt2']]);

        ok(facet.get('isList'), 'A small list is considered a list, whereas previously it was not O_o');
    });
});

QUnit.module("Stream v2 Ember Frontend - Editable Detail Text Component");
QUnit.test("Editable Detail Text Component", 4, function() {
    beginTest( {}, function () {
        var textbox = Stream2.EditableDetailTextComponent.create();
        textbox.set('storedValue', null);

        equal(textbox.get('value'), '', 'value should be empty string as storedValue is null');

        // simulate entering nothing when it was null
        textbox.set('value', '');
        textbox.focusOut();
        equal(textbox.get('storedValue'), null, 'stored value should not change');

        //add a proper value
        textbox.set('value','Ninja Pandas are Cool');
        textbox.focusOut();
        equal(textbox.get('storedValue'), 'Ninja Pandas are Cool', 'stored value should now be something err. sensible');

        // can we clear it?
        textbox.set('value','');
        textbox.focusOut();
        equal(textbox.get('storedValue'), '', 'stored value should now be empty');

    });
});


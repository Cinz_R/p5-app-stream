        QUnit.module("Stream v2 Ember Frontend - Loading",
            {
                beforeEach: function() {
                    Ember.run(Stream2, Stream2.advanceReadiness);
                }
            },
            function () {
                QUnit.test("Loading screen shows", 2, function () {
                    ok( $('#loading-screen:not(":hidden")').length, "loading screen is showing" );
                    wait().andThen( function () {
                        ok( $('#loading-screen:hidden').length, "loading screen is hidden" );
                    });
                });
            }
        );

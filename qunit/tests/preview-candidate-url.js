QUnit.module("Preview Candidate URL",
    function () {
        QUnit.test("Load correct candidate", 1, function(assert) {
            beginTest( {}, function () {
                visit('/search/CRITERIA_FROM_URL/careerbuilder/page/1?search_id=123456&candidate_idx=1');
                var done = assert.async();
                Stream2.one( 'resultsLoaded', function () {
                    wait().andThen( function () {
                        equal( $('div.profile-box:visible').find('div.contact-details').children('h2').first().text(), 'IM THE SECOND CANDIDATE' );
                        done();
                    });
                });
            });
        });
    }
);


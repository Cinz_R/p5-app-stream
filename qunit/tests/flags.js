        /* Test hooks */
        QUnit.module(
            "Stream v2 Ember Frontend - flagging",
            function ( hooks ) {

                $.mockjax({
                    url: '/api/admin/response-flags',
                    type: 'GET',
                    responseTime: 0,
                    contentType: 'text/json',
                    responseText: { data: { flags: [ { colour_hex_code: "#ff0000" , description : "Red", flag_id :1, has_rules :"true", setting_mnemonic :"responses-flagging-enable_red_flag", type:"standard"}, { flag_id: 22, colour_hex_code: '#ff0000', description: 'this is a flag', type: "custom" } ] } }
                });

                $.mockjax({
                    url: '/api/admin/response-flags',
                    type: 'POST',
                    responseTime: 0,
                    contentType: 'text/json',
                    responseText: { data: { flag_id: 21, description: "NEW FLAG" } }
                });
                QUnit.test("Add a new flag", 7, function(assert) {
                    beginTest(
                        {
                            user: { is_overseer: true, is_admin: true, settings: { "behaviour-search-use_ofccp_field": false } },
                        },
                        function () {
                            visit('/admin/flags').then( function () {

                                var standard = $('div.flag-table-container').first();
                                equal( standard.find('div.flag-line > div.flag-description').first().text().trim(), "Red" );

                                var container = $('div.flag-table-container').last();
                                ok( container.find('div.flag-line > div.flag-description').first().text().trim() == "this is a flag", "showing 1 custom flag" );
                                ok( container.find('div.flag-line > div > i').attr('style') == "color: #ff0000", "showing the correct colour flag" );
                                click($('div.new-flag-button > p')).then( function () {
                                    ok( $('div.admin-add-flag-form'), "add form is shown after clicking" );
                                    ok( $('div.admin-add-flag-form > div > button').prop('disabled'), "form is disabled when no description is given" );
                                    fillIn( $('div.admin-add-flag-form > div> input').last(), "NEW FLAG" ).then( function () {
                                        ok( ! $('div.admin-add-flag-form > div> button').prop('disabled'), "form becomes enabled after entering a description" );
                                        click( $('div.admin-add-flag-form > div> button') ).then( function () {
                                            ok( container.find('div.flag-line > div.flag-description').last().text().trim() == "NEW FLAG", "added a new flag" );
                                        });
                                    });
                                });
                            });
                        }
                    );
                });
                QUnit.test("Edit existing flag", 2, function ( assert ) {

                    $.mockjax({
                        url: '/api/admin/response-flags/21',
                        type: 'PUT',
                        responseTime: 0,
                        contentType: 'text/json',
                        responseText: { data: { flag_id: 21, description: "now I'm different" } }
                    });

                    var container = $('div.flag-table-container').last();
                    click(container.find('div.flag-line > div').last().find('p').first()).then( function () {
                        equal( container.find('input').last().val(), "NEW FLAG" );
                        fillIn( container.find('input').last(), "now I'm different" ).then( function () {
                            click( container.find('button').first() );
                            wait().andThen( function () {
                                equal( container.find('div.flag-description').last().text().trim(), "now I'm different" );
                            });
                        });
                    });
                });

                QUnit.test("Candidate flags", 16, function(assert) {
                    var old_RESULT_MOCKJAX = $.mockjax.handler(window._RESULT_MOCKJAX);
                    $.mockjax.clear( window._RESULT_MOCKJAX );
                    window._RESULT_MOCKJAX = $.mockjax({
                        url: '/search_json/results/page/1',
                        responseTime: 100,
                        contentType: 'text/json',
                        responseText: {
                            "facets": [],
                            "notices": [],
                            "results":[
                                { "can_downloadcv": 1, "has_profile": 1, "has_cv": 1, "destination": "adcresponses", "name": "John Smith Senior", "email":"john.smith.snr@hotmail.com", "broadbean_adcresponse_rank": 3, "current_position": { "description": "I am a sausage" } },
                                { "can_downloadcv": 1, "has_profile": 1, "has_cv": 1, "destination": "adcresponses", "name": "John Smith Senior", "email":"john.smith.snr@hotmail.com", "broadbean_adcresponse_rank": 21, "current_position": { "description": "I am a sausage" } },
                                { "destination": "adcresponses", "name": "John Smith Junior", "email":"john.smith.jnr@hotmail.com", "current_position": { "description": "I am a tofu" }, "broadbean_adcadvert_id": "45", "has_cv": 1, "can_downloadcv": 1 }
                           ]
                        }
                    });

                    var _SHORTLIST_MOCKJAX = $.mockjax({
                        url: '/api/cheesesandwiches/adc_shortlist',
                        responseTime: 100,
                        contentType: 'text/json',
                        responseText: {
                            "suggestions":[{"label":"test advert","list":"adc_shortlist","value":"45"}]
                        }
                    });

                    var _UPDATE_MOCKJAX = $.mockjax({
                        url: '/results/results/adcresponses/candidate//properties',
                        responseTime: 100,
                        contentType: 100,
                        responseText: {}
                    });

                    beginTest(
                        {
                            user: {
                                is_overseer: false,
                                search_mode: 'responses',
                                adcresponse_flags: [
                                    {
                                        "colour_hex_code":"#ff0000",
                                        "description":"Red",
                                        "flag_id":1,
                                        "setting_mnemonic":"responses-flagging-enable_red_flag",
                                        "type":"standard"
                                    },
                                    {
                                        "colour_hex_code":"#ffa500",
                                        "description":"Amber",
                                        "flag_id":3,
                                        "setting_mnemonic":"responses-flagging-enable_amber_flag",
                                        "type":"standard"
                                    },
                                    {
                                        "colour_hex_code":"#008000",
                                        "description":"Green",
                                        "flag_id":5,
                                        "setting_mnemonic":"responses-flagging-enable_green_flag",
                                        "type":"standard"
                                    },
                                    {
                                        "colour_hex_code":"#ffffff",
                                        "description":"Custom 1",
                                        "flag_id": 20,
                                        "setting_mnemonic":"adcresponses-custom-flag-20",
                                        "type":"custom"
                                    },
                                    {
                                        "colour_hex_code":"#000000",
                                        "description":"Custom 2",
                                        "flag_id": 21,
                                        "setting_mnemonic":"adcresponses-custom-flag-21",
                                        "type":"custom"
                                    }
                                ],
                                settings: {
                                    "behaviour-search-use_ofccp_field": false,
                                    "responses-flagging-enable_amber_flag": 1,
                                    "responses-flagging-enable_green_flag": 1,
                                    "responses-flagging-enable_red_flag": 0,

                                    groupsettings: {
                                        "adcresponses-custom-flag-20" : 1,
                                        "adcresponses-custom-flag-21" : 0
                                    }
                                }
                            }
                        },
                        function () {
                            visit('/search');

                            wait().andThen( function() {

                                equal( $('div.result:nth-child(2)').last().find('div.ranking-flags').children().length, 4, "Second result has 4 flags" );
                                equal( $('div.result:nth-child(1)').last().find('div.ranking-flags').children().length, 3, "First result has 3 flags" );
                                equal( $('div.result:nth-child(2)').last().find('div.ranking-flags').children().last().attr('title'), 'Custom 2', 'second result has a disabled flag' );
                                equal( $('div.result:nth-child(1)').last().find('div.ranking-flags').children().last().attr('title'), 'Custom 1', 'first result has no disabled flags' );

                                equal( $('div.result').first().find('div.ranking-flags').children().length, 3, "We show 3 out of the 5 flags" );
                                equal( $('div.result').first().find('div.ranking-flags > i:nth-child(1)').attr('title'), 'Amber' );
                                equal( $('div.result').first().find('div.ranking-flags > i:nth-child(2)').attr('title'), 'Green' );
                                equal( $('div.result').first().find('div.ranking-flags > i:nth-child(3)').attr('title'), 'Custom 1' );

                                equal( $('div.result').first().find('div.ranking-flags').children().first().attr('style'), 'color: #ffa500', 'candidate is amber flagged, amber flag is highlighted' );
                                equal( $('div.result').first().find('div.ranking-flags').find(':nth-child(2)').attr('style'), 'color: grey', 'candidate is amber flagged, red flag is greyed out' );
                                equal( $('div.result').first().find('div.ranking-flags').children().last().attr('style'), 'color: grey', 'candidate is amber flagged, green flag is greyed out' );

                                equal( $('div.result').last().find('div.ranking-flags').children().first().attr('style'), 'color: #ffa500', 'candidate is not flagged, red flag is coloured' );
                                equal( $('div.result').last().find('div.ranking-flags').find(':nth-child(2)').attr('style'), 'color: #008000', 'candidate is not flagged, amber flag is coloured' );
                                equal( $('div.result').last().find('div.ranking-flags').children().last().attr('style'), 'color: #ffffff', 'candidate is not flagged, green flag is coloured' );

                                click($('div.result').last().find('i.icon-shortlist0').first()).then( function () {
                                    click($('div.result').last().find('.select2-choice').last());
                                    wait().andThen( function () {
                                        click($('.select2-result-label'));
                                        click($('div.result').last().find('.btn-primary'));
                                        $.mockjax.clear( _SHORTLIST_MOCKJAX );
                                        var done = assert.async();
                                        setTimeout( function () {
                                            equal( $('div.result').last().find('div.ranking-flags').children().first().attr('style'), 'color: grey', 'flag has been updated to suitable' );
                                            equal( $('div.result').last().find('div.ranking-flags').find(':nth-child(2)').attr('style'), 'color: #008000', 'shortlisting candidate against same colour results in green flag' );
                                            $.mockjax.clear( _UPDATE_MOCKJAX );
                                            $.mockjax.clear( window._RESULT_MOCKJAX );
                                            window._RESULT_MOCKJAX = $.mockjax( old_RESULT_MOCKJAX );
                                            done();
                                        }, 500 );
                                    });
                                });
                            });
                        }
                    );
                });

                QUnit.test("Candidate flags dropdown", 4, function(assert) {
                    var old_RESULT_MOCKJAX = $.mockjax.handler(window._RESULT_MOCKJAX);
                    $.mockjax.clear( window._RESULT_MOCKJAX );
                    window._RESULT_MOCKJAX = $.mockjax({
                        url: '/search_json/results/page/1',
                        responseTime: 100,
                        contentType: 'text/json',
                        responseText: {
                            "facets": [],
                            "notices": [],
                            "results":[
                                { "can_downloadcv": 1, "has_profile": 1, "has_cv": 1, "destination": "adcresponses", "name": "John Smith Senior", "email":"john.smith.snr@hotmail.com", "broadbean_adcresponse_rank": 3, "current_position": { "description": "I am a sausage" } },
                                { "destination": "adcresponses", "name": "John Smith Junior", "email":"john.smith.jnr@hotmail.com", "current_position": { "description": "I am a tofu" }, "broadbean_adcadvert_id": "45", "has_cv": 1, "can_downloadcv": 1 }
                           ]
                        }
                    });

                    beginTest(
                        {
                            user: {
                                is_overseer: false,
                                search_mode: 'responses',
                                adcresponse_flags: [
                                    {
                                        "colour_hex_code":"#ff0000",
                                        "description":"Red",
                                        "flag_id":1,
                                        "setting_mnemonic":"responses-flagging-enable_red_flag",
                                        "type":"standard"
                                    },
                                    {
                                        "colour_hex_code":"#ffa500",
                                        "description":"Amber",
                                        "flag_id":3,
                                        "setting_mnemonic":"responses-flagging-enable_amber_flag",
                                        "type":"standard"
                                    },
                                    {
                                        "colour_hex_code":"#008000",
                                        "description":"Green",
                                        "flag_id":5,
                                        "setting_mnemonic":"responses-flagging-enable_green_flag",
                                        "type":"standard"
                                    },
                                    {
                                        "colour_hex_code":"#ffffff",
                                        "description":"Custom 1",
                                        "flag_id": 20,
                                        "setting_mnemonic":"adcresponses-custom-flag-20",
                                        "type":"custom"
                                    },
                                    {
                                        "colour_hex_code":"#000000",
                                        "description":"Custom 2",
                                        "flag_id": 21,
                                        "setting_mnemonic":"adcresponses-custom-flag-21",
                                        "type":"custom"
                                    },
                                    {
                                        "colour_hex_code":"#00ff00",
                                        "description":"Custom 3",
                                        "flag_id": 22,
                                        "setting_mnemonic":"adcresponses-custom-flag-22",
                                        "type":"custom"
                                    },
                                    {
                                        "colour_hex_code":"#00ffff",
                                        "description":"Custom 4",
                                        "flag_id": 23,
                                        "setting_mnemonic":"adcresponses-custom-flag-23",
                                        "type":"custom"
                                    }
                                ],
                                settings: {
                                    "behaviour-search-use_ofccp_field": false,
                                    "responses-flagging-enable_amber_flag": 1,
                                    "responses-flagging-enable_green_flag": 1,
                                    "responses-flagging-enable_red_flag": 0,

                                    groupsettings: {
                                        "adcresponses-custom-flag-20" : 1,
                                        "adcresponses-custom-flag-21" : 1,
                                        "adcresponses-custom-flag-22" : 1,
                                        "adcresponses-custom-flag-23" : 1
                                    }
                                }
                            }
                        },
                        function () {
                            visit('/search');

                            var done = assert.async();
                            var _UPDATE_MOCKJAX = $.mockjax({
                                url: '/results/results/adcresponses/candidate//properties',
                                responseTime: 100,
                                contentType: 100,
                                response: function ( settings ) {
                                    equal( JSON.parse(settings.data.q).broadbean_adcresponse_rank, 20, 'candidate updated with first custom flag value' );
                                    this.responseText = "{}";
                                    $.mockjax.clear( _UPDATE_MOCKJAX );
                                    done();
                                }
                            });

                            wait().andThen( function() {
                                ok( $('.ranking-flags').first().children('div.select2-container').length, "if there are more than 5 flags we show a dropdown" );
                                equal( $('.ranking-flags').first().find('.select2-chosen').text().trim(), "Amber", "Preselection of dropdown works" );
                                click($('.ranking-flags').first().find('.select2-choice'));
                                wait().andThen( function () {
                                    equal( $('ul.select2-results > li.select2-result-selectable').length, 6, "There are 6 flags to choose from the select2 list" );
                                    click( $($('ul.select2-results > li.select2-result-selectable')[2]) );
                                    $.mockjax.clear( window._RESULT_MOCKJAX );
                                    window._RESULT_MOCKJAX = $.mockjax( old_RESULT_MOCKJAX );

                                });
                            });
                        }
                    );
                });

            }
        );

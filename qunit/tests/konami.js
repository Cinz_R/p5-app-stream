        /* Test hooks */
        QUnit.module(
            "Stream v2 Ember Frontend - konami",
            function ( hooks ) {
                QUnit.test("Type konami code", 1, function(assert) {
                    beginTest(
                        {
                            user: { is_overseer: true, is_admin: false, settings: { "behaviour-search-use_ofccp_field": false } },
                        },
                        function () {
                            visit('/search').then( function () {

                                // Easter egg only appears for users belonging
                                // to company cinzwonderland
                                var origGroup = stream2_current_user_group_id;
                                stream2_current_user_group_id = 'cinzwonderland';
                                var done = assert.async();
                                window.easter_egg.code();
                                wait().andThen(function(){
                                    assert.ok( $('div.jerome-face:visible').length , 'Jerome is visible' );
                                    // add that at the end of each test (from the done function).
                                    done();
                                });
                                stream2_current_user_group_id = origGroup;
                            });
                        }
                    );
                });
            });

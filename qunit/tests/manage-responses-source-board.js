/* Test hooks */
QUnit.module(
    "Stream v2 Ember Frontend - Manage Responses: Application Sources",
    function ( hooks ) {
        QUnit.test("Sources of applications are present", 4, function(assert) {

            $.mockjax.clear( window._ACTIVE_BOARDS_MOCKJAX_INDEX );
            var newMockjaxIndex = $.mockjax({
                url: '/api/boards/active.json',
                responseTime: 100,
                contentType:  'text/json',
                responseText: {
                    "boards":[
                        {
                            "nice_name": "Disabled Board",
                            "name":      "disabled_board",
                            "type":      "external",
                            "disabled":  1,
                            "user_quotas": {}
                        },
                        {
                            "nice_name": "CareerBuilder",
                            "name":      "careerbuilder",
                            "type":      "external",
                            "user_quotas": {}
                        },
                        {
                            "nice_name": "Responses",
                            "name":      "adcresponses",
                            "type":      "internal",
                            "user_quotas": {}
                        }
                    ]
                }
            });

            var old_RESULT_MOCKJAX = $.mockjax.handler(window._RESULT_MOCKJAX);
            $.mockjax.clear( window._RESULT_MOCKJAX );
            window._RESULT_MOCKJAX = $.mockjax({
                url: '/search_json/results/page/1',
                responseTime: 100,
                contentType: 'text/json',
                responseText: {
                    "facets":[
                        {
                            "Id":"adcresponses_broadbean_adcresponse_is_read",
                            "Label":"Viewed",
                            "Name":"broadbean_adcresponse_is_read",
                            "Options":[
                                [ "Read", "true", 71 ],
                                [ "Unread", "false", 7 ]
                            ],
                            "SelectedValues":{},
                            "SortMetric":80,
                            "Type":"multilist"
                        },
                        {
                            "Id":"adcresponses_broadbean_adcadvert_id",
                            "Label":"Advert",
                            "Name":"broadbean_adcadvert_id",
                            "Options": {
                                "live": {
                                    "Label": "Live", "Options": [
                                        {
                                            "label": "Software Company",
                                            "value": "144881",
                                            "count": 9
                                        },
                                        {
                                            "label": "Penguin Tap Dance Teacher",
                                            "value": "144869",
                                            "count": 7
                                        }
                                    ]
                                }
                            },
                            "SelectedValues":{},
                            "SortMetric":20,
                            "Type":"advert"
                      }
                   ],
                   "notices":[],
                   "results":[
                        {
                            "destination": "adcresponses",
                            "broadbean_adcadvert_title": "Monkey Hand Painter",
                            "broadbean_adcboard_nicename": "Primate Careers",
                            "broadbean_adcresponse_source_id": 3,
                            "can_downloadcv": 1, "has_profile": 1,
                            "has_cv": 1,
                            "name": "John Smith Senior",
                            "email":"john.smith.snr@hotmail.com",
                            "broadbean_adcresponse_rank": 3,
                            "current_position": {
                                "description": "I am a sausage"
                            }
                        },
                        {
                            "destination": "adcresponses",
                            "broadbean_adcadvert_title": "Invisible Toilet Attendant",
                            "broadbean_adcboard_nicename": "Special People",
                            "broadbean_adcresponse_source_id": 1,
                            "name": "John Smith Junior",
                            "email":"john.smith.jnr@hotmail.com",
                            "broadbean_adcresponse_rank": 5,
                            "current_position": {
                                "description": "I am a tofu"
                            }
                        },
                   ]
                }
            });

            beginTest(
                {
                    user: {
                        is_overseer: false,
                        is_admin:    false,
                        //search_mode: "responses",
                        settings: {
                            "behaviour-search-use_ofccp_field": false
                        }
                    },
                },
                function () {
                    visit('/search').then( function () {
                        wait().andThen( function() {

                            // submit an empty search to get some responses
                            jQuery("[data-id=adcresponses]").click();
                        }).andThen( function() {
                            var $sources = jQuery("span.application-source");
                            assert.ok(
                                $sources.size() > 0,
                                "One or more elements of class application-source"
                            );
                            assert.ok(
                                $('div.facets#adcresponses_broadbean_adcadvert_id').find('div.facetExpand:visible').length,
                                "AdCresponses advert facet is open by default"
                            );
                            assert.ok(
                                $sources.first().text().indexOf("Search") == 0,
                                "'Search' prefix when application came from Search"
                            );
                            assert.ok(
                                /Special People/.test( $sources.eq(1).text() ),
                                "Expected job board name detected"
                            );

                            $.mockjax.clear( window._RESULT_MOCKJAX );
                            window._RESULT_MOCKJAX = $.mockjax( old_RESULT_MOCKJAX );
                            $.mockjax.clear( newMockjaxIndex );
                            window._ACTIVE_BOARDS_MOCKJAX_INDEX = $.mockjax( window._ACTIVE_BOARDS_MOCKJAX_DATA );
                        });
                    });
                }
            );
        });
    }
);

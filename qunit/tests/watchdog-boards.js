QUnit.module(
    "Stream v2 Ember Frontend - Watchdog Available Boards",
    function( hooks ) {
        QUnit.test("Watchdog Boards", 2, function(assert) {

            // override mockjax response for /api/boards/active.json
            $.mockjax.clear( window._ACTIVE_BOARDS_MOCKJAX_INDEX );
            var newMockjaxIndex = $.mockjax( {
                url: '/api/boards/active.json',
                responseTime: 100,
                contentType:  'text/json',
                responseText: {
                    "boards":[
                        {
                            "nice_name": "Disabled Board",
                            "name":      "disabled_board",
                            "type":      "external",
                            "disabled":  1
                        },
                        {
                            "nice_name": "CareerBuilder",
                            "name":      "careerbuilder",
                            "type":      "external"
                        },
                        {
                            "nice_name": "Responses",
                            "name":      "adcresponses",
                            "type":      "internal"
                        }
                    ]
                }
            } );

            var checkBoards = function(url, assertionMsg) {
                visit(url).then( function() {
                    var $boards = jQuery("ul.board-selection li");
                    var responsesBoardPresent = false;
                    $boards.each( function(index) {
                        if ( jQuery(this).text().indexOf("Responses") >= 0 ) {
                            responsesBoardPresent = true;
                            return false;   // quit processing more
                        };
                        return true;    // process next board
                    });
                    assert.ok( !responsesBoardPresent, assertionMsg );
                });
            };

            beginTest(
                {
                    user: {
                        is_admin: false,
                        is_overseer: false,
                        search_mode: 'search'

                        // is_admin: false,
                    }
                },
                function() {
                    checkBoards('/watchdogs/create/CRITERIA_FROM_URL',
                        "Create Watchdog: Responses board is absent");
                    checkBoards('/watchdogs/edit/7',
                        "Edit Watchdog: Responses board is absent");
                    // restore previous active boards mockjax response
                    $.mockjax.clear( newMockjaxIndex );
                    window._ACTIVE_BOARDS_MOCKJAX_INDEX = $.mockjax( window._ACTIVE_BOARDS_MOCKJAX_DATA );
                }
            );

        } );
    }
);

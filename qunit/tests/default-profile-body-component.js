QUnit.module("Stream v2 Ember Frontend - Unit Tests",
    function () {
        QUnit.test("DefaultProfileBodyComponent", 13, function( assert ) {
            var component = Stream2.DefaultProfileBodyComponent.create();
            var element = $('<div />');
            component['$'] = function () { return element; };

            element.html("bacon");
            component.set('searchTerms', ['ac']);
            component.didRender();
            equal( element.find( 'mark' ).text(), 'ac', 'very simple highlight works' );
            
            element.html("bacon");
            component.set('searchTerms', ['ac*']);
            component.didRender();
            equal( element.find( 'mark' ).text(), 'acon', 'very simple wildcard highlight works' );

            element.html("this bacon is very old");
            component.set('searchTerms', ['*ac*']);
            component.didRender();
            equal( element.find( 'mark' ).text(), 'bacon', 'Double wildcard highlight in a sentence works' );

            element.html("this bacon is very old");
            component.set('searchTerms', ['hi', '*ac*', 'er*', '*l']);
            component.didRender();

            var marks = element.find( 'mark' );
            equal( marks.length, 4, 'have highlighted 4 words' );
            equal( marks.get(0).innerHTML, 'hi' );
            equal( marks.get(1).innerHTML, 'bacon' );
            equal( marks.get(2).innerHTML, 'ery' );
            equal( marks.get(3).innerHTML, 'ol' );

            element.html( "Maître Renard, par l'odeur alléché" );
            component.set('searchTerms', ['maitr*', '*odeur', 'all*']);
            component.didRender();

            marks = element.find( 'mark' );
            equal( marks.length, 3, 'have highlighted 3 words' );
            equal( marks.get(0).innerHTML, 'Maître' );
            equal( marks.get(1).innerHTML, "l'odeur" );
            equal( marks.get(2).innerHTML, 'alléché' );

            element.html('C++');
            component.set('searchTerms', ['C++']);
            component.didRender();
            equal( element.find( 'mark' ).text(), 'C++', 'Works with regex in keywords');
        });
    }
);

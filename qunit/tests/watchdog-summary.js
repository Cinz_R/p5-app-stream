QUnit.module(
    "Stream v2 Ember Frontend - Watchdog Summary",
    function( hooks ) {
        QUnit.test("Watchdog Summary", 1, function(assert) {

            beginTest(
                {
                    user: {
                        is_admin: false,
                        is_overseer: false,
                        search_mode: 'search'
                    }
                },
                function() {
                    visit('/watchdogs/summary').then(function() {
                        var originalConfirm = window.confirm;
                        var deleteBtn = $('a[title="Delete this watchdog"]')[0];
                        window.confirm = function() {
                            assert.ok(true, 'Called confirm when deleting watchdog in summary page');
                        };
                        deleteBtn.click();
                        wait().andThen(function() {});
                        window.confirm = originalConfirm;
                    });
                }
            );

        } );
    }
);

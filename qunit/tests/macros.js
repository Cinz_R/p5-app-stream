/* Test hooks */
QUnit.module(
    "Stream v2 Ember Frontend - macros",
    function ( hooks ) {
        
        $.mockjax({
            url: '/api/admin/group_macros',
            type: 'GET',
            responseTime: 0,
            contentType: 'text/json',
            responseText: { data: { macros: [] } }
        });

        // $.mockjax({
        //     url: '/api/admin/response-flags',
        //     type: 'POST',
        //     responseTime: 0,
        //     contentType: 'text/json',
        //     responseText: { data: { flag_id: 21, description: "NEW FLAG" } }
        // });
        QUnit.test("Macro crud", 3, function(assert) {
            beginTest(
                {
                    user: { is_overseer: true, is_admin: true, settings: { "behaviour-search-use_ofccp_field": false } },
                },
                function () {
                    visit('/admin/macros').then( function () {
                        ok( $('i.icon-plus-1').size() , "Ok can find the plus button");
                        click( $('i.icon-plus-1').first() ).then(function(){
                            ok( $('button.btn-primary').prop('disabled') , "Ok button is disabled");
                            fillIn( $('input.ember-text-field').first() , "Some name" ).then(function(){
                                ok( $('button.btn-primary').prop('disabled') , "Ok button is still disabled - Missing program");
                            });
                        });
                    });
                })
        });
    }
);

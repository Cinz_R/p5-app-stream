QUnit.module("Candidates Model",
    function () {
        QUnit.test("Enhancements", 7, function(assert) {
            beginTest( {}, function () {
                $.mockjax({
                    url: '/results/500/reed/candidate/10/enhancements',
                    data: {section: 'result'},
                    type: 'GET',
                    responseTime: 100,
                    contentType: 'text/json',
                    responseText: { status_id: 23423 },
                    status: 200
                });

                $.mockjax({
                    url: '/api/asyncjob/*/status',
                    responseTime: 200,
                    status: 200,
                    contentType: 'text/json',
                    responseText: {"status":"completed", "context": { "result": { "enhancements": { nfl_season: true } } } }
                });

                var candidate = Stream2.Candidate.create();
                candidate.set('results_id', 500);
                candidate.set('destination', 'reed');
                candidate.set('idx', 10);
                candidate.set('candidate_id', 'wonton');

                equal(candidate.get('result_enhancement_template'), 'board/result/enhancements/reed', 'result enhancement template path is good');

                // we're calling two routes, expect 2 async calls
                var done = assert.async(2);

                setTimeout(function() {
                    candidate.enhance('result');
                    assert.equal(candidate.get('enhanceCandidateLoading'), true, 'sets a candidate loading boolean');
                    done();
                });

                // this is the job request, make it one wait for the first request to complete
                setTimeout(function(){
                    assert.equal(candidate.get('nfl_season'), true, 'set additional attribute on the candidate');
                    assert.equal(candidate.get('has_loaded_result_enhancement'), true, 'set a property to show the result enhancements');
                    assert.equal(candidate.get('has_done_enhancement'), true, 'set a done property for enhancements');
                    assert.equal(candidate.get('enhancementToggleId'), 'wonton-enhancements-toggle', 'set a enhancements toggle ID for the candidate');
                    assert.equal(candidate.get('showing_enhancements'), true, 'set a property to show/hide the enhancement properties in snippet');
                    done();
                }, 500);
            });
        });
    }
);

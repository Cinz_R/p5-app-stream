QUnit.module(
    "Stream v2 Ember Frontend - Manage Responses: Zombie Restrictions",
    function( hooks )  {
        QUnit.test(
            "Zombies cannot see all links",
            1,
            function(assert) {
                beginTest(
                    {
                        user: {
                            is_admin: false,
                            search_mode: "responses",
                            contact_details: {
                                office: "zombie"
                            }
                        }
                    },
                    function() {
                        visit("/search").then( function() {
                            assert.ok(
                                jQuery("#search-side-control")
                                    .find("i.icon-search").size() == 0,
                                "Search External Boards link is hidden"
                            );
                        });
                    }
                );
            }
        );
    }
);
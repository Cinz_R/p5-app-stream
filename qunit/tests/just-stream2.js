        QUnit.module("Stream v2 Ember Frontend - Loading",
            {
                beforeEach: function() {
                    Ember.run(Stream2, Stream2.advanceReadiness);
                }
            },
            function () {
                QUnit.test("Testing Stream2 global object", 2, function () {
                    ok( Stream2.get('cbOneIamAuth') , "Ok got cbAuth cbOneIamAuth" );
                    ok( Stream2.get('cbOneIamAuth') === Stream2.get('cbOneIamAuth') , "Ok this is lazy");
                });
            }
        );

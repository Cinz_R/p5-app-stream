/* Prove that frontend logic at URI /admin/reports behaves as expected */

QUnit.module(
    "Stream v2 Ember Frontend - Search Reports",
    function( hooks ) {

        QUnit.test("Report tables behave properly", 3, function( assert ) {
            beginTest(
                {
                    user: {
                        is_admin:    true,
                        is_overseer: true,
                        settings:    {}
                    }
                },

                function() {
                    visit('/admin/reports');

                    wait().andThen( function() {
                        $("button").click();

                        wait().andThen( function() {

                            // assert that the column titles match expectations
                            var expectedColumnTitles = [
                                'Company', 'Database', 'Searches', 'Viewed',
                                'Downloaded', 'Saved', 'Shortlisted', 'Longlisted',
                                '(Tearsheeted)', 'Forwarded', 'Messaged', 'Tagged',
                                'Edited'
                            ];
                            var allTitlesMatch = true;    // be optimistic
                            $("table.data-table tr:first th").each( function(idx, elem) {
                                if ( $(elem).text() != expectedColumnTitles[idx] ) {
                                    return allTitlesMatch = false;
                                }
                            } );
                            ok(allTitlesMatch, "all column titles match");

                            // assert that only one row exists for each board
                            var boardNames = {};
                            var boardNamesAreUnique = true;
                            $("table.data-table tbody tr").each( function(idx, elem) {
                                var boardName = $(elem).find("td").eq(1).text().trim();
                                if ( boardNames.hasOwnProperty(boardName) ) {
                                    return boardNamesAreUnique = false;
                                }
                                else {
                                    boardNames[boardName] = true;
                                }
                            } );
                            ok(boardNamesAreUnique, "No duplicate board names detected");

                            // assert that a totals row exists in the table
                            ok( boardNames['Totals:'], "Totals row exists" );

                            // TODO:
                            // assert that clicking an action count link in a
                            // cell produces at least one section.dated-actions
                            // element
                            $("table.data-table td a").trigger("click");
                            // TODO: more mockjaxery needed here
                        });
                    });
                }
            );
        } );
    }
);
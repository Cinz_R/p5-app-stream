        QUnit.module("Stream v2 Ember Frontend - Errors",
            function () {

                QUnit.test("Show an unmanaged error", 1, function(assert) {
                    beginTest(
                        {
                            user: { is_overseer: false, settings: { "behaviour-search-use_ofccp_field": false } }
                        },
                        function () {
                            var old_STATUS_MOCKJAX = $.mockjax.handler(window._STATUS_MOCKJAX);
                            $.mockjax.clear(window._STATUS_MOCKJAX);
                            var tmpMock = $.mockjax({
                              url: '/api/asyncjob/123456/status',
                              responseTime: 100,
                              contentType: 'text/json',
                              responseText: {"status":"failed","context": { "is_unmanaged": true, "error": { "message": "Something unmanaged has happened" } } }
                            });
                            visit('/search');
                            click('.search-btn-grp > button');
                            wait().andThen( function () {
                                ok ( $('div.right-pane').find('div.alert').text().match('There has been an unmanaged error') );
                                $.mockjax.clear(tmpMock);
                                window._STATUS_MOCKJAX = $.mockjax( old_STATUS_MOCKJAX );
                            });
                        }
                    );
                });

                QUnit.test("Show a managed error", 1, function(assert) {
                    var old_STATUS_MOCKJAX = $.mockjax.handler(window._STATUS_MOCKJAX);
                    $.mockjax.clear(window._STATUS_MOCKJAX);
                    var tmpMock = $.mockjax({
                      url: '/api/asyncjob/123456/status',
                      responseTime: 100,
                      contentType: 'text/json',
                      responseText: {"status":"failed","context": { "error": { "message": "Something managed has happened" } } }
                    });
                    visit('/search');
                    click('.search-btn-grp > button');
                    wait().andThen( function () {
                        ok ( $('div.right-pane').find('div.alert').text().match('Something managed has happened') );
                        $.mockjax.clear(tmpMock);
                        window._STATUS_MOCKJAX = $.mockjax( old_STATUS_MOCKJAX );
                    });
                });

                QUnit.test("Show a keyword error, no limit", 2, function(assert) {
                    var old_STATUS_MOCKJAX = $.mockjax.handler(window._STATUS_MOCKJAX);
                    $.mockjax.clear(window._STATUS_MOCKJAX);
                    var tmpMock = $.mockjax({
                      url: '/api/asyncjob/123456/status',
                      responseTime: 100,
                      contentType: 'text/json',
                      responseText: {"status":"failed","context": { "error": { "message": "Keywords too long: bacon sausage banana", "properties": { "type": "keywordstoolong" } } } }
                    });
                    visit('/search');
                    click('.search-btn-grp > button');
                    wait().andThen( function () {
                        ok ( $('div.right-pane').find('div.alert').text().match('this board wants you to be more succinct') );
                        ok ( ! $('div.right-pane').find('div.alert').text().match('supernumerous') );
                        $.mockjax.clear(tmpMock);
                        window._STATUS_MOCKJAX = $.mockjax( old_STATUS_MOCKJAX );
                    });
                });

                QUnit.test("Show a keyword error, with limit", 2, function(assert) {
                    var old_STATUS_MOCKJAX = $.mockjax.handler(window._STATUS_MOCKJAX);
                    $.mockjax.clear(window._STATUS_MOCKJAX);
                    var tmpMock = $.mockjax({
                      url: '/api/asyncjob/123456/status',
                      responseTime: 100,
                      contentType: 'text/json',
                      responseText: {"status":"failed","context": { "error": { "message": "Keywords too long: bacon sausage 200 banana hello", "properties": { "type": "keywordstoolong", "limit": 10 } } } }
                    });
                    visit('/search');
                    click('.search-btn-grp > button');
                    wait().andThen( function () {
                        ok ( $('div.right-pane').find('div.alert').text().match('this board wants you to be more succinct') );
                        ok ( $('div.right-pane').find('div.alert').text().match('supernumerous') );
                        $.mockjax.clear(tmpMock);
                        window._STATUS_MOCKJAX = $.mockjax( old_STATUS_MOCKJAX );
                    });
                });
            }
        );

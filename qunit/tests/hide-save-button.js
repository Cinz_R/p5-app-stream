QUnit.module("Stream v2 Ember Frontend - Hide Save Button",
    function () {
        /*
        QUnit.test("Normal settings", 2, function( assert ) {
                var searchController = { selectedBoard: { type: 'external' } };
                var controller = Stream2.BoardResultsController.create({
                    searchUser: { feature_exclusions: {}, settings: { 'candidate-actions-import-enabled': true } },
                    controllers: { search: searchController }
                });

                // normal settings, external board 
                assert.equal( controller.get('showSaveButton'), true, 'this is the default setting for users' );
                
                // normal settings, internal board 
                controller.set('controllers.search.selectedBoard.type','external');
                assert.equal( controller.get('showSaveButton'), true, 'this is the default setting for users for internal boards' );
        });
        QUnit.test('Setting turned off for the user', 2, function ( assert ){
                var searchController = { selectedBoard: { type: 'external' } };
                var controller = Stream2.BoardResultsController.create({
                    searchUser: { feature_exclusions: {}, settings: { 'candidate-actions-import-enabled': false } },
                    controllers: { search: searchController }
                });

                // feature turned off in the users search settings, external board 
                assert.equal( controller.get('showSaveButton'), false, 'The user has it turned off in their settings, external board' );
                
                // feature turned off in the users search settings, internal board 
                controller.set('controllers.search.selectedBoard.type','internal');
                assert.equal( controller.get('showSaveButton'), false, 'The user has it turned off in their settings, internal board' );
        });
        QUnit.test('hide_save sent in the ripple request', 2, function ( assert ){
                var searchController = { selectedBoard: { type: 'external' } };
                var controller = Stream2.BoardResultsController.create({
                    searchUser: { feature_exclusions: { hide_save: true }, settings: { 'candidate-actions-import-enabled': true } },
                    controllers: { search: searchController }
                });

                // feature turned off in the users search settings, external board 
                assert.equal( controller.get('showSaveButton'), false, 'hide_save has been sent in the ripple request, external board' );
                
                // feature turned off in the users search settings, internal board 
                controller.set('controllers.search.selectedBoard.type','internal');
                assert.equal( controller.get('showSaveButton'), false, 'hide_save has been sent in the ripple request, internal board' );
        });
        QUnit.test('hide_save_internal sent in the ripple request', 2, function ( assert ){
                var searchController = { selectedBoard: { type: 'external' } };
                var controller = Stream2.BoardResultsController.create({
                    searchUser: { feature_exclusions: { hide_save_internal: true }, settings: { 'candidate-actions-import-enabled': true } },
                    controllers: { search: searchController }
                });

                // feature turned off in the users search settings, external board 
                assert.equal( controller.get('showSaveButton'), true, 'hide_save_internal has been sent in the ripple request, external board' );
                
                // feature turned off in the users search settings, internal board 
                controller.set('controllers.search.selectedBoard.type','internal');
                assert.equal( controller.get('showSaveButton'), false, 'hide_save_internal has been sent in the ripple request, internal board' );
        });
        */
    }
);

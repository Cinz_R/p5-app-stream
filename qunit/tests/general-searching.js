/* Prove that clicking a board name submits a new search if the criteria -
 * e.g. keywords - has changed.
 */

QUnit.module(
    "Stream v2 Ember Frontend - General Searching",
    function( hooks ) {

        // returns the name of the first candidate in the search results
        var getFirstCandidateName = function(jQuery) {
            return jQuery("#candidate_0 h4:first > span > span").text();
        };

        // change what URL /search_json/results/page/1 returns
        var switchSearchResults = function() {
            var origHandler = $.mockjax.handler( window._RESULT_MOCKJAX );
            $.mockjax.clear( window._RESULT_MOCKJAX );

            var results = getSearchResults();
            results.results[0].name = 'Memphis Prestona';
            window._RESULT_MOCKJAX = $.mockjax({
                url: '/search_json/results/page/1',
                responseTime: 100,
                contentType: 'application/json',
                responseText: results
            });

            return origHandler;
        };

        // restore original return value for URL /search_json/results/page/1
        var restoreSearchResults = function( origHandler ) {
            $.mockjax.clear( window._RESULT_MOCKJAX );
            window._RESULT_MOCKJAX = $.mockjax( origHandler );
        };

        QUnit.test("New criteria forces new search", 3, function( assert ) {
            beginTest(
                {
                    user: {
                        is_admin:    false,
                        is_overseer: false,
                        settings:    {
                            "behaviour-search-use_ofccp_field": false
                        }
                    }
                },

                /* Enter some keywords then click a board name to submit the
                 * search. Change the keywords, thus changing the criteria,
                 * then click the same board name. Prove that a new search is
                 * sent to the server.
                 */
                function() {
                    var origHandler;
                    visit('/search');
                    wait().andThen( function() {
                        $('#tour-keywords > textarea').val("Manager");
                        $(".selected.internal").click();

                        var whatever = assert.async();
                        setTimeout( function () { whatever(); }, 10000);

                    }).andThen( function() {
                        ok( getFirstCandidateName($) == 'Hien Vu',
                            "Expected set of results" );

                        origHandler = switchSearchResults();

                        $('#tour-keywords > textarea').val("Teacher");
                        $(".selected.internal").click();
                    }).andThen( function() {
                        ok( getFirstCandidateName($) == 'Memphis Prestona',
                            "Expected set of results");

                        restoreSearchResults(origHandler);

                        $('#tour-keywords > textarea').val("Monk");
                        $(".selected.internal").click();
                    }).andThen( function() {
                        ok( getFirstCandidateName($) == 'Hien Vu',
                            'Restoration of search results succeeded' );
                    });
                }
            );
        } );
    }
);
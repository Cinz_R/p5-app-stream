/* Test hooks */

QUnit.module(
"Stream v2 Ember Frontend - Admin Quota",
function ( hooks ) {
    $.mockjax({
        url: '/api/admin/user_quota/',
        type: 'GET',
        responseTime: 0,
        contentType: 'text/json',
        responseText: {"quota":{"/Company":{"reset_dow": 4, "board":"talentsearch","id":3311,"remaining":"121212","path":"/Company","type":"search-cv"},"/Company/office2/team3/user1":{"reset_dow": 4, "board":"talentsearch","id":123,"remaining":"1234","reset_to": "3000","path":"/Company","type":"search-cv"}}}
    });
    $.mockjax({
        url: '/api/admin/user_quota/',
        type: 'POST',
        responseTime: 0,
        contentType: 'text/json',
        responseText: {"quota":{"/Company":{"board":"talentsearch","id":331,"path":"/Company","remaining":121212,"type":"search-cv"}}}
    });
    QUnit.test("Admin Quota panel", 5, function () {
        beginTest(
            {
                user: { is_overseer: true, settings: { "behaviour-search-use_ofccp_field": true } }
            },
            function () {
                visit('/admin/quota?board=talentsearch&type=search-cv').andThen( function () {
                    equal( $('div.select2-container').first().text().trim(), "Talent Search" );
                    var reset_selects = $('div.admin-section div select');
                    // resetOption
                    equal( reset_selects[0].value, "5", "reset value is correct" );
                    // rolloverOption "" as no option is set
                    equal( reset_selects[1].value, "", "rollover value is correct" );

                    // office2 team 3 user1 has the correct remaining and rollover
                    var team3user1 = $('div.quota-row > div:contains("user1")')[2].parentElement.childNodes[3];
                    equal( team3user1.childNodes[1].childNodes[0] .value, "1234")
                    equal( team3user1.childNodes[3].childNodes[0] .value, "3000")
                });
            }
        );
    });
}
);

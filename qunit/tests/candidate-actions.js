        /* Test hooks */
        QUnit.module(
            "Stream v2 Ember Frontend - Candidate Actions",
            function ( hooks ) {

                $.mockjax({
                    url: '/api/admin/response-flags',
                    type: 'GET',
                    responseTime: 0,
                    contentType: 'text/json',
                    responseText: { data: { flags: [ { colour_hex_code: "#ff0000" , description : "Red", flag_id :1, has_rules :"true", setting_mnemonic :"responses-flagging-enable_red_flag", type:"standard"}, { flag_id: 22, colour_hex_code: '#ff0000', description: 'this is a flag', type: "custom" } ] } }
                });

                QUnit.test("Candidate Actions", 4, function(assert) {
                    beginTest(
                        {
                            user: { is_overseer: false, is_admin: false, settings: { "behaviour-search-use_ofccp_field": false } },
                        },
                        function () {
                            visit('/search');
                            wait().andThen( function() {
                                ok( $('a#saved_searches_link').size() == 1 && $('a#tour-watchdog-dropdown').size() == 1, 'Both watchdog and saved search links visible' );

                                click($('.search-btn-grp > .btn-success'));

                                wait().andThen( function () {
                                    var cheeseButton = $('div.result').first().find('button[title="Cheese Sandwich"]');
                                    ok( cheeseButton.length, "Cheese sandwich button is there" );
                                    click( cheeseButton );
                                }).andThen( function () {
                                    triggerEvent( $('div.result').first().find('span.select2-arrow'), "mousedown" );
                                }).andThen( function () {
                                    var firstChoice = $('ul.select2-results').last().children().first();
                                    ok ( firstChoice.text() === "cheese on toast", "cheese sandwhich results are shown" );

                                    firstChoice.mouseup();
                                }).andThen( function () {
                                    var _SHORTLIST_MOCKJAX = $.mockjax({
                                        url: '/results/results/talentsearch/candidate/0/shortlist',
                                        responseTime: 0,
                                        contentType: 'text/json',
                                        response: function ( request ) {
                                            var query = JSON.parse(request.data.q);
                                            this.responseText = { error: { message: query.list_name+":"+query.advert_id } };
                                            this.status = 400;
                                        }
                                    });

                                    $('div.result').first().find('button:contains("Submit")').click();
                                    var done = assert.async();
                                    setTimeout( function () {
                                        var error = $('div.result').first().find('div.alert-error:not(":hidden")').text().trim();
                                        ok( error === 'cheese_sandwich:131604', "correct cheese sandwich sent" );
                                        done();
                                        $.mockjax.clear( _SHORTLIST_MOCKJAX );
                                    }, 300);
                                });
                            });
                        }
                    );
                });
            }
        );

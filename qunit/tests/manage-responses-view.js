QUnit.module(
    "Stream v2 Ember Frontend - Manage Responses: Responses View",
    function ( hooks ) {
        QUnit.test("Responses view is different", 10, function(assert) {
            var old_RESULT_MOCKJAX = $.mockjax.handler(window._RESULT_MOCKJAX);
            $.mockjax.clear( window._RESULT_MOCKJAX );
            window._RESULT_MOCKJAX = $.mockjax({
                url: '/search_json/results/page/1',
                responseTime: 100,
                contentType: 'text/json',
                responseText: {
                   "facets":[
                      {
                         "Id":"adcresponses_broadbean_adcresponse_is_read",
                         "Label":"Viewed",
                         "Name":"broadbean_adcresponse_is_read",
                         "Options":[ [ "Read", "true", 71 ], [ "Unread", "false", 7 ] ],
                         "SelectedValues":{},
                         "SortMetric":80,
                         "Type":"multilist"
                      },
                      {
                         "Id":"adcresponses_broadbean_adcadvert_id",
                         "Label":"Advert",
                         "Name":"broadbean_adcadvert_id",
                         "Options": { "live": { "Label": "Live", "Options": [ { "label": "Software Company", "value": "144881", "count": 9 }, { "label": "Penguin Tap Dance Teacher", "value": "144869", "count": 7 } ] } },
                         "SelectedValues":{},
                         "SortMetric":20,
                         "Type":"advert"
                      }
                   ],
                   "notices":[],
                   "results":[
                        { "can_downloadcv": 1, "has_profile": 1, "has_cv": 1, "destination": "adcresponses", "name": "John Smith Senior", "email":"john.smith.snr@hotmail.com", "broadbean_adcresponse_rank": 3, "current_position": { "description": "I am a sausage" } },
                        { "can_downloadcv": 0, "has_profile": 1, "has_cv": 0, "destination": "adcresponses", "name": "John Smith Junior", "email":"john.smith.jnr@hotmail.com", "broadbean_adcresponse_rank": 5, "current_position": { "description": "I am a tofu" } },
                   ]
                }
            });

            beginTest(
                {
                    user: { is_overseer: false, search_mode: 'responses', settings: { "behaviour-search-use_ofccp_field": false } }
                },
                function () {
                    visit('/search');

                    wait().andThen( function() {
                        ok ( ! $('label[for="cv_updated_within"]').length, 'certain controls are missing for responses view' );
                        ok( $('div.result').first().find('button[title="Download to desktop"]:visible').length, "Download to desktop is available" );
                        var firstResultDescription = $('.response-snippet:visible').first().children('div').last();
                        ok( firstResultDescription.text().replace(/^\s+/,"").replace(/\s+$/,"").match("sausage") );
                        ok( $('.snippet-check').prop('checked') );
                        $('.snippet-check').prop('checked',false);
                        $('.snippet-check').change();
                        ok( $('a#saved_searches_link').hasClass('restrictUI') && $('a#tour-watchdog-dropdown').hasClass('restrictUI'), 'Both watchdog and saved search links greyed out' );
                    }).andThen( function (){
                        ok( ! $('div.result').first().find('button[title="Download to desktop"]').length, "Download to desktop is hidden" );
                        ok( $('div.result').first().find('button[title="Preview"]').length, "Preview button is shown still" );
                        $('.snippet-check').prop('checked',true);
                        $('.snippet-check').change();
                    }).andThen( function () {
                        ok( $('.response-snippet:visible').length );
                        equal(
                            $('div.result').find('button[title="Downloading this candidate\'s CV is not possible"]').hasClass('disabled-action-btn'),
                            true,
                            "Download CV button indicates no CV available"
                        );
                    }).andThen( function () {
                        $('div.result:nth-child(2)').find('button[title="Preview"]').click();
                    }).andThen( function() {
                        equal(
                            $('div.profile-box').find('div[title="Downloading this candidate\'s CV is not possible"]').hasClass('disabled-action-btn'),
                            true,
                            "Profile button is disabled for responses candidate with no CV"
                        );
                        $('div.profile-controls > div.profile-navigation > div.close-container > a.profile-close').click();
                    }).andThen( function() {
                        $.mockjax.clear( window._RESULT_MOCKJAX );
                        window._RESULT_MOCKJAX = $.mockjax( old_RESULT_MOCKJAX );
                    });
                }
            );
        });
    }
);

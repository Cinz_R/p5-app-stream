        QUnit.module("Stream v2 Ember Frontend",
            {
                beforeEach: function () {
                    Ember.run(Stream2, Stream2.advanceReadiness);
                }
            },
            function () {

                // Test hooks
                QUnit.test("Admin Saved Searches Panel", 1, function () {
                    beginTest(
                        {
                            user: { is_overseer: true, settings: { "behaviour-search-use_ofccp_field": true } }
                        },
                        function () {
                            visit('/admin/savedsearches').andThen( function () {
                                equal( $('div.title').first().children('span').text(), "test saved search" );
                            });
                        }
                    );
                });

                QUnit.asyncTest("Messaging History", 2, function() {
                    wait().andThen( function () {
                        visit('/admin/emails').then( function() {
                            ok( find( 'div.email-component' ).length === 1,
                                    "There is an email message list container" );
                            var emails = find( 'div.email-component > div > div.emails > div' );
                            ok( emails.length > 0, "There is an email" );
                            start();
                        });
                    });
                });

                QUnit.asyncTest("Previous Searches", 2, function () {
                    wait().andThen( function () {
                        visit('/admin/previoussearches').then(
                            function () {
                                var result = $('div.search-row');
                                ok( result.length > 0, "previous searches are shown" );
                                ok( $(result).find('a').last().text() == "Log", "devuser can see log button" );
                                visit('/').then( function () {
                                    start();
                                    Stream2.reset();
                                });
                            }
                        );
                    });
                });
                QUnit.asyncTest("Admin Settings List", 13, function () {
                    wait().andThen( function () {
                        visit('/admin/settings?setting=1').then( function () {
                            ok( find( 'div.settings-row-label:visible' ).length === 1, "user rows are shown, uncollapsed so just 1" );
                            // expand all rows
                            $('div.settings-row-label').children('strong').click();
                            ok( find( 'div.settings-row-label:visible' ).length > 1, "user rows are collapsed" );

                            var offValues = $('select').map( function ( i, item ) { return $(item).val() } )
                                            .filter( function ( i, item ) { return item == "0" } )
                                            .length;
                            ok( $('select').length === offValues, "all are off" );
                            $('select').first().val('168');
                            $('select').first().change();

                            wait().andThen( function () {
                                var sixMonthValues = $('select').map( function ( i, item ) { return $(item).val() } )
                                            .filter( function ( i, item ) { return item == "168" } )
                                            .length
                                ok( $('select').length === sixMonthValues, "changing top level value changes all children" );

                                // change one of the users
                                $($('div.settings-row > div:contains("user3")')[1]).siblings('select').val("0");
                                $($('div.settings-row > div:contains("user3")')[1]).siblings('select').change();

                                wait().andThen( function () {
                                    // team has updated
                                    ok( $($('div.settings-row > div:contains("team2")')[1]).siblings('select').val() === undefined,
                                        "parent hierarchy updated appropriately" );
                                    // office has updated
                                    ok( $($('div.settings-row > div:contains("office1")')[1]).siblings('select').val() === undefined,
                                        "parent hierarchy updated appropriately" );
                                    // company has updated
                                    ok( $($('div.settings-row > div:contains("Company")')[1]).siblings('select').val() === undefined,
                                        "parent hierarchy updated appropriately" );

                                    var newSixMonthValues = $('select').map( function ( i, item ) { return $(item).val() } )
                                                .filter( function ( i, item ) { return item == "168" } )
                                                .length

                                    ok( newSixMonthValues === ( sixMonthValues - 4 ), "all other children remain intact" );

                                    $('div.settings-row:contains("user3")').last().children('select').val("0");
                                    $('div.settings-row:contains("user3")').last().children('select').change();

                                    wait().andThen( function () {
                                        var newNewSixMonthValues = $('select').map( function ( i, item ) { return $(item).val() } )
                                                .filter( function ( i, item ) { return item == "168" } )
                                                .length;

                                        ok( newNewSixMonthValues === newSixMonthValues - 3, "only 3 more children updated" );
                                        ok( $('div.settings-row:contains("office2")').children('select').val() === null, "office2 is mixed" );
                                        ok( $('div.settings-row:contains("team4")').children('select').val() === null, "team4 is mixed" );
                                        ok( $('div.settings-row:contains("user1")').last().children('select').val() == "168", "changed sibling is intact" );
                                        ok( $('div.settings-row:contains("team3")').children('select').val() === "168", "team3 is intact" );

                                        visit('/').then( function () {
                                            start();
                                            Stream2.reset();
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
                QUnit.asyncTest("Admin Settings String", 8, function () {
                    wait().andThen( function () {
                        visit('/admin/settings?setting=3').then( function () {
                            ok( $('div.alert.alert-info').text().replace(/[^\w\d]+$/g,'').replace(/^[^\w\d]+/g,'') === "setting information", "Setting information is displayed at the head of the page" );
                            $('strong.left.clickable').click();
                            fillIn( $('#settings-hierarchy-container').find('input[type="text"]'), 'lemon' ).then( function () {
                                ok( ! Ember.A( $('#settings-hierarchy-container').find('input[type="text"]').toArray() ).find( function ( item ) { return $(item).val() !== "lemon"; } ), "all fields are set to lemon" );
                                fillIn( $('div.settings-row:contains("user1")').last().find('input'), 'bacon' ).then( function () {
                                    triggerEvent($('div.settings-row:contains("user1")').last().find('input'),'blur');
                                    wait().andThen( function () {
                                        ok( $('div.settings-row:contains("team4")').find('input').val() === "" );
                                        ok( $('div.settings-row:contains("office2")').find('input').val() === "" );
                                        ok( $('div.settings-row:contains("Company")').find('input').val() === "" );
                                        fillIn( $('div.settings-row:contains("office2")').find('input[type="text"]'), 'lime' ).then( function () {
                                            triggerEvent($('div.settings-row:contains("office2")').last().find('input'),"blur");
                                            wait().andThen( function () {
                                                ok( $('div.settings-row:contains("team3")').find('input').val() === "lime" );
                                                ok( $('div.settings-row:contains("team4")').find('input').val() === "lime" );
                                                ok( $($('div.settings-row:contains("user1")')[2]).find('input').val() === "lime" );

                                                visit('/').then( function () {
                                                    start();
                                                    Stream2.reset();
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
                QUnit.test("Email Templates", 11, function () {
                    wait().andThen( function() {
                        visit('/admin/emailtemplates').then(function () {

                            var first_template_icons =  $($('ul.tags > li')[0]).children('i');
                            ok(  $(first_template_icons[0]).hasClass('icon-puzzle'), 'first template icon-puzzle is present' );
                            ok(  $(first_template_icons[1]).hasClass('icon-flag-circled'), 'first template icon-flag-circled is present' );

                            var second_template_icons =  $($('ul.tags > li')[1]).children('i');
                            ok(  $(second_template_icons[0]).hasClass('icon-puzzle'), 'second template  icon-puzzle is present' );
                            ok(  $(second_template_icons[1]).hasClass('icon-asterisk'), 'second template  icon-flag-circled is present' );
                            ok(  $(second_template_icons[2]).hasClass('icon-flag-circled'), 'second template  icon-flag-circled is present' );

                            var t_link = $('ul.tags > li > a').first();
                            ok( t_link.children('b').first().text() === 'other template' );
                            t_link.click();
                            wait().andThen( function () {

                                // template name
                                ok( $($('input')[0]).val() === 'other template', 'template name field is present' );
                                // reply to
                                ok( $($('input')[1]).val() === 'blah@example.com', 'reply to field is present' );
                                // bcc
                                ok( $($('input')[2]).val() === 'bcc@example.com', 'bcc field is present' );
                                // subject
                                ok( $($('input')[3]).val() === 'this is the other template', 'subject field is present' );
                                // body
                                ok( $('textarea').val() === 'this is the email body', 'body is loaded' )

                                visit('/').then( function () {
                                    Stream2.reset();
                                });
                            });
                        });
                    });
                });

                QUnit.asyncTest("Admin panel", 12, function () {
                    wait().andThen( function () {
                        var links = $('div.menu-pill-header > div').children('a');
                        ok(
                            !! links.filter(function(i,a){return !!( $(a).text() == 'Admin Panel')}).length,
                            "Admin panel link is available for super-admins"
                        );

                        visit('/admin').then(
                            function () {
                                var tag1 = find( 'span.tag-flavour-default:contains("Tag 1")' );
                                var hotlist1 = find( 'span.tag-flavour-hotlist:contains("Hotlist 1")' );
                                var shortlist1 = find( 'span.tag-flavour-shortlist:contains("Shortlist 1")' );
                                ok( tag1.length, "ok tags are displayed" )
                                ok( hotlist1.length, "ok hotlists are displayed" );
                                ok( shortlist1.length, "ok shortlists are displayed" );

                                ok( hotlist1.parent().find('input').prop('disabled') === false, "hotlist tag can be changed to none hotlist" );
                                ok( hotlist1.parent().find('input').prop('checked'), "hotlist checkbox is checked" );

                                ok( tag1.parent().find('input').prop('disabled') === false, "default tag can be changed to hotlist" );
                                ok( tag1.parent().find('input').prop('checked') === false, "default tag, hotlist checkbox is not ticked" );

                                ok( shortlist1.parent().find('input').prop('disabled'), "shortlist tag cannot be changed to hotlist" );

                                tag1.siblings().first().click();
                                wait().andThen( function () {
                                    ok( find( 'button.btn-primary:contains("Delete this tag")' ).length, "Delete tag button is visible" );
                                    find( 'button.btn-primary:contains("Delete this tag")' ).click();
                                    wait().andThen( function () {
                                        $('input.text').val("test");
                                        $('input.text').keyup();
                                        wait().andThen( function () {
                                            ok( ! find( 'span.tag-flavour-default:contains("Tag 1")' ).length, "tag1 has been deleted" );
                                            $('span.tag-flavour-default:contains("Tag 2")').siblings().last().children().click();
                                            wait().andThen( function () {
                                                ok( find( 'span.tag-flavour-hotlist:contains("Tag 2")' ), "setting a tag as hotlist works" );
                                                visit('/').then( function () {
                                                    start();
                                                    newUser({ is_overseer: false });
                                                    Stream2.reset();
                                                });
                                            });
                                        });
                                    });
                                });
                            }
                        );
                    });
                });
                QUnit.test("Check boards list is returned", 1, function() {
                    wait().andThen( function () { // act cool
                        var boards = find('ul.board-selection li').map(function() {
                            // for the text we only want the board name, nothing trailing
                            return $(this).text().replace(/[^\w\d]+$/g,'').replace(/^[^\w\d]+/g,'');
                        }).get();
                        deepEqual(boards, ['Disabled Board','Talent Search','CareerBuilder','Responses'], "All boards which have been returned are being displayed");
                    });
                });

                QUnit.test("language is correct", 3, function () {
                    ok( $('.menu-pill-header > div:nth-of-type(2) > a:nth-of-type(2)').text() === 'kittens', 'translations and locale is set correctly for user' );
                    ok( $('label[for="cv_updated_within"]').siblings().first().children().first().val() === 'john', 'pre-compiled criteria is shipped with the locale data');
                    ok( $('#tour-keywords > textarea').attr('placeholder') === 'TESTWHAT', 'placeholder translations are working' );
                });

                QUnit.test("Disabled boards are unselectable", 2, function () {
                    wait().andThen( function () {
                        var selected_board = find('ul.board-selection > li[class=selected]');
                        var disabled_board = find('ul.board-selection > li[class^=disabled]');
                        ok( disabled_board[0], "disabled board exists" );
                        disabled_board.click();
                        andThen( function () {
                            equal( selected_board[0], find('ul.board-selection > li[class=selected]')[0], "can't click disabled boards" );
                        });
                    });
                });

                QUnit.test("Move to another board", 3, function () {
                    fillIn( find( 'input[required]' ), 'some value')
                        .andThen(function(){
                            equal( $('ul.board-selection > li.selected').text().trim(), 'Talent Search', "TalentSearch is selected by default" );
                            var newBoards = find('ul.board-selection > li:not([class*=selected]):not([class*=disabled])');
                            ok( !!newBoards.length, "More than one board has been returned" );
                            click(newBoards[0]);
                            andThen( function () {
                                ok( exists('ul.board-selection > li[class^=selected]:not(":contains(\'TalentSearch\')")')
                                    , "A board which is not TalentSearch has been selected");
                            });
                        });
                });

                QUnit.test("Facet defaults", 1, function () {
                    ok(find('.facetContainer div#sticky-facet-box > div#facet-box > .facets > .facetExpand > select')[0].value == 4,"Drop down default maintained");
                });

                QUnit.test("Facet saved criteria", 3, function () {
                    ok( find('.facet-option:contains("Accountancy") > input[type="checkbox"]')
                        .prop("checked") == true,
                        "saved criteria populates facets ( Accountancy )"
                    );
                    ok( find('.facet-option:contains("Legal") > input[type="checkbox"]').length, "facets expand if hidden are selected" );
                    find('.facetExpand > a:contains("more")').click();
                    ok( find('.facet-option:contains("Legal") > input[type="checkbox"]')
                        .prop("checked") == true,
                        "saved criteria populates facets ( Legal )"
                    );
                });
                QUnit.test("watchdog counts", 4, function () {
                    wait().andThen( function () {
                        ok( find('.total-watchdog-results-count').text().replace(/[^\w\d]+$/g,'').replace(/^[^\w\d]+/g,'') == "17" );
                        ok( $('#watchdogs-dropdown').css('display') == "none" );
                        $('a[data-dropdown="#watchdogs-dropdown"]').click();
                        wait().andThen( function () {
                            ok( $('#watchdogs-dropdown').css('display') == "block" );
                            var text = $('table.watchdogs-list > tbody > tr > td.watchdog-result > div.badge-new').text();
                            ok( text.replace(/[^\w\d]+$/g,'').replace(/^[^\w\d]+/g,'') == "17" );
                        });
                    });
                });

                QUnit.test("Load criteria from ID", 5, function () {
                        visit('/search/CRITERIA_FROM_URL/careerbuilder/page/1');
                        andThen( function () {
                            equal(find('.keywords').val(),"perl developer", "Keywords loaded correctly");
                            equal($('span.unspecified-salaries').children('input').prop('checked'), false, "unspecified salary checkbox gets value");
                            equal($(find('ul.board-selection > li.selected')).text().replace(/[^\w\d]+$/g,'').replace(/^[^\w\d]+/g,''),"CareerBuilder", "The URL controls the selected board");
                            equal( $('div.hero > div > div > select').first().val() , "3Y", "user defaults maintained");
                        });
                        visit('/search/ANOTHER_CRITERIA_FROM_URL/careerbuilder/page/1');
                        andThen( function () {
                            equal( $('div.hero > div > div > select').first().val() , "3D", "cv_updated_within default is ignored in lieu of a pre-defined value" );
                        });
                });

                QUnit.test("Display an error for incorrect criteria_id ( as an example )", 4, function () {
                    ok( find( 'div.hidden > div.alert-error' ).length, "The error box is currently hiddden" );
                    stream2_support_email="kittens";
                    visit('/search/BAD_CRITERIA_ID/careerbuilder/page/1').
                        andThen( function () {
                            ok( !find('div.hidden > div.alert-error')[0] , "The error box is showing" );
                            equal( $( find('div.alert-error > p')[0] ).text(), "There has been an unmanaged error.", "The generated error is displayed correctly" );
                            ok( /kittens/.test($('div.alert-error > p > a')[0].href), "The support email address is picked up and used correctly" );
                        });
                });

                QUnit.test('Error for empty OFCCP field', 1, function(){
                    fillIn( find( 'input[required]' ), '').andThen(function(){
                        $('button.btn-success:contains("kittens")').click();
                        equal( $( find('div.alert-error > p')[0] ).text().trim(), "You must fill in the OFCCP Context field", "OFCCP Error is there" );
                    });
                });

                // ALL the results are going to have to jump into here for now until I figure out how to run multiple
                // on results load type testing
                QUnit.asyncTest("Results are displayed", 49, function () {
                    beginTest(
                        {
                            user: { is_overseer: false, is_admin: false, settings: { "behaviour-search-use_ofccp_field": false }, contact_details: { contact_devuser: null } }
                        },
                        function () {
                            reloadResults().then( function () {
                                ok( $('#loading-screen:hidden').length, "loading screen is hidden" );

                                ok( !! $('div.result#candidate_0').find('img.candidate-history-icon:visible').length, "first candidate has got history and icon is shown" );
                                ok( ! $('div.result#candidate_1').find('img.candidate-history-icon:visible').length, "second candidate has not got history and icon is hidden" );

                                var cur_options = $('label[for="salary_cur"]').siblings().first().children();
                                ok( cur_options.length === 2, "Only two currency options for this user" );
                                ok( $(cur_options[0]).val() === "SAUSAGE", "currency options from user details are adheared to in main search criteria" );
                                ok( $(cur_options[1]).val() === "TOFU" );

                                ok( exists("div.facets > div > label:contains('Accountancy')") );

                                // find the second result with a button that contains the mail icon and is disabled
                                ok( exists($($('div.result')[1]).find('div.controls > div > button > i.icon-mail').parent()), "second result mail icon is disabled" );

                                // group multi lists
                                var mgl1 = find("div.facets > div > h5:contains('list1')");
                                ok( mgl1.length, "we show grouped multi lists" );
                                ok( $(mgl1).siblings('div.facetGroupExpand').children('label').children('span:contains("l1si1")'), "the grouped multi lists have multiple levels of options" );

                                ok( !!find('div#stickybanner').find("option[value=Relevance]").length, "Order by drop down displaying" );

                                ok( !!find('div.result').first().find('button[title="Forward"]:not(:hidden)').length, "first result has got a forward button" );
                                ok( !find('div.result').last().find('button[title="Forward"]:not(:hidden)').length, "last result hasnt got a forward button" );

                                // Results are displayed
                                ok( new RegExp(/Found\s[\d\s]+\sresults/).test( find('.facetHeader > h4').text() ), "Results number displayed" );
                                ok( exists('div#where_the_results_go > div.result'), "Result divs have been added" );

                                // Paging is available
                                var pages = $('div.pager:first > ul > li')
                                equal( pages[0].textContent, 1 );
                                equal( pages[3].textContent, '..' );
                                equal( pages[4].textContent, 10 );

                                // selecting candidates
                                var resultCheckboxes = find('div.head-container > div > div > input[type="checkbox"]');
                                var masterCheckbox = find('div#stickybanner > div > input[type="checkbox"]');

                                // clicking master checkbox turns on all candidates ( oo-err )
                                click( masterCheckbox );

                            wait().andThen( function () {

                                    ok ( masterCheckbox.prop('checked'), "master checkbox is checked" );

                                    var allChecked = true;
                                    resultCheckboxes.each( function () { if ( ! $(this).prop('checked') ) allChecked = false; } );
                                    ok ( allChecked, "the master checkbox controls the checkness of the result checkboxes" );

                                    // uncheck master
                                    click( masterCheckbox );
                            }).andThen( function () {

                                    ok ( ! masterCheckbox.prop('checked'), "master checkbox is unchecked" );
                                    var allChecked = false;
                                    resultCheckboxes.each( function () { if ( $(this).prop('checked') ) allChecked = true; } );
                                    ok ( ! allChecked, "the master checkbox controls the checkness of the result checkboxes" );

                                    // check each result individually
                                    resultCheckboxes.each ( function () {
                                        click( $(this) );
                                    });

                            }).andThen( function () {
                                    ok ( masterCheckbox.prop('checked'), "master checkbox is checked automatically once all candidates are checked" );

                                    click( resultCheckboxes[0] );
                            }).andThen( function () {
                                    ok ( ! masterCheckbox.prop('checked'), "master checkbox is only checked if all candidates are" );

                                    // revert
                                    click( masterCheckbox );
                                    click( masterCheckbox );
                            }).andThen( function () {

                                var download = $('div.controls:first > div > .btn[title="Download to desktop"]');
                                ok( exists( download ), "There is a download button" );

                                var favourite = $('div.controls:first > div > .btn[title="Favourite"]');
                                ok( !exists( favourite ), "There is no favourite button because it has been excluded" );

                                var save = $('div.controls:first > div > .btn[title="Save to your database"]');
                                ok( ! exists( save ), "Save has been excluded" );

                                var email = $('div.controls:first > div > .btn[title="Email"]');
                                ok( exists( email ), "There is an email button" );

                                var preview = $('div.controls:first > div > .btn[title="Preview"]');
                                ok( exists( preview ), "There is a preview button" );

                                var longlist = $('div.controls:first > div > .btn[title="Add to Longlist"]');
                                ok( exists( longlist ) , "There is a longlist button");

                                longlist.click();
                                wait().andThen(function(){
                                    ok(  exists( $('div.inline-form.inline-action') ) , "Ok there is an inline form that is open");
                                    ok(  exists( $('div.inline-form button.btn:contains("Cancel")') ) , "Ok there is a cancel button");
                                });

                                preview.click();

                                // Preview controls work
                                wait().andThen( function () {
                                    var lb = find( 'div.profile-box' );
                                    ok( ! $(lb).hasClass('hidden'), "Profile box is showing" );
                                    ok( exists( 'div.profile-controls > div.profile-navigation > div.prev-next-container > a.nav-disabled' ),
                                        "Previous candidate button is disabled" );

                                    var download_button = $('.candidate-action-btn:visible > .candidate-action-btn-inner > i.icon-download');
                                    ok( exists( download_button ), "Download button is available" );

                                    var forward_button = $('.candidate-action-btn > .candidate-action-btn-inner > i.icon-forward');
                                    ok( exists( forward_button ), "Forward button is not available because it has been excluded" );

                                    var save_button = $('.candidate-action-btn > .candidate-action-btn-inner > i.icon-save');
                                    ok( ! exists( save_button ), "Save button is not available because it has been excluded" );

                                    var shortlist_button = $('.candidate-action-btn:visible > .candidate-action-btn-inner > i.icon-shortlist0');
                                    ok( exists( shortlist_button ) , "Found shortlist icon in the preview");

                                    var close = $('div.profile-controls > div.profile-navigation > div.close-container > a.profile-close');
                                    ok( exists( close ), "close button available" );
                                    close.click();
                                    wait().andThen( function () {
                                        ok( $(lb).hasClass('hidden'), "Profile box is hidden" );
                                    });
                                });
                            }).andThen( function () {
                                var first_candidate_tags = find('.candidate-tags-container > div > ul:first > li');
                                equal( first_candidate_tags.length, 2, 'Tags are displayed' );
                                equal( $(first_candidate_tags[0]).children('label').text(), "test1" );
                                equal( $(first_candidate_tags[1]).children('label').text(), "sausage" );
                                $(first_candidate_tags[0]).children('span').click();
                                wait().andThen( function () {
                                    first_candidate_tags = find('.candidate-tags-container > div > ul:first > li');
                                    equal( first_candidate_tags.length, 1, 'deleted a tag' );
                                });
                            }).andThen( function () {
                                var tagsDiv = find('div.facets#talentsearch_tags'); // used to be find('div.facets').last();
                                equal( tagsDiv.children('.facetExpand').children('.facet-group').children('div').text().trim(), 'Tag ▼', 'faceted search options are loading correctly' );
                                var tagModal = find('div.facet-search-box');
                                $(tagsDiv).children('a').click();
                                tagsDiv.children('.facetExpand').children('a').click();
                                ok( ! tagModal.hasClass('hidden'), 'tag modal is showing' );
                                var tagGroupBox = tagModal.children('div')[1];
                                equal( $(tagGroupBox).children('div.facet-group').children('div').length, 2, 'all tags are grouped and displayed' );
                                $(tagModal).siblings('div.darkness').click();
                                ok( $(tagModal).hasClass('hidden'), 'tag modal is hidden' );
                            }).andThen(function(){
                                var emailButton = find('div.sprite.message');
                                emailButton.click();
                                wait().andThen( function(){
                                    var candidateChooser = find('div.candidate-chooser');
                                    ok( candidateChooser , 'Candidate chooser is there');
                                });
                            }).andThen( function () {
                                visit('/').then( function () {
                                    start();
                                    Stream2.reset();
                                });
                            });
                        });
                    });
                });

                QUnit.test('Auto expand keywords', 2, function () {
                    wait().andThen( function () {
                        var keywords = $('textarea.keywords')[0];
                        var initialHeight = keywords.style.height;
                        $(keywords).keyup();
                        andThen( function () {
                            ok( keywords.style.height == initialHeight, "the keywords box doesn't grow unless you choose something" );
                            $(keywords).val( Array.apply(null, new Array(100)).map(String.prototype.valueOf,"kittens").join(' ') );
                            $(keywords).keyup();
                            wait().andThen( function () {
                                ok( parseInt(keywords.style.height) > parseInt(initialHeight), "the keywords box has grown slightly" );
                            });
                        });
                    });
                });

                QUnit.test("Admin search-as is nicely ordered", 7, function() {
                    ok(find('#search-as-options'));
                    wait().andThen(function() {
                        var $tds = $("table.watchdogs-list > tbody > tr > td");
                        ok($tds.eq(1).html(), 'of1te1u1');
                        ok($tds.eq(4).html(), 'of1te1u2');
                        ok($tds.eq(7).html(), 'of2te1u1');
                        ok($tds.eq(10).html(), 'of2te1u3');
                        ok($tds.eq(13).html(), 'of1te2u1');
                        ok($tds.eq(16).html(), 'of1te2u2');
                    });
                });

            QUnit.test("Watchdogs: unique name enforcement", 3, function(assert) {
                newUser( { is_overseer: false } );
                visit('/watchdogs/create/CRITERIA_FROM_URL');

                // form should not be submissible despite selecting job boards because
                // string matches name of an existing watchdog
                var existingName, uniqueName, nameElem, submitBtn;
                wait().andThen( function() {
                    existingName = 'java developer and php developer';
                    uniqueName   = existingName + 'p';
                    nameElem     = find('input[placeholder="Name your watchdog"]');
                    submitBtn    = find('button:last');

                    ok( submitBtn.text().toLowerCase() == 'create watchdog',
                        "The button we have is indeed the submit button!" );

                    // create a valid form state: 1+ boards selected with unique name
                    click("ul.board-selection li");
                    fillIn(nameElem, uniqueName);
                } ).andThen( function() {

                    // have to wait for click() and fillIn() to complete, for some reason
                    ok( submitBtn.attr('disabled') === undefined,
                        "Submit button is not disabled" );

                    // type an existing watchdog name to invalidate form state
                    fillIn(nameElem, existingName);
                }).andThen( function () {
                    ok( submitBtn.attr('disabled'), 'Typing an existing Watchdog name '
                        + 'invalidates form state' );
                } );
            });

            QUnit.test("Saved Searches: unique name enforcement", 1, function(assert) {
                newUser( { is_overseer: false } );
                visit('/search');

                wait().andThen( function() {

                    // open save search dialog. Enter "Dummy", a saved search that already
                    // exists. Dialog box should remain
                    fillIn( 'div#tour-keywords > textarea', 'my keywords' );
                    find('#save-search-link').click();
                    fillIn('#save-search-dialog input:first', 'Dummy');
                    click('#save-search-dialog button:first');

                    var dialogBox = find('#save-search-dialog');
                    ok( dialogBox.css('display') == 'block',
                        "Dialog box still open: attempt to save search failed" );
                } );
            });

            QUnit.test("Saved Searches select list is working", 6, function () {
                newUser( { is_overseer: false } );
                visit('/search');

                wait().andThen( function() {
                    click( '#saved_searches_link' );
                }).andThen( function () {
                    var email_campaign_header = $('.saved-search-header:contains("Les petite campaign de email"):visible');
                    ok( email_campaign_header.length > 0, 'Email campaigns are listed' );
                    ok( $('.saved-search-header:contains("Making bacon"):visible').length > 0, 'An unknown flavour type is shown' );
                    var ss_lists = email_campaign_header.siblings('table.watchdogs-list');
                    ok( ss_lists.length == 3, 'three lists of saved searches are shown' );
                    ok( ss_lists.first().find('a').first().text().trim() == 'Dummy', 'a saved search link is shown' );
                    ok( ss_lists.last().find('a').first().text().trim() == 'Dummy', 'an unknown search flavour link is shown' );
                    ok( $(ss_lists[1]).find('a').first().text().trim() == 'php asp', 'an email campaign link is shown' );
                });
            });

        }
    );

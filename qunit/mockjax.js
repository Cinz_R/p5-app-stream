var _mockjax_TAGS = [{"flavour":null,"value":"Tag 1","label":"Tag 1"},{"flavour":null,"value":"Tag 2","label":"Tag 2"},{"flavour":"hotlist","value":"Hotlist 1","label":"Hotlist 1"},{"flavour":"shortlist","value":"Shortlist 1","label":"Shortlist 1"}];

/*

  Looking for user details ? Or userDetails?

  Look at newUser in index.html. It's used to inject
  a new user structure in this mockjackery dynamically
  as the test requires.

*/

$.mockjax({
  url: '/api/admin/emails/search',
  responseTime: 0,
  contentType: 'text/json',
  responseText: {"emails":[{"recipientName":null,"recipient":"sausage@example.com","from":"Patrick Mooney","subject":"sausage test test","date":"2015-11-17 11:43:23","id":"138"}],"pager":{"first":1,"last_page":14,"current_page":1,"entries_per_page":10,"last":10,"entries_on_this_page":10,"next_page":2,"previous_page":null,"total_entries":138,"first_page":1}}
});

window._ACTIVE_BOARDS_MOCKJAX_DATA = {
  url:          '/api/boards/active.json',
  responseTime: 100,
  contentType:  'text/json',
  responseText: {
    "boards": [
      {
        "nice_name": "Disabled Board",
        "name":      "disabled_board",
        "type":      "external",
        "disabled":  1,
        "user_quotas": {}
      },
      {
        "nice_name": "Talent Search",
        "name":      "talentsearch",
        "type":      "internal",
        "user_quotas": {}
      },
      {
        "nice_name": "CareerBuilder",
        "name":      "careerbuilder",
        "type":      "external",
        "user_quotas": {}
      },
      {
        "nice_name": "Responses",
        "name":      "adcresponses",
        "type":      "internal",
        "user_quotas": {}
      }
    ]
  }
};
window._ACTIVE_BOARDS_MOCKJAX_INDEX
  = $.mockjax( window._ACTIVE_BOARDS_MOCKJAX_DATA );

/*
    This is an empty response to test whether we
    are able to run search without the need for
    users setting quotas. Mock this request in
    your individual tests when requiring
    quota logic.
*/
$.mockjax({
  url: '/api/user/quota',
  responseTime: 100,
  contentType: 'text/json',
  responseText: {}
});

$.mockjax({
  url: '/results/results/talentsearch/candidate/0/profile',
  responseTime: 100,
  contentType: 'text/json',
  responseText: {"profile_body":"this is the html body"}
});

$.mockjax({
  url: '/results/results/talentsearch/candidate/0/application_history',
  responseTime: 100,
  contentType: 'text/json',
  responseText: {"application_history": []}
});

$.mockjax({
  url: '/results/results/talentsearch/candidate/0/longlist',
  responseTime: 100,
  contentType: 'text/json',
  responseText: {"longlist_membership": {}}
});

$.mockjax({
  url: '/results/results/talentsearch/candidate/0/tags',
  responseTime: 100,
  contentType: 'text/json',
  responseText: { "status_id" : "123456" },
  type: 'DELETE'
});

$.mockjax({
    url: '/api/user/custom_lists',
    resonseTime: 1,
    contentType: 'text/json',
    responseText: [ { icon_class: 'icon-shortlist0', name: 'adc_shortlist' , label: 'Shortlist' }, { icon_class: 'icon-shortlist0', name: 'cheese_sandwich', label: 'Cheese Sandwich' } ]
});

$.mockjax({
    url: '/api/user/keyword-suggest',
    responseTime: 1,
    contentType: 'text/json',
    responseText: { suggestions: [] }
});

$.mockjax({
    url: '/api/admin/user_hierarchy',
    responseTime: 1,
    contentType: 'text/json',
    responseText: {
      "label":"Company","path":"/Company",
        "children":[
          {"label":"office1",  "path":"/Company/office1","children": [
            {"label":"team1",  "path":"/Company/office1/team1","children":[
              {"label":"user1","path":"/Company/office1/team1/user1","id":"1"},
              {"label":"user2","path":"/Company/office1/team1/user2","id":"2"},
              {"label":"user3","path":"/Company/office1/team1/user3","id":"3"}
            ]},
            {"label":"team2",  "path":"/Company/office1/team2","children":[
              {"label":"user1","path":"/Company/office1/team2/user1","id":"4"},
              {"label":"user2","path":"/Company/office1/team2/user2","id":"5"},
              {"label":"user3","path":"/Company/office1/team2/user3","id":"6"}
            ]}
          ]},
          {"label":"office2",  "path":"/Company/office2","children":[
            {"label":"team3",  "path":"/Company/office1/team3","children":[
              {"label":"user1","path":"/Company/office2/team3/user1","id":"7"},
              {"label":"user2","path":"/Company/office2/team3/user2","id":"8"},
              {"label":"user3","path":"/Company/office2/team3/user3","id":"9"}
            ]},
            {"label":"team4",  "path":"/Company/office1/team4","children":[
              {"label":"user1","path":"/Company/office1/team4/user1","id":"10"},
              {"label":"user2","path":"/Company/office1/team4/user2","id":"11"},
              {"label":"user3","path":"/Company/office1/team4/user3","id":"12"}
            ]}
          ]}
        ]}
});

$.mockjax({
    url: '/api/admin/available_settings',
    responseTime: 1,
    contentType: 'text/json',
    responseText: [{"meta_data":{"options":[["Off","0"],["3 Months","84"],["6 Months","168"]]},"type":"string","id":1,"label":"enum type"},{"type":"boolean","id":2,"label":"boolean type"},{"type":"string","id":3,"label":"string type", "meta_data":{"information":"setting information"}}],
});

$.mockjax({
    url: '/api/admin/user_settings/3',
    responseTime: 1,
    contentType: 'text/json',
    responseText: {"users_map":[],"default":{"boolean_value":false,"string_value":"0"}}
});

$.mockjax({ url: '/static/translations/en.json?v=somefakeversion' });

$.mockjax({
    url: '/api/admin/user_settings/1',
    responseTime: 1,
    contentType: 'text/json',
    responseText: {"users_map":[],"default":{"boolean_value":false,"string_value":"0"}}
});

$.mockjax({
    url: '/api/admin/user_settings/2',
    responseTime: 1,
    contentType: 'text/json',
    responseText: {"users_map":[],"default":{"boolean_value":false,"string_value":"0"}}
});


$.mockjax({
    url: '/results/results/talentsearch/candidate/0/tags',
    responseTime: 100,
    contentType: 'text/json',
    responseText: {}
});

$.mockjax({
    url: '/static/translations/fr.json?v=somefakeversion',
    responseTime: 100,
    contentType: 'text/json',
    responseText: {
        criteria: { cv_updated_within_options : [[ 'john', 'smith' ]] }, placeholders: { placeholderWhere: "TESTWHERE"  , placeholderKeywords: "TESTWHAT" }, i18nextKeys: { fr: { translation: { 'Search': 'kittens', 'Email Campaigns': 'Les petite campaign de email' } } }
    }
});

$.mockjax({
    url: '/api/admin/tags/1',
    contentType: 'text/json',
    responseText: {"pager":{"first":1,"last_page":4,"current_page":1,"entries_per_page":20,"last":20,"entries_on_this_page":20,"next_page":2,"previous_page":null,"total_entries":65,"first_page":1},"tags":_mockjax_TAGS}
});

$.mockjax({
    url: '/api/admin/tags/tag/delete',
    contentType: 'text/json',
    responseText: { "message" : "whatever" },
    onAfterSuccess: function () { _mockjax_TAGS.shift(); }
});

$.mockjax({
    url: '/api/admin/tags/tag/set_flavour',
    responseText: { "message":"ok" }
});

$.mockjax({
    url: '/api/user/tags',
    responseTime: 100,
    contentType: 'text/json',
    responseText: {"suggestions":[{"value":"test3","label":"test3"}]}
});

$.mockjax({
    url: '/api/admin/tags/tag/info',
    contentType: 'text/json',
    responseText: {"tag":{"n_candidates":4,"flavour":null,"value":"Tag 1","label":"Tag 1"}}
});

$.mockjax({
    url: '/api/user/oversees',
    responseTime: 100,
    contentType: 'text/json',
    responseText: {"overseen_users":[{"office":"office1","consultant":"of1te2u2","id":"100","team":"team2","company":"company1"},
                                     {"office":"office2","consultant":"of2te1u1","id":"3","team":"team1","company":"company1"},
                                     {"office":"office1","consultant":"of1te2u1","id":"6","team":"team2","company":"company1"},
                                     {"office":"office1","consultant":"of1te1u1","id":"1","team":"team1","company":"company1"},
                                     {"office":"office2","consultant":"of2te1u3","id":"5","team":"team1","company":"company1"},
                                     {"office":"office1","consultant":"of1te1u2","id":"2","team":"team1","company":"company1"}
                                     ]}
    });

$.mockjax({
    url: '/api/user/isadmin',
    responseTime: 100,
    contentType: 'text/json',
    responseText: {"is_admin":true}
});


$.mockjax({
  url: '/api/savedsearches',
  responseTime: 900,
  contentType: 'text/json',
  responseText: {"data":{"searches":[{"flavour":"search","insert_datetime":"2013-08-28 13:29:58","criteria_id":"nTiyD3os4QEhHw_tEx64O8d2V1U","update_datetime":"2013-08-28 13:29:58","active":"1","name":"Dummy","user_id":"andy@users.live.andy","id":"35"},{"flavour":"making_bacon","insert_datetime":"2013-08-28 13:30:25","criteria_id":"anDfW1UiXunxHCGF7d7lGnuMVTs","update_datetime":"2013-08-28 13:30:25","active":"1","name":"Dummy","user_id":"andy@users.live.andy","id":"36"},{"flavour":"email_campaign","insert_datetime":"2013-09-04 10:32:02","criteria_id":"liAR39NYbMwlF6Leff8EqP3y5rs","update_datetime":"2013-09-04 10:32:02","active":"1","name":"php asp","user_id":"andy@users.live.andy","id":"37"}]}}
});

$.mockjax({
  url: '/search_json/run',
  responseTime: 100,
  contentType: 'text/json',
    responseText: {"sort_by_token":{"Value":"Distance","Options":[["Relevance","Relevance"],["Last Updated","Last Updated"],["Distance","Distance"]],"Type":"sort","Board":"talentsearch","SortMetric":255,"Label":"order_by","Name":"order_by"},"criteria_id":"_IpgMgEnP8rXjhn6ZGES2TE-AlQ","tokens":[{"Options":[["Accountancy","Accountancy"],["Admin and Secretarial","Admin and Secretarial"],["Advertising and PR","Advertising and PR"],["Aerospace","Aerospace"],["Agriculture Fishing and Forestry","Agriculture Fishing and Forestry"],["Arts","Arts"],["Automotive","Automotive"],["Banking","Banking"],["Building and Construction","Building and Construction"],["Call Centre and Customer Service","Call Centre and Customer Service"],["Consultancy","Consultancy"],["Defence and Military","Defence and Military"],["Design and Creative","Design and Creative"],["Education and Training","Education and Training"],["Electronics","Electronics"],["Engineering","Engineering"],["FMCG","FMCG"],["Fashion","Fashion"],["Financial Services","Financial Services"],["Graduates and Trainees","Graduates and Trainees"],["Health and Safety","Health and Safety"],["Hospitality and Catering","Hospitality and Catering"],["Human Resources and Personnel","Human Resources and Personnel"],["IT","IT"],["Insurance","Insurance"],["Legal","Legal"],["Leisure and Sport","Leisure and Sport"],["Logistics Distribution and Supply Chain","Logistics Distribution and Supply Chain"],["Manufacturing and Production","Manufacturing and Production"],["Marketing","Marketing"],["Media","Media"],["Medical and Nursing","Medical and Nursing"],["New Media and Internet","New Media and Internet"],["Not for Profit and Charities","Not for Profit and Charities"],["Pharmaceuticals","Pharmaceuticals"],["Property and Housing","Property and Housing"],["Public Sector and Government","Public Sector and Government"],["Purchasing and Procurement","Purchasing and Procurement"],["Recruitment Consultancy","Recruitment Consultancy"],["Retail","Retail"],["Sales","Sales"],["Science and Research","Science and Research"],["Senior Appointments","Senior Appointments"],["Social Care","Social Care"],["Telecommunications","Telecommunications"],["Transport and Rail","Transport and Rail"],["Travel and Tourism","Travel and Tourism"],["Utilities","Utilities"]],"Type":"multilist","Board":"talentsearch","Name":"advert_industry","SortMetric":70,"Label":"Industry","SelectedValues":{"Accountancy":1,"Legal":1}},{"Options":[["unknown","0"],["all","-1"],["gcse","1"],["a level \/ as level \/ btec \/ nvq","2"],["hnd","3"],["ba \/ bsc \/ beng","4"],["ma \/ msc \/ meng","5"],["phd","6"]],"Id":"theitjobboard_education","Default":"4","Board":"theitjobboard","Label":"Education","Type":"list","SortMetric":140,"Name":"education"},{"Options":[{"Label":"list1","Options":[["l1si1","1"],["l1si2","2"]]},{"Label":"list2","Options":[["l2si1","3"],["l2si2"]]}],"Label":"GroupedList","Type":"groupedmultilist"}],"status_id":"123456"}
});

$.mockjax({
    url: '/api/admin/savedsearches',
    type: 'GET',
    responseTime: 0,
    contentType: 'text/json',
    responseText: {"pager":{"first":1,"last_page":1,"current_page":1,"entries_per_page":5,"last":1,"entries_on_this_page":1,"next_page":null,"previous_page":null,"total_entries":1,"first_page":1},"searches":[{"insert_datetime":"2014-10-29 14:18:15","criteria_id":"a9ec9KgVYvHEwg3qkFJ1uWay7qI","user":"Dont Remove Me","name":"test saved search","id":"77"}]}
});

$.mockjax({
    url: new RegExp('/search_json/[^\/]+/criteria'),
    responseTime: 0,
    contentType: 'text/json',
    responseText: {"salary_per":["annum"],"_search_terms":[],"distance_unit":["miles"],"location_within":["30"],"salary_unknown":["1"],"salary_cur":["GBP"]}
});


window._STATUS_MOCKJAX = $.mockjax({
  url: '/api/asyncjob/123456/status',
  responseTime: 100,
  contentType: 'text/json',
  responseText: {"status":"completed","context":{"result":{"success":1,"max_pages":null,"destination":"talentsearch","current_page":1,"per_page":50,"results_per_scrape":50,"error_msg":null,"total_results":"500","expires_after":10800,"id":"results","error_type":null,"echo_candidate":{"recent_actions":[],"future_message": ""}}}}
});


$.mockjax({
  url: '/api/asyncjob/status/status',
  responseTime: 100,
  contentType: 'text/json',
  responseText: {"status":"completed","context":{"result":{"success":1,"max_pages":null,"destination":"talentsearch","current_page":1,"per_page":50,"results_per_scrape":50,"error_msg":null,"total_results":"500","expires_after":10800,"id":"results","error_type":null}}}
});


// the results of a search: wrapped in a function to make it more accessible
// and changeable
function getSearchResults() {
  return {
    "facets" : [
      {
        "Options" : {
          "default" : {
            "Options" : [
              {
                "count" : 1,
                "value" : "cat",
                "flavour" : "default"
              },
              {
                "count" : 2,
                "value" : "dog",
                "flavour" : "default"
              }
            ],
            "Label" : "Tag"
          }
        },
        "Type" : "tag",
        "Id" : "talentsearch_tags",
        "Board" : "talentsearch",
        "Name" : "tags",
        "Label" : "Tags"
      }
    ],
    "results" : [
      {
        "recent_actions": [ "download" ],
        "advert_salary_currency" : null,
        "advert_salary_per" : null,
        "has_cv" : 1,
        "can_forward": 1,
        "can_downloadcv": 1,
        "application_time" : "2011-11-24T16:45:47",
        "destination" : "talentsearch",
        "advert_county" : "London",
        "employer_org_position_description" : null,
        "advert_industry" : "Banking",
        "email" : "hien@broadbean.com",
        "can_paymessage": true,
        "highlight_title" : null,
        "employer_org_position_end_date" : null,
        "latitude" : 51.501145,
        "address" : null,
        "advert_language" : "en",
        "advert_country" : "GB",
        "telephone" : "0207 536 1661",
        "longitude" : -0.023381,
        "name" : "Hien Vu",
        "highlight_description" : null,
        "advert_postcode" : "EC4M 9BB",
        "application_time_epoch" : 1322092800,
        "uri" : null,
        "highlight_cv" : [
           ", them, save, then, physic, model, ispf, modem, outsid, throughout, sent, drive, automat, enterpris, command, com, workstat, via, fulfil, associ, event, <em>asp</em>, alloc, scsi, correct, statement, capac, channel, allow, recoveri, ids, atm, definit, ticket, alpha, memori, book, each, cisco, timelin, within"
        ],
        "advert_job_reference" : "PERLD64887",
        "html_body" : "HTML_BODY_REMOVED",
        "applicant_availability_date" : "2012-03-08T00:00:00",
        "has_profile" : 1,
        "idx" : 0,
        "employer_org_position_start_date" : null,
        "advert_salary_from_pw" : null,
        "results_id" : "F4580AD4-16DD-11E3-82E7-389CA11FD6DF",
        "cv_text" : "CV_TEXT_REMOVED",
        "employer_org_name" : null,
        "advert_user_name" : "chrisp@users.live.andy",
        "mobile" : null,
        "extracted_postcode" : "E14 9TP",
        "advert_office" : "live",
        "telephone_norm" : null,
        "advert_job_title" : "Perl Developer",
        "employer_org_position_title" : "Software Manager",
        "cv_url" : "s3://attachments.broadbean.com/3909f2bb669dc6741aecc2f6b3da475cff943d1d",
        "candidate_id" : "434682",
        "messaging_uri" : "email://?to=hien%40broadbean.com",
        "channel_id" : "talentsearch",
        "advert_salary_to_pw" : null,
        "advert_id" : "64887",
        "mobile_norm" : null,
        "latlon" : "51.501145,-0.023381",
        "tags" : [
          {
            "flavour" : "hotlist",
            "value" : "test1"
          },
          {
            "value" : "test2",
            "label" : "sausage",
          }
        ],
        "advert_job_type" : "Permanent",
        "advert_team" : "users",
        "rank" : 0,
        "advert_city" : "City of London",
        "applicant_availability_date_epoch" : 1331164800
      },
      {
        "advert_salary_currency" : null,
        "advert_salary_per" : null,
        "has_cv" : 1,
        "application_time" : "2011-11-24T16:45:47",
        "destination" : "talentsearch",
        "advert_county" : "London",
        "employer_org_position_description" : null,
        "advert_industry" : "Banking",
        "email" : "hien@broadbean.com",
        "highlight_title" : null,
        "employer_org_position_end_date" : null,
        "latitude" : 51.501145,
        "address" : null,
        "advert_language" : "en",
        "advert_country" : "GB",
        "telephone" : "0207 536 1661",
        "longitude" : -0.023381,
        "name" : "IM THE SECOND CANDIDATE",
        "highlight_description" : null,
        "advert_postcode" : "EC4M 9BB",
        "application_time_epoch" : 1322092800,
        "uri" : null,
        "highlight_cv" : [
           ", them, save, then, physic, model, ispf, modem, outsid, throughout, sent, drive, automat, enterpris, command, com, workstat, via, fulfil, associ, event, <em>asp</em>, alloc, scsi, correct, statement, capac, channel, allow, recoveri, ids, atm, definit, ticket, alpha, memori, book, each, cisco, timelin, within"
        ],
        "advert_job_reference" : "PERLD64887",
        "html_body" : "HTML_BODY_REMOVED",
        "applicant_availability_date" : "2012-03-08T00:00:00",
        "has_profile" : 1,
        "idx" : 1,
        "employer_org_position_start_date" : null,
        "advert_salary_from_pw" : null,
        "results_id" : "F4580AD4-16DD-11E3-82E7-389CA11FD6DF",
        "employer_org_name" : null,
        "cv_text" : "CV_TEXT_REMOVED",
        "advert_user_name" : "chrisp@users.live.andy",
        "mobile" : null,
        "extracted_postcode" : "E14 9TP",
        "advert_office" : "live",
        "telephone_norm" : null,
        "advert_job_title" : "Perl Developer",
        "employer_org_position_title" : "Software Manager",
        "cv_url" : "s3://attachments.broadbean.com/3909f2bb669dc6741aecc2f6b3da475cff943d1d",
        "candidate_id" : "434682",
        "messaging_uri" : "email://?to=hien%40broadbean.com",
        "channel_id" : "talentsearch",
        "advert_salary_to_pw" : null,
        "advert_id" : "64887",
        "mobile_norm" : null,
        "latlon" : "51.501145,-0.023381",
        "advert_job_type" : "Permanent",
        "advert_team" : "users",
        "rank" : 0,
        "advert_city" : "City of London",
        "applicant_availability_date_epoch" : 1331164800
      },
      {
        "advert_salary_currency" : null,
        "advert_salary_per" : null,
        "has_cv" : 1,
        "application_time" : "2011-11-24T16:45:47",
        "destination" : "talentsearch",
        "advert_county" : "London",
        "employer_org_position_description" : null,
        "advert_industry" : "Banking",
        "email" : "hien@broadbean.com",
        "block_message": 1,
        "highlight_title" : null,
        "employer_org_position_end_date" : null,
        "latitude" : 51.501145,
        "address" : null,
        "advert_language" : "en",
        "advert_country" : "GB",
        "telephone" : "0207 536 1661",
        "longitude" : -0.023381,
        "name" : "Hien Vu",
        "highlight_description" : null,
        "advert_postcode" : "EC4M 9BB",
        "application_time_epoch" : 1322092800,
        "uri" : null,
        "highlight_cv" : [
           ", them, save, then, physic, model, ispf, modem, outsid, throughout, sent, drive, automat, enterpris, command, com, workstat, via, fulfil, associ, event, <em>asp</em>, alloc, scsi, correct, statement, capac, channel, allow, recoveri, ids, atm, definit, ticket, alpha, memori, book, each, cisco, timelin, within"
        ],
        "advert_job_reference" : "PERLD64887",
        "html_body" : "HTML_BODY_REMOVED",
        "applicant_availability_date" : "2012-03-08T00:00:00",
        "has_profile" : 1,
        "idx" : 2,
        "employer_org_position_start_date" : null,
        "advert_salary_from_pw" : null,
        "results_id" : "F4580AD4-16DD-11E3-82E7-389CA11FD6DF",
        "employer_org_name" : null,
        "cv_text" : "CV_TEXT_REMOVED",
        "advert_user_name" : "chrisp@users.live.andy",
        "mobile" : null,
        "extracted_postcode" : "E14 9TP",
        "advert_office" : "live",
        "telephone_norm" : null,
        "advert_job_title" : "Perl Developer",
        "employer_org_position_title" : "Software Manager",
        "cv_url" : "s3://attachments.broadbean.com/3909f2bb669dc6741aecc2f6b3da475cff943d1d",
        "candidate_id" : "434682",
        "messaging_uri" : "email://?to=hien%40broadbean.com",
        "channel_id" : "talentsearch",
        "advert_salary_to_pw" : null,
        "advert_id" : "64887",
        "mobile_norm" : null,
        "latlon" : "51.501145,-0.023381",
        "advert_job_type" : "Permanent",
        "advert_team" : "users",
        "rank" : 0,
        "advert_city" : "City of London",
        "applicant_availability_date_epoch" : 1331164800
      }
    ]
  };
}

window._RESULT_MOCKJAX = $.mockjax({
  url: '/search_json/results/page/1',
  responseTime: 100,
  contentType: 'text/json',
  response: function(settings) {
    this.responseText = getSearchResults();
  }
});

$.mockjax({
  url: '/search_json/CRITERIA_FROM_URL/criteria',
  responseTime: 100,
  contentType: 'text/json',
  responseText: { "criteria_id": "CRITERIA_FROM_URL", "status_id": "status", "criteria": { "keywords":["perl developer"], "include_unspecified_salaries":[0] }}
});

$.mockjax({
  url: '/search_json/CRITERIA_FROM_URL/resume/careerbuilder/page/1',
  responseTime: 100,
  contentType: 'text/json',
  responseText: { "criteria_id": "CRITERIA_FROM_URL", "status_id": "status", "criteria": { "keywords":["perl developer"], "include_unspecified_salaries":[0] } }
});

$.mockjax({
  url: '/search_json/ANOTHER_CRITERIA_FROM_URL/careerbuilder/page/1',
  responseTime: 100,
  contentType: 'text/json',
  responseText: { "criteria_id": "CRITERIA_FROM_URL", "status_id": "status", "criteria": { "keywords":["perl developer"], "include_unspecified_salaries":[0], "cv_updated_within": "3D" }}
});

$.mockjax({
  url: '/search_json/ANOTHER_CRITERIA_FROM_URL/resume/careerbuilder/page/1',
  responseTime: 100,
  contentType: 'text/json',
  responseText: { "criteria_id": "CRITERIA_FROM_URL", "status_id": "status", "criteria": { "keywords":["perl developer"], "include_unspecified_salaries":[0], "cv_updated_within": "3D" } }
});

$.mockjax({
  url: '/search_json/BAD_CRITERIA_ID/resume/careerbuilder/page/1',
  responseTime: 100,
  contentType: 'text/json',
  responseText: { "criteria_id": "CRITERIA_FROM_URL", "error": { "message" : "ERROR TEST", "properties": { "type": "keywordstoolong" } }, "is_unmanaged": 1 }
});


$.mockjax({
  url: '/search_json/BAD_CRITERIA_ID/criteria',
  responseTime: 100,
  contentType: 'text/json',
  responseText: { "criteria_id": "CRITERIA_FROM_URL", "status_id": "status", "criteria": { "keywords":["perl developer"] } }
});

$.mockjax({
  url: '/api/user/message-defaults',
  responseTime: 100,
  contentType: 'text/json',
  responseText: {}
});

$.mockjax({
  url: '/api/watchdogs',
  contentType: 'text/json',
  responseText: {"watchdogs":[{"insert_datetime":"2014-07-28 15:32:25","criteria_id":"IZkuPDIAH4hEdPRHinuS21MvIBY","name":"java developer and php developer","boards":[{"count":7,"nice_name":"Indeed","name":"indeed","last_run_error":null,"last_run_datetime":"2014-07-29 07:26:16"},{"count":10,"nice_name":"Talent Search","name":"talentsearch","last_run_error":null,"last_run_datetime":"2014-07-29 07:26:21"}],"id":7,"new_results":17}]}
});

$.mockjax({
  url: '/api/admin/previous-searches',
  contentType: 'text/json',
  responseText: {"pager":{"first":1,"last_page":11,"current_page":1,"entries_per_page":25,"last":25,"entries_on_this_page":25,"next_page":2,"previous_page":null,"total_entries":273,"first_page":1},"facets":{"destination":[{"count":196,"label":"talentsearch","term":"talentsearch"}],"user":[{"count":222,"label":"Dutchie Dutch","term":"96"}]},"results":[{"_id":"esCvGhZtQ9OkBPqCMIlfuA","destination":"bullhorn","user_provider":"adcourier","board_specific":[],"s3_log_id":"EBB1A9DA-C1A9-11E4-B0B9-563DDBDC46D8","criteria_id":"AXHZeyj9_2X3Lb_Gd4AoEeGrM-w","user":{"email":"patrick@broadbean.com","name":"Patrick Mooney","label":"Patrick Mooney","id":"70"},"time_completed":"2015-03-03T13:33:47","company":"pat","n_results":0,"user_provider_id":"150780","tokens":{"keywords":"","insert_datetime":"2014-12-04 17:17:39","tokens":{"salary_to":[""],"salary_from":[""],"distance_unit":["miles"],"keywords":[""],"salary_per":["annum"],"default_jobtype":[""],"location_within":[30],"salary_cur":["GBP"]},"update_datetime":"2014-12-04 17:17:39","id":"AXHZeyj9_2X3Lb_Gd4AoEeGrM-w"},"stream2_base_url":"https:\/\/192.168.1.213:3001\/","duration":0,"watchdog":0,"user_stream2_id":"70","error_type":"LOGIN ERROR"}]}
});

$.mockjax({
    url: '/api/cheesesandwiches/cheese_sandwich',
    contentType: 'text/json',
    responseText: {"suggestions":[{"value":"131604","label":"cheese on toast","list":"cheese_sandwich"},{"value":"131603","label":"squirty cheese","list":"cheese_sandwich"}]}
});

$.mockjax({
    url: '/api/admin/email-templates',
    contentType: 'text/json',
    responseText: {"pager":{"first":1,"last_page":1,"current_page":1,"entries_per_page":10,"last":2,"entries_on_this_page":2,"next_page":null,"previous_page":null,"total_entries":2,"first_page":1},"emailtemplates":[{"insert_datetime":"2015-06-11T15:18:39Z","name":"other template","id":"2", "rules_data":[{ "flag_id": "1", "id": "111", "role_name": "ResponseFlagged" }]},{"insert_datetime":"2015-06-08T12:42:27Z","name":"Patrick","id":"1","rules_data": [{ "flag_id": "222", "id": "29", "role_name": "ResponseFlagged" },{ "id": "223", "role_name": "ResponseReceived" }]}]}
});

$.mockjax({
    url: '/api/user/email-templates/2',
    contentType: 'text/json',
    responseText: {"emailReplyto":"blah@example.com","emailBody":"this is the email body","emailSubject":"this is the other template","templateName":"other template","id":"2","emailBcc":"bcc@example.com", "templateRules": []}
});

$.mockjax({
    url: '/api/admin/user_groupsettings/email_template-2-active',
    contentType: 'text/json',
    responseText: { "users_map": [], "default": { "boolean_value": false, "string_value": "0" } }
});


$.mockjax({
  url: '/api/user/activity_report',
  contentType: 'text/json',
  responseText: {"status_id": "54321"}
});

$.mockjax({
    url:         '/api/asyncjob/54321/status',
    contentType: 'text/json',
    responseText: {
        "status":"completed",
        "context":{
            "result":{
                "log_chunk_id":"B462A9D6-4C6A-11E7-AC15-793371F03515",
                "table":{
                    "body":[
                        [
                            {
                                "value":"cinzwonderland"
                            },
                            {
                                "value":"London"
                            },
                            {
                                "value":"Team_2"
                            },
                            {
                                "value":"stan"
                            },
                            {
                                "value":"talentsearch"
                            },
                            {
                                "value":0
                            },
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            {
                                "value":2,
                                "action":"candidate_tag_add",
                                "board":"talentsearch",
                                "user_id":13,
                                "header":"Tagged"
                            },
                            ""
                        ],
                        [
                            {
                                "value":"cinzwonderland"
                            },
                            {
                                "value":"London"
                            },
                            {
                                "value":"team_one"
                            },
                            {
                                "value":"cinzia.second"
                            },
                            {
                                "value":"adcresponses"
                            },
                            {
                                "value":19
                            },
                            {
                                "value":3,
                                "action":"profile",
                                "board":"adcresponses",
                                "user_id":11,
                                "header":"Viewed"
                            },
                            {
                                "value":1,
                                "action":"download",
                                "board":"adcresponses",
                                "user_id":11,
                                "header":"Downloaded"
                            },
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            ""
                        ],
                        [
                            {
                                "value":"cinzwonderland"
                            },
                            {
                                "value":"London"
                            },
                            {
                                "value":"team_one"
                            },
                            {
                                "value":"cinzia.second"
                            },
                            {
                                "value":"candidatesearch"
                            },
                            {
                                "value":1
                            },
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            ""
                        ],
                        [
                            {
                                "value":"cinzwonderland"
                            },
                            {
                                "value":"London"
                            },
                            {
                                "value":"team_one"
                            },
                            {
                                "value":"cinzia.second"
                            },
                            {
                                "value":"reed"
                            },
                            {
                                "value":16
                            },
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            ""
                        ],
                        [
                            {
                                "value":"cinzwonderland"
                            },
                            {
                                "value":"London"
                            },
                            {
                                "value":"team_one"
                            },
                            {
                                "value":"cinzia.second"
                            },
                            {
                                "value":"talentsearch"
                            },
                            {
                                "value":91
                            },
                            {
                                "value":1,
                                "action":"profile",
                                "board":"talentsearch",
                                "user_id":11,
                                "header":"Viewed"
                            },
                            {
                                "value":2,
                                "action":"download",
                                "board":"talentsearch",
                                "user_id":11,
                                "header":"Downloaded"
                            },
                            "",
                            {
                                "value":1,
                                "action":"shortlist_adc_shortlist",
                                "board":"talentsearch",
                                "user_id":11,
                                "header":"Shortlisted"
                            },
                            "",
                            "",
                            "",
                            "",
                            {
                                "value":8,
                                "action":"candidate_tag_add",
                                "board":"talentsearch",
                                "user_id":11,
                                "header":"Tagged"
                            },
                            {
                                "value":1,
                                "action":"update",
                                "board":"talentsearch",
                                "user_id":11,
                                "header":"Edited"
                            }
                        ],
                        [
                            {
                                "value":"cinzwonderland"
                            },
                            {
                                "value":"London"
                            },
                            {
                                "value":"team_one"
                            },
                            {
                                "value":"cinzia.second"
                            },
                            {
                                "value":"totaljobs"
                            },
                            {
                                "value":4
                            },
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            ""
                        ]
                    ],
                    "head":[
                        [
                            {
                                "value":"Company"
                            },
                            {
                                "value":"Office"
                            },
                            {
                                "value":"Team"
                            },
                            {
                                "value":"User"
                            },
                            {
                                "value":"Database"
                            },
                            {
                                "value":"Searches"
                            },
                            {
                                "value":"Viewed"
                            },
                            {
                                "value":"Downloaded"
                            },
                            {
                                "value":"Saved"
                            },
                            {
                                "value":"Shortlisted"
                            },
                            {
                                "value":"Longlisted"
                            },
                            {
                                "value":"(Tearsheeted)"
                            },
                            {
                                "value":"Forwarded"
                            },
                            {
                                "value":"Messaged"
                            },
                            {
                                "value":"Tagged"
                            },
                            {
                                "value":"Edited"
                            }
                        ]
                    ]
                },
                "meta":{
                    "min_action_id":48849750,
                    "max_action_id":48849769,
                    "path_length":5
                },
                "s3_files":[
                    {
                        "content_type":"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                        "file_name":"search_activity.xlsx",
                        "file_url":"http://s3-eu-west-1.amazonaws.com/broadbean-eu-generic/search/private_files/reports/cinzwonderland/B555F7A8-4C6A-11E7-883D-7F3371F03515?AWSAccessKeyId=0D4C6D19XJAR7MZ9Q482&Expires=1496966400&Signature=Rtx%2FJwRnPu%2BHCc9nwc8%2BbraM4Sc%3D"
                    }
                ]
            }
        }
    }

});
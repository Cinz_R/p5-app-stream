var tour, tour2;
var touri18n = {};


/*
    why are we difining the tour elements inside seperate functions?
    its because the elements which use document.querySelectorAll to specify a target
    get evaluated when they are defined. this isn't ideal for tour2 especially as it
    depends on results being loaded before hand!
*/

var _searchIntroTour = function () {

    touri18n = {
        nextBtn: i18n.__('cool!'),
        prevBtn: i18n.__('again!'),
        doneBtn: i18n.__('done'),
        closetooltip: i18n.__('stop this!')
    };

    tour = {
        id: "search-intro-tour",
        steps: [
            {
                title: i18n.__('Welcome to Search!'),
                content: '<br /><p>' + i18n.__("Looks like your first time here: so, let's run a search!") + '</p>',
                target: document.querySelectorAll(".title-box")[0],
                width: '500',
                placement: 'right',
                yOffset: '-20'
            },
            {
                title: i18n.__('Keywords'),
                content: i18n.__('Type your keywords here using Boolean'),
                target: 'tour-keywords',
                placement: "bottom",
                xOffset: 'center'
            },
            {
                title: i18n.__('Location'),
                content: i18n.__('Set radius and start typing a location'),
                target: 'tour-location',
                placement: "bottom",
                arrowOffset: '200'
            },
            {
                title: i18n.__('More Options'),
                content: i18n.__('More search options can be found here'),
                target: 'tour-more-options',
                placement: 'right',
                yOffset: '-20'
            },
            {
                title: i18n.__('Job Boards'),
                content: i18n.__('Select a job board and run your search'),
                target: 'tour-board-selection',
                placement: 'top',
                arrowOffset: 'center',
                xOffset: 'center'
            },
            {
                title: i18n.__('Job Boards'),
                content: i18n.__('You can reorder these boards by dragging/dropping them too'),
                target: 'tour-board-selection',
                placement: 'top',
                arrowOffset: 'center',
                xOffset: 'center'
            },
            {

                /* 
                    this is not the last step of the tour! because we have to wait for results to be 
                    loaded to actually continue, I have split the tour up... the second part of the tour will
                    begin once results have loaded
                */
                title: i18n.__('Board Filters'),
                content: i18n.__('Filter the results based on the job board specific options'),
                target: 'facet-box',
                placement: 'right',
                showNextButton: false,
                ctaLabel: i18n.__('cool!'),
                showCTAButton: true,
                onCTA: function () {
                    if ( document.querySelectorAll(".controls > .unit")[0] ){
                        Ember.run.next( null, function () { _searchIntroTour2(); } );
                    }
                    else {
                        Stream2.one( 'resultsLoaded', function () {
                            Ember.run.next( null, function () { _searchIntroTour2(); } );
                        });
                    }
                    hopscotch.endTour();
                }
            }
        ],
        i18n: touri18n
    };

    $(tour.steps).each( function ( i, step ) {
        step.showPrevButton = true;
        step.skipIfNoElement = true;
    });

    hopscotch.startTour(tour);
};

var _searchIntroTour2 = function () {
    tour2 = {
        id: "search-intro-tour-2",
        steps: [
            {
                title: i18n.__('Candidate Actions'),
                content: i18n.__('Download a candidate as a test'),
                target: document.querySelectorAll(".controls > .unit")[0],
                placement: 'top',
                showSkip: true,
                ctaLabel: touri18n.prevBtn,
                showCTAButton: true,
                onCTA: function () {
                    Ember.run.next( null, function () {
                        hopscotch.startTour(tour, tour.steps.length-1);
                    });
                    hopscotch.endTour();
                },
                nextOnTargetClick: true
            },
            {
                title: i18n.__('Candidate History'),
                content: i18n.__('The icon has turned green to remind you in the future that have actioned the candidate.'),
                target: document.querySelectorAll(".action-success")[0],
                placement: 'top'
            },
            {
                title: i18n.__('Candidate History'),
                content: i18n.__('Actions can be reviewed in the History section: your colleagues will see this too') + '<br /><p>' + i18n.__('by clicking this icon...') + ' <img src="/static/images/intro-history-example.png"/></p>',
                target: document.querySelectorAll(".candidate-history-icon")[0],
                placement: 'left',
                xOffset: '-350',
                yOffset: '-100',
                onShow: function () {
                    Ember.run.next( null, function () {
                        $(document.querySelectorAll(".candidate-history-icon")[0]).click();
                    });
                },
                onPrev: function () {
                    Ember.run.next( null, function () {
                        $(document.querySelectorAll(".candidate-history-icon")[0]).dropdown('hide');
                    });
                },
                onNext: function () {
                    Ember.run.next( null, function () {
                        $(document.querySelectorAll(".candidate-history-icon")[0]).dropdown('hide');
                    });
                }
            },
            {
                title: i18n.__('Candidate Tags/Hotlists'),
                content: i18n.__('Now try to create a test tag for this candidate: you can easily remove it straight afterwards...'),
                target: document.querySelectorAll('.head-container > .unit > .add-tag')[0],
                onShow: function () {
                    Ember.run.next( null, function () {
                        $(document.querySelectorAll('.head-container > .unit > .add-tag')[0]).dropdown('show');
                    });
                },
                onNext: function () {
                    Ember.run.next( null, function () {
                        $(document.querySelectorAll('.head-container > .unit > .add-tag')[0]).dropdown('hide');
                    });
                },
                onPrev: function () {
                    Ember.run.next( null, function () {
                        $(document.querySelectorAll('.head-container > .unit > .add-tag')[0]).dropdown('hide');
                    });
                },
                placement: 'top',
                xOffset: '-270',
                arrowOffset: '260'
            },
            {
                title: i18n.__('Bulk Actions'),
                content: i18n.__('From here you can bulk shortilist, message and tag. Try to select the first 2 candidates and add a test tag to them.'),
                target: document.querySelectorAll("#stickybanner > .unit")[0],
                placement: 'top',
                onPrev: function () {
                    Ember.run.next( null, function () {
                        $(document.querySelectorAll('.head-container > .unit > .add-tag')[0]).dropdown('show');
                    });
                }
            },
            {
                title: i18n.__('Save a Search'),
                content: i18n.__("Let's save this search..."),
                target: document.querySelectorAll(".facetHeader > ul > li > a")[1],
                placement: 'right',
                yOffset: '-23',
                nextOnTargetClick: true,
                showSkip: true,
                onNext: function () {
                    Ember.run.next( null, function () {
                        $('#saved_searches_link').dropdown('show');
                    });
                }
            },
            {
                title: i18n.__('Saved Searches'),
                content: i18n.__('You can find your saved search here, rerun it or delete it if you want!'),
                target: 'saved_searches_link',
                placement: 'left',
                yOffset: '-80',
                arrowOffset: '80',
                onNext: function () {
                    Ember.run.next( null, function () {
                        $('#saved_searches_link').dropdown('hide');
                    });
                },
                onPrev: function () {
                    Ember.run.next( null, function () {
                        $('#saved_searches_link').dropdown('hide');
                    });
                }
            },
            {
                title: i18n.__('Create a Watchdog'),
                content: i18n.__('This will schedule your search to re-run every night...'),
                target: document.querySelectorAll(".facetHeader > ul > li > a")[2],
                placement: 'right',
                yOffset: '-23',
                onPrev: function () {
                    Ember.run.next( null, function () {
                        $('#saved_searches_link').dropdown('show');
                    });
                },
                onNext: function () {
                    Ember.run.next( null, function () {
                        $('#tour-watchdog-dropdown').dropdown('show');
                    });
                }

            },
            {
                title: i18n.__('Watchdogs'),
                content: '<p>' + i18n.__('Your existing watchdogs can be found here...') + '</p><p>' + i18n.__("The green badge indicates the number of new results you've got") + '</p><img src="/static/images/intro-wd-example.png"/><br />',
                target: 'tour-watchdog-dropdown',
                width: '400',
                placement: 'left',
                yOffset: '-10',
                xOffset: '-85',
                onNext: function () {
                    Ember.run.next( null, function () {
                        $('#tour-watchdog-dropdown').dropdown('hide');
                    });
                },
                onPrev: function () {
                    Ember.run.next( null, function () {
                        $('#tour-watchdog-dropdown').dropdown('hide');
                    });
                }
            },
            {
                title: i18n.__('Start Over'),
                content: i18n.__('Click here to see the tour again'),
                target: 'tour-start-button',
                placement: 'bottom',
                arrowOffset: '250',
                xOffset: '-250',
                onPrev: function () {
                    Ember.run.next( null, function () {
                        $('#tour-watchdog-dropdown').dropdown('show');
                    });
                }
            }
        ],
        i18n: {
            nextBtn: i18n.__('cool!'),
            prevBtn: i18n.__('again!'),
            doneBtn: i18n.__('done'),
            closetooltip: i18n.__('stop this!')
        }
    };

    // this is so the step numbers are consistent accross the tours
    var t1Steps = tour.steps.length;
    tour2.i18n.stepNums = [];
    for ( var stepi = 1; stepi <= tour2.steps.length; stepi++ ){
        tour2.i18n.stepNums.push( t1Steps + stepi );
    }

    $(tour2.steps).each( function ( i, step ) {
        step.showPrevButton = true;
        step.skipIfNoElement = true;
    });

    hopscotch.startTour(tour2);
};

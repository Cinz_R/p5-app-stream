// jshint -W097
//"use strict";
//

/*
  Global settings for the Stream2 front end application.
*/
window._initApp = _initApp = function () {
    var DEBUG_ENABLED = false;
    Ember.LOG_TRANSITIONS_INTERNAL = DEBUG_ENABLED;
    Ember.LOG_TRANSITIONS = DEBUG_ENABLED;
    Ember.LOG_BINDINGS = DEBUG_ENABLED;
    Ember.STRUCTURED_PROFILE = DEBUG_ENABLED;

    // make the functions consitent with the rest of the code base so they can be properly read by xgettext
    i18n.__ = i18n.__x = i18n.__n = i18n.__nx = i18n.t;
    i18n.__p = function ( context, key ){
        return i18n.__( key, { context: context } );
    };

    window.Stream2 = Stream2 = Ember.Application.extend();

    var currentAjaxCount = Ember.Object.create({ count: 0 });

    Ember.Application.initializer({
      name: 'user',
      initialize: function(container, application) {
        // will be populated first thing at application model load
        application.register('user:main', Stream2.SearchUser.create(), { instantiate: false });
        [ 'route', 'controller', 'view', 'component' ].forEach( function ( type ) {
            application.inject( type, 'searchUser', 'user:main' );
        });
      }
    });

    Ember.Application.initializer({
      name: 's2i18n',
      initialize: function(container, application) {
        // will be populated first thing at application model load
        application.register('s2i18n:main', Ember.Object.create(), { instantiate: false });
        [ 'route:application', 'component', 'controller' ].forEach( function ( type ) {
            application.inject( type, 'translations', 's2i18n:main' );
        });
      }
    });

    Ember.Application.initializer({
        name: 'currentAjaxCount',
        initialize: function(container, application) {
            // will be populated first thing at application model load
            application.register('currentAjaxCount:main', currentAjaxCount ,{ instantiate: false});
            application.inject( 'controller', 'currentAjaxCount', 'currentAjaxCount:main' );
        }
    });


    // setting global init settings for tinyMCE
    if ( tinyMCE ){
        tinyMCE.baseURL = '/static/tinymce';
    }

    /**
     * Stream2
     * @namespace Stream2
     * @class Stream2
     * @extends Ember.Application
     */
    Stream2 = Ember.Application.extend( AjaxMixin, AnalyticsMixin, Ember.Evented, {
      rootElement: '#application',
      available_boards: {},
      defaultRoute:  'search',
      default_board: null,
      currentAjaxCountObject: currentAjaxCount,


      // Lazy property
     cbOneIamAuth: function(){
         var cbAuth = initializeCBAuth({
             "environment": window.cb_oneiam_environment,
             "clientId":    window.cb_oneiam_client_id,
             "authCallback": window.cb_oneiam_authcallback
         });
         this.set('cbOneIamAuth', cbAuth );

         // An extra method on the cbAuth to assume a given cb_oneiam_user
         // is connected.
         // Returns a promise that is fullfilled with the CB session,
         // and rejected with a message saying what's going on.
         cbAuth.loaded_manage_session_triggers = false;
         cbAuth.assumeCbOneIAMUser = function( cbOneIamUser ){
             var _this = this;
             return new Ember.RSVP.Promise( function ( resolve, reject ){
                 // Manage session only once and returns a promise.
                 console.log("Trying to fullfill the promise that a cbOneIamUser is there ", cbOneIamUser );
                 var has_not = function( status ){
                     console.log("CB OneIAM session is not there or has ended. status: " , status );
                     Stream2.singleRequest('/external/oneiam_sso/logout', null, 'GET').then(
                         function(status){
                             window.parent.focus();
                         }
                     );
                 };
                 var has = function( status ){
                     console.log("CB OneIAM session is there. status: ", status );
                     // TODO: Check the consistency of this session to check it contains the right
                     // user (cbOneIamUser).
                     Stream2.singleRequest('/external/oneiam_sso/verifycb', {"token": status.auth_response.id_token, "ripple_user": cbOneIamUser}, 'POST').then(
                         function(data) {
                             resolve( _this.getCurrentSession() );
                         },
                         function(error) {
                             Stream2.singleRequest('/external/oneiam_sso/logout', null, 'GET').then( function(status){
                                 window.parent.focus();
                             });
                         }
                     );
                 };
                 _this.manageSession({
                     "has_not": has_not,
                     "has": has,
                     "iframe": {
                         "has_not": has_not,
                         "has": has
                     },
                     event_cb: function(checkstatus){
                         // This is called by the manageSession function.
                         //
                         // In turn, we call checkstatus().
                         // Calling this will trigger has or has_not in due time.
                         // We want this to happen now, and not on any sort of window events
                         // like the default does in
                         // https://bitbucket.org/broadbean/cb-openid-widgets/src/184899acdc472c44270a71101a0e563700efef4f/assets/cb/openid/widget.js?at=master&fileviewer=file-view-default
                         checkstatus();
                     }
                 });
             });
         };
         return cbAuth;
      }.property(),
      /**
       * @method aboveIE8
       * @param above {function} Runs if above IE8
       * @param below {function} Runs if below IE8 (included)
       * @return whatever the given functions return.
       */
      aboveIE8: function( above, below ){
          if( ! above ){ above = function(){}; }
          if( ! below ){ below = function(){}; }

          if( $('html').hasClass('lt-ie9') ){
              return below.apply(this);
          }else{
              return above.apply(this);
          }
      },

      getUrl: function (url) {
        return url.match('^/') ? url : "/search_json/" + url;
      },

      popupAlert: function ( title, text ){
          this.aboveIE8(function(){
              if ( window.clippyAgent ) {
                  return window.clippyAgent.speak(title + ", " + text);
              }
              $.gritter.add({
                  title: title,
                  text: text
              });
          });
      },
      popupError: function ( error ) {
          // can pass a string directly from your response or, preferably, an object
          if ( typeof error === "object" ) {
              error = error.message;
          }
          if ( window.clippyAgent ) {
              return window.clippyAgent.speak(error);
          }
          this.popupAlert(i18n.__('Dang it...'), error);
      },



        /*
          compiles once as its a computed property, instead of each time a candidate history is being displayed
        */
        candidateHistoryMeta: function () {
            return [
                {
                    action: 'message',
                    label:  i18n.__('Messages Sent'),
                    singular_label: i18n.__('Message Sent'),
                    icon:   'icon-mail'
                },
                {
                    action: 'message_auto',
                    label:  i18n.__('Auto-responses on Application'),
                    singular_label: i18n.__('Auto-response on Application'),
                    icon:   'icon-mail'
                },
                {
                    action: 'candidate_future',
                    label:  i18n.__('Future actions'),
                    singular_label: i18n.__('Future action'),
                    icon:   'icon-clock',
                },
                {
                    action: 'candidate_future_message',
                    label:  i18n.__('Messages to be sent'),
                    singular_label: i18n.__('Message to be sent'),
                    icon:   'icon-clock',
                },
                {
                    action: 'shortlist_adc_shortlist',
                    label:  i18n.__('Shortlist'),
                    singular_label: i18n.__('Shortlist'),
                    icon:   'icon-shortlist0',
                },
                {
                    action: 'shortlist_bullhorn_jobs',
                    label:  i18n.__('Shortlist (Bullhorn Jobs)'),
                    singular_label: i18n.__('Shortlist (Bullhorn Jobs)'),
                    icon:   'icon-shortlist1',
                },
                {
                    action: 'shortlist_bullhorn_tearsheets',
                    label:  i18n.__('Shortlist (Bullhorn Tearsheets)'),
                    singular_label: i18n.__('Shortlist (Bullhorn Tearsheets)'),
                    icon:   'icon-shortlist2',
                },
                {
                    action: 'note',
                    label:  i18n.__('Note Additions'),
                    singular_label: i18n.__('Note'),
                    icon:   'icon-edit',
                },
                {
                    action: 'candidate_tag_add',
                    label:  i18n.__('Tag Additions'),
                    singular_label: i18n.__('Tag Added'),
                    icon:   'icon-tags',
                },
                {
                    action: 'import',
                    label:  i18n.__('CV Imports'),
                    singular_label: i18n.__('CV Imported'),
                    icon:   'icon-floppy',
                },        // save cv
                {
                    action: 'download',
                    label:  i18n.__('CV Downloads'),
                    singular_label: i18n.__('CV Downloaded'),
                    icon:   'icon-download',
                },
                {
                    action: 'profile',
                    label:  i18n.__('Profile Views'),
                    singular_label: i18n.__('Profile Viewed'),
                    icon:   'icon-profile-view',
                },
                {
                    action: 'update',
                    label:  i18n.__('Candidate Updates'),
                    singular_label: i18n.__('Candidate Updated'),
                    icon:   'icon-star',
                }, // update property in talent search
                {
                    action: 'delete_note',
                    label:  i18n.__('Note Deletions'),
                    singular_label: i18n.__('Deleted Note'),
                    icon:   'icon-trash-1',
                },
                {
                    action: 'candidate_tag_delete',
                    label:  i18n.__('Tag Deletions'),
                    singular_label: i18n.__('Tag Deleted'),
                    icon:   'icon-trash-1',
                },
                {
                    action: 'forward',
                    label:  i18n.__('Forward'),
                    singular_label: i18n.__('Forwarded'),
                    icon:   'icon-forward',
                },
                {
                    action: 'download_fail',
                    label:  i18n.__('CV Download Failures'),
                    singular_label: i18n.__('CV Download Failed'),
                    icon:   'icon-cancel',
                },
                {
                    action: 'profile_fail',
                    label:  i18n.__('Profile Views Failures'),
                    singular_label: i18n.__('CV View Failed'),
                    icon:   'icon-cancel',
                },
                {
                    action: 'longlist_add',
                    label:  i18n.__('Added to Longlist'),
                    singular_label: i18n.__('Added to Longlist'),
                    icon:   'icon-plus-1',
                },
                {
                    action: 'longlist_remove',
                    label:  i18n.__('Removed from Longlist'),
                    singular_label: i18n.__('Removed from Longlist'),
                    icon:   'icon-cancel',
                },
                {
                    action: 'message_fail',
                    label:  i18n.__('Messaging Failures'),
                    singular_label: i18n.__('Message failed'),
                    icon:   'icon-cancel',
                },
                {
                    action: 'import_fail',
                    label:  i18n.__('Import Failures'),
                    singular_label: i18n.__('Import Failed'),
                    icon:   'icon-cancel',
                },
                {
                    action: 'forward_fail',
                    label:  i18n.__('Forwarding Failures'),
                    singular_label: i18n.__('Fowarded Fail'),
                    icon:   'icon-cancel',
                },
                {
                    action: 'candidate_application',
                    label:  i18n.__('Candidate Applications'),
                    singular_label: i18n.__('Candidate Applied'),
                    icon:   'icon-star',
                },
                {
                    action: 'enhancement',
                    label:  i18n.__('Result Enhancements'),
                    singular_label: i18n.__('Result Enhancement'),
                    icon:   'icon-vcard',
                },
                {
                    action: 'enhancement_fail',
                    label:  i18n.__('Enhancement Failures'),
                    singular_label: i18n.__('Enhancement Fail'),
                    icon:   'icon-vcard',
                },
                {
                    action: 'delegate',
                    label:  i18n.__('Delegate candidates'),
                    singular_label: i18n.__('Delegate candidate'),
                    icon:   'icon-exchange',
                },
                {
                    action: 'delegate_fail',
                    label:  i18n.__('Delegation Failures'),
                    singular_label: i18n.__('Delegation Fail'),
                    icon:   'icon-exchange',
                },
            ];
        }.property(),
        candidateHistoryMetaByKey: function () {
            var historyHash = {};
            this.get('candidateHistoryMeta').forEach( function ( item ) {
                historyHash[item.action] = item;
            });
            return historyHash;
        }.property('candidateHistoryMeta')
    }).create();

    // Protect against silly languages..
    try{
        var numeralLangCode
            = getMatchingLangCode( stream2_current_user_locale, 'numeral' );
        numeral.language(numeralLangCode);
    }
    catch(error){
        console.log("Error: " , error , "Falling back to en");
        numeral.language('en');
    }

    // the tinymce plugin uses XMLHttpRequest directly instead of going via jQuery.
    // I have overwritten send to add extra headers
    var superSend = XMLHttpRequest.prototype.send;
    XMLHttpRequest.prototype.send = function () {
        this.setRequestHeader('X-Stream2-CurrentUserId' , stream2_current_user_id );
        this.setRequestHeader('X-Stream2-VERSION' , stream2_VERSION );
        this.setRequestHeader('X-Stream2-VERSIONFRONTEND' , stream2_VERSION_FRONTEND );
        superSend.apply( this, arguments );
    };
};

$(function () {
    try {
        window.easter_egg = new Konami();
        window.easter_egg.code = function () {
            if ( stream2_current_user_group_id != 'cinzwonderland' || window.clippyAgent ){
                return;
            }
            window.clippyAgent = jeromeFace;
            jeromeFace.load();
        };
        window.easter_egg.load();
    }
    catch ( error ) {}
});


/* Gritter global settings (see https://github.com/jboesch/Gritter/blob/master/js/jquery.gritter.js ) */
$(function(){
    $.extend(
        $.gritter.options,
        {
            time: 2000,
            position: 'bottom-right'
        }
    );
});


/*
    On my browser ( chrome ), when I want to restart the app I delete everything after the host.
    This causes chrome to do a "prefetch" of the page in order to render it quicker... although
    it doesn't seem to work properly, it just hassles our server with a full search of talentsearch
    and then when you hit the page it does it all again.
    In order to catch prefetches in chrome you need to use their JS api which is below, other prefetching
    browsers are courteous enough to send a header!

    This JS will not load the app unless the state of the page is "visible"
*/
var chromeVisibility = document.webkitVisibilityState;
if ( typeof( chromeVisibility ) != "undefined" && chromeVisibility == "prerender" ){
    document.addEventListener("webkitvisibilitychange", function () {
        if ( document.webkitVisibilityState == "visible" ) _initApp();
    }, false);
}
else {
    _initApp();
}

// e.g. get me the lang code for numeral.js that best matches our 'nl':
//      getMatchingLangCode('nl', 'numeral');
function getMatchingLangCode(code, library) {

    var langCodeMaps = {

        // key = our lang code; value = tinyMCE's name for it
        tinyMCE: {
            de:    'de',
            en:    'en_GB',
            en_US: 'en',
            es:    'es',
            fr:    'fr_FR',
            fr_CA: 'fr_FR',
            ja:    'ja',
            nl:    'nl',
            pt_BR: 'pt_BR',
            th:    'th_TH',
            sv:    'sv_SE'
        },
        numeral: {
            de:    'de',
            en:    'en-gb',
            en_US: 'en-gb',
            es:    'es',
            fr:    'fr',
            fr_CA: 'fr-CA',
            ja:    'ja',
            nl:    'nl-nl',
            pt_BR: 'pt-br',
            th:    'th',
            sv:    'en'
        }
    };

    if ( !langCodeMaps[library] ) {
        console.log("No mapping exists for library '" + library
            + "', therefore returning unmapped '" + code + "'.");
        return code;
    }

    var translated_lang_code = langCodeMaps[library][code];
    if ( !translated_lang_code ) {
        console.log("Library '" + library + "' does not support lang "
            + "code '" + code + "': falling back to 'en'");
        return 'en';
    }

    return translated_lang_code;
}

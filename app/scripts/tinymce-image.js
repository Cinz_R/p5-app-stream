/*

    imagechooser

    This is a tinymce plugin which extends the insert image plugin by providing an interface
    to manage and upload images

*/

$(function () {
    if ( typeof( tinymce ) !== "undefined" ){
        // only load this plugin once the user language has been established
        Stream2.one( 'userLoaded', function () {
            tinymce.PluginManager.add('imagechooser', function ( editor, url ) {

                var myWin = null;
                var selectedImg = null;
                var mainFileDialogName = null;

                var uploadUrl = '/api/tinymce/upload';
                var listImagesUrl = '/api/tinymce/images';
                var deleteUrl = '/api/tinymce/image';

                var imageContainerStyle = {
                    padding         : '5px',
                    margin          : '5px',
                    border          : '2px solid rgb(215, 215, 215)',
                    'border-radius' : '5px',
                    'float'         : 'left',
                    position        : 'relative',
                    'text-align'    : 'center'
                };
                var chosenImageContainerStyle = {
                    padding : '3px',
                    border  : '4px solid rgb(0, 191, 236)'
                };
                var noImageTextStyle = {
                    margin  : '10px',
                    color   : 'darkGrey'
                };
                var delImageStyle = {
                    position            : 'absolute',
                    bottom              : '15px',
                    right               : '5px',
                    padding             : '2px 4px',
                    'background-color'  : '#DDD',
                    cursor              : 'pointer',
                    display             : 'none'
                };
                var imgLabelStyle = {
                    'font-size'         : '11px',
                    'background-color'  : '#DDD'
                };

                var deleteImage = function ( imgId ) {
                    return $.ajax({
                        type: 'DELETE',
                        url: deleteUrl,
                        data: { id: imgId },
                        success: function () { reloadImages(); }
                    });
                };

                var reloadImages = function () {

                    var myContainer = myWin.find('#insertimagecontainer');
                    if ( ! myContainer.length ){ return; }

                    // set loading state
                    var container = $(myContainer[0].getEl()).children('div').first();
                    $(container).html( $('<img />', { src: '/static/images/loader.gif' }).css({ margin: '40px 0px 0px 320px' }) );

                    // fetch up-to-date list of images for this user
                    $.getJSON(listImagesUrl).done( function ( data ) {
                        if ( data.images.length ){
                            $(container).empty();

                            $.each( data.images, function ( index, item ) {
                                var shortName = ( item.name.length > 20 ) ? item.name.substr( 0, 18 ) + '..' : item.name;
                                var imgEl = $('<img />', { src: item.url, title: item.name }).css({ height: '50px' });
                                var imgContainer = $('<div />').css(imageContainerStyle).html( imgEl );
                                var nameEl = $('<p />').css(imgLabelStyle).html( shortName ).appendTo( imgContainer );

                                $(imgEl).on('click', function () {
                                    if ( selectedImg === imgContainer ){
                                        // unselect chosen image
                                        selectedImg = null;
                                        imgContainer.css(imageContainerStyle);
                                    }
                                    else if ( selectedImg ){
                                        // select a different image
                                        selectedImg.css(imageContainerStyle);
                                        imgContainer.css(chosenImageContainerStyle);
                                        selectedImg = imgContainer;
                                    }
                                    else {
                                        // select a new image
                                        $(imgContainer).css(chosenImageContainerStyle);
                                        selectedImg = imgContainer;
                                    }
                                });

                                var deleteElement = $('<span />').html('&#10005;').css(delImageStyle);
                                deleteElement.on('click', function () {
                                    if ( confirm(i18n.__('Would you like to delete this image?') ) ){
                                        deleteImage( item.id );
                                    }
                                });
                                deleteElement.appendTo(imgContainer);

                                $(imgContainer).hover(
                                    function () {
                                        deleteElement.show();
                                    },
                                    function () {
                                        deleteElement.hide();
                                    }
                                );

                                $(container).append(imgContainer);
                            });
                            $(container).append($('<div />').css({ clear: 'both' }));
                        }
                        else {
                            $(container).html( $('<p />').css(noImageTextStyle).html(i18n.__('No images')) );
                        }
                    });
                };

                var updateProgress = function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    var myContainer = myWin.find('#insertimagecontainer');
                    var container = $(myContainer[0].getEl()).children('div').first();
                    $(container).html( $('<p />').css(noImageTextStyle).html( progress + "%" ) );
                };

                editor.settings.file_browser_callback = function ( field_name, url, type, win ) {
                    myWin = editor.windowManager.open({
                        title: 'Image Manager',
                        width: 800,
                        height: 400,
                        body: {
                            type:  'form',
                            items: [
                                {
                                    type: 'label',
                                    text: i18n.__('Upload an image'),
                                    style: 'font-size: 14px; font-weight: bold'
                                },
                                {
                                    type: 'form',
                                    layout: 'grid',
                                    pack: 'justify',
                                    columns: 2,
                                    items: [
                                        {
                                            label: i18n.__('Select an image to upload'),
                                            name: 'image-chooser-filename',
                                            type: 'combobox',
                                            icon: 'browse',
                                            onaction: function (){
                                                var fileSelector = $('<input type="file" name="image"/>');
                                                fileSelector.fileupload({
                                                    url: uploadUrl,
                                                    progressall: updateProgress,
                                                    done: function () {
                                                        reloadImages();
                                                    },
                                                    error: function ( data ) {
                                                        var json = data.responseJSON;
                                                        var error = json.error ? json.error : i18n.__('Upload failed');
                                                        alert( error );
                                                    }
                                                });
                                                fileSelector.click();
                                            }
                                        }
                                    ]
                                },
                                {
                                    type: 'label',
                                    text: i18n.__('Insert an image'),
                                    style: 'font-size: 14px; font-weight: bold',
                                },
                                {
                                    type: 'label',
                                    text: i18n.__('Select an image to insert and then click OK'),
                                    style: 'margin: 5px 18px;'
                                },
                                {
                                    type: 'container',
                                    layout: 'flow',
                                    minHeight: 200,
                                    style: 'padding: 10px; overflow-y: auto; overflow-x: hidden;',
                                    name: 'insertimagecontainer'
                                }
                            ]
                        },
                        onsubmit: function(e) {
                            // Insert content when the window form is submitted
                            var imgId = $(selectedImg).children('img').first().attr('src');
                            $('#' + field_name).val( imgId );
                        }
                    });

                    // run this in the next loop
                    setTimeout( function () { reloadImages(); }, 0 );
                    return false;
                };
            });
        });
    }
});

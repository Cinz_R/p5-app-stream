// Global settings in the tinymce global namespace.
$(function () {
    if ( typeof( tinymce ) !== "undefined" ){
        // only load this plugin once the user language has been established
        Stream2.one( 'userLoaded', function () {
            var rawMenuItems = [
                [ 'advert_link', i18n.__( 'Advert Link' ) ],
                [ 'advert_email', i18n.__( 'Advert Email' ) ],
                [ 'advert_title', i18n.__( 'Advert Title' ) ],
                [ 'jobtype', i18n.__( 'Advert Job Type' ) ],
                [ 'jobref', i18n.__( 'Advert Reference' ) ],
                [ 'salary_from', i18n.__( 'Advert Salary From' ) ],
                [ 'salary_to', i18n.__( 'Advert Salary To' ) ],
                [ 'currency', i18n.__( 'Advert Salary Currency' ) ],
                [ 'salary_per', i18n.__( 'Advert Salary Frequency' ) ],
                [ 'salary_benefits', i18n.__( 'Advert Salary Benefits' ) ],
                [ 'skills', i18n.__( 'Advert Skills' ) ],
                [ 'industry', i18n.__( 'Advert Industry' ) ],
                [ 'startdate', i18n.__( 'Advert Start Date' ) ],
                [ 'duration', i18n.__( 'Advert Duration' ) ],

                [ 'custom_source_code', i18n.__( 'Board Custom Source Code' ) ],
                [ 'boardid', i18n.__( 'Board ID' ) ],
                [ 'board_nice_name', i18n.__( 'Board Name' ) ],

                [ 'candidate_name', i18n.__( 'Candidate Name' ) ],
                [ 'from_email', i18n.__( 'Candidate Email' ) ],

                [ 'company', i18n.__( 'Company' ) ],
                [ 'contactname', i18n.__( 'Contact Name' ) ],
                [ 'consultant', i18n.__( 'Consultant' ) ],
                [ 'consultant_email', i18n.__( 'Consultant Email' ) ],
                [ 'eza_number', i18n.__( 'EZA Number' ) ],
                [ 'feedbackemail', i18n.__( 'Feedback Email' ) ],
                [ 'office', i18n.__( 'Office' ) ],
                [ 'team', i18n.__( 'Team' ) ],

            ];

            tinymce.PluginManager.add('placeholders', function ( editor, url ) {
                var menuItems = [];
                $.each(rawMenuItems,  function ( index, item ) {
                    menuItems.push({
                        text: item[1],
                        onclick: function () { editor.insertContent( '[' + item[0] + ']' ); }
                    });
                });

                editor.addButton('placeholders', {
                    text: i18n.__( 'Merge Fields' ),
                    type: 'menubutton',
                    icon: false,
                    menu: menuItems
                });
            });
        });
    }
});

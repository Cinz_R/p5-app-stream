var jeromeFace = {
    timeout: null,
    closed: true,
    face: null,
    caption: null,

    wordSpewTime: 300,
    captionFadeTime: 1000,

    load: function () {
        var face = document.createElement('div');
        var caption = document.createElement('div');
        face.className = "jerome-face";
        caption.className = "jerome-face-caption";
        document.getElementsByTagName('body')[0].appendChild(face);
        document.getElementsByTagName('body')[0].appendChild(caption);
        this.face = face;
        this.caption = caption;

        var dPos = this.documentPos();
        this.face.style.left = dPos.left + 100 + "px";
        this.face.style.top = ( dPos.top + window.innerHeight ) - 300 + "px";

        var self = this;
        var dragMove = function (e) {
            var dPos = self.documentPos();
            var relX = dPos.left + e.clientX;
            var relY = dPos.top + e.clientY;
            self.face.style.left = relX - self._mouseXOffset + "px";
            self.face.style.top = relY - self._mouseYOffset + "px";
            self.positionCaption();
        };

        var dragOn = function (e) {
            var dPos = self.documentPos();
            var relX = dPos.left + e.clientX;
            var relY = dPos.top + e.clientY;
            self._mouseXOffset = relX - parseInt( self.face.style.left );
            self._mouseYOffset = relY - parseInt( self.face.style.top );
            window.addEventListener( 'mousemove', dragMove, true );
        };

        var dragOff = function () {
            window.removeEventListener( 'mousemove', dragMove, true );
        };


        face.addEventListener( 'mousedown', dragOn, true );
        window.addEventListener( 'mouseup', dragOff, true );
    },

    _mouseXOffset: null,
    _mouseYOffset: null,

    documentPos: function () {
        var doc = document.documentElement;
        var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
        var top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

        return { top: top, left: left };
    },

    speak: function ( what ) {
        var self = this;
        var words = what.split(' ');
        this.caption.innerHTML = "";
        this.caption.style.display = "block";
        this.positionCaption();
        this.sayWord( words );
    },

    positionCaption: function () {
        this.caption.style.top = parseInt(this.face.style.top) + 100 + "px";
        this.caption.style.left = parseInt(this.face.style.left) + 80 + "px";
    },

    sayWord: function ( words ){
        var next = words.shift();

        this.caption.innerHTML = this.caption.innerHTML + " " + next;
        this.animate();

        var self = this;
        if ( words.length ){
            this.timeout = setTimeout( function () {
                self.sayWord( words );
            }, this.wordSpewTime );
        }
        else {
            if ( !this.closed ){
                this.timeout = setTimeout( function () { self.animate(); }, this.wordSpewTime );
            }
            setTimeout( function () { self.caption.style.display = "none"; }, this.captionFadeTime );
        }
    },
    animate: function () {
        if ( this.closed ){
            this.face.style.backgroundPosition = "0px 0px";
        }
        else {
            this.face.style.backgroundPosition = "100px 0px";
        }
        this.closed = ! this.closed;
    }
};

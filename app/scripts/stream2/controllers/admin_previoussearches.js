Stream2.AdminPrevioussearchesController = Ember.ObjectController.extend({
    needs: ['application'],
    queryParams: ['page','user_id','destination'],
    page: 1,
    user_id: null,
    destination: null,
    nullValue: null,

    userPrefix: null,
    showAllUsers: false,

    hasMoreUsers: function(){
        return this.get('filteredUsers').length > 10;
    }.property('filteredUsers'),
    
    shortenedUsers: function(){
        if( this.get('showAllUsers') ){
            return this.get('filteredUsers');
        }
        return this.get('filteredUsers').slice(0, 10);
    }.property('filteredUsers' , 'showAllUsers'),
    
    filteredUsers: function(){
        var prefix = this.get('userPrefix');
        if( ! prefix ){ return this.get('model.facets.user') };
        return this.get('model.facets.user').filter(function(item){
            return item.label.toLowerCase().startsWith(prefix.toLowerCase());
        });
    }.property('model.facets.user.@each', 'userPrefix'),
    
    isFiltered: function () {
        return ( this.get('user_id') || this.get('destination') );
    }.property('user_id','destination'),


    actions: {
        toggleShowAllUsers: function(){
            this.toggleProperty('showAllUsers');
        }
    }
});

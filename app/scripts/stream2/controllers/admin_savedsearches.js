Stream2.AdminSavedsearchesController = Ember.Controller.extend({
    needs: ['application'],
    queryParams: ['page'],
    page: 1, // This is the default page number. A link with this page number will not contain the parameter at all
             // Ember is magic.

    // This pager will be calculated by the function that calculates the searches
    pager: null,
    textSearch: '',

    savedsearches: function() {
        var _this = this;
        var savedsearches = Ember.A();
        this.set('pager',null);

        Stream2.singleRequest(
            '/api/admin/savedsearches', {}, 'GET',
            {
                page: this.get('page'),
                textSearch: this.get('textSearch')
            }
        ).then(function(data) {
            $.each( data.searches, function(index, search ) {
                savedsearches.pushObject( Stream2.SavedSearch.create(search) );
            });
            _this.set('pager', Stream2.Pager.create( data.pager ));
        });

        return savedsearches;
    }.property('page', 'textSearch'),

    textSearchChanged: function() {
        this.set('page', 1);
    }.observes('textSearch')
});

Stream2.MessageController = Ember.Controller.extend({
    needs: ['application'],

    customLists: null,

    // informs the backend where the request came from.
    uiOrigin: 'message',
    linkableCustomLists: function(){
        return this.get('customLists').filterBy('name' , 'adc_shortlist');
    }.property('customLists.@each.name'),

    // Adverts are allowed to be shown if they can be 'linked'
    advertsAreAllowed: function(){
        return ! Ember.isEmpty( this.get('linkableCustomLists') );
    }.property('linkableCustomLists.@each'),

    advertId: function(){
        var cs = this.get('cheeseSandwich');
        if( cs ){
            return cs.id;
        }
        return null;
    }.property('cheeseSandwich'),


    advertList: function(){
        var cs = this.get('cheeseSandwich');
        if( cs ){
            return cs.list;
        }
        return null;
    }.property('cheesesSandwich'),

    cheeseSandwich: null,
    // The Object counter part of the Cheese Sandwich
    cheeseSandwichObject: function(){
        var cs = this.get('cheeseSandwich');
        if( ! cs ){ return null }
        return Stream2.CheeseSandwich.create(cs);
    }.property('cheeseSandwich'),

    sendingMessage: false, // state
    from: null,
    outcomes: [],
    recipients: [],
    paidRecipients: function(){
        return this.get('recipients').filter(function(c){ return c.get('hasPayMessage'); });
    }.property('recipients.@each'),
    hasPaidRecipients: function(){
        return ! Ember.isEmpty( this.get('paidRecipients') );
    }.property('paidRecipients.@each'),
    formInvalid: function (){
        if (
            ! this.get('messageContent') ||
            ! this.get('messageSubject') ||
            ! this.get('messageEmail')
        ) return true; // it is invalid
    }.property('messageContent','messageSubject', 'messageEmail'),

    _updateMessageContent: function () {
        var _this = this;
        if ( ! this.get('advertId') ) return;

        var message_content = this.get('messageContent');
        if ( ! message_content ) message_content = "";

        if ( this.get('advertId') && ! /\[advert_link\]/.test(message_content) ){
            this.set('_tinymceUpdateContent',true);
            Ember.run.next(function(){
                _this.set('messageContent', message_content + "\n[advert_link]");
            });
        }
    }.observes('advertId'),

    actions: {
        queryCheeseSandwiches: function(query, deferred){
            var query = Ember.Object.create({
                search: query.term,
                ui_origin: this.get('uiOrigin'),
            });

            var promises = this.get('linkableCustomLists').map(function(list){
                return Stream2.request( '/api/cheesesandwiches/' + list.get('name') , query , 'GET' );
            });
            Ember.RSVP.all(promises).then(function(array){
                var cheeseSandwiches = [];
                // Map over each successfull data
                array.map(function(data){
                    // Map over each suggestion
                    Ember.A(data.suggestions).map(function(cheeseSandwich){
                        cheeseSandwiches.push({ id: cheeseSandwich.value,
                                                text: cheeseSandwich.label,
                                                list: cheeseSandwich.list
                                              });
                    })
                });
                deferred.resolve(cheeseSandwiches);
            });
        },
        queryEmailTemplates: function ( query, deferred ) {

            var term = query.term;
            Stream2.request( '/api/user/email-templates', { search: term }, 'GET' ).then(
                function ( data ) {
                    var templates = [];
                    Ember.A( data.emailtemplates ).map( function ( template ) {
                        templates.push({
                            id      : template.id,
                            text    : template.name
                        });
                    });
                    deferred.resolve( templates );
                }
            );

        }
    },
    emailTemplateChanged: function () {
        var et = this.get('emailTemplate');
        if ( ! et ) { return; }

        var _this = this;
        Stream2.request( '/api/user/email-templates/' + et.id, {}, 'GET' ).then(
            function ( data ) {
                _this.set('_tinymceUpdateContent',true);
                Ember.run.next(function(){
                    _this.set('messageContent', data.emailBody);
                    _this.set('messageSubject', data.emailSubject);
                    _this.set('messageEmail', data.emailReplyto);
                    _this.set('messageBcc', data.emailBcc);
                });
            }
        );
    }.observes('emailTemplate').on('init')
});

Stream2.BoardController = Ember.ObjectController.extend({
    needs: ["search","board_results","application"],
    beginPoll: function ( status_id ) {
        var _this = this;
        return new Ember.RSVP.Promise( function ( resolve, reject ){
            Stream2.setTimeout( function (){ _this._poll( resolve, reject, status_id ); }, Stream2._poll_interval );
        });
    },
    _poll: function ( pollResolve, pollReject, status_id ){
        var _this = this;
        Stream2.timeMark("polling");
        return Stream2.request( '/api/asyncjob/' + status_id + "/status" , {} , 'GET' ).then(
            function ( data ){
                if ( data.context && data.context.error ){
                    pollReject( data.context );
                }
                else if ( data.status === "completed" ){
                    var context = data.context.result;
                    _this.set('_results', []);
                    _this.get('content').setSearchContext( context );
                    if ( _this.get('selectedPage') ){
                        _this.get('selectedPage').set('results_id', context.id);
                    }
                    pollResolve( data );
                }
                else {
                    Stream2.setTimeout( function (){ _this._poll( pollResolve, pollReject, status_id ); }, Stream2._poll_interval );
                }
            },
            function ( error ){
                pollReject({ error:  error, is_unmanaged: 1 });
            }
        );
    },

    initBackToTopButton: function() {
        var duration = 500;     // milliseconds

        Ember.run.schedule('afterRender', this, function() {
            var $btn = $('.back-to-top');
            $btn.on( 'click', function(event) {
                event.preventDefault();
                $('html, body').animate( { scrollTop: 0 }, duration );
                return false;
            });

            $('#stickybanner').on('sticky-start', function() {
                $btn.fadeIn(duration);
            } );
            $('#stickybanner').on('sticky-end', function() {
                $btn.fadeOut(duration);
            } );

            if ( this.get('searchUser.manageResponsesView') ) {
                // line up the top facet with the top of the sticky banner when in new manage responses SEAR-2028
                var facetHeaderHeight = ( $('#stickybanner').offset().top - $('.facetHeader').offset().top ) - 20;
                $('.facetHeader').css({ "min-height": facetHeaderHeight + "px" });
            }
        });

    }.on('init'),      // recommended way of "overriding" init()


    /* cvCostGuard

       If active, Executes the given 'guarded' code
       If the user agrees to the cv_cost_warning

       Returns whatever the guarded function returns,
       or nothing

     */
    cvCostGuard: function( active , guarded ){
        var costWarning = this.get('cv_cost_warning');

        if( !active || Ember.isNone(costWarning)) {
            return guarded.apply(this);
        }

        if( confirm(costWarning) ){
            return guarded.apply(this);
        }
        return null;
    },

    disableBulkMessage: function() {
        return this.get('disableBulkAction')
          || ! this.get('selectedCandidates').filterBy('canBulkMessage').length;
    }.property('disableBulkAction'),

    disableBulkTagging: function() {

        // restricted users should never have bulk shortlisting ability
        if ( this.searchUser.get("restrictUI") ) { return true; }

        return this.get("disableBulkAction");
    }.property("disableBulkAction"),
    disableBulkShortlist: function() {
        // restricted users should never have bulk shortlisting ability
        if ( this.searchUser.get("restrictUI") ) { return true; }

        if ( ! this.get('disableBulkAction') ){
            var selectedCandidates = this.get('selectedCandidates');
            return ! selectedCandidates.filterBy('canBulkShortlist').length;
        }
        return true;
    }.property('disableBulkAction'),
    disableBulkAction: function () {
        var selectedCandidates = this.get('selectedCandidates');
        if ( selectedCandidates && selectedCandidates.length ){
            return false;
        }
        return true;
    }.property('selectedCandidates.length'),
    disableBulkForward: function() {
        if ( ! this.get('disableBulkAction') ){
            var selectedCandidates = this.get('selectedCandidates');
            return ! selectedCandidates.filterBy('canBulkForward').length;
        }
        return true;
    }.property('disableBulkAction'),
    disableBulkDelegate: function () {
        if ( ! this.get('disableBulkAction') ){
            var selectedCandidates = this.get('selectedCandidates');
            return ! selectedCandidates.filterBy('canBulkDelegate').length;
        }
        return true;
    }.property('disableBulkAction'),
    // Ticking all the results individually should tick the master box ( and visa versa )

    // checkbox is bound to this attribute
    allSelected: false,

    // when allSelected changes this gets ran
    selectCandidates: function (){
        var results = this.get('controllers.board_results').get('content');
        if ( ! results ) return;
        var selected = this.get('selectedCandidates');

        if( Ember.isNone(selected) ){
            return;
        }

        if ( this.get('allSelected') ){
            if ( selected.length === results.length ) return;
            results.setEach( "isSelected", true );
        }
        else {
            if ( selected.length < results.length ) return;
            results.setEach( "isSelected", false );
        }
    }.observes('allSelected'),

    // if all boxes are ticked, ensure the allSelected is true ( and visa versa )
    selectEachCandidate: function () {
        if ( ! this.get('_results') || ! this.get('_results').length ) return;
        if ( this.get('selectedCandidates').length === this.get('_results').length ) {
            if ( this.get('allSelected') ) return;
            return this.set('allSelected',true);
        }
        if ( ! this.get('allSelected') ) return;
        this.set('allSelected',false);
    }.observes('selectedCandidates.length'),

    // clicking the master checkbox
    actions: {
        selectAll : function () {
            this.set('allSelected', ! this.get('allSelected') );
            return true;
        }
    },

    selectedCandidates: function () {
        var board_results = this.get('controllers.board_results');
        if ( ! board_results ) return [];
        return board_results.get('selectedCandidates');
    }.property('controllers.board_results.selectedCandidates')
});

Stream2.BoardView = Ember.View.extend({
    didInsertElement: function () {
        $('div#stickybanner').sticky({
            topSpacing: 0,
            responsiveWidth: true,
            getWidthFrom: 'div.right-pane',
            zIndex: 100,
        });
    }
});

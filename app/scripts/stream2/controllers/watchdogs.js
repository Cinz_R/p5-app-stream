Stream2.WatchdogsController = Ember.ArrayController.extend({
    needs: ['application'],
    totalResults: function () {
        var total = 0;
        this.forEach( function ( watchdog ) {
            total += watchdog.get('new_results');
        });
        return total;
    }.property('content.@each.new_results')
});

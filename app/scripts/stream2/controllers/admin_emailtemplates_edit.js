Stream2.AdminEmailtemplatesEditController = Stream2.AdminEmailtemplatesController.extend({
    needs: ['application'],
    templateId: null,
    actions: {
        saveTemplate: function() {
            var _this = this;
            Stream2.request('/api/admin/email-templates/' + _this.get('templateId'), _this.templateParams(), 'PUT').then(
                function(data) {
                    _this.set('error', null);
                    Stream2.popupAlert(i18n.__("Template saved"), data.message);
                },
                function(error) {
                    _this.set('error', error);
                }
            );
            return false;
        }
    }
});

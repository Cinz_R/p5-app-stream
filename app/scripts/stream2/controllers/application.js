Stream2.ApplicationController = Ember.Controller.extend({
    // The search controller is accessible from here
    // needs: ['search'],

    userFilter: '',

    overseenUsers: function() {
        var user = this.searchUser;
        if ( user === null ){
            return null;
        }//consultant

        // return only usernames beginning with the filter string
        var filterStr = this.get("userFilter").toLowerCase();
        return user.get('oversees').sort(function(a, b) {
            // sort for  office > team > consultant
            return a.office.localeCompare(b.office) || a.team.localeCompare(b.team) || a.consultant.localeCompare(b.consultant) || 0;
        }).filter( function(elem) {
            return elem.consultant.toLowerCase().indexOf(filterStr) === 0;
        });
    }.property('userFilter'),

    savedSearches: [],

    originalUser: function(){
        var user = this.searchUser;
        if( user === null ){
            return null;
        }
        return user.get('original_user');
    }.property(),

    isAdmin: function(){
        var user = this.searchUser;
        if( user === null ){
            return null;
        }
        return user.get('is_admin');
    }.property(),

    isOverseer: function(){
        var user = this.searchUser;
        if( user === null ){
            return null;
        }
        return user.get('is_overseer');
    }.property(),

    watchdogs: function () {
        return Stream2.Watchdogs.find();
    }.property(),

    totalWatchdogResults: function () {
        var total = 0;
        this.get('watchdogs').forEach( function ( wd ) {
            total += wd.get('new_results');
        });
        return total;
    }.property('watchdogs.@each.new_results'),

    isSavedSearch: function (criteria_id) {
        var matches = this.get('savedSearches').findProperty('criteria_id', criteria_id);
        if (matches){
            return true;
        }
        return false;
    }.property('savedSearches.@each'),

    loadSavedSearches: function() {
        var _this = this;
        this.set('savedSearches',[]);
        Stream2.request("/api/savedsearches", {}, 'GET').then(
            function(data){
                $.each(data.data.searches, function(index, search){
                    _this.get('savedSearches').pushObject(Stream2.SavedSearch.create(search));
                });
            },
            function(error_string){
                alert("Unable to load saved searches");
            }
        );
    },

    // don't show if they are an overseer
    showGlobalSearchActions: function () {
        return !this.get('searchUser.is_overseer');
    }.property('searchUser.is_overseer'),

    watchdogsTarget: function() {
        return this.searchUser.get("restrictUI")
            ? null : "#watchdogs-dropdown";
    }.property("searchUser.restrictUI"),

    savedSearchesTarget: function() {
        return this.searchUser.get("restrictUI")
            ? null : "#saved-searches-dropdown";
    }.property("searchUser.restrictUI")

});

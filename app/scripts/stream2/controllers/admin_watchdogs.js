Stream2.AdminWatchdogsController = Ember.Controller.extend({
    needs: ['application'],
    queryParams: ['page'],
    page:1, // This is the default page number. A link with this page number will not contain the parameter at all.
            // Ember is magic.

    // This pager will be calculated by the function that calculates the watchdogs
    pager: null,
    textSearch: '',

    watchdogs: function(){
        var _this = this;
        this.set('pager', null);
        var watchdogs = Ember.A();

        Stream2.singleRequest(
            '/api/admin/watchdogs', {}, 'GET',
            {
                page: this.get('page'),
                textSearch: this.get('textSearch')
            }
        ).then(function(data){
            $.each( data.watchdogs , function(index, watchdog ){
                watchdogs.pushObject( Stream2.Watchdog.create(watchdog) );
            });
            _this.set('pager', Stream2.Pager.create( data.pager ));
        });

        return watchdogs;
    }.property('page', 'textSearch'),

    textSearchChanged: function () {
        this.set('page' , 1 );
    }.observes('textSearch')

});

Stream2.AdminSettingsController = Ember.ObjectController.extend({
    queryParams: ['setting'],
    needs: ['application'],
    setting: null,
    previousSetting: null,
    autoExpand: true,

    //watch the select-2 dropdown and only update view when a they select a new setting
    newSetting: function () {
        var newSetting = this.get('setting');
        // previousSetting to ensure we aren't unnecessarily re-loading settings
        if ( newSetting && newSetting != this.get('previousSetting') ){
            this.updateModel();
            this.set('previousSetting', newSetting);
        }
    }.observes('setting','hierarchy'),

    // return current setting object from ID
    currentSetting: function () {
        if ( ! this.get('setting') ) return;
        return this.get('availableSettings').findBy('id', parseInt(this.get('setting')));
    }.property('setting'),

/*

    updateModel

    called each time the user changes the setting drop down
    gets the current state for each user and applies it to the view

*/
    updateModel: function () {
        var _this = this;
        this._getUserSettings( this.get('setting') ).then(
            function ( setting_map ){
                // foreach leaf/user, set the default
                $.each( _this.get('_userMap').get('ids'), function ( index, id ) {
                    _this.get('_userMap').get('map')[id].setProperties( setting_map["default"] );
                });

                // foreach leaf/user with a specific setting, update them
                $.each( setting_map.users_map, function ( index, setting ) {
                    $.each( setting.users, function ( index, id ) {
                        var user = _this.get('_userMap').get('map')[id];
                        if ( user ) {
                            user.setProperties( setting.value );
                        }
                    });
                });

                // And glow the container. Cause we like it innit.
                $('#settings-hierarchy-container > .ember-view > .settings-row').pulse({ backgroundColor: '#DAF5E9' },
                                                                                       { duration: 150, pulses: 5 });
            },
            function ( error ) {
            }
        );
    },
/*

    _getUserSettings

    should return the current state of given setting for all users
    default dictates the beginning state for each user
    map tells us user specific changes to the setting

    {
        "default": { boolean_value: false },
        "users_map": [
            {
                value: { boolean_value: true },
                users: [ 'Rogers' ]
            }
         ]
    };
*/
    _getUserSettings: function ( setting ) {
        return Stream2.request('/api/admin/user_settings/' + setting, {}, 'GET');
    },

    // for the template to assess the type of the currently selected setting
    isList: function () {
        var setting = this.get('currentSetting');
        return ( setting && setting.type === 'string' && Ember.isArray(setting.meta_data.options) );
    }.property('currentSetting'),
    isString: function () {
        var setting = this.get('currentSetting');
        return ( setting && setting.type === 'string' && !Ember.isArray(setting.meta_data.options) );
    }.property('currentSetting'),
    isBool: function () { return this.get('currentSetting.type') === 'boolean' }.property('currentSetting')
});

Stream2.ShortlistController = Ember.Controller.extend({
    needs: ['application'],

    cheeseSandwich: null,

    // informs the backend where the request came from (admins can view all adverts).
    uiOrigin: 'shortlist',

    // gives the ability to show shortlists in a grouped fashion in select2 input
    groupSandwiches: false,

    // gives the ability filter out archived jobs (or not)
    filterOutArchived: true,

    sendingMessage: false, // state
    outcomes: [],
    selectedCandidates: [],

    advertId: function(){
        var cheeseSandwich = this.get('cheeseSandwich');
        if( cheeseSandwich ){
            return cheeseSandwich.id;
        }
        return null;
    }.property('cheeseSandwich'),

    // List of paid selected candidates.
    paidSelectedCandidates: function(){
        // Note that you dont have to never have to pay when using talentsearch.
        return this.get('selectedCandidates').filter(function(c){
            return ( c.get('destination') !== "talentsearch" )  && ( ! c.get('has_done_download') ) ;
        });
    }.property('selectedCandidates.@each'),

    // Does this thing holds any candidate where the CV hasnt been downloaded yet?
    hasPaidSelectedCandidates: function(){
        return ! Ember.isEmpty( this.get('paidSelectedCandidates') );
    }.property('paidSelectedCandidates.@each'),

    formInvalid: function (){
        if( ! this.get('cheeseSandwich') ){
            return true;
        }
    }.property('cheeseSandwich'),

    actions: {
        queryCheeseSandwiches: function(query, deferred){
            var query = Ember.Object.create({
                search: query.term,
                ui_origin: this.get('uiOrigin'),
                group_sandwiches: this.get('groupSandwiches'),
                filter_out_archived: this.get('filterOutArchived')
            });

            var toCancel = this.get('cheeseTimer');
            if( toCancel ){
                Ember.run.cancel( toCancel );
            }

            this.set('cheeseTimer', Ember.run.later(this, function(){
                Stream2.singleRequest( '/api/cheesesandwiches/' + this.get('list').get('name'), query , 'GET' ).then(
                    function ( data ) {
                        deferred.resolve(Ember.A(data.suggestions).map(function(item){
                            return {
                                id: item.value,
                                text: item.label
                            }
                        }));
                    // [ { id: term.length , text: term + 'BLA'} ]);
                    });
            }, 1000) );
        }
    }
});

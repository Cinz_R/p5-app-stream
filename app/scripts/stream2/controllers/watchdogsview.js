Stream2.WatchdogsviewController = Ember.ObjectController.extend({
    needs: ['search','application'],
    setupWatchdog: function ( board ) {
        var _this = this;
        var id = this.get('id');
        Stream2.Watchdogs.findById( id ).then (
            function ( watchdog ) {
                _this.set('content',watchdog);
                var search = _this.get('controllers.search');

                // Make sure the current watchdog is cleared
                search.clearWatchdog();

                search.expireAllResults();
                search.set('watchdog', watchdog);

                // this holds we board we will be transitioning too,
                // if a board is supplied, this takes precedence
                var transition_board = board;

                var result_boards = _this.get('boards');
                var watchdog_board_with_results;
                result_boards.forEach( function ( wd_board ) {
                    board = Stream2.Boards.findById(wd_board.name);
                    board.set('newResults', wd_board.get('count'));
                    if (wd_board.get('count') > 0) {
                        watchdog_board_with_results = board
                    }
                });

                // decide on where will transition too
                if ( Ember.isEmpty(transition_board) ) {
                    transition_board = watchdog_board_with_results || board
                }
                var criteria = Stream2.Criterias.findById(  _this.get('criteria_id') );

                _this.transitionToRoute('board.results',
                    criteria,
                    transition_board,
                    { id: 1 }
                );
            },
            function () {
                _this.set('error', i18n.__('Watchdog not found'));
                Ember.run.later( _this, function () {
                    this.transitionToRoute('search.index');
                });
            }
        );
    }
});

Stream2.WatchdogsCreateController = Ember.Controller.extend(
    ErrorMixin,
    {

        needs: ['application'],


        formInvalid: function(){
            var name = this.get('watchdog.name');

            if ( !name || this.get('nameAlreadyExists') ){
                return true;
            }
            if( ! this.get('boards').filterBy('selected').length ){
                return true;
            }

            return false;

        }.property('watchdog.name', 'boards.@each.selected'),

        hasKeywords: function() {
            return Ember.isPresent( this.get('criteria').keywords );
        }.property('criteria.keywords'),

        nameAlreadyExists: function() {
            var newName = this.get('watchdog').get('name');
            return Stream2.Watchdogs.find().findBy('name', newName);
        }.property('watchdog.name'),

        actions:{
            selectBoard: function(board){
                board.toggleProperty('selected');
            },

            createWatchdog: function(){
                var _this = this;

                var criteria = this.get('criteria').serialise();
                this.get('boards').filterBy('selected').forEach( function ( boardModel ) {
                    if (boardModel.hasFacets){
                        var facets = boardModel.serialiseFacets();
                        criteria = $.extend(criteria, facets);
                    }
                });

                var selectedBoards = this.get('boards').filterBy('selected').map(function(board){ board.set('selected', false); return board.get('name'); });
                var name = this.get('watchdog').get('name');

                Stream2.request('/api/watchdogs',
                    {
                        name: name,
                        criteria: criteria,
                        boards : selectedBoards
                    }, 'POST'
                ).then(
                    function( data ){
                        Stream2.Watchdogs.reset();
                        _this.get('controllers.application').set('watchdogs', Stream2.Watchdogs.find() );
                        _this.get('criteria').set('id', data.criteria_id);
                        _this.transitionToRoute('watchdogs.summary');
                    },
                    function( error ){
                        _this.set('error' , error);
                    }
                );
            }
        }

});

Stream2.AdminEmailtemplatesController = Ember.ObjectController.extend({
    needs: ['application'],

    // Template related variables
    templateName: null,

    // Email content
    emailReplyto: null,
    emailBcc: null,
    emailSubject: null,
    emailBody: null,

    // Template rules
    templateRules: [],

    formInvalid: function() {
        if (Ember.isEmpty(this.get('templateName'))) {
            return true;
        }else{
            // If a character is more than 3 byte utf8 encoded - http://stackoverflow.com/questions/10771117/javascript-variable-less-than-4-characters
            if (this.get('templateName').match(/(?:[\uD800-\uDBFF][\uDC00-\uDFFF])/g)){
                return true;
            }
        }

        if (Ember.isEmpty(this.get('emailSubject'))) {
            return true;
        }
        if (this.get('emailSubject').length > 80) {
            return true;
        }
        if (Ember.isEmpty(this.get('emailBody'))) {
            return true;
        }
        // Check the rules.
        var templateRules = this.get('templateRules');
        if( Ember.isEmpty( templateRules ) ){
            // No template rules, for is valid.
            return false;
        }

        // Is any template invalid?
        return !! this.get('templateRules').findBy('dataInvalid');
        
    }.property('templateName', 'emailBody', 'emailSubject', 'templateRules.@each.dataInvalid'),
    templateParams: function() {
        return {
            templateName: this.get('templateName'),
            emailReplyto: this.get('emailReplyto'),
            emailBcc:     this.get('emailBcc'),
            emailSubject: this.get('emailSubject'),
            emailBody:    this.get('emailBody'),
            permissionChanges: this._permissionChanges(),
            templateRules: this.get('templateRules').map(function(rule){
                return rule.serialize();
            })
        }
    },
    _permissionChanges: function() {
        // only send the differences, from what we last sent, so when we have
        // huge company only minimal id with their changes are sent.
        var true_changes = Ember.A();
        var false_changes = Ember.A();
        $.each(this.get('model')._userMap.map, function(index, child) {
            if (!Ember.isNone(child.id)) {
                // we are only interested in the differing settings
                if (child.current_setting != child.boolean_value) {
                    if (child.boolean_value) {
                        true_changes.push(child.id)
                    }
                    else {
                        false_changes.push(child.id)
                    }
                    child.current_setting = child.boolean_value;
                }
            }
        });
        return{ allow: true_changes, deny: false_changes} ;
    },
    actions: {
        removeRule: function( rule ){
            this.get('templateRules').removeObject( rule );
        },
        /**
         * Adds a default template rule to this controller.
         * @method addFlagRule
         */
        addFlagRule: function(){
            this.get('templateRules').pushObject( Stream2.EmailTemplateRuleResponseFlagged.create({}) );
        },
        /**
         * Adds a template rule about response being received.
         * @method addResponseReceivedRule
         */
        addResponseReceivedRule: function(){
            this.get('templateRules').pushObject( Stream2.EmailTemplateRuleResponseReceived.create({}) );
        },

        /**
         * Sends a sample of this email to the current user's email address.
         * @method sendSample
         */
        sendSample: function() {
            var _this = this;
            Stream2.request('/api/admin/email-templates/sample', _this.templateParams(), 'POST').then(
                function(data) {
                    _this.set('error', null);
                    Stream2.popupAlert(i18n.__("Sample sent"), data.message);
                },
                function(error) {
                    _this.set('error', error);
                }
            );
            return false;
        }
    }
});

Stream2.AdminTagsController = Ember.Controller.extend({
    needs: ['application'],
    _requestURL: '/api/admin/tags',
    requestURL: function () { return this.get('_requestURL') + "/" + this.get('page'); }.property('page'),
    page: 1,
    search: function () {
        var _this = this;
        Stream2.singleRequest( this.get('requestURL'), this.criteria(), 'GET' ).then(
            function ( data ) {
                _this.set('tags', data.tags.map( function ( item ) {
                    item.isHotlist = ( item.flavour == 'hotlist' );
                    return Stream2.TagOption.create( item );
                }) );
                _this.set('pager', data.pager);
                if ( _this.get('page') > data.pager.last_page ){
                    _this.transitionToRoute('admin.tags',1);
                }
            }
        );
    },
    tags: null,
    criteria: function () {
        return {
            search: this.get('textSearch'),
            flavour: this.get('hotlistsOnly') ? 'hotlist' : ""
        };
    },
    actions:{
        addTag: function (){
            var _this = this;
            var tag = { "value": this.get('textSearch'), "label": this.get('textSearch') };
            Stream2.request( '/api/admin/tags/tag', tag, 'POST' ).then(
                function ( data ) {
                    _this.set('error', null);
                    if ( data['new'] ) {
                        _this.set('tags', [Stream2.TagOption.create(tag)]);
                        Stream2.popupAlert(i18n.__('Added'));
                    }
                },
                function(error) {
                    _this.set('error', error);
                }
            );
        }
    },

    pager: null,
    availablePages: function () {
        var pager = this.get('pager');
        var pages = [];

        if ( pager ) {
            [
                pager.first_page,
                pager.previous_page,
                pager.current_page,
                pager.next_page,
                pager.last_page
            ].forEach( function ( item ) {
                if ( ! pages.contains(item) ) pages.push(item);
            });
        }

        return pages.sort(function(a, b){return a-b;});
    }.property('pager'),
    hotlistsOnly: false,
    textSearch: "",
    hotlistsOnlyChanged: function () { this.search(); }.observes('hotlistsOnly'),
    textSearchChanged: function () { this.search(); }.observes('textSearch')
});

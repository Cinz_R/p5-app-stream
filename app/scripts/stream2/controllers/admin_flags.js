/* AdminFlagsController
    Controls the interface which allows the admin to update and set permissions
    for standard flags, as well as defining custom flags.
*/
Stream2.AdminFlagsController = Ember.Controller.extend(ErrorMixin,{
    showAddForm: false,

    // called by the update perms action in the router
    allFlags: Ember.computed.union('model.responseFlags','model.standardFlags'),
    permsFlag: function () {
        return this.get('allFlags').findBy('_showHierarchy');
    }.property('allFlags.@each._showHierarchy'),
    isBool: true,

    actions: {
        // adding a new flag
        showAddForm: function () {
            this.set('showAddForm', true);
        },
        cancelAdd: function(){
            this.set('showAddForm', false);
        },

        // updating a flag
        editFlag: function ( flag ) {
            flag.set('editMode', true);
        },
        cancelEdit: function ( flag ) {
            flag.set('editMode', false);
        },
        openHierarchyView: function ( flag ) {
            this.get('allFlags').setEach('_showHierarchy', false);
            flag.set('_showHierarchy',true);
            this._loadHierarchy( flag.get('setting_url') );
        },
        closeHierarchyView: function () {
            this.get('allFlags').setEach('_showHierarchy', false);
        },
        openFlagRulesView: function ( flag ) {
            this.get('allFlags').setEach('_showFlagRules', false);
            flag.set('_showFlagRules',true);
            this._loadHierarchy( flag.get('setting_url') );
        },
        closeFlagRulesView: function () {
            this.get('allFlags').setEach('_showFlagRules', false);
        }
    },

    _loadHierarchy: function( url ) {
        var _this = this;
        Stream2.request( url , {}, 'GET').then(
            function ( setting_map ){
                // foreach leaf/user, set the default
                $.each( _this.get('model._userMap').get('ids'), function ( index, id ) {
                    _this.get('model._userMap').get('map')[id].setProperties( setting_map["default"] );
                });

                // foreach leaf/user with a specific setting, update them
                $.each( setting_map.users_map, function ( index, setting ) {
                    $.each( setting.users, function ( index, id ) {
                        var user = _this.get('model._userMap').get('map')[id];
                        if ( user ) {
                            user.setProperties( setting.value );
                        }
                    });
                });
            },
            function ( error ) {
            }
        );
    }
});


Stream2.BoardResultsController = Ember.Controller.extend({
    showMessageHistory: false,

    model: [],

    needs: ['application','search'],

    queryParams: ['search_id','candidate_idx'],
    candidate_idx: null,
    search_id: null,

    showSnippet: true,

    // This is used in the board/watchdog-results page to seperate results
    // based on time found
    timeGroups: [],

    // not as simple as adding it to the array controller, some stuff needs to be set up in the candidate
    addResult: function ( result ) {

        // is the current user is internal, always show the history icon as it may contain failure logs
        result.hasHistory = result.hasHistory || ! Ember.isEmpty( this.searchUser.get('contact_details.contact_devuser') );

        var candidate_obj;
        var model_name = result.destination + '_candidate';
        if ( Stream2.__container__.lookup( 'model:' + model_name ) ){
            candidate_obj = Stream2[model_name.classify()].create( result );
        }
        else {
            candidate_obj = ( result.editable ) ?
                                Stream2.MutableCandidate.create( result ) :
                                Stream2.Candidate.create( result );
        }

        // IMPORTANT: setting the tags default as [] in the candidate prototype, means that the one [] you have defined
        // is shared amongst ALL candidates unless you define a new one per candidate
        candidate_obj.set( 'tags', Ember.A() );
        if ( result.tags ){
            candidate_obj.set( 'tags', result.tags.map( function ( obj ) {
                return Ember.Object.create(obj);
            }) );
        }

        candidate_obj.set('results_id',this.get('results_id'));
        this.get('model').pushObject( candidate_obj );

        // if the client has a found_time then we want to add them to a time group
        // for display purposes.
        // we need to do this one candidate at a time for IE8 reasons
        var t = candidate_obj.get('relativeTime');
        if ( t ) {
            this.addToTimeGroup( candidate_obj, t );
        }
    },

    // when viewing a watchdog, we group the candidates by when they were found
    // if they belong to a time group then add them to the existing group, otherwise create a new
    // group and add them to it.
    addToTimeGroup: function ( candidate_obj, t ) {
        var timeGroups = this.get('timeGroups');
        for ( var i = 0; i < timeGroups.length; i++ ){
            var group = timeGroups[i];
            if ( group.moment == t ){
                return group.candidates.pushObject( candidate_obj );
            }
        }
        var candidates = Ember.A();
        candidates.pushObject( candidate_obj );
        this.get('timeGroups').pushObject(
            {
                moment: t,
                candidates: candidates
            }
        );
    },

    previewCandidate: null,
    hasPreviewCandidate: function () {
        return ( this.get('previewCandidate') !== null );
    }.property('previewCandidate'),
    profileBoxDisplay: function () {
        var _this = this;

        // trash existing handlers so we don't pile more up
        Ember.$(document).off("keyup.boardResultPreviewKeys");
        if ( this.get('previewCandidate') !== null ) {

            this.get('previewCandidate').fetchProfileHistory();

            this.positionProfileBox();
            Ember.run.next( this, function () {
                this.setupProfileBox();
                $('.perfect-scrollbar').perfectScrollbar();
            } );

            Ember.$(document).on(
                'keyup.boardResultPreviewKeys', function(evt) {
                    var isInputFocused =
                        document.activeElement.tagName.toLowerCase() == 'input';

                    // cursor left = 37, cursor right = 39, ESC = 27
                    var keyCode = evt.keyCode;
                    if ( keyCode == 37 && _this.get('hasPreviousProfile')
                            && !isInputFocused )
                        _this.send('previousProfile');
                    else if ( keyCode == 39 && _this.get('hasNextProfile')
                            && !isInputFocused )
                        _this.send('nextProfile');
                    else if ( keyCode == 27 )
                        _this.send('hideProfile');
            });
        }
        else {
            this.hideProfileBox();
        }

    }.observes('previewCandidate'),
    setupProfileBox: function () {
        var _this = this;
        $('div.profile-container').scrollTop(0);

        // This sets up any calendar type fields
        // when the field is changed it will update the candidate's attribute given
        // as the name of the field
        // It will display a nice date in the field but convert it to iso for artirix
        var date_field = $('input.date-proxy');
        if ( date_field.length ) {
            date_field.datepicker({
                onSelect: function ( dateStr, date ) {
                    var dateObj = new Date( Date.UTC( date.currentYear, date.currentMonth, date.currentDay ) );
                    _this.get('previewCandidate').set( date_field.attr("name"), dateObj.toISOString() );
                }
            });
            var current_availability_date = this.get('previewCandidate').get( date_field.attr("name") );
            if ( current_availability_date ) {
                date_field.datepicker(
                    "setDate",
                    new Date( current_availability_date )
                );
            }
        }
    },

    positionProfileBox: function () {
        var _this = this;
        // code to setup a standard lightbox which fills the screen
        // correctly and offers the correct scrolling
        $('body').css({ "overflow": "hidden" });
        window.onresize = window.onscroll = function () { _this.positionProfileBox(); };
        $('div.profile-box').css({
            "top": ( $(document).scrollTop() ) + "px"
        });
        $('div.darkness').css({
            "top": $(document).scrollTop() + "px",
            "height": $(window).height() + "px"
        });

        $('div.profile-container').css({
            "top": $(document).scrollTop() + "px",
            "height" : ( $(window).height() - 65 ) + "px"
        });

    },
    hideProfileBox: function () {
        // box hides if there is no candidate being previewed.
        // scroll controls need to be returned to the window
        if( this.get('previewCandidate') ){
            // Empty the history to avoid reloading it on actions.
            this.get('previewCandidate').set('historicActionsChronological', null);
        }
        this.set('previewCandidate',null);
        this.set('showMessageHistory', false);
        $('body').css({ "overflow": "scroll" });
        window.onresize = window.onscroll = null;
    },
    hasNextProfile: function () {
        var cand = this.get('previewCandidate');
        if ( cand !== null ){
            var index = this.get('model').indexOf( cand );
            return ( cand != this.get('model.lastObject') );
        }
        return false;
    }.property('previewCandidate'),
    hasPreviousProfile: function () {
        var cand = this.get('previewCandidate');
        if ( cand !== null ){
            return ( cand != this.get('model.firstObject') );
        }
        return false;
    }.property('previewCandidate'),
    hasTalentSearch: function () {
        return Stream2.Boards.find().isAny('name','talentsearch');
    }.property(),
    onRendered: function () {
        // Don't "activate" the add-tag buttons if the user's viewing the
        // Responses channel and they lack TalentSearch
        if ( this.searchUser.get("restrictUI") ) { return; }

        // Because we are using jquery.dropdown we need a way to subscribe to their "show" hook
        // this gets called once the results have been rendered
        Ember.run.next( this, function () {
            $('.add-tag').siblings('div.dropdown').on('show',function( event ) {
                $( event.target ).find( 'input' ).focus();
                $( event.target ).find( 'input' ).click();
            });
        });
    }.property().volatile(),
    selectedCandidates: function () {
        return this.get('model').filterProperty('isSelected',true);
    }.property('model.@each.isSelected'),
    currencyIcon: function() {
        var currency_icon;
        var user_currency = this.get('controllers.search').get('user').currency;
        var currency_icon_map = {
            'GBP' : 'pound',
            'USD' : 'dollar',
            'AUD' : 'dollar',
            'EUR' : 'euro'
        };
        currency_icon = (typeof user_currency !== "undefined") ? currency_icon_map[user_currency.toUpperCase()] : 'pound';
        return 'icon-' + currency_icon;
    }.property('controllers.search.user.currency'),

    /* Switchoff ALL the action properties in the given namespace.
       The namespace addresses which UI component
       we are considering.
    */
    switchOffShownActionPanels: function(candidate, namespace){
        if( Ember.isEmpty(namespace) ){ namespace = '' }
        candidate.set(namespace + 'showForward' , false);
        candidate.set(namespace + 'showMessage' , false);
        candidate.set(namespace + 'showLongLists' , false);
        candidate.set(namespace + 'showDelegate' , false);
        this.get('controllers.search.customLists').map(function(item){
            candidate.set(namespace + 'showShortlist_' + item.get('name'));
        });
    },

    /* showSaveButton
        Ripple can configure whether the save button is hidden, two types: hide_save controlls
        the save button for all boards and hide_save_internal will control the save button
        for internal boards only
    */
    showSaveButton: Ember.computed(
        'controllers.search.selectedBoard.type',
        function () {
            var shouldHide = this.searchUser.feature_exclusions.hide_save ||
                ( this.searchUser.feature_exclusions.hide_save_internal && this.get('controllers.search.selectedBoard.type') === 'internal' );
            var isEnabled = this.searchUser.settings['candidate-actions-import-enabled'];

            return ( !shouldHide && isEnabled );
        }
    ),

    actions: {
        //
        queryUsertags: function( query, deferred ){
            var term = query.term;

            Stream2.singleRequest(
                '/api/user/suggest_usertag',
                { search: term },
                'GET'
            ).then(
                function ( data ) {
                    deferred.resolve( data.suggestions );
                }
            );
        },

        cancelFutureActionId: function( candidate, action_id ){
            if( ! confirm(i18n.__("Are you sure you want to cancel this action?") ) ){
                return;
            }
            // The user has agreed.
            Stream2.request( candidate.apiURL( 'future_action', action_id ), {},
                             'DELETE'
                           )
                .then(function( data ){
                    Stream2.popupAlert( data.message );
                    // Reload the history
                    if( candidate.get('future_message') &&
                        candidate.get('future_message').action_id == action_id ){
                        candidate.set('future_message', null );
                    }
                    if( candidate.get('historicActionsChronological') ){
                        // The history is there. We should refresh it.
                        candidate.fetchProfileHistory();
                    }
                });
        },
        
        addTag: function ( candidate, tag ) {
            candidate.addTag( tag );
            $('#add-tag-'+candidate.get('idx')).dropdown('hide');
        },
        switchOnForward: function(candidate, namespace){
            if( Ember.isEmpty(namespace) ){ namespace = '' }
            if(!candidate.get('forwardEnabled')) {
                return;
            }
            this.switchOffShownActionPanels(candidate, namespace);
            candidate.set( namespace + 'showForward' , true);
        },
        // In the profile, selecting a tab
        // by clicking on tabs go in there:
        selectTab: function( candidate , tab ){
            candidate.set('selectedTab' , tab);
        },
        switchOnShortlist: function(candidate,list, namespace){
            if ( this.searchUser.get("restrictUI")
                 || !candidate.get('shortlistEnabled') ) {
                return;
            }
            if( Ember.isEmpty(namespace) ){ namespace = '' }
            this.switchOffShownActionPanels(candidate, namespace);
            var propertyName = 'showShortlist_' + list.get('name');
            candidate.set(namespace + propertyName , true);
        },
        switchOnMessage: function(candidate, namespace){
            if( Ember.isEmpty(namespace) ){ namespace = '' }
            if ( candidate.get('block_message') || !candidate.get('messageEnabled'))
                return;
            this.switchOffShownActionPanels(candidate, namespace);
            candidate.set(namespace + 'showMessage' , true);
        },
        switchOnLongLists: function(candidate, namespace){
            if ( this.searchUser.get("restrictUI") ) { return; }
            if( Ember.isEmpty(namespace) ){ namespace = '' }
            this.switchOffShownActionPanels(candidate, namespace);
            candidate.set(namespace + 'showLongLists', true);
        },
        switchOnDelegate: function(candidate, namespace){
            if ( this.searchUser.get("restrictUI") ) { return; }
            if( Ember.isEmpty(namespace) ){ namespace = '' }
            this.switchOffShownActionPanels(candidate, namespace);
            candidate.set(namespace + 'showDelegate', true);
        },
        enhanceCandidate: function(candidate, namespace){
            if( Ember.isEmpty(namespace) ){ namespace = '' }
            candidate.enhance('result');
        },
        toggleEnhancements: function(candidate, namespace){
            if( Ember.isEmpty(namespace) ){ namespace = '' }
            var toggleId = '#' + candidate.get('enhancementToggleId');
            var enhancementBody = $(toggleId).parents('.controls').siblings('.result-body').find('.enhancement-body');
            if (candidate.get('showing_enhancements')) {
                enhancementBody.hide({duration: 300, easing: "linear"});
                candidate.set('showing_enhancements', false);
                $(toggleId).find('i').removeClass().addClass('icon-angle-down');
            }
            else {
                enhancementBody.show({duration: 300, easing: "linear"});
                candidate.set('showing_enhancements', true);
                $(toggleId).find('i').removeClass().addClass('icon-angle-up');
            }
        }
    }
});

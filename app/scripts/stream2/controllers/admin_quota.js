Stream2.AdminQuotaController = Ember.ObjectController.extend({
    queryParams: ['board','type'],
    needs: ['application'],
    board: null,
    _prevBoard: null,
    type: null,
    _prevType: null,
    autoExpand: true,
    loading: true,

    rollover: false,
    resetOption: null,

    availableTypes: function () {
        return [
            { id: 'search-search', label: i18n.__('Search') },
            { id: 'search-cv', label: i18n.__('Download CV') },
            { id: 'search-profile', label: i18n.__('View Profile') },
            { id: 'search-profile-or-cv', label: i18n.__('Download CV or View Profile') }
        ].map( function ( item ) { return Ember.Object.create( item ); } );
    }.property(),

    optionsChanged: function () {
        this.set('loading',true);
        var board = this.get('board');
        var type = this.get('type');
        if ( ! board || ! type ) {
            return;
        }
        if ( board == this.get('_prevBoard') && type == this.get('_prevType') ){
            return;
        }
        this.set('_prevBoard',board);
        this.set('_prevType',type);
        this.updateModel();
    }.observes('board','type'),
    /*
        updateModel

        called each time the user changes the setting drop down
        gets the current state for each user and applies it to the view
    */
    updateModel: function () {
        var _this = this;
        Stream2.request('/api/admin/user_quota/', {}, 'GET', { board: this.get('board'), type: this.get('type') }).then(
            function ( quota_map ){
                _this.set('error', null);
                _this.set('current_quota', quota_map.quota);

                // reset all settings
                _this._resetChildren( _this.get('hierarchy') );
                _this.set('_resetOption', 0);
                _this.set('_rolloverOption', null);
                // in search, all users should have the same global values which are listed below so we should only have
                // to asses the first user
                $.each( quota_map.quota, function ( path, user ) {
                    _this._setReset( user );
                    return 0;
                });

                // foreach leaf/user, set the default
                $.each( _this.get('_userMap').get('paths'), function ( index, path ) {
                    var value = quota_map.quota[path] || {};
                    _this.get('_userMap').get('map')[path].setProperties(value);
                });

                // calculate if the view needs to be expanded or not
                _this._expandChildren( _this.get('hierarchy') );

                _this.set('loading',false);
            },
            function ( error ) {
                _this.set('error', error);
                _this.set('loading',false);
            }
        );
    },
    _expandChildren: function ( dec ) {
        var _this = this;
        var shouldExpand = false;
        if ( dec.get('children') ){
            dec.get('children').forEach( function ( child ) {
                if ( _this._expandChildren( child ) ){
                    shouldExpand = true;
                }
            });
            if ( shouldExpand ) {
                dec.set('showChildren',true);
                return true;
            }
            else {
                dec.set('showChildren',false);
            }
        }
        if ( !Ember.isEmpty(dec.get('remaining')) || !Ember.isEmpty(dec.get('reset_to')) ){
            return true;
        }

        return false;
    },

    _resetOption: 0, // the index of a valid resetOption
    _rolloverOption: null,
    /*
        resetOptions

        The option to reset Quota isn't just a number but must also be
        attributed with periodic designation. Therefor the "value" of our
        select options isn't just a plain old string/int, but we can make
        it as such by assigning a lookup, the value of the select list is a pointer
        to a more complicated value
    */
    _setReset: function ( user ) {
        var _this = this;
        var options = this.get('resetOptions');
        ['reset_dow','reset_interval','reset_dom'].any( function ( key ) {
            var value = _this._intOrNull( user[key] );
            var selected = options.filter( function ( option ) {
                return ( option.value[key] && option.value[key] === value );
            });
            if ( selected.length ){
                _this.set('_resetOption',selected[0].index);
                return true;
            }
        });

        var value = this._intOrNull( user.rollover );
        this.set('_rolloverOption',value);
    },
    _intOrNull: function ( val ) {
        var number = parseInt( val );
        if ( isNaN( number ) ) { return null; }
        return number;
    },
    resetOption: function () {
        var index = this.get('_resetOption');
        if ( ! isNaN( index ) ){
            var option = this.get('resetOptions').findBy('index',this.get('_resetOption'));
            return $.extend({ rollover: this.get('_rolloverOption'), reset_dow: null, reset_dom: null, reset_interval: null }, option.value);
        }
        return { reset_dow: null, reset_dom: null, reset_interval: null, rollover: this.get('_rolloverOption') };
    }.property('_resetOption'),
    resetOptions: function () {
        var options = [
            { label: i18n.__('Never'), value: {}, group: null },
            { label: i18n.__('Daily'), value: { reset_interval: 1 }, group: null },
        ];

            // Day of week
        var __ = i18n.__, dowi = 1;
        [__('Monday'),__('Tuesday'),__('Wednesday'),__('Thursday'),__('Friday'),__('Saturday'),__('Sunday')].forEach ( function ( dow ) {
            options.push( { label: i18n.__x('every {weekday}', { weekday: dow }), value: { reset_dow: dowi++ }, group: '-- ' + i18n.__('Weekly') + ' --' } );
        });

        // Day of month
        for ( var i = 1; i <= 28; i++ ){
            options.push( { label: i18n.__x( 'on day {day_number} of the month', { day_number: i }), value: { reset_dom: i }, group: '-- ' + i18n.__('Monthly') + ' --' } );
        }

        // Last day of the month
        options.push( { label: i18n.__('on the last day of the month'), value: { reset_dom: 31 }, group: '-- ' + i18n.__('Monthly') + ' --' } );

        // each item should have a value which is a pointer to itself so we can do a lookup later of the more complicated value
        var i = 0;
        options.forEach( function ( item ) { item.index = i++; } );

        return options;
    }.property(),

    rolloverOptions: function () {
        return [
            { label: i18n.__('Ignore any unused quota'), value: null },
            { label: i18n.__('Carry over any unused quota'), value: 1 },
        ];
    }.property(),
    /*
        _readChildren

        iterates through hierachy from given level ( e.g. team )
        extracts boolean_value for leaf nodes
    */
    _readChildren: function ( user, quotaChanges ){
        var _this = this;
        if ( Ember.isNone( quotaChanges ) ) quotaChanges = [];

        var oldUser = this.get('current_quota')[user.path] || {};

        var newUser = this._hasChanges( $.extend( user, this.get('resetOption') ), oldUser );

        if ( newUser ) {
            quotaChanges.push( newUser );
        }

        if ( user.children ){
            $.each( user.children, function ( index, child ) {
                _this._readChildren( child, quotaChanges );
            });
        }

        return quotaChanges;
    },
    _resetChildren: function ( user ) {
        user.setProperties({ remaining: null, reset_to: null, error: null });
        if ( user.children ){
            var _this = this;
            $.each( user.children, function ( index, child ) {
                _this._resetChildren( child );
            });
        }
    },
    /*
        _hasChanges
        Iterates over each field in the row and checks for changes
    */
    _hasChanges: function ( user, oldUser ) {
        user.set('error',null);
        var newUser = this._findChanges( user, oldUser, ['remaining','reset_to','reset_dow','reset_dom','reset_interval','rollover'] );

        if ( newUser ) {
            // the user has changed and the changes matter as they have a valid quota setting or
            // the user is having their quota unset so we shouild still send the change
            if ( !Ember.isEmpty(newUser.remaining) || ( Ember.isEmpty(newUser.remaining) && ! Ember.isEmpty(oldUser.remaining) ) ){
                return newUser;
            }
        }

        return false;
    },
    _findChanges: function ( user, oldUser, fields ) {
        var newUser = { path: user.path };
        var hasChanged = false;
        fields.forEach( function ( key ) {
            var oldValue = typeof oldUser[key] !== 'undefined' ? oldUser[key] : ''; // if oldUser[key] will not work due to 0 being false
            var newValue = user[key] || "";

            // all values must be numeric
            if ( isNaN(newValue) ){
                var error = i18n.__x('{user}: {field} has an invalid value', { field: key, user: user.label });
                user.set('error',error);
                throw new Error(error);
            }
            if ( newValue !== oldValue ){
                hasChanged = true;
            }
            if ( ! Ember.isEmpty( newValue ) ){
                newUser[key] = parseInt(newValue);
            }
        });
        return hasChanged ? newUser : false;
    },
    actions: {
        updateUsers: function () {
            var quotaChanges = [];
            this.set('error',null);
            try {
                quotaChanges = this._readChildren( this.get('hierarchy') );
            }
            catch ( e ){
                this.set('error', e);
            }
            if ( quotaChanges.length === 0 ){ return; }
            this.send('postQuota', quotaChanges);
        },
        undoUsers: function () {
            this.set('loading',true);
            this.set('error',null);
            this.updateModel();
        }
    }
});

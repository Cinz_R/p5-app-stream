Stream2.AdminUsersController = Ember.Controller.extend({
    needs: ['application'],
    requestURL: '/api/admin/overseen_users',
    users: [],
    search: function () {
        var _this = this;
        Stream2.singleRequest( this.get('requestURL'), {} , 'GET' ).then(
            function ( data ) {
                _this.set('users', data.overseen_users.map( function ( item ) {
                    return Stream2.AdCourierUser.create( item );
                }) );
            }
        );
    }
});

Stream2.AdCourierUser = Ember.Object.extend({
    office: null,
    consultant: null,
    id: null,
    team: null,
    company: null
});

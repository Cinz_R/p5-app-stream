Stream2.DelegateController = Ember.Controller.extend({
    needs: ['application'],

    delegating: false, // state
    colleague: null,
    outcomes: [],
    formInvalid: function() {
        if (!this.get('colleague')) {
            return true;
        }
    }.property('colleague'),

    actions: {
        queryColleagues: function(query, deferred) {
            var term = query.term;

            Stream2.singleRequest('/api/user/colleagues', {
                search: term,
                excludeCurrentUser: true
            }, 'GET').then(
                function(data) {
                    deferred.resolve(data.suggestions);
                }
            );
        }
    }
});

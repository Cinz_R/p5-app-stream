Stream2.AdminEmailtemplatesNewController = Stream2.AdminEmailtemplatesController.extend({
    needs: ['application'],
    actions: {
        saveTemplate: function() {
            var _this = this;
            Stream2.request('/api/admin/email-templates', _this.templateParams(), 'POST').then(
                function(data) {
                    _this.set('error', null);
                    Stream2.popupAlert(i18n.__("Template saved"), data.message);
                    // rebuild the  email model, breaking out the template_id
                    // is important for routing
                    var templateModel = Ember.Object.create({
                        template_id: data.id,
                        email_template: data,
                        hierarchy: _this.model.hierarchy,
                        _userMap: _this.model._userMap,
                        autoExpand: true
                    });

                    _this.replaceRoute('admin.emailtemplates.edit', templateModel);
                },
                function(error) {
                    _this.set('error', error);
                }
            );
            return false;
        }
    }
});

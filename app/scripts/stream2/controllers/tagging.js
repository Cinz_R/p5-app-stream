Stream2.TaggingController = Ember.Controller.extend({
    needs: ['application'],
    tag: null,
    sendingTags: false, // state
    outcomes: [],
    requestURI: '/api/user/tags',
    formInvalid: function (){
        return ( ! this.get('tag') );
    }.property('tag'),
    tagSuggestions: function () {
        var suggestions = this.get('autocompleteSuggestions');
        if ( suggestions && suggestions.length ) {
            return suggestions.map( function ( item ) {
                return Stream2.TagOption.create( item );
            });
        }
        return [];
    }.property('autocompleteSuggestions')
});

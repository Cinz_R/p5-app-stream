Stream2.SearchController = Ember.Controller.extend( ErrorMixin, {
    needs: ['board_results','application'],

    criteria: null,
    hideMoreOptions: true,
    watchdog: null,
    boards  : [],
    customLists: null,

    onInit: function() {
        Ember.run.schedule('afterRender', this, function() {
            $('#sticky-facet-box').on('sticky-start', function() {
                $('#sticky-facet-box > div#facet-box').addClass('stickyFacetContainer');
            });
            $('#sticky-facet-box').on('sticky-end', function() {
                $('#sticky-facet-box > div#facet-box').removeClass('stickyFacetContainer');
            });
        });
    }.on('init'),

    user    : function() {
        return this.searchUser;
    }.property(),

    quotaGuard: function(cb) {
        // longlist is technically not a board, doesn't require quota guarding.
        if (this.get('selectedBoard.name') === 'longlist') {
            return cb(this);
        }
        if ( !this.get('selectedBoard').get('quota.canSearch') ) {
            Stream2.popupAlert(
                i18n.__('Oops...'),
                i18n.__('No credits available to search this board.')
            );
            return;
        }
        return cb(this);
    },

    withWatchdog: function(code , alternative){
        if(! code ){
            code = function(){ return null; };
        }
        if( ! alternative ){
            alternative = function(){ return null; };
        }

        var watchdog = this.get('watchdog');
        if( watchdog ){
            return code.apply(watchdog);
        }
        return alternative.apply();
    },

    watchdogId: function(){
        return this.withWatchdog(function(){
            return this.get('id');
        });
    }.property('watchdog'),

    hasWatchdog: function(){
        return ! Ember.isNone(this.get('watchdog'));
    }.property('watchdog'),


    hasWatchdogCriteriaChanged: function(){
        var _this = this;
        return _this.withWatchdog(function(){
            // this is the watchdog
            return this.get('criteria_id') != _this.criteria.get('id');
        });
    }.property('watchdog.criteria_id','criteria.id'),

    selectedBoard: function () {
        return this.get('boards').findProperty('selected');
    }.property('boards.@each.selected'),

    expireCurrentResults: function () {
        this.get('selectedBoard').expireResults();
    },

    expireAllResults: function () {
        var _this = this;
        this.get('boards').forEach( function ( board ){
            board.expireResults();
            if ( Ember.isNone( _this.get('selectedBoard') ) ){
                return;
            }
            if ( _this.get('selectedBoard').get('name') != board.get('name') ) {
                board.clearFacets();
            }
        });
    },

    onError: function () {
        window.scrollTo( 0, 0 );
    },

    /* Watchdog related methods */
    // Clears the current watchdog and its counting pills
    clearWatchdog: function(){

        this.set('watchdog', null);
        this.get('boards').forEach( function ( board ){
            board.set('newResults' , null );
        });
    },

    /* Clears the viewed long list and
       make sure a suitable board is selected.
    */
    clearLongList: function(){
        this.set('viewedLongList', null);
        this.set('criteria' , this.get('previousCriteria') );
        this.set('previousCriteria' , null);
        Stream2.Boards.selectFirstSelectable();
    },


    /*
    *******************************************************

        Searching methods

    ********************************************************
    */

    // clean slate
    reset: function () {
        var _this = this;
        if ( this.beforeTransition() ) {
            this.expireAllResults();
            this.get('criteria').clear();
            this.get('criteria').setDefaultsFromUser(this.searchUser);

            this.get('boards').forEach( function ( board ){
                board.expireResults();

                // Set the new results to NULL only if
                // no watchdog is currently loaded.
                _this.withWatchdog(null,function(){
                    board.set('newResults', null);
                });

                board.clearFacets();
            });
            return true;
        }
        return false;
    },

    // Like runNewSearch except only expires current board's results
    facetSearch: function () {
        if ( this.beforeTransition() ) {
            Ember.run.next( this, function () {
                this.refreshBoard();
            });
            return true;
        }
        return false;
    },

    // When you hit the search button
    runNewSearch: function () {
        if ( this.beforeTransition() ){
            return this.quotaGuard(function(controller) {
                controller.resetPage();
                controller.expireAllResults();
                controller.submitSearch();
                return true;
            });
        }
        return false;
    },

    // changing page
    pageSearch: function ( page ) {
        if ( this.beforeTransition() ){
            this.get('selectedBoard').changePage(page);
            return this.quotaGuard(function(controller) {
                controller.submitSearch();
                return true;
            });
        }
        return false;
    },


    // Refresh results on specific board
    refreshBoard: function () {
        this.quotaGuard(function(controller) {
            controller.resetPage();
            controller.expireCurrentResults();
            controller.submitSearch();
        });
    },

    // Before we do anything to change the results shown on the page
    beforeTransition: function () {
        var board_results = this.get('controllers.board_results');
        if ( board_results && board_results.get('selectedCandidates').length ){
            if ( confirm( i18n.__("Your candidate selection will be lost, are you sure?") ) ){
                this.get('selectedBoard').get('_results').setEach( "isSelected", false );
                return true;
            }
            return false;
        }
        return true;
    },

    // Gets rid of some of the previous search meta data
    resetPage: function () {
        var selectedBoard, selectedPage;

        selectedBoard = this.get('selectedBoard');
        if ( !selectedBoard ) return;

        selectedPage = selectedBoard.get('selectedPage');
        if ( !selectedPage ) return;

        selectedPage.set('isSelected', false);
    },

    // ******************************************************

    loadCustomLists: function(){
        var _this = this;
        // if this has already been called, don't call it again
        if ( ! Ember.isNone( this.get('customLists') ) ) return;
        this.set('customLists', []);
        Stream2.request("/api/user/custom_lists", {}, 'GET').then(
            function(data){
                $.each(data, function(index, list_item){
                    _this.get('customLists').pushObject(Stream2.CustomList.create(list_item));
                });
            },
            function(error_string){
                alert("Unable to load custom lists");
            }
        );
    },

    submitSearch: function ( options ) {
        var _this = this;

        if ( ! options ){ 
            options = {}; 
        }

        this.set('error',null);

        if( this.searchUser.settings['behaviour-search-use_ofccp_field'] ){
            if( Ember.isEmpty( this.get('criteria').oneValue('ofccp_context' )) ){
                this.throwManagedError(i18n.__('You must fill in the OFCCP Context field'));
                return;
            }
        }

        this.set('searchHasRun',true);

        Stream2.popupAlert(
            i18n.__('Getting your results...'),
            i18n.__('Just grabbing my coat, I will be back shortly.')
        );

        Stream2.resetMark();

        var board = this.get('selectedBoard');
        if ( board.get('id') !== 'adcresponses' && this.searchUser.get('manageResponsesView') ){
            var responseBoard = Stream2.Boards.findById( 'adcresponses' );
            if ( responseBoard ){
                this.changeSelectedBoard( responseBoard );
                board = responseBoard;
            }
            else {
                return this.throwManagedError(
                        i18n.__("It appears that the new manage responses is not available in your account") );
            }
        };

        var selected_page = board.get('selectedPage') ? board.get('selectedPage').get('id') : 1;
        var new_criteria = this._newCriteria();

        if ( board.get('hasOldResults') || this.get("criteria").get("isDirty") ) {

            // This is what make the search search
            // from a serialized criteria, an optional watchdog and the current searchUser.
            board.search( new_criteria.serialise() , this.get('watchdog') , this.get('searchUser') ).then(
                function ( data ){
                    if ( data.error ){
                        _this.throwUnmanagedError( data.error );
                    }
                    else{
                        new_criteria.set( 'id', data.criteria_id );
                        _this.set('criteria', new_criteria);
                        _this.preLoadCriteria( data );
                        if ( options.noHistory ){
                            _this.replaceRoute( 'board.results', new_criteria, // replaceRoute replaces the history of the current page - for 'back button' purposes
                            board,
                            { status_id: data.status_id, id: selected_page } );
                        }
                        else{
                            _this.transitionToRoute( 'board.results', new_criteria,
                            board,
                            { status_id: data.status_id, id: selected_page } );
                        }
                    }
                },
                function ( error ){
                    if ( error.reason ) {
                        _this.throwManagedError( error.reason );
                    }
                    else{
                        _this.throwUnmanagedError( error );
                    }
                }
            );
        }
        else {
            // if the results are still current, no reason to run a new search
            _this.transitionToRoute( 'board.results', _this.get('criteria'),
                                                      board,
                                                      { results_id: board.get('results_id'), id: selected_page } );
        }
    },

    /*  unmanagedError

        In the event of an unmanaged error, display something neutral to the user
        while providing means to forward the specifics to support

            searchController.throwUnmanagedError('Terrible stack trace mess');

        Since errors used to be just plain strings, there is a type check
        for backwards compatability
    */
    unmanagedError: false,
    throwUnmanagedError: function ( error ) {
        if ( typeof error === "string" ) {
            error = { "message": error };
        }
        this.set('error', error);
        this.set('unmanagedError', true);
    },

    /* throwManagedError

        When we want to validate the search form before performing a search,
        this can be used to show an error message with the failure reason
        straight away.

        Note: errors should be Objects with the following structure:
        { message: "error occurred", properties: { type: "login" } }

        Since errors used to be just plain strings, there is a type check
        for backwards compatability
    */
    throwManagedError: function ( error ) {
        if ( typeof error === "string" ) {
            error = { "message": error };
        }
        this.set('error', error);
    },

    errorCleared: function (){
        if ( ! this.get('error') ){
            this.set('unmanagedError', false);
        }
    }.observes('error'),

    changeBoardByName: function ( boardName ) {
        var board = Stream2.Boards.findById( boardName );
        if ( board ) this.changeSelectedBoard( board );
    },
    changeSelectedBoard: function ( board ) {
        if ( this.get('selectedBoard') ) this.get('selectedBoard').set('selected',false);
        board.set('selected', true);
    },

    selectedBoardChanged: function(){
        var board = this.get('boards').findProperty('selected');
        if( ! board ){ return ; }

        // Run next. Give a chance to the GUI to display some stuff before
        // doing any swoop swooping
        Ember.run.next( this , function(){
            // console.log("New selected is " , board );
            if( board.get('isHidden') ){
                // Make sure there can be no watchdog displayed concurrently with
                // a hidden board (like a longlist).
                this.clearWatchdog();
                $('div#search-main-control').slideUp();
                $('div#search-side-control').hide();
            }else{
                // Make sure we clear any viewedLongList (a long list viewing is not an hidden thing)
                this.set('viewedLongList' , null);
                $('div#search-main-control').slideDown();
                $('div#search-side-control').show();
            }
        });
    }.observes('boards.@each.selected'),

    preLoadCriteria: function ( data ) { // gets called when the resumeSearch method finishes
        var _this = this,
            board = this.get('selectedBoard'),
            criteria = this.get('criteria');

        var cb = function () {
            Stream2.pushEvent('facet','click');
            return _this.facetSearch();
        };

        if ( data.tokens ) {
            board.facetPopulate( data.tokens, cb );
        }

        if ( data.sort_by_token ) board.setSortBy( data.sort_by_token, cb );

        // search terms are a word breakdown of the keywords for use with highlighting
        // should be stored in the current criteria object so it is properly disregarded after a new search
        // but it should not be of the criteria
        if ( data._search_terms ) criteria.set('_search_terms', data._search_terms);

        // Set the defaults dynamically according to the user defaults
        // Before deserializing.
        // This will fix https://broadbean.atlassian.net/browse/SEAR-699
        criteria.setDefaultsFromUser(this.searchUser);

        if ( data.criteria ) criteria.deserialise( data.criteria );

        if ( criteria.hasChangedCriteria() ) { this.set('hideMoreOptions', false); }
    },

    // _newCriteria
    // When we run a search we pass the current criteria down the chain so it populates the URL with the criteria ID
    // This is nice as people can then save their URL or share it etc so its quite important that we can offer this
    // criteria as a whole is split into two parts...
    //
    //  Global criteria ( this.get('criteria') )
    //  This holds the main criteria, keywords, location, etc.
    //
    //  Board criteria ( board.get('facets') )
    //  Each board takes care of its own specific criteria
    //
    //  This object is always changing and as soon as it does change it becomes a whole new search
    //  It is important then that we create a new ember object to reflect this change
    _newCriteria: function () {

        // get the current global criteria
        // this will contain all criteria including board specific criteria
        var criteria = {};
        if ( this.get('criteria') ){
            criteria = this.get('criteria').serialise();
        }
        else {
            // if we are to create a brand new criteria object
            // then we should set the defaults of the user
            criteria.distance_unit = this.searchUser.get('distance_unit');
            criteria.salary_cur = this.searchUser.get('currency');
            criteria.cv_updated_within
                = this.searchUser.get("manageResponsesView")
                ? "ALL"
                : this.searchUser.get('settings.criteria-cv_updated_within-default');
        }

        // Merge the global criteria with the current facet state of the board
        var board = this.get('selectedBoard');
        if ( board && board.get('hasFacets') ){
            var facets = board.serialiseFacets();
            for ( var facet in facets ) {
                criteria[facet] = facets[facet];

                // we only really want to make this check with board facets
                if ( Ember.isEmpty( criteria[facet] ) ) delete( criteria[facet] );
            }
        }

        return Stream2.Criteria.create( criteria );
    },

    /*
      Called by the route board_results.js with the context returned by the last polling
    */
    watchdogViewed: function ( context ) {
        var wd = this.get('watchdog');
        if( ! wd ){
            return;
        }

        var newWdSubscriptionCount = context.result.new_wd_subscription_count;
        if( Ember.isNone(newWdSubscriptionCount) ){
            throw("Missing new_wd_subscription_count in context.result");
        }

        var searchedBoard = this.get('selectedBoard');
        var wd_board = wd.get('boards').findBy('name',searchedBoard.get('name'));
        if ( wd_board ) {
            // Set the new count agains the viewed board and
            // also in the watchdog board.
            searchedBoard.set('newResults' , newWdSubscriptionCount);
            wd_board.set('count',  newWdSubscriptionCount );
        }

    },
    isWatchdogBoard: function ( board ) {
        var wd = this.get('watchdog');
        if ( wd ){
            return !! wd.get('boards').findBy('name',board.get('name'));
        }
        return false;
    },
    showSelectedAdverts: function () {
        return ( this.get('selectedBoard.name') === 'adcresponses' );
    }.property('selectedBoard'),

    actions: {
        toggleSnippet: function () {
            this.get('controllers.board_results').toggleProperty('showSnippet');
        },
        /* This will be called when clicking on the 'Mark all as read' action per board */
        clearWatchdogBoardResults: function(board){
            var wd = this.get('watchdog');
            if( ! wd ){ return false; }

            if( ! confirm( i18n.__("Are you sure you want to mark all those board watchdog's results as read?") ) ){
                return false;
            }

            var wd_board = wd.get('boards').findBy('name',board.get('name'));
            if( wd_board ){

                Stream2.popupAlert( i18n.__("Marking this board's watchdog results as read") );

                Stream2.resetMark();
                Stream2.requestAsync('/api/watchdog/' + wd.get('id') + '/view/board/' + board.get('name') + '/viewed_results', {}, 'DELETE')
                    .then(function( data ){
                        Stream2.timeMarkEvent('Watchdogs','reset board result');
                        Stream2.popupAlert( data.message );
                        wd_board.set('count', 0);
                        board.set('newResults' , 0 );
                    });

            }
            return false;
        },

        // Closes the current watchdog and reinstantiate the
        // search interface.
        closeWatchdog: function(){
            if( this.get('hasWatchdogCriteriaChanged') ){
                if( ! confirm( i18n.__("Are you sure you want to leave this watchdog without saving your criteria changes?") ) ){
                    return;
                }
            }
            this.clearWatchdog();
            this.quotaGuard(function(controller) {
                controller.expireAllResults();
                controller.submitSearch();
            });
        },

        submit: function () {
            // Big <indeterminate colour> submit button
            Stream2.pushEvent('search','submit');
            this.runNewSearch();
        },

        load_saved_search: function(savedSearchObj){
            // They have chosen a search from their "saved searches" drop-down
            // hide the drop down
            $('#saved-searches-dropdown').dropdown('hide');

            // if they have candidates selected, confirm before transition
            if ( ! this.beforeTransition() ) return;

            // set the current criteria ID to that of the saved search
            var criteria_id = savedSearchObj.get('criteria_id');

            // if it's already loaded, don't continue
            if ( this.get('criteria.id') == criteria_id ){
                return;
            }

            this.expireAllResults();

            this.transitionTo('board.results', criteria_id, this.get('selectedBoard.name'), 1 );
            Stream2.pushEvent('search','load_saved_search');
        },

        changeBoard: function( board ) {
            if (!board.get('quota.canSearch')) {
                Stream2.popupAlert(
                    i18n.__('Oops...'),
                    i18n.__('No credits available to search this board.')
                );
                return;
            }
            Stream2.pushEvent('search','changeBoard');
            if ( ! this.beforeTransition() ) return;
            this.changeSelectedBoard(board);
            Ember.run.next( this, function () {
                this.quotaGuard(function(controller) {
                    controller.submitSearch();
                });
            });
        },
        clearSearch: function () {
            Stream2.pushEvent('search','clearSearch');
            if ( this.reset() ){
                this.quotaGuard(function(controller) {
                    controller.submitSearch();
                });
            }
        },
        clearLongList: function(){
            Stream2.pushEvent('longlist','return to search');
            this.clearLongList();
            // Submitting search again. The clearLongList
            // would have set the criteria with the previous one.
            this.submitSearch();
        },

        // Saves the current criteria as the new watchdog criteria
        watchdogSave: function(){
            var _this = this;
            _this.withWatchdog(
                function(){
                    var watchdog = this;

                    // ALWAYS build a new criteria ID from the current criteria
                    var criteria = _this._newCriteria();

                    Stream2.request('/api/create_criteria' , { criteria : criteria.serialise() } , 'POST' ).then(
                        function(success){
                            Stream2.request('/api/watchdog/' + watchdog.get('id') + '/criteria_id',
                                            { criteria_id: success.data.id }, 'POST').then(
                                                function(watchdog_data){
                                                    Stream2.popupAlert(
                                                        i18n.__('Saving watchdog criteria'),
                                                        i18n.__x('Watchdog \'{name}\' criteria updated', { name: watchdog.get('name') }) );
                                                    watchdog.set('criteria_id', watchdog_data.criteria_id );
                                                    // This will force a reload of the criteria
                                                    var criteria = Stream2.Criteria.create( watchdog_data.criteria );
                                                    watchdog.set('criteria', criteria );
                                                    // Transition to editing the watchdog settings
                                                    _this.transitionToRoute('watchdogs.edit' , watchdog.get('id'));
                                                }
                                            );
                        }
                    );
                }); // End of with watchdog
        } // end of watchdogSave
    } // end of actions
});

Stream2.SearchView = Ember.View.extend({
    didInsertElement: function () {
        this.applyExpandable();
        Ember.run.schedule('afterRender', this, function () {
            $('div#sticky-facet-box').sticky({
                topSpacing: 0,
                responsiveWidth: true,
                getWidthFrom: 'div.facetContainer'
            });
        });
    },
    applyExpandable: function () {
        var _this = this;
        $('textarea.keywords').each( function () {
            $(this).on('keyup', function ( ev ) {
                // pressing enter ( without the shift key ) should run a search
                if ( ev.keyCode === 13 && ! ev.shiftKey ){
                    _this.get('controller').runNewSearch();
                    return false;
                }

                this.style.height = "";
                this.style.height = ( Math.min(this.scrollHeight, 500) - 8 ) + "px";
            });
            $(this).on('keypress', function ( ev ) {
                // pressing enter ( without the shift key ) should not enter a character
                if ( ev.keyCode === 13 && ! ev.shiftKey ){
                    return false;
                }
            });
            $(this).keyup();
        });
    }
});

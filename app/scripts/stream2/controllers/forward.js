Stream2.ForwardController = Ember.Controller.extend({
    needs: ['application'],
    forward: null,
    forwardingCVs: false,

    recipients: [],
    messageContent: '',

    sendAsBulk: false,

    outcomes: [],
    formInvalid: function (){
        // console.log(this.get("recipients"));
        if (
            ! this.get('selectedCandidates') ||
            Ember.isEmpty( this.get('recipients') )
        ) return true;
    }.property('selectedCandidates' , 'recipients'),
    hasRecipientDomainRestrictions: Ember.computed.bool('recipientDomainRestrictions'),
    recipientDomainRestrictions: Ember.computed('searchUser.settings.forwarding-restrict_recipient_domain',function () {
        return this.searchUser.get('settings.forwarding-restrict_recipient_domain');
    }),
    actions: {
        queryRecipients: function(query, deferred){
            var term = query.term;
            // check to see if the user is forwarding from the responses channel
            var isInResponses = this.get('selectedCandidates.0').get('destination') == 'adcresponses';
            Stream2.singleRequest( '/api/user/colleagues', { search: term, isResponses: isInResponses } , 'GET' ).then(
                function ( data ) {
                    deferred.resolve(data.suggestions);
                    // [ { id: term.length , text: term + 'BLA'} ]);
                });
        }
    }
});

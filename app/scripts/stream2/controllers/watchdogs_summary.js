Stream2.WatchdogsSummaryController = Ember.ArrayController.extend({
    needs: ['application'],
        actions: {
            unsaveWatchdog: function(watchdog){
                var _this = this;
                if( confirm(i18n.__('Are you sure?')) ){
                    Stream2.request('/api/watchdogs' , { id: watchdog.get('id')  } , 'DELETE' ).then(
                        function(data){
                            Stream2.Watchdogs.reset();
                            Stream2.popupAlert(i18n.__('Success'), data.message );
                        },
                        function(error){
                            Stream2.popupError(error);
                        }
                    );
                }
            }
        }
});

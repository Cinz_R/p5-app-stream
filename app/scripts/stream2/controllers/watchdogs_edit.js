Stream2.WatchdogsEditController = Ember.Controller.extend( ErrorMixin, {

    needs: ['application'],

    formInvalid: function(){
        if( ! this.get('watchdog') || ! this.get('watchdog').get('name') ){
            return true;
        }
        if( ! this.get('boards').filterBy('selected').length ){
            return true;
        }

        return false;

    }.property('watchdog.name', 'boards.@each.selected'),

    actions: {
        selectBoard: function ( board ) {
            board.set('selected', ! board.get('selected') );
        },
        updateWatchdog: function(){
            var _this = this;
            var selectedBoards = this.get('boards').filterBy('selected').map( function ( board ) { return board.get('name'); } );
            var name = this.get('watchdog').get('name');

            Stream2.request(
                '/api/watchdog/' + _this.get('watchdog').get('id'),
                {
                    name: name,
                    boards: selectedBoards
                },
                'POST'
            ).then(
                function ( data ) {
                    Stream2.popupAlert(i18n.__('Updated Watchdog'),i18n.__x('Watchdog \'{name}\' settings updated successfully.', {name:name}));
                    Stream2.Watchdogs.reset();
                    _this.get('controllers.application').set('watchdogs', Stream2.Watchdogs.find() );
                    _this.transitionToRoute('watchdogs.summary');
                },
                function( error ){
                    _this.set('error' , error);
                }
            );
        }
    },

    onError: function ( error ) {
        Stream2.popupError(error);
    }
});

/**
 * Blockly javascript components for stream2. All in one file (for now).
 *
 * Note that the full extend of Language::LispPerl with the addition of Stream2::Lisp::Core is not implemented here.
 * We'll add stuff as we need.
 * @namespace Blockly
 * @class Blocks.s2
 */

// 1 - The blocks
Blockly.Blocks.s2 = {};
Blockly.Blocks.s2.HUE = 110;

Blockly.Blocks['s2_do_later'] = {
    init: function() {
        this.appendValueInput("MINUTES")
            .setCheck("Number")
            .appendField("After");
        this.appendDummyInput()
            .appendField("minutes");
        this.appendStatementInput("DO")
            .setCheck(null)
            .appendField("Do this");

        this.setInputsInline(true);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        
        this.setColour( Blockly.Blocks.s2.HUE );
        this.setTooltip('Postpone the execution of the given block after the given amount of minutes');
    }
};


Blockly.Blocks['s2_user_in_office'] = {
    init: function() {
        this.setColour( Blockly.Blocks.s2.HUE );
        this.setOutput(true, 'Boolean');

        this.appendValueInput('OFFICE')
            .setCheck('String')
            .appendField('User in Office');
        this.setTooltip('Is the current user in the given office?');
    }
};

Blockly.Blocks['s2_user_in_team'] = {
    init: function() {
        this.setColour( Blockly.Blocks.s2.HUE );
        this.setOutput(true, 'Boolean');

        this.appendValueInput('TEAM')
            .setCheck('String')
            .appendField('User in Team');
        this.setTooltip('Is the current user in the given team?');
    }
};

Blockly.Blocks['s2_response_is_being_flagged'] = {
    s2_options: [], // To be injected by the blockly-program.js component.
    init: function(){
        this.setColour( Blockly.Blocks.s2.HUE );
        this.setOutput(true, 'Boolean');
        this.setTooltip('Is this response being flagged with the given flag?');

        this.appendDummyInput()
            .appendField('Response is flagged with')
            .appendField(new Blockly.FieldDropdown(
                Blockly.Blocks['s2_response_is_being_flagged'].s2_options
            ), 'FLAGID');
    }
};

Blockly.Blocks['s2_candidate_is_response'] = {
    init: function() {
        this.setColour( Blockly.Blocks.s2.HUE );
        this.setOutput(true, 'Boolean');
        this.appendDummyInput('ISRESPONSE').appendField("Is a response");
        this.setTooltip('Is the current candidate a response?');
    }
};

Blockly.Blocks['s2_send_email_template'] = {
    "s2_options": [], // To be injected by the the blockly-program.js component.
    // Note that init is called everytime a block is 'built'
    // which is when the block menu is shown or the block cloned and
    // dragged to the workspace.
    init: function() {
        this.setColour( Blockly.Blocks.s2.HUE );
        this.setPreviousStatement(true);
        this.setNextStatement(true);

        this.setTooltip('Send the given email template to the current candidate');

        this.appendDummyInput()
            .appendField('Send email template')
            .appendField(new Blockly.FieldDropdown(
                Blockly.Blocks['s2_send_email_template'].s2_options
            ), 'TEMPLATEID');
    }
}

Blockly.Blocks['s2_send_candidate'] = {
    init: function() {
        this.setColour( Blockly.Blocks.s2.HUE );
        this.setPreviousStatement(true);
        this.setNextStatement(true);

        this.appendValueInput('VALUE')
            .setCheck('String')
            .appendField('Send candidate to URL');
        this.setTooltip('Send the current candidate to the given URL');
    }
};

Blockly.Blocks['s2_forward_candidate'] = {
    init: function() {
        this.setColour( Blockly.Blocks.s2.HUE );
        this.setPreviousStatement(true);
        this.setNextStatement(true);

        this.appendValueInput('VALUE')
            .setCheck('String')
            .appendField('Forward candidate to email address');
        this.setTooltip('Forward the current candidate to the given email address');
    }
};

Blockly.Blocks['s2_do_action'] = {
    init: function() {
        this.setColour( Blockly.Blocks.s2.HUE );
        this.setPreviousStatement(true);
        this.setNextStatement(true);

        this.appendDummyInput()
            .appendField('Perform Action')
            .appendField(new Blockly.FieldDropdown([
                ['Import Candidate','ImportCandidate']
            ]), 'VALUE');
    }
};

// 2 - The language itself ( https://metacpan.org/pod/Language::LispPerl )

Blockly.LispPerl = new Blockly.Generator('LispPerl');
Blockly.LispPerl.addReservedWords('.,ns,list,car,cdr,cons,length,append,type,meta,fn,apply,eval,require,def,set!,let,defmacro,if,while,begin,!,eq,ne,equal');
Blockly.LispPerl.init = function(workspace) {};
Blockly.LispPerl.finish = function(code) { return code; };

/**
 * Encode a string as a properly escaped Lisp string, complete with
 * quotes.
 * @method quote_
 * @param {string} string Text to encode.
 * @return {string} JavaScript string.
 * @private
 */
Blockly.LispPerl.quote_ = function(string) { // Quote for perllisp
    string = string.replace(/\\/g, '\\\\')
        .replace(/\n/g, '\\\n')
        .replace(/"/g, '\\\"');
    return '"' + string + '"';
};

/**
 * Common tasks for generating LispPerl from blocks.
 * Handles comments for the specified block and any connected value blocks.
 * Calls any statements following this block.
 * @method scrub_
 * @param {!Blockly.Block} block The current block.
 * @param {string} code The LispPerl code created for this block.
 * @return {string} LispPerl code with comments and subsequent blocks added.
 * @private
 */
Blockly.LispPerl.scrub_ = function(block, code) {
    var commentCode = '';
    // Only collect comments for blocks that aren't inline.
    if (!block.outputConnection || !block.outputConnection.targetConnection) {
        // Collect comment for this block.
        var comment = block.getCommentText();
        comment = Blockly.utils.wrap(comment, 50);
        if (comment) {
            if (block.getProcedureDef) {
                // Use a comment block for function comments.
                commentCode += ';;\n' +
                    Blockly.LispPerl.prefixLines(comment + '\n', '; ') +
                    ' ;;\n';
            } else {
                commentCode += Blockly.LispPerl.prefixLines(comment + '\n', ';; ');
            }
        }
        // Collect comments for all value arguments.
        // Don't collect comments for nested statements.
        for (var i = 0; i < block.inputList.length; i++) {
            if (block.inputList[i].type == Blockly.INPUT_VALUE) {
                var childBlock = block.inputList[i].connection.targetBlock();
                if (childBlock) {
                    var comment = Blockly.LispPerl.allNestedComments(childBlock);
                    if (comment) {
                        commentCode += Blockly.LispPerl.prefixLines(comment, ';; ');
                    }
                }
            }
        }
    }
    var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
    var nextCode = Blockly.LispPerl.blockToCode(nextBlock);
    return commentCode + code + nextCode;
};

// The generator bits for the language:
Blockly.LispPerl['controls_if'] = function(block) {
    // If/else condition. Notice there is no ifelse
    var n = 0;
    var  conditionCode, trueCode, falseCode;

    conditionCode = Blockly.LispPerl.valueToCode(block, 'IF' + n ) || 'nil';
    trueCode = Blockly.LispPerl.statementToCode(block, 'DO' + n);
    if (block.getInput('ELSE')) {
        falseCode = Blockly.LispPerl.statementToCode(block, 'ELSE');
    }
    // Note that true code and falseCode can consist of several statements.
    // Several statements are bound by a begin in lisp.
    return '(if ' + conditionCode + '\n ' + ( trueCode ? '(begin ' + trueCode + ')' : 'nil' ) + '\n'
        + ( falseCode ? ' (begin ' + falseCode + ')' : '' ) + ')\n';
};
Blockly.LispPerl['logic_operation'] = function(block){
    var operator = block.getFieldValue('OP') == 'AND' ? 'and' : 'or';
    var argument0 = Blockly.LispPerl.valueToCode(block, 'A') || 'nil';
    var argument1 = Blockly.LispPerl.valueToCode(block, 'B') || 'nil';
    return [ '('+ operator + ' ' + argument0 + ' ' + argument1 + ')' ];
};

Blockly.LispPerl['text'] = function(block) {
    // Text value.
    return [ Blockly.LispPerl.quote_(block.getFieldValue('TEXT')) ];
};

Blockly.LispPerl['math_number'] = function(block) {
    return [ block.getFieldValue('NUM') ];
}

Blockly.LispPerl['s2_do_later'] = function(block) {
    var minutes = Blockly.LispPerl.valueToCode(block, 'MINUTES' ) || 0;
    var theCode = Blockly.LispPerl.statementToCode(block, 'DO');
    return '(s2#do-later ' + minutes + ' (begin ' + theCode + '))\n';
};

Blockly.LispPerl['s2_user_in_office'] = function(block) {
    var argument0 = Blockly.LispPerl.valueToCode(block, 'OFFICE') || '""';
    return [ '(s2#user-in-office ' + argument0 + ')' ];
};
Blockly.LispPerl['s2_user_in_team'] = function(block) {
    var argument0 = Blockly.LispPerl.valueToCode(block, 'TEAM') || '""';
    return [ '(s2#user-in-team ' + argument0 + ')' ];
};

Blockly.LispPerl['s2_response_is_being_flagged'] = function(block){
    return [ '(s2#response-is-being-flagged ' + Blockly.LispPerl.quote_( block.getField('FLAGID').getValue() ) + ')' ];
};

Blockly.LispPerl['s2_candidate_is_response'] = function(block) {
    return [ '(s2#candidate-is-response)' ];
};

Blockly.LispPerl['s2_send_candidate'] = function(block) {
    var argument0 = Blockly.LispPerl.valueToCode(block, 'VALUE' ) || '""';
    return '(s2#send-candidate ' + argument0 + ')';
};

Blockly.LispPerl['s2_forward_candidate'] = function(block) {
    var argument0 = Blockly.LispPerl.valueToCode(block, 'VALUE' ) || '""';
    return '(s2#forward-candidate ' + argument0 + ')';
};

Blockly.LispPerl['s2_send_email_template'] = function(block) {    
    return '(s2#send-email-template ' + Blockly.LispPerl.quote_( block.getField('TEMPLATEID').getValue() ) + ')';
};

Blockly.LispPerl['s2_do_action'] = function(block) {
    return '(s2#do-action ' + Blockly.LispPerl.quote_( block.getField('VALUE').getValue() ) + ')';
};


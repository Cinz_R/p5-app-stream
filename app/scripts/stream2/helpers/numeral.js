Ember.Handlebars.registerBoundHelper('formatInteger', function(integerNumber) {
    return numeral(integerNumber).format('0,0');
});

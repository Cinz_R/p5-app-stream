Stream2.QuotaSummaryComponent = Ember.Component.extend({
    didInsertElement: function() {
        Ember.run.schedule('afterRender', this, function() {
            $('div#stickyquotabar').sticky({
                topSpacing: $('div#stickybanner-sticky-wrapper').height(),
                zIndex: 100,
            });
        });
    }
});

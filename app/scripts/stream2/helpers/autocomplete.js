// These three blocks are parts of a re-usable auto-complete text field
// When difining you can specify the URL and the valueBinding, this view takes care of the rest
Stream2.AutoCompleteFieldComponent = Ember.TextField.extend({
    _xhr : null,
    _timer: null,
    _prevVal: null,

    // Settings. You can set these from the HTML (hbs):
    requestURL: null, // Where to send ajax request
    delayTimer: 1000, // Time between the GUI event and the actual search
    autocompMinLength: 3, // Min lenght of the string typed before triggering a search.
    allowVoidSearch: false, // Allow this view to trigger its autocomplete just on clicking the field.

    options: [],
    selectedVal: null,

    suggestionList: null,
    noMatch: false,
    lastMatch: null,
    _getSuggestionListView : function () {
        return Stream2.SuggestionListComponent.create();
    },
    showSuggestionList : function ( options ) {
        var suggestionList = this.get('suggestionList');

        // One is already there. It has to go.
        if( suggestionList ){
            suggestionList.remove();
        }

        // Then we replace it with a new one.
        suggestionList = this.createChildView(this._getSuggestionListView());
        this.set('suggestionList', suggestionList);

        if(suggestionList.loadOptions( options )) {
            var position = this.$().offset();
            suggestionList.set('styleOptions', {
                position: "absolute",
                top:      position.top + this.$().outerHeight() + "px",
                left:     position.left,
                width:    (this.$().outerWidth()-2) + "px"
            });
            suggestionList.appendTo('body');
            this.$().removeClass(this.get('searchingClass'));
        }
    },
    _clearAjax: function (){
        if ( this.get('_xhr') !== null ){
            this.get('_xhr').abort();
        }
    },
    _clearTimeout: function () {
        if ( this.get('_timer') !== null ){
            Ember.run.cancel(this.get('_timer'));
        }
    },
    didInsertElement: function () {
        // when the page loads, it seems the value of this is only populated at the very last second
        // which means the observer for the change method picks up the fact the value has changed and goes to look for suggestions
        // the docs say I should be able to schedule this after the "action" queue and all should be well,
        // I have tried all queues and none worked so here we are
        Ember.run.later( this, function(){
            if ( ! this.isDestroyed ) {
                this.set('_prevVal', this.get('value'));
                this.addObserver('value', this, 'change');
            }
        }, 500);
    },
    _suggestionRequest: function(requestURL, searchKeyword) {
        var _this = this;
        Stream2.request(requestURL, { search: searchKeyword }, 'GET' ).then(
            function ( data ) {
                _this.showSuggestionList( data.suggestions );
            }
        );
    },
    _getSuggestion: function () {
        this._suggestionRequest(this.get('requestURL'), this.get('value'));
    },
    change: function () {
        var newValue = this.get('value');

        if( Ember.isEmpty(newValue) ) {
            this.clearField();
            return;
        }

        if ( newValue == this.get('_prevVal') ) {
            // this has been called without a real change
            return;
        }

        // Avoid autocompleting values shorter than the setting.
        if( newValue.length < this.get('autocompMinLength') ){
            return;
        }

        this._clearAjax();
        this._clearTimeout();
        this.$().addClass(this.get('searchingClass'));
        var _this = this;
        this.set('_timer', Ember.run.later( this, function (){ _this.set('_prevVal',_this.get('value'));_this._getSuggestion(); }, this.get('delayTimer') ) );
    },
    optionChoose: function ( item ) {
        this.set('selectedVal', item.value);
        this.set('_prevVal', item.label);
        this.set('value', item.label);
        this.$().removeClass(this.get('searchingClass'));
        this.$().addClass(this.get('selectedClass'));
    },
    clearField: function() {
        this.$().removeClass(this.get('selectedClass'));
        this.set('selectedVal', '');
        this.set('_prevVal', '');
        this.set('value', '');
    },
    click: function () {
	this.clearField();
        // Autocomplete with nothing.
        if( this.get('allowVoidSearch' ) ){
            // It's like a change but without the stuff about
            // previous value and minimal length.
            this._clearAjax();
            this._clearTimeout();
            this.$().addClass(this.get('searchingClass'));
            var _this = this;
            this.set('_timer', Ember.run.later( this, function (){ _this.set('_prevVal',_this.get('value'));_this._getSuggestion(); }, this.get('delayTimer') ) );
            return;
        }
    }
});
Stream2.SuggestionListComponent = Ember.ContainerView.extend({
    tagName: "div",
    classNames: "autocomplete_hint",
    attributeBindings: ['tabindex'],
    tabindex: -1,
    didInsertElement: function () {
        var _this = this;
        this.$().css( this.get('styleOptions') );
        // Remove on blur.
        this.$().blur( function () { _this.focusOut(); } );
        this.$().focus();
    },
    focusOut: function () {
        this.remove();
    },
    selected: function () {
        this.remove();
    },
    _loadOptions: function ( options ) {
        var _this = this;
        options.forEach( function ( item ) {
            item.onclick = function () { _this.get('parentView').optionChoose(item); };
            var sugCont = Ember.Object.create(item);
            var sugOpt = { controller: sugCont };
            var childView = _this.createChildView( Stream2.SuggestionOptionsComponent, sugOpt );
            _this.pushObject( childView );
        });
        return true;
    },
    loadOptions: function ( options ){
        var _this = this;
        if( options.length < 1 ){
            var sugg = Ember.Object.create({
                label: '- Not found -',
                value: undefined,
                onclick: function(){
                    // Focus. Let the user fix his non matching string.
                    _this.get('parentView').$().focus();
                }
            });

            _this.pushObject(
                _this.createChildView( Stream2.SuggestionOptionsComponent , { controller: sugg })
            );
            return false;
        }
        return this._loadOptions(options);
    }
});

Stream2.SuggestionOptionsComponent = Ember.Component.extend({
    classNames: "clickable",
    templateName: 'autocomplete-suggestion-option',
    didInsertElement: function () {
        var _this = this;
        // Why aren't these normal events? I don't know, why don't you ask ember?
        // putting them as actions doesn't seem to work
        this.$().css({ cursor: "pointer" });
        this.$().hover( function () { if ( _this.$() ) _this.$().addClass("selected"); },
                        function () { if ( _this.$() ) _this.$().removeClass("selected"); } );
        this.$().click( function () { _this.get('controller').onclick(); _this.get('parentView').selected(); } );
    }
});

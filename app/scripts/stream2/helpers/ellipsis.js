Ember.Handlebars.registerBoundHelper('ellipsis', function(string,maxLength) {
    if( string.length > maxLength ){
        return string.substr(0, maxLength -1 ) + '\u2026';
    }
    return string;
});

// You can only have one Router for the whole app
Stream2.Router.map(function() {
    this.route('welcome-admin');
    this.resource('search', function () {
      this.resource('criteria', { path: ':criteria_id' }, function () {
          this.resource('board', { path: ':board_id' }, function () {
              this.resource('shortlist', { path: 'shortlist/:custom_list_id' } , function () {
                  this.route('candidate', { path: ':candidate_id' } );
              });
              this.resource('message', function () {
                  this.route('candidate', { path: ':candidate_id' } );
              });
              this.resource('forward',function(){
                  this.route('candidate', { path: ':candidate_id' } );
              });
              this.resource('delegate',function(){
                  this.route('candidate', { path: ':candidate_id' } );
              });
              this.resource('tagging');
              this.route('results', { path: 'page/:page_id' } );
          });
      });
      this.resource('noboards');
    });

    this.resource('longlists', function(){
        this.route('view', { path: ':id/view' });
    });

    this.resource('watchdogs', function(){
        this.route('create', { path : 'create/:criteria_id' });
        this.route('edit', { path : 'edit/:watchdog_id' });
        this.resource('watchdogsview', { path : ':watchdog_id/view' }, function () {
            this.route('board', { path: ':board_id' });
        });
        this.route('summary');
    });

    this.resource('admin', function () {
        this.route('settings');
        this.route('quota');
        this.route('previoussearches');
        this.route('flags');
        this.route('emailtemplates', function () {
            this.route('new');
            this.route('edit', { path: ':template_id/edit' });
        });
        this.resource('admin.tag', { path: 'tag/:value' },
                      function(){
                          this.route('delete' , { path : 'delete' } );
                      });
        this.route('tags', { path : 'tags/:page_id' });
        this.route('macros', function(){
            this.route('new');
            this.route('edit', { path: '/edit/:macro_id' });
        });

        // The route to search as someone else (or do anything else on users)
        this.route('savedsearches');
        this.route('watchdogs', { path: 'watchdogs' });
        this.route('emails');
        this.route('reports');
    });

    this.resource( 'emails', function() {
        this.route('summary');
    } );
});

// Enable pushState urls instead of hash-based ones
// if supported by the browser
if (window.history && window.history.pushState) {
  Stream2.Router.reopen({
    location: 'history'
  });
}

/**
 * Represents an adcresponses ranking flag, required as
 * now we support 'custom' flags ( colours/descriptions )
 *
 * @namespace Stream2
 * @class ResponseFlagModel
 * @extends Ember.Object
 */

Stream2.ResponseFlagModel = Ember.Object.extend({
    id: Ember.computed.alias('flag_id'),
    type: "standard",
    hasRules: Ember.computed.alias('has_rules'),
    isCustom: function () {
        return ! ( this.get('type') === "standard" );
    }.property('type'),
    isRejectionFlag: Ember.computed('id', function() {
        return [1, 3].includes(this.get('id'));
    }),
    setting_url: function () {
        if ( this.get('isCustom') ){
            return '/api/admin/user_groupsettings/' + this.get('setting_mnemonic');
        }
        return '/api/admin/user_settings_by_mnemonic/' + this.get('setting_mnemonic');
    }.property('isCustom'),
    _showHierarchy: false,
    _showFlagRules: false,
});

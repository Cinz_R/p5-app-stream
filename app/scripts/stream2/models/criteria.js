Stream2.Criteria = Stream2.Model.extend({

    location_object: null,

    // Inject the change in the location_object
    locationObjectChanged: function(){
        var location_object = this.get('location_object');
        if( location_object && location_object.id ){
            // console.log("Setting location properties to " , location_object );
            this.set('location_id' , [ location_object.id ] );
            this.set('location' , [ location_object.text ] );
        }else{
            // console.log("Nulling location properties");
            this.set('location_id' , null);
            this.set('location' , null);
        }
    }.observes('location_object'),

    // Update the location object from the flat location properties.
    updateLocationObject: function(){
        var location_id = this.get('flat_location_id');
        var location    = this.get('flat_location');
        var location_object = null;

        if( !Ember.isEmpty(location_id) && !Ember.isEmpty(location) ){
            location_object = { id: location_id , text: location };
        }
        // console.log("Setting location object = ", location_object);
        this.set('location_object' , location_object);
    },

    flat_location: function(){
        var location = this.get('location');
        if( Ember.isArray(location) ){
            return location[0];
        }
        return location;
    }.property('location'),

    flat_location_id: function(){
        var location_id = this.get('location_id');
        if( Ember.isArray(location_id) ){
            return location_id[0];
        }
        return location_id;
    }.property('location_id'),

    defaults: {
        location_within: 30,
        cv_updated_within: "3M",
        default_jobtype: "",
        salary_cur: 'GBP',
        salary_per: "annum",
        salary_from: "",
        salary_to: "",
        distance_unit: 'miles',
        include_unspecified_salaries: true
    },
    more_option_keys: ['cv_updated_within','default_jobtype', 'salary_cur', 'salary_per', 'salary_from', 'salary_to', 'include_unspecified_salaries'],
    more_option_expansion_fields: ['default_jobtype', 'salary_cur', 'salary_per', 'salary_from', 'salary_to', 'include_unspecified_salaries'],
    hasChangedCriteria: function() {
        var _this = this;
        var criteria_has_updated = false;

        $.each(this.more_option_expansion_fields, function(index, key) {
            // if data key has been defined but is not the same as the default value,
            // criteria has been updated
            if((!Ember.isNone(_this.get(key))) && _this.get(key) != _this.defaults[key]) {
                criteria_has_updated = true;
                return false;
            }
        });
        return criteria_has_updated;
    },
    init: function() {
        var _this = this;

        // set dropdown defaults
        $.each(this.defaults, function(key, val) {
            _this.set(key, _this.getWithDefault(key, val));
        });

        this.updateLocationObject();
    },

    /* latch variable: once criteria is changed, it's considered dirty for
     * its entire lifespan: till the search is submitted to the server. When
     * the server responds, a "clean" criteria object will replace this one.
     */
    isDirty: false,
    dirtyObserver: function() {
        this.set("isDirty", true);
    }.observes(
        'keywords',
        'salary_cur',
        'salary_from',
        'salary_to',
        'salary_per',
        'location_id'
    ),

    serialise: function () {
        var criteria_obj = this.getProperties(Ember.keys(this));
        /*
            search terms is an array of words from the keywords ( excluding boolean terms and punctuation )
            at some point it would be great if this was calculated 'before save' in the criteria object and store
            next to any criteria in the DB
            for now, I don't want to save it each time because it will do funny things with the criteria ID and DB
        */

        // Clear location_object. The backend should not know anything about it.
        delete( criteria_obj.location_object );
        delete( criteria_obj._search_terms );
        delete( criteria_obj.id );

        // quirks of the auto complete system, we need to keep a record of their specific postcode entered
        // even if they choose a location as a result. Some feeds rely on the entered postcode
        // If there is no chosen location, then don't send a postcode
        if ( ! criteria_obj.location_id ){
            delete( criteria_obj.location_id_postcode );
        }

        return criteria_obj;
    },
    deserialise: function ( tokens ){

        // console.log("Tokens: ", tokens);

        delete ( tokens.board );
        var _this = this;

        // We store all tokens as arrays, lets break some of the global tokens out of the array
        // otherwise they aren't represented correctly in the gui
        $.each(this.more_option_keys, function(index, key) {
            var value = tokens[key];
            if ( Ember.isArray( value ) ){
                tokens[key] = value[0];
            }
        });

        // parse the value for include_unspecified_salaries into a boolean so the checkbox recognises it
        var inc_inspec_sal = tokens.include_unspecified_salaries;
        if ( ( ! Ember.isNone( inc_inspec_sal ) ) && typeof( inc_inspec_sal ) !== "boolean" ){
            try {
                tokens.include_unspecified_salaries = Boolean( parseInt( inc_inspec_sal ) );
            } catch ( e ){ console.log( 'invalid include_unspecified_salaries value: ' + inc_inspec_sal ); }
        }

        this.setProperties( tokens );

        // Update the location object from the deserialized stuff.
        this.updateLocationObject();

    },

    /*
      Returns one and only one value for the given field name,
      regardless of its array or scalar nature.
     */
    oneValue: function( field_name ){
        var value = this.get(field_name);
        if( Ember.isNone( value )){
            return;
        }
        if( Ember.isArray( value ) ){
            return value[0];
        }
        return value;
    },

    clear: function () {
        var _this = this;
        // Blat all the criteria, or if there is
        // a default, set the default.
        this.forEachCriteria( function (criteria) {
            var default_value = this.defaults[criteria];
            if( default_value !== null ){
                this.set(criteria, default_value);
            }else{
                this.set(criteria, "");
            }
        });

        // Update the location object just in case.
        this.updateLocationObject();

    },

    forEachCriteria: function (cb) {
        Ember.keys(this).forEach(cb, this);
    },

    /* Returns a promise that will be resolved with data = { id : "...." }
     */
    withId: function(){
        return Stream2.request('/api/create_criteria' , { criteria : this.serialise() } , 'POST' );
    },

    /*
        Some criteria defaults are determined through the search user,
        call it to set certain values of the this.defaults object
        based on the users model.
    */
    setDefaultsFromUser: function(searchUser) {
        if ( searchUser ) {
            this.set('defaults.distance_unit', searchUser.get('distance_unit'));
            this.set('defaults.salary_cur', searchUser.get('currency'));
            if ( searchUser.get("manageResponsesView") ) {
                this.set('defaults.cv_updated_within', 'ALL');
            }
            else {
                this.set('defaults.cv_updated_within', searchUser.get('settings.criteria-cv_updated_within-default'));
            }
        }
    }


});

/* Stream2.Quota - Quota model for managing credits on boards

Quota is determined by having a numeric value, including zero. If no quota is
defined for any type, that means the user has no record for that type on the
board and we won't consider quota guarding for it.

*/

Stream2.Quota = Stream2.Model.extend({
    destination: null,

    hasRemainingCvQuota: Ember.computed.gt('search-cv', 0),
    hasRemainingProfileQuota: Ember.computed.gt('search-profile', 0),
    hasRemainingSearchQuota: Ember.computed.gt('search-search', 0),
    hasRemainingProfileOrCvQuota: Ember.computed.gt('search-profile-or-cv', 0),


    // These check if the board has the quota set as opposed to equaling zero.
    // Some don't use quotas so we shouldn't display quota logic across all boards.
    usingCvQuota: Ember.computed.notEmpty('search-cv'),
    usingProfileQuota: Ember.computed.notEmpty('search-profile'),
    usingSearchQuota: Ember.computed.notEmpty('search-search'),
    usingProfileOrCvQuota: Ember.computed.notEmpty('search-profile-or-cv'),

    // These check to see if the user has quotas for the board and if they have quota remaining.
    // If we didn't set quotas on this board for the user, will return true.
    // That's because if it wasn't set, we assume quota guarding isn't needed.
    canSearch: Ember.computed('usingSearchQuota', 'hasRemainingSearchQuota', function() {
        if ( !this.get('usingSearchQuota') ) return true;
        return (this.get('usingSearchQuota') && this.get('hasRemainingSearchQuota')) ?
            true : false;
    }),
    canViewProfile: Ember.computed('usingProfileQuota', 'hasRemainingProfileQuota', 'usingProfileOrCvQuota', 'hasRemainingProfileOrCvQuota', function() {
        if ( !this.get('usingProfileQuota') && !this.get('usingProfileOrCvQuota') ) return true;
        return ((this.get('usingProfileQuota') && this.get('hasRemainingProfileQuota')) // user has profile quota remaining
                || (this.get('usingProfileOrCvQuota') && this.get('hasRemainingProfileOrCvQuota') && !this.get('usingProfileQuota'))) ? // user has profile-or-cv quota and profile quota isn't set and 0
            true : false;
    }),
    canDownloadCV: Ember.computed('usingCvQuota', 'hasRemainingCvQuota', 'usingProfileOrCvQuota', 'hasRemainingProfileOrCvQuota', function() {
        if ( !this.get('usingCvQuota') && !this.get('usingProfileOrCvQuota') )  return true;
        return ((this.get('usingCvQuota') && this.get('hasRemainingCvQuota')) // user has cv quota remaining
                || (this.get('usingProfileOrCvQuota') && this.get('hasRemainingProfileOrCvQuota') && !this.get('usingCvQuota'))) ? // user has profile-or-cv quota and cv quota isn't set and 0
            true : false;
    }),

    hasCandidateActionQuota: Ember.computed('usingProfileQuota', 'usingCvQuota', 'usingProfileOrCvQuota', function() {
        return (this.get('usingProfileQuota') || this.get('usingCvQuota') || this.get('usingProfileOrCvQuota')) ?
            true : false;
    }),

    formattedProfileQuota: Ember.computed('profileQuota', function() {
        if (!this.get('profileQuota')) return null;
        return numeral( this.get('profileQuota') ).format('0,0');
    }),

    formattedCVQuota: Ember.computed('cvQuota', function() {
        if (!this.get('cvQuota')) return null;
        return numeral( this.get('cvQuota') ).format('0,0');
    }),

    formattedProfileOrCVQuota: Ember.computed('profileOrCvQuota', function() {
        if (!this.get('profileOrCvQuota')) return null;
        return numeral( this.get('profileOrCvQuota') ).format('0,0');
    }),

    // Quota value properties
    cvQuota: function() {
        return this.get('search-cv');
    }.property('search-cv'),

    profileQuota: function() {
        return this.get('search-profile');
    }.property('search-profile'),

    profileOrCvQuota: function() {
        return this.get('search-profile-or-cv');
    }.property('search-profile-or-cv'),

    searchQuota: function() {
        return this.get('search-search');
    }.property('search-search'),

    // CSS state classes
    cvQuotaClass: function() {
        return this.contextClass('cvQuota', 'quota');
    }.property('cvQuota'),

    profileQuotaClass: function() {
        return this.contextClass('profileQuota', 'quota');
    }.property('profileQuota'),

    profileOrCvQuotaClass: function() {
        return this.contextClass('profileOrCvQuota', 'quota');
    }.property('profileOrCvQuota'),

    searchQuotaClass: function() {
        return this.contextClass('searchQuota', 'badge');
    }.property('searchQuota'),

    // Placeholders - must be bound to reflect latest model values
    searchQuotaPlaceholder: function() {
        var quota = this.get('searchQuota');
        if (quota === 0) return i18n.__('No search credits left for this board.');
        if (!quota) return '';
        return i18n.__(quota + ' Search credits remaining.');
    }.property('searchQuota'),

    threshold: 10,
    contextClass: function(property, namespace) {
        var quota = this.get(property);
        if (quota === 0) return namespace + '-danger';
        if (quota <= this.get('threshold')) return namespace + '-warning';
        if (quota > this.get('threshold')) return namespace + '-success';
        return namespace + '-info';
    },

    updateQuota: function() {
        var _this = this;
        Stream2.singleRequest('/api/user/quota', { board: this.get('destination') }, 'GET').then(
            function(data) {
                _this.setProperties(data[_this.get('destination')]);
            }
        );
    },

    // TODO: Replace updateQuota calls with this once we can be more certain
    // in regards to when quota is *actually* deducted
    decrementQuota: function(type) {
        var fullType = 'search-' + type;
        var quota = this.get(fullType);
        if (Ember.isEmpty(quota) || quota === 0) return;
        this.set(fullType, quota - 1);
    }
});

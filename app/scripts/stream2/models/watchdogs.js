// Watchdogs _FACTORY_

Stream2.Watchdogs = Ember.Object.extend({});

Stream2.Watchdogs.reopenClass({
    // findById is a promise which returns a watchdog
    findById: function ( id ) {
        var _this = this;
        this.find();
        return new Ember.RSVP.Promise( function ( resolve, reject ){
            _this.loaded.then( function () {

                var numberid = id;
                // The watchdog filtering is only happy with a numeric ID
                // not a string one
                if( Ember.typeOf(numberid)  == 'string' ){
                    numberid = parseInt(numberid , 10 );
                }

                var found = _this.find().filterProperty('id', numberid);
                var first = found.get('firstObject');
                if ( first ) {
                    resolve( first );
                }
                reject();
            } );
        } );
    },
    find: function(params) {
        if ( params && params.board_id ) {
          return this.findById(params.board_id);
        }

        // if we have already loaded the list of boards, return it
        if (this._watchdogs) { return this._watchdogs; }

        // fetch the list as a promise and update the returned Array later
        return this._watchdogs = this._fetchAll();
    },
    _fetchAll: function(params) {
        var _this = this;
        var collection = Em.A();
        this.loaded = Stream2.request('/api/watchdogs', {}, 'GET').then (
            function(data) {
                if ( data.watchdogs ) {
                    data.watchdogs.forEach(function (watchdog) {
                        // make sure new_results will be computed
                        // dynamically in the watchdog object.
                        delete watchdog.new_results;

                        var model = Stream2.Watchdog.create();
                        var boards = watchdog.boards || [];
                        var boardObjects = [];
                        // Turn the boards into plain Ember.Object(s) so
                        // we can use get/set on them.
                        for( var i = 0 ; i < boards.length ; i++ ){
                            boardObjects.push( Ember.Object.create(boards[i]) );
                        }
                        watchdog.boards = boardObjects;

                        model.setProperties(watchdog);
                        collection.pushObject(model);
                    });
                }
            },
            null
        );

        return collection;
    },
    /* reset
        fetches the most up to date list of watchdogs and
        then updates the wathdog list singleton with the new
        values
    */
    reset: function() {
        var _this = this;
        var collection = this._fetchAll();
        this.loaded.then(function() {
            _this._watchdogs.setObjects( collection );
        });
    }
});

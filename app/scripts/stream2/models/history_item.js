Stream2.HistoryItem = Stream2.Model.extend({
    action: null,
    actor:  null,
    label:  null,
    date:   null,
    data:   null,
    icon:   null,
    id: null,
    action_id: null,
    
    partialName: function(){
        var template_name = 'profilebox/history_items/' + this.get('action');
        if( Ember.isEmpty( Ember.TEMPLATES[template_name] ) ){
            return 'profilebox/history_items/generic';
        }
        return template_name;
    }.property('action'),

    inTheFuture: function(){
        if( Ember.isNone( this.get('data').at_time ) ){
            return false;
        }
        return this.get('data').at_time > ( new Date()).toISOString();
    }.property('data.at_time'),
});

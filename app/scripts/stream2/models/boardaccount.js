Stream2.BoardAccount = Stream2.Model.extend({
  logo_src: function () {
    var logo = this.get('logo');
    return Ember.isEmpty(logo) ? 'http://placehold.it/150x70' : '/static/board_logos/'+logo;
  }.property('logo'),

  stream_nice_name: function () {
    return this.get('cvsearch_display_name') || this.get('nice_name') || this.get('name');
  }.property('cvsearch_display_name','nice_name')
});

Stream2.BoardAccount.reopenClass({
  find: function() {
    var collection = Em.A();
    jQuery.getJSON('/api/settings/boardaccounts.json').then(
      function (value) {
        value.boardaccounts.forEach(function (record) {
          var model = Stream2.BoardAccount.create();
          model.setProperties(record);
          collection.pushObject(model);
        });

        collection.set('loaded', true);
      }
    );

    return collection;
  }
});

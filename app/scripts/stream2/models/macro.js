/** The front end object representing a group macro. Properties are the same as Stream2::O::GroupMacro
 * @class Macro
 * @namespace Stream2
 * @extends Stream2.Model
 * @uses ErrorMixin
*/
Stream2.Macro = Stream2.Model.extend( ErrorMixin, {
    
    id: undefined, // TODO All of that is hardcoded for now (Hacker day)
    name: undefined,
    on_action: undefined,
    lisp_source: undefined,
    blockly_xml: undefined,
    /**
     * Is this macro correct enough to be saved?
     * @property isInvalid
     * @type {boolean}
     */
    isInvalid: function(){
        return Ember.isEmpty( this.get('name')  ) ||
            Ember.isEmpty( this.get('lisp_source') );
    }.property('name', 'lisp_source' ),

    /**
     * Delete this macro from the backend.
     * @method delete
     * @return {Promise}
     */
    delete: function(){
        return Stream2.request( '/api/admin/group_macros/' + this.get('id') , {} , 'DELETE').then(
            function ( data ) {
                Stream2.popupAlert(i18n.__('Macro Deleted'), i18n.__('Your macro has been deleted'));
            }
        )
    },
    /**
     * Save this macro in the backend (or update it if it has already an id)
     * @method save
     * @return {Promise}
     */
    save: function(){
        var _this = this;
        if( Ember.isNone( this.get('lisp_source') ) || Ember.isNone( this.get('blockly_xml') ) ){
            alert("Empty program :/");
            return;
        }
        return Stream2.request( '/api/admin/group_macros/' + ( this.get('id') || '' )  , {
            name : this.get('name'),
            on_action: this.get('on_action'),
            lisp_source: this.get('lisp_source'),
            blockly_xml: this.get('blockly_xml')
        }, 'POST').then(
            function ( data ) {
                Stream2.popupAlert(i18n.__('Macro Saved'), i18n.__('Your macro has been saved'));
                _this.set('id', data.macro.id );
                _this.set('error', null);
            },
            function( error ){
                _this.set('error', error.message );
                return Promise.reject();
            }
        )
    }
});

/*
  A cheese Sandwich object. Contains its own preview logic
*/
Stream2.CheeseSandwich = Ember.Object.extend( {
    id: null,
    list: null,

    // Search application specific preview URL
    internalPreviewUrl: function(){
        return '/api/cheesesandwiches/' + this.get('list') + '/' + this.get('id')  + '/preview/999';
    }.property('id', 'list')
});

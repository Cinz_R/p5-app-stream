/*
    Stream.MutableCandidate

    A subclass of the Candidate model, has extra methods for dealing with alterations
    to it's own properties as candidate editing is available with some boards ( probably
    internal boards only )
*/
Stream2.MutableCandidate = Stream2.Candidate.extend({
    // observeChanged and propertyUpdated are used for the TS profile detail editing
    isEditable: true,
    suspendSinglePropertyUpdate: false,
    editable_keys: [
        'name', 'email', 'telephone', 'mobile', 'first_name', 'last_name', 'phone',
    ],

    init: function () {
        var _this = this;
        // after render, observe all properties for changes
        Ember.run.schedule('afterRender', function () {
            // gets called n_candidates * n_properties
            _this.get('editable_keys').forEach( function ( property ) {
                _this.addObserver(property, _this, 'propertyUpdated');
            });
        });
        return this._super();
    },

    propertyUpdated: function ( object, propertyName) {

        if ( this.get('suspendSinglePropertyUpdate') ) {
            return;
        }

        var _this = this;
        var value = this.get(propertyName);
        if( typeof( value ) == "undefined" ){
            // undefined values are not to be set ever.
            console.log("Atempt to set undefined value on " + propertyName + ". Will NOT do anything");
            return;
        }
        Stream2.request( this.apiURL( 'property', propertyName ), { value: value }, 'POST').then(
            function ( data ) {
                Stream2.popupAlert(i18n.__('Detail updated'), data.message );
                if( _this.get('historicActionsChronological') ){
                    // The history is there. We should refresh it.
                    _this.fetchProfileHistory();
                }
            },
            function ( error ) {
                _this.set('error',error);
                Stream2.popupError(error);
            }
        );
        Stream2.pushEvent('candidate','update details');
    },

    updateProperties: function ( properties ) {
        var _this = this;
        //  disable the value updating observers so we update in bulk
        this.set('suspendSinglePropertyUpdate',true);
        this.setProperties(properties);
        this.set('suspendSinglePropertyUpdate',false);

        Stream2.requestCandidateAsync( this, this.apiURL( 'properties' ), properties, 'POST').then(
            function ( data ) {
                Stream2.popupAlert(i18n.__('Detail updated'), data.message );
                if( _this.get('historicActionsChronological') ){
                    // The history is there. We should refresh it.
                    _this.fetchProfileHistory();
                }
            }, function( error ){ console.log( error ); }
        );
        Stream2.pushEvent('candidate','update details');
    },

    // If there is a board specific editable-details template then render that, otherwise use the detault
    detailsPartialName: function () {
        var destination = this.get('destination');
        var customTemplate = 'profile/editable/' + destination;

        // check if a template exists
        if ( Stream2.__container__.lookup( 'template:' + customTemplate ) ){
            return customTemplate;
        }

        return 'profile/editable_details';
    }.property('destination')

});

Stream2.EmailTemplateRule = Stream2.Model.extend({
    // The ID of the rule when it already exists in the DB.
    id: null,
});

Stream2.EmailTemplateRuleResponseFlagged = Stream2.EmailTemplateRule.extend({
    role_name: 'ResponseFlagged',
    componentName: 'email-template-rule-response-flagged',
    // The flag
    flag_id: null,
    // The delay
    in_minutes: 1,

    serialize: function(){
        return this.getProperties('id', 'role_name', 'flag_id', 'in_minutes' );
    },

    // Data is invalid when the flagId is empty or the in_minutes is
    // incorrect.
    dataInvalid: function(){
        if(  Ember.isEmpty( this.get('flag_id') ) ||
             Ember.isEmpty( this.get('in_minutes') ) ){
            return true;
        }
        var inMinutes = parseInt( this.get('in_minutes' ) , 10 );
        if( inMinutes != this.get('in_minutes') ){
            return true;
        }
        return inMinutes < 0;
    }.property('flag_id', 'in_minutes')
});


Stream2.EmailTemplateRuleResponseReceived = Stream2.EmailTemplateRule.extend({
    role_name: 'ResponseReceived',
    componentName: 'email-template-rule-response-received',
    serialize: function(){
        return this.getProperties('id', 'role_name');
    },
    // Data is always valid, as there is no other property to validate
    dataInvalid: false
});

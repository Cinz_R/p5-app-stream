/**
 * Disabled CV-related actions unless the profile has been viewed.
 * @class EfinancialXmlCandidate
 * @namespace Stream2
 * @extends Stream2.MutableCandidate
 */
Stream2.EfinancialXmlCandidate = Stream2.MutableCandidate.extend({
    cvEnabled:        Ember.computed.bool('has_done_profile'),
    forwardEnabled:   Ember.computed.bool('has_done_profile'),
    importEnabled:    Ember.computed.bool('has_done_profile'),
    shortlistEnabled: Ember.computed.bool('has_done_profile'),
    messageEnabled:   Ember.computed.bool('has_done_profile'),
});

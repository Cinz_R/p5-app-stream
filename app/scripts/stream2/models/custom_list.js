Stream2.CustomList = Ember.Object.extend( {
    name: null,
    label: null,
    icon_class: null,
    id: function(){
        return this.get('name');
    }.property('name'),
    allowPreview: null,
    bulkLabel: function(){
        return i18n.__x('{action} selected', { action : this.get('label') });
    }.property('label')

});

Stream2.SavedSearch = Ember.Object.extend( ErrorMixin, {
    name: null,
    displayName: function(){
        return i18n.__x('Run Saved Search "{name}"', {name: this.get('name')});
    }.property('name'),
    criteria: function(){
        var criteria_id = this.get('criteria_id');
        if( ! criteria_id ){ return null ;}

        return Stream2.Criterias.findById(criteria_id);

    }.property('criteria_id')
});

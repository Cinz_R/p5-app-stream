
/* A LongList object */
Stream2.LongList = Ember.Object.extend(
    Em.Copyable,
    {
        id: null,
        name: null,

        buttonTitle: function(){
            var on = this.get('on');
            if( on ){
                return i18n.__x('Remove from {name}' , { name: this.get('name') } );
            }else{
                return i18n.__x('Add to {name}' , { name: this.get('name') } );
            }
            
        }.property('name' , 'on'),

        displayName: function(){
            return i18n.__x('View Longlist {name}' , { name: this.get('name') });
        }.property('name'),
        
        copy: function(){
            return Stream2.LongList.create({ id: this.get('id'),
                                             name: this.get('name') });
        }
    });

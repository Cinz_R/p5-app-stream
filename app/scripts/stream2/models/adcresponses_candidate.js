/*
    Stream.AdcresponsesCandidate

    A subclass of the Mutable Candidate model
    Has editable fields specific to candidate in Adcresponses
*/
Stream2.AdcresponsesCandidate = Stream2.MutableCandidate.extend({

    handlebars_template: 'board/result/adcresponses',
    profileBodyComponent: 'adcresponses-profile-body',

    editable_keys: [
        'broadbean_adcresponse_rank',
        'broadbean_adcresponse_rank_reason',
        'email'
    ],
    currentApplication: function(){
        var response_epoch_date =  moment.utc(this.get('broadbean_adcresponse_date')).unix();
        return {
            application_time: response_epoch_date,
            adcresponse_id: this.get('broadbean_adcresponse_id'),
            advert_link: this.get('advert_preview_url'),
            attachments: this.get('broadbean_adcresponse_attachments'),
            rank: this.get('broadbean_adcresponse_rank'),
            job_title: this.get('broadbean_adcadvert_title')
        }
    }.property('broadbean_adcresponse_attachments','broadbean_adcresponse_rank'),

    // generates a string describing the origin of this candidate's application
    applicationSource: function() {
        var prefix = this.get("broadbean_adcresponse_source_id") == 3
                   ? 'Search - ' : '';
        return prefix + this.get("broadbean_adcboard_nicename");
    }.property("broadbean_adcboard_nicename", "broadbean_adcresponse_source_id"),

    isApplicationResponse: Ember.computed( 'broadbean_adcresponse_source_id', function () {
        return ! ( this.get("broadbean_adcresponse_source_id") == 3 );
    }),

    // See super class Candidatte
    filterApplicationHistory: function( app_history ){
        var currentApplication = this.get('currentApplication');
        // There is a current application against this candidate.
        // Time to filter it out of this history of other applications.
        app_history = app_history.filter( function( item , idx ){
            return item.adcresponse_id != currentApplication.adcresponse_id;
        });
        return app_history;
    },

    /* Requisition advert responses are considered general submissions */
    isGeneralSubmission: Ember.computed.equal('broadbean_adcadvert_is_requisition_ad', 'True'),
    canBulkDelegate: Ember.computed.equal('broadbean_adcadvert_is_requisition_ad', 'True'),
});

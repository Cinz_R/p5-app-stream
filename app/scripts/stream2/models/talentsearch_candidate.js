/*
    Stream.TalentsearchCandidate

    A subclass of the Mutable Candidate model
    Has editable fields specific to candidate in TS
*/
Stream2.TalentsearchCandidate = Stream2.MutableCandidate.extend({
    handlebars_template: 'board/result/talentsearch',
    editable_keys: [
        'name', 'email', 'telephone', 'mobile', 'first_name', 'last_name', 'phone',
        'advert_industry', 'advert_job_type', 'applicant_recruitment_status', 'applicant_notice_period', 'applicant_availability_date',
        'salary_currency', 'salary_from', 'salary_to', 'salary_per', 'currently_employed',
    ]
});

Stream2.Board = Stream2.Model.extend({
    _searchURL :    'run',

    label:          function () { return this.get('nice_name'); }.property('nice_name'),
    id:             function () { return this.get('name'); }.property('name'),
    type:null,

    cv_cost_warning: null,

    selected:       false,
    disabled:       0,
    hasFacets:      Ember.computed( 'facets', function () {
        return !! this.get('facets.length');
    }),
    _results:       null, // cache of current results if the search hasnt changeda

    // New results count for watchdog display. Always null outside of watchdogs.
    newResults:     null,

    // Lazy property.
    supportsCbOneIAM: function(){
        var name = this.get('name');
        this.set('supportsCbOneIAM',
                 ['cb_mysupply', 'careerbuilder', 'recruitmentedge' ].any(
                     function(item){
                         return item == name;
                     }
                 )
                );
        return this.get('supportsCbOneIAM');
    }.property('name'),
    // a Stream2.Quota model object containing remaining quotas for the user on this board
    quota:          null,

    isHidden: function(){
        var type = this.get('type');
        if( ! type ){ return false; };
        return type == 'hidden';
    }.property('type'),

    hasResults:     function () {
        var total = this.get('totalResults');
        return ( typeof( total ) !== "undefined" && total !== null );
    }.property('totalResults'),

    formattedTotalResults: function(){
        var total = Number(this.get('totalResults'));
        if( Ember.isNone(total) ){
            return 0;
        }
        return numeral(total).format('0,0');
    }.property('totalResults'),

    results_id:     function () {
        var page = this.get('selectedPage');
        if ( page ) {
            return page.get('results_id');
        }
    }.property('selectedPage'), // used when switching back to board without changing criteria
    hasOldResults:  function () {
        var page = this.get('selectedPage');
        if ( page ) {
            return page.get('hasOldResults');
        }
        return true;
    }.property('selectedPage'),

    /*
      Performs the actual search on this board, given a serialized Criteria
      , an optional watchdog and the user performing the search.

      Returns a promise fullfilled with the search result from the server,
      or rejected with an error.reason
    */
    search:     function ( criteria , watchdog , searchUser ) {
        var _this = this;
        var search = { _searchOptions: {} };

        var cbOneIAMPromise;
        var cbOneIAMUser = searchUser.get('cbOneIAMUser');
        if( this.get('supportsCbOneIAM') &&  cbOneIAMUser ){
            console.log("This board supports CBOneIAM and the user has got " , cbOneIAMUser );
            // This is a promise that the user is
            // 1. connected to CBOneIAm,
            // 2. with the right cbOneIAMUser
            cbOneIAMPromise = Stream2.get('cbOneIamAuth').assumeCbOneIAMUser( cbOneIAMUser );
        }

        if ( ! this.get('hasFacets') ){
            search._searchOptions.include_facets = true;
        }

        search.board = this.get('name');
        search.criteria = criteria;

        if( watchdog ){
            search.watchdog_id = watchdog.get('id');
        }

        if ( this.get('selectedPage') ){
            search._searchOptions.page = this.get('selectedPage').get('id');
            search._searchOptions.results_per_scrape = this.get('context').results_per_scrape;
        }

        Stream2.timeMark("submitting search");

        this.set('context', {});
        if( ! cbOneIAMPromise ){
            // Plain search, without any CB OneIAM stuff.
            return Stream2.singleRequest( _this.get('_searchURL'), search );
        }else{
            // Resolve the OneIAM connection first, then do the search.
            return cbOneIAMPromise.then(
                function( cbOneIAMSession ){
                    console.log("Performing search with oneIAM Session");
                    search._searchOptions.cb_oneiam_auth_code = cbOneIAMSession.auth_response.auth_code;
                    return Stream2.singleRequest( _this.get('_searchURL'), search );
                },
                function( error ){
                    console.log("cbOneIAMPromise Error is : ", error );
                    return new Ember.RSVP.Promise( function ( resolve, reject ){
                        reject({ reason : error });
                    });
                }
            );
        }
    },

    /*
    ********
    * Facets *
    ********
    */
    sort_by: null,
    setSortBy: function ( sort_by, callback ) {
        this.removeObserver( 'sort_by.Value' );
        this.set( 'sort_by', this._facetObject( sort_by ) );
        Ember.run.schedule( 'afterRender', this, function () {
            this.addObserver( 'sort_by.Value', callback );
        });
    },
    facets: [],
    facetPopulate: function (facets, callback){
        this.set('facets',[]);
        this.addReplaceFacets( facets, callback );
    },

    // if we are replacing, we will have to transfer the current values
    // Will also make sure the facets are sorted by 'SortMetric' to
    // respect the order of stuff defined in the Feed, regardless of
    // the dynamicity of the facets.
    addReplaceFacets: function ( facets, callback ) {
        var _this = this;
        var facet_arr = [];
        var current_facets = this.get('facets');
        facets.forEach( function ( facet ) {
            if ( facet.Type == "sort" ) return 1;
            var c_facet = _this.getFacetByName( facet.Name );
            var insert_index = current_facets.indexOf( c_facet );
            facet.onChange = callback;
            if ( c_facet ) {
                facet.Value = facet.Value || c_facet.get('Value');
                facet.SelectedValues = facet.SelectedValues || c_facet.get('SelectedValues');
                current_facets.removeAt( insert_index );
                current_facets.insertAt( insert_index, _this._facetObject( facet ) );
            }
            else {
                current_facets.pushObject( _this._facetObject( facet ) );
            }
        });
        // Sort the facets according to their SortMetric property
        // And reinject as the facets
        var sortedFacets = this.get('facets').toArray().sort(function( faceta , facetb ){
            var orderA = faceta.SortMetric;
            var orderB = facetb.SortMetric;
            if( ! orderA ){ orderA = 0; }
            if( ! orderB ){ orderB = 0; }
            return orderA > orderB ? 1 : -1;
        });
        this.set('facets' , sortedFacets);
    },
    getFacetByName: function ( name ) {
        var facets = this.get('facets');
        for ( var fi = 0; fi < facets.length; fi++ ){
            if ( facets[fi].Name === name ) return facets[fi];
        }
    },
    _facetObject: function ( primative ) {
        if ( Ember.isEmpty( primative.Options ) ) primative.Options = Ember.A();
        if ( Ember.isEmpty( primative.SelectedValues ) ) primative.SelectedValues = {};
        return Stream2.Facet.create( primative );
    },
    serialiseFacets: function () {
        var facetsChosen = {};
        var _this = this;

        this.get('facets').forEach( function (item) {
            var facet_name = _this.get('name') + "_" + item.get('Name');
            facetsChosen[facet_name] = _this.facetValue( item );
        });

        if ( this.get('sort_by') !== null ){
            var sort_by = this.get('sort_by');
            facetsChosen[this.get('name') + "_" + sort_by.get('Name')] = sort_by.get('Value');
        }

        return facetsChosen;
    },

    facetValue: function ( item ) {
        // need to cycle through child elements to detect chosen values
        if ( item.get('Type') === "groupedmultilist" || item.get('Type') === "multilist" || item.get('Type') === "tag" || item.get('Type') === "advert" ){
            var result_arr = $.map( item.get('SelectedValues'), function ( val, key ) { if ( val ) return key; } );
            return result_arr;
        }
        else {
            if ( ! Ember.isEmpty(item.get('Value')) ){
                return item.get('Value');
            }
        }
    },

    clearFacets: function () {
        this.get('facets').forEach( function ( item ) {
            item.destroy(); // does not trigger the search
        });
        this.set('facets',[]);
    },

    /*
    ********
    * Paging *
    ********
    */
    selectedPage: function () {
        return this.get('pageNumbers').findBy('isSelected',true);
    }.property('pageNumbers.@each.isSelected'),
    pageNumbers: [],
    displayPages: [],
    _setPages: function ( current_page ) {
        if ( !current_page ) return;

        this.set('displayPages',[]);
        this.get('pageNumbers').forEach( function( page ) {
            page.set('isVisible',false);
        });

        // context.current_page is a string, needs to be int for comparison purposes
        current_page = current_page * 1;

        var start_page  = current_page - 1,
            end_page    = current_page + 1,
            page        = null;

        var pages = (this.get('context').max_pages*1)
                  || Math.min( Math.ceil( this.get('totalResults') / ( this.get('context').results_per_scrape || this.get('context').per_page )  ), 100 );

        if ( ! pages ) pages = 1;


        while ( start_page < 1 ){
            start_page++;
            end_page++;
        }
        while ( end_page > pages ){
            end_page--;
            start_page--;
        }
        if ( start_page < 1 ) start_page = 1;
        if ( end_page > pages ) end_page = pages;

        if ( start_page > 1 ) {
            page = this.getCreatePage(1);
            page.setProperties({ isVisible: true, isSelected: ( current_page === 1 ) ? true : false });
            this.get('displayPages').pushObject(page);
        }
        if ( start_page > 2 ) this.get('displayPages').pushObject(Stream2.PageType.create({ isClickable: false, label: "..", isVisible: true }));

        for ( var p_it = start_page; p_it <= end_page; p_it++ ){
            page = this.getCreatePage(p_it);
            page.setProperties({ isVisible: true, isSelected: ( current_page === p_it ) ? true : false });
            this.get('displayPages').pushObject(page);
        }

        if ( end_page < pages - 1 ) this.get('displayPages').pushObject(Stream2.PageType.create({ isClickable: false, label: "..", isVisible: true }));
        if ( end_page < pages ) {
            page = this.getCreatePage(pages);
            page.setProperties({ isVisible: true, isSelected: ( current_page === pages ) ? true : false });
            this.get('displayPages').pushObject(page);
        }

        /* On first rendering, PageNumber links should be non clickable until
         * some results are available. getCreatePage() tries to reuse existing
         * PageNumbers whose isClickable might already have been enabled
         * elsewhere, so reset them all en masse to false, to be certain.
         */
         this.get("pageNumbers").setEach('isClickable', false);

    },
    getCreatePage: function ( id ) {
        var page = this.pageNumbers.findBy( 'id', id );
        if ( page === null || typeof( page ) == "undefined" ){
            page = Stream2.PageNumber.create({
                'id': id,
                isVisible: false,
                isClickable: false
            });
            this.pageNumbers.pushObject( page );
        }
        return page;
    },
    changePage: function ( page ){
        this.get('selectedPage').set('isSelected',false);
        page.set('isSelected', true);
        this._setPages( page.get('id') );
        this.set('_results',[]);
    },
    totalResults: function () {
        if ( ! this.get('context') ) return null;
        if ( this.get('context').total_results ) {
            return this.get('context').total_results;
        }
        if ( this.get('_results') ){
            return this.get('_results').length || null;
        }
        return null;
    }.property('context','_results'),

    setSearchContext: function ( context ) {
        this.set('context', context );
        if ( ! this.get('pageNumbers').length ){
            this.set('pageNumbers',[]);
            this._setPages( context.current_page );
        }
    },
    expireResults: function () {
        this.get('pageNumbers').clear();
        this.set('_results',[]);
    },

    /* Has some new results in the sense of watchdog new results */
    hasNewResults: function(){
        return ! Ember.isNone(this.get('newResults'));
    }.property('newResults'),

    isInternal: function () {
        return ( this.get('type') === "internal" );
    }.property('type')
});

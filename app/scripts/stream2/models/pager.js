Stream2.Pager = Stream2.Model.extend({
    total_entries: null,
    entries_per_page: null,
    current_page: null,
    entries_on_this_page: null,
    first_page: null,
    last_page: null,
    first: null,
    last: null,
    previous_page: null,
    next_page: null
});

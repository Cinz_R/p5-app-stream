Stream2.BulkCandidateOutcome = Ember.Object.extend({
    candidate: null,
    finished: false,
    error: false,

    message: null,

    warnings: [],
    _warnings: function () {
        return this.get('_warnings').map( function ( item ) {
            return Ember.Object.create({ message: item });
        });
    }.property( '_warnings' ),

    success: function () {
        return ( this.get('finished') && ! this.get('error') );
    }.property( 'finished', 'error' )
});

/**
 * A file attachment associated with a candidate
 * @class Attachment
 * @namespace Stream2
 */
Stream2.Attachment = Ember.Object.extend({
    /**
     * Filename to label the file with in the profile.
     * Not necessarily the filename of the downloaded file
     * @property filename {string}
     * @default null
     */
    filename: null,

    /**
     * HTML preview of the file
     * @property rendered_html {string}
     * @default null
     */
    rendered_html: null,

    /**
     * The candidate to whom this attachment belongs
     * @property candidate {Candidate}
     */
    candidate: null,

    /**
     * The key distinguising this attachment from other attachments the
     * candidate may have
     * @property key {string}
     * @default null
     */
    key: null,

    /**
     * Determines how the attachment preview is rendered in the profile.
     * Possible values: 'html' and 'pdf'
     * @property render_type {string}
     * @default html
     */
    render_type: 'html',

    /**
     * Returns the attachement download URL
     * @method downloadURL
     * @return download URL {string}
     */
    downloadURL: function(){
        var candidate = this.get('candidate');
        if( ! candidate ){ return null; }
        return  candidate.apiURL('download_other_attachment') + '?' +
          $.param({key: this.get('key')});
    }.property('candidate'),

    /**
     * src attribute for PDF preview frame, if appliccable.
     * @method pdfFrameSrc
     * @return frame_src {string}
     */
    pdfFrameSrc: function() {
        if(this.get('render_type') !== 'pdf') {
            return '';
        }

        return '/static/pdfjs/web/viewer.html?file='
          + encodeURIComponent(this.get('downloadURL'));
    }.property('render_type'),
});

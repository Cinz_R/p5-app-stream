Stream2.Watchdog = Ember.Object.extend( ErrorMixin, {
    name: null,
    id: null,
    boards : null,
    criteria_id: null,

    new_results: function(){
        return this.computeNewResults();
    }.property('boards.@each.count'),

    computeNewResults: function(){
        var acc = 0;
        var boards = this.get('boards');
        if( Ember.isNone(boards) ){ return null; }
        boards.map(function(board){
            acc = acc + board.get('count');
        });
        return acc;
    },

    displayName: function(){
        return i18n.__x('Open Watchdog "{name}"', {name: this.get('name')});
    }.property('name'),
    criteria: function(){
        var criteria_id = this.get('criteria_id');
        if( ! criteria_id ){ return null ;}

        return Stream2.Criterias.findById(criteria_id);

    }.property('criteria_id')
});

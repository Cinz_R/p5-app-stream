Stream2.Translations = Ember.Object.extend({});
Stream2.Translations.reopenClass({
    load: function ( locale ) {
        var _this = this;
        return new Ember.RSVP.Promise( function ( r, f ){
            var en_promise = _this._loadLocale( 'en' );
            if ( locale != 'en' ){
                var lang_promise = _this._loadLocale( locale );
                lang_promise.then (
                    function ( locale_data ) {
                        _this._loadTranslations( locale, locale_data )
                            .done( function () { r( locale_data ); } );
                    },
                    function () {
                        en_promise.then( function ( locale_data ) {
                            _this._loadTranslations( 'en', locale_data )
                                .done( function () { r( locale_data ); } );
                        });
                    }
                );
            }
            else{
                en_promise.then( function ( locale_data ) {
                    _this._loadTranslations( 'en', locale_data )
                        .done( function () { r( locale_data ); } );
                });
            }
        });
    },

    _loadLocale: function ( lang ) {
        var static_res = Ember.Object.create( this._static() );
        var URL = '/static/translations/' + lang + '.json?v=' + stream2_VERSION;
        return new Ember.RSVP.Promise( function ( r, f ) {
            $.ajax({
                method: 'GET',
                url: URL,
                dataType: 'json',
                accept: 'json',
                // when we have the resources from the server, merge them with our static resources
                success: function ( d ) { r( $.extend( true, static_res, d ) ); },
                error: function () { f(); }
            });
        });
    },

    _loadTranslations: function ( lang, locale_data ) {
        return i18n.init({
            lng: lang || 'en',
            resStore: locale_data.i18nextKeys || {},
            interpolationPrefix: "{",
            interpolationSuffix: "}",
            nsseparator: ':::',
            keyseparator: '::',
            fallbackOnEmpty: true,
            fallbackOnNull: true,
            fallbackLng: 'en'
        });
    },

    _static: function () {
        return {
            criteria : {
                cv_updated_within_options : [
                    ['ALL','Any time'],
                    ['TODAY','Today'],
                    ['YESTERDAY','Since yesterday'],
                    ['3D','in the last 3 days'],
                    ['1W','in the last week'],
                    ['2W','in the last 2 weeks'],
                    ['1M','in the last month'],
                    ['2M','in the last 2 months'],
                    ['3M','in the last 3 months'],
                    ['6M','in the last 6 months'],
                    ['1Y','in the last year'],
                    ['2Y','in the last 2 years'],
                    ['3Y','in the last 3 years']
                ],

                industry_options : [
                    ['','Not specified'],
                    ['Accountancy','Accountancy'],
                    ['Admin and Secretarial','Admin and Secretarial'],
                    ['Advertising and PR','Advertising and PR'],
                    ['Aerospace','Aerospace'],
                    ['Agriculture Fishing and Forestry','Agriculture Fishing and Forestry'],
                    ['Arts','Arts'],
                    ['Automotive','Automotive'],
                    ['Banking','Banking'],
                    ['Building and Construction','Building and Construction'],
                    ['Call Centre and Customer Service','Call Centre and Customer Service'],
                    ['Consultancy','Consultancy'],
                    ['Defence and Military','Defence and Military'],
                    ['Design and Creative','Design and Creative'],
                    ['Education and Training','Education and Training'],
                    ['Electronics','Electronics'],
                    ['Engineering','Engineering'],
                    ['FMCG','FMCG'],
                    ['Fashion','Fashion'],
                    ['Financial Services','Financial Services'],
                    ['Graduates and Trainees','Graduates and Trainees'],
                    ['Health and Safety','Health and Safety'],
                    ['Hospitality and Catering','Hospitality and Catering'],
                    ['Human Resources and Personnel','Human Resources and Personnel'],
                    ['IT','IT'],
                    ['Insurance','Insurance'],
                    ['Legal','Legal'],
                    ['Leisure and Sport','Leisure and Sport'],
                    ['Logistics Distribution and Supply Chain','Logistics Distribution and Supply Chain'],
                    ['Manufacturing and Production','Manufacturing and Production'],
                    ['Marketing','Marketing'],
                    ['Media','Media'],
                    ['Medical and Nursing','Medical and Nursing'],
                    ['New Media and Internet','New Media and Internet'],
                    ['Not for Profit and Charities','Not for Profit and Charities'],
                    ['Pharmaceuticals','Pharmaceuticals'],
                    ['Property and Housing','Property and Housing'],
                    ['Public Sector and Government','Public Sector and Government'],
                    ['Purchasing and Procurement','Purchasing and Procurement'],
                    ['Recruitment Consultancy','Recruitment Consultancy'],
                    ['Retail','Retail'],
                    ['Sales','Sales'],
                    ['Science and Research','Science and Research'],
                    ['Senior Appointments','Senior Appointments'],
                    ['Social Care','Social Care'],
                    ['Telecommunications','Telecommunications'],
                    ['Transport and Rail','Transport and Rail'],
                    ['Travel and Tourism','Travel and Tourism'],
                    ['Utilities','Utilities']
                ],

                jobtype_options : [
                    ['','Any'],
                    ['permanent','Permanent/Employee'],
                    ['contract','Contract'],
                    ['temporary','Temporary']
                ],

                talentsearch_jobtype_options : [
                    ['','Any'],
                    ['Permanent','Permanent/Employee'],
                    ['Contract','Contract'],
                    ['Temporary','Temporary']
                ],

                notice_options : [
                    ['','Not specified'],
                    ['1','Immediate'],
                    ['2','1 week'],
                    ['3','2 weeks'],
                    ['4','3 weeks'],
                    ['5','5 weeks'],
                    ['6','8 weeks'],
                    ['7','16 weeks'],
                    ['8','3 months+']
                ],

                recruitment_status_options : [
                    ['','Not specified'],
                    ['Available','Available'],
                    ['In Progress','In Progress'],
                    ['On Assignment','On Assignment']
                ],

                salary_cur_options : [
                    ['USD','$'],
                    ['GBP','£'],
                    ['EUR','€'],
                    ['AUD','AUD']
                ],

                talentsearch_salary_cur_options : [
                    ['',''],
                    ['USD','$'],
                    ['GBP','£'],
                    ['EUR','€'],
                    ['AUD','AUD']
                ],

                salary_per_options : [
                    ['annum','year'],
                    ['month','month'],
                    ['week','week'],
                    ['day','day'],
                    ['hour','hour']
                ],

                talentsearch_salary_per_options : [
                    ['',''],
                    ['annum','year'],
                    ['month','month'],
                    ['week','week'],
                    ['day','day'],
                    ['hour','hour']
                ],

                currently_employed_options: [
                    [ 'True', 'True' ],
                    [ 'False', 'False']
                ]

            },
            placeholders : {
                titleWatchdogsSummaryLink       : 'View your watchdogs details',
                titleTagHotlistSelected         : 'Tag Hotlist selected',
                titleEmailSelected              : 'Email selected',
                titleForwardSelected            : 'Forward selected',
                titleDelegateSelected           : 'Delegate selected',

                placeholderCandidateDelegate    : 'Delegate this candidate to a colleague',
                placeholderCandidateEnhancement : 'More free information',
                placeholderRemoveFromLongList   : 'Remove from Longlist',
                placeholderAddToLongList        : 'Add to Longlist',
                placeholderNewLongList          : 'New Longlist Name',
                placeholderOpenThisWatchdog     : 'Open this watchdog',
                placeholderEditThisWatchdog     : 'Edit this watchdog',
                placeholderDeleteThisWatchdog   : 'Delete this watchdog',
                placeholderDeleteThisSearch     : 'Delete this search',
                placeholderWatchdogsRunDaily    : 'Watchdogs run daily and new candidates are emailed to you.',
                placeholderNewWatchdogResults   : 'New watchdog results',
                placeholderWhat                 : 'What?',
                placeholderWhere                : 'Where?',
                placeholderSavedSearchName      : 'User or saved search name...',
                placeholderTagName              : 'Tag Name...',
                placeholderWatchdogName         : 'User or watchdog name...',
                placeholderPleaseSelect         : 'Please Select',
                placeholderSubject              : 'Subject...',
                placeholderMessage              : 'Message...',
                placeholderReplyTo              : 'Reply To...',
                placeholderPleaseTypeJobTitle   : 'Please type in a job title',
                placeholderNameYourWatchdog     : 'Name your watchdog',
                placeholderLoadingSearchResults : 'Loading search results...',
                placeholderDownloadToDesktop    : 'Download to desktop',
                placeholderForward              : 'Forward',
                placeholderTalentSearch         : 'TalentSearch',
                placeholderSaveTalentSearch     : 'Save to TalentSearch',
                placeholderSaveYourDatabase     : 'Save to your database',
                placeholderBoardMightCharge     : 'Board might charge',
                placeholderEmail                : 'Email',
                placeholderExternalProfile      : 'External profile',
                placeholderProfile              : 'Profile',
                placeholderChargeableProfile    : 'Board may charge you for the profile view',
                placeholderPreview              : 'Preview',
                placeholderFavouriteReviewLater : 'Favourite to review later',
                placeholderThisCandidateNotes   : 'This candidate has notes',
                placeholderThisCandidateHistory : 'This candidate has history',
                placeholderTagHotlist           : 'Tag/Hotlist',
                placeholderFlaggingHistory      : 'Flagging History',
                placeholderDeleteThisNote       : 'Delete this note',
                placeholderResearchOnXing       : 'Research on XING',
                placeholderResearchOnLinkedIn   : 'Research on LinkedIn',
                placeholderResearchOnFacebook   : 'Research on Facebook',
                placeholderResearchOnGooglePlus : 'Research on Google Plus',
                placeholderResearchOnDuedil     : 'Research on Duedil',
                placeholderResearchOnTwitter    : 'Research on Twitter',
                placeholderBackToSearch         : 'Back to search',
                placeholderNameThisSearch       : 'Name this search?',
                placeholderFilterUser           : 'Type username to filter',
                placeholderSelectRecipients2    : 'click here or start typing an address'
            }
        };
    }
});

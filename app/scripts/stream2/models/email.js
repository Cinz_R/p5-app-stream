/* an email message sent to a candidate in the search results via the email 
 * action. These objects are stored in DatedEmailList model objects
 */
Stream2.Email = Stream2.Model.extend( ErrorMixin, {
    id:            null,
    date:          '',
    subject:       '',
    from:          '',
    recipient:     '',
    recipientName: '',
    
    mailtoLink: function() {
        var addr = this.get('recipient');
        var name = this.get('recipientName') || addr;
        return '<a href="mailto:' + addr + '">' + name + '</a>';
    }.property('recipient', 'recipientName'),

    url: function () {
        return '/api/user/emails/' + this.get('id')
    }.property('id'),

    body: null,
    loadBody: function () {
        var _this = this;
        Stream2.request( this.get('url'), null, 'GET' ).then(
            function ( value ) {
                _this.set('body', value.body);
            },
            function ( error ) {
                _this.set('error',error);
            }
        );
    }
});

/** A Candidate as a result of a search.
 * @class Candidate
 * @namespace Stream2
 * @extends Stream2.Model
 * @uses ErrorMixin
 */
Stream2.Candidate = Stream2.Model.extend( ErrorMixin, {
    id: function () {
        return this.get('idx');
    }.property('idx'),
    idx: null,
    recent_actions: [],
    isSelected: false,
    notes: Ember.A(),

    profileBodyComponent: 'default-profile-body',

    can_enhance: false,
    result_enhancement_template: function() {
        return 'board/result/enhancements/' + this.get('destination');
    }.property('destination'),

    hasHistory: false,
    has_done_properties: ['download', 'import', 'forward', 'profile', 'message', 'profile', 'longlist_add' , 'longlist_remove', 'enhancement', 'delegate' ],
    profile_body: null,

    /**
     * Whether the candidate has any attachments (besides the CV).
     * Can be set during profile requests.
     * @property {boolean} has_other_attachments
     * @default false
     */
    has_other_attachments: false,

    /**
     * The currently selected tab in the candiate profile popup
     * @property selectedTab {String}
     * @default "profile"
     */
    selectedTab: 'profile',

    historicActionsChronological: null,

    /**
     * An array of Stream2.YearMonthContainer
     * @property historicActionsByMonth {Array}
     */
    historicActionsByMonth: null,

    /**
     * True if CV actions are permitted
     * @property cvEnabled
     * @type {boolean}
     * @default true
     */
    cvEnabled: true,
    /**
     * True if forward actions are permitted
     * @property forwardEnabled
     * @type {boolean}
     * @default true
     */
    forwardEnabled: true,
    /**
     * True if import actions are permitted
     * @property importEnabled
     * @type {boolean}
     * @default true
     */
    importEnabled: true,
    /**
     * True if shortlist actions are permitted
     * @property shortlistEnabled
     * @type {boolean}
     * @default true
     */
    shortlistEnabled: true,
    /**
     * True if message actions are permitted
     * @property messageEnabled
     * @type {boolean}
     * @default true
     */
    messageEnabled: true,

    /**
     * @property {object} future_message
     *   @property {string} action_id
     *   ID of the future action (for potential cancellation).
     *   @property {string} at_time
     *   ISO date at which the future message will be sent
     *   @property {string} template_name
     *   Name of the email template to be sent.
     * @default null
     */
    future_message: null,

    /* Do all sorts of clever stuff */
    init: function() {
        var _this = this;

        this.updateRecentActions();

        // go through each item which we consider to be historical.
        // For each item which has not yet been carried out on this
        // candidate, add an observer which fires when this action
        // is stimulated

        var hasNotDone = this.has_done_properties.map( function ( key ) {
            return 'has_done_' + key; // add observers to has_done_ attributes
        }).filter( function ( key ) {
            return ! _this[key]; // but only if they aren't already true
        });


        if ( hasNotDone.length < this.has_done_properties.length ) {
            this.set('hasHistory',true);
        }

        hasNotDone.forEach( function ( key ) {
            _this.addObserver( key, _this, "historyUpdated");
        });

    },

    /**
     * Update the candidate's has_done_* properties from the recent_actions property
     * @method updateRecentActions
     */
    updateRecentActions: function() {
        var _this = this;
        this.get('recent_actions').forEach( function ( item ) {
            _this.set('has_done_' + item, true );
        });
    }.observes('recent_actions'),

    selectedProfile: function(){
        return this.get('selectedTab') === 'profile';
    }.property('selectedTab'),

    selectedHistory: function(){
        return this.get('selectedTab') === 'history';
    }.property('selectedTab'),

    selectedMessage: function(){
        return this.get('selectedTab') === 'message';
    }.property('selectedTab'),

    selectedOtherAttachments: function(){
        return this.get('selectedTab') === 'otherAttachments';
    }.property('selectedTab'),

    hasFlaggingHistory: function () {
        return !! ( this.get('flagging_history') && ! Ember.isEmpty( Ember.keys( this.get('flagging_history') ) ) );
    }.property('flagging_history'),

    fetchProfileHistory: function(){
        var candidate = this;

        candidate.set('historicActionsChronological', Ember.A());
        candidate.set('historicActionsByMonth', Ember.A());

        var metaHash = Stream2.get('candidateHistoryMetaByKey');
        Stream2.request( candidate.apiURL( 'profile_history' ), {}, 'GET').then(
            function( response ){
                var data = response.data;
                var action_records = data.actions || [];
                var currentYearMonth = moment.utc('1900-01-01T00:00:00Z');
                var currentYearMonthContainer;
                $.each(action_records, function( idx, action_record ){
                    var displayProperties = metaHash[action_record.action] ||
                        { action: action_record.action,
                          singular_label:  action_record.action,
                          icon: 'icon-star'
                        };
                    var sent_email;
                    if( action_record.sent_email ){
                        sent_email = Stream2.Email.create(action_record.sent_email);
                    }
                    var history_item = Stream2.HistoryItem.create({
                        id:        action_record.id,
                        action_id: action_record.action_id,
                        action:    action_record.action,
                        actor:     action_record.user,
                        label:     action_record.data.label || displayProperties.singular_label,
                        date:      action_record.datetime,
                        data:      Ember.Object.create( action_record.data ),
                        icon:      displayProperties.icon,
                    });
                    if( sent_email ){
                        history_item.set('sent_email', sent_email);
                    }

                    var localYearMonth = moment.utc( history_item.get('date') );
                    if( localYearMonth.year() != currentYearMonth.year() ||
                        localYearMonth.month() != currentYearMonth.month() ){
                        // New current year month and container (and push it to the historicActionsChronological)
                        currentYearMonth = localYearMonth;
                        currentYearMonthContainer = Stream2.YearMonthContainer.create({
                            yearMonthDate : localYearMonth,
                            collection: Ember.A()
                        });
                        candidate.get('historicActionsByMonth').pushObject( currentYearMonthContainer );
                    }

                    currentYearMonthContainer.get('collection').pushObject( history_item );

                    candidate.get('historicActionsChronological').pushObject( history_item );
                });
            },
            function(error){
                candidate.set('error', error);
            }
        );
    },

    /**
     * Returns the candidate's attachments. If the attachments are not yet loaded,
     * then loads the attachments. Once the attachmentes are then loaded, can be called agian
     * to retrieve the attachments. See also otherAttachmentsLoading
     * @method otherAttachments
     * @return attachments {Stream2.Attachment[]}
     */
    otherAttachments: function(){
        var _this = this;
        _this.set('otherAttachmentsLoading' , true );
        Stream2.requestAsync( this.apiURL( 'other_attachments' ), {}, 'GET').then(
            function( data ){
                var attachments = data.other_attachments || [];
                var attachmentObjects = [];
                attachments.forEach(function(item){
                    var attachment_object = Stream2.Attachment.create( item );
                    attachment_object.set('candidate' , _this );
                    attachmentObjects.pushObject( attachment_object );
                });

                // Set myself, that's magic.
                _this.set('otherAttachments',attachmentObjects);
                _this.set('otherAttachmentsLoading' , false );
            },
            function(error){
                _this.set('error', error);
                _this.set('otherAttachmentsLoading' , false );
            }
        );
        return [];
    }.property(),

    /**
     * True if this candidate's other attachments are loading
     * @property otherAttachmentsLoading {boolean}
     * @default false
     */
    otherAttachmentsLoading: false,

    enhance: function(section){
        var _this = this;
        if ( _this.get('has_loaded_' + section + '_enhancement') ) {return;}
        _this.set('enhanceCandidateLoading', true);
        Stream2.requestAsync(this.apiURL('enhancements'), {}, 'GET', { section: section }).then(
            function( data ){
                var enhancements = data.enhancements;
                for (var prop in enhancements) {
                    _this.set(prop, enhancements[prop]);
                }
                _this.set('enhanceCandidateLoading', false);
                _this.set('has_done_enhancement', true);

                // shows the hbs template
                _this.set('has_loaded_' + section + '_enhancement', true);

                // used to toggle the enhancements section
                _this.set('enhancementToggleId', _this.get('candidate_id') + '-enhancements-toggle');
                _this.set('showing_enhancements', true);
            },
            function(error){
                _this.set('error', error);
                _this.set('enhanceCandidateLoading', false);
            }
        );
        Stream2.pushEvent('candidate', 'enhancement');
    },

    /**
     * True if the candidate can participate in bulk messaging:
     * if it is not blocked and if it has an email or a pay message
     * @property canBulkMessage
     * @type {boolean}
     * @default true
     */
    canBulkMessage: function(){
        return ( ! this.get('block_message') ) // if individual messaging is disabled for this candide then so is bulk
            && ( ! this.get('block_bulk_message') ) // individual messaging may be possible but board won't allow bulk
            && ( this.get('email') || this.get('hasPayMessage') ) // candidate also requires the bare necessities for messaging
            && ( this.get('messageEnabled') );
    }.property('stream2_block_bulk_message', 'email', 'hasPayMessage', 'messageEnabled'),
    /**
     * True if the candidate can participate in builk shortlisting:#
     * if the candidate has a CV and shortlisting is not disabled
     * @property canBulkShortlist
     * @type {boolean}
     * @default true
     */
    canBulkShortlist: function() {
        return this.get('has_cv') && this.get('shortlistEnabled');
    }.property('has_cv', 'shortlistEnabled'),
    /**
     * True if the candidate can particupate in bulk forwarding:
     * if the candidate can forward (see can_forward)
     * @property canBulkForward
     * @type {boolean}
     * @default true
     */
    canBulkForward : function() {
        return this.get('can_forward') && this.get('forwardEnabled');
    }.property('can_forward', 'forwardEnabled'),

    /* inLongLIst
       True if this candidate is in a long list at the
       time it is viewed.
    */
    inLongList: function(){
        return this.get('viewed_destination') == 'longlist';
    }.property('viewed_destination'),

    // The destination this candidate is viewed from. Default to the same
    // as the destination
    viewed_destination: function(){
        return this.get('destination');
    }.property('destination'),

    // Destination board object.
    destinationBoard: function(){
        var destination = this.get('destination');
        if( ! destination ){ return null; }
        return Stream2.Boards.findById(destination);
    }.property('destination'),

    inTalentSearch: function(){
        return ( this.get('destination') || '' ) == 'talentsearch';
    }.property('destination'),

    // Is this candidate in an internal search (a destination that is marked as internal)
    inInternalSearch: function(){
        var destination = this.get('destination');
        if( ! destination ){ return null; }
        return Stream2.Boards.findById(destination).get('type') == 'internal';
    }.property('destination'),

    unviewed: Ember.computed( '_bb_wd_viewed', function () {
        var viewed = this.get('_bb_wd_viewed');
        if ( Ember.isEmpty( viewed ) ){
            // this is not a watchdog result
            return false;
        }
        return ! parseInt(viewed);
    }),

    appDateHasChanged: function () {
        var isoDate = this.get('applicant_availability_date');
        if ( isoDate ){
            var date = new Date( isoDate );
            this.set('applicant_availability_date_epoch', ( date.getTime() / 1000 ) );
        }
    }.observes('applicant_availability_date'),

    historyUpdated: function(sender, key, value, rev) {
        this.set('hasHistory', true);
    },
    hasProfile: function () {
        return ( this.get('has_profile') || this.get('profile_chargeable') );
    }.property('has_profile','profile_chargeable'),
    profileBody: function () {
        var body = this.get('profile_body');
        if ( body ) return body;
        return "<div class='loading-candidate-data'>"+i18n.__("Please wait...")+"</div>";
    }.property('profile_body'),
    identity: function () {
        var name = this.get('name');
        var email = this.get('email');
        if ( ! name ){
            return email;
        }
        return name;
    }.property('email','name'),

    // the DownloadCV action can have different states based on availability of their CV,
    // this updates the 'title' attribute accordingly.
    downloadCVPlaceholder: function() {
        return this.get('hasCV') ?
            i18n.__('Download to desktop') :
            i18n.__('Downloading this candidate\'s CV is not possible');
    }.property('hasCV'),

    canDownloadProfile: function() {
        if (!this.get('hasProfileQuota')) {
            Stream2.popupAlert(
                i18n.__('Oops...'),
                i18n.__('Your profile viewing credits have finished for this board.')
            );
            this.set('error', {
                message: i18n.__('Your profile viewing credits have finished for this board.'),
                type: 'quota'
            });
            return false;
        }
        return this.get('hasProfile');
    }.property('hasProfile', 'hasProfileQuota'),
    /*
      canDownloadCV: Checks to see if a CV is available and the user has
      enough cv quota available on the board (if applicable) to download.
     */
    canDownloadCV: function() {
        if (!this.get('hasCVQuota')) {
            Stream2.popupAlert(
                i18n.__('Oops...'),
                i18n.__('Your CV credits have finished for this board.')
            );
            this.set('error', {
                message: i18n.__('Your CV credits have finished for this board.'),
                type: 'quota'
            });
            return false;
        }
        return this.get('hasCV');
    }.property('hasCV', 'hasCVQuota'),
    /*
      hasCV: Use this to determine whether a CV is available for the candidate,
      which is determined by two attributes.
    */
    hasCV: function() {
        return this.get('has_cv') && this.get('can_downloadcv');
    }.property('can_downloadcv', 'has_cv'),
    /*
      hasPayMessage: It is possible to show a potential pay message (meaning we might have
      to download the CV to this candidate).
    */
    hasPayMessage: function(){
        return ! this.get('email') && ! this.get('has_free_cv') && this.get('hasCV');
    }.property('hasCV', 'email', 'has_free_cv'),

    /*
      hasFreeMessage: It's possible for some boards to offer cv downloading without
      requiring payment, such as some social boards.
      It is also possible to message a candidate if we already have their email,
      as we do not need to download the CV to retrieve it.
    */
    hasFreeMessage: function() {
        return ( this.get('has_free_cv') || this.get('email') ) && this.get('hasCV');
    }.property('has_free_cv', 'email', 'hasCV'),

    calculatedLatLong: function () {
        var lat = this.get('latitude') || this.get('applicant_latitude');
        var lon = this.get('longitude') || this.get('applicant_longitude');
        if ( lat && lon ){
            return lat + "," + lon;
        }

        var zipcode = this.get('postcode') || this.get('zipcode');
        if ( zipcode ) {
            return zipcode;
        }
    }.property('latitude','longitude','applicant_latitude','applicant_longitude','postcode','zipcode'),

    /*
      Server side URL of this candidate viewed in this results_id about this viewed_destination.
      Usage:

         candidate.apiURL('future_action', 1234 ); -> /path/to/candidate/future_action/1234
     */
    apiURL: function () {
        return [ '', 'results', this.get('results_id'), this.get('viewed_destination'), 'candidate', this.get('idx') ]
            .concat( Array.prototype.slice.call(arguments, 0) )
            .join( '/' );
    },

    /* usertags section */
    usertags: [],
    usertagsChanged: function(sender, key, value, context, rev){
        var usertags = this.get('usertags');
        // Note that id here is not the primary key of the tags, but their 'tag_name'
        // therefore it is always safe to send them all.
        Stream2.request( this.apiURL( 'usertags' ), { tag_names: usertags.mapBy('id') } , 'PUT' ).then(function( data ){
            Stream2.popupAlert( data.message );
        });
    }.observes('usertags'),

    /* tags section */
    tags: null,
    removeTag: function ( tag ){
        var _this = this;

        // remove the tag from the local candidate instantly
        Stream2.popupAlert( i18n.__('Removing Tag...') );
        this.get('tags').removeObject( tag );

        Stream2.requestCandidateAsync(this, this.apiURL( 'tags' ), { tag_name: tag.get('value') }, 'DELETE' ).then(
            function () {
                Stream2.popupAlert( i18n.__('Tag Removed') );
                if( _this.get('historicActionsChronological') ){
                    _this.fetchProfileHistory();
                }
            },
            function(error){
                // put the tag back if we failed to remove it
                _this.get('tags').pushObject(tag);
                Stream2.popupError( error );
            }
        );
        Stream2.pushEvent('candidate','remove tag');
        return true;
    },
    addTag: function ( item ) {
        if ( this.get('tags').find(
            function ( tag ) {
                if ( item.value == tag.get('value') ) {
                    tag.set('new', true);
                    return true;
                }
            }
        )){
            return false;
        }

        var _this = this;

        Stream2.resetMark();
        Stream2.requestCandidateAsync(this, _this.apiURL( 'tags' ), { tag_name: item.value }, 'POST' ).then (
            function () {
                Stream2.timeMarkEvent('Candidate Actions', 'tag');
                Stream2.popupAlert( i18n.__('Added') );
                _this.get('tags').pushObject( Ember.Object.create({ value: item.value , "new": true , flavour: item.flavour }) );
                if( _this.get('historicActionsChronological') ){
                    _this.fetchProfileHistory();
                }
            },
            function ( error ){
                Stream2.popupError( error );
            }
        );

        Stream2.pushEvent('candidate','add tag');
        return true;
    },
    /* end of tags section */

    relativeTime: function () {
        var time_found = this.get('time_found');
        if ( time_found ){
            return moment( time_found ).calendar();
        }
    }.property('time_found'),

    _applicationHistory: null,

    // Given an array of application history items,
    // returns the ones to really set here.
    // See AdcResponsesCandidate for a non default implementation.
    filterApplicationHistory: function( history ){
        return history;
    },

    applicationHistory: function () {
        var _this = this;
        if ( Ember.isNone( this.get('_applicationHistory') ) ) {
            Stream2.request( _this.apiURL( 'application_history' ), {}, 'GET' ).then (
                function ( data ){
                    var app_history = data.application_history.length ? data.application_history : [];
                    app_history = _this.filterApplicationHistory( app_history );
                    // Moved the 'true' hack here.
                    // This is there because the template tests for app_history 'trueness' to
                    // decide if it has been loaded or not. And empty array is false in js.
                    _this.set('_applicationHistory', app_history.length ? app_history : true );
                },
                function ( error ){
                    Stream2.popupError( error );
                    _this.set('error', error);
                }
            );
        }
        return this.get('_applicationHistory');
    }.property('_applicationHistory'),

    detailsPartialName: 'profile/details',

    actionsChronologicalSansProfileView: function () {
        console.log( this.get( 'historicActionsChronological' ) );
        return this.get('historicActionsChronological').filter( function ( item ) {
            return item.action !== 'profile';
        });
    }.property('historicActionsChronological.@each'),

    hasCVQuota: function() {
        if (this.get('has_done_download')) return true;

        var board = this.get('destinationBoard');
        return board.get('quota.canDownloadCV');
    }.property('destinationBoard.quota.canDownloadCV', 'has_done_download'),
    hasProfileQuota: function() {
        if (this.get('has_done_profile')) return true;

        var board = this.get('destinationBoard');
        return board.get('quota.canViewProfile');
    }.property('destinationBoard.quota.canViewProfile', 'has_done_profile'),

    /* When one of these actions gets completed successfully, triggers a request
        to retrieve the latest quota information. A request is made due to the
        ambiguity of whether some actions will actually require a download, so
        let the backend supply the data.
    */
    observeCVQuota: function() {
        Ember.run.next(this, function() {
            this.get('destinationBoard.quota').updateQuota();
        });
    }.observes('has_done_download'),
    observeProfileQuota: function() {
        Ember.run.next(this, function() {
            this.get('destinationBoard.quota').updateQuota();
        });
    }.observes('has_done_profile')
});

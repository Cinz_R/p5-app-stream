/* Boards factory. Also contain a list of all the boards
   accessible from the current instance of the whole application,
   and a few helper class methods.
*/

Stream2.Boards = Stream2.Model.extend({});

// This implements stuff at class level (not at instance level)
Stream2.Boards.reopenClass({
  findById: function(id) {
    // this assumes the list has loaded :(
    var found = this.find().filterProperty('name', id);
    var first = found.get('firstObject');
    return first;
  },

  // Called without params, will load all the boards in this._boards
  find: function(params) {
    if ( params && params.board_id ) {
      return this.findById(params.board_id);
    }

    // if we have already loaded the list of boards, return it
    if (this._boards) { return this._boards; }

    var _this = this;
    var collection = Em.A();
    Stream2.boardPromise = Stream2.request('/api/boards/active.json');
    Stream2.boardPromise.then(
        function(data) {
            data.boards.forEach(function (board) {
                var model = Stream2.Board.create();

                if (board.user_quotas) {
                    model.set('quota', Stream2.Quota.create(board.user_quotas));
                    model.set('quota.destination', board.name);
                }

                model.setProperties(board);
                collection.pushObject(model);
            });

            /* A longlist board is an INTERFACE ONLY
               board. All the users have it and its only
               there to provide access to longlists.

               Note that a user NEVER holds a subscription to a longlist.
            */
            var longlist_board = Stream2.Board.create({ nice_name: 'DO NOT DISPLAY THIS',
                                                        name:'longlist',
                                                        type: 'hidden',
                                                      });
            collection.pushObject(longlist_board);
            collection.set('loaded', true);
        },
        null
    );

    this._boards = collection;
    return collection;
  },
  _reset: function() {
    delete this._boards;
  },


  /* Pick the first non hidden and non disabled board
     from the collection
  */
  firstSelectableBoard: function(){
      var boards = this.find();
      return boards.filter(function(item){
          if( item.get('disabled') ){ return false; }
          if( item.get('isHidden') ){ return false; }
          return true;
      })[0];
  },

  /* make sure the selected board is selectable (i.e. non hidden), or select one
     appropriate board if none is selected
   */
  selectFirstSelectable: function(){
      var boards = this.find();
      var selectedBoard = boards.filterBy('selected')[0];
      if(  ( ! selectedBoard ) || selectedBoard.get('isHidden') ){
          if( selectedBoard ){ selectedBoard.set('selected' , false); }
          // Pick the first appropriate one and select it.
          this.firstSelectableBoard().set('selected',true);
      }
  }
});

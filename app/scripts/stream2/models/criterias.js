// Criterias _FACTORY_

Stream2.Criterias = Ember.Object.extend({});

// defaults ( object ) can be passed in to impose default values on the criteria model
Stream2.Criterias.reopenClass({
    findById: function( criteria_id, defaults ){
        if ( Ember.isNone( defaults ) ) {
            defaults = {};
        }
        var criteria = Stream2.Criteria.create({ id: criteria_id });
        Stream2.request( '/search_json/' + criteria_id + '/criteria', {} , 'GET')
            .then(function(data){
                $.map( defaults, function ( val, key ) {
                    if ( Ember.isNone( data[key] ) ){
                        data[key] = val;
                    }
                });
                criteria.deserialise( data );
            });
        return criteria;
    }
});

Stream2.TagOptions = Ember.Object.extend({});

Stream2.TagOptions.reopenClass({
    /*
      Returns a promise fullfilled with the found Stream2.TagOption or
      rejected with an error.
     */
    findByValue: function(tag_value){
        return Stream2.request('/api/admin/tags/tag/info', { tag_name: tag_value }, 'GET').then(
            function(data){
                var tagOption = Stream2.TagOption.create({value: tag_value});
                tagOption.setProperties(data.tag);
                var whatever = tagOption.get('hasNCandidates'); // Force recomputation of hasNCandidates
                return tagOption;
            }
        );
    }
});
Stream2.TagOption = Ember.Object.extend({
    flavour: null,
    label: null,
    value: null,
    isHotlist: null,
    n_candidates: null,

    id: function(){
        return this.get('value');
    }.property('value'),

    hasNCandidates: function(){
        return this.get('n_candidates') !== null;
    }.property('n_candidates'),

    formattedNCandidates: function(){
        if( ! this.get('hasNCandidates') ){ return null; }
        return numeral( this.get('n_candidates') ).format('0,0');
    }.property('n_candidates'),

    flavourClass: function () {
        var flavour = this.get('flavour') || 'default';
        return "tag-flavour-" + flavour;
    }.property('flavour'),

    hotlistChanged: function () { // will one day be flavourChanged
        var _this = this;
        var options = {
            tag_name: this.get('value'),
            flavour_name: this.get('isHotlist') ? 'hotlist' : ''
        };
        Stream2.request('/api/admin/tags/tag/set_flavour', options, 'POST').then(
            function () {
                Stream2.popupAlert( i18n.__('Tag type set') );
                _this.set('flavour', options.flavour_name);
                _this.set('isHotlist', ( options.flavour_name == 'hotlist' ) );
            },
            function ( error ) {
                Stream2.popupError( error );
            }
        );
    }.observes('isHotlist'),

    flavourNotChangeable: function () {
        return ( this.get('flavour') === 'shortlist' );
    }.property('flavour')
});

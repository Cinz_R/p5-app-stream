/* Stream2 SearchUser funky model */
Stream2.SearchUser = Ember.Object.extend( Stream2.UserFlagMixin, {

    // All the ripple settings in one go.
    ripple_settings: null,
    // The last used longlist. Only persist in the interface (for now)
    last_used_longlist: null,
    // List of longlists for this user. Also stored serverside.
    long_lists: [],

    // Long list
    long_lists_limit: 0,

    cbOneIAMUser: function(){
        return this.get('ripple_settings').cb_oneiam_user;
    }.property('ripple_settings.cb_oneiam_user'),

    isZombie: function() {
        return this.get("contact_details.office") == 'zombie';
    }.property("contact_details.office"),

    subscriptions: function () {
        return Stream2.Boards.find();
    }.property(),
    selectedBoard: function() {
        var selected = this.get("subscriptions").findProperty("selected");
        if ( selected ) {
            return selected.get("id");
        }
    }.property('subscriptions.@each.selected'),

    // returns an HTML class name reflecting whether this user can access any
    // of the internal boards
    talentSearchStatus: function() {
        var hasTS = Stream2.Boards.findById('talentsearch');
        var hasMS = Stream2.Boards.findById('cb_mysupply');
        var hasCS = Stream2.Boards.findById('candidatesearch');
        return hasTS || hasMS || hasCS
            ?  'has-talent-search' : 'no-talent-search';
    }.property(),

    // returns an HTML class name reflecting whether the currently selected
    // channel is Responses
    responsesStatus: function() {
        var isResponses = this.get("manageResponsesView")
            || this.get('selectedBoard')
            == "adcresponses";
        return isResponses ? 'is-responses' : 'not-responses';
    }.property("manageResponsesView","selectedBoard"),

    hasTalentSearch: function() {
        return this.get("talentSearchStatus") == "has-talent-search";
    }.property("talentSearchStatus"),

    isResponsesChannel: function() {
        return this.get("responsesStatus") == 'is-responses';
    }.property("responsesStatus"),

    restrictUI: function() {
        return this.get("isResponsesChannel") && !this.get("hasTalentSearch");
    }.property("isResponsesChannel", "hasTalentSearch"),

    // Can the user add a longlist?
    canAddLongList: function(){
        var user = this;
        return user.get('long_lists').length < user.get('long_lists_limit');
    }.property('long_lists.@each'),

    /* add_longlist

       Adds the given Stream2.LongList object to this users longlist list
       or updates the one that already exists by ID

     */
    add_longlist: function(longlist){
        // Try to hit a longlist with the same ID
        var already  = this.get('long_lists').findBy('id' , longlist.get('id') );
        if( already ){
            already.setProperties( longlist.getProperties() );
            return already;
        };
        this.get('long_lists').pushObject(longlist);
        return longlist;
    },

    removeLongList: function(longlist){
        this.get('long_lists').removeObject(longlist);
    },

    setProperties: function(properties){

        // Turn the long_list plain JS structure in objects
        var long_lists = properties.long_lists || [];
        var long_list_objects = long_lists.map(function(hash){
            return Stream2.LongList.create(hash);
        });
        properties.long_lists = long_list_objects;

        return this._super(properties);
    },

    hasManagesResponses: function () {
        return ! Ember.isNone( Stream2.Boards.findById('adcresponses') );
    }.property(),

    /* manageResponsesView
        flag on whether they are using search through a manage response context
    */
    manageResponsesView: function () {
        return ( this.get('search_mode') === 'responses' );
    }.property('search_mode'),

    search_modeChanged: Ember.observer('search_mode', function(){
        // Can be 'search' or 'responses'
        ga('set', 'dimension3', this.get('search_mode') );
    }),

    /*
      Same as currencyList, except it contains empty element as a correct
      value for undefined currency properties.
     */
    maybeCurrencyList: function(){
        return [ [ '' , i18n.__('--') ] ].concat( this.get('currencyList') );
    }.property('currencyList'),

    /* currencyList
        returns the users available currencies in the correct format with translations
    */
    currencyList: function () {
        var list = this.get('currencies').map( function ( item ) {
            return [ item, i18n.__( item ) ];
        });
        return list;
    }.property('currencies'),
    cbOneIAMAuthCode: function() {
        /*
        * Assumptions made here about user SSO session:
        *
        *   1. current authcode is valid.
        *   2. current authcode belongs to current active user.
        *
        * How? Because:
        *   1. If user is active on browser then user is guaranteed to be the original user who initiated SSO session.
        *   2. If user is semi-active on browser, that is if user's browser/tab regains focus, then we already performed session check.
        *
        * Compromise: It is possible that a race event will take place such that user can perform an action on UI before session validation is done.
        * This case will happen if user's connection is slow such that user-driven events outperformed async/promise check.
        * this will need to be included for new actions as well.
        */
        var cbOneIAMUser = this.get('cbOneIAMUser');
        if( cbOneIAMUser ) {
            return Stream2.get('cbOneIamAuth').getAuthCode();
        }

        return;
    },

});

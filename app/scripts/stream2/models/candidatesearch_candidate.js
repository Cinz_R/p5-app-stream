/*
    Stream.CandidatesearchCandidate

    A subclass of the MutableCandidate model
    Has editable fields which are specific to the candidatesearch feed
*/
Stream2.CandidatesearchCandidate = Stream2.MutableCandidate.extend({
    handlebars_template: 'board/result/candidatesearch',
    editable_keys: [
        'name', 'email', 'telephone', 'mobile', 'first_name', 'last_name', 'phone',
        'broadbean_notice_period', 'broadbean_advert_job_type', 'broadbean_desired_salary_currency', 'broadbean_desired_salary_minimum',
        'broadbean_desired_salary_range_top', 'broadbean_desired_salary_pay_period', 'broadbean_recruitment_status', 'broadbean_advert_industry',
        'broadbean_applicant_availability_time'
    ]
});

/**
 * Model for a board-specific search filter.
 * @class Facet
 * @namespace Stream2
 * @extends Ember.Object
 */
Stream2.Facet = Ember.Object.extend({
    id        : null,
    type      : null,
    size      : null,
    label     : null,
    prevValue : null,

    Name      : null,

    /**
     * Alternative to Default. Takes priority
     * @property Value
     * @default null
     */
    Value     : null,

    /**
     * The default value of the facet
     * @property Default
     * @default null
     */
    Default   : null,

    Options   : null,

    SelectedValues: null,

    /**
     * True if the facet should be expaned initially.
     * @property StartShown
     * @default null
     */
    StartShown: null,

    /**
     * Modify how the facet is displayed. Current options:
     *
     *     'major_filter'
     *
     * @property DisplayStyle
     * @default ''
     * @type string
     */
    DisplayStyle: '',

    // set to true if facet comes sorted or is done in the backend
    isSorted  : false,

    isTag: function () {
        return ( this.get('Type') === 'tag' );
    }.property('Type'),

    isAdvert: function () {
        return ( this.get('Type') === 'advert' );
    }.property('Type'),

    isFlagging: function () {
        return ( this.get('Name') === 'broadbean_adcresponse_rank' );
    }.property('Name'),

    // This is about 'How it should be displayed'
    isList: function () {
        if ( this.get('_isList') ){
            return true;
        }
        return false;
    }.property('Type'),

    isGroupedMultiList: function () {
        if ( this.get('Type') === "groupedmultilist" ){
            return true;
        }
        return false;
    }.property('Type','Options'),
    isGroupedList: function () {
        if ( this.get('Type') === "groupedlist" ){
            return true;
        }
        return false;
    }.property('Type','Options'),

    // This is about the real data type.
    _isList: function() {
        return this.get('Type') === "list" || this.get('Type') === "sort";
    }.property('Type'),

    isEmptyList: function() {
        return ( this.get('_isList')  || this.get('_isMultiList') ) && this.get('Options').length === 0;
    }.property('_isList', '_isMultiList', 'Options'),

    isTextField : function() {
        return this.get('Type') === 'text';
    }.property('Type'),
    isTextArea : function() {
        return this.get('Type') === 'textarea';
    }.property('Type'),

    // checkbox facets
    // This is about the real datatype
    _isMultiList: function() {
        return this.get('Type') === 'multilist';
    }.property('Type'),

    // This is about 'how should it be displayed'.
    isMultiList: function () {
        if ( this.get('_isMultiList') && this.get('Options').length <= 10 ){
            return true;
        }
        return false;
    }.property('_isMultiList', 'Options'),

    // This is about 'how should it be displayed'
    isBigList: function () {
        if ( this.get('_isMultiList') && this.get('Options').length > 10 ){
            return true;
        }
        return false;
    }.property('_isMultiList', 'Options'),
    isHeading: function () {
        return this.get('Type') === 'heading';
    }.property('Type')
});

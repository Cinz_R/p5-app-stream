Stream2.UserHierarchyMixin = Ember.Mixin.create({
    /*
        _vivifyModel

        converts primitive data structure into a nested set of ember objects
        ready to be observed
    */
    _vivifyModel: function ( model, user_map ) {
        var _this = this;
        model = Ember.Object.create( model );

        if ( model.get('children') ){
            var children = model.get('children');
            model.set('children', Ember.A() );
            $.each( children, function ( index, item ) {
                model.get('children').pushObject( _this._vivifyModel( item, user_map ) );
            });
        }
        if ( model.get('id') ){
            // create flat map of user objects
            user_map.get('map')[model.get('id')] = model;
            user_map.get('ids').push(model.get('id'));
        }

        return model;
    },
    /*
        _readChildren

        iterates through hierachy from given level ( e.g. team )
        extracts boolean_value for leaf nodes
    */
    _readChildren: function ( user, idList ){
        var _this = this;
        if ( Ember.isNone( idList ) ) idList = [];
        if ( user.id ){
            idList.push( user.id );
        }
        if ( user.children ){
            $.each( user.children, function ( index, child ) {
                idList.concat( _this._readChildren( child, idList ) );
            });
        }
        return idList;
    },
    /*
        _userValue

        depending on the type of the setting
    */
    _userValue: function ( user ) {
        if ( this.controller.get('isBool') ){
            return user.get('boolean_value');
        }
        return user.get('string_value');
    }
});

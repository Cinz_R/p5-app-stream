// Mix that anything to get instant error and warnings properties and predicates
ErrorMixin = Ember.Mixin.create({

    error: null,
    warnings: null,

    hasError: function () {
        return this.get('error') ? true : false;
    }.property('error'),
    hasWarnings: function(){
        return ! Ember.isEmpty( this.get('warnings') );
    }.property('warnings'),

    errorThrown: function () {
        if( this.get('hasError') ){
            this.onError();
        }
    }.observes('error'),
    onError: function (){}
});

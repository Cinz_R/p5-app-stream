AjaxMixin = Ember.Mixin.create({
    _poll_interval: 1000,

    isTrue: function( item ) {
        if ( typeof( item ) === "undefined" ) return false;
        if ( Ember.isEmpty(item) ) return false;
        if ( item === 0 ) return false;
        return true;
    },
    setTimeout: function ( callback, time ){
        this._clearTimeout();
        this.set('_timer', Ember.run.later( this, callback, time  ));
    },

    incAjax: function(){
        this.currentAjaxCountObject.incrementProperty('count');
    },
    decAjax: function(){
        this.currentAjaxCountObject.decrementProperty('count');
    },

    _xhr : null,
    _timer: null,
    _clearAjax: function (){
        var xhr = this.get('_xhr');
        if ( xhr === null ){ return; }
        // Dont do anything when the readyState says the request is over
        if( xhr.readyState === 4 ){ return; }

        // We do this here, because the Ajax failure callback
        // in _ajaxMakeRequest will not percolate up the logical level
        // of .request, .requestAsync and .requestSingle
        // will not percolate up
        Ember.run(this,function(){ this.decAjax(); });
        xhr.abort();
    },
    _clearTimeout: function () {
        if ( this.get('_timer') !== null ){
            Ember.run.cancel( this.get('_timer') );
        }
    },

    // This is the low level wrapped around jQuery standard $.ajax
    // It adds a bit of cleverness to the error handling.
    _ajaxMakeRequest: function ( url, data, callback, failure, method, url_params ) {
        var _this = this;


        if (!method){ method = 'POST'; }
        if ( typeof( failure ) === "undefined" || failure === null ){
            failure = function( error_data){ Ember.run(this,function(){ console.log(error_data); });};
        }

        data = data || {};
        url_params = url_params || {};

        if ( ! data ) data = {};
        // Calculate actual sent parameters
        var actual_params = { q: JSON.stringify(data),
                              ts: ( new Date() ).getTime() // The time stamp of the query. Avoids caching
                            };
        for( var extra_key in url_params ){
            actual_params[extra_key] = url_params[extra_key];
        }


        var failure_callback = (function(failure){
            return function(xhr,textStatus, error){

                // if xhr.status is 0 then the ajax request has been aborted through user action
                if ( ! xhr.status ) return;

                var error_data;
                try{
                    error_data = $.parseJSON(xhr.responseText);
                }catch( e ){
                    error_data = { error: "General server error: " + error };
                }
                console.log("---- AJAX ERROR ----");
                console.log(textStatus);
                console.log(error);

                // Conflictual request
                // For instance inconsistency in logged in user. The server will tell in
                // the response text.
                if( xhr.status == 409 ){
                    if( stream2_current_user_login_provider == 'adcourier' ){
                        if( confirm('Conflict detected: ' + error_data.error.message + ' Press OK to reload this application.') ){
                            window.location.reload();
                        }
                    }else{
                        // We are not directly in adcourier (we are in an ats)
                        alert('Conflict detected: ' + xhr.responseText + '. Please reload this page.');
                    }
                    return;
                }


                if( xhr.status == 403 ){
                    // TODO: Replace alert by whatever nicity we choose.
                    alert( i18n.__("Your session has expired. This application will reload itself.") );
                    window.location = '/' ;
                    return;
                }

                if( xhr.status == 502 ){
                    Stream2.pushEvent('exception','gateway error');
                    return failure.apply(this, [ { error: i18n.__("The server is not available right now. Please try again.") } ] );
                }


                if ( xhr.status == 404 ){
                    Stream2.pushEvent('exception','page not found');
                    return failure.apply(this, [ { error: i18n.__("Page not found") } ] );
                }


                failure.apply(this, [ error_data ] );
            };
        })(failure);


        return $.ajax({
            type: method,
            url: this.getUrl(url),
            data: actual_params,
            dataType: "json",
            success: callback,
            error: failure_callback
        });
    },
    fileDownload: function ( url ) {
        var _this = this;
        return new Ember.RSVP.Promise( function ( resolve, reject ){
            _this.incAjax();
            $.fileDownload(url, {
                successCallback: function ( url ) {
                    _this.decAjax();
                    resolve();
                },
                failCallback: function (html, url) {
                    _this.decAjax();
                    // The html is the content which is generated by an IFRAME, json is rendered using pre tags in chrome
                    var error = null;
                    try {
                        var json = JSON.parse( html.match(/({[^}]+}+)/)[0] );
                        error = json.error;
                    } catch ( e ){
                        error = html;
                    }
                    reject( error );
                }
            });
        });
    },
    // Returns an Ember.RSVP.Promise that can have a 'resolve' handler (called with the JSON decoded data),
    // and an error handler (called with the error message).
    //
    // params WILL turn into a json href
    // under the URL parameter 'q'
    //
    // url_params will turn into plain old url_params
    //
    request : function ( url, params, method, url_params, opts ){
        var _this = this;
        if(! opts ){ opts = {}; }
        if(! opts.leave_ajax_alone ){ _this.incAjax(); }
        return new Ember.RSVP.Promise(
            function ( resolve, reject ){
                _this._ajaxMakeRequest(
                    url,
                    params,
                    function ( data ){
                        if(! opts.leave_ajax_alone ){
                            Ember.run(function(){ _this.decAjax(); });
                        }
                        Ember.run( _this, resolve, data );
                    },
                    function ( error_data ){
                        if(! opts.leave_ajax_alone ){
                            Ember.run(function(){ _this.decAjax(); });
                        }
                        Ember.run( _this, function(){ reject( error_data.error ); });
                    },
                    method,
                    url_params
                );
            });
    },

    /**
     * Same as requestAsync but with a first argument being the candidate
       to be updated.
     * @method requestCandidateAsync
    */
    requestCandidateAsync: function( candidate, url, params, method, url_params ){
        return this.requestAsync( url , params , method, url_params ).then(
            function( result ){
                candidate.set('warnings', result.warnings);
                candidate.set('recent_actions', result.echo_candidate.recent_actions );
                if( Ember.isPresent( result.echo_candidate.email )){
                    candidate.set('email', result.echo_candidate.email );
                }
                candidate.set('future_message', result.echo_candidate.future_message );
                // No error occured.
                candidate.set('error',null);
                return result;
            },
            function ( error ) {
                candidate.set('error',error);
                // Ember says throw the error for the next error handler to handle it.
                throw error;
            }
        );
    },

    /*
      Sent the params to the given URL using the given method and
      returns a Promise to be fullfilled later.

      Expects a status_id back from the URL. It will then
      poll regularly to fullfull this query.

      Note that from the point of view of the caller, this has the SAME
      API as the 'request' function.
     */
    requestAsync: function( url, params , method, url_params ){
        var _this = this;
        _this.incAjax();
        // _this.incrementProperty('currentAjaxCount');
        return _this.request( url, params , method, url_params, { leave_ajax_alone: true } ).then(
            function( data ){
                if( ! data.status_id ){
                    _this.decrementProperty('currentAjaxCount');
                    return new Ember.RSVP.Promise( function( resolve, reject ){
                        reject("Missing data.status_id in returned data");
                    });
                }
                return new Ember.RSVP.Promise( function( resolve, reject ){
                    var timeTried = 0;
                    var pollAsyncJob = function(){
                        // Poll the status NOW. You never know
                        _this.request( '/api/asyncjob/' + data.status_id + '/status', {} , 'GET', { leave_ajax_alone: true } ).then(
                            function(data){
                                if( data.status === 'completed' ){
                                    // We resolve with the results of the action, as we
                                    // would resolve directly with the data coming back
                                    // from the synchronous action in Stream2.request.
                                    _this.decAjax();
                                    resolve( data.context.result );
                                }else if( data.status === 'failed' ){
                                    _this.decAjax();
                                    reject( data.context.error );
                                }else{
                                    // Status is not completed or failed yet. Do the same thing
                                    // in one second + some random time that grows with time.
                                    Ember.run.later( _this ,  pollAsyncJob , 1000 + Math.round( timeTried * Math.random() * 200 ) );
                                    timeTried++;
                                }
                            },
                            function(error){
                                _this.decAjax();
                                reject( error );
                            }
                        );
                    };
                    pollAsyncJob();
                });
            },
            function( errorString ){
                _this.decAjax();
                return new Ember.RSVP.Promise( function (resolve, reject){
                    // Return a promise just for the sake of consistency
                    reject( errorString );
                });
            }
        );
    },

    // Returns an Ember.RSVP.Promise that can have a 'resolve' handler (called with the JSON decoded data),
    // and an error handler (called with the error message).
    singleRequest : function ( url, params, method , url_params ){ // enforces only one concurrent request
        this._clearAjax();
        this._clearTimeout();
        var _this = this;
        _this.incAjax();

        return new Ember.RSVP.Promise( function ( resolve, reject ){
            var ajax = Stream2._ajaxMakeRequest( url, params,
                                                 function ( data ){
                                                     Ember.run(function (){
                                                         _this.decAjax();
                                                         resolve(data);
                                                     });
                                                 },
                                                 function ( error_data ){
                                                     Ember.run( _this, function(){
                                                         _this.decAjax();
                                                         reject( error_data.error );
                                                     });
                                                 },
                                                 method,
                                                 url_params
                                               );
            _this.set('_xhr', ajax );
        });
    },
    error: function ( errMsg ){
        console.log( errMsg );
        return errMsg;
    },
    stat: function ( stat ){
        console.log( stat );
        return stat;
    }
});

/*

    ########################
    # Stream2.PromiseGroup #
    ########################

    Want to wait until a group of asynchronous tasks have been completed before continuing?

    Stream2.PromiseGroup.collect(
        promise1,
        promise2,
        ...
        promise4321
    ).then(
        function ( outcomes ){

            var promise1_outcome = outcomes.shift();
            var promise2_outcome = outcomes.shift();
            ...
            var promise4321_outcome = outcomes.shift();

            if ( promise1_outcome.success )
                do_something_awesome( promise1_outcome.success );
            else
                throw_error( promise1_outcome.error );

        }
    );
*/

Stream2.PromiseGroup = Ember.Object.extend();
Stream2.PromiseGroup.reopenClass({
    collect: function () {
        var _args = arguments;
        return new Ember.RSVP.Promise( function ( resolve, reject ) {
            var promises = Array.prototype.slice.call( _args, 0 );
            var outcomes = [];
            var cb = function () {
                var c = true;
                if ( Ember.isEmpty( promises.rejectBy( '_state' ) ) ){
                    resolve( outcomes );
                }
            };
            $(promises).each( function ( index ) {
                if ( ! this || ! this._subscribers ){
                    return 1;
                }
                this.then(
                    function ( success ) {
                        outcomes[index] = { success: success };
                        cb();
                    },
                    function ( error ) {
                        outcomes[index] = { error: error };
                        cb();
                    }
                );
            });
        });
    }
});

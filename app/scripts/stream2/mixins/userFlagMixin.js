/*
 * UserFlagMixin
 * Contains methods and properties which control
 * the display of adc response flagging for a user
 */
Stream2.UserFlagMixin = Ember.Mixin.create({
    // this is populated from user deatils.
    adcresponse_flags: [],

    flags: function () {
        var _this = this;
        return this.get('adcresponse_flags').map(
            function ( flag ) {
                return {
                    value: flag.flag_id,
                    style: 'color: ' + flag.colour_hex_code,
                    active: _this._is_flag_active( flag ),
                    setting_mnemonic: flag.setting_mnemonic,
                    description: flag.description
                };
            }
        );
    }.property('adcresponse_flags'),

    /*
        _is_flag_active
        Don't display flags if they are inactive or they have
        been set as disabled by the admin
    */
    _is_flag_active: function ( flag ) {
        if ( Ember.isPresent( flag.setting_mnemonic ) ){
            var settings = this.get(flag.type === "standard" ? 'settings' : 'settings.groupsettings');
            return !! settings[flag.setting_mnemonic];
        }
        return false;
    },

    activeFlags: function () {
        return this.get('flags').filterBy('active',true);
    }.property('flags'),

    flagRankToStyle: function ( rank ) {
        var flag = this.get('flags').findBy('value', parseInt(rank));
        if ( flag ){
            return flag.style;
        }
        return 'color: #000000';
    }
});

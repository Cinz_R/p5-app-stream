AnalyticsMixin = Ember.Mixin.create({

    /* 
        time debug methods

        use:

            Stream2.resetMark();
            // send ajax request
            Stream2.timeMark('polling');
            // poll
            Stream2.timeMark('polling');
            // data
            Stream2.timeMarkEvent('category','label');

        output:

            console.log( 'polling - 1023ms' );
            console.log( 'polling - 2042ms' );
            console.log( 'category: label - 4221ms' );
            // imports timed event to analytics

    */
    _LOADTIMESTART: new Date(),
    timeMark: function ( subject ) {
        var date_now = new Date();
        console.log( subject + " - " + (date_now - this._LOADTIMESTART) + "ms");
    },
    timeMarkEvent: function ( category, label ) {
        var date_now = new Date();
        var diff = (date_now - this._LOADTIMESTART);
        this.timeMark( category + ": " + label );
        this.timingEvent( category, label, diff );
    },
    resetMark: function () {
        var date_now = new Date();
        this._LOADTIMESTART = new Date();
    },

    ga_defined: function () {
        return typeof( ga ) == "function";
    },
    /*
        pushEvent

        log an event in google analytics, such as a user action

        use:
            Stream2.pushEvent( 'candidate', 'download' );
    */
    pushEvent: function (category,action,label,value) {
        if ( this.ga_defined() ){
            ga('send', 'event', category, action, label, value);
        }
    },
    /*
        timingEvent

        allows logging of timing for events in google analtics
        time should be given in ms

        use:
            Stream2.timingEvent( 'Results Loading', 'talentsearch', 2200 );
    */
    timingEvent: function ( category, label, value ) {
        if ( this.ga_defined() ){
            ga('send', 'timing', category, label, value);
        }
    }
});

Stream2.AdminEmailtemplatesEditRoute = Ember.Route.extend(Stream2.UserHierarchyMixin, {
    templateName: 'admin/emailtemplates/edit',
    setting_mnemonic: function(id) {
        return 'email_template-' + id + '-active'
    },
    model: function(params) {
        var _this = this;
        return new Ember.RSVP.Promise(function(resolve, reject) {
            Stream2.PromiseGroup.collect(
                Stream2.request('/api/user/email-templates/' + params.template_id, {}, 'GET'),
                Stream2.request('/api/admin/user_hierarchy', {}, 'GET'),
                Stream2.request('/api/admin/user_groupsettings/' + _this.setting_mnemonic(params.template_id), {}, 'GET')
            ).then(
                function(outcomes) {

                    // reject if any of the calls fail
                    outcomes.forEach(function(outcome) {
                        if (Ember.isPresent(outcome.error)) {
                            return reject(outcome.error);
                        }
                    });

                    var email_templates_data = outcomes.shift();
                    var hierarchy_data = outcomes.shift();
                    var setting_map = outcomes.shift();

                    email_templates_data = email_templates_data.success;
                    setting_map = setting_map.success;


                    var userMap = Ember.Object.create({ map: {}, ids: [] });
                    hierarchy_data = _this._vivifyModel(hierarchy_data.success, userMap);

                    $.each(userMap.get('ids'), function(index, id) {
                        userMap.get('map')[id].setProperties({
                            current_setting: true,
                            boolean_value: true
                        });
                    });

                    $.each(setting_map.users_map, function(index, setting) {
                        $.each(setting.users, function(index, id) {
                            var user = userMap.get('map')[id];
                            if (user) {
                                user.setProperties({
                                    current_setting: setting.value.boolean_value,
                                    boolean_value: setting.value.boolean_value
                                });
                            }
                        });
                    });

                    return resolve({
                        email_template: email_templates_data,
                        hierarchy: hierarchy_data,
                        _userMap: userMap,
                        autoExpand: true
                    });
                },
                function(error) {
                    return reject(error);
                }
            );
        });
    },
    setupController: function(controller, model) {
        // unpack the email data
        var template = model.email_template;

        var templateRules = model.email_template.templateRules.map(function( item ){
            return Stream2['EmailTemplateRule' + item.role_name].create( item );
        });
        
        controller.setProperties({
            "templateId": template.id,
            "templateName": template.templateName,
            'emailReplyto': template.emailReplyto,
            'emailBcc': template.emailBcc,
            'emailSubject': template.emailSubject,
            'emailBody': template.emailBody,
            'templateRules' : templateRules
        });
        this._super(controller, model);
    },
    actions: {
        deleteTemplate: function() {
            var _this = this;
            if (confirm(i18n.__('Are you sure?'))) {
                return Stream2.request(
                    '/api/admin/email-templates/' +
                    _this.controller.get('templateId'), {}, 'DELETE').then(
                    function(data) {
                        Stream2.popupAlert(
                            i18n.__("Template deleted"), data.message);
                        _this.transitionTo('admin.emailtemplates.index');
                    },
                    function(error) {
                        _this.controller.set('error', error);
                        return false;
                    }
                );
            }
        }
    }
});

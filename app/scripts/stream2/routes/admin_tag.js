Stream2.AdminTagRoute = Ember.Route.extend({
    model: function(model){
        return Stream2.TagOptions.findByValue(model.value).then(
            function( tagOption ){
                return tagOption;
            },
            function( error ){
                return null;
            }
        );
    },
    setupController: function(controller, tagOption){
        var _this = this;
        if ( tagOption ){
            controller.set('content', tagOption );
        }
        else{
            alert( i18n.__("Tag not found. Has it been deleted by someone else?"));
            _this.transitionTo('/admin/tags/1');
        }
    }
});

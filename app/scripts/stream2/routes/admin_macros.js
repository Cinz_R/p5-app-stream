Stream2.AdminMacrosIndexRouteOnActionOptions = [
    { "label": "- No action -",        "value": null },
    { "label": "Preview Candidate",    "value": "Stream2::Action::DownloadProfile" },
    { "label": "Forward Candidate",    "value": "Stream2::Action::ForwardCandidate" },
    { "label": "Download Candidate",   "value": "Stream2::Action::DownloadCV" },
    { "label": "Message Candidate",    "value": "Stream2::Action::MessageCandidate" },
    { "label": "Updated Candidate",    "value": "Stream2::Action::UpdateCandidate" },
    { "label": "Save Candidate",       "value": "Stream2::Action::ImportCandidate" },
    { "label": "Shortlist Candidate",  "value": "Stream2::Action::ShortlistCandidate" },
    { "label": "Response Received",    "value": "Stream2::Action::NewBBCSSResponse" }
];

Stream2.AdminMacrosIndexRoute = Ember.Route.extend(Stream2.UserHierarchyMixin,{
    queryParams: {
        page: {
            refreshModel: true
        }
    },
    model: function ( params ) {
        return new Ember.RSVP.Promise(function(resolve, reject){
            Stream2.request( '/api/admin/group_macros' , {}, 'GET').then(
                function( data ){ resolve( data ); },
                function( error ){ reject( error ); }
            );
        });
    }
});


Stream2.AdminMacrosNewRoute = Ember.Route.extend(Stream2.UserHierarchyMixin,{
    templateName: 'admin/macros/edit',
    model: function( params ){
        return { macro: Stream2.Macro.create(), macroOnActionOptions: Stream2.AdminMacrosIndexRouteOnActionOptions }; 
    },
    actions:{
        saveMacro: function(){
            var _this = this;
            var macro = this.controller.get('macro');
            macro.save().then(function(){
                _this.replaceWith('admin.macros.edit' , macro.get('id'));
            });
        }
    }
});

Stream2.AdminMacrosEditRoute = Ember.Route.extend(Stream2.UserHierarchyMixin,{
    model: function ( params ) {
        // Its an existing macro. Load it.
        return new Ember.RSVP.Promise(function(resolve){
            Stream2.request( '/api/admin/group_macros/' + params.macro_id , {}, 'GET').then(
                function( data ){
                    resolve( { "macro": Stream2.Macro.create( data.data ), macroOnActionOptions: Stream2.AdminMacrosIndexRouteOnActionOptions } );
                }
            );
        });
    },
    actions:{
        deleteMacro: function(){
            var _this = this;
            if( confirm(i18n.__("Are you sure you want to delete this macro?") ) ){
                this.controller.get('macro').delete().then(function(){
                    _this.replaceWith('admin.macros');
                });
            }
        },
        saveMacro: function(){
            this.controller.get('macro').save();
        }
    }
});

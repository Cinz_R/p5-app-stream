Stream2.CriteriaRoute = Ember.Route.extend({
    model: function ( model, transition ) {
        model.id = model.criteria_id;

        return new Ember.RSVP.Promise( function ( r, j ) {
            var criteria = Stream2.Criteria.create({ id: model.id });
            Stream2.request( '/search_json/' + model.id + '/criteria', {} , 'GET').then(
                function(data){
                    // Set the default values for the criteria that are not set.
                    $.map( criteria.defaults, function ( val, key ) {
                        if ( Ember.isNone( data[key] ) ){
                            data[key] = val;
                        }
                    });
                    // Set the defaults dynamically according to the calculated user defaults
                    criteria.setDefaultsFromUser(this.searchUser);

                    // And set the criteria from the returned data.
                    criteria.deserialise( data );

                    r( criteria );
                }
             );
        });
    },
    setupController: function ( controller, criteria ){
        var _this = this;
        controller.set('content', criteria);
        this.controllerFor('search').set('criteria', criteria);
    }
});

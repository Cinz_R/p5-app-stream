Stream2.WatchdogsEditRoute = Ember.Route.extend({
    model: function ( model ){
        Stream2.Watchdogs.find();
        return model;
    },
    setupController: function ( controller, model ) {

        controller.set('boards',[]);
        controller.set('error','');
        var availableBoards = Stream2.Boards.find();

        // eliminate the Responses board for users with no internal board
        // subscriptions
        if ( ! this.searchUser.get("hasTalentSearch") ) {
            availableBoards = availableBoards.filter( function(board) {
                return board.name != 'adcresponses';
            } );
        }

        Stream2.Watchdogs.findById( model.watchdog_id ).then( function ( wd ) {
            controller.set('watchdog', wd);
            Stream2.boardPromise.then( function () {
                availableBoards.forEach( function ( board ) {
                    // we want a list of available boards with the correct ones selected
                    // we don't want the selection to affect the master list of boards
                    var selectedBoard = !! wd.boards.findBy('name',board.get('name'));
                    controller.get('boards').pushObject( Stream2.Board.create({ 
                        name: board.get('name'),
                        nice_name: board.get('nice_name'),
                        type: board.get('type'),
                        selected: selectedBoard
                    }) );
                });
            });
        });
    }
});

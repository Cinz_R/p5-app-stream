Stream2.AdminPrevioussearchesRoute = Ember.Route.extend({
    queryParams: {
        page: {
            refreshModel: true
        },
        user_id: {
            refreshModel: true
        },
        destination: {
            refreshModel: true
        },
        watchdog: {
            refreshModel: true

        }
    },
    model: function ( params ) {
        return new Ember.RSVP.Promise(function ( resolve, reject ) {
            Stream2.request( '/api/admin/previous-searches', params, 'GET' ).then(
                function ( data ) { return resolve( data ); },
                function ( error ) { return reject( error ); }
            );
        });
    },
    setupController: function ( controller, model ) {

        // group results together by comparing criteria_id
        var results = model.results;
        if ( results.length ) {
            var crit_id = results[0].criteria_id;
            var user_id = results[0].user.id;
            results.forEach( function ( result, index ) {
                if ( result.criteria_id != crit_id || result.user.id != user_id ){
                    crit_id = result.criteria_id;
                    user_id = result.user.id;
                    results[index-1].lastInGroup = true;
                }
            });
        }

        this._super( controller, model );
    }
});

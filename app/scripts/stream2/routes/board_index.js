Stream2.BoardIndexRoute = Ember.Route.extend({
    model: function ( model ){
        return model;
    },
    setupController: function (){
        var _this = this;
        Stream2.boardPromise.then(
            function () {
                _this.replaceWith('board.results', _this.controllerFor('board').get('content'), { id: 1 } );
            }
        );
    }
});

Stream2.AdminSettingsRoute = Ember.Route.extend(Stream2.UserHierarchyMixin,{
    queryParams: {
        'setting' : {}
    },
    model: function ( params ) {
        var _this = this;
        // this route requires two objects to be loaded
        return new Ember.RSVP.Promise( function ( resolve, reject ) {
            Stream2.PromiseGroup.collect(
                // a list of available predifined user settings
                Stream2.request('/api/admin/available_settings', {}, 'GET' ),
                // a nested structure of any number of levels describing the user hierarchy
                // { label: "company", children: [ { label: "user1", id: 1 } .. ] }
                Stream2.request('/api/admin/user_hierarchy', {} , 'GET' )
            ).then( function ( outcomes ) {
                var settings_data = outcomes.shift();
                var hierarchy_data = outcomes.shift();

                if ( settings_data.error ){
                    return reject(settings_data);
                }
                if ( hierarchy_data.error ) {
                    return reject(hierarchy_data);
                }


                // using select2 requires each item in the list to be an ember object
                // and for the selected option to be an object
                var availableSettingArray = settings_data.success;

                var userMap = Ember.Object.create({ map: {}, ids: [] });
                return resolve({
                    hierarchy           : _this._vivifyModel( hierarchy_data.success, userMap ),
                    availableSettings   : availableSettingArray,
                    _userMap            : userMap,
                    autoExpand          : true
                });
            });
        });
    },
    actions: {
        updateUsers: function ( user ) {
            var _this = this;

            var settings = {
                value   : this._userValue( user ),
                users   : this._readChildren( user )
            };
            Stream2.request( '/api/admin/user_settings/' + this.controller.get('setting'), settings, 'POST' ).then(
                null, // Dont do anything in case of success.
                function ( error ){
                    Stream2.popupError(i18n.__('Your settings have not been saved'));
                    _this.controller.propertyDidChange('setting');
                }
            );
        }
    },
    setupController: function( controller , model ){
        this._super(controller, model);
        if( controller.get('setting') ){
            controller.updateModel();
        }
    }
});

Stream2.IndexRoute = Ember.Route.extend({
    redirect: function () {
        if ( Stream2.defaultRoute ) {
            Stream2.timeMark("Begining route");
            this.transitionTo(Stream2.defaultRoute);
        }
    }
});

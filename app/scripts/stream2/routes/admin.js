Stream2.AdminRoute = Ember.Route.extend({
    setupController: function () {
        if ( this.searchUser && this.searchUser.get('is_overseer') ){
            return true;
        }
        this.transitionTo('search');
    },
    actions: {
        tags: function () {
            this.transitionTo('admin.tags',{ id : 1 });
        }
    }
});

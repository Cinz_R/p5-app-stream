Stream2.AdminIndexRoute = Ember.Route.extend({
    redirect: function () {
        if( this.searchUser.get('is_admin') ){
            this.transitionTo('admin.tags', 1);   
        }else{
            // Standard overseers are redirected to settings as the first page.
            this.transitionTo('admin.settings');
        }
    }
});

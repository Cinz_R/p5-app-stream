Stream2.WatchdogsviewRoute = Ember.Route.extend({
    // this is another entry into the app. Links to this route will
    // be included in watchdog emails
    model: function( model, transition ){
        return model;
    },
    setupController: function ( controller, model ) {

        var _this = this;
        var id = model.id || model.watchdog_id;
        var watchdog = Stream2.Watchdog.create({ "id": id });
        controller.set('content', watchdog);
        $('#watchdogs-dropdown').dropdown('hide');

        // the rest is taken care of in .index or .board
    }
});

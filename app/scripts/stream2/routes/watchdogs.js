Stream2.WatchdogsRoute = Ember.Route.extend({
    model: function ( model ) {
        return Stream2.Watchdogs.find();
    },
    setupController: function ( controller, model ) {
        controller.set('content',model);
    }
});

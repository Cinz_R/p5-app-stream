Stream2.AdminQuotaRoute = Ember.Route.extend({
    queryParams: {
        'board' : {},
        'type' : {},
    },
    model: function ( params ) {
        var _this = this;
        // this route requires two objects to be loaded
        Stream2.Boards.find(); // This will load the boards if they are not there yet.
        return new Ember.RSVP.Promise( function ( resolve, reject ) {
            Stream2.PromiseGroup.collect(
                Stream2.boardPromise,
                // a nested structure of any number of levels describing the user hierarchy
                // { label: "company", children: [ { label: "user1", id: 1 } .. ] }
                Stream2.request('/api/admin/user_hierarchy', {} , 'GET' )
            ).then( function ( outcomes ) {
                var boards_data = outcomes.shift();
                var hierarchy_data = outcomes.shift();

                if ( boards_data.error ){
                    return reject(boards_data);
                }
                if ( hierarchy_data.error ) {
                    return reject(hierarchy_data);
                }

                var userMap = Ember.Object.create({ map: {}, paths: [] });

                // sorted by nice name and don't show longlist and simplify for use with select-2
                var boards = Stream2.Boards.find()
                    .filter(function(item){return (item.get('name') != "longlist"); })
                    .sortBy('nice_name')
                    .map(function(item){return Ember.Object.create({ id: item.get('name'), label: item.get('nice_name') })});

                return resolve({
                    hierarchy           : _this._vivifyModel( hierarchy_data.success, userMap ),
                    availableBoards     : boards,
                    _userMap            : userMap,
                    autoExpand          : true
                });
            });
        });
    },
    setupController: function( controller , model ){
        this._super(controller, model);
    },
    /*
        _vivifyModel

        converts primitive data structure into a nested set of ember objects
        ready to be observed
    */
    _vivifyModel: function ( model, user_map ) {
        var _this = this;
        model = Ember.Object.create( model );

        if ( model.get('children') ){
            var children = model.get('children');
            model.set('children', Ember.A() );
            $.each( children, function ( index, item ) {
                model.get('children').pushObject( _this._vivifyModel( item, user_map ) );
            });
        }
        if ( model.get('path') ){
            // create flat map of user objects
            user_map.get('map')[model.get('path')] = model;
            user_map.get('paths').push(model.get('path'));
        }

        return model;
    },

    actions: {
        postQuota: function( quotaChanges ){
            var _this = this;
            Stream2.request(
                '/api/admin/user_quota/',
                {
                    board: this.controller.get('board'),
                    type: this.controller.get('type'),
                    quota: quotaChanges
                },
                'POST'
            ).then(
                function ( data ) {
                    _this.controller.updateModel();
                    Stream2.popupAlert(i18n.__('Quota'),i18n.__('Quota has been updated'));
                },
                function ( error ) {
                    _this.controller.set('error', error);
                }
            );
        }
    }
});

Stream2.LonglistsViewRoute = Ember.Route.extend({

    /* Only gets call on manual navigation. Contains the parameters defined in the route (+ GET params) */
    model: function(params){
        // This longlist has to be stored against the searchUser.
        return this.searchUser.get('long_lists').findBy('id', parseInt(params.id));
    },

    afterModel: function(longlist){
        // Gui stuff. Hide the dropdown list.
        $('#longlists-dropdown').dropdown('hide');

        var _this = this;

        var searchController = this.controllerFor('search');
        var wasInLongList = searchController.get('viewedLongList');
        var previousCriteria = searchController.get('criteria');

        searchController.set('viewedLongList' , longlist);

        if( ! wasInLongList ){
            searchController.set('previousCriteria' , previousCriteria );
        }
        Stream2.pushEvent('longlist','load longlist');
        var newCriteria = Stream2.Criteria.create({ longlist_id: [ longlist.get('id') ]});
        return new Ember.RSVP.Promise(function(resolve, reject){
            newCriteria.withId().then(function( success ){
                newCriteria.set('id', success.data.id );
                return _this.transitionTo('board' ,  newCriteria , 'longlist');
            });
        });
    }
});

Stream2.WelcomeAdminRoute = Ember.Route.extend({
    activate: function () {
        Ember.run.next(null, function() {
            $('#search-as-options').on('hide', function(event, dropdownData) {
                hopscotch.endTour();
            });

            $('#search-as-options').on('show', function(event, dropdownData) {
                $('#search-as-options input').focus();
            });

            $('#search-as-options-link').dropdown('show');
            hopscotch.startTour({
                id: 'welcome-admin-tour',
                steps: [{
                    title: i18n.__("Welcome"),
                    content: i18n.__("Select a user to begin"),
                    target: "search-as-options",
                    placement: 'right',
                    showNextButton: false,
                    yOffset: 30
                }],
                i18n: {
                    stepNums: [""]
                }
            });
        });
    },
    deactivate: function() {
        hopscotch.endTour();
        $('#search-as-options-link').dropdown('hide'); 
    }
});

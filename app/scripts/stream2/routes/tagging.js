Stream2.TaggingRoute = Ember.Route.extend({
    model: function ( data ) {
        return data;
    },
    setupController: function ( controller, model ){
        var results = this.controllerFor('board_results');
        var selectedCandidates = results.get('selectedCandidates');

        if ( typeof(selectedCandidates) == "undefined" ){
            return this.transitionTo('board.index', this.controllerFor('board').get('content'));
        }

        controller.set('selectedCandidates', selectedCandidates);
        controller.set('sendingTags',false);
        controller.set('outcomes',[]);
    },
    actions: {
        multiTag: function () {
            var _this = this;

            var tag_value = this.controller.get('tag.value');
            this.controller.set('tag',null);
            this.controller.set('sendingTags',true);

            this.controller.get('selectedCandidates').forEach( function( item ) {
                var outcome = Stream2.BulkCandidateOutcome.create({
                    candidate: item
                });
                Stream2.resetMark();
                _this._multiTag( item, tag_value ).then(
                    function ( message ) {
                        Stream2.timeMarkEvent('Candidate Actions','tag');
                        outcome.set('finished',true);
                        outcome.set('message',message.message);
                        outcome.set('warnings',message.warnings);
                        // We set the shortlist with the list name suffix.
                        item.get('tags').pushObject( Ember.Object.create({ value: tag_value, new: true }) );
                    },
                    function ( error ) {
                        outcome.set('finished',true);
                        outcome.set('error',true);
                        outcome.set('message',error.message);
                    }
                );
                _this.controller.get('outcomes').pushObject( outcome );
            });

            Stream2.pushEvent('candidate','bulk add tag');
        }
    },
    _multiTag: function ( candidate, tag_value ) {
        return Stream2.requestAsync( candidate.apiURL( 'tags' ), { tag_name: tag_value }, 'POST' );
    }
});

Stream2.AdminTagDeleteRoute = Ember.Route.extend({
    setupController: function(controller){
        // Controller for the route called 'tag'
        var tagController = this.controllerFor('admin.tag');
        controller.set('content' , tagController.get('content'));
    },
    actions: {
        deleteTag: function(){
            var _this = this;
            var tagOption = this.controller.get('content');

            Stream2.request('/api/admin/tags/tag/delete' , { tag_name: tagOption.get('value') } , 'POST' )
                .then(function(data){
                    Stream2.popupAlert(i18n.__('Tag deletion successful') , data.message);
                        _this.transitionTo('admin.tags' , 1 );
                    });

        }
    }
});

Stream2.ShortlistRoute = Ember.Route.extend({
    model: function ( data ) {
        return data;
    },
    setupController: function ( controller, model ){
        var results = this.controllerFor('board_results');
        var selectedCandidates = results.get('selectedCandidates');

        if ( typeof(selectedCandidates) == "undefined" ){
            return this.transitionTo('board.index', this.controllerFor('board').get('content'));
        }

        controller.set('list', model);

        controller.set('advert_title',null);
        controller.set('cheeseSandwich' , null);

        controller.set(
            'selectedCandidates',
            selectedCandidates.filterBy('canBulkShortlist')
        );
        controller.set('sendingMessage',false);
        controller.set('outcomes',[]);
    },
    actions: {
        shortlist: function () {
            var _this = this;
            this.controller.set('sendingMessage',true);

            this.controller.get('selectedCandidates').forEach( function( item ) {
                var outcome = Stream2.BulkCandidateOutcome.create({
                    candidate: item
                });
                Stream2.resetMark();
                _this._shortlist( item ).then(
                    function ( message ) {
                        Stream2.timeMarkEvent('Candidate Actions','shortlist');
                        outcome.set('finished',true);
                        outcome.set('message',message.message);
                        outcome.set('warnings',message.warnings);
                    },
                    function ( error ) {
                        outcome.set('finished',true);
                        outcome.set('error',true);
                        outcome.set('message',error.message);
                    }
                );
                _this.controller.get('outcomes').pushObject( outcome );
            });

            Stream2.pushEvent('candidate','bulk shortlist');
        }
    },
    _shortlist: function ( candidate ) {
        return Stream2.requestCandidateAsync(candidate, candidate.apiURL( 'shortlist' ),
            {
                advert_id: this.controller.get('cheeseSandwich').id,
                advert_label: this.controller.get('cheeseSandwich').text,
                list_name: this.controller.get('list').get('name')
            }
        );
    }
});

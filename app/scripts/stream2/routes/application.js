Stream2.ApplicationRoute = Ember.Route.extend({
    model: function () {
        // everything in here must be resolved before the app starts
        // load the user object and the translations
        var _this = this;

        // update status on loading screen
        // because this app hasn't got the user's language yet, lets rely on pre-translated string
        $('#loading-status1').hide();
        $('#loading-status2').show();
        if ( $('html').is('.lt-ie9') ) {
            return _this._loadingError("Your browser version is no longer "
                + "supported: please update it to use Broadbean", "");
        }

        // loading all required assets for the application to run
        return new Ember.RSVP.Promise( function ( resolve, reject ) {

            Stream2.Boards.find();
            var lang = stream2_current_user_locale || 'en';
            Stream2.PromiseGroup.collect(
                Stream2.request('/api/user/details', {}, 'GET' ),
                Stream2.request('/api/user/oversees', {} , 'GET' ),
                Stream2.Translations.load( lang ),
                Stream2.boardPromise
            ).then( function ( outcomes ) {
                var user_data = outcomes.shift();
                var oversees_data = outcomes.shift();
                var locale_data = outcomes.shift();

                if ( user_data.success )
                    user_data = user_data.success;
                else
                    return _this._loadingError( 'User failed to load', user_data.error );

                if ( oversees_data.success ) {
                    user_data.details.oversees = oversees_data.success.overseen_users.map(
                        function ( item ) {
                            return Stream2.AdCourierUser.create( item );
                        }
                    );
                }
                if ( locale_data.success ){
                    locale_data = locale_data.success;
                }

                // finally once everything has loaded... hide the loading screen
                Ember.run.scheduleOnce( 'afterRender', null, function () {
                    $('#loading-screen').hide();
                });

                // If they are a Search2 Virgin, show them a good time
                if ( user_data.details && user_data.details.first_time ){
                    Stream2.one( 'resultsLoaded', function () {
                        Ember.run.schedule( 'afterRender', null, _searchIntroTour );
                    });
                }

                // primitives into ember objects

                // datepicker setup
                var datepicker_lang = lang.replace( '_', '-' );
                // datepicker takes "en" as "en-US"
                if ( datepicker_lang === 'en' ) datepicker_lang = 'en-GB';
                var dateDefaults = $.datepicker.regional[datepicker_lang];
                if ( dateDefaults ) {
                    $.datepicker.setDefaults(dateDefaults);
                }

                _this.searchUser.setProperties( user_data.details );
                _this.translations.setProperties( locale_data.placeholders || {} );
                resolve( { user: user_data.details, locale: locale_data });
            });
        });
    },
    afterModel: function(){
        moment.lang(stream2_current_user_locale, {
            calendar : {
                lastDay : '[' + i18n.__('Yesterday') + ']',
                sameDay : '[' + i18n.__('Today') + ']',
                nextDay : '[' + i18n.__('Tomorrow') + ']',
                lastWeek : '[' +  i18n.__('Last') + '] dddd',
                nextWeek : 'dddd',
                sameElse : 'll'
            }
        });

    },
    _loadingError: function ( description, error  ){
        $('#loading-status2').text( description  + '. ' + error );
        $('#loading-status2').css({ color: "red" });
        $('#loading-screen > img').first().hide();
    },
    setupController: function ( controller, model ) {
        controller.set('criteriaLists', model.locale.criteria || {});
        controller.set('featureExclusions', model.user.feature_exclusions);
        controller.loadSavedSearches();

        Stream2.trigger('userLoaded');
    },
    actions:{

        unsaveLongList: function( longlist ){
            var _this = this;
            if( ! confirm( i18n.__("Are you sure you want to delete this Longlist?") ) ){
                return false;
            }

            $('#longlists-dropdown').dropdown('hide');

            Stream2.request("/api/user/longlists/" + longlist.get('id') , {}, 'DELETE').then(function(data){
                Stream2.popupAlert(i18n.__('Success'), data.message );
                _this.searchUser.removeLongList(longlist);
                // We need to clear the searchUser last_used_longlist if its the same one.
                var lastUsedLL = _this.searchUser.get('last_used_longlist');
                if( lastUsedLL && lastUsedLL.get('id') == longlist.get('id') ){
                    _this.searchUser.set('last_used_longlist' , null);
                }
                // We need to close ALL action panels against each open candidates
                var board_results_controller = _this.controllerFor('board_results');
                if( board_results_controller ){
                    board_results_controller.model.forEach(function(candidate){
                        board_results_controller.switchOffShownActionPanels(candidate);
                    });
                }
                var search_controller = _this.controllerFor('search');
                if( search_controller ){
                    var viewedLongList = search_controller.get('viewedLongList');
                    if( viewedLongList && viewedLongList.get('id') == longlist.get('id') ){
                        // send the search controller a clearLongList
                        search_controller.send('clearLongList');
                    }
                }
            });

        },


        clearWatchdogViewedResults: function(watchdog){
            var searchController = this.controllerFor('search');

            if( ! confirm( i18n.__("Are you sure you want to mark all this watchdog's results as read?") ) ){
                return false;
            }

            Stream2.popupAlert( i18n.__("Marking watchdog results as read") );

            Stream2.resetMark();
            Stream2.requestAsync('/api/watchdog/' + watchdog.get('id') + '/viewed_results' , {}, 'DELETE' ).then(function( data ){
                Stream2.timeMarkEvent('Watchdogs','reset all results');
                watchdog.get('boards').map(function(board){
                    board.set('count', 0 ) ;
                });
                if( searchController ){
                    searchController.withWatchdog(function(){
                        var searchWd = this;
                        if( searchWd.get('id') == watchdog.get('id') ){
                            Stream2.boardPromise.then(function(){
                                // We are viewing this watchdog.
                                watchdog.get('boards').map(function(board){
                                    Stream2.Boards.findById(board.name).set('newResults', 0);
                                });
                            })
                        }
                    });
                }// End of if searchController
                Stream2.popupAlert( data.message );
            });// End of DELETE ajax
        },

        unsaveWatchdog: function(watchdog){
            var _this = this;
            var searchController = _this.controllerFor('search');

            if( confirm( i18n.__('Are you sure?') ) ){
                Stream2.request('/api/watchdogs' , { id: watchdog.get('id')  } , 'DELETE' ).then(
                    function(data){
                        _this.controller.get('watchdogs').removeObject(watchdog);
                        Stream2.popupAlert(i18n.__('Success'), data.message );
                        if( ! searchController ){ return ; }

                        // Make sure to clear the current controller from
                        // its watchdog if it contains the same one.
                        searchController.withWatchdog(function(){
                            var wd = this;
                            if( wd.get('id') == watchdog.get('id') ){
                                searchController.clearWatchdog();
                                searchController.expireAllResults();
                                searchController.submitSearch();
                            }
                        });
                    },
                    function(error){
                        Stream2.popupError(error);
                    }
                );
            }
        },

        cancelNav: function() {

            // cancel click event (i.e. return false) for users with
            // restricted UI
            return !this.get("searchUser").get("restrictUI");
        }
    }
});

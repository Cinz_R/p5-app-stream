Stream2.SearchIndexRoute = Ember.Route.extend({ // only gets called if specifically going to search route
    setupController: function ( controller, model ) {
        var searchController = this.controllerFor('search');
        searchController.clearWatchdog();
        searchController.expireAllResults();
        searchController.set("searchHasRun", false);

        if ( this.searchUser.get('is_overseer') ){
            return this.replaceWith ('welcome-admin'); // replace instead of transition replaces history, so the back button doesn't just reload the page
        }

        if ( this.searchUser.get('manageResponsesView') ){
            return Stream2.boardPromise.then( function () { searchController.submitSearch( { 'noHistory': true } ); } );
        }
        // This used to create an empty search when hitting '/search/'
        // we do not want that anymore following the magic story https://www.pivotaltracker.com/story/show/79297858
        // Stream2.boardPromise.then( function () { searchController.submitSearch(); } );
    }
});

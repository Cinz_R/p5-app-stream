Stream2.MessageRoute = Ember.Route.extend({
    model: function ( data ) {
        return data;
    },
    setupController: function ( controller, model ){
        var results = this.controllerFor('board_results');

        if ( Ember.isNone(results) || Ember.isEmpty(results.get('selectedCandidates')) ){
            return this.transitionTo('board.index', this.controllerFor('board').get('content'));
        }

        var selectedCandidates = results.get('selectedCandidates');
        var searchController = this.controllerFor('search');

        controller.set(
            'recipients',
            selectedCandidates.filterBy('canBulkMessage')
        );
        controller.set('sendingMessage',false);
        controller.set('cheeseSandwich',null);
        controller.set('advert_title',null);
        controller.set('outcomes',[]);
        controller.set('customLists', searchController.get('customLists') );

        Stream2.request( '/api/user/message-defaults', { destination: this.controllerFor('board').get('name') }, 'GET' ).then(
            function ( data ) {
                if ( data.from ) controller.set( 'messageEmail'  , data.from );
                if ( data.content ){
                    controller.set('_tinymceUpdateContent',true);
                    controller.set( 'messageContent', data.content );
                }
                if ( data.subject ) controller.set( 'messageSubject', data.subject );
                controller.set('messageSubjectPrefix' , data.subject_prefix );
                controller.set('show_advert_preview', data.show_advert_preview);
            },
            null
        );
    },

    actions: {
        // Reset the template on transitioning to allow choosing it
        didTransition: function(){
            this.controller.set('emailTemplate', null);
        },

        sendMessage: function () {
            var _this = this;
            this.controller.set('sendingMessage',true);
            this.controller.get('recipients').forEach( function( item ) {
                var outcome = Stream2.BulkCandidateOutcome.create({
                    candidate: item
                });
                Stream2.resetMark();
                _this._sendMessage( item ).then(
                    function ( message ) {
                        Stream2.timeMarkEvent('Candidate Actions','message');
                        outcome.set('finished',true);
                        outcome.set('message',message.message);
                        outcome.set('warnings',message.warnings);
                        item.set('has_done_message', true);
                        if( ! item.get('email') ){
                            // Item didnt have an email. It has been downloaded.
                            item.set('has_done_download', true);
                        }
                    },
                    function ( error ) {
                        outcome.set('finished',true);
                        outcome.set('error',true);
                        outcome.set('message',error.message);
                    }
                );
                _this.controller.get('outcomes').pushObject( outcome );
            });
            Stream2.pushEvent('candidate','bulk message');
        }
    },
    _sendMessage: function ( candidate ) {
        var controller = this.controller;
  
        return Stream2.requestAsync( candidate.apiURL( 'message' ),
            {
                messageEmail:         controller.get('messageEmail'),
                messageContent:       controller.get('messageContent'),
                messageSubject:       controller.get('messageSubject'),
                messageSubjectPrefix: controller.get('messageSubjectPrefix'),
                messageBcc:           controller.get('messageBcc'),
                advertId:             controller.get('advertId'),
                advertList:           controller.get('advertList'),

                emailTemplateId:      controller.getWithDefault('emailTemplate.id',''),
                emailTemplateName:    controller.getWithDefault('emailTemplate.text',''),

                cb_oneiam_auth_code:  this.searchUser.cbOneIAMAuthCode()
            }
        );
    }
});

Stream2.WatchdogsSummaryRoute = Ember.Route.extend({
    setupController: function ( controller ) {
        $('#watchdogs-dropdown').dropdown('hide');
        controller.set('content', Stream2.Watchdogs.find());
    }
});

Stream2.BoardResultsRoute = Ember.Route.extend({
    model: function ( model, transition ){ return model; },
    renderTemplate: function ( controller, model ) {
        if ( this.isWatchdog() ){
            return this.render('board/watchdog-results', {
                controller: this.controller
            });
        }
        return this.render('board/results', {
            controller: this.controller
        });
    },

    // activate: function(controller){
    //     if( controller.get('hasPreviousProfile') ){
    //         controller.positionProfileBox();
    //     }
    // },

    isWatchdog: function () {
        var search = this.controllerFor('search');
        var watchdog = search.get('watchdog');
        var board = search.get('selectedBoard');
        if ( watchdog && board ){
            return search.isWatchdogBoard( board );
        }
        return false;
    },
    setupController: function  ( controller, model ) {
        // we have some results and they are current
        // Going to need to do some server activity
        var _this = this;
        var board = this.controllerFor('board');
        var criteria = this.controllerFor('criteria');
        var searchController = this.controllerFor('search');
        var page_id = model.id ? model.id : model.page_id;
        var status_id = model.status_id || model.search_id;

        // cancel resultLoader? any Ember.run task returns a pointer object which can be used to cancel it.
        // this will cancel any existing result-loading tasks
        Ember.run.cancel( controller.get('resultLoaderTask') );
        controller.set('model',[]);
        controller.set('searchFinished',false);

        // Reset the previewCandidate too.
        controller.set('previewCandidate', null);

        searchController.set('error', null);

        Stream2.boardPromise.then(
            function () {
                // Board already has results
                if ( model.results_id ){
                    _this.retrieveResults( model.results_id, page_id ).then(
                        null,
                        // facetSearch = clear current results and run the search again
                        function () { searchController.refreshBoard(); }
                    );

                }

                // Search in progress - user has clicked a facet or the big search button
                else if ( status_id ){
                    board.beginPoll( status_id ).then(
                        function ( data ) {

                            Stream2.timeMarkEvent('Results', _this.controllerFor('board').get('name'));

                            // console.log("Got context: " , data.context);

                            _this.retrieveResults( data.context.result.id, page_id ).then(
                                function(){
                                    if (!Ember.isEmpty(searchController.get('selectedBoard.quota'))) {
                                        searchController.get('selectedBoard.quota').updateQuota();
                                    }
                                    // If there is a watchdog, tell the controller that
                                    // a watchdog is being viewed in this returned context
                                    searchController.watchdogViewed(data.context);
                                },
                                function ( error ){
                                    Stream2.popupError( error );
                                   _this.controllerFor('search').set('error', error);
                                }
                            );
                        },
                        function ( context ){
                            // Setting this controller's results id to be null.
                            // This is to avoid the running _injectResults to carry on
                            // injecting results in this page.
                            console.log("An error occured in polling. Voiding current results");
                            _this.controller.set('results_id' , null);
                            _this.controller.set('model' , [] );
                            if ( context.is_unmanaged ){
                                searchController.throwUnmanagedError( context.error );
                            }
                            else {
                                searchController.throwManagedError( context.error );
                            }
                            controller.set('searchFinished',true);

                        }
                    );
                }

                // Coming straight from URL
                else {
                    if ( (board.get('name') !== 'longlist') && !searchController.get('selectedBoard.quota.canSearch')) {
                        Stream2.popupAlert(
                            i18n.__('Oops...'),
                            i18n.__('No credits available to search this board.')
                        );
                        searchController.throwManagedError({
                            message: i18n.__('No credits available to search this board.'),
                            type: 'quota'
                        });
                        controller.set('searchFinished',true);
                        return;
                    }
                    Stream2.resetMark();

                    if(board.get('name') === 'longlist') {
                        var longlist = _this.searchUser.get('long_lists').findBy('id', parseInt(criteria.get('longlist_id')[0]));
                        if (longlist) {
                            searchController.set('viewedLongList', longlist);
                        }
                    }

                    var cbOneIAMPromise;
                    var cbOneIAMUser = _this.get('searchUser').get('cbOneIAMUser');
                    if( board.get('supportsCbOneIAM') &&  cbOneIAMUser ){
                        // This is a promise that the user is
                        // 1. connected to CBOneIAm,
                        // 2. with the right cbOneIAMUser
                        cbOneIAMPromise = Stream2.get('cbOneIamAuth').assumeCbOneIAMUser( cbOneIAMUser );
                    }


                    // STEP 1: Resume the search based on the content of the URL

                    var resumeSearchPromise;
                    if( cbOneIAMPromise ){
                        resumeSearchPromise = cbOneIAMPromise.then(
                            function( cbOneIAMSession ){
                                return Stream2.request( [criteria.get('id'), 'resume', board.get('id'), 'page', page_id].join('/'),
                                                        { watchdog_id: searchController.get('watchdogId'),
                                                          cb_oneiam_auth_code: cbOneIAMSession.auth_response.auth_code
                                                        } );
                            },
                            function(error){
                                console.log("OneIAM error: ", error );
                                return new Ember.RSVP.Promise(function(resolve, reject){
                                    resolve( { error: error } );
                                });
                            }
                        );
                    }else{
                        resumeSearchPromise = Stream2.request( [criteria.get('id'), 'resume', board.get('id'), 'page', page_id].join('/'),
                                                               { watchdog_id: searchController.get('watchdogId') });
                    }

                    resumeSearchPromise.then(
                        function ( data ) {
                            if ( data.error ){
                                if ( data.is_unmanaged ){
                                    searchController.throwUnmanagedError( data.error );
                                }
                                else {
                                    searchController.throwManagedError( data.error );
                                }
                                controller.set('searchFinished',true);
                            }
                            else {
                                searchController.preLoadCriteria( data );
                                // STEP 2: the resume action returns the criteria and starts a search - poll the search
                                board.beginPoll( data.status_id ).then(
                                    function ( data ) {
                                        if ( data.error ){
                                            searchController.set('error', data.error);
                                            controller.set('searchFinished',true);
                                        }
                                        else {

                                            // console.log("Got context: " , data.context);

                                            Stream2.timeMarkEvent('Results', _this.controllerFor('board').get('name'));
                                            _this.retrieveResults( data.context.result.id, page_id ).then (
                                                function(){
                                                    if (!Ember.isEmpty(searchController.get('selectedBoard.quota'))) {
                                                        searchController.get('selectedBoard.quota').updateQuota();
                                                    }
                                                    // If there is a watchdog, tell the controller that
                                                    // a watchdog is being viewed in this returned context
                                                    searchController.watchdogViewed(data.context);
                                                },
                                                function ( error ){
                                                    Stream2.popupError( error );
                                                    _this.controllerFor('search').set('error', error);
                                                }
                                            );
                                        }
                                    },
                                    function ( context ) {
                                        console.log("An error occured in polling. Voiding current results");
                                        _this.controller.set('results_id' , null);
                                        _this.controller.set('model' , [] );
                                        if ( context.is_unmanaged ){
                                            searchController.throwUnmanagedError( context.error );
                                        }
                                        else {
                                            searchController.throwManagedError( context.error );
                                        }
                                        controller.set('searchFinished',true);
                                    }
                                );
                            }
                        },
                        function ( error ){
                            searchController.throwUnmanagedError( error );
                            controller.set('searchFinished',true);
                        }
                    );
                }

                // This will hold the candidate ID of the currently previewed candidate.
                var preview_candidate  = null;
                if( controller.get('previewCandidate') ){
                    preview_candidate = controller.get('previewCandidate').get('candidate_id');
                }

                // After all results are loaded (using the funky IE mitigation technique, remember)
                // we can reinstantiate the previewCandidate if there was such an thing before entering the route.
                Stream2.one( 'resultsLoaded', function () {
                    Ember.run.schedule('afterRender', null,
                        function(){
                            var previewCandidate = null;
                            if( preview_candidate ){
                                previewCandidate = controller.findBy('candidate_id' , preview_candidate);
                            }
                            else if ( model.candidate_idx ) {
                                previewCandidate = controller.get('model').objectAt( model.candidate_idx );
                            }
                            if( previewCandidate ){
                                _this.send('downloadProfile' , previewCandidate);
                            }
                        }
                    );
                });
            },

            null
        );
    },

    actions: {

        toggleSelected: function ( candidate ) {
            candidate.set('isSelected', !candidate.get('isSelected') );
            Stream2.pushEvent('candidate','selected');
        },
        importCV: function(candidate){
            var _this = this;
            if (
                ( !candidate.get('has_done_import') &&
                !candidate.get('canDownloadCV') )
                || !candidate.get('importEnabled')
            ) return;

            this.controllerFor('board').cvCostGuard(! candidate.get('has_done_download'),
                function(){
                    Stream2.popupAlert(
                        i18n.__('Importing'),
                        i18n.__('Importing CV in your Talent Pool')
                    );

                    Stream2.resetMark();
                    Stream2.requestCandidateAsync( candidate, candidate.apiURL( 'import' ), { cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode() } , 'GET' ).then(
                        function(data){
                            Stream2.timeMarkEvent('Candidate Actions','import');
                            if( candidate.get('historicActionsChronological') ){
                                // The history is there. We should refresh it.
                                candidate.fetchProfileHistory();
                            }
                        },
                        function(error){
                            Stream2.popupError( error );
                        }
                    );
                    Stream2.pushEvent('candidate','import cv');
                }
            );
        },

        candidateHistory: function ( candidate ) {
            var history = {};
            var historic_actions_performed = {};
            var historic_actions = Ember.A();
            var _this = this;

            candidate.set('historic_actions', historic_actions);
            candidate.set('history', history);

            Stream2.request( candidate.apiURL( 'history' ),{ cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode() }, 'GET').then(
                function (data) {
                    if(!Ember.isNone(data.history)) {
                        $.each(data.history, function(index, historic_action) {

                            if ( ! Ember.isArray( history[historic_action.action] ) ) {
                                history[historic_action.action] = Ember.A();
                            }
                            history[historic_action.action].pushObject(historic_action);
                        });

                        // store actions that were performed on this candidate
                        Ember.keys(history).forEach( function( _action, index, arr ) {
                            historic_actions_performed[_action] = 1;
                        });

                        // group ordering
                        Stream2.get('candidateHistoryMeta').forEach( function ( action_group ) {
                            // if we have data back from the API matching the current 'action_group'
                            if(!Ember.isNone(history[action_group.action])) {
                                historic_actions.pushObject({
                                    action: action_group.action,
                                    label:  action_group.label,
                                    data:   history[action_group.action],
                                    icon:   action_group.icon,
                                });
                                delete historic_actions_performed[action_group.action];
                            }
                        });

                        // shove the remaining actions we didnt catch into the historic_actions array
                        if(!Ember.isNone(historic_actions_performed)) {
                            $.each(Ember.keys(historic_actions_performed), function(index, _action) {
                                historic_actions.pushObject({
                                    action: _action,
                                    label:  _action,
                                    data:   history[_action]
                                });
                            });
                        }
                        candidate.set('historic_actions', historic_actions);
                    }
                },
                function (error) {
                    alert(i18n.__("There was an error when trying to retrieve the candidate history. Please refresh the search results and try again"));
                }
            );
        },
        addNote: function ( candidate, content, privacy ) {

            var params = {
                privacy: privacy,
                content: content,
                cb_oneiam_auth_code: this.searchUser.cbOneIAMAuthCode()
            };
            Stream2.request( candidate.apiURL( 'note' ), params, 'POST').then(
                function (data) {
                    params.id = data.id;
                    params.relative_time = 'just now';
                    params.insert_epoch = (new Date()).getTime() / 1000;
                    params.author = i18n.__("You");
                    candidate.get('notes').unshiftObject(
                        Stream2.Note.create( params )
                    );
                    if( candidate.get('historicActionsChronological') ){
                        // The history is there. We should refresh it.
                        candidate.fetchProfileHistory();
                    }

                },
                function (error) {
                    Stream2.popupError( error );
                    candidate.set('error', error);
                }
            );

            Stream2.pushEvent('candidate','add note');
        },
        deleteNote: function ( candidate, note ) {
            var _this = this;
            Stream2.request( candidate.apiURL( 'note', note.id ), { cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode() }, 'DELETE').then(
                function () {
                    candidate.get('notes').removeObject( note );
                    if( candidate.get('historicActionsChronological') ){
                        // The history is there. We should refresh it.
                        candidate.fetchProfileHistory();
                    }
                }
            );

            Stream2.pushEvent('candidate','delete note');
        },


        removeFromLongList: function( candidate ){
            var _this = this;
            if( ! confirm(i18n.__("Are you sure you would like to remove this candidate from this Longlist?") ) ){
                return;
            }
            var longlist_id = candidate.get('stream2_longlist_id');

            Stream2.request( candidate.apiURL( 'longlist' ), {
                id: longlist_id,
                remove: true,
                cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode()
            }, 'POST').then(
                function(data){
                    Stream2.popupAlert('Success!', data.message );
                    Stream2.pushEvent('candidate','remove from longlist');
                    var searchController = _this.controllerFor('search');
                    // No preview
                    searchController.set('previewCandidate', null);
                    // Refresh the view of this board.
                    searchController.refreshBoard();
                },
                function(error){
                    Stream2.popupError(error);
                    candidate.set('error', error);
                }
            );
        },
        deleteCandidate: function( candidate ){

            var _this = this;

            if(! confirm("Are you sure? This is your LAST CHANCE to abort deleting this candidate. Press OK if you want to proceed") ){
                return;
            }

            Stream2.request( candidate.apiURL( 'delete_candidate' ), { cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode() } , 'POST' ).then(
                function(data){
                    Stream2.popupAlert('Success!', data.message );
                    var searchController = _this.controllerFor('search');
                    // No preview
                    searchController.set('previewCandidate', null);
                    // Refresh the view of this board.
                    searchController.refreshBoard();

                },
                function(error){
                    Stream2.popupError(error);
                    candidate.set('error', error);
                }
            );

            Stream2.pushEvent('candidate','delete candidate');
        },

        /* Given a filename from an adcourier attachment */
        downloadAttachment: function ( filename ) {
            var _this = this;
            Stream2.fileDownload( '/api/candidate/download_response_attachment?' + $.param( { filename: filename, cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode()  } ) ).then (
                function () {},
                function ( error ) {
                    Stream2.popupError( error );
                }
            );
            Stream2.pushEvent('candidate','download attachment');
        },

        downloadCV: function ( candidate ) {
            if(
                this.searchUser.get('manageResponsesView') &&
                !candidate.get('hasCV')
            ) return;

            if (
                !candidate.get('has_done_download') &&
                !candidate.get('canDownloadCV')
            ) return;

            if(!candidate.get('cvEnabled')) {
                return;
            }

            var _this = this;
            this.controllerFor('board').cvCostGuard(!candidate.get('has_done_download'),
                                                    function(){

                Stream2.popupAlert(
                    i18n.__('Downloading...'),
                    i18n.__('Just popping to the board to get that for you.')
                );
                Stream2.fileDownload( candidate.apiURL( 'cv' ) + '?' + $.param({ cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode()  }) ).then (
                    function () {
                        candidate.set('error',null);
                        candidate.set('has_done_download', true);
                        if( candidate.get('historicActionsChronological') ){
                            // The history is there. We should refresh it.
                            candidate.fetchProfileHistory();
                        }
                    },
                    function ( error ) {
                        Stream2.popupError( error );
                        candidate.set('error', error);
                    }
                );
                Stream2.pushEvent('candidate','download cv');
            });

        },

        profileNotes: function ( candidate ) {
            this.send('downloadProfile',candidate);
            Ember.run.next( this, function () {
                $('div.profile-details-content').animate({
                    scrollTop: ($('div.notes#notes').position().top + 10)
                }, 1000);
            });
            return false;
        },

        // handles a click on the candidate email history icon, auto opens the
        // message history
        profileEmailHistory: function( candidate ) {
            this.send('downloadProfile', candidate);
            Ember.run.next( this, function() {
               this.controller.send('selectTab', candidate, 'message');
            });
            return false;
        },

        scrollToHistory: function( candidate, history ){
            candidate.set('selectedTab', 'history');
            Ember.run.next(this, function(){
                var $profileContent = $('div#profile-content');
                var $historyCard = $('div.history-card#history-' + history.get('id'));
                // This will reset the position property of
                // the history card to the right thing.
                $profileContent.scrollTop(0);

                // Then we can animate to the right place.
                $profileContent.animate({
                    scrollTop: ( $historyCard.position().top )
                }, 200, function(){
                    $historyCard.pulse({ backgroundColor: '#fbf9f9' },
                                       { duration: 200, pulses: 3 } );
                });
            });
        },

        /*
          Add the given candidate to the given longlist. Used in the add to longlist single button
         */
        addToLongList: function( candidate , longlist ){
            var _this = this;
            Stream2.request( candidate.apiURL( 'longlist' ),
                             {
                                 id: longlist.get('id'),
                                 remove: false,
                                 cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode()
                             }, 'POST')
                .then(function(data){
                    var newLongList =  Stream2.LongList.create( data.longlist );
                    _this.get('searchUser').add_longlist( newLongList );
                    _this.get('searchUser').set('last_used_longlist' , newLongList );
                    candidate.set('has_done_longlist_add' , true);
                    Stream2.popupAlert( i18n.__("Candidate Added") , data.message );
                    Stream2.pushEvent('candidate','add to longlist');
                }, function( error ){ candidate.set('error' , error ); } );
        },

        downloadProfile: function ( candidate ) {
            var _this = this;
            if (
                ! candidate.get('has_done_profile') &&
                ! candidate.get('canDownloadProfile')
            ) return;

            this.controller.set('previewCandidate',candidate);
            if ( candidate.get('profile_body') !== null ) return;

            Stream2.resetMark();
            Stream2.request( candidate.apiURL( 'profile' ), { cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode() }, 'GET' ).then(
                function( response ){
                    var data = response.data;
                    Stream2.timeMarkEvent('Candidate Actions','profile');
                    candidate.set('error', null);
                    candidate.setProperties( data );
                    candidate.set('has_done_profile', true);

                    // This is a hack to do stuff on download profile.
                    // One day we will change that for a proper macro.
                    // Do NOT extend this
                    Ember.run.next( _this, function () {
                        var CVDownloadOnPreview = _this.searchUser.get('settings').auto_download_cvs_on_preview || false;
                        if( CVDownloadOnPreview ){
                            // Do NOT do this if the candidate is in talentsearch or cb_mysupply
                            if( ! candidate.get('inInternalSearch') ){
                                _this.send('importCV', candidate);
                            }
                        }
                    } );
                    // End of witty comment.
                },
                function(error){
                    candidate.set('error' , error);
                }
            );
            Stream2.pushEvent('candidate','download profile');
        },
        hideProfile: function () {
            this.controller.set('previewCandidate',null);
        },
        nextProfile: function () {
            if ( ! this.controller.get('hasNextProfile') ) return false;
            var nextCandidate = this.controller.get('model').objectAt(
                this.controller.get('model').indexOf( this.controller.get('previewCandidate') ) + 1
            );
            if (
                !nextCandidate.get('has_done_profile') &&
                !nextCandidate.get('canDownloadProfile')
            ) return false;
            this.send('downloadProfile',nextCandidate);
        },
        previousProfile: function () {
            if ( ! this.controller.get('hasPreviousProfile') ) return false;
            var previousCandidate = this.controller.get('model').objectAt(
                this.controller.get('model').indexOf( this.controller.get('previewCandidate') ) - 1
            );
            if (
                !previousCandidate.get('has_done_profile') &&
                !previousCandidate.get('canDownloadProfile')
            ) return false;
            this.send('downloadProfile',previousCandidate);
        }
    },

    /* Injects the given results array in the
       controller in a way that gives control back to
       the browser from time to time (every 2 results)

       This will avoid 'long running script' warnings in slow
       browsers.

    */
    _injectResults: function( results, results_id, index ){
        // Munch through 2 results
        var i = 1;
        var _this = this;

        if( this.controller.get('results_id') !== results_id ){
            console.log('Board result controller is not about the same results id. Doing nothing');
            return;
        }
        if ( typeof( index ) == "undefined" ){
            index = 0;
        }

        // Linter doesnt like functions declared in loops
        // so we have this injectTwo local function
        var injectTwo = function(){
            var count = 2;
            while( count-- ){
                var result = results[index++];
                if( ! result ){ return false; }
                _this.controller.addResult( result );
            }
            return true;
        };

        while( i-- ){
            if( ! injectTwo() ){
                // The search has only truly finished once the results are on display
                Stream2.timeMark('finished');
                Stream2.trigger('resultsLoaded');
                _this.controller.set('searchFinished',true);
                return;
            }
        }

        // At this point, we want to inject the rest of the results
        // in a different run loop, allowing the browser to render
        // those results so far, thus avoiding blocking for too long.
        if( index < results.length ){
            this.controller.set(
                'resultLoaderTask',
                Ember.run.next(this, function(){
                    this._injectResults(results, results_id, index);
                })
            );
        }
        else {
            // The search has only truly finished once the results are on display

            console.log( 'FINISHED: ' + ( (new Date).getTime() - window._resultLoadTime ) );
            Stream2.timeMark('finished');
            Stream2.trigger('resultsLoaded');
            _this.controller.set('searchFinished',true);
        }
    },


    retrieveResults: function ( results_id, page_id ) {
        var controller = this.controller;
        var search = this.controllerFor('search');
        var board = search.get('selectedBoard');

        // The controller content is the board results content
        board.set('_results',[]);
        controller.set('results_id',results_id);

        var options = {};
        if ( this.isWatchdog() ){
            options.watchdog_id = search.get('watchdog').get('id');
        }

        var _this = this;
        return Stream2.request( results_id + "/page/" + page_id, options, 'GET' ).then(
            function ( data ) {
                if ( data.error ) {
                    search.set('error', data.error);
                }
                else {
                    // results_id needs to be maintained here to ensure that the loop is working on the
                    // most current results
                    // cancel all currently existing results again before proceeding.
                    controller.set('model',[]);
                    controller.set('timeGroups', []);
                    controller.set('searchFinished',true);
                    board.set('_results', data.results);

                    controller.set('userNotices' , data.notices || [] );

                    // search.watchdogViewed( board );

                    Ember.run.cancel( controller.get('resultLoaderTask') );

                    // enable PageNumber links, now that results are in
                    board.get('pageNumbers').setEach('isClickable', true);
                    //  only set the total results if it is supplied.
                    if (data.total_results) {
                        board.get('context').total_results = data.total_results;
                    }

                    window._resultLoadTime = (new Date).getTime();
                    _this._injectResults(data.results, results_id);
                    if ( data.facets ) {
                        var cb = function () {
                            Stream2.pushEvent('facet','click');
                            return search.facetSearch();
                        };
                        board.addReplaceFacets( data.facets, cb );
                    }
                }
                if ( board.get('selectedPage') ){
                    board.get('selectedPage').set('hasOldResults',false);
                }
            },
            null
        );
    }
});

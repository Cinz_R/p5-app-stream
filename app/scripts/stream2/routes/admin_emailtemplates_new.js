Stream2.AdminEmailtemplatesNewRoute = Ember.Route.extend(Stream2.UserHierarchyMixin,{
    templateName: 'admin/emailtemplates/edit',
    model: function(params) {
        var _this = this;
        return new Ember.RSVP.Promise(function(resolve, reject) {
            Stream2.request('/api/admin/user_hierarchy', {}, 'GET').then(
                function(hierarchy_data) {
                    var userMap = Ember.Object.create({ map: {}, ids: [] });
                    hierarchy_data = _this._vivifyModel(hierarchy_data, userMap);
                    // the default is set everyone able to use a emailtemplate
                    $.each(userMap.get('ids'), function(index, id) {
                        userMap.get('map')[id].setProperties({
                            current_setting: true,
                            boolean_value: true
                        });
                    });
                    return resolve({
                        hierarchy: hierarchy_data,
                        _userMap: userMap,
                        autoExpand: true
                    });
                },
                function(error) {
                    return reject(error);
                }
            );
        });
    },

    setupController: function(controller, model) {        
        controller.setProperties({
            "templateName": null,
            'emailReplyto': null,
            'emailBcc': null,
            'emailSubject': null,
            'emailBody': '',
            'templateRules' : []
        });
        this._super(controller, model);
    }
});

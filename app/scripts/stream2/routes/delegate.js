Stream2.DelegateRoute = Ember.Route.extend({
    model: function ( data ) {
        return data;
    },
    setupController: function ( controller, model ){
        var results = this.controllerFor('board_results');

        if ( Ember.isNone(results) || Ember.isEmpty(results.get('selectedCandidates')) ){
            return this.transitionTo('board.index', this.controllerFor('board').get('content'));
        }

        var selectedCandidates = results.get('selectedCandidates');
        var searchController = this.controllerFor('search');

        controller.set('delegating',false);
        controller.set(
            'selectedCandidates',
            selectedCandidates.filterBy('canBulkDelegate')
        );
        controller.set('outcomes',[]);
    },

    actions: {
        delegateBulk: function () {
            var _this = this;
            this.controller.set('delegating',true);
            this.controller.get('selectedCandidates').forEach( function( item ) {
                var outcome = Stream2.BulkCandidateOutcome.create({
                    candidate: item
                });
                Stream2.resetMark();
                _this.delegateCandidate( item ).then(
                    function ( data ) {
                        Stream2.timeMarkEvent('Candidate Actions','delegate');
                        outcome.set('finished',true);
                        outcome.set('message',data.message);
                        outcome.set('warnings',data.warnings);
                        item.set('has_done_delegate', true);
                    },
                    function ( error ) {
                        outcome.set('finished',true);
                        outcome.set('error',true);
                        outcome.set('message',error.message);
                    }
                );
                _this.controller.get('outcomes').pushObject( outcome );
            });
            Stream2.pushEvent('candidate','bulk delegate');
        }
    },
    delegateCandidate: function(candidate) {
        var colleague = this.controller.get('colleague');
        return Stream2.requestAsync(candidate.apiURL('delegate'), { colleague: colleague }, 'POST');
    },
});

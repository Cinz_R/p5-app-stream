// They are hitting http://XXXX/search/CRITERIA_ID directly
Stream2.CriteriaIndexRoute = Ember.Route.extend({
    model: function ( model, transition ){
        return model;
    },
    setupController: function ( controller, model ){

        $('#saved_searches_link').dropdown('hide');
        var searchController = this.controllerFor('search');

        // clear all alternative states
        searchController.expireAllResults();
        searchController.clearWatchdog();
        searchController.set('viewedLongList', null);

        var _this = this;
        Stream2.boardPromise.then(
            function () {
                Stream2.Boards.selectFirstSelectable();
                // If facets exist when a criteria is being built then the criteria will be overwritten to the existing facet values in controllers/search.js:_newCriteria,
                // clearing these rebuilds the facets with values provided by the server.
                searchController.get('selectedBoard').clearFacets();
                searchController.submitSearch({ 'noHistory': true });
            }
        );
    }
});

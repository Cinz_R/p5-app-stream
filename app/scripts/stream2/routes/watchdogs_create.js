Stream2.WatchdogsCreateRoute = Ember.Route.extend({
    model: function(criteria){
        return Stream2.Criterias.findById( criteria.criteria_id );
    },
    setupController: function ( controller, model ) {
        controller.set('criteria', model);
        // var kw = model.get('keywords') || "";

        // // Flatten kw to a pure string in case it
        // // is an array.
        // if( Ember.isArray(kw) ){
        //     kw = kw[0] || "";
        // }
        controller.set('watchdog' , Stream2.Watchdog.create({ boards: [] , name: ''}));

        // remove Responses board for users without Talent Search
        var boards = Stream2.Boards.find();
        var _this = this;
        return Stream2.boardPromise.then( function () {
            if ( ! _this.searchUser.get("hasTalentSearch") ) {
                boards = boards.filter( function(board) {
                    return board.name != 'adcresponses';
                });
            }
            controller.set('boards', boards);
        });
    }
});

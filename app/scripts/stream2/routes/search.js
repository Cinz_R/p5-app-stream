Stream2.SearchRoute = Ember.Route.extend({
    setupController: function ( controller, model ) {
        controller.set('criteria', controller._newCriteria());

        // once we have started looking for the boards
        // lets lodge a step which handles errors
        var _this = this;
        Stream2.boardPromise.then(
            function () {
                var boards = Stream2.Boards.find(); // Get all the boards
                controller.set('boards', boards);
                var selectedBoards = boards.filterBy('selected', true);
                if ( selectedBoards.length == 0 ){
                    Stream2.Boards.selectFirstSelectable();
                } else if (selectedBoards.length > 1) {
                    // make sure we always have one board selected, as some
                    // routes allow for multiple board selection
                    selectedBoards.setEach('selected', false);
                    Stream2.Boards.selectFirstSelectable();
                }
            },
            function () {
                _this.transitionTo('noboards');
            }
        );

        controller.loadCustomLists();
    }
});

Stream2.ShortlistCandidateRoute = Ember.Route.extend({
    model: function ( data ) {
        return data;
    },
    setupController: function ( controller, model ){
        var sl = this.controllerFor('shortlist');
        sl.set('selectedCandidates',[model]);
    }
});

Stream2.BoardRoute = Ember.Route.extend({
    model : function ( model, transition ){
        Stream2.Boards.find(); // This will load the boards if they are not there yet.
        return Stream2.boardPromise.then( function () {
            var id = model.id || model.board_id;
            var board = Stream2.Boards.findById(id);
            return board;
        });
    },
    setupController: function ( controller, board ) {
        if ( board.get('id') !== 'adcresponses' && this.searchUser.get('manageResponsesView') ){
            return this.transitionTo( '/' );
        }
        if ( board.get('disabled') ){
            return this.transitionTo( '/' );
        }
        controller.set('content', board);
        this.controllerFor('search').changeSelectedBoard( board );
    },
    actions: {
        facetSearch: function (){
            // this is called when you hit enter while editing a text facet
            Stream2.pushEvent('facet','click');
            return this.controllerFor('search').facetSearch();
        },
        changePage: function ( page ) {
            Stream2.pushEvent('page','click');
            return this.controllerFor('search').pageSearch( page );
        },
        goBack: function () {
            this.transitionTo('board.results', {
                id:         this.controller.get('selectedPage').get('id'),
                results_id: this.controller.get('results_id')
            });
        }
    },
    activate: function () {
        this.render('facets', { outlet: 'boardFacets', into: 'search' });
    }
});

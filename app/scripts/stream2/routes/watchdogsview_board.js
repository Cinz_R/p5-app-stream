Stream2.WatchdogsviewBoardRoute = Ember.Route.extend({
    model : function ( model ) {
        return model;
    },
    setupController: function ( controller, model ) {
        // You get to this route by choosing a watchdog to look with a specific board
        var wdController = this.controllerFor('watchdogsview');
        var id = wdController.get('id');
        var board_id = model.id || model.board_id;
        Stream2.Boards.find();
        Stream2.boardPromise.then(function () {
            return wdController.setupWatchdog( Stream2.Boards.findById( board_id ) );
        });
    }
});

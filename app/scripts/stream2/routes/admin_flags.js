Stream2.AdminFlagsRoute = Ember.Route.extend(Stream2.UserHierarchyMixin,{
    model: function () {
        var _this = this;
        return new Ember.RSVP.Promise( function ( resolve, reject ) {
            Stream2.PromiseGroup.collect(
                Stream2.request('/api/admin/response-flags', {}, 'GET'),
                Stream2.request('/api/admin/user_hierarchy', {} , 'GET' )
            ).then(
                function ( outcomes ) {

                    // reject if any of the calls fail
                    outcomes.forEach( function( outcome ) {
                        if ( Ember.isPresent( outcome.error ) ){
                            return reject( outcome.error );
                        }
                    });

                    var customFlagsData = outcomes.shift();
                    var hierarchyData = outcomes.shift();

                    var responseFlags = Ember.A();
                    var standardFlags = Ember.A();

                    customFlagsData.success.data.flags.forEach( function ( flag ) {
                        if (flag.type === 'standard') {
                            // not placed(9), unranked(7).
                            if (flag.flag_id != 9 && flag.flag_id != 7) {
                                standardFlags.pushObject(Stream2.ResponseFlagModel.create(flag));
                            }
                        }
                        else if (flag.type === 'custom') {
                            responseFlags.pushObject(Stream2.ResponseFlagModel.create(flag));
                        };
                    });

                    var userMap = Ember.Object.create({ map: {}, ids: [] });
                    resolve(Ember.Object.create({
                        standardFlags: standardFlags,
                        responseFlags: responseFlags,
                        hierarchy: _this._vivifyModel( hierarchyData.success, userMap ),
                        _userMap: userMap
                    }));
                },
                function ( error ) {
                    reject({ error: error });
                }
            );
        });
    },
    actions: {
        updateFlagPermissions: function ( user ) {
            var _this = this;

            var settings = {
                value   : this._userValue( user ),
                users   : this._readChildren( user )
            };
            Stream2.request( this.controller.get('permsFlag.setting_url'), settings, 'POST' ).then(
                null, // Dont do anything in case of success.
                function ( error ){
                    Stream2.popupError(i18n.__('Your settings have not been saved'));
                    _this.controller.propertyDidChange('setting');
                }
            );
        },
        updateFlag: function ( flag ) {
            var _this = this.controller;
            var newProperties = flag.getProperties('description','colour_hex_code');

            return Stream2.request('/api/admin/response-flags/' + flag.get('id'), newProperties, 'PUT').then(
                function ( data ) {
                    Stream2.popupAlert(
                        i18n.__('Flag updated'),
                        i18n.__x('Your flag "{name}" has been updated', { name: flag.get('description') })
                    );

                    // overwrite existing flag with new updated properties
                    var flagData = data.data;
                    flagData.editMode = false;
                    _this.get('model.responseFlags').findBy( 'id', flag.get('id') ).setProperties( flagData );
                },
                function ( error ) {
                    Stream2.popupError( error );
                }
            );
        },
        addFlag: function ( flag ) {
            var _this = this.controller;
            Stream2.request(
                '/api/admin/response-flags',
                flag.getProperties( 'id', 'description', 'colour_hex_code'),
                'POST')
            .then(
                function ( data ) {
                    flag.setProperties(data.data);
                    Stream2.popupAlert(
                        i18n.__('Flag added'),
                        i18n.__x('Your flag "{name}" has been added', { name: flag.get('description') })
                    );
                    _this.get('model.responseFlags').pushObject( flag );
                    _this.set('showAddForm', false);
                },
                function ( error ) {
                    Stream2.popupError( error );
                }
            );
        }
    }
});

Stream2.ForwardRoute = Ember.Route.extend({
    model: function ( data ) {
        return data;
    },
    setupController: function ( controller, model ){
        var results = this.controllerFor('board_results');
        var selectedCandidates = results.get('selectedCandidates');

        if ( typeof(selectedCandidates) == "undefined" ){
            return this.transitionTo('board.index', this.controllerFor('board').get('content'));
        }

        controller.set('forwardingCVs',false);
        controller.set('outcomes',[]);

        // Set the recipients to the current user
        var contact_email = this.searchUser.get('contact_email');
        if( contact_email ){
            controller.set('recipients' , [{ id: this.searchUser.get('user_id'),
                                             text: this.searchUser.get('contact_name') + ' (' + contact_email + ')' }] );
        }else{
            controller.set('recipients' , [] );
        }
        controller.set(
            'selectedCandidates',
            selectedCandidates.filterBy('canBulkForward')
        );

    },
    actions: {
        multiForward: function () {
            var _this = this;
            this.controller.set('forwardingCVs',true);

            if ( this.controller.get('sendAsBulk') ){
                this.forwardBulk();
            }
            else {
                this.forwardIndividual();
            }

            Stream2.pushEvent('candidate','bulk forward cv');
        }
    },
    forwardIndividual: function () {
        var _this = this;
        this.controller.get('selectedCandidates').forEach( function( item ) {
             var outcome = Stream2.BulkCandidateOutcome.create({
                 candidate: item
             });
             Stream2.resetMark();
             _this._forwardCV( item ).then(
                 function ( data ) {
                    Stream2.timeMarkEvent('Candidate Actions','forward');
                    outcome.set('finished',true);
                    outcome.set('message',data.message);
                    outcome.set('warnings',data.warnings);
                 },
                function ( error ) {
                    outcome.set('finished',true);
                    outcome.set('error',true);
                    outcome.set('message',error.message);
                }
             );
             _this.controller.get('outcomes').pushObject( outcome );
        });
    },
    forwardBulk: function () {
        var _this = this;
        var outcome = Stream2.BulkCandidateOutcome.create({
            candidate: Ember.Object.create({
                identity: 'Bulk Forward'
            })
        });

        var candidates = this.controller.get('selectedCandidates');

        Stream2.resetMark();
        Stream2.requestAsync(
            candidates[0].get('results_id') + "/bulk_forward",
            {
                recipients         : this.controller.get('recipients').getEach('id'),
                message            : this.controller.get('messageContent'),
                candidate_idxs     : candidates.getEach('idx'),
                board              : this.controllerFor('board').get('content.id'),
                cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode()
            },
            'POST'
        ).then(
            function ( message ) {
                Stream2.timeMarkEvent('Candidate Actions','bulk_forward');

                outcome.set('finished', true);
                outcome.set('message', message.message);
                outcome.set('warnings', message.warnings);

                candidates.setEach('has_done_forward', true);
                candidates.setEach('has_done_download', true);
            },
            function ( error ) {
                outcome.set('finished', true);
                outcome.set('error', true);
                outcome.set('message', error.message);
            }
        );
        this.controller.get('outcomes').pushObject( outcome );
    },

    _forwardCV: function ( candidate ) {
        var _this = this;
        return Stream2.requestCandidateAsync(
            candidate,
            candidate.apiURL( 'forward' ),
            {
                recipients: this.controller.get('recipients').getEach('id'),
                message : this.controller.get('messageContent'),
                cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode()
            },
            'POST'
        );
    }
});

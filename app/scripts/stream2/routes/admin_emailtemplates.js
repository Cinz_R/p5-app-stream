Stream2.AdminEmailtemplatesIndexRoute = Ember.Route.extend({
    queryParams: {
        page: {
            refreshModel: true
        }
    },

    model: function( params ){
        return new Ember.RSVP.Promise(function ( resolve, reject ) {
            Stream2.request( '/api/admin/email-templates', params, 'GET' ).then(
                function ( data ) {
                    data.emailtemplates.forEach(function(template_data){
                        template_data.rules_data = template_data.rules_data.map(function( rule_data ){
                            return Stream2['EmailTemplateRule' + rule_data.role_name].create( rule_data );
                        })
                    });
                    return resolve( data );
                },
                function ( error ) { return reject( error ); }
            );
        });
    }
});


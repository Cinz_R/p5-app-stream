Stream2.AdminTagsRoute = Ember.Route.extend({
    model : function ( data ) {
        return data;
    },
    setupController: function ( controller, model ) {
        controller.set('page', model.page_id);
        controller.search();
    }
});

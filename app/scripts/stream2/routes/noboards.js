Stream2.NoboardsRoute = Ember.Route.extend({
    // If they hit this route but they do have boards
    // this will run the search
    setupController: function ( controller ) {
        var search = this.controllerFor('search');
        Stream2.boardPromise.then(
            function () {
                search.submitSearch();
            },
            null
        );
    }
});

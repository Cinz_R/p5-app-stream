Stream2.WatchdogsviewIndexRoute = Ember.Route.extend({
    setupController: function ( controller ) {
        // You get to this route by choosing a watchdog to look at but not a specific board
        var _this = this;
        var wdController = this.controllerFor('watchdogsview');
        var id = wdController.get('id');
        Stream2.Boards.find();
        Stream2.boardPromise.then(function () {
            return wdController.setupWatchdog();
        });
    }
});

Stream2.CandidateChooserComponent = Ember.Component.extend({
    actions: {
        deselect: function ( candidate ) {
            this.get('candidates').removeObject( candidate );
            candidate.set('isSelected',false);
            if ( this.get('candidates').length <= 0 ){
                this.sendAction('back');
            }
        }
    }
});


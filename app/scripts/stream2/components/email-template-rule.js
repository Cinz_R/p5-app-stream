Stream2.EmailTemplateRuleComponent = Ember.Component.extend({
    rule: null
});
Stream2.EmailTemplateRuleResponseFlaggedComponent = Stream2.EmailTemplateRuleComponent.extend({
    flagsAsDropdown: Ember.computed( 'searchUser.flags', function (){
        return this.get('searchUser.flags').map( function ( flag ) {
            return {
                id: flag.value,
                style: flag.style,
                text: flag.description + ( flag.active ? '' : ' (inactive)' )
            };
        });
    })
});

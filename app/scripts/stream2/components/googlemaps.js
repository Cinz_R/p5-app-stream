// this is a compnent that will help us deal with googlemaps in a more
// reuseable way
Stream2.GoogleMapsBaseComponent = Ember.Component.extend({
    tagName:'',
    hSize: 400,
    wSize: 400,
    zSize: 10,
    candidate: null,

    googlemapStaticImageUrl: function() {
        var latlon = this.get('candidate.calculatedLatLong');
        return 'https://maps.googleapis.com/maps/api/staticmap?center=' +
            latlon + '&zoom=' + this.zSize + '&size=' + this.hSize + 'x' +
            this.wSize + '&maptype=roadmap&sensor=false&markers=' + latlon;
    }.property('candidate.calculatedLatLong'),

    googlemapUrl: function() {
        var latlon = this.get('candidate.calculatedLatLong');
        return 'https://www.google.com/maps/search/'+ latlon;
    }.property('candidate.calculatedLatLong')


});


// // this is a compnent that will help us deal with googlemaps in a more
// // reuseable way
Stream2.GoogleMapsStaticImageComponent = Stream2.GoogleMapsBaseComponent.extend({
    hasLink: null
});

Stream2.GoogleMapsUrlComponent = Stream2.GoogleMapsBaseComponent.extend({
    anchorValue: null
});

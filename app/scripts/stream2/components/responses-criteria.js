/* Stream2.ResponsesCriteriaComponent

Contains view methods used within the main criteria portion
of the Search interface while in a responses context

*/
Stream2.ResponsesCriteriaComponent = Stream2.MainCriteriaComponent.extend({
    hideRead: false,
    showSnippet: true,

    /* readFacet
        Will remain in-sync with the "viewed" facet for adcresponses
    */
    readFacet: function () {
        return this.get('board').getFacetByName('broadbean_adcresponse_is_read');
    }.property('board.facets'),
    _hideRead_observe: function () {
        // update the criteria and the facet as well
        if ( this.get('_suspendObservations') ) { return; }

        var facet = this.get('readFacet');

        // if the boards facets have yet to be loaded then we don't need to fiddle with them,
        // instead we should add to the global criteria directly.
        if ( ! facet ){
            return this.set( 'criteria.adcresponses_broadbean_adcresponse_is_read', [ this.get('hideRead') ? "false" : "" ] );
        }

        this.set('_suspendObservations', true);
        facet.set('SelectedValues', this.get('hideRead') ? { "false": 1 } : {} );
        facet.propertyDidChange('Options');
        facet.onChange();
        Ember.run.once(this, function() { this.set('_suspendObservations',false); });
    }.observes('hideRead'),
    _facetObserver: function () {
        // the facet or criteria has changed independently of the checkbox
        if ( this.get('_suspendObservations') ) { return; }
        var facetOptions = this.get('readFacet.Options') || [];
        if ( ! facetOptions.length ) { return; }

        var selected = this.get('criteria.adcresponses_broadbean_adcresponse_is_read') || [];
        this.set('_suspendObservations', true);
        if ( selected.length === 1 && selected[0] == "false" ){
            this.set('hideRead', true);
        }
        else {
            this.set('hideRead', false);
        }
        Ember.run.once(this, function() { this.set('_suspendObservations',false); });
    }.observes('criteria.adcresponses_broadbean_adcresponse_is_read', 'readFacet.Options'),

    _showSnippet_observe: function () {
        this.sendAction('toggleSnippet');
    }.observes('showSnippet')

});

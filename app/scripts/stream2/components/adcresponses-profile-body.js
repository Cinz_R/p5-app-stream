/* AdcresponsesProfileComponent
    a special component to represent the adcresponses profile body
    in a more dynamic and prettier way
*/
Stream2.AdcresponsesProfileBodyComponent = Stream2.DefaultProfileBodyComponent.extend({
    showEmailBody: false,
    actions: {
        toggleEmailBody: function () {
            this.toggleProperty('showEmailBody');
        }
    },
    isLoaded: Ember.computed( 'candidate.profile_body', function () {
        return ! Ember.isNone( this.get('candidate.profile_body') );
    })
});

/* displays the inline application history for a candidate */
Stream2.CandidateFlaggingHistoryComponent = Ember.Component.extend({
    classNames: ['unit', 'flag-history'],
    attributeBindings: ['title'],
    title: Ember.computed.alias('view.translations.placeholderFlaggingHistory'),
    flags: function () {

        var _this = this;
        var flags = [];
        var flagging_history = this.get('flagging_history');

        // show standard flags first
        // unranked, red, amber, green, progressed(green)
        [ 7, 1, 3, 5, 6 ].forEach( function ( rank ) {
            if ( flagging_history[rank] ){
                flags.push( Ember.Object.create({ style: _this.searchUser.flagRankToStyle(rank), count: flagging_history[rank] }) );
            }
        });

        // show custom flags last
        $.each( flagging_history, function ( rank, count ) {
            if ( rank >= 20 ){
                flags.addObject( Ember.Object.create({ style: _this.searchUser.flagRankToStyle(rank), count: flagging_history[rank] }) );
            }
        });

        return flags;

    }.property('flagging_history')
});

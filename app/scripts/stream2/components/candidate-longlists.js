/*
  A self contained component that
  allows adding a candidate to a longlist and to create another
  longlist in the process.

  Inline style candidate longlists
   Bind the 'shown' property to an external controlling object (here c) by using it like:

   {{candidate-longlists-inline candidate=c shownBinding="c.showLongLists"}}
*/

Stream2.CandidateLonglistsInlineComponent = Ember.Component.extend({
    candidate: null,
    user: null,

    // Richer lists is a clone of
    // the user.long_lists
    // where each long list has got an extra property
    // called 'on'
    richerLists: [],
    
    // Gui stuff
    newListName: null,
    trimmedListName: function(){
        var name = this.get('newListName');
        if( Ember.isEmpty(name) ){ return null; }
        return name.replace(/^\s+|\s+$/g, '');
    }.property('newListName'),
    
    formIsShown: false,

    formInvalid: function(){
        if( ! this.get('formIsShown') ){
            return true;
        }
        // A word character in the newListName is required.
        if( ( this.get('newListName') || '' ).match( /\w/ ) ){
            return false;
        }
        return true;
    }.property('formIsShown' , 'newListName'),

    didInsertElement: function(){
    },

    shown: false, // Shown is a slot to be Bound to an external property
    shownHasChanged: function(){
        if( this.get('shown') ){
            this.send('showForm');
        }else{
            this.send('hideForm');
        }
    }.observes('shown'),

    actions:{
        switchOff: function(){
            this.set('shown', false);
        },
        showForm: function(){
            var _this = this;
            this.set('formIsShown' , true);
            Ember.run.next(this,function(){
                Stream2.request(this.get('candidate').apiURL('longlist'), {} , 'GET' ).then(
                    function ( data ) {
                        var longlist_membership = data.longlist_membership;
                        var newRicherLists = _this.get('user').get('long_lists').map(function( item ){
                            var clone = item.copy();
                            clone.set('on' , longlist_membership[clone.get('id')]);
                            return clone;
                        }).filter(function(item){
                            var candidate_longlist_id = _this.get('candidate').get('stream2_longlist_id');
                            if( ! candidate_longlist_id ){ return true; } // Candidate not in long list, no filter.
                            return candidate_longlist_id != item.get('id'); // Dont fiddle with the viewed long list.
                        });
                        _this.set('richerLists' , newRicherLists); 
                    },
                    null
                );

                this.$().find('.inline-form').first().slideDown('fast');
            });
        },
        hideForm: function(){
            var _this = this;
            this.$().find('.inline-form').first().slideUp('fast', function(){
                _this.set('formIsShown' , false);
            });
        },
        /* Toggle the candidate in the given longlist or creates and add the candidate to
           a new one that goes by the name 'trimmedListName'
        */
        createOrToggle: function(longlist , onlyAddPlease){
            var _this = this;
            var candidate = this.get('candidate');
            var url = candidate.apiURL('longlist');
            var newName = this.get('trimmedListName');

            if( Ember.isEmpty(longlist) && Ember.isEmpty(newName) ){
                candidate.set('error' , "No longlist clicked and no newName");
                return;
            }
            this.$().find('.inline-form').first().slideUp('fast', function(){
                // After success:
                _this.set('formIsShown' , false);
                // Make sure this is switched off
                _this.send('switchOff');
                _this.set('newListName' , null);

                // This is toggling an existing longlist
                var should_remove = ( longlist ? longlist.get('on') : false );
                if( onlyAddPlease ){
                    // Force should_remove to false
                    should_remove = false;
                }
                Stream2.resetMark();
                Stream2.request( candidate.apiURL( 'longlist' ),
                                 { new_long_list_name: newName,
                                   id: ( longlist ? longlist.get('id') : null ),
                                   remove: should_remove,
                                 }, 'POST')
                    .then(function(data){
                        var newLongList =  Stream2.LongList.create( data.longlist );

                        if( ! longlist ){
                            // A long list was created
                            Stream2.pushEvent('search','create_longlist');
                        }
                        _this.get('user').add_longlist( newLongList );


                        if( should_remove ){
                            Stream2.timeMarkEvent('Candidate Actions','longlist_remove');
                            candidate.set('has_done_longlist_remove', true);
                            Stream2.pushEvent('candidate','remove from longlist');
                        }else{
                            Stream2.timeMarkEvent('Candidate Actions','longlist');
                            _this.get('user').set('last_used_longlist' , newLongList );
                            candidate.set('has_done_longlist_add' , true);
                            Stream2.pushEvent('candidate','add to longlist');
                        }

                        if( candidate.get('historicActionsChronological') ){
                            // The history is there. We should refresh it.
                            candidate.fetchProfileHistory();
                        }
                        Stream2.popupAlert(should_remove ? i18n.__("Candidate Removed") : i18n.__("Candidate Added") , data.message );
                    }, function( error ){ candidate.set('error' , error ); } );
            });
        }
    }
});

/*
  A self contained component that
  allows messaging a single candidate inline
*/
Stream2.CandidateMessageComponent = Ember.Component.extend({
    candidate: null,
    customLists: null,
    // informs the backend where the request came from (admins can view all adverts).
    uiOrigin: 'message',
    linkableCustomLists: function(){
        return this.get('customLists').filterBy('allowPreview');
    }.property('customLists.@each.name'),

    // Logical stuff

    // This cheese sandwich is set by the select2
    cheeseSandwich: null,

    // The Object counter part of the Cheese Sandwich
    cheeseSandwichObject: function(){
        var cs = this.get('cheeseSandwich');
        if( ! cs ){ return null }
        return Stream2.CheeseSandwich.create(cs);
    }.property('cheeseSandwich'),

    messageContent: null,
    messageSubject: null,
    messageEmail:  null,
    messageBcc: null,

    // For the preview
    advertId: function(){
        var cs = this.get('cheeseSandwich');
        if( cs ){
            return cs.id;
        }
        return null;
    }.property('cheeseSandwich'),

    advertList: function(){
        var cs = this.get('cheeseSandwich');
        if( cs ){
            return cs.list;
        }
        return null;
    }.property('cheeseSandwich'),

    cheeseSandwichChanged: function(){
        var _this = this;
        var cs = this.get('cheeseSandwich') || {};

        var message = this.get('messageContent') || '';
        var subject = this.get('messageSubject') || '';
        if( ! cs.id ){
            // No cheese sandwich. Make sure to wipe all [advert_link] from the message content
            // Do not do anything destructive for now (we could remove the advert_link automatically.
            // but no.
           return;
        }

        if( ! /\[advert_link\]/.test(message) ){
            this.set('_tinymceUpdateContent',true);
            Ember.run.next(function(){
                _this.set('messageContent', message + "\n[advert_link]");
            });
        }
        return;
    }.observes('cheeseSandwich'),

    emailTemplateChanged: function () {
        var et = this.get('emailTemplate');
        if ( ! et ) { return; }

        var _this = this;
        Stream2.request( '/api/user/email-templates/' + et.id, {}, 'GET' ).then(
            function ( data ) {
                _this.set('_tinymceUpdateContent',true);
                Ember.run.next(function(){
                    _this.set('messageContent', data.emailBody);
                    _this.set('messageSubject', data.emailSubject);
                    _this.set('messageEmail', data.emailReplyto);
                    _this.set('messageBcc', data.emailBcc);
                });
            }
        );
    }.observes('emailTemplate').on('init'),

    /* Is this candidate paid? */
    hasPaidCandidate: function(){
        if( Ember.isNone( this.get('candidate') ) ){
            return false;
        }
        return this.get('candidate').get('hasPayMessage');
    }.property('candidate'),

    // Gui stuff
    formIsShown: false,

    // Adverts are allowed to be shown if they can be 'linked'
    advertsAreAllowed: function(){
        return ! Ember.isEmpty( this.get('linkableCustomLists') );
    }.property('linkableCustomLists.@each'),

    formInvalid: function(){
        if( ! this.get('formIsShown') ){
            return true;
        }
        if (
                ! this.get('messageContent') ||
                ! this.get('messageSubject') ||
                ! this.get('messageEmail')
        ) return true; // it is invalid

        return false;
    }.property('formIsShown', 'messageContent','messageSubject', 'messageEmail'),

    onNotShown: function(){
        if( ! this.get('formIsShown') ){
            this.set('emailTemplate', null);
        }
    }.observes('formIsShown'),
    canMessage: function() {
        if (this.get('candidate.has_done_message')) return true;
        return this.get('candidate.canDownloadCV');
    }.property('candidate.has_done_message', 'candidate.canDownloadCV'),

    actions:{

        showForm: function(){
            var _this = this;
            if (!this.get('canMessage')) return;
            this.set('formIsShown' , true);
            Ember.run.next(this,function(){

                // Query the defaults from the API and set them
                Stream2.request( '/api/user/message-defaults', { destination: this.get('candidate').get('destination') } , 'GET' ).then(
                    function ( data ) {
                        if ( data.from )    _this.set( 'messageEmail'  , data.from );
                        if ( data.content ){
                            _this.set('_tinymceUpdateContent',true);
                            // Run only after _tinymceUpdateContent has been set
                            Ember.run.next(function(){
                                _this.set( 'messageContent', data.content );
                            });
                        }
                        if ( data.subject ) _this.set( 'messageSubject', data.subject );
                        _this.set('messageSubjectPrefix' , data.subject_prefix );
                    },
                    null
                );


                this.$().find('.inline-form').first().slideDown('fast');
            });
        },

        hideForm: function(){
            var _this = this;
            this.$().find('.inline-form').first().slideUp('fast', function(){
                _this.set('formIsShown' , false);
            });
        },

        switchOff: function(){
            // Does nothing here. Might do in subclasses
        },

        // queryCheeseSandwiches for the select2 component.
        // Note that select2 is cool and will keep the 'list' property of the
        // cheeseSandwich as it is.
        queryCheeseSandwiches: function(query, deferred){
            var query = Ember.Object.create({
                search: query.term,
                ui_origin: this.get('uiOrigin'),
            });

            var promises = this.get('linkableCustomLists').map(function(list){
                return Stream2.request( '/api/cheesesandwiches/' + list.get('name') , query , 'GET' );
            });
            Ember.RSVP.all(promises).then(function(results){
                var cheeseSandwiches = [];
                // Map over each successfull data
                results.map(function(data){
                    // Map over each suggestion
                    Ember.A(data.suggestions).map(function(cheeseSandwich){
                        cheeseSandwiches.push({ id: cheeseSandwich.value,
                                                text: cheeseSandwich.label,
                                                list: cheeseSandwich.list
                                              });
                    })
                });
                deferred.resolve(cheeseSandwiches);
            });
        },

        queryEmailTemplates: function ( query, deferred ) {
            var term = query.term;
            var _this = this;
            Stream2.request( '/api/user/email-templates', { search: term, candidate: this.get('candidate.id')  }, 'GET' ).then(
                function ( data ) {
                    var templates = [];

                    Ember.A( data.emailtemplates ).forEach( function ( template ) {
                        var alreadySent = _this.get('candidate').sent_template_ids.includes(template.id)
                        var templateName = alreadySent ? Ember.String.htmlSafe( i18n.__x('{templateName} (template already sent)', { templateName: template.name }) ) : template.name;
                        var richLabel = Ember.View.create({
                            template: _this.container.lookup('template:email-templates'),
                            context: {
                                alreadySent: alreadySent,
                                template: template
                            }
                        }).createElement().element.innerHTML;

                        templates.push({
                            id         : template.id,
                            rich_label : richLabel,
                            text       : templateName
                        });
                    });
                    deferred.resolve( templates );
                }
            );
        },

        // Perform the message sending for real on this candidate.
        sendMessage: function(){
            var _this = this;
            var candidate = this.get('candidate');
            var emailTemplate = this.get('emailTemplate');

            this.$().find('.inline-form').first().slideUp('fast', function(){
                _this.set('formIsShown' , false);

                // Make sure this is switched off
                _this.send('switchOff');

                Stream2.popupAlert(i18n.__("Sending message.."));
                Stream2.resetMark();

                Stream2.requestCandidateAsync(
                    candidate,
                    candidate.apiURL( 'message' ),
                    {
                        messageEmail:          _this.get('messageEmail'),
                        messageContent:        _this.get('messageContent'),
                        messageSubject:        _this.get('messageSubject'),
                        messageSubjectPrefix:  _this.get('messageSubjectPrefix'),
                        messageBcc:            _this.get('messageBcc'),
                        advertId:              _this.get('advertId'),
                        advertList:            _this.get('advertList'),
                        emailTemplateName:     emailTemplate ? emailTemplate.name : '',
                        emailTemplateId:       emailTemplate ? emailTemplate.id   : '',
                        cb_oneiam_auth_code:   _this.searchUser.cbOneIAMAuthCode(),
                    }
                ).then(
                    function ( message ) {
                        Stream2.timeMarkEvent('Candidate Actions','message');
                        Stream2.popupAlert(i18n.__("Message sent"), message.message );
                        if( emailTemplate ){
                            // So that the template title indicates it has been sent without needing to reload results
                            candidate.get('sent_template_ids').push(emailTemplate.id);
                        }
                        if( candidate.get('historicActionsChronological') ){
                            // The history is there. We should refresh it.
                            candidate.fetchProfileHistory();
                        }
                    }, function( error ){ console.log( error ); }
                );
            });

            Stream2.pushEvent('candidate','message');
        }

    }
});


/* Inline style candidate message
   Bind the 'shown' property to an external controlling object (here c) by using it like:

   {{candidate-forward-inline candidate=c shownBinding="c.showForward"}}

 */
Stream2.CandidateMessageInlineComponent = Stream2.CandidateMessageComponent.extend({
    shown: false, // Shown is a slot to be Bound to an external property

    scrollToCandidate: function(){
        var candidate = this.get('candidate');
        Ember.run.later(function(){
            $('html, body').animate({
                scrollTop: ( $("#candidate_" +  candidate.get('idx')).offset().top - 45 )
            }, 550);
        });

    },

    shownHasChanged: function(){
        if( this.get('shown') ){
            this.send('showForm');
            this.scrollToCandidate();
        }else{
            this.send('hideForm');
        }
    }.observes('shown'),

    actions:{
        /* Use that for the cancel button in this component */
        switchOff: function(){
            this.set('shown', false);
            this.scrollToCandidate();
        }
    }
});

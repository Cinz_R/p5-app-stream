/*
  A self contained component that
  allows shortlisting a single candidate inline
*/
Stream2.CandidateShortlistComponent = Ember.Component.extend({
    // Instance properties
    candidate: null,
    list: null,

    // Logical stuff
    cheeseSandwich: null,

    // informs the backend where the request came from (admins can view all adverts).
    uiOrigin: 'shortlist',

    // gives the ability to show shortlists in a grouped fashion in select2 input
    groupSandwiches: false,

    // gives the ability filter out archived jobs (or not)
    filterOutArchived: true,

    // Gui stuff
    formIsShown: false,

    formInvalid: function(){
        if( ! this.get('formIsShown') ){
            return true;
        }

        if( Ember.isEmpty( this.get('cheeseSandwich') ) ){
            return true;
        }
        return false;

    }.property('formIsShown' , 'cheeseSandwich'),

    canShortlist: function() {
        var list_name = this.get('list.name');
        if (this.get('candidate').get('has_done_shortlist_' + list_name)) return true;
        return this.get('candidate.canDownloadCV');
    }.property('list.name', 'candidate.canDownloadCV'),

    actions:{

        doShortlist: function(){
            var _this = this;
            var candidate = _this.get('candidate');
            var list = _this.get('list');

            this.$().find('.inline-form').first().slideUp('fast', function(){

                _this.set('formIsShown' , false);

                // Make sure this is switched off
                _this.send('switchOff');
                Stream2.popupAlert(i18n.__x("Adding to {list_item_name}", { list_item_name : list.get('label') } ));

                // one exception! - if the candidate is already in adcresponses and the user is
                // shortlisting the response against the same advert... then just change the flag
                if (
                    candidate.get('destination')            === 'adcresponses' &&
                    list.get('name')                        === 'adc_shortlist' &&
                    candidate.get('broadbean_adcadvert_id') === _this.get('cheeseSandwich').id
                ){
                    return candidate.set('broadbean_adcresponse_rank', 5);
                }

                Stream2.resetMark();
                return Stream2.requestCandidateAsync(
                    candidate,
                    candidate.apiURL( 'shortlist' ),
                    {
                        advert_id          : _this.get('cheeseSandwich').id,
                        advert_label       : _this.get('cheeseSandwich').text,
                        list_name          : list.get('name'),
                        cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode()
                    }
                ).then(
                    function ( message ) {
                        Stream2.timeMarkEvent('Candidate Actions','shortlist');

                        // Only update stuff in talentsearch
                        if( candidate.get('destination') == 'talentsearch' ){
                            // Force a refresh of application history..
                            candidate.set('_applicationHistory', null);

                            // Increment the green flags count,
                            // only if the list name is internal adcourier,
                            // because green flags work only with internal adcourier
                            // adverts.
                            if( list.get('name') === 'adc_shortlist' ){
                                if( Ember.isNone(candidate.get('flagging_history').green) ){
                                    candidate.get('flagging_history').green = 0
                                }
                                candidate.get('flagging_history').green++;
                                candidate.propertyDidChange('flagging_history');
                            }
                        }
                        if( candidate.get('historicActionsChronological') ){
                            // The history is there. We should refresh it.
                            candidate.fetchProfileHistory();
                        }
                        Stream2.popupAlert(list.get('label'), message.message );
                    }, function( error ){ console.log( error ); }
                );
            });

            Stream2.pushEvent('candidate','shortlist');
        },

        showForm: function(){
            if (!this.get('canShortlist')) return;
            this.set('formIsShown' , true);
            Ember.run.next(this,function(){
                this.$().find('.inline-form').first().slideDown('fast');
            });
        },

        switchOff: function(){
            // Does nothing here. Might do in subclasses
        },


        hideForm: function(){
            var _this = this;
            this.$().find('.inline-form').first().slideUp('fast', function(){
                _this.set('formIsShown' , false);
            });
        },

        queryCheeseSandwiches: function(query, deferred){
            var query = Ember.Object.create({
                search: query.term,
                ui_origin: this.get('uiOrigin'),
                group_sandwiches: this.get('groupSandwiches'),
                filter_out_archived: this.get('filterOutArchived')
            });
            var toCancel = this.get('cheeseTimer');
            if( toCancel ){
                Ember.run.cancel( toCancel );
            }
            this.set('cheeseTimer', Ember.run.later(this, function(){
                Stream2.singleRequest( '/api/cheesesandwiches/' + this.get('list').get('name'), query , 'GET' ).then(
                    function ( data ) {
                        // if the suggestions are grouped, create a structure that select2
                        // can use to nest the options, otherwise resolve a straight array
                        var sandwiches = [];
                        if ( data.groups && data.groups.length > 0 ) {
                            sandwiches = data.groups.map(function(group) {
                                return {
                                    text: group,
                                    children: Ember.A(data.suggestions[group]).map(function(item){
                                        return {
                                            id: item.value,
                                            text: item.label
                                        }
                                    })
                                }
                            });
                        }
                        else {
                            sandwiches = Ember.A(data.suggestions).map(function(item){
                                return {
                                    id: item.value,
                                    text: item.label
                                }
                            });
                        }
                        deferred.resolve(sandwiches);
                    });
            },1000));
        }
    }
});


/* Inline style candidate shortlisting
   Bind the 'shown' property to an external controlling object (here c) by using it like:

   {{candidate-shorlist-inline candidate=c list=customList shownBinding="c.showShorlist"}}

 */
Stream2.CandidateShortlistInlineComponent = Stream2.CandidateShortlistComponent.extend({

    // Prefix to which we want to bind
    shown_binding_prefix: null,

    shown: false, // Shown is a slot to be Bound to an external property

    init: function(){
        var bindingSource = "candidate." + this.get('shown_binding_prefix') + this.get('list').get('name');
        // console.log("Binding source : " + bindingSource );
        var binding = Ember.Binding.from(bindingSource).to('shown');
        binding.connect(this);
        return this._super();
    },


    shownHasChanged: function(){
        if( this.get('shown') ){
            this.send('showForm');
        }else{
            this.send('hideForm');
        }
    }.observes('shown'),

    actions:{
        /* Use that for the cancel button in this component */
        switchOff: function(){
            this.set('shown', false);
        }
    },

    title: Ember.computed( 'list.name','searchUser.manageResponsesView', function () {
        if ( this.get('list.name') === 'adc_shortlist' ){
            if ( this.get('searchUser.manageResponsesView') ){
                return i18n.__p( 'manage-responses-view', 'Shortlist' );
            }
            return i18n.__( 'Shortlist' );
        }
        return this.get('list.label');
    })

});

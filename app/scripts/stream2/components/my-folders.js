/**
 * This component represents an input element, expecting the name of a
 * MyFolder to pair with a {{#crossLink "Candidate"}}{{/crossLink}}, and a
 * list of 0 or more
 * {{#crossLink "MyFoldersSuggestionComponent"}}{{/crossLink}}s which the user
 * can click instead of typing the name of a folder in full. Such suggestions
 * dynamically change to match the value of the input, aiming to auto-
 * complete it for the user's convenience.
 *
 * @class MyFoldersComponent
 * @constructor
 */
Stream2.MyFoldersComponent = Ember.Component.extend( {

    candidate: null,    // candidate.usertags contains existing folders
    input: '',
    suggestedFolders: [],
    isShowing: false,
    dropdown: null,

    // whether the last call to the server to get matching folders failed
    folderLoadError: false,

    /* This component uses the jQuery Bootstrap style dropdown
     * (http://labs.abeautifulsite.net/jquery-dropdown/) to render itself.
     * Subscribe to its hide and show events so we can tell when the user
     * has opened and closed it
     */
    didInsertElement: function() {
        var dropdownId = "#add-folder-" + this.get("candidate").get("idx");
        var $dropdown  = this.$(dropdownId);
        this.set("dropdown", $dropdown);
        var _this = this;
        $dropdown
            .on("show", function() {
                _this.set("isShowing", true);
                _this.loadSuggestions();
            })
            .on("hide", function() {
                _this.set("isShowing", false);
                _this.set("input", "");
            });
    },

    // for making the component's view update ASAP
    folderNames: function() {
        var content = this.get("candidate").get("usertags").mapBy("id");
        return Ember.ArrayProxy.create( { content: Ember.A(content) } );
    }.property("candidate.usertags"),

    inputChanged: function() {
        this.loadSuggestions();
    }.observes("input"),

    /**
     * Loads the names of existing folders that begin with the value of this
     * object's `input` property. Creates an instance of
     * {{#crossLink "MyFoldersSuggestionComponent"}}{{/crossLink}} for each
     * one.
     *
     * @method loadSuggestions
     */
    loadSuggestions: function() {
        var outerThis = this;
        Stream2.request(
            '/api/user/suggest_usertag',
            { search: this.get("input") },
            'GET'
        ).then(
            function ( data ) {
                var matches = data.suggestions.filter( function(elem) {
                    var existingTags = outerThis.get("candidate")
                                                .get("usertags").mapBy("id");
                    return !elem.hasOwnProperty("description")
                           && existingTags.indexOf(elem.id) == -1;
                }).map( function(elem) {
                    return elem.text;
                });
                outerThis.set( "folderLoadError", false );
                outerThis.set( "suggestedFolders", matches );
            },
            function (error) {
                outerThis.set( "folderLoadError", true );
            }
        );
    },

    actions: {

        /**
         * Associates a folder with a {{#crossLink "Candidate"}}{{/crossLink}}
         *
         * @method save
         */
        save: function() {
            var newTagName = this.get("input");
            var candidate  = this.get("candidate");
            var folders    = candidate.get("usertags");
            folders.push( { id: newTagName, text: newTagName } );

            Stream2.singleRequest(
                candidate.apiURL("usertags"),
                { tag_names: folders.mapBy("id") }, 'PUT'
            ).then( function(data) {
                Stream2.popupAlert( data.message );
            } );

            candidate.set("usertags", folders);
            this.get("folderNames").set("content", folders.mapBy("id"));

            /* jquery-dropdown v1.0.6 doesn't reliably trigger the "hide"
             * event when its hide() method is called, so we have to manually
             * do it here. See SEAR-2227.
             */
            this.get("dropdown").hide();
            this.get("dropdown").trigger("hide");
        },

        /**
         * Dissociates a folder from a {{#crossLink "Candidate"}}{{/crossLink}}
         *
         * @method delete
         */
        delete: function(folder) {
            var candidate  = this.get("candidate");
            var tagsToKeep = candidate.get("usertags").filter(
                function(e) { return e.id != folder; }
            );
            candidate.set("usertags", tagsToKeep);
        }
    }

} );

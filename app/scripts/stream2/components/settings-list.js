/*
    SettingsListComponent

    given a user hierarchy object ( vivified to ember objects )

    var hierarchy = {
        label: "Company1",
        children: [
            {
                label: "office1",
                children: [
                    {
                        label: "this can go on forever"
                        children: [
                            ...
                        ]
                    },
                    ...
                ]
            },
            ...
        ]
    };

    {{settings-list-component user=hierarchy}}
    produces a cascading list setting type view

*/

Stream2.SettingsListComponent = Ember.Component.extend({
    changeDetected: false,
    didInsertElement: function () {
        // I feel a little bit like this is cheating but we don't want to observe the fluctuation of the view
        // until we know it is the user whom is making the alterations, 0.2s after the setting is loaded, start listening
        Ember.run.later( this, function () {
            this.addObserver( 'user.string_value', this, 'updateChildren' );
            this.addObserver( 'user.children.@each.string_value', this, 'watchChildren' );
            if ( this.get('user.children') ){
                this.watchChildren();
            }
        }, 200);
    },
    actions: {
        toggleChildren: function () {
            this.set('user.showChildren', ! this.get('user.showChildren'));
            this.set('autoExpand', false);
        }
    },
    /*

        Binding a select's value directly to the source value is a bit of nightmare, even worse is allowing other lists to update
        eachother and properly reporting the changes to the server

        updateChildren - propagate my value to my children
        watchChildren - read changes to my children and update my value accordingly
        changeValueSilently - set appropriate flags so that corollary updates are not reported to server

        We have the paradox in that, when we want to update the values of our children, how does the child
        know that this has come about through a direct action of the user or if it is just a display update?
        ONLY update ones value and set appropriate flags if completely necessary

        change value directly:
            _isParentChange = false - UPDATE server

        change to a child value detected:
            set _isChildChange = true, set string_value = <aggregatedValueOfChildren>
                - when the change to my value is detected, it's update is not reported to the server and not
                  re-propagated to the children

        change to parent value propagated down to this view:
            _isParentChange = true - don't UPDATE server, just continue propagation
    */
    updateChildren: function () {
        Ember.run.next( this, function () {

            var selectValue = this.$().children('div.settings-row').children('select').val();
            var string_value = this.get('user.string_value');

            if ( ! this.get('user._isParentChange') && ! this.get('user._isChildChange') ){
                // only send update if the user has a value
                if ( ! Ember.isNone( string_value ) && selectValue !== null && this.get('updateAction') ) {
                    this.get('master').send( this.get('updateAction'), this.get('user') );
                }
            }

            if ( this.get('user.children') && ! this.get('user._isChildChange') ){
                if ( ! Ember.isNone( string_value ) && selectValue !== null ) {
                    this.get('user.children').forEach( function ( child ) {
                        if ( child.get('string_value') !== string_value ){
                            child.set('_isParentChange', true);
                            child.set('string_value', string_value);
                        }
                    });
                }
            }

            this.set('user._isParentChange',false);
            this.set('user._isChildChange',false);
        });
    }, // observes user.string_value

    watchChildren: function () {
        if ( this.get('changeDetected') ){ return; }
        this.set('changeDetected', true);
        Ember.run.next( this, function () { this._updateFromChildren(); } );
    }, // observes user.children.@each.string_value

    _updateFromChildren: function () {
        this.set('changeDetected', false);
        if ( this.get('autoExpand') ) this.set('user.showChildren', false);
        var seen = {};
        this.get('user.children').map( function ( child ) {
            seen[child.get('string_value')] = true;
        });

        if ( Object.keys( seen ).length == 1 && Object.keys( seen )[0] !== "undefined" ){
            return this.changeValueSilently( Object.keys( seen )[0] );
        }

        if ( this.get('autoExpand') ) {
            this.set('user.showChildren', true);
        }

        this.changeValueSilently();
    },
    changeValueSilently: function ( newValue ) {
        if ( this.get('user.string_value') !== newValue ) {
            this.set('user._isChildChange', true);
            this.set('user.string_value', newValue);
        }
    }
});

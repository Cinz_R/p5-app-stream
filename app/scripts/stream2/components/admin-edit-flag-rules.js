/**
 * Stream2.AdminEditFlagRulesComponent
 * Small UI component for editing flag rules
 *
 * @namespace Stream2
 * @class AdminEditFlagRulesComponent
 * @type {component}
 * @extends Ember.Component
 */

Stream2.AdminEditFlagRulesComponent = Ember.Component.extend({

    flag: null,
    showAddReason: false,
    maxReasonLength: 50,
    maxNumberofReasons: 10,
    newReasonValue: '',
    // rejection reasons are stored in groupSettings,
    // however a working copy is stored against the flag
    rejectionReasons: Ember.computed('flag', 'searchUser.settings.groupsettings', function() {
        var flag = this.get('flag');
        var rejectionReasons = flag.get('rejectionReasons');
        if (Ember.isEmpty(rejectionReasons)) {

            var groupSettings = this.searchUser.settings.groupsettings;
            var groupSettingRejection = groupSettings['flag-rejection-' + this.get('flag.id')];

            if (!Ember.isEmpty(groupSettingRejection)) {
                flag.set('rejectionReasons', groupSettingRejection.reasons);
            } else {
                flag.set('rejectionReasons', Ember.A());
            }
            rejectionReasons = flag.get('rejectionReasons');
        }
        return rejectionReasons;
    }),
    hasNewReasonValue: Ember.computed.empty('newReasonValue'),

    actions: {
        addRejectionRule: function() {

            var rejectionReasons = this.get('rejectionReasons');
            // we are only allow 10 reasons, also enforced in the backend
            if (rejectionReasons.length > this.get('maxNumberofReasons')) {
                Stream2.popupError(
                    i18n.__('Oops...'),
                    i18n.__('You can only have reached the maxium number of predefined rejectionreasons.')
                );
                return;
            }

            var reason = this.get('newReasonValue');
            // the inputbox "should" catch this but just in case, also enforced in the backend
            if (reason.length > this.get('maxReasonLength')) {
                Stream2.popupAlert(
                    i18n.__('Oops...'),
                    i18n.__('You rejection reason is too long.')
                );
                return;
            }
            var _this = this;
            var newFlagRejectionReason = Stream2.FlagRejectionReason.create({ label: reason });
            Stream2.request('/api/admin/add-flag-rejection-reason', {
                reason: newFlagRejectionReason,
                flagId: _this.get('flag').get('id')
            }, 'POST').then(
                function(data) {
                    newFlagRejectionReason.set('id', data.reasonId);
                    rejectionReasons.pushObject(newFlagRejectionReason);
                    _this.set('newReasonValue', '');
                },
                function(error) {
                    Stream2.popupError(i18n.__(error.message))
                    return;
                }
            );
            return;
        },
        deleteRejectionRule: function(reason) {
            var _this = this;
            var rejectionReasons = this.get('rejectionReasons');

            Stream2.request('/api/admin/delete-flag-rejection-reason', {
                reasonId: reason.id,
                flagId: _this.get('flag').get('id')
            }, 'POST').then(
                function(data) {
                    rejectionReasons.removeObject(reason);
                    Stream2.popupAlert(
                        i18n.__x('"{rejectionReason}" has been deleted', { rejectionReason: reason.label })
                    )
                },
                function(error) {
                    Stream2.popupError(i18n.__(error.message))
                    return;
                }
            );
            return;
        },
        toggleShowAddReason: function() {
            this.toggleProperty('showAddReason');
        }
    }

});


/**
 * Stream2.FlagRejectionReason
 * A tiny object to hold a Flag Rejection Reason
 *
 * @namespace Stream2
 * @class FlagRejectionReason
 * @type {object}
 * @extends Ember.Object
 */

Stream2.FlagRejectionReason = Ember.Object.extend({
    label: '',
    id: null
});

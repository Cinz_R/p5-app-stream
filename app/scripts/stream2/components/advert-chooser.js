/* AdvertChooserComponent
    A select-2 multi-select box which lets the user select from their
    AdCourier adverts which will filter their adcresponses candidates
*/
Stream2.AdvertChooserComponent = Stream2.CandidateShortlistComponent.extend({

    cheeseSandwich: [],

    // informs the backend where the request came from.
    uiOrigin: 'advertchooser',

    // always group adverts so they show up as optgroups in the select2 input
    groupSandwiches: true,

    // the list is always the same here
    list: Ember.Object.create({
        name: 'adc_shortlist',
        label: 'Adverts'
    }),

    // Do not filter out the Archived jobs as we want to see them.
    filterOutArchived: false,

    _suspendObservations: false,

    /* allFacetOptions
        Options for an advert facet are grouped by status, eg:
        { live => { Options => [{ label => 'Penguins', value => '12345' }], Label => 'Live' } }

        This goes through such a data structure and returns all Options for each group.
    */
    allFacetOptions: function() {
        var options = [];

        if ( Ember.isEmpty(this.get('facet.Options')) ) { return options; }

        for (group in this.get('facet.Options')) {
            this.get('facet.Options')[group].Options.forEach(function(item) {
                options.push(item);
            });
        }
        return options;
    }.property('facet.Options'),

    /* _cheeseObserver
        When a user adds an advert using the advert-chooser, also select the same advert in the
        adcresponses facet
    */
    _cheeseObserver: function () {
        if ( this.get('_suspendObservations') ) { return; }

        // if the boards facets have yet to be loaded then we don't need to fiddle with them,
        // instead we should add to the global criteria directly.
        if ( ! this.get('facet') ){
            return this.set( 'criteria.adcresponses_broadbean_adcadvert_id', this.get('cheeseSandwich').getEach('id') );
        }

        // If the adcadvert_id facet is available, launch a search by adding to it's selected values
        // and calling the change callback
        var map = {};
        this.get('cheeseSandwich').forEach( function ( item ){
            map[item.id] = 1;
        });

        this.set('_suspendObservations', true);
        this.set('facet.SelectedValues',map);
        this.get('facet').propertyDidChange('Options');
        this.get('facet').onChange();
        Ember.run.once(this, function() { this.set('_suspendObservations',false); });
    }.observes('cheeseSandwich'),

    /* _facetObserver
        If the user selects an advert from the facet, update the select-2 to reflect it
    */
    _facetObserver: function () {
        if ( this.get('_suspendObservations') ) { return; }
        var facetOptions = this.get('allFacetOptions');

        if ( ! facetOptions.length ) { return; }
        var facetOptionMap = {};
        facetOptions.forEach( function (item) {
            facetOptionMap[item.value] = item.label;
        });

        // make a list of adverts which are in the select2 and not in the facet
        var select2Items = this.cheeseSandwich.filter( function ( item ) {
            return Ember.isNone(facetOptionMap[item.id]);
        });

        var _this = this;
        this.set('_suspendObservations', true);
        var selected = this.get('criteria.adcresponses_broadbean_adcadvert_id') || [];
        selected.forEach( function ( item ) {
            var facetLabel = facetOptionMap[item];
            if ( facetLabel ) {
                select2Items.pushObject({ id: item, text: facetLabel });
            }
            else {
                _this.get('criteria.adcresponses_broadbean_adcadvert_id').removeObject(item);
            }
        });

        this.set('cheeseSandwich', select2Items);
        Ember.run.once(this, function() { this.set('_suspendObservations',false); });
    }.observes('criteria.adcresponses_broadbean_adcadvert_id', 'facet.Options')
});

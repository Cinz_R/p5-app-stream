/* Stream2.AddTagComponent
A component for fetching available tags for the user to choose

    // will set the controller selected tag to that of this component
    {{add-tag selectedTag=mySelectedTag}}

    // will fire an action when a user chooses a tag
    {{add-tag action="tag-chosen"}}
*/

Stream2.AddTagComponent = Ember.Component.extend({
    classNames: ['tag-suggestions'],
    selectedTag: null,
    actions: {
        setTag: function( tag ) {
            this.set('selectedTag',tag);
        },
        entered: function () {
            // This is what populates the suggestion list when
            // the mouse enters the search input box (see add-tag.hbs)
            Ember.run.debounce( this, this.search, 200 );
        },
        unsetTag: function () {
            this.set('selectedTag',null);
        }
    },
    inputObserve: Ember.observer('input',function () {
        // This is what pupulates the suggestions when you type something
        // in the input box.
        Ember.run.debounce( this, this.search, 200 );
    }),
    input: "",
    suggestions: [],
    hasOptions: Ember.computed( 'suggestions', function () {
        return !! this.get('suggestions').length;
    }),
    search: function () {
        var _this = this;
        Stream2.request('/api/user/tags', { search: this.get('input') }, 'GET').then(
            function ( data ) {
                _this.set('suggestions',data.suggestions);
            }
        );
    }
});

Stream2.AddTagToCandidateComponent = Stream2.AddTagComponent.extend({
    templateName: 'components/add-tag',
    candidate: null,
    didInsertElement: function () {
        var _this = this;
        this.$('input').keypress( function ( e ) {
            if ( e.keyCode === 13 && ! Ember.isNone(_this.get('input')) ){
                _this.send('setTag',Ember.Object.create({ value: _this.get('input') }));
            }
        });
    },
    actions: {
        setTag: function( tag ) {
            if (!tag.value) {
                Stream2.popupError(
                    i18n.__('Oops... Tag name cannot be empty.')
                );
                return;
            }
            this.sendAction('action',this.get('candidate'), tag);
            this.set('input',"");
        }
    }
});

Stream2.CandidateTagComponent = Ember.Component.extend({
    tagName: 'li',
    classNames: ['pill'],
    classNameBindings: ['tagFlavour'],
    tagFlavour: function () {
        var flavour = this.get('tag').get('flavour');
        if ( flavour ){
            return 'tag-flavour-' + flavour;
        }
        return 'tag-flavour-default';
    }.property('tag.flavour'),
    didInsertElement: function () {
        if ( Ember.isNone( this.get('tag.label') ) ){
            this.set('tag.label', this.get('tag.value'));
        }
        this.tagAdded();
    },
    glow: function () {
        var _this = this;

        var $toGlow = this.$();

        var originalColor = $toGlow.css('backgroundColor');

        // The first glow will also expand the tag box
        $toGlow.animate(
            {
                backgroundColor: "#5bb75b"
            },
            {
                duration: 400,
                start: function () {
                    if ( _this.get('parentView') && _this.get('parentView').mouseEnter )
                        _this.get('parentView').mouseEnter();
                }
            }
        )

        // glow again
        .animate({ backgroundColor: originalColor }, 400)
        .animate({ backgroundColor: "#5bb75b" }, 400)

        // final transition back to original colour, also closes tag box
        .animate(
            {
                backgroundColor: originalColor
            },
            {
                duration: 400,
                done: function () {
                    if ( _this.get('parentView') && _this.get('parentView').mouseLeave )
                        _this.get('parentView').mouseLeave();

                    $toGlow.css('background-color' , "");
                }
            }
        );
    },
    tagAdded: function () {
        if ( this.get('tag').get('new') ){

            // This tag view could be on the page twice, once in the results view and
            // once in the profile view which means we have 1 tag with two observers. 
            // If we observe a tag and then set its status back
            // immediately, then only one of the instances are going to glow
            Ember.run.next( this, function () { this.get('tag').set('new',false); } );
            this.glow();
        }
    }.observes( 'tag.new' ),
    actions: {
        removeTag: function () {
            this.get('candidate').removeTag( this.get('tag') );
        }
    }
});

Stream2.CandidateTagListComponent = Ember.Component.extend({
    tagName: 'div',
    classNames: ['candidate-tags','closed'],

    // Dynamically attach mouseover effects. Apply them in all cases except
    // when the user's on the Responses channel and they lack TalentSearch
    didInsertElement: function() {
        var skipMouseovers = this.searchUser.get("restrictUI");

        if ( !skipMouseovers ) {
            this.mouseEnter = function() {
                this.$().removeClass('closed');
                this.$().css({
                    position: "absolute",
                    top:      this.$().position().top + 3,
                    left:     this.$().position().left,
                    width:    this.$().width()
                });
            };
            this.mouseLeave = function() {
                this.$().addClass('closed');
                this.$().css({
                    position: "inherit",
                    top:      "auto",
                    left:     "auto",
                    width:    "100%"
                });
            };
        }
    }
});

Stream2.TagSuggestionComponent = Ember.Component.extend({
    classNames: ["pill clickable"],
    classNameBindings: ['tagFlavour'],
    click: function () { this.sendAction('action',this.get('tag')); },
    tagFlavour: function () {
        var flavour = this.get('tag.flavour');
        if ( flavour ){
            return 'tag-flavour-' + flavour;
        }
        return 'tag-flavour-default';
    }.property('controller.flavour')
});

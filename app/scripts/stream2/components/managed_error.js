/* ManagedErrorComponent
    For displaying nicer versions of the errors returned to us by the Engine
*/
Stream2.ManagedErrorComponent = Ember.Component.extend({

    error: null,

    /* errorType
        if we have a special view for an error we can return it's type here
        the error type needs to be detected through string matching, we already
        know it is a "managed error" so the number of possibilities are limit
    */
    errorType: function () {

        var error = this.get('error');
        if ( Ember.isEmpty(error) ) { return; }

        var properties = error.properties || {};

        /* cinzia wants some special display if there is a keywords error SEAR-1555 */

        // We don't have views for all managed errors individually, only return
        // the ones we have.
        var type = properties.type;
        if ( type === 'keywordstoolong' ) {
            return 'error/keywordstoolong';
        }
        else if ( type === 'loginerror' ) {
            return 'error/loginerror';
        }
        else if ( type === 'quota' ) {
            return 'error/quota';
        }

    }.property('error'),

    _keywordsGoodString: function () {
        var keywords = this.get('criteria').oneValue('keywords') || '';
        return keywords.substr( 0, this.get('error.properties.limit') );
    }.property('error'),
    _keywordsBadString: function () {
        var keywords = this.get('criteria').oneValue('keywords') || '';
        return keywords.substr( this.get('error.properties.limit') );
    }.property('error')
});

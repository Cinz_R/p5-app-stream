/**
 * This component displays (and allows the download of) a Stream2.Attachment
 * @class AttachmentBoxComponent
 * @namespace Stream2
 */
Stream2.AttachmentBoxComponent = Ember.Component.extend({
    /**
     * The current attachment
     * @property attachemnt
     * @type Stream.Attachment
     * @default null
     */
    attachment: null,

    actions: {
        /**
         * Download the current attachment. Prompts the user to save the file.
         * @method actions.downloadAttachment
         */
        downloadAttachment: function () {
            Stream2.fileDownload( this.get('attachment').get('downloadURL') )
                .then (
                    function () {},
                    function ( error ) {
                        Stream2.popupError( error );
                    }
                );
            Stream2.pushEvent('candidate','download attachment');
        }
    }
});

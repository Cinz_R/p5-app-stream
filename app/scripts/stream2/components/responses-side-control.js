Stream2.ResponsesSideControlComponent = Stream2.SearchSideControlComponent.extend({
    actions: {
        /* updates server side session to say they have exited responses mode without
            having to restart the whole app
        */
        exitResponses: function () {
            this.get('searchUser').set('search_mode','search');
            Stream2.request('/exit-responses', {}, 'GET');
        }
    }
});

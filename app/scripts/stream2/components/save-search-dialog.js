/*

The dialog that shows up when clicking 'Save Search'

*/

Stream2.SaveSearchDialogComponent = Ember.Component.extend({
    criteria: null,
    savedSearches: [],
    flavour: "search",
    name: '',

    // returns true if the user already has a saved search named newName
    doesNameAlreadyExist: function( newName ) {
        var flavour = this.get('flavour');
        return this.get('savedSearches').some( function(elem) {
            return ( elem.flavour === flavour && elem.name === newName );
        } );
    },

    actions:{
        clickOk: function(){
            // No keywords specified. Do you *really* want to save?
            if ( !this.get('criteria').keywords ) {
                var userWantsToSave = window.confirm( i18n.__("No keywords in your search. Are you sure you want to save it?") );
                if (!userWantsToSave) {
                    Stream2.popupAlert( i18n.__('Cancelled Save Search') );
                    this.send('clickCancel');
                    return;
                }
            }

            var name = this.get('name');
            if ( this.doesNameAlreadyExist(name) ) {
                window.alert( i18n.__('A search named "' + name + '" already exists. Please enter a new name.') );
                this.$('input').select();
                return;
            }

            this.save( name );
            $('#save-search-link').dropdown('hide');
            this.set('name', '');
        },
        clickCancel: function(){
            $('#save-search-link').dropdown('hide');
            this.set('name', '');
        }
    },

    // This is the 'save_search' save search action
    save: function (name) {
        var criteria = this.get('criteria').serialise();
        var boardModel = this.get('selectedBoard');
        var savedSearches = this.get('savedSearches');

        if (boardModel.hasFacets){
            var facets = boardModel.serialiseFacets();
            criteria = $.extend(criteria, facets);
        }

        Stream2.request(
            "/api/savedsearches",
            {
                criteria: criteria,
                name: name,
                flavour: this.get('flavour')
            },
            'POST'
        ).then(
            function(data) {
                if ( data.warnings ) {
                    if( savedSearches ){
                        savedSearches.findBy('id',data.search.id).setProperties( data.search );
                    }
                    Stream2.popupAlert(i18n.__("Notice:"), data.warnings);
                }
                else {
                    if (savedSearches) {
                        savedSearches.unshiftObject(Stream2.SavedSearch.create(data.search));
                    }
                    Stream2.popupAlert(i18n.__("Search saved"), i18n.__('Your search have been saved successfully'));
                }
                $("#saved_searches_link").fadeIn(100).fadeOut(100).fadeIn(100).fadeOut(100).fadeIn(100);
            },
            function(data) {
                alert("Sorry your search cannot be saved right now. Please try again later.");
            }
        );

        Stream2.pushEvent('search','save');
    }

});

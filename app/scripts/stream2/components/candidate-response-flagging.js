/**
 * An input element which displays options as a list of flag icons.
 * Can instantiate as
 *     {{candidate-response-flagging candidate=myCandidate flags=myFlags rankField='response_rank'}}
 * Flags should be a list of objects with
 *     { value, colour|style },
 * Default fetches flags from searchUser
 * @class CandidateResponseFlaggingComponent
 * @namespace Stream2
 */
Stream2.CandidateResponseFlaggingComponent = Ember.Component.extend({
    tagName: 'div',
    classNames: ['ranking-flags'],
    attributeBindings: ['title'],
    title: i18n.__('Candidate Rank'),

    candidate: null,
    rankField: 'response_rank',

    /**
     * Filtered for our needs, we don't want any inactive
     * flags... unless it is the current rank of the candidate
     * in question
     * @property flags
     * @type Array
     */
    flags: Ember.computed('searchUser.flags', function () {
        var rank = this.get('candidate').get(this.get('rankField'));
        return this.searchUser.get('flags').filter( function ( flag ) {
            if ( ! flag.active && flag.value == rank ) {
                return true;
            }
            return flag.active;
        });
    }),

    flagType: function () {
        var _this = this;
        return this.get('flags').map( function ( item ) {
            item.isSelected = !! ( _this.get('candidate').get(_this.get('rankField')) == item.value );
            return Ember.Object.create( item );
        });
    }.property('flags'),

    candidateIsFlagged: function() {
        var value = this.get('candidate').get(this.get('rankField'));

        if (!value) { return false; }

        return this.get('flags').isAny('value', value);
    }.property('candidate', 'flags'),

    didInsertElement: function () {
        var _this = this;
        this.addObserver( 'candidate.' + this.get('rankField'), function () {
            _this._changeFlag(_this.get('candidate.' + _this.get('rankField')));
        });
        Ember.run.next(this, function () {
            _this._changeFlag(_this.get('candidate.' + _this.get('rankField')));
        });
    },
    // rejection reason specific code
    showRejectionReasons: false,
    selectedRejectionReason: null,
    hasNoSelectedRejectionReason: Ember.computed('selectedRejectionReason', function() {
        return Ember.isEmpty(this.get('selectedRejectionReason'));
    }),
    rejectionReasons: Ember.computed('_newRank_rejection', function() {

        var groupSettings = this.searchUser.settings.groupsettings;
        var groupSettingRejection = groupSettings['flag-rejection-' + this.get('_newRank_rejection')];

        if (!Ember.isEmpty(groupSettingRejection)) {
            return groupSettingRejection.reasons;
        }

        return [];
    }),
    // this builds a tiny object that that contains the flag rank, and the rejection reason.
    _buildFlagUpdatePayload: function (newRank, rejectionReason) {
            var newValues = {};
            newValues[this.get('rankField')] = newRank;
            newValues['broadbean_adcresponse_rank_reason'] = rejectionReason;
            return newValues;
    },

    actions: {
        hover: function ( tempRank ) {
            this._changeFlag( tempRank );
        },
        leave: function () {
            var flagRank = this.get('candidate').get(this.get('rankField'));
            if (!Ember.isEmpty(this.get('_newRank_rejection'))) {
                flagRank = this.get('_newRank_rejection');
            }
            this._changeFlag( flagRank );
        },
        flagClick: function ( newRank ) {
            this._beforeFlagChange();
            this.setProperties({ showRejection: false });
            // if the ranks are 1 or 3, we may need to show another dialog to allow
            // for a user to select their favourite rejection reason.
            if ( this._flagHasRejections( newRank ) ) {
                return;
            }

            this.get('candidate').updateProperties(
                this._buildFlagUpdatePayload(
                    newRank
                )
            );
        },
        hideRejectionReasons: function () {
            this._changeFlag(this.get('candidate').get(this.get('rankField')));
            this.setProperties({ showRejection: false, _newRank_rejection: null });
        },
        selectReason: function () {
            var candidate = this.get('candidate');
            candidate.updateProperties(
                this._buildFlagUpdatePayload(
                    this.get('_newRank_rejection'),
                    this.get('selectedRejectionReason.text')
                )
            );
            this.setProperties({ selectedRejectionReason: null, showRejection: false, _newRank_rejection: null });
        },
        // although the data is already in the searchUser, this allows for 
        // select-2 to be dynamic
        getRejectionReasons: function (a,deferred) {
            var _this = this;
            deferred.resolve(
                _this.get('rejectionReasons').map(function(item){
                    return {
                        id: item.id,
                        text: item.label
                    }
                })
            )
        },
    },
    _flagHasRejections: function ( newRank ) {

        this.setProperties({ showRejection: false });
        // if the ranks are 1 or 3, we may need to show another dialog to allow
        // for a user to select their favourite rejection reason.
        if ( newRank == 1 || newRank == 3 ) {
            // clear out the selectedRejectionReason and set the new_rank
            this.setProperties({selectedRejectionReason: null, _newRank_rejection: newRank});
            if ( this.get('rejectionReasons').length > 0) {
                this.set('showRejection', true);
                return true;
            }
        };
        return false;
    },

    /**
     * Get's called before updating the candidat's
     * rank, will refresh flag list and remove any flags which
     * appeared through the virtue of being assigned
     * @method _beforeFlagChange
     */
    _beforeFlagChange: function () {
        var currentRank = this.get('candidate').get(this.get('rankField'));
        if ( ! this.get('flagType').filterBy('value', currentRank).get('firstObject.active') ){
            Ember.run.next( this, function () {
                this.notifyPropertyChange('searchUser.flags');
            });
        }
    },

    _changeFlag: function ( newVal ) {
        var flagType = this.get('flagType');
        if ( ! flagType ){ return; }

        if ( ! newVal || newVal == 7 ){ // if candidate is unranked... highlight all flags
            return flagType.setEach( 'isSelected', true );
        }

        flagType.setEach( 'isSelected', false );
        var selected = flagType.findBy('value',newVal);
        if ( selected ) {
            selected.set('isSelected',true);
        }
    }
});

/**
* Stream2.CandidateResponseFlaggingDropdownComponent
* adcresponse flag selection displayed as a select-2 dropdown
*
* @class CandidateResponseFlaggingDropdownComponent
* @type {component}
* @example
* //such name
* {{candidate-response-flagging-dropdown candidate=c rankField='broadbean_adcresponse_rank'}}
*/
Stream2.CandidateResponseFlaggingDropdownComponent = Stream2.CandidateResponseFlaggingComponent.extend({

    selectedDropdownFlag: null,
    flagsAsDropdown: Ember.computed('flags', function (){
        return this.get('flags').map( function ( flag ) {
            return {
                id: flag.value,
                style: flag.style,
                text: flag.description
            };
        });
    }),

    didInsertElement: function () {
        var _this = this;
        var selectedFlag = this.get('flagType').filterBy('isSelected').get('firstObject');
        if ( selectedFlag ){
            this.set(
                'selectedDropdownFlag',
                Ember.Object.create({
                    id: selectedFlag.get('value'),
                    style: selectedFlag.get('style'),
                    text: selectedFlag.get('description')
                })
            );
        }

        Ember.run.next( this, function () {
            var _this = this;
            this.addObserver( 'selectedDropdownFlag', function () {
                _this._beforeFlagChange();
                // check to see if the flag has rejection reasons, and only
                // only pdate the rank if one is set.
                var newRank = _this.get('selectedDropdownFlag.id');
                if (_this._flagHasRejections( newRank ) ) {
                    return;
                }
                this.get('candidate').updateProperties(
                    this._buildFlagUpdatePayload(
                        newRank
                    )
                );
            });
        });
    }

});

Stream2.CandidateResponseFlagComponent = Ember.Component.extend({
    tagName: 'i',
    classNames: ['icon-flag-circled'],//,'clickable'],
    attributeBindings: ['style','title'],

    style: function () {
        return this.get('isSelected') ? this.get('styleDesignation') : 'color: grey';
    }.property('isSelected'),

    title: function () {
        return this.get('description');
    }.property('description'),

    mouseEnter: function () {
        Ember.run.next( this, function () {
            this.sendAction('hover',this.get('numericDesignation'));
        });
    },
    mouseLeave: function () {
        Ember.run.next( this, function () {
            this.sendAction('leave');
        });
    },
    click: function () {
        Ember.run.next( this, function () {
            this.sendAction('flagClick',this.get('numericDesignation'));
        });
    }
});

/*
    SettingsQuotaComponent

    given a user hierarchy object ( vivified to ember objects )

    var hierarchy = {
        label: "Company1",
        children: [
            {
                label: "office1",
                children: [
                    {
                        label: "this can go on forever"
                        children: [
                            ...
                        ]
                    },
                    ...
                ]
            },
            ...
        ]
    };

    {{settings-quota user=hierarchy}}
    produces a cascading list setting type view of text boxes, the quota type does not have
    any interaction between other levels however.
*/

Stream2.SettingsQuotaComponent = Ember.Component.extend({
    placeHolder: "",
    padding: 0,
    addPadding: Ember.computed('padding', function() { return this.get('padding') + 30; }),
    actions: {
        toggleChildren: function () {
            this.toggleProperty('user.showChildren');
            this.set('autoExpand', false);
        }
    }
});

/*

  Renders a Stream2.Pager in a nice (and nicer) way.

*/

Stream2.PagerRenderComponent = Ember.Component.extend({
    route: null,
    pager: null
});

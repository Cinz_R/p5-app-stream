Stream2.BlocklyProgramComponent = Ember.Component.extend({
    _blocklyWorkspace: null,
    
    program: null,
    
    didInsertElement: function() {
        var _this = this;

        // Set the email template drop down according to
        // the current user
        Stream2.request('/api/admin/email-templates', {}, 'GET')
         .then(
            function ( templatesData ) {
                var flagsData     = _this.searchUser.get('flags');
                // The email templates
                Blockly.Blocks['s2_send_email_template'].s2_options = templatesData.emailtemplates.map(function(template){
                    return [ template.name + '',
                             template.id  + '' ];
                });
                Blockly.Blocks['s2_send_email_template'].s2_options.unshift([ "- Choose template -" , "NONE"]);

                // The flags
                Blockly.Blocks['s2_response_is_being_flagged'].s2_options = flagsData.map(function(flag){
                    return [ flag.description + ( flag.active ? '' : ' (inactive)' ), flag.value + '' ];
                });
                Blockly.Blocks['s2_response_is_being_flagged'].s2_options.unshift([ "- Choose flag -" , "NONE" ]);
            }
        ).then(
            function(){
                // install blockly in this container:
                var workspace = Blockly.inject('the-blockly-container',
                                               {toolbox: document.getElementById('toolbox'),
                                               });
                _this._blocklyWorkspace = workspace;
                
                var blocklyXML = _this.get('program').get('blockly_xml');
                if( ! Ember.isNone( blocklyXML ) ){
                    // Inject the existing XML for edition.
                    Blockly.Xml.domToWorkspace( Blockly.Xml.textToDom( blocklyXML ),
                                                workspace );
                };

                // Listen to changes and reflect in the program object
                // as blockly_xml and lisp_source.
                workspace.addChangeListener(function(event){
                    if( event.type == Blockly.Events.UI ){
                        // Do not handle pure UI events.
                        return;
                    }
                    var workspace = _this._blocklyWorkspace;
                    var blocklyXML = Blockly.Xml.domToPrettyText( Blockly.Xml.workspaceToDom( workspace ) );
                    var lispPerl = Blockly.LispPerl.workspaceToCode( workspace );
                    _this.get('program').set('blockly_xml', blocklyXML );
                    _this.get('program').set('lisp_source', lispPerl );
                });
            },
            function ( error ) { return alert(error); }
        );
        
    },
    actions: {
        previewCode: function(){
            alert( Blockly.LispPerl.workspaceToCode( this._blocklyWorkspace ) );
        }
    }
});

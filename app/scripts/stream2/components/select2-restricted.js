/* Displays a UI-restricted view of the select-2 component. Such a view is
 * typically reserved for users who lack subscriptions to certain key boards
 * and appears "greyed out" and with a "not-allowed" cursor when moused over.
 */
Stream2.Select2RestrictedComponent = Stream2.Select2Component.extend({
    classNameBindings: ['inputSize', 'enabled::restrictUI-cursor'],
    enabled: Ember.computed.not('searchUser.restrictUI')
});

/* Stream2.MainCriteriaComponent

Contains view methods used within the main criteria portion
of the Search interface

*/
Stream2.MainCriteriaComponent = Ember.Component.extend({
    tagName: 'div',
    classNames: 'hero',

    selectedAdvertsFacet: function () {
        if ( this.get('board') ){
            return this.get('board').getFacetByName('broadbean_adcadvert_id');
        }
    }.property('board.facets'),

    actions: {
        showMoreOptions: function () {
            Stream2.pushEvent('search','showMoreOptions');
            this.set('hideMoreOptions', false);
        },
        showLessOptions: function () {
            Stream2.pushEvent('search','showLessOptions');
            this.set('hideMoreOptions', true);
        },
        submit: function () {
            this.sendAction('submit');
        },
        suggestLocation: function(query, deferred){
            var term = query.term;

            if( term.length < 3 ){
                deferred.resolve([]);
                return;
            }

            if ( this.isUKPostcode( term ) ){
                this.get('criteria').set('location_id_postcode', term);
            }
            else {
                this.get('criteria').set('location_id_postcode', null);
            }

            Stream2.singleRequest(
                '/api/locations/suggest',
                { search: term },
                'GET'
            ).then(
                function ( data ) {
                    // [ { id: term.length , text: term + 'BLA'} ]);
                    deferred.resolve(data.suggestions);
                }
            );
        },
        watchdogSave: function () {
            this.sendAction('watchdogSave');
        }
    },
    isUKPostcode: function ( postcode ) {
        postcode = postcode.replace(/\s/g, "");
        var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
        return regex.test(postcode);
    }
});

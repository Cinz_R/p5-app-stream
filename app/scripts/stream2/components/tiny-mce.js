/*

Usage:

 {{tiny-mce valueBinding=myControllerValue}}

*/

Stream2.TinyMceComponent = Ember.Component.extend( ErrorMixin, {
    tagName: 'textarea',

    uploadURL: '/api/tinymce/upload',
    height: 600,

    tinyMCELang: function () {
        var serverAssignedLangCode = i18n.lng();
        return getMatchingLangCode( serverAssignedLangCode, 'tinyMCE' );
    },
    didInsertElement: function () {
        var _this = this;
        this.$().val( _this.get('value') );

        if ( typeof( tinymce ) === "undefined" ) {
            return this.set('error','Tiny MCE is not available');
        }

        var tinyMCELanguage = this.tinyMCELang();
        tinymce.init({
            language                : tinyMCELanguage,
            entity_encoding         : "raw",
            selector                : 'textarea#' + this.$().attr('id'),
            height                  : this.get('height'),
            menubar                 : false,
            statusbar               : false,
            plugins                 : 'paste,table,searchreplace,nonbreaking,spellchecker,code,link,hr,textcolor,placeholders,image,imagechooser',
            paste_webkit_styles     : 'all', // Allow copy/pasting in webkit.
            toolbar                 : [ 'undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | formatselect fontsizeselect | table',
                                        'cut copy paste cleanup | replace | bullist numlist | outdent indent | link unlink | forecolor hr removeformat spellchecker | placeholders | image | code' ],
            toolbar_items_size      : "small",
            keep_styles             : false, // Clear styles on enter
            extended_valid_elements : "iframe[class=myclass|src|border:0|alt|title|width|height]", // Not sure we want the iframe or not.
            invalid_elements        : "script",
            theme_url               : '/static/tinymce/theme.min.js',
            skin_url                : '/static/tinymce',

            spellchecker_rpc_url    : 'https://' + window.location.host + '/api/tinymce/spellcheck',
            spellchecker_language   : i18n.lng(),
            spellchecker_languages  : "English=en,English (US)=en_US,French=fr,German=de,Dutch=nl,Swedish=sv",

            setup                   : function( editor ){
                _this.set('editor', editor);
                editor.on( 'keyup change', function(event){
                    // console.log( editor.getContent() );
                    _this.set('value' , editor.getContent());
                });
            }
        });
    },
    valueChanged: function () {
        if ( this.get('updateContent') ){
            var _this = this;
            var ed = this.get('editor');

            var updateFunc = function () {
                ed.setContent( _this.get('value') );
                _this.set('updateContent', false);
            };

            // if the editor has yet to initialise it may never get the value
            if ( ed.initialized ){
                updateFunc();
            }
            else {
                ed.on('init', updateFunc);
            }
        }
    }.observes('value')
});

/*
 * Stream2.AdminEmailtemplateItemComponent
 * Small UI component that holds a email template item
*/
Stream2.AdminEmailtemplateItemComponent = Ember.Component.extend({
    tagName: 'li',
    template: null,

    hasResponseReceivedRule: Ember.computed.notEmpty('responseReceivedRule'),
    responseReceivedRule: Ember.computed.filterBy(
        'template.rules_data', 'role_name', 'ResponseReceived'),
    hasResponseFlaggedRules: Ember.computed.notEmpty('responseFlaggedRules'),
    responseFlaggedRules: Ember.computed.filterBy(
        'template.rules_data', 'role_name', 'ResponseFlagged')
});

/* a textarea element to which an instance of the jQuery autocomplete
 * component (http://api.jqueryui.com/autocomplete) is attached. As the user
 * types in the textarea, a list of suggestions will appear below it.
 */
Stream2.AutoCompleteComponent = Ember.TextArea.extend({
    tagName:           "textarea",
    attributeBindings: ["placeholder", "value"],
    placeholder:       "",

    // REQUIRED: name/URL of a data source to load suggestions from. See
    // getDataSource() below
    url: null,
    
    // minimum number of characters that must exist in the input element
    // before conducting a search for suggestions. Underlying jQuery component
    // defaults this to 1.
    minLength: null,

    // milliseconds to wait after last key press before loading suggestions.
    // Underlying jQuery component defaults this to 300.
    delay: null,

    // whether to auto focus on the 1st suggestion as soon as suggestions are
    // loaded. Underlying jQuery components defaults this to false.
    autoFocus: null,

    // instantiate the jQuery component and attach it to *this* textarea
    didInsertElement: function() {
        var dataSource = this.getDataSource( this.get('url') );
        if ( Ember.isEmpty(dataSource) ) {
            throw {
                name:    "UnknownDataSource",
                message: "Unknown or unspecified data source. Perhaps you " +
                         "forgot to pass the 'url' param."
            };
        }
        var options = { source: dataSource };
        
        var outerThis = this;
        [ "minLength", "delay", "autoFocus" ].forEach( function(propName) {
            var propValue = outerThis.get(propName);
            if ( propValue != null ) {
                options[propName] = propValue;
            }
        });

        jQuery(this.element).autocomplete( options );
    },

    // returns a function that knows how to load suggestions from url
    getDataSource: function( url ) {

        // url should match one of the keys in the map below
        var urlHandlerMap = {

            // "colors", a pseudo URL representing an in-memory list of colors
            "colors": function( request, responseFunc ) {
                responseFunc( ["blue", "brown", "green", "grey", "red"] );
            },

            "/api/user/keyword-suggest": function(request, responseFunc) {
                var query       = { search: request.term };
                var queryString = "q=" + JSON.stringify(query)
                                + "&ts=" + new Date().getTime();
                jQuery.getJSON( url + '?' + queryString, function(data) {
                    responseFunc( data.suggestions );
                } );
            }
        };

        return urlHandlerMap[ url ];
    }

});
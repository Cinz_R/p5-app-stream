Stream2.AdcResponseFlagComponent = Ember.Component.extend({
    tagName: 'i',
    classNames: ['icon-flag-circled'],
    attributeBindings: ['flagStyle:style'],
    flagStyle: function () {
        return 'color: ' + this.get('flag.colour_hex_code');
    }.property('flag.colour_hex_code')
});

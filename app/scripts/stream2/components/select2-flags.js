/**
* A select-2 dropdown which also displays the colour of the flag
*
* @class Select2FlagsComponent
* @type {component}
* @example
* // Render a flag select-2
* {{select-2-flags content=myFlags}}
*/

Stream2.Select2FlagsComponent = Stream2.Select2Component.extend({
    searchEnabled: false,

    didInsertElement: function () {
        var options = this._buildOptions();

        options.formatSelection = options.formatResult = function( state ){
            if (!state) { return; }
            var flagEl = $('<i />',{ "class": "icon-flag-circled", "style": state.style});
            return $('<span />').append(flagEl.prop('outerHTML')).prop('outerHTML') + " " + state.text;
        };
        options.dropdownCssClass = 'select2-flags-dropdown';

        this.setupSelect2( options );
    }
});

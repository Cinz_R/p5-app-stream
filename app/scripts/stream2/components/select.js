
/* http://emberjs.com/deprecations/v1.x/#toc_ember-select */
Stream2.SelectComponent = Ember.Component.extend({
    content: null,
    selectedValue: null,

    didInitAttrs: function (attrs) {
        this._super.apply(this,arguments);
        var content = this.get('content');

        if (!content) {
            this.set('content', []);
        }
    },

    actions: {
        change: function() {
            var changeAction = this.get('action');
            var selectedEl = this.$('select')[0];
            var selectedIndex = selectedEl.selectedIndex;
            var content = this.get('content');
            var selectedValue = content[selectedIndex];

            this.set('selectedValue', selectedValue);
            changeAction( Ember.get(selectedValue,this.get('optionValuePath')) );
        }
    },

    optionGroups: Ember.computed( 'content', function () {
        var _this = this;
        var groups = {};
        content.forEach( function( item ) {
            var group = Ember.get(item, _this.get('optionGroupPath'));

            if ( typeof( groups[group] ) == "undefined" ){
                groups[group] = {
                    label: group,
                    options: []
                }
            }

            groups[group].options.push(item);
        });
        return Object.values(groups);
    })
});
        
/*

The dialog that shows up when clicking 'Share Search'

*/



Stream2.ShareSearchDialogComponent = Ember.Component.extend({
    recipients:     [],
    messageContent: '',
    formIsShown:    false,

    // returns true if form's state is invalid to submit; false if valid
    formInvalid: function() {
        var recipients = this.get('recipients');
        var isValid    = recipients && recipients.length > 0;
        return !isValid;
    }.property('formIsShown' , 'recipients'),

    actions:{
        doShare: function() {
            var _this = this;

            var recipients = this.get('recipients');
            var message    = this.get('messageContent');

            // share current URL, minus any page/n params at the end
            searchURL      = location.href.replace( /\/page(?:\/\d*)?$/, '' );

            var recipients_emails = [];
            for( var i = 0 ; i < recipients.length ; i++ ) {
                recipients_emails.push( recipients[i].id );
            }
            Stream2.popupAlert(i18n.__("Forwarding..."), i18n.__('The recipients will get an email shortly.') );
            Stream2.singleRequest( '/api/user/share', 
                { 
                    recipients: recipients_emails,
                    message:    message,
                    searchURL:  searchURL
                }, 
                'POST'
            ).then( function(data) {
                Stream2.popupAlert( data.message );
            } );
            Stream2.pushEvent('search', 'share search');

            this.send('clickCancel');   // clear state
        },
        showForm: function() {
            this.set('formIsShown', true);
        },
        queryRecipients: function(query, deferred){
            var term = query.term;
            Stream2.singleRequest( '/api/user/colleagues', { search: term } , 'GET' ).then(
                function ( data ) {
                    deferred.resolve(data.suggestions);
                    // [ { id: term.length , text: term + 'BLA'} ]);
                });
        },
        clickCancel: function(){
            this.$('#share-search-link').dropdown('hide');
            this.set('recipients', []);
            this.set('messageContent', '');
            this.set('formIsShown', false);
        }
    }
});

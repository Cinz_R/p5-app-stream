/**
 * Component for facets
 * @class FacetBaseComponent
 * @namespace Stream2
 * @extends Ember.Component
 */

// TODO: this 'Facet' object requires a re-write
// At the moment it is a mixture of view and model and would probably be better off split
// into a component

Stream2.FacetBaseComponent = Ember.Component.extend({

    classNameBindings: ['_displayClass', ':facets','isRestricted:restrictUI:'],
    attributeBindings: ['id', 'title'],
    id: Ember.computed.alias('facet.Id'),
    title: null,
    textFilter: null, // used to filter list/tag options by user input for display

    /**
     * Additional classname determined by this.DisplayStyle
     * @property _displayClass
     * @type string
     * @default ''
     */
    _displayClass: function() {
        var styleKey = this.get('facet').DisplayStyle;
        var validKeys = [
            'major_filter',
        ];
        return validKeys.indexOf(styleKey) !== -1 ? styleKey : '';
    }.property(),

    /**
     * True if the facet is currently expanded.
     * @property showing
     * @type boolean
     */
    showing: function () {
        var facet = this.get('facet');

        if(facet.get('StartShown')) {
            return true;
        }

        /* advert facet should be auto-open initially */
        if ( facet.get('isAdvert') ){
            return true;
        }

        if ( facet.get('_isMultiList') || facet.get('isTag') || facet.get('isGroupedMultiList') ){
            if ( Ember.isEmpty( facet.get('Options') ) ) return false;
            return ! Ember.isEmpty( $.map( facet.get('SelectedValues'), function ( k ) { return k; } ) );
        }

        if ( facet.get('_isList') ){
            if ( Ember.isEmpty( facet.get('Options') ) ) return false;
            return ( this.get('value') !== facet.get('Options')[0][1] );
        }
        return !! ( this.get('value') );
    }.property('facet.SelectedValues'),

    /**
     * The initial style of the component. jQuery will controll the style forthwith
     * @property style
     * @type string
     */
    style: function () {
        return this.get('showing') ? 'display: block' : 'display: none';
    }.property(),

    // A bit of a tweak for some integration quirks.
    // The efinancial board has "0" as the default for some of the fields
    // 0 is not a validOption
    //
    // Failure in this function to return a correct indication
    // will lead to nightmares like in https://www.pivotaltracker.com/story/show/83414226
    //
    validOption: function ( value ) {

        var facet = this.get('facet');
        // The facet holds the 'type indicators' (_isList, _isMultiList)..
        if ( facet.get('_isList') || facet.get('_isMultiList') ){
            var found = this.get('facet.Options').filter(function ( item ) {
                return ( item[1] === value );
            });

            return !! found.length;
        }

        return true;
    },
    value     : function () {
        var v = this.get('facet.Value');
        var d = this.get('facet.Default');

        if ( ! Ember.isNone( v ) && this.validOption( v ) ) return v;
        else if ( ! Ember.isNone( d ) && this.validOption( d ) ) return d;
        else if ( this.get('facet.Options').length ) return this.get('facet.Options')[0][1];

        return "";
    }.property('facet.Value','facet.Default'),

    // returns true if this facet is restricted
    isRestricted: function() {
        if ( !this.get("searchUser.restrictUI") ) {
            return false;
        }
        var name = this.get("facet").get("Name");
        var restrictedFacetNames = [
            "broadbean_employer_org_job_title",
            "broadbean_employer_org_name",
            "broadbean_employer_years",
            "broadbean_adcboard_id",
            "country",
            "skill_list",
            "broadbean_adcresponse_tags"
        ];

        // alas Array.prototype.includes() isn't available yet
        var matchedFacets = restrictedFacetNames.filter( function(elem) {
            return elem == name;
        } );
        return matchedFacets.length > 0;
    }.property('searchUser.restrictUI'),

    didInsertElement: function() {
        this.set('prevValue',this.get('value'));
        if ( this.get('isRestricted') ){
            this.set('title', i18n.__('Contact your account manager to upgrade!'));
        }
    },

    value_changed : function () { //this whole thing stops the onchange from being called while the element is being populated...
        if ( this.get('value') !== this.get('prevValue') ) {
            return true;
        }
        return false;
    },
    valueChange: function () {
        var facet = this.get('facet');
        if ( ( facet.get('_isList') || facet.get('isGroupedList') ) && this.value_changed() ) {
            facet.set('Value',this.get('value'));
            if ( facet.onChange() ) {
                this.set('prevValue', this.get('value') );
            }
            else {
                this.set('value',this.get('prevValue'));
                facet.set('Value',this.get('prevValue'));
            }
        }
    }.observes('value'),

    // view methods
    getOptions: function() {
        var sorted = [];
        var _this = this;
        var facet = this.get('facet');

        var callback = function (){
            facet.get('SelectedValues')[this.get('value')] = this.get('isChecked');
            if ( ! facet.onChange() ) {
                facet.get('SelectedValues')[this.get('value')] = ! this.get('isChecked');
                this.revert();
            }
        };

        if ( facet.get('Options') ){
            facet.get('Options').forEach( function (item) {

                var label = item[0] || '-';
                var value = item[1];
                var count = item[2] || null;

                var listObj = null;
                if ( facet.get('_isMultiList') ){
                    var isChecked = ( facet.get('SelectedValues')[value] ) ? true : false;
                    listObj = Stream2.FacetCheck.create({
                        _label      : label,
                        value       : value,
                        isChecked   : isChecked,
                        onChange    : callback,
                        count       : count
                    });
                }
                else {
                    listObj = Stream2.ListItem.create({
                        _label      : label,
                        value       : value,
                        count       : count
                    });
                }
                sorted.pushObject(listObj);
            });
        }
        return sorted;
    }.property('facet.Options'),

    // if the facet is a "groupedmultilist" we need to compile the options as such so handlebars can render
    // them appropriately
    getOptionListGroups: function () {
        var facet = this.get('facet');
        var options = facet.get('Options');
        var groups = Ember.A();
        // s1 templates seperate groups by using BBTECH_OPT_GROUP as a value for the group label seperator.
        // other than that it is one solid group list

        var callback = function (){
            facet.get('SelectedValues')[this.get('value')] = this.get('isChecked');
            if ( ! facet.onChange() ) {
                facet.get('SelectedValues')[this.get('value')] = ! this.get('isChecked');
                this.revert();
            }
        };

        options.forEach(function ( item ) {
            var showing = item.Label ? false : true;
            var groupObject = Stream2.FacetListGroup.create({ Label : item.Label, Options: Ember.A(), showing: showing });
            item.Options.forEach( function ( subitem ) {
                var label = subitem[0] || '-';
                var value = subitem[1];
                var count = subitem[2] || null;

                var listObj = null;
                if ( facet.get('isGroupedMultiList') ){
                    var isChecked = ( facet.get('SelectedValues')[value] ) ? true : false;
                    if ( isChecked ) groupObject.set('showing',true);
                    listObj = Stream2.FacetCheck.create({
                        _label      : label,
                        value       : value,
                        isChecked   : isChecked,
                        onChange    : callback,
                        count       : count
                    });
                }
                else {
                    listObj = Stream2.ListItem.create({
                        _label      : label,
                        value       : value,
                        count       : count
                    });
                }
                groupObject.get('Options').pushObject( listObj );
            });
            groups.pushObject( groupObject );
        });
        return groups;
    }.property('facet.Options'),

    hideMoreFacets: function () {
        var facet = this.get('facet');
        var hiddenOptions = this.get('hiddenFacets');
        var hidden = true;
        $.each( facet.get('SelectedValues'), function ( key, val ) {
            $.each( hiddenOptions, function ( index, option ) {
                if ( option.get('value') === key ){
                    hidden = false;
                    return hidden;
                }
            });
            return hidden;
        });
        return hidden;
    }.property('facet.SelectedValues'),

    // for facets that contain options, returns the first 10
    // facets filtered by a text inputs value.
    topFilteredFacets: function () {
        var filter = this.get('textFilter');
        if ( filter ) {
            // We want to match on any word, as long as it
            // starts with a word boundary. SEAR-1593
            var regex = new RegExp( '\\b' + filter, 'i');
            return this.get('getOptions').filter(function(option) {
                return option.get('_label').match(regex);
            }).slice(0,10);
        }
        return this.get('getOptions').slice(0,10);
    }.property('facet.Options', 'textFilter'),
    hiddenFacets: function () {
        return this.get('getOptions').slice(10);
    }.property('facet.Options'),

    filterHasFocus: false,
    actions: {
        toggleFacetShow: function ( facet, w, h ){
            if ( this.get('isRestricted') ) { return; }
            this.$().children('div').toggle('slide', { direction: 'up' })
            this.toggleProperty('showing');
        },
        toggleGroupShow: function ( group ) {
            group.set('showing', ! group.get('showing'));
        },
        facetSearch: function () {
            this.get('facet').set('Value',this.get('value'));
            this.get('facet').onChange();
        },
        showMoreFacets: function (){
            this.set('hideMoreFacets', ! this.get('hideMoreFacets') );
            Stream2.pushEvent('show more facets','click');
        },
        filterHasFocus: function () {
            this.set('filterHasFocus',true);
        },
        filterLostFocus: function () {
            this.set('filterHasFocus',false);
        }
    }
});

/* FacetGroupedComponent

  Some facets (tags, adverts) come in a grouped data structure and are are to
  be displayed similar to HTML <optgroups>. This component is responsible
  for this type of grouping. Also provides these types of facets with
  a more interactive search box to select multiple facet options.

  Usage:
    {{facet-grouped facetType='tag' facet=facet facetSearch=facetSearch}}

*/
Stream2.FacetGroupedComponent = Stream2.FacetBaseComponent.extend({
    // the type of grouped facet (eg 'tag' or 'advert' etc)
    facetType: null,

    orderedGroups: function( groups ) {
        return groups.sort(function(groupA, groupB) {
            return groupA.get('sortOrder') - groupB.get('sortOrder');
        });
    },

    /* tag + advert + group facet
        ### ADVERTS v // expandable facet
            [ Search box ] // filterable
            -- group 1 -- v // expandable group
                - option 1 // multi clickable option
                - option 2
                         more // show more of this group
            -- group 2 -- > // closed expandable group
                   search all // open modal with all options


        if TAG - all groups expanded, "more" options hidden
        if Advert - first group expanded, "more" options hidden
        if Group - all groups hidden ( unless there is only one ), "more" options hidden
    */

    getOptionGroups: function () {
        var groups = Ember.A();

        var Options = this.get('facet').get('Options');
        if ( Ember.isArray( Options ) ) return [];

        for ( var group in Options ) {
            var option_primitives = this.getGroupOptions(group);

            option_primitives.forEach(function(option, index, options) {
                // move the selected options to the top
                if ( option.get('isChecked') ) {
                    var selected_option = options.splice(index, 1)[0];
                    options.unshift(selected_option);
                }
            });

            if ( !this.get('facet').get('isSorted') ) {
                option_primitives = option_primitives.sortBy('count').sortBy('isChecked').reverse();
            }

            var largeGroup = ( option_primitives.length > 10 );

            groups.pushObject(Ember.Object.create({
                label       : Options[group].Label,
                name        : group,
                options     : option_primitives,
                firstTen    : option_primitives.slice( 0, 10 ),
                largeGroup  : largeGroup,
                expanded    : false, // "show more"
                showing     : option_primitives.any(function(item) { return item.get('isChecked'); }), // "show top options"
                sortOrder   : Options[group].SortOrder,
            }));
        }
        var ordered = this.orderedGroups(groups);

        // if there is only one group, auto expand it and also if it's the advert facet
        if ( ordered.length === 1 ){
            ordered.set('firstObject.showing',true);
        }

        // Automatically show some groups if they exist
        if ( this.get('facet.isAdvert') && ordered.length >= 1 ) {
            return ordered.map(function(group) {
                ['general_submissions', 'default'].forEach(function(name) {
                    if ( Ember.isEqual(group.get('name'), name) ) { group.set('showing', true); }
                });
                return group;
            });
        }

        return ordered;
    }.property('facet.Options'),
    getAllOptionGroups: function () {
        var groups = [];

        var Options = this.get('facet').get('Options');
        if ( Ember.isArray( Options ) ) return [];
        for ( var group in Options ){
            var option_primitives = this.getGroupOptions(group);
            if ( !this.get('facet').get('isSorted') ) {
                option_primitives = option_primitives.sortBy('count').reverse();
            }
            groups.pushObject(Ember.Object.create({
                label     : Options[group].Label,
                name      : group,
                options   : option_primitives,
                count     : option_primitives.length,
                expanded  : false,
                sortOrder : Options[group].SortOrder,
            }));
        }
        return this.orderedGroups(groups);
    }.property('facet.Options'),
    getGroupOptions: function ( group ) {
        var options = [];
        var _this = this;
        var facet = this.get('facet');

        var callback = function (){
            facet.get('SelectedValues')[this.get('value')] = this.get('isChecked');
            if ( ! facet.onChange() ) {
                facet.get('SelectedValues')[this.get('value')] = ! this.get('isChecked');
                this.revert();
            }
        };

        if ( facet.get('Options') ){
            facet.get('Options')[group].Options.forEach( function ( item ) {
                var isChecked = ( facet.get('SelectedValues')[item.value] ) ? true : false;
                options.pushObject( Stream2.FacetCheck.create({
                    _label      : item.label ? item.label : item.value,
                    count       : item.count,
                    value       : item.value,
                    groupId     : facet.get('Name'),
                    isChecked   : isChecked,
                    onChange    : callback
                }) );
            });
        }
        return options;
    },
    getOptions: function () {
        var groups = this.get('getOptionGroups');
        var options = [];
        this.get('getOptionGroups').forEach( function ( item ) {
            options.addObjects( item.options );
        });
        return options;
    },
    _setOption: function ( newvalue, option ) {
        option.set( '_suspendObservation', true );
        option.set( 'isChecked', newvalue);
        this.get('facet').get('SelectedValues')[option.get('value')] = newvalue;
        option.set( '_suspendObservation', false );
    },
    actions: {
        searchAll: function ( facet ) {
            this.set('_facetSearch', facet);
        },
        close: function () {
            if ( this.get('hasChanged') ){
                this.get('facet').onChange();
            }
            this.set('textFilter',null);
            this.set('hasChanged',false);
            this.set('_facetSearch', null);
        },
        facetClick: function ( option ) {
            var _this = this;
            this.set('hasChanged',true);
            this._setOption(!option.get('isChecked'), option)
            this.set('textFilter',null);
        },
        showMore: function ( group ) {
            group.set('expanded', true);
        },
        showLess: function( group ){
            group.set('expanded' , false );
        },
        setAllOptions: function ( newValue, group ) {
            var _this = this;
            selectOption = function (option) {
                _this._setOption(newValue, option)
            }
            group.get('options').forEach( selectOption );
            this.set('textFilter',null);
            _this.get('facet').onChange();
         }
    },

    // a special view for searching grouped facets
    _facetSearch: null,
    facetSearchBoxSetup: function () {
        if ( this.get('_facetSearch') ){
            this.positionFacetBox();
        }
        else {
            $('body').css({ "overflow": "scroll" });
            window.onresize = window.onscroll = null;
        }
    }.observes('_facetSearch'),
    positionFacetBox: function () {
        var _this = this;
        // code to setup a standard lightbox which fills the screen
        // correctly and offers the correct scrolling
        $('body').css({ "overflow": "hidden" });
        $('html, body').animate( { scrollTop: 0 }, 200 );
        window.onresize = window.onscroll = function () { _this.positionFacetBox(); };
        var containerOffset = $('.facetContainer').offset();
        $('div.facet-search-box').css({
            "top": ( containerOffset.top - 50 ) + "px",
            "max-height": ( $(window).height() - 150 ) + "px"
        });
        $('div.darkness').css({
            "position": "fixed",
            "top": $(document).scrollTop() + "px",
            "height": $(window).height() + "px"
        });
    },
    filteredGroups: function () {
        var filter = this.get('textFilter');
        var unfilteredGroups = this.get('getOptionGroups');

        if ( ! filter ) return unfilteredGroups;

        var groups = Ember.A();
        var regex = new RegExp(filter.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'),'i');
        unfilteredGroups.forEach( function ( unfilteredGroup ) {
            var filteredGroup = unfilteredGroup.options.filter( function ( item ) {
                if ( filter ) return item.get('_label').match( regex );
                return item;
            });
            groups.pushObject(
                Ember.Object.create({ options: filteredGroup, label: unfilteredGroup.label, name: unfilteredGroup.name, showing: true })
            );
        });
        return groups;
    }.property('getOptionGroups','textFilter'),

    hasGroups: function() {
        return !Ember.isEmpty( Object.keys(this.get('facet.Options')) );
    }.property('facet.Options'),

    hasManyOptionsInGroups: function() {
        // always show the text filter for advert facets,
        // see https://broadbean.atlassian.net/browse/SEAR-1587
        if ( this.get('facetType') === "advert" && this.get('hasGroups') ) {
            return true;
        }

        // used to determine whether to display the textFilter
        var groups = this.get('getAllOptionGroups');
        var count = 0;
        groups.forEach(function(item) {
            count += item.count;
        });
        if (count > 10) {
            return true;
        }
        return false;
    }.property('getAllOptionGroups'),

    hasChanged: false
});

Stream2.ListItem = Ember.Object.extend({
    value               : null,
    _label              : null,
    count               : null,

    /* label
        returns a short version of the label if necessary ( followed by an elipsis )
        and count if available
    */
    label               : function () {
        var label = this.get('translatedLabel');
        if ( label.length > 35 ) {
            label = label.substr(0,35) + "…";
        }
        if ( this.get('count') ){
            var count = numeral( this.get('count') ).format('0,0');
            label += " (" + count + ")";
        }
        return label;
    }.property('_label'),
    /* longLabel
        returns a full length translated version of the label with a count
        where available
    */
    longLabel           : function () {
        var label = this.get('translatedLabel');
        if ( this.get('count') ){
            var count = numeral( this.get('count') ).format('0,0');
            label += " (" + count + ")";
        }
        return label;
    }.property('_label'),
    /* translatedLabel
        simply returns a translated version of the facet option label
    */
    translatedLabel     : function () {
        var label = this.get('_label');
        label = String(label);
        return i18n.__(label);
    }.property('_label'),
    _suspendObservation : false
});

Stream2.FacetListGroup = Ember.Object.extend({
    Label: '',
    hasLabel: function () {
        return !! this.get('Label');
    }.property('Label'),
    Options: null,
    showing: false,
    style: function () {
        return this.get('showing') ? 'display: block' : 'display: none';
    }.property('showing')
});

Stream2.FacetCheck = Stream2.ListItem.extend({
    isChecked           : false,
    Change              : function () {
        // if we want programatically revert the selection of this
        // checkbox in case the user has selected candidates
        // and chooses not to proceed with the resulting search
        if ( !this.get('_suspendObservation') ){
            this.onChange();
        }
    }.observes("isChecked"),
    onChange            : null,
    revert              : function () {
        this.set( '_suspendObservation', true );
        this.set( 'isChecked', ! this.get('isChecked') );
        this.set( '_suspendObservation', false );
    }
});

Stream2.RadioButtonComponent = Ember.Component.extend({
    tagName : "label",
    classNameBindings: [ ":facet-option", ":vertical-check", "checked:strong", ":radio-option" ],
    click : function() {
        this.set("selection", this.get('value') );
    },
    checked : function() {
        return this.get("value") == this.get("selection");
    }.property('selection'),
    didInsertElement: function () {
        this.$().html('<span class="label">'+this.get('label')+'</span>');
    }
});

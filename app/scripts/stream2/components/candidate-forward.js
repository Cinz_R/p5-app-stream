/*
  A self contained component that
  allows forwarding a single candidate inline
*/
Stream2.CandidateForwardComponent = Ember.Component.extend({
    candidate: null,

    // Logical stuff
    recipients: [],
    messageContent: '',

    // Gui stuff
    formIsShown: false,

    formInvalid: function(){
        if( ! this.get('formIsShown') ){
            return true;
        }

        if( Ember.isEmpty( this.get('recipients') ) ){
            return true;
        }
        return false;

    }.property('formIsShown' , 'recipients'),

    didInsertElement: function(){
        // Set the recipients to the current user
        if( this.searchUser.get('contact_email') ){
            this.set('recipients' , [{ id: this.searchUser.get('user_id'),
                                             text: this.searchUser.get('contact_name') + ' (' + this.searchUser.get('contact_email') + ')' }] );
        }else{
            this.set('recipients' , [] );
        }
    },
    hasRecipientDomainRestrictions: Ember.computed.bool('recipientDomainRestrictions'),
    recipientDomainRestrictions: Ember.computed('searchUser.settings.forwarding-restrict_recipient_domain',function () {
        return this.searchUser.get('settings.forwarding-restrict_recipient_domain');
    }),
    actions:{

        doForward: function(){
            var _this = this;


            this.$().find('.inline-form').first().slideUp('fast', function(){
                _this.set('formIsShown' , false);

                // Make sure this is switched off
                _this.send('switchOff');

                var candidate = _this.get('candidate');
                var recipients_objs = _this.get('recipients');
                var message = _this.get('messageContent');
                var recipients_emails = [];
                for( var i = 0 ; i < recipients_objs.length ; i++ ){
                    recipients_emails.push( recipients_objs[i].id );
                }
                Stream2.popupAlert(i18n.__("Forwarding..."), i18n.__('The recipients will get an email shortly.') );

                Stream2.resetMark();
                Stream2.requestCandidateAsync(
                    candidate,
                    candidate.apiURL( 'forward' ), { recipients: recipients_emails,
                                                     message: message,
                                                     cb_oneiam_auth_code: _this.searchUser.cbOneIAMAuthCode()
                                                   }, 'POST' ).then(
                    function ( data ) {
                        Stream2.timeMarkEvent('Candidate Actions','forward');
                        Stream2.popupAlert(i18n.__("Forwarding..."), data.message );
                        if( candidate.get('historicActionsChronological') ){
                            // The history is there. We should refresh it.
                            candidate.fetchProfileHistory();
                        }
                    }, function( error ){ console.log( error ); });
            });

            Stream2.pushEvent('candidate','forward cv');
        },

        showForm: function(){
            this.set('formIsShown' , true);
            Ember.run.next(this,function(){
                this.$().find('.inline-form').first().slideDown('fast');
            });
        },

        hideForm: function(){
            var _this = this;
            this.$().find('.inline-form').first().slideUp('fast', function(){
                _this.set('formIsShown' , false);
            });
        },

        switchOff: function(){
            // Does nothing here. Might do in subclasses
        },

        queryRecipients: function(query, deferred){
            var term = query.term;

            Stream2.singleRequest( '/api/user/colleagues', { search: term, isResponses: this.get('candidate').get('destination') == 'adcresponses' } , 'GET' ).then(
                function ( data ) {
                    deferred.resolve(data.suggestions);
                    // [ { id: term.length , text: term + 'BLA'} ]);
                });
        }
    }
});


/* Inline style candidate forward 
   Bind the 'shown' property to an external controlling object (here c) by using it like:

   {{candidate-forward-inline candidate=c shownBinding="c.showForward"}}

 */
Stream2.CandidateForwardInlineComponent = Stream2.CandidateForwardComponent.extend({
    shown: false, // Shown is a slot to be Bound to an external property
    shownHasChanged: function(){
        if( this.get('shown') ){
            this.send('showForm');
        }else{
            this.send('hideForm');
        }
    }.observes('shown'),

    actions:{
        /* Use that for the cancel button in this component */
        switchOff: function(){
            this.set('shown', false);
        }
    }
});

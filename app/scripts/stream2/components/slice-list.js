/*
    Stream2.SliceListComponent
    this slices a list to a maximum size from a give offset.

    Can instantiate with a plain array

    {{#slice-list list=people as |list_element|}}
       {{list_element}}
    {{/slice-list}}

    or you can handle an array of hashes

    {{#slice-list list=people  as |list_element|}}
        {{list_element.firstname}} {{list_element.lastname}}
    {{/slice-list}}

    list should be an array of single values, you can extend this component to
    take advantage of array of hashes (see Stream2.EducationListSpliceComponent).

    optional args:
    limit,  default to 5, this determines the maximum of the slice.
    offset, default to 0, this determines what which part of the array the splice
        starts at.

    note:
    "as |list_element|" is important as it proxies the context from
    the component to calling block.
*/

Stream2.SliceListComponent = Ember.Component.extend({

    list: null,
    offset: 0,
    limit: 5,
    sliced_list: function () {
        return this.get('list').splice(
            this.get('offset'),this.get('limit')
        );
    }.property(),

});

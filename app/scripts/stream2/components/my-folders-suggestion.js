/**
 * Represents a clickable MyFolders name. Instances of this class are
 * created in response to keyboard input to represent the names of existing
 * MyFolders the user can click on to short circuit the need to enter the
 * folder's name in pleno.
 *
 * @class MyFoldersSuggestionComponent
 * @constructor
 */
Stream2.MyFoldersSuggestionComponent = Ember.Component.extend( {
    tagName: 'li',
    name: '',
    parent: null,    // The MyFoldersComponent that contains this suggestion

    attributeBindings: ['style'],
    style: "cursor: pointer; float: left;",

    /**
     * Sets the input of the outer MyFoldersComponent to the name of this
     * suggestion. Ember calls this method when the suggestion's DOM element
     * is clicked.
     *
     * @method click
     */
    click: function() {
        this.get("parent").set( "input", this.get("name") );
    }
});
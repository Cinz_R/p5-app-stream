/**
 * This component extends the Text field to enable mutable text details
 * @class EditableDetailTextComponent
 * @namespace Stream2
 */

Stream2.EditableDetailTextComponent = Ember.TextField.extend({

    _previousValue: null,
    /**
     * The current stored value
     * @property storedValue
     * @default null
     */
    storedValue: null,
    /**
     * Sets the up the object at render time
     * @method didRender
     */
    didRender: function() {
        this.set('_previousValue', this.get('storedValue'));
    },

    /**
     * Computes the current visable string in the text field
     * @property value
     */
    value: Ember.computed( 'storedValue',  function() {
        if (Ember.isEmpty(this.get('storedValue'))) {
            return ''
        }
        return this.get('storedValue');
    }),

    /**
     * Updates the storedValue when the focus leaves the text
     * field and thus trips any observers on the storedValue
     * @method focusOut
     */
    focusOut: function() {

        var currentValue = this.get('value');
        var previousValue = this.get('_previousValue');

        // No point updating the stored value if the current and
        // previous values are empty ( '', null, undefined or [])
        if (Ember.isEmpty(currentValue) && Ember.isEmpty(previousValue)) {
            return;
        }
        if (currentValue != previousValue) {
            // a change has occured
            this.set('_previousValue', currentValue);

            // this will initiate a watcher in the candidate
            this.set('storedValue', currentValue);
        }
    }
});

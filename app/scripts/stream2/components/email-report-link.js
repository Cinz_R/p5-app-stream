Stream2.EmailReportLinkComponent = Ember.Component.extend({
    tagName: 'a',
    attributeBindings: ['href'],
    browserAttributes: [
        "appCodeName", "appName", "appMinorVersion", "platform",
        "systemLanguage", "userLanguage",
        "appVersion", "userAgent", "cookieEnabled"
    ],
    searchAttributes: {
        "SearchVersion"     : "stream2_VERSION",
    },
    userAttributes: {
        "SearchUserId"      : "user_id",
        "AdCourierUserId"   : "provider_id",
        "AdCourierCompany"  : "group_identity",
        "Provider"          : "provider"
    },

    // a link which takes them to the error calendar log list view
    logLink: function () {

        var timestamp = parseInt((new Date()).getTime() / 1000, 10);
        var query_object = {
            "facets"        : {
                "destination"   : [ this.get('board_id') ]
            },
            "date_epoch"    : timestamp,
            "action"        : "log_examples"
        };

        if ( this.searchUser.get('provider_id') ) {
            query_object.user_id = this.searchUser.get('provider_id');
        }
        if ( this.searchUser.get('group_identity') ) {
            query_object.facets.company = [ this.searchUser.get('group_identity') ];
        }

        var json = JSON.stringify( query_object );
        var query_string = "q=" + encodeURIComponent(json);

        return "http://errorcalendar.steamcourier.com/view-logs?" + query_string;
    }.property('board_id','error'),

    // information about the search/adcourier user/state
    userDump: function () {
        var dumpStr = "";

        // properties of the current application/environment
        var searchAttrs = this.get('searchAttributes');
        $.each( searchAttrs, function ( label, attr ) {
            if ( window[attr] ) {
                dumpStr += label + ": " + window[attr] + "\n";
            }
        });

        // properties of the current user
        var userAttrs = this.get('userAttributes');
        var searchUser = this.searchUser;
        $.each( userAttrs, function ( label, attr ){
            if ( searchUser.get( attr ) ){
                dumpStr += label + ": " + searchUser.get( attr ) + "\n";
            }
        });

        return dumpStr;
    }.property(),

    // interesting info about the browser
    browserDump: function () {
        var attrs = this.get('browserAttributes');
        var dumpStr = "";
        attrs.forEach( function ( attr ) {
            if ( navigator[attr] ) {
                dumpStr += attr + ": " + navigator[attr] + "\n";
            }
        });
        return dumpStr;
    }.property(),

    // this is where the mailto link is build when the view is rendered
    href: function () {
        var body_string = "\n\n" +
                          "---------------------------------------" +
                          "\n\nError Details for Broadbean Support...." +
                          "\n\n - Error\n" + this.get('error') +
                          "\n\n - Board\n" + this.get('board_id') +
                          "\n\n - Logs\n" + this.get('logLink') +
                          "\n\n - User Info\n" + this.get('userDump') +
                          "\n\n - Browser Info\n" + this.get('browserDump');
        var subject_string = "Search v2 Error report";
        var mail_to = stream2_support_email || 'support@broadbean.com';
        return 'mailto:' + mail_to + '?' +
               "body=" + encodeURIComponent( body_string ) +
               "&subject=" + encodeURIComponent( subject_string );
    }.property('error'),
    didInsertElement: function () {
        this.$().html(i18n.__('Email support'));
    }
});

/**
 * Transfers candidate from current user to "colleague"
 *
 * @class CandidateDelegateComponent
 */
Stream2.CandidateDelegateComponent = Ember.Component.extend({
    /**
     * The candidate to be delegated
     *
     * @property candidate
     * @type Stream2.Candidate
     * @default null
     */
    candidate: null,

    /**
     * The user that will receive the candidate
     *
     * @property colleague
     * @type Ember.Object
     * @default null
     */
    colleague: null,

    /**
     * Boolean property to toggle the delegate form
     *
     * @property formIsShown
     * @type boolean
     * @default false
     */
    formIsShown: false,

    /**
     * Determines whether the form contains all the correct data
     *
     * @method formInvalid
     * @return {Boolean}
     */
    formInvalid: function() {
        if (!this.get('formIsShown')) {
            return true;
        }
        if (Ember.isEmpty(this.get('colleague'))) {
            return true;
        }
        return false;
    }.property('formIsShown', 'colleague'),

    actions: {
        /**
         * Does the delegating of the candidate to colleague through an async
         * request, setting success/error properties on the candidate where
         * applicable.
         *
         * @method actions.delegateCandidate
         */
        delegateCandidate: function() {
            var _this = this;

            this.$().find('.inline-form').first().slideUp('fast', function() {
                _this.set('formIsShown', false);
                _this.send('switchOff');

                var candidate = _this.get('candidate');
                var colleague = _this.get('colleague');
                Stream2.resetMark();
                Stream2.requestCandidateAsync(
                        candidate,
                        candidate.apiURL('delegate'), {
                            colleague: colleague
                        },
                        'POST')
                    .then(
                        function(data) {
                            Stream2.timeMarkEvent('Candidate Actions', 'delegate');
                            Stream2.popupAlert(i18n.__('Delegation successful!'), data.message);
                            if (candidate.get('historicActionsChronological')) {
                                // The history is there. We should refresh it.
                                candidate.fetchProfileHistory();
                            }
                            Stream2.popupAlert(
                                i18n.__("Delegating..."),
                                i18n.__('This candidate is being transferred to your colleague')
                            );
                        }, function(error){ console.log( error ); }
                    );
            });

            Stream2.pushEvent('candidate', 'delegate');
        },

        /**
         * Sets the formIsShown property to true and slides open
         * the delegate form.
         *
         * @method actions.showForm
         */
        showForm: function() {
            this.set('formIsShown', true);
            Ember.run.next(this, function() {
                this.$().find('.inline-form').first().slideDown('fast');
            });
        },

        /**
         * Overridable method, use this to close the form in different contexts
         *
         * @method actions.switchOff
         */
        switchOff: function() {
            // Does nothing here. Might do in subclasses
        },

        /**
         * Opposite of showForm
         *
         * @method actions.hideForm
         */
        hideForm: function() {
            var _this = this;
            this.$().find('.inline-form').first().slideUp('fast', function() {
                _this.set('formIsShown', false);
            });
        },

        /**
         * Queries for a list of the users in the current users company,
         * used to populate the select-2 list for this forms select box.
         * Returns a list of users excluding the current user.
         *
         * @method actions.delegateCandidate
         */
        queryColleagues: function(query, deferred) {
            var term = query.term;

            Stream2.singleRequest('/api/user/colleagues', {
                search: term,
                excludeCurrentUser: true
            }, 'GET').then(
                function(data) {
                    deferred.resolve(data.suggestions);
                }
            );
        }
    }
});

/**
 * Inline delegation component for use in profile and result snippets of a candidate.
 *
 * @class CandidateDelegateInlineComponent
 * @extends CandidateDelegateComponent
 */
Stream2.CandidateDelegateInlineComponent = Stream2.CandidateDelegateComponent.extend({
    /**
     * Boolean property to determine whether this component is currently shown
     *
     * @property shown
     * @type Boolean
     * @default false
     */
    shown: false,

    /**
     * Observing property that listens for changes to the shown property,
     * executing the correct actions (show/hide form)
     *
     * @property shownHasChanged
     */
    shownHasChanged: function() {
        if (this.get('shown')) {
            this.send('showForm');
        } else {
            this.send('hideForm');
        }
    }.observes('shown'),

    actions: {
        /**
         * Use this as the action for the cancel button in this component.
         *
         * @property shown
         * @type Boolean
         * @default false
         */
        switchOff: function() {
            this.set('shown', false);
        }
    },
});

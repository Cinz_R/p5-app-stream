/*
    SettingsStringComponent

    given a user hierarchy object ( vivified to ember objects )

    var hierarchy = {
        label: "Company1",
        children: [
            {
                label: "office1",
                children: [
                    {
                        label: "this can go on forever"
                        children: [
                            ...
                        ]
                    },
                    ...
                ]
            },
            ...
        ]
    };

    {{settings-string user=hierarchy}}
    produces a cascading list setting type view of text boxes which interact with eachother
*/

Stream2.SettingsStringComponent = Ember.Component.extend({
    proxyVal: "",
    placeHolder: "",

    didInsertElement: function () {
        this.set('proxyVal',this.get('user.string_value'));
    },

    actions: {
        update: function () {
            if ( this.get('proxyVal') !== this.get('user.string_value') ){
                this.set('user.string_value', this.get('proxyVal'));
                if ( this.get('updateAction') ){
                    this.get('master').send( this.get('updateAction'), this.get('user') );
                }
                this.set('autoExpand', false);
            }
        },
        toggleChildren: function () {
            this.toggleProperty('user.showChildren');
            this.set('autoExpand', false);
        }
    },

    valueChanged: function (){
        if ( this.get('proxyVal') !== this.get('user.string_value') ){
            this.set('proxyVal', this.get('user.string_value'));
        }

        if ( this.get('_childChange') ){
            this.set('_childChange', false);
        }
        else if ( this.get('user.children') ){
            this.get('user.children').setEach('string_value', this.get('proxyVal'));
        }
    }.observes('user.string_value'),

    watchChildren: function () {
        var valHash = {};
        if ( this.get('autoExpand') ) this.set('user.showChildren', false);
        this.set('placeHolder','');

        // how many different values do my children have
        this.get('user.children').forEach( function ( child ) {
            var val = child.get('string_value') || '';
            valHash[val] = 1;
        });

        var numberOfKeys = Object.keys(valHash).length;
        var newVal = Object.keys(valHash)[0] || "";

        // if there is a consensus, then the parent should relfect that
        if( numberOfKeys < 2 ){
            if ( this.get('user.string_value') !== newVal ) {
                this.set('_childChange', true);
                this.set('user.string_value', newVal);
            }
            return;
        }
        // if there is dissonance, set parent value to blank
        else {
            if ( this.get('user.string_value') !== '' ) {
                this.set('_childChange', true);
                this.set('user.string_value', '');
            }
            this.set('placeHolder','---');
            if ( this.get('autoExpand') ) this.set('user.showChildren', true);
        }
    }.observes('user.children.@each.string_value')

});

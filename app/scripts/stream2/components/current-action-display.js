Stream2.CurrentActionDisplayComponent = Ember.Component.extend({
    attributeBindings: ['title'],
    currentAjaxCount: null,
    title: function () {
        return i18n.__('Current server actions underway');
    }.property(),
    classNames: ['global-status','current-actions'],
    mouseEnter: function () {
        this.$().children('div.actions-count').first().show("slide", { direction: "right" }, 300);
    },
    mouseLeave: function () {
        this.$().children('div.actions-count').first().hide("slide", { direction: "right" }, 300);
    }
});

Stream2.CandidateApplicationComponent = Ember.Component.extend({
    showing: false,
    tagName: 'li',
    classNames: ['application-item'],
    rankStyle: function () {
        return this.searchUser.flagRankToStyle( this.get('application.rank') );
    }.property('application.rank'),
    didInsertElement: function () {
        Ember.run.next( this, function () {
            if ( this.get('itemIndex') === 0 ){
                this.set('showing', true);
                this.$().children('div.application-attachments').first().show();
            }
        });
    },
    actions: {
        toggleAttachments: function () {
            this.set('showing', ! this.get('showing'));

            if ( this.get('showing') ){
                this.$().children('div.application-attachments').first().slideDown('fast');
            }
            else {
                this.$().children('div.application-attachments').first().slideUp('fast');
            }
        },
        downloadAttachment: function ( filename ) {
            Stream2.fileDownload( '/api/candidate/download_response_attachment?' + $.param( { filename: filename } ) ).then (
                function () {},
                function ( error ) {
                    Stream2.popupError( error );
                }
            );
            Stream2.pushEvent('candidate','download attachment');
        }
    }
});

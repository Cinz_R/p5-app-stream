Stream2.FacetFlaggingComponent = Stream2.FacetBaseComponent.extend({
    showing: true,
    isExpanded: false,      // show a "more" link when false

    flagsWithColours: Ember.computed( 'getOptions', 'isExpanded', function () {
        var _this = this;
        var flags = this.get("isExpanded")
                  ? this.get("getOptions")
                  : this.get("getOptions").slice(0, 10);
        return flags.map( function ( flag ) {
            flag.set('style',_this.searchUser.flagRankToStyle(flag.get('value')*1));
            return flag;
        });
    }),

    actions: {
        showMore: function() {
            this.set("isExpanded", true);
        },
        showLess: function() {
            this.set("isExpanded", false);
        }
    }
});

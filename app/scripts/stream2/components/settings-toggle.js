/*
    SettingsToggleComponent

    given a user hierarchy object ( vivified to ember objects )

    var hierarchy = {
        label: "Company1",
        children: [
            {
                label: "office1",
                children: [
                    {
                        label: "this can go on forever"
                        children: [
                            ...
                        ]
                    },
                    ...
                ]
            },
            ...
        ]
    };

    {{settings-toggle-component user=hierarchy}}

    produces a cascading toggle switch setting type view

    OFF            MAYBE        ON
    [(O  off)]     [(  O  )]    [(on   O)]

    When set to on or off, will call the given 'action' with the current user ( level in the hierarchy )
*/

Stream2.SettingsToggleComponent = Ember.Component.extend({
    displayConfirm: false,
    actions: {
        toggle: function ( new_boolean_value ) {
            // if they are switching from "maybe" get them to confirm
            if ( Ember.isNone(this.get('user.boolean_value')) && Ember.isNone(new_boolean_value) && this.get('user.children') ){
                return this.showConfirm();
            }

            if ( Ember.isNone( new_boolean_value ) ){
                new_boolean_value = ! ( this.get('user.boolean_value') || false );
            }

            // update my value
            this.set('user.boolean_value', new_boolean_value);

            // update children to reflect changes
            if ( this.get('user.children') )
                this.get('user.children').setEach('boolean_value', this.get('user.boolean_value'));

            // hide confirmation
            this.set('displayConfirm', false);

            if ( this.get('updateAction') ){
                this.get('master').send( this.get('updateAction'), this.get('user') );
            }

            this.set('autoExpand', false);
        },
        toggleChildren: function () {
            this.set('user.showChildren', ! this.get('user.showChildren'));
            this.set('autoExpand', false);
        },
        toggleConfirm: function () {
            this.set('displayConfirm', !this.set('displayConfirm'));
        }
    },
    /* Use that to display something on over */
    overTitle: function(){
        if( this.get('maybe') ){
            return i18n.__('Reset Options');
        }
        if( this.get('active') ){
            return i18n.__('Switch Off');
        }
        return i18n.__('Switch On');
    }.property('active', 'maybe'),
    active: function () {
        return ( this.get('user.boolean_value') || false );
    }.property('user.boolean_value'),
    maybe: function () {
        return ( Ember.isNone( this.get('user.boolean_value') ) );
    }.property('user.boolean_value'),
    updateChildren: function () {
        if ( this.get('user.children') ){
            var boolean_value = this.get('user.boolean_value');
            if ( ! Ember.isNone(boolean_value) )
                this.get('user.children').setEach('boolean_value', boolean_value);
        }
    }.observes('user.boolean_value'),
    showConfirm: function () {
        this.set('displayConfirm', true);
    },
    watchChildren: function () {
        if ( this.get('autoExpand') ) this.set('user.showChildren', false);

        if ( ! Ember.isArray( this.get('user.children') ) ){
            return;
        }

        // if all children are true
        var number_of_true_children = this.get('user.children').filterBy('boolean_value',true).length;
        var number_of_false_children = this.get('user.children').filterBy('boolean_value',false).length;
        var number_of_maybe_children = this.get('user.children').filterBy('boolean_value',null).length;

        // some maybe children
        if ( !! number_of_maybe_children ) {
            if ( this.get('autoExpand') ) this.set('user.showChildren', true);
            return this.set('user.boolean_value', null);
        }

        // some false and some true
        if ( number_of_true_children && number_of_false_children ) {
            if ( this.get('autoExpand') ) this.set('user.showChildren', true);
            return this.set('user.boolean_value', null);
        }

        this.set('displayConfirm', false);

        // some false children and no true children
        if ( ! number_of_true_children )
            return this.set('user.boolean_value', false);

        // some true children and no false children
        this.set('user.boolean_value', true);
    }.observes('user.children.@each.boolean_value').on('init')
});

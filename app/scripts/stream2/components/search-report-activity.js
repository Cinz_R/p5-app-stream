/**
 *
 * @namespace Stream2
 * @class SearchReportActivityComponent
 * @extends Ember.Component
 *
 *
*/
Stream2.SearchReportActivityComponent = Ember.Component.extend({
    fromDate: null,
    toDate:   null,
    maxDate:  null,

    currentlyProcessing: false,
    error: null,
    report: null,
    selectedCell: null,
    datedActions: null,
    showActionDetails: false,

    init: function(){
        var now = moment();
        // Last day of last month.
        var lastMonth = now.clone().date(1).subtract({ days: 1 });
        var defaultToDate = lastMonth.format('YYYY-MM-DD');
        // First day of last month.
        var defaultFromDate = lastMonth.clone().date(1).format('YYYY-MM-DD');
        this.set('fromDate',  defaultFromDate );
        this.set('toDate',  defaultToDate );
        this.set('maxDate', now.clone().format('YYYY-MM-DD') );
        return this._super();
    },

    formInvalid: function(){
        if( Ember.isEmpty(this.get('fromDate')) ||
            Ember.isEmpty(this.get('toDate') )){
            return true;
        }
        if( this.get('fromDate') >= this.get('toDate') ){
            return true;
        }
        return false;
    }.property('fromDate', 'toDate'),

    buttonInvalid: function(){
        return this.get('formInvalid') ||
            this.get('currentlyProcessing')
    }.property('formInvalid', 'currentlyProcessing'),

    // merge multiple rows for the same board into one, summing their counts
    // as we go
    summariseReport: function(report) {

        // remove the Office, Team and User columns
        var columnHeaders = report.table.head[0];
        columnHeaders.splice(1, 3);
        report.table.body.forEach( function(row) {
            row.splice(1, 3);
        } );

        // store first sighted occurrence of a board row, keyed by board name
        var seenBoards = {};
        report.table.body.forEach( function(row, idx) {
            var boardName = row[1].value;
            var origRow   = seenBoards[boardName];

            if ( origRow ) {

                // add elements 2 to 12 (inclusive) from row into origRow
                for (var i = 2; i <= 12; i++) {

                    // if there is something to add from row, ensure the
                    // "receiving" origRow cell is 0 rather than undefined
                    if ( typeof row[i] == 'object' ) {
                        if ( typeof origRow[i] != 'object' ) {
                            origRow[i] = {
                                action:  row[i].action,
                                board:   row[i].board,
                                header:  row[i].header,
                                user_id: row[i].user_id,
                                value:   row[i].value
                            };
                        }
                        else {
                            origRow[i].value += row[i].value;
                        }
                    }
                }
                report.table.body.splice(idx, 1);
            }
            else {
                seenBoards[boardName] = row;
            }
        } );

        // add a totals row to the bottom of the table
        var newRow = [ '', { value: i18n.__('Totals:') } ];
        for (var i = 2; i <= 12; i++) {
            var total = 0;      // total for current column
            report.table.body.forEach( function(row) {
                var cell = row[i];
                total += typeof cell == 'object' ? cell.value : 0;
            } );
            newRow.push( { value: total } );
        }
        report.table.body.push( newRow );
    },

    actions: {
        hideActionDetails: function() {
            this.set('showActionDetails', false);
        },
        showActionsInCell: function( cell ){
            var _this = this;
            this.set('error', null);
            this.set('datedActions', null);
            this.set('selectedCell', cell);

            Stream2.request('/api/user/actions', { action : cell.action,
                                                   board  : cell.board,
                                                   max_action_id: this.get('report').meta.max_action_id,
                                                   min_action_id: this.get('report').meta.min_action_id
                                                 }, 'GET' )
                .then(
                    function( data ){

                        /* make datedActions look like this:
                         * [
                         *   { ymd: '2017-05-28', actions: [actionObj, actionObj, ...] },
                         *   { ymd: '2017-05-26', actions: [actionObj, actionObj, ...] },
                         * ]
                         */
                        var datedActions = [];
                        var lastYMD      = null;
                        data.actions.forEach( function(action) {
                            var newYMD = action.insert_datetime.substr(0, 10);
                            if (newYMD != lastYMD) {
                                datedActions.push( { ymd: newYMD, actions: [] } );
                                lastYMD = newYMD;
                            }
                            datedActions[ datedActions.length - 1 ]
                                .actions.push({
                                    time:           action.insert_datetime.substr(11, 8) + " UTC",
                                    candidateName:  action.data.candidate_name,
                                    candidateEmail: action.data.candidate_email,
                                    advertLink:     action.data.advert_link
                                });
                        });
                        _this.set( 'datedActions', datedActions.reverse() );
                        _this.set( 'showActionDetails', true );
                    },
                    function(error){
                        _this.set('error', error);
                    }
                )
        },
        saveActionsToClientDisk: function() {
            var actionsByDate = this.get("datedActions");
            var csv = "Date\tTime\tName\tEmail\tAdvertLink\n";
            actionsByDate.forEach( function(actionsOnSameDay) {
                var date = actionsOnSameDay.ymd;
                actionsOnSameDay.actions.forEach( function(action) {
                    csv += date + "\t" + action.time + "\t"
                        + action.candidateName + "\t"
                        + action.candidateEmail + "\t"
                        + (action.advertLink || "N/A") + "\n";
                } );
            });
            csv.trimRight();    // remove terminal newline char
            var blob = new Blob( [csv], {type: "text/csv;charset=utf-8"} );
            saveAs(blob, "action-details.csv");
        },
        generateReport: function(){
            var _this = this;
            this.set('currentlyProcessing', true);
            this.set('datedActions', null);
            this.set('report', null );
            this.set('error', null);

            Stream2.requestAsync(
                '/api/user/activity_report', { from_str: this.get('fromDate'),
                                               to_str:   this.get('toDate')
                                             },
                'GET').then(
                    function(report){
                        _this.set('currentlyProcessing', false);

                        var reportObject = Ember.Object.create( report );
                        _this.summariseReport( reportObject );
                        _this.set('report', reportObject );
                    },
                    function( error ){
                        _this.set('currentlyProcessing', false);
                        _this.set('error', error);
                    }
                );
        }
    }
});

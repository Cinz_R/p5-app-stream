
/* http://emberjs.com/deprecations/v1.x/#toc_ember-select */
Stream2.SelectListComponent = Ember.Component.extend({
    tagName: 'select',
    content: null,
    selectedValue: null,

    didInitAttrs: function (attrs) {
        this._super.apply(this,arguments);
        var content = this.get('content');

        if (!content) {
            this.set('content', []);
        }
    },

    ob: Ember.observer( 'selectedValue', function () {
        this.$().val( this.get('selectedValue') );
    }),

    change: function() {
        var selectedEl = this.$()[0];
        var selectedIndex = selectedEl.selectedIndex;
        var content = this.get('content');
        var selectedValue = content[selectedIndex];
        var value = Ember.get(selectedValue,this.get('optionValuePath'));
        this.set('selectedValue', value);
        var changeAction = this.get('action');
        if ( typeof(changeAction) === 'function' ) {
            changeAction( value );
        }
    },

    optionGroups: Ember.computed( 'content', function () {
        var _this = this;
        var groups = {};
        var content = this.get('content');
        content.forEach( function( item ) {
            var group = Ember.get(item, _this.get('optionGroupPath'));

            if ( typeof( groups[group] ) == "undefined" ){
                groups[group] = {
                    label: group,
                    options: []
                }
            }

            groups[group].options.push(item);
        });

        return Object.keys( groups ).map( function ( key ) {
            return groups[key];
        });
    })
});
        

/* candidate-manage-tags

   A component to manage a collection of tags

*/

Stream2.CandidateManageTagsComponent = Ember.Component.extend({
    candidate: null,
    namespace: "tags",
    addTitle: "Add Tag/Hotlist",
    icon: "icon-folder-add"
});

/* A list of email messages grouped by date. There is no "display" related 
 * logic to it.
 */
Stream2.DatedEmailsListComponent = Ember.Component.extend({
    classNames: ['email-component'],
    candidate: null,
    keywords:  null,
    page:      1,
    pager:     null,
    emailLists: function() {
        var datedEmailLists = Ember.A();

        var url;
        var query_string = { page: this.get('page') };
        var candidate    = this.get('candidate');
        var keywords     = this.get('keywords');

        if ( keywords ) {
            query_string.keywords = keywords;
        }

        var base_url = this.searchUser.is_admin ? '/api/admin' : '/api/user';

        if (candidate) {
            url = candidate.apiURL('email_history');
        }
        else {
            url = base_url + '/emails/search';
        }

        var _this = this;
        Stream2.request(url, {}, 'GET', query_string).then( function(value) {

            // group emails by date
            var curEmailList;
            value.emails.forEach( function ( email ) {
                var ymd   = email.date.substr(0, 10);

                if ( !curEmailList || curEmailList.date != ymd ) {
                    curEmailList = Stream2.DatedEmailList.create(
                                                { date: ymd, emails: [] } );
                    datedEmailLists.pushObject( curEmailList );
                }
                curEmailList.emails.push( Stream2.Email.create( {
                    id:            email.id,
                    date:          email.date,
                    subject:       email.subject,
                    from:          email.from,
                    recipient:     email.recipient,
                    recipientName: email.recipientName
                } ) );
            });
            _this.set( 'pager', Stream2.Pager.create(value.pager) );
        });

        return datedEmailLists;
    }.property('keywords', 'page'),

    // user has entered a new search string: starts new search: reset page
    keywordsChanged: function() {
        this.set('page', 1);
    }.observes('keywords'),

    actions: {
        firstPage: function() {
            this.set('page', this.get('pager').first_page);
        },
        prevPage: function() {
            this.set('page', this.get('pager').previous_page);
        },
        nextPage: function() {
            this.set('page', this.get('pager').next_page);
        },
        lastPage: function() {
            this.set('page', this.get('pager').last_page);
        }
    }
});

/*

Stream2.DefaultProfileBodyComponent

@description For general candidate profile viewing, this component mainly
just renders the profile HTML as returned by the server. It also handles
search term highlighting when keywords are provided, this includes the use
of a wildcard character '*'. For our international friends, it also matches
ascii vowels against their more exotic counterparts.

*/

Stream2.DefaultProfileBodyComponent = Ember.Component.extend({
    i18nreplacements: [
        [/[.*+?^${}()|[\]\\]/g, '\\$&'], // Escape regex from user's keywords
        [ /([ao])e/ig, "$1" ],
        [ /e/ig, "[eèéêë]" ],
        [ /a/ig, "([aàâä]|ae)" ],
        [ /i/ig, "[iîï]" ],
        [ /o/ig, "([oôö]|oe)" ],
        [ /u/ig, "[uùûü]" ],
        [ /y/ig, "[yÿ]" ],
    ],
    didRender: function () {
        if ( Ember.isEmpty( this.get('searchTerms') ) ){
            return;
        }
        var _this = this;
        this.get('searchTerms').forEach( function ( term ) {
            var hasLeadingWildcard = /^\*/.test(term);
            var hasTrailingWildcard = /\*$/.test(term);
            term = term.replace(/^\*/, '').replace(/\*$/, '');

            var regexpString = _this.get('i18nreplacements').reduce(
                function ( input, re ) {
                    return input.replace( re[0], re[1] );
                },
                term
            );
            if(hasLeadingWildcard) {
                regexpString = '\\S*' + regexpString;
            }
            if(hasTrailingWildcard) {
                regexpString += '\\S*';
            }

            _this.$().markRegExp( new RegExp( regexpString, "i" ) );
        });
    }
});

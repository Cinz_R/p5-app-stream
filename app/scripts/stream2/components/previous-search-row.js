Stream2.PreviousSearchRowComponent = Ember.Component.extend({
    expanded: false,
    attributeBindings: ['criteria_id:data-criteria-id'],
    classNameBindings: [':line',':search-row'],
    criteria_id: function () {
        return this.get('result.criteria_id');
    }.property('result.criteria_id'),
    logURL: function () {
        var result = this.get('result');
        return result.stream2_base_url + 'searchrecords/' + result._id;
    }.property('result'),
    searchURL: function () {
        var result = this.get('result');
        var url = "/login/as/" + result.user_provider_id;
        var params = "redirect_to=" + encodeURIComponent( ['', 'search', result.criteria_id, result.destination, 'page', 1].join('/') );
        return url + "?" + params;
    }.property('result'),
    /*
        globalCriteria

        returns an array of common tokens with a label and a nice value,
        ready to be displayed
    */
    globalCriteria: function () {
        var result = this.get('result');
        var tokens = result.tokens.tokens;

        var crit_tokens = [
            {
                label: "Salary",
                value: [
                    tokens.salary_cur + tokens.salary_from,
                    i18n.__("to"),
                    tokens.salary_cur + tokens.salary_to,
                    i18n.__("per"),
                    i18n.__( tokens.salary_per )
                ].join(" ")
            },
            {
                label: "Updated",
                value: this._listLookup( 'cv_updated_within_options', tokens.cv_updated_within )
            },
            {
                label: "Job Type",
                value: this._listLookup( 'jobtype_options', tokens.default_jobtype )
            }
        ];
        return crit_tokens;
    }.property('result'),
    actions: {
        expand: function () {
            this.set('expanded', !this.get('expanded'));
        }
    },

    /*
        _listLookup

        we have the value of the selected option, when this value belongs to a list
        it could very well be just an ID which means nothing to the user
        do a reverse lookup to find the corresponding label
    */
    _listLookup: function ( listName, value ){
        if ( typeof( value ) === "object" )
            value = value[0];

        var lists = this.get('criteriaLists') || {};
        var list = lists[listName];
        if ( ! list ) return;

        // WARNING - the global criteria is in the format [ value, label ]
        // whereas the board specific criteria is in the format [ label, value ]
        var entry = list.filter( function ( item ) {
            return ( item[0] == value );
        });

        if ( entry.length ){
            return entry[0][1];
        }

        return value;
    }
});

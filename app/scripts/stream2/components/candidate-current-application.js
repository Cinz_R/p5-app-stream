Stream2.CandidateCurrentApplicationComponent = Stream2.CandidateApplicationComponent.extend({
    templateName: 'components/candidate-application',
    rankStyle: function() {
        return this.searchUser.flagRankToStyle( this.get('candidate.broadbean_adcresponse_rank') );
    }.property('candidate.broadbean_adcresponse_rank')
});

/**
 *
 * @namespace Stream2
 * @class JqueryUiDatepickerComponent
 * @extends Ember.Component
 *
 * A Date Picker. Grep the code for jquery-ui-datepicker for an example.
 *
*/


Stream2.JqueryUiDatepickerComponent = Ember.TextField.extend(
    JqueryUiMixin,
    {
        uiType: 'datepicker',
        uiOptions: ["altField", "altFormat", "appendText", "autoSize",
                    "beforeShow", "beforeShowDay", "buttonImage", "buttonImageOnly",
                    "buttonText", "calculateWeek", "changeMonth", "changeYear", "closeText",
                    "constrainInput", "currentText", "dateFormat", "dayNames", "dayNamesMin",
                    "dayNamesShort", "defaultDate", "duration", "firstDay", "gotoCurrent",
                    "hideIfNoPrevNext", "isRTL", "maxDate", "minDate", "monthNames",
                    "monthNamesShort", "navigationAsDateFormat", "nextText", "numberOfMonths",
                    "onChangeMonthYear", "onClose", "onSelect", "prevText",
                    "selectOtherMonths", "shortYearCutoff", "showAnim", "showButtonPanel",
                    "showCurrentAtPos", "showMonthAfterYear", "showOn", "showOptions",
                    "showOtherMonths", "showWeek", "stepMonths", "weekHeader", "yearRange",
                    "yearSuffix"]
    }
);

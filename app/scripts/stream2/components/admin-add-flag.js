/*
 * Stream2.AdminAddFlagComponent
 * Small UI component for defining the characteristics
 * of a new response flag
*/
Stream2.AdminAddFlagComponent = Ember.Component.extend({

    classNames: ['admin-flag-form','lastUnit'],
    flag: null,

    // we only want to alter the properties of the baseFlag if the
    // update is successful so we can roll back easily
    _flag: function () {
        var flag = Stream2.ResponseFlagModel.create({ colour_hex_code: '#000000' });
        var baseFlag = this.get('flag');
        if ( baseFlag ){
            flag.setProperties( baseFlag.getProperties('id','description','colour_hex_code') );
        }
        return flag;
    }.property('flag'),

    didInsertElement: function () {
        this.$().children('input').first().spectrum({
            preferredFormat: 'hex'
        });
    },
    actions: {
        submitFlag: function () {
            this.sendAction('submitFlag', this.get('_flag'));
        },
        cancel: function () {
            this.sendAction('cancel', this.get('flag'));
        }
    },

    formInvalid: function () {
        return Ember.isBlank(this.get('_flag.description'));
    }.property('_flag.description')
});

Stream2.BoardControlComponent = Ember.Component.extend({
    actions: {
        clearWatchdogBoardResults: function ( board ) {
            this.sendAction('clearWatchdogBoardResults', board);
        },
        changeBoard: function ( board ) {
            this.sendAction('changeBoard', board);
        }
    },

    didInsertElement: function() {

        // allow boards to be draggable so the user can change their order
        var $ulElem          = jQuery("#tour-board-selection");
        var $movableBoards   = $ulElem.children("li:not(.internal)");
        var $immovableBoards = $ulElem.children("li.internal");

        $movableBoards.attr( "title", i18n.__("Drag & drop to change order") );

        $ulElem.sortable( {
            items:                $movableBoards,
            placeholder:          'job-board-dropzone',
            forcePlaceholderSize: true,

            // dragging a board has started
            start: function(event, ui) {
                ui.item.css('cursor', 'move');
                $immovableBoards.css("opacity", "0.5");
            },

            // stop is an event that fires when the user has finished
            // dragging and dropping a board
            stop: function(event, ui) {
                ui.item.css('cursor', 'pointer');
                $immovableBoards.css('opacity', '1');
                var boardNames = $ulElem.children().map( function(i, e) {
                    var name = e.getAttribute('data-id');
                    return name;
                } ).get();
                Stream2.request(
                    "/api/boards/set-positions",
                    { boards: boardNames },
                    'POST'
                ).then(
                    function(data) { /* no data expected */ },

                    // isn't constructive to tell the user their board
                    // re-ordering wasn't saved (?)
                    function(err_str) {}
                );
            }
        } );
    }
});

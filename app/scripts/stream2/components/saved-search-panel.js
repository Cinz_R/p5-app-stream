/* Stream2.SavedSearchPanel
    controls the drop down which displays current saved
    searches for a user
*/
Stream2.SavedSearchPanelComponent = Ember.Component.extend({
    _defaultFlavours: function () {
        return {
            search: i18n.__('Saved Searches'),
            email_campaign: i18n.__('Email Campaigns')
        };
    }.property(),
    actions: {
        unsaveSearch: function(saved_search) {
            var _this = this;
            Stream2.request("/api/savedsearches", {id: saved_search.id}, 'DELETE').then(
                function(data) {
                    _this.get('savedSearches').removeObject(saved_search);
                },
                function(data) {
                    alert("Sorry your saved search cannot be removed right now. Please try again later.");
                }
            );
            Stream2.pushEvent('search','unsave');
        }
    },
    /* flavours
        returns an array of flavour objects which contain a label and associated
        searches
    */
    flavours: function () {
        var _this=this;
        var flavour_map = {};
        this.get('savedSearches').forEach( function ( saved_search ) {
            var flavour = saved_search.get('flavour');
            if ( typeof(flavour_map[flavour]) == "undefined" ){
                flavour_map[flavour] = {
                    name: flavour,
                    label: _this.get('_defaultFlavours')[flavour] || flavour.replace("_"," ").capitalize(),
                    searches: []
                };
            }
            flavour_map[flavour].searches.pushObject( saved_search );
        });
        var flavour_array = $.map(flavour_map, function( obj ) { return obj; })
                .sort( function ( a, b ){
                    if ( a.name === "search" ){ return -1; }
                    if ( b.name === "search" ){ return 1; }
                    return ( a.name > b.name ) ? 1 : -1;
                });
        return flavour_array;
    }.property('savedSearches.@each')
});

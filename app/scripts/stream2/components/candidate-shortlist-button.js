/* A shortlist button that reacts to the right type of list
 of cheese sandwich.

 if withAction="true" and action="blaBla" is given, this will
 send the action "blaBla" with the argument candidate and customList to the managing
 controller or route.

*/
Stream2.ShortListButtonBaseComponent = Ember.Component.extend({
    tagName: "button",
    classNameBindings: [
        'candidate.has_cv::hidden',
        'searchUser.restrictUI:restrictUI'
    ],
    attributeBindings: ['title'],

    // Building properties
    candidate: null,
    customList: null,

    // Extra setting
    withAction: false,

    // Which part of the GUI does this work into?
    // Default to ''
    namespace: '',

    click: function(){
        if( this.get('withAction') ){
            this.sendAction('action', this.get('candidate') , this.get('customList') , this.get('namespace') );
        }
        return true;
    },

    customListName: function(){
        return this.get('customList').get('name');
    }.property('customList'),

    init: function(){
        var _this = this;
        var shortListPropertyName = 'has_done_shortlist_' + this.get('customListName');
        // Observes the right has_done_shortlist_ property on the candidate.
        this.get('candidate').addObserver(shortListPropertyName,
                                          function(){
                                              var hasShortlist = !!  this.get(shortListPropertyName);
                                              _this.set('hasDoneShortlist', hasShortlist);
                                          });
        return this._super();
    },

    hasDoneShortlist: function(){
        var shortlistFlagname = 'has_done_shortlist_' + this.get('customListName');
        return !! this.get('candidate').get( shortlistFlagname );
    }.property("candidate", "customListName"),

    didInsertElement: function(){
        var icon_class = this.get('customList.icon_class');
        this.$().html('<div class="candidate-action-btn-inner"><i class="sprite-fontello ' + icon_class + '"></i></div>');
    },

    title: Ember.computed( 'customList.name','searchUser.manageResponsesView', function () {
        if ( this.get("searchUser.restrictUI") ){
            return i18n.__('Contact your account manager to upgrade!');
        }
        else{
            if ( this.get('customList.name') === 'adc_shortlist' ){
                if ( this.get('searchUser.manageResponsesView') ){
                    return i18n.__p( 'manage-responses-view', 'Shortlist' );
                }
                return i18n.__('Shortlist');
            }
            return this.get('customList.label');
        }
    })
});

// This is to display in candidate result 'snippets'
Stream2.ShortListButtonComponent = Stream2.ShortListButtonBaseComponent.extend({
    classNameBindings: [
        ':btn',
        'candidate.hasCVQuota::disabled-action-btn',
        'hasDoneShortlist:btn-success',
        'candidate.shortlistEnabled::disabled-action-btn',
    ]
});

// This is to display a round button in the new candidate preview
Stream2.ShortListRoundButtonComponent = Stream2.ShortListButtonBaseComponent.extend({
    tagName: 'div',
    classNameBindings: [
        ':candidate-action-btn',
        'candidate.hasCVQuota::disabled-action-btn',
        'hasDoneShortlist:success',
        'candidate.shortlistEnabled::disabled-action-btn',
    ]
});

// This is to display in the candidate preview action toolbar
Stream2.ShortListLongButtonComponent  = Stream2.ShortListButtonBaseComponent.extend({
    classNameBindings: [
        ':btn',
        ':action-button',
        'candidate.hasCVQuota::disabled-action-btn',
        'hasDoneShortlist:btn-success',
        'candidate.shortlistEnabled::disabled-action-btn',
    ],
    didInsertElement: function(){
        this.$().html(this.get('title'));
    }
});

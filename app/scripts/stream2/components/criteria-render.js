/*

  Renders a Stream2.Criteria

  Parameters:

     criteria a Stream2.Criteria

     showUpdated: defaults to false. Show the 'Updated Withing' criteria.

*/

Stream2.CriteriaRenderComponent = Ember.Component.extend({
    criteria: null,
    showUpdated: false,

    hasShowableSalary: function(){
        var criteria = this.get('criteria');
        if( Ember.isNone(criteria) ){
            return false;
        }
        return criteria.get('salary_from') || criteria.get('salary_to');
    }.property('criteria')

});

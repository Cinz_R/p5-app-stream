Stream2.SearchSideControlComponent = Ember.Component.extend({
    saveSearchTarget: function() {
        return this._dropdownTarget("#save-search-dialog");
    }.property('searchUser.restrictUI'),

    shareSearchTarget: function() {
        return this._dropdownTarget("#share-search-dialog");
    }.property('searchUser.restrictUI'),

    _dropdownTarget: function(targetId) {
        var user = this.get("searchUser");
        return user.get("restrictUI") ? null : targetId;
    },

    actions: {
        closeWatchdog: function (){
            this.sendAction('closeWatchdog');
        },
        clearSearch: function () {
            this.sendAction('clearSearch');
        },
        cancelNav: function() {

            // cancel click event (i.e. return false) for users with restricted
            // UI
            return !this.get("searchUser").get("restrictUI");
        }
    }
});

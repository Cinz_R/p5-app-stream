Stream2.CandidateAddNoteComponent = Ember.Component.extend({
    notePrivacy: "user",
    noteContent: null,
    // As of story 79104524 , a note is ALWAYS public.
    // privacyValues: [{ 'label':'Private', 'value':'user' },{ 'label':'Public', 'value':'group' }],
    actions: {
        add: function () {
            var candidate = this.get('candidate');
            var content = this.get('noteContent');
            if ( content ){
                // As of story 79104524 , a note is ALWAYS public (group level privacy).
                // var privacy = this.get('notePrivacy');
                this.sendAction( 'addNote', candidate, content, 'group');
                this.set('noteContent','');
                this.set('notePrivacy','user');
            }
        },
        addEmoticon: function ( icon ) {
            var content = this.get('noteContent') || "";
            this.set('noteContent', content + icon);
        }
    }
});

Stream2.CandidateNoteComponent = Ember.Component.extend({
    tagName: 'li',

    note: null,  // The note looked at
    candidate: null, // The candidate looked at
    user: null, // The current user

    relativeTime: function (){
        return moment.unix(this.get('note.insert_epoch')).fromNow();
    }.property('note.insert_epoch'),
    insertDateTime: function () {
        return (new Date(this.get('note.insert_epoch') * 1000)).toLocaleString();
    }.property('note.insert_epoch'),
    author: function () {
        return this.get('note.author');
        // As of story 79104524 , a note is ALWAYS public.
        // return this.get('note.privacy') == "user" ? "You" : this.get('note.author');
    }.property('note.privacy','note.author'),

    /* True if the current user can delete this note */
    canDelete: function(){
        // get('note').get('author_id') does NOT work
        // This is because notes against a candidate are pure javascript structures,
        // and not fancy Ember objects

        var user = this.get('user');

        // Logged in as super admin (i.e. having an original_user allows to delete any note
        // otherwise, just the author
        return  ( !Ember.isNone(user.get('original_user')) ) || ( this.get('note.author_id') == user.get('user_id') );
    }.property('note.author_id', 'user'),

    "private": function () {
        return false;
        // As of story 79104524 , a note is ALWAYS public.
        // return ( this.get('note.privacy') == "user" );
    }.property('note.privacy'),

    actions: {
        "delete": function () {
            if ( confirm( i18n.__("Are you sure?") ) ){
                this.sendAction( 'deleteNote', this.get('candidate'), this.get('note') );
            }
        }
    }
});

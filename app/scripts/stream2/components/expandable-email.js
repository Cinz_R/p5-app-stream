/* An email message that can be viewed in 2 states: expanded and collapsed. In
 * collapsed state, only the message's date, recipient and subject are 
 * visible. In expanded state, the message body is also shown, directly below
 * the aforementioned fields.
 */
Stream2.ExpandableEmailComponent = Ember.Component.extend({
    email: null,
    bodyShowing: false,
    showFromTo: true,

    didInsertElement: function(){
        Ember.run.schedule('afterRender', this, function(){
            this.refreshBodyShowing();
        });
    },

    refreshBodyShowing : function(){
        var $bodyElem = this.$().find('.messageBody');
        if ( this.get('bodyShowing') ) {
            $bodyElem.slideDown(250);
        }
        else $bodyElem.slideUp(250);
    },

    actions: {
        toggleBody: function(email) {
            this.toggleProperty('bodyShowing');
            this.refreshBodyShowing();
        }
    }
});

function standardiseSalary ( salaryText ) {
    // trim leading/trailing whitespace
    var newSalary = salaryText.replace(/^\s+/,'').replace(/\s+$/,'');

    // expand 20k to 20000
    newSalary = newSalary.replace(/[kK]$/, '000');

    // remove any commas
    newSalary = newSalary.replace(',','');

    return newSalary;
}

// Keeps the iframe at 100%-ish of content height
// Resizes the frame on resize events
function maintainIframeContentHeight ( iframe ) {
    var resizeFrame = function(iframe) {
        var height = iframe.contentWindow.document.body.scrollHeight;

        // Avod being triggered by our own changes.
        if(iframe.lastHeight == height) {
            return;
        }

        iframe.style.height = height + 'px';

        iframe.lastHeight = iframe.contentWindow.document.body.scrollHeight;

        // Edge case cause of scrolling
        iframe.contentWindow.document.body.style.overflow = 'hidden';
        iframe.contentWindow.document.body.style.margin = '0';
    }

    resizeFrame(iframe);
    iframe.contentWindow.addEventListener(
        'resize',
        function() {
            resizeFrame(iframe);
        }
    );
}
Ember.Handlebars.helper('trimString', function(passedString,maxChars) {
    if ( passedString ) {
        return passedString.substring(0,maxChars);
    }
    return "";
});

// safestring tells handlebars not to html escape the string which is its default behaviour
Ember.Handlebars.helper('trimSafeString', function(passedString,maxChars) {
    if ( passedString ) {
        var theString = passedString.substring(0,maxChars);
        return new Ember.Handlebars.SafeString(theString);
    }
    return "";
});

// special case: we want the stirng to be escaped, but not the highlighting!
Ember.Handlebars.helper('handleHighlight', function ( text ) {
    return new Ember.Handlebars.SafeString(
        Ember.Handlebars.Utils.escapeExpression( text ).replace(/&lt;em&gt;(.*?)&lt;\/em&gt;/g,'<em>$1<\/em>')
    );
});

Ember.Handlebars.helper('epoch2DateTime', function(epoch, timezone) {
    var date = moment.unix(epoch);
    if ( timezone ) {
        date = date.tz( timezone );
    }
    return date.format('lll zz');
});

Ember.Handlebars.helper('epoch2Date', function(epoch, timezone) {
    var date = moment.unix(epoch);
    if ( timezone ) {
        date = date.tz( timezone );
    }
    return date.format('ll');
});


Ember.Handlebars.helper('isoDateToNiceDate', function (isoDate, timezone) {
    var date =  moment.utc(isoDate);
    if ( timezone ) {
        date = date.tz( timezone );
    }
    return date.format('lll zz');
});

Ember.Handlebars.helper('momentFormat', function( moment , format ){
    return moment.format(format);
});

Ember.Handlebars.helper('isoDateFromNow', function( isoDate, timezone ){
    var date =  moment.utc(isoDate);
    if ( timezone ) {
        date = date.tz( timezone );
    }
    return date.fromNow();
});

Ember.Handlebars.helper('isoDateToShortDate', function ( isoDate, timezone ) {
    var date =  moment.utc(isoDate);
    if ( timezone ) {
        date = date.tz( timezone );
    }
    return date.format('ll');
});

// e.g. 12th Nov 2015 - common European format
Ember.Handlebars.helper('isoDateToEuroDate', function(isoDate, timezone) {
    var date = moment.utc(isoDate);
    if (timezone) {
        date = date.tz(timezone);
    }
    return date.format('Do MMM, YYYY');
});

Ember.Handlebars.helper('toJson', function ( dataStructure ) {
    return JSON.stringify( dataStructure );
});

// highlights the given text and terms ( array )
// returns it as a "SafeString" ready to be inserted into handlebars
Ember.Handlebars.helper('highlightSafeText', function ( text, terms ) {
    if ( Ember.isNone( text ) ){
        return '';
    }
    if ( ! Ember.isEmpty( terms ) ) {
        // jquery-highlight usually takes an element and converts the data therein, thats no use to us with ember
        // and handlebars and so on
        text = $('<div />').html(text).highlight( terms, { element: 'em', wordsOnly: true , wordsBoundary: '(?:\\b|\\W|$)' } ).html();
    }
    return new Ember.Handlebars.SafeString(text);
});

Ember.Handlebars.helper('textOrDefault', function (label, text) {
    label = i18n.t(label);
    text = Ember.Handlebars.Utils.escapeExpression(text);
    if ( !text ) {
        text = '<em class="font-grey">Not available</em>';
    }
    return new Ember.Handlebars.SafeString('<span><strong>' + label + ':</strong> ' + text + '</span><br />');
});

// this is an unbound helper which will let you translatye keys in place for use with element attibutes
Ember.Handlebars.registerHelper('__', function(i18n_key, stash) {
    var result = i18n.t(i18n_key);
    return new Ember.Handlebars.SafeString(result);
});
// this binds object changes to translations of the keys
Ember.Handlebars.helper('__b', function(i18n_key, stash) {
    var result = i18n.t(i18n_key);
    return new Ember.Handlebars.SafeString(result);
});
Ember.Handlebars.helper('__x', function(i18n_key, stash) {
    var result = i18n.t(i18n_key, stash.hash);
    return new Ember.Handlebars.SafeString(result);
});
Ember.Handlebars.helper('__n', function(i18n_key, plural, stash) {
    var result = i18n.t(i18n_key, stash.hash);
    return new Ember.Handlebars.SafeString(result);
});
Ember.Handlebars.helper('__nx', function(i18n_key, plural, count, stash) {
    stash.hash.count = count;
    var result = i18n.t(i18n_key, stash.hash);
    return new Ember.Handlebars.SafeString(result);
});

String.prototype.toTitleCase = function () {
        return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

Ember.Handlebars.helper( 'is-equal', function(leftSide, rightSide) {
      return leftSide === rightSide;
});

/* see https://github.com/begedin/ember-select-list/ */
Ember.Handlebars.helper( 'get-attr-path', function ( object, path ) {
    if ( path ) {
        return Ember.get(object,path);
    }
    else {
        return object;
    }
});
Ember.Handlebars.helper( 'is-equal-by-path', function(leftSide, rightSide, path) {
    if (path) {
        return Ember.get(leftSide, path) === rightSide;
    }
    else {
        return leftSide === rightSide;
    }
});

Ember.Handlebars.helper( 'gt', function ( left, right ) {
    return left > right;
});

Ember.Handlebars.helper( 'flag-style', function ( searchUser, flag_id ) {
    var adcFlags = searchUser.adcresponse_flags;
    if ( Ember.isEmpty(adcFlags) ){
        return "color: #000000"
    }

    var flagSettings = adcFlags.findBy('flag_id', parseInt(flag_id));
    if (flagSettings) {
        return "color: " + flagSettings.colour_hex_code;
    }
    return "color: #000000";
});


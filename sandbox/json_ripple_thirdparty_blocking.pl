#! perl -w

use local::lib qw/local/;

use strict;
use warnings;

use Log::Any qw/$log/;
use Log::Any::Adapter ('Stderr');

use Carp::Always;
use Crypt::JWT;

use LWP::UserAgent;
use Bean::Stream2::JRipple;

use HTTP::Request::Common;
use JSON;

# my $endpoint = 'https://search.adcourier.com/ripple/search-results' ;
my $endpoint = 'https://127.0.0.1:3001/ripple/search-results';
# my $endpoint = 'https://svrus4srch2stg.cb.careerbuilder.com:3001/ripple/search-results';


my $api_key = '0d2492261404'; # TextKernel Key
my $secret_key = '71d3e52c-0401-4af6-ac43-bcfab70c5e7f';

my $ua = LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 });

my $jripple = Bean::Stream2::JRipple->new({
    api_key => $api_key,
    endpoint => $endpoint,
    useragent => LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 } )
});

my $group_identity = 'wowsuchcompany';
my $provider_id = 'manyUserID3';
my $subscriptions = {
    totaljobs => {
        auth_tokens => {
            'totaljobs_password' => 'Random100',
            'totaljobs_username' => 'cinzia@broadbean.com'
        },
    },
    reed => {
        auth_tokens => {
            reed_username => 'aliz@broadbean.com',
            reed_posting_key => '98fac8fd-8920-42a6-8d3a-5bcdd7b9c7ba',
        },
    },
    'jobsite_robot' => {
        auth_tokens => {
            jobsite_robot_username => 'afletcher@encoredriving.co.uk',
            jobsite_robot_password => 'Encore123',
            jobsite_robot_scode => '9079',
        },
    },
};

my $account = {
    provider       => 'third_party',
    secret_key     => $secret_key,
    group_identity => 'wowsuchcompany',
    provider_id    => 'manyUserID2',
    contact_name => 'Esmeralda Degros',
    contact_email => 'esmeralda@degros.com',
    subscriptions  => $subscriptions
};

# Time to build a $jwt_token
my $jwt_token = Crypt::JWT::encode_jwt(
    payload         => $account,
    alg             => 'PBES2-HS256+A128KW',
    enc             => 'A128GCM',
    relative_exp    => 60,
    key             => $secret_key
);

my $ripple_request = {
    account => {
        provider    => 'third_party',
        jwt_token   => $jwt_token
    },
    api_key => $jripple->api_key
};

my $request = HTTP::Request::Common::POST(
    $endpoint,
    'Content-Type' => 'application/json',
    Content => JSON::encode_json( {
        api_key => $jripple->api_key,
        channel => { type => 'external',
                     name => 'totaljobs'
                 },
        account => {
            provider => 'third_party',
            jwt_token => $jwt_token,
        },
        query => { ofccp_context => 'HOYEAHBABY' , keywords => "" ,
                   # City of London
                   # location => { "latitude" => "51.513591",
                   #               "longitude" => "-0.098514" },
                   # London (according to google).
                   # location => { "latitude" => "51.5074",
                   #               "longitude" => "-0.1278" },
                   # radius => 30,
               },
    } )
);

my $response = $ua->request( $request );
print $response->as_string();

use Data::Dumper;
print Dumper( JSON::decode_json($response->content()) );

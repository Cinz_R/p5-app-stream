#!/usr/bin/env perl

use local::lib qw/local/;

# moo barr doubel quack
use strict; use warnings;

use Bean::Stream2::JRipple;
use Data::Dumper;
use LWP::UserAgent;

use HTTP::Request::Common;


use Log::Any::Adapter qw/Stderr/;
$| = 1;

=head1 DESCRIPTION

I do a JSON ripple request

=cut



# my $endpoint = 'https://search.adcourier.com/ripple';
# my $endpoint = 'https://127.0.0.1:3001/ripple';
my $endpoint = 'https://svrus4srch2stg.cb.careerbuilder.com/ripple';

my $api_key = '1097103005';
my $secret_key = 'b8a59be6c456206f5a97be4fe6f770fb3ec7143fbbce4ceb54ed30352a8ff43a';
my $username = 'cinzia.second@team_one.London.cinzwonderland';

my $jripple = Bean::Stream2::JRipple->new({
    api_key => $api_key,
    endpoint => $endpoint,
    useragent => LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 } )
});


## Step 1 , Login with all sorts of settings.

my $login_query = {
    username => $username,
    secret_key => $secret_key,
    config => {
        #cb_oneiam_user => 'hien.vu@careerbuilder.com',
        cb_oneiam_user => 'jerome.eteve@careerbuilder.com',
        stylesheet_url => '/static/stylesheets/custom-demo.css',
        shortlists => [
            { name => 'cheese_sandwiches',
              label => 'Cheese Sandwich',
              options_url => '_this_is_dummy_'
          }
        ],
        new_candidate_url => '_this_is_dummy_',
    },
    custom_fields => [
        { name => 'saussage',  asHttpHeader => 0,  content => 'Field 1 Content' },
        { name => 'mash',  asHttpHeader => 1,  content => 'Field 2 Content' },
    ]
};

my $request = $jripple->build_login_request( $login_query );

my $login_data = $jripple->make_request( $request );
warn Dumper( $login_data );

## Step 2, Build a criteria ID from criteria.
## Note there's no fancy method in the client to do that (yet).

my $plain_search = $endpoint; $plain_search =~ s/\/ripple.*//;
my $criteria = { keywords => 'manager' , ofccp_context => 'boudinblanc' }; # Etc..
$request = HTTP::Request::Common::POST( $plain_search.'/api/create_criteria', [ q => $jripple->json()->encode({ criteria =>  $criteria }) ],
                                        'x-ripple-session-id' => $login_data->{ripple_session_id}
                                    );
my $criteria_data = $jripple->make_request( $request );
warn Dumper( $criteria_data );

## Step 3, log your user directly in search with the prefilled criteria and board.
print "\n\n".$plain_search.'/search/'.$criteria_data->{data}->{id}.'/careerbuilder?STOK='.$login_data->{STOK}."\n\n";


#!/usr/bin/env perl
# moo barr doubel quack
use strict; use warnings;

use Bean::Stream2::JRipple;
use Data::Dumper;
# use Log::Any::Adapter qw/Stderr/;
$| = 1;

=head1 DESCRIPTION

I do a JSON ripple request

=cut



#my $endpoint = 'https://search.adcourier.com/ripple';
# my $endpoint = 'https://127.0.0.1:3001/ripple';
my $endpoint = 'https://svrus4srch2stg.cb.careerbuilder.com:3001/ripple';

my $api_key = '1097103005';
my $secret_key = 'b8a59be6c456206f5a97be4fe6f770fb3ec7143fbbce4ceb54ed30352a8ff43a';
my $username = 'cinzia.second@team_one.London.cinzwonderland';
my $results_per_page = 12;
my @results;
my $jripple = Bean::Stream2::JRipple->new({
    api_key => $api_key,
    endpoint => $endpoint,
    useragent => LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 } )
});

warn "performing request:";
my $search_out = $jripple->search_request(
    {
        account => { username           => $username, secret_key => $secret_key },
        config  => { results_per_page   => $results_per_page, show_adcourier_navigation => 1 },
        channel => { type               => 'external' , name => 'talentsearch' },
    }
);

my $status_out;
while ( !$status_out || ( $status_out->{status} !~ m/\A(?:completed|failed)\z/ ) ){
    sleep( 1 );
    $status_out = $jripple->status_request( $search_out->{ripple_session_id} );
}

if ( my $error = $status_out->{context}->{error} ){
    die $error;
}

my $results = $jripple->results_request( $search_out->{ripple_session_id} );

printf("There are %s results, we expected %s, the total was %s\n",
    scalar @{$results->{results}},
    $results_per_page,
    $status_out->{context}->{result}->{total_results}
);

print "\n---Candidate Name---\n";
print $results->{results}->[0]->{name};
print "\n---Profile URL---\n";
print $results->{results}->[0]->{profile_url};
print "\n------\n";
# use DDP;
# print np $results;

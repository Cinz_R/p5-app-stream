#!/usr/bin/env perl

use strict; use warnings;

use Bean::Stream2::JRipple;
use Data::Dumper;
use Log::Any::Adapter ('Stdout');
$| = 1;

=head1 DESCRIPTION

I do a JSON ripple request

=cut


# use Log::Any::Adapter qw/Stderr/;

#my $endpoint = 'https://search.adcourier.com/ripple';
# my $endpoint = 'https://127.0.0.1:3001/ripple';
my $endpoint = 'https://svrus4srch2stg.cb.careerbuilder.com:3001/ripple';

# my $api_key = '4015265503';
# my $secret_key = '2c515ad9b81e34f73fedb552a33dda9bf2da2ce90fd1ef71acfc1bb3989b7158';
# my $username = 'pat@pat.pat.pat';

my $api_key = '1097103005';
my $secret_key = 'b8a59be6c456206f5a97be4fe6f770fb3ec7143fbbce4ceb54ed30352a8ff43a';
my $username = 'cinzia.second@team_one.London.cinzwonderland';

my $jripple = Bean::Stream2::JRipple->new({
    api_key => $api_key,
    endpoint => $endpoint,
    useragent => LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 } )
});

{
    my $search_out = $jripple->search_request(
        {
            account => { username           => $username, secret_key => $secret_key },
            config  => { results_per_page   => 12,
                         stylesheet_url => '/static/stylesheets/custom-demo.css',
                         shortlists => [
                             { name => 'cheese_sandwiches',
                               label => 'Cheese Sandwich',
                               options_url => '_this_is_dummy_'
                           }
                         ],
                         new_candidate_url => '_this_is_dummy_',
                     },
            channel => { type               => 'external' , name => 'talentsearch' },
        },
        {
            # render_results => 1,
        }
    );

    my $status_out;
    while ( !$status_out || ( $status_out->{status} !~ m/\A(?:completed|failed)\z/ ) ){
        sleep( 1 );
        $status_out = $jripple->status_request( $search_out->{ripple_session_id} );
    }

    if ( my $error = $status_out->{context}->{error} ){
        die $error;
    }

    my $results = $jripple->results_request( $search_out->{ripple_session_id} );

    use DDP;
    p $results;
    # use DDP;
    # p @{ $results->{results}}[0..2];
}

# print "\n\n\nAND NOW AS ANOTHER USER\n\n\n\n";

# {
#     my $results = $jripple->search_request(
#         $secret_key,
#         {
#             account => { username           => 'cinzinpoland@Team_2.London.cinzwonderland' },
#             config  => { results_per_page   => 12 },
#             channel => { type               => 'external' , name => 'talentsearch' },
#         },
#         {
#             render_results => 1,
#         }
#     );

#     use DDP;
#     p @{ $results->{results}}[0..2];
# }

# warn  scalar @{$results->{results}};

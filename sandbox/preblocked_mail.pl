#!/usr/bin/perl
use strict;
use warnings;
use LWP::UserAgent;
use HTTP::Request;
use JSON;

use Time::HiRes qw( gettimeofday );
use Data::Dumper;
use Log::Any::Adapter;

# toggle verbose errors to go to STDERR
# use Carp::Always;
Log::Any::Adapter->set('Stderr');

my $lwp = LWP::UserAgent->new;
$lwp->ssl_opts( SSL_verify_mode => 0x00 );
$lwp->ssl_opts( verify_hostname => 0 );
my $email = 'A_email@gmail.com';
my $type  = 'blocked';
my $uri = 'https://127.0.0.1:3001/external/mailjet_notification/transport';
# my $uri = 'https://svrus4srch2stg.cb.careerbuilder.com:3001/external/mailjet_notification';
# my $uri = 'https://search.adcourier.com/external/mailjet_notification';
for (1..2) {
    warn $_;
    my ( $seconds, $microseconds ) = gettimeofday;
    my $json = {
        "event"            => $type,
        "time"             => $seconds,
        "MessageID"        => '16607025220435159',
        "email"            => $email,
        "mj_campaign_id"   => 0,
        "mj_contact_id"    => 0,
        "customcampaign"   => "",
        "CustomID"         => "helloworld",
        "Payload"          => "",
        "error_related_to" => "recipient",
        "error"            => "spam preblocked",
        "hard_bounce"      => 0,
    };
    my $req = HTTP::Request->new( 'POST', $uri );
    $req->header( 'Content-Type' => 'application/json' );
    $req->content( JSON::to_json($json) );
    my $res = $lwp->request($req);
}
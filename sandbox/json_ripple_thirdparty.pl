#! perl -w

use strict;
use warnings;

use Log::Any qw/$log/;
use Log::Any::Adapter ('Stderr');

use Carp::Always;
use Crypt::JWT;

use Bean::Stream2::JRipple;

#my $endpoint = 'https://search.adcourier.com/ripple' ;
#my $endpoint = 'https://127.0.0.1:3001/ripple';
my $endpoint = 'https://svrus4srch2stg.cb.careerbuilder.com:3001/ripple';

my $api_key = '0d2492261404'; # TextKernel Key
my $secret_key = '71d3e52c-0401-4af6-ac43-bcfab70c5e7f';

my $jripple = Bean::Stream2::JRipple->new({
    api_key => $api_key,
    endpoint => $endpoint,
    useragent => LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 } )
});

my $group_identity = 'wowsuchcompany';
my $provider_id = 'manyUserID2';
my $subscriptions = {
    totaljobs => {
        auth_tokens => {
            'totaljobs_password' => 'Random100',
            'totaljobs_username' => 'cinzia@broadbean.com'
        },
    }
};

my $account = {
    provider       => 'third_party',
    secret_key     => $secret_key,
    group_identity => 'wowsuchcompany',
    provider_id    => 'manyUserID2',
    contact_name => 'Esmeralda Degros',
    contact_email => 'esmeralda@degros.com',
    subscriptions  => $subscriptions
};

# Time to build a $jwt_token
my $jwt_token = Crypt::JWT::encode_jwt(
    payload         => $account,
    alg             => 'PBES2-HS256+A128KW',
    enc             => 'A128GCM',
    relative_exp    => 60,
    key             => $secret_key
);

my $ripple_request = {
    account => {
        provider    => 'third_party',
        jwt_token   => $jwt_token
    },
    api_key => $jripple->api_key
};

my $http_request = HTTP::Request->new( POST => $jripple->endpoint . "/login" );
$http_request->content_type('application/json');
$http_request->content( $jripple->json()->encode( $ripple_request ) );
$http_request->header( 'Accept' => 'application/json' );

my $res = $jripple->useragent->request( $http_request );
my $res_ref = $jripple->json->decode( $res->decoded_content );
my $session = $jripple->session( $res_ref->{ripple_session_id} );

use DDP;
p $session;

# ok we have a session. Time to do a search :)

my $search_out = $jripple->search_request(
    {
        ripple_session_id => $session->ripple_session_id(),
        config  => { results_per_page   => 1 },
        channel => { name => 'totaljobs' },
    }
);

my $status_out;
while ( !$status_out || ( $status_out->{status} !~ m/\A(?:completed|failed)\z/ ) ){
    sleep( 1 );
    $status_out = $jripple->status_request( $search_out->{ripple_session_id} );
}

if ( my $error = $status_out->{context}->{error} ){
    die $error;
}

my $results = $jripple->results_request( $search_out->{ripple_session_id} );

#hit the phonecall url

$results->{results}->[1]->{ actions }->{ cv } =~ /^(^.*)\/cv.*/;

my $phonecall_url = $1 . '/phonecall';

$http_request = HTTP::Request->new( POST => $phonecall_url );
$http_request->header('x-api-key' => $jripple->api_key() );
$http_request->header('x-ripple-session-id' => $session->ripple_session_id() );
# warn $phonecall_url;
p $jripple->useragent->request( $http_request );

p $results;

print "Phonecall response:\n";

p $results->{results}->[1];

print "This was in session:\n";

p $session;

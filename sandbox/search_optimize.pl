#!/usr/bin/env perl

use strict; use warnings;
use Time::HiRes qw( gettimeofday tv_interval );
use Data::Dumper;
use Stream2;
use Stream2::Action::Search;
use lib qw(.);
use Stream::Scraper::XML;


BEGIN {
    my $DEFAULT_UA_MOCK = ( $ENV{TEST_LIVE} || $ENV{TEST_URL} ) ? 'passthrough' : 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;


my $c_file = $ARGV[0] || die 'config file needed as first arg';

my $s2 = Stream2->new({ config_file => $c_file });
$s2->stream_schema();
my $test_params = {
    'template' => 'gojobsite',
    'options' => {
        'include_facets' => '1'
    },
    'remote_ip' => '192.168.1.213',
    'criteria_id' => 'vOyw0vfM6wGtmB0jm5xQZQkkwB8',
    'user' => {
        'auth_tokens' => {
            'gojobsite_email' => 'chris.hines@jmdigital.com',
            'gojobsite_agency_id' => '18195'
        },
        'user_id' => '92',
        'group_identity' => 'jm'
    },
    'stream2_base_url' => 'https://search.adcourier.com/'
};

my $job = MyJob->new({
    params => $test_params,
    stream2 => $s2
});


my %cases = (
    findvalue_fast     => \&findvalue_fast,
    findvalue_original => \&findvalue_maybefast,
    findvalue_cached   => \&findvalue_cached,
    findvalue_cached_f => \&findvalue_cached_fast,
);

no warnings 'redefine';
while (my ($label, $code) = each %cases ) {
    local *Stream::Scraper::XML::findvalue = $code;

    local $SIG{__WARN__} = sub { 1; };
    foreach my $iteration ( 1 .. 10) {
        my $t0 = [gettimeofday];
	my $r = Stream2::Action::Search->perform( $job );
	printf "%s: %s: %.2f\n", $iteration, $label, tv_interval ( $t0 );
        LWP::UserAgent::Mockable->finished();
        if ( $ENV{LWP_UA_MOCK} eq 'record' ) {
            exit;
        }
        LWP::UserAgent::Mockable->reset($ENV{ LWP_UA_MOCK }, $ENV{ LWP_UA_MOCK_FILE });
    }

    print "\n\n";
}


exit;

{
    package MyJob;
    sub new {
        my ( $class, $ref ) = @_;
        return bless $ref, $class;
    }
    sub parameters {
        return $_[0]->{params};
    }
    sub guid {
        return "TEST-SCRIPT-" . time;
    }
}

sub findvalue_fast {
    my $self = shift;
    my $xpath = shift;

    my $doc = $self->doc();
    if ( !$doc ) {
        $self->log_warn('Unable to findvalue(%s): no parsed xml', $xpath );
        return;
    }

    ## XML::LibXML->findvalue returns a concated string instead of an array when it matches multiple nodes
    ## I want it to return an array
    #my @values = map {
    #    $_->to_literal();
    #} $doc->findnodes( $xpath, @_ );

    #if ( wantarray ) {
    #    return @values;
    #}

    #return shift @values;
    return $doc->find($xpath);
}

sub findvalue_maybefast {
    my $self = shift;
    my $xpath = shift;

    my $doc = $self->doc();
    if ( !$doc ) {
        $self->log_warn('Unable to findvalue(%s): no parsed xml', $xpath );
        return;
    }

    ## XML::LibXML->findvalue returns a concated string instead of an array when it matches multiple nodes
    ## I want it to return an array
    my @values = map {
        $_->to_literal();
    } $doc->findnodes( $xpath, @_ );

    if ( wantarray ) {
        return @values;
    }

    return shift @values;
}

my %cache;
sub findvalue_cached {
    my $self = shift;
    my $xpath = shift;

    my $doc = $self->doc();
    if ( !$doc ) {
        $self->log_warn('Unable to findvalue(%s): no parsed xml', $xpath );
        return;
    }

    my $compiled_xpath = $cache{$xpath} ||= XML::LibXML::XPathExpression->new($xpath);  

    ## XML::LibXML->findvalue returns a concated string instead of an array when it matches multiple nodes
    ## I want it to return an array
    my @values = map {
        $_->to_literal();
    } $doc->findnodes( $compiled_xpath, @_ );

    if ( wantarray ) {
        return @values;
    }

    return shift @values;
}

my %cache2;
sub findvalue_cached_fast {
    my $self = shift;
    my $xpath = shift;

    my $doc = $self->doc();
    if ( !$doc ) {
        $self->log_warn('Unable to findvalue(%s): no parsed xml', $xpath );
        return;
    }

    my $compiled_xpath = $cache2{$xpath} ||= XML::LibXML::XPathExpression->new($xpath);  

    return $doc->find($compiled_xpath);
}

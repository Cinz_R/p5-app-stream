use strict;

use JSON;
use File::Slurp;
use Data::Dumper;

use HTTP::Request;
use LWP::UserAgent;

my $ua = LWP::UserAgent->new(ssl_opts => {
                                          verify_hostname => 0
                                         });

my %fields = do{
    my $req = HTTP::Request->new(GET => 'https://broadbean:wor2lahT@es.bb.artirix.com/_mapping');

    my $resp = $ua->request($req);
    unless( $resp->is_success() ){
        die $resp->as_string();
    }

    my $json = $resp->content();
    my $struct = JSON::decode_json($json);
    %{ $struct->{items1}->{applicant}->{properties} };
};

my $res = do{
    my $req = HTTP::Request->new(GET => 'https://broadbean:wor2lahT@es.bb.artirix.com/items/applicant/_search');

    my $resp = $ua->request($req);
    unless( $resp->is_success() ){
        die $resp->as_string();
    }
    my $json = $resp->content();
    my $struct = JSON::decode_json($json);
    $struct;
};


my @docs = map{ $_->{_source} } @{ $res->{hits}->{hits} };

foreach my $field ( sort keys ( %fields ) ){
    print qq|=== $field ===

|;
    my %props = %{ $fields{$field} };
    foreach my $property ( sort keys %props ){
        print qq|$property|.q|: |.$props{$property}."\n\n";
    }

    my @samples = map{ length($_) > 100 ? substr($_,0,100).'...' : $_ }  grep { length($_) } map{ ref($_) ? join('|' , @$_) : $_ }  map{ $_->{$field} } @docs ;
    my $sample_string = join(', ', map{ $_ =~ s/\n//g ; $_  } grep{ $_ } @samples[0..5]);
    print qq|sample: |.$sample_string."\n\n";
    print qq|
|;
}


# print Dumper($res);

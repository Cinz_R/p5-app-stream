-- Useful monitoring queries to be run manually for now.

-- Check watchdog run delays.
select w.id , u.timezone , timediff(ws.last_run_datetime , w.next_run_datetime - INTERVAL 1 DAY ) as delay ,  w.next_run_datetime , last_run_datetime, ws.insert_datetime,  s.board  from watchdog_subscription ws JOIN cv_subscriptions s ON ws.subscription_id = s.id JOIN watchdog w  ON w.id = ws.watchdog_id JOIN cv_users u ON u.id = w.user_id WHERE w.insert_datetime < '2014-11-17' order by  delay  desc  limit 40;

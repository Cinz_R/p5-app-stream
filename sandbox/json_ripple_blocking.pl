#!/usr/bin/env perl

use local::lib qw/local/;
use strict; use warnings;

use LWP::UserAgent;
use Bean::Stream2::JRipple;
use Data::Dumper;
use JSON;

use Log::Any::Adapter ('Stdout');
$| = 1;


=head1 DESCRIPTION

I do a JSON ripple request

=cut

my $ua = LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 });

# use Log::Any::Adapter qw/Stderr/;

#my $endpoint = 'https://search.adcourier.com/ripple/search-results';
# my $endpoint = 'https://127.0.0.1:3001/ripple/search-results';
my $endpoint = 'https://svrus4srch2stg.cb.careerbuilder.com:3001/ripple/search-results';

# my $api_key = '4015265503';
# my $secret_key = '2c515ad9b81e34f73fedb552a33dda9bf2da2ce90fd1ef71acfc1bb3989b7158';
# my $username = 'pat@pat.pat.pat';

my $api_key = '1097103005';
my $secret_key = 'b8a59be6c456206f5a97be4fe6f770fb3ec7143fbbce4ceb54ed30352a8ff43a';
my $username = 'cinzia.second@team_one.London.cinzwonderland';

my $jripple = Bean::Stream2::JRipple->new({
    api_key => $api_key,
});

my $request = $jripple->build_search_request(
    {
        account => {
            secret_key  => $secret_key,
            username    => $username
        },
        channel => { type => 'external' , name => 'talentsearch' },
        query => { ofccp_context => 'HOYEAHBABY' },
    }
);

$request->uri( $endpoint );

my $response = $ua->request( $request );

print Dumper( JSON::decode_json( $response->content ) );

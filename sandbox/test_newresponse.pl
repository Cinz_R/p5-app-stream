#!/usr/bin/env perl bin/s2-shell
use strict; use warnings;

use LWP::UserAgent;
use JSON;

my $url = 'https://127.0.0.1:3001/external/sns_bbcss_responses';
# my $url = 'https://svrus4srch2stg.cb.careerbuilder.com:3001/external/sns_bbcss_responses';
my $notification = {
    api_url => 'https://bbcss.adcourier.com/api/',
    document_id => 4074240, # This is a real document in cinzwonderland responses.
    customer_key => 'bbts_cinzw314f294977', # cinzwonderland bbcss live responses holder.
    broadbean_adcuser_id => 206757, # This is cinz.second, the owner of this document ID.
};

my $encoded_message = $s2->pubsub_client()->serialize({ data => $notification } );

my $ua = LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 });

my $res = $ua->post( $url,
                     'x-amz-sns-message-type' => 'Notification',
                     Content => JSON::to_json({
                         TopicArn => 'arn:aws:sns:eu-west-1:868002146347:Broadbean_BBCSS_Response',
                         Message => $encoded_message,
                     }));

print $res->as_string();

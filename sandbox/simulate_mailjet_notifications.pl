#!/usr/bin/perl
use strict;
use warnings;

use LWP::Useragent;
use HTTP::Request;
use JSON        ();
use Time::HiRes ();

my $lwp = LWP::UserAgent->new;
$lwp->ssl_opts( SSL_verify_mode => 0x00 );
$lwp->ssl_opts( verify_hostname => 0 );

# options

my $email = 'stuart_test_block_email_addresses@broadbean.com';

my $type = 'bounce_soft';
# my $type  = 'bounce_hard';
# my $type  = 'block';
# my $type  = 'spam';

# my $server =
# 'https://svrus4srch2stg.cb.careerbuilder.com:3001/external/mailjet_notification';

my $server = 'https://127.0.0.1:3001/external/mailjet_notification';
# my $server = 'https://search.adcourier.com/external/mailjet_notification';

my $number_of_requests = 10;

# this should be

my %disp_table = (
    bounce_soft => \&bounce_soft,
    bounce_hard => \&bounce_hard,
    spam        => \&spam,
    block       => \&block,
);

run_requests( $number_of_requests, $type, $email, $server );

# this is a test.
run_requests( 5, 'bounce_hard', $email, $server );

sub run_requests {
    my ( $number_of_requests, $type, $email, $server ) = @_;
    for ( 1 .. $number_of_requests ) {
        warn "$_ $type";
        my $req = HTTP::Request->new( 'POST', $server );
        $req->header( 'Content-Type' => 'application/json' );
        $req->content( json_request( $type, $email, $server ) );
        $lwp->request($req);
    }
}

sub json_request {
    my ( $type, $email, $server ) = @_;
    my ( $seconds, $microseconds ) = Time::HiRes::gettimeofday;
    return JSON::to_json(
        {
            "MessageID"        => $seconds . $microseconds,
            "customcampaign"   => "",
            "email"            => $email,
            "error_related_to" => "recipient",
            "mj_campaign_id"   => 4861658149,
            "mj_contact_id"    => 0,
            "time"             => $seconds,
            "mj_contact_id"    => $seconds . $microseconds,
            %{ $disp_table{$type}->() }

        }
    );
}

sub bounce_soft {
    return {
        "blocked"          => JSON::false,
        "error"            => "",
        "error_related_to" => "",
        "event"            => "bounce",
        "hard_bounce"      => JSON::false,
    };
}

sub bounce_hard {
    return {
        "blocked"          => JSON::true,
        "error"            => "user unknown",
        "error_related_to" => "recipient",
        "event"            => "bounce",
        "hard_bounce"      => JSON::true,

    };
}

sub spam {
    return {
        "event"  => "spam",
        "source" => "SOME REASON",
    };
}

sub block {
    return {
        "error"            => "preblocked",
        "error_related_to" => "mailjet",
        "event"            => "blocked",
    };
}


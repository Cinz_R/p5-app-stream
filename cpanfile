#-*- mode: Perl -*-
# vim: set syntax=perl:

# Needed for autodieing systemA
requires 'IPC::System::Simple';

# This require Cpanel::JSON::XS@2.3310
# is to AVOID JSON::MaybeXS that is
# installed by some sub dependency to
# require JSON::XS >= 3.0, which would break our
# build.
# JSON::MaybeXS has got some DYNAMIC dependencies,
# which really doesn't play well with carton.
#
# In the build, you MIGHT have to force install this before
# doing the carton install, as the install order is NOT guaranteed.
#
# See https://metacpan.org/source/ETHER/JSON-MaybeXS-1.003005/Makefile.PL
# See https://build.adcourier.com/job/p5-app-stream_build--dev/configure
requires 'Cpanel::JSON::XS' , '== 2.3310';
#
# End of witty comments

requires 'B::Deparse';

requires 'Spiffy'  , '== 0.31';
requires 'Carp::Always';
requires 'Redis', '== 1.972';

requires 'Action::Retry', '>= 0.24';
requires 'Cache::FastMmap' , '>= 1.40';
requires 'Capture::Tiny' , '>= 0.42';
requires 'CHI', '>= 0.60';
requires 'CHI::Driver::DBI';
requires 'CHI::Driver::Redis' , '>= 0.10';
requires 'Compress::Zlib', '>= 2.074';
requires 'DBI', '>=1.63';
requires 'DBD::mysql', '>= 4.041';
requires 'DBIx::Class', '>= 0.08250';
requires 'DBIx::Class::Fixtures';
requires 'DBIx::Class::Migration', '>= 0.044';
requires 'DBIx::Class::TimeStamp';
requires 'DBIx::Class::Wrapper', '>=0.007';
requires 'DBIx::Class::InflateColumn::Serializer' , '>= 0.08';
requires 'Digest::MD5', '>= 2.51';
requires 'Digest::SHA1';
requires 'Digest::SHA';
requires 'Email::Valid', '>= 1.196';
requires 'Encode', '>= 2.88';
requires 'Exporter::Declare';
requires 'File::ShareDir';
requires 'File::Spec', '>= 3.47';
requires 'Filesys::Df' , '>= 0.92';
requires 'Filesys::Notify::Simple';
requires 'File::Touch' , '>= 0.09';
requires 'Geo::Coder::Google';
requires 'Getopt::Long', '>=2.43';
requires 'HTML::Entities';
requires 'Hash::MD5', '>= 0.08';
requires 'IO::Interactive' , '>= 0.0.6';
requires 'JSON';
## See https://rt.cpan.org/Public/Bug/Display.html?id=112960
requires 'JSON::XS', '==2.34';
requires 'Clownfish', '>=0.4.2';
requires 'Language::LispPerl', '>= 0.006';
requires 'Lucy' , '>=0.4.2';
requires 'MIME::Base64::URLSafe';
requires 'Mail::RFC822::Address';
requires 'Module::CPANfile' , '>= 1.0002'; # Missing in the new Hijk for the new Search::Elasticsearch
requires 'Mojolicious', '== 6.60' ; # introduced around_action hook # pinned to 6.60 as 7 was released see SEAR-1645
requires 'Mojolicious::Plugin::TtRenderer';
requires 'Mojolicious::Plugin::BasicAuth';
requires 'Mojolicious::Plugin::Sessions3S', '>= 0.004';
requires 'Net::Twitter', '>= 4.01041';
requires 'RRD::Editor';
requires 'Search::Tools' , '>= 1.003';
requires 'String::Truncate' , '>= 1.100602';
requires 'String::Snip' , '>= 0.002';
requires 'Scalar::Util', '>=1.42';
requires 'Schedule::LongSteps' , '>= 0.014';
requires 'Scope::Guard', '>=0.21';
requires 'Template';
requires 'Template::Plugin::JSON', '>= 0.06';
requires 'Test::Most', 0;
requires 'Text::CSV', '>= 1.32';
requires 'Try::Tiny';
requires 'YAML', 0;
requires 'Text::CSV', 0;
requires 'Text::Aspell';
requires 'Text::Autoformat';
requires 'URI::Template';
requires 'Array::Utils';
# This does not install on mac nor on the
# jenkins boxed :/ Neutering for now.
# requires 'Bean::Mojolicious::SAML2', '>= 0.002';

requires 'Moo', '>= 2.000001';

requires    'LWP::UserAgent', '>= 5.827'; # need fix for [RT#17208]
requires    'WWW::Mechanize', '>= 1.29_01'; # older versions of WWW::Mechanize die if you change $0 (due to the way it taints data)
#requires    'Time::HiRes';
requires    'HTTP::Message' , '>= 6.11';
#requires    'Digest::HMAC_SHA1';      # needed for Linked In authentication
requires    'Crypt::Blowfish';        # used by Stream::Accounts::Security
#requires    'HTTP::BrowserDetect';
#requires    'XML::LibXSLT';
#requires    'String::CRC32';
#requires    'SQL::Abstract', '>= 1.52';    # first version that supports order_by => { -asc => ... }
#requires    'SQL::Abstract::Limit';
#requires    'HTTP::Lite';
#requires    'ElasticSearch';
requires     'Excel::Writer::XLSX', '>= 0.85';
#requires    'BSD::Resource';
requires     'DateTime::Format::MySQL';
#requires    'DateTime::Format::ISO8601';
requires     'DateTime', '>=1.0';
#requires    'Mozilla::CA';
#requires    'MIME::Base64';
#requires    'Crypt::DES_EDE3';
requires     'Crypt::JWT' , '>= 0.011';
requires    'Crypt::OpenSSL::RSA';
requires     'Crypt::OpenSSL::AES';
requires    'Gearman::Client', '>= 1.10';   # 1.09 does not handle large tasks packets correctly
#requires    'Term::ProgressBar';
requires    'Term::ANSIColor';
#requires    'Devel::StackTrace'; # used by Bean::HopToad::Notifier
#requires    'Net::IP';
requires    'XML::Simple'; #save_config_xml in RuleMapper::ConfigHandler
#requires    'Crypt::Rijndael'; #used by MyTemplate 
requires     'EV';
requires     'Promises', '>= 0.04';
requires     'Log::Any' , '>=1.03';
requires     'Log::Any::Adapter::Log4perl' , '>=0.07';
requires     'Log::Contextual';
requires     'Log::Log4perl';
#
# We need to pin that to 0.09, until
# https://github.com/getsentry/perl-raven/pull/9 is merged.
#
# I blame the internet.
#
requires     'Sentry::Raven' , '== 0.09';
requires     'Log::Log4perl::Appender::Raven' , '>= 0.005';
requires     'Log::Log4perl::Appender::Chunk' , '>= 0.012';
requires     'Log::Log4perl::Appender::Chunk::Store::S3';
requires     'Search::Elasticsearch' , '>= 1.20';
requires     'Search::Elasticsearch::Async' , '>= 1.20';

## Deprecated dependencies?
##requires    'HTML::TreeBuilder::LibXML'; # required strict html

### Stream (frontend + backend)
requires    'List::MoreUtils';
requires    'Class::Accessor::Fast';
##requires    'TokyoTyrant';

### Stream frontend
#requires 'Plack::Middleware::NoMultipleSlashes';

### Stream backend
## Stream::TestBarrel
#requires    'Math::Combinatorics';
#requires    'HTML::TreeBuilder::XPath';
#requires    'Encode::Detect::Detector'; # use by Stream::Results::Candidate to detect plain text encoding from technojobs
#requires    'SOAP::Lite'; # Stream::FeedBuilder::RBI
#requires    'MIME::Lite';
requires "Stream::Keywords";
requires "Stream::Scraper";
#requires "Stream::Templates";
#requires "Stream::TemplateLoader";
requires "String::CamelCase";

## Stream2::Results::Store
requires 'Data::UUID';
requires 'Data::Page';
requires 'Moose', '>=2.1205';

## Stream::Control
#requires    'Proc::ProcessTable'; #used by Bean::CVSourcers::Base::Debugger

## Stream::Constants - TODO, remove this requirement, no benefit in being readonly
#requires    'Readonly';
#requires    'Readonly::XS';

## Stream::Accounts
requires    'Crypt::CBC', '>= 2.30'; # 2.28 encoded strings cannot be decoded by 2.30

## Stream::Templates::
requires 'DateTime::Format::ISO8601';
requires 'DateTime::Format::Strptime', '>= 1.0800';
requires 'DateTime::Format::DateParse';
requires 'LWP::Protocol::https';
requires 'Crypt::SSLeay';

requires 'XML::LibXSLT', '== 1.78'; # 1.79 and beyond require LibXSLT 1.1.28+ and ubuntu only ships with 1.1.26
requires 'Bean::XML';
requires 'Bean::HTMLStrip', 0;
requires 'HTML::Strip', 0;
requires 'Clone', 0;

#Stream::FeedCrawler::Crawler
requires 'Path::Tiny', '>= 0.088';


# Stream2 backend
#requires 'Stream2::Role';
#requires 'Stream2::Queue', '>= 0.002';
requires 'Qurious', '>= 0.033';
requires 'Bean::AdcApi::Client', '>= 0.04';
requires 'Bean::OAuth::Client', '>= 0.007';
requires 'Bean::OAuth::CareerBuilder', '>= 0.007';

#
# wondering why we depend on this? Well this is to avoid hard
# dependency on Sentry::Raven >= 1.00 which we cannot satisfy anyway
# See the story at https://bitbucket.org/broadbean/p5-bean-datastore/pull-requests/23/refactored-dependencies-a-little-bit-to/diff
#
requires 'Bean::DataStore', '>= 0.029';
requires 'Bean::PubSub', '>= 0.006';
requires 'Bean::TextExtractor::Client', '>= 0.012';
requires 'Bean::TSImport::Client', '>= 0.015';
requires 'Bean::CVP::Client', '>= 1.7';
requires 'Bean::Quota', '>=0.005';
# For the dummy feed.
requires 'Lingua::EN::Numbers';
requires 'XML::LibXML', '>= 2.0108';

# For some feeds:
requires 'Math::Round', '>= 0.06';


# for Docker
#requires 'Parallel::ForkManager';

### Messaging layer
#requires    'Log::Any::Adapter'; # used by new messaging layer

## Unclassified
#requires    'MIME::Lite::HTML';

### Needed to get Bean librares to work
## Bean::FileStore::S3
requires    'Net::Amazon::S3' , '>= 0.80';
requires    'Text::Unaccent'; # used by Bean::FileStore::S3 to remove accents from headers
requires    'Text::Levenshtein';
requires    'Tie::IxHash';
requires    'IO::WrapTie';

## Bean::AppContext
requires    'CGI::Session';

## Bean::Hacks
#requires    'Unicode::String';

## Bean::BeanConfig
requires    'MIME::Types' , '>= 2.13';
#

#### Bean libraries
#requires 'Stream::Assets';
requires 'Bean::Utils::Tiny';

requires 'Bean::AWS::S3FileFactory', '>= 0.003';

#Juice API
requires 'Bean::Juice::APIClient' , '>=0.095';
requires 'Bean::Juice::APIClient::Location';
requires 'Bean::Juice::APIClient::User';
requires 'Bean::GeoLocation::APIClient';
requires 'Bean::CareerBuilder::CandidateSearch', '>=0.053';
requires 'Bean::CareerBuilder::ParseNormalize', '>= 0.001';

requires 'Bean::AdvertService::Client', '>=0.009';
requires 'Bean::CareerBuilder::ParseNormalize', '>=0.001';

on 'develop' => sub {
    requires 'Bean::Stream2::JRipple', '>=0.015';
};

#### Stream::ElasticSearch
# requires 'ElasticSearch';
# requires 'HTTP::Lite';
# requires 'ElasticSearch::Transport::HTTPLite';

#### Testing
requires 'Class::Mockable';
test_requires 'App::Sqitch', '>= 0.9994';
test_requires 'Class::AutoAccess', '>= 0.03';
test_requires 'File::Which', '>= 1.21';
test_requires 'Test::MockObject';
test_requires 'Test::Mock::LWP::Dispatch';
test_requires 'Test::MockDateTime';
test_requires 'Module::Pluggable::Object', '>= 5.2';
test_requires 'File::Share';
test_requires 'LWP::UserAgent::Mockable';
test_requires 'Email::Valid';
test_requires 'Test::Perl::Critic';
test_requires 'Test::mysqld', '== 0.17';
test_requires 'IO::CaptureOutput';
recommends 'Devel::Cover';

#### Hacky redis script
requires 'File::pushd';

### Sending emails.
requires 'MIME::Entity';
requires 'Email::Abstract';
requires 'Email::Address';
requires 'Email::Sender';
requires 'Bean::EmailService::Client';
requires 'Bean::MailjetAPI::Client', '>= 0.004';
requires 'MIME::Base64';
requires 'Authen::SASL';
requires 'Net::SMTP::SSL';

requires 'Test::MockModule';
requires 'Test::Mock::Redis';
requires 'HTML::FormatText';
requires 'HTML::Scrubber';

#### For the shell
requires 'Pod::Text';
requires 'Devel::REPL';
requires 'Lexical::Persistence';
requires 'File::Next';

requires 'Parse::HTTP::UserAgent';

requires 'Locale::TextDomain';
requires 'Locale::PO';

# App::Stream2::Plugin::Utils
requires 'Data::Validate::IP' , '>= 0.27';

# This will break the built:
## requires 'Term::ReadLine::Perl';
# Look at installing Term::ReadLine::Gnu instead, that depends on  libncurses5-dev
#

on 'develop' => sub {
    requires 'Hash::Diff';
    requires 'Bean::LanguageDetector';
};

#!/bin/sh

# increase the number of files that brunch is allowed to open
# brunch opens up every file it watches so it needs a higher limit
ulimit -n 10000
export MOJO_CONFIG='app-stream2.vagrant.conf'

LOCAL_DIR="local"
if [ -d "$LOCAL_DIR" ]; then
    echo "exit" | perl -Mlocal::lib=local ./bin/redis
    (
        echo "perl -Mlocal::lib=local local/bin/morbo bin/app.pl -l http://*:3000 -l https://*:3001"
        echo "./local-node/bin/node ./node_modules/.bin/brunch watch"
        echo "perl -Mlocal::lib=local ./bin/app.pl worker"
    ) | xargs -P 3 -IX bash -c "X"
else
    # Run webserver and compass at the same time
    echo "exit" | perl ./bin/redis
    (
      echo "morbo bin/app.pl -l http://*:3000 -l https://*:3001"
      echo "./local-node/bin/node ./node_modules/.bin/brunch watch"
      echo "perl ./bin/app.pl worker"
    ) | xargs -P 3 -IX bash -c "X"
    # -P 3 = xargs in parallel mode, max 3 processes
fi

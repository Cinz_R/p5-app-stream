use strict;
use warnings;

# TODO: consider using Test::Perl::Critic::Progressive instead. Running
# Perl::Critic on a large codebase that doesn't necessarily comply with
# coding standards generates a vast amount of warnings.
# Test::Perl::Critic::Progressive reports errors more gradually (kindly!?)

use Test::Perl::Critic
    -severity => 3,    # 1 to 5 (1 = brutal, 5 = gentle)
    -verbose  => 9;

all_critic_ok( qw/lib bin dev-bin t/ );

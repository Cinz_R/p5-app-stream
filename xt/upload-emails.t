use FindBin;
use lib "$FindBin::Bin/../lib";
use feature qw/state/;

use Bean::AWS::S3FileFactory;
use Getopt::Long::Descriptive;
use JSON qw/decode_json/;
use MIME::Parser;
use Mojo::UserAgent;
use Stream2;
use Test::Most;

=head1 SYNOPSIS

prove needs '::' to differentiate the test file's args from its own

    $ prove -lv xt/upload-emails.t :: --server=localhost:3002 \
        --config=app-stream2.mhawkes.conf --num-emails=50

=head1 DESCRIPTION

This test file aims to prove that all emails in the cvsearch.sent_email DB
table exist in the remote Email Service. (I.e. it aims to prove that
F<bin/upload-emails-to-email-api.pl> has done its job properly.) How?

=over 4

=item 1. Comparing Quantities
Count all emails per day. The same daily totals should exist in the email
service.

=item 2. Comparing Content within N Randomly Selected Messages
Randomly select N emails. Ensure they exist in the email server with the same
headers, body and meta data.

=back

=head2 Developer Notes About Comparing Emails

Almost all emails in Search have a single recipient. There are 2 that don't.
We can find them by searching for recipient fields that contain a comma or
semicolon (email address separators).

    SELECT id, recipient_name, recipient
    FROM sent_email
    WHERE CONCAT(recipient_name, recipient) RLIKE '[,;]';

Note that no recipient_name fields contain an email address (containing '@')

    SELECT innerT.id, innerT.recipient_name FROM (
      SELECT id, recipient_name, recipient
      FROM sent_email
      WHERE CONCAT(recipient_name, recipient) RLIKE '[,;]'
    ) innerT
    WHERE INSTR( innerT.recipient_name, '@' ) >= 1;
    Empty set

=cut

my ( $email_server, $search_config_file, $num_email_comparisons )
    = get_command_line_args();

my $EMAIL_SERVICE_KEY  = 'Y66Tg7wqW9yuXEn4JXw7ey1iHsuiHGG7H2o99fhwjdzy768H'
                       . 'ZzO0jdVb44uItsF8uGH9ZlQmMftvBo4j9twa5zHlq19k420l'
                       . 'P21rklr3YXyv72dVdD46s7Nbca4QmLt3';

# Get counts of emails per day on local and remote sides. They must match.
my $s2          = Stream2->new( config_file => $search_config_file );
my $sent_emails = $s2->factory('SentEmail');

# SELECT DATE(sent_date), COUNT(sent_date)
# FROM sent_email
# GROUP BY DATE(sent_date)
my $rs = $sent_emails->search(
    {},
    {
        select   => [ { DATE => 'sent_date' }, { COUNT => 'sent_date' } ],
        as       => [ 'date', 'total' ],
        group_by => [ { DATE => 'sent_date' } ]
    }
);

# UA for fetching emails from Email Service. Every GET request will include
# the specified Authorization header.
my $ua = Mojo::UserAgent->new;
$ua->on( start => sub {
    my ($ua, $tx) = @_;
    $tx->req->headers->header( Authorization => $EMAIL_SERVICE_KEY );
});
my $body          = $ua->get("$email_server/emails/count")->res->body;
my $remote_emails = decode_json( $body );
while ( my $row = $rs->next ) {
    my $date  = $row->get_column('date');
    my $total = $row->get_column('total');
    ok $total == $remote_emails->{response}->{$date},
        "number of emails match for $date";
}

# Randomly grab some local emails. Ensure they exist correctly in the Email
# Service.
for ( 1 .. $num_email_comparisons ) {
    my $local_email  = get_random_local_email( $sent_emails );
    my $email_id     = $local_email->id;
    my $remote_body  = $ua->get("$email_server/emails/external/"
                     . "stream2/$email_id")->res->body;
    my $remote_email = decode_json( $remote_body );
       $remote_email = $remote_email->{response}->[0];

    ok is_local_email_equal_to_remote_email(
        $local_email, $remote_email, $s2->config->{aws} ),
        "Local email ID $email_id exists correctly in the email service";
}

done_testing();


### Subroutines

# returns true if various fields of $local_email and $remote_email match.
# $local_email->isa('Stream2::O::SentEmail'). $remote_email is a hashref.
# $config is a hashref representing Search2's AWS config.
sub is_local_email_equal_to_remote_email {
    my ($local_email, $remote_email, $config) = @_;

    state $ua         = Mojo::UserAgent->new;
    state $s3_factory = Bean::AWS::S3FileFactory->new( config => {
        aws_access_key_id     => $config->{aws_access_key_id},
        aws_secret_access_key => $config->{aws_secret_access_key},
        retry                 => $config->{private_file_s3}->{retry},
        secure                => $config->{private_file_s3}->{secure},
        bucket_name           => $config->{private_file_s3}->{bucket_name},
        acl_short             => $config->{private_file_s3}->{acl_short},
        location_constraint   => $config->{private_file_s3}
                                        ->{location_constraint},
        host                  => $config->{private_file_s3}->{host},
        key_prefix            => '',
        encryption            => $config->{private_file_s3}->{encryption},
    } );
    
    # compare Subject field value. First remove any "[placeholder]" or
    # "<a href..." markup at the end as Search uploads emails to the Email
    # Service before placeholders are converted to HTML
    state $placeholder_pattern = qr/\s*\[[^\]]*\]\s*$/;
    state $html_pattern        = qr/\s*<a.*>\s*$/;
    my $local_subject          = $local_email->subject;
    my $remote_subject         = $remote_email->{subject};
    $local_subject  =~ s/$_// for ( $placeholder_pattern, $html_pattern );
    $remote_subject =~ s/$_// for ( $placeholder_pattern, $html_pattern );
    my $subjects_match = $local_subject eq $remote_subject;

    my $dates_match = $local_email->sent_date eq $remote_email->{date};

    # Search2 emails only store the To recipient
    my $lr                = $local_email->recipient;
    my $recipient_matches = $remote_email->{recipientsTo}->[0] =~ /<$lr>/;

    # compare contents of local and remote email bodies as stored in S3
    my $le_url
        = $s3_factory->authenticated_uri( $local_email->aws_s3_key, 5 );
    my $re_url
        = $s3_factory->authenticated_uri( $remote_email->{awsS3Key}, 5 );
    
    my $parser = MIME::Parser->new;
    $parser->output_to_core(1);
    $parser->tmp_to_core(1);

    # $le_url->isa('URI::http'): must stringify it else crashes :-/
    my $le_contents    = $ua->get( $le_url . '' )->res->body;
    my $re_contents    = $ua->get( $re_url . '' )->res->body;
    my $local_me       = $parser->parse_data( $le_contents );  # MIME::Entity
    my $remote_me      = $parser->parse_data( $re_contents );
    my $s3_files_match =
        $local_me->stringify_body eq $remote_me->stringify_body;
    
    return $subjects_match && $dates_match && $recipient_matches
        && $s3_files_match;
}

# returns an instance of Stream2::O::SentEmail representing an email randomly
# chosen from the cvsearch.sent_email table
sub get_random_local_email {
    my $sent_emails = shift;

    # SELECT MAX(id) FROM sent_email
    state $max_id   = do {
        my $res = $sent_emails->search(
            {},
            {
                select => [ { MAX => 'id' } ],
                as     => [ 'max_id' ],
                rows => 1,
            }
        );
        my $max_id = $res->single->get_column('max_id');   # or first()?
    };

    # loop in case there are gaps in the sent_email table's AUTO_INCREMENT
    # sequence and $random_id matches nothing
    my $random_email;
    until ( $random_email ) {

        # generate a random integer from the inclusive range 1 to $max_id
        my $random_id = ( int rand $max_id ) + 1;
        $random_email = $sent_emails->find( $random_id );
    }
    return $random_email;
}

sub get_command_line_args {
    my ($options, $usage) = describe_options(
        "%c %o",
        [],
        ['Verifies that all emails in Search2 exist in the email service. '],
        ['Additionally, with --num-emails=n checks the contents of n '],
        ['randomly selected emails to ensure they\'re the same.'],
        [],
        [
            'server|s=s',
            'hostname:port of email service',
            { required => 1, default => 'emailservice.adcourier.com:80' },
        ],
        [
            'config|c=s',
            'Search2 config file to use for DB connection',
            { required => 1, default => 'app-stream2.conf' },
        ],
        [
            'num-emails|n=i',
            'Number of emails to compare',
            { required => 0, default => 1000 },
        ],
        [ 'help|h', 'Print usage message and exit' ],
        [],
    );

    if ( $options->help ) {
        warn $usage->text;
        BAIL_OUT('Help option for command line usage invoked');
        return undef;
    }
    
    my $server = 'http://' . $options->server;
    return ( $server, $options->config, $options->num_emails );
}
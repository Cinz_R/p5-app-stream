use Test::Most;

use lib q{./bin/lib};
use ImportEmailTemplates;
use Test::Stream2;
use Test::MockModule;

my $config_file = './t/app-stream2.conf';
our $s2 = Test::Stream2->new({ config_file => $config_file });
$s2->deploy_test_db;

subtest "_create_user_map" => sub {
    my $sync_mock = Test::MockModule->new('Stream2::Action::SynchroniseAdCourierUserSiblings');
    $sync_mock->mock( instance_perform => sub { 1; } );

    my $user_args = {
        provider => 'whatever',
        last_login_provider => 'whatever',
        group_identity => 'company1',
        settings => {}
    };

    my $user_factory = $s2->factory('SearchUser');
    do {
        my $user = $user_factory->new_result({
            %$user_args,
            %$_
        });
        $user->save();
    } for (
        {
            provider_id => 1,
            contact_details => {
                company => 'company1',
                office => 'office22',
                team => 'teamwhatever',
                consultant => 'useruser'
            }
        },
        {
            provider_id => 2,
            contact_details => {
                company => 'company1',
                office => 'office1',
                team => 'teamwhatever2222',
                consultant => 'con1'
            }
        },
        {
            provider_id => 3,
            contact_details => {
                company => 'company1',
                office => 'office1',
                team => 'teamwhatever2222'
            }
        },
        {
            provider_id => 4,
            contact_details => {
                company => 'company1',
                office => 'office1',
                team => 'teamwhatever2222',
                consultant => 'con2'
            }
        }
    );

    my $expected = {
        '/company1/office22/teamwhatever/useruser' => 1,
        '/company1/office1/teamwhatever2222/con1' => 2,
        '/company1/office1/teamwhatever2222/con2' => 4
    };

    my $importer = ImportEmailTemplates->new({ s2 => $s2, company => 'company1', type => 'candidate_response' });

    is_deeply(
        $importer->user_map,
        $expected
    );
};

subtest "_vivify_sub_identifiers" => sub {

    my $user_map = {
        '/company7/office12/team1/user6' => 1,
        '/company8/office12/team1/user6' => 1,
        '/company8/office12/team1/user5' => 1,
        '/company8/office12/team1/user4' => 1,
        '/company8/office12/team1/user3' => 1,
        '/company8/office12/team1/user2' => 1,
        '/company8/office12/team1/user1' => 1,
        '/company8/office12/team2/user6' => 1,
        '/company8/office12/team2/user5' => 1,
        '/company8/office12/team2/user4' => 1,
        '/company8/office12/team2/user3' => 1,
        '/company8/office11/team1/user6' => 1,
        '/company8/office11/team1/user5' => 1,
        '/company8/office11/team1/user4' => 1,
        '/company8/office11/team1/user3' => 1,
        '/company8/office11/team1/user2' => 1,
        '/company8/office11/team1/user1' => 1,
    };

    my $tests_ref = [
        {
            input => {
                company => 'company8',
                office => 'office12'
            },
            expected => [
                '/company8/office12/team1/user6',
                '/company8/office12/team1/user5',
                '/company8/office12/team1/user4',
                '/company8/office12/team1/user3',
                '/company8/office12/team1/user2',
                '/company8/office12/team1/user1',
                '/company8/office12/team2/user6',
                '/company8/office12/team2/user5',
                '/company8/office12/team2/user4',
                '/company8/office12/team2/user3'
            ]
        },
        {
            input => {
                company => 'company8',
                office => 'office12',
                team => 'team2'
            },
            expected => [
                '/company8/office12/team2/user6',
                '/company8/office12/team2/user5',
                '/company8/office12/team2/user4',
                '/company8/office12/team2/user3'
            ]
        },
        {
            input => {
                company => 'company8',
                office => 'office12',
                team => 'team2',
                consultant => 'user5'
            },
            expected => [
                '/company8/office12/team2/user5'
            ]
        }
    ];

    plan tests => scalar( @$tests_ref );

    my $importer = ImportEmailTemplates->new({ s2 => $s2, company => 'company8', type => 'candidate_response', user_map => $user_map });
    foreach my $test ( @$tests_ref ){
        is_deeply(
            [sort($importer->_vivify_sub_identifiers($test->{input}))],
            [sort(@{$test->{expected}})]
        );
    }
};


subtest "fix_up_template" => sub {

    my $test_templates = [
        {
            name=> 'fix up Candidate Name',
            body =>
                q|Dear [Candidate Name],
                  Tilde squid pabst, ethical gentrify thundercats cronut
                  literally 8-bit XOXO hexagon. Lomo [jobtitle] beard umami,
                  taxidermy cray (reference:[jobref]). Literally keytar photo booth
                  bespoke gastropub activated charcoal quinoa mixtape,
                  drinking vinegar knausgaard fap snackwave gentrify
                  dreamcatcher tilde.
                  Best regards
                  [contactname]|,
            expected => qr(candidatename)
        },
        {
            name=> 'dont fix up candidate name',
            body =>
                q|Dear [candidate name],
                  Tilde squid pabst, ethical gentrify thundercats cronut
                  literally 8-bit XOXO hexagon. Lomo [jobtitle] beard umami,
                  taxidermy cray (reference:[jobref]). Literally keytar photo booth
                  bespoke gastropub activated charcoal quinoa mixtape,
                  drinking vinegar knausgaard fap snackwave gentrify
                  dreamcatcher tilde.
                  Best regards
                  [contactname]|,
            expected => qr(candidate\sname)
        },
    ];

    plan tests => scalar( @$test_templates );

    my $importer = ImportEmailTemplates->new({ s2 => $s2, company => 'company8', type => 'candidate_response' });
    for my $test_template (@$test_templates) {
        like($importer->fix_up_template($test_template)->{body}, $test_template->{expected}, $test_template->{name});
    }

};

done_testing();

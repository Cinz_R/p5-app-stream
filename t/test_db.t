#! perl -w

use Test::Most;

# use Log::Any::Adapter qw/Stderr/;
use Test::Stream2;

my $config_file = 't/app-stream2.conf';

ok(
    my $stream2 = Test::Stream2->new(
        {
            config_file           => $config_file,
            # rebuild_test_db_files => 1
        }
    ),
    "Ok can build stream2"
);

# NOTE: there is no deploy here, this is just here to make sure the test db we
# deplyed is all correct.

# REBULDING THE TEST DB
# if you need to refresh, uncomment out the line rebuild_test_db_files, and
# rerun this test. (dont forget to re comment the line).

# HARD REBULDING THE TEST DB
# if you have done something really drastic, delete the folder t/mysql folder
# in the share dir, then follow te rebuilding instuctions.

lives_ok(
    sub { $stream2->developer()->run_sqitch( ['verify'] ) },
    'the db should verify correctly, if not you may need to update it.'
);

done_testing();

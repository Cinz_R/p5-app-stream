use strict;

use FindBin;
use Test::More tests => 1;

use lib "$FindBin::RealBin/lib";

use TestAutoId;

my $dummy = TestAutoId->new();
ok($dummy->id(), "Generated an id");

use strict;
use Test::More tests => 8;

use_ok('Stream2::Role::Stringable');

{
  package SuperClass;
  use Moose;

  has 'inherited' => (
    is => 'ro',
  );
};

{
  package SubClass;
  use Moose;

  extends 'SuperClass';
  with 'Stream2::Role::Stringable';

  has 'public' => (
    is => 'ro',
  );

  has '_private' => (
    is => 'ro',
  );

  has '_traited' => (
    is => 'ro',
    traits => ['Stream2::Trait::Stringable'],
    stringable => 1,
  );

  has 'traited' => (
    is => 'ro',
    traits => ['Stream2::Trait::Stringable'],
    stringable => 0,
  );
};

my $instance = SubClass->new(
  inherited => 'inherited from parent',
  public    => 'stringable by default',
  _private  => 'not stringable by default',
  _traited  => 'stringable through trait',
  traited   => 'not stringable through trait',
);

my $data_str = $instance->serialise();
ok( $data_str, "Data serialised");
ok( !ref($data_str), "... into a string");

my $data_ref = SubClass->deserialise($data_str);
is( $data_ref->{inherited}, 'inherited from parent', 'Serialised inherited attribute' );
is( $data_ref->{public},    'stringable by default', 'Public methods are serialised by default');
ok( !exists($data_ref->{traited}), '... unless the trait says not to');
ok( !$data_ref->{_private}, 'Private methods are not serialised by default');
is( $data_ref->{_traited},  'stringable through trait', '... unless the trait says it is');

#! perl

use Test::More;
use File::Temp;
use Path::Class ();
use JSON ();

my ( $fh, $filename ) = File::Temp::tempfile();
my $command =  './local-node/bin/node ./node_modules/.bin/phantomjs ./vendor/phantomjs-qunit-runner/runner.js qunit/index.html --json --output ' . $filename;
diag("Doing: $command");
my $exitcode = system( $command );

ok( ! $exitcode, "Frontend tests completed without failure" );

my $test_ref = JSON::decode_json( Path::Class::file( $filename )->slurp() );
foreach my $module ( @{$test_ref->{modules}} ){
    foreach my $test ( @{$module->{tests}} ) {
        subtest $module->{name}." - ".$test->{name} => sub {
            plan tests => $test->{total};
            foreach my $assertion ( @{$test->{assertions}} ){
                ok( $assertion->{result}, $assertion->{message} );
            }
        };
    }
}

done_testing();

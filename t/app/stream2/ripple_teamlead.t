#! perl -w
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use strict;
use warnings;

use utf8;

use Test::More;
use Test::Mojo::Stream2;
use Mojo::URL;
use Promises;
use JSON;

use Encode;

my $t = Test::Mojo::Stream2->new();
$t->ua->on(start => sub {
    my ($ua, $tx) = @_;
    $tx->req->headers->accept('text/xml');
});

# V2 request, with signature instead of password
{
    ## Note that we need to do some mockery of $self->auth_provider({ provider => 'adcourier_ats'})->oauth_client->credentials_grant( %$grant_params );
    no warnings;
    local *Bean::OAuth::AsyncClient::credentials_grant = sub{
        my ($self, %params) = @_;
        is( $params{signature} , 'boudinblanc', "Ok Good signature goes through");
        is( $params{time_ms}, 'Whatever',  "Ok good time goes through");
        my $d = Promises::deferred;
        $d->resolve({
                'expires_in' => 43200,
                'user' => {'user_role' => 'TEAMLEADER',
                           'user_id' => '209669',
                           'read_contact_details' => {
                                                      'contact_telephone' => '1234567894',
                                                      'contact_email' => 'jerome@broadbean.com',
                                                      'contact_name' => 'Search Demo',
                                                      'contact_devuser' => undef,
                                                      'company' => 'andy'
                                                     },
                           'read_navigation_tabs' => [],
                           'read_stream_subscriptions' => {'talentsearch' => {'nice_name' => 'Talent Search',
                                                                              'auth_tokens' => {'talentsearch_security_key' => 'a9ad6247-8d14-4df9-8364-3de9ff613c5a'},
                                                                              'type' => 'internal',
                                                                              'user_role' => 'USER',
                                                                              'user_id' => '209669'
                                                                             },
                                                          }
                          },
                'access_token' => '26E42650-3356-11E4-8CB0-F98427886EDC',
                'token_type' => 'Bearer'
               });
        return $d->promise;
    };
    use warnings;


    my $response = $t->post_ok('/ripple',Encode::encode_utf8(q|<?xml version="1.0" encoding="UTF-8" ?>
<CV_Search_Query>
  <Version>1.0.0-r118179</Version>
  <APIKey>1097103005</APIKey>
  <Account>
    <UserName>searchdemo@users.live.andy</UserName>
    <Signature>
       <Hash>boudinblanc</Hash>
       <Time>Whatever</Time>
    </Signature>
  </Account>

  <Stream_Query>
    <Keywords>developer</Keywords>
  </Stream_Query>
  <ChannelList>
    <Channel>
      <ChannelId>talentsearch</ChannelId>
    </Channel>
  </ChannelList>
  <CustomFields>
    <CustomField name="custom1">field</CustomField>
    <CustomField name="custom2">field</CustomField>
  </CustomFields>


</CV_Search_Query>
|));

    $response->status_is(200);
    my ($url) =  ( $response->tx()->res()->content()->get_body_chunk(0) =~ /<StreamURL>(.+)<\/StreamURL>/ );
    ok( $url , "Ok got a redirect URL");

    my $user = $t->app->stream2->factory('SearchUser')
        ->search( { last_login_provider => 'adcourier_ats' } )->first;
    my $session = $t->get_session_from_STOK( $url );
    my $custom_fields = $session->{ripple_settings}->{custom_fields};
    ok @{$custom_fields} == 2, 'custom fields present in old Ripple version';

    is(  scalar( keys( $session->{ripple_settings} ) ), 4 , "Ok good number of keys (includes custom_fields)" );
    ok(  $session->{ripple_settings}->{login_provider} eq 'adcourier_ats' );
    ok(  $session->{ripple_settings}->{custom_lists} );
    ok(  @{$session->{ripple_settings}->{custom_lists}} == 1  );

    # Check we can hit the redirect URL
    {
        my $mojo_url = Mojo::URL->new($url);
        my $r = $t->get_ok($mojo_url->path()->to_string().'?'.$mojo_url->query());
        $t->status_is(302);
        $t->header_like( Location => qr/\/welcome-admin/ ); # Ok got redirect to the welcome-admin page.

        # my $search_url =  Mojo::URL->new(scalar($t->tx->res->headers()->header('Location')));
        # $t->get_ok($search_url->path->to_string());
        # ## Check we can call the route that gives the nipple settings.
        # $t->get_ok('/api/user/custom_lists', "Custom shortlists GET");
        # $t->content_like(qr/Shortlist/, "Those custom list are shortlists"); 
    }
}

LWP::UserAgent::Mockable->finished();
done_testing();

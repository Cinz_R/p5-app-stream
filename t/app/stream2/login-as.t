use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;

### Tests for the /login/as/123 route

my $t = Test::Mojo::Stream2->new();

# override $t->login_ok: specify "adcourier", instead of "demo", as provider
$t->post_ok( '/login', form => {
    provider => 'adcourier',
    username => 'doesnt matter',
    password => 'doesnt matter'
} );

# The last user returned isn't inherently a zombie. It's a zombie only because
# its provider_id matches that of a user which
# Bean::Juice::APIClient::User::oversees() returns whose office is "zombie"
my ( $admin_user, $normal_user, $zombie_user )
    = create_users( $t->app->stream2->factory('SearchUser') );


### Mock some infrastructure...

my $auth_mock = Test::MockModule->new('App::Stream2::Plugin::Auth');
$auth_mock->mock( current_user => sub {
    return $admin_user;
} );

my $juice_mock = Test::MockModule->new('Bean::Juice::APIClient::User');
$juice_mock->mock( oversees => sub {
    return [
        {
            office     => 'treetops',
            user_role  => 'USER',
            consultant => 'Pedro',
            id         => $normal_user->provider_id,
            team       => 'testicule',
            company    => 'monkey'
        },
        {
            office     => 'zombie',
            user_role  => 'USER',
            consultant => 'Pablo',
            id         => $zombie_user->provider_id,
            team       => 'testicule',
            company    => 'monkey'
        }
    ];
});


### The assertions...

# note "Switching to a normal user is fine";
# $t->get_ok( '/login/as/' . $normal_user->provider_id )
#   ->status_is( 302 )
#   ->header_is( Location => '/search' );

# "/login/as" should eally return a 4xx or 5xx code, not 200!
# note "Switching to a zombie user is forbidden";
# $t->get_ok( '/login/as/' . $zombie_user->provider_id )->status_is(200);
# $t->content_like( qr/User Unavailable/, "Zombie user cannot /search" );

# $t->reset_session;
# $t->get_ok("/?manage_responses_view=1");
# $t->app->sessions->storage->list_sessions->[0]->{manage_responses_view} = 1;
# $t->get_ok( '/login/as/' . $zombie_user->provider_id )->status_is(302);
# $t->content_like( qr/monkeycuck/ );

done_testing();


### Supporting subroutines

sub create_users {
    my $users_factory = shift;

    my @users;
    for my $provider_id ( 1, 10, 11 ) {
        my $user = $users_factory->new_result({
            contact_details     => {
                contact_name  => "doesnt matter",
                contact_email => "doesnt matter"
            },
            provider            => 'adcourier',
            last_login_provider => 'adcourier',
            provider_id         => $provider_id,
            group_identity      => 'monkey',
            group_nice_name     => 'Monkey Monkaye',
            settings            => {}
        });
        $user->save;
        push @users, $user;
    }

    # make the 1st user an admin user. This probably isn't even necessary
    $users[0]->is_admin(1);
    $users[0]->is_overseer(1);

    return @users;
}
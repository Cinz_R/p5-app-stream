#! perl -w

use Test::Most tests => 5;
use Test::Mojo::Stream2;

sub test_app { return Test::Mojo::Stream2->new(); }

subtest "Login page displayed visiting /login" => sub {
  my $t = test_app();
  $t->get_ok('/login')
    ->status_is(200)
    ->element_exists('input[name="username"]')
    ->element_exists('input[name="password"]')
    ->header_like('P3P' => qr/policyref/ , "Ok got P3P header") ;
};

subtest "Given the correct credentials, we are taken to the search page" => sub {
  my $t = test_app();
  $t->ua->max_redirects(0);
  $t->post_ok('/login', form => { provider => 'demo',  username => $t->username, password => $t->password })
    ->status_is(302)
    ->element_exists_not('input[name="username"]'); # they should not see this field
};

subtest "Remain on login page if our credentials are incorrect" => sub {
  my $t = test_app();
  $t->ua->max_redirects(1);
  $t->post_ok('/login', form => { provider => 'demo', username => "wrong username" })
    ->status_is(200)
    ->element_exists('input[name="username"]'); # they should still be on the login page
};

subtest "Visit a page without logging in" => sub {
  my $t = test_app();

  # No redirect please.
  $t->ua->max_redirects(0);

  # Accessing a protected page should take us to the login page
  $t->get_ok('/search')->status_is( 302 )->header_like( Location => qr/oauth\.adcourier\.com/ );

  # Then login (that will use the demo provider)
  # and we should be taken back to our original page
  # (without losing the original information?)

  # Accept redirects, because the login will be successful
  # and redirect to the right page.
  $t->ua->max_redirects(1);
  $t->post_ok('/login', form => { provider => 'demo',  username => $t->username, password => $t->password });
  like($t->tx->req->url(), qr{/search$});
};

subtest "Login and then visit a page" => sub {
  my $t = test_app();

  $t->post_ok('/login', form => { provider => 'demo', username => $t->username, password => $t->password })
    ->get_ok('/search')
    ->status_is(200);
};

done_testing();

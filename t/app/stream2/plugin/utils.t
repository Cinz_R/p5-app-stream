#!/usr/bin/env perl
use strict;
use warnings;

use Test::More;
use Test::Exception;
use Test::Mojo::Stream2;
use Mojo::Base;
use Mojolicious::Lite;
use Data::Dumper;

use Mojo::Headers;
use Mojo::Transaction::HTTP;

my $utils_plugin_class = "App::Stream2::Plugin::Utils";

{
    use_ok($utils_plugin_class) or BAIL_OUT('Could not load $utils_plugin_class');
}

my $t = Test::Mojo::Stream2->new();

my $app = $t->app();
my $config = $app->config();

my $headers = Mojo::Headers->new();
my @ip_tests = (
    {
        header => [ 'X-Real-IP' => '8.8.8.8' ],
        expected_ip => '8.8.8.8',
        message => 'Uses IP from X-Real-IP header',
    },
    {
        expected_ip => '1.1.1.1',
        message => 'No Headers',
    },
    {
        header => [ 'Forwarded' => 'for=9.2.4.12' ],
        expected_ip => '9.2.4.12',
        message => 'Uses IP from Forwarded header',
    },
    {
        header => [ 'Forwarded' => 'for=13.13.12.12, for=24.11.94.32' ],
        expected_ip => '13.13.12.12',
        message => 'Use first IP from Forwarded header',
    },
    {
        header => [ 'X-Forwarded-For' => '1.2.3.4' ],
        expected_ip => '1.2.3.4',
        message => 'Uses IP from X-Forwarded-For header',
    },
    {
        header => [ 'X-Forwarded-For' => '42.62.95.123, 4.4.4.4' ],
        expected_ip => '42.62.95.123',
        message => 'Uses IP from first X-Forwarded-For header',
    },
    {
        header => [ 'X-Real-IP' => '127.0.0.13' ],
        expected_ip => '1.1.1.1',
        message => 'Skips private IP from X-Real-IP header',
    },
    {
        header => [ 'X-Real-IP' => '10.1.2.3' ],
        expected_ip => '1.1.1.1',
        message => 'Skips private IP from X-Real-IP header',
    },
    {
        header => [ 'X-Real-IP' => '192.168.0.13' ],
        expected_ip => '1.1.1.1',
        message => 'Skips private IP from X-Real-IP header',
    },
);

foreach my $ip_test (@ip_tests) {
    my $tx = Mojo::Transaction::HTTP->new();

    $tx->req->headers->add( @{$ip_test->{header}} ) if $ip_test->{header};
    $tx->remote_address('1.1.1.1') unless $tx->remote_address;

    my $controller = $app->build_controller;
    $controller->tx($tx);

    is($controller->remote_ip, $ip_test->{expected_ip}, $ip_test->{message});
}

done_testing();


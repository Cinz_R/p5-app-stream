use Test::Most;
use Test::MockObject;
use App::Stream2::Plugin::Auth;

# Mock Stream2 application:
# just need an object with a translations() method that returns an object
# we can call supported_languages() on
my $stream2      = Test::MockObject->new;
my $translations = Test::MockObject->new;
$translations->mock( supported_languages => sub {
    [ qw/en fr fr_CA en_US/ ]
} );
$stream2->mock( translations => sub { $translations } );

my @tests = (
    { input => 'en',     expected_output => 'en' },
    { input => 'fr',     expected_output => 'fr' },
    { input => 'en_US',  expected_output => 'en_US' },
    { input => 'en_usa', expected_output => 'en_US' },
    { input => 'en_au',  expected_output => 'en' },
    { input => 'fr_CH',  expected_output => 'fr' },
    { input => 'zz',     expected_output => 'en' },
);

for my $test ( @tests ) {
    is App::Stream2::Plugin::Auth::_fix_locale( $stream2, $test->{input} ),
       $test->{expected_output}, 'got expected lang code resolution';
}

done_testing();

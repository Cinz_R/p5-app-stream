#! perl -w

use Test::More;

use_ok('App::Stream2::Plugin::Queue');

use Mojolicious::Controller;
use Mojo::IOLoop;

{
    package App::QueueTest;

    use Mojo::Base 'Mojolicious';
    has _queue => sub { return TestQueue->new; }
}

{
    package TestQueue;
    use Moose;
    use Data::Dumper;
    extends 'Qurious';
    has redis => ( is => "ro" );
    has jobs => ( is => "rw", isa => "HashRef", default => sub { return {} } );
    sub push {
        my $self = shift;
        my $j = shift;

        $j->perform;
        $j->complete_job( "OK" );

        $self->save_job( $j );
        return $j
    }
    sub load_job {
        my $self = shift;
        my $guid = shift;

        return $self->jobs->{$guid};
    }
    sub save_job { $_[0]->jobs->{$_[1]->guid} = $_[1]; }
    sub expire_job { 1 }
}

{
    package TestAction;
    sub perform {
        return "OK";
    }
}

my $app = App::QueueTest->new;
my $plugin = App::Stream2::Plugin::Queue->new();
$plugin->register( $app );

subtest "Queue job and manually poll" => sub {
    my $job = $app->queue_job (
        class => "TestAction",
        parameters => {
            a => "b"
        }
    );

    my $completed_job = $app->retrieve_job( $job->guid );
    is ( $completed_job->status, "completed", "We can queue jobs and check their status later" );
};

subtest "Do deferred job and wait" => sub {

    $app->do_job(
        class => "TestAction",
        parameters => {
            a => "b"
        }
    )->then(
        sub {
            my $j = shift;
            is( $j->result->{result}, "OK", "deferred job was returned succesfully" );
            Mojo::IOLoop->stop;
        }
    );

    Mojo::IOLoop->start unless Mojo::IOLoop->is_running; #blocking
};

subtest "Wait for a specific job to finish before continuing" => sub {

    my $job_1 = $app->_queue->create_job(
        class => "TestAction",
        parameters => {
            a => "b"
        }
    );

    is ( $job_1->status, "unstarted", "We have created a job and are yet to queue it" );

    $job_1->enqueue;

    $app->wait_job( $job_1 )->then(
        sub {
            my $j = shift;
            is( $j->result->{result}, "OK", "queue job then later, wait for it to finish" );
            Mojo::IOLoop->stop;
        }
    );

    Mojo::IOLoop->start unless Mojo::IOLoop->is_running; #blocking

};

subtest "Wait for a few jobs to finish before continuing" => sub {

    my $number_of_jobs = 5;
    my @unfinished_jobs = ();
    for (my $job_number = 0; $job_number < $number_of_jobs; $job_number++ ){
        my $job = $app->_queue->create_job(
            class => "TestAction",
            parameters => {
                a => "b",
                job_number => $job_number
            }
        );
        is ( $job->status, "unstarted", "We have created a job and are yet to queue it" );

        $job->enqueue;
        push @unfinished_jobs, $job;
    }

    my $loop_count_test = 0;
    Mojo::IOLoop->recurring( 0.1, sub {
        $loop_count_test++;
    });

    $app->wait_jobs( @unfinished_jobs )->then(
        sub {
            my @finished_jobs = @_;
            for (my $job_number = 0; $job_number < $number_of_jobs; $job_number++ ){
                my $job = $finished_jobs[$job_number];
                is ( $job->status, "completed", "The job was succesfully completed" );
                is ( $job->parameters->{job_number}, $job_number, "Job returned in the correct order" );
            }
            Mojo::IOLoop->stop;
        }
    );

    Mojo::IOLoop->start unless Mojo::IOLoop->is_running; #blocking

    ok ( $loop_count_test > 1, "Double check our deferred methods aren't blocking the process of other events" );

};

done_testing();

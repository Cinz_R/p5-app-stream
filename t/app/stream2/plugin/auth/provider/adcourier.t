use Test::Most;
use Mojolicious::Controller;
use Mojo::Transaction::HTTP;
use Test::Mojo::Stream2;
use Test::Stream2;
use Promises;
use Test::MockModule;
use_ok('App::Stream2::Plugin::Auth::Provider::Adcourier');

my $config_file = 't/app-stream2.conf';

my $user_ref = {
    user => {
        user_id => 1,
        read_contact_details => {
            company => "test",
        },
        read_stream_subscriptions => undef,
        read_navigation_tabs => []
    }
};

my $advertapi_mock = Test::MockModule->new('Bean::AdvertService::Client');
$advertapi_mock->mock(search_adverts => sub {
    return (
        { advert_id => 123, jobtitle => 'Gorilla Tamer', jobref => 'GOR-123', visible => 1 },
        { advert_id => 456, jobtitle => 'Eagle Holder', jobref => 'EGL-123', visible => 0 },
    );
});

# TEST SETUP
my $t = Test::Mojo::Stream2->new();

my $controller= Test::Controller->new();
# Inject an empty transaction to avoid undef transactioin crashes
$controller->tx(Mojo::Transaction::HTTP->new());
$controller->app( $t->app );

my $provider = Test::ProviderMask->new({ oauthserver => "url", client_id => 1 , ripple_settings => {}  });

ok( $provider->user_hierarchy_header(), "Ok got header" );

ok ! $provider->authenticate( $controller, {}), "code is required";
# set the payload you should get back
$provider->oauth_client->set_response($user_ref);
my $promise = $provider->authenticate( $controller, { code => "it doesnt matter" } );
my $cv = AE::cv;
    $promise->then( sub{$cv->send(shift);}, sub{$cv->send({});} );
my $ref = $cv->recv;
is ($promise->status, 'resolved', 'has authenticated (promise resolved)') ;

is_deeply({
        user_id         => $user_ref->{user}->{user_id},
        identity_provider  => 'adcourier',
        login_provider => 'adcourier',
        group_identity  => $user_ref->{user}->{read_contact_details}->{company},
        group_nice_name => $user_ref->{user}->{read_contact_details}->{company_nice_name},
        navigation      => [],
        is_admin        => 0,
        is_overseer     => 0,
}, $ref->{provider}, "correct provider data");

is_deeply($user_ref->{user}->{read_contact_details}, $ref->{contact_details}, "correct contact details");

is_deeply({
    'custom_lists' => [
        { name => 'adc_shortlist',
          label => 'Shortlist',
          allowPreview => 1,
        },
        ]
}, $ref->{ripple_settings}, "correct ripple settings");

is_deeply({}, $ref->{subscriptions}, "got no subscriptions");
{

    my $tabs = [
        {
            show => 1,
            order => 3,
            level => "primary",
            id => 3
        },
        {
            show => 1,
            order => 1,
            level => "primary",
            id => 1,
        },
        {
            show => 0,
            order => 2,
            level => "primary",
            id => 2,
        },
        {
            show => 1,
            level => "not primary",
            order => 5,
            id => 5
        },
        {
            show => 1,
            level => "primary",
            order => 6,
            id => "contactus"
        },
        {
            show => 1,
            order => 4,
            level => "primary",
            id => 4
        }
    ];
    map { $_->{href} = ""; $_->{text} = $_->{id} } @$tabs;
    my @expected = qw/1 3 4/;
    my $out = App::Stream2::Plugin::Auth::Provider::Adcourier->_filter_tabs( $tabs );
    is_deeply( [ map { $_->{t} } @$out ], [ @expected ], "the correct navigation tabs are being displayed" );
}

# test that the label we generate for the shortlist advert list is correct
foreach my $test ( @{[
    {
        label   => 'jobref - jobtitle - location_2, location_1',
        advert  => {
            jobref      => 'jobref',
            jobtitle    => 'jobtitle',
            location_1  => 'location_1',
            location_2  => 'location_2'
        }
    },
    {
        label   => 'DC/CUST/SA - Customer Service Assistant - Kent, United Kingdom',
        advert  => {
            jobref      => 'DC/CUST/SA',
            jobtitle    => 'Customer Service Assistant',
            location_1  => 'United Kingdom',
            location_2  => '--Kent'
        }
    }
]} ){
    my $label = App::Stream2::Plugin::Auth::Provider::Adcourier->_build_advert_label( $test->{advert} );
    is ( $label, $test->{label}, "Generated label matches" );
}

{
    # set the office to be a zombie
    $user_ref->{user}->{read_contact_details}->{office} = 'zombie';
    $provider->oauth_client->set_response($user_ref);

    my $promise = $provider->authenticate( $controller, { code => "it doesnt matter" } );
    my $cv = AE::cv;
        $promise->then( sub{$cv->send(shift);}, sub{$cv->send(shift);} );
    my $ref = $cv->recv;
    is ($promise->status, 'rejected', 'has authenticated (promise rejected)') ;
    is $ref,"User is a zombie\n", 'User is a zombie';
}

{
    ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
    ok( $stream2->deploy_test_db, "Ok can run deploy" );

    my $user_factory = $stream2->factory('SearchUser');
    my $user = $user_factory->new_result({
        id => 91,
        provider => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id => 157656, # simonr@test.simonr.simonr
        group_identity => 'acme',
    });
    $user->save();

    my $adverts_ref = App::Stream2::Plugin::Auth::Provider::Adcourier->get_adverts(
        $user,
        { group_adverts => 1 }
    );

    is_deeply( $adverts_ref->{groups}, ['Live', 'Archived'], 'grouping adverts returns the list of groups available' );

    my @advert_keys = keys($adverts_ref->{adverts});
    is_deeply( \@advert_keys, ['Live', 'Archived'], 'adverts are grouped only under the groups available' );

    is($adverts_ref->{adverts}->{Live}->[0]->{advert_id}, 123, 'correctly grouped a live advert');
    is($adverts_ref->{adverts}->{Archived}->[0]->{advert_id}, 456, 'correctly grouped an archived advert');
}

done_testing();

{
    package Test::ProviderMask;
    use base 'App::Stream2::Plugin::Auth::Provider::Adcourier';
    sub _build_oauth_client { return new Test::OAuth; }
    1;
}

{
    package Test::OAuth;
    use base 'Bean::OAuth::AsyncClient';
    sub new { return bless {}, $_[0]; }
    sub set_response { $_[0]->{response} = $_[1] }
    sub exchange_code {
        my $d = Promises::deferred;

        $d->resolve( $_[0]->{response} );

        return $d->promise;
    }
    1;
}
{
    package Test::Controller;
    use base 'Mojolicious::Controller';
    sub queue_job { 1; }
}

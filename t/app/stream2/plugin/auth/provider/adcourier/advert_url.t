#!/usr/bin/env perl
use strict; use warnings;

use Test::Most;
use Test::MockModule;

use Data::Dumper;

use Stream2;
use App::Stream2::Plugin::Auth::Provider::Adcourier;

my $config_file = 't/app-stream2.conf';
my $stream2 = Stream2->new({ config_file => $config_file });

my $advert_ref = {
    Consultant => "john.smith",
    AplitrakID => "12345",
    AdvertID   => 123
};
my $user_ref = {
    stream2 => $stream2,
    group_identity => 'dev'
};
my $opts = {
    board_id => 999,
    placeholder => 'test'
};


my $adcapi_mock = Test::MockModule->new( 'Bean::AdcApi::Client' );
$adcapi_mock->mock( request_json => sub {
    my $self = shift;
    my $method = shift;
    my $url = shift;

    my $uri = URI->new( $url );
    my %params = $uri->query_form();

    my $json_ref = JSON::decode_json( $params{variables} );
    is_deeply( $json_ref, $opts );

    return { applyonline => "sausage" };
});

my $url = App::Stream2::Plugin::Auth::Provider::Adcourier->advert_preview_url( $advert_ref, TestUser->new( $user_ref ), $opts );

ok( $url );

done_testing();

{
    package TestUser;
    sub new { bless pop, shift; }
    sub group_identity { shift->{group_identity}; }
    sub stream2 { shift->{stream2} }
}

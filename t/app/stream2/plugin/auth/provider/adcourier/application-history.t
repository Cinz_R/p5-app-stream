#!/usr/bin/env perl
use strict; use warnings;

use Test::Most;
use Stream2;
use App::Stream2::Plugin::Auth::Provider::Adcourier;
use Test::MockModule;

my $config_file = 't/app-stream2.conf';
my $stream2 = Stream2->new({ config_file => $config_file });
$stream2->stream_schema->deploy();

my $users_factory = $stream2->factory('SearchUser');
# Create a test user. This is dutch pat
my $user = $users_factory->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 183807,
    group_identity => 'pat',
});
$user->save();

my $now = time();
my $seven_months_ago = $now - ( 86400 * 28 * 7 );

my $api_response =  [
    {
        response => {
            response_id     => 1,
            attachments     => '123-cv.doc|coverletter.pdf',
            rank            => 5,
            time            => $now,
        },
        advert => {
            responses_url    => 'script_action=sausages',
            jobtitle        => 'guard dog',
            advert_id => 123,
        }
    },
    {
        response => {
            response_id     => 2,
            attachments     => '123-cv.doc|coverletter.pdf',
            rank            => 1,
            time            => $seven_months_ago
        },
        advert => {
            apply_url       => 'script_action=sausages',
            jobtitle        => 'lemon squeezer',
            advert_id => 123,
        }
    }
];

my $expected = [
    {
        'attachments' => [ '123-cv.doc', 'coverletter.pdf' ],
        'application_time' => $now,
        'advert_link' => 'script_action=view_shortlist#response_1',
        'job_title' => 'guard dog',
        'rank' => '5',
        'adcresponse_id' => 1,
        'advert_id' => 123,
    },
    {
        'attachments' => [], # dont show attachments for applications older than 6 months
        'application_time' => $seven_months_ago,
        'advert_link' => 'script_action=sausages',
        'job_title' => 'lemon squeezer',
        'rank' => 1,
        'adcresponse_id' => 2,
        'advert_id' => 123,
    }
];

my $api_mock = Test::MockModule->new('Bean::Juice::APIClient::Candidate');
$api_mock->mock( candidate_applications => sub { $api_response; });

ok ( my $app_ref = App::Stream2::Plugin::Auth::Provider::Adcourier->application_history( $user, Test::Candidate->new({ email => 1 }) ) );
is_deeply( $app_ref, $expected );

done_testing();

{
    package Test::Candidate;
    sub new { bless pop, shift; }
    sub email { shift->{email} }
}

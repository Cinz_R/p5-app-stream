#!/usr/bin/env perl
use strict; use warnings;

use Test::Most;
use App::Stream2::Plugin::Auth::Provider::Adcourier;
use Test::MockModule;

use Stream2;

my $config_file = 't/app-stream2.conf';
my $stream2 = Stream2->new({ config_file => $config_file });
$stream2->stream_schema->deploy();

my $users_factory = $stream2->factory('SearchUser');
# Create a test user. This is dutch pat
my $user = $users_factory->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 183807,
    group_identity => 'pat',
});
$user->save();


my $api_response = {
    'BigWord5ts_hello@BIGBIGBIG.com' => {'5' => '1'},
    'BigWord5ts_hello@bigbigbig.com' => {'3' => '2'},
    'bigword5ts_hello@bigbigbig.com' => {'7' => '5'},
    'jordit111@gmail.com' => {'7' => '1'},
    'adam@onitsfeet.com' => {'1' => '1'},
    'adamlucasuk@hotmail.com' => {'1' => '2','5' => '1'},
    'asif.saleem@yahoo.com' => {'7' => '1'},
    'mowelsadam@yahoo.co.uk' => {'7' => '1'},
    'pete.hardie@hotmail.co.uk' => {'7' => '1'},
    'nabeesa.fernandes@gmail.com' => {'7' => '1'},
    'dbeveridge6@gmail.com' => {'1' => '1','7' => '2'},
    'adam.grace@hotmail.co.uk' => {'7' => '1'},
    'hakim.bensiali888@gmail.com' => {'1' => '1'},
    'adamwright_uk@me.com' => {'7' => '1'},
    'yadz65@gmail.com' => {'1' => '1'},
    'ben.pardey@googlemail.com' => {'7' => '1'},
    'murrg30779m@yahoo.co.uk' => {'1' => '1'},
    'aatudorjones@gmail.com' => {'1' => '1'},
    'ian.dean@internode.on.net' => {'7' => '1'},
    'adamlucasuk@Hotmail.com' => {'7' => '4'},
    'adamkcooper@hotmail.com' => {'7' => '1'},
    'louisbrown.brown@googlemail.com' => {'7' => '1'},
    'call_me_tk@hotmail.co.uk' => {'7' => '1'},
    'diren_3@msn.com' => {'1' => '1'},
    'melvin.long@outlook.com' => {'7' => '2'},
    'drurya@gmail.com' => {'1' => '1'},
    'adamedwards246@gmail.com' => {'1' => '1'},
    'samuel_romao@yahoo.com' => {'1' => '1' },
};

my $expected = {
    'BigWord5ts_hello@bigbigbig.com' => { '5' => '1', '3' => '2' },
    'bigword5ts_hello@bigbigbig.com' => {'7' => '5'},
    'jordit111@gmail.com' => {'7' => '1'},
    'adam@onitsfeet.com' => {'1' => '1'},
    'adamlucasuk@hotmail.com' => {'1' => '2','5' => '1', '7' => '4'},
    'asif.saleem@yahoo.com' => {'7' => '1'},
    'mowelsadam@yahoo.co.uk' => {'7' => '1'},
    'pete.hardie@hotmail.co.uk' => {'7' => '1'},
    'nabeesa.fernandes@gmail.com' => {'7' => '1'},
    'dbeveridge6@gmail.com' => {'1' => '1','7' => '2'},
    'adam.grace@hotmail.co.uk' => {'7' => '1'},
    'hakim.bensiali888@gmail.com' => {'1' => '1'},
    'adamwright_uk@me.com' => {'7' => '1'},
    'yadz65@gmail.com' => {'1' => '1'},
    'ben.pardey@googlemail.com' => {'7' => '1'},
    'murrg30779m@yahoo.co.uk' => {'1' => '1'},
    'aatudorjones@gmail.com' => {'1' => '1'},
    'ian.dean@internode.on.net' => {'7' => '1'},
    'adamkcooper@hotmail.com' => {'7' => '1'},
    'louisbrown.brown@googlemail.com' => {'7' => '1'},
    'call_me_tk@hotmail.co.uk' => {'7' => '1'},
    'diren_3@msn.com' => {'1' => '1'},
    'melvin.long@outlook.com' => {'7' => '2'},
    'drurya@gmail.com' => {'1' => '1'},
    'adamedwards246@gmail.com' => {'1' => '1'},
    'samuel_romao@yahoo.com' => {'1' => '1' },

    # these should have been consolidated into the same email with lower case domain
    'adamlucasuk@Hotmail.com' => {},
    'BigWord5ts_hello@BIGBIGBIG.com' => {},
};

my $api_mock = Test::MockModule->new('Bean::Juice::APIClient::Candidate');
$api_mock->mock( flagging_history => sub { $api_response; });
my $results = [map{ Test::Candidate->new({ email => $_ })}keys(%$api_response)];
App::Stream2::Plugin::Auth::Provider::Adcourier->flagging_history( $user, $results );

foreach my $result ( @$results ) {
    is_deeply( $result->{flagging_history}, $expected->{$result->{email}} );
}
done_testing();

{
    package Test::Candidate;

    sub new { bless pop, shift }
    sub email { shift->{email} }
    sub attr {
        my ( $self, $attr, $val ) = @_;
        $self->{$attr} = $val;
    }
}

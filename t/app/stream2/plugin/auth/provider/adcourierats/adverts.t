#! perl -w

use strict;
use warnings;

use Test::Most;
use Test::MockModule;
use Test::Stream2;

use HTTP::Response;

# use Log::Any::Adapter qw/Stderr/;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

ok( my $provider = $stream2->build_provider('adcourier_ats') , "OK can build provider");

my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
    settings => {
        "auto_download_cvs_on_preview" => JSON::XS::false,
        "some_other_random_setting" => JSON::true,
        'stream' => {
            'forward_to_tp2' => '0'
        },
    }
});
my $user_id = $memory_user->save();

$memory_user->ripple_settings()->{custom_lists} = [
    {
        name => 'saussage_list',
        label => 'Saussage',
        allowPreview => 1,
        options_url => 'http://www.example.com/',
    },
    {
        name => 'kitten_list',
        label => 'Kitten',
        options_url => 'http://www.example.com/',
    }
];

my $mocked_agent = Test::MockModule->new( ref( $stream2->user_agent() ) );
$mocked_agent->mock('request' => sub{
                        my ($self, $request) = @_;
                        like( $request->content(), qr/1234321/, "Ok request contains the right advert ID");
                        my $res = HTTP::Response->new( 200, "OK");
                        $res->header('Content-Type' => 'text/xml');
                        $res->content('<?xml version="1.0" encoding="UTF-8"?>
  <SearchCustomListResponse>
    <Version>3.0</Version>
    <Options>
        <Option value="1234" previewURL="http://www.saussage.com/">Bratwurst</Option>
    </Options>
  </SearchCustomListResponse>
');
                        return $res;
                    });

{
    # Something from the list where the allowPreview is true
    ok( my $advert = $provider->find_advert( $memory_user , 1234321 , { list_name => 'saussage_list' }) , "Ok can find advert");
    is( $advert->{advert_id} , 1234 , "Ok good advert ID as given by the API");
    is( $advert->{preview_url} , 'http://www.saussage.com/' , "Good preview URL");
    is( $provider->advert_preview_url( $advert, $memory_user ) , 'http://www.saussage.com/', "Ok good URL");
}

{
    # Something from the list where there is no allowPreview
    ok( my $advert = $provider->find_advert( $memory_user , 1234321 , { list_name => 'kitten_list' }) , "Ok can find advert, as just a fake one");
    is( $advert->{advert_id} , 1234321 , "Ok got fake advert ID given by just the query (no ripple back calls were made");
    is( $provider->advert_preview_url( $advert, $memory_user ) , undef, "Ok no preview URL");
}

done_testing();

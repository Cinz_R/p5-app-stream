use Test::Most;
use App::Stream2::Plugin::Auth::Provider::AdcourierAts;

ok (
    App::Stream2::Plugin::Auth::Provider::AdcourierAts->user_can_import_from(
        TestObject->new({ ripple_settings => { new_candidate_url => 'hello' } }),
        'talentsearch'
    ),
    'User can import from anywhere as long as they have an import url'
);

ok (
    ! App::Stream2::Plugin::Auth::Provider::AdcourierAts->user_can_import_from(
        TestObject->new({ ripple_settings => {}, has_adcourier_search => 1 }),
        'talentsearch'
    ),
    'User can not import from an internal database without an import url'
);


ok (
    ! App::Stream2::Plugin::Auth::Provider::AdcourierAts->user_can_import_from(
        TestObject->new({ ripple_settings => {}, has_adcourier_search => 0 }),
        'careerbuilder'
    ),
    'User can not import from an external board unless they have an internal board to import to'
);

ok (
    App::Stream2::Plugin::Auth::Provider::AdcourierAts->user_can_import_from(
        TestObject->new({ ripple_settings => {}, has_adcourier_search => 1 }),
        'careerbuilder'
    ),
    'User can import from external to an internal board without an import url'
);

ok (
    ! App::Stream2::Plugin::Auth::Provider::AdcourierAts->user_can_import_from(
        TestObject->new({ ripple_settings => { hide_save_internal => 1 }, has_adcourier_search => 1 }),
        'cb_mysupply'
    ),
    'User cannnot import when in an internal search and hide_save_internal is true'
);

ok (
    ! App::Stream2::Plugin::Auth::Provider::AdcourierAts->user_can_import_from(
        TestObject->new({ ripple_settings => { hide_save => 1 }, has_adcourier_search => 1 }),
        'careerbuilder'
    ),
    'User cannnot import when in an external search and hide_save is true'
);

done_testing();

{
    # TestObject provides a getter for any attribute
    package TestObject;
    use AutoLoader;
    sub new { bless pop, shift; }
    sub AUTOLOAD {
        our $AUTOLOAD;
        my ( $method ) = $AUTOLOAD =~ m/\ATestObject::(.*)\z/;
        return $method eq 'DESTROY' ? () : shift->{$method};
    }
    sub has_subscription {
        my ($self, $destination) = @_;
        if ( grep { $_ eq $destination } qw/talentsearch cb_mysupply/ ) {
            return { type => 'internal' };
        }
        return { type => 'external' };
    }
}

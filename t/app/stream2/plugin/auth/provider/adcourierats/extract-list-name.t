#!/usr/bin/env perl
use Test::Most;
use App::Stream2::Plugin::Auth::Provider::AdcourierAts;

my @tests = (
    { in => 'shortlist_sausage', out => 'sausage' },
    { in => 'shortlist_sausage_list', out => 'sausage_list' },
    { in => 'sausage', out => undef },
    { in => 'shortlist_sausage_list_fail', out => 'sausage_list' },
    { in => 'shortlist_sausage_fail', out => 'sausage' }
);

foreach my $test ( @tests ) {
    is ( App::Stream2::Plugin::Auth::Provider::AdcourierAts->_extract_list_name( $test->{in} ), $test->{out} );
}


done_testing();

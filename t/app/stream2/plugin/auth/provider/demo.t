use Test::Most tests => 4;
use Mojolicious::Controller;
use AnyEvent;
my $class = 'App::Stream2::Plugin::Auth::Provider::Demo';
use_ok($class);

my $controller= Mojolicious::Controller->new();

{
    my $cv = AE::cv;
        $class->new()->authenticate($controller, { username => 'wrong', password => 'wrong' })
            ->then( sub{$cv->send(shift);}, sub{$cv->send(undef,shift);} );
    ok !$cv->recv, 'Wrong username and password';
}
{
    my $cv = AE::cv;
        $class->new()->authenticate($controller, { username => 'demo', password => 'demo' })
            ->then( sub{$cv->send(shift);}, sub{$cv->send(shift);} );
    ok $cv->recv, 'Default username and password';
}
{
    my $cv = AE::cv;
        $class->new( username => 'username', password => 'password' )->authenticate( $controller, { username => 'username', password => 'password' })
           ->then( sub{$cv->send(shift);}, sub{$cv->send(shift);} );
    ok $cv->recv, 'Custom username and password';
}

done_testing();

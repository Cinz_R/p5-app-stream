use Test::Most;

use Mojolicious;
use App::Stream2::Plugin::Auth;

my @tests = (
  [ "No config"    => undef ],
  [ "Empty config" =>  {} ],
  [ "No config returned by callback" => sub {} ],
);

plan tests => scalar(@tests);

foreach my $test ( @tests ) {
  my ($test_name, $config) = @$test;

  my $auth = App::Stream2::Plugin::Auth->new();
  $auth->register(Mojolicious->new(), $config);

  ok( $auth->default_provider(), $test_name );
}

done_testing();

use Test::Most;

use strict;
use warnings;
use AnyEvent;

use_ok('App::Stream2::Plugin::Auth');

use Mojolicious;
use Mojolicious::Controller;
use Mojo::Transaction::HTTP;

my %config = (
  providers => {
    demo => {
      username => username(),
      password => password(),
    },
  },
);

subtest "Config hash" => sub {
  test_plugin( App::Stream2::Plugin::Auth->new( config => { %config }) );
};

subtest "Config callback" => sub {
  my $plugin = App::Stream2::Plugin::Auth->new( config => sub { +{ %config } } );
  test_plugin($plugin);
};

done_testing();

sub test_plugin {
  my $plugin = shift;

  ok(!authenticate($plugin),                               "Missing username and password");
  ok(!authenticate($plugin, "wrong username", password()), "Wrong username");
  ok(!authenticate($plugin, username(),"wrong password"),  "Wrong password");
  ok( authenticate($plugin, username(), password()),       "Valid account details");
}

sub username { 'andy@users.live.andy' }
sub password { 'demo' }

sub authenticate {
  my ($plugin, $username, $password) = @_;

  my $controller = Mojolicious::Controller->new();
  # Make sure there's an app in this controller.
  $controller->app(Mojolicious->new);
  # Make sure there is a current transaction. this is
  # because the first username/password will be undef and the
  # demo auth plugin will try to look into the transaction request.
  $controller->tx(Mojo::Transaction::HTTP->new);

  my %options = (
      provider => 'demo',
      username => $username,
      password => $password,
  );
  $controller->app->helper( current_user => sub {} );
  # auth_provider()->authenticate return promises, we need to provide a condvar
  my $authenticate_promise = $plugin->auth_provider( $controller )->authenticate($controller, \%options);
  return unless $authenticate_promise;
  my $cv = AE::cv;
    $authenticate_promise->then( sub{$cv->send(shift);}, sub{$cv->send(undef,shift);} );
  return $cv->recv;
}

use Test::Most;

{
  package App::Stream2::Plugin::Auth::Provider::Testing;

  use Moose;

  has 'required' => ( is => 'ro', required => 1 );
  has 'optional' => ( is => 'ro' );
  has 'login_url'   => ( is => 'ro', default => 'test' );
  has 'retry_login' => ( is => 'ro', default => 0 );

  sub advert_shortlist_candidate{ confess("Nothing here"); }
  sub advert_preview_url{}
  sub find_advert{}
  sub get_adverts{}
  sub authenticate{}
  sub application_history{}
  sub flagging_history{}
  sub user_import_candidate{}
  sub user_can_import_from{}
  sub user_can_download_from{}

  with 'App::Stream2::Plugin::Auth::Role::Provider';

};

my $class = 'App::Stream2::Plugin::Auth::Providers';
use_ok($class);

subtest "Loading providers from name" => sub {
    my $providers = $class->new();
    dies_ok { $providers->load_provider("Missing"); };

    my %provider_tests = (
        'Module name' => 'Testing',
        'lower case'  => 'testing',
        'Full module' => 'App::Stream2::Plugin::Auth::Provider::Testing',
    );

    while ( my ($test_label, $provider_name) = each %provider_tests ) {
        subtest $test_label => sub {
            my %config = ( required => 'config' );
            my $provider = $providers->register_provider($provider_name, %config);
            ok($provider, "Loaded '$provider_name' provider");
        };
    }

    throws_ok { $providers->register_provider('DoesNotExist', {}) } qr/Could not find the "DoesNotExist" provider/;

    throws_ok { $providers->register_provider('Test::Most', {}) } qr/Could not find the "Test::Most" provider/, "Ignore existing classes that are not providers";
};

subtest "Required provider config" => sub {
    my $providers = $class->new();
    dies_ok { $providers->register_provider('Testing') };

    my $provider = $providers->register_provider('Testing', { required => 'config' });
    is($provider->required, 'config');
};

subtest "Optional provider config" => sub {
    my $providers = $class->new();
    my $provider = $providers->register_provider('Testing', { required => 'config', optional => 'optional' });
    is($provider->optional, 'optional');
};

done_testing();

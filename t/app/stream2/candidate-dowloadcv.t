#! perl -w
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Test::Mojo::Stream2;
use Qurious;
use MIME::Base64;
use JSON;
use Promises qw/deferred/;
use Stream2::Action::DownloadCV;

use Log::Log4perl qw/:easy/;
# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log4perl');
# Log::Any::Adapter->set('Stderr');

my $mocked_candidate = Stream2::Results::Result->new({
                                                      destination     => "talentsearch",
                                                      candidate_id    => 1,
                                                      cv_url          => "s3://attachments.broadbean.com/fe861f0c780b9446fcfa2aae2edfcc8be428f4a0"
                                                     });


# TEST SETUP
my $t = Test::Mojo::Stream2->new({stream2_class => 'Test::Stream2'});
{
    # Monkey patch so we dont actually enqueue anything
    no strict;
    no warnings;
    *{'Stream2::increment_analytics'} = sub{};
    use strict;
    use warnings;
}

$t->app->helper('do_job' => sub {
    my $d = deferred;
    my $job = test_job();
    Stream2::Action::DownloadCV->perform( $job );
    $d->resolve($job);
    return $d;
});


$t->login_ok();


# TEST 2 - 3
# status is 200 only when the CV is being served.
$t->get_ok('/results/1/talentsearch/candidate/0/cv')->status_is(200);

# done_testing();
# exit(0);


# Wait for 2 seconds to ensure the times are different in the database ( updated / inserted )
sleep( 2 );

# TEST 4
# cv is downloaded from board and is correct
my $job = test_job();
Stream2::Action::DownloadCV->perform( $job );
my $response = MIME::Base64::decode_base64( $job->result->{result} );
like( $response, qr/Skilled Telecommunication Engineer with Project Management Experience/, "CV returned correctly" );

# TEST 5 - 8
# cv download is remembered and state is correct
my $candidate = $mocked_candidate;

LWP::UserAgent::Mockable->finished();
done_testing();

sub test_job {
    my $app = shift;

    my $job = TestQuriousJob->new({
        class => "Stream2::Action::DownloadCV",
        parameters => {
            results_id => 1,
            candidate_idx => 0,
            user => {
                user_id => 1,
                group_identity => "andy",
                auth_tokens => {}
            },
            ripple_settings => {}
        }
    });

    # A stream_schema holding Application mockery (See bottom of this file)
    $job->{stream2} = $t->app->stream2();
    return $job;
}
{
    package TestQuriousJob;
    use base "Qurious::Job";
    sub save {}
}

{
    package MockedResults;
    use Moose;
    sub find{
        return $mocked_candidate;
    }
}
1;

## Just mock find_results in the Stream2 class
{
    no warnings 'redefine';
    package Stream2;
    sub find_results{ return MockedResults->new(); }
    1;
    use warnings;
}

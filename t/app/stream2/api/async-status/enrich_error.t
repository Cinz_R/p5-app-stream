#!/usr/bin/env perl
use Test::Most;
use Qurious::Job;
use App::Stream2::Api::AsyncJob;

my $job = Qurious::Job->new({ class => 'nonsense' });
my $guid = $job->guid;

my $result = {
    error => sprintf( 'Something very bad has gone wrong on line 666 Please quote guid {%s} to CS', $guid )
};
is_deeply( App::Stream2::Api::AsyncJob->_fixup_result_error( $job, $result ), { %$result, is_unmanaged => 1 }, 'The error has been recognised as being unmanaged' );

$result = {
    error => 'Something fairly normal has happened, don\'t worry about it too much'
};
is_deeply( App::Stream2::Api::AsyncJob->_fixup_result_error( $job, $result ), $result, 'The error has been recognised as being managed' );

done_testing();

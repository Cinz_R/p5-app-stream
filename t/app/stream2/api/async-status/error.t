#!/usr/bin/env perl
use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;
use JSON;

use_ok('App::Stream2::Api::AsyncJob');

# mock the queue to return undef.

my $queue_mock = Test::MockModule->new('App::Stream2::Plugin::Queue');
$queue_mock->mock( 'retrieve_job' => sub { return } );

# TEST SETUP
my $t = Test::Mojo::Stream2->new();
$t->login_ok();

$t->get_ok('/api/asyncjob/I_DO_NO_EXIST/status')->status_is(404);

my $response_ref = JSON::decode_json( $t->tx->res->content()->get_body_chunk(0) );
is($response_ref->{error}->{message}, 'Sorry this action has expired.',  'Got a sensible erorr back.');


done_testing();

use Test::Most;
use Test::Mojo::Stream2;
use JSON;

use Log::Log4perl qw/:easy/;
# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log4perl');
# Log::Any::Adapter->set('Stderr');

# TEST SETUP
my $t = Test::Mojo::Stream2->new();
$t->app->helper('queue_job' => sub { 1 });
$t->login_ok();

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}
my $stream2 = $t->app->stream2;
my $schema = $stream2->stream_schema();

# TESTS

# no watchdogs
$t->get_ok('/api/watchdogs')
    ->status_is(200)
    ->json_is({ watchdogs => []});

# add some watchdogs
my $criteria = Stream2::Criteria->new( schema => $schema );
$criteria->add_token( 'keywords' , 'gentleman' );
$criteria->save();

my $user = $stream2->factory('SearchUser')->find(1);
$user->subscriptions_ref({
                          flickr_xray => {
                                          nice_name => "Flickr",
                                          auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
                                          type => "social"
                                         },
                          'whatever' => {
                                         nice_name => 'Whatever',
                                         auth_tokens => { whatever_login => 'Boudin', whatever_passwd => 'Blanc' },
                                         type => 'social'
                                        }
                         });
$user->save;

my $watchdog1 = $stream2->watchdogs->create({ user_id => $user->id, name => 'sub1', criteria_id => $criteria->id() });
my $flickr = $user->subscriptions()->find({ board => 'flickr_xray' });
$watchdog1->add_to_watchdog_subscriptions( { subscription => $flickr } );

my $watchdog_subscription = $watchdog1->watchdog_subscriptions()->first();
$watchdog_subscription->watchdog_results()->create({ candidate_id => 'whatever',  viewed => 0 });
$watchdog_subscription->watchdog_results()->create({ candidate_id => 'othercandidate',  viewed => 1 });

# no watchdogs
$t->get_ok('/api/watchdogs')
    ->status_is(200)
    ->json_is('/watchdogs/0/criteria_id', $criteria->id, 'The criteria has made it into the watchdogs list')
    ->json_is('/watchdogs/0/new_results', 1, 'only our unviewed added result is showing as unviewed')
    ->json_is('/watchdogs/0/boards/0/count', 1, 'at board level too');

$watchdog_subscription->watchdog_results()->find({ candidate_id => 'othercandidate' })->update({viewed => 0});

$t->get_ok('/api/watchdogs')
    ->json_is('/watchdogs/0/new_results', 2, 'only our unviewed added result is showing as unviewed');

$watchdog_subscription->watchdog_results()->find({ candidate_id => 'othercandidate' })->update({viewed => 1});

$t->get_ok('/api/watchdogs')
    ->json_is('/watchdogs/0/new_results', 1, 'only our unviewed added result is showing as unviewed');

$watchdog_subscription->watchdog_results()->find({ candidate_id => 'whatever' })->delete();

$t->get_ok('/api/watchdogs')
    ->json_is('/watchdogs/0/new_results', 0, 'only our unviewed added result is showing as unviewed');

done_testing();

1;

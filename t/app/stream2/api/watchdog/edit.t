use Test::Most;
use Test::Mojo::Stream2;
use JSON;

use Log::Log4perl qw/:easy/;
# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log4perl');
#Log::Any::Adapter->set('Stderr');

# TEST SETUP
my $t = Test::Mojo::Stream2->new();
$t->app->helper('queue_job' => sub { 1 });
$t->login_ok();

# $test_schema->deploy;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}
my $stream2 = $t->app->stream2;
my $schema = $stream2->stream_schema();
my $watchdogs = $stream2->watchdogs;
use Stream2::Criteria;

# add some watchdogs
my $criteria = Stream2::Criteria->new( schema => $schema );
$criteria->add_token( 'keywords' , 'gentleman' );
$criteria->save();

# This user ID =1 would already be there thanks to the test harness.
my $user = $stream2->factory('SearchUser')->find(1);
$user->subscriptions_ref({
                          flickr_xray => {
                                          nice_name => "Flickr",
                                          auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
                                          type => "social"
                                         },
                          'whatever' => {
                                         nice_name => 'Whatever',
                                         auth_tokens => { whatever_login => 'Boudin', whatever_passwd => 'Blanc' },
                                         type => 'social'
                                        }
                         });
$user->save();

my $watchdog1 = $watchdogs->create({ user_id => $user->id, name => 'sub1', criteria_id => $criteria->id() });
ok( my $flickr = $user->subscriptions()->find({ board => 'flickr_xray' }) );
ok( my $whatever = $user->subscriptions()->find({ board => 'whatever' }) );

$watchdog1->add_to_watchdog_subscriptions( { subscription => $flickr } );
$watchdog1->add_to_watchdog_subscriptions( { subscription => $whatever } );

is( $watchdogs->count() , 1  , "Ok got one watchdog");

# TESTS

# list displays my new watchdog with its subscriptions
$t->get_ok('/api/watchdogs')
    ->status_is(200)
    ->json_is('/watchdogs/0/name', 'sub1')
    ->json_is('/watchdogs/0/boards/0/name', 'flickr_xray')
    ->json_is('/watchdogs/0/boards/1/name', 'whatever');

my $new_name = 'new_name';

$t->post_ok('/api/watchdog/' . $watchdog1->id,
    form => {
        q => JSON::encode_json({
            boards => [ 'flickr_xray' ],
            name   => $new_name
        })
    }
);

# second subscription should be gone and name should be different
$t->get_ok('/api/watchdogs')
    ->status_is(200)
    ->json_is('/watchdogs/0/name', $new_name)
    ->json_is('/watchdogs/0/boards/0/name', 'flickr_xray')
    ->json_hasnt('/watchdogs/0/boards/1/name');

# editing a watchdog should not cause a renaming error
$t->post_ok('/api/watchdog/' . $watchdog1->id,
    form => {
        q => JSON::encode_json({
            boards => [ 'flickr_xray' ],
            name   => $new_name
        })
    }
)->status_is(200);


# lets create a new Watchdog, then try and rename it to something we already have

my $watchdog2 = $watchdogs->create({ user_id => $user->id, name => 'Spot', criteria_id => $criteria->id() });
$watchdog2->add_to_watchdog_subscriptions( { subscription => $flickr } );

is( $watchdogs->count() , 2  , "Ok got two watchdog");

$t->get_ok('/api/watchdogs')
    ->status_is(200)
    ->json_is('/watchdogs/0/name', 'Spot')
    ->json_is('/watchdogs/0/boards/0/name', 'flickr_xray');

$t->post_ok('/api/watchdog/' . $watchdog2->id,
    form => {
        q => JSON::encode_json({
            boards => [ 'flickr_xray' ],
            name   => $new_name
        })
    }
)->status_is(400)
 ->json_is('/error/message', "Watchdog '$new_name' already exists.");

done_testing();

1;

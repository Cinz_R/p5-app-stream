use Test::Most;
use Test::Mojo::Stream2;
use JSON;
my $t = Test::Mojo::Stream2->new;
$t->login_ok;
my $s2 = $t->app->stream2();

# This is a canary test for the presence of a incorrect conversion of a
# JSON::Boolean to a perl ture (1) or false (0) between the front end
# and back end. IF JSON::XS was greater than 2.34 then JSON cannot use it,
# and falls back to JSON:PP, and there is something funky with the way its
# uses JSON::PP::Boolean, if true, the value seems to want to eval to 'true'
# instead of 1.

my $user = $s2->factory('SearchUser')->create({
    contact_details => {
        contact_name  => "Cinzia",
        contact_email => 'cinxia@cb-bb-bbf.com'
    },
    provider        => 'adcourier',
    provider_id     => 1,
    group_identity  => 'tequilla_sunrise_co',
    navigation      => [],
    stream2         => $s2,
    is_admin        => 0,
    is_overseer     => 1,
    locale          => 'en',
    last_login_provider => 'adcourier',
});
$user->save();

$t->app->helper( 'current_user' => sub{ return $user } );

{
    # Global product setting
    my $setting = $t->app->stream2->factory('Setting')->find({
        setting_mnemonic => 'candidate.actions.forward.enabled'
    });

    $t->post_ok("/api/admin/user_settings/".$setting->id => { Accept => 'text/json' },
                form => {
                    q => JSON::encode_json({
                        value => JSON::false, # Set the value to non default.
                        users => [$user->id],
                    }),
                    ts => time
                }
            )->status_is(200)->content_like(qr({"message":"Success"}));

    # {"default":{"boolean_value":true,"string_value":null},"users_map":[{"users":[2],"value":{"boolean_value":false,"string_value":null}}]}
    $t->get_ok('/api/admin/user_settings/'.$setting->id() => { Accept => 'text/json' } )
        ->status_is(200)->json_is('/default/boolean_value', JSON::true )->json_is('/users_map/0/users/0', 2 )
        ->json_is('/users_map/0/value/boolean_value', JSON::false );
}

{
    # Group specific setting.
    # Need to create one first
    my $setting = $t->app()->stream2()->factory('Groupsetting', { group_identity => $user->group_identity })
        ->create({ setting_mnemonic => 'response_flag-1-active', # Note we use kebab case here. SQLite does not like 'response_flag.1.active'
                   setting_description => 'Response flag 1 active?',
                   default_boolean_value => 1,
                   setting_type => 'boolean'
               });
    ok( $setting );

    $t->post_ok("/api/admin/user_groupsettings/".$setting->setting_mnemonic() => { Accept => 'text/json' },
                form => {
                    q => JSON::encode_json({
                        value => JSON::false, # Set the value to non default.
                        users => [$user->id],
                    }),
                    ts => time
                }
            )->status_is(200)->content_like(qr({"message":"Success"}));

    # {"default":{"boolean_value":true,"string_value":null},"users_map":[{"users":[2],"value":{"boolean_value":false,"string_value":null}}]}
    $t->get_ok('/api/admin/user_groupsettings/'.$setting->setting_mnemonic() => { Accept => 'text/json' } )
        ->status_is(200)->json_is('/default/boolean_value', JSON::true )->json_is('/users_map/0/users/0', 2 )
        ->json_is('/users_map/0/value/boolean_value', JSON::false );
}


done_testing;

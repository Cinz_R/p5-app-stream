#!/usr/bin/env perl
use Test::Most;
use App::Stream2::Api::Admin;

my $grouped_token = {
    'Options' => [
        {
            'Options' => [
                [
                    'Sales Ledger Clerk',
                   '1562'
                ],
                [
                    'Treasury',
                    '1920'
                ]
            ],
            'Label' => 'Accountancy'
        },
        {
            'Options' => [
                [
                    'Tax Accountant',
                    '857'
                ],
                [
                    'Treasury',
                    '1676'
                ],
                [
                    'VAT Specialist',
                    '1965'
                ]
            ],
            'Label' => 'Finance'
        },
        {
            'Options' => [
                [
                    'Secretary',
                    '32'
                ],
                [
                    'Team Secretary',
                    '1563'
                ],
                [
                    'Typist',
                    '869'
                ]
            ],
            'Label' => 'Admin, Secretarial & PA'
        },
    ],
    'Type' => 'groupedmultilist',
};

my $list_token = {
    Options => [
        [ 'Option 1', 1 ],
        [ 'Option 2', 2 ],
        [ 'Option 3', 3 ],
        [ 'Option 4', 4 ]
    ],
    Type => 'multilist'
};

my $group_values = App::Stream2::Api::Admin->_extract_list_values( $grouped_token, [ 1965, 869, 1562, 1920 ] );
is_deeply(
    [ sort @$group_values ],
    [ sort 'Accountancy - Sales Ledger Clerk', 'Accountancy - Treasury', 'Finance - VAT Specialist', 'Admin, Secretarial & PA - Typist' ],
    'can extract groupedmultilist value labels'
);

my $list_values = App::Stream2::Api::Admin->_extract_list_values( $list_token, [ 4, 1 ] );
is_deeply(
    [ sort @$list_values ],
    [ sort 'Option 1', 'Option 4' ],
    'can extract multilist value labels'
);

done_testing();

use Test::Most;
use Test::Mojo::Stream2;
use JSON;
my $t = Test::Mojo::Stream2->new({stream2_class => 'Test::Stream2'});
$t->login_ok;
my $s2 = $t->app->stream2();

my $user = $s2->factory('SearchUser')->create(
    {   contact_details => {
            contact_name  => "Jason",
            contact_email => 'Jason@JSON_breakersyard.com'
        },
        provider            => 'adcourier',
        provider_id         => 1,
        group_identity      => 'jsonbrakers',
        navigation          => [],
        stream2             => $s2,
        is_admin            => 1,
        is_overseer         => 1,
        locale              => 'en',
        last_login_provider => 'adcourier',
    }
);
$user->save();

$t->app->helper( 'current_user' => sub { return $user } );

$t->post_ok(
    "/api/admin/email-templates" => { Accept => 'text/json' },
    form                         => {
        q => JSON::encode_json(
            {   templateName => "a_unique_name",
                emailReplyto => undef,
                emailBcc     => undef,
                emailSubject => "a subject line",
                emailBody    => "this does not matter",
                templateRules => []
            }
        ),
        ts => time
    }
    )->status_is(200)
    ->content_like(qr(Your new email template \\"a_unique_name\\" has been created));

$t->post_ok(
    "/api/admin/email-templates" => { Accept => 'text/json' },
    form                         => {
        q => JSON::encode_json(
            {   templateName => "a_unique_name",
                emailReplyto => undef,
                emailBcc     => undef,
                emailSubject => "a subject line",
                emailBody    => "this does not matter",
                templateRules => [],
            }
        ),
        ts => time
    }
)->status_is(400)->content_like(
    qr(This template is not valid: You already have a template called \\"a_unique_name\\")
);

$t->app->stream2->factory('EmailTemplate')->search()->delete();

subtest 'create some templates' => sub {
    # create lots of templates
    for my $number ( 0 .. 20 ) {
        my $name = "a_unique_name_$number";
        $t->post_ok(
            "/api/admin/email-templates" => { Accept => 'text/json' },
            form                         => {
                q => JSON::encode_json(
                    {
                        templateName => "a_unique_name_$number",
                        emailReplyto => undef,
                        emailBcc     => undef,
                        emailSubject => "a subject line",
                        emailBody    => "this does not matter",
                        templateRules => []
                    }
                ),
                ts => time
            }
          )->status_is(200)
          ->content_like(qr(Your new email template \\"$name\\" has been created));
    }
};

# lets do some paging

subtest 'no page' => sub {
    my $res = $t->get_ok(
        '/api/admin/email-templates' => { Accept => 'text/json' } );
    $res->status_is(200);
    $res->json_has('/pager')->json_has('/emailtemplates');
    $res->json_is('/emailtemplates/0/name' => 'a_unique_name_0');
    # the ids are returned in alphabetical order
    $res->json_is('/emailtemplates/9/name' => 'a_unique_name_17');
};


subtest 'with page -1' => sub {
    my $res = $t->get_ok(
        '/api/admin/email-templates?q={"page":-1}' => { Accept => 'text/json' } );
    $res->status_is(200);
    $res->json_has('/pager')->json_has('/emailtemplates');
    $res->json_is('/pager/current_page' => 1);
    $res->json_is('/emailtemplates/0/name' => 'a_unique_name_0');
    $res->json_is('/emailtemplates/9/name' => 'a_unique_name_17');
};

subtest 'with page 0' => sub {
    my $res = $t->get_ok(
        '/api/admin/email-templates?q={"page":0}' => { Accept => 'text/json' } );
    $res->status_is(200);
    $res->json_has('/pager')->json_has('/emailtemplates');
    $res->json_is('/pager/current_page' => 1);
    $res->json_is('/emailtemplates/0/name' => 'a_unique_name_0');
    $res->json_is('/emailtemplates/9/name' => 'a_unique_name_17');
};

subtest 'with page 1' => sub {
    my $res = $t->get_ok(
        '/api/admin/email-templates?q={"page":1}' => { Accept => 'text/json' } );
    $res->status_is(200);
    $res->json_has('/pager')->json_has('/emailtemplates');
    $res->json_is('/pager/current_page' => 1);
    $res->json_is('/emailtemplates/0/name' => 'a_unique_name_0');
    $res->json_is('/emailtemplates/9/name' => 'a_unique_name_17');
};

subtest 'with page 2' => sub {
    my $res = $t->get_ok(
        '/api/admin/email-templates?q={"page":2}' => { Accept => 'text/json' } );
    $res->status_is(200);
    $res->json_has('/pager')->json_has('/emailtemplates');
    $res->json_is('/pager/current_page' => 2);
    $res->json_is('/emailtemplates/0/name' => 'a_unique_name_18');
    $res->json_is('/emailtemplates/9/name' => 'a_unique_name_8');
};

subtest 'with page 3' => sub {
    my $res = $t->get_ok(
        '/api/admin/email-templates?q={"page":3}' => { Accept => 'text/json' } );
    $res->status_is(200);
    $res->json_has('/pager')->json_has('/emailtemplates');
    $res->json_is('/pager/current_page' => 3);
    $res->json_is('/emailtemplates/0/name' => 'a_unique_name_9');
    $res->json_is('/emailtemplates/9/name' => undef);
};

subtest 'with page 4 (there is not 4th page)' => sub {
    my $res = $t->get_ok(
        '/api/admin/email-templates?q={"page":4}' => { Accept => 'text/json' } );
    $res->status_is(200);
    $res->json_is('/pager/current_page' => 3);
    $res->json_has('/pager')->json_has('/emailtemplates');
    $res->json_is('/emailtemplates/' => undef);
};



subtest 'email template with rules' => sub {

    # update a template with some rules.
    my $res = $t->put_ok(
        "/api/admin/email-templates/3" => { Accept => 'text/json' },
        form                            => {
            q => JSON::encode_json(
                {
                    templateName  => "a_unique_name_0",
                    emailReplyto  => 'little_silhouetto@of-a.man',
                    emailBcc      => undef,
                    emailSubject  => "a subject line",
                    emailBody     => "this does not matter",
                    templateRules => [
                        { id => JSON::null, role_name => "ResponseReceived" },
                        { id => JSON::null, role_name => "ResponseFlagged", flag_id => 1 }
                    ],
                }
            ),
            ts => time
        }
      )->status_is(200);
    $res->json_is('/id' => 3);
    $res->json_is('/message' => 'Your template "a_unique_name_0" has been updated');

    # now get the templates again and see if the template has been updated.
    $res = $t->get_ok( '/api/admin/email-templates' => { Accept => 'text/json' } );
    $res->status_is(200);
    $res->json_is('/emailtemplates/0/name' => 'a_unique_name_0');
    # rule data check
    $res->json_is('/emailtemplates/0/rules_data/0/id' => 1 );
    $res->json_is('/emailtemplates/0/rules_data/0/role_name' => 'ResponseReceived');
   
    $res->json_is('/emailtemplates/0/rules_data/1/id' => 2 );
    $res->json_is('/emailtemplates/0/rules_data/1/role_name' => 'ResponseFlagged');
    $res->json_is('/emailtemplates/0/rules_data/1/flag_id' => 1 );
 
    # check for flag association
    $res = $t->get_ok( '/api/admin/response-flags' => { Accept => 'text/json' } );
    $res->json_is('/data/flags/0/has_template_association' => 1, 'red flag has an association' );
    $res->json_hasnt('/data/flags/1/has_template_association', 'Amber flag has no association');
};


done_testing;

use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;

my $juice_mock = Test::MockModule->new('Bean::Juice::APIClient::User');
$juice_mock->mock( oversees => sub { []; } );

my $t = Test::Mojo::Stream2->new;
my $stream2 = $t->app->stream2();

ok( $stream2->factory('SearchUser') );

my $admin_user = $stream2->factory('SearchUser')->new_result({
    contact_details => { contact_name => "Keith" },
    provider        => 'adcourier',
    provider_id     => 1,
    group_identity  => 'company name',
    navigation      => [],
    stream2         => $t->app->stream2,
    id              => 1,
    is_admin        => 1,
    is_overseer     => 1,
    last_login_provider => 'adcourier'
});


$t->login_ok;
$t->get_ok('/api/admin/savedsearches')
  ->status_is(403)
  ->json_is({ error => { message => 'Only overseers can access this' } });

# Inject the admin user.
$t->app->helper( 'current_user' => sub{ return $admin_user } );

$t->get_ok('/api/admin/savedsearches')
  ->status_is(200)
  ->json_has('/pager')
  ->json_has('/searches');

done_testing();

use feature qw/state/;

use Test::Most;
use Test::Mojo::Stream2;
use JSON ();

# Prove the admin API enforces the maximum allowed number of custom flags

note 'Create test fixtures...';
my $t     = Test::Mojo::Stream2->new( { stream2_class => 'Test::Stream2' } );
my $s2    = $t->app->stream2();
my $admin = $s2->factory('SearchUser')->create( {
    contact_details => {
        contact_name  => 'Ren',
        contact_email => 'ren@renandstimpy.com'
    },
    provider            => 'adcourier',
    provider_id         => 1,
    group_identity      => 'Acme',
    stream2             => $s2,
    is_admin            => 1,
    is_overseer         => 1,
    locale              => 'en',
    last_login_provider => 'adcourier'
} );
$admin->save();
$t->app->helper( current_user => sub { return $admin } );

note 'Create the maximum allowed number of custom flags';
make_custom_flag()->status_is(200)
    for ( 1 .. $t->app->config->{custom_flags_limit} );

note 'The next attempt to make a custom flag should fail';
make_custom_flag()->status_is(400);

done_testing();

sub make_custom_flag {
    state $epoch_millis = time * 1000;
    return $t->post_ok(
        '/api/admin/response-flags',
        form => {
            q => JSON::encode_json(
                {
                    colour_hex_code => '#FFFFFF',
                    description     => 'a test flag'
                }
            ),
            ts => $epoch_millis
        }
    );
}
#! perl -w

use Test::Most;
use Test::Mojo::Stream2;
use JSON qw();

my $t = Test::Mojo::Stream2->new( { stream2_class => 'Test::Stream2' } );
$t->login_ok;
my $s2 = $t->app->stream2();

my $user = $s2->factory('SearchUser')->create(
    {
        contact_details => {
            contact_name  => "Jason",
            contact_email => 'Jason@JSON_breakersyard.com'
        },
        provider            => 'adcourier',
        provider_id         => 1,
        group_identity      => 'jsonbrakers',
        navigation          => [],
        stream2             => $s2,
        is_admin            => 1,
        is_overseer         => 1,
        locale              => 'en',
        last_login_provider => 'adcourier',
    }
);
$user->save();

$t->app->helper( 'current_user' => sub { return $user } );

my $set_resaons;

# lets add 10 resasons
for my $id ( 0 .. 9 ) {
    $t = add_flag_rejection_reason($t,1,"omg you are too good \N{U+2661} $id");
    $t->status_is(200)->json_has('rejectionId');
    $set_resaons->{1}->{$t->tx->res->json->{reasonId}}++;
}

is( scalar keys %{ $set_resaons->{1} },
    10, "the rejection reason ids should be unique, hence 10 of them" );

# # test if we can create some more, and fail
$t = add_flag_rejection_reason($t,1,"omg you are too good 'gobang'");
$t->status_is(400)->json_is(
    '/error/message' => 'You have reached the limit rejection reasons.' );


my $first_key = (keys %{$set_resaons->{1}})[0];
$t = delete_flag_rejection_reason($t,1,$first_key);
$t->status_is(200)->json_has('rejectionId');

# # try and add a reason that is too long (more than 50 chars)
$t = add_flag_rejection_reason($t,1,'omg' x 20);
$t->status_is(400)->json_is(
    '/error/message' => 'The rejection reason needs to be less than 50 charactors long.' );

# # adding one should be be fine
$t = add_flag_rejection_reason($t,1,"omg you are too good 'go through'");
$t->status_is(200)->json_has('rejectionId');

# # test if we can create some more, and fail
$t = add_flag_rejection_reason($t,1,"go bang as attempting to have 11");
$t->status_is(400)->json_is(
    '/error/message' => 'You have reached the limit rejection reasons.' );



sub add_flag_rejection_reason {
    my ($t,$flag_id,$rejection_reason) = @_;
    $t->post_ok(
        "/api/admin/add-flag-rejection-reason" => { Accept => 'text/json' },
        form                                   => {
            q => JSON::encode_json(
                {
                    flagId => $flag_id,
                    reason => { 'label' => $rejection_reason },
                }
            ),
            ts => time
        }
    );
    return $t;
}


sub delete_flag_rejection_reason {
    my ($t,$flag_id,$reasonId) = @_;
    $t->post_ok(
        "/api/admin/delete-flag-rejection-reason" => { Accept => 'text/json' },
        form                                   => {
            q => JSON::encode_json(
                {
                    flagId => $flag_id,
                    reasonId => $reasonId,
                }
            ),
            ts => time
        }
    );
    return $t;
}


done_testing;

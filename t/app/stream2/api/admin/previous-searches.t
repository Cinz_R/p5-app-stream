#! perl -w

use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;

# use Carp::Always;

my $t = Test::Mojo::Stream2->new;
$t->login_ok;
my $s2 = $t->app->stream2();

# test that we correctly enrich error-calendar records

my $user = $s2->factory('SearchUser')->create({
                                                              contact_details => { contact_name => "Patrick", contact_email => 'patrick@broadbean.com' },
                                                              provider        => 'adcourier',
                                                              last_login_provider => 'adcourier',
                                                              provider_id     => 1,
                                                              group_identity  => 'pat',
                                                              navigation      => [],
                                                              stream2         => $s2,
                                                              is_admin        => 0,
                                                              is_overseer     => 0,
                                                              locale          => 'en',
                                                             });
$user->save();
# Inject the admin user.
$t->app->helper( 'current_user' => sub{ return $user } );

$s2->factory('Board')->create({ name => 'flickr_xray', nice_name => 'Flickr X-Ray', type => 'social' });

my $criteria_ref = {
    10 => {
        id => 10,
        tokens => {
            keywords => "PHP Developer",
            talentsearch_industry => 14,
        },
        keywords => "PHP Developer",
        insert_datetime => "2014-04-01T14:00:00Z",
        update_datetime => "2014-04-01T14:00:00Z",
    },
    20 => {
        id => 20,
        tokens => {
            keywords => "",
            location_text => "London",
        },
        keywords => "",
        insert_datetime => "2014-04-01T14:00:00Z",
        update_datetime => "2014-04-01T14:00:00Z",
    }
};
my @tests = (
    {
        criteria => $criteria_ref->{10},
        nice_name => 'Talent Search',
        board_specific => [
            {
                'value' => 'Nice Name',
                'label' => 'Industry',
                'id' => 'talentsearch_industry'
            }
        ],
        user => {
            email => 'patrick@broadbean.com',
            name => 'Patrick',
            label => 'Patrick',
            id => 2
        }
    },
    {
        criteria => $criteria_ref->{20},
        nice_name => 'Talent Search',
        board_specific => [],
        user => {
            email => 'patrick@broadbean.com',
            name => 'Patrick',
            label => 'Patrick',
            id => 2
        }
    }
);

# create criterias
map {
    $s2->stream_schema->resultset('Criteria')->create( $criteria_ref->{$_} )
} keys %$criteria_ref;

my $test_ref = {
    'hits' => {
        'hits' => [
        {
            '_source' => {
                'user_provider_id' => '150780',
                'user_provider' => 'adcourier',
                'destination' => 'talentsearch',
                'stream2_base_url' => 'https://192.168.1.213:3001/',
                's3_log_id' => 'EBB1A9DA-C1A9-11E4-B0B9-563DDBDC46D8',
                'duration' => 0,
                'watchdog' => 0,
                'criteria_id' => '10',
                'user_stream2_id' => $user->id,
                'error_type' => 'LOGIN ERROR',
                'company' => $user->group_identity,
                'time_completed' => '2015-03-03T13:33:47'
            },
        },
        {
            '_source' => {
                'user_provider_id' => '150780',
                'user_provider' => 'adcourier',
                'destination' => 'talentsearch',
                'stream2_base_url' => 'https://192.168.1.213:3001/',
                's3_log_id' => 'EBB1A9DA-C1A9-11E4-B0B9-563DDBDC46D8',
                'duration' => 0,
                'watchdog' => 0,
                'criteria_id' => '20',
                'user_stream2_id' => $user->id,
                'error_type' => 'LOGIN ERROR',
                'company' => $user->group_identity,
                'time_completed' => '2015-03-03T13:33:47'
            },
        },
        ],
    },
    'facets' => {
        'destination' => {
            'terms' => [
            {
                'count' => 2,
                'term' => 'flickr_xray'
            },
            {
                'count' => 3,
                'term' => 'talentsearch'
            }
            ],
        },
        'user' => {
            'terms' => [
            {
                'count' => 1,
                'term' => $user->id
            }
            ],
        }
    },
};

my $feed_module = Test::MockModule->new('Stream::TemplateLoader');
$feed_module->mock( list_tokens => sub {
    return (
        {
            Id => 'talentsearch_industry',
            Options => [["Nice Name", 14]],
            Label => 'Industry',
        }
    );
});

my $controller_module = Test::MockModule->new( 'Mojolicious::Controller' );
$controller_module->mock( url_for => sub {
    return Mojo::URL->new( "http://www.example.com" );
});

my $date_module = Test::MockModule->new( 'DateTime' );
$date_module->mock( 'iso8601' => sub { return "sausages"; } );

use App::Stream2::Api::Admin;
my $controller = App::Stream2::Api::Admin->new( app => $t->app );
my $out = $controller->_enrich_previous_searches( $test_ref );

ok( scalar(@{$out->{results}}), "actual results are returned" );
for ( my $ti = 0; $ti < scalar(@{$out->{results}}); $ti++ ){
    is_deeply( $out->{results}->[$ti]->{tokens}, $tests[$ti]->{criteria}, "criteria enrichilization is successful" );
    is_deeply( $out->{results}->[$ti]->{board_specific}, $tests[$ti]->{board_specific}, "board token vivification is correct" );
    is_deeply( $out->{results}->[$ti]->{user}, $tests[$ti]->{user}, "user vivification is correct" );
    is_deeply( $out->{results}->[$ti]->{nice_name}, $tests[$ti]->{nice_name}, "result contains the nice_name of board" );
}

is($out->{facets}->{destination}->[0]->{label}, 'Flickr X-Ray', 'the destination facets label is the nice_name');

# no users specified in filter, so the controller will generate one that
# contains the IDs of all the users the current user oversees. That means we
# expect this structure:
# {
#     bool => {
#         must => [
#             {
#                 term => { "stream2_base_url" => 'http://www.example.com' }
#             },
#             {
#                 term => { "watchdog" => 0 }
#             },
#             {
#                 bool => {
#                     should => [
#                         { term => { user_stream2_id => 2 } },
#                         { term => { user_stream2_id => 15 } },
#                         { term => { user_stream2_id => 17 } },
#                         ...
#                     ]
#                 }
#             },
#             {
#                 range => {
#                     time_completed => {
#                         gte => "sausages"
#                     }
#                 }
#             }
#         ]
#     }
# };

# This user does not over see anyone and doesn't need to query juice for that.
my $adc_auth_provider =
  Test::MockModule->new( 'App::Stream2::Plugin::Auth::Provider::Adcourier' );
$adc_auth_provider->mock( 'user_oversees' => sub { return; } );

my $generated_filter = $controller->_build_filter();

ok keys( $generated_filter ) == 1 && exists $generated_filter->{bool},
    "Generated filter contains a single entry keyed by 'bool'";

ok scalar @{ $generated_filter->{bool}->{must} } == 4,
    "Generated filter contains 4 'must' criteria";

# now delete the "bool" item from the "must" items so we can do an is_deeply
# on the rest


my $must_items = $generated_filter->{bool}->{must};

is_deeply $generated_filter, my $filter_test_ref = {
    bool => {
        must => [
            {
                term => { "stream2_base_url" => 'http://www.example.com' }
            },
            {
                term => { "watchdog" => 0 }
            },
            {
                range => { time_completed => { gte => "sausages" } }
            },
            {
                'terms' => {
                    'user_stream2_id' => [
                        $user->id()
                    ]
                }
            }
        ]
    }
}, "Generated filter's structure is correct";

my $query_ref = {
    user_id     => 999999,
    destination => "ELEPHANTS!!!!!!!!!"
};
$controller->stash(query_ref => $query_ref);
push @{$filter_test_ref->{bool}->{must}},
    (
        { term => { user_stream2_id => 999999 } },
        { term => { destination => "ELEPHANTS!!!!!!!!!" } }
    );

throws_ok(sub{ $controller->_build_filter() }, qr/You do not have permission/, "Invalid parameters just make it die" );

done_testing;

use Test::Most;
use Test::Mojo::Stream2;

use Log::Log4perl qw/:easy/;
# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log4perl');
#Log::Any::Adapter->set('Stderr');

# TEST SETUP
my $t = Test::Mojo::Stream2->new();

#  some valid codes
for my $code (200,403,404,500,502,503,504){
$t->get_ok("/api/errorcode/$code")
    ->status_is($code);
}

#  invalid codes
for my $code (20, 2000, 'abc','abc200', '400abc','abc200xyz'){
$t->get_ok("/api/errorcode/$code")
    ->status_is(404);
}

done_testing();
1;
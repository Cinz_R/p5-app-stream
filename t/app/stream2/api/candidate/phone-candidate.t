use Test::Most;
use Data::Dumper;
use MIME::Base64;
use Test::Mojo::Stream2;
use Test::MockModule;

my $test = {
    candidate  => {
        destination => 'dummy',
        name        => 'John Smith',
        email       => 'john@example.com',
    },
    subscription_ref => {
        dummy => {
            nice_name => 'Dummy'
        }
    },
    board_nice_name       => 'Dummy',
};

my $t = Test::Mojo::Stream2->new({stream2_class => 'Test::Stream2'});
$t->login_ok;

my $cand_api_module = Test::MockModule->new('App::Stream2::Api::Candidate');

$cand_api_module->mock(
    'load_candidate' => sub {
        my ($self) = @_;
        my $candidate = Stream2::Results::Result->new( $test->{candidate} );
        $self->stash( 'candidate' => $candidate );
        my $user = $self->current_user;
        $user->subscriptions_ref( $test->{subscription_ref} );
        my $user_ref = {
            user_id        => $user->id,
            group_identity => $user->group_identity,
            auth_tokens    => {},
        };
        $self->stash( 'current_user', $user );
        # We also need the subscription (board) info.
        $self->stash( 'board_name', $test->{candidate}->{destination} );
        $self->stash( 'subscription_info',
            { nice_name => $test->{board_nice_name} } );
        return 1;
    }
);

my $phoned = 0;

# Replace the queue_job helper to return a fake job
$t->app->helper(
    queue_job => sub {
        $phoned = 1; 
        return test_job->new( {} );
    }
);

# this actually starts the process
$t->post_ok( '/results/1/dummy/candidate/1/phonecall' => { Accept => 'text/json' } )->status_is(200);

is( $phoned, 1, 'OK queue_job was called' );


{

    package test_job;
    sub new { bless $_[1], $_[0]; }

    sub result {
        return { result => 'does not matter' };
    }
    sub guid { 1; }
}

done_testing;
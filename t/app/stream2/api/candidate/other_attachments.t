use Test::Most;
use Data::Dumper;
use JSON qw/decode_json/;
use Test::Mojo::Stream2;
use Test::MockModule;

use JSON;
use HTTP::Response;
use Stream2::Results::Result;
use Email::Abstract;
use Promises qw/ deferred /;

my $test = {
    cv_content => "TEST CV",
    recipients => ['patrick@example.com'],
    candidate  => {
        destination => 'talentsearch',
        name        => 'John Smith',
        email       => 'john@example.com',
        cv_html     => 'TEST CV',
    },
    subscription_ref => {
        dummy => {
            nice_name => 'Dummy'
        }
    },
    board_nice_name       => 'Dummy',
    candidate_has_cv      => 1,
    candidate_has_profile => 1,
    forward_bcc           => 'test@example.com',
    forward_subject       => '[Talent Search] John Smith',
    profile_body          => 'THIS IS THE PROFILE BODY',
    candidate_has_cv      => 1,
    candidate_has_profile => 1,
};

my $t = Test::Mojo::Stream2->new();
$t->login_ok();

my $email_sent = 0;

my $cand_api_module = Test::MockModule->new('App::Stream2::Api::Candidate');

$cand_api_module->mock(
    'load_candidate' => sub {
        my ($self) = @_;
        my $candidate = Stream2::Results::Result->new( $test->{candidate} );
        $self->stash( 'candidate' => $candidate );
        my $user = $self->current_user;
        $user->subscriptions_ref( $test->{subscription_ref} );
        my $user_ref = {
            user_id        => $user->id,
            group_identity => $user->group_identity,
            auth_tokens    => {},
        };
        $self->stash( 'current_user', $user );
        # We also need the subscription (board) info.
        $self->stash( 'board_name', $test->{candidate}->{destination} );
        $self->stash( 'subscription_info',
            { nice_name => $test->{board_nice_name} } );
        return 1;
    }
);

# Replace the queue_job helper to return a fake job
$t->app->helper(
    queue_job => sub {
        return test_job->new( {} );
    }
);

# this actually starts the process
$t->get_ok( '/results/1/dummy/candidate/1/other_attachments' =>
      { Accept => 'text/json' } )->status_is(200)
  ->content_like(qr/{"status_id":1}/);

{

    package test_job;
    sub new { bless $_[1], $_[0]; }

    sub result {
        return { result => 'does not matter' };
    }
    sub guid { 1; }
}

done_testing;

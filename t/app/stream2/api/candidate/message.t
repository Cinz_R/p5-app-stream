use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;
use JSON ();

my $t = Test::Mojo::Stream2->new({stream2_class => 'Test::Stream2'});

my $cand_api_module = Test::MockModule->new('App::Stream2::Api::Candidate');
$cand_api_module->mock('_with_cv' => sub {
    my ( $self, $success ) = @_;
    my $response = HTTP::Response->new(
        200, "OK", 
        [ "Content-Type" => "text/xml", "Content-Disposition" => q{inline; filename="file.txt"} ],
        "Such Content"
    );
    my $res = eval{ &$success($response); };
});
$cand_api_module->mock('load_candidate' => sub {
    my ( $self ) = @_;
    my $candidate = Stream2::Results::Result->new(
        {
            destination     => 'talentsearch',
            name            => 'John Smith',
            email           => 'john@example.com',
            cv_html         => 'TEST CV',
        }
    );

    $self->stash('candidate' => $candidate);

    # Put the 'job_user_ref' too, we'll need it.
    # By the way, the fact that this extends App::Stream2::Controller::Authenticated
    # means the current user will ALWAYS be there.
    my $user = $self->current_user;
    my $user_ref = {
        user_id         => $user->id,
        group_identity  => $user->group_identity,
        auth_tokens     => {},
    };

    # Also stash the current user, as calling 'current_user' is expensive.
    $self->stash('current_user' , $user );
    $self->stash('job_user_ref' , $user_ref);
    # We also need the subscription (board) info.
    $self->stash('board_name', 'talentsearch');
    $self->stash('subscription_info' , { nice_name => 'Talentsearch' } );
    return 1;
});

$t->login_ok();

$t->post_ok(
    '/results/1/talentsearch/candidate/1/message',
    { Accept => 'application/json' },
    form => {
        q => JSON::encode_json({
            messageEmail            => 'john@example.com',
            messageContent          => '', # NO CONTENT
            messageSubject          => 'This is the subject',
            messageSubjectPrefix    => 'Such prefix',
            messageBcc              => '',
            advertId                => 123,
            advertList              => 'adc_shortlist',
            emailTemplateId         => 543,
            emailTemplateName       => 'Name of template'
        })
    }
)->status_is(500)->content_like( qr/Message content is required/ );

$t->post_ok(
    '/results/1/talentsearch/candidate/1/message',
    { Accept => 'application/json' },
    form => {
        q => JSON::encode_json({
            messageEmail            => 'john@example.com',
            messageContent          => 'This is the content',
            messageSubject          => '', # No subject
            messageSubjectPrefix    => 'Such prefix',
            messageBcc              => '',
            advertId                => 123,
            advertList              => 'adc_shortlist',
            emailTemplateId         => 543,
            emailTemplateName       => 'Name of template'
        })
    }
)->status_is(500)->content_like( qr/Message subject is required/ );

$t->post_ok(
    '/results/1/talentsearch/candidate/1/message',
    { Accept => 'application/json' },
    form => {
        q => JSON::encode_json({
            messageEmail            => '', # no email
            messageContent          => 'This is the content',
            messageSubject          => 'This is the subject',
            messageSubjectPrefix    => 'Such prefix',
            messageBcc              => '',
            advertId                => 123,
            advertList              => 'adc_shortlist',
            emailTemplateId         => 543,
            emailTemplateName       => 'Name of template'
        })
    }
)->status_is(500)->content_like( qr/Sender email address is required/ );

my $queue_job_called = 0;
$t->app->helper( queue_job => sub {
    $queue_job_called = 1;
    my ( $self, %args ) = @_;
    ok( delete $args{parameters}->{base_url} );
    is_deeply(
        \%args, 
        {
            parameters => {
                'message_email'         => 'john@example.com',
                'ripple_settings'       => {
                    'login_record_id'   => '1',
                    'custom_lists'      => [
                        {
                            'name'  => 'vanilla_shortlist',
                            'label' => 'Shortlist'
                        }
                    ],
                    'custom_fields' => [
                    ]
                        ,
                    'login_provider'    => 'demo'
                },
                'message_subject'       => 'This is the subject',
                'results_id'            => '1',
                'email_template_name'   => 'Name of template',
                'advert_id'             => 123,
                'remote_ip'             => '127.0.0.1',
                'message_content'       => 'This is the content',
                'message_bcc'           => '',
                'list_name'             => 'adc_shortlist',
                'candidate_idx'         => '1',
                'user'                  => {
                    'auth_tokens'       => {},
                    'user_id'           => 1,
                    'group_identity'    => 'andy'
                },
                'subject_prefix'        => 'Such prefix',
                'email_template_id'     => 543
            },
            'class'                   => 'Stream2::Action::MessageCandidate',
            'queue'                   => 'ForegroundJob'
        }
    );
    return MyJob->new( 'abcdef' );
});

$t->post_ok(
    '/results/1/talentsearch/candidate/1/message',
    { Accept => 'application/json' },
    form => {
        q => JSON::encode_json({
            messageEmail            => 'john@example.com',
            messageContent          => 'This is the content',
            messageSubject          => 'This is the subject',
            messageSubjectPrefix    => 'Such prefix',
            messageBcc              => '',
            advertId                => 123,
            advertList              => 'adc_shortlist',
            emailTemplateId         => 543,
            emailTemplateName       => 'Name of template'
        })
    }
)->status_is(200)
->json_is( { status_id => 'abcdef' } );
ok( $queue_job_called );

$queue_job_called = 0;
$t->app->helper( queue_job => sub {
    $queue_job_called = 1;
    my ( $self, %args ) = @_;
    ok( delete $args{parameters}->{base_url} );
    is_deeply(
        \%args, 
        {
            parameters => {
                'message_email'         => '[consultant_email]',
                'ripple_settings'       => {
                    'login_record_id'   => '1',
                    'custom_lists'      => [
                        {
                            'name'  => 'vanilla_shortlist',
                            'label' => 'Shortlist'
                        }
                    ],
                    custom_fields => [],
                    'login_provider'    => 'demo'
                },
                'message_subject'       => 'This is the subject',
                'results_id'            => '999',
                'remote_ip'             => '127.0.0.1',
                'message_content'       => 'This is the content',
                'message_bcc'           => 'test@example.com',
                'candidate_idx'         => '888',
                'advert_id'             => undef,
                'list_name'             => undef,
                'user'                  => {
                    'auth_tokens'       => {},
                    'user_id'           => 1,
                    'group_identity'    => 'andy'
                },
                'subject_prefix'        => 'Such prefix',
            },
            'class'                   => 'Stream2::Action::MessageCandidate',
            'queue'                   => 'ForegroundJob'
        }
    );
    return MyJob->new( 'abcdef' );
});

$t->post_ok(
    '/results/999/talentsearch/candidate/888/message',
    { Accept => 'application/json' },
    form => {
        q => JSON::encode_json({
            messageEmail            => '[consultant_email]',
            messageContent          => 'This is the content',
            messageSubject          => 'This is the subject',
            messageSubjectPrefix    => 'Such prefix',
            messageBcc              => 'test@example.com',
        })
    }
)->status_is(200)
->json_is( { status_id => 'abcdef' } );
ok( $queue_job_called );

done_testing();

{
    package MyJob;
    sub guid { shift->{status_id}; }
    sub new { bless { status_id => pop }, shift; }
}

use Test::Most;
use Data::Dumper;
use JSON qw/decode_json/;
use Test::Mojo::Stream2;
use Test::MockModule;

use JSON;
use HTTP::Response;
use Stream2::Results::Result;
use Email::Abstract;
use Promises qw/ deferred /;

my $defaults = {
    cv_content  => "TEST CV",
    recipients  => [ 'patrick@example.com' ],
    candidate   => {
        destination     => 'talentsearch',
        name            => 'John Smith',
        email           => 'john@example.com',
        cv_html         => 'TEST CV',
    },
    subscription_ref        => {
        talentsearch => {
            nice_name   => 'Talent Search'
        }
    },
    board_nice_name         => 'Talent Search',
    candidate_has_cv        => 1,
    candidate_has_profile   => 1,
    forward_bcc             => 'test@example.com',
    forward_subject         => '[Talent Search] John Smith',
    profile_body            => 'THIS IS THE PROFILE BODY',
};
my $tests = [ map {
    { %$defaults, %$_ }
} @{[
    {
        candidate_has_cv => 1,
        candidate_has_profile => 1,
    },
    {
        candidate_has_cv => 0,
        candidate_has_profile => 1,
    },
    {
        candidate_has_cv => 0,
        candidate_has_profile => 0,
    }
]} ];

my $t = Test::Mojo::Stream2->new();
$t->login_ok();


foreach my $test ( @$tests ){

    my $email_sent = 0;

    #######################
    # LOAD CANDIDATE + CV #
    #######################
    my $cand_api_module = Test::MockModule->new('App::Stream2::Api::Candidate');

    $cand_api_module->mock('_with_cv' => sub {
        my ( $self, $success ) = @_;
        my $response = HTTP::Response->new( 200, "OK", 
            [ "Content-Type" => "text/xml", "Content-Disposition" => q{inline; filename="file.txt"} ],
            $test->{cv_content} );
        my $res = eval{ &$success($response); };
    });
    $cand_api_module->mock('load_candidate' => sub {
        my ( $self ) = @_;
        my $candidate = Stream2::Results::Result->new(
            $test->{candidate}
        );
        $candidate->has_cv( $test->{candidate_has_cv} );
        $candidate->has_profile( $test->{candidate_has_profile} );

        $self->stash('candidate' => $candidate);

        # Put the 'job_user_ref' too, we'll need it.
        # By the way, the fact that this extends App::Stream2::Controller::Authenticated
        # means the current user will ALWAYS be there.
        my $user = $self->current_user;
        $user->subscriptions_ref( $test->{subscription_ref} );
        my $user_ref = {
                        user_id         => $user->id,
                        group_identity  => $user->group_identity,
                        auth_tokens     => {},
                       };

        # Also stash the current user, as calling 'current_user' is expensive.
        $self->stash('current_user' , $user );
        $self->stash('job_user_ref' , $user_ref);
        # We also need the subscription (board) info.
        $self->stash('board_name' , $test->{candidate}->{destination});
        $self->stash('subscription_info' , { nice_name => $test->{board_nice_name} } );
        return 1;
    });

    ##############
    # SEND EMAIL #
    ##############
    my $stream2_module = Test::MockModule->new('Stream2');
    $stream2_module->mock( 'send_email' => sub {
        my ( $self, $email ) = @_;

        my $abstract_email = Email::Abstract->new($email);
        is( $abstract_email->get_header('To'), join( ' ,', @{$test->{recipients}}), "final email has matching bcc" );
        is( $abstract_email->get_header('Subject'), $test->{forward_subject}, "final email has correct subject" );

        if ( $test->{candidate_has_cv} ) {
            my $body = MIME::Base64::decode_base64( join( '', @{$email->parts(1)->body} ) );
            is ( $body, $test->{cv_content}, "cv has been attached" );
        }
        elsif ( $test->{candidate_has_profile} ){
            my $body = join( '', @{$email->parts(0)->body} );
            ok( $body =~ m/candidate_profiles/, "candidate profile is rendered in the email body" );
            ok( $body =~ m/a very fine/ , "Ok good custom text in candidate with profile");
        }
        else {
            my $body = join( '', @{$email->parts(0)->body} );
            ok( $body =~ m/results\/body\//, "candidate result body is rendered in the email body" );
            ok( $body =~ m/a very fine/ , "Ok good custom text in general");
        }

        $email_sent = 1;
    });

    $t->app->helper(log_action => sub { 1; } );
    $t->app->helper(do_job => sub {
        my $d = deferred;
        $d->resolve( test_job->new({
            result => JSON::encode_json( $test->{candidate} )
        }) );
        return $d;
    });

    # Replace the queue_job helper to return a fake job
    $t->app->helper(queue_job => sub {
                        return test_job->new({});
                    });

    my $demo_provider_module = Test::MockModule->new('App::Stream2::Plugin::Auth::Provider::Demo');
    $demo_provider_module->mock( 'advert_shortlist_candidate' => sub {
        my $self = shift;
        $t->app->auth_provider({ provider => $test->{provider} })->advert_shortlist_candidate( @_ );
    });

    # this actually starts the process
    $t->post_ok( '/results/1/talentsearch/candidate/1/forward' => { Accept => 'text/json' },
        form => { q => JSON::encode_json({ recipients => $test->{recipients} , message => 'This is a very fine candiidate'  }) }
    )->status_is(200);

}

{
    package test_job;
    sub new { bless $_[1], $_[0]; }
    sub result { { result => shift->{result} }; }
    sub guid { 1; }
}

done_testing;

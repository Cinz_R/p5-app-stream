use Test::Most;

use Test::Mojo::Stream2;
use JSON;

my $t = Test::Mojo::Stream2->new;
$t->login_ok();

my $s2 = $t->app->stream2();

my $user = $s2->factory('SearchUser')->create(
    {   contact_details => {
            contact_name  => "Jason Jr",
            contact_email => 'Jasonjr@JSON_breakersyard.com'
        },
        provider            => 'adcourier',
        provider_id         => 2,
        group_identity      => 'jsonbrakers',
        navigation          => [],
        stream2             => $s2,
        is_admin            => 0,
        is_overseer         => 0,
        locale              => 'en',
        last_login_provider => 'adcourier',
    }
);
$user->save();
$t->app->helper( 'current_user' => sub { return $user } );

$s2->factory('AdcresponsesCustomFlag')->create({
    group_identity => 'jsonbrakers',
    description => 'Custom Flag 1',
    colour_hex_code => '#ff00eee'
});

$t->get_ok('/api/user/adcresponse_flags')
    ->status_is(200);
my $content = $t->tx->res->json;

is ( scalar( @{$content->{data}->{adcresponse_flags}} ), 7, '7 flags ahev been returned' );
is_deeply(
    $content->{data}->{adcresponse_flags}->[2],
    {
        'setting_mnemonic' => 'responses-flagging-enable_green_flag',
        'flag_id' => '5',
        'colour_hex_code' => '#008000',
        'type' => 'standard',
        'description' => 'Green'
    },
    'A standard flag is returned'
);
is_deeply(
    $content->{data}->{adcresponse_flags}->[-1],
    {
        'setting_mnemonic' => 'adcresponses-custom-flag-20',
        'flag_id' => '20',
        'colour_hex_code' => '#ff00eee',
        'type' => 'custom',
        'description' => 'Custom Flag 1',
        'group_identity' => 'jsonbrakers'
    },
    'A custom flag is returned'
);

done_testing();

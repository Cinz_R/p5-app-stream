#! perl -w

use Test::Most;
use Test::Mojo::Stream2;
use JSON qw();
my $t = Test::Mojo::Stream2->new( { stream2_class => 'Test::Stream2' } );
$t->login_ok;

my $s2 = $t->app->stream2();

my $db_users = set_up_users($s2);
$t->app->helper('current_user' => sub { return $s2->find_user( $db_users->[0]->id ) } );

# A blank search
$t = query_colleagues($t,'')->status_is(200)->json_has('suggestions');
is (scalar @{$t->tx->res->json->{suggestions}},6, "there are 6 regular users");

# A blank search with zombies included.
$t = query_colleagues($t,'',1)->status_is(200)->json_has('suggestions');
is (scalar @{$t->tx->res->json->{suggestions}},7, "there are 6 regular users and a zombie");

# A full search
$t = query_colleagues($t,'yakko')->status_is(200)->json_has('suggestions');
is (scalar @{$t->tx->res->json->{suggestions}},1, "there is only one yakko");

# A parial search
$t = query_colleagues($t,'b')->status_is(200)->json_has('suggestions');
is (scalar @{$t->tx->res->json->{suggestions}},2, "user with a b");

# A parial search with zombies
$t = query_colleagues($t,'b',1)->status_is(200)->json_has('suggestions');
is (scalar @{$t->tx->res->json->{suggestions}},3, "users and zombies with b");

# A valid email from external channel
$t = query_colleagues($t,'otto@scratchansniff.com')->status_is(200)->json_has('suggestions');
is (scalar @{$t->tx->res->json->{suggestions}},0, "valid email from external channel not allowed");

# A valid email from responses
$t = query_colleagues($t,'otto@scratchansniff.com',0,1)->status_is(200)->json_has('suggestions');
is (scalar @{$t->tx->res->json->{suggestions}},1, "valid email from responses is allowed");

# change the current user to one with 'can_forward_to_any_email_address'
$t->app->helper('current_user' => sub { return $s2->find_user( $db_users->[1]->id ) } );

# A valid email from responses outside of new managed resposnes and can_forward_to_any_email_address on
$t = query_colleagues($t,'otto@scratchansniff.com',0,0)->status_is(200)->json_has('suggestions');
is (scalar @{$t->tx->res->json->{suggestions}},1, "valid email from responses is allowed via can_forward_to_any_email_address");


# next lot of tests has to do with the forwarding-restrict_recipient_domain setting.
my $setting = $s2->factory('Setting')->find({ setting_mnemonic => 'forwarding-restrict_recipient_domain' });


my $test_domains = [
    {
        name                    => 'no domain in search',
        test_address            => 'naffinator@google.com',
        suggestions_expected    => 0,
    },
    {
        name                    => 'no domain in managed responses',
        test_address            => 'naffinator@google.com',
        in_managed_responses    => 1,
        suggestions_expected    => 1,
    },
    {
        name                    => 'google.com domain and correct domain',
        test_address            => 'naffinator@google.com',
        white_list              => 'google.com',
        in_managed_responses    => 1,
        suggestions_expected    => 1,
    },
    {
        name                    => 'google.com domain and incorrect domain',
        test_address            => 'naffinator@the-google.com',
        white_list              => 'google.com',
        in_managed_responses    => 1,
        suggestions_expected    => 0,
    },
    {
        name                    => 'multiple domains (comma), correct domain',
        test_address            => 'naffinator@the-google.com',
        white_list              => 'google.com,the-google.com',
        in_managed_responses    => 1,
        suggestions_expected    => 1,
    },
    {
        name                    => 'multiple domains (space), correct domain',
        test_address            => 'naffinator@the-google.com',
        white_list              => 'google.com the-google.com',
        in_managed_responses    => 1,
        suggestions_expected    => 1,
    },
    {
        name                    => 'multiple domains (mixed), correct domain',
        test_address            => 'naffinator@fuzz-google.com',
        white_list              => 'google.com the-google.com,fuzz-google.com',
        in_managed_responses    => 1,
        suggestions_expected    => 1,
    },
    # now with a user who is not in managed resposnes, but can forward (basically the same).
    {
        name                    => 'google domain, correct domain with can_forward_to_any_email_address',
        setting_user_id         => 1,
        test_address            => 'naffinator@google.com',
        white_list              => 'google.com',
        suggestions_expected    => 1,
    },
    {
        name                    => 'multiple domains (space), correct domain with can_forward_to_any_email_address',
        setting_user_id         => 1,
        test_address            => 'naffinator@the-google.com',
        white_list              => 'google.com the-google.com',
        suggestions_expected    => 1,
    },
    {
        name                    => 'multiple domains (space), incorect domain with can_forward_to_any_email_address',
        setting_user_id         => 1,
        test_address            => 'naffinator@teh-google.com',
        white_list              => 'google.com the-google.com',
        suggestions_expected    => 0,
    },
    # now with some "random" capital charactors inputted domains
    {
        name                    => 'multiple domains (space), correct domain, odd capitals',
        setting_user_id         => 1,
        test_address            => 'naffinator@google.com',
        white_list              => 'gOOgle.com the-gOOgle.com',
        suggestions_expected    => 1,
    },
    {
        name                    => 'with current user exclusion',
        test_address            => '',
        exclude_current_user    => 1,
        suggestions_expected    => 5,
    },
];

for my $test (@$test_domains) {
    my $setting_user_id = $test->{setting_user_id} // 2;
    my $test_user_id = $db_users->[$setting_user_id]->id;

    # make sure we are testing against the correct user.
    $t->app->helper( 'current_user' =>
          sub { return $s2->find_user( $test_user_id ) } );

    # set the users white list.
    $setting->set_users_value( $db_users->[0], [$test_user_id],
        $test->{white_list} // ''  );

    # query the route.
    $t = query_colleagues(
        $t, $test->{test_address},
        $test->{include_zombies} // 0,
        $test->{in_managed_responses} // 0,
        $test->{exclude_current_user} // 0,
    )->status_is(200)->json_has('suggestions');

    # test we have the right number of suggestion
    is(
        scalar @{ $t->tx->res->json->{suggestions} },
        $test->{suggestions_expected},
        $test->{name}
    );

    #  test to see if we get the right domain back.
    if ( $test->{test_address} && $test->{suggestions_expected} ) {
        is( $t->tx->res->json->{suggestions}->[0]->{text},
            $test->{test_address},
            sprintf( "got back %s", $test->{test_address}) );
    }
}


sub set_up_users {
    my ($s2) = @_;
    my @users = (
        {
            contact_name  => 'pinky',
            contact_email => 'pink@narfindustries.com',
            office        => 'lab',
        },
        {
            contact_name  => 'brain',
            contact_email => 'brain@narfindustries.com',
            office        => 'lab',
        },
        {
            contact_name  => 'bender',
            contact_email => 'bender@planetexpress.com',
            office        => 'planetexpress',
        },
        {
            contact_name  => 'yakko',
            contact_email => 'yakko@watertowerindustries.com',
            office        => 'tower'
        },
        {
            contact_name  => 'wakko',
            contact_email => 'wakko@watertowerindustries.com',
            office        => 'tower'
        },
        {
            contact_name  => 'dot',
            contact_email => 'dot@watertowerindustries.com',
            office        => 'tower'
        },
        {
            contact_name  => 'bach',
            contact_email => 'bach@deadcomposersclub.com',
            office        => 'zombie'
        },
    );

    my @db_users;
    my $id = 0;

    for my $user (@users) {
        my $db_user = $s2->factory('SearchUser')->create(
            {
                contact_details => {
                    contact_name  => $user->{contact_name},
                    contact_email => $user->{contact_email},
                    office        => $user->{office},
                },
                provider            => 'adcourier',
                provider_id         => $id,
                group_identity      => 'animaniacs_and_co',
                navigation          => [],
                stream2             => $s2,
                is_admin            => 0,
                is_overseer         => 0,
                locale              => 'en',
                last_login_provider => 'adcourier',
                settings            => {
                    has_adcourier_v5 => 1,
                    $id == 1 ? (can_forward_to_any_email_address => 1) : (),
                },
            }
        );
        $id++;
        $db_user->save();

        push @db_users, $db_user;
    }

    return \@db_users;
}

sub query_colleagues {
    my ( $t, $search_term, $include_zombies, $is_responses, $exclude_current_user ) = @_;

    $t->get_ok(
        "/api/user/colleagues" => { Accept => 'text/json' },
        form                   => {
            q => JSON::encode_json(
                {
                    search => $search_term,
                    isResponses => $is_responses ? 1 : 0,
                    $include_zombies ? ( includeZombies => 1 ) : (),
                    $exclude_current_user ? ( excludeCurrentUser => 1 ) : (),
                }
            ),
            ts => time
        }
    );
    # warn JSON::encode_json($t->tx->res->json);
    return $t;
}

done_testing;

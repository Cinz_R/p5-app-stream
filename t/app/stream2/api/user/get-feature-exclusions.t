use Test::Most;
use Data::Dumper;
use JSON qw/decode_json/;
use Test::Mojo::Stream2;

my $t = Test::Mojo::Stream2->new();
$t->login_ok;
$t->get_ok( '/api/user/details' )
  ->status_is(200);

is_deeply decode_json( $t->tx->res->content->get_body_chunk(0) )->{details}->{feature_exclusions}, {}, 'No exclusions by default';

done_testing;

#! perl -w

use Test::Simple tests => 8;

use Test::Mojo::Stream2;
use JSON;

# load test stream2 to deploy quota schema
my $t = Test::Mojo::Stream2->new({stream2_class => 'Test::Stream2'});
$t->login_ok();

my $s2 = $t->app->stream2();
$s2->deploy_test_db();

my $user = $s2->factory('SearchUser')->create(
    {   contact_details => {
            contact_name  => "Jason Jr",
            contact_email => 'Jasonjr@JSON_breakersyard.com',
            username => 'jason@rakers.bakers.jsonbrakers',
        },
        provider            => 'adcourier',
        provider_id         => 2,
        group_identity      => 'jsonbrakers',
        navigation          => [],
        stream2             => $s2,
        is_admin            => 0,
        is_overseer         => 0,
        locale              => 'en',
        last_login_provider => 'adcourier',
    }
);
$user->subscriptions_ref({
    careerbuilder   => { nice_name => 'Careerbuilder', type => 'external' },
    recruitmentedge => { nice_name => 'Recruitment Edge', type => 'external' },
});
$user->save();
$t->app->helper( 'current_user' => sub { return $user } );

foreach my $board ( keys(%{$user->subscriptions_ref}) ) {
    foreach my $type ( qw/search-cv search-profile search-search/ ) {
        $s2->quota_object->create(
            {
                board => $board,
                type => $type,
                quota => [
                    {
                        path => $user->hierarchical_path,
                        remaining => 5
                    },
                ],
            },
            {
                actioned_by => 'batman',
                remote_addr => 'batcave',
            }
        );
    }
}

# get quota for single board
$t->get_ok('/api/user/quota' => form => { q => JSON::encode_json({ board => 'careerbuilder' }) })
    ->status_is(200)
    ->json_is('/careerbuilder/search-cv', 5);

# get quota for all subscriptions of the user
$t->get_ok('/api/user/quota')
    ->status_is(200)
    ->json_is('/careerbuilder/search-cv', 5)
    ->json_is('/recruitmentedge/search-profile', 5);

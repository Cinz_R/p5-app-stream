use Test::Most;

use Test::Mojo::Stream2;
use JSON;

my $t = Test::Mojo::Stream2->new;
$t->login_ok();

my $s2 = $t->app->stream2();

my $user = $s2->factory('SearchUser')->create(
    {   contact_details => {
            contact_name  => "Jason Jr",
            contact_email => 'Jasonjr@JSON_breakersyard.com'
        },
        provider            => 'adcourier',
        provider_id         => 2,
        group_identity      => 'jsonbrakers',
        navigation          => [],
        stream2             => $s2,
        is_admin            => 0,
        is_overseer         => 0,
        locale              => 'en',
        last_login_provider => 'adcourier',
    }
);
$user->save();

subtest 'Create many email templates' => sub {
    my $admin_user = $s2->factory('SearchUser')->create(
        {   contact_details => {
                contact_name  => "Jason Sn",
                contact_email => 'Jasonsn@JSON_breakersyard.com'
            },
            provider            => 'adcourier',
            provider_id         => 1,
            group_identity      => 'jsonbrakers',
            navigation          => [],
            stream2             => $s2,
            is_admin            => 1,
            is_overseer         => 1,
            locale              => 'en',
            last_login_provider => 'adcourier',
        }
    );
    $admin_user->save();

    $t->app->helper( 'current_user' => sub { return $admin_user } );

    my $groupsettings = $s2->factory('Groupsetting', { group_identity => 'jsonbrakers' } );

    for ( 1 .. 25 ) {
        $groupsettings->create({ setting_mnemonic => "email_template-$_-active",
               setting_description => "email template $_",
               default_boolean_value => 0,
               setting_type => 'boolean'
        });

        $t->post_ok(
            "/api/admin/email-templates" => { Accept => 'text/json' },
            form                         => {
                q => JSON::encode_json(
                    {   templateName => "$_ a_unique_name",
                        emailReplyto => undef,
                        emailBcc     => undef,
                        emailSubject => "a subject line",
                        emailBody    => "this does not matter",
                        templateRules => [],
                        permissionChanges => {
                            ( $_ % 2 ) ? ( allow => [$user->id()] ) : (),
                        }
                    }
                ),
                ts => time
            }
        )->status_is(200);
    }
};

$t->app->helper( 'current_user' => sub { return $user } );

$t->get_ok('/api/user/email-templates')->status_is(200)->json_has('/emailtemplates');
my $templates = $t->tx->res->json->{emailtemplates};
is(scalar(@$templates), 10, 'we have more than 10 templates for this user, but we should return 10 templates as the maximum (SEAR-1880)');

ok($templates->[0]->{id}, 'id fields are returned');
ok($templates->[0]->{name}, 'name fields are returned');

$t->get_ok('/api/user/email-templates' => form => { q => JSON::encode_json({ search => '2' }) })->status_is(200)->json_has('/emailtemplates');
$templates = $t->tx->res->json->{emailtemplates};
is(scalar(@$templates), 3, 'can query by template name');

done_testing();

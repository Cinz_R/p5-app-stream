use Test::Most;
use Test::Mojo::Stream2;

subtest "We get the search page for all parts of the search url" => sub {
  my $t = Test::Mojo::Stream2->new();
  $t->login_ok();

  my $example_url = '/search/_IpgMgEnP8rXjhn6ZGES2TE-AlQ/talentsearch/page/1';
  my @url_parts = split '/', $example_url;

  while ( scalar(@url_parts) ) {
    my $test_url = join('/', @url_parts);
    unless( $test_url ){
        last;
    }
    $t->get_ok($test_url)
      ->status_isnt(404)
      ->status_isnt(500)
      ->content_unlike(qr/Login/);

    pop @url_parts;
  }
};

done_testing();

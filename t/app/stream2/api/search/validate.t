use Test::Most;
use Test::Exception;

# a normal query should not die
lives_ok ( sub { 
    SearchTest->_valid_query({
        keywords => 'test'
    });
}, "empty query is valid" );

# check for salary from and salary to validity
dies_ok ( sub { 
    SearchTest->_valid_query({
        keywords => 'test',
        salary_from => 2,
        salary_to => 1
    });
}, "dies when salary from is higher than salary to" );

{
    package SearchTest;
    use base 'App::Stream2::Api::Search';
    sub __ { shift; return @_; }
}

done_testing();

#! perl -w
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;
use App::Stream2::Api::Tinymce;

my $t = Test::Mojo::Stream2->new();
$t->login_ok;

my $max_bytes = 100;
$t->app->config->{max_image_upload_size_bytes} = $max_bytes;

my $content;
$content .= 1 for 1..$max_bytes;
my $filename = 'myimage.jpg';

# Good request
my $working_file_req = { image => { 'Content-Type' => 'image/jpeg', content => $content, filename => $filename } };
$t->post_ok('/api/tinymce/upload' => form => $working_file_req)->status_is( 200 )->json_has('/message');

# bad param name
my $missing_param_req = { foo => { 'Content-Type' => 'image/jpeg', content => $content, filename => $filename } };
$t->post_ok('/api/tinymce/upload' => form => $missing_param_req)->status_is(400)->json_has('/error');

# missing mime type
my $missing_content_type_req = { image => { content => $content, filename => $filename } };
$t->post_ok('/api/tinymce/upload' => form => $missing_content_type_req)->status_is(400)->json_has('/error');

# wrong mime type
my $bad_content_type_req = { image => { content => $content, filename => $filename, 'Content-Type' => 'breakfast/sausage' } };
$t->post_ok('/api/tinymce/upload' => form => $bad_content_type_req)->status_is(400)->json_has('/error');

# file too big
$content .= 1;
my $size_too_big_req = { image => { 'Content-Type' => 'image/jpeg', content => $content, filename => $filename } };
$t->post_ok('/api/tinymce/upload' => form => $size_too_big_req)->status_is( 400 )->json_has('/error');

my $factory = $t->app->stream2->factory('GroupFile');
is ( $factory->search({ group_identity => 'andy' })->count(), 1, "we have inserted one image" );
is ( $factory->search()->first()->name, $filename, "the only image we have inserted matches the filename we gave" );

$content = '';
$content .= 1 for 1..$max_bytes;

# filename myimage.jpg
$t->post_ok('/api/tinymce/upload' => form => { image => { 'Content-Type' => 'image/jpeg', content => $content, filename => $filename } })->status_is( 200 )->json_has('/message');
$t->post_ok('/api/tinymce/upload' => form => { image => { 'Content-Type' => 'image/jpeg', content => $content, filename => $filename } })->status_is( 200 )->json_has('/message');

# filename my.ima.1.ge.jpg
$t->post_ok('/api/tinymce/upload' => form => { image => { 'Content-Type' => 'image/jpeg', content => $content, filename => 'my.ima.1.ge.jpg' } })->status_is( 200 )->json_has('/message');
$t->post_ok('/api/tinymce/upload' => form => { image => { 'Content-Type' => 'image/jpeg', content => $content, filename => 'my.ima.1.ge.jpg' } })->status_is( 200 )->json_has('/message');

# filename noext
$t->post_ok('/api/tinymce/upload' => form => { image => { 'Content-Type' => 'image/jpeg', content => $content, filename => 'noext' } })->status_is( 200 )->json_has('/message');
$t->post_ok('/api/tinymce/upload' => form => { image => { 'Content-Type' => 'image/jpeg', content => $content, filename => 'noext' } })->status_is( 200 )->json_has('/message');

is ( $factory->search({ group_identity => 'andy' })->count(), 7, "we have inserted 3 more images" );
my @images = map { $_->name } $factory->search()->all();
is_deeply( \@images, [ 'myimage.jpg', 'myimage-1.jpg', 'myimage-2.jpg', 'my.ima.1.ge.jpg', 'my.ima.1.ge-1.jpg', 'noext', 'noext-1' ], "Renaming images if a similar filename is given works" );

my %fn_tests = (
    'image-1'               => [ qw/image 1/ ],
    'image-2'               => [ qw/image 2/ ],
    'image-1.jpg'           => [ 'image.jpg', '1' ],
    'i.m.a.€-kitten.jpg'    => [ 'i.m.a.€.jpg', 'kitten' ]
);

while ( my ( $expected, $params_ref ) = each %fn_tests ){
    is ( App::Stream2::Api::Tinymce->_postfix_filename( @$params_ref ), $expected, "filename postfixing is working as exptect" );
}

LWP::UserAgent::Mockable->finished();
done_testing();

use Test::Most;

use Stream2;
use Test::MockModule;
use Test::Mojo::Stream2;
use App::Stream2::Api::Adverts;
use Mojo::Transaction::HTTP;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

my $provider_mock = Test::MockModule->new('App::Stream2::Plugin::Auth::Provider::Adcourier');

my $t = Test::Mojo::Stream2->new;

$t->login_ok();

ok( my $stream2 = $t->app->stream2, "Ok can build stream2");

ok( my $users_factory = $stream2->factory('SearchUser'), 'can get user factory' );

ok( my $memory_user = $users_factory->create({
    provider            => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id         => 1234,
    group_identity      => 'acme',
    locale              => 'en',
}), 'made new user' );

$memory_user->save();

$t->app->helper(current_user => sub { return $memory_user; });

my $adverts = [
    { label => 'EPG-123 - Exotic pet groomer - Alabama', advert_id =>12345, visible => 1 },
    { label => 'DIY-123 - DIY Salesman - Ohio', advert_id =>67789, visible => 0 },
];

{
    my $controller = App::Stream2::Api::Adverts->new( app => $t->app, tx => Mojo::Transaction::HTTP->new );

    $provider_mock->mock(get_adverts => sub {
        my ($self, $s2, $query) = @_;
        ok( !$query->{user_id}, 'user_id is not sent when original user is present and can view all adverts' );
        is( $query->{keywords}, 'is awesome', 'read the search keywords' );
        return $adverts;
    });

    $t->app->helper(original_user => sub { return $memory_user; });

    $controller->stash( query_ref => {
            search => 'is awesome',
            ui_origin => 'shortlist',
            group_sandwiches => 0,
        }
    );

    ok( $controller->search(), 'performed search for adverts' );

    my $json = $controller->res->json;
    is( $json->{suggestions}->[0]->{label}, 'EPG-123 - Exotic pet groomer - Alabama', 'can read advert label' );
    is( $json->{suggestions}->[0]->{value}, 12345, 'value of advert corresponds to advert_id' );
}

{
    my $controller = App::Stream2::Api::Adverts->new( app => $t->app, tx => Mojo::Transaction::HTTP->new );

    $provider_mock->mock(get_adverts => sub {
        my ($self, $s2, $query) = @_;
        ok( $query->{user_id}, 'user_id is sent for normal users of search' );
        is( $query->{keywords}, 'is awesome', 'read the search keywords' );
        return $adverts;
    });

    $controller->stash( query_ref => {
            search => 'is awesome',
            group_sandwiches => 0,
        }
    );

    ok( $controller->search(), 'performed search for adverts' );
}

{
    my $controller = App::Stream2::Api::Adverts->new( app => $t->app, tx => Mojo::Transaction::HTTP->new );

    $provider_mock->mock(get_adverts => sub {
        my ($self, $s2, $query) = @_;
        ok( $query->{user_id}, 'user_id is sent for normal users of search' );
        is( $query->{keywords}, 'is awesome', 'read the search keywords' );
        return [ grep { $_->{visible} } @$adverts ];
    });

    $controller->stash( query_ref => {
            search => 'is awesome',
            group_sandwiches => 0,
            filter_out_archived => 1
        }
    );

    ok( $controller->search(), 'performed search for adverts' );
    my $json = $controller->res->json;
    is (scalar @{$json->{suggestions}}, 1, "there is only one visible advert");
    is( $json->{suggestions}->[0]->{label}, 'EPG-123 - Exotic pet groomer - Alabama', 'can read advert label' );
}



{
    my $controller = App::Stream2::Api::Adverts->new( app => $t->app, tx => Mojo::Transaction::HTTP->new );

    $provider_mock->mock(get_adverts => sub {
        my ($self, $s2, $query) = @_;
        ok( $query->{user_id}, 'user_id is sent for normal users of search' );
        ok( $query->{group_adverts}, 'read group adverts option' );
        return {
            adverts => { Live => [ $adverts->[0] ], Archived => [ $adverts->[1] ] },
            groups  => ['Live', 'Archived'],
        };
    });

    $controller->stash( query_ref => {
            group_sandwiches => 1,
        }
    );

    ok( $controller->search(), 'performed search for adverts' );

    my $json = $controller->res->json;

    is_deeply( $json->{groups}, ['Live', 'Archived'], 'grouping adverts returns the list of groups available' );

    ok( $json->{suggestions}->{Archived}, 'suggestions contains adverts grouped under the Archived group' );
    ok( $json->{suggestions}->{Live}, 'suggestions contains adverts grouped under the Live group' );
}

done_testing();

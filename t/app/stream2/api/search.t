use Test::Most;
use Test::Mock::Redis;
use Test::Mojo::Stream2;
use Test::MockModule;
use JSON ();
use Stream2::Criteria;
use Stream2::Results::Result;

my $stream2_mock = Test::MockModule->new('Stream2');
$stream2_mock->mock(redis => sub { return Test::Mock::Redis->new(); });

my $results_store_mock = Test::MockModule->new('Stream2::Results::Store');
$results_store_mock->mock(fetch => sub {
    my ($self) = @_;
    $self->destination('dummy');
    $self->facets([{
        Type => 'advert',
        Options => [
            [ 'a live advert', 38802, 20, 'default' ],
            [ 'an archived advert', 94382, 4, 'archived' ],
            [ 'a general submission advert', 59955, 0, 'general_submissions' ],
        ]
    }]);
    return $self;
});
$results_store_mock->mock(default_results => sub {
    return [ Stream2::Results::Result->new({destination => 'dummy'}) ];
});

my $t = Test::Mojo::Stream2->new();
$t->login_ok();

$t->post_ok('/api/create_criteria' => { Accept => 'application/x-www-form-urlencoded' } => form => { q => JSON::encode_json({ criteria => { wow => 'cool' } }) })
    ->status_is(200)
    ->json_has('/data/id')
    ->json_like('/data/search_url', qr#search/\w+#);

{
    $t->post_ok('/api/create_criteria' => { Accept => 'application/x-www-form-urlencoded' } => form => { q => JSON::encode_json({ criteria => { wow => 'such criteria' }, board => 'mysupply', page_id => 1 }) })
        ->status_is(200)
        ->json_has('/data/id')
        ->json_has('/data/search_url');

    my $json = $t->tx->res->json;
    like($json->{data}->{search_url}, qr#search/$json->{data}->{id}/mysupply/page/1#, 'search url uses the criteria id generated');
    my $criteria = Stream2::Criteria->new( schema => $t->app->stream2->stream_schema, id => $json->{data}->{id} );
    $criteria->load();
    is_deeply({ $criteria->tokens }, { wow => ['such criteria'] }, 'criteria contains the correct data');
}

$t->post_ok('/search_json/555/page/1' => { Accept => 'application/x-www-form-urlencoded' } => form => { q => JSON::encode_json({}) } )
    ->status_is(200)
    ->json_has('/results')
    ->json_has('/facets')
    ->json_has('/notices');
my $facets = $t->tx->res->json->{facets};
foreach my $facet ( @$facets ) {
    if ( $facet->{Type} eq 'advert' ) {
        ok($facet->{isSorted}, 'advert facet indicates its already sorted');
        is(ref($facet->{Options}), 'HASH', 'advert options should be a hashref');
        my @option_groups = keys(%{$facet->{Options}});
        is_deeply(\@option_groups, [qw/general_submissions default archived/], 'advert options contain the right groups');
        foreach my $group ( @option_groups ) {
            like($facet->{Options}->{$group}->{Label}, qr/Live|General Submissions|Archived/, 'group contains label');
            ok($facet->{Options}->{$group}->{SortOrder}, 'group contains a sort order property');
            if ( $group eq 'general_submissions' ) {
                is($facet->{Options}->{$group}->{SortOrder}, 1, 'general submission group should always be the first group');
            }
        }
    }
}

done_testing();

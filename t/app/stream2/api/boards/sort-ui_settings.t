use Test::Most;
use Test::Mojo::Stream2;
use JSON ();

# create a user, subscribed to some boards
my $t    = Test::Mojo::Stream2->new({stream2_class => 'Test::Stream2'});
$t->app->stream2->deploy_test_db();

my $subscriptions_ref = {
    talentsearch => {
        name        => 'talentsearch',
        nice_name   => 'TalentSearch',
        type        => 'internal',
        auth_tokens => {
            tsname => 'davros123',
            tspw   => 'exterminate'
        }
    },
    totaljobs => {
        name        => 'totaljobs',
        nice_name   => "TotalJobs",
        type        => "external",
        auth_tokens => {
            totaljobs_username => 'dalek',
            totaljobs_password => 'dastardly'
        }
    },
    mentaljobs => {
        name        => 'mentaljobs',
        nice_name   => 'MentalJobs',
        type        => 'social',
        auth_tokens => {
            mental_login    => 'dave123',
            mental_password => 'borstal'
        }
    },
    antisocialjobs => {
        name        => 'antisocial',
        nice_name   => 'Antisocial Jobs',
        type        => 'external',
        auth_tokens => {
            as_login    => 'dave123',
            as_password => 'borstal'
        }
    },
    indeed => {
        name        => 'indeed',
        nice_name   => 'Indeed',
        type        => 'external',
        auth_tokens => {
            indeed_user     => 'dav',
            indeed_password => 'ros'
        }
    }
};

my $user = $t->app->stream2->factory('SearchUser')->new_result( {
    contact_details => {
        contact_name  => "Davros",
        contact_email => 'davros@acme.com',
        username => 'dav@ros.looneytoons.acme',
    },
    provider            => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id         => 123,
    contact_name        => 'Davros',
    group_identity      => 'acme',

    subscriptions_ref => $subscriptions_ref
});
$user->save;

# login as $user
$t->login_ok();
$t->app->sessions->storage->list_sessions->[0]->{user_id} = $user->id;

note 'Set our preferred job board order';
my @preferred_board_order
    = qw/indeed antisocialjobs mentaljobs totaljobs talentsearch/;

$t->post_ok('/api/boards/set-positions' => form => {
    q => JSON::encode_json( { boards => \@preferred_board_order } )
});

note "Retrieve boards. They should appear in our preferred order but enforce the internal boards being first";
$t->post_ok( '/api/boards/active.json' );
my $response = JSON::decode_json( $t->tx->res->body );
my @boards   = map { $_->{name} } @{ $response->{boards} };

is_deeply \@boards, [ 'talentsearch', @preferred_board_order[0..($#preferred_board_order-1)] ],
    "Board order matches user preference";

note "Include a board, 'premiumjobs', the user doesn't subscribe to.";
$t->post_ok('/api/boards/set-positions' => form => {
    q => JSON::encode_json( {
        boards => [ 'premiumjobs', @preferred_board_order ]
    } )
});

note "The endpoint should only return the intersection of a user's ordered " .
     "boards with those that are allowed.";
$t->post_ok( '/api/boards/active.json' );
$response = JSON::decode_json( $t->tx->res->body );
@boards   = map { $_->{name} } @{ $response->{boards} };
is_deeply \@boards, [ 'talentsearch', @preferred_board_order[0..($#preferred_board_order-1)] ],
    "Boards the user cannot access are ignored when resolving preferred order";

# add a new subscription to an existing sort order
$subscriptions_ref->{made_up} = {
    name        => 'made_up',
    nice_name   => 'Made Up',
    type        => 'external',
    auth_tokens => {}
};
$subscriptions_ref->{adcresponses} = {
    name        => 'adcresponses',
    nice_name   => 'Responses',
    type        => 'internal',
    auth_tokens => {}
};

Stream2::SearchUser::Subscriptions->new({
    ref     => $subscriptions_ref,
    key     => $t->app->stream2->config()->{subscription_key},
    user    => $user,
    schema  => $t->app->stream2->stream_schema(),
})->save;

$t->get_ok( '/api/boards/active.json' );
@boards = map { $_->{name} } @{$t->tx->res->json->{boards}};
is_deeply(
    \@boards,
    [ 'adcresponses', 'talentsearch', @preferred_board_order[0..($#preferred_board_order-1)], 'made_up' ],
    'unknown external board is added to end of list, unknown internal: to the start'
);

done_testing();

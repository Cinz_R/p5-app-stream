#!/usr/bin/env perl

use Test::Most;

use_ok('App::Stream2::Api::Boards');

# For anonymous users - rather, a user with no ui-settings - boards should be
# ordered by type (internal first), then by name (alphabetically ascending).

my @boards = (
    {
        nice_name => 'Talentsearch',
        name => 'talentsearch',
        type => 'internal'
    },
    {
        nice_name => 'zzzzz',
        name => 'zzzzz',
        type => 'internal',
    },
    {
        nice_name => 'aaaaa',
        name => 'aaaaa',
        type => 'internal'
    },
    {
        nice_name => 'Monster XML (USA)',
        name => 'monster_xml',
        type => 'external'
    },
    {
        nice_name => 'CareerBuilder',
        name => 'careerbuilder',
        type => 'external'
    },
    {
        nice_name => 'Indeed',
        name => 'indeed',
        type => 'external'
    },
    {
        nice_name => 'Stack Overflow',
        name => 'stackoverflow',
        type => 'social'
    },
    {
        nice_name => 'Flickr',
        name => 'flickr',
        type => 'social'
    }
);

my @out = App::Stream2::Api::Boards::_sort_boards_by_type( @boards );
my @order = ( 'Talentsearch', 'aaaaa', 'zzzzz', 'CareerBuilder', 'Indeed', 'Monster XML (USA)', 'Flickr', 'Stack Overflow' );

is_deeply( [map { $_->{nice_name} } @out], \@order, "ordered by type and then by name" );

done_testing();

use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;

my $board_mock = Test::MockModule->new('Bean::Juice::APIClient::Board');
$board_mock->mock( 'list_boards' => sub {} );

my $t = Test::Mojo::Stream2->new({stream2_class => 'Test::Stream2'});
my $adcourier_user = $t->app->stream2->factory('SearchUser')->new_result({
    contact_details => {
        contact_name  => "Jason Jr",
        contact_email => 'Jasonjr@JSON_breakersyard.com',
        username => 'jason@rakers.bakers.jsonbrakers',
    },
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 3141,
    group_identity => 'acme',
    subscriptions_ref => {
        totaljobs => {
            nice_name => "TotalJobs",
            auth_tokens => { totaljobs_username => 'shaun' , totaljobs_password => 'the_sheep' },
            type => "external",
            allows_acc_sharing => 0,
        },
        'whatever' => {
            nice_name => 'Whatever',
            auth_tokens => { whatever_login => 'Boudin', whatever_passwd => 'Blanc' },
            type => 'social',
        }
    }
});
$adcourier_user->save;

$t->get_ok('/api/board/talentsearch?locale=fr')
    ->status_is( '403' );

# LOGIN the user and change our current sessions to point to
# the user we have created above
$t->login_ok();
$t->app()->sessions()->storage()->list_sessions()->[0]->{user_id} = $adcourier_user->id;

$t->get_ok('/api/board/madeup?locale=fr')
    ->status_is( '404' );

$t->get_ok('/api/board/talentsearch?locale=fr')
    ->status_is( '403' )
    ->content_like( qr/subscription/i );

$t->get_ok('/api/board/totaljobs')
    ->status_is( 200 )
    ->json_is( '/data/auth_tokens/1/Label', 'Password' );

$t->get_ok('/api/board/totaljobs?locale=fr')
    ->status_is( 200 )
    ->json_is( '/data/auth_tokens/1/Label', 'Mot de passe' );

my $s2 = $t->app->stream2;
$s2->deploy_test_db();
$t->app->helper( 'current_user' => sub { return $adcourier_user } );

foreach my $board ( keys(%{$adcourier_user->subscriptions_ref}) ) {
    foreach my $type ( qw/search-cv search-profile search-search/ ) {
        $s2->quota_object->create(
            {
                board => $board,
                type => $type,
                quota => [
                    {
                        path => $adcourier_user->hierarchical_path,
                        remaining => 5
                    },
                ],
            },
            {
                actioned_by => 'batman',
                remote_addr => 'batcave',
            }
        );
    }
}

$t->get_ok('/api/boards/active')
    ->status_is( 200 )
    ->json_has('/boards/0/user_quotas')
    ->json_has('/boards/0/user_quotas/search-search');

done_testing();

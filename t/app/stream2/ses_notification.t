#! perl -w
use strict;
use warnings;


use Test::More;
use Test::MockModule;
use Test::Mojo::Stream2;

use Mojo::URL;

# use Log::Any qw/Stderr/;

my $lwp_mock = Test::MockModule->new( 'LWP::UserAgent' );

my $t = Test::Mojo::Stream2->new();
$t->ua->on(start => sub {
    my ($ua, $tx) = @_;
    $tx->req->headers->accept('text/json');
});

## Simulates what the doc says it will send to the notification endpoint
## see http://docs.aws.amazon.com/sns/latest/dg/SendMessageToHttp.html for the full documentation



## First a subscription message, with its mocked callback response
$lwp_mock->mock( request => sub {
                     my $response = HTTP::Response->new( '200', 'OK' );
                     $response->header( 'Content-Type' => 'text/xml' );
                     $response->content( q|<ConfirmSubscriptionResponse xmlns="http://sns.amazonaws.com/doc/2010-03-31/">
  <ConfirmSubscriptionResult>
    <SubscriptionArn>arn:aws:sns:us-west-2:123456789012:MyTopic:2bcfbf39-05c3-41de-beaa-fcfcc21c8f55</SubscriptionArn>
  </ConfirmSubscriptionResult>
  <ResponseMetadata>
    <RequestId>075ecce8-8dac-11e1-bf80-f781d96e9307</RequestId>
  </ResponseMetadata>
  </ConfirmSubscriptionResponse>| );
                     return $response;
                 });


$t->post_ok('/external/ses_notification',{
    Accept => 'text/json',
    'x-amz-sns-message-type' => 'SubscriptionConfirmation'
}, q|
{
  "Type" : "SubscriptionConfirmation",
  "MessageId" : "165545c9-2a5c-472c-8df2-7ff2be2b3b1b",
  "Token" : "2336412f37fb687f5d51e6e241d09c805a5a57b30d712f794cc5f6a988666d92768dd60a747ba6f3beb71854e285d6ad02428b09ceece29417f1f02d609c582afbacc99c583a916b9981dd2728f4ae6fdb82efd087cc3b7849e05798d2d2785c03b0879594eeac82c01f235d0e717736",
  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
  "Message" : "You have chosen to subscribe to the topic arn:aws:sns:us-west-2:123456789012:MyTopic.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
  "SubscribeURL" : "http://www.example.com",
  "Timestamp" : "2012-04-26T20:45:04.751Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLEpH+DcEwjAPg8O9mY8dReBSwksfg2S7WKQcikcNKWLQjwu6A4VbeS0QHVCkhRS7fUQvi2egU3N858fiTDN6bkkOxYDVrY0Ad8L10Hs3zH81mtnPk5uvvolIC1CXGu43obcgFxeL3khZl8IKvO61GWB6jI9b5+gLPoBc1Q=",
  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem"
  }
|)->status_is(200);

## Lets move to notifications

# A notification, with an example message of a delivery

$t->post_ok( '/external/ses_notification' , {
    Accept => 'text/json',
    'x-amz-sns-message-type' => 'Notification'
}, q|{
  "Type" : "Notification",
  "MessageId" : "da41e39f-ea4d-435a-b922-c6aae3915ebe",
  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
  "Subject" : "test",
  "Message" : "{\"notificationType\":\"Delivery\",\"mail\":{\"timestamp\":\"2015-09-30T13:55:10.314Z\",\"source\":\"noreply@broadbean.net\",\"sourceArn\":\"arn:aws:ses:eu-west-1:868002146347:identity/broadbean.net\",\"sendingAccountId\":\"868002146347\",\"messageId\":\"000001501e890f6a-97a44ae4-4929-45a3-9f4a-c46428a3d42f-000000\",\"destination\":[\"willtcooper@yahoo.co.uk\"]},\"delivery\":{\"timestamp\":\"2015-09-30T13:55:10.935Z\",\"processingTimeMillis\":621,\"recipients\":[\"willtcooper@yahoo.co.uk\"],\"smtpResponse\":\"250 ok dirdel\",\"reportingMTA\":\"a6-226.smtp-out.eu-west-1.amazonses.com\"}}",
  "Timestamp" : "2012-04-25T21:49:25.719Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLElDMXvB8r9R83tGoNn0ecwd5UjllzsvSvbItzfaMpN2nk5HVSw7XnOn/49IkxDKz8YrlH2qJXj2iZB0Zo2O71c4qQk1fMUDi3LGpij7RCW7AW9vYYsSqIKRnFS94ilu7NFhUzLiieYr4BKHpdTmdD6c0esKEYBpabxDSc=",
  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem",
  "UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:123456789012:MyTopic:2bcfbf39-05c3-41de-beaa-fcfcc21c8f55"
}| )->status_is(200);

ok( ! $t->app()->stream2()->factory('EmailNotification')->search()->count() , "Ok no notification is stored");

# A notification, with an example of bounce email
$t->post_ok( '/external/ses_notification' , {
    Accept => 'text/json',
    'x-amz-sns-message-type' => 'Notification'
}, q|{
  "Type" : "Notification",
  "MessageId" : "da41e39f-ea4d-435a-b922-c6aae3915ebe",
  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
  "Subject" : "test",
  "Message" : "{\n       \"notificationType\":\"Bounce\",\n       \"bounce\":{\n          \"bounceType\":\"Permanent\",\n          \"reportingMTA\":\"dns; email.example.com\",\n          \"bouncedRecipients\":[\n             {\n                \"emailAddress\":\"username@example.com\",\n                \"status\":\"5.1.1\",\n                \"action\":\"failed\",\n                \"diagnosticCode\":\"smtp; 550 5.1.1 <username@example.com>... User\"\n             }\n          ],\n          \"bounceSubType\":\"General\",\n          \"timestamp\":\"2012-06-19T01:07:52.000Z\",\n          \"feedbackId\":\"00000138111222aa-33322211-cccc-cccc-cccc-ddddaaaa068a-000000\"\n       },\n       \"mail\":{\n          \"timestamp\":\"2012-06-19T01:05:45.000Z\",\n          \"source\":\"sender@example.com\",\n          \"sourceArn\": \"arn:aws:ses:us-west-2:888888888888:identity/example.com\",\n          \"sendingAccountId\":\"123456789012\",\n          \"messageId\":\"00000138111222aa-33322211-cccc-cccc-cccc-ddddaaaa0680-000000\",\n          \"destination\":[\n             \"username@example.com\"\n          ]\n       }\n    }",
  "Timestamp" : "2012-04-25T21:49:25.719Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLElDMXvB8r9R83tGoNn0ecwd5UjllzsvSvbItzfaMpN2nk5HVSw7XnOn/49IkxDKz8YrlH2qJXj2iZB0Zo2O71c4qQk1fMUDi3LGpij7RCW7AW9vYYsSqIKRnFS94ilu7NFhUzLiieYr4BKHpdTmdD6c0esKEYBpabxDSc=",
  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem",
  "UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:123456789012:MyTopic:2bcfbf39-05c3-41de-beaa-fcfcc21c8f55"
}| )->status_is(200);

ok( $t->app()->stream2()->factory('EmailNotification')->search({ email => 'username@example.com', notification => 'Bounce'} )->count() , "Ok found the notification about the bounce in the storage");

# A notification, with an example of complaint email
$t->post_ok( '/external/ses_notification' , {
    Accept => 'text/json',
    'x-amz-sns-message-type' => 'Notification'
}, q|{
  "Type" : "Notification",
  "MessageId" : "da41e39f-ea4d-435a-b922-c6aae3915ebe",
  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
  "Subject" : "test",
  "Message" : "{\n      \"notificationType\":\"Complaint\",\n      \"complaint\":{\n         \"complainedRecipients\":[\n            {\n               \"emailAddress\":\"recipient1@example.com\"\n            }\n         ],\n         \"timestamp\":\"2012-05-25T14:59:38.613-07:00\",\n         \"feedbackId\":\"0000013786031775-fea503bc-7497-49e1-881b-a0379bb037d3-000000\"\n      },\n      \"mail\":{\n         \"timestamp\":\"2012-05-25T14:59:38.613-07:00\",\n         \"messageId\":\"0000013786031775-163e3910-53eb-4c8e-a04a-f29debf88a84-000000\",\n         \"source\":\"email_1337983178613@amazon.com\",\n         \"sourceArn\": \"arn:aws:ses:us-west-2:888888888888:identity/example.com\",\n         \"sendingAccountId\":\"123456789012\",\n         \"destination\":[\n            \"recipient1@example.com\",\n            \"recipient2@example.com\",\n            \"recipient3@example.com\",\n            \"recipient4@example.com\"\n         ]\n      }\n   }",
  "Timestamp" : "2012-04-25T21:49:25.719Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLElDMXvB8r9R83tGoNn0ecwd5UjllzsvSvbItzfaMpN2nk5HVSw7XnOn/49IkxDKz8YrlH2qJXj2iZB0Zo2O71c4qQk1fMUDi3LGpij7RCW7AW9vYYsSqIKRnFS94ilu7NFhUzLiieYr4BKHpdTmdD6c0esKEYBpabxDSc=",
  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem",
  "UnsubscribeURL" : "https://sns.us-west-2.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-west-2:123456789012:MyTopic:2bcfbf39-05c3-41de-beaa-fcfcc21c8f55"
}| )->status_is(200);

ok( $t->app()->stream2()->factory('EmailNotification')->search({ email => 'recipient1@example.com', notification => 'Complaint'} )->count() , "Ok found the notification about the complaint in the storage");

done_testing();

#! perl -w

use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;
use Promises;
# TEST SETUP
my $t = Test::Mojo::Stream2->new();
my $s2 = $t->app->stream2;

my $grant_response = {
    user => {
            read_stream_subscriptions => {
                talentsearch => {
                    type => "internal",
                    auth_tokens => { client_id => "pat" },
                    nice_name => "TalentSearch",
                },
                dummy => {
                    type => "dummy",
                    auth_tokens => {},
                    nice_name => "Dummy",
                }
            },
            read_contact_details => {
                contact_email => q{pat@example.com},
                company => "test"
            },
            user_id => 1
    },
    access_token => "it doesnt matter"
};

my $oauth_cli_module = Test::MockModule->new('Bean::OAuth::AsyncClient');
$oauth_cli_module->mock('credentials_grant' => sub {
    my $d = Promises::deferred;

    $d->resolve( $grant_response );

    return $d->promise
});
my $user_module = Test::MockModule->new('Stream2::O::SearchUser');
$user_module->mock( 'conform_to_sibling_settings' => sub {
    return 1;
});

my $test = {
    request => {
        api_key => 'test',
        account => {
            username    => 'test',
            signature   => {
                hash    => 'test',
                time    => 'test'
            }
        },
        config => {
            hide_save => 1,
            shortlists => [
                {
                    name => 'sl1', label => 'shortlist1', options_url => 'http://www.example.com', allowPreview => 1
                }
            ]
        },
        custom_fields => [
            {
                name => 'custom_field_1',
                content => 'abc',
            },
            {
                name => 'custom_field_2',
                content => 123
            }
        ]
    }
};

$t->post_ok( '/ripple/login', { Accept => "application/json", "Content-Type" => "application/json" },
    JSON::encode_json( $test->{request} // {} ) )
        ->status_is( 200 )
        ->json_is( '/status', 'OK' )
        ->json_like( '/ripple_session_id', qr/\A[a-z0-9\-]+\z/ );

my $ripple_session = $t->tx->res->json->{ripple_session_id};

$t->get_ok( '/api/user/details', { 'x-ripple-session-id' => $ripple_session } )
    ->json_is( '/details/feature_exclusions/hide_save', 1 );

$t->get_ok( '/api/user/custom_lists', { 'x-ripple-session-id' => $ripple_session } )
    ->json_is( '/0/label', 'shortlist1' );

{
    my $session = $t->app()->sessions()->storage()->list_sessions()->[0];
    my $user_id = $s2->factory('SearchUser')->search({ group_identity => 'test', provider_id => 1 })->first()->id();
    my $user = $s2->find_user( $user_id, $session->{ripple_settings} );
    is (
        $user->ripple_xdoc_custom_fields( XML::LibXML::Document->new( '1.0', 'UTF-8' ) )->toString,
        '<CustomFields><CustomField name="custom_field_1">abc</CustomField><CustomField name="custom_field_2">123</CustomField></CustomFields>',
        'Custom fields are being generated as expected'
    );
}

$t->post_ok('/ripple/update_session', { 'x-ripple-session-id' => $ripple_session },
    JSON::encode_json({
        config => {
            hide_download => 1,
            shortlists => [
                {
                    name => 'sl2', label => 'shortlist2', options_url => 'http://www.example.com', allowPreview => 1
                }
            ]
        },
        custom_fields => [
            {
                name => 'custom_field_4',
                content => 'bacon',
            },
            {
                name => 'custom_field_99',
                content => 'sausages'
            }
        ]
    })
)->status_is(200);

$t->get_ok( '/api/user/details', { 'x-ripple-session-id' => $ripple_session } )
    ->json_hasnt( '/details/feature_exclusions/hide_save' )
    ->json_is( '/details/feature_exclusions/hide_download', 1 );

$t->get_ok( '/api/user/custom_lists', { 'x-ripple-session-id' => $ripple_session } )
    ->json_is( '/0/label', 'shortlist2' );

{
    my $session = $t->app()->sessions()->storage()->list_sessions()->[0];
    my $user_id = $s2->factory('SearchUser')->search({ group_identity => 'test', provider_id => 1 })->first()->id();
    my $user = $s2->find_user( $user_id, $session->{ripple_settings} );
    is (
        $user->ripple_xdoc_custom_fields( XML::LibXML::Document->new( '1.0', 'UTF-8' ) )->toString,
        '<CustomFields><CustomField name="custom_field_4">bacon</CustomField><CustomField name="custom_field_99">sausages</CustomField></CustomFields>',
        'Custom fields are being generated as expected'
    );
}

done_testing();

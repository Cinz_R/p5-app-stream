#! perl -w

use utf8;

use strict;
use warnings;

use Test::More;
use Test::MockModule;
use Test::Mojo::Stream2;

use Log::Log4perl qw/:easy/;

# use Log::Any::Adapter qw/Stderr/;

use JSON;
use Digest::SHA qw(hmac_sha256_hex);

# TEST SETUP
my $t = Test::Mojo::Stream2->new();

# DO _NOT_ LOGIN. This would set a cookie and
# a session against $t, rendering the tests below
# uneffective at testing alternate sessions.
# $t->login_ok();

$t->ua->max_redirects(1);    # Allow one redirection (for the magic URLs)

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'"
      . ( $config_file || 'UNDEF' ) . "'";
}
my $stream2 = $t->app->stream2;
my $schema  = $stream2->stream_schema();

my $criteria_ref = {
    ofccp_context => 'Pinguin Groomer',
    keywords      => "sausages",
    location      => { postcode => "lemon-party", country => "North Korea" },
    include_unspecified_salaries => JSON::false()
};

my $secret_key = q{1234};
my $username   = q{pat@pat.pat.pat};
my $api_key    = "4321";
my $time_ms    = time() * 1000;
my $auth2 =
  _generate_digest( $secret_key, join( '|', $username, $time_ms, $api_key ) );

my $grant_response = {
    user => {
        read_stream_subscriptions => {
            talentsearch => {
                type        => "internal",
                auth_tokens => { client_id => "pat" },
                nice_name   => "TalentSearch",
            },
            dummy => {
                type        => "dummy",
                auth_tokens => {},
                nice_name   => "Dummy",
            }
        },
        read_contact_details => {
            contact_email => q{pat@example.com},
            company       => "test"
        }
    },
    access_token => "it doesnt matter"
};

my $test = {
    request => {
        api_key => $api_key,
        account => {
            username  => $username,
            signature => {
                hash => $auth2,
                time => $time_ms
            }
        },
        query   => $criteria_ref,
        channel => {
            type => "internal",
        },
        config => {
            results_per_page => 12,
            stylesheet_url   => '/static/stylesheets/custom-demo.css',
            shortlists       => [
                {
                    name        => 'cheese_sandwiches',
                    label       => 'Cheese Sandwich',
                    options_url => '_this_is_dummy_'
                }
            ],
            new_candidate_url => '_this_is_dummy_',
        },
        custom_fields => [
            {
                name         => 'field_1',
                asHttpHeader => 0,
                content      => 'Field 1 Content',
            },
            {
                name         => 'field_2',
                asHttpHeader => 1,
                content      => 'Field 2 Content',
            },
            {
                name         => 'field_3',
                asHttpHeader => 0,
                content      => "нолюёжжэ",
            },
            {
                name         => 'field_4',
                asHttpHeader => 1,
                content      => "Jérôme Étévé",
            }
        ]
    },
    expected => {
        custom_fields => {
            headers => {
                field_2 => Encode::encode( 'MIME-Q', 'Field 2 Content' ),
                field_4 => Encode::encode( 'MIME-Q', "Jérôme Étévé" ),
            },
            xdoc => {
                num_fields => 2,
            },
        },
        subscriptions => {
            talentsearch => {
                type        => "internal",
                auth_tokens => { client_id => "pat" },
                nice_name   => "TalentSearch",
            },
            dummy => {
                type        => "dummy",
                auth_tokens => {},
                nice_name   => "Dummy",
            }
        },
        grant_response     => $grant_response,
        status             => 200,
        template_should_be => "talentsearch"
    }
};

my $location_api_module =
  Test::MockModule->new('Bean::Juice::APIClient::Location');
$location_api_module->mock(
    'find' => sub {
        return "test_location";
    }
);

my $oauth_cli_module = Test::MockModule->new('Bean::OAuth::AsyncClient');
my $user_module      = Test::MockModule->new('Stream2::O::SearchUser');
my $feed_tokens      = Test::MockModule->new("Stream2::FeedTokens");
$user_module->mock(
    'conform_to_sibling_settings' => sub {
        return 1;
    }
);

# hard code some static facets to prove that "GET /ripple/status" will NOT include
# them in its response as there was an error
$feed_tokens->mock(
    tokens => sub {
        [ { Id => 'Pepsi' }, { Id => 'Sprite' }, { Id => 'Tango' } ];
    }
);

my $user_id = 500;

$t->app->helper(
    queue_job => sub {
        my ( $self, %params ) = @_;
        return "test_job";
    }
);
$t->app->helper(
    'retrieve_job' => sub {
        return "test_job";
    }
);

$oauth_cli_module->mock(
    'credentials_grant' => sub {
        my ( $self, %params ) = @_;
        my $d = Promises::deferred;

        if (   $params{signature} eq $auth2
            && $params{time_ms} eq $time_ms
            && $params{api_key} eq $api_key
            && $params{username} eq $username )
        {
            $test->{expected}->{grant_response}->{user}->{user_id} = $user_id++;
            $d->resolve( $test->{expected}->{grant_response} );
        }
        else {
            $d->reject("Bad Credentials");
        }

        return $d->promise;
    }
);
$user_module->mock(
    'subscriptions_ref' => sub {
        return $test->{expected}->{subscriptions} // {};
    }
);

$t->post_ok(
    '/ripple/search',
    { Accept => "application/json", "Content-Type" => "application/json" },
    $t->app()->stream2()->json->encode( $test->{request} // {} )
)->status_is(200)->json_like( 'ripple_session_id', qr/^\S+$/ );

ok( !$t->tx->res->headers()->header('set-cookie'), "Ok no set cookie here" );
unless ( $test->{expected}->{status} == 200 ) {
    next;
}
ok( $t->tx->res->json()->{status_url}, "Ok got status_url" );
ok( $t->tx->res->json()->{search_url},
    "Ok got URL to the same search (criteria + channel)" );

my $status_url = $t->tx->res->json()->{status_url};

{
    $t->get_ok( $status_url,
        { Accept => "application/json", "Content-Type" => "application/json" } )
      ->status_is(200)->json_like( '/status', qr/FAKESTATUS/ )
      ->json_hasnt('/context/result/facets');
}

done_testing();

sub _generate_digest {
    my ( $key, $data ) = @_;
    my $digest = hmac_sha256_hex( $data, $key );
    return $digest;
}

{

    package test_job;
    sub guid   { 1; }
    sub status { 'FAKESTATUS' }
    sub result { { 'error' => { 'message' => 'something went wrong' } }; }
    sub parameters { { template_name => 'talentsearch' } }
}

{

    package test_location;
    sub id            { 5; }
    sub specific_path { "seven seas"; }
}

#!/usr/bin/enf perl

use Test::More tests => 7;
use Test::MockModule;
use Test::Mojo::Stream2;

use Log::Log4perl qw/:easy/;
#use Log::Any::Adapter qw/Stderr/;
use JSON;
use Digest::SHA qw(hmac_sha256_hex);
use Promises;

# TEST SETUP
my $t = Test::Mojo::Stream2->new();
$t->login_ok();
my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}
my $stream2 = $t->app->stream2;
my $schema = $stream2->stream_schema();

my $criteria_ref = { keywords => "sausages", location => { postcode => "lemon-party", country => "North Korea" }, include_unspecified_salaries => JSON::false() };

my $grant_response = {
    user => {
            read_stream_subscriptions => {
                talentsearch => {
                    type => "internal",
                    auth_tokens => { client_id => "pat" },
                    nice_name => "TalentSearch",
                },
                dummy => {
                    type => "dummy",
                    auth_tokens => {},
                    nice_name => "Dummy",
                }
            },
            read_contact_details => {
                contact_email => q{pat@example.com},
                company => "test"
            },
            user_id => 1
    },
    access_token => "it doesnt matter"
};

my $requests = [
    {
        request => {
            api_key => 'test',
            account => {
                username    => 'test',
                signature   => {
                    hash    => 'test',
                    time    => 'test'
                }
            },
            query => $criteria_ref,
            channel => {
                type => "all"
            },
            candidate_action => {
                class => 'Stream2::Action::Candidate::Save'
            }
        },
        subscriptions => {
            talentsearch => {
                type => "internal", # this board won't be searched upon
                auth_tokens => { client_id => "pat" },
                nice_name => "TalentSearch",
            },
            dummy => {
                type => "dummy",
                auth_tokens => {},
                nice_name => "Dummy",
            },
            other_board => {
                type => "external",
                auth_tokens => {},
                nice_name => "OTHER BOARD"
            }
        },
        grant_response => $grant_response,
        status => 202,
        template_should_be => "dummy"
    }
];

my $location_api_module = Test::MockModule->new('Bean::Juice::APIClient::Location');
$location_api_module->mock('find' => sub { return test_location; });
my $oauth_cli_module = Test::MockModule->new('Bean::OAuth::AsyncClient');
my $user_module = Test::MockModule->new('Stream2::O::SearchUser');
$user_module->mock( 'conform_to_sibling_settings' => sub {
    return 1;
});

my $user_id = 500;
my %seen = ();
foreach my $test ( @$requests ){
    $t->app->helper( queue_job => sub {
        my ( $self,  %params ) = @_;
        my $matched_subscription = grep { ! $seen{$_}++ } grep { $_ eq $params{parameters}->{template_name} } keys %{$test->{subscriptions}};
        ok ( $matched_subscription, sprintf("Search queued for %s",$params{parameters}->{template_name}) );
        is ( $params{parameters}->{options}->{candidate_action}->{class}, 'Stream2::Action::Candidate::Save', "Candidate action is passed to job" );
        return test_job;
    });
    $oauth_cli_module->mock('credentials_grant' => sub {
        my $d = Promises::deferred;

        $d->resolve( $test->{grant_response} );

        return $d->promise;
    });
    $user_module->mock( 'subscriptions_ref' => sub { return $test->{subscriptions} // {} });

    $t->post_ok( '/ripple/search', { Accept => "application/json", "Content-Type" => "application/json" },
        JSON::encode_json( $test->{request} // {} ) )
            ->status_is( $test->{status} // 200 );
}

{
    package test_job;
    sub guid { 1; }
}

{
    package test_location;
    sub id { 5; }
    sub specific_path { "seven seas"; }
}

#! perl -w

use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;
use Promises;

# Don't go to internet
my $auth_mock = Test::MockModule->new('Bean::OAuth::AsyncClient');
$auth_mock->mock(credentials_grant => sub {
    my $d = Promises::deferred;
    return $d->resolve({
        user => {
            user_id => 123,
            read_contact_details => {
                company => 'cinzwonderland'
            },
            read_stream_subscriptions => {
                adcresponses => {
                    nice_name => 'Responses',
                    type => 'internal',
                },
                talentsearch => {
                    nice_name => 'Talent Search',
                    type => 'internal',
                }
            },
            read_navigation_tabs => [
                {
                    'subtoolbar' => '',
                    'parent' => '',
                    'show' => '1',
                    'FF' => 'Post',
                    'level' => 'primary',
                    'order' => '1',
                    'text' => 'Post an Advert',
                    'href' => '/broadcast-step-1.cgi',
                    'category' => '',
                    'id' => 'post'
                },
                {
                    'subtoolbar' => '',
                    'parent' => '',
                    'show' => '1',
                    'FF' => 'Home',
                    'level' => 'primary',
                    'order' => '0.5',
                    'text' => 'Home',
                    'href' => '/index.cgi',
                    'category' => '',
                    'id' => 'home'
                }
            ]
        }
    });
});
my $user_mock = Test::MockModule->new('Bean::Juice::APIClient::User');
$user_mock->mock( provider_settings => sub {
    return { provider_name => 'bond', webhooks => [ { config => {tagdoc_type => 'hr_xml'}, url => 'http://www.adcourier.com/dump_stuff.cgi' } ] };
});
$user_mock->mock( siblings => sub { return []; });

# wrap calls to user_sign_in so we can assert the presence of currencies
my $plugin_auth_mock = Test::MockModule->new('App::Stream2::Plugin::Auth');
$plugin_auth_mock->mock(
    user_sign_in => sub {
        my ($self, $controller, $user_ref) = @_;
        ok( ref( $user_ref->{currencies} ) eq 'ARRAY', 'currencies exist' );

        my $orig_method = $plugin_auth_mock->original('user_sign_in');
        return $orig_method->( @_ );
    }
);

my $t = Test::Mojo::Stream2->new();
$t->post_ok(
    '/ripple/login',
    json => {
        api_key => 'sometestingapikey',
        account => {
            username => 'cinzia.second@team_one.London.cinzwonderland',
            password => 'wejaoiwjefioejf'
        },
        config => {
            hide_save => 1,
            hide_save_internal => 1,
            show_adcourier_navigation => 1
        },
        manage_responses_view => 1,
    })
    ->status_is( 200 )
    ->json_is( '/status', 'OK' )
    ->json_like( '/ripple_session_id', qr/\A[a-z0-9\-]+\z/ );

my $ripple_login_json = $t->tx->res->json;
my $session_url = $ripple_login_json->{session_url};

$t->ua->max_redirects(1);
$t->get_ok($session_url)
    ->status_is(200)
    ->content_like( qr/\Qbroadcast-step-1.cgi">Post an Advert\E/ );

$t->ua->cookie_jar->empty;
$t->post_ok(
    '/ripple/login',
    json => {
        api_key => 'sometestingapikey',
        account => {
            username => 'cinzia.second@team_one.London.cinzwonderland',
            password => 'wejaoiwjefioejf'
        },
        config => {
            hide_save => 1,
            hide_save_internal => 1
        },
        manage_responses_view => 1,
    })
    ->status_is( 200 )
    ->json_is( '/status', 'OK' )
    ->json_like( '/ripple_session_id', qr/\A[a-z0-9\-]+\z/ );

$ripple_login_json = $t->tx->res->json;
$session_url = $ripple_login_json->{session_url};

$t->ua->max_redirects(1);
$t->get_ok($session_url)
    ->status_is(200)
    ->content_unlike( qr/\Qbroadcast-step-1.cgi">Post an Advert\E/ );

$t->get_ok('/api/user/details', { 'x-ripple-session-id' => $ripple_login_json->{ripple_session_id} } )
    ->status_is(200)
    ->json_is('/details/feature_exclusions/hide_save', 1)
    ->json_is('/details/feature_exclusions/hide_save_internal', 1);

done_testing();

use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;
use URI;

my $t = Test::Mojo::Stream2->new();
$t->login_ok;

# set json_ripple job_id
$t->app->routes->get('/set_sesh')->to( cb => sub {
    my ( $self ) = @_;
    $self->session( json_ripple => { job_id => 123456 } );
    return $self->render( data => 'OK' );
});
$t->get_ok('/set_sesh');

my $stream2_mock = Test::MockModule->new('Stream2');
$stream2_mock->mock( 'redis' => sub { return TestObject->new({ get => '{ "destination": "talentsearch", "search_record_id": 54321, "notices":[], "facets":[] }' }); } );
$stream2_mock->mock( find_results => sub { return TestObject->new({ find => TestObject->new({ idx => 0, email => 'john@example.com', destination => 'talentsearch', id => 111 }) }); } );
my $result_mock = Test::MockModule->new('Stream2::Results::Store');
$result_mock->mock( 'fetch_results' => sub {
    my ( $self ) = @_;
    return $self->to_results('{ "idx": 0, "email": "john@example.com" }', '{ "idx": 1, "email": "tom@example.com" }');
});
my $candidate_api_mock = Test::MockModule->new('App::Stream2::Api::Candidate');
$candidate_api_mock->mock( 'downloadprofile' => sub {
    return shift->render( json => { data => { profile_data => 'hello' } } );
});

$t->get_ok('/ripple/results')
    ->status_is(200)
    ->json_like( '/results/0/actions/profile', qr/\/results\/123456\/talentsearch\/candidate\/0\/profile/ )
    ->json_like( '/results/0/actions/profile_history', qr/results\/123456\/talentsearch\/candidate\/0\/profile_history/ );
my $json_ref = $t->tx->res->json;
my $profile_url = $json_ref->{results}->[0]->{actions}->{profile};
$profile_url =~ s@\Ahttp://[^/]+@@;

$t->get_ok( $profile_url )
    ->status_is( 200 )
    ->json_is( '/data/profile_data', "hello" );

done_testing();

{
    # TestObject provides a getter for any attribute
    package TestObject;
    use AutoLoader;
    sub new { bless pop, shift; }
    sub AUTOLOAD {
        our $AUTOLOAD;
        my ( $method ) = $AUTOLOAD =~ m/\ATestObject::(.*)\z/;
        return $method eq 'DESTROY' ? () : shift->{$method};
    }
}

#! perl -w

use utf8;

use strict;
use warnings;

use Test::More;
use Test::MockModule;
use Test::Mojo::Stream2;

use Log::Log4perl qw/:easy/;
# use Log::Any::Adapter qw/Stderr/;

use JSON;
use Digest::SHA qw(hmac_sha256_hex);

# TEST SETUP
my $t = Test::Mojo::Stream2->new();

# DO _NOT_ LOGIN. This would set a cookie and
# a session against $t, rendering the tests below
# uneffective at testing alternate sessions.
# $t->login_ok();

$t->ua->max_redirects(1);    # Allow one redirection (for the magic URLs)

my $stream2 = $t->app->stream2;
my $schema  = $stream2->stream_schema();

my $criteria_ref = {
    ofccp_context => 'Pinguin Groomer',
    keywords      => "sausages",
    # location      => { postcode => "lemon-party", country => "North Korea" },
    location => { latitude => "51.50853" , longitude => '-0.12574' },
    include_unspecified_salaries => JSON::false()
};

my $secret_key = q{1234};
my $username   = q{pat@pat.pat.pat};
my $api_key    = "4321";
my $time_ms    = time() * 1000;
my $auth2 =
  _generate_digest( $secret_key, join( '|', $username, $time_ms, $api_key ) );

my $grant_response = {
    user => {
        read_stream_subscriptions => {
            talentsearch => {
                type        => "internal",
                auth_tokens => { client_id => "pat" },
                nice_name   => "TalentSearch",
            },
            dummy => {
                type        => "dummy",
                auth_tokens => {},
                nice_name   => "Dummy",
            }
        },
        read_contact_details => {
            contact_email => q{pat@example.com},
            company       => "test"
        }
    },
    access_token => "it doesnt matter"
};

my $test = {
    request => {
        api_key => $api_key,
        account => {
            username  => $username,
            signature => {
                hash => $auth2,
                time => $time_ms
            }
        },
        query   => $criteria_ref,
        channel => {
            type => "internal",
        },
        config => { results_per_page   => 12,
                    stylesheet_url => '/static/stylesheets/custom-demo.css',
                    shortlists => [
                        { name => 'cheese_sandwiches',
                          label => 'Cheese Sandwich',
                          options_url => '_this_is_dummy_'
                      }
                    ],
                    new_candidate_url => '_this_is_dummy_',
                },
        custom_fields => [
            {
                name => 'field_1',
                asHttpHeader => 0,
                content => 'Field 1 Content',
            },
            {
                name => 'field_2',
                asHttpHeader => 1,
                content => 'Field 2 Content',
            },
            {
                name => 'field_3',
                asHttpHeader => 0,
                content => "нолюёжжэ",
            },
            {
                name => 'field_4',
                asHttpHeader => 1,
                content => "Jérôme Étévé",
            }
        ]
    },
    expected => {
        custom_fields => {
            headers => {
                field_2 => Encode::encode( 'MIME-Q', 'Field 2 Content' ),
                field_4 => Encode::encode( 'MIME-Q', "Jérôme Étévé" ),
            },
            xdoc => {
                num_fields => 2,
            },
        },
        subscriptions => {
            talentsearch => {
                type        => "internal",
                auth_tokens => { client_id => "pat" },
                nice_name   => "TalentSearch",
            },
            dummy => {
                type        => "dummy",
                auth_tokens => {},
                nice_name   => "Dummy",
            }
        },
        grant_response     => $grant_response,
        status             => 200,
        template_should_be => "talentsearch"
    }
};

my $location_api_module =
  Test::MockModule->new('Bean::Juice::APIClient::Location');
$location_api_module->mock(
    'find' => sub {
        my ( $self, $param_ref ) = @_;
        is( $param_ref->{latitude},  $criteria_ref->{location}->{latitude} );
        is( $param_ref->{longitude}, $criteria_ref->{location}->{longitude} );
        # Note that test_location is a package (see below)
        return "test_location";
    }
);

$location_api_module->mock(
    find_by_id => sub{
        my ($self, $id) = @_;
        is( $id , 39550 , "Ok london is being queried");
        # Note this is a package (see below).
        return "london_location";
    }
);

my $oauth_cli_module = Test::MockModule->new('Bean::OAuth::AsyncClient');
my $user_module      = Test::MockModule->new('Stream2::O::SearchUser');
my $feed_tokens      = Test::MockModule->new("Stream2::FeedTokens");
$user_module->mock(
    'conform_to_sibling_settings' => sub {
        return 1;
    }
);

# hard code some static facets to prove that "GET /ripple/status" will include
# them in its response
$feed_tokens->mock(
    tokens => sub { [
        { Id => 'Pepsi' }, { Id => 'Sprite' }, { Id => 'Tango' }
    ] }
);

my $user_id = 500;

$t->app->helper(
    queue_job => sub {
        my ( $self, %params ) = @_;
        my $criteria = $stream2->stream_schema()->resultset('Criteria')
          ->find( { id => $params{parameters}->{criteria_id} } );
        is(
            $params{parameters}->{template_name},
            $test->{expected}->{template_should_be},
            "correct board is chosen"
        );

        if ( $test->{request}->{config}
            && ( my $rpp = $test->{request}->{config}->{results_per_page} ) )
        {
            is( $params{parameters}->{options}->{results_per_page},
                $rpp, "results per page is passed in as a search option" );
        }

        is(
            $criteria->keywords(),
            $criteria_ref->{keywords},
            "criteria from json request is stored"
        );
        is_deeply( $criteria->tokens()->{'cv_updated_within'},
            ['3M'], 'Ok updated within has got default value' );
        is_deeply( $criteria->tokens()->{location_id} , [ '39550' ] );
        is_deeply( $criteria->tokens()->{location} , [ 'London, England' ] );
        # If you are wondering why this is London's location ID, and not North East London,
        # see https://broadbean.atlassian.net/browse/SEAR-2654
        my $tokens = $criteria->tokens;
        is(
            $tokens->{include_unspecified_salaries}->[0],
            $criteria_ref->{include_unspecified_salaries},
            "criteria from json request is stored"
        );
        # Note that test_job is a class (see below)
        return "test_job";
    }
);
$t->app->helper(
    'retrieve_job' => sub {
        # Note that test_job is a class (see below)
        return "test_job";
    }
);

$oauth_cli_module->mock(
    'credentials_grant' => sub {
        my ( $self, %params ) = @_;
        my $d = Promises::deferred;

        if (   $params{signature} eq $auth2
            && $params{time_ms} eq $time_ms
            && $params{api_key} eq $api_key
            && $params{username} eq $username )
        {
            $test->{expected}->{grant_response}->{user}->{user_id} = $user_id++;
            $d->resolve( $test->{expected}->{grant_response} );
        }
        else {
            $d->reject( "Bad Credentials" );
        }

        return $d->promise;
    }
);
$user_module->mock(
    'subscriptions_ref' => sub {
        return $test->{expected}->{subscriptions} // {};
    }
);

$t->post_ok(
    '/ripple/search',
    { Accept => "application/json", "Content-Type" => "application/json" },
    $t->app()->stream2()->json->encode( $test->{request} // {} )
  )->status_is( $test->{expected}->{status} // 200 )
  ->json_like( 'ripple_session_id', qr/^\S+$/ );

# use DDP;
# p $t->tx->res->headers();
ok( !$t->tx->res->headers()->header('set-cookie'), "Ok no set cookie here" );
unless ( $test->{expected}->{status} == 200 ) {
    next;
}
ok( $t->tx->res->json()->{status_url}, "Ok got status_url" );
ok( $t->tx->res->json()->{search_url},
    "Ok got URL to the same search (criteria + channel)" );


my $session = $t->app()->sessions()->storage()->list_sessions()->[0];

is_deeply( $session->{ripple_settings} ,
           {
               'login_record_id' => 1,
               'login_provider' => 'adcourier_ats', # The ripple settings are enriched with the login provider.
               'new_candidate_url' => '_this_is_dummy_',
               'stylesheet_url' => '/static/stylesheets/custom-demo.css',
               'custom_lists' => [
                   {
                       'name' => 'cheese_sandwiches',
                       'label' => 'Cheese Sandwich',
                       'options_url' => '_this_is_dummy_'
                   }
               ],
               'custom_fields' => [
                   {
                       name => 'field_1',
                       asHttpHeader => 0,
                       content => 'Field 1 Content',
                   },
                   {
                       name => 'field_2',
                       asHttpHeader => 1,
                       content => 'Field 2 Content',
                   },
                   {
                       name => 'field_3',
                       asHttpHeader => 0,
                       content => "нолюёжжэ",
                   },
                   {
                       name => 'field_4',
                       asHttpHeader => 1,
                       content => "Jérôme Étévé",
                   }
               ]
           } , "Ok good ripple settings in the session" );

{
    # Get the user and check it injects the right custom fields in
    # the places it's supposed to.
    ok( my $user = $t->app()->stream2()->find_user( $session->{user_id} , $session->{ripple_settings} ) );
    {
        my $xdoc = XML::LibXML::Document->new( '1.0', 'UTF-8' );
        my $cfields = $user->ripple_xdoc_custom_fields( $xdoc );
        $xdoc->setDocumentElement( $cfields );
        #<?xml version="1.0" encoding="UTF-8"?>
        # <CustomFields>
        # <CustomField name="plainNumOne">1</CustomField><CustomField name="customString">boudin blanc</CustomField><CustomField name="utf8String">нолюёжжэ</CustomField>
        # <CustomField name="latin1String">Jérôme Étévé</CustomField>
        #</CustomFields>;
        my @fields = $xdoc->findnodes( '/CustomFields/CustomField' );
        is( scalar( @fields ) , $test->{expected}->{custom_fields}->{xdoc}->{num_fields} // 0  , "Ok good number of custom fields in xDoc (where asHttpHeader is false");
    }

    my $headers = $user->ripple_header_custom_fields();

    is_deeply( $headers , $test->{expected}->{custom_fields}->{headers} // {} , "Ok good headers" );
}

my $status_url = $t->tx->res->json()->{status_url};

# diag("STATUS URL: ". $t->tx->res->json()->{status_url} );

# Two ways to get the status
# First with the X-Ripple-Session-Id header:
{
    my $ripple_session_id = $t->tx->res->json()->{'ripple_session_id'};
    $t->get_ok(
        '/ripple/status',
        {
            Accept                => "application/json",
            "Content-Type"        => "application/json",
            'X-Ripple-Session-Id' => $ripple_session_id
        }
    )->status_is(200)->json_like( '/status', qr/FAKESTATUS/ );
}

# Second with the straight ripple status URL (No need for header,
# the magic token is already in the URL.
{
    $t->get_ok( $status_url,
        { Accept => "application/json", "Content-Type" => "application/json" } )
      ->status_is(200)->json_like( '/status', qr/FAKESTATUS/ );

    my $json      = $t->tx->res->json;
    my @facet_ids = map { $_->{Id} } @{ $json->{context}->{result}->{facets} };
    is_deeply \@facet_ids, [ qw/Pepsi Sprite Tango/ ],
        'static facets - from Stream2::FeedTokens::tokens() - were present ' .
        'in response';
}

done_testing();

sub _generate_digest {
    my ( $key, $data ) = @_;

    my $digest = hmac_sha256_hex( $data, $key );

    return $digest;
}

{

    package test_job;
    sub guid   { 1; }
    sub status { 'FAKESTATUS' }
    sub result { { 'fakeresult' => 1 }; }
    sub parameters { { template_name => 'talentsearch' } }
}

{
    package test_location;
    sub id            { '70881' ; } # This is 'North East London'.
    # If you wonder why this is tested here, see https://broadbean.atlassian.net/browse/SEAR-2654
    sub specific_path { "seven seas"; }
}

{
    package london_location;
    sub id            { '39550' ; }
    sub specific_path { "London, England"; }
}

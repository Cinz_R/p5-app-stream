#! perl -w
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::More;
use Test::MockModule;
use Test::Mojo::Stream2;

my $t = Test::Mojo::Stream2->new();

$t->ua->max_redirects( 1 ); # Allow one redirection (for the magic URLs)

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

my $username = q{pat@pat.pat.pat};
my $password = q{patlovesbat};
my $api_key = "4321";

my %request = (
    api_key => $api_key,
    channel => {
        type => 'external',
        name => 'dummy',
    }
);

my $oauth_cli_module = Test::MockModule->new('Bean::OAuth::AsyncClient');

$t->app->helper( queue_job => sub {
    return test_job;
});
$t->app->helper( retrieve_job => sub {
    return test_job;
});

my $grant_response = {
     user => {
             read_stream_subscriptions => {
                 talentsearch => {
                     type => "internal",
                     auth_tokens => { client_id => "pat" },
                     nice_name => "TalentSearch",
                 },
                 dummy => {
                     type => "dummy",
                     auth_tokens => {},
                     nice_name => "Dummy",
                 }
             },
             read_contact_details => {
                 contact_email => q{pat@example.com},
                 company => "test"
             }
     },
     access_token => "it doesnt matter"
};

$oauth_cli_module->mock('credentials_grant' => sub {
    my ( $self, %params ) = @_;
    my $d = Promises::deferred;

    if ( $params{password} eq $password &&
         $params{api_key} eq $api_key &&
         $params{username} eq $username ){

        $grant_response->{user}->{user_id} = 500;
        $d->resolve( $grant_response );
    }
    else{
        $d->reject( "Bad Credentials" );
    }

    return $d->promise;
});

{ # successful login with username/password
    $request{account}{username} = $username;
    $request{account}{password} = $password;
    $t->post_ok( '/ripple/search',
        { Accept => "application/json", "Content-Type" => "application/json" },
        json => \%request )
        ->status_is( 200 )
        ->json_like('ripple_session_id' , qr/^\S+$/ );

    ok( $t->tx->res->json()->{status_url} , "ripple search request is successful with username/password login");
    ok( $t->tx->res->json()->{search_url} , "ripple search returned search_url with username/password login");
}

{ # oauth client returns error with incorrect username/password
    $request{account}{username} = $username;
    $request{account}{password} = 'nogood';
    $t->post_ok( '/ripple/search',
        { Accept => "application/json", "Content-Type" => "application/json" },
        json => \%request )
        ->status_is( 401 )
        ->json_like('/error/message', qr/Bad Credentials/);
}

{ # validation error with username/password
    $request{account}{username} = $username;
    $request{account}{password} = undef;
    $t->post_ok( '/ripple/search',
        { Accept => "application/json", "Content-Type" => "application/json" },
        json => \%request )
        ->status_is( 400 )
        ->json_is('/error/message', 'required field account/signature OR account/password missing or invalid. ');
}

LWP::UserAgent::Mockable->finished();
done_testing();

{
    package test_job;
    sub guid { 1; }
    sub status{ 'FAKESTATUS' }
    sub parameters{ {} }
    sub result{ { 'fakeresult' => 1 }; }
}

{
    package test_location;
    sub id { 5; }
    sub specific_path { "seven seas"; }
}

#! perl -w

use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;

use Promises;

# Don't go to internet
my $auth_mock = Test::MockModule->new('Bean::OAuth::AsyncClient');
$auth_mock->mock(credentials_grant => sub {
    my $d = Promises::deferred;
    return $d->resolve({
        user => {
            user_id => 123,
            read_contact_details => {
                company => 'cinzwonderland'
            },
            read_stream_subscriptions => {
                adcresponses => {
                    nice_name => 'Responses',
                    type => 'internal',
                },
                talentsearch => {
                    nice_name => 'Talent Search',
                    type => 'internal',
                }
            }
        }
    });
});

my $qurious_job_mock = Test::MockModule->new('Qurious::Job');
$qurious_job_mock->mock(enqueue => sub { return 1; });
$qurious_job_mock->mock(guid => sub { return int(rand(100)); });

# Mock controller to test setting of session values and assessment of request params
my $rippleapi_mock = Test::MockModule->new('App::Stream2::Ripple::Json');

$rippleapi_mock->mock(_login_with_adcourier_ats => sub {
    my ($controller, $request_ref) = @_;
    unless ( $request_ref->{refresh_session} ) {
        ok($request_ref->{manage_responses_view}, 'manage responses view was passed to sign in the user');
    }
    my $promise = $rippleapi_mock->original('_login_with_adcourier_ats')->($controller, $request_ref);
    $promise->then(sub {
        my $current_user = shift;
        if ($request_ref->{refresh_session}) {
            is($controller->session('manage_responses_view'), 0, 'manage responses session is not set when refreshing session to go into search');
        }
        else {
            is($controller->session('manage_responses_view'), 1, 'manage responses session variable is set after successful authentication');
        }
        return $current_user;
    });
    return $promise;
});

$rippleapi_mock->mock(single_search => sub {
    my ($controller, $criteria, $user, $request_ref, $search_options) = @_;
    if ( $controller->session('manage_responses_view') ) {
        is($request_ref->{channel}->{name}, 'adcresponses', 'set adcresponses as the channel to search on');
    }
    return $rippleapi_mock->original('single_search')->($controller, $criteria, $user, $request_ref, $search_options);
});

my $t = Test::Mojo::Stream2->new();
$t->post_ok(
    '/ripple/login',
    json => {
        api_key => 'sometestingapikey',
        account => {
            username => 'cinzia.second@team_one.London.cinzwonderland',
            password => 'wejaoiwjefioejf'
        },
        manage_responses_view => 1,
        config => {
            cb_oneiam_user => 'hien.vu@careerbuilder.com',
        },
        custom_fields => [
            { name => 'saussage',  asHttpHeader => 0,  content => 'Field 1 Content' },
            { name => 'mash',  asHttpHeader => 1,  content => 'Field 2 Content' },
        ]
    })
    ->status_is( 200 )
    ->json_is( '/status', 'OK' )
    ->json_like( '/ripple_session_id', qr/\A[a-z0-9\-]+\z/ );

#    diag(explain($t->tx));

my $ripple_login_json = $t->tx->res->json;
my $session_url = $ripple_login_json->{session_url};
ok( $ripple_login_json->{STOK} , "Ok got session magic token");

my $session = $t->app()->sessions()->storage()->list_sessions()->[0];
is( $session->{ripple_settings}->{cb_oneiam_user} , 'hien.vu@careerbuilder.com' );
my $user = $t->app()->stream2()->factory('SearchUser')->find( $session->{user_id} );
is( $session->{ripple_settings}->{custom_fields}->[0]->{name} , 'saussage' );
is( $session->{ripple_settings}->{custom_fields}->[1]->{name} , 'mash' );


$t->get_ok($session_url)->status_is(302);

$t->post_ok(
    '/ripple/search',
    json => {
        api_key => 'sometestingapikey',
        account => {
            username => 'cinzia.second@team_one.London.cinzwonderland',
            signature => {
                hash => "39ce30deacac4ba833db75771244ad3fdc26d09bac422d9f5c9ead16898173e8",
                time => 1424446101000
            },
        },
        channel => {
            type => 'internal',
            name => 'talentsearch',
        },
        manage_responses_view => 1,
    })
    ->status_is(200);
#   diag(explain($t->tx));

$t->post_ok(
    '/ripple/search',
    json => {
        api_key => 'sometestingapikey',
        account => {
            username => 'cinzia.second@team_one.London.cinzwonderland',
            signature => {
                hash => "39ce30deacac4ba833db75771244ad3fdc26d09bac422d9f5c9ead16898173e8",
                time => 1424446101000
            },
        },
        channel => {
            name => 'talentsearch',
        },
        refresh_session => 1,
    })
    ->status_is(200);
# diag(explain($t->tx));

done_testing();

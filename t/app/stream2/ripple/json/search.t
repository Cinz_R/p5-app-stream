#! perl -w

use Test::More;
use Test::MockModule;
use Test::Mojo::Stream2;

# use Log::Log4perl qw/:easy/;
# use Log::Any::Adapter qw/Stderr/;

use JSON;
use Digest::SHA qw(hmac_sha256_hex);

# TEST SETUP
my $t = Test::Mojo::Stream2->new();

# DO _NOT_ LOGIN. This would set a cookie and
# a session against $t, rendering the tests below
# uneffective at testing alternate sessions.
# $t->login_ok();

$t->ua->max_redirects( 1 ); # Allow one redirection (for the magic URLs)

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}
my $stream2 = $t->app->stream2;
my $schema = $stream2->stream_schema();

my $criteria_ref = {
    ofccp_context => 'Pinguin Groomer',
    keywords => "sausages",
    location => { postcode => "lemon-party", country => "North Korea" },
    include_unspecified_salaries => JSON::false()
};


my $secret_key = q{1234};
my $username = q{pat@pat.pat.pat};
my $api_key = "4321";
my $time_ms = time()  * 1000;
my $auth2 = _generate_digest( $secret_key, join( '|', $username, $time_ms, $api_key ) );

my $grant_response = {
    user => {
            read_stream_subscriptions => {
                talentsearch => {
                    type => "internal",
                    auth_tokens => { client_id => "pat" },
                    nice_name => "TalentSearch",
                },
                dummy => {
                    type => "dummy",
                    auth_tokens => {},
                    nice_name => "Dummy",
                }
            },
            read_contact_details => {
                contact_email => q{pat@example.com},
                company => "test"
            }
    },
    access_token => "it doesnt matter"
};



my $requests = [
    {
        request => {},
        expected => {
            status => 400
        }
    },
    {
        request => {
            api_key => $api_key
        },
        expected => {
            status  => 400
        }
    },
    {
        request => {
            api_key => $api_key,
            account => {
                username    => $username,
                signature   => {
                    hash    => "kittens",
                    time    => $time_ms
                }
            },
            query => $criteria_ref,
            channel => {
                type => "external",
                name => "talentsearch"
            }
        },
        expected => {
            grant_response => $grant_response,
            status => 401
        }
    },
    {
        request => {
            api_key => $api_key,
            account => {
                username    => $username,
                signature   => {
                    hash    => $auth2,
                    time    => $time_ms
                }
            },
            query => $criteria_ref,
            channel => {
                type => "internal",
            }
        },
        expected => {
            grant_response => $grant_response,
            subscriptions => {
                dummy => { type => "external", auth_tokens => {}, nice_name => "Dummy" }
            },
            status => 400
        }
    },
    {
        request => {
            api_key => $api_key,
            account => {
                username    => $username,
                signature   => {
                    hash    => $auth2,
                    time    => $time_ms
                }
            },
            query => $criteria_ref,
            channel => {
                type => "internal",
            }
        },
        expected => {
            subscriptions => {
                talentsearch => {
                    type => "internal",
                    auth_tokens => { client_id => "pat" },
                    nice_name => "TalentSearch",
                },
                dummy => {
                    type => "dummy",
                    auth_tokens => {},
                    nice_name => "Dummy",
                }
            },
            grant_response => $grant_response,
            status => 200,
            template_should_be => "talentsearch"
        }
    },
    {
        request => {
            api_key => $api_key,
            account => {
                username    => $username,
                signature   => {
                    hash    => $auth2,
                    time    => $time_ms
                }
            },
            query => $criteria_ref,
            channel => {
                type => "internal",
            }
        },
        expected => {
            subscriptions => {
                any_old_internal_board => {
                    type => "internal",
                    nice_name => "Any old internal board",
                    auth_tokens => {}
                },
                cb_mysupply => {
                    type => "internal",
                    nice_name => "My Supply",
                    auth_tokens => {}
                },
                dummy => {
                    type => "dummy",
                    auth_tokens => {},
                    nice_name => "Dummy",
                }
            },
            grant_response => $grant_response,
            status => 200,
            template_should_be => "cb_mysupply"
        }
    },
    {
        request => {
            api_key => $api_key,
            account => {
                username    => $username,
                signature   => {
                    hash    => $auth2,
                    time    => $time_ms
                }
            },
            query => $criteria_ref,
            channel => {
                type => "internal",
            }
        },
        expected => {
            # type internal can only apply to cb_mysupply and talentsearch for now
            subscriptions => {
                any_old_internal_board => {
                    type => "internal",
                    nice_name => "Any old internal board",
                    auth_tokens => {}
                },
                dummy => {
                    type => "dummy",
                    auth_tokens => {},
                    nice_name => "Dummy",
                }
            },
            grant_response => $grant_response,
            status => 400,
        }
    },
    {
        request => {
            api_key => $api_key,
            account => {
                username    => $username,
                signature   => {
                    hash    => $auth2,
                    time    => $time_ms
                }
            },
            config => { results_per_page => 1 },
            query => $criteria_ref,
            channel => {
                type => "external",
                name => "dummy"
            }
        },
        expected => {
            subscriptions => {
                talentsearch => {
                    type => "internal",
                    auth_tokens => { client_id => "pat" },
                    nice_name => "TalentSearch",
                },
                dummy => {
                    type => "dummy",
                    auth_tokens => {},
                    nice_name => "Dummy",
                }
            },
            grant_response => $grant_response,
            status => 200,
            template_should_be => "dummy"
        }
    }
];

my $location_api_module = Test::MockModule->new('Bean::Juice::APIClient::Location');
$location_api_module->mock('find' => sub {
    my ( $self, $param_ref ) = @_;
    is ( $param_ref->{country}, $criteria_ref->{location}->{country} );
    is ( $param_ref->{postcode}, $criteria_ref->{location}->{postcode} );
    return test_location;
});
my $oauth_cli_module = Test::MockModule->new('Bean::OAuth::AsyncClient');
my $user_module = Test::MockModule->new('Stream2::O::SearchUser');
$user_module->mock( 'conform_to_sibling_settings' => sub {
    return 1;
});
$user_module->mock( 'settings_values_hash' => sub {
    return {
        'criteria-cv_updated_within-default' => '3M',
        'behaviour-search-use_ofccp_field' => 1
    };
});

my $user_id = 500;
foreach my $test ( @$requests ){
    $t->app->helper( queue_job => sub {
        my ( $self,  %params ) = @_;
        my $criteria = $stream2->stream_schema()->resultset( 'Criteria' )->find({ id => $params{parameters}->{criteria_id} });
        is ( $params{parameters}->{template_name}, $test->{expected}->{template_should_be}, "correct board is chosen" );

        if ( $test->{request}->{config} && ( my $rpp = $test->{request}->{config}->{results_per_page} ) ){
            is ( $params{parameters}->{options}->{results_per_page}, $rpp, "results per page is passed in as a search option" );
        }

        is ( $criteria->keywords(), $criteria_ref->{keywords}, "criteria from json request is stored" );
        is_deeply( $criteria->tokens()->{'ofccp_context'} , [ 'Pinguin Groomer' ], "Ok ofccp_conext has got value" );
        is_deeply( $criteria->tokens()->{'cv_updated_within'}, [ '3M' ], 'Ok updated within has got default value');

        my $tokens = $criteria->tokens;
        is ( $tokens->{include_unspecified_salaries}->[0], $criteria_ref->{include_unspecified_salaries}, "criteria from json request is stored" );
        return test_job;
    });
    $t->app->helper( 'retrieve_job' => sub{
                         return test_job;
                     });

    $oauth_cli_module->mock('credentials_grant' => sub {
        my ( $self, %params ) = @_;
        my $d = Promises::deferred;

        if ( $params{signature} eq $auth2 &&
             $params{time_ms} eq $time_ms &&
             $params{api_key} eq $api_key &&
             $params{username} eq $username ){

            $test->{expected}->{grant_response}->{user}->{user_id} = $user_id++;
            $d->resolve( $test->{expected}->{grant_response} );
        }
        else{
            $d->reject( "Bad Credentials" );
        }

        return $d->promise;
    });
    $user_module->mock( 'subscriptions_ref' => sub {
        return $test->{expected}->{subscriptions} // {}
    });

    $t->post_ok( '/ripple/search', { Accept => "application/json", "Content-Type" => "application/json" },
        JSON::encode_json( $test->{request} // {} ) )
        ->status_is( $test->{expected}->{status} // 200 )->json_like('ripple_session_id' , qr/^\S+$/ );
    # use DDP;
    # p $t->tx->res->headers();
    ok( ! $t->tx->res->headers()->header('set-cookie' ) , "Ok no set cookie here" );
    unless( $test->{expected}->{status} == 200 ){
        next;
    }
    ok( $t->tx->res->json()->{status_url} , "Ok got status_url");
    ok( $t->tx->res->json()->{search_url} , "Ok got URL to the same search (criteria + channel)");

    my $status_url = $t->tx->res->json()->{status_url};

    # diag("STATUS URL: ". $t->tx->res->json()->{status_url} );

    # Two ways to get the status
    # First with the X-Ripple-Session-Id header:
    {
        my $ripple_session_id = $t->tx->res->json()->{'ripple_session_id'};
        $t->get_ok( '/ripple/status' , { Accept => "application/json", "Content-Type" => "application/json",
                                         'X-Ripple-Session-Id' => $ripple_session_id } )->status_is( 200 )->json_like('/status', qr/FAKESTATUS/ );
    }
    # Second with the straight ripple status URL (No need for header,
    # the magic token is already in the URL.
    {
        $t->get_ok( $status_url , { Accept => "application/json", "Content-Type" => "application/json" })
            ->status_is( 200 )->json_like('/status', qr/FAKESTATUS/ );
    }

}

done_testing();

sub _generate_digest {
    my ($key, $data) = @_;

    my $digest = hmac_sha256_hex( $data, $key );

    return $digest;
}

{
    package test_job;
    sub guid { 1; }
    sub status{ 'FAKESTATUS' }
    sub result{ { 'fakeresult' => 1 }; }
    sub parameters { { template_name => 'talentsearch' } }
}

{
    package test_location;
    sub id { 5; }
    sub specific_path { "seven seas"; }
}

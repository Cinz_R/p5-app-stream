#!/usr/bin/env perl

use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;
use Promises;
# TEST SETUP
my $t = Test::Mojo::Stream2->new();
my $s2 = $t->app->stream2;

my $grant_response = {
    user => {
            read_stream_subscriptions => {
                talentsearch => {
                    type => "internal",
                    auth_tokens => { client_id => "pat" },
                    nice_name => "TalentSearch",
                },
                dummy => {
                    type => "dummy",
                    auth_tokens => {},
                    nice_name => "Dummy",
                }
            },
            read_contact_details => {
                contact_email => q{pat@example.com},
                company => "test"
            },
            user_id => 1
    },
    access_token => "it doesnt matter"
};

my $test = {
    request => {
        api_key => 'test',
        account => {
            username    => 'test',
            signature   => {
                hash    => 'test',
                time    => 'test'
            }
        }
    },
    subscriptions => {
        talentsearch => {
            type => "internal", # this board won't be searched upon
            auth_tokens => { client_id => "pat" },
            nice_name => "TalentSearch",
        },
        dummy => {
            type => "dummy",
            auth_tokens => {},
            nice_name => "Dummy",
        },
        other_board => {
            type => "external",
            auth_tokens => {},
            nice_name => "OTHER BOARD"
        }
    },
    status => 202,
    template_should_be => "dummy"
};

my $oauth_cli_module = Test::MockModule->new('Bean::OAuth::AsyncClient');
$oauth_cli_module->mock('credentials_grant' => sub {
    my $d = Promises::deferred;

    $d->resolve( $grant_response );

    return $d->promise
});
my $user_module = Test::MockModule->new('Stream2::O::SearchUser');
$user_module->mock( 'conform_to_sibling_settings' => sub {
    return 1;
});

$t->post_ok( '/ripple/login', { Accept => "application/json", "Content-Type" => "application/json" },
    JSON::encode_json( $test->{request} // {} ) )
        ->status_is( 200 )
        ->json_is( '/status', 'OK' )
        ->json_like( '/ripple_session_id', qr/\A[a-z0-9\-]+\z/ );

my $ripple_session = $t->tx->res->json->{ripple_session_id};

my $user = $s2->factory('SearchUser')->search({ group_identity => 'test', provider_id => 1 })->first();

my $criteria_1 = { id => "critid1", tokens => { keywords => [ 'test keywords' ], my_other_token => [ 'sausage', 'tofu' ] } };
$s2->factory('SavedSearches')->create({
    user_id => $user->id,
    criteria => $criteria_1,
    name => 'savedsearch number 1',
    active => 1
});

my $criteria_2 = { id => "critid2", tokens => {} };
$s2->factory('SavedSearches')->create({
    user_id => $user->id,
    criteria => $criteria_2,
    name => 'savedsearch number 2',
    active => 1
});

$t->get_ok( '/api/savedsearches', { 'x-ripple-session-id' => $ripple_session } )
    ->status_is( 200 )
    ->json_has( '/data/searches/1', 'Response has at least two saved searches' )
    ->json_hasnt( '/data/searches/0/tokens', 'Criteria has not been expanded' )
    ->json_is( '/pager/total_entries', 2 );

$t->get_ok( '/api/savedsearches', { 'x-ripple-session-id' => $ripple_session }, form => { prefetch => 'tokens' } )
    ->status_is( 200 )
    ->json_has( '/data/searches/0/tokens', 'Criteria has been expand' )
    ->json_is( '/data/searches/0/tokens/keywords/0', 'test keywords' )
    ->json_is( '/data/searches/1/tokens', {} );

$t->get_ok( '/api/savedsearches', { 'x-ripple-session-id' => $ripple_session }, form => { rows => 1 } )
    ->status_is( 200 )
    ->json_hasnt( '/data/searches/1' )
    ->json_is( '/data/searches/0/name', 'savedsearch number 1' )
    ->json_is( '/pager/total_entries', 2 )
    ->json_is( '/pager/entries_per_page', 1 );

$t->get_ok( '/api/savedsearches', { 'x-ripple-session-id' => $ripple_session }, form => { rows => 1, page => 2 } )
    ->status_is( 200 )
    ->json_hasnt( '/data/searches/1' )
    ->json_is( '/data/searches/0/name', 'savedsearch number 2' );

done_testing();

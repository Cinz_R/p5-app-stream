#! perl -w

use Test::More;
use Test::MockModule;
use Test::Mojo::Stream2;
use App::Stream2::Ripple::Json;

use Log::Log4perl qw/:easy/;
#use Log::Any::Adapter qw/Stderr/;

use JSON;
use Digest::SHA qw(hmac_sha256_hex);

# TEST SETUP
my $t = Test::Mojo::Stream2->new();

# DO _NOT_ LOGIN. This would set a cookie and
# a session against $t, rendering the tests below
# uneffective at testing alternate sessions.
# $t->login_ok();

$t->ua->max_redirects( 1 ); # Allow one redirection (for the magic URLs)

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}
my $stream2 = $t->app->stream2;
my $schema = $stream2->stream_schema();

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 157656,
    group_identity => 'acme',
});

subtest "Has talentsearch and adcresponses" => sub {
    $user->subscriptions_ref({
        adcresponses => {
            type => "internal",
            auth_tokens => {},
            nice_name => "Responses",
        },
        talentsearch => {
            type => "internal",
            auth_tokens => { client_id => "pat" },
            nice_name => "TalentSearch",
        },
        dummy => {
            type => "dummy",
            auth_tokens => {},
            nice_name => "Dummy",
        }
    });

    my $queue_job_called = 0;
    $t->app->helper( queue_job => sub {
        $queue_job_called = 1;
        my ( $self,  %params ) = @_;
        is ( $params{parameters}{template_name}, "talentsearch", "selected template is talentsearch" );
    });

    my $tx = Mojo::Transaction->new();$tx->remote_address( '127.0.0.1' );
    my $c = App::Stream2::Ripple::Json->new->new( app => $t->app, stash => {}, tx => $tx );
    ok( $c->single_search(
        TestObject->new({ id => 123 }),
        $user,
        {
           channel => {
               type => 'internal'
            }
        },
        {}
    ));
    ok( $queue_job_called, "job was queued" );
};

subtest "Has only adcresponses" => sub {
    $user->subscriptions_ref({
        adcresponses => {
            type => "internal",
            auth_tokens => {},
            nice_name => "Responses",
        },
        dummy => {
            type => "dummy",
            auth_tokens => {},
            nice_name => "Dummy",
        }
    });
    my $queue_job_called = 0;
    $t->app->helper( queue_job => sub {
        $queue_job_called = 1;
    });
    my $c_mock = Test::MockModule->new( 'App::Stream2::Ripple::Json' );
    my $render_called = 0;
    $c_mock->mock( render => sub {
        $render_called = 1;
        my ( $self, %stuff ) = @_;
        is ( $stuff{status}, 400, 'bad request status recieved' );
        like ( $stuff{json}{error}{message}, qr/no internal boards/i, 'no internal boards available' );
    });
    my $c = App::Stream2::Ripple::Json->new( app => $t->app, stash => {}, tx => $t->app->build_tx );
    ok( $c->single_search(
        TestObject->new({ id => 123 }),
        $user,
        {
           channel => {
               type => 'internal'
            }
        },
        {}
    ));
    ok( ! $queue_job_called, "job was not queued" );
    ok( $render_called, "an error was thrown" );

};

done_testing();

{
    # TestObject provides a getter for any attribute
    package TestObject;
    use AutoLoader;
    sub new { bless pop, shift; }
    sub AUTOLOAD {
        our $AUTOLOAD;
        my ( $method ) = $AUTOLOAD =~ m/\ATestObject::(.*)\z/;
        return $method eq 'DESTROY' ? () : shift->{$method};
    }
}

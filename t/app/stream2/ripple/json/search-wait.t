#! perl -w

use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;

# use Log::Log4perl qw/:easy/;
# use Log::Any::Adapter qw/Stderr/;

use JSON;
use Digest::SHA qw(hmac_sha256_hex);

use Stream2::Results::Result;

# TEST SETUP
my $t = Test::Mojo::Stream2->new();

# DO _NOT_ LOGIN. This would set a cookie and
# a session against $t, rendering the tests below
# uneffective at testing alternate sessions.
# $t->login_ok();

$t->ua->max_redirects( 1 ); # Allow one redirection (for the magic URLs)

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}
my $stream2 = $t->app->stream2;
my $schema = $stream2->stream_schema();

my $secret_key = q{1234};
my $username = q{pat@pat.pat.pat};
my $api_key = "4321";
my $time_ms = time()  * 1000;
my $auth2 = _generate_digest( $secret_key, join( '|', $username, $time_ms, $api_key ) );

my $grant_response = {
    user => {
        user_id => 5555,
        read_stream_subscriptions => {
            talentsearch => {
                type => "internal",
                auth_tokens => { client_id => "pat" },
                nice_name => "TalentSearch",
            },
            dummy => {
                type => "dummy",
                auth_tokens => {},
                nice_name => "Dummy",
            }
        },
        read_contact_details => {
            contact_email => q{pat@example.com},
            company => "test",
        }
    },
    access_token => "it doesnt matter"
};



my $request = {
    api_key => $api_key,
    account => {
        username    => $username,
        signature   => {
            hash    => $auth2,
            time    => $time_ms
        }
    },
    channel => {
        type => "external",
        name => "dummy"
    }
};

my $oauth_cli_module = Test::MockModule->new('Bean::OAuth::AsyncClient');
my $user_module = Test::MockModule->new('Stream2::O::SearchUser');
$user_module->mock( 'conform_to_sibling_settings' => sub {
    return 1;
});
$user_module->mock( 'settings_values_hash' => sub {
    return {
        'criteria-cv_updated_within-default' => '3M',
        'behaviour-search-use_ofccp_field' => 1
    };
});

my $qurious_job_mock = Test::MockModule->new('Qurious::Job');
$qurious_job_mock->mock(enqueue => sub { return 1; });
$qurious_job_mock->mock(save => sub { return 1; });
$qurious_job_mock->mock(guid => sub { return int(rand(100)); });

my $tokens_called = 0;
my $tokens_mock = Test::MockModule->new( 'Stream2::FeedTokens' );
$tokens_mock->mock( 'tokens' => sub {
    $tokens_called = 1;
    is ( $_[0]->board, 'dummy' );
    return [
        {
            Label => 'I am a static token',
            Id => 'static_token'
        }
    ];
});

my @results = (
    Stream2::Results::Result->new({
        idx => 0,
        email => 'vladimir@thekremlin.com',
        destination => 'sausage',
        location_text => 'Isle of Man',
        name => 'Vladimir Putin',
        firstname => 'Vlad',
        lastname => 'Putin'
    }),
    Stream2::Results::Result->new({
        idx => 1,
        email => 'cameron@downingsteeet.com',
        destination => 'sausage',
        location_text => 'Tokyo',
        name => 'David Cameron'
    }),
    Stream2::Results::Result->new({
        idx => 1,
        email => 'Hollis.Doyle@texas.com',
        destination => 'sausage',
        location_text => 'Texas',
        firstname => 'Hollis',
        lastname => 'Doyle',
    })
);

my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( fetch_results => sub { @results } );
$results_mock->mock( fetch => sub {
    $_[0]->destination( 'sausage' );
    $_[0]->facets([{ Label => 'Industry', Id => 'dummy_industry' }]);
});


$oauth_cli_module->mock('credentials_grant' => sub {
    my ( $self, %params ) = @_;
    my $d = Promises::deferred;
    $d->resolve( $grant_response );
    return $d->promise;
});
$user_module->mock( 'subscriptions_ref' => sub {
    return $grant_response->{user}->{read_stream_subscriptions};
});

my $poll_count = 3;
my $refresh_called = 0;
$qurious_job_mock->mock( refresh => sub {
    my $job = shift;
    $refresh_called = 1;
    unless ( $poll_count-- ){
        $job->complete_job({
            max_pages => 1,
            total_results => 3,
            per_page => 3,
            dont_show_me => 1
        });
    }
});

$t->post_ok( '/ripple/search-results', { Accept => "application/json", "Content-Type" => "application/json" },
    JSON::encode_json( $request // {} ) )
    ->status_is( 200 )
    ->json_is( '/data/facets', [{"Label"=>"Industry",Id=>"dummy_industry"},{"Id"=>"static_token","Label"=>"I am a static token"}] )
    ->json_like( '/data/results/0/actions/cv', qr/\Ahttp/ )
    ->json_like( '/data/results/0/name', qr/\AVladimir Putin/ )
    ->json_like( '/search_url', qr/\Ahttp/ )
    ->json_like( '/ripple_session_id', qr/\A[0-9a-z\-]+\z/i )
    ->json_like( '/data/results/2/name', qr/\AHollis Doyle/ )
    ->json_is( '/context/total_results', 3 )
    ->json_is( '/context/per_page', 3 )
    ->json_hasnt( '/context/dont_show_me' )
    ->json_is( '/context/max_pages', 1 );

is ( $refresh_called, 1, "the qurious job was polled" );
ok ( $poll_count < 1, "the qurious job was polled the appropriate amount of times" );
ok ( $tokens_called, "Feed tokens was called" );

subtest 'test error response' => sub {
    $qurious_job_mock->mock( refresh => sub {
        my $job = shift;
        $job->fail_job({
            error => {
                log_id => '123log_id',
                message => 'should fail',
                properties => {
                    type => 'whatever'
                }
            }
        });
    });

    $t->post_ok( '/ripple/search-results', { Accept => "application/json", "Content-Type" => "application/json" },
        JSON::encode_json( $request ) )
        ->status_is( 400 )
        ->json_has('error')
        ->json_has('error/message')
        ->json_has('error/log_id')
        ->json_has('error/properties/type');
};

done_testing();

sub _generate_digest {
    my ($key, $data) = @_;

    my $digest = hmac_sha256_hex( $data, $key );

    return $digest;
}

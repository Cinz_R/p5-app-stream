#! perl -w

use strict;
use warnings;

# use Carp::Always;
use Crypt::JWT;

use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;
use Promises;

use Log::Any::Adapter qw/Stderr/;

# TEST SETUP
my $t = Test::Mojo::Stream2->new();
my $s2 = $t->app->stream2;

my $api_key = 'abc123';
my $secret_key = 'W0wSuchS3cr3t';

my $jwt_token = Crypt::JWT::encode_jwt(
    payload => {
        provider        => 'third_party',
        group_identity  => 'ABCD',          # 40 chars max.
        group_nice_name => 'Company ABCD',  # Opt. 255 chars max, for reporting.
        provider_id   => 'tk_user_id12345', # 255 chars max.
        contact_name  => 'User Full Name',  # Opt. 255 chars max, for reporting.
        contact_email => 'user@client.com', # Opt. 255 chars max, for reporting
        locale => 'nl',
        subscriptions => {
            dummy        => { auth_tokens => {}, },

            other_board => {
                type        => "somewackytype",
                auth_tokens => { such_token => 'wow' },
            },
            cb_mysupply => {
                type => 'whocares',
                auth_tokens => { cb_mysupply_token => 123 }
            },
            talentsearch => {
                auth_tokens => { talentsearch_client_id => 'cinzwonderland' }
            },
            adcresponses => {
                auth_tokens => {}
            }
        }
    },
    alg          => 'PBES2-HS256+A128KW',
    enc          => 'A128GCM',
    relative_exp => 60,
    key          => $secret_key,
);


# diag("JWT TOKEN: ".$jwt_token);

$t->post_ok( '/ripple/login', { Accept => "application/json", "Content-Type" => "application/json" },
             JSON::encode_json( { api_key => 'sometestingapikey', account => { provider => 'third_party', jwt_token => $jwt_token  } } ) )
    ->status_is( 200 )
    ->json_is( '/status', 'OK' )
    ->json_like( '/ripple_session_id', qr/\A[a-z0-9\-]+\z/ );

my $ripple_session = $t->tx->res->json->{ripple_session_id};

my $user = $s2->factory('SearchUser')->search({ provider => 'third_party' })->first();
is( $user->group_identity(), 'TP96b39e-ABCD' , "Ok group identity contains prefix");
is( $user->provider_id(), 'TP96b39e-tk_user_id12345', "Ok user ID contains prefix");
ok( $user->identity_provider()->isa('App::Stream2::Plugin::Auth::Provider::ThirdParty') , "Identity provider is ThirdParty" );
ok( $user->login_provider()->isa('App::Stream2::Plugin::Auth::Provider::ThirdParty') , "Identity provider is ThirdParty" );
{
    ok( my $sub = $user->has_subscription( 'other_board' ), "Ok has other board subscription");
    ok( ! $user->has_subscription( 'talentsearch' ), 'Third party users can\'t access internal databases' );
    ok( ! $user->has_subscription( 'adcresponses' ), 'Checking to make sure more than one internal database is removed' );
    ok( ! $user->has_subscription( 'cb_mysupply' ), 'Checking that internal boards are unsubscribable' );
    is( $sub->{auth_tokens}->{such_token} , 'wow' , "Ok good auth_token");

    ok( my $board = $s2->factory('Board')->find({ name => 'other_board' }) );
    is( $board->nice_name() , 'other_board' , "Ok got default name");
    is( $board->type() , 'external' , "Ok got default type, as we dont trust third parties to set board meta properties");
}

# Can we use saved searches?
my $criteria_1 = { id => "critid1", tokens => { keywords => [ 'test keywords' ], my_other_token => [ 'sausage', 'tofu' ] } };
$s2->factory('SavedSearches')->create({
    user_id => $user->id,
    criteria => $criteria_1,
    name => 'savedsearch number 1',
    active => 1
});

my $criteria_2 = { id => "critid2", tokens => {} };
$s2->factory('SavedSearches')->create({
    user_id => $user->id,
    criteria => $criteria_2,
    name => 'savedsearch number 2',
    active => 1
});

$t->get_ok( '/api/savedsearches', { 'x-ripple-session-id' => $ripple_session } )
    ->status_is( 200 )
    ->json_has( '/data/searches/1', 'Response has at least two saved searches' )
    ->json_hasnt( '/data/searches/0/tokens', 'Criteria has not been expanded' )
    ->json_is( '/pager/total_entries', 2 );

$t->get_ok( '/api/user/details', { 'x-ripple-session-id' => $ripple_session } )
    ->status_is( 200 )
    ->json_is( '/details/contact_details/locale', 'nl' );

done_testing();

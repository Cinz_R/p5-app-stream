use Test::Most;

use Data::Dumper;
use JSON qw/decode_json/;
use Mojo::URL;
use Test::Mojo::Stream2;
use Test::MockModule;

my $t = Test::Mojo::Stream2->new();
$t->ua->max_redirects(1);
$t->ua->on(start => sub {
    my ($ua, $tx) = @_;
    $tx->req->headers->accept('text/xml');
});

my $oauth_mock = Test::MockModule->new('Bean::OAuth::AsyncClient');
$oauth_mock->mock( credentials_grant => sub {
    my ($self, %params) = @_;
    my $d = Promises::deferred;
    $d->resolve({
        'expires_in' => 43200,
        'user' => {
            'user_role' => 'USER',
            'user_id' => '209669',
            'read_contact_details' => {
                'contact_telephone' => '1234567894',
                'contact_email' => 'jerome@broadbean.com',
                'contact_name' => 'Search Demo',
                'contact_devuser' => undef,
                'company' => 'andy'
            },
            'read_navigation_tabs' => [],
            'read_stream_subscriptions' =>
                {
                    'talentsearch' => {
                        'nice_name' => 'Talent Search',
                        'auth_tokens' => {
                            'talentsearch_security_key' => 'a9ad6247-8d14-4df9-8364-3de9ff613c5a'
                        },
                        'type' => 'internal',
                        'user_role' => 'USER',
                        'user_id' => '209669'
                    },
                }
        },
        'access_token' => '26E42650-3356-11E4-8CB0-F98427886EDC',
        'token_type' => 'Bearer'
    });
    return $d->promise;
});

my $user_mock = Test::MockModule->new('Bean::Juice::APIClient::User');
$user_mock->mock( provider_settings => sub {
    return { provider_name => 'bond', webhooks => [ { config => {tagdoc_type => 'hr_xml'}, url => 'http://www.adcourier.com/dump_stuff.cgi' } ] };
});
$user_mock->mock( siblings => sub { return []; });


$t->post_ok( '/ripple', q|<?xml version="1.0" encoding="UTF-8" ?>
<Search>
  <Version>3.0</Version>
  <APIKey>BBTECH_RESERVED_IP</APIKey>
  <Account>
    <UserName>searchdemo@users.live.andy</UserName>
    <Password>stream4life</Password>
  </Account>
  <Query>
    <Keywords>Project Manager</Keywords>
  </Query>
  <Settings>
    <HideShortlist>true</HideShortlist>
    <HideDownload>true</HideDownload>
    <HideChargeableMessage>true</HideChargeableMessage>
  </Settings>
</Search>| )
  ->status_is(200);

#diag $t->tx->res->to_string;

my ( $url ) = $t->tx->res->content->get_body_chunk(0) =~ /<URL>(.+)<\/URL>/;
$url = Mojo::URL->new( $url );

my $session         = $t->get_session_from_STOK( $url );
my $ripple_settings = $session->{ripple_settings};

ok $ripple_settings->{no_adcourier_shortlist}, 'no_adcourier_shortlist is true in DB';
ok $ripple_settings->{hide_download}, 'hide_download is true in DB';
ok $ripple_settings->{hide_chargeable_message}, 'hide_chargeable_message is true in DB';
ok !$ripple_settings->{hide_forward}, 'hide_forward is false in DB';

$t->get_ok( $url->path->to_string . '?' . $url->query )
  ->status_is(200);

#diag $t->tx->res->to_string;

$t->get_ok( '/api/user/details' )
  ->status_is(200)
  ->json_has( '/details/feature_exclusions' );

is_deeply decode_json( $t->tx->res->content->get_body_chunk(0) )->{details}->{feature_exclusions}, {
    no_adcourier_shortlist => 1,
    hide_download => 1,
    hide_chargeable_message => 1,
}, 'Only chosen exclusions are set';

done_testing;

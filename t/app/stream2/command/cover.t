use Test::Most;

my $command = 'App::Stream2::Command::cover';
use_ok($command);

my $cover = $command->new;
ok $cover->description, 'has a description';
ok $cover->usage,       'has usage information';

done_testing;

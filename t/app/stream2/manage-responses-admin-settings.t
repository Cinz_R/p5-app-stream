use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;
use JSON ();
use Promises;

### Proves that some Admin settings are disabled in the New Manage Responses
### view of Search

my $t = Test::Mojo::Stream2->new();

### mock underlying infrasturcture

my $queue_mock = Test::MockModule->new('App::Stream2::Plugin::Queue');
$queue_mock->mock( queue_job => sub{ 1; } );

my $user_mock = Test::MockModule->new('Stream2::O::SearchUser');
$user_mock->mock( is_overseer => sub { 1 } );

my $oauth_mock = Test::MockModule->new('Bean::OAuth::AsyncClient');
$oauth_mock->mock( exchange_code => sub {
    my $d = Promises::deferred;
    $d->resolve( {
        user => {
            read_contact_details => { company => 'dev' },
            user_id              => 12345,
            read_navigation_tabs => [],
            is_admin             => 1,
            is_overseer          => 1
        }
    } );
    return $d->promise;
} );

### assertions...

subtest search_mode => sub {
    $t->get_ok('/auth/adcourier?code=whatever')
      ->status_is(302)
      ->header_is( Location => '/search' );

    $t->get_ok('/api/user/details')
      ->status_is(200)
      ->json_is( '/details/search_mode', 'search' );

    cmp_ok( count_search_mode_only_settings_found(), '>=', 1,
        "Search mode: some search-mode-only admin settings found" );
};

subtest responses_mode => sub {
    my $json_state = JSON::encode_json({ manage_responses_view => 1 });

    $t->get_ok('/?manage_responses_view=1')
      ->status_is(302)
      ->header_like('Location',qr/%22manage_responses_view%22%3A\+?1/);

    $t->get_ok(
        '/auth/adcourier', {},
        form => { code => 'whatever', state => $json_state }
    )->status_is(302)
     ->header_is( Location => '/search' );

    $t->get_ok('/api/user/details')
      ->status_is(200)
      ->json_is( '/details/search_mode', 'responses' );

    cmp_ok( count_search_mode_only_settings_found(), '==', 0,
        "Responses mode: no search-mode-only admin settings found" );
};

# returns a count of the search-mode-only admin settings that
# /api/admin/available_settings returns.
sub count_search_mode_only_settings_found {
    my @search_mode_only_settings = qw/
        candidate-actions-import-enabled
        candidate-actions-update-enabled
        behaviour-downloadcv-use_internal
        criteria-cv_updated_within-default
    /;

    $t->get_ok( '/api/admin/available_settings?q={}&ts=' . time() * 1000 );
    my $body      = $t->tx->res->body;
    my $json_body = JSON::decode_json( $body );

    my $settings_found = 0;
    for my $setting ( @$json_body ) {
        $settings_found++ if grep { $setting->{mnemonic} eq $_ }
            @search_mode_only_settings;
    }
    return $settings_found;
}

done_testing();

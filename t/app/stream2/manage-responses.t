#!/usr/bin/env perl

use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;
use JSON ();
use Promises;

my $t = Test::Mojo::Stream2->new();

my $queue_mock = Test::MockModule->new('App::Stream2::Plugin::Queue');
$queue_mock->mock( 'queue_job' => sub{ 1; } );
my $user_mock = Test::MockModule->new('Stream2::O::SearchUser');
$user_mock->mock( 'conform_to_sibling_settings' => sub { 1; } );

my $oauth_mock = Test::MockModule->new('Bean::OAuth::AsyncClient');
$oauth_mock->mock( exchange_code => sub {
    my $d = Promises::deferred;
    $d->resolve({
        user => {
            read_contact_details => { company => 'dev' },
            user_id => 12345,
            read_navigation_tabs => []
        }
    });
    return $d->promise;
});

subtest "normal view" => sub {
    $t->get_ok('/auth/adcourier?code=whatever')
        ->status_is(302)
        ->header_is( Location => '/search' );

    $t->get_ok('/api/user/details')
        ->status_is(200)
        ->json_is( '/details/search_mode', 'search' );
};

subtest "responses view" => sub {
    my $json_state = JSON::encode_json({ manage_responses_view => 1 });

    $t->get_ok('/?manage_responses_view=1')
        ->status_is(302)
        ->header_like('Location',qr/%22manage_responses_view%22%3A\+?1/);

    $t->get_ok('/auth/adcourier', {}, form => { code => 'whatever', state => $json_state })
        ->status_is(302)
        ->header_is( Location => '/search' );

    $t->get_ok('/api/user/details')
        ->status_is(200)
        ->json_is( '/details/search_mode', 'responses' );

    $t->get_ok('/exit-responses')
        ->status_is(200);

    $t->get_ok('/api/user/details')
        ->status_is(200)
        ->json_is( '/details/search_mode', 'search' );
};

subtest "responses view user with advert" => sub {
    my $json_state = JSON::encode_json({ manage_responses_view => 1, criteria_id => 'iamaduck' });

    $t->get_ok('/?manage_responses_view=1&adcresponses_broadbean_adcadvert_id=123')
        ->status_is(302)
        ->header_like('Location',qr/%22manage_responses_view%22%3A\+?1/)
        ->header_like('Location',qr/criteria_id/);

    $t->get_ok('/auth/adcourier', {}, form => { code => 'whatever', state => $json_state })
        ->status_is(302)
        ->header_is( Location => '/search/iamaduck' );

};

subtest 'should auto set cv_update_within to all' => sub {
    $t->get_ok('/?manage_responses_view=1&adcresponses_broadbean_adcadvert_id=123');

    my $redir_url = $t->tx->res->headers->header('Location');
    my $uri = URI->new( $redir_url );
    my %params = $uri->query_form();
    my $state_ref = JSON::decode_json($params{state});
    my $criteria_row = $t->app->stream2->factory('Criteria')->search({ id => $state_ref->{criteria_id} })->first();
    is ( $criteria_row->tokens->{cv_updated_within}->[0], 'ALL', 'has defaulted cv_updated_within to all' );
    is ( $criteria_row->tokens->{adcresponses_broadbean_adcadvert_id}->[0], '123', 'advert id is saved' );


    $t->get_ok('/?manage_responses_view=1&adcresponses_broadbean_adcadvert_id=123&cv_updated_within=3M');
    $redir_url = $t->tx->res->headers->header('Location');
    $uri = URI->new( $redir_url );
    %params = $uri->query_form();
    $state_ref = JSON::decode_json($params{state});
    $criteria_row = $t->app->stream2->factory('Criteria')->search({ id => $state_ref->{criteria_id} })->first();

    is ( $criteria_row->tokens->{cv_updated_within}->[0], '3M', 'has taken my option' );
};


$oauth_mock->mock( exchange_code => sub {
    my $d = Promises::deferred;
    $d->resolve({
        user => {
            read_contact_details => { company => 'dev' },
            user_id => 12345,
            read_navigation_tabs => [],
            user_role => 'SUPERADMIN'
        }
    });
    return $d->promise;
});

# a admin log into NMR
subtest "responses view as admin" => sub {
    my $json_state = JSON::encode_json({ manage_responses_view => 1 });

    $t->get_ok('/?manage_responses_view=1')
        ->status_is(302)
        ->header_like('Location',qr/%22manage_responses_view%22%3A\+?1/);

    $t->get_ok('/auth/adcourier', {}, form => { code => 'whatever', state => $json_state })
        ->status_is(302)
        ->header_is( Location => '/search' );
};



# a admin with a advert_id and no consultant_id we should redirect to /search
subtest "responses view admin with advert" => sub {
    my $json_state = JSON::encode_json({ manage_responses_view => 1, criteria_id => 'iamaduck' });

    $t->get_ok('/?manage_responses_view=1&adcresponses_broadbean_adcadvert_id=123')
        ->status_is(302)
        ->header_like('Location',qr/%22manage_responses_view%22%3A\+?1/)
        ->header_like('Location',qr/criteria_id/);

    $t->get_ok('/auth/adcourier', {}, form => { code => 'whatever', state => $json_state })
        ->status_is(302)
        ->header_is( Location => '/search' );
};


# a admin with a advert_id and consultant_id we should redirect to /search
subtest "responses view admin with advert and consultant " => sub {
    my $json_state = JSON::encode_json({ manage_responses_view => 1, criteria_id => 'iamaduck', consultant_id => 1212 });

    $t->get_ok('/?manage_responses_view=1&adcresponses_broadbean_adcadvert_id=123&consultant_id=1212')
        ->status_is(302)
        ->header_like('Location',qr/%22manage_responses_view%22%3A\+?1/)
        ->header_like('Location',qr/criteria_id/);

    $t->get_ok('/auth/adcourier', {}, form => { code => 'whatever', state => $json_state })
        ->status_is(302)
        ->header_is( Location => '/login/as/1212?redirect_to=/search/iamaduck' );
};


# a admin with a consultant_id we should redirect to /search
subtest "responses view admin with no advert and consultant " => sub {
    my $json_state = JSON::encode_json({ manage_responses_view => 1, consultant_id => 1212 });

    $t->get_ok('/?manage_responses_view=1&consultant_id=1212')
        ->status_is(302)
        ->header_like('Location',qr/%22manage_responses_view%22%3A\+?1/);

    $t->get_ok('/auth/adcourier', {}, form => { code => 'whatever', state => $json_state })
        ->status_is(302)
        ->header_is( Location => '/search' );
};


done_testing();

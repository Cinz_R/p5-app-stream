use Test::Most tests => 1;
use Test::Mojo::Stream2;

my $t = Test::Mojo::Stream2->new();

# Make sure the secret key is changed
isnt $t->app->secrets()->[0], $t->app->moniker, "Using a custom secret key";

done_testing();

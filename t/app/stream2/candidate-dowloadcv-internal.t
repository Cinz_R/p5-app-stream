use strict; use warnings;
use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;
use Stream2::Action::DownloadCV;
use MIME::Base64;
use HTTP::Response;
# use Log::Any::Adapter qw/Stderr/;

use Bean::TSImport::Client;

my $t = Test::Mojo::Stream2->new({stream2_class => 'Test::Stream2'});
$t->login_ok;

my $user = $t->app->stream2->factory('SearchUser')->find(1);
my $setting = $t->app->stream2->factory('Setting')->find({ setting_mnemonic => 'behaviour-downloadcv-use_internal'});

$setting->update({
                  setting_description => '',
                  setting_type => 'string',
                  default_string_value => '0',
                  setting_meta_data => { options => [] }
                 });

$setting->set_users_value( $user, [ $user->id() ], '28' );

my $candidate = Stream2::Results::Result->new({ candidate_id => 1, destination => "external_board" });

my $s2_mock = Test::MockModule->new('Stream2');
$s2_mock->mock( find_results => sub { return MyResults->new({ result => $candidate }); } );
$s2_mock->mock( increment_analytics => sub { 1; } );
Test::MockModule->new('Log::Log4perl::MDC')->mock( put => sub { 1; } );

$t->app->stream2->stream_schema->resultset('CandidateInternalMapping')->create({
    import_id => 1,
    group_identity => $user->group_identity,
    candidate_id => "external_board_from_internal_not_complete",
    backend => "Artirix"
});
$t->app->stream2->stream_schema->resultset('CandidateInternalMapping')->create({
    import_id => 2,
    group_identity => $user->group_identity,
    candidate_id => "external_board_from_internal",
    backend => "Artirix",
    internal_id => 15,
    completed => 1
});
$t->app->stream2->stream_schema->resultset('CandidateInternalMapping')->create({
    import_id => 3,
    group_identity => $user->group_identity,
    candidate_id => "external_board_too_old",
    backend => "Artirix",
    internal_id => 20,
    completed => 1,
    insert_datetime => "2013-01-01 00:00:00"
});


my $feed = MyFeed->new( {} );
$feed->{user_object} = $user;
$s2_mock->mock( build_template => sub {
    return $feed;
});

foreach my $test (
    {
        cv_content => 'OK',
        candidate_id => 1,
        destination => "external_board",
    },
    {
        cv_content => 'FROM INTERNAL',
        candidate_id => 'from_internal',
        new_id => 15,
        new_destination => "talentsearch",
        destination => "external_board"
    },
    {
        cv_content => 'NOT COMPLETE',
        candidate_id => 'from_internal_not_complete',
        destination => "external_board"
    },
    {
        cv_content => 'TOO OLD',
        candidate_id => 'too_old',
        destination => 'external_board'
    }
){
    my $mock_client = Test::MockModule->new('Bean::TSImport::Client');
    $mock_client->mock(import_status => sub{
                           return {}; # Its always a failure.
                       });

    $candidate->{destination} = $test->{destination};
    $candidate->{candidate_id} = $test->{candidate_id};
    $feed->{_content} = $test->{cv_content};

    my $job = MyJob->new({
        class => 'Stream2::Action::DownloadCV',
        parameters => {
            user => {
                user_id => $user->id,
                group_identity => $user->group_identity
            },
            ripple_settings => {},
            results_id => 'sausage',
            candidate_idx => 1
        }
    });
    $job->{stream2} = $t->app->stream2;

    ok( Stream2::Action::DownloadCV->perform( $job ), "job is completed" );

    my $new_id = $test->{new_id} ? $test->{new_id} : $test->{candidate_id};
    my $new_destination = $test->{new_destination} ? $test->{new_destination} : $test->{destination};
    is ( $candidate->{candidate_id}, $new_id, "candidate ID post download is correct" );
    is ( $candidate->{destination}, $new_destination, "candidate destination post download is correct" );

    my $raw_result = MIME::Base64::decode_base64( $job->result->{result} );
    my $resp = HTTP::Response->parse( $raw_result );
    is( $resp->decoded_content, $test->{cv_content}, "the correct content has been returned" );
}

done_testing();

{
    package MyJob;
    use base qw/Qurious::Job/;
    sub save { 1; }
}
{
    package MyResults;
    sub new { bless $_[1], $_[0]; }
    sub find { return shift->{result} }
}
{
    package MyFeed;
    use base qw/Stream::Engine/;
    use HTTP::Response;
    sub download_cv {
        my $self = shift;
        my $resp = HTTP::Response->new();
        $resp->code(200);
        $resp->content( $self->{_content} );
        return $resp;
    }
}

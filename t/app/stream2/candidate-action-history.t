
use Test::Most;
use Test::Mojo::Stream2;
use JSON;
use DateTime;

use_ok('Stream2::CandidateActionLog');

# TEST SETUP
my $t = Test::Mojo::Stream2->new();
$t->login_ok();

my $group_id = "andy"; # hard coded in App::Stream2::Plugin::Auth::Provider::Demo
my $user_id = 1; # first and only user on test schema

my $mocked_candidate = Stream2::Results::Result->new({
                                                      destination     => "dummy",
                                                      candidate_id    => 1
                                                     });

my @fixture_actions = qw/message
                         profile
                         download
                         note
                         shortlist_bullhorn_tearsheets
                         shortlist_bullhorn_jobs
                         shortlist_adc_shortlist
                         candidate_tag_add
                         import
                         update
                         note_delete
                         candidate_tag_delete/;

my $mocked_action_log = Stream2::CandidateActionLog->new(schema => $t->app->stream2->stream_schema);

my $test_data = [];

foreach my $action (@fixture_actions) {
    my $insert_datetime = DateTime->now();

    my $action_log_fixture = {
        action          => $action,
        candidate_id    => $mocked_candidate->id,
        destination     => $mocked_candidate->destination,
        user_id         => $user_id,
        group_identity  => $group_id,
        insert_datetime => $insert_datetime,
        data            => undef,
    };
    my $action_object = $mocked_action_log->insert($action_log_fixture);
    unshift(@$test_data, {
        id => $action_object->id(),
        action   => $action,
        data     => undef,
        datetime => $insert_datetime->iso8601().'Z',
        user     => App::Stream2::Plugin::Auth::Provider::Demo->demo_user->{contact_details}->{contact_name},
    });
}

$t->get_ok(
    sprintf('/results/1/%s/candidate/0/history', $mocked_candidate->destination)
)->status_is(200);

my $response_ref = JSON::decode_json( $t->tx->res->content()->get_body_chunk(0) );
is_deeply($response_ref->{history}, $test_data,  "Got correct action log data back in correct order");

done_testing();

{
    package MockedResults;
    use Moose;
    sub find{
        return $mocked_candidate;
    }
}
1;

## Just mock find_results in the Stream2 class
{
    no warnings 'redefine';
    package Stream2;
    sub find_results{ return MockedResults->new(); }
    1;
    use warnings;

}

use Test::Most;
use Test::Mojo::Stream2;

my $t = Test::Mojo::Stream2->new();

for my $method (qw(__ __x __n __nx __xn __p __px __np __npx)) {

    is( $t->app->$method(''), '', "translation: empty string should be empty with $method and not the PO header");

}

done_testing();

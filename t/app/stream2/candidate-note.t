use Test::Most;
use Test::Mojo::Stream2;
use JSON;

use_ok('Stream2::CandidateActionLog');

my $mocked_candidate = Stream2::Results::Result->new({
                                                      destination     => "adcresponses",
                                                      candidate_id    => 1
                                                     });

my $group_id = "andy"; # hard coded in App::Stream2::Plugin::Auth::Provider::Demo
my $user_id = 1; # first and only user on test schema

my $wrong_user_id = 2;
my $wrong_group_id = "pat";

# TEST SETUP
my $t = Test::Mojo::Stream2->new();
$t->login_ok();

$t->post_ok('/results/1/dummy/candidate/0/note', form => { q => JSON::encode_json({ content => "private", privacy => "group" }) })
    ->status_is(200)
    ->json_has('/id');

my $response_ref = JSON::decode_json( $t->tx->res->content()->get_body_chunk(0) );
my $first_note_id = $response_ref->{id};


$t->post_ok('/results/1/dummy/candidate/0/note', form => { q => JSON::encode_json({ content => "public", privacy => "group" }) })
    ->status_is(200)
    ->json_has('/id');

my $cal = Stream2::CandidateActionLog->new(
    {
        schema => $t->app->stream2->stream_schema
    }
);

my $notes_ref = $cal->get_notes( $user_id, $group_id, [$mocked_candidate->id] );
is ( scalar( @{$notes_ref->{$mocked_candidate->id}} ), 2 );

# Same number of notes (no privacy anymore).
$notes_ref = $cal->get_notes( $wrong_user_id, $group_id, [$mocked_candidate->id] );
is ( scalar( @{$notes_ref->{$mocked_candidate->id}} ), 2 );

$notes_ref = $cal->get_notes( $wrong_user_id, $wrong_group_id, [$mocked_candidate->id] );
is_deeply( $notes_ref, {} );

# delete the first note
$t->delete_ok(qq{/results/1/dummy/candidate/0/note/$first_note_id})->status_is(200);

$notes_ref = $cal->get_notes( $user_id, $group_id, [$mocked_candidate->id] );
is ( scalar( @{$notes_ref->{$mocked_candidate->id}} ), 1 );

$notes_ref = $cal->get_notes( $wrong_user_id, $group_id, [$mocked_candidate->id] );
is ( scalar( @{$notes_ref->{$mocked_candidate->id}} ), 1 );

done_testing();

{
    package MockedResults;
    use Moose;
    sub find{
        return $mocked_candidate;
    }
}
1;

## Just mock find_results in the Stream2 class
{
    no warnings 'redefine';
    package Stream2;
    sub find_results{ return MockedResults->new(); }
    1;
    use warnings;
}

#! perl -w

use Test::Most;
use Test::Mojo::Stream2;
use Mojo::IOLoop;

my $timeout_time = 1;

my $t = Test::Mojo::Stream2->new();

$t->app->routes()->get('/timeouttest')->to(
    controller => "App::Stream2::Controller",
    cb         => sub {
        my $self = shift;
        $self->render_later();
        $self->extend_timeout( $timeout_time,
            sub { $self->render( text => "this was a timeout" ); } );
    }
);

$t->get_ok('/timeouttest')->content_is("this was a timeout")->status_is(200);

done_testing();


#
#  extend_timeout is a nice way of telling the client that the request is taking too long.
#

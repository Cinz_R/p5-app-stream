#! perl
use strict;
use warnings;

use utf8;

use Test::More;
use Test::Mojo::Stream2;
use Mojo::URL;
use Test::MockModule;

use Bean::Juice::APIClient::Result::Location;

use JSON;

use Encode;

my $t = Test::Mojo::Stream2->new();
$t->ua->on(start => sub {
    my ($ua, $tx) = @_;
    $tx->req->headers->accept('text/xml');
});

my $oauth_mock = Test::MockModule->new('Bean::OAuth::AsyncClient');
$oauth_mock->mock( credentials_grant => sub {
    my ($self, %params) = @_;
    my $d = Promises::deferred;
    $d->reject({
        error => 'bad'
    });
    return $d->promise;
});

my $loc_mock = Test::MockModule->new('Bean::Juice::APIClient::Location');
$loc_mock->mock( find_by_postcode => sub {
    my $self = shift;
    return Bean::Juice::APIClient::Result::Location->new({
        api_client => $self,
        id => 39557,
        name => 'London',
        type => 'place',
        specific_path => 'South West London, England'
    });
});

$loc_mock->mock( find_by_lat_long => sub {
    my $self = shift;
    return Bean::Juice::APIClient::Result::Location->new({
        api_client => $self,
        id => 39557,
        name => 'London',
        type => 'place',
        specific_path => 'South West London, England'
    });
});


my $user_mock = Test::MockModule->new('Bean::Juice::APIClient::User');
$user_mock->mock( provider_settings => sub {
    return { provider_name => 'bond', webhooks => [ { config => {tagdoc_type => 'hr_xml'}, url => 'http://www.adcourier.com/dump_stuff.cgi' } ] };
});
$user_mock->mock( siblings => sub { return []; });


# Create a legacy adcourier_ats user that will have the same
# provider_id. This is to check later on
# if its data has been transfered to the logged in ripple user
# with the same provider id
#
# Note that after the successful login, this user shouldnt be there anymore.
my $legacy_user = $t->app->stream2()->factory('SearchUser')->new_result({
    provider => 'adcourier_ats',
    last_login_provider => 'adcourier_ats',
    provider_id => 209669,
    group_identity => 'andy',
});
my $legacy_user_id = $legacy_user->save();
{
    # Add a tiny bit of data to this legacy user,
    # so we can check its transfered later on
    $legacy_user->longlists()->create({ user_id => $legacy_user->id ,  name => 'MyLegacyLongList'});
}

# Not XML at all query
{
    $t->post_ok('/ripple' ,  'blabla')
      ->status_isnt(404)
        ->status_isnt(500)
          ->status_is(400);
}

# Malformed XML
{
    $t->post_ok('/ripple' , '<some>thing</som>')
      ->status_is(400);
}

# Invalid XML
{
    $t->post_ok('/ripple', '<some>thing</some>')
      ->status_is(400);
}

my $expected_custom_fields = [
                              { name => 'plainNumOne', 'content' => 1 , asHttpHeader => 1},
                              { name => 'customString', 'content' => 'boudin blanc' , asHttpHeader => 0 },
                              { name => 'utf8String', content => 'нолюёжжэ' , asHttpHeader => 0 },
                              { name => 'latin1String', content => 'Jérôme Étévé' , asHttpHeader => 1 }
                             ];

my $custom_fields_str = join("\n", map{ '<CustomField '.( $_->{asHttpHeader} ? 'asHttpHeader="true" ' : '' ).' name="'.$_->{name}.'">'.$_->{content}.'</CustomField>' } @{$expected_custom_fields} );

my $api_key = 4015265503;

# Some valid but semantically incorrect XML
{
    my $response = $t->post_ok('/ripple',Encode::encode_utf8(q|<?xml version="1.0" encoding="UTF-8" ?>
<Search>
  <Version>2.0</Version>
  <APIKey>|.$api_key.q|</APIKey>
  <Account>
    <UserName>pat@pat.pat.pat</UserName>
    <Password>mysearchpassword</Password>
  </Account>
  <Query>
    <Keywords>Project Manager</Keywords>
    <Location>
      <Postcode>SW12 2AW</Postcode>
      <Country>UK</Country>
    </Location>
   <OfccpContext>Pinguin Grooming Specialist</OfccpContext>
  </Query>
  <CustomFields>|.$custom_fields_str.q|
  </CustomFields>
</Search>|));

    $response->status_is(400);
}

# Now some valid and semantically correct XML.
{

    my $response = $t->post_ok('/ripple',Encode::encode_utf8(q|<?xml version="1.0" encoding="UTF-8" ?>
<Search>
  <Version>3.0</Version>
  <APIKey>|.$api_key.q|</APIKey>
  <Account>
    <UserName>pat@pat.pat.pat</UserName>
    <Password>mysearchpassword</Password>
  </Account>
  <Query>
    <Keywords>Project Manager</Keywords>
    <Location>
      <Postcode>SW12 2AW</Postcode>
      <Country>UK</Country>
    </Location>
   <OfccpContext>Pinguin Grooming Specialist</OfccpContext>
  </Query>
  <CustomFields>|.$custom_fields_str.q|
  </CustomFields>
</Search>|));

    $response->status_is(401);
    $response->content_like(qr/code="401"/, 'Got code="401" in response' );
}

$oauth_mock->mock( credentials_grant => sub {
    my ($self, %params) = @_;
    my $d = Promises::deferred;
    $d->resolve({
        'expires_in' => 43200,
        'user' => {
            'user_role' => 'USER',
            'user_id' => '209669',
            'read_contact_details' => {
                'contact_telephone' => '1234567894',
                'contact_email' => 'jerome@broadbean.com',
                'contact_name' => 'Search Demo',
                'contact_devuser' => undef,
                'company' => 'andy'
            },
            'read_navigation_tabs' => [],
            'read_stream_subscriptions' =>
                {
                    'talentsearch' => {
                        'nice_name' => 'Talent Search',
                        'auth_tokens' => {
                            'talentsearch_security_key' => 'a9ad6247-8d14-4df9-8364-3de9ff613c5a'
                        },
                        'type' => 'internal',
                        'user_role' => 'USER',
                        'user_id' => '209669'
                    },
                }
        },
        'access_token' => '26E42650-3356-11E4-8CB0-F98427886EDC',
        'token_type' => 'Bearer'
    });
    return $d->promise;
});

# Now some valid, semantically and credential correct XML
my $redirect_url;
{
    my $response = $t->post_ok('/ripple',Encode::encode_utf8(q|<?xml version="1.0" encoding="UTF-8" ?>
<Search>
  <Version>3.0</Version>
  <APIKey>|.$api_key.q|</APIKey>
  <Account>
    <UserName>searchdemo@users.live.andy</UserName>
    <Password>stream4life</Password>
  </Account>
  <Query>
    <Keywords>Project Manager</Keywords>
    <Location>
      <Postcode>SW12 2AW</Postcode>
      <Country>UK</Country>
    </Location>
    <Radius>314</Radius>
    <ContractType>permanent</ContractType>
    <SalaryCur>EUR</SalaryCur>
    <SalaryMin>10</SalaryMin>
    <SalaryMax>12345456</SalaryMax>
    <SalaryRate>month</SalaryRate>
    <IncludeUnspecifiedSalaries>1</IncludeUnspecifiedSalaries>
    <UpdatedWithin>3M</UpdatedWithin>
    <OfccpContext>Pinguin Grooming Specialist</OfccpContext>
  </Query>
  <Settings>
     <StyleSheet>https://my.shiny.example.com/stylesheet.css</StyleSheet>
     <Shortlist name="cheese_sandwiches">
       <Label>Cheese Sandwich</Label>
       <URL>_this_is_dummy_</URL>
     </Shortlist>
     <Shortlist name="beers">
       <Label>Beer</Label>
       <URL>_this_is_dummy_</URL>
     </Shortlist>
     <ImportCandidateURL>_this_is_dummy_</ImportCandidateURL>
  </Settings>
  <CustomFields>|.$custom_fields_str.q|
  </CustomFields>
</Search>|));

    $response->status_is(200);
    $response->content_like(qr/code="200"/, 'Got code="200" in response' );
    # Capture the redirect URL.
    # <URL>http://localhost:52572/ripple/enter/661FD40A-D9C3-11E3-B0A5-1D73CAE4B59E</URL>
    ($redirect_url) =  ( $response->tx()->res()->content()->get_body_chunk(0) =~ /<URL>(.+)<\/URL>/ );
    ok( $redirect_url , "Ok got a URL");
    # diag("GOT URL: $redirect_url");
}

# V2 request - straight from the ripple v2 docs
my $LAST_SESSION;
{
    my $response = $t->post_ok('/ripple',Encode::encode_utf8(q|<?xml version="1.0" encoding="UTF-8" ?>
<CV_Search_Query>
  <Version>1.0.0-r118179</Version>
  <APIKey>|.$api_key.q|</APIKey>
  <Account>
    <UserName>searchdemo@users.live.andy</UserName>
    <Password>stream4life</Password>
  </Account>

  <Config>
     <StyleSheet>https://my.shiny.example.com/stylesheet.css</StyleSheet>
     <Shortlist name="cheese_sandwiches" allowPreview="1">
       <Label>Cheese Sandwich</Label>
       <URL>_this_is_dummy_</URL>
     </Shortlist>
     <Shortlist name="beers">
       <Label>Beer</Label>
       <URL>_this_is_dummy_</URL>
     </Shortlist>
     <ImportCandidateURL>_this_is_dummy_</ImportCandidateURL>
  </Config>

  <Stream_Query>
    <Keywords>developer</Keywords>
    <OfccpContext>Pinguin Grooming Specialist</OfccpContext>
  </Stream_Query>
  <ChannelList>
    <Channel>
      <ChannelId>talentsearch</ChannelId>
    </Channel>
    <Channel>
      <ChannelId>myChannel</ChannelId>
      <Extension name="myField">myValue</Extension>
    </Channel>
    <Channel>
      <ChannelId>myChannel</ChannelId>
      <Extension name="myChannel_myOtherField">myOtherValue</Extension>
    </Channel>
  </ChannelList>
  <CustomFields>
    | . $custom_fields_str . q|
  </CustomFields>

</CV_Search_Query>
|));

    $response->status_is(200);
    my ($url) =  ( $response->tx()->res()->content()->get_body_chunk(0) =~ /<StreamURL>(.+)<\/StreamURL>/ );
    ok( $url , "Ok got a URL");

    my $session         = $t->get_session_from_STOK( $url );
    $LAST_SESSION = $session;
    my $ripple_settings = $session->{ripple_settings};
    my ($id, $ripple_request);

    is $ripple_settings->{new_candidate_url},
        '_this_is_dummy_',
        'Ok got good new candidate URL';
    is $ripple_settings->{stylesheet_url},
        'https://my.shiny.example.com/stylesheet.css',
        'Ok got stylesheet setting';

    my $expected_webhooks = [{
                            'url' => 'http://www.adcourier.com/dump_stuff.cgi',
                            'config' => { 'tagdoc_type' => 'hr_xml' },
                             }];
    is_deeply( $ripple_settings->{webhooks}, $expected_webhooks, "Got correct webhook data");

    is_deeply($ripple_settings->{custom_lists},
              [
               {
                'name' => 'cheese_sandwiches',
                'label' => 'Cheese Sandwich',
                'options_url' => '_this_is_dummy_',
                'allowPreview' => 1,
               },
               {
                'name' => 'beers',
                'label' => 'Beer',
                'options_url' => '_this_is_dummy_',
                'allowPreview' => 0,
               }
              ], "Ok good custom shortlist buttons");

    ok( ! $t->app->stream2()->find_user( $legacy_user_id ),
        "legacy user does not exist");

    # extract "abc" from "search/abc?key=val"
    my ($criteria_id) = $url =~ m|search/(.+?)\?|;
    my $criteria = Stream2::Criteria->new(
        id     => $criteria_id,
        schema => $t->app->stream2->stream_schema()
    );
    $criteria->load();
    my $expected_criteria = {
        ofccp_context => [ 'Pinguin Grooming Specialist' ],
        include_unspecified_salaries => [ 1 ],
        keywords => [ 'developer' ],
        myChannel_myField => [ 'myValue' ],
        myChannel_myOtherField => [ 'myOtherValue' ],
    };
    is_deeply( { $criteria->tokens() }, $expected_criteria, "passing channel specific fields is possible" );
} # End of V2 request


# diag("GETTING ".$url->path().'?'.$url->query());

my $r = $t->get_ok($redirect_url);
# Redirected to something.
$t->status_is(302);
$t->header_like( Location => qr/\/search\/.+/  ); # Ok got redirected to search with some criteria part

# Get this location
my $search_url = Mojo::URL->new(scalar($t->tx->res->headers()->header('Location')));

$t->get_ok($search_url->path->to_string());

## Check we can call the route that gives the nipple settings.
$t->get_ok($redirect_url);
$t->get_ok('/api/user/custom_lists', "Custom shortlists GET");

# Check the criteria.
my $expected_criteria = {
    'ofccp_context' => [ 'Pinguin Grooming Specialist' ],
    'salary_from' => ['10'],
    'salary_to' => ['12345456'],
    'location' => ['South West London, England'],
    'location_id' => ['39557'],
    'keywords' => ['Project Manager'],
    'salary_per' => ['month'],
    'default_jobtype' => ['permanent'],
    'location_within' => [314],
    'salary_cur' => ['EUR'],
    'distance_unit' => ['miles'],
    'cv_updated_within' => ['3M'],
    'include_unspecified_salaries' => [1],
};
my ( $criteria_id ) = $redirect_url =~ m|search/(.+?)\?|;
# diag("Criteria ID :".$criteria_id);
ok( my $criteria = Stream2::Criteria->new(id => $criteria_id,
                                          schema => $t->app->stream2->stream_schema()
                                      ) ,
    "Ok can load criteria");
$criteria->load();
# use Data::Dumper;
# diag("TOKENS:" .Dumper({ $criteria->tokens() }));
is_deeply( { $criteria->tokens() } , $expected_criteria , "Ok good criteria back");

## Check the user
## We get it from the user table.
my $user_id = $t->app->stream2->factory('SearchUser')->search({ provider => 'adcourier' })->first()->id();
ok( my $user = $t->app->stream2->find_user($user_id, $LAST_SESSION->{ripple_settings} ) , "Ok found user");
is( $user->provider() , 'adcourier' );
is( $user->longlists()->count(), 1,  "Ok user already has a longlist");
is( $user->longlists()->first()->name() , 'MyLegacyLongList' );

# Inject the login_provider in the ripple settings
$user->ripple_settings()->{login_provider} = 'adcourier_ats';
isa_ok( $user->login_provider(), 'App::Stream2::Plugin::Auth::Provider::AdcourierAts' , "Ok got a login provider");
isa_ok( $user->identity_provider(), 'App::Stream2::Plugin::Auth::Provider::Adcourier' , "Ok got an identity provider");

{
    # Test the injection of the good custom fields in an XML document.
    my $xdoc =  XML::LibXML::Document->new( '1.0', 'UTF-8' );
    my $cfields = $user->ripple_xdoc_custom_fields( $xdoc );
    $xdoc->setDocumentElement( $cfields );
    #<?xml version="1.0" encoding="UTF-8"?>
    # <CustomFields>
    # <CustomField name="plainNumOne">1</CustomField><CustomField name="customString">boudin blanc</CustomField><CustomField name="utf8String">нолюёжжэ</CustomField>
    # <CustomField name="latin1String">Jérôme Étévé</CustomField>
    #</CustomFields>;
    my @fields = $xdoc->findnodes( '/CustomFields/CustomField' );
    is( scalar( @fields ) , 2 , "Ok only two custom fields (where asHttpHeader is false");

    my $headers = $user->ripple_header_custom_fields();
    is_deeply( $headers , {
        latin1String => "=?UTF-8?Q?J=C3=A9r=C3=B4me_=C3=89t=C3=A9v=C3=A9?=", # rfc2047 states spaces are to be _ https://www.ietf.org/rfc/rfc2047.txt
        plainNumOne  => 1
    }, "Ok good headers" );
}

{
    # Give a go at retrieving jobs.
    # with a stupid list name
    my $r = $t->get_ok('/api/cheesesandwiches/blablabla');
    $r->status_is(500);
}


my $CUSTOM_LISTS;

{
    ## get the list of lists.
    $t->get_ok('/api/user/custom_lists');
    $t->status_is(200);
    $CUSTOM_LISTS = decode_json($t->tx->res->content()->get_body_chunk(0));
}

# use Data::Dumper;
# diag(Dumper($CUSTOM_LISTS));

{
    ## We can retrieve jobs from real list names.
    foreach my $list ( @$CUSTOM_LISTS ){
        my $r = $t->get_ok('/api/cheesesandwiches/'.$list->{name});
        $r->status_is(200);
    }
}


# V2 request, without any config
# V2 request - straight from the ripple v2 docs
{
    my $response = $t->post_ok('/ripple',Encode::encode_utf8(q|<?xml version="1.0" encoding="UTF-8" ?>
<CV_Search_Query>
  <Version>1.0.0-r118179</Version>
  <APIKey>1097103005</APIKey>
  <Account>
    <UserName>searchdemo@users.live.andy</UserName>
    <Password>stream4life</Password>
  </Account>

  <Stream_Query>
    <Keywords>developer</Keywords>
  </Stream_Query>
  <ChannelList>
    <Channel>
      <ChannelId>talentsearch</ChannelId>
    </Channel>
  </ChannelList>
  <CustomFields>
    <CustomField name="custom1">field</CustomField>
    <CustomField name="custom2">field</CustomField>
  </CustomFields>


</CV_Search_Query>
|));

    $response->status_is(200);
    my ($url) =  ( $response->tx()->res()->content()->get_body_chunk(0) =~ /<StreamURL>(.+)<\/StreamURL>/ );
    ok( $url , "Ok got a redirect URL");

    my $session = $t->get_session_from_STOK( $url );
    my $custom_fields = $session->{ripple_settings}->{custom_fields};
    ok @{$custom_fields} == 2, 'custom fields present in old Ripple version';

    # Check we can hit the redirect URL
    {
        my $mojo_url = Mojo::URL->new($url);
        my $r = $t->get_ok($mojo_url->path()->to_string().'?'.$mojo_url->query());
        $t->status_is(302);
        $t->header_like( Location => qr/\/search\/.+/ ); # Ok got redirect with some criteria
        my $search_url =  Mojo::URL->new(scalar($t->tx->res->headers()->header('Location')));
        $t->get_ok($search_url->path->to_string());
        ## Check we can call the route that gives the nipple settings.
        $t->get_ok('/api/user/custom_lists', "Custom shortlists GET");
        $t->content_like(qr/Shortlist/, "Those custom list are shortlists"); 
    }
}

#done_testing();
#exit(0);


# V2 request, with an config that de-activate the adcourier shortlist
{
    my $response = $t->post_ok('/ripple',Encode::encode_utf8(q|<?xml version="1.0" encoding="UTF-8" ?>
<CV_Search_Query>
  <Version>1.0.0-r118179</Version>
  <APIKey>1097103005</APIKey>
  <Account>
    <UserName>searchdemo@users.live.andy</UserName>
    <Password>stream4life</Password>
  </Account>

  <Config>
   <HideShortlist>true</HideShortlist>
  </Config>

  <Stream_Query>
    <Keywords>developer</Keywords>
  </Stream_Query>
  <ChannelList>
    <Channel>
      <ChannelId>talentsearch</ChannelId>
    </Channel>
  </ChannelList>
  <CustomFields>
    <CustomField name="custom1">field</CustomField>
    <CustomField name="custom2">field</CustomField>
  </CustomFields>


</CV_Search_Query>
|));

    $response->status_is(200);
    my ($url) =  ( $response->tx()->res()->content()->get_body_chunk(0) =~ /<StreamURL>(.+)<\/StreamURL>/ );
    ok( $url , "Ok got a redirect URL");


    my $session = $t->get_session_from_STOK( $url );
    ok $session->{ripple_settings}, 'got some Ripple settings';
    ok $session->{ripple_settings}->{no_adcourier_shortlist},
        'No adcourier shortlisting option detected';

    my $custom_fields = $session->{ripple_settings}->{custom_fields};
    ok @{$custom_fields} == 2, 'custom fields present in old Ripple version';
    is_deeply( $custom_fields,
               [
                   {
                       name => 'custom1', content => 'field', asHttpHeader => 0
                   },
                   {
                       name => 'custom2', content => 'field', asHttpHeader => 0
                   }
            ] , "Ok custom fields are stored and retrieved correctly");

    # Check we can hit the redirect URL
    {
        my $mojo_url = Mojo::URL->new($url);
        my $r = $t->get_ok($mojo_url->path()->to_string().'?'.$mojo_url->query());
        $t->status_is(302);
        $t->header_like( Location => qr/\/search\/.+/ ); # Ok got redirect with some criteria
        my $search_url =  Mojo::URL->new(scalar($t->tx->res->headers()->header('Location')));
        $t->get_ok($search_url->path->to_string());
        ## Check we can call the route that gives the nipple settings.
        $t->get_ok('/api/user/custom_lists', "Custom shortlists GET");
        $t->content_unlike(qr/Shortlist/, "Those custom list are shortlists"); 
    }
}



# V2 request, with signature instead of password
{

    my $response = $t->post_ok('/ripple',Encode::encode_utf8(q|<?xml version="1.0" encoding="UTF-8" ?>
<CV_Search_Query>
  <Version>1.0.0-r118179</Version>
  <APIKey>1097103005</APIKey>
  <Account>
    <UserName>searchdemo@users.live.andy</UserName>
    <Signature>
       <Hash>boudinblanc</Hash>
       <Time>Whatever</Time>
    </Signature>
  </Account>

  <Stream_Query>
    <Keywords>developer</Keywords>
  </Stream_Query>
  <ChannelList>
    <Channel>
      <ChannelId>talentsearch</ChannelId>
    </Channel>
  </ChannelList>
  <CustomFields>
    <CustomField name="custom1">field</CustomField>
    <CustomField name="custom2">field</CustomField>
  </CustomFields>


</CV_Search_Query>
|));

    $response->status_is(200);
    my ($url) =  ( $response->tx()->res()->content()->get_body_chunk(0) =~ /<StreamURL>(.+)<\/StreamURL>/ );
    ok( $url , "Ok got a redirect URL");

    my $session = $t->get_session_from_STOK( $url );
    ok @{$session->{ripple_settings}->{custom_fields}} == 2, 'custom fields present in old Ripple version';

    # Check we can hit the redirect URL
    {
        my $mojo_url = Mojo::URL->new($url);
        my $r = $t->get_ok($mojo_url->path()->to_string().'?'.$mojo_url->query());
        $t->status_is(302);
        $t->header_like( Location => qr/\/search\/.+/ ); # Ok got redirect with some criteria
        my $search_url =  Mojo::URL->new(scalar($t->tx->res->headers()->header('Location')));
        $t->get_ok($search_url->path->to_string());
        ## Check we can call the route that gives the nipple settings.
        $t->get_ok('/api/user/custom_lists', "Custom shortlists GET");
        $t->content_like(qr/Shortlist/, "Those custom list are shortlists");
    }
}

done_testing();

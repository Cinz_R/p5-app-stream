#! perl -w

use strict;
use warnings;

use Test::Most;
use Test::Mojo::Stream2;

my $t = Test::Mojo::Stream2->new();

$t->login_ok();

$t->get_ok('/logout')
    ->content_like(qr/You have logged out/);

# When not logged in, just redirects to our own adcourier users oauth.
$t->ua->max_redirects(0);
$t->get_ok('/search')->status_is( 302 )->header_like( Location => qr/oauth\.adcourier\.com/ );

done_testing();

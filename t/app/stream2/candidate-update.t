use Test::Most;
use Test::Mojo::Stream2;
use Qurious;
use JSON;
use Promises qw/deferred/;

# use Log::Log4perl qw/:easy/;
# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log4perl');
# # Log::Any::Adapter->set('Stderr');

use_ok( 'Stream2::Action::UpdateCandidate' );

my $mocked_candidate = Stream2::Results::Result->new({
                                                      destination     => "dummy",
                                                      candidate_id    => 1
                                                     });

# TEST SETUP
my $t = Test::Mojo::Stream2->new();

$t->app->helper('queue_job' => sub {
    my $job = test_job();
    return $job;
});

$t->login_ok();


# TEST 2 - 3
# status is 200 only when the CV is being served.
$t->post_ok('/results/1/dummy/candidate/0/property/name', { q => JSON::encode_json({ value => "david" }) })
    ->status_is(200);

# TEST 4
# cv is downloaded from board and is correct

# TEST 5 - 8
# cv download is remembered and state is correct
my $candidate = $mocked_candidate;

done_testing();

sub test_job {
    my $app = shift;

    my $job = TestQuriousJob->new({
        class => "TestJob",
        parameters => {
            results_id => 1,
            candidate_idx => 0,
            fields => {},
            user => {
                user_id => 1,
                group_identity => "andy",
                auth_tokens => {}
            },
            ripple_settings => {},
        }
    });

    # A stream_schema holding Application mockery (See bottom of this file)
    $job->{stream2} = $t->app->stream2();
    return $job;
}
{
    package TestQuriousJob;
    use base "Qurious::Job";
    sub save {}
    sub result {
        return { result => 1 };
    }
}

{
    package TestJob;
    use base "Stream2::Action::UpdateCandidate";

    sub get_credentials {}
}

{
    package MockedResults;
    use Moose;
    sub find{
        return $mocked_candidate;
    }
}
1;

## Just mock find_results in the Stream2 class
{
    no warnings 'redefine';
    package Stream2;
    sub find_results{ return MockedResults->new(); }
    1;
    use warnings;
}

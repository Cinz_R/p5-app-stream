use Test::Most;
use Test::Mojo::Stream2;

my $t = Test::Mojo::Stream2->new();

isnt $t->app->sessions->cookie_name(), 'mojolicious', "Using a custom session";
ok( ! $t->app->sessions->default_expiration(), "The session expires with the browser" );

done_testing();

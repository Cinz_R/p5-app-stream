#! perl -w

use utf8;
use Test::Most;

use Test::Mojo::Stream2;

$ENV{MOJO_MODE} = 'production';

my $t = Test::Mojo::Stream2->new();
$t->app->routes()->get('/_errortest' => sub { die "sausage €\n"; } );
$t->app->routes()->post('/_errortest' => sub { return shift->render( exception => "sausage €", status => 400 ); } );


subtest 'text/html' => sub {
    $t->post_ok( '/_errortest' )
        ->status_is( 400 )
        ->header_is( 'Content-Type' => 'text/html;charset=UTF-8' )
        ->content_like( qr/sausage/ );
    $t->get_ok( '/_errortest' )
        ->status_is( 500 )
        ->header_is( 'Content-Type' => 'text/html;charset=UTF-8' )
        ->content_like( qr/I'm sorry.+?sausage €/s );
};

subtest 'application/json' => sub {
    $t->post_ok( '/_errortest' => { Accept => 'application/json' } )
        ->status_is( 400 )
        ->header_is( 'Content-Type' => 'application/json;charset=UTF-8' )
        ->json_is( '/error/message' => 'sausage €' );
    $t->get_ok( '/_errortest' => { Accept => 'application/json' } )
        ->status_is( 500 )
        ->header_is( 'Content-Type' => 'application/json;charset=UTF-8' )
        ->json_is( '/error/message' => "sausage €" );
};

subtest 'text/xml' => sub {
    $t->post_ok( '/_errortest' => { Accept => 'text/xml' } )
        ->status_is( 400 )
        ->header_is( 'Content-Type' => 'text/xml;charset=UTF-8' )
        ->content_like( qr/<Status code="400">sausage €<\/Status/ );
};


done_testing();

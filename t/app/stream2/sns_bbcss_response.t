#! perl -w
use strict;
use warnings;


use Test::More;
use Test::MockModule;
use Test::Mojo::Stream2;

use Mojo::URL;

# use Log::Any qw/Stderr/;

my $lwp_mock = Test::MockModule->new( 'LWP::UserAgent' );

my $t = Test::Mojo::Stream2->new();
$t->ua->on(start => sub {
    my ($ua, $tx) = @_;
    $tx->req->headers->accept('text/json');
});
my $stream2 = $t->app()->stream2();

## First a subscription message, with its mocked callback response
{
    $lwp_mock->mock( request => sub {
                         my $response = HTTP::Response->new( '200', 'OK' );
                         $response->header( 'Content-Type' => 'text/xml' );
                         $response->content( q|<ConfirmSubscriptionResponse xmlns="http://sns.amazonaws.com/doc/2010-03-31/">
  <ConfirmSubscriptionResult>
    <SubscriptionArn>arn:aws:sns:us-west-2:123456789012:MyTopic:2bcfbf39-05c3-41de-beaa-fcfcc21c8f55</SubscriptionArn>
  </ConfirmSubscriptionResult>
  <ResponseMetadata>
    <RequestId>075ecce8-8dac-11e1-bf80-f781d96e9307</RequestId>
  </ResponseMetadata>
  </ConfirmSubscriptionResponse>| );
                         return $response;
                     });


    $t->post_ok('/external/sns_bbcss_responses',{
        Accept => 'text/json',
        'x-amz-sns-message-type' => 'SubscriptionConfirmation'
    }, q|
{
  "Type" : "SubscriptionConfirmation",
  "MessageId" : "165545c9-2a5c-472c-8df2-7ff2be2b3b1b",
  "Token" : "2336412f37fb687f5d51e6e241d09c805a5a57b30d712f794cc5f6a988666d92768dd60a747ba6f3beb71854e285d6ad02428b09ceece29417f1f02d609c582afbacc99c583a916b9981dd2728f4ae6fdb82efd087cc3b7849e05798d2d2785c03b0879594eeac82c01f235d0e717736",
  "TopicArn" : "arn:aws:sns:us-west-2:123456789012:MyTopic",
  "Message" : "You have chosen to subscribe to the topic arn:aws:sns:us-west-2:123456789012:MyTopic.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
  "SubscribeURL" : "http://www.example.com",
  "Timestamp" : "2012-04-26T20:45:04.751Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLEpH+DcEwjAPg8O9mY8dReBSwksfg2S7WKQcikcNKWLQjwu6A4VbeS0QHVCkhRS7fUQvi2egU3N858fiTDN6bkkOxYDVrY0Ad8L10Hs3zH81mtnPk5uvvolIC1CXGu43obcgFxeL3khZl8IKvO61GWB6jI9b5+gLPoBc1Q=",
  "SigningCertURL" : "https://sns.us-west-2.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem"
  }
|)->status_is(200);

}
## Lets move to notifications

# A notification, with an example message of a delivery
{
    my $saved_job = 0;
    my $mockrious = Test::MockModule->new('Qurious::Job');
    $mockrious->mock( enqueue => sub{
                          my ($job) = @_;
                          use Data::Dumper;
                          $saved_job = 1;
                      });
    my $notification = {
        api_url => 'http://www.ratemycandidate.com',
        document_id => 1234321,
        customer_key => 'some_totally_random_key',
        broadbean_adcuser_id => 54321,
    };
    my $encoded_message = $stream2->pubsub_client()->serialize({ data => $notification } );
    $t->post_ok( '/external/sns_bbcss_responses' => { 'x-amz-sns-message-type' => 'Notification' },
                 json => {
                     TopicArn => 'arn:aws:sns:eu-west-1:868002146347:Broadbean_BBCSS_Response',
                     Message => $encoded_message,
                 } )->status_is( 200 );
    ok( $saved_job );
}

done_testing();

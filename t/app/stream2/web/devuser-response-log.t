#!/usr/bin/env perl
use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;
use AnyEvent::Promises;
use URI;

my $t = Test::Mojo::Stream2->new();

# lets create a mock dev user
my $demo_mock = Test::MockModule->new('App::Stream2::Plugin::Auth::Provider::Demo');
$demo_mock->mock( demo_user => sub {
    return {
        provider        => {
            user_id         => 'andy',
            login_provider  => 'demo',
            identity_provider => 'demo',
            group_identity  => 'pat',
        },
        contact_details => {contact_devuser => 'andy'},
        subscriptions   => {},
        ripple_settings => {}
    };
});

my $did_call_juice = 0;
my $juice_mock = Test::MockModule->new('Bean::Juice::APIClient::Candidate');
$juice_mock->mock( request_json_async => sub {
    $did_call_juice = 1;
    my ( $self, $method, $url ) = @_;

    my $uri = URI->new( $url );
    is ( $uri->path, '/candidate/response' );
    is_deeply( {$uri->query_form}, { company => 'pat', response_id => 123 } );

    return AnyEvent::Promises::make_promise({ response => { logfile => 'sausage' }});
});

$t->login_ok;
$t->ua->max_redirects(0);
$t->get_ok('/devuser/view_response_log/123')
    ->status_is( 302 )
    ->header_is( location => 'https://www.adcourier.com/manage/queues/search_by_board/extract-log.cgi?log=sausage' );

done_testing();

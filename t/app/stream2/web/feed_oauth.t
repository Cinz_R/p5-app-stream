use Test::Most;

use Crypt::JWT ();
use Stream2;
use Test::MockModule;
use Test::Mojo::Stream2;

my $juice_mock_called = 0;
my $juiceapi_mock = Test::MockModule->new('Bean::Juice::APIClient::Stream');
$juiceapi_mock->mock(update_oauth_tokens => sub {
    $juice_mock_called = 1;
    return 1;
});

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

my $t = Test::Mojo::Stream2->new;

ok( my $stream2 = $t->app->stream2, "Ok can build stream2");
ok( my $users_factory = $stream2->factory('SearchUser'), 'can get user factory' );
ok( my $memory_user = $users_factory->create({
    provider            => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id         => '1234',
    group_identity      => 'acme',
    locale              => 'en',
}), 'made new user' );
$memory_user->save();

my $jwt = Crypt::JWT::encode_jwt(
    key => 'stream',
    alg => 'HS256',
    payload => {
        criteria_id => 'such_criteria',
        page => 1,
        provider_id => 1234
    }
);

$t->get_ok('/external/twitter/oauth/callback' => form => { jwt => $jwt } )->status_is(302);
like($t->tx->res->headers->location, qr{search/such_criteria/twitter/page/1}, 'redirection url points to the search url');
ok($juice_mock_called, 'updated oauth subscriptions in juice');

$juice_mock_called = 0;
$t->get_ok('/external/twitter/oauth/callback' => form => { jwt => 'nogood' } )->status_is(400);
like($t->tx->res->body, qr{search/\w+/twitter/page/1}, 'redirection url points to the search url on error with empty criteria');
ok(!$juice_mock_called, 'did not update oauth subscriptions in juice on failure');

done_testing();

#!/usr/bin/env perl
BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}
use Test::Most;
use Test::Mojo::Stream2;
use Email::Sender::Simple;

use JSON;
use_ok('App::Stream2::Web::Unsubscribe');
use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');

my $t = Test::Mojo::Stream2->new();
my $stream2 = $t->app->stream2;
$stream2->factory('Group')->create({ identity => "mygroup", nice_name => "My Group" });
$stream2->factory('Group')->create({ identity => "othergroup", nice_name => "" });

# somewhere to capture them pesky emails.
my @sent_emails;
# Time to create some users for those companies.
my $recruiter = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'demo',
    provider_id => 123,
    group_identity => 'mygroup',
    contact_email => 'test2@mygroup.com',
    contact_name  => 'Whoever'
});
$recruiter->save();


my $recruiter_other = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'demo',
    provider_id => 456,
    group_identity => 'othergroup',
    contact_email => 'test2@othergroup.com',
    contact_name  => 'Spoonie'
});
$recruiter_other->save();


subtest 'link validation' => sub {
    $t->get_ok( '/external/unsubscribe' => {} => form => { group_identity => 'test' } )
        ->status_is( 400 )
        ->content_like( qr/Invalid Unsubscribe URL/ );
    $t->get_ok( '/external/unsubscribe' => {} => form => { email => 'test' } )
        ->status_is( 400 )
        ->content_like( qr/Invalid Unsubscribe URL/ );


    $t->get_ok( '/external/unsubscribe' => {} => form => { email => 'test', group_identity => 'test' } )
        ->status_is( 400 )
        ->content_like( qr/Invalid Unsubscribe URL/ );


    {
        my $form =  { email => 'test', group_identity => 'mygroup' , rec => $recruiter->id() , purpose => 'whatever' };
        $form->{sign} = $t->app()->stream2()->sign_hash($form);

        $t->get_ok( '/external/unsubscribe' => {} => form => $form )
            ->status_is( 200 )
            ->content_like( qr/test<\/strong> from receiving any further job opportunities from <strong>My Group/ );
    }

    {
        my $form = { email => 'test', group_identity => 'othergroup' , purpose => 'whatever' , rec => $recruiter_other->id() };
        $form->{sign} = $t->app()->stream2()->sign_hash($form);
        $t->get_ok( '/external/unsubscribe' => {} => form => $form )
            ->status_is( 200 )
            ->content_like( qr|test</strong> from receiving any further job opportunities from <strong>othergroup| );
    }
};


subtest 'form validation' => sub {
    {
        my $form = { group_identity => 'test' , email => '' , rec => 123 , purpose => 'whatever' };
        $form->{sign} = $t->app()->stream2()->sign_hash($form);
        $t->post_ok( '/external/unsubscribe' => {} => form => $form )
            ->status_is( 400 )
            ->content_like( qr/Invalid Unsubscribe URL/ );
    }

    {
        my $form = { group_identity => '' , email => 'test' , rec => 123 , purpose => 'whatever' };
        $form->{sign} = $t->app()->stream2()->sign_hash($form);
        $t->post_ok( '/external/unsubscribe' => {} => form => $form )
            ->status_is( 400 )
            ->content_like( qr/Invalid Unsubscribe URL/ );
    }

    {
        my $form = { group_identity => 'test' , email => 'test' , rec => 123 , purpose => 'whatever' };
        $form->{sign} = $t->app()->stream2()->sign_hash($form);
        $t->post_ok( '/external/unsubscribe' => {} => form => $form )
            ->status_is( 400 )
            ->content_like( qr/Invalid Unsubscribe URL/ );
    }
};

subtest 'blacklist is created' => sub {
    {
        my $form = { email => 'test', group_identity => 'mygroup', purpose => 'candidate.message' , rec => $recruiter->id() };
        $form->{sign} = $t->app()->stream2()->sign_hash($form);

        $t->post_ok( '/external/unsubscribe' => {} => form => $form )
            ->status_is( 200 )
            ->content_like( qr/You have been successfully unsubscribed/ );
        my $bl1 = $t->app->stream2->factory('EmailBlacklist')->search()->first();
        is ( $bl1->email, 'test' );
        is ( $bl1->group_identity, 'mygroup' );
        is ( $bl1->comments, undef );
        is ( $bl1->purpose, 'candidate.message' );
        @sent_emails = Email::Sender::Simple->default_transport->deliveries;
        ok($sent_emails[-1]->{envelope}->{to}->[0],'test2@mygroup.net')
    }

    {
        my $form = { email => 'test2@mygroup.com', group_identity => 'mygroup',  purpose => 'made up purpose' , rec => $recruiter->id()  };
        $form->{sign} = $t->app()->stream2()->sign_hash($form);
        # Note that comments are free and dont count in the signature
        $form->{comments} = 'this is a test ' x 18;

        $t->post_ok( '/external/unsubscribe' => {} => form => $form )
            ->status_is( 200 )
            ->content_like( qr/You have been successfully unsubscribed/ );
        my @results = $t->app->stream2->factory('EmailBlacklist')->search()->all();
        my $bl2 = $results[-1];
        is ( $bl2->email, 'test2@mygroup.com' );
        is ( $bl2->group_identity, 'mygroup' );
        is ( $bl2->comments, 'this is a test ' x 17 );
        is ( $bl2->purpose, 'made up purpose' );
        @sent_emails = Email::Sender::Simple->default_transport->deliveries;
        ok($sent_emails[-1]->{envelope}->{to}->[0],'test2@mygroup.net')
    }
};

subtest 'uuids' => sub {

### create test data
my $emails_factory = $stream2->factory('SentEmail');
my $users_factory  = $stream2->factory('SearchUser');
my $uuid = $stream2->uuid()->create_str();
my $messages = [
    {
        user           => $recruiter,
        sent_date      => \'NOW()',
        subject        => 'Job Opportunity',
        recipient      => 'jsmith@example.com',
        recipient_name => 'John Smith',
        raw_uuid       => $uuid = $stream2->uuid()->create_str(),
        aws_s3_key     => "search/private_files/$uuid",
        plain_body     => 'it has been a lovely day',
    },
    {
        user           => $recruiter,
        sent_date      => \'NOW()',
        subject        => 'LovElY Job Opportunity',
        recipient      => 'pb123@example.com',
        recipient_name => 'Paula Bastable',
        aws_s3_key     => 'search/private_files/'. $stream2->uuid()->create_str(),
        raw_uuid       => 'I_SHOULD_FAIL',
        plain_body     => 'Hello. Please call if interested.',
    }
];

for my $message ( @{$messages} ) {

    my $sender = $message->{user};

    # each SentEmail must have a CandidateActionLog (FK constraint)
    my $action = $stream2->factory('CandidateActionLog')->create({
        action         => 'message',
        user_id        => $sender->id,
        candidate_id   => 1,
        destination    => "sausage",
        group_identity => $sender->group_identity
    });

    my $msg_result = $stream2->factory('SentEmail')->create({
        user_id        => $sender->id,
        action_log_id  => $action->id,
        sent_date      => $message->{sent_date},
        subject        => $message->{subject},
        recipient      => $message->{recipient},
        recipient_name => $message->{recipient_name},
        aws_s3_key     => $message->{aws_s3_key},
        plain_body     => $message->{plain_body},
    });
}

# these next two should pass, this test that the uuid can be passed
note 'should show unsubscribed page';
$t->get_ok( '/external/unsubscribe/'.$messages->[0]->{raw_uuid} => {} => form => { } )
        ->status_is( 200 )
        ->content_like( qr|jsmith\@example.com</strong> from receiving any further job opportunities from <strong>My Group| );

note 'should show has unsubscribed page';
$t->get_ok( '/external/unsubscribe/'.$messages->[0]->{raw_uuid} . '/auto' => {} => form => { } )
        ->status_is( 200 )
        ->content_like( qr/You have been successfully unsubs/ );

my @results = $t->app->stream2->factory('EmailBlacklist')->search()->all();
my $bl2 = $results[-1];
is ( $bl2->email, 'jsmith@example.com' );
is ( $bl2->group_identity, 'mygroup' );
is ( $bl2->comments, 'The candidate unsubscribed via their email provider.' );
is ( $bl2->purpose, 'candidate.message' );

$t->get_ok( '/external/unsubscribe/'.$messages->[1]->{raw_uuid} => {} => form => { } )
        ->status_is( 400 )
        ->content_like( qr/Sorry but this unsubscribe URL/ );

$t->get_ok( '/external/unsubscribe/'.$messages->[1]->{raw_uuid} . '/auto' => {} => form => { } )
        ->status_is( 400 )
        ->content_like( qr/Sorry but this unsubscribe URL/ );


@sent_emails = Email::Sender::Simple->default_transport->deliveries;
ok($sent_emails[-1]->{envelope}->{to}->[0],'test2@mygroup.net')
};


subtest 'client block check' => sub {

### create test data
my $emails_factory = $stream2->factory('SentEmail');
my $users_factory  = $stream2->factory('SearchUser');
my $uuid = $stream2->uuid()->create_str();
my $message = {
    user           => $recruiter,
    sent_date      => \'NOW()',
    subject        => 'Job Opportunity',
    recipient      => 'janesmith@example.com',
    recipient_name => 'Jane Smith',
    aws_s3_key     => "search/private_files/$uuid",
    raw_uuid       => $uuid,
    plain_body     => 'it has been a lovely day',
};

# each SentEmail must have a CandidateActionLog (FK constraint)
my $action = $stream2->factory('CandidateActionLog')->create({
    action         => 'message',
    user_id        => $recruiter->id,
    candidate_id   => 1,
    destination    => "sausage",
    group_identity => $recruiter->group_identity
});

my $msg_result = $stream2->factory('SentEmail')->create({
    user_id        => $recruiter->id,
    action_log_id  => $action->id,
    sent_date      => $message->{sent_date},
    subject        => $message->{subject},
    recipient      => $message->{recipient},
    recipient_name => $message->{recipient_name},
    aws_s3_key     => $message->{aws_s3_key},
    plain_body     => $message->{plain_body},
});

$stream2->factory('EmailBlacklist')->create({
    email          => $recruiter->contact_email,
    purpose        => 'delivery.blocked'
});

# the candidate can get to the unsubscribe page
$t->get_ok( '/external/unsubscribe/'.$message->{raw_uuid} => {} => form => { } )
        ->status_is( 200 )
        ->content_like( qr|janesmith\@example.com</strong> from receiving any further job opportunities from <strong>My Group| );


# the candidate should be able to unsubscribe even if the recruiter is on the blacklist
lives_ok sub {$t->get_ok( '/external/unsubscribe/'.$message->{raw_uuid} . '/auto' => {} => form => { } )
        ->status_is( 200 )
        ->content_like( qr/You have been successfully unsubs/ );
        }, "this should still pass after client is blacklisted";
ok(1);
};



done_testing();

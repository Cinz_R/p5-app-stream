#!/usr/bin/env perl
use Test::Most;
use Test::Mojo::Stream2;
use Test::MockModule;
# use Carp::Always;
my $send_mock = Test::MockModule->new( 'Email::Sender::Simple' );
$send_mock->mock( send => sub { 1; } );

my $t = Test::Mojo::Stream2->new();

my $message_id =  int( rand(10000) ) + 1 ;

# create two softbounceblock
for my $count ( 0..10 ) {

    $t->post_ok(
        '/external/mailjet_notification/transport', {},
        json => {
            "event" => "bounce",
            "time" => time,
            "MessageID" => $message_id++,
            "email" => sprintf("bounce%s\@mailjet.com",$count % 2),
            "mj_campaign_id" => 0,
            "mj_contact_id" => 0,
            "customcampaign" => "",
            "CustomID" => "helloworld",
            "Payload" => "",
            "error_related_to" => "recipient",
            "error" => "user unknown"
        }
    )->status_is( 200 );
}

# add some hard bounces
for my $count ( 0..1 ) {

    $t->post_ok(
        '/external/mailjet_notification/transport', {},
        json => {
            "event" => "bounce",
            "time" => time,
            "MessageID" => $message_id++,
            "email" => sprintf("bounce%s\@mailjet.com",$count % 2),
            "mj_campaign_id" => 0,
            "mj_contact_id" => 0,
            "customcampaign" => "",
            "CustomID" => "helloworld",
            "Payload" => "",
            "error_related_to" => "recipient",
            "error" => "user unknown",
            "blocked" => 1,
            "hard_bounce" => 1,
        }
    )->status_is( 200 );
}

# create a blocked
$t->post_ok(
    '/external/mailjet_notification/transport', {},
    json => {
        "event" => "blocked",
        "time" => time,
        "MessageID" => $message_id++,
        "email" => 'blocked@mailjet.com',
        "mj_campaign_id" => 0,
        "mj_contact_id" => 0,
        "customcampaign" => "",
        "CustomID" => "helloworld",
        "Payload" => "",
        "error_related_to" => "recipient",
        "error" => "user unknown"
    }
)->status_is( 200 );


my $email_blacklist = $t->app->stream2->factory('EmailBlacklist');
my $email_notifications = $t->app->stream2->factory('EmailNotification');


is($email_blacklist->search()->count,5,"there should be five entries in the blacklist");
is($email_notifications->search()->count,14,"there should be 14 entries in the notifications");

note('accessing the /devuser route without as non-dev should fail');
$t->get_ok(
    '/devuser/view_blacklisted_emails')->status_is( 403 )
        ->content_like( qr|Access Denied\.| );

# lets create a mock dev user
my $demo_mock = Test::MockModule->new('App::Stream2::Plugin::Auth::Provider::Demo');
$demo_mock->mock( demo_user => sub {

    return {
        provider        => {
            user_id         => 'andy',
            login_provider  => 'demo',
            identity_provider => 'demo',
            group_identity  => 'andys company',
        },
        contact_details => {contact_devuser => 'andy'},
        subscriptions   => {},
        ripple_settings => {}
    };
});

note('log in and try again');
$t->login_ok;
$t->get_ok(
    '/devuser/view_blacklisted_emails')->status_is( 200 )
        ->content_like( qr|Email Blacklist| );


note('try and delete blocked@mailjet.com, and there should be an error');
$t->get_ok(
    '/devuser/view_blacklisted_emails?delete_id=3')->status_is( 200 )
        ->content_like( qr|Soft bounce blocks are currently the only thing that can be removed here| );

is($email_blacklist->search()->count,5,"there should still be five entries in the blacklist");

# try a notification delete while trying to remove blocked@mailjet.com, this should fail
# and keep the notifications
$t->get_ok(
    '/devuser/view_blacklisted_emails?delete_id=3&delete_notifications=1')->status_is( 200 )
        ->content_like( qr|Soft bounce blocks are currently the only thing that can be removed here| );
is($email_notifications->search()->count,14,"there should still be 14 entries in the notifications");

note('delete bounce0@mailjet.com, notifications should remain');
$t->get_ok(
    '/devuser/view_blacklisted_emails?delete_id=1')->status_is( 200 )
        ->content_like( qr|bounce0\@mailjet.com has been removed from the blacklist.| );


note('retry an delete of the same id');
$t->get_ok(
    '/devuser/view_blacklisted_emails?delete_id=1')->status_is( 200 )
        ->content_like( qr|There is no record with this ID \(1\), it may have been already deleted| );


is($email_blacklist->search()->count,4,"there should now four entries in the blacklist");
is($email_notifications->search()->count,14,"there should still be 14 entries in the notifications");


note('delete bounce1@mailjet.com with its notification');
$t->get_ok(
    '/devuser/view_blacklisted_emails?delete_id=2&delete_notifications=1')->status_is( 200 )
        ->content_like( qr|bounce1\@mailjet.com has been removed from the blacklist.| );

is($email_blacklist->search()->count,3,"there should now three entries in the blacklist");
is($email_notifications->search()->count,9,"there should still be nine entries in the notifications");

note('We don\'t bother searching for a malformed email address');
$t->get_ok('/devuser/view_blacklisted_emails?email=foo@bar.com%E2%80%82')->status_is(400);


done_testing();

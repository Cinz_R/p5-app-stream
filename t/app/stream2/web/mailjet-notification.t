#!/usr/bin/env perl
use Test::Most;
use Test::Mojo::Stream2;
use Test::Exception;
use bigint;
use Test::MockModule;
# use Carp::Always;
# use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');

my $send_mock = Test::MockModule->new( 'Email::Sender::Simple' );
$send_mock->mock( send => sub { 1; } );
my $t = Test::Mojo::Stream2->new();
$t->post_ok(
    '/external/mailjet_notification/static_transport', {},
    json => {
        "event" => "blocked",
        "time" => 1430812195,
        "MessageID" => 13792286917004336,
        "email" => 'bounce@mailjet.com',
        "mj_campaign_id" => 0,
        "mj_contact_id" => 0,
        "customcampaign" => "",
        "CustomID" => "helloworld",
        "Payload" => "",
        "error_related_to" => "recipient",
        "error" => "user unknown"
    }
)->status_is( 200 );

$t->post_ok(
    '/external/mailjet_notification/transport', {},
    json => {
        "event" => "sausage",
        "time" => 1430812196,
        "MessageID" => 13792286917004336,
        "email" => 'awesome@example.com',
        "mj_campaign_id" => 0,
        "mj_contact_id" => 0,
        "customcampaign" => "",
        "CustomID" => "helloworld",
        "Payload" => "",
        "error_related_to" => "recipient",
        "error" => "user unknown"
    }
)->status_is( 200 );

$t->post_ok(
    '/external/mailjet_notification/transport', {},
    json => {
        "event" => "bounce",
        "time" => 1430812195,
        "MessageID" => 13792286917004336,
        "email" => 'softbounce@mailjet.com',
        "mj_campaign_id" => 0,
        "mj_contact_id" => 0,
        "customcampaign" => "",
        "CustomID" => "helloworld",
        "Payload" => "",
        "error_related_to" => "recipient",
        "error" => "user unknown",
        "hard_bounce" => 0
    }
)->status_is( 200 );

$t->post_ok(
    '/external/mailjet_notification', {},
    json => {
        "event" => "bounce",
        "time" => 1430812195,
        "MessageID" => 13792286917004336,
        "email" => 'hardbounce@mailjet.com',
        "mj_campaign_id" => 0,
        "mj_contact_id" => 0,
        "customcampaign" => "",
        "CustomID" => "helloworld",
        "Payload" => "",
        "error_related_to" => "recipient",
        "error" => "user unknown",
        "hard_bounce" => 1
    }
)->status_is( 200 );

my $preblocked = {
        "event" => "blocked",
        "time" => 1430812195,
        "MessageID" => 13792286917004336,
        "email" => 'preblocked@mailjet.com',
        "mj_campaign_id" => 0,
        "mj_contact_id" => 0,
        "customcampaign" => "",
        "CustomID" => "helloworld",
        "Payload" => "",
        "error_related_to" => "mailjet",
        "error" => "spam preblocked",
};

$t->post_ok(
    '/external/mailjet_notification', {},
    json => $preblocked
)->status_is( 200 );

$t->post_ok(
    '/external/mailjet_notification/whatever', {},
    json => {}
)->status_is( 400 );

is ( $t->app->stream2->factory('EmailNotification')->count(), 4 );
ok ( my $row = $t->app->stream2->factory('EmailNotification')->first() );
is ( $row->email, 'bounce@mailjet.com' );
is ( $row->notification, 'blocked' );
is ( $row->notification_body->{MessageID}, "13792286917004336" );
is ( $row->transport_method, 'static_transport', 'stored the transport method in the notification' );

ok ( my $blacklist = $t->app->stream2->factory('EmailBlacklist')->first(), 'a block notification will result in a blacklist' );
is ( $blacklist->email, 'bounce@mailjet.com' );
is ( $blacklist->purpose, 'delivery.blocked' );

is ( $t->app->stream2->factory('EmailNotification')->search({ email => 'softbounce@mailjet.com' })->count(), 1 );
is ( $t->app->stream2->factory('EmailNotification')->search({ email => 'hardbounce@mailjet.com' })->count(), 1 );

my $email = Email::Simple->create(
    header => [
        To => 'bounce@mailjet.com',
        From => 'whatever@broadbean.com',
        Subject => 'Boudin blanc',
        Bcc => 'jeje@broadbean.com ; someother@broadbean.com',
    ],
    body => 'Some message sent'
);

throws_ok( sub{ $t->app->stream2->send_email($email); } , 'Stream::EngineException::MessagingError' , "Ok good exception trying to send this email"  );

$email->header_set("To", 'awesome@example.com');

lives_ok( sub{ $t->app->stream2->send_email($email); }, "emails which have not been blocked will send ok" );

$email->header_set("Reply-To", 'bounce@mailjet.com');

lives_ok( sub{ $t->app->stream2->send_email($email); }, "emails sent ok even if the Reply-To is blocked" );

# test for bounces being promoted into softbounceblock.

my $multi_bounce_emailaddress = 'multiplesoftbounces@mailjet.com';
$email->header_set("To", $multi_bounce_emailaddress);

# create some bounces
my $bounce_information = {
    "event" => "bounce",
    "time" => time,
    "MessageID" => 13792286917004336,
    "email" => $multi_bounce_emailaddress,
    "mj_campaign_id" => 0,
    "mj_contact_id" => 0,
    "customcampaign" => "",
    "CustomID" => "helloworld",
    "Payload" => "",
    "error_related_to" => "recipient",
    "error" => "user unknown",
    "hard_bounce" => 0
};

# current softbounce->softbounceblock logic is:
# if there are 5 soft bounces within a 7 days then we promote to softbounceblock.
for (1..4) {
    $t->post_ok(
        '/external/mailjet_notification/transport', {}, json => $bounce_information
    )->status_is( 200 );

    lives_ok(
        sub{ $t->app->stream2->send_email($email); },
        'softbounce email should be sent,as the '
    );
};

# one more to trigger the blocking
$t->post_ok(
    '/external/mailjet_notification/transport', {}, json => $bounce_information
)->status_is( 200 );

dies_ok(
    sub{ $t->app->stream2->send_email($email); },
    'soft bounces  emails which have not been blocked will send ok'
);

# We sent a preblocked early which was sent, now sending 3 more to be just under the cap of 5 in 7 days.

my $preblocked_email = Email::Simple->create(
    header => [
        To => 'preblocked@mailjet.com',
        From => 'whatever@broadbean.com',
        Subject => 'Boudin blanc',
        Bcc => 'jeje@broadbean.com ; someother@broadbean.com',
    ],
    body => 'Some message sent'
);

for (1..3) {
    $t->post_ok(
        '/external/mailjet_notification/transport', {}, json => $preblocked
    )->status_is( 200 );

    lives_ok(
        sub{ $t->app->stream2->send_email($preblocked_email); },
        'preblocked email sent ok'
    );
};

is ( $t->app->stream2->factory('EmailBlacklist')->search({ email => 'preblocked@mailjet.com' })->count(), 0, 'preblocked email is not on the blacklist');

# send one more which should bring us to the cap.
$t->post_ok(
    '/external/mailjet_notification/transport', {}, json => $preblocked
)->status_is( 200 );

dies_ok(
    sub{ $t->app->stream2->send_email($preblocked_email); },
    'preblocked email not sent at cap'
);

is ( $t->app->stream2->factory('EmailBlacklist')->search({ email => 'preblocked@mailjet.com' })->count(), 1, 'preblocked email is now on the blacklist');
is ( $t->app->stream2->factory('EmailBlacklist')->search({ email => 'preblocked@mailjet.com' })->first->purpose, 'delivery.preblock', 'preblocked purpose is correct');

done_testing();

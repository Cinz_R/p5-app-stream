#! perl
use strict;
use warnings;

use Test::More;

unless( $ENV{ALIEN_TEST} ){
    plan skip_all => 'No ALIEN_TEST in ENV. Skipping';
}

use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');



## From the code. Traditional endpoints
# lib/Stream/TP2/ElasticSearch.pm:    my $response = $tp2->json_get('https://es.bb.artirix.com/items/applicant/' . $id);
# lib/Stream/TP2/Constants.pm:    return 'https://bbsearch.artirix.com/solr/applicants/edismax/';
# lib/Stream/TP2/Constants.pm:    return 'https://es.bb.artirix.com/items/applicant/_search';
# lib/Stream/TP2/Constants.pm:    return 'https://bbapi.artirix.com/api/add_document';
# lib/Stream/TP2/Constants.pm:    return 'https://bbapi.artirix.com/api_b/add_document';

use Search::Elasticsearch;

my $e = Search::Elasticsearch->new(
                                   client => '0_90::Direct',
                                   nodes => [ 'https://broadbean:wor2lahT@es.bb.artirix.com' ],
                                  );

ok( $e->ping() , "Ok the ping works");

# diag explain $e->info() ;

my $indices = $e->indices();
my $indices_stats = $indices->stats();

ok( $indices->exists( index => 'items2' ) , "Ok our index items2 exists");

ok( $indices->get_aliases( index => 'items2')->{'items2'}->{aliases}->{items} , "Ok items is an alias of items2");

# The applicant mapping (aka type).
# diag explain $indices->get_mapping( index => 'items2' , type => 'applicant' );

## See https://juice.adcourier.com/dev_work/view-ticket.cgi?id=42259
# We want client_id to test with to be  cinzwonderland
# We want to search for the phrase "Cloud server" and check the highlighting is not fucked
# Using the new artirix.

my $filter = {
              bool => { must => { term => { client_id => "cinzwonderland" }}}
             };

my $query = [
             { 'query_string' => { 'query' => ' "Cloud server"', 'default_field' => '_all' } }
            ];

my $highlight = { 'fields' => { cv_text => {} },
                  'fragment_size' => 20,
                  'number_of_fragments' => 3
                };

my $search_body = {  filter => $filter , query => $query , highlight => $highlight };

my $old_highlight;

{
    # First check with the old one.
    my $results = $e->search( body => { %$search_body , size => 1 } );
    ok( $results->{hits}->{total} , "Ok got total");
    diag("Did hit ".$results->{hits}->{total}." Results");
    # diag explain $results->{hits};

    #  Check the first hit highlight
    my @cv_text_hls = @{ $results->{hits}->{hits}->[0]->{highlight}->{cv_text} };

    $old_highlight = join(' ... ' , @cv_text_hls);
    # diag "Highlight size is ".length($old_highlight);
}


my $new_e = Search::Elasticsearch->new(
                                       # client => '0_90::Direct',
                                       nodes => [ 'https://broadbean:wor2lahT@bbapi.qa.artirix.com' ] );
diag( explain ( $new_e->info() ));
ok( $new_e->ping(), "Ok new elastic search is oup");
ok( my $new_idx = $new_e->indices() , "Ok got new index");
# diag $new_idx->stats();
# ok( $new_idx->exists( index => 'items' ), "OK got items index");

my $new_highlight;

{
    my $matching_id = 49862349;

    ## Index the same document in our new beta search.
    ## This is the CV matching '"Cloud server"'
    my $document = $e->get_source( index => 'items2' , type => 'applicant' , id => $matching_id );
    # diag explain $document;

    $new_e->index( index => 'items',
                   type => 'applicant',
                   id => $matching_id,
                   body => $document );

    my $search_body = {  filter => $filter , query => $query , highlight => { %$highlight,
                                                                            }};

    my $new_results = $new_e->search( index => 'items',  body => { %$search_body , size => 1 } );
    ok( $new_results->{hits}->{total} , "Ok got total");
    # diag("Did hit on new one ".$new_results->{hits}->{total}." Results");
    # diag explain $results->{hits};

    #  Check the first hit highlight
    my @cv_text_hls = @{ $new_results->{hits}->{hits}->[0]->{highlight}->{cv_text} };

    $new_highlight = join(' ... ' , @cv_text_hls);
    # diag "New Highlight size is ".length($new_highlight);
}

cmp_ok( length( $old_highlight ) , '>' , length( $new_highlight ) , "New highlight is shorter than the old one");


done_testing();

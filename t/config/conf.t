#!/usr/bin/env perl
use strict; use warnings;
use Test::Most;
use File::Temp qw/ tempfile /;
use Data::Dumper;
use Hash::Merge qw/ merge /;
use Stream2;
use File::Basename;

my ($single_fh, $single_fn) = tempfile();
my ($master_fh, $master_fn) = tempfile();
my ($child1_fh, $child1_fn) = tempfile();
my ($child2_fh, $child2_fn) = tempfile();

print $single_fh q|{
    key_1 => 'yellow',
    key_2 => 'pink',
    key_3 => 'green',
    key_4 => 'blue',
    key_array => [qw/rouge rose grun/],
    key_hash => {
        key_hash_1 => 'Wednesday',
        key_hash_inner => {
            key_hash_inner_1 => 'Tuesday',
            key_hash_inner_2 => 'Friday'
        },
        key_hash_2 => 'Thursday'
    }
}|;
close $single_fh;

# the joining of the below three should result in the above

print $child1_fh q|{
    key_1 => 'joune', # replaced by child2
    key_2 => 'mouve', # replaced by master
    key_4 => 'blue',
    key_array => [qw/rouge rose grun/],
    key_hash => {
        key_hash_1 => 'The Beach, starring Leonardo Dicaprio'
    }
}|;
close $child1_fh;

print $child2_fh q|{
    key_1 => 'yellow',
    key_3 => 'green',
    key_hash => {
        key_hash_1 => 'Wednesday',
        key_hash_2 => 'Sunday',
        key_hash_inner => {
            key_hash_inner_1 => 'Tuesday',
            key_hash_inner_2 => 'The Departed, starring Leonardo Dicaprio'
        }
    }
}|;
close $child2_fh;

my ( $child1_bn ) = fileparse( $child1_fn );
my ( $child2_bn ) = fileparse( $child2_fn );

print $master_fh qq|{
    include_config => [
        '$child1_bn',
        '$child2_bn',
    ],

    key_hash => {
        key_hash_inner => {
            key_hash_inner_2 => 'Friday'
        },
        key_hash_2 => 'Thursday'
    },

    key_2 => 'pink'
}|;
close $master_fh;

my $s2_single = Stream2->new({ config_file => $single_fn });
my $s2_joined = Stream2->new({ config_file => $master_fn });

delete( $s2_joined->config->{include_config} ); # should be the only difference

is_deeply( $s2_joined->config, $s2_single->config );

done_testing();

# cleanup ( doesnt tempfile do this? )
unlink( $_ ) for $single_fn, $master_fn, $child1_fn, $child2_fn;


use Test::Most;
use Cwd qw/abs_path/;

note 'Each conf file must have a unique installation ID.';
note "We don't test conf/*.conf files because they're just include files.";

my $home_dir   = _get_search_home_dir();
my @conf_files = glob "$home_dir/app-stream2*.conf";

my %installation_ids;
for my $conf_file ( @conf_files ) {
    note("Looking at $conf_file");
    my $id = _get_installation_id( $conf_file );
    note("Found installation_id = $id");
    if(my $alread_file = $installation_ids{$id} ){
        fail("Installation ID $id is duplicated in both $conf_file and $alread_file");
    }
    $installation_ids{ $id } = $conf_file;
}

ok scalar( keys %installation_ids ) == scalar( @conf_files ),
    'Number of (unique) keys matches the number of config files';


# returns the value of the installation_id key in the $conf_file
sub _get_installation_id {
    my $conf_file = shift;
    open my $fh, '<', $conf_file or die "failed to open $conf_file: $!\n";
    my $contents = do { local $/; <$fh> };
    close $fh;
    my $conf = eval $contents;
    return $conf->{installation_id};
}

# return the directory that contains the t/ dir
sub _get_search_home_dir {
    my $abs_path = abs_path( __FILE__ );
    return $abs_path =~ m|(.*?)/t/| ? $1 : undef;
}

done_testing();

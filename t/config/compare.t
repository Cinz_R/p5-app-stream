#!/usr/bin/env perl
use strict; use warnings;
use Test::Most;
use Data::Dumper;
use Stream2;

my $s2_main_single = Stream2->new({ config_file => 't/config/original.conf' });
my $s2_main_joined = Stream2->new({ config_file => 't/config/app-stream2.conf' });

ok( delete $s2_main_joined->config->{im_test_master}, 'ensure we have the right file to test with' );
ok( delete $s2_main_joined->config->{im_test_child}, 'ensure it is reading in the correct children' );
ok( delete $s2_main_joined->config->{include_config} ); # should be the only difference

is_deeply( $s2_main_joined->config, $s2_main_single->config );

done_testing();

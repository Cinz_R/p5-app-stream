use Test::Most tests => 2;

use_ok('Test::Mojo::Stream2');

subtest "Auth plugin uses our config" => sub {
    my $t = Test::Mojo::Stream2->new();
    is( $t->app->auth_providers->config->{default_provider}, 'demo' );
    is( $t->app->config->{plugins}->{auth}->{default_provider}, 'demo' );
};

done_testing();

use Test::Most tests => 2;

use_ok 'Bean::AdcDB::Schema';
ok $Bean::AdcDB::Schema::VERSION, "We need a version for DBIC-Migration";

done_testing;

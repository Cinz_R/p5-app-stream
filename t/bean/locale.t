#!/usr/bin/env perl
use Test::Most;
use Locale::Maketext;

use Encode;

use_ok( 'Bean::Locale' );

is ( Bean::Locale::localise_in('made_up_locale','test'), 'test', 'Fallback to en' );
is ( Bean::Locale::localise_in('who_cares','BBTECH_RESERVED_LEFT_BRACKET'), '[', 'Well the maketext overwrite is working' );
is ( Bean::Locale::localise_in('de','Control Operator'), Encode::decode('utf-8','Schaltwärter/-in'), 'text translation encoding maintained' );

# setting to 0 allows the use of any locale name, otherwise it falls back to english
$Locale::Maketext::USING_LANGUAGE_TAGS = 0;
is ( Bean::Locale::localise_in('en','test'), 'test', 'Fallback to en' );
is ( Bean::Locale::localise_in('test_locale', 'chicken'), 'soy based alternative', 'Translations work fine' );

# dateformat
is ( Bean::Locale::localise_in('fr','[dateformat,1399950000,_DATEFORMAT_DISPLAY_DATE]'), 'mar. 13 mai 2014', 'Intropolated dateformat function works' );
$ENV{BB_TIMEZONE} = 'America/New_York';
is ( Bean::Locale::localise_in('fr','[dateformat,1399950000,_DATEFORMAT_DISPLAY_DATE]'), 'lun. 12 mai 2014', 'Timezone is picked up from env var' );

# casual_time
my $now = time;
is ( Bean::Locale::localise_in('en',"[casual_time,$now]"), 'Just now' );
my $last_week = $now - ( 86400 * 7 );
like ( Bean::Locale::localise_in('en',"[casual_time,$last_week]"), qr/last week\z/ );

# html
is ( Bean::Locale::localise_in('zh','[html,<h1>I HAVE LOTS OF TOFU & "Sausages" lol</h1>]'), '&lt;h1&gt;I HAVE LOTS OF TOFU &amp; &quot;Sausages&quot; lol&lt;/h1&gt;' );

# ord
is ( Bean::Locale::localise_in('en',"[ord,1]"), '1st' );
is ( Bean::Locale::localise_in('fr',"[ord,1]"), '1' ); # not implemented


done_testing();

{
    package Bean::Locale::test_locale;
    use base q|Bean::Locale|;
    sub _lex_refs {
        return [
            {
                chicken => 'soy based alternative'
            }
        ];
    }
    1;
}

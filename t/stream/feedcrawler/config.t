#!/usr/bin/env perl -w
use warnings;
use strict;
use Test::More;
use Test::Exception;

use Stream2;
use URI;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

use_ok( 'Stream::FeedCrawler::Config', 'Loaded moule OK' );

my $config = Stream::FeedCrawler::Config->new(
    board => 'dummy',
    stream2 => $stream2
);

dies_ok(
    sub {
        $config->set_xpath(fail => '[bad');
    },
    'Fails invalid XPath'
);

dies_ok(
    sub {
        $config->set_regex(fail => '[bad');
    },
    'Fails invalid Regex'
);

dies_ok(
    sub {
        $config->set_uri(login => '/login');
    },
    'Fail set_uri without baseurl set'
);


lives_ok(
    sub {
        $config->base_url(URI->new('http://example.com/'));
        $config->set_xpath(foo => '/should/work');
        $config->set_regex(bar => '[123abc]{3,}');
        $config->set_value(day => 'Monday');
        $config->set_uri(login => '/login');
    },
    'Added config values'
);

is($config->get_url('login'), 'http://example.com/login', 'Got URL alright');
is($config->get_xpath('foo'), '/should/work', 'Got XPath alright');
is($config->get_regex('bar'), qr/[123abc]{3,}/, 'Got Regex alright');
is($config->get_value('day'), 'Monday', 'Got generic value alright');


dies_ok(
    sub {
        $config->get_url('not a key');
    },
    'get_url dies with bad key'
);

dies_ok(
    sub {
        $config->get_xpath('not a key');
    },
    'get_xpath dies with bad key'
);

dies_ok(
    sub {
        $config->get_regex('not a key');
    },
    'get_regex dies with bad key'
);

dies_ok(
    sub {
        $config->get_value('not a key');
    },
    'get_value dies with bad key'
);



done_testing();

#!/usr/bin/env perl -w
use warnings;
use strict;
use Test::More;
use Test::Exception;

use HTML::TreeBuilder::XPath;

use Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

use_ok( 'Stream::FeedCrawler::Crawler', 'Loaded moule OK' );

my $crawler = Stream::FeedCrawler::Crawler->new(
    board => 'dummy',
    base_url => 'http://example.com',
    stream2 => $stream2
);


# Create a HTML doc for XPath testing
my $html = <<EOF;
<!doctype html>
<html>
    <head>
        <title>XPath test doc</title>
    </head>
<body>
    <h1>My Page</h1>
    <ul id="doge">
        <li>Such test</li>
        <li>coverage</li>
        <li>Much class</li>
        <li>Wow</li>
    </ul>
    <div id="full_name">
        <span class="name">Cat</span>
        <span class="name">Dog</span>
    </div>
</body>
</html>
EOF

my $xpc = HTML::TreeBuilder::XPath->new_from_content($html);

# XPATH NODES
ok(
    my $list = $crawler->validate_xpath_nodes(
        label          => 'Doge List',
        context_node   => $xpc,
        path           => '//ul[1]',
        expected_count => 1,
        severity       => 'optional'
    ),
    'Expected nodes count'
);

ok(
    my $list_item = $crawler->validate_xpath_nodes(
        label          => 'Wow item',
        context_node   => $list,
        path           => './li[4]',
        expected_count => 1,
        severity       => 'optional'
    ),
    'Context node relative path'
);

# Test the validation

dies_ok(
    sub {
        $crawler->validate_xpath_nodes(
            label => 'List',
            expected_count => 20,
            context_node => $xpc,
            path => '//il',
            severity => 'fatal',
        );
    },
    'expected_count mismatch'
);

dies_ok(
    sub {
        $crawler->validate_xpath_nodes(
            label => 'List',
            min_count => 20,
            context_node => $xpc,
            path => '/',
            severity => 'fatal'
        );
    },
    'min_count'
);

dies_ok(
    sub {
        $crawler->validate_xpath_nodes(
            label => 'List',
            max_count => 2,
            context_node => $xpc,
            path => '//*',
            severity => 'fatal'
        );

    },
    'max_count'
);

is( $list_item->as_text, 'Wow', 'Fetched item ');

# VALUES
is(
    $crawler->validate_xpath_value(
        label        => 'List ID',
        context_node => $list_item,
        path         => '../@id',
        severity     => 'optional',
    ),
    'doge',
    'XPath for values'
);

is(
    $crawler->validate_xpath_value(
        label        => 'List Labels',
        context_node => $xpc,
        path         => '//div[@id="full_name"]/span/text()',
        severity     => 'optional',
    ),
    'CatDog',
    'XPath for multiple values'
);

ok(
    $crawler->validate_xpath_value(
        label        => 'Name div ID',
        context_node => $xpc,
        path         => '//div[@id="full_name"]/@id',
        severity     => 'fatal',
        like         => qr/\wull_name/
    ),
    'XPath value like regex'
);

dies_ok(
    sub {
        $crawler->validate_xpath_value(
            label        => 'Name div ID',
            context_node => $xpc,
            path         => '//div[@id="full_name"]/@id',
            severity     => 'fatal',
            like         => qr/nonsense/
        );
    },        
    'XPath value like regex error throw'
);

#Other methods

done_testing();

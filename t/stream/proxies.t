#! perl

use Test::More;
use strict;
use warnings;

use Stream::Proxies::UK;
use Stream::Proxies::Monster;
use Stream::Proxies::Indeed;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

my $proxy = Stream::Proxies::UK->new();
ok( $proxy->proxy_url() , "Ok got proxy URL");

my $monster_proxy = Stream::Proxies::Monster->new();
ok($monster_proxy->proxy_url('http://rsx.monster.com/bla_bla_bla') , "Ok got a proxy URL");

my $indeed_proxy = Stream::Proxies::Indeed->new();
ok($indeed_proxy->proxy_url('http://www.indeed.com/r/85098903580923859/pdf'), "Ok got a proxy URL");
ok(! $indeed_proxy->proxy_url('http://www.indeed.com/somestuff'), "Ok got no proxy URL");

done_testing();


use Test::Most;
use Stream::TemplateLoader;
use File::Temp;
use Path::Tiny;

my $libdir = File::Temp->newdir();
push @INC, "$libdir";

my $example_feed = path($libdir)->child("ExampleFeed.pm");

$example_feed->spew(<<__EOF_MOD);
syntax error
__EOF_MOD

dies_ok { Stream::TemplateLoader->load('ExampleFeed') } "Unable to load feeds that have a syntax error";

$example_feed->spew(<<__EOF_MOD);
package ExampleFeed;
use base 'Stream::Templates::Default';
sub loaded { 1 }
1;
__EOF_MOD

lives_ok { Stream::TemplateLoader->load('ExampleFeed') } "Until the syntax error is fixed";
ok ExampleFeed->loaded, 'Then we can reload the feed';

done_testing();

use Test::Most tests => 2;

use Stream::TemplateLoader;

{
    package Stream::Templates::Example;
    use base 'Stream::Templates::Default';

    our (%standard_tokens, %posting_only_tokens, %authtokens, %global_authtokens);

    $authtokens{token_1} = {
        Label => 'Authtoken 1',
        SortMetric => 1,
        Type  => 'Text',
    };

    $global_authtokens{global_1} = {
        Type => 'Text',
        SortMetric => 1,
    };

    $global_authtokens{another_global} = {
        Type => 'Text',
        SortMetric => 2,
    };
};

subtest "Normal authtokens" => sub {
    my (@authtokens) = Stream::TemplateLoader->list_authtokens('Example');
    is( scalar(@authtokens), 1, "There is one authtoken" );

    my (@authtoken_names) = Stream::TemplateLoader->list_authtoken_names('Example');
    is( scalar(@authtoken_names), 1, "and we get the same number of names" );

    my $all_names = 1;
    foreach my $authtoken_name ( @authtoken_names ) {
        if ( ref( $authtoken_name ) ) {
            diag("authtoken $authtoken_name looks like an ref, expecting just the token name");
            $all_names = 0;
        }
    }

    ok( $all_names, "all authtokens accounted for" );
};

subtest "Global authtokens" => sub {
    my (@global_authtokens) = Stream::TemplateLoader->list_global_authtokens('Example');
    is scalar(@global_authtokens), 2, "There are two global authtoken";
    is $global_authtokens[0]->{Name}, "global_1", "Ordered by sort metric";
};

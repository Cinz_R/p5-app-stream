use Test::Most tests => 3;

use_ok('Stream::TemplateLoader');

{
  package Stream::Templates::LoadedAlready;
  use base 'Stream::Templates::Default';
};

lives_ok { Stream::TemplateLoader->list_tokens('LoadedAlready') };
dies_ok  { Stream::TemplateLoader->list_tokens('NonExistentFeed') };

done_testing();

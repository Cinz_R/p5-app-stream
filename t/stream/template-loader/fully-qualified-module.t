use Test::More;

use Stream::TemplateLoader;

# use_ok('Stream::TemplateLoader');

{
  package Example;
  use base 'Stream::Templates::Default';
};

{
  package Stream::Templates::Partial;
  use base 'Stream::Templates::Default';
};


is( Stream::TemplateLoader->load('Example'), 'Example', 'Can load module without any :: in them' );
is( Stream::TemplateLoader->load('Partial'), 'Stream::Templates::Partial', 'Can load partially qualified feeds' );
is( Stream::TemplateLoader->load('Stream::Templates::Partial'), 'Stream::Templates::Partial', 'Can load fully qualified feeds');

done_testing();

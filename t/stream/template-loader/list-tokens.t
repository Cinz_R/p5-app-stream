use Test::Most tests => 7;
use Stream::TemplateLoader;

{
    package Stream::Templates::Example;
    use base 'Stream::Templates::Default';

    our (%standard_tokens, %posting_only_tokens, %authtokens, %global_authtokens);
    $standard_tokens{location} = {
        Label   => 'Location',
        Type    => 'Text',
        Default => 'Canary Wharf',
        Size    => '60',
    };

    $standard_tokens{mandatory_token} = {
        Label     => 'Mandatory Token',
        Type      => 'Text',
        Default   => '',
        Mandatory => 1,
        Size      => 60,
    };

    $standard_tokens{essential_tokens} = {
        Type      => 'Text',
        Essential => 1,
    };

    $posting_only_tokens{location_copy} = {
        Helper  => 'COPY(%location%)',
    };

    $authtokens{token_1} = {
        Label => 'Authtoken 1',
        SortMetric => 1,
        Type  => 'Text',
    };
};

my (@mandatory_tokens) = Stream::TemplateLoader->list_mandatory_tokens('Example');
is scalar(@mandatory_tokens),    1,                 "Expecting 1 mandatory token";
is $mandatory_tokens[0]->{Name}, "mandatory_token", "    and it is the right one";

my (@optional_tokens) = Stream::TemplateLoader->list_optional_tokens('Example');
is scalar(@optional_tokens),    1,          "... 1 optional token";
is $optional_tokens[0]->{Name}, "location", "    and it is the right one";

my (@essential_tokens) = Stream::TemplateLoader->list_essential_tokens('Example');
is scalar(@essential_tokens),    1,                  "and 1 essential token";
is $essential_tokens[0]->{Name}, "essential_tokens", "    and it is the right one";

my (@standard_tokens) = Stream::TemplateLoader->list_tokens('Example');
is scalar(@standard_tokens), 3, "list_tokens() lists all tokens except posting only and authtokens";

done_testing();

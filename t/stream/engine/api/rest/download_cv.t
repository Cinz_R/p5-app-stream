#! perl -w
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Stream2;
use Stream2::Results::Result;

use_ok 'Stream::Templates::RestMock';

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $user_factory = $stream2->factory('SearchUser');
my $user = $user_factory->new_result({
    id => 91,
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 157656,
    group_identity => 'acme',
});
$user->save();

my $candidate = Stream2::Results::Result->new({
    candidate_id        => '123455',
    email               => 'fakeyfake@faker.com',
    resume_text         => 'fakey cv dont hire me',
    resume_filename     => 'fakey.txt',
    resume_content_type => 'txt',
});

ok( my $template = $stream2->build_template('RestMock', undef , { user_object => $user } ) , "Ok got template");

ok( my $response = $template->download_cv($candidate), 'rest api can download the cv and receive response');

is $response->header('X-Candidate-Email'), 'fakeyfake@faker.com', 'cv response returns candidates email';
is $response->header('Content-Type'), 'txt', 'can get content type from fake response';

LWP::UserAgent::Mockable->finished();
done_testing();

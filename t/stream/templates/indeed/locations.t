use Test::Most;
use Stream2;

use_ok "Stream::Templates::indeed";

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
};

use LWP;
use LWP::UserAgent::Mockable;

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

ok( my $template = $stream2->build_template('indeed', undef , { location_api => $stream2->location_api() } ) , "Ok got template");

my $locations = [
    {
        location_id =>  971,
        miles  =>  8,
        expected_location => 'High Point, NC',
        expected_iso => 'US',
    },
    {
        location_id   =>  25488,
        miles  =>  100,
        expected_location => 'California',
        expected_iso => 'US',
    },
    {
        location_id   =>  39493,
        miles  =>  10,
        expected_location => '', # England
        expected_iso => 'GB',
    },
    {
        location_id   =>  39585,
        miles  =>  20,
        expected_location => 'Leicestershire',
        expected_iso => 'GB',
    },
    {
        location_id   =>  39499,
        miles  =>  50,
        expected_location => 'Huntingdon, Cambridgeshire',
        expected_iso => 'GB',
    },
    {
        location_id   =>  39550,
        miles  =>  50,
        expected_location => 'London',
        expected_iso => 'GB',
    },
    {
        location_id   =>  39551,
        miles  =>  50,
        expected_location => 'City of London',
        expected_iso => 'GB',
    },
    {
        location_id   =>  39798,
        miles  =>  50,
        expected_location => 'Northumberland',
        expected_iso => 'GB',
    },
    {
        location_id   =>  40512,
        miles  =>  50,
        expected_location => 'Wellington Somerset',
        expected_iso => 'GB',
    },
    {
        location_id   =>  35682,
        miles  =>  50,
        expected_location => 'Ipswich QLD',
        expected_iso => 'AU',
    },

];

foreach (@$locations) {
    my $location = $template->indeed_location($_->{location_id}, $_->{miles});
    is($location, $_->{expected_location});
    is($template->token_value('country_code'), $_->{expected_iso});
}

done_testing;

END {
    # END block ensures cleanup if script dies early
    LWP::UserAgent::Mockable->finished;
};

use Test::Most;
use DateTime;

use_ok "Stream::Templates::jobing";

my $template = Stream::Templates::jobing->new();

my $cv_html = <<'eof';
<div id="cv">
    <h1 onclick="inject();">Foo's CV</h1>
    <script>
        inject();
    </script>
    <a href="javascript:inject();">Boop</a>
    <iframe href="http://www.exmaple.com"></iframe>
</div>
eof
my $expected_cv_html = <<'eof';
<div>
    <h1>Foo's CV</h1>
    
    Boop
    
</div>
eof

my $clean_cv_html = $template->scrub_html_cv($cv_html);
is($clean_cv_html, $expected_cv_html, 'CV sanitised good');


my $now = DateTime->now();

my $duration;
$duration = $now->clone()->add(seconds => 3) - $now;
is($template->duration_nice_name($duration), '3 seconds ago.', 'OK nicename duration (seconds)');

$duration = $now->clone()->add(minutes => 3) - $now;
is($template->duration_nice_name($duration), '3 minutes ago.', 'OK nicename duration (minutes)');

$duration = $now->clone()->add(hours => 3) - $now;
is($template->duration_nice_name($duration), '3 hours ago.', 'OK nicename duration (hours)');

$duration = $now->clone()->add(days => 3) - $now;
is($template->duration_nice_name($duration), '3 days ago.', 'OK nicename duration (days)');

$duration = $now->clone()->add(months => 3) - $now;
is($template->duration_nice_name($duration), '3 months ago.', 'OK nicename duration (months)');

$duration = $now->clone()->add(years => 3) - $now;
is($template->duration_nice_name($duration), '3 years ago.', 'OK nicename duration (years)');

done_testing();

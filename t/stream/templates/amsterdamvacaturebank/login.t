BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
};


use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Test::MockModule;
use Stream2;

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});

my $valid_params = {
        username => 'ali@broadbean.com',
        password => 'cors5637',
        autologinvac => '1',
        loginvac => 'Inloggen',
};
my $invalid_params = {
    fail => 'yes'
};

my $current_params;
my $feed_mock = Test::MockModule->new('Stream::Templates::Stadvacaturebank');
$feed_mock->mock( _login_params => sub {
    return $current_params;
});

ok(
    my $template = $stream2->build_template(
        'amsterdamvacaturebank',
        undef,
        {user_object => $memory_user}
    ),
    "Ok got template"
);


$current_params = $valid_params;

ok($template->_login, 'Logged in successfully');

ok($template->_logout, 'Logged out OK');

$current_params = $invalid_params;
dies_ok(sub {$template->_login}, 'Handled failed login');

done_testing();

END {
    LWP::UserAgent::Mockable->finished;
}
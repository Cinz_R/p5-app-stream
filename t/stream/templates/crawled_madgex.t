#!/usr/bin/env perl -w
use warnings;
use strict;
use Test::More;
#use Test::Exception;


use Stream2;

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

use_ok( 'Stream::Templates::CrawledMadgex');

ok(
    my $template = $stream2->build_template(
        'CrawledMadgex',
        undef,
        { location_api => $stream2->location_api() }
    ),
    "Ok got template"
);

done_testing();

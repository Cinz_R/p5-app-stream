#! perl -w

use strict;
use warnings;
use Test::Most;
use Test::MockModule;

use Stream2;
use Stream::Templates::cb_mysupply;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();

# Ok the setting are not empty
ok( $stream2->factory('Setting')->count() , "Ok got some settings");

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser') , "Ok can get resultset");

my $users_factory = $stream2->factory('SearchUser');

# Time to create a user.
my $memory_user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});


my $template = Stream::Templates::cb_mysupply->new();
$template->{user_object} = $memory_user;

my $mock_mysupply = Test::MockModule->new( ref( $template ));
$mock_mysupply->mock( 'download_profile' => sub{
                          my ($self, $candidate) = @_;
                          return $candidate;
                      });

my $candidate = Stream2::Results::Result->new();

$candidate->attr( 'has_image' , 'False' );
is_deeply($template->rendered_attachments( $candidate ), [], "Ok no has_image, no attachment");

$candidate->attr( 'has_image' , 'True' );
is_deeply($template->rendered_attachments( $candidate ), [], "Ok has_image but no base64 thing.");

$candidate->attr('name' , 'Ima Wiener' );
$candidate->attr('candidate_id' , 'BLA');

$candidate->attr('cv_base64' , 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/7QA4UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAAA4QklNBCUAAAAAABDUH');

ok( my $attachments = $template->rendered_attachments($candidate), 'Got rendered attachment');
is( scalar @$attachments, 1, 'Single attachment');

ok( my $render = $attachments->[0], 'Got attachment' );
ok( my $response = $template->download_attachment($candidate, $render->{key}));

is( $response->content_type , 'image/jpeg' );
is( $response->filename , 'BLA-Ima Wiener.jpeg');

done_testing();

use Test::Most;

use Stream2;
use Test::MockModule;

my $mock_provider = Test::MockModule->new("App::Stream2::Plugin::Auth::Provider::Adcourier");
$mock_provider->mock('find_advert', sub {
    return {
        stream2_jobref => 'mockery1234',
        stream2_jobtitle => 'who cares'
    }
});

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 157656, # simonr@test.simonr.simonr
    group_identity => 'acme',
});

$user->save();

$user->ripple_settings()->{login_provider} = 'adcourier';

ok( my $template = $stream2->build_template('cb_mysupply', undef , { user_object => $user } ) , "Ok got template");

my $tests = [
    {
        input   =>  'csp_job_shortlist:jobref1234/1234',
        output  =>  'jobref1234',
        msg     =>  'csp list with jobref and id should return jobref',
    },
    {
        input   =>  'adc_shortlist:mockery1234/wwfewfaefw',
        output  =>  'mockery1234',
        msg     =>  'adc list with jobref and id should return jobref',
    },
    {
        input   =>  'poobar-not-a-req-id',
        output  =>  'poobar-not-a-req-id',
        msg     =>  'jobref we dont know should return as-is',
    },
    {
        input   =>  'csp_job_shortlist:80781BR',
        output  =>  '80781BR',
        msg     =>  'csp list with jobref and no id should return jobref',
    },

];

foreach my $test (@$tests) {
    is $template->_job_req_id_to_ref($test->{input}), $test->{output}, $test->{msg};
}

done_testing();

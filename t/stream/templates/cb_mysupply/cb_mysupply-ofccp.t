#!/usr/bin/env perl
use Test::Most;
use Test::MockModule;
use Stream::Templates::cb_mysupply;
use HTTP::Response;

my $config_file = 't/app-stream2.conf';
ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

my $feed = Stream::Templates::cb_mysupply->new();
my $candidate = Stream2::Results::Result->new({ candidate_id => 'test candidate' });
my $feed_mock = Test::MockModule->new( 'Stream::Templates::cb_mysupply' );


my $headers = HTTP::Headers->new();
$headers->header('Content-Type' => 'text/xml');
my $response = HTTP::Response->new(
    200,
    'OK',
    $headers,
    '<xml>
        <success>hello</success>
        <Resume>Good worker</Resume>
    </xml>'
);
$feed_mock->mock( scrape_download_cv => sub {} );
$feed->{user_object} = TestUser->new({});
$feed->{company} = 'blah';
$feed->token_value( account_did => 'no no no' );
$feed->token_value( timeframe => 'wahetevr' );
$feed_mock->mock( search_success => sub { 'hello'; } );

# download cv no ofccp
$feed_mock->mock( get => sub {
    my ( $s, $url ) = @_;

    unlike ( $url, qr/OFCCPSearchTitle/ );

    $feed_mock->mock( response => sub { return $response; } );
    return $response;
});
$feed->download_cv( $candidate );
$feed->download_profile_submit( $candidate );
$feed->search_submit();


# download cv with ofccp
$candidate->set_attr('ofccp_context', 'TEST OFCCP' );
$feed->token_value( ofccp_context => 'TEST OFCCP' );
$feed_mock->mock( get => sub {
    my ( $s, $url ) = @_;

    like ( $url, qr/OFCCPSearchTitle=TEST(?:\+|%20)OFCCP/ );

    $feed_mock->mock( response => sub { return $response; } );
    return $response;
});
$feed->download_cv( $candidate );
$feed->download_profile_submit( $candidate );
$feed->search_submit();

done_testing();

{
    package TestUser;
    sub new { bless pop, shift; }
    sub stream2 { return $stream2; }
    sub to_hash { return {}; }
    sub contact_email { return 'Bob.Boberson@BobsCarRental.com'; }
}

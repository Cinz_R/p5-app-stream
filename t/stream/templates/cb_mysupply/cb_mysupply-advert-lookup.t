#! perl -w

use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;
use Stream::Templates::cb_mysupply;

my $feed_mock = Test::MockModule->new( 'Stream::Templates::cb_mysupply' );
my $auth_mock = Test::MockModule->new( 'App::Stream2::Plugin::Auth::Provider::Adcourier' );
$auth_mock->mock( find_advert => sub { return { stream2_jobref => 'sausage' }; } );

my $t = Test::Mojo::Stream2->new();
$t->login_ok();
my $user = $t->app->stream2->factory('SearchUser')->find(1);
$user->ripple_settings()->{login_provider} = 'adcourier_ats';
$user->provider('adcourier');
$feed_mock->mock( user_object => sub { $user } );

# internal adcourier shortlist
is ( Stream::Templates::cb_mysupply->_job_req_id_to_ref( 'adc_shortlist:look_me_up' ), 'sausage' );

# if the custom list can't be resolved, remove it from the options
is ( Stream::Templates::cb_mysupply->_job_req_id_to_ref( 'not_adc:tofu' ), '__PLEASE_SCRAPE_ME_OFF_' );

# at the moment the list cheese sandwhich resolving just returns the list_name:advert_id
$user->ripple_settings()->{custom_lists} = [ { name => 'not_adc' } ];
is ( Stream::Templates::cb_mysupply->_job_req_id_to_ref( 'not_adc:tofu' ), 'not_adc:tofu' );

done_testing();

use Test::Most;
use Test::MockModule;

use_ok "Stream::Templates::cb_mysupply";

my $tests = [
    {
        input   =>  10,
        output  =>  1,
    },
    {
        input   =>  30,
        output  =>  2,
    },
    {
        input   =>  100,
        output  =>  5,
    },
    {
        input   =>  101,
        output  =>  6,
    },
    {
        input   =>  500,
        output  =>  25,
    },
    {
        input   =>  1000,
        output  =>  50,
    },
    {
        input   =>  2000,
        output  =>  100,
    },
    {
        input   =>  2500,
        output  =>  100,
    },
    {
        input   =>  109210210291,
        output  =>  100,
    }
];

my $mock_feed = Test::MockModule->new('Stream::Templates::cb_mysupply');

my $max_page;
my $total_results;
$mock_feed->mock( max_pages => sub {
    my ($self, $count) = @_;
    $max_page = $count;
});

$mock_feed->mock( total_results => sub { return $total_results } );

my $template = Stream::Templates::cb_mysupply->new();
$template->{company} = 'a_company';
foreach my $test (@$tests) {

    $total_results = $test->{input};
    $template->scrape_paginator();
    is($max_page, $test->{output});
}

done_testing();

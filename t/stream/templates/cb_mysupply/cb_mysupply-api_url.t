#! perl -w

use Test::Most tests => 5;
use Test::MockModule;
use Stream::Templates::cb_mysupply;
use HTTP::Response;
use Stream2;

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $users_factory = $stream2->factory('SearchUser');
my $user          = $users_factory->new_result({
    provider       => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id    => 1,
    group_identity => 'acme',
});

$user->ripple_settings()->{login_provider} = 'adcourier';

$user->save();
ok( my $feed = $stream2->build_template('cb_mysupply', undef,
    {
        location_api => $stream2->location_api(),
        user_object  => $user,
        company => 'minons'
    } ) , "Ok got template"
);

my $candidate = Stream2::Results::Result->new({ candidate_id => 'test candidate' });
my $feed_mock = Test::MockModule->new( 'Stream::Templates::cb_mysupply' );


my $headers = HTTP::Headers->new();
$headers->header('Content-Type' => 'text/xml');
my $response = HTTP::Response->new(
    200,
    'OK',
    $headers,
    '<xml>
        <success>hello</success>
        <Resume>Good worker</Resume>
    </xml>'
);
$feed_mock->mock( scrape_download_cv => sub {} );
$feed->token_value( account_did => 'no no no' );
$feed->token_value( timeframe => 'wahetevr' );
$feed->token_value( cb_user_code => 'ASDSFDDFGFGHGHJGHK' );
$feed_mock->mock( search_success => sub { 'hello'; } );
# Avoids remote request
$feed_mock->mock( _auth_header => sub { return (); } );
# download cv no ofccp
$feed_mock->mock( get => sub {
    my ( $s, $url ) = @_;

    like( $url, qr/wwwtest.api.careerbuilder.com/, 'Using CB OAuth URL' );
    $feed_mock->mock( response => sub { return $response; } );
    return $response;
});

$feed->download_cv( $candidate );
$feed->download_profile_submit( $candidate );
$feed->search_submit();


done_testing();

BEGIN {
    $ENV{ LWP_UA_MOCK } ||= 'playback';
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}

use Test::Most;
use Test::MockModule;
use Stream::Templates::cb_mysupply;
use HTTP::Response;
use Test::Stream2;
use LWP;
use LWP::UserAgent::Mockable;


=comment

Test OAuth for the cb_mysupply feed:
    Check that auth headers are correct
    and that oneiam runs and caches separately

=cut

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });

my $users_factory = $stream2->factory('SearchUser');
my $user          = $users_factory->new_result({
    provider       => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id    => 1,
    group_identity => 'acme',
});

$user->save();

my $expires_at = time() + 300;
my $feed_mock = Test::MockModule->new('Stream::Templates::cb_mysupply');
my $oauth_mock = Test::MockModule->new('Bean::OAuth::CareerBuilder');

sub new_feed {
    return $stream2->build_template(
        'cb_mysupply',
        undef,
        {
            location_api => $stream2->location_api(),
            user_object  => $user,
            company => 'minons',
        }
    );
}

ok( my $feed = new_feed(), 'Ok got template' );

# With client-credential oauth
my @header = $feed->_auth_header;
is($header[0], 'Authorization', 'Correct header name');
like($header[1], qr/Bearer \w+/, 'Got bearer token');
$oauth_mock->mock( get_token => sub {
    return ('new_mock_client_token', $expires_at);
});
is($header[1], ($feed->_auth_header)[1], 'Used cached auth header');

# With oneiam oauth
my $oneiam_feed = new_feed();
$oneiam_feed->token_value(cb_user_code => 'nonsense');
$oauth_mock->unmock('get_token');
dies_ok(
    sub {
        $oneiam_feed->_auth_header;
    },
    'Fake cb_user_code fails'
);

$oauth_mock->mock( get_token => sub {
    return ('mock_oneiam_token', $expires_at);
});

my @oneiam_header = $oneiam_feed->_auth_header;
is($oneiam_header[0], 'Authorization', 'Correct header name');
is($oneiam_header[1], 'Bearer mock_oneiam_token', 'Got oneiam bearer token');
ok($oneiam_header[1] ne $header[1], 'OneIAM not using client-flow token');

$oauth_mock->mock( get_token => sub {
    return ('new_mock_oneiam_token', $expires_at, 'mock_refresh_token');
});
my $cache_oneiam_feed = new_feed();
$cache_oneiam_feed->token_value(cb_user_code => 'nonsense');
is( ($cache_oneiam_feed->_auth_header)[1], $oneiam_header[1], 'Reused OneIAM token');

my $separate_oneiam_feed = new_feed();
$separate_oneiam_feed->token_value(cb_user_code => 'new user code');
is(
    ($separate_oneiam_feed->_auth_header)[1],
    'Bearer new_mock_oneiam_token',
    'Different user code uses different cache',
);

# For good measure test regular flow again
$oauth_mock->unmock('get_token');
$feed->token_value('cb_user_code' => undef);
is($header[1], ($feed->_auth_header)[1], 'Regular OAuth header still cached/used separately');

LWP::UserAgent::Mockable->finished;

done_testing();

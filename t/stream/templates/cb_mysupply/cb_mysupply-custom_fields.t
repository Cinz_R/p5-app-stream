#! perl -w
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use strict;
use warnings;
use Test::Most;

use_ok("Stream::Templates::cb_mysupply");

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $users_factory = $stream2->factory('SearchUser');
my $user          = $users_factory->new_result({
    provider       => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id    => 1,
    group_identity => 'acme',
    contact_details => {
        contact_email => 'example@test.com'
    }
});
$user->save();
ok( my $template = $stream2->build_template('cb_mysupply', undef,
    {
        location_api => $stream2->location_api(),
        user_object  => $user,
        company => 'minons'
    } ) , "Ok got template"
);

#
# General tokens
#

my $tokens =
    {
        keywords        =>  'Project Manager',
        account_did     =>  '123',
        sort_by         =>  'sortyourself',
        sort_direction  =>  'onedirection',
        timeframe       =>  '3',
        cv_updated_within   =>  '1M',
        CB_semantic_query  => 'pretty semantic',
    }
;

foreach my $token ( keys %$tokens ) {
    $template->token_value($token => $tokens->{$token});
}

my %fields = $template->search_fields();

is($fields{'Keywords'}, $template->token_value('keywords'));
is($fields{'Rows'}, $template->results_per_page());
is($fields{'PageNumber'}, $template->current_page());
is($fields{'AccountDID'}, $template->token_value('account_did'));
is($fields{'MostRecentActivity'}, '30 days');
is($fields{'CB_semantic_query'}, $template->token_value('CB_semantic_query'));
is($fields{useremail}, 'example@test.com', 'username is always sent for CB tracking');

my $location_tests = [
    {
        location_id => 25735, # LA, California, USA
        expected => {
            Location => 'Los Angeles, California, USA',
        }
    },
    {
        location_id => 43409, # Chennai, India
        expected => {
            Location => 'Chennai, Tamil Nadu, India',
        }
    },
    {
        location_id => 39550, # London, England
        expected => {
            Location => 'London, England',
        }
    },
    {
        location_id => 43308, # India
        expected => {
            Location => 'India',
        }
    },
    {
        location_id => 46272, # Toronto, Ontario, CA
        expected => {
            Location => 'Toronto, Ontario, Canada',
        }
    }

];

foreach ( @$location_tests ) {
    $template->token_value('location_id' => $_->{location_id});
    %fields = $template->search_fields();
    is($fields{Location}, $_->{expected}->{Location}, 'location freetext is ok');
}

### Ensure input mileage is within bounds. mySupply supports mileage radii 
### between 10 and 500 inclusively
my ($min_mileage, $max_mileage) = (10, 500);
my $legal_mileage               = int( ($max_mileage - $min_mileage) / 2 );
my $radius_tests = [
    { input => $min_mileage - 3,    expected => $min_mileage },
    { input => $min_mileage,        expected => $min_mileage },
    { input => $legal_mileage,      expected => $legal_mileage },
    { input => $max_mileage + 1500, expected => $max_mileage },
    { input => $max_mileage,        expected => $max_mileage},
];
$template->token_value( location_id => 5455 );  # Butler, Pennsylvania
for my $radius_test ( @{$radius_tests} ) {
    my ($input, $expected) = @{$radius_test}{ qw/input expected/ };
    $template->token_value( location_within => $input );
    %fields = $template->search_fields;
    ok( $fields{Radius} == $expected,
        "Mileage Radius input $input normalized to $expected" );
}

$template->token_value( location_within => 0 );
%fields = $template->search_fields;
is( $fields{Radius}, undef, 
    'radius field omitted for zero mile radius searches' );

SKIP: {
    my $response;

    unless( $ENV{ALIEN_TEST} ){
        skip 'No ALIEN_TEST in ENV. Skipping', 2;
    }

    # actual request with keywords and without location
    $template->token_value( 'location_id' => '' );
    $template->token_value( 'cb_mysupply_account_did' => 'A7F48Q74LM3HNDYT063' );
    $response = $template->search_submit();
    like($response->content, qr{Results}, 'search is successful without location');

    # actual request without either keywords or location
    $template->token_value( 'location' => '' );
    $template->token_value( 'keywords' => '' );
    $response = $template->search_submit();
    like($response->content, qr{Applications}, 'Search works just fine with no keywords and no location');
}

LWP::UserAgent::Mockable->finished();
done_testing();

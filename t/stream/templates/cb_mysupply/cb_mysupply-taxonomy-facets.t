use Test::Most;
use Test::MockModule;
use XML::LibXML;
# use Log::Any::Adapter qw/Stderr/;

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
};

use LWP;
use LWP::UserAgent::Mockable;

use_ok("Stream::Templates::cb_mysupply");

my $template_mock = Test::MockModule->new('Stream::Templates::cb_mysupply');

$template_mock->mock(before_scraping_results => sub {
    my ($self) = @_;

    my $s2 = $self->user_object()->stream2;
    my ( $parent_node ) = $self->findnodes('/Response/Data/SuggestedFilters');

    foreach my $element ( $parent_node->childNodes() ) {
        my $type = $element->nodeName() or next;
        next unless $type =~ m/\A(?:skillsv4|carotenetitlesv3|onet17)\z/i;

        $type =~ s/titles//i;

        foreach my $option ( $element->childNodes() ) {
            my $res = $s2->factory('Taxonomy')->dbic_rs()->find_or_new({
                field_type  => lc $type,
                taxonomy_id => uc $option->findvalue('./@Code'),
                label       => $type . ' fake label ' . int(rand(90)),
                lang        => 'en',
            });
            $res->insert() if !$res->in_storage();
        }
    }

    $self->_mysupply_parse_facets($parent_node);
});

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $users_factory = $stream2->factory('SearchUser');
my $user          = $users_factory->new_result({
    provider       => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id    => 1,
    group_identity => 'acme',
    contact_details => {
        contact_email => 'me@me.com'
    }
});

$user->ripple_settings()->{login_provider} = 'adcourier';

$user->save();
ok( my $template = $stream2->build_template('cb_mysupply', undef,
    {
        location_api => $stream2->location_api(),
        user_object  => $user,
        company => 'minons'
    } ) , "Ok got template"
);

# Test CustomFields facet building
my $facet_xml = <<'EOF';
<root>
    <CustomFields>
        <test_field label="Facet Label">
            <Name Name="Option Label" Code="option_id" >100</Name>
        </test_field>
        <empty_field label="Empty Field Label"></empty_field>
    </CustomFields>
    <Attributes>
        <campaign_interest label="CAMPAIGN_INTEREST">
            <Name Name="Attribute Option Label" Code="attribute_option_id">1</Name>
            <Name Name="Attribute Option Label 2" Code="attribute_option_id2">2</Name>
        </campaign_interest>
        <campaign_delivered label="CAMPAIGN_DELIVERED">
            <Name Name="Second Attribute Option Label" Code="second_attribute_option_id">3</Name>
        </campaign_delivered>
        <random_attribute label="Random">
            <Name Name="Wanted" Code="Relevant">123</Name>
        </random_attribute>
    </Attributes>
</root>
EOF

my $facet_doc = XML::LibXML->load_xml( string => $facet_xml );

my @custom_filters = map {
    $template->_build_custom_field_facet(
        $_,
        $template->_custom_field_prefix
    )
} $facet_doc->findnodes('/root/CustomFields/*')->get_nodelist;

my @attribute_filters = map {
    $template->_build_custom_field_facet(
        $_,

        $template->_attribute_field_prefix
    );
} $facet_doc->findnodes('/root/Attributes/*')->get_nodelist;

is_deeply(
    $custom_filters[0],
    {
        Name       => 'custom_field_test_field',
        Id         => 'cb_mysupplytest_field',
        Label      => 'Facet Label',
        Type       => 'multilist',
        Options    => [
            ['Option Label' => 'option_id', 100]
        ],
    },
    'CustomFields facet built'
);
is(scalar @custom_filters, 2, 'Kept empty CustomFields field');

is_deeply(
    \@attribute_filters,
    [
        {
            Name    => 'attribute_campaign_interest',
            Id      => 'cb_mysupplycampaign_interest',
            Label   => 'CAMPAIGN_INTEREST',
            Type    => 'multilist',
            Options => [
                ['Attribute Option Label' => 'attribute_option_id', 1],
                ['Attribute Option Label 2' => 'attribute_option_id2', 2]
            ],
        },
        {
            Name    => 'attribute_campaign_delivered',
            Id      => 'cb_mysupplycampaign_delivered',
            Label   => 'CAMPAIGN_DELIVERED',
            Type    => 'multilist',
            Options => [
                ['Second Attribute Option Label' => 'second_attribute_option_id', 3]
            ],
        },
        {
            Name    => 'attribute_random_attribute',
            Id      => 'cb_mysupplyrandom_attribute',
            Label   => 'Random',
            Type    => 'multilist',
            Options => [
                ['Wanted' => 'Relevant', 123]
            ],
        }
    ],
    'Attribute facets built'
);

#End of CustomFields facet building
my $order_labels = [
    'Talent Network',
    'Member Joined Language'
];

my $facet_list_to_order = [
    { Label => 'Three' },
    { Label => 'Four' },
    { Label => 'Member Joined Language' },
    { Label => 'Five' },
    { Label => 'Talent Network' },
    { Label => 'Six' }
];

#Should't reorder unless everything in $order is present
my $facet_list_not_to_order = [
    { Label => 'Three' },
    { Label => 'Four' },
    { Label => 'Member Joined Language' },
    { Label => 'Five' },
    { Label => 'Six' }
];

$template->_order_facets($facet_list_to_order, $order_labels);
$template->_order_facets($facet_list_not_to_order, $order_labels);

is_deeply(
    $facet_list_to_order,
    [
        { Label => 'Talent Network' },
        { Label => 'Member Joined Language' },
        { Label => 'Three' },
        { Label => 'Four' },
        { Label => 'Five' },
        { Label => 'Six' }
    ],
    'Facet list reordered'
);
is_deeply(
    $facet_list_not_to_order,
    [
        { Label => 'Three' },
        { Label => 'Four' },
        { Label => 'Member Joined Language' },
        { Label => 'Five' },
        { Label => 'Six' }
    ],
    'Facet lists order doesn\' change unless all items to order are present'
);

$template->token_value(cv_updated_within => 'ALL');
$template->token_value(keywords => 'manager');
$template->token_value(account_did => 'AKD3FF709ZM3TNTHVZV');

ok my $results = $template->search(), 'searched mysupply';

foreach my $facet ( @{$results->facets()} ) {
    if ( $facet->{Name} eq 'SkillsV4' ) {
        foreach my $option ( @{$facet->{Options}} ) {
            like $option->[0], qr/skillsv4 fake label/i, 'skill facet labels were retrieved';
        }
    }
    elsif ( $facet->{Name} eq 'CaroteneTitlesV3' ) {
        foreach my $option ( @{$facet->{Options}} ) {
            like $option->[0], qr/carotenev3 fake label/i, 'carotene facet labels were retrieved';
        }
    }
    elsif ( $facet->{Name} eq 'ONet17' ) {
        foreach my $option ( @{$facet->{Options}} ) {
            like $option->[0], qr/onet17 fake label/i, 'onet facet labels were retrieved';
        }
    }
}

END {
    # END block ensures cleanup if script dies early
    LWP::UserAgent::Mockable->finished;
};

done_testing();

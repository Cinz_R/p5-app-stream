#! perl -w

use Test::Most;
use Test::MockModule;

use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');

use Test::Stream2;
use Stream2::Action::DownloadProfile;
use Stream::Templates::BBCSS;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';

    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}

use LWP;
use LWP::UserAgent::Mockable;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'" . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ), "Ok can build stream2" );

$stream2->deploy_test_db;

my $user = $stream2->factory('SearchUser')->new_result(
    {   provider            => 'demo',
        last_login_provider => 'demo',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_name        => 'Andy Jones',
        contact_email       => 'andy@broadbean.com',
        settings            => {}
    }
);
$user->save();

my $candidate = Stream2::Results::Result->new(
    {   destination => 'adcresponses',
        name        => 'John Smith',
        candidate_id => '321',
        cv_url      => 'my_cv.pdf'
    }
);

{
    my $mock_bbcss = Test::MockModule->new( 'Stream::Templates::adcresponses' );
    $mock_bbcss->mock( 'download_cv' => sub{
        my $response = HTTP::Response->new();
        $response->content( q[<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Resume for Smithy</title>
<link rel="stylesheet" type="text/css" href="smithy">
<style>
                h2 { color: #0066aa !important; }
            </style>
</head>
<body><div class="page">
<div class="head line">
<div class="unit size3of4">
<h1>Smithy</h1>
</div>
</div></body>
</html>
]);
        $response->content_type( 'text/html' );
        return $response;
    });

    # Not interested in these
    $mock_bbcss->mock( 'update_candidate' => sub{
        1;
    });

    $mock_bbcss->mock( 'bbcss_customer' => sub{
        1;
    });

    ##

    my $download_profile = Stream2::Action::DownloadProfile->new(
        stream2          => $stream2,
        stream2_base_url => 'http://127.0.0.1/',
        user_object      => $user,
        ripple_settings  => $user->ripple_settings(),
        candidate_object => $candidate,
        candidate_idx    => 0,
        results_id       => 'abcd',
        jobid            => "Calling Smithy",
    );

    my $candidate_ref = $download_profile->instance_perform();

    ok ( ! $candidate_ref->{error}, "Ok, no error in candidate_ref" );
    like( $candidate_ref->{cv_html} , qr/Resume for Smithy/ , "Ok, returned correct info");
}

LWP::UserAgent::Mockable->finished();
done_testing();

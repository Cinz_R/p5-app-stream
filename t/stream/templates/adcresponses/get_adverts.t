#! perl -w

use Test::Most;
use Test::MockModule;

use Test::Stream2;
use Stream::Templates::adcresponses;

my $config_file = 't/app-stream2.conf';
my $stream2 = Test::Stream2->new( { config_file => $config_file } );

my $user = $stream2->factory('SearchUser')->new_result(
    {   provider            => 'demo',
        last_login_provider => 'demo',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_name        => 'Banana Rama',
        contact_email       => 'person@example.cpm',
        settings            => {}
    }
);
$user->save();

my $test_query = undef;
my $mock_api = Test::MockModule->new('Bean::AdvertService::Client');
my $mock_feed = Test::MockModule->new('Stream::Templates::adcresponses');

# Change these variables to modify the api mock
# How many ads in total to return.
my $ad_return_count = 0;
# Incremeneted with each request. Reset it accordingly
my $request_count = 0;
$mock_api->mock('search_adverts' => sub {
    my ($self, $query ) = @_;
    $request_count++;
    if($test_query) {
        is_deeply($query, $test_query, 'query_matches');
    }
    my $start = $query->{offset};
    my $end = $start + $query->{limit};
    $end = $ad_return_count if $end > $ad_return_count;

    my @adverts = map {
        {
            advert_id => 'id_' . $_,
            unwanted  => 'excessive',
            jobtitle  => 'Dog Walker',
            advert_id => 1234,
            jobref    => 4321,
            visible   => 'yes',
            time      => 149123987,
        }
    } ($start + 1..$end);
    return @adverts;
});

# Set by the mock caching method.
my ($key, $value, $time);
$mock_feed->mock( set_account_value => sub {
    my $self = shift;
    ($key, $value, $time) = @_;
});

my $feed = $stream2->build_template('adcresponses', {}, {user_object => $user});
my $ads = [];

$request_count = 0;
$ad_return_count = 123;
$ads = $feed->get_adverts();
is(scalar(@$ads), 123, 'Got 123 adverts');
is($request_count, 1, 'Make 1 request');
is_deeply(
    $ads->[11],
    {
        advert_id => 'id_12',
        jobtitle  => 'Dog Walker',
        advert_id => 1234,
        jobref    => 4321,
        visible   => 'yes',
        time      => 149123987,
    },
    'Stripped out unwanted hash values'
);

$request_count = 0;
$ad_return_count = 1200;
$ads = $feed->get_adverts();
is(scalar(@$ads), 1200, 'Got 1200 adverts');
is($request_count, 3, 'Made 3 requests');

$feed->get_adverts();

$request_count = 0;
$ad_return_count = 1814;
$feed->get_adverts();
my $first_request_count = $request_count;
ok($first_request_count, 'Made requests');


done_testing();
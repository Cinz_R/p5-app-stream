#! perl -w
use Test::Most;
use Test::MockModule;
use HTTP::Response;
use JSON ();

use Test::Stream2;

my $adverts_mock = Test::MockModule->new('Bean::AdvertService::Client');
$adverts_mock->mock(search_adverts => sub {
    return (
        { advert_id => 12345, jobref => 'HYPPO123', jobtitle => 'Hyppo Massager' },
        { advert_id => 38384, jobref => 'FALCO123', jobtitle => 'Falcon Trainer' },
        { advert_id => 39403, jobref => 'RHINO123', jobtitle => 'Rhino Trainer', is_requisition_ad => 1 },
    );
});

my $template_mock = Test::MockModule->new('Stream::Templates::adcresponses');
$template_mock->mock(adcresponses_custom_fields => sub {
    return {
        broadbean_adcadvert_id => {
            'Type' => 'advert',
            'Id' => 'adcresponses_broadbean_adcadvert_id',
            'SortMetric' => 20,
            'Label' => 'Advert',
            'Name' => 'broadbean_adcadvert_id'
        },
        broadbean_adcresponse_source_id => {
            'Type' => 'multilist',
            'Id' => 'broadbean_adcresponse_source_id',
            'SortMetric' => 20,
            'Label' => 'CV Origin',
            'Name' => 'broadbean_adcresponse_source_id'
        },
    };
});
$template_mock->mock(search_submit => sub {
    return HTTP::Response->new(
        200,
        'OK',
        undef,
        JSON::to_json({
            data => {
                facets => [
                    { name => "broadbean_adcadvert_id" , values => [{count => 1, name => 12345}, {count => 1, name => 38384}, {count => 1, name => 39403}] },
                    { name => "broadbean_adcresponse_source_id" , values => [{count => 5, name => 1}, {count => 5, name => 3}] }
                ],
                results => {
                    documents => [{ candidate_id => 12345, email => 'bob@careerbuilder.com', cv_text => 'works at careerbuilder', broadbean_adcboard_id => 4, broadbean_adcadvert_id => 3 }]
                },
                total_matching_records => 1,
            }
        })
    );
});

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->deploy_test_db, "Ok can run deploy" );

my $user_factory = $stream2->factory('SearchUser');
my $user = $user_factory->new_result({
    id => 91,
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 157656, # simonr@test.simonr.simonr
    group_identity => 'acme',
});
$user->save();

ok( my $template = $stream2->build_template('adcresponses', undef , { user_object => $user } ) , "Ok got template");

ok( my $results = $template->search(), 'retrieved mocked results');

foreach my $facet ( @{$results->facets} ) {
    if ( $facet->{Name} eq 'broadbean_adcadvert_id' ) {
        foreach my $option ( @{$facet->{Options}} ) {
            like( $option->[0], qr/123 - (Hyppo|Falcon|Rhino)/, 'facet label is a combination of jobref and jobtitle');
            like( $option->[1], qr/(12345|38384|39403)/, 'facet value is an advert_id');
            is( $option->[2], 1, 'facet count is read correctly from response');
            like( $option->[3], qr/(default|archived|general_submission)/, 'facet group is set correctly');
        }
    }
    elsif ( $facet->{Name} eq 'broadbean_adcresponse_source_id' ) {
        is($facet->{Label}, 'CV Origin', 'facet label is set to CV Origin - SEAR-1780');
        foreach my $option ( @{$facet->{Options}} ) {
            like( $option->[0], qr/Applications|Shortlisted from Search/, 'option labels are either applications or shortlisted from search');
            is( $option->[2], 5, 'facet count is read correctly from response');
        }
    }
}

done_testing();

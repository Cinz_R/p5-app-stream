use Test::Most;
use Test::MockModule;
use Stream::Templates::adcresponses;

my $bbcss_mock = Test::MockModule->new( 'Stream::Templates::BBCSS' );
my $candidate = Stream2::Results::Result->new({
    candidate_id => 12345,
    destination => 'adcresponses',
});

subtest 'Candidate already viewed' => sub {
    $candidate->attr('broadbean_adcresponse_is_read', 'True');

    my $download_called = 0;
    $bbcss_mock->mock( download_cv => sub {
        my ( $self, $c ) = @_;
        is( $c->candidate_id, $candidate->candidate_id );
        $download_called = 1;
    });

    $bbcss_mock->mock( update_candidate => sub {
        fail ( "I should not be called" );
    });

    Stream::Templates::adcresponses->download_cv( $candidate );

    ok( $download_called, "download called but update was not" );
};

subtest 'Candidate not already viewed' => sub {
    $candidate->attr('broadbean_adcresponse_is_read', 'False');

    my $download_called = 0;
    $bbcss_mock->mock( download_cv => sub {
        my ( $self, $c ) = @_;
        is( $c->candidate_id, $candidate->candidate_id );
        $download_called = 1;
    });

    my $update_called = 0;
    $bbcss_mock->mock( update_candidate => sub {
        my ( $self, $c, $fields ) = @_;
        $update_called = 1;
        is_deeply( $fields, { broadbean_adcresponse_is_read => 'True' } );
    });

    Stream::Templates::adcresponses->download_cv( $candidate );

    ok( $download_called, "download called" );
    ok( $update_called, "update called" );

};

done_testing();

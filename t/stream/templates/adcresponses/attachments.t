use Test::Most;
use Test::MockModule;
use Stream::Templates::adcresponses;

my $config_file = 't/app-stream2.conf';

## Having a stream2 will make sure that we have the historical templates in the path.
ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

my $candidate = Stream2::Results::Result->new({
    candidate_id => 12345,
    destination => 'adcresponses',
    broadbean_adcresponse_attachement_cv => 'cv',
    broadbean_adcresponse_attachments => [qw[cv one two]],
});


my $feed = Stream::Templates::adcresponses->new(
    {},
    { user_object => MyUser->new() }
);

my $renders = $feed->rendered_attachments($candidate);
is_deeply(
    $renders,
    [
        { filename => 'one.pdf', key => 'one', render_type => 'pdf' },
        { filename => 'two.pdf', key => 'two', render_type => 'pdf' },
    ],
    'Two renders, skipped CV, render as PDF'
);

ok(my $download = $feed->download_attachment($candidate, 'one'), 'Download attachmeny by key');
ok($download->isa('HTTP::Response'), 'Got HTTP::Response');
ok($download->filename, 'one.pdf');

my $downloads = $feed->download_attachments($candidate);
is( @$downloads, 2, 'Got 2 total attachments');
ok($downloads->[1]->isa('HTTP::Response'), 'Downloads are HTTP::Response obejcts');
is($downloads->[1]->filename, 'two.pdf', 'Downloads are HTTP::Response obejcts');

done_testing();

package MyProvider {
    sub new { bless {}, shift }
    sub download_attachment {
        my ($self, $user, $key) = @_;
        return {
            filename => $key . '.pdf',
            content => 'blah',
        };
    }

    1;
};

package MyUser {
    sub new { bless {}, shift }
    sub identity_provider {
        return MyProvider->new();
    }
    sub stream2 {
        return $stream2;
    }
    1;
}
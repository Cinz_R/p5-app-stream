#! perl -w

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Stream2;
use_ok "Stream::Templates::rengineering";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

ok( my $template = $stream2->build_template('rengineering', undef , { location_api => $stream2->location_api() } ) , "Ok got template");

my $locations = [
    {
        location_id =>  971, # High Point, NC
        expected_country_id => 5522,
        expected_city => 'High Point',
    },
    {
        location_id =>  40024, # SE England
        expected_country_id => '', # not mapped
        expected_city => 'South East England',
    },
    {
        location_id   =>  25488, # California, USA
        expected_country_id => 5522,
        expected_city => 'California',
    },
    {
        location_id   =>  39550, # London
        expected_country_id => 5521,
        expected_city => 'London',
    }
];

foreach (@$locations) {
    my ($country_id, $city) = $template->rengineering_location($_->{location_id});
    is($country_id, $_->{expected_country_id}, 'ID is as expected');
    is($city, $_->{expected_city}, 'got back city');
}

my $cv_updated = [
    {
        interval => 'ALL',
        expected => 36,
    },
    {
        interval => '3M',
        expected => 31,
    }
];

foreach ( @$cv_updated ) {
    is $template->rengineering_cv_updated($_->{interval}), $_->{expected}, 'date format for cv_updated_within is as expected';
}

my $jobtypes = [
    {
        jobtype => 'Permanent',
        expected => 50
    },
    {
        jobtype => 'Contract',
        expected => '',
    },
    {
        jobtype => 'Temporary',
        expected => 2001,
    }
];

foreach ( @$jobtypes ) {
    is $template->rengineering_jobtype($_->{jobtype}), $_->{expected};
}

LWP::UserAgent::Mockable->finished();
done_testing();

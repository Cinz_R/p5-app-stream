#! perl -w

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Stream2;

use_ok "Stream::Templates::rengineering";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

ok( my $t = $stream2->build_template('rengineering', undef , { location_api => $stream2->location_api() } ) , "Ok got template");

$t->recent_content(q{{"page":0,"totalResults":1,"resultsPerPage":10,"candidates":[{"Key Skills":"KVA experience.Emergency response to environmental spills and recovery of heavy haulage transport and product.","Post Code":null,"Phone Number":"+260966832036","Employer Name":"Camloc Enterprises Ltd","City/Town":"Lusaka","CV Name":"Kevin Coke Norris CV 05/07/2013","id":524040,"Preferred Location":5521,"Desired Salary":5750,"Email":"coke.norris at gmail.com","Upload Your CV":{"id":761359,"ownerId":524040,"filename":"CV - Kevin Coke Norris 130705 UK.doc","filesize":64512,"content_type":"application/msword","file":null,"textContent":null,"remotePath":null,"remoteBucket":null},"County/State":null,"Surname":"Coke Norris","Current or Last Job Title":"Managing Director/Engineer","Post Date":1373005933092,"Preferred Role Type":2001,"Country of Residence":5531,"Industry":[5298,5285,5293,5299,5295,5300,5289,5297,5292,5296,5283,5294,5288],"Dates for previous employment":"Jan 2012 - Present"}]}});

ok $t->scrape_page(), 'performed candidate scrape from json';

ok my $results = $t->results()->results, 'captured results';

my $candidate = $results->[0];

ok defined $candidate->candidate_id, 'candidate_id should always be set';
ok $candidate->attr('has_cv'), 'everybody has cv, until otherwise :)';

is $candidate->attr('resume_content_type'), 'application/msword', 'content type for cv is set';
is $candidate->attr('resume_filename'), 'CV - Kevin Coke Norris 130705 UK.doc', 'filename for cv is set';
is $candidate->attr('postcode'), undef, 'no postcode for this guy';
is $candidate->attr('employer'), 'Camloc Enterprises Ltd', 'employer is good';
is $candidate->attr('city'), 'Lusaka', 'city is good';
is $candidate->attr('name'), 'Kevin Coke Norris CV 05/07/2013', 'name is good';

# these are ID fields, not likely to be displayed
#is $candidate->attr('preferred_loc'), 5521, 'preferred loc is an ID (a label would be nice)';
#is $candidate->attr('desired_salary'), 5750, 'desired salary is an ID (a label would be nice)';
#is $candidate->attr('preferred_role'), 2001, 'pref role is an ID (a label would be nice)';
#is $candidate->attr('country_of_residence'), 5531, 'country is ID (a label would be nice)';

is $candidate->attr('email'), 'coke.norris at gmail.com', 'email is good';
is $candidate->attr('county'), undef, 'county is not defined';
is $candidate->attr('prev_jobtitle'), 'Managing Director/Engineer', 'prev jobtitle is good';
is $candidate->attr('date_last_employed'), 'Jan 2012 - Present', 'date last employed is good';


like $candidate->attr('phone'), qr/[\d\+\s]+/, 'phone is good';
like $candidate->attr('key_skills'), qr/KVA experience/,'key skills captured';
foreach ( @{$candidate->attr('industries')} ) {
    unlike $_, qr/\d+/, 'industry IDs should be converted to their labels';
}

$t->token_value('email' => 'broadbean@reng.com');
$t->token_value('password' => 'Broadbean_testing');

ok $candidate = $t->download_profile($candidate), "downloaded profile for candidate $candidate->candidate_id";
ok defined $candidate->attr('cv_html'), 'new field cv_html should be available with the candidates HTML CV';

LWP::UserAgent::Mockable->finished();
done_testing();

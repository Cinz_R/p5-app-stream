#! perl -w

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Stream2;
use Test::MockModule;

use_ok "Stream::Templates::rengineering";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $mock = new Test::MockModule('Stream::Templates::rengineering');
$mock->mock(default_url => sub { return 'http://httpbin.org' });
$mock->mock(search_url => sub { return 'http://httpbin.org/get' });

ok( my $t = $stream2->build_template('rengineering', undef , { location_api => $stream2->location_api() } ) , "Ok got template");

is $t->default_url(), 'http://httpbin.org', 'default_url is one for testing to play nice';

ok $t->token_value(email => 'broadbean@reng.com'), 'set auth token "email"';
ok $t->token_value(password => 'Broadbean_testing'), 'set auth token "password"';

my $criteria = [
    {
        keywords => 'Project Manager',
        cv_updated_within => '3D',
        categories => [5297, 5283],
        default_jobtype => 'Permanent',
        willing_to_relocate => '5270',
        location_id => 25735, # Los Ang., CA
    },
    {
        keywords => '("electrical design" or "electrical designer" or "Electrical Design engineer") and ("consultancy" or "building services")',
        cv_updated_within => '3M',
        categories => [5293],
        default_jobtype => 'Contract',
        willing_to_relocate => '5271',
        location_id => 39550, # London
    },
    {
        keywords => 'kitten cuddler',
        cv_updated_within => '3M',
        categories => [5283],
        default_jobtype => 'Contract',
        willing_to_relocate => '5271',
        location_id => 43409, # Chennai, In.
    },

];

foreach my $tokens ( @$criteria ) {
    ok $t->token_value($_ => $tokens->{$_}), "set key for $_" for keys %$tokens;
    ok my %fields = $t->search_fields(), 'grabbed search fields';
    is $fields{email}, 'broadbean@reng.com', 'got right auth token email';
    is $fields{password}, 'Broadbean_testing', 'got right auth token email';
    is $fields{keyword}, $tokens->{keywords}, "keyword field has value that was inputted";
    is $fields{date_posted}, $t->rengineering_cv_updated($tokens->{cv_updated_within}), "date_posted field has value that was inputted";
    is $fields{category}, join(',', @{$tokens->{categories}}), "category field has value that was inputted";
    is $fields{job_type}, $t->rengineering_jobtype($tokens->{default_jobtype}), "jobtype field has expected value";
    is $fields{willing_to_relocate}, $tokens->{willing_to_relocate}, "will relocate field has expected value";
    my ($country, $city) = $t->rengineering_location($tokens->{location_id});
    is $fields{country}, $country, 'country field is ok';
    is $fields{city}, $city, 'city field is ok';
}

$t->recent_content(q{{"page":0,"totalResults":2,"resultsPerPage":10,"candidates":[{"Key Skills":"KVA experience.Emergency response to environmental spills and recovery of heavy haulage transport and product.","Post Code":null,"Phone Number":"+260966832036","Employer Name":"Camloc Enterprises Ltd","City/Town":"Lusaka","CV Name":"Kevin Coke Norris CV 05/07/2013","id":524040,"Preferred Location":5521,"Desired Salary":5750,"Email":"coke.norris at gmail.com","Upload Your CV":{"id":761359,"ownerId":524040,"filename":"CV - Kevin Coke Norris 130705 UK.doc","filesize":64512,"content_type":"application/msword","file":null,"textContent":null,"remotePath":null,"remoteBucket":null},"County/State":null,"Surname":"Coke Norris","Current or Last Job Title":"Managing Director/Engineer","Post Date":1373005933092,"Preferred Role Type":2001,"Country of Residence":5531,"Industry":[5298,5285,5293,5299,5295,5300,5289,5297,5292,5296,5283,5294,5288],"Dates for previous employment":"Jan 2012 - Present"},{"Key Skills":"Leadership & Supervisory Skills Excellent Interpersonal & Cross Cultural Communication Research & Analytical Skills Ability to work under pressure – to meet deadlines Logical approach to solving problems Creative, innovative & Team-player Tact,","Post Code":null,"Phone Number":"+260 212 223044","Employer Name":"Copperstone University","City/Town":"Kitwe","CV Name":"Likando MULIOKELA","id":174804,"Preferred Location":5327,"Desired Salary":5750,"Email":"likmulyo52 at gmail.com","Upload Your CV":{"id":176080,"ownerId":174804,"filename":"A- Curriculum Vitae - 2012 Jan.doc","filesize":64000,"content_type":"application/msword","file":"attachments/resume/174/804/176080","textContent":null,"remotePath":null,"remoteBucket":null},"County/State":null,"Surname":"MULIOKELA","Current or Last Job Title":"Head, Communication & Media Studies Department","Post Date":1341567968114,"Preferred Role Type":50,"Country of Residence":5531,"Industry":[5284,5283],"Dates for previous employment":"1/10/10 to Date"}]}});

ok $t->scrape_page(), 'performed candidate scrape from json';
ok $t->scrape_number_of_results(), 'scraped for number of results';
is $t->scrape_paginator(), undef, 'scrape_paginator returns;';

is $t->max_pages(), 1,'got one page of results for 10 results per page';
is $t->total_results(), 2,'reading totalResults is correct';

LWP::UserAgent::Mockable->finished();
done_testing();

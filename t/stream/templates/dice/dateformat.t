use Test::Most;
use Stream::Templates::dice;

my $feed = Stream::Templates::dice->new();

ok($feed, 'Created feed');

my %date_tests = (
    'invalid format'         => 'invalid format',
    '2016-10-06T00:00:00Z'   => '06 Oct 2016',
    '2000-11-01T00:00:00Z'   => '01 Nov 2000',
    '2000-02-01T00:00:00Z'   => '01 Feb 2000',
    '1995-09-01T00:00:00Z'   => '01 Sep 1995',
    '2016-12-22T18:11:22.0Z' => '22 Dec 2016',
    '2016-12-22T18:11:22.0Z' => '22 Dec 2016',
    '2016-04-02T05:00:00.0Z' => '02 Apr 2016',
);

my %time_tests = (
    'invalid format'         => 'invalid format',
    '2016-10-06T00:00:00Z'   => '00:00 06 Oct 2016',
    '2000-11-01T00:00:00Z'   => '00:00 01 Nov 2000',
    '2000-02-01T00:00:00Z'   => '00:00 01 Feb 2000',
    '1995-09-01T00:00:00Z'   => '00:00 01 Sep 1995',
    '2016-12-22T18:11:22.0Z' => '18:11 22 Dec 2016',
    '2016-12-22T18:11:22.0Z' => '18:11 22 Dec 2016',
    '2016-04-02T05:00:00.0Z' => '05:00 02 Apr 2016',
);

while( my($input, $expected_output) = each %date_tests) {
    my $output = $feed->format_dice_datetime($input);
    is($output, $expected_output, 'Correctly formatted date ' . $input);
}

while( my($input, $expected_output) = each %time_tests) {
    my $output = $feed->format_dice_datetime($input, 1);
    is($output, $expected_output, 'Correctly formatted date and time' . $input);
}
done_testing();

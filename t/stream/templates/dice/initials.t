use Test::Most;
use Stream::Templates::dice;

my $feed = Stream::Templates::dice->new();

ok($feed, 'Created feed');

my %test_names = (
    'John Smith' => ['J', 'S'],
    'John Bernard Smithson' => ['J', 'S'],
    '' => [],
);

while(my($name, $expected_initials) = each %test_names) {
    my $output_initials = $feed->name_initials($name);
    is_deeply(
        $output_initials,
        $expected_initials,
        'Got correct initials for ' . $name
    );
}

done_testing();
use Test::Most;
use Stream::Templates::dice;

my $feed = Stream::Templates::dice->new();

ok($feed, 'Created feed');

my @tests = (
    {
        args => [],
        expected_output => undef,
        description => 'Empty',
    },
    {
        args => [100, ''],
        expected_output => '[100 to 100000000]',
        description => '100 to empty',
    },
    {
        args => [100, 0],
        expected_output => '[0 to 100]',
        description => '100 to 0',
    },
    {
        args => ['', 100],
        expected_output => '[0 to 100]',
        description => 'empty to 100',
    },
    {
        args => [0, 100],
        expected_output => '[0 to 100]',
        description => '0 to 100',
    },
);

foreach my $test (@tests) {
    my $output = $feed->map_salary(@{$test->{args}});
    is(
        $output,
        $test->{expected_output},
        'Mapped salary: ' . $test->{description}
    );
}

done_testing();
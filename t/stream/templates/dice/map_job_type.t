use Test::Most;
use Stream::Templates::dice;

my $feed = Stream::Templates::dice->new();

ok($feed, 'Created feed');

my %tests = (
    'invalid'       => ['invalid'],
    'FULLTIME'      => ['Full-Time'],
    'PARTTIME'      => ['Part-Time'],
    'CON_HIRE'      => ['Contract to Hire'],
    'CON_HIRE_CORP' => ['Contract to Hire-Corp-to-Corp'],
    'CON_HIRE_IND'  => ['Contract to Hire-Independent'],
    'CON_HIRE_W2'   => ['Contract to Hire-W2'],
    'CON_CORP'      => ['Contract-Corp-to-Corp'],
    'CON_IND'       => ['Contract-Independent'],
    'CON_W2'        => ['Contract-W2'],
    'CON'           => ['Contract'],
    'CON,CON_W2'    => ['Contract', 'Contract-W2'],
    'CON,invalid'   => ['Contract', 'invalid'],
    'FULLTIME,PARTTIME,CON' => ['Full-Time', 'Part-Time', 'Contract'],
);

while( my($input, $expected_output) = each %tests) {
    my $output = $feed->map_dice_job_type($input);
    is_deeply($output, $expected_output, 'Correctly map job type ' . $input);
}

done_testing();
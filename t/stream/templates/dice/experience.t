use Test::Most;
use Stream::Templates::dice;

my $feed = Stream::Templates::dice->new();

ok($feed, 'Created feed');

my @tests = (
    [ [undef, undef] => '', 'Double undef'],
    [ [2, undef]     => '[2 to 100]', 'int and undef'],
    [ [undef, 2]     => '[0 to 2]', 'undef and int'],
    [ [2, 4]         => '[2 to 4]', 'Valid values'],
    [ [4, 2]         => '[2 to 4]', 'Opposite order'],
    [ ['foo', 'bar'] => '', 'Strings' ],
    [ ['foo', 4]     => '[0 to 4]', 'String and integer']
);

foreach my $test (@tests) {
    my ($args, $expected_output, $desc) = @$test;
    my $output = $feed->map_experience(@$args);
    is(
        $output,
        $expected_output,
        'Mapped experience: ' . $desc
    );
}

done_testing();
use Test::Most;
use Stream2;
use Test::MockModule;
use HTTP::Response;

use_ok "Stream::Templates::bullhorn";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $users_factory = $stream2->factory('SearchUser');

my $user = $users_factory->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 21024, # This is a bullhorn adcourier test user.
    group_identity => 'acme',
});
$user->save();

ok( my $template = $stream2->build_template('bullhorn', undef , { user_object => $user } ) , "Ok got template");
# the rest URL needs to be set to avoid a warning.
$template->{rest_url} = 'moo';
# Private Label fetching
{
    my $module = new Test::MockModule('Stream::Templates::bullhorn');
    $module->mock('get', sub { return HTTP::Response->new(200, "OK", ["Content-Type", "application/json"], '{
        "privateLabelId" : {
            "id" : 3940,
            "name" : "C and M Recruitment Ltd."
        }
    }'); });

    ok( $template->bullhorn_private_label_id(), 'called for private label');
    is( $template->get_account_value('private_label_id'), '3940', 'stored label in acct value' );
}

done_testing;

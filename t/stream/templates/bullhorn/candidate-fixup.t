#!/usr/bin/env perl
use Test::Most;
use Stream::Templates::bullhorn;
use Stream2::Results::Result;

# bullhornStaffingHost and the Candidates cand_id need to be set.

my $candidate = Stream2::Results::Result->new({'cand_id'=>1});
my $template = Stream::Templates::bullhorn->new();
$template->{bullhornStaffingHost} = 'foo';
ok( $template->scrape_fixup_candidate( $candidate ), "process runs" );
is( $candidate->attr('block_bulk_message'), 1, "bullhorn candidates block bulk messaging" );

done_testing();

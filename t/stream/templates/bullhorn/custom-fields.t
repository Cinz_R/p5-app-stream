use Test::Most;
use Stream2;

use_ok "Stream::Templates::bullhorn";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $users_factory = $stream2->factory('SearchUser');

my $user = $users_factory->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 21024, # This is a bullhorn adcourier test user.
    group_identity => 'acme',
});
$user->save();

ok( my $template = $stream2->build_template('bullhorn', undef , { user_object => $user } ) , "Ok got template");

ok($template->_bullhorn_build_facets(
    (
	    {
		    name	    =>	'customTextBlock4',
    		label	    =>	'CustomTextBlock4',
	    	dataType	=>	'String',
	    	omdays      =>  'i need to get married',
	    	maxLength   =>  239024092834,
	    },
	    {
		    name	    =>	'desiredLocations',
    		label	    =>	'Desired Locations',
	    	dataType	=>	'String',
	    	options     =>  [   {
	    	                        'value' =>  'Local',
	    	                        'label' =>  'Local',
	    	                    },
	    	                    {
	    	                        'value' =>  'Regional',
	    	                        'label' =>  'Regional',
	    	                    }
	    	                ],
	    },
	    {
            name        =>  'dateLastComment',
            label       =>  'Date Last Comment',
            dataType    =>  'Timestamp',
	    },
	    {
	        name        =>  'dayRate',
            label       =>  'Day Rate',
            dataType    =>  'BigDecimal',
	    },
        {
            name        =>  'sausages',
            label       =>  'Big Sausages',
            dataType    =>  'BigDecimal',
            hideFromSearch => 1
        },
        {
            name        =>  'confidential_sausage',
            label       =>  'Confidential Sausage',
            dataType    =>  'BigDecimal',
            confidential => 1
        },
        {
            name        =>  'readonly_porridge',
            label       =>  'Readonly Porridge',
            dataType    =>  'BigDecimal',
            readOnly    => 1
        },
        {
            name => 'localAddtionalWitholdingsAmount',
            label => 'Local tax amount',
            dataType => 'BigDecimal'
        },
        {
            name => 'federalAddtionalWitholdingsAmount',
            label => 'Federal tax amount',
            dataType => 'BigDecimal'
        },
        {
            name => 'stateAddtionalWitholdingsAmount',
            label => 'State tax amount',
            dataType => 'BigDecimal'
        }
    )
), 'did it');

my $facets = $template->account_value('client_facets');
ok(scalar @$facets > 0, 'we have facets');

is ( scalar( grep { $_->{Name} =~ m/AdditionalWithholdingsAmount/ } @$facets ), 0, "tax fields will be skipped" );
is ( scalar( grep { $_->{Name} eq 'sausages' } @$facets ), 0, "facets with hide from search on will be skipped" );
is ( scalar( grep { $_->{Name} eq 'confidential_sausage' } @$facets ), 0, "facets with confidential on will be skipped" );
is ( scalar( grep { $_->{Name} eq 'readonly_porridge' } @$facets ), 0, "facets with readOnly on will be skipped" );

foreach my $facet ( @$facets ) {
    ok($facet->{'Name'}, 'i have a name');
    ok($facet->{'Label'}, 'i have a label');
    ok($facet->{'Type'}, 'i have a type');
    ok($facet->{'SortMetric'}, 'i have a sortmetric');
    #Let's test some fields
    if ( $facet->{'Name'} eq 'desiredLocations' ) { #lists
        ok($facet->{'Options'}, 'i have options');
        is($facet->{'Type'}, 'list', 'type is list');
        is($facet->{'Options'}->[2]->[0], 'Regional', '3rd option for desiredLocations is Regional');
    }
    if ( $facet->{'Name'} eq 'dateLastComment' ) { #timestamps
        ok($facet->{'Options'}, 'i have options');
        is($facet->{'Type'}, 'list', 'type is list');
        like($facet->{'Options'}->[2]->[1], qr{\[\d+\sTO\s\d+\]}, '3rd option for dateLastComment is a date range');
    }
    if ( $facet->{'Name'} eq 'customTextBlock4' ) { #strings
        ok(!$facet->{'Options'}, 'i dont have options');
        is($facet->{'Type'}, 'text', 'type is text');
        is($facet->{'Size'}, 239024092834, 'size is big');
    }
    isnt($facet->{'Name'}, 'dayRate', 'we ignore some fields');
    isnt($facet->{'Label'}, 'Num Owners', 'some unwanted labels are skipped');
}


done_testing;

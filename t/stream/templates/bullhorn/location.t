#! perl -w
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Stream2;

use Test::MockModule;

use_ok "Stream::Templates::bullhorn";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $users_factory = $stream2->factory('SearchUser');

my $user = $users_factory->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 21024, # This is a bullhorn adcourier test user.
    group_identity => 'acme',
});
$user->save();

my $geocoder_mock = Test::MockModule->new('Geo::Coder::Google::V3');
$geocoder_mock->mock(geocode => sub {
    return {
        geometry => {
            location    => { lat => '40.3231123846154', lng => '-40.3231123846154' },
            bounds      => {
                southwest   => { lat => '40.3231123846154', lng => '-40.3231123846154' },
                northeast   => { lat => '40.3231123846154', lng => '-40.3231123846154' },
            },
        }
    };
});

ok( my $template = $stream2->build_template('bullhorn', undef , { user_object => $user, location_api => $stream2->location_api() } ) , "Ok got template");

my $tests = [
    {
        location_id => 971, # High Point, NC - Represent!
        miles  => 50,
        expected => ' AND (address.latitude:[39.598474703456 TO 41.0477500657748] AND address.longitude:[\-41.0477500657748 TO \-39.598474703456])',
        desc => 'expecting lat/long lucene string for High Point, NC',
    },
    {
        location_id => 13542, # Phoenix, Arizona
        miles  => '',
        expected => ' AND address:((((address.city:"Phoenix" AND (address.state:"AZ" OR address.state:"Arizona")) OR address.zip:"85096")))',
        desc => 'expecting state/city/zip lucene string for Phoenix, Arizona with no radius',
    },
    {
        location_id => 39493, # England
        miles  => 5,
        expected => ' AND (address.latitude:[40.2506486164995 TO 40.3955761527313] AND address.longitude:[\-40.3955761527313 TO \-40.2506486164995])',
        desc => 'expecting lat/long lucene string for England',
    },
    {
        location_id => 25488, # California, USA
        miles  => '',
        expected => ' AND address:(((address.state:"CA" OR address.state:"California") AND address.country.name:"United States"))',
        desc => 'expecting state/country lucene string for California, USA',
    },
    {
        location_id => '',
        miles  => 10,
        expected => undef,
        desc => 'expecting nothing',
    },
];

foreach ( @{$tests} ) {
    $template->token_value('location_within_miles' => $_->{miles});
    is($template->bullhorn_location($_->{location_id}), $_->{expected}, $_->{desc} );
}

LWP::UserAgent::Mockable->finished();
done_testing();

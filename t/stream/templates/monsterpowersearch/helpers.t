#! perl -w
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Stream2;

use_ok "Stream::Templates::MonsterPowerSearchBase";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

ok( my $template = $stream2->build_template('MonsterPowerSearchBase', undef , { location_api => $stream2->location_api() } ) , "Ok got template");

my $locations = [
    {
        location_id =>  971, # High Point, NC
        miles  =>  8,
        expected => 'High Point, NC-8',
    },
    {
        location_id   =>  25488, # California, USA
        miles  =>  100,
        expected => 'California-100',
    },
    {
        location_id => 39550, # London, UK
        miles => 10,
        expected => 'London-10',
    },
    {
        location_id => 58501, # Vila Andrade, BR - sub-place
        miles => 0,
        expected => 'Vila Andrade',
    },
    {
        location_id => 13878, # Minnesota, USA - state
        miles => 10,
        expected => 'Minnesota-10',
    },
];

foreach (@$locations) {
    my $location_sent = $template->monster_location($_->{location_id}, $_->{miles});
    is($location_sent, $_->{expected}, 'Location is sent as expected');
}

LWP::UserAgent::Mockable->finished();
done_testing();

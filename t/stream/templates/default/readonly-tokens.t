#!/usr/bin/env perl

use Test::Most tests => 2;

{
  package Stream::Templates::ReadOnlyTokens;

  use base qw(Stream::Templates::Default);
  use vars qw(%standard_tokens);

  $standard_tokens{first_token} = {
      Label  => 'Number of Results',
      Type   => 'List',
      Values => [ 10, 20, 30 ],
  };
};

my ($first_token) = fetch_token('first_token');
$first_token->{Edited} = 1;
push @{$first_token->{Options}}, 40;

my ($check_token) = fetch_token('first_token');
ok(!$check_token->{Edited}, "You can't add new properties to a token");
is(scalar(@{$check_token->{Options}}), 3, "You can't add new options to a list token");

sub fetch_token {
  my $token_name = shift;

  my $tokens = Stream::Templates::ReadOnlyTokens->standard_tokens();
  return $tokens->{$token_name};
}

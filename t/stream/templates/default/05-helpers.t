#!/usr/bin/env perl

use strict;

use Test::More;

# use Carp::Always;

plan tests => 11;

# 1 test
use_ok('Stream::Templates::RobotMock')
    or BAIL_OUT('Stream::Templates::RobotMock does not compile');

# 10 tests
{
    my $feed = Stream::Templates::RobotMock->new();

    $feed->token_value('location', 'Canary Wharf');
    $feed->token_value('salary_from', 50000);

    my $ok = eval { $feed->process_posting_only_tokens(); 1 };

    ok( $ok, 'Posting only tokens ran' )
       or diag( "error running posting only tokens: $@" );

    is( $feed->token_value('location_copy'), 'Canary Wharf' );
    is( $feed->token_value('test_1'), 1+1 );
    is( $feed->token_value('test_2'), 2+1 );
    is( $feed->token_value('test_3'), 2+3 );
    is( $feed->token_value('test_4'), 23  );

    is_deeply( [ $feed->token_value( 'array' ) ], [ qw/ XXX XXX XXX / ] );
    unlike( $feed->token_value('array_test_1'), qr/ARRAY/ );
    is( $feed->token_value('array_test_1'), 'XXX,XXX,XXX' );
    $feed->SALARY_CHANGE_FREQUENCY('salary_from', 'annum', 'hour', 1);
    is( $feed->token_value('salary_from'), 24, 'salary frequency helper converted salary from annum to hour');
};

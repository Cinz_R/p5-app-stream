#!/usr/bin/env perl

use strict;

use Test::More;
use Stream::TemplateLoader;

use Stream2;

# Note: you HAVE to build a Stream2 in order to fix INC.

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");



# bug: if the dev_monster template was loaded after the Monsterxml->list_tokens method
# was called, the board would be the parent template, not the current template

my $tests_run = 0;

{
    my @tokens = Stream::TemplateLoader->list_tokens('monsterxml');

    $tests_run += scalar(@tokens);

    foreach my $token_ref ( @tokens ) {
        is( $token_ref->{Board}, 'monsterxml' );
    }
};

{
    my @tokens = Stream::TemplateLoader->list_tokens('monster_germany');

    $tests_run += scalar(@tokens);

    foreach my $token_ref ( @tokens ) {
        # should not be 'monsterxml'
        is( $token_ref->{Board}, 'monster_germany' );
    }
};

done_testing();

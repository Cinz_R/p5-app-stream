#!/usr/bin/env perl

use strict;

use Test::More tests => 10;

use_ok('Stream::Templates::Default')
    or BAIL_OUT('Stream::Engine does not compile');

{
    my $engine = Stream::Templates::Default->new();

    $engine->token_value('token_1' => 'value_1');
    $engine->token_value('token_2' => 'value_2');
    $engine->token_value('array_1' => 'a1_value_1','a1_value_2');
    $engine->token_value('array_2' => 'a2_value_1','a2_value_2','a2_value_3');

    # basics
    is( $engine->fill_in_tokens('no token'), 'no token', 'no token' );
    is( $engine->fill_in_tokens('%token_1%'), 'value_1', 'single token' );
    is( $engine->fill_in_tokens('%token_2%'), 'value_2', 'single token' );
    is_deeply(
        [ $engine->fill_in_tokens('%array_1%') ],
        ['a1_value_1','a1_value_2'],
        'array test',
    );

    # special cases
    is( $engine->fill_in_tokens('%token'), '%token', 'half token' );
    is( $engine->fill_in_tokens('%token_1%%token_2%'), 'value_1value_2', 'two tokens' );
    is( $engine->fill_in_tokens('%token_1%|%token_2%'),'value_1|value_2', 'two tokens with separator' );
    is( $engine->fill_in_tokens('%token_1%%missing_token%'), 'value_1', 'missing token' );

    # weird cases
    is( $engine->fill_in_tokens('%token_1%token_2%'), 'value_1token_2%', 'token + half token');
};

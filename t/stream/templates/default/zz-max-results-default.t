#!/usr/bin/env perl

use strict;

use Test::More tests => 3;

{
    package Stream::Templates::Tests::MaxResults;

    use base qw(Stream::Engine::API::XML);

    our %standard_tokens = (
        max_results => {
            Label => 'Number of Results',
            Type  => 'List',
            Options => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
        },
    );
};

{
    my $tokens_ref = Stream::Templates::Tests::MaxResults->standard_tokens();
    ok( $tokens_ref );

    my $max_results_token_ref = $tokens_ref->{max_results};

    ok( $max_results_token_ref );

    diag( 'Monster will return '.$max_results_token_ref->{Default}. ' results by default' );
    ok( $max_results_token_ref->{Default} );
};

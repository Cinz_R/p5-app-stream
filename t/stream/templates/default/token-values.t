use Test::Most;

{
  package Stream::Templates::example;
  use base 'Stream::Templates::Default';
};

subtest "Empty feed" => sub {
  my $feed = Stream::Templates::example->new();

  ok( !$feed->token_values );
  ok( !$feed->token_values('not_set_yet') );
  ok( !$feed->token_values('multiple','missing','tokens') );
};

subtest "Feed with tokens" => sub {
  my $feed = Stream::Templates::example->new();
  $feed->token_value(foo => 'bar');
  $feed->token_value(bar => 'baz');
  $feed->token_value(example_baz => [qw/foo bar/]);

  ok( !$feed->token_values );
  is( $feed->token_values('foo'), 'bar' );
  is_deeply( [$feed->token_values(qw/foo bar/)], [qw/bar baz/] );
  is_deeply( [$feed->token_values(qw/foo baz/)], [qw/bar foo bar/] );
  is_deeply( [$feed->token_values('example_baz')], [qw/foo bar/] );
};

done_testing();

#!/usr/bin/env perl

use strict;

use Test::More tests => 3;

# bug: if one template inherited tokens from another, any changes to the tokens
# in the child template would affect the parent template

# we should keep the tokens separate instead
{
  package BaseFeed;

  use base qw(Stream::Engine::API::XML);

  use vars qw(%standard_tokens);

  %standard_tokens = (
    twlid => {
      Label => 'Target Work Location',
      Type => 'MultiList',
      Options => '(PARENT FEED=1)',
    },
  );
};

{
   package ChildFeed;

   use base qw(BaseFeed);

   use vars qw(%standard_tokens);

   __PACKAGE__->inherit_tokens();

   $standard_tokens{twlid}->{Options} = '(CHILD FEED=2)';
};

my $base_token = do {
    no strict 'refs';
    ${"BaseFeed::standard_tokens"}{"twlid"};
};
my $child_token = do {
    no strict 'refs';
    ${"ChildFeed::standard_tokens"}{"twlid"};
};

isnt( $base_token->{'Options'}, $child_token->{'Options'}, "The base and child have separate tokens rather than sharing them" );

like( $base_token->{'Options'},  qr/PARENT FEED/, "The parent feed has the parent token" );
like( $child_token->{'Options'}, qr/CHILD FEED/,  "The child feed has the child token" );

use Test::Most;
use Stream2;
use Stream2::Results::Result;

use_ok 'Stream::Templates::Default';

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new( { config_file => $config_file } ),
    "Ok can build stream2" );
$stream2->stream_schema->deploy();

my $user_factory = $stream2->factory('SearchUser');
my $user         = $user_factory->new_result(
    {
        id                  => 91,
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 157656,
        group_identity      => 'acme',
    }
);
$user->save();

my $candidate = Stream2::Results::Result->new(
    {
        candidate_id => '123455',
        email        => 'troy@smerkel.com',
    }
);

ok(
    my $template =
      $stream2->build_template( 'Default', undef, { user_object => $user } ),
    "Ok got template"
);


my @tests = (
    [ 'N', 'E', 'P',  'Please contact your account administrator, N at E or on P.'],
    [ 'N', 'E',  0 ,  'Please contact your account administrator, N at E.'],
    [ 'N',  0 , 'P',  'Please contact your account administrator, N on P.'],
    [ 'N',  0 ,  0 ,  'Please contact your account administrator, N.'],
    [ 0  , 'E', 'P',  'Please contact your account administrator, at E or on P.'],
    [ 0  , 'E',  0 ,  'Please contact your account administrator, at E.'],
    [ 0  ,  0 , 'P',  'Please contact your account administrator, on P.'],
    [ 0  ,  0 ,  0 ,  'Please contact your account administrator.' ],
);

for my $test (@tests) {

    $template->user_object->contact_details(
        {
            admin_name  => $test->[0],
            admin_email => $test->[1],
            admin_phone => $test->[2],
        }
    );

    # why a eval in a dies_ok?
    # it is so we can rethrow the error but with the message, so we can test it.
    throws_ok sub {
        eval { $template->_throw_quota_exceeded_error('foo'); };
        if ($@) {
            die $@->message;
        }
    }, qr/$test->[3]/;

}

done_testing();

#!/usr/bin/env perl

use strict;

use FindBin;

use Test::More;

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}

use LWP;
use LWP::UserAgent::Mockable;

# We need an instance of Stream2 so the translated error
# message returned by the gojobsite template would work.
my $config_file = 't/app-stream2.conf';
ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");


use Stream::Templates::gojobsite;

{


    my $feed = Stream::Templates::gojobsite->new();

    my $results = eval {
        $feed->search();
    };

    my $error_msg = $@;

    if ( $error_msg ) {
        diag( "Jobsite returned an error: $error_msg" );
    }

    ok( !$results );
    ok( $error_msg->isa('Stream::EngineException::LoginError') , "OK error message is from the good class");
    is( $error_msg->human_message() , "Login Error: Missing Agency ID and Email address - fill in your agency id and email address and try again", "OK Good human message");
    is( $error_msg->error_calendar_id() , 'LOGIN ERROR' , "Ok good error calendar version");
};


LWP::UserAgent::Mockable->finished();
done_testing();

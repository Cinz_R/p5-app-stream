#!/usr/bin/env perl

use strict;

use Test::More tests => 12;

use lib qw();

use_ok('Stream::Templates::RobotMock')
    or BAIL_OUT('Stream::Templates::RobotMock does not compile');

=pod

Expected behaviour

+-----+-------------------------+-----------------------------+
|     |  Global token           | Template specific token     |
+-----+-------------------------+-----------------------------+
|SET  |  sets global token only | sets template & global token|
+-----+-------------------------+-----------------------------+
|GET  |  returns template first | only returns template token |
|     |  or global token        |                             |
+-----+-------------------------+-----------------------------+

=cut


foreach my $token_prefix ( qw/token_1 default_jobtype/ ) {
    diag("testing token <$token_prefix>");
    my $template = Stream::Templates::RobotMock->new();

    # bug fix: ensure the template can identify itself
    ok( $template->destination() );

    # test global tokens
    my $global_token = $token_prefix;
    my $feed_token   = $template->destination().'_'. $token_prefix;

    my $global_value = 'global_value';
    my $feed_value   = 'feed_value';

    $template->set_token_value( $global_token => $global_value );
    is( $template->get_token_value( $global_token ), $global_value );

    # template specific token - does not inherit
    is( $template->get_token_value( $feed_token ), undef );

    # set the template specific token
    $template->set_token_value( $feed_token => $feed_value );

    # global token should return template specific token first
    is ( $template->get_token_value( $global_token ) => $feed_value );

    # template specific token should only return template specific token
    is ( $template->get_token_value( $feed_token ) => $feed_value );
};

is( Stream::Templates::Default->determine_isa(), undef, 'Default template does not inherit' );

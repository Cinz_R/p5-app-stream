#!/usr/bin/env perl

use strict;

use FindBin;

use Test::More;

plan tests => 6;

# 1 test
use_ok('Stream::Templates::Default')
    or BAIL_OUT('Stream::Templates::Default does not compile');

# 5 tests
{
    is( Stream::Templates::Default->MULTILIST_COMMA(1,2,3,4), '1,2,3,4', '1,2,3,4' );
    is( Stream::Templates::Default->MULTILIST_COMMA(), '', '' );
    is( Stream::Templates::Default->MULTILIST_COMMA(undef), '', '' );
    is( Stream::Templates::Default->MULTILIST_COMMA(1,2,2,3,4), '1,2,2,3,4', '1,2,2,3,4' );
    is( Stream::Templates::Default->MULTILIST_COMMA(1,undef,2,2,3,4), '1,2,2,3,4', '1,undef,2,2,3,4' );
};



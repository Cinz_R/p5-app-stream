use Test::Most tests => 2;

{
  package Stream::Templates::example;

  use base 'Stream::Templates::Default';

  our %standard_tokens = (
    token => {
      Type => 'Text',
    },
  );
};

is(Stream::Templates::example->destination(),        "example", "Class method");
is(Stream::Templates::example->new()->destination(), "example", "Instance method");

done_testing();

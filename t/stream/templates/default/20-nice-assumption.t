#!/usr/bin/env perl

use strict;

use Test::More tests => 2;
use Stream::Templates::RobotMock;

{
    # by sheer fluke, token_value automatically unpacks array refs
    # turns out this behaviour is quite handy :)
    # this test case is to document this unintended behaviour to ensure it stays
    my $feed = Stream::Templates::RobotMock->new();
    $feed->token_value('array_ref' => [1,2,3] );
    $feed->token_value('array'     => (1,2,3) );
    is_deeply( [$feed->token_value('array_ref')] => [$feed->token_value('array')] );
    is_deeply( [$feed->token_value('array_ref')] => [1,2,3] );
};

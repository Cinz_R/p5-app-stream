use Test::Most;

{
  package Stream::Templates::example;

  use base 'Stream::Templates::Default';

  our %standard_tokens = (
    token_without_id => {
      Type => 'Text',
    },

    token_with_id => {
      Id   => 'custom_id',
      Type => 'Text',
    },
  );
};

diag(Stream::Templates::example->destination());

my $tokens_ref = Stream::Templates::example->standard_tokens();

subtest "All tokens have an id" => sub {
  foreach my $token_ref ( values %$tokens_ref ) {
    ok($token_ref->{Id});
  }
};

is( $tokens_ref->{token_without_id}->{Id}, 'example_token_without_id', 'Search generates the ID' );
is( $tokens_ref->{token_with_id}->{Id},    'example_token_with_id',    'We ignore the supplied Id' );

done_testing();

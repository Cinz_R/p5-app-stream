#!/usr/bin/env perl

use strict;

use Test::More;

my %expected_tokens = (
    location => 1,
    mandatory_token => 1,
);

plan tests => 1 + scalar(keys %expected_tokens);


use_ok('Stream::Templates::RobotMock')
    or BAIL_OUT('Stream::Templates::RobotMock does not compile');

{
    my ($tokens_found_ref) = Stream::Templates::RobotMock->standard_tokens();

    diag( "Found " . scalar(keys %$tokens_found_ref) . " tokens" );

    foreach my $expected_token ( keys %expected_tokens ) {
        ok( delete $tokens_found_ref->{ $expected_token }, $expected_token );
    }

    while (my ($token_name, $token_ref) = each %$tokens_found_ref ) {
        fail( $token_name . ': unexpected token' );
    }
};

#!/usr/bin/env perl

use strict;

use Test::More tests => 7;

use_ok('Stream::Templates::Default')
    or BAIL_OUT('Stream::Templates::Default does not compile');

use_ok('Stream::Token')
    or BAIL_OUT('Stream::Token does not compile');

{
  package Stream::Templates::Tests::MaxResults;

  use base qw(Stream::Engine::Robot);

  use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

  $standard_tokens{max_results} = {
      Label  => 'Number of Results',
      Type   => 'List',
      Values => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
  };
};

my $default = 'Stream::Templates::Default';
{
    my $token = Stream::Token->new({
        Type => 'List',
        Options => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
    });

    is( $default->results_per_page_from_token( $token ), 50 );
};

{
    my $token = Stream::Token->new({
        Type => 'List',
        Options => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
        Default => 10,
    });

    is( $default->results_per_page_from_token( $token ), 10 );
};

{
    my $token = Stream::Token->new({
        Type => 'List',
        Options => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50|60 results max=60)',
    });

    is( $default->results_per_page_from_token( $token ), 50 );
};

{
    my $token = Stream::Token->new({
        Type => 'List',
        Options => '(10 results max=10|20 results max=20|30 results max=30)',
    });

    is( $default->results_per_page_from_token( $token ), 30 );
};

{
    my $feed = Stream::Templates::Tests::MaxResults->new();

    is( $feed->results_per_page(), 50 );
};

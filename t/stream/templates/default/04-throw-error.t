#!/usr/bin/env perl

use strict;

use Test::More tests => 2;

{
  package Stream::Templates::Tests::ThrowErrorDuringSearch;

  use base qw(Stream::Engine::Robot);

  sub search_navigate_to_page {
    my $self = shift;

    return $self->throw_error('BBTECH_ERROR:LoginFailed(Missing username)');
  }
};

{
    my $feed = Stream::Templates::Tests::ThrowErrorDuringSearch->new();

    my $results = eval {
        $feed->search();
    };

    my $error_msg = $@;

    ok( !$results );
    like( $error_msg, qr/\QBBTECH_ERROR:LoginFailed(Missing username)\E/ );
};

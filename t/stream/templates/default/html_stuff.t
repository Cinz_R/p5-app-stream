#! perl -w

use Test::Most;
use Stream::Templates::Default;
use XML::LibXML;
use Stream2;


my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");


my $simple_cv_html = q|
<html>
    <body>
        <h1>MY CV</h1>
        <p> this <a class="whatever">is</a> a test</p>
        <a href="http://www.exampl.com">test</a>
    </body>
</html>|;

my $expected = q|<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
    <body>
        <h1>MY CV</h1>
        <p> this <span>is</span> a test</p>
        <span>test</span>
    </body>
</html>
|;

my $default_t = Stream::Templates::Default->new();
$default_t->{html_parser} = $stream2->html_parser();
my $parser = XML::LibXML->new();
my $xdoc = $parser->load_html( string => Encode::encode('UTF-8', $simple_cv_html), encoding => 'UTF-8');
$default_t->cleanup_xdoc_anchors($xdoc);

is( $xdoc->toString(), $expected, "all <a> tags were removed from the xdoc" );

is( $default_t->generate_html_fragment( $simple_cv_html ),
    q|
        <h1>MY CV</h1>
        <p> this <span>is</span> a test</p>
        <span>test</span>
    | );

done_testing();

#!/usr/bin/env perl

use strict;

use Test::More tests => 5;

{
  package TestTemplate;

  use base 'Stream::Templates::Default';

  use vars qw(%standard_tokens);

  $standard_tokens{location} = {
      Label   => 'Location',
      Type    => 'Text',
      Default => 'Canary Wharf',
      Size    => '60',
  };
};

my $feed = TestTemplate->new();

is( $feed->token_value('location'), undef );
is( $feed->token_value_or_default('location'), 'Canary Wharf' );

$feed->token_value('location', 'Testing');
is( $feed->token_value_or_default('location'), $feed->token_value('location') );

$feed->token_value('location', '');
is( $feed->token_value_or_default('location'), $feed->token_value('location') );

# you can't unset a token
$feed->token_value('location', undef );
is( $feed->token_value_or_default('location'), $feed->token_value('location') );

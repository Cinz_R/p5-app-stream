use Test::Most;
use Stream::Templates::Default;

subtest "Inline list/multilists" => sub {
  my %tokens = (
    inline_list => {
      Type => 'list(one=1|two=2)',
    },
    inline_multilist => {
      Type => 'MultiList(one=1|two=2)',
    },
  );

  my $standardised_tokens = standardise(\%tokens);

  my $list = $standardised_tokens->{inline_list};
  my $multilist = $standardised_tokens->{inline_multilist};

  subtest "types are lowercase and no longer have the options in them" => sub {
    is( $list->{Type}, 'list' );
    is( $multilist->{Type}, 'multilist' );
  };

  subtest "options are always in the Options key" => sub {
    is_deeply( $list->{Options}, [ [ "one" => 1 ], [ "two" => 2 ] ] );
    is_deeply( $multilist->{Options}, [ [ "one" => 1 ], [ "two" => 2 ] ] );
  };
};

subtest "Options vs Values" => sub {
  foreach my $token_type ( qw/list multilist sort/ ) {
    subtest $token_type => sub {
      my %tokens = (
        options => {
          Type => $token_type,
          Options => '(one=1|two=2)',
        },
        values => {
          Type => $token_type,
          Values => '(one=1|two=2)', # DEPRECATE?
        },
      );

      my $standardised_tokens = standardise(\%tokens);

      ok( $standardised_tokens->{options}->{Options} );
      ok( $standardised_tokens->{values}->{Options} );
    };
  }
};

subtest "Token types are not case-sensitive" => sub {
  my %tokens = (
    mixed => {
      Type => 'tEXt',
    },
    upper => {
      Type => 'LIST',
    },
    lower => {
      Type => 'text',
    },
  );

  my $standardised_tokens = standardise(\%tokens);
  is $standardised_tokens->{mixed}->{Type}, 'text', 'Mixed case';
  is $standardised_tokens->{upper}->{Type}, 'list', 'Upper case';
  is $standardised_tokens->{lower}->{Type}, 'text', 'Lower case';
};

subtest "Token validation" => sub {
  my %tokens = (
    select_max => {
      Validation => 'Select-30',
    },
    select_between => {
      Validation => 'Select1-2',
    },
    numeric => {
      Validation => 'Numeric',
    },
  );

  my $standardised_tokens = standardise(\%tokens);
  my $numeric = $standardised_tokens->{numeric};
  is $numeric->{Validation}, 'Numeric';

  my $max = $standardised_tokens->{select_max};
  is $max->{Validation}, 'Select';
  ok !$max->{Minimum};
  is $max->{Maximum}, 30;

  my $between = $standardised_tokens->{select_between};
  is $between->{Validation}, 'Select';
  is $between->{Minimum}, 1;
  is $between->{Maximum}, 2;
};

subtest "Default sort metrics" => sub {
  my %tokens = (
    default => {
      SortMetric => 1,
    },
    default_zero => {
      SortMetric => 0,
    },
    no_sort_metric => {
    },
  );

  my $standardised_tokens = standardise(\%tokens);
  is $standardised_tokens->{default}->{SortMetric}, 1;
  is $standardised_tokens->{default_zero}->{SortMetric}, 0;
  ok $standardised_tokens->{no_sort_metric}->{SortMetric};
};

done_testing();

sub standardise {
  my ($tokens) = @_;
  Stream::Templates::Default->_standardise_tokens($tokens);
}

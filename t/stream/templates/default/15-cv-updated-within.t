#!/usr/bin/env perl

use strict;
use warnings;

use FindBin;

use Test::More;

plan tests => 20;

use DateTime;

# 1 test
use_ok('Stream::Templates::Default')
    or BAIL_OUT('Stream::Templates::Default does not compile');

# 2 tests
{
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('TODAY'), 0, 'today' );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('YESTERDAY'), 1, 'yesterday' );
};

# 4 tests
{
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('1D'), 1, '1 day' );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('2D'), 2, '2 days' );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('3D'), 3, '3 days' );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('50D'), 50, '50 days' );
};

# 4 tests
{
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('1W'), 7 );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('2W'), 14 );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('3W'), 21 );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('4W'), 28 );
};

# 1 test
{
    # After the 29 of feb (included) and in leap years,
    # one year ago is 366 days, not 365.
    # The year AFTER a leap year, before the 29th of feb, last year is 366 days ago,
    # and 365 after.
    like( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_DAYS('1Y'), qr/36[56]/ ); # this should never fail
};

# 1 test
{
    # fix - February month days seem to mess up this helper
    # redefine date to 31 May for this test to apply, number of days difference is always higher when comparing against february dates
    no strict 'refs';
    no warnings 'redefine';
    local *{'DateTime::today'} = sub {
        return DateTime->new(
            year  => 2012,
            month => 5,
            day   => 31
        );
    };
    is (Stream::Templates::Default->CV_UPDATED_WITHIN_TO_MONTHS('3M'), 3, "3 months - leap year" );
};

#7 tests
{
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_MONTHS('5W'), 2, "5 weeks" );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_MONTHS('6M'), 6, "6 months" );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_MONTHS('1Y'), 12, "1 year" );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_MONTHS('3D'), 1, "3 days" );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_MONTHS('YESTERDAY'), 1, "Yesterday" );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_MONTHS('TODAY'), 1, "Today" );
    is( Stream::Templates::Default->CV_UPDATED_WITHIN_TO_MONTHS('ALL'), undef, "All" );
};


use Test::Most;

{
  package Stream::Templates::dummy;

  use base 'Stream::Templates::Default';

  our %standard_tokens = (
      essential => {
        Essential => 1,
      },
      non_essential => {
        Essential => 0,
      },
      default => {},
  );

  our %authtokens = (
      authtoken => {},
  );

  our %global_authtokens = (
      globalauthtoken => {},
  );
};

my $feed = 'Stream::Templates::dummy';

my @tests = (
    {
        name => 'Essential tokens',
        got_tokens => $feed->essential_tokens(),
        expected_tokens => ['essential'],
    },

    {
        name => 'Authtokens',
        got_tokens => $feed->authtokens(),
        expected_tokens => ['authtoken'],
    },

    {
        name => 'Global Authtokens',
        got_tokens => $feed->global_authtokens(),
        expected_tokens => ['globalauthtoken'],
    },
);

plan tests => scalar(@tests);

foreach my $test_defn ( @tests ) {
    subtest $test_defn->{name} => sub { tokens_match_ok($test_defn->{got_tokens}, $test_defn->{expected_tokens}) };
}

sub tokens_match_ok {
    my ($got_tokens, $expected_tokens) = @_;

    ok($got_tokens, "got tokens");

    is scalar(keys %$got_tokens), scalar(@$expected_tokens), "have the right number of tokens";

    my @missing_tokens;
    foreach my $token ( @$expected_tokens ) {
        push @missing_tokens, $token if !$got_tokens->{$token};
    }

    ok(!@missing_tokens, "got all expected tokens")
        or diag("Expected :" . explain(\@missing_tokens));
}

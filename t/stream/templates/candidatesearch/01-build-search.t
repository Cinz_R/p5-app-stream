#!/usr/bin/env perl
use strict; use warnings;

use Test::Most;
use Test::MockModule;
use Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}
ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();
my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});

my $template = $stream2->build_template('candidatesearch' , undef, { user_object => $user });
my $template_mock = Test::MockModule->new( 'Stream::Templates::candidatesearch' );
$template_mock->mock( bbcss_customer => sub {
    {
        careerbuilder_customer_key => 'my_customer_key',
        config => {
            custom_fields => [
                {
                    field_name => 'brocolli'
                },
                {
                    field_name => 'broadbean_advert_industry'
                }
            ]
        }
    };
});
$template_mock->mock( current_page => sub { 1; } );

my $expected = {
    'start_page' => 1,
    'customer_key' => 'my_customer_key',
    'fields_to_return' => [ $template->candidatesearch_required_fields(), 'broadbean_advert_industry' ],
    'results_per_page' => 20
};

# Set the default value, so we dont have a filter on specified salaries.
$template->set_token_value( include_unspecified_salaries => 1);

is_deeply( $template->candidatesearch_build_search_json(), $expected, 'search body, no criteria, is build correctly' );

# test 2, keywords
$template->set_token_value( keywords => 'pancakes' );
$expected->{keyword} = 'pancakes';
is_deeply( $template->candidatesearch_build_search_json(), $expected, 'search body, with keywords, is build correctly' );

# test 3, filter criteria
$template->set_token_value( candidatesearch_degree_name => 'sausages' );
$expected->{filter_aggregates} = { filters => [{ field_name => 'degree_name', operation => 'OR', values => [{ value => 'sausages' }] }], operation => 'AND' };

is_deeply( $template->candidatesearch_build_search_json(), $expected, 'search body, with filters, is build correctly' );

done_testing();

#!/usr/bin/env perl
use strict; use warnings;

use Test::Most;
use Test::MockModule;

use_ok( 'Stream::Templates::candidatesearch' );

my $template = Stream::Templates::candidatesearch->new();
my $template_mock = Test::MockModule->new( 'Stream::Templates::candidatesearch' );
$template_mock->mock( bbcss_customer => sub {
    {
        careerbuilder_customer_key => 'my_customer_key',
        config => {
            custom_fields => []
        }
    };
});

$template_mock->mock( current_page => sub { 1; } );

$template_mock->mock( response_to_JSON => sub {
    {"data" => {"results" => {"num_documents" => 20,"type" => "documents","documents" => []},"queryFacets" => [],"total_matching_records" => 1651,"status_code" => 200,"facets" => [{"values" => [{"count" => 7,"name" => "be"},{"count" => 6,"name" => "de"},{"name" => "ru","count" => 4}],"name" => "country"},{"values" => [{"name" => "high school","count" => 142},{"count" => 26,"name" => "associate degree"},{"count" => 10,"name" => "doctorate"}],"name" => "degree_name"},{"values" => [{"name" => "522120","count" => 3},{"count" => 3,"name" => "523120"},{"name" => "722513","count" => 3}],"name" => "naics_code"},{"values" => [{"name" => "false","count" => 1650},{"name" => "true","count" => 1}],"name" => "currently_employed"},{"values" => [],"name" => "skill_list"}]},"timing" => {"time_elapsed" => 0.7644,"time_received" => "2015-07-28T12 => 47 => 45.3224000Z"},"forensics" => {"input" => {"results_per_page" => 20,"debug" => "0","fields_to_return" => ["document_id","first_name","last_name","email","city","postal_code","latitude","longitude","country","phone","currently_employed","total_years_experience","activity_history","address_1","admin_area_1"],"facet" => [{"name" => "country","count" => 10},{"name" => "degree_name","count" => 10},{"count" => 10,"name" => "naics_code"},{"count" => 10,"name" => "onet_17"},{"count" => 10,"name" => "currently_employed"},{"count" => 10,"name" => "skill_list"}],"customer_key" => "bbts_cinzw20768ec378","radius" => 30,"group_by_candidate_limit" => 1,"by_candidate" => "0","group_by_candidate" => "0","enable_multi_facet" => "0","start_page" => 1}}};
});
$template_mock->mock( bbcss_naics_lookup => sub {
    (
        522120 => 'Savings Institutions',
        523120 => 'Securities',
        722513 => 'Limited-Service'
    );
});
$template_mock->mock( bbcss_onet_lookup => sub {} );

my $results = Stream2::Results->new();
$template->results( $results );

my $expected = [
  {
    'Options' => [ [ 'Belgium', 'be', 7 ], [ 'Germany', 'de', 6 ], [ 'Russian Federation', 'ru', 4 ] ],
    'Type' => 'multilist',
    'Id' => 'candidatesearch_country',
    'Label' => 'Country',
    'Name' => 'country',
    'SelectedValues' => {},
  },
  # {
  #   'Options' => [ [ 'high school', 'high school', 142 ], [ 'associate degree', 'associate degree', 26 ], [ 'doctorate', 'doctorate', 10 ] ],
  #   'Type' => 'multilist',
  #   'Id' => 'candidatesearch_degree_name',
  #   'Label' => 'Degree Name',
  #   'Name' => 'degree_name',
  #   'SelectedValues' => {},
  # },
  # {
  #   'Options' => [ [ 'Savings Institutions', '522120', 3 ], [ 'Securities', '523120', 3 ], [ 'Limited-Service', '722513', 3 ] ],
  #   'Type' => 'multilist',
  #   'Id' => 'candidatesearch_naics_code',
  #   'Label' => 'Industry Experience',
  #   'Name' => 'naics_code',
  #   'SelectedValues' => {},
  # },
  # {
  #   'Options' => [],
  #   'Type' => 'multilist',
  #   'Id' => 'candidatesearch_onet_17',
  #   'Label' => 'Occupation',
  #   'Name' => 'onet_17',
  #   'SelectedValues' => {},
  # },
  {
    'Options' => [ [ 'Any', '', 1651 ], [ 'false', 'false', 1650 ], [ 'true', 'true', 1 ] ],
    'Type' => 'list',
    'Id' => 'candidatesearch_currently_employed',
    'Label' => 'Currently Employed',
    'Name' => 'currently_employed',
    'Value' => undef,
  },
  {
    'Options' => [],
    'Type' => 'multilist',
    'Id' => 'candidatesearch_skill_list',
    'Label' => 'Skills',
    'Name' => 'skill_list',
    'SelectedValues' => {},
  }
];

my $facets = $template->scrape_facets();

# don't test that sort metrics are consistent as they are likely to change
delete $_->{SortMetric} for @$facets;

is_deeply( [sort { $a->{Name} cmp $b->{Name} } @$facets], [sort { $a->{Name} cmp $b->{Name} } @$expected], "facets are scraped and formated correctly" );

done_testing();

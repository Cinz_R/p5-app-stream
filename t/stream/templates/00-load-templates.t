#!/usr/bin/env perl -w
use warnings;
use strict;
use Test::More;
use Test::Exception;

# use Carp::Always;

use Template::Parser;

use Class::Load;
use Module::Pluggable::Object;

use Stream2;
use Stream2::Templates;
use Path::Tiny;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

## Having a stream2 will make sure that we have the historical templates in the path.
ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

my $templates = Stream2::Templates->new(stream2 => $stream2);
my @shared_dirs = map {
    Path::Tiny->new( $_ )->child('stream');
} @{$templates->paths};

my $snippet_parser = Template::Parser->new( ABSOLUTE => 1 );

#Anything matching Stream::Templates::[name] should be a feed. Gets them all.
my $mp = Module::Pluggable::Object->new(
    search_path => [ 'Stream::Templates' ],
    min_depth => 3,
    max_depth => 3
);
my @template_classes = $mp->plugins();

foreach my $template_class ( @template_classes ){
    subtest  'Testing ' . $template_class, sub {
        lives_ok { Class::Load::load_class($template_class) } "Ok can load $template_class";
        my $template = $template_class->new();
        ok($template->isa('Stream::Templates::Default'), 'Ok extends Stream::Default::Template');

        my @snippets = (
            {
                label => 'Candidate result snippet',
                subdir => '/result/body'
            },
            {
                label => 'Candidate profile snippet',
                subdir => '/candidate_profiles'
            }
        );

        foreach my $snippet (@snippets) {
            my $destination = $template->destination;

            foreach my $snippet_dir (@shared_dirs) {
                SKIP: {
                    my $snippet_file = $snippet_dir->child($snippet->{subdir})
                      ->child($destination . '.inc');
                    if( ! $snippet_file->is_file ) {
                        #Snippet isn't required for ALL modules.
                        skip($snippet_file . ' doens\'t exist', 1);
                    }
                    ok(my $snippet_string = $snippet_file->slurp(), 'Snippet for ' . $snippet->{label} . ' read.')
                      or next;
                    ok($snippet_parser->parse($snippet_string), $destination . ' snippet for ' . $snippet->{label} . ' parsed.');
                }
            }
        }
    };
}

done_testing();

#! perl -w
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Stream2;

use_ok "Stream::Templates::Madgex3";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

ok( my $template = $stream2->build_template('Madgex3', undef , { location_api => $stream2->location_api() } ) , "Ok got template");

my $locations = [
    {
        location_id =>  971, # High Point, NC
        miles  =>  8,
        expected_id => 5854150,
        expected_miles => 10,
    },
    {
        location_id   =>  25488, # California, USA
        miles  =>  100,
        expected_id => 5828448,
        expected_miles => 50,
    }
];

foreach (@$locations) {
    my ($id, $miles) = $template->madgex_location($_->{location_id}, $_->{miles});
    is($id, $_->{expected_id}, 'ID is as expected');
    is($miles, $_->{expected_miles}, 'Miles are as expected');
}

my $custom_fields = [
    {
       sectors => [2929, 9090, 2929],
       qualifications => [1981],
       availability => [39393],
       default_jobtype => 'temporary',
    },
];

foreach (@$custom_fields) {
    foreach my $token ( keys %{$_} ) {
        if ( ref $_->{$token} eq 'ARRAY' ) {
            $template->token_value($token => @{$_->{$token}});
        }
        else {
            $template->token_value($token => $_->{$token});
        }
    }
    my @fields = $template->madgex_custom_fields();
    is(@fields, 5, 'custom fields come out with the right number of question terms with duplicates removed');
    like($fields[3], qr/\d{7}\|\d+/, 'format is as expected for question terms');
    is($fields[-1], '3000023|513024', 'jobtype is taken from default_jobtype token');
}

LWP::UserAgent::Mockable->finished();
done_testing();

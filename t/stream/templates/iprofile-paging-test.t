#! perl
# Test a basic search (on the dummy search template).


# If you run your test normally, you should have to change this.
# In the case you use LWP_UA_MOCK=record more than 24 hours
# after the last LWP_UA_MOCK 'record', you will have to update
# this value with the session ID that will be displayed at the end
# of this test.

use strict;
use warnings;

use FindBin;
use Test::More;
use Archive::Tar;
use Data::UUID;

use Stream2;

use Log::Log4perl qw/:easy/;
Log::Log4perl->easy_init($ERROR);

use Log::Any::Adapter;
Log::Any::Adapter->set('Log::Log4perl');


my $config_file = 't/app-stream2.conf';

my $template_name = 'iprofile';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
$user->save();


ok( my $template = $stream2->build_template($template_name, undef , { user_object => $user } ) , "Ok got template");
my $results_per_page = 50;
$template->token_value('max_results' => $results_per_page);
is ( $template->results_per_page(), $template->token_value('max_results') );

my $current_page = 1;
$template->current_page( $current_page );
is ( $template->current_page, $current_page, "current_page starts at page 1" );

# we keep the XML file saved as I believe Iprofile return different results each search ( different order anyway )
# This means that even if we record it using LWP, each test will have different set of results to compare to
my $tar_fn = "$FindBin::Bin/iprofile-test.tgz";
my $tar = Archive::Tar->new($tar_fn);
my ( $tar_file ) = $tar->get_files(qw/iprofile-test.xml/);

# hack the engine to return the contents of the XML file
my $response = HTTP::Response->new();
$response->header( 'Content-Type' => 'text/xml' );
no strict 'refs'; # messing with the symbol table
*{ref($template).'::response'} = sub { return $response; };
$template->recent_content( $tar_file->get_content() );

my $tests = [
    {
        page => 1,
        first => 'David Baker',
        last => 'Nick Franks',
        iprof_page => 1,
        iprof_offset => 0
    },
    {
        page => 2,
        first => 'Eugenia Okafor',
        last => 'Matthew Rhind-Tutt',
        iprof_page => 1,
        iprof_offset => 50
    },
    {
        page => 10,
        first => 'James Stuart',
        last => 'Chris Shepherd',
        iprof_page => 1,
        iprof_offset => 450
    },
    { # asks for page 2 from iprofile, offset is 0
        page => 11,
        first => 'David Baker',
        last => 'Nick Franks',
        iprof_page => 2,
        iprof_offset => 0
    },
    { # going overboard but still a valid test
        page => 119,
        first => 'John Bailey',
        last => 'Vivek Chavan',
        iprof_page => 12,
        iprof_offset => 400
    }
];

foreach my $test ( @$tests ) {

    my $ug = Data::UUID->new();
    my $results_id = $ug->create_str();
    my $results_collector = Stream2::Results->new({
        id          => $results_id,
        destination => $template_name,
    });
    $template->results($results_collector);

    $template->{_iprofile_node_index} = 0;
    $template->current_page( $test->{page} );

    $template->scrape_generic_search_results();
    is ( scalar( @{$results_collector->results()} ), 50, "we should only ever parse 50 results" );

    my $first_result = $results_collector->results->[0];
    $first_result->fixup();
    my $last_result = $results_collector->results->[-1];
    $last_result->fixup();

    is ( $first_result->name, $test->{first}, "first result name correct" );
    is ( $last_result->name, $test->{last}, "last result name correct" );

    is ( $template->iprofile_current_page, $test->{iprof_page} );
    is ( $template->iprofile_node_start(), $test->{iprof_offset} );

}

done_testing();

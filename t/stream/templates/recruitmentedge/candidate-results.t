use Test::Most;
use Stream2;

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
};

use LWP;
use LWP::UserAgent::Mockable;

use_ok "Stream::Templates::recruitmentedge";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $user_factory = $stream2->factory('SearchUser');
my $user = $user_factory->new_result({
    id => 91,
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 157656, # simonr@test.simonr.simonr
    group_identity => 'acme',
});
$user->save();

ok( my $template = $stream2->build_template('recruitmentedge', undef , { location_api => $stream2->location_api(), user_object => $user } ) , "Ok got template");

$template->token_value(refresh_token => 'ab40fc4e429c49eab3a2863972bdaf957152561322c228a8a74cee25b848ebee');
$template->token_value(keywords => 'real-estate agent');
$template->token_value(cv_updated_within => '3M');

my $results = eval { $template->search(); };

ok @{$results->facets()} > 0, 'received facets';

ok $template->candidates_seen() <= $template->results_per_page(), 'saw expected number of candidates for one search';

foreach my $candidate ( @{$results->results()} ) {
    ok $candidate->candidate_id, 'everyone has an id';
    ok $candidate->attr('block_bulk_message'), 'block bulk messaging';
    ok $candidate->attr('has_free_cv'), 'recruitmentedge candidate should have attr for free messaging';

    # some have them, some don't, both cases are okay
#    ok $candidate->attr('recent_education'), 'recent_education is good';
#    ok $candidate->attr('recent_position'), 'recent_position is good';

    is ref $candidate->attr('keywords'), 'HASH', 'keywords are a hash of skills';
    is $candidate->has_profile(), 1, 'everyone has profile';
    is $candidate->has_cv(), 1, 'everyone has a cv';


    my $recent_position = $candidate->attr('recent_position');

    my $headline = join ' - ', grep {$_} (
        $recent_position->{Position} || $candidate->attr('jobtitle'),
        $recent_position->{Employer},
    );
    is(
        $candidate->attr('headline'),
        $headline,
        'populated jobtitle as a headline'
    );
    is(
        $candidate->get_attr_arrayref('headline')->[0],
        $headline,
        'populated jobtitle as a headline using arrayref'
    );

    if ( $candidate->attr('last_viewed') ) {
        like $candidate->attr('last_viewed'), qr/\d{4}-\d{2}-\d{2}/, 'date field is only date part of datetime format';
    }
    if ( $candidate->attr('location') ) {
        my @parts = grep { defined } ( $candidate->attr('county'), $candidate->attr('city'), $candidate->attr('state_iso') );
        is $candidate->attr('location'), join(', ', @parts), 'set a location from location parts';
    }
    if ( $candidate->attr('photo') ) {
        unlike $candidate->attr('photo'), qr/gravtar/, "don't set gravatar images as they 404";
    }
    if ( $candidate->attr('social_sources') ) {
        explain '
            social_sources are labels of channels the candidate can be found in, some are duplicates (eg ResumeDBDID & ResumeWordDocDBDID which both mean Careerbuilder),
            others come as LinkedInID and LinkedInName which are different ways to get to the profile page (linkedin.com/123 or linkedin.com/username) but the URL is always
            empty from search results (available in profile) so we use only the labels for snippets';
        unlike $candidate->attr('social_sources'), qr/WordDoc|ID|Name/, 'social_sources are user friendly labels';
    }
}

END {
    # END block ensures cleanup if script dies early
    LWP::UserAgent::Mockable->finished;
};

done_testing();

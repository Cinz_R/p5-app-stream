use Test::Most;
use Stream2;

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
};

use LWP;
use LWP::UserAgent::Mockable;

use_ok "Stream::Templates::recruitmentedge";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $user = $stream2->factory('SearchUser')->new_result({
    id => 91,
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 157656, # simonr@test.simonr.simonr
    group_identity => 'acme',
});

$user->save();

ok( my $template = $stream2->build_template('recruitmentedge', undef , { location_api => $stream2->location_api(), user_object => $user } ) , "Ok got template");

$template->token_value(refresh_token => 'ab40fc4e429c49eab3a2863972bdaf957152561322c228a8a74cee25b848ebee');
$template->token_value(keywords => 'manager');
$template->token_value(cv_updated_within => '3M');

my $result_bucket = eval { $template->search(); };

ok my $candidate = $result_bucket->results()->[0], 'get the first candidate';

ok $candidate = $template->download_profile($candidate), 'downloaded profile for candidate';

my $response = $template->response_to_JSON();

foreach ( @{$response->{data}->{ID}} ) {
    if ( $_->{Network} eq 'Email' ) {
        is $candidate->email, $_->{Identifiers}->[0]->{ID}, 'candidate has their real email';
        is $candidate->emails->[0]->{Metadata}->{Validity}, 'Valid', 'email used for candidate in Search is valid (according to the board)';
    }
}

foreach my $network ( @{$candidate->attr('social_networks')} ) {
    unlike $network->{Name}, qr/ID|Name/, 'network name should be cleaned up';
    unlike $network->{Name}, qr/onetapi/, 'social network should not contain an onet attribute';
    ok exists $network->{URL}, 'social network should have a link to its social profile';
    ok defined $network->{IconClass}, 'social network always has an icon class';
}

for my $i ( 0 .. 2 ) {
    my $skill = $candidate->attr('top_skills')->[$i];
    is $skill->{name}, $response->{data}->{Keywords}->{$skill->{name}}->{Keyword}, "top skill matches index $i of Data > Keyword";
    like $skill->{reason}, qr/is listed as a skill|mentioned in the resume/, "got reason for skill $i";
}

if ( $candidate->attr('last_modified') ) {
    like $candidate->attr('last_modified'), qr/^\d{4}-\d{2}-\d{2}$/, 'date field is only date part of datetime format';
}

if ( $candidate->attr('education') ) {
    my $grad_date = $response->{data}->{Educations}->[0]->{DateGraduated};
    is($candidate->attr('education')->[0]->{DateGraduated}, join('-', grep { defined } ($grad_date->{UtcYear}, $grad_date->{UtcMonth}, $grad_date->{UtcDay})), 'education date graduated field is a date string');
}

if ( $candidate->attr('employment') ) {
    my $from = $response->{data}->{Employments}->[0]->{DateFrom};
    my $to = $response->{data}->{Employments}->[0]->{DateTo};
    is($candidate->attr('employment')->[0]->{DateFrom}, join('-', grep { defined } ($from->{UtcYear}, $from->{UtcMonth}, $from->{UtcDay})), 'employment date from field is a date string');
    is($candidate->attr('employment')->[0]->{DateTo}, join('-', grep { defined } ($to->{UtcYear}, $to->{UtcMonth}, $to->{UtcDay})), 'employment date to field is a date string');
}

if ( my $cb_cv = $response->{data}->{Profiles}->{ResumeDBWordDocDID}->[0]->{URL} ) {
    is($candidate->attr('cb_cv'), $cb_cv, 'candidates with a cb_cv have their value set as the resume download url');
}

is_deeply(\@{$candidate->attr('about')}, $response->{data}->{About} // []);
is_deeply(\@{$candidate->attr('awards')}, $response->{data}->{Awards} // []);
is_deeply(\@{$candidate->attr('phones')}, $response->{data}->{Phones} // []);
is_deeply(\@{$candidate->attr('websites')}, $response->{data}->{Website} // []);
is_deeply(\@{$candidate->attr('industries')}, $response->{data}->{Industries} // []);
is_deeply(\@{$candidate->attr('memberships')}, $response->{data}->{Memberships} // []);
is_deeply(\@{$candidate->attr('publications')}, $response->{data}->{Publications} // []);
is_deeply(\@{$candidate->attr('certificates')}, $response->{data}->{Certifications} // []);
is_deeply(\@{$candidate->attr('spoken_languages')}, $response->{data}->{SpokenLanguage} // []);
is_deeply(\@{$candidate->attr('current_locations')}, $response->{data}->{Locations}->{CurrentLocations} // []);
is_deeply(\@{$candidate->attr('desired_locations')}, $response->{data}->{Locations}->{DesiredLocations} // []);

is_deeply(\%{$candidate->attr('scored_keywords')}, $response->{data}->{Keywords} // {});

END {
    # END block ensures cleanup if script dies early
    LWP::UserAgent::Mockable->finished;
};

done_testing();

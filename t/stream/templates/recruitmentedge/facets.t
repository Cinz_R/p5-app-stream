use Test::Most;
use Stream2;

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
};

use LWP;
use LWP::UserAgent::Mockable;

use_ok "Stream::Templates::recruitmentedge";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $user_factory = $stream2->factory('SearchUser');
my $user = $user_factory->new_result({
    id => 91,
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 157656, # simonr@test.simonr.simonr
    group_identity => 'acme',
});
$user->save();

ok( my $template = $stream2->build_template('recruitmentedge', undef , { location_api => $stream2->location_api(), user_object => $user } ) , "Ok got template");

$template->token_value(refresh_token => 'ab40fc4e429c49eab3a2863972bdaf957152561322c228a8a74cee25b848ebee');
$template->token_value(keywords => 'real-estate agent');
$template->token_value(cv_updated_within => '3M');

my $results = eval { $template->search() };

foreach my $facet ( @{$results->facets()} ) {
    is $facet->{Type}, 'multilist', 'type is always multilist';
    ok $facet->{ID}, 'id set for facet';
    ok $facet->{Name}, 'name set for facet';
    ok $facet->{Label}, 'label set for facet';
    ok $facet->{SortMetric}, 'sortmetric set for facet';
    is scalar( @{$_} ), 3, 'all options have a label, name and count' for ( @{$facet->{Options}} );
}

END {
    # END block ensures cleanup if script dies early
    LWP::UserAgent::Mockable->finished;
};

done_testing();

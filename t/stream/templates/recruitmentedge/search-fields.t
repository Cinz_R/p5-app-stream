#! perl -w

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Stream2;

use_ok "Stream::Templates::recruitmentedge";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $user_factory = $stream2->factory('SearchUser');
my $user = $user_factory->new_result({
    id => 91,
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 157656, # simonr@test.simonr.simonr
    group_identity => 'acme',
});
$user->save();

ok( my $template = $stream2->build_template('recruitmentedge', undef , { location_api => $stream2->location_api(), user_object => $user } ) , "Ok got template");

$template->token_value( refresh_token => 'ab40fc4e429c49eab3a2863972bdaf957152561322c228a8a74cee25b848ebee');
$template->token_value( keywords => 'real-estate agent' );
$template->token_value( names => 'Nanny Mcphee' );
$template->token_value( exclude_terms => 'mole' );
$template->token_value( location_id => 971 );
$template->token_value( location_within => 30 );
$template->token_value( cv_updated_within => '3M' );
$template->token_value( salary_from => 50000 );
$template->token_value( salary_to => 90000 );
$template->token_value( salary_cur => 'USD' );
$template->token_value( salary_per => 'annum' );
$template->token_value( min_years_exp => 2 );
$template->token_value( CurrentEmployer => 'Broadbean' ); #facet

$template->create_default_tokens();

ok my %fields = $template->search_fields(), 'got search fields to analyse';

is $fields{DeveloperKey}, $template->recruitmentedge_developer_key(), 'developer_key param is good';
is $fields{Page}, "1", 'page param starts at 1';
is $fields{ResultsPerPage}, $template->results_per_page(), 'results_per_page is good';
is $fields{IncludeFacets}, 'true', 'always return facets';
is $fields{IncludeStats}, 'true', 'stats are nice too';
is $fields{Query}, $template->token_value('keywords'), 'keywords sent in query param';
is $fields{Names}, $template->token_value('names'), 'names sent in names param';
is $fields{ExcludeTerms}, $template->token_value('exclude_terms'), 'exclude_terms sent in exclude_terms param';
is $fields{Locations}, 'High Point, NC, US', 'location sent in location param';
is $fields{LocationRadius}, $template->token_value('location_within_miles'), 'location radius sent in locationradio param';

is $fields{Filter}, '(CurrentEmployer:"Broadbean") (ResumeModified:[NOW-90DAYS TO NOW]) RecentWageYearly:[50000 USD TO 90000 USD] YearsOfExperience:[2 TO *]', 'filter param contains all additional fields';

END {
    LWP::UserAgent::Mockable->finished();
}

done_testing();

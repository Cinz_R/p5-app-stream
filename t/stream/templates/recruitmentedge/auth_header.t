BEGIN {
    $ENV{ LWP_UA_MOCK } ||= 'playback';
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}

use Test::Most;
use Test::MockModule;
use Stream::Templates::recruitmentedge;
use HTTP::Response;
use Test::Stream2;
use LWP;
use LWP::UserAgent::Mockable;


=comment

Test OAuth for recruitment edge feed:
    Check that auth headers are correct
    and that oneiam runs and caches separately

=cut

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });

my $users_factory = $stream2->factory('SearchUser');
my $user          = $users_factory->new_result({
    provider       => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id    => 1,
    group_identity => 'acme',
});

$user->save();

my $expires_at = time() + 300;
my $feed_mock = Test::MockModule->new('Stream::Templates::recruitmentedge');
my $auth_mock = Test::MockModule->new('Bean::OAuth::CareerBuilder');

sub new_feed {
    return $stream2->build_template(
        'recruitmentedge',
        undef,
        {
            location_api => $stream2->location_api(),
            user_object  => $user,
            company => 'minons'
        }
    );
}

my $feed = new_feed();
ok($feed, 'Ok got template');
$feed->token_value(refresh_token => 'ab40fc4e429c49eab3a2863972bdaf957152561322c228a8a74cee25b848ebee');

my $request_count = 0;
$feed_mock->mock(post => sub {
    my ($self, $original, @args) = @_;
    $request_count++;
    return $feed_mock->original('post')->(@_);
});

my @auth_header = $feed->_auth_header;
my $first_request_count = $request_count;
ok($first_request_count > 0, 'Make requests to get token');
is($auth_header[0], 'Authorization', 'Header key correct');
like($auth_header[1], qr/Bearer \w+/, 'Header value correct');

my $cache_feed = new_feed();
$cache_feed->token_value(refresh_token => 'ab40fc4e429c49eab3a2863972bdaf957152561322c228a8a74cee25b848ebee');
is_deeply(
    [ $cache_feed->_auth_header ],
    \@auth_header,
    'Got the same cached on second request'
);
is(
    $request_count,
    $first_request_count,
    'No additional requests for second token (uses cache)'
);


$feed_mock->mock(_offline_access_token => sub {
    my ($self) = @_;
    return 'mock_offline_token';
});

$auth_mock->mock( get_token => sub {
    return ('mock_cb_token', $expires_at);
});

my $new_refresh_token_feed = new_feed();
$new_refresh_token_feed->token_value(refresh_token => 'no_real');
my @new_refresh_header = $new_refresh_token_feed->_auth_header;
ok($new_refresh_header[1] ne $auth_header[1], 'new refresh token skips cache');

my $oneiam_feed = new_feed();
$oneiam_feed->token_value( cb_user_code => 'not-real');
$oneiam_feed->token_value( refresh_token => undef);
my @cb_header = $oneiam_feed->_auth_header;
is_deeply(
    [@cb_header], ['Authorization' => 'Bearer mock_cb_token'],
    'Got correct cb token'
);

$auth_mock->mock( get_token => sub {
    return ('new_mock_cb_token', $expires_at);
});

my $cache_oneiam_feed = new_feed();
$cache_oneiam_feed->token_value(cb_user_code => 'not-real');
is_deeply(
    [ $cache_oneiam_feed->_auth_header ], [@cb_header],
    'Uses cached oneiam token'
);

my $new_oneiam_feed = new_feed();
$new_oneiam_feed->token_value(cb_user_code => 'differnet_oneiam');
my @new_auth_header = $new_oneiam_feed->_auth_header;
ok($new_auth_header[1] ne $cb_header[1], 'new cb_user_code skips cache');

my $old_cache_feed = new_feed();
$old_cache_feed->token_value(refresh_token => 'ab40fc4e429c49eab3a2863972bdaf957152561322c228a8a74cee25b848ebee');
$feed_mock->unmock('_offline_access_token');
is_deeply(
    [$old_cache_feed->_auth_header],
    [@auth_header],
    'Refresh token still separate'
);

LWP::UserAgent::Mockable->finished;

done_testing();

#! perl -w

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Stream2;

use_ok "Stream::Templates::recruitmentedge";

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $user_factory = $stream2->factory('SearchUser');
my $user = $user_factory->new_result({
    id => 91,
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 157656, # simonr@test.simonr.simonr
    group_identity => 'acme',
});
$user->save();

ok(
    my $template = $stream2->build_template(
        'recruitmentedge',
        undef,
        {
            location_api => $stream2->location_api(),
            user_object => $user
        }
    ),
    "Ok got template"
);

my $location_tests = [
    {
        id => 25488,
        expected => 'California, US',
    },
    {
        id => 39493,
        expected => 'England',,
    },
    {
        id => 39585,
        expected => 'Leicestershire, England',,
    },
    {
        id => 3883,
        expected => 'Huntingdon, PA, US',,
    },
    {
        id => 39499,
        expected => 'Huntingdon, Cambridgeshire, England',,
    },
    {
        id => 39551,
        expected => 'City of London, London, England',,
    },
    {
        id => 39936,
        expected => 'Manchester, Greater Manchester, England',,
    },
    {
        id => 39374,
        expected => 'Madrid, Spain',,
    },
    {
        id => 7243,
        expected => 'Washington, DC, US',
    }
];

foreach ( @$location_tests ) {
    my ($location, $miles) = $template->recruitmentedge_location($_->{id}, 50);
    is($location, $_->{expected}, 'location search produces correct location string');
}

END {
    LWP::UserAgent::Mockable->finished();
}
done_testing();
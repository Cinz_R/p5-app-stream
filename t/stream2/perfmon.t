#! perl
# Test some quota stuff.

## WARNING
##
## If you change the LWP_UA_MOCK of this test, you'll have to set pat's
## quotas correctly in the search quota interface.
##
## Login as pat superadmin
##
## Go to https://www.adcourier.com/cvsearch/?action=quota_view;board=talentsearch
##
## Set quota to 1000 for the company  and 2 for dutch user.
##
##

use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Fatal qw/dies_ok lives_ok exception/;


BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';

}

use LWP;
use LWP::UserAgent::Mockable;


use Stream2;

use Email::Sender::Simple;

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

ok( my $monitor = $stream2->perfmonitor() );

my $ug = Data::UUID->new();

ok( $monitor->with_editor(sub{
                              my ($self, $editor) = @_;
                              $self->record_measure($editor,
                                                    {
                                                     oauthRespTime => 1,
                                                     juiceUserTime => 2,
                                                     juiceSubsTime => 3,
                                                     juiceLocaTime => 4,
                                                    });
                              return 1;
                          }), "Ok can update rrd file");

LWP::UserAgent::Mockable->finished();

done_testing();

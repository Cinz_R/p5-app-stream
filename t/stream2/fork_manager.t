#! /usr/bin/env perl
use strict;
use warnings;
use Test::More;

# Silence!
# use Log::Any::Adapter;
# use Log::Log4perl qw/:easy/;
# Log::Log4perl->easy_init($TRACE);

# Log::Any::Adapter->set('Log4perl');

use File::Temp;
use File::Slurp;

use POSIX ":sys_wait_h";

use Stream2::ForkManager;

my $main = sub{
    # diag("Process $$");
    1;
};

if( $^O =~ /(MacOS|darwin)/ ) {
      plan skip_all => 'Yesterday it worked...Today it is not working...Mac OS is like that.';
}


{
    # The simpliest Forking ever
    ok( my $fm = Stream2::ForkManager->new({ main => $main }) , "Ok can build fork manager");
    is( $fm->wait_children() , 1, "Ok for and wait returned 1");
    is( scalar(@{$fm->children()}) , 0 , "Zero children left");
}

{
    # Forking with different parameters one to one (as many processes as parameters)

    my @filenames = ();
    foreach my $i ( 1..3 ){
        my ($fh, $fn) = File::Temp::tempfile();
        close $fh;
        push @filenames , $fn;
    };

    my $arguments_array = [ [ 0 , $filenames[0] ],
                            [ 1 , $filenames[1] ],
                            [ 2 , $filenames[2] ]
                          ];

    my $main = sub{
        my ($fork_manager, $number, $file) = @_;
        open( my $fh, '>>' , $file) or die "Cannot open $file for writing";
        print $fh $number;
        close $fh;
        return 1;
    };

    ok( my $fm = Stream2::ForkManager->new({ main => $main,
                                             start_n => 3,
                                             arguments_array => $arguments_array
                                           }), "OK can build fork manager");
    is( $fm->wait_children() , 3,  "OK can wait children");

    my $file_idx = 0;
    foreach my $filename ( @filenames ){
        is( File::Slurp::read_file($filename) , $file_idx x 1 , "File $filename was written");
        unlink $filename;
        $file_idx++;
    }
}

{
    # Forking two to one.
    my @filenames = ();
    foreach my $i ( 1..3 ){
        my ($fh, $fn) = File::Temp::tempfile();
        close $fh;
        push @filenames , $fn;
    };

    my $arguments_array = [ [ 0 , $filenames[0] ],
                            [ 1 , $filenames[1] ],
                            [ 2 , $filenames[2] ]
                          ];

    my $main = sub{
        my ($fork_manager, $number, $file) = @_;
        open( my $fh, '>>' , $file) or die "Cannot open $file for writing";
        print $fh $number;
        close $fh;
        return 1;
    };

    ok( my $fm = Stream2::ForkManager->new({ main => $main,
                                             start_n => 6,
                                             arguments_array => $arguments_array
                                           }), "OK can build fork manager");
    is( $fm->wait_children() , 6,  "OK can wait children");

    my $file_idx = 0;
    foreach my $filename ( @filenames ){
        is( File::Slurp::read_file($filename) , $file_idx x 2 , "File $filename was written");
        unlink $filename;
        $file_idx++;
    }
}


{
    # Forking not matching number.
    # Forking two to one.
    my @filenames = ();
    foreach my $i ( 1..3 ){
        my ($fh, $fn) = File::Temp::tempfile();
        close $fh;
        push @filenames , $fn;
    };

    my $arguments_array = [ [ 0 , $filenames[0] ],
                            [ 1 , $filenames[1] ],
                            [ 2 , $filenames[2] ]
                          ];

    my $main = sub{
        my ($fork_manager, $number, $file) = @_;
        open( my $fh, '>>' , $file) or die "Cannot open $file for writing";
        print $fh $number;
        close $fh;
        return 1;
    };

    ok( my $fm = Stream2::ForkManager->new({ main => $main,
                                             start_n => 5,
                                             arguments_array => $arguments_array
                                           }), "OK can build fork manager");
    is( $fm->wait_children() , 5,  "OK can wait children");

    is( File::Slurp::read_file($filenames[0]) , '0' x 2 , "File 0  was written twice");
    is( File::Slurp::read_file($filenames[1]) , '1' x 2 , "File 1  was written twice");
    is( File::Slurp::read_file($filenames[2]) , '2' x 1 , "File 2  was written only once");
    map{ unlink $_ ; } @filenames;
}



{
    # No waiting. Will autostart and output some warning on destroy.
    my ($fh, $filename) = File::Temp::tempfile();
    close($fh);

    my $main = sub{
        File::Slurp::write_file($filename, { append => 1 }, 'RAN' );
    };
    ok( my $fm = Stream2::ForkManager->new({ main => $main, start_n => 2 }) , "Ok can build fork manager");
    sleep(3);
    $fm = undef;
    # is( File::Slurp::read_file($filename) , 'RAN' x 2 , "Children were ran");

    # Check we have NO children at all
    ok( waitpid(-1, WNOHANG) <= 0 , "No child there");
}

{
    # Same but with no autostart
    ok( my $fm = Stream2::ForkManager->new({ main => $main , autostart => 0 }) , "Ok can build fork manager with no autostart");
    is( $fm->wait_children() , 1, "Ok for and wait returned 1");
}

{
    # No autostart and manual forking
    my $main = sub{ sleep(10) ; };
    ok( my $fm = Stream2::ForkManager->new({ main => $main , autostart => 0 }) , "Ok can build fork manager with no autostart");
    ok( my @children = @{$fm->children()} , "Ok got some children");
    is( $fm->kill_all() , scalar(@children) , "Ok sent as many signals as children");
    is( $fm->wait_children() , scalar(@children) , "Ok waited for as many children");
    is( $fm->wait_children(), 0 , "Nothing to wait for. This call is now not blocking anymore");
}



{
    # Now with 2
    ok( my $fm = Stream2::ForkManager->new({ main => $main , start_n => 10 }) , "Ok can build fork manager");
    is( $fm->wait_children() , 10 , "Ok for and wait returned 10");
}

{
    # Something that goes bang in your face
    my $main = sub{
        # diag("Exiting processs $$");
        die "GWAHAHAHA"; };
    ok( my $fm = Stream2::ForkManager->new({ main => $main , start_n => 2 }) , "Ok can build fork manager");
    is( $fm->wait_children() , 2 , "Ok for and wait returned 2");
}

# diag("Now testing reforking");

{
    # Refork test.
    my $main = sub{ #diag("[$$]: Sleeping for 2 sec and reforking");
	            # Note that on OSX, sleep can prevent the kill signal to be
	            # Received properly. Hence the loop
	            for( my $i = 0 ; $i < 2 ; $i++ ){
			sleep(1);
		    }
                    Stream2::ForkManager->refork(); };
    # We need to encapsulate the fork manager in another one

    my $top = sub{
        # diag ("Top process $$");
        my $fm = Stream2::ForkManager->new({ main => $main , start_n => 2});
        $SIG{ALRM} = sub{ # kill all the children.
            $fm->kill_all();
        };
        alarm 5;
        # This should never return, as the $main code will always refork
        # But the alarm will sort that out.
        my $n_ps = $fm->wait_children();
    };

    my $fm = Stream2::ForkManager->new({ main => $top });
    is( $fm->wait_children() , 1 , "Ok got one process back (the top one)");
}


{
    # Refork test because of signals.

    my ($fh, $filename) = File::Temp::tempfile();
    close($fh);

    my $main = sub{ # diag("[$$]: Sleeping for 2 sec and killing myself");
                    sleep(2);
                    File::Slurp::write_file($filename, { append => 1 }, 'KILLED' );
                    # Commit suicide with one of the default managed signals.
                    kill 11 , $$;
                };
    # We need to encapsulate the fork manager in another one

    my $top = sub{
        # diag("Top process $$");
        my $fm = Stream2::ForkManager->new({ main => $main , start_n => 2});
        $SIG{ALRM} = sub{ # Kill all children.
            $fm->kill_all();
        };
        alarm 5;
        # This should never return, as the $main code will always refork
        # But the alarm will sort that out.
        my $n_ps = $fm->wait_children();
    };

    my $fm = Stream2::ForkManager->new({ main => $top });

    # The fact that this returns is a good sign :)

    is( $fm->wait_children() , 1 , "Ok got one process back (the top one)");

    # Heres a timeline of what happened
    #   +
    #   |Fork+---------+++-------------++
    #   |               |Sleep          |Sleep
    #   |               |               |
    #   |               |               |
    #   |               |               |
    #   |               |Write          |Write
    #   |Refork+-------+vSig 11         vSig 11
    #   |Refork+------------------------+
    #   |               +               +
    #   |               |Sleep          |Sleep
    #   |               |               |
    #   |               |               |
    #   |               |Write          |Write
    #   |               v               v
    #   |
    #   |               +               +
    #   |               |Sleep          |Sleep
    #   |               |               |
    #   vKill ALL+---------------------------------+

    # This is why we have 4 successful writes.
    my $killed_string = File::Slurp::read_file($filename);
    # Test is time dependent. Find a better way.
    #is($killed_string , 'KILLED' x 4, "Only 4 main processes managed to go through the write.");
}

done_testing();

#! perl

use utf8;
use strict;
use warnings;

use FindBin;
use Test::More;

use Redis;
use Stream2;
use Stream2::Results::Result;
use XML::LibXML;

use Qurious;
use Test::MockModule;

use File::Slurp;

use Log::Any::Adapter;
#Log::Any::Adapter->set('Stderr');

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}


my $redis = Redis->new( server => 'whatever' , no_auto_connect_on_new => 1 );

ok( my $stream2 = Stream2->new({ config_file => $config_file , redis => $redis  }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
my $mock_qurious = Test::MockModule->new(ref($stream2->qurious()));


# Deploying schema
$stream2->stream_schema()->deploy();

my $users_factory = $stream2->factory('SearchUser');

# Time to create a user.
my $memory_user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");

my $candidate = Stream2::Results::Result->new();
$candidate->destination('whatever');

$candidate->enrich_with_cvparsing({
    first_name => 'Jean Pierre',
    last_name  => 'Marcel',
    email => 'jean@pierre.com',
    bg_xml => q|<?xml version='1.0' encoding='iso-8859-1'?>
           <ResDoc>
            <resume canonversion='2' dateversion='2' iso8601='2015-10-05' present='735878' xml:space='preserve'>
               <contact><name><givenname>Jerome</givenname> <surname>Étévé☭</surname></name></contact>
           </resume>
          </ResDoc>
|
});

my $xdoc = XML::LibXML->load_xml( string => <<'EOT');
<?xml version="1.0" encoding="UTF-8"?>
<SearchCandidate>
<Candidate>
</Candidate>
</SearchCandidate>
EOT

$memory_user->set_cached_data('ripple_settings', { webhooks => [ { url => 'https://www.example.com',
                                                                   config => { tagdoc_type => 'bgt_xml' }
                                                               } ] } );

$mock_qurious->mock( create_job => sub{
                         my ($self, %args) = @_;
                         ok( %args );
                         return QueuableO->new();
                     });


$memory_user->emit_webhook('candidate/shortlist', $candidate , $xdoc);

$memory_user->set_cached_data('ripple_settings', { webhooks => [ { url => 'https://www.example.com',
                                                                   config => { tagdoc_type => 'bgt_xml' }
                                                               } ] } );

$xdoc = XML::LibXML->load_xml( string => <<'EOT');
<?xml version="1.0" encoding="UTF-8"?>
<SearchCandidate>
<Candidate>
</Candidate>
</SearchCandidate>
EOT

$memory_user->emit_webhook('candidate/shortlist', $candidate , $xdoc);


# Now call to_xml on the candidate. It should work too.
{
    ok( my $xml_doc = $candidate->to_xml( undef, { cv_mimetype => 'image/jpeg',
                                                   cv_filename => 'kitten.jpeg',
                                                   cv_content => scalar( File::Slurp::read_file( 't/files/kitten.jpg'  , { binmode => ':raw' }) )
                                               },
                                          $memory_user,
                                          { tagged_doc_type => 'BGT',
                                            board_nice_name => 'Some nice Name. Who cares'
                                        }) , "Ok got XML doc from candidate");
}

{
    ok( my $xml_doc = $candidate->to_xml( undef, { cv_mimetype => 'image/jpeg',
                                                   cv_filename => 'kitten.jpeg',
                                                   cv_content => scalar( File::Slurp::read_file( 't/files/kitten.jpg'  , { binmode => ':raw' }) )
                                               },
                                          $memory_user,
                                          { tagged_doc_type => 'HR',
                                            board_nice_name => 'Some nice Name. Who cares'
                                        }) , "Ok got XML doc from candidate");
}

done_testing();

{
    package QueuableO;
    sub new{ return bless {} , shift; }
    sub enqueue{return 1};
}

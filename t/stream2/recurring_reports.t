#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use DateTime;

use Redis;
use Stream2;

use Email::Sender::Simple;
use Test::MockModule;

# use Log::Any::Adapter qw/Stderr/;

my $qurious_mock = Test::MockModule->new( 'Qurious' );


my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file:'".( $config_file || 'UNDEF')."'";
}

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $redis = Redis->new( server => 'whatever' , no_auto_connect_on_new => 1 );

ok( my $stream2 = Stream2->new({ config_file => $config_file , redis => $redis }) , "Ok can build stream2");
$stream2->stream_schema()->deploy();

# # Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");
$memory_user->contact_email('bla@bla.com');
$memory_user->save();

my $now = DateTime->now();

ok( $stream2->factory('RecurringReport') , "Ok got recurring report factory");
ok( my $report = $stream2->factory('RecurringReport')->create({
    next_run_datetime => $now,
    class => 'Stream2::Role::Report::Void',
    settings => { foo => 'bar' }
}) , "Ok can create a report");


ok( $stream2->factory('RecurringReport')->due_reports()->count(), "Ok got due_reports");

{
    $qurious_mock->mock( 'create_job' => sub{
                             my ($self, %options) = @_;
                             is( $options{parameters}->{report_id} , $report->id() );
                             return FakeJob->new();
                         });
    is( $stream2->factory('RecurringReport')->fire_due_reports() , 1 , "Ok fired one report");
}


# Adds the user to this report.
$report->add_to_users($memory_user->o());

my $original_next_rundatetime = $report->next_run_datetime();
ok( $report->does('Stream2::Role::Report::Void') , "Ok report does the good role" );
ok( $report->run_this() , "Ok can run this report");
cmp_ok( $report->next_run_datetime() , '>' , $original_next_rundatetime , "Ok next run datetime is now in the future");

my @deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries)  , 1 ,  "Ok 1 mail delivery has happened");


done_testing();


{
    package FakeJob;
    sub new{ bless {} , shift; };
    sub enqueue{ 1 }
    1;
}

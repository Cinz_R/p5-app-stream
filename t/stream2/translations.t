use Test::Most;
use Stream2;

# All compiled language translations under share/translations/compiled should
# be reported as a supported language.

my $s2 = Stream2->new( { config_file => 't/config/app-stream2.conf' } );
my $supported_langs = $s2->translations->supported_languages;

my $share_dir = $s2->shared_directory;
my @lang_dirs = split /\n/, `ls ${share_dir}translations/compiled`;
is_deeply
    [ sort @{$supported_langs} ],
    [ sort @lang_dirs ],
    'All compiled language translations are available for use';


for my $method (qw(__ __x __n __nx __xn __p __px __np __npx)) {

    is( $s2->translations()->$method(''),'', "translation: empty string should be empty with $method and not the PO header");
    is( $s2->template_translations()->$method(''),'', "tenplate-translation: empty string should be empty with $method and not the PO header");

}

done_testing();

#! perl -w

use strict;
use Test::Most;

# use Log::Any::Adapter qw/Stderr/;

use DateTime;
use Test::Stream2;
use Stream2::Criteria;

use File::Copy qw//;

{
    package TestReportClass;
    use Moose;
    has 'user_object' => ( is => 'ro', required => 1);
    has 'stream2' => ( is => 'ro', isa => 'Stream2', required => 1);
    with qw/Stream2::Role::Report::CompanyActivity/; # What we want to test.
    1;
}

my $now = DateTime->now();
my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();

my @destinations = ( 'talentsearch', 'fancyboard' );

# Time to create some users
my $N_users = 20;
my @users = do{
    my $ts_subscriptions_ref = {
        talentsearch => {
            nice_name   => "Talentsearch",
            auth_tokens => {
                talentsearch_username => 'smd',
                talentsearch_password => 'smdpass'
            },
            type               => 'internal',
            allows_acc_sharing => 1,
        },
        fancyboard => {
            nice_name => 'Fancy Board',
            auth_tokens => {},
            type => 'external',
            allows_acc_sharing => 1,
        }
    };
    my $search_user_factory = $stream2->factory('SearchUser');
    my $companies_users;
    {
        for( my $i = 0; $i < $N_users ; $i++){
            my $company_name = 'acme'.( $i % 3 );
            my $memory_user = $search_user_factory->new_result({
                provider => 'dummy',
                last_login_provider => 'dummy',
                provider_id => 31415 + $i,
                group_identity => $company_name,
                timezone => (0 == $i % 6) ? 'Sol/Earth' : 'Sol/Mars',
                subscriptions_ref => (0 == $i % 2) ? $ts_subscriptions_ref : {},
            });
            my $user_id = $memory_user->save();
            # generate a list of company users
            push @{$companies_users->{$company_name}},
                {
                    id => 31415 + $i,
                    name => "$user_id-$company_name",
                    email => "$user_id\@$company_name.com",
                    company => $company_name
                }
            }
    }
    $search_user_factory->all();
};

# Criterias
my @criterias = ();
{
    my $criteria = Stream2::Criteria->new( schema => $stream2->stream_schema() );
    $criteria->add_token('keywords' , 'foo' );
    $criteria->save();
    push @criterias , $criteria;
    $criteria = Stream2::Criteria->new( schema => $stream2->stream_schema() );
    $criteria->add_token('keywords' , 'bar' );
    $criteria->save();
    push @criterias , $criteria;
}

#
# Search records
# 2200 over 3 months = one per hour
#
my $N_search_records = 400;
#my $N_search_records = 2200;
my @search_records = ();
{
    my $date = $now->clone->subtract( months => 3 );
    for( my $i = 0; $i < $N_search_records; $i++ ){
        my $user = $users[ $i % $N_users ];
        my $search_record = $stream2->stream_schema()->resultset('SearchRecord')->create({
            criteria_id     => $criterias[ ( $i + int(rand(2)) ) % 2 ]->id(),
            user_id         => $user->id,
            board           => $destinations[ (  $i + int(rand(2)) ) % 2 ],
            remote_ip       => '127.0.0.1',
            n_results       => 10 + int(rand(10000)),
            insert_datetime => $date->iso8601(),
        });
        $date->add(minutes => 30);
        push @search_records , $search_record;
    }
}

# All the actions mentionned in
# https://broadbean.atlassian.net/wiki/display/SEAR/New+Search+Reports
# report #1
my @actions = qw(profile download import shortlist_adc_shortlist longlist_add forward message candidate_tag_add update);
my $N_actions = scalar @actions;
# 4400 over three months = one per half hour
# my $N_action_logs = 4400;
my $N_action_logs = 1000;
{
    my $date = $now->clone->subtract( months => 3 );
    for( my $i = 0 ; $i < $N_action_logs; $i++ ){
        my $search_record = $search_records[ $i % $N_search_records ];
        my $user =  $search_record->user();
        my $destination = $search_record->get_column('board');
        $stream2->factory('CandidateActionLog')->create({
            action => $actions[int( $i % $N_actions)],
            user_id => $user->id(),
            destination => $destination,
            group_identity => $user->group_identity(),
            data => {},
            candidate_id => $destination.'_'.$i,
            insert_datetime => $date->iso8601(),
        });
        $date->add(minutes => 30);
    }
}

## Now that the data is populated, time to test the report.

ok( my $test_report = TestReportClass->new({
    stream2 => $stream2,
    from_datetime => $now->clone()->subtract( months => 3 ),
    to_datetime => $now->clone()->subtract( months => 2 ),
    user_object => $users[0],
}), "OK can build report");

my $aggregation = $test_report->generate_aggregation();
is( ref( $aggregation->{dummy}->{ $users[0]->group_identity() }->{$users[0]->id()} ) , 'HASH' , "Ok good data structure" );
ok( $aggregation->{_meta}->{min_action_id} );
ok( $aggregation->{_meta}->{max_action_id} );
is( $aggregation->{_meta}->{path_length} , 4 );
is( $aggregation->{dummy}->{ $users[0]->group_identity() }->{$users[0]->id()}->{fancyboard}->{_meta_bucket} , 1 , "Ok got bucket");
is( $aggregation->{dummy}->{ $users[0]->group_identity() }->{_meta_bucket} , 0 , "Ok got no bucket");

ok( my $table = $test_report->generate_table({ aggregation => $aggregation }) );
# use Data::Dumper;
# diag(Dumper( $table ));

my @files = @{ $test_report->generate_files({ aggregation => $aggregation }) };
foreach my $file ( @files ){
    ok( $file->name() );
    ok( -e $file->disk_file() );
    # File::Copy::cp( $file->disk_file(), 'sandbox/nogit/report1.xlsx' );
    # diag( "Preview: open sandbox/nogit/report1.xlsx");
}

ok(1);

done_testing();

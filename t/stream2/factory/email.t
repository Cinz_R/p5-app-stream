#! perl -w

use Test::Most;
use Email::Abstract;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

use Test::Stream2;

my $config_file = 't/app-stream2.conf';
ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ),
    "Ok can build stream2" );
ok( $stream2->deploy_test_db, "Ok can run deploy" );

my %email_addresses = (
    sen            => 'Sen@Yubabas_wash_house.com',
    kamaji         => 'Kamaji@Yubabas_wash_house.com',
    lin            => 'Lin@Yubabas_wash_house.com',
    haku           => 'Haku@Yubabas_wash_house.com',
    yubabas        => 'Yubabas@Yubabas_wash_house.com',
    yubabas_broken => 'Yubabas@Yubabas_wash_house.cmo',
    zeniba         => 'Zeniba@swamp_bottom_mail.net',
);

my $email = Email::Simple->create(
    header => [
        To      => $email_addresses{sen},
        From    => $email_addresses{haku},
        Subject => 'You Name',
        Bcc     => join( ';', $email_addresses{kamaji}, $email_addresses{lin} ),
    ],
    body =>
'Do not forget your real name Chihiro Ogino, once you do you will not be able to save your family.'
);

#  test to make sure that Stream2 can still send emails
ok( $stream2->send_email($email), "Ok can send_email" );

my @deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries), 3,
    "Ok 3 deliveries (one for the To, and 2 for the BCCs" );
is( $deliveries[0]->{envelope}->{to}->[0], $email_addresses{sen} );
is( $deliveries[1]->{envelope}->{to}->[0], $email_addresses{kamaji} );
is( $deliveries[2]->{envelope}->{to}->[0], $email_addresses{lin} );

# clear
clear_deliveries();

# lets start testing the factory.

my $email_factory = $stream2->factory('Email');
$email_factory->send($email);

@deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries), 3,
    "Ok 3 more deliveries (one for the To, and 2 for the BCCs" );
is( $deliveries[0]->{envelope}->{to}->[0], $email_addresses{sen} );
is( $deliveries[1]->{envelope}->{to}->[0], $email_addresses{kamaji} );
is( $deliveries[2]->{envelope}->{to}->[0], $email_addresses{lin} );
clear_deliveries();

{
    my @emails =
      $email_factory->_extract_email_addresses( Email::Abstract->new($email) );
    is_deeply(
        \@emails,
        [
            $email_addresses{sen}, $email_addresses{kamaji},
            $email_addresses{lin}
        ],
        "extracted email addresses"
    );
}

# now to test the blacklisting functions.
my $abstracted_email = Email::Abstract->new($email);
is(
    undef,
    $email_factory->email_blacklisted($abstracted_email),
    "There are no blacklisted email addresses"
);

my $ebl = $stream2->factory('EmailBlacklist');
for my $email_to_blacklist ( $email_addresses{sen}, $email_addresses{kamaji} )
{    # lets add one the To to the blacklist.

    my $blacklised_email_record = $ebl->create(
        {
            email   => $email_to_blacklist,
            purpose => 'spam'
        }
    );
    is( $email_factory->email_blacklisted($abstracted_email)->email,
        $email_to_blacklist, "$email_to_blacklist is blacklisted" );
    $blacklised_email_record->delete;

    is( $email_factory->email_blacklisted($abstracted_email),
        undef, "There are no blacklisted email addresses" );
}

# lets create a email with a type in the domain name to test the email_valid

is( $email_factory->email_valid($abstracted_email), 1, 'is a valid email' );

my $broken_email = $email_factory->build(
    [
        From => $email_addresses{zeniba},
        To   => $email_addresses{yubabas_broken},
        Encoding => "quoted-printable",
        Subject  => Encode::encode( 'MIME-Q', 'My Golden stamp' ),
        Data     => [ Encode::encode( 'UTF-8', 'I would like it back.' ) ]
    ]
);

is(
    $email_factory->email_valid(
        Email::Abstract->new( $broken_email->entity )
    ),
    0,
    'is not a valid email'
);

# now lets stringify the email, then try and parse the email
# once we have fixed the typo

my $stringiefied_email = $broken_email->entity->as_string;
$stringiefied_email =~ s/cmo/com/;

my $fixed_email = $email_factory->parse($stringiefied_email);
is(
    $email_factory->email_valid( Email::Abstract->new( $fixed_email->entity ) ),
    1,
    'is now a valid email'
);

$email_factory->send($fixed_email);

@deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries), 1, "Ok 1 email was sent" );
is( $deliveries[0]->{envelope}->{to}->[0], $email_addresses{yubabas} );
clear_deliveries();

dies_ok( sub { $email_factory->send($broken_email) },
    'sending a broken email should die' );

@deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries), 0, "No emails were sent" );
clear_deliveries();

sub clear_deliveries {
    Email::Sender::Simple->default_transport->clear_deliveries;
}

done_testing();

#! perl -w

use Test::Most;

use Test::Mojo::Stream2;
use Test::MockModule;

# TEST SETUP
my $t = Test::Mojo::Stream2->new();
$t->login_ok();

my @tests = (
    {
        in => {
            options => [
                [ "sausages", "1" ], ["vegan sausages", "2"]
            ]
        },
        out => {
            options => [
                [ "sausages", "1" ], ["vegan sausages", "2"]
            ]
        },
        label => "scalar labels"
    },
    {
        in => {
            options => [
                [ { x => [ "sausage and {what}", "what", "beans" ] }, "1" ], [ "no translation", "2" ]
            ]
        },
        out => {
            options => [
                [ "sausage and beans", "1" ], [ "no translation", "2" ]
            ]
        },
        label => "translation with dynamic segment"
    },
    {
        in => {
            options => [
                [ { n => [ "sausage", "sausages", 2 ] }, "1", ],
                [ { x => [ "{this} and {that}", "this", "sausage", "that", "beans" ] }, "2" ],
                [ { nx => [ "{n} sausage", "{n} sausages", 2, "n", 2 ] }, "3" ],
                [ "vegan sausage", "4" ]
            ]
        },
        out => {
            options => [
                [ "sausages", "1" ],
                [ "sausage and beans", "2" ],
                [ "2 sausages", "3" ],
                [ "vegan sausage", "4" ]
            ]
        },
        label => "mixture of label translation types"
    }
);

my $s2 = $t->app->stream2;
foreach my $test ( @tests ){
    my $setting = $s2->factory('Setting')->new_result({ setting_meta_data => $test->{in} });
    is_deeply( $setting->setting_meta_data, $test->{out}, $test->{label} );
}

done_testing;

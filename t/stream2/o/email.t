#! perl -w
use strict;
use warnings;

use FindBin;
use Test::More;

use Test::Stream2;

use Encode;
use MIME::Entity;

use Stream2::O::Email;

use File::Slurp;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'"
      . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ),
    "Ok can build stream2" );
ok( $stream2->stream_schema(), "Ok got schema" );

# Deploying schema
$stream2->deploy_test_db;
my $email_factory = $stream2->factory('Email');
{
    # A very simple single part entity
    my $mime = Stream2::O::Email->new(
        {
            factory => $email_factory,
            entity  => MIME::Entity->build(
                Type     => 'text/html; charset=UTF-8',
                Encoding => 'quoted-printable',
                Data     => [
                    Encode::encode(
                        'UTF-8',
                        "<html><body>Some html \N{U+00CA} Kitten</body></html>"
                    )
                ]
            )
        }
    );
    my $identity = $mime->transform_entities( sub { return shift; } );
    is( $identity->stringify(),
        $mime->stringify(), "Ok same strings for both entities" );

    # Transform by adding one header
    my $transformed = $mime->transform_entities(
        sub {
            my ($s2_entity) = @_;
            if ( $s2_entity->effective_type() !~ /html/ ) {

                # Not html leave as it is.
                return $s2_entity;
            }

            # This is html. Generate an multipart/alternative.
            my $container =
              MIME::Entity->build( Type => 'multipart/alternative' );
            $container->add_part( $s2_entity->entity() );
            $container->attach(
                Type        => 'text/plain; charset=UTF-8',
                Encoding    => 'quoted-printable',
                Disposition => 'inline',
                Data        => [
                    Encode::encode(
                        'UTF-8',
                        "Stop living in the past and get an HTML client."
                    )
                ]
            );
            return Stream2::O::Email->new(
                { entity => $container, factory => $email_factory } );
        }
    );
    like( $transformed->stringify(), qr/Stop living in the past/ );
}

{
    # A very simple single part html entity, with kitten source innit.
    my $mime = Stream2::O::Email->new(
        {
            factory => $email_factory,
            entity  => MIME::Entity->build(
                Type     => 'text/html; charset=UTF-8',
                Encoding => 'quoted-printable',
                Data     => [
                    Encode::encode(
                        'UTF-8',
"<html><body><img src='http://kitten.com/'></img>Some html \N{U+00CA} Kitten</body></html>"
                    )
                ]
            )
        }
    );
    my $transformed = $mime->resolve_images(
        sub {
            my ($url) = @_;
            is( $url, 'http://kitten.com/' );
            return {
                local_file => 't/files/kitten.jpg',
                mime_type  => 'image/jpeg'
            };
        }
    );

    # my $string_mime = $transformed->stringify();
    # File::Slurp::write_file( '/tmp/emails/new/bla.eml' , $string_mime );
    is( $transformed->effective_type(), 'multipart/related' );
    my @parts = $transformed->parts();
    is( $parts[0]->effective_type(), 'text/html' );
    is( $parts[1]->effective_type(), 'image/jpeg' );
}

{
    ## A multipart/mixed content . Meaning you can attach files to this main mime
    my $mime = Stream2::O::Email->new(
        {
            factory => $email_factory,
            entity  => MIME::Entity->build(
                Type => 'multipart/mixed',
            )
        }
    );

    # The container for alternative text or html. We attach this alternative to
    # provide a text/plain and a text/html parts.
    my $container = $mime->attach( Type => 'multipart/alternative' );

    # text version of candidate html body
    $container->attach(
        Type        => 'text/plain; charset=UTF-8',
        Encoding    => 'quoted-printable',
        Disposition => 'inline',
        Data => [ Encode::encode( 'UTF-8', "Some raw \N{U+00CA}text kitten" ) ]
    );

    # html version of candidate html body
    $container->attach(
        Type     => 'text/html; charset=UTF-8',
        Encoding => 'quoted-printable',
        Data     => [
            Encode::encode(
                'UTF-8',
                "<html><body>Some html \N{U+00CA} kitten</body></html>"
            )
        ]
    );
    my $identity = $mime->transform_entities( sub { return shift; } );
    is( $identity->stringify(),
        $mime->stringify(), "Ok same strings for both entities" );

    ## Transform the HTML bit for an Auto text alternative.
    my $transformed = $mime->transform_entities(
        sub {
            my ($s2_entity) = @_;
            if ( $s2_entity->effective_type() !~ /html/ ) {

                # Not html leave as it is.
                return $s2_entity;
            }

            # This is html. Generate an multipart/alternative.
            my $container =
              MIME::Entity->build( Type => 'multipart/alternative' );
            $container->add_part( $s2_entity->entity() );
            $container->attach(
                Type        => 'text/plain; charset=UTF-8',
                Encoding    => 'quoted-printable',
                Disposition => 'inline',
                Data        => [
                    Encode::encode(
                        'UTF-8',
                        "Stop living in the past and get an HTML client."
                    )
                ]
            );
            return Stream2::O::Email->new(
                { entity => $container, factory => $email_factory, } );
        }
    );

    my $string_mime = $transformed->stringify();

    # File::Slurp::write_file( '/tmp/emails/new/bla.eml' , $string_mime );
    like( $string_mime, qr/text kitten/ );
    like( $string_mime, qr/Stop living in the past/ );

    # diag($string_mime);
}

{
    ## A multipart/mixed content with a kitten bearing HTML version
    my $mime = Stream2::O::Email->new(
        {
            factory => $email_factory,
            entity  => MIME::Entity->build(
                Type    => 'multipart/mixed',
                Subject => 'Check this kitten',
                From    => 'jerome@broadbean.com',
                To      => 'patrick@broadbean.com',
            )
        }
    );

    # The container for alternative text or html. We attach this alternative to
    # provide a text/plain and a text/html parts.
    my $container = $mime->attach( Type => 'multipart/alternative' );

    # text version of candidate html body
    $container->attach(
        Type        => 'text/plain; charset=UTF-8',
        Encoding    => 'quoted-printable',
        Disposition => 'inline',
        Data => [ Encode::encode( 'UTF-8', "Some raw \N{U+00CA}text kitten" ) ]
    );

    # html version of candidate html body
    $container->attach(
        Type     => 'text/html; charset=UTF-8',
        Encoding => 'quoted-printable',
        Data     => [
            Encode::encode(
                'UTF-8',
"<html><body><img src='http://kitten.com/'></img>Some html \N{U+00CA} kitten</body></html>"
            )
        ]
    );
    my $transformed = $mime->resolve_images(
        sub {
            my ($url) = @_;
            is( $url, 'http://kitten.com/' );
            return {
                local_file => 't/files/kitten.jpg',
                mime_type  => 'image/jpeg'
            };
        }
    );

# Check the structure.
# my $string_mime = $transformed->stringify();
# File::Slurp::write_file( '/tmp/emails/new/bla.eml' , $transformed->stringify() );
    is( $transformed->effective_type(), 'multipart/mixed' );
    my @parts = $transformed->parts();
    is( $parts[0]->effective_type(), 'multipart/alternative' );
    my @subparts = $parts[0]->parts();
    is( $subparts[0]->effective_type(), 'text/plain' );
    is( $subparts[1]->effective_type(), 'multipart/related' );
    is( ( $subparts[1]->parts() )[0]->effective_type(), 'text/html' );
    is( ( $subparts[1]->parts() )[1]->effective_type(), 'image/jpeg' );
}

done_testing();

#! perl -w

use strict;
use warnings;
use Test::Most;
use Test::Stream2;

#use Log::Any::Adapter qw/Stderr/;

my $config_file = 't/app-stream2.conf';
my $stream2 = Test::Stream2->new({ config_file => $config_file });
$stream2->deploy_test_db();


my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'dev',
    settings => {
        "auto_download_cvs_on_preview" => JSON::XS::false,
        "some_other_random_setting" => JSON::true,
        'stream' => {
            'forward_to_tp2' => '0'
        },
    }
});
$memory_user->save();



my $shortlist_flavour = $stream2->stream_schema->resultset('TagFlavour')->create({
    group_identity => $memory_user->group_identity(),
    name           => 'shortlist'
});
$shortlist_flavour->discard_changes();

my @tests = (
    {
        tag_name    => 'sausage',
        tag_label   => 'sausage',
    },
    {
        tag_name    => 'sausage',
        tag_label   => 'sausage',
        flavour_id  => $shortlist_flavour->id
    },
    {
        tag_name    => ';f;f;3dU*?;_-+H94fh849iun(UH(UH$(Uhn;a',
        tag_label   => ';f;f;3dU*?;_-+H94fh849iun(UH(UH$(Uhn;a',
    },
    {
        tag_name    => ';f;f;3dU*?;_-+H94fh849iun(UH(UH$(Uhn;a',
        tag_label   => ';f;f;3dU*?;_-+H94fh849iun(UH(UH$(Uhn;a',
        flavour_id  => $shortlist_flavour->id
    },
    {
        tag_name    => 'go_away;;test',
        tag_label   => 'go_away;;test',
    },
    {
        tag_name    => 'go_away;;test',
        tag_label   => 'go_away;;test',
        flavour_id  => $shortlist_flavour->id
    }
);

# Fake candidate ID generator
my $cand_id = 0;

foreach my $test ( @tests ){

    my $label = delete( $test->{tag_label} );
    my $tag = $stream2->stream_schema()->txn_do(
        sub{
            return $stream2->factory('Tag')->find_or_create({
                group_identity => $memory_user->group_identity(),
                tag_name_ci => 'super ridiculous',
                %$test
            });
        });

    is( $tag->tag_label, $label, "tag label is as expected" );
    $tag->discard_changes();
    is( $tag->tag_name_ci() , lc( $tag->tag_name() ), "Ok good tag_name_ci, as trigger did override the 'super ridiculous'");

    # Tag an imaginary candidate with it.
    $stream2->factory('CandidateTag')->create({ group_identity => $memory_user->group_identity(),
                                                user_id  => $memory_user->id(),
                                                candidate_id => 'blabla_'.$cand_id++,
                                                tag => $tag->o() });
}

done_testing();

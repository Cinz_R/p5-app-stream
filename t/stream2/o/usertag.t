#! perl -w

use strict;
use warnings;
use Test::Most;
use Test::Stream2;

# use Log::Any::Adapter qw/Stderr/;

my $config_file = 't/app-stream2.conf';
my $stream2 = Test::Stream2->new({ config_file => $config_file });
$stream2->deploy_test_db();


my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'dev',
    settings => {
        "auto_download_cvs_on_preview" => JSON::XS::false,
        "some_other_random_setting" => JSON::true,
        'stream' => {
            'forward_to_tp2' => '0'
        },
    }
});
$memory_user->save();


my @tests = (
    {
        tag_name    => 'sausage',
        tag_label   => 'sausage',
    },
    {
        tag_name    => ';f;f;3dU*?;_-+H94fh849iun(UH(UH$(Uhn;a',
        tag_label   => ';f;f;3dU*?;_-+H94fh849iun(UH(UH$(Uhn;a',
    },
    {
        tag_name    => 'go_away;;test',
        tag_label   => 'go_away;;test',
    },
);

foreach my $test ( @tests ){

    my $label = delete( $test->{tag_label} );
    my $tag = $stream2->stream_schema()->txn_do(
        sub{
            return $stream2->factory('Usertag')->find_or_create({
                user => $memory_user->o(),
                tag_name_ci => 'super ridiculous',
                %$test
            });
        });

    is( $tag->tag_name, $label, "tag name is as expected" );
    $tag->discard_changes();
    is( $tag->tag_name_ci() , lc( $tag->tag_name() ), "Ok good tag_name_ci, as trigger did override the 'super ridiculous'");
}

done_testing();

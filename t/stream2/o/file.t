#! perl -w

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}

use LWP;
use LWP::UserAgent::Mockable;


use Test::Most;

use Stream2;
use Stream2::O::File;

use MIME::Base64;

my $config_file = 't/app-stream2.conf';
my $stream2 = Stream2->new({ config_file => $config_file });

my $base64_content = do{ local $/ = undef, <DATA>; };

{
    my $file = Stream2::O::File->new({ name => 'kitten.jpg',
                                       mime_type => 'image/jpeg',
                                       binary_content => MIME::Base64::decode_base64( $base64_content ) });
    ok( my $html = $file->html_render() );

    ok( my $disk_file = $file->disk_file() );
    ok( -e $disk_file );

    $file = undef;
    ok( ! -e $disk_file );
}
{
    # Now a pure html file.
    my $file = Stream2::O::File->new({ name => 'bla.html',
                                       mime_type => 'text/html',
                                       binary_content => '<html><body><p>Hello</p></body></html>'
                                   });
    ok( my $html = $file->html_render() );
    is( $html , q|<div><div style="position: relative;"><tk_metadata name="last_modified" value=""> </tk_metadata><p> </p><p style="text-align:left;margin-top:0.00in;margin-bottom:0.00in;"><br/><span style="font-family:'Times New Roman';font-size:12pt;">Hello</span></p><br/> </div></div>| , "Ok got good html snippet (no html full doc)" );
}

LWP::UserAgent::Mockable->finished();
done_testing();

# Below lives a kitten
__DATA__
/9j/4AAQSkZJRgABAQAAAQABAAD//gA8Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcg
SlBFRyB2NjIpLCBxdWFsaXR5ID0gMTAwCv/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB
AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEB
AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/AABEIADIA
MgMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQD
BQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygp
KjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJma
oqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/
xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQID
EQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RF
RkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqy
s7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/
AP7+KKKjmlWCGWZgxWKN5GCKzuVRSx2ogZnbAOFVWZjwASQKAPmb9qD4ma14M8FW2ieCNWi0vxr4
s1WHS7XVlRLl/DWj2kT6r4h10xPFPALqHSrWSz0yO4jYy6jqFrJFBdCCSFvn7wF+1f8AEN9H0e11
3TNN16Y2Nzqt14gjtZYHm0S2eWK1kmSzf7C+q6pNCYLa1to45TG32iS3ykjN82fGzV9R1zWfiV8R
rzxPqOieEbCx1LWNWvpLY3LaJovha11e9TU9OuI4LOSeOwgjkuZdMMM8s/lGGcHzWgOP+wt+0L8J
v2if2bfCHxr+Fc+saz4E8aar4xttP1G80K400NL4d8S6rpmqRRWd0pla2bXdL1aGNlclyLnLGRSI
/LnXneWIXtHSU40lyN8kXJOUVJ7c8knK1rq6WqtzerGVP2dLAuGHVROpiOaVOHt5t8kJN1Le19jF
csY0+b2XNCdRR53Nn6A3X7X+i2VrbBPBfiLW9Tn3NJaaObQQ28cEK3V08st3NHIJYrVl2W6QyGS7
IsRKJ/MEX2JFIs0UcqHKSokin/ZdQw/Q1+KPgf4wfDv4neJfiL4m+Emv+GvGeleGvEN54a1WLw1q
dtrbWPiTRI4INW0IXFpJNbW2ppKby61Cxll3WgEQ+zrdTziv1p+E3iC78SeB9I1C+t3trhYhbNFJ
gSBYERFEgHyh1H7tgOAyHk9a9CPMnyzcr2V4zioyjJJcyaSTTV1fm62tbryVVSdGnKnCCfPKUqsK
kpxqwqKPs1FXcFGDjO0oay5/ebSio+k0UUVocoUyVBLFJESQJI3QkdQHUqSPcZyKeTgEnoOTXzV+
0J8bdL+H3hTUdO0jU428V6pbi000Wk0Eklg12xtzeSP5pFtPCXVrYTRuHleJ/LeJZSkznGEXKT0S
2vq/JeZUIylJRindvp08/RH5SftL/Ey8/Z68SeMfD+uXpvPDOnatHqd/cJc6Zdx6l4fv5JLrVbXU
9Ru40SyntdNdI5NPhghuZi5hSVJZVnPO+Cf2rPA3hHxv8MfAvgvxZ8L/AAh8KLzw3/aml+BNJ0LQ
NChutKu5kubC58NWMEMED2uu22ptLbLpU9s8cFnf37Wt2LkyVm/E3xKfFN1Z6fqPhzTPFMd5rkiR
Q3dhd6hPq1kILpNSu9TupdKuYC17JBd2NlYo8H2y/s55LOSJ7JGjwPiz+xzofxT+BumXvwf8TS+A
fGq6La+IfDl5Gx1H4Y+ItaihgvLi18VeFbC7EGoadqoW6tb+90yaDVLBbhtWsL4X1pCj+XRynCY6
tQpVMzqZXKrXjy1K3tqmBXKpcntfYRqVqcnKah7RUasYwnKXuNOS9jD55jMjeKxlLKMJnMZZfi8H
OhUw+Dr4uMcXCNGpXwtPME8MsRQipV6FSFWhXpVqcJUpykuV+xfBD4D/ALPX7KuoX/w9/Za8EQeF
fC3xN8Q+KfiPe6Fa6prniS71bx34wuIZ9TurW81/UdSuG04SwCwt7KC5SPS7awtbWzhxG+P2P+Df
hjVvCXgDRNJ124gudW8o3N9JBZpZKLi4w8iNFGzIXDZLOp2lmIXKhWb8NP8Agnj8VPD3jfwB8DPG
GravpFv4u1jz/C9xaM8jQeGdZ+Heq3HhTxLYxXmpXF3fz3z6hps0yy3ENjJrEN02rRW8DX5hj/oc
UhlUggggEEcggjqCOMHtXtYvAYrLswxuDx0pPGYTEVcNiVKTlavRl7GqrvdRlScYte64q8W1Y8PC
4/DZhluBxeBilgsZh6WKwrUVDmw9aCrUpW6OUavNJfFfSWqYtFFFZlEF0u63mXdIuY35iIEgGDnY
W4DY6E4we461/OL+278VLz4dSeIbmfUTpYg1FTHrjWSzzWMVzqWmxLfR2s8trc3eow3GpRRefFcy
S2pklvp1t7W0llr+j9gSrAYyVIGemSMDOOcZ61+Mf7Zv7J3xA8ZC/wDJ8F3XxG0TVTdzXLeHLOCf
VLRRcWN3FCLG71G1up18yOZVWPUreWUGfFzHHKYq5MTBydN2k0ubmavZJ8vRX311aa01t16sLOMX
JO13ytLS7SbulfRvVO19ei3t8KfDbxjdan4f8CeIr+41FNG8SX1teXLacYY57zxJd4s9PtrJr6dW
h086fD4g1eOH7Uk5+0JYWcolBml7r4//ABc8e/Dn4DXuk+GrC/ufFuhWfhq90fw7o8NsNajtJb3S
9PvPC2mxWYit9RXUpZrzTbS0tbNJ5ILrdYQvf28EEvmnwS+G3xb+HHg2L4c+OPAOu6JaWkWpXtpo
+u6Oy6tZ6bBf3B0+xtY9KW2VrhbHTYoQtvdw3Wr3Ec1smoxCaS8k9AufD/jn4j3kXjCXwb4x+0/D
66v41sJdAvUl1Wz1HS9Jsb27l062hkuY9X0+bUtYufDzSubiwgvL/TG0o3gs7ePmniFhsLia1Ol9
Yq4eE6lGimk5ShZrler0XvNRvJ2ainPlT+h4ey/D5rxFlOXY/G08ty/HY3DYXFZhUjz0cPTrVIwT
qNtU1zyap89WUKUOdSrVIUozkvLf2OPDvjH4C6lB4w/aB8FT6fpeoeNbnUrHTvCa2+pm1vdZuLCJ
NSu7SKUR2VnrE8q3UEM6Q31l5N2NUt4rtrO3uf6ifCPinQ/GvhrR/FHhy9S/0XWbKK7sblAQSjDa
8UqH5op4JVeCeJhujljdD0yfxc8OfDP4veONPh+x/D7xtNDKlraz32saTdabFOLxkgS4FzqUNqLh
Y3kV7qeFJFs03XN0IoFaVf1z+C/gRPht8OtA8Hiwh0+XTI7h7uOG6N79ovru5luru8ku2VDPLczS
tIzCOJFBWKOKKKNI0qHEedcUZvjc3zOjQh9ZpUp1Z08NVw3PiY8tNTpxk5Rkp04Sda0laqlJX52l
9Vxv4d8AeG3D2TZDwvmOMxWYYfG4mjDDSzPC5pRwuVwpRqSpYmdNxr4etDEVqTwTnF+2w9SrCSis
NTb9Tooor0j8qCiiigCrPbW00kEk1vBLJCzGJ5Yo5HiLbQxjZ1LIWAAYqQSAAelWdq4xgYHQYGB3
/nRRSW8vX9EAtFFFMAooooA//9k=

use strict;
use warnings;

use Test::Most;
use aliased 'Stream2::Results::Result';
use Email::Valid;

### tests to prove that names in Email addresses are properly quoted. I.e.
### if display-name in...
###
###     display-name <msgbox@domain>
###
### ...contains any punctuation/sensitive chars, according to RFC 2822, it
### should be quoted thus:
###
###     "display-name" <msgbox@domain>

my $CANDIDATE_EMAIL_ADDR = 'jdoe@example.com';

my @tests = (
    {
        candidate => Result->new( {
            name  => "Jon Doe",
            email => $CANDIDATE_EMAIL_ADDR
        } ),
        expected  => Encode::encode( 'MIME-Q', "Jon Doe" ). " <$CANDIDATE_EMAIL_ADDR>",
        comment   => '"Jon Doe" is just word chars and spacing: needn\'t '
                  .  'be quoted'
    },
    {
        candidate => Result->new( {
            name  => "Jon P. Doe",
            email => $CANDIDATE_EMAIL_ADDR
        } ),
        expected  => Encode::encode( 'MIME-Q', '"Jon P. Doe"' ). " <$CANDIDATE_EMAIL_ADDR>",
        comment   => 'Punctuation (period) in display-name: must be quoted'
    },
    {
        candidate => Result->new( {
            name  => $CANDIDATE_EMAIL_ADDR,
            email => $CANDIDATE_EMAIL_ADDR
        } ),
        expected  => Encode::encode( 'MIME-Q', '"'.$CANDIDATE_EMAIL_ADDR.'"' ). " <$CANDIDATE_EMAIL_ADDR>",
        comment   => 'email address (containing "@" char) as display-name: '
                  .  'must be quoted'
    }
);

for my $test ( @tests ) {
    my $mime_entity = $test->{candidate}->build_target_entity;
    my $to_header   = $mime_entity->head->get('to');
    chomp $to_header;
    is( $to_header, $test->{expected}, $test->{comment} );
    ok( Email::Valid->address( $to_header ), 'Email::Valid likes it too' );
}

done_testing();
#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Mock::Redis;
use Test::MockModule;

use JSON;

use Stream2;

use Stream2::Results::Store;
use Stream2::Results::Result;


my $config_file = 't/app-stream2.conf';
my $stream2 = Stream2->new({ config_file => $config_file });

my $candidate = Stream2::Results::Result->new();

my $redis = Test::Mock::Redis->new();

{
    # Checking result storage with NO search_record_id
    my $storage = Stream2::Results::Store->new({ store => $redis });

    my $has_pushed_candidate = 0;
    my $mock_redis = Test::MockModule->new( ref( $storage->store() ) );
    $mock_redis->mock( rpush => sub{
                           my ($self, $key , $data ) = @_;
                           $data = JSON::decode_json( $data );
                           $has_pushed_candidate = 1;
                           is( $data->{search_record_id}  , undef );
                       });

    ok( $storage->add_candidate( $candidate ) , 'Ok can add candidate' );
    is( $candidate->search_record_id() , undef , 'Ok the search_record_id is still undef');
    ok( $has_pushed_candidate  , 'Ok has pushed candidate' );
    ok(my $candidate_ref = $candidate->to_ref(), "Ok can call to_ref on a candidate");
    is_deeply( $candidate_ref->{recent_actions} , [] , "Ok no recent actions, but the property is there");
    is_deeply( $candidate_ref->{sent_template_ids} , [] , "Ok no sent template IDs but the property is there");
    ok( !exists( $candidate_ref->{future_message} ) , "Ok no future messages");
}

{
    # Checking result storage with WITH a search_record_id
    my $storage = Stream2::Results::Store->new({ store => $redis, search_record_id => 1234 });

    my $has_pushed_candidate = 0;
    my $mock_redis = Test::MockModule->new( ref( $storage->store() ) );
    $mock_redis->mock( rpush => sub{
                           my ($self, $key , $data ) = @_;
                           $data = JSON::decode_json( $data );
                           $has_pushed_candidate = 1;
                           is( $data->{search_record_id}  , 1234 );
                       });

    ok( $storage->add_candidate( $candidate ) , 'Ok can add candidate' );
    ok( $has_pushed_candidate  , 'Ok has pushed candidate' );
}

done_testing();

use Test::Most;
use Data::Dumper;

use Stream2;
use Stream2::Results::Result;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

my @test_values = (
    { result_name => 'fullname', canonical_name => 'name', value => 'Bob Boberson' },
    { result_name => 'home_address', canonical_name => 'address', value => '189 Marsh Wall' },
    { result_name => 'city', canonical_name => 'city', value => 'London' },
    { result_name => 'postcode', canonical_name => 'postal_code', value => 'E14 9SR' },
    { result_name => 'main_telephone_contact_number', canonical_name => 'telephone', value => '0800 085 3061' },
    { result_name => 'past_education', canonical_name => 'education', value => ['aaa','bbb'] },
);


my $defaults = {};
my %canonical_mapping = ();

for my $test_value ( @test_values ) {
    # set the values for the results object
    $defaults->{$test_value->{result_name}} = $test_value->{value};
    # build the canonical value mapping
    $canonical_mapping{$test_value->{canonical_name}} = $test_value->{result_name}
}

ok(
    my $candidate = Stream2::Results::Result->new( $defaults ),
    'Stream2::Results::Result should build'
);
# test with no mappping
dies_ok(
    sub {$candidate->set_canonical_values()},
    'no mapping given'
);
# build the mappings
lives_ok(
    sub {$candidate->set_canonical_values(%canonical_mapping)},
    'mapping given'
);

# now to check the mappings

while (my ($canonical_name, $feed_name) = each %canonical_mapping) {
    # get_attr uses wantarray, we need to make sure what we get back is correct.
    my @canonical_value = $candidate->get_attr('canonical_'.$canonical_name);
    is_deeply(
        $defaults->{$feed_name},
        (1 == scalar @canonical_value ? $canonical_value[0] : \@canonical_value),
        "this should match $feed_name => $canonical_name"
    )
}


# all other canocial names should be undef
foreach my $canonical_name (qw(canonical_cv_text canonical_education_level)) {
       is(
        $defaults->{$canonical_name},
        undef,
        "$canonical_name should be undef"
    )
}

done_testing;

#!/usr/bin/env perl
use Test::Most;

use Email::Valid;
use Stream2::Results::Result;

my $user = TestUser->new({
    group_identity => 'dev'
});
my $result = Stream2::Results::Result->new({
    candidate_id => '12345',
    destination => 'sausage'
});
my $made_up_email = $result->make_email_up( $user );

is ( $result->id, 'sausage_12345', "id is generated as expected" );
is ( $made_up_email, 'unknown_email@sausage-12345-dev.com', "email is as expected" );
ok ( Email::Valid->address( $made_up_email ), "email is valid according to Email::Valid" );

{
    package TestUser;
    sub new { bless pop, shift }
    sub group_identity { shift->{group_identity} }
}

done_testing();

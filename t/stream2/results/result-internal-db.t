#! perl -w

use strict;
use warnings;

use Test::More;
use Test::Exception;
use Test::MockModule;

use Stream2;
use Stream2::Results::Result;

my $stream2 = Stream2->new({ config_file => './t/app-stream2.conf' });

# A result in saussage
{
    my $result = Stream2::Results::Result->new({
        candidate_id => '12345',
        destination => 'sausage'
    });
    throws_ok { $result->get_internal_db_ids() ; } qr/only makes sense for an/ ;
}

# A result in adcresponses, with no tsimport_id
{
    my $result = Stream2::Results::Result->new({
        candidate_id => 12345,
        destination => 'adcresponses',
    });
    is( $result->get_internal_db_ids() , undef , 'No internal DB to look this one up in');
}

# A result in adcresponse with a completed tsimport_id
{
    my $result = Stream2::Results::Result->new({
        candidate_id => 2377756, # Example from live. Look for the response where document_id = 2377756 in bbcss live DB
        destination => 'adcresponses',
    });
    # Let's pretend we get that back from the BBCSS.
    $result->set_attr('tsimport_id', 13758279 );

    # Lets mock the tsimport_api client why not.
    # We know what it is going to be.
    my $mock_client = Test::MockModule->new( ref( $stream2->tsimport_api() ) );
    $mock_client->mock('import_status' => sub{
                           return
                               {
                                   'status' => {
                                       'source' => 'response://resourcinggroup/952839',
                                       'batch_id' => '690',
                                       'parameters' => '{"source":"response://resourcinggroup/952839","backend":["Artirix"],"locale":null,"attributes":{"backends":["Artirix"],"applicant_application_time":1476872070}}',
                                       'created_at' => '2016-10-19 10:14:30',
                                       'updated_at' => '2016-10-19 10:14:37',
                                       'error' => undef,
                                       'id' => '13758279',
                                       'completed' => bless( do{\(my $o = 1)}, 'JSON::XS::Boolean' ),
                                       'result' => '142802203'
                                   }
                               };
                       });

    # Now we can call get_internal_db_ids() and check we have the right things.
    is_deeply( $result->get_internal_db_ids() , { Artirix => 142802203 } );
    my @siblings = $result->get_internal_siblings( $result->get_internal_db_ids() );
    ok( scalar( @siblings ) , "Ok got internal siblings candidates");
    is( $siblings[0]->destination() , 'talentsearch');
    is( $siblings[0]->id() , 'talentsearch_'.142802203);
}

# A result in adcresponse with a tsimport_id but not completed yet.
{
    my $result = Stream2::Results::Result->new({
        candidate_id => 2377756, # Example from live. Look for the response where document_id = 2377756 in bbcss live DB
        destination => 'adcresponses',
    });
    # Let's pretend we get that back from the BBCSS.
    $result->set_attr('tsimport_id', 13758279 );

    # Lets mock the tsimport_api client why not.
    # We know what it is going to be.
    my $mock_client = Test::MockModule->new( ref( $stream2->tsimport_api() ) );
    $mock_client->mock('import_status' => sub{
                           return
                               {
                                   'status' => {
                                       'source' => 'response://resourcinggroup/952839',
                                       'batch_id' => '690',
                                       'parameters' => '{"source":"response://resourcinggroup/952839","backend":["Artirix"],"locale":null,"attributes":{"backends":["Artirix"],"applicant_application_time":1476872070}}',
                                       'created_at' => '2016-10-19 10:14:30',
                                       'updated_at' => '2016-10-19 10:14:37',
                                       'error' => undef,
                                       'id' => '13758279',
                                       'completed' => bless( do{\(my $o = 0)}, 'JSON::XS::Boolean' ),
                                       'result' => '142802203'
                                   }
                               };
                       });

    # Now we can call get_internal_db_ids() and check we have the right things.
    is( $result->get_internal_db_ids() , 'INTERNAL_DBS_PLEASE_WAIT' );
}

# A result in adcresponse with a tsimport_id, completed but with no internal DB ID :/
{
    my $result = Stream2::Results::Result->new({
        candidate_id => 2377756, # Example from live. Look for the response where document_id = 2377756 in bbcss live DB
        destination => 'adcresponses',
    });
    # Let's pretend we get that back from the BBCSS.
    $result->set_attr('tsimport_id', 13758279 );

    # Lets mock the tsimport_api client why not.
    # We know what it is going to be.
    my $mock_client = Test::MockModule->new( ref( $stream2->tsimport_api() ) );
    $mock_client->mock('import_status' => sub{
                           return
                               {
                                   'status' => {
                                       'source' => 'response://resourcinggroup/952839',
                                       'batch_id' => '690',
                                       'parameters' => '{"source":"response://resourcinggroup/952839","backend":["Artirix"],"locale":null,"attributes":{"backends":["Artirix"],"applicant_application_time":1476872070}}',
                                       'created_at' => '2016-10-19 10:14:30',
                                       'updated_at' => '2016-10-19 10:14:37',
                                       'error' => undef,
                                       'id' => '13758279',
                                       'completed' => bless( do{\(my $o = 1)}, 'JSON::XS::Boolean' ),
                                       'result' => ''
                                   }
                               };
                       });

    # Now we can call get_internal_db_ids() and check we have the right things.
    is( $result->get_internal_db_ids() , 'INTERNAL_DBS_NO_MAPPING' );
}

done_testing();

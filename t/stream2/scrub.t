#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Log::Log4perl qw/:easy/;

# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log::Log4perl');

use Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

my $stream2 = Stream2->new({ config_file => $config_file });

{
    my $data = {
        a => 1,
        b => "foo",
        object => $stream2,
        array => [ 'bah', 'bah', 'black', $stream2, 'sheep' ],
    };
    my $scrubbed = $stream2->scrubber()->scrub_objects( $data );
    is_deeply( $scrubbed, { a => 1 , b => "foo" , array => [ 'bah', 'bah', 'black', 'sheep' ] } );
}

done_testing();

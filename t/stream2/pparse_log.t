#! perl -w


use Test::Most;
use Stream2;

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}

use LWP;
use LWP::UserAgent::Mockable;

# use Log::Any::Adapter qw/Stderr/;

### Tests for the Stream2::Factory::PrivateFile class

note 'Test instantiation';
my $stream2 = Stream2->new({ config_file => './t/app-stream2.conf' });
ok $stream2, 'Built stream2';

my $pparse_logs = $stream2->factory('PParseLog');

# This is known to be there.
ok(my $url = $pparse_logs->get_response_log_url( 'cinzwonderland' , 62977 )  , "A response thats there gets a URL");
note( $url );

# One that cannot be there.
ok(! $pparse_logs->get_response_log_url( 'cinzwonderland' , 123 )  , "A response that has not been logged by the new Persistent Parse is not there");

END {
    # END block ensures cleanup if script dies early
    LWP::UserAgent::Mockable->finished;
};
done_testing();

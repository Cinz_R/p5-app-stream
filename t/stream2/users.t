#! perl -w

use strict;
use warnings;


BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';

    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}
use LWP;
use LWP::UserAgent::Mockable;

use JSON qw//;
use JSON::XS qw//;

use FindBin;
use Test::More;

use Stream2;

use Scalar::Util;

use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();

# Ok the setting are not empty
ok( $stream2->factory('Setting')->count() , "Ok got some settings");

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser') , "Ok can get resultset");

my $users_factory = $stream2->factory('SearchUser');

# Time to create a user.
my $memory_user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
    settings => {
        "auto_download_cvs_on_preview" => JSON::XS::false,
        "some_other_random_setting" => JSON::true,
        'stream' => {
            'forward_to_tp2' => '0'
        },
    }
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");
ok( ! $memory_user->contact_email() , "Ok no contact email found");
ok( ! $memory_user->locale() , "Ok no locale found");
ok( ! $memory_user->timezone() , "Ok no timezone found");
ok( ! $memory_user->currency() , "Ok no currency found");
ok( ! $memory_user->distance_unit() , "Ok no distance_unit found");
ok( $memory_user->company_nice_name() , "Ok got company nice name");


{
    # Re-import the same user with a contact_email this time
    $memory_user = $users_factory->find({
                                         provider => 'whatever',
                                         provider_id => 31415 });
    $memory_user->contact_details({
                                   contact_email => 'yadayada@example.com' ,
                                   contact_name => 'Yada Yada',
                                   locale => 'en',
                                   timezone => 'Europe/London',
                                   currency => 'GBP',
                                   distance_unit => 'miles',
                                  });
    $memory_user->save();
}


{
    # Check we can get the setting values of this user, even though none has been set against it.
    ok(  my $setting_values = $memory_user->settings_values_hash() , "Ok got setting values hash");
    ok( scalar( keys %$setting_values )  , "Ok got keys in setting values");
}

{
    # Check we can get the settings front end hash
    ok( my $front_end_hash = $memory_user->to_hash(), "Ok got frontendy hash");

    # Check that NONE of the settings are blessed.
    my $settings = $front_end_hash->{settings};
    foreach my $key ( keys %$settings ){
        if( Scalar::Util::blessed( $settings->{$key} ) ){
            fail( "Setting '$key' is a blessed object. We DONT want that, as later Mojolicious serialization can mess it up big time");
        }else{
            pass("Setting '$key' is NOT blessed. Which is a good thing");
        }
    }

    is( $front_end_hash->{settings}->{stream}->{forward_to_tp2} , '0' , "Other settings structure is conserved");
}

ok( $memory_user->contact_email(), "contact email defined on user object" );
ok( $memory_user->locale(), "locale defined on user object" );
ok( $memory_user->timezone(), "timezone defined on user object" );
ok( $memory_user->currency(), "currency defined on user object" );
ok( $memory_user->distance_unit(), "distance_unit defined on user object" );
ok( $memory_user->contact_email(), "contact email stored in db" );
ok( $memory_user->locale(), "locale stored in db" );
ok( $memory_user->timezone(), "timezone stored in db" );
ok( $memory_user->currency(), "currency stored in db" );
ok( $memory_user->distance_unit(), "distance_unit stored in db" );

is( $memory_user->watchdogs()->count() , 0 , "Ok this user has no watchdogs");

ok( $memory_user->set_cached_value('somekey' , 'some data' ), "Ok can set some never expiring value");
is( $memory_user->get_cached_value('somekey') , 'some data' , "Ok can get the value back");

ok( ! $memory_user->has_subscription('boudinblanc') , "Ok no subscription boudin blanc");
ok( ! $memory_user->has_adcourier_search() , "Ok no adcourier search neither");

is_deeply( $memory_user->ripple_header_custom_fields() , {} , "Ok No Custom fields, no crash");

# Try set cached data
ok( $memory_user->set_cached_data('some_data' , { 'a' => 1 , b => [ 'boudin' , 'blanc' ] } ), "Ok can set some cached data");
is_deeply( $memory_user->get_cached_data('some_data'),
           { 'a' => 1 , b => [ 'boudin' , 'blanc' ] },
           "Ok got right data back");


{
    # Try user 1, check we have some jobs from it.
    my $memory_user = $users_factory->find({
                                            provider => 'adcourier',
                                            provider_id => 1,
                                           });
}


{
    # Try adcourier user ID 5 and check he oversees other users.
    # This number 5 is taken from the use_user_admin_api.t test of bean-juiceapi-client

    my $memory_user = $users_factory->new_result({
        provider => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id => 5,
        group_identity => 'acme',
    });
    my @oversees = $memory_user->adcourier_oversees();
    ok( scalar( @oversees ) , "Ok this admin user oversees others");
}

LWP::UserAgent::Mockable->finished();
done_testing();

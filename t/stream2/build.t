#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Log::Log4perl qw/:easy/;

# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log::Log4perl');

use Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
is( $stream2->config_place() , "Config file: ".$config_file , "The config was from a file");
ok( $stream2->stream_schema() , "Ok got schema");
ok( $stream2->dbic_schema() , "Ok got DBIC Schema");
ok( $stream2->watchdogs() , "Ok got Watchdogs factory");
ok( $stream2->perfmonitor() , "Ok got performance monitor");

ok( $stream2->factory('PublicFile') , "Ok got a factory about cloudy public files");
cmp_ok( $stream2->factory('PublicFile').'' , 'eq',
    $stream2->factory('PublicFile').'' , "Ok is a singleton");
ok( $stream2->factory('CandidateActionLog') , "Ok got CandidateActionLog factory");
ok( $stream2->factory('CandidateActionLog')->isa('Stream2::DBICWrapper') , "Good factory class");

ok( $stream2->perl_deparser() , "Ok got deparser");
ok( $stream2->pubsub_client(), "Ok got pubsub client");

ok( -d $stream2->shared_directory() , "Ok shared directory is a directory");

ok( $stream2->json(), "Ok can have json service");
ok( $stream2->scrubber(), "Ok can get scrubber");

# Deploying schema
$stream2->stream_schema()->deploy();

{
    # Try a Wrapper factory
    ok( my $users = $stream2->dbic_factory('CvUser') , "Ok got factory for users");
    isa_ok( $users , 'Stream2::DBICWrapper::CvUser' , "Ok good class");
}

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser') , "Ok can get resultset");
is( $cv_users->count() , 0, "No users. This is a fresh DB deployment");

ok( $stream2 eq Stream2->instance() , "Ok good instance");
ok( $stream2->stream2_cache() , "OK got stream2 cache");
ok( $stream2->users_cache(), "Ok got users cache");
ok( $stream2->location_api() , "Ok got location API");
ok( $stream2->user_api() , "Ok got user API");
ok( $stream2->tsimport_api(), "Ok got tsimport_api client");
ok( $stream2->documents_api() , "Ok got documents API");
ok( $stream2->currency_api(), "Ok got currency API");
ok( $stream2->adcourier_api() , "Ok can get adcourier API");
ok( $stream2->cvp_api(), "Ok can get CV parser API");
ok( $stream2->cvp_api()->retry_on_failure() , "Ok retry_on_failure is positive in the cvp_api");
ok( $stream2->text_extractor(), "Ok got client to text extraction");
ok( $stream2->lisp(), "Ok got lisp interpreter");
ok( $stream2->configurations_api(), "Ok got configurations API");

ok( $stream2->factory('Tag'), "Ok can get tags factory");

ok( $stream2->VERSION() , "Ok can get version");
ok( $stream2->VERSION_FRONTEND(), "Ok can get front end version");

# Test building a template (the talentsearch one).
ok( my $feed = $stream2->build_template('talentsearch') , "Ok can build talentsearch template");

{
    # Test cloning
    is( Stream2->instance().'' , $stream2.'' , "The current instance is good");

    ok( my $s2_clone = $stream2->clone() , "Ok can clone");

    is( Stream2->instance().'' , $s2_clone.'' , "Ok not the current instance is the last clone in this process");

    is( $s2_clone->config_place() , "Memory Config Hash" , "A clone gets its config from memory");
    # Check the config stayed the same
    is_deeply( $s2_clone->config() , $stream2->config() , "Both config are the same");
}

ok( $stream2->feed_proxy('monster') , "Ok can get a feed proxy");

# Try loading a dtd.
ok( $stream2->dtd('/ripple/ripplev3.dtd') , "Ok can load ripplev3 dtd");

# Sign a hash, check that the same hash but in a different way signs to the same thing.
is(
    $stream2->sign_hash({
        a => 1,
        b => 'foo',
        c => undef,
    }), $stream2->sign_hash({
        c => undef,
        b => 'foo',
        a => '1'.'',
    }),
    "Two hashes are the same"
);

is( $stream2->sign_hash({ a => 2 }), '8395de' );

ok( $stream2->parse_normalize_api(), "Ok can get parse_normalize_api");

is( $stream2->whoami('Stream2::OneIAM'), 'OneIAM', 'i am one');
is( $stream2->whoami('Stream2'), 'Stream2', 'Single namespace is good');

done_testing();

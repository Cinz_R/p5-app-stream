#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Log::Log4perl qw/:easy/;
Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;

# Log::Any::Adapter->set('Log::Log4perl');

use Test::Stream2;



my $config_file = 't/app-stream2.conf';
my $tsv_file = 't/files/candidate_tags.tsv';

unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");


# Deploying schema
$stream2->deploy_test_db();

ok( $stream2->factory('Tag') , "Ok cat get tags factory");

ok( $stream2->factory('Tag')->import_file($tsv_file) , "Ok can import file");

# Create a CSV file with tags innit and import it.


done_testing();

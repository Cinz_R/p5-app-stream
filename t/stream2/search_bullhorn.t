#! perl

use strict;
use warnings;

# use Carp::Always;

use FindBin;
use Test::More;
use Test::Exception;

use Data::UUID;

ok(1);

# done_testing();
# exit(0);

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}

use LWP;
use LWP::UserAgent::Mockable;

use Stream2;

use Log::Log4perl qw/:easy/;
Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log::Log4perl');


my $config_file = 't/app-stream2.conf';

my $template_name = 'bullhorn';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $ug = Data::UUID->new();

my $users_factory = $stream2->factory('SearchUser');

# A test user.
my $user = $users_factory->new_result({provider => 'adcourier',
                                       last_login_provider => 'adcourier',
                                       provider_id => 21024, # This is a bullhorn adcourier test user.
                                       group_identity => 'acme',
                                      });
$user->save();


# A typical search
test_search_with_tokens({'salary_to', [ '' ], 'salary_from', [ '' ], 'location', [ 'South West London, England' ], 'location_id', [ '39557' ], 'keywords', [ 'Project Manager' ], 'salary_per', [ '' ], 'jobtype', [ '' ], 'location_within', [ 0 ], 'cv_updated_within', [ 'ALL' ], 'salary_cur', [ '' ] });

# Do it again
test_search_with_tokens({'keywords', ['Manager'], 'cv_updated_within', ['ALL']});


LWP::UserAgent::Mockable->finished();
done_testing();


sub test_search_with_tokens{
    my ($tokens) = @_;

    $tokens //= {};

    # This is needed for Stream::SharedUtils
    # to know about the IP.
    local $ENV{REMOTE_ADDR} = '127.0.0.1';

    ok( my $template = $stream2->build_template($template_name, undef , { user_object => $user } ) , "Ok got template");

    my $results_id = $ug->create_str();

    ok( my $results_collector = Stream2::Results->new({
                                                       id          => $results_id,
                                                       destination => $template_name,
                                                      }),
        "Ok can build a result collector");

    # Inject the result collector in the template object.
    $template->results($results_collector);

    # Set the tokens
    foreach my $token_name ( keys %$tokens ){
        $template->token_value( $token_name , @{$tokens->{$token_name}} );
    }
    lives_ok(sub{ $template->search({}); } , "Ok searching lives");
}

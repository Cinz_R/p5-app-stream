#! perl
# Test some provider stuff

use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Fatal qw/dies_ok lives_ok exception/;


BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';

}

use LWP;
use LWP::UserAgent::Mockable;


use Stream2;

use Email::Sender::Simple;

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $ug = Data::UUID->new();

my $users_factory = $stream2->factory('SearchUser');

# Create a test user. This is dutch pat, check it exists remotely
{
    my $user = $users_factory->new_result({
        provider => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id => 183807,
        group_identity => 'pat',
    });
    $user->save();
    $user->ripple_settings()->{login_provider} = 'adcourier';
    ok( $user->login_provider() , "Ok can get provider object");
    ok( $user->identity_provider(), "OK can get identity provider");
    ok( $user->exists_remotely() , "Ok exists remotely");
}

# Create a user that will not be there.
{
    my $user = $users_factory->new_result({
        provider => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id => -123456,
        group_identity => 'pat',
    });
    $user->save();
    $user->ripple_settings()->{login_provider} = 'adcourier';
    ok( $user->login_provider() , "Ok can get provider object");
    ok( $user->identity_provider(), "Ok can get identity provider");
    ok( ! $user->exists_remotely() , "Ok does not exists remotely");
}


LWP::UserAgent::Mockable->finished();

done_testing();

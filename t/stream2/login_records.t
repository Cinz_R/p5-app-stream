#! perl -w

use strict;
use warnings;

use Test::More;
use Test::Stream2;

# use Log::Any::Adapter qw/Stderr/;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

$stream2->deploy_test_db();
my $users_factory = $stream2->factory('SearchUser');

# Time to create a user.
my $memory_user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
    settings => {},
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");

foreach my $i ( 1..10 ){
    $stream2->factory('LoginRecord')->create({ user_id => $user_id,
                                               remote_ip => '127.0.0.1',
                                               login_provider => 'adcourier',
                                               hostname => 'foobar'
                                           });
}

is( $stream2->factory('LoginRecord')->count(), 10 );

# Send the first records three months and five days in the past.
# using 5 days to give some buffer to the problem of having two long months as
# month 1 and month 3.
my $three_months = DateTime->now()->subtract( months => 3 , days => 5 );
my @to_update = $stream2->factory('LoginRecord')->search(undef, { order_by => { -asc => 'id' } , rows => 3  })->all();
foreach my $to_update ( @to_update ){
    ok( $to_update->insert_datetime() , "Ok got insert datetime");
    $to_update->insert_datetime( $three_months );
    $to_update->update();
}

$stream2->factory('LoginRecord')->clear_old_records();

is( $stream2->factory('LoginRecord')->count(), 7, "After clearing, only 7 records remain." );


done_testing();

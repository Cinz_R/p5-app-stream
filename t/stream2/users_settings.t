#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Stream2;

use Log::Any::Adapter;
#Log::Any::Adapter->set('Stderr');

my $default_values = {
    boolean_value => undef,
    string_value => undef
};

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser') , "Ok can get resultset");

my $users_factory = $stream2->factory('SearchUser');

# Time to create some users.
my $group_identity = 'acme';
my @users = map{
    my $user = $users_factory->create({ provider => 'demo',
                                        last_login_provider => 'demo',
                                        provider_id => 'provid_'.$_,
                                        group_identity => $group_identity
                                      });
    $user->save();
    $user;
} ( 1..100);

ok( $users[0]->id() , "Ok users have got an id");

# Make sure we can get a hierarchy from a top user
ok( $users[0]->overseen_hierarchy() , "Ok got hierarchy structure");

is( scalar( $users[0]->siblings_ids() ) , 99 , "Ok this user has got 99 siblings (demo provider)");
is( scalar( @users ) ,100 , "Ok 100 users created");

ok( my $setting = $stream2->factory('Setting')->find({ setting_mnemonic => 'candidate.actions.profile.enabled' }) , "Ok can get setting");
ok( my $string_setting = $stream2->factory('Setting')->find({ setting_mnemonic => 'behaviour-downloadcv-use_internal'}) , "Ok can get string setting");
ok( my $other_string_setting = $stream2->factory('Setting')->find({ setting_mnemonic => 'criteria-cv_updated_within-default' }) , "Ok can get string setting for cv_updated_within");

is_deeply( $setting->get_group_identity_values($group_identity, 'demo') , { default => { %$default_values, boolean_value => 1 },
                                                                            users_map => [
                                                                                         ]
                                                                          },
           "Good group_identity values when no specific settings have been made" );

my @true_user_ids = map { $_->id() } @users[0..56];

# Set the value 1 against those 56 users
$setting->set_users_value($users[0], \@true_user_ids , 1);

is_deeply( $setting->get_group_identity_values($group_identity, 'demo') , { default => { %$default_values, boolean_value => 1 },
                                                                            users_map => [
                                                                                         ]
                                                                          },
           "Good group_identity values when no specific settings have been made" );

# Set the value 0 against some other users
my @false_user_ids = map{ $_->id() } @users[57..$#users];

$setting->set_users_value($users[0], \@false_user_ids , 0);

{
    my $all_values = $setting->get_group_identity_values($group_identity, 'demo');
    # Make sure the test is stable
    $all_values->{users_map}->[0]->{users} = [ sort @{ $all_values->{users_map}->[0]->{users} } ];

    is_deeply($all_values  , { default => { %$default_values, boolean_value => 1 },
                               users_map => [{
                                              value => { %$default_values, boolean_value => 0 },
                                              users => [ sort @false_user_ids ]
                                             }
                                            ]
                             },
              "Good group_identity values with some specific settings have been made" );
}


# A part of the false users become true
push @true_user_ids , splice(@false_user_ids , 0 , 10 );

$setting->set_users_value($users[0], \@true_user_ids , 1 );
{
    my $all_values = $setting->get_group_identity_values($group_identity, 'demo');
    # Make sure the test is stable
    $all_values->{users_map}->[0]->{users} = [ sort @{ $all_values->{users_map}->[0]->{users} } ];
    is_deeply($all_values  , { default => { %$default_values, boolean_value => 1 },
                               users_map => [{
                                              value => { %$default_values, boolean_value => 0 },
                                              users => [ sort @false_user_ids ]
                                             }
                                            ]
                             },
              "Good group_identity values with some specific settings have been made" );
}

# A part of those true users become false again.
push @false_user_ids , splice( @true_user_ids , 0 , 20 );
$setting->set_users_value($users[0], \@false_user_ids , 0 );

{
    my $all_values = $setting->get_group_identity_values($group_identity, 'demo');
    # Make sure the test is stable
    $all_values->{users_map}->[0]->{users} = [ sort @{ $all_values->{users_map}->[0]->{users} } ];
    is_deeply($all_values  , { default => { %$default_values, boolean_value => 1 },
                               users_map => [{
                                              value => { %$default_values, boolean_value => 0 },
                                              users => [ sort @false_user_ids ]
                                             }
                                            ]
                             },
              "Good group_identity values with some specific settings have been made" );
}

# We now have a mix of true and false for the setting.
# Lets consider a true user as new and check it doesnt get infected by a false value.

{
    my $new_user = $users_factory->find($true_user_ids[0]);
    $new_user->inherit_siblings_settings();
    is( $new_user->settings_values_hash()->{$setting->setting_mnemonic()} , $setting->default_value() , "Ok setting is default value");
}

{
    # Now we set ALL user IDs to the default value
    $setting->set_users_value($users[0],[ map{ $_->id } @users ] , 1 );
    # And we check the same business about inheriting siblings settings.
    my $new_user = $users_factory->find($users[0]->id());
    $new_user->inherit_siblings_settings();
    is( $new_user->settings_values_hash()->{$setting->setting_mnemonic()} , $setting->default_value() , "Ok setting is default value");
}

{
    # Now we set ALL (Except the first one) to the non default value.
    my @only = @users[1..$#users];
    $setting->set_users_value($users[0],[ map{ $_->id } @only ] , 0 );
    $string_setting->set_users_value($users[0] , [ map{ $_->id } @only ] , 'SomeString' );

    # And we check that some inheritance will take place
    my $new_user = $users_factory->find($users[0]->id());

    # Try this, why not :)
    $new_user->conform_to_sibling_settings();

    is( $new_user->settings_values_hash()->{$setting->setting_mnemonic()} , 0  , "Ok setting is the same as its brothers");
    is( $new_user->settings_values_hash()->{$string_setting->setting_mnemonic()} , 'SomeString' , "Ok string setting is the same as its brothers");
}

{
    # Set everything to the default value
    my @all_settings = $stream2->factory('Setting')->search({ setting_type => 'boolean'})->all();
    map{ $_->set_users_value($users[0], [ map { $_->id() } @users ] , $_->default_value() ); } @all_settings;
    is( $stream2->factory('UserSetting')->search({ 'setting.setting_type' => 'boolean' }, { join => 'setting'} )->count() , 0 , "Ok no setting saved at all");

    # Set stuff to non default for half the settings for all users but 0
    my @non_default_settings = @all_settings[0..int(scalar(@all_settings)/2)];
    map{ $_->set_users_value($users[0], [ map { $_->id() } @users[1..$#users] ] , 0   ); } @non_default_settings;

    my @half_users_settings = @all_settings[int(scalar(@all_settings) /2) + 1 .. $#all_settings];
    map{ $_->set_users_value($users[0], [ map { $_->id() } @users[1..int(scalar(@users/2)) ] ] , 0 ); } @half_users_settings;

    # Reload user 0 and inherit siblings settings.
    # This user 0 should have: Non default for @non_default_settings and default for @half_users_settings

    # No need to reload the user 0 from the DB btw, discard_changes should be enough
    my $new_user = $users[0];
    # Check this user has got ALL defaults.
    foreach my $setting ( @all_settings ){
        is( $new_user->settings_values_hash()->{$setting->setting_mnemonic()} , 1 , "Ok got default for all settings");
    }

    $new_user->inherit_siblings_settings();

    $new_user->discard_changes(); # This should be enough to change the settings_values_hash for later tests

    foreach my $non_default ( @non_default_settings ){
        is( $new_user->settings_values_hash()->{$non_default->setting_mnemonic()} , 0 , "Ok good inheritance of accross the board non default");
    }

    foreach my $half_non_default ( @half_users_settings ){
        is( $new_user->settings_values_hash()->{$half_non_default->setting_mnemonic()} , $half_non_default->default_value() , "Ok good inheritance of half the users non default");
    }

}

done_testing();

#! perl -w

use strict;
use warnings;

use FindBin;
use Test::More;

use Test::Stream2;
use Encode;

# use Carp::Always;

# use Log::Any::Adapter qw/Stderr/;


my $stream2 = Test::Stream2->new();
$stream2->deploy_test_db();

my $users_factory = $stream2->factory('SearchUser');
my $memory_user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
my $user_id = $memory_user->save();

my $group = $stream2->factory('Group')->find('acme');
my $template = $stream2->factory('EmailTemplate')->create({ groupclient => $group,
                                                            name => 'My Shiny Template'
                                                        });
my $rule_id;
{
    # Save a rule
    ok( my $rule = $template->add_rule( 'ResponseFlagged', { flag_id => 12345 , in_minutes => 11}) );
    ok( $rule->flag_id() , "Ok rule contains flag");
    is( $rule->in_minutes() , 11, "11 minutes");
    ok( $rule->group_macro() , "Ok rule contains group_macro");
    ok( $rule->group_macro()->lisp_source() , "Ok group_macro has got lisp source against it");
    is( $rule->group_macro()->lisp_source() , '(if (s2#response-is-being-flagged "12345") (s2#do-later 11 (begin (s2#send-email-template "1"))))' );
    $rule_id = $rule->id();
}


{
    # Find the same rule.
    my $rule = $stream2->factory('EmailTemplateRule')->find( $rule_id );
    is( $rule->flag_id() , 12345 , "Ok rule has got good flag ID");
    is( $rule->in_minutes() , 11, "11 minutes");
    is_deeply( $rule->to_hash(), { id => '1',
                                   role_name => 'ResponseFlagged',
                                   flag_id => 12345,
                                   in_minutes => 11
                               }, "Ok good rule to hash");
    ok( $rule->group_macro()->email_template() , "Ok the group macro of the rule has got the template associated");
}

# Find the rules from the template.
{
    my ( $rule ) = $template->rules()->all();
    is( $rule->flag_id() , 12345 , "Ok can find rule from template rules");
}

# Delete the rules and see that all is cool.
{
    map{ $_->delete() } $template->rules()->all();
}

# Add a rule about the Response received.
{
    my $rule = $template->add_rule( 'ResponseReceived', {});
    ok( $stream2->factory('GroupMacro')->count() , "Ok some group macros are there");
    ok( $rule->group_macro() , "Ok rule contains group_macro");
    ok( $rule->group_macro()->lisp_source() , "Ok group_macro has got lisp source against it");
}

# Add a rule again and check one can delete the template without going bang.
{
    my $rule = $template->add_rule( 'ResponseFlagged', { flag_id => 12345, in_minutes => 0 });
    is( $rule->group_macro()->lisp_source() , '(if (s2#response-is-being-flagged "12345") (begin (s2#send-email-template "1")))', 'Zero minutes. No do-later' );
    my $same_rule = $stream2->factory('EmailTemplateRule')->find( $rule->id() );
    is( $same_rule->in_minutes() , 0 );
    is_deeply( $rule->to_hash(), {
        id => 3,
        role_name => 'ResponseFlagged',
        flag_id => 12345,
        in_minutes => 0
    });

    ok( $stream2->factory('GroupMacro')->count() , "Ok some group macros are there");
    $template->delete();
    ok( ! $stream2->factory('GroupMacro')->count() , "Ok group macros are gone");
}


done_testing();

#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;

use Test::MockModule;

use DateTime;

use Log::Any::Adapter;

use Email::Simple;

# Log::Any::Adapter->set('Stderr');

use Stream2;
use Stream2::Criteria;

no warnings;
*Stream2::log_search_calendar = sub {};
use warnings;


my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}


ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser') , "Ok can get resultset");

my $schema = $stream2->stream_schema();

my $users_factory = $stream2->factory('SearchUser');

my %kitten_board = (
    'name' => 'kitten',
    'id' => '999',
    'nice_name' => 'Broadbean Test Board',
    'board_type' => 'external',
    'cvsearch_temp_disabled' => 1,
    'allows_acc_sharing' => '1',
);
my $boards_ref = {
    kitten => {
        %kitten_board,
        other => 'stuff',
        more  => 'things'
    }
};

my $juice_mock = Test::MockModule->new('Bean::Juice::APIClient::Board');
$juice_mock->mock( list_boards => sub {
    my ( $self, $where_ref ) = @_;
    # return matching boards, but only once! they should be cached after the first time
    if ( my $board_ref = delete($boards_ref->{$where_ref->{name}}) ){
        return [ $board_ref ];
    }
});

# Time to create some users and check boards stuff
my $user_id;
my $memory_user;
{
    $memory_user = $users_factory->new_result({  provider => 'dummy',
                                                 last_login_provider => 'dummy',
                                                 provider_id => 31415,
                                                 group_identity => 'acme',
                                                 subscriptions_ref => {
                                                                       flickr_xray => {
                                                                                       nice_name => "Flickr",
                                                                                       auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
                                                                                       type => "social",
                                                                                       allows_acc_sharing => 0,
                                                                                       board_id => '123abc'
                                                                                      },
                                                                       'whatever' => {
                                                                                      nice_name => 'Whatever',
                                                                                      auth_tokens => { whatever_login => 'Boudin', whatever_passwd => 'Blanc' },
                                                                                      type => 'social',
                                                                                     }
                                                                      }
                                              });
    ok( $user_id = $memory_user->save() , "Ok can save this user");

    is( $schema->resultset('CvSubscription')->search({ user_id => $user_id })->count() , 2 , "Ok two subscriptions in the DB");

    ok( ! $schema->resultset('CvSubscription')->search({ board => 'flickr_xray'} , { prefetch => 'board_object' } )->first()->board_object()->allows_acc_sharing() , "Ok saved the right Account sharing setting (flickr not allow)");
    ok( $schema->resultset('CvSubscription')->search({ board => 'whatever'})->first()->board_object->allows_acc_sharing() , "Ok saved the right Account sharing setting (whatever is allowed)");
    is( $stream2->factory('Board')->name_to_id( 'flickr_xray' ) , '123abc' );
    is( $stream2->factory('Board')->name_to_id( 'whatever' ) , undef );
    is( $stream2->factory('Board')->name_to_id( 'will never match') , undef );
}

ok( my $flickr = $memory_user->subscriptions()->find({ board => 'flickr_xray' }) , "Ok can get subscription via row user relationship");

my $kitten_board_from_api = $stream2->factory('Board')->fetch({ name => 'kitten' });
is( $kitten_board_from_api->type, $kitten_board{board_type}, "board is fetched from API" );
is( $kitten_board_from_api->disabled, $kitten_board{cvsearch_temp_disabled}, "board is fetched from API" );
is( $kitten_board_from_api->nice_name, $kitten_board{nice_name}, "board is fetched from API" );
my $kitten_board_from_cache = $stream2->factory('Board')->fetch({ name => 'kitten' });
is( $kitten_board_from_cache->nice_name, $kitten_board{nice_name}, "board is fetched from cache" );
is( $stream2->factory('Board')->name_to_id( 'kitten' ), '999', "get kitten board id from cache" );

done_testing();

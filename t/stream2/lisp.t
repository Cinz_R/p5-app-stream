#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;

use Log::Log4perl qw/:easy/;

# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log::Log4perl');

use Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

{
    my $atom = $stream2->lisp()->eval(q|( s2#about )| );
    like( $atom->value() , qr/pure functions/ );
}

{
    my $atom = $stream2->lisp()->eval(q|( s2#about ) ( s2#about )| );
    like( $atom->value() , qr/pure functions/ );
}

{
    throws_ok { my $atom = $stream2->lisp()->eval(q|( s2#about ) ( s2#do-crash )| ) }
        qr/Something went very wrong/;
}

done_testing();

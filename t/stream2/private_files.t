#! perl -w


use Test::Most;
use Stream2;
use LWP::UserAgent;

### Tests for the Stream2::Factory::PrivateFile class

note 'Test instantiation';
my $stream2 = Stream2->new({ config_file => 't/app-stream2.conf' });
ok $stream2, 'Built stream2';

my $private_files = $stream2->factory('PrivateFile');
my $other_private_files = $stream2->factory('PrivateFile');
isa_ok( $private_files, 'Stream2::Factory::PrivateFile',
    'got a PrivateFile instance');
ok( $private_files == $other_private_files, '$private_files is a singleton' );

## Note that this test is very small. This is because the
## private file factory just inherits from Bean::AWS::S3FileFactory
## which is itself tested.

done_testing();

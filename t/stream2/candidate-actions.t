#!/usr/bin/env perl -w

use Test::Most;
use Data::Dumper;
# use Log::Any::Adapter qw/Stderr/;
# use Test::Mojo::Stream2;

use_ok('Stream2');
use_ok('Stream2::CandidateActionLog');

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}


my $stream2 = Stream2->new({ config_file => $config_file });
# Deploying schema
$stream2->stream_schema()->deploy();


my $schema = $stream2->stream_schema();

my $log = Stream2::CandidateActionLog->new( schema => $schema );

my $destination = "dummy";
my $group_identity = "andy";
my $user_id = "10";
my $test_data = { data => undef, arr => [qw/1 2 3/], ref => { a => "b"} };
my $candidates = {
    1 => [ qw/ download message shortlist / ],
    2 => [ qw/ forward message preview / ]
};
my @actions = qw/ download message shortlist preview forward /;

foreach my $action ( @actions ) {
    $schema->resultset('CandidateActions')->create({
        action => $action
    });
}
foreach my $candidate ( keys %$candidates ) {
    foreach my $action ( @{$candidates->{$candidate}} ){
        $test_data->{data} = $candidate;
        $log->insert({
            action          => $action,
            candidate_id    => $candidate,
            destination     => $destination,
            group_identity  => $group_identity,
            user_id         => $user_id,
            data            => $test_data
        });
    }
}

# Test get_actions
{
    my $action_ref = $log->get_actions( $user_id, $group_identity, [map { $_ } keys %$candidates] );
    foreach my $candidate ( keys %$candidates ) {
        foreach my $action ( @{$candidates->{$candidate}} ){
            ok( $action_ref->{$candidate}->{$action}, "$action action has been logged" );
            $test_data->{data} = $candidate;
            is_deeply( $test_data, $action_ref->{$candidate}->{$action}->data(), "data is serialised and deserialised" );
        }
    }
}


my $in_5_minutes_str = DateTime->now()->add( minutes => 5 )->iso8601().'Z';

{
    foreach my $candidate_id ( keys %$candidates ){
        $log->insert({
            action          => 'candidate_future_message',
            candidate_id    => $candidate_id,
            destination     => $destination,
            group_identity  => $group_identity,
            user_id         => $user_id,
            data            => {
                "at_time" => , $in_5_minutes_str,
                "template_name" => 'Shiny email'
            }
        });
    }
}


# Test get_all_actions
{
    my $action_ref = $log->get_all_actions( $user_id, $group_identity, [map { $_ } keys %$candidates] );
    foreach my $candidate ( keys %$candidates ) {
        foreach my $action ( @{$candidates->{$candidate}} ){
            ok( $action_ref->{$candidate}->{$action}->[0], "$action action has been logged" );
            $test_data->{data} = $candidate;
            is_deeply( $test_data, $action_ref->{$candidate}->{$action}->[0]->data(), "data is serialised and deserialised" );
        }
        my $candidate_object = Stream2::Results::Result->new({ id => $candidate,
                                                               destination => 'dummy'
                                                           });
        my $ref = $candidate_object->to_ref({candidate_action_ref => $action_ref->{$candidate} });
        is_deeply([ sort @{ $ref->{recent_actions} } ], [ sort ( 'candidate_future_message' , @{ $candidates->{$candidate} } ) ], "Ok recent_actions are as expected" );
        is( $ref->{future_message}->{at_time}, $in_5_minutes_str );
        is( $ref->{future_message}->{template_name}, 'Shiny email' );
        ok( $ref->{future_message}->{action_id} );
    }
}

is( $stream2->factory('CandidateActionLog')->count() , 8 );

# Artifically spread the candidate action over 6 days and find the
# one in the middle.
my $now = DateTime->now();
my $date = $now->clone();
@actions = $stream2->factory('CandidateActionLog')->search(undef, { order_by => { -desc => 'me.id' } })->all();
foreach my $action ( @actions ){
    $action->insert_datetime($date);
    $action->update();
    $date->subtract( days => 1 );
}

my $middle_date = DateTime->now();
$middle_date->subtract( days => 3 );

# Now find one in the middle.
{
    ok( my $action = $stream2->factory('CandidateActionLog')->find_after_date($middle_date) , "Ok can find action after given date");
}

done_testing();

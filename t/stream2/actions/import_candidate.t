#! perl -w

use Test::Most;
use Test::MockModule;

use FindBin;
use Test::More;

use Stream2;
use Stream2::Results::Result;
use Stream2::Action::ImportCandidate;

# use Log::Any::Adapter qw/Stderr/;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'"
      . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Stream2->new( { config_file => $config_file } ),
    "Ok can build stream2" );
ok( $stream2->stream_schema(), "Ok got schema" );

# Deploying schema
$stream2->stream_schema()->deploy();

# Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result(
    {
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_email       => 'sausage@tofu.co.uk',
        contact_name        => 'Patrick',
        settings            => { 'responses' => { bg_language_pack => 'uk' } },
        subscriptions_ref   => {
            sausage => {
                nice_name   => 'Sausage Board',
                auth_tokens => {}
            },
            indeed => {
                nice_name   => 'Indeed Board',
                auth_tokens => {}
            },
            talentsearch => {
                nice_name   => 'Talent Search',
                auth_tokens => {}
            }
        }
    }
);

# use DDP;
# p( $memory_user->o() );

ok( my $user_id = $memory_user->save(), "Ok can save this user" );
is( $memory_user->tagger_locale() , 'en_gb' );

my %tests = (
    WithEmail => {
        email            => 'vladimir@thekremlin.com',
        destination      => 'sausage',
        location_text    => 'Isle of Man',
        name             => 'Vladimir Putin',
        search_record_id => 1234,
        cv_content       => 'with_email_cv_content',
        cv_filename      => 'with_email_cv_content.cv',
        enrichment_email => 'I SHOULD NOT BE HERE',
        correct_email    => 'vladimir@thekremlin.com',
        nice_name        => 'Sausage Board',
    },
    NoEmail => {
        destination      => 'sausage',
        location_text    => 'Isle of Man',
        name             => 'Vladimir Putin',
        search_record_id => 2345,
        cv_content       => 'with_email_cv_content',
        cv_filename      => 'with_email_cv_content.cv',
        enrichment_email => 'vladimir@thekremlin.ru',
        correct_email    => 'vladimir@thekremlin.ru',
        nice_name        => 'Sausage Board',
    },
    Indeed => {
        destination      => 'indeed',
        location_text    => 'Isle of Man',
        name             => 'Vladimir Putin',
        search_record_id => 3456,
        cv_content       => 'with_email_cv_content',
        cv_filename      => 'with_email_cv_content.cv',
        correct_email    => 'unknown_email',
        enrichment_email => 'I SHOULD NOT BE HERE',
        nice_name        => 'Indeed Board',
    }
);


foreach my $board_test ( keys %tests ) {

    note( $board_test );

    my $test_params = $tests{ $board_test };

    my $stream_mock = Test::MockModule->new('Stream2');
    $stream_mock->mock(
        find_results => sub {
            return $board_test;
        }
    );

    my $downloadcv_mock = Test::MockModule->new('Stream2::Action::DownloadCV');
    $downloadcv_mock->mock(
        'instance_perform' => sub {
            my $response = HTTP::Response->new();
            $response->content( $test_params->{cv_content} );
            $response->content_type( 'application/pdf' );
            $response->header(
                'Content-Disposition' => sprintf(
                    "attachment; filename=%s",
                    $test_params->{cv_filename} )
            );
            return $response;
        }
    );

    # Mock the ts import API. This is because we want
    # the full user_import_candidate to be executed.
    my $tsimport_mock = Test::MockModule->new( ref( $stream2->tsimport_api() ) );
    $tsimport_mock->mock( import_cv_content => sub{
                              my ($self, %args ) = @_;
                              is( $args{attributes}->{locale} , 'en_GB' );
                              return { id => 123456 };
                          });

    my $provider_mock =
      Test::MockModule->new('App::Stream2::Plugin::Auth::Provider::Adcourier');
    $provider_mock->mock(
        user_import_candidate => sub {
            my ( $self, $user, $cv_ref, $candidate, $options_ref ) = @_;
            is( $user->contact_name,
                'Patrick',
                'the user should be called Patrick' );
            is( $cv_ref->{cv_filename},
                $test_params->{cv_filename},
                'cv files should match'
            );
            is( $cv_ref->{cv_content},
                MIME::Base64::encode_base64( $test_params->{cv_content} ),
                "CV contents should match" );
            is( $candidate->name,
                $test_params->{name},
                'names should match' );
            like( $candidate->email,
                qr($test_params->{correct_email}),
                'emails should be coorect' );
            is ( $options_ref->{board_nice_name},
                $test_params->{nice_name},
                'board nicenames should match' );
            is( $options_ref->{candidate_attributes}->{test},
                'armadillo',
                'candidate_attributes should match' );
            return $provider_mock->original('user_import_candidate')->( $self, $user, $cv_ref, $candidate, $options_ref );
        }
    );
    $provider_mock->mock(
        get_save_email_settings => sub {
            {
                bcc => 'patrick@broadbean.com',
                subject => 'This is the subject'
            };
        }
    );

    my $search_user_mock =
      Test::MockModule->new('Stream2::O::SearchUser');
    $search_user_mock->mock(
        parse_cv_with_locale => sub {
            return { email => $test_params->{enrichment_email} };
        }
    );



    ok(
        my $action = Stream2::Action::ImportCandidate->new(
            {
                jobid             => 1,
                stream2           => $stream2,
                user              => { user_id => $user_id },
                ripple_settings   => { login_provider => $memory_user->last_login_provider() },
                results_id        => 1,
                candidate_idx     => 1,
                log_chunk_id      => 12345,
                log_chunk_capture => 0,
                options => { candidate_attributes => { test => 'armadillo' } }
            }
        )
    );
    ok( $action->candidate_object, 'there is a $action->candidate_object' );

    $stream_mock->mock(
        send_email => sub {
            my ( $self, $email ) = @_;
            my $body = $email->body_as_string();
            my $head = $email->header_as_string();

            like( $head,
                qr/patrick\@broadbean\.com/,
                "bcc address is used" );
            like( $body,
                qr/$test_params->{name}/,
                "Candidate name exists in email" );
            like(
                $body,
                qr/$test_params->{nice_name}/,
                "Candidate destination nice name is in email"
            );
            like( $body,
                qr/$test_params->{location_text}/,
                "Candidate details are in email" );
        }
    );
    ok( $action->instance_perform(), 'perform the action' );

    my @all_actions = $stream2->factory('CandidateActionLog')->all();
    is( $all_actions[-1]->search_record_id,
        $test_params->{search_record_id},
        'the latest search_record_id should match' );
    is_deeply( $all_actions[-1]->data() ,
               {
                   s3_log_id => 12345,
                   $action->candidate_object()->action_hash()
               });
}

done_testing();

{
    package WithEmail;

    sub find {
        Stream2::Results::Result->new(
            {
                email            => $tests{WithEmail}->{email},
                destination      => $tests{WithEmail}->{destination},
                candidate_id     => 1234,
                location_text    => $tests{WithEmail}->{location_text},
                name             => $tests{WithEmail}->{name},
                search_record_id => $tests{WithEmail}->{search_record_id},
            }
        );
    }
}

{
    package NoEmail;

    sub find {
        Stream2::Results::Result->new(
            {
                destination      => $tests{NoEmail}->{destination},
                candidate_id     => 1234,
                location_text    => $tests{NoEmail}->{location_text},
                name             => $tests{NoEmail}->{name},
                search_record_id => $tests{NoEmail}->{search_record_id},
            }
        );
    }
}

{
    package Indeed;

    sub find {
        Stream2::Results::Result->new(
            {
                destination      => $tests{Indeed}->{destination},
                candidate_id     => 1234,
                location         => $tests{Indeed}->{location_text},
                name             => $tests{Indeed}->{name},
                search_record_id => $tests{Indeed}->{search_record_id},
            }
        );
    }
}

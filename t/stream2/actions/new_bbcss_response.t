#! perl -w

use strict;
use warnings;
use Test::Most;
use Test::MockModule;

use Stream2::Action::NewBBCSSResponse;

use Log::Any::Adapter;

# Log::Any::Adapter->set('Stderr');

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';

    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}
use LWP;
use LWP::UserAgent::Mockable;


use Test::Stream2;


my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'" . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ), "Ok can build stream2" );

$stream2->deploy_test_db;


{ package MockedS3Object; use base qw/Class::AutoAccess/; sub new{ bless $_[1], $_[0]; } 1; }
my $files_mock = Test::MockModule->new( ref( $stream2->factory('PrivateFile') ) );
$files_mock->mock( 'create' => sub{
                       return MockedS3Object->new({ key => 'abcdefg'});
                   });

my $user = $stream2->factory('SearchUser')->new_result(
    {   provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 206757,
        group_identity      => 'cinzwonderland',
        contact_name        => 'Andy Jones',
        contact_email       => 'andy@broadbean.com',
        settings            => { responses => { bg_language_pack => 'au' } }
    }
);
$user->save();

{
    # With a silly adcuser_id, the thing does not even do HasUser
    my $new_bbcss_response = Stream2::Action::NewBBCSSResponse->new({
        stream2          => $stream2,
        api_url => 'https://bbcss.adcourier.com/api/',
        document_id => 1234321,
        customer_key => 'bbts_cinzw314f294977', # cinzwonderland bbcss live responses holder.
        broadbean_adcuser_id => 00000000,
    });
    ok( ! $new_bbcss_response->does('Stream2::Role::Action::HasUser') );
    $new_bbcss_response->instance_perform();
}


{
    # Create a side effect triggering macro
    my $mime_entity = $stream2->factory('Email')->build([
        Type     => 'text/html; charset=UTF-8',
        Encoding => 'quoted-printable',
        Subject  => "Check that",
        'Reply-To' => 'reply@example.com',
        Bcc => 'bcc@example.com',
        Disposition => 'inline',
        Data        => [ '<html><body><h1>Hello template [advert_title]</h1></body></html>' ]
    ]);

    my $template = $stream2->factory('EmailTemplate')->create(
        {   group_identity => $user->group_identity(),
            name           => 'supertemplate'
        }
    );
    $template->mime_entity_object( $mime_entity );
    $template->update();

    my $macro = $user->groupclient()->add_to_macros(
        {
            on_action => 'Stream2::Action::NewBBCSSResponse',
            name        => 'send_template_to_new_candidate',
            lisp_source => q|( s2#send-email-template |.$template->id().q| )|
        }
    );

}



{
    # A good notification with an existing user and an existing application
    my $new_bbcss_response = Stream2::Action::NewBBCSSResponse->new({
        stream2          => $stream2,
        api_url => 'https://bbcss.adcourier.com/api/',
        document_id => 720821,
        customer_key => 'bbts_cinzw314f294977', # cinzwonderland bbcss live responses holder.
        broadbean_adcuser_id => 206757,
    });
    ok( $new_bbcss_response->does('Stream2::Role::Action::HasUser') );
    ok( $new_bbcss_response->does('Stream2::Role::Action::HasFeed') );
    ok( $new_bbcss_response->does('Stream2::Role::Action::HasAdvert') );
    ok( $new_bbcss_response->does('Stream2::Role::Action::TriggerMacros') );
    ok( $new_bbcss_response->user_object() );
    ok( $new_bbcss_response->feed() );
    ok( $new_bbcss_response->get_advert() );
    ok( $new_bbcss_response->candidate_object()->attr('has_cv') , "Ok candidate has CV");

    # run the instance
    my @result = $new_bbcss_response->instance_perform();

    my @deliveries = Email::Sender::Simple->default_transport->deliveries();
    is( scalar(@deliveries) , 2 , "One normal delivery, and one BCC, as the result of the macro being run" );
    like( $deliveries[0]->{email}->get_body() , qr/Hello template Penguin Tap Dance Teacher/, "job title is included in the template" );
    my $last_action = $stream2->factory('CandidateActionLog')->search(undef, { order_by => { -desc => 'id' } })->first();
    is( $last_action->action() , "message_auto");
}

done_testing();
END{
    LWP::UserAgent::Mockable->finished();
}

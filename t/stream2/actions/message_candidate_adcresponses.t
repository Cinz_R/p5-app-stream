#! perl

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';

    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Data::Dumper;
use JSON qw/decode_json/;
use Test::Mojo::Stream2;
use Test::MockModule;

use Data::Dumper;

use Stream2::Action::MessageCandidate;

# use Log::Any::Adapter qw/Stderr/;

my $config_file = 't/app-stream2.conf';
ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
# Deploying schema
$stream2->stream_schema()->deploy();

# Mock the S3
{ package MockedS3Object; use base qw/Class::AutoAccess/; sub new{ bless $_[1], $_[0]; } 1; }
my $files_mock = Test::MockModule->new( ref( $stream2->factory('PrivateFile') ) );
$files_mock->mock( 'create' => sub{
                       return MockedS3Object->new({ key => 'abcdefg'});
                   });

# Mock this as the email service stores emails we send.
my $email_client = Test::MockModule->new('Bean::EmailService::Client');
$email_client->mock( store_email => sub {
    return;
});

# we need a constant, predictable value for Mailjet CustomID
my $uuid_mock = Test::MockModule->new('Data::UUID');
$uuid_mock->mock( create_str => sub { '1' x 36; } );

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'demo',
    last_login_provider => 'demo',
    provider_id => 31415,
    group_identity => 'acme',
    contact_name => 'Andy Jones',
    contact_email => 'andy@broadbean.com',
    settings => { responses => { bg_language_pack => 'au' } }
});
$user->save();

my $test = {
    cv_content  => "TEST CV",
    email_subject => 'test', # The user 1 (the default in test) is andy
    email_body => 'Made Up Board 12345',
    candidate   => {
        destination                     => 'adcresponses',
        broadbean_adcboard_id           => '12345',
        broadbean_adcboard_nicename     => 'Made Up Board',
        name                            => 'John Smith',
        cv_html                         => 'TEST CV',
        search_record_id                => 1234,
    },

    query_ref => {
        messageContent => "[board_nice_name] [boardid]",
        messageSubject => "test",
        messageSubjectPrefix => "",
        messageEmail => 'recruiter@example.com',
        emailTemplateName => 'doodoo',
    },

    cvp_response => { email => 'john@example.com' },

    subscription_ref        => {
        talentsearch => {
            nice_name => 'Talent Search'
        },
        adcresponses => {
            nice_name => 'AdC Responses'
        }
    },
    board_nice_name         => 'Talent Search',
    candidate_has_cv        => 1,
    email_sent              => 1,
    status => 'success'
};

$test->{ signature } = Digest::SHA1::sha1_base64( $test->{query_ref}->{messageContent}, $test->{query_ref}->{messageSubject} );

# Test CVP is correctly used
my $cvp_client_module = Test::MockModule->new('Bean::CVP::Client');
$cvp_client_module->mock( 'parse_cv_contents' => sub {
    return { email => 'john@example.com' };
});


my $email_sent = 0;
my $email;

my $email_module = Test::MockModule->new('Email::Sender::Simple');
$email_module->mock( send => sub {
    my ($self, $local_email ) = @_;
    $email = $local_email;
    $email_sent = 1;
    1;
});

my $cand_api_module = Test::MockModule->new('Stream2::Action::DownloadCV');
$cand_api_module->mock('instance_perform' => sub {
    my ( $self ) = @_;
    return HTTP::Response->new( 200, "OK",
        [ "Content-Type" => "text/xml", "Content-Disposition" => q{inline; filename="file.txt"} ],
        $test->{cv_content}
    );
});

my $candidate = Stream2::Results::Result->new(
    $test->{candidate}
);
$candidate->has_cv(1);
$user->subscriptions_ref( $test->{subscription_ref} );

my $email_action = Stream2::Action::MessageCandidate->new({
    stream2 => $stream2,
    base_url => 'http://localhost.com/',
    message_bcc => '',
    message_subject => $test->{query_ref}->{messageSubject},
    message_content => $test->{query_ref}->{messageContent},
    message_email => $test->{query_ref}->{messageEmail},
    subject_prefix => $test->{query_ref}->{messageSubjectPrefix},
    email_template_name => $test->{query_ref}->{emailTemplateName},
    jobid => 'blabla',
    candidate_object => $candidate,
    user_object => $user,
    ripple_settings => $user->ripple_settings(),
    advert_id => 123
});

my $mocked_email_action = Test::MockModule->new( ref( $email_action ) );
$mocked_email_action->mock( get_advert => sub{ return { AdvertID => 123 }; });
$mocked_email_action->mock( _advert_preview_url => sub { return '<a href="link">Advert title</a>'; } );

eval{
    $email_action->instance_perform();
};
if( my $err = $@ ){
    if( $test->{status} eq 'error' ){
        pass("Correctly erroneous");
    }else{
        fail("Test ".Dumper($test)." is supposed to pass");
        die $err;
    }
}

is ( $email_sent, $test->{email_sent} );

{
    my $last_action = $stream2->factory('CandidateActionLog')->search(undef, { order_by => { -desc => 'id' } })->first();
    is( $last_action->search_record_id() , 1234 );
    is_deeply( $last_action->data(), {
        s3_log_id           => 'blabla',
        candidate_name      => $candidate->name(),
        email_template_name => 'doodoo',
        mailjet_custom_id   => Data::UUID->new->create_str,
        mailjet_account     => 'transport',
        ( $candidate->email() ? ( candidate_email => $candidate->email() ) : () ),
        ( $last_action->action eq 'message' ? ( signature => $test->{signature} ) : () ),
    });
}

if( $test->{email_sent} ){
    is( $email->get_header('Subject') , Encode::encode( 'MIME-Q', ' ' . $test->{email_subject} . "\n" ) ); # space is added due to empty prefix, newline due to Email::Header
    ok( $email->get_header('List-unsubscribe'),"has unsubscribe header link");
    like( $email->get_header('List-unsubscribe'),qr(external/unsubscribe/[\w\-]{36}/auto),'valid_link');

    like( MIME::QuotedPrint::decode_qp($email->get_body), qr/$test->{email_body}/, 'email content is placeholdered');
}

END {
    LWP::UserAgent::Mockable->finished();
    done_testing();
}

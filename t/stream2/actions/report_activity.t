#! perl -w

use Test::Most;
use Test::MockModule;

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;


use Log::Any::Adapter;

# Log::Any::Adapter->set('Stderr');

use Stream2;
use Test::Stream2;
use Stream2::Action::ReportActivity;
use Stream2::O::File;


my $stream2 = Test::Stream2->new( { config_file => 't/app-stream2.conf' } );

$stream2->deploy_test_db;

my $user = $stream2->factory('SearchUser')->new_result(
    {   provider            => 'dummy',
        last_login_provider => 'dummy',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_name        => 'Andy Jones',
        contact_email       => 'andy@broadbean.com',
        settings            => { responses => { bg_language_pack => 'au' } }
    }
);
$user->save();


my $report_activity = Stream2::Action::ReportActivity->new(
    stream2          => $stream2,
    user_object      => $user,
    ripple_settings  => {},
    jobid            => "Calling Smithy",
);

my $mock_report_activity = Test::MockModule->new( ref( $report_activity ) );
# Note that this file needs to be stable (IE exactly the same from one test run
# to another) to have this test work with S3 + LWP::UA::Mockable
$mock_report_activity->mock( 'generate_files' => sub{
                                 return [ Stream2::O::File->new({
                                     name => 'kitten.jpg',
                                     mime_type => 'image/jpeg',
                                     disk_file => 't/files/kitten.jpg',
                                     persistent => 1,
                                 }) ];
                             });

# run the instance
ok( my $result = $report_activity->instance_perform() );
ok( $result->{s3_files} , "Ok got s3 files" );
ok( $result->{log_chunk_id} , "Ok got log chunk ID" );


END {
    LWP::UserAgent::Mockable->finished();
    done_testing();
}

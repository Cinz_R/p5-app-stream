use Test::Most;
use Stream2::Action::TagCandidate;
use Log::Any::Adapter;
use Test::MockModule;

use Test::Stream2;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'" . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ), "Ok can build stream2" );

$stream2->deploy_test_db;

my $user = $stream2->factory('SearchUser')->new_result(
    {
        provider            => 'demo',
        last_login_provider => 'demo',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_name        => 'Andy Jones',
        contact_email       => 'andy@broadbean.com',
        settings            => { responses => { bg_language_pack => 'au' } }
    }
);
$user->save();

my $mock_result = Test::MockModule->new( 'Stream2::Results::Result' );
my $bbcss_mock = Test::MockModule->new( 'Stream::Templates::BBCSS' );
my $tag_mock = Test::MockModule->new( 'Stream2::Action::TagCandidate');

subtest "Not adcresponses" => sub {

    my $candidate = Stream2::Results::Result->new(
        {
            candidate_id => 555,
            destination => 'candidatesearch',
            name        => 'John Smith',
        }
    );

    my $bbcss_called = 0;
    $bbcss_mock->mock( tag_candidate => sub {
        $bbcss_called++;
        my ( $self, $candidate, $tag ) = @_;
        is ( $candidate->candidate_id, 555 );
        is ( $tag->tag_name, 'sausage' );
    });
    $tag_mock->mock( tag_internal => sub {
        fail( "I should not be called" );
    });

    my $tag_candidate = Stream2::Action::TagCandidate->new(
        stream2          => $stream2,
        user_object      => $user,
        candidate_object => $candidate,
        jobid            => "Tagging Smithy",
        ripple_settings  => $user->ripple_settings(),
        tag_name         => 'sausage'
    );

    # run the instance
    $tag_candidate->instance_perform();

    is( $bbcss_called, 1, "bbcss was called" );
};

subtest "adcresponses with tsimport" => sub {
    my $candidate = Stream2::Results::Result->new(
        {
            candidate_id => 555,
            destination => 'adcresponses',
            name        => 'John Smith',
        }
    );
    $candidate->attr( 'tsimport_id', 123 );
    $tag_mock->unmock_all();

    my $bbcss_called = 0;
    $bbcss_mock->mock( tag_candidate => sub {
        $bbcss_called++;
        my ( $self, $candidate, $tag ) = @_;
        is ( $candidate->candidate_id, 555 );
        is ( $tag->tag_name, 'sausage' );
    });

    my $get_internal_called = 0;
    $mock_result->mock( 'get_internal_db_ids' => sub {
        $get_internal_called++;
        +{
            'Artirix' => '142802203'
        };
    });

    my $tag_candidate = Stream2::Action::TagCandidate->new(
        stream2          => $stream2,
        user_object      => $user,
        candidate_object => $candidate,
        jobid            => "Tagging Smithy",
        ripple_settings  => $user->ripple_settings(),
        tag_name         => 'sausage'
    );

    # run the instance
    $tag_candidate->instance_perform();

    my @all_longsteps = $stream2->stream_schema->resultset('LongStepProcess')->search()->all();

    is( $bbcss_called, 1, "bbcss was called" );
    is( $get_internal_called, 1, "get_internal_db_ids was called" );
    is( scalar( @all_longsteps ), 1, "A longstep process has been added" );
    is( $all_longsteps[0]->state->{tag_name}, 'sausage' );
};


done_testing();

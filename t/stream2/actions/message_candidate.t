#! perl

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';

    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Data::Dumper;
use JSON qw/decode_json/;
use Stream2;
use Test::MockModule;
use Encode;
use Digest::SHA1;

use Data::Dumper;

use Stream2::Action::MessageCandidate;

# use Log::Any::Adapter qw/Stderr/;

my $config_file = 't/app-stream2.conf';
ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
# Deploying schema
$stream2->stream_schema()->deploy();

# Mock the S3
{ package MockedS3Object; use base qw/Class::AutoAccess/; sub new{ bless $_[1], $_[0]; } 1; }
my $files_mock = Test::MockModule->new( ref( $stream2->factory('PrivateFile') ) );
$files_mock->mock( 'create' => sub{
                       return MockedS3Object->new({ key => 'abcdefg'});
                   });

# Mock this as the email service stores emails we send.
my $email_client = Test::MockModule->new('Bean::EmailService::Client');
$email_client->mock( store_email => sub {
    return;
});

# We need a constant, predictable value for Mailjet CustomID
my $uuid_mock = Test::MockModule->new('Data::UUID');
$uuid_mock->mock( create_str => sub { '1' x 36; } );

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'demo',
    last_login_provider => 'demo',
    provider_id => 31415,
    group_identity => 'acme',
    contact_name => 'Andy Jones',
    contact_email => 'andy@broadbean.com',
    settings => { responses => { bg_language_pack => 'au' } }
});
$user->save();

my $defaults = {
    cv_content  => "TEST CV",

    email_subject => 'TEST PREFIX test subject Andy Jones andy@broadbean.com Plus something very long that will...', # The user 1 (the default in test) is andy

    email_body => 'this is a test with some Talent Search Andy Jones <a href="link">Advert title</a> placeholders',
    candidate   => {
        destination     => 'talentsearch',
        name            => 'John Smith',
        cv_html         => 'TEST CV',
        search_record_id => 1234,
    },

    query_ref => {
        messageContent => "this is a test with some [board_nice_name] [contactname] [advert_link] placeholders \N{U+262D}",
        messageSubject => "test subject [contactname] [consultant_email] Plus something very long that will probably be over 80 characters, well I hope anyway, otherwise I cannot test that properly. Innit",
        messageSubjectPrefix => "TEST PREFIX",
        messageEmail => 'recruiter@example.com',
        emailTemplateName => 'doodoo',
    },

    cvp_response => { email => 'john@example.com' },

    subscription_ref        => {
        talentsearch => {
            nice_name   => 'Talent Search'
        }
    },
    board_nice_name         => 'Talent Search',
    candidate_has_cv        => 1,
    email_sent              => 1,
    status => 'success',
    expected_reply_to => 'recruiter@example.com'
};

$stream2->stream_schema->resultset('EmailBlacklist')->create({
    email => 'blocked@example.com'
});

my $tests = [ map {
    { %$defaults, %$_ }
} @{[
    {},
    {
        candidate   => {
            destination     => 'talentsearch',
            name            => 'John Smith',
            email           => 'john@example.com',
            cv_html         => 'TEST CV',
            search_record_id => 1234,
        },
        cvp_response => { email => 'blacked@example.com' }
    },
    {
        candidate => {
            destination     => 'talentsearch',
            name            => 'John Smith',
            cv_html         => 'TEST CV',
            search_record_id => 1234,
        },
        cvp_response => {},
        email_sent => 0,
        status => 'error'
    },
    {
        candidate => {
            destination     => 'talentsearch',
            name            => 'John Smith',
            email           => 'blocked@example.com',
            cv_html         => 'TEST CV',
            search_record_id => 1234,
        },
        email_sent => 0,
        status => 'error'
    },
    {
        query_ref => {
            messageContent => "this is a test with some [board_nice_name] [contactname] [advert_link] placeholders",
            messageSubject => "test subject [contactname] [consultant_email] Plus something very long that will probably be over 80 characters, well I hope anyway, otherwise I cannot test that properly. Innit",
            messageSubjectPrefix => "TEST PREFIX",
            messageEmail => '[consultant_email] this is a valid email address regardless of this nonsense',
            emailTemplateName => 'doodoo',
        },
        expected_reply_to => 'andy@broadbean.com this is a valid email address regardless of this nonsense'
    },
    {
        query_ref => {
            messageContent => "this is a test with some [board_nice_name] [contactname] [advert_link] placeholders",
            messageSubject => "test subject [contactname] [consultant_email] Plus something very long that will probably be over 80 characters, well I hope anyway, otherwise I cannot test that properly. Innit",
            messageSubjectPrefix => "TEST PREFIX",
            messageEmail => 'this is a load of nonsense',
            emailTemplateName => 'doodoo',
        },
        status => 'error',
        email_sent => 0,
        expected_error => qr/Invalid.*?Reply To/
    }


]}];

foreach my $test ( @$tests ) {
    # Test CVP is correctly used
    my $cvp_client_module = Test::MockModule->new('Bean::CVP::Client');
    $cvp_client_module->mock( 'parse_cv_contents' => sub {
        my ( $self, $content, $options ) = @_;
        is( $options->{tagger_locale}, 'en_au', 'cv parse locale is passed to parser' );
        is( $content, $test->{cv_content}, 'cv content is passed to parser' );
        return $test->{cvp_response};
    });

    #######################
    # LOAD CANDIDATE + CV #
    #######################

    my $email_sent = 0;
    my $email;

    my $email_module = Test::MockModule->new('Email::Sender::Simple');
    $email_module->mock( send => sub {
                            my ($self, $local_email ) = @_;
                            $email = $local_email;
                            $email_sent = 1;
                            1;
                         });

    my $cand_api_module = Test::MockModule->new('Stream2::Action::DownloadCV');
    $cand_api_module->mock('instance_perform' => sub {
                               my ( $self ) = @_;
                               return HTTP::Response->new( 200, "OK", 
                                                           [ "Content-Type" => "text/xml", "Content-Disposition" => q{inline; filename="file.txt"} ],
                                                           $test->{cv_content} );
                           });
    my $candidate = Stream2::Results::Result->new(
        $test->{candidate}
    );
    $candidate->has_cv( $test->{candidate_has_cv} );
    $user->subscriptions_ref( $test->{subscription_ref} );

    #     # Also stash the current user, as calling 'current_user' is expensive.
    #     $self->stash('current_user' , $user );
    #     $self->stash('job_user_ref' , $user_ref);
    #     # We also need the subscription (board) info.
    #     $self->stash('board_name' , $test->{candidate}->{destination});
    #     $self->stash('subscription_info' , { nice_name => $test->{board_nice_name} } );
    #     return 1;
    # });

    my $email_action = Stream2::Action::MessageCandidate->new({
        stream2 => $stream2,
        base_url => 'http://localhost.com/',
        message_bcc => '',
        message_subject => $test->{query_ref}->{messageSubject},
        message_content => $test->{query_ref}->{messageContent},
        message_email => $test->{query_ref}->{messageEmail},
        subject_prefix => $test->{query_ref}->{messageSubjectPrefix},
        email_template_name => $test->{query_ref}->{emailTemplateName},
        jobid => 'blabla',
        candidate_object => $candidate,
        user_object => $user,
        ripple_settings => $user->ripple_settings(),
        advert_id => 123
    });

    ok(my $template_stash =  $email_action->get_template_stash() );
    foreach my $expected_field ( qw/contactname
                                    consultant
                                    consultant_email
                                    feedbackemail
                                    team
                                    office
                                    company
                                    from_email
                                    boardid
                                    nice_board_name
                                    template_name
                                    eza_number
                                    custom_source_code
                                    method_id
                                    method_name/ ){
        ok( exists( $template_stash->{$expected_field} ) , "Ok expected field is in stash");
    }

    my $mocked_email_action = Test::MockModule->new( ref( $email_action ) );
    $mocked_email_action->mock( get_advert => sub{ return {}; });
    $mocked_email_action->mock( _advert_preview_url => sub { return '<a href="link">Advert title</a>'; } );

    my $out = eval{
        $email_action->instance_perform();
    };
    if( my $err = $@ || $out->{error}->{message} ){
        if( $test->{status} eq 'error' ){
            if ( my $expected_err = $test->{expected_error} ) {
                $err =~ $expected_err ? pass("Correctly erroneous") : fail("Died correctly, but with wrong error") && die $err;
            }
            else {
                pass("Correctly erroneous");
            }
        }
        else {
            fail("Test ".Dumper($test)." is supposed to pass");
            die $err;
        }
    }

    is ( $email_sent, $test->{email_sent} );

    {
        my $last_action = $stream2->factory('CandidateActionLog')->search( undef, { order_by => { -desc => 'id' } } )->first();
        # generate the message signature.
        my $signature = Digest::SHA1::sha1_base64(
            Encode::encode_utf8( $test->{query_ref}->{messageContent} ),
            Encode::encode_utf8( $test->{query_ref}->{messageSubject} ) );

        is( $last_action->search_record_id(), 1234 );
        is_deeply(
            $last_action->data(),
            {   s3_log_id           => 'blabla',
                candidate_name      => $candidate->name(),
                email_template_name => 'doodoo',
                ( $candidate->email()               ? ( candidate_email => $candidate->email() ) : () ),
                ( $last_action->action eq 'message' ? ( signature       => $signature )          : () ),
                ( $test->{email_sent} ?
                    (
                        mailjet_custom_id => Data::UUID->new->create_str,
                        mailjet_account   => 'transport'
                    ) : ()
                ),
            }
        );
    }


    if( $test->{email_sent} ){
        is( Encode::decode( 'MIME-Header', $email->get_header('Subject')), $test->{email_subject}."\n", 'Subject is correct'); # newline due to Email::Header
        ok( $email->get_header('List-unsubscribe'),"has unsubscribe header link");
        like( $email->get_header('List-unsubscribe'),qr(external/unsubscribe/[\w\-]{36}/auto),'valid_link');

        like( MIME::QuotedPrint::decode_qp($email->get_body), qr/$test->{email_body}/, 'email content is placeholdered'); # quoted-printable creates =3D instead of =

        my $sent_email = $stream2->stream_schema->resultset('SentEmail')->search_rs({ subject => $test->{email_subject} })->first;
        ok (defined $sent_email, 'email with subject exists in DB');

        is( $email->get_header('Reply-To'), $test->{expected_reply_to} );
    }
}

END {
    LWP::UserAgent::Mockable->finished();
    done_testing();
}

#! perl
use strict;
use warnings;

# emails should be seen to test
BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';

    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}

use LWP;
use LWP::UserAgent::Mockable;

use Test::More;
use Test::Exception;
use Test::MockModule;
use Stream2::Criteria;

use Data::Dumper;
use Stream2;
use Stream2::Action::ForwardCandidate;

use Log::Any::Adapter;

# toggle verbose errors to go to STDERR
# use Carp::Always;
# Log::Any::Adapter->set('Stderr');

use Test::Stream2;

my $config_file = 't/app-stream2.conf';

my $stream2 = Test::Stream2->new( { config_file => $config_file } );
$stream2->deploy_test_db;

# mock some stuff
my $stream2_api_mock = Test::MockModule->new( ref $stream2->stream_api );
$stream2_api_mock->mock( get_forward_email_settings => sub { return undef; } );

# we need a constant, predictable value for Mailjet CustomID
my $uuid_mock = Test::MockModule->new('Data::UUID');
$uuid_mock->mock( create_str => sub { '1' x 36; } );

my $user_module = Test::MockModule->new('Stream2::O::SearchUser');
$user_module->mock( 'settings_values_hash' => sub {
    return {
        'forwarding-candidate_email_subject' => 'Subject from the search settings.',
    };
});

my $search_query = {
    "_searchOptions" => {},
    "board"          => "talentsearch",
    "criteria"       => {
        "location_within"       => 30,
        "cv_updated_within"     => "3M",
        "default_jobtype"       => "",
        "salary_cur"            => "",
        "salary_per"            => "",
        "salary_from"           => "",
        "salary_to"             => "",
        "keywords"              => undef,
        "talentsearch_order_by" => "Relevance",
    },
    "_tokens" => {
        'keywords' => 'wooooo'
    }
};

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser'),
    "Ok can get resultset" );

my $users_factory = $stream2->factory('SearchUser');

my @user_ids;

my $forwarding_user = $users_factory->new_result(
    {   contact_name        => 'Scrooge McDuck',
        contact_email       => 'scrooge_mcduckmoo@DTinc.com',
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 0,
        group_identity      => 'acme',
        contact_details => { username => 'scrooge@theteam.theoffice.acme' },
        subscriptions_ref   => {
            cvlibrary => {
                nice_name   => "CV library",
                auth_tokens => {
                    cvlibrary_username => 'smd',
                    cvlibrary_password => 'smdpass'
                },
                type               => "social",
                allows_acc_sharing => 0,
            },
            adcresponses => {
                type => 'internal',
                nice_name => 'Responses'
            }
        }
    }
);

# save and keep the user id for forwarding
push @user_ids, $forwarding_user->save();

my $criteria = Stream2::Criteria->from_query( $stream2->stream_schema(), $search_query->{criteria} );
my $search_record = $stream2->stream_schema()->resultset('SearchRecord')->create({
    id => 1234,
    user_id => $forwarding_user->id,
    criteria_id => $criteria->id,
});



my @cv_library_users = (
    {   contact_name        => 'Huey',
        contact_email       => 'huey@DTinc.com',
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 1,
        group_identity      => 'acme',
        subscriptions_ref   => {
            cvlibrary => {
                nice_name   => "CV library",
                auth_tokens => {
                    cvlibrary_username => 'hmd',
                    cvlibrary_password => 'hmdpass'
                },
                type               => "social",
                allows_acc_sharing => 0,
            }
        }
    },
    {   contact_name        => 'Duey',
        contact_email       => 'duey@DTinc.com',
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 2,
        group_identity      => 'acme',
        subscriptions_ref   => {
            cvlibrary => {
                nice_name   => "CV library",
                auth_tokens => {
                    cvlibrary_username => 'dmd',
                    cvlibrary_password => 'dmdpass'
                },
                type               => "social",
                allows_acc_sharing => 0,
            }
        }
    },
    {   contact_name        => 'louie',
        contact_email       => 'louie@DTinc.com',
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 9,
        group_identity      => 'acme',
        subscriptions_ref   => {
            flickr_xray => {
                nice_name   => "Flickr",
                auth_tokens => {
                    flickr_username => 'lmd',
                    flickr_password => 'lmd_pass'
                },

                type               => "social",
                allows_acc_sharing => 0,
            }
        }
    }
);

# load in the users and keep their Ids for the forward
for my $user (@cv_library_users) {
    push @user_ids, $users_factory->new_result($user)->save;
}

# Set up my Candidate
my $candidate = Stream2::Results::Result->new();
$candidate->name('Lanchpad McQuack');
$candidate->email('lanchpadmcquak@Quackwerks.com');
$candidate->candidate_id('5318008');
$candidate->destination('cvlibrary');
$candidate->attr('search_record_id' , 1234);

# we dont have a CV for this user, so use the build_forward_profile (which will need to be mocked)
$candidate->has_cv(0);
$candidate->has_profile(1);

my $forward_candidate_package
    = Test::MockModule->new('Stream2::Action::ForwardCandidate');
$forward_candidate_package->mock(
    'build_forward_profile' => sub {
        my ($self) = @_;
        return "<p>DUCKTAILS!</p>";
    }
);

# this tests that a user receiving a CV from cvlibrary user has a current subscription
subtest 'user_receiving_CV_from_CVLibrary_has_subscription' => sub {
    my $forwardCV = Stream2::Action::ForwardCandidate->new(
        jobid             => "DUCKTAIL Wo0o0o0o",
        stream2           => $stream2,
        user_object       => $forwarding_user,
        ripple_settings   => $forwarding_user->ripple_settings(),
        candidate_object  => $candidate,
        message           => "This guy would make a great pilot",
        recipients_ids    => \@user_ids,
        recipients_emails => [ 'gerard@mensoif.com' ]
    );

    # run the instance
    my @result = $forwardCV->instance_perform();

    # Check there is an action record.
    my @all_actions = $stream2->factory('CandidateActionLog')->search()->all();
    is( scalar ( @all_actions ) , 1 );
    is( $all_actions[0]->search_record_id() , 1234 );
    is_deeply( $all_actions[0]->data(), { s3_log_id => 'DUCKTAIL Wo0o0o0o',
                                          'recipients_emails' => [
                                              'scrooge_mcduckmoo@DTinc.com',
                                              'huey@DTinc.com',
                                              'duey@DTinc.com',
                                              'gerard@mensoif.com'
                                          ],
                                          mailjet_custom_id => Data::UUID->new->create_str,
                                          mailjet_account => 'static_transport',
                                          'message' => 'This guy would make a great pilot',
                                          'candidate_name' => 'Lanchpad McQuack',
                                          'candidate_email' => 'lanchpadmcquak@Quackwerks.com'
                                      });

    my @deliveries = Email::Sender::Simple->default_transport->deliveries;

    # check the emails sent
    is( scalar(@deliveries), 1, "Ok 1 email should be sent" );
    is( $deliveries[0]->{envelope}->{to}->[0],
        'scrooge_mcduckmoo@DTinc.com',
        "Scrooge should be emailed"
    );
    is( $deliveries[0]->{envelope}->{to}->[1],
        'huey@DTinc.com', "Huey should be emailed" );
    is( $deliveries[0]->{envelope}->{to}->[2],
        'duey@DTinc.com', "Duey should be emailed" );
    is( $deliveries[0]->{envelope}->{to}->[3],
        'gerard@mensoif.com', "Gerard should be emailed" );

    # check if Reply-To header is set
    is ( $deliveries[0]->{email}->get_header('Reply-To'), $forwarding_user->contact_email(),
        'Reply-To header is set to user email'
    );

    # check for the warnings
    is( $result[0]->{warnings}->[0],
        "louie doesn't have a CV-Library user license. Please contact your administrator.",
        "Louie should have a warning"
    );

    # check if Reply-To header is set
    is ( $deliveries[0]->{email}->get_header('Reply-To'), $forwarding_user->contact_email(),
        'Reply-To header is set to user email'
    );

    # now change the juice setting for BCC
    $stream2_api_mock->mock( get_forward_email_settings => sub {
        return {
            bcc     => 'webbigail@DTinc.com,duckworth@DTinc.com,ñoñó1234@server.com',
            subject => 'Subject from the Juice settings',
        };
     });

    my $userapi_client_module = Test::MockModule->new('Bean::Juice::APIClient::User');
    $userapi_client_module->mock( get_user => sub {
        return {
            consultant => 'andy',
            team       => 'test',
            office     => 'whatever',
            company    => 'andy'
        };
    });

    # clear
    clear_deliveries();

    $forwardCV->instance_perform();
    @deliveries = Email::Sender::Simple->default_transport->deliveries;

    is( scalar(@deliveries), 3, "Ok 3 emails should be sent" ); # a 'forward' and 2 'bcc'

    # check the subject was picked up from the search settings
    is( $deliveries[0]->{email}->get_header('Subject'), Encode::encode('MIME-Q', 'Subject from the search settings.' ), "Subject should be from search settings" );
    is( $deliveries[1]->{envelope}->{to}->[0], 'webbigail@DTinc.com', "Webbi should be emailed" );
    is( $deliveries[2]->{envelope}->{to}->[0], 'duckworth@DTinc.com', "Duckworth should be emailed" );

    is( $deliveries[1]->{email}->get_header('Subject'), Encode::encode('MIME-Q', 'Subject from the Juice settings' ), "Subject should be from Juice settings" );

    # Turn off the juice setting since $stream2_api_mock is scoped globally.
    $stream2_api_mock->mock( get_forward_email_settings => sub { return undef; } );

    # clear
    clear_deliveries();
};

subtest 'email_not_sent_no_recipients' => sub {

    my $forwardCV = Stream2::Action::ForwardCandidate->new(
        jobid             => "DUCKTAIL Wo0o0o0o",
        stream2           => $stream2,
        user_object       => $forwarding_user,
        ripple_settings   => $forwarding_user->ripple_settings(),
        candidate_object  => $candidate,
        message           => "This guy would make a great pilot",
        recipients_ids    => [10],
        recipients_emails => []
    );

    # run the instance that gets an exception
    my $result = $forwardCV->instance_perform();
    like($result->{error}->{message}, qr/Forwarding error/, 'test for failure if there was no Email forwarded due to lack of recipients emails');

    # clear
    clear_deliveries();
};

$candidate->destination('dummy');

subtest 'forward_candidate_running_macros' => sub {
    my $forwardCV = Stream2::Action::ForwardCandidate->new(
        jobid             => "DUCKTAIL Wo0o0o0o",
        stream2           => $stream2,
        user_object       => $forwarding_user,
        ripple_settings   => $forwarding_user->ripple_settings(),
        candidate_object  => $candidate,
        message           => "This guy would make a great pilot",
        recipients_ids    => \@user_ids,
        recipients_emails => []
    );

    my $has_done_import = 0;
    my $mock_import = Test::MockModule->new('Stream2::Action::ImportCandidate');
    $mock_import->mock( instance_perform => sub{
                            my ($self) = @_;
                            $has_done_import = 1;
                            $self->_trigger_macros();
                        });

    # We also want to save the candidate
    $stream2->factory('GroupMacro')->create({ group_identity => 'acme',
                                              name => 'also_save',
                                              on_action => 'Stream2::Action::ForwardCandidate',
                                              lisp_source => q|( if ( s2#user-in-office "theoffice" ) ( s2#do-action "ImportCandidate" ) )|
                                          });

    # Create some horrible recursion.
    $stream2->factory('GroupMacro')->create({ group_identity => 'acme',
                                              name => 'also_save_recurse',
                                              on_action => 'Stream2::Action::ImportCandidate',
                                              lisp_source => q|( if ( s2#user-in-office "theoffice" ) ( s2#do-action "ImportCandidate" ) )|
                                          });

    # And another macro that goes bang. Because why not.
    $stream2->factory('GroupMacro')->create({ group_identity => 'acme',
                                              name => 'go_bang',
                                              on_action => 'Stream2::Action::ForwardCandidate',
                                              lisp_source => q|( if (  s2#user-in-office "theoffice" ) ( s2#do-crash ) )|
                                          });
    # run the instance
    # Note that it will also run the macros,
    # as the class of $forwardCV also consumes the TriggerMacros role
    my $result = $forwardCV->instance_perform();

    is( $result->{message} , "success - With macros 'also_save', 'go_bang'" );

    ok( $has_done_import , "Has done the import as well!");

    my @deliveries = Email::Sender::Simple->default_transport->deliveries;

    # check the emails sent
    is( scalar(@deliveries), 1, "there should be one email" );
    is( $deliveries[0]->{envelope}->{to}->[0],
        'scrooge_mcduckmoo@DTinc.com',
        "Scrooge should be emailed"
    );
    is( $deliveries[0]->{envelope}->{to}->[1],
        'huey@DTinc.com', "Huey should be emailed" );
    is( $deliveries[0]->{envelope}->{to}->[2],
        'duey@DTinc.com', "Duey should be emailed" );
    is( $deliveries[0]->{envelope}->{to}->[3],
        'louie@DTinc.com', "Louie should be emailed" );

    like( $deliveries[0]->{email}->get_body() , qr/Other attachments/ );
    like( $deliveries[0]->{email}->get_body() , qr/someubercoolfile\.txt/ );

    # check for the warnings
    like( $result->{warnings}->[0], qr|\QSomething went very wrong in Stream2::Lisp::Core\E|, "there should be one warning about the failing macro" );

    clear_deliveries();
};

$candidate->destination('adcresponses');

subtest 'new manage responses candidate emails that are forwarded contain advert details' => sub {
    my $advertapi_mock = Test::MockModule->new('Bean::AdcApi::Client');
    $advertapi_mock->mock(request_json => sub {
        return {
            Advert => {
                JobReference    => 'jobref1234',
                JobTitle        => 'Test title',
                AdvertID        => 1234,
                Consultant      => 'whatever',
                AplitrakID      => 'blabla',
            }
        };
    });
    $candidate->set_attr('broadbean_adcadvert_id' => 1234);

    my $forwardCV = Stream2::Action::ForwardCandidate->new(
        jobid             => "DUCKTAIL Wo0o0o0o",
        stream2           => $stream2,
        user_object       => $forwarding_user,
        ripple_settings   => $forwarding_user->ripple_settings(),
        candidate_object  => $candidate,
        message           => "This guy would make a great pilot",
        recipients_ids    => \@user_ids,
        recipients_emails => []
    );
    my $result = $forwardCV->instance_perform();

    is( $result->{message} , "success - With macros 'also_save', 'go_bang'" );

    my @deliveries = Email::Sender::Simple->default_transport->deliveries;

    # check the emails sent
    is( scalar(@deliveries), 1, "there should be one email" );
    is( $deliveries[0]->{envelope}->{to}->[0],
        'scrooge_mcduckmoo@DTinc.com',
        "Scrooge should be emailed"
    );

    like( $deliveries[0]->{email}->get_body() , qr/Test title/, 'email contains advert details' );

    clear_deliveries();
};

sub clear_deliveries {
    Email::Sender::Simple->default_transport->clear_deliveries;
}

END {
    LWP::UserAgent::Mockable->finished();
    done_testing();
}

use Test::Most;

use Test::Stream2;
use Test::MockModule;
use Stream2::Criteria;
use Stream2::Action::Search;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();
my $schema = $stream2->stream_schema;

my @results = (
    Stream2::Results::Result->new({
        candidate_id => 1,
        email => 'cameron@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Tokyo',
        name => 'David Cameron'
    }),
    Stream2::Results::Result->new({
        candidate_id => 2,
        email => 'may@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Djibouti',
        name => 'Theresa May'
    }),
    Stream2::Results::Result->new({
        candidate_id => 3,
        email => 'brown@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Sandringham',
        name => 'Gordon Brown'
    }),
    Stream2::Results::Result->new({
        candidate_id => 4,
        email => 'jezza@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Woodstock',
        name => 'Jeremy Corbyn'
    })
);

my $feed_mock = Test::MockModule->new( 'Stream::Templates::twitter_xray' );
$feed_mock->mock( quota_guard => sub { pop->() } );
# results store that doesn't go to redis
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { shift->results; } );
$results_mock->mock( fetch_results => sub { @results } );

my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( redis => sub { MyRedis->new(); } );

my $criteria = Stream2::Criteria->new( schema => $schema );
$criteria->add_token( 'keywords' , 'php developer');
$criteria->add_token('cv_updated_within' , 'TODAY' );
$criteria->save();

# Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider            => 'adcourier',
    last_login_provider => 'dummy',
    provider_id         => 31415,
    group_identity      => 'acme',
    subscriptions_ref   => {
        twitter_xray    => {
            nice_name   => "Twitter",
            type        => "social",
            auth_tokens => {}
        }
    }
});
my $user_id = $memory_user->save();

subtest 'Normal search reports success appropriately' => sub {

    my ( $search_called, $inc_anal_called, $calendar_called );

    # return results as expected
    $feed_mock->mock( search => sub {
        $search_called = 1;
        my $self = shift;
        $self->total_results( scalar( @results ) );
        return @results;
    });
   
    $stream2_mock->mock( increment_analytics => sub {
        my ( $self, %content ) = @_;
        $inc_anal_called = 1;
        is_deeply(
            \%content,
            {
                'successful_threads' => 1,
                'board' => 'twitter_xray',
                'user_id' => '31415',
                'threads' => 1,
                'results' => 4
            },
            "We send a success report to the mongdb"
        );
    });
    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $content ) = @_;
        $calendar_called = 1;

        ok( defined( delete $content->{duration} ) );

        is_deeply(
            $content,
            {
                'remote_ip' => '123.456.789.10',
                'destination' => 'twitter_xray',
                'user_provider' => 'adcourier',
                'user_provider_id' => '31415',
                'n_results' => 4,
                's3_log_id' => 'doesnt matter',
                'stream2_base_url' => 'http://127.0.0.1',
                'criteria_id' => 'nbAxl-U__swCHRLFT1OEx6THMz0',
                'search_record_id' => '1',
                'user_stream2_id' => 1,
                'company' => 'acme'
            },
            "We send a success report to the calendar"
        );
    });

    my $results = Stream2::Action::Search->new({
        criteria_id         => $criteria->id(),
        template_name       => 'twitter_xray',
        stream2_base_url    => 'http://127.0.0.1',
        stream2             => $stream2,
        user                => { user_id => $user_id },
        ripple_settings     => {},
        log_chunk_capture   => 0,
        options             => { results_per_page => 11 },
        jobid               => 'doesnt matter',
        remote_ip           => '123.456.789.10'
    })->instance_perform();

    my $record = $stream2->factory('SearchRecord')->search({},{ order_by => { -desc => 'id' } })->first();
    is_deeply(
        {
            ( map { $_ => $record->get_column($_) } qw/remote_ip n_results board criteria_id user_id/ ),
            data => $record->data
        },
        {
            'remote_ip'         => '123.456.789.10',
            'n_results'         => 4,
            'board'             => 'twitter_xray',
            'criteria_id'       => 'nbAxl-U__swCHRLFT1OEx6THMz0',
            'user_id'           => 1,
            'data'              => {
                's3_log_id' => 'doesnt matter'
            }
        },
        "The search record is as expected"
    );

    ok( $search_called && $inc_anal_called && $calendar_called, "All recording mechanisms have been informed" );
};

subtest 'Unmanaged fail search reports fail appropriately' => sub {

    my ( $search_called, $inc_anal_called, $calendar_called );

    # return results as expected
    $feed_mock->mock( search => sub {
        $search_called = 1;
        die "I die unmanagedly";
    });
   
    $stream2_mock->mock( increment_analytics => sub {
        my ( $self, %content ) = @_;
        $inc_anal_called = 1;
    });

    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $content ) = @_;
        $calendar_called = 1;

        ok( defined( delete $content->{duration} ) );

        is_deeply(
            $content,
            {
                'remote_ip' => '123.456.789.10',
                'destination' => 'twitter_xray',
                'user_provider' => 'adcourier',
                'user_provider_id' => '31415',
                's3_log_id' => 'doesnt matter',
                'stream2_base_url' => 'http://127.0.0.1',
                'criteria_id' => 'nbAxl-U__swCHRLFT1OEx6THMz0',
                'search_record_id' => '2',
                'user_stream2_id' => 1,
                'company' => 'acme',
                'error_type' => 'UNKNOWN INTERNAL'
            },
            "We send a success report to the calendar"
        );
    });

    my $results = Stream2::Action::Search->new({
        criteria_id         => $criteria->id(),
        template_name       => 'twitter_xray',
        stream2_base_url    => 'http://127.0.0.1',
        stream2             => $stream2,
        user                => { user_id => $user_id },
        ripple_settings     => {},
        log_chunk_capture   => 0,
        options             => { results_per_page => 11 },
        jobid               => 'doesnt matter',
        remote_ip           => '123.456.789.10'
    })->instance_perform();

    my $record = $stream2->factory('SearchRecord')->search({},{ order_by => { -desc => 'id' } })->first();
    is_deeply(
        {
            ( map { $_ => $record->get_column($_) } qw/remote_ip n_results board criteria_id user_id/ ),
            data => $record->data
        },
        {
            'remote_ip'         => '123.456.789.10',
            'n_results'         => undef,
            'board'             => 'twitter_xray',
            'criteria_id'       => 'nbAxl-U__swCHRLFT1OEx6THMz0',
            'user_id'           => 1,
            'data'              => {
                's3_log_id'     => 'doesnt matter',
                'error_type'    => 'UNKNOWN INTERNAL'
            }
        },
        "The search record is as expected"
    );

    ok( $search_called && ! $inc_anal_called && $calendar_called, "All applicable recording mechanisms have been informed" );
};

subtest 'Managed fail search reports fail appropriately' => sub {

    my ( $search_called, $inc_anal_called, $calendar_called );

    # return results as expected
    $feed_mock->mock( search => sub {
        $search_called = 1;
        shift->throw_login_error( "I die managedly" );
    });
   
    $stream2_mock->mock( increment_analytics => sub {
        my ( $self, %content ) = @_;
        $inc_anal_called = 1;
    });

    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $content ) = @_;
        $calendar_called = 1;

        ok( defined( delete $content->{duration} ) );

        is_deeply(
            $content,
            {
                'remote_ip' => '123.456.789.10',
                'destination' => 'twitter_xray',
                'user_provider' => 'adcourier',
                'user_provider_id' => '31415',
                's3_log_id' => 'doesnt matter',
                'stream2_base_url' => 'http://127.0.0.1',
                'criteria_id' => 'nbAxl-U__swCHRLFT1OEx6THMz0',
                'search_record_id' => '3',
                'user_stream2_id' => 1,
                'company' => 'acme',
                'error_type' => 'LOGIN ERROR'
            },
            "We send a success report to the calendar"
        );
    });

    my $results = Stream2::Action::Search->new({
        criteria_id         => $criteria->id(),
        template_name       => 'twitter_xray',
        stream2_base_url    => 'http://127.0.0.1',
        stream2             => $stream2,
        user                => { user_id => $user_id },
        ripple_settings     => {},
        log_chunk_capture   => 0,
        options             => { results_per_page => 11 },
        jobid               => 'doesnt matter',
        remote_ip           => '123.456.789.10'
    })->instance_perform();

    my $record = $stream2->factory('SearchRecord')->search({},{ order_by => { -desc => 'id' } })->first();
    is_deeply(
        {
            ( map { $_ => $record->get_column($_) } qw/remote_ip n_results board criteria_id user_id/ ),
            data => $record->data
        },
        {
            'remote_ip'         => '123.456.789.10',
            'n_results'         => undef,
            'board'             => 'twitter_xray',
            'criteria_id'       => 'nbAxl-U__swCHRLFT1OEx6THMz0',
            'user_id'           => 1,
            'data'              => {
                's3_log_id'     => 'doesnt matter',
                'error_type'    => 'LOGIN ERROR'
            }
        },
        "The search record is as expected"
    );

    ok( $search_called && ! $inc_anal_called && $calendar_called, "All applicable recording mechanisms have been informed" );
};

my $watchdog = $stream2->watchdogs->create(
    {
        user_id     => $user_id ,
        name        => 'molecular monkey',
        criteria_id => $criteria->id(),
        base_url    => 'http://www.example.com/'
    }
);
my $subscription = $watchdog->add_to_watchdog_subscriptions(
    {
        subscription => $memory_user->subscriptions()->find({ board => 'twitter_xray' })
    }
);

subtest 'Normal AUTO watchdog reports success appropriately' => sub {

    my ( $search_called, $inc_anal_called, $calendar_called );

    # return results as expected
    $feed_mock->mock( search => sub {
        $search_called = 1;
        my $self = shift;
        $self->total_results( scalar( @results ) );
        return @results;
    });
   
    $stream2_mock->mock( increment_analytics => sub {
        fail ( 'I should not be called for an auto watchdog' );
    });
    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $content ) = @_;
        $calendar_called = 1;

        ok( defined( delete $content->{duration} ) );

        is_deeply(
            $content,
            {
                'remote_ip' => '123.456.789.10',
                'destination' => 'twitter_xray',
                'user_provider' => 'adcourier',
                'user_provider_id' => '31415',
                'n_results' => 4,
                's3_log_id' => 'doesnt matter',
                'stream2_base_url' => 'http://127.0.0.1',
                'criteria_id' => 'nbAxl-U__swCHRLFT1OEx6THMz0',
                'search_record_id' => '4',
                'user_stream2_id' => 1,
                'company' => 'acme',
                'watchdog' => 1
            },
            "We send a success report to the calendar"
        );
    });

    my $results = Stream2::Action::WatchdogLoad->new({
        watchdog_id             => $watchdog->id,
        watchdog_subscription   => $subscription,
        criteria_id             => $criteria->id(),
        template_name           => 'twitter_xray',
        stream2_base_url        => 'http://127.0.0.1',
        stream2                 => $stream2,
        user                    => { user_id => $user_id },
        ripple_settings         => {},
        log_chunk_capture       => 0,
        options                 => { page => 1, auto_run => 1 },
        jobid                   => 'doesnt matter',
        remote_ip               => '123.456.789.10'
    })->instance_perform();

    my $record = $stream2->factory('SearchRecord')->search({},{ order_by => { -desc => 'id' } })->first();
    is_deeply(
        {
            ( map { $_ => $record->get_column($_) } qw/remote_ip n_results board criteria_id user_id/ ),
            data => $record->data
        },
        {
            'remote_ip'         => '123.456.789.10',
            'n_results'         => 4,
            'board'             => 'twitter_xray',
            'criteria_id'       => 'nbAxl-U__swCHRLFT1OEx6THMz0',
            'user_id'           => 1,
            'data'              => {
                's3_log_id' => 'doesnt matter',
                'watchdog'  => 1
            }
        },
        "The search record is as expected"
    );

    $subscription->discard_changes();
    is ( $subscription->last_run_error(), undef, "Last run of watchdog was not an error" );

    ok( $search_called && ! $inc_anal_called && $calendar_called, "All recording mechanisms have been informed" );
};

subtest 'Unmanaged AUTO fail watchdog reports failure appropriately' => sub {

    my ( $search_called, $inc_anal_called, $calendar_called );

    # return results as expected
    $feed_mock->mock( search => sub {
        $search_called = 1;
        die "I die unmanagedly";
    });
   
    $stream2_mock->mock( increment_analytics => sub {
        my ( $self, %content ) = @_;
        $inc_anal_called = 1;
    });

    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $content ) = @_;
        $calendar_called = 1;

        ok( defined( delete $content->{duration} ) );

        is_deeply(
            $content,
            {
                'remote_ip' => '123.456.789.10',
                'destination' => 'twitter_xray',
                'user_provider' => 'adcourier',
                'user_provider_id' => '31415',
                's3_log_id' => 'doesnt matter',
                'stream2_base_url' => 'http://127.0.0.1',
                'criteria_id' => 'nbAxl-U__swCHRLFT1OEx6THMz0',
                'search_record_id' => '5',
                'user_stream2_id' => 1,
                'company' => 'acme',
                'watchdog' => 1,
                'error_type' => 'UNKNOWN INTERNAL'
            },
            "We send a success report to the calendar"
        );
    });

    my $results = Stream2::Action::WatchdogLoad->new({
        watchdog_id             => $watchdog->id,
        watchdog_subscription   => $subscription,
        criteria_id             => $criteria->id(),
        template_name           => 'twitter_xray',
        stream2_base_url        => 'http://127.0.0.1',
        stream2                 => $stream2,
        user                    => { user_id => $user_id },
        ripple_settings         => {},
        log_chunk_capture       => 0,
        options                 => { page => 1, auto_run => 1 },
        jobid                   => 'doesnt matter',
        remote_ip               => '123.456.789.10'
    })->instance_perform();

    my $record = $stream2->factory('SearchRecord')->search({},{ order_by => { -desc => 'id' } })->first();
    is_deeply(
        {
            ( map { $_ => $record->get_column($_) } qw/remote_ip n_results board criteria_id user_id/ ),
            data => $record->data
        },
        {
            'remote_ip'         => '123.456.789.10',
            'n_results'         => undef,
            'board'             => 'twitter_xray',
            'criteria_id'       => 'nbAxl-U__swCHRLFT1OEx6THMz0',
            'user_id'           => 1,
            'data'              => {
                'error_type'    => 'UNKNOWN INTERNAL',
                's3_log_id' => 'doesnt matter',
                'watchdog'  => 1
            }
        },
        "The search record is as expected"
    );

    $subscription->discard_changes();
    like ( $subscription->last_run_error(), qr/I die unmanagedly/, "Last run of watchdog was an unmanaged error" );

    ok( $search_called && ! $inc_anal_called && $calendar_called, "All recording mechanisms have been informed" );
};

subtest 'Managed AUTO fail watchdog reports failure appropriately' => sub {

    my ( $search_called, $inc_anal_called, $calendar_called );

    # return results as expected
    $feed_mock->mock( search => sub {
        $search_called = 1;
        shift->throw_login_error( "I die managedly" );
    });
   
    $stream2_mock->mock( increment_analytics => sub {
        my ( $self, %content ) = @_;
        $inc_anal_called = 1;
    });

    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $content ) = @_;
        $calendar_called = 1;

        ok( defined( delete $content->{duration} ) );

        is_deeply(
            $content,
            {
                'remote_ip' => '123.456.789.10',
                'destination' => 'twitter_xray',
                'user_provider' => 'adcourier',
                'user_provider_id' => '31415',
                's3_log_id' => 'doesnt matter',
                'stream2_base_url' => 'http://127.0.0.1',
                'criteria_id' => 'nbAxl-U__swCHRLFT1OEx6THMz0',
                'search_record_id' => '6',
                'user_stream2_id' => 1,
                'company' => 'acme',
                'watchdog' => 1,
                'error_type' => 'LOGIN ERROR'
            },
            "We send a success report to the calendar"
        );
    });

    my $results = Stream2::Action::WatchdogLoad->new({
        watchdog_id             => $watchdog->id,
        watchdog_subscription   => $subscription,
        criteria_id             => $criteria->id(),
        template_name           => 'twitter_xray',
        stream2_base_url        => 'http://127.0.0.1',
        stream2                 => $stream2,
        user                    => { user_id => $user_id },
        ripple_settings         => {},
        log_chunk_capture       => 0,
        options                 => { page => 1, auto_run => 1 },
        jobid                   => 'doesnt matter',
        remote_ip               => '123.456.789.10'
    })->instance_perform();

    my $record = $stream2->factory('SearchRecord')->search({},{ order_by => { -desc => 'id' } })->first();
    is_deeply(
        {
            ( map { $_ => $record->get_column($_) } qw/remote_ip n_results board criteria_id user_id/ ),
            data => $record->data
        },
        {
            'remote_ip'         => '123.456.789.10',
            'n_results'         => undef,
            'board'             => 'twitter_xray',
            'criteria_id'       => 'nbAxl-U__swCHRLFT1OEx6THMz0',
            'user_id'           => 1,
            'data'              => {
                'error_type'    => 'LOGIN ERROR',
                's3_log_id' => 'doesnt matter',
                'watchdog'  => 1
            }
        },
        "The search record is as expected"
    );

    $subscription->discard_changes();
    like ( $subscription->last_run_error(), qr/I die managedly/, "Last run of watchdog was a managed error" );

    ok( $search_called && ! $inc_anal_called && $calendar_called, "All recording mechanisms have been informed" );
};

# set the error to be something what should not be changed
$subscription->last_run_error( 'leave me alone' );
$subscription->update();

subtest 'Normal INTERACTIVE watchdog reports success appropriately' => sub {

    my ( $search_called, $inc_anal_called, $calendar_called );

    # return results as expected
    $feed_mock->mock( search => sub {
        $search_called = 1;
        my $self = shift;
        $self->total_results( scalar( @results ) );
        return @results;
    });
   
    $stream2_mock->mock( increment_analytics => sub {
        my ( $self, %content ) = @_;
        $inc_anal_called = 1;
        is_deeply(
            \%content,
            {
                'successful_threads' => 1,
                'board' => 'twitter_xray',
                'user_id' => '31415',
                'threads' => 1,
                'results' => 4
            },
            "We send a success report to the mongdb"
        );
    });
    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $content ) = @_;
        $calendar_called = 1;

        ok( defined( delete $content->{duration} ) );

        is_deeply(
            $content,
            {
                'remote_ip' => '123.456.789.10',
                'destination' => 'twitter_xray',
                'user_provider' => 'adcourier',
                'user_provider_id' => '31415',
                'n_results' => 4,
                's3_log_id' => 'doesnt matter',
                'stream2_base_url' => 'http://127.0.0.1',
                'criteria_id' => 'nbAxl-U__swCHRLFT1OEx6THMz0',
                'search_record_id' => '7',
                'user_stream2_id' => 1,
                'company' => 'acme'
            },
            "We send a success report to the calendar"
        );
    });

    my $results = Stream2::Action::WatchdogLoad->new({
        criteria_id         => $criteria->id(),
        template_name       => 'twitter_xray',
        options             => { page => 1 },
        user                => { user_id => $user_id },
        stream2_base_url    => 'http://127.0.0.1',
        ripple_settings     => {},
        remote_ip           => '123.456.789.10',
        watchdog_id         => $watchdog->id,
        stream2             => $stream2,
        log_chunk_capture   => 0,
        jobid               => 'doesnt matter',
    })->instance_perform();

    my $record = $stream2->factory('SearchRecord')->search({},{ order_by => { -desc => 'id' } })->first();
    is_deeply(
        {
            ( map { $_ => $record->get_column($_) } qw/remote_ip n_results board criteria_id user_id/ ),
            data => $record->data
        },
        {
            'remote_ip'         => '123.456.789.10',
            'n_results'         => 4,
            'board'             => 'twitter_xray',
            'criteria_id'       => 'nbAxl-U__swCHRLFT1OEx6THMz0',
            'user_id'           => 1,
            'data'              => {
                's3_log_id' => 'doesnt matter'
            }
        },
        "The search record is as expected"
    );

    $subscription->discard_changes();
    is ( $subscription->last_run_error(), 'leave me alone', "Last run of watchdog was not changed" );

    ok( $search_called && $inc_anal_called && $calendar_called, "All recording mechanisms have been informed" );
};

subtest 'Unmanaged INTERACTIVE fail watchdog reports failure appropriately' => sub {

    my ( $search_called, $inc_anal_called, $calendar_called );

    # return results as expected
    $feed_mock->mock( search => sub {
        $search_called = 1;
        die "I die unmanagedly";
    });
   
    $stream2_mock->mock( increment_analytics => sub {
        my ( $self, %content ) = @_;
        $inc_anal_called = 1;
    });

    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $content ) = @_;
        $calendar_called = 1;

        ok( defined( delete $content->{duration} ) );

        is_deeply(
            $content,
            {
                'remote_ip' => '123.456.789.10',
                'destination' => 'twitter_xray',
                'user_provider' => 'adcourier',
                'user_provider_id' => '31415',
                's3_log_id' => 'doesnt matter',
                'stream2_base_url' => 'http://127.0.0.1',
                'criteria_id' => 'nbAxl-U__swCHRLFT1OEx6THMz0',
                'search_record_id' => '8',
                'user_stream2_id' => 1,
                'company' => 'acme',
                'error_type' => 'UNKNOWN INTERNAL'
            },
            "We send a success report to the calendar"
        );
    });

    my $results = Stream2::Action::WatchdogLoad->new({
        criteria_id         => $criteria->id(),
        template_name       => 'twitter_xray',
        options             => { page => 1 },
        user                => { user_id => $user_id },
        stream2_base_url    => 'http://127.0.0.1',
        ripple_settings     => {},
        remote_ip           => '123.456.789.10',
        watchdog_id         => $watchdog->id,
        stream2             => $stream2,
        log_chunk_capture   => 0,
        jobid               => 'doesnt matter',
    })->instance_perform();

    my $record = $stream2->factory('SearchRecord')->search({},{ order_by => { -desc => 'id' } })->first();
    is_deeply(
        {
            ( map { $_ => $record->get_column($_) } qw/remote_ip n_results board criteria_id user_id/ ),
            data => $record->data
        },
        {
            'remote_ip'         => '123.456.789.10',
            'n_results'         => undef,
            'board'             => 'twitter_xray',
            'criteria_id'       => 'nbAxl-U__swCHRLFT1OEx6THMz0',
            'user_id'           => 1,
            'data'              => {
                'error_type'    => 'UNKNOWN INTERNAL',
                's3_log_id' => 'doesnt matter',
            }
        },
        "The search record is as expected"
    );

    $subscription->discard_changes();
    is ( $subscription->last_run_error(), 'leave me alone', "Last run of watchdog was unchanged" );

    ok( $search_called && ! $inc_anal_called && $calendar_called, "All recording mechanisms have been informed" );
};

subtest 'Managed AUTO fail watchdog reports failure appropriately' => sub {

    my ( $search_called, $inc_anal_called, $calendar_called );

    # return results as expected
    $feed_mock->mock( search => sub {
        $search_called = 1;
        shift->throw_login_error( "I die managedly" );
    });
   
    $stream2_mock->mock( increment_analytics => sub {
        my ( $self, %content ) = @_;
        $inc_anal_called = 1;
    });

    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $content ) = @_;
        $calendar_called = 1;

        ok( defined( delete $content->{duration} ) );

        is_deeply(
            $content,
            {
                'remote_ip' => '123.456.789.10',
                'destination' => 'twitter_xray',
                'user_provider' => 'adcourier',
                'user_provider_id' => '31415',
                's3_log_id' => 'doesnt matter',
                'stream2_base_url' => 'http://127.0.0.1',
                'criteria_id' => 'nbAxl-U__swCHRLFT1OEx6THMz0',
                'search_record_id' => '9',
                'user_stream2_id' => 1,
                'company' => 'acme',
                'error_type' => 'LOGIN ERROR'
            },
            "We send a success report to the calendar"
        );
    });

    my $results = Stream2::Action::WatchdogLoad->new({
        criteria_id         => $criteria->id(),
        template_name       => 'twitter_xray',
        options             => { page => 1 },
        user                => { user_id => $user_id },
        stream2_base_url    => 'http://127.0.0.1',
        ripple_settings     => {},
        remote_ip           => '123.456.789.10',
        watchdog_id         => $watchdog->id,
        stream2             => $stream2,
        log_chunk_capture   => 0,
        jobid               => 'doesnt matter',
    })->instance_perform();

    my $record = $stream2->factory('SearchRecord')->search({},{ order_by => { -desc => 'id' } })->first();
    is_deeply(
        {
            ( map { $_ => $record->get_column($_) } qw/remote_ip n_results board criteria_id user_id/ ),
            data => $record->data
        },
        {
            'remote_ip'         => '123.456.789.10',
            'n_results'         => undef,
            'board'             => 'twitter_xray',
            'criteria_id'       => 'nbAxl-U__swCHRLFT1OEx6THMz0',
            'user_id'           => 1,
            'data'              => {
                'error_type'    => 'LOGIN ERROR',
                's3_log_id' => 'doesnt matter',
            }
        },
        "The search record is as expected"
    );

    $subscription->discard_changes();
    is ( $subscription->last_run_error(), 'leave me alone', "Last run of watchdog was a managed error" );

    ok( $search_called && ! $inc_anal_called && $calendar_called, "All recording mechanisms have been informed" );
};

done_testing();

{
    package MyRedis;
    sub rpush { 1; }
    sub expire { 1; }
    sub new { return bless {}; }
}

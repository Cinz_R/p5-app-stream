#! perl

use Test::Most;
use Data::Dumper;
use JSON qw/decode_json/;
use Test::Mojo::Stream2;
use Test::MockModule;

use JSON;
use HTTP::Response;
use Stream2::Results::Result;
use Email::Abstract;

use Stream2::Action::ShortlistCandidate;
# use Log::Any::Adapter qw/Stderr/;

use Log::Any qw/$log/;

my $defaults = {
    cv_content      => "CV Test\n\nname: John Smith",
    firstname       => "John",
    surname         => "Smith",
    email           => q{jsmith@test.com},
    advert_id       => 123,
    advert_label    => 'Cheese Sandwich',
    should_import   => 1,
    should_email    => 0,
    ats_shortlisted => 0,
    provider        => 'adcourier',
    last_login_provider => 'adcourier',
    candidate_destination => 'monsterxml',
    user_ref    => {
        consultant => 'andy',
        team       => 'test',
        office     => 'whatever',
        company    => 'andy'
    },
    ripple_settings => {},
    custom_fields   => [],
    shortlist_name  => 'adc_shortlist',
    doctype         => 'hr_xml',
};

my $tests = [
    {
        email       => "",
        email_test  => qr/^unknown/,
        candidate_destination => "monsterxml",
        shortlist_subject => "test subject",
        shortlist_subject_outcome => "test subject",
        shortlist_bcc => q{email@test.com},
        should_email    => 1,
        should_import   => 0
    },
    {
        candidate_destination => "monsterxml",
        advert_id   => 321,
        shortlist_bcc => q{test@bcc.test;bcc@test.com},
        should_email    => 1,
        should_import => 0
    },
    {
        candidate_destination => "talentsearch",
        should_import   => 0,
        advert_id       => 555,
    },
    { # ats action tests
        provider                => 'adcourier',
        last_login_provider     => 'adcourier_ats',
        ats_shortlisted         => 1,
        advert_id               => 'test_that_cheese_sandwich_id_is_correct',
        ripple_settings         => {
            new_candidate_url       => '/ATS_SHORTLIST_TEST',
            tagged_doc_type         => 'HR',
            custom_fields           => [
                { name => "field1", content => "abc" },
                { name => "field2", content => "123" }
            ],
        },
        shortlist_name          => 'cheese_sandwich_test',
        should_import => 1
    },
    {
        candidate_destination => 'monsterxml',
        shortlist_subject => 'Hey man do you wana be the next [jobtitle]/[jobref] at [board_nice_name]?',
        shortlist_subject_outcome => 'Hey man do you wana be the next JavaScript Ninja/JSNinja123 at Monster?',
        adcadvert_details => {
            JobTitle => 'JavaScript Ninja',
            JobReference   => 'JSNinja123',
        },
        shortlist_bcc => q{email@test.com},
        should_email => 1,
        should_import => 0
    }
];
$tests = [ map {
    { %$defaults, %$_ }
} @$tests ];


my $config_file = 't/app-stream2.conf';
ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
# Deploying schema
$stream2->stream_schema()->deploy();

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'demo',
    last_login_provider => 'demo',
    provider_id => 31415,
    group_identity => 'acme',
    settings => { responses => { bg_language_pack => 'nl' } }
});

ok( my $user_id = $user->save() , "Ok can save this user");

foreach my $test ( @$tests ){
    my $imported = 0;
    my $email_sent = 0;
    my $shortlisted = 0;
    my $increment_analytics = 0;
    my $ats_shortlisted = 0;

    # Note that we are always in the Demo login provider (see user construction).
    $user = $stream2->find_user( $user_id , {  %{ $test->{ripple_settings} } , login_provider => 'demo' } );

    is_deeply( $user->ripple_settings, { %{ $test->{ripple_settings} } , login_provider => 'demo' } );

    my $candidate =  Stream2::Results::Result->new();
    $candidate->candidate_id( '12345' );
    $candidate->destination( $test->{candidate_destination} );

    $user->subscriptions_ref( { talentsearch => {}, monsterxml => { nice_name => 'Monster', eza_number => 123 } } );


    #######################
    # LOAD CANDIDATE + CV #
    #######################
    my $cand_api_module = Test::MockModule->new('Stream2::Action::DownloadCV');
    $cand_api_module->mock('instance_perform' => sub {
                               my ( $self ) = @_;
                               ok( $self->instance_memory_cache()->{some_cache_key} , "Ok subaction download CV has got the cache set by the containing action");
        return HTTP::Response->new( 200, "OK", 
                                    [ "Content-Type" => "text/xml", "Content-Disposition" => q{inline; filename="file.txt"} ],
                                    $test->{cv_content} );
    });

    ############
    # PARSE CV #
    ############
    my $cvp_client_module = Test::MockModule->new('Bean::CVP::Client');
    $cvp_client_module->mock( 'parse_cv_contents' => sub {
        my ( $self, $content, $options ) = @_;
        is( $options->{tagger_locale}, 'du_nl', 'cv parse locale is passed to parser' );
        is( $content, $test->{cv_content}, 'cv content is passed to parser' );
        return {
            first_name => $test->{firstname}, last_name => $test->{surname}, email => $test->{email},
            $test->{doctype} => sprintf('<Resume><StructuredXMLResume><ContactInfo><PersonName><FormattedName>%s %s</FormattedName><InternetEmailAddress>%s</InternetEmailAddress></PersonName></ContactInfo></StructuredXMLResume></Resume>', $test->{firstname}, $test->{surname}, $test->{email}),
        };
    });

    ################
    # IMPORT TO TS #
    ################
    my $adcourier_provider_module = Test::MockModule->new('App::Stream2::Plugin::Auth::Provider::Adcourier');
    $adcourier_provider_module->mock( 'user_import_candidate' => sub {
        my ( $self, $user, $attr_ref, $cand ) = @_;

        is( $cand->destination, $test->{candidate_destination} );
        ok( "talentsearch" ne $test->{candidate_destination}, "dont import candidates from TS to TS" );

        is( $cand->name, join( " ", $test->{firstname}, $test->{surname} ), "name matches" );
        if ( my $e_test = $test->{email_test} ){
            ok( $cand->email =~ $e_test, "email matches" );
        }
        else{
            is( $cand->email, $test->{email}, "email matches" );
        }

        $user->stream2->factory('CandidateInternalMapping')->find_or_create({
            import_id => 998877,
            group_identity => $user->group_identity,
            candidate_id => $cand->id,
            backend => 'Artirix'
        });

        $imported = 1;

        1;
    });

    ####################
    # SHORTLIST IN ADC #
    ####################
    my $demo_provider_module = Test::MockModule->new('App::Stream2::Plugin::Auth::Provider::Demo');
    $demo_provider_module->mock( 'find_advert' => sub {
        return {
            AdvertID => $test->{advert_id},
            %{ $test->{adcadvert_details} // {} },
        };
    });

    # This method will actually use the real provider (last_login_provider) of the test.
    $demo_provider_module->mock( 'advert_shortlist_candidate' => sub {
        my $self = shift;
        $stream2->build_provider( $test->{last_login_provider} , $test->{ripple_settings} )->advert_shortlist_candidate( @_ );
    });

    my $adcapi_client_module = Test::MockModule->new('Bean::AdcApi::Client');
    $adcapi_client_module->mock( 'perform_request' => sub {
        my ( $self, $req ) = @_;

        ok( $req->uri =~ /adverts\/$test->{advert_id}\/shortlist\z/i, "the advert id is sent in the shortlist request" );

        my $param_obj = Mojo::Parameters->new();
        my $param_ref = $param_obj->parse( $req->content )->to_hash;

        is( $param_ref->{source_channel}, $test->{candidate_destination}, "shortlist source matches" );
        is( MIME::Base64::decode_base64($param_ref->{cv_content}), $test->{cv_content}, "shortlist cv matches" );

        if ( my $e_test = $test->{email_test} ){
            ok( $param_ref->{candidate_email} =~ $e_test, "shortlist email match" );
        }
        else {
            is( $param_ref->{candidate_email}, $test->{email}, "shortlist email match" );
        }

        is( $param_ref->{candidate_name}, join( " ", $test->{firstname}, $test->{surname} ), "shortlist name matches" );

        my $response = HTTP::Response->new( 200, "OK", [ "Content-Type" => "text/xml" ], JSON::encode_json({success=>1}) );

        $shortlisted = 1;
        return $response;
    });

    ####################
    # SHORTLIST IN ATS #
    ####################
    my $searchuser_module = Test::MockModule->new( 'Stream2::O::SearchUser' );
    $searchuser_module->mock( 'get_cached_data' => sub {
                                  my ( $self, $attr ) = @_;
                                  fail("DO NOT USE THAT");
                                  if ( $attr eq 'custom_fields' ){
                                      return $test->{custom_fields} || []
                                  }
                              });

    ##############
    # SEND EMAIL #
    ##############
    my $stream2_module = Test::MockModule->new('Stream2');
    $stream2_module->mock( 'send_email' => sub {
        my ( $self, $email ) = @_;

        # unwrap the Mime::Entity from Stream2::O::Email.
        if ( Scalar::Util::blessed($email)
            && $email->isa('Stream2::O::Email') )
        {
            $email = $email->entity;
        }

        my $sub = Encode::encode('MIME-Q', $test->{shortlist_subject_outcome} );
        if ( $email->head->get('Subject') ) {
            $sub .= "\n"; # newline due to Email::Header
            $sub =~ s/\r//; # can occur with long subjects - strange that this is in one version, both subjects encoded the same way.
        }

        is( $email->head->get('Subject'), $sub, "final email has correct subject" );
        $email = Email::Abstract->new($email);
        is( $email->get_header('To'), $test->{shortlist_bcc}, "final email has matching bcc" );

        $email_sent = 1;
    });

    # this handles posting the candidate to the ats webhook
    $stream2_module->mock( 'user_agent' => sub {
        my ( $self ) = @_;
        return test_ua->new({
            cb => sub {
                my $request = shift;
                my $url = $request->uri();

                is( $url, $test->{ripple_settings}->{new_candidate_url} );

                my $content = $request->content();

                # test for custom fields
                my @custom_fields = ();
                my ( %raw_fields ) = ( $content =~ m/<CustomField name="([^"]+)">([^<]+)<\/CustomField>/g );
                while( my ( $name, $content ) = each( %raw_fields ) ) {
                    push( @custom_fields, { name => $name, content => $content } );
                }
                is_deeply(
                    [ sort { $a->{name} cmp $b->{name} } @custom_fields ],
                    [ sort { $a->{name} cmp $b->{name} } @{$test->{ripple_settings}->{custom_fields}} ],
                    "custom fields are present and correct"
                );

                my ( @doc_attrs ) = ( $content =~ m/<Doc\s([^>]+)/g );
                my ( @raw_docs ) = ( $content =~ m/<Doc\s[^>]+>([^<]+)/g );
                my $doc = MIME::Base64::decode_base64( $raw_docs[-1] );
                my $cv = MIME::Base64::decode_base64( $raw_docs[0] );

                is ( $cv, $test->{cv_content}, "cv is sent in ripple xml" );

                my %attr = map {
                    my $a = $_;
                    $a =~ s/"//g;
                    split '=', $a;
                } split( '" ', pop( @doc_attrs ) );

                is( $attr{DocumentType}, $test->{ripple_settings}->{tagged_doc_type}, "correct doc type supplied in candidate XML" );
                ok( defined( $attr{type} ), "type attr of Doc element is required by ATS team, was part of original v3 spec" );

                my ( $xml_name ) = $content =~ m/<Name>([^<]+)<\/Name>/;
                my ( $xml_email ) = $content =~ m/<Email>([^<]+)<\/Email>/;
                my ( $doc_name ) = $doc =~ m/<FormattedName>([^<]+)<\/FormattedName>/;
                my ( $doc_email ) = $doc =~ m/<InternetEmailAddress>([^<]+)<\/InternetEmailAddress>/;

                is ( $doc_name, sprintf("%s %s", $test->{firstname}, $test->{surname}), "HR XML conv contains name" );
                is ( $doc_email, $test->{email}, "HR XML conv contains email" );

                is ( $xml_name, sprintf("%s %s", $test->{firstname}, $test->{surname}), "Name has been extracted from tag doc and put into can xml" );
                is ( $xml_email, $test->{email}, "Email has been extracted from tag doc and put into can xml" );

                my ( $shortlist_name ) = $content =~ m/<CustomList name="([^"]+)">/;
                my ( $advert_id ) = $content =~ m/<Option value="([^"]+)"\/>/;

                is ( $shortlist_name, $test->{shortlist_name}, "we are sending the correct name of the list to which the candidate is being attributed" );
                is ( $advert_id, $test->{advert_id}, "we are sending the correct id of the list to which the candidate is being attributed" );

                $ats_shortlisted = 1;

                my $resp = HTTP::Response->new( 200, "OK" );
                return $resp;
            }
        });
    });

    $demo_provider_module->mock( 'get_shortlist_email_settings' => sub {
        my $return = {};
        if ( my $sub = $test->{shortlist_subject} ){
            $return->{subject} = $sub;
        }
        if ( my $bcc = $test->{shortlist_bcc} ){
            $return->{bcc} = $bcc;
        }
        return keys(%$return) ? $return : 0;
    });
    my $userapi_client_module = Test::MockModule->new('Bean::Juice::APIClient::User');
    $userapi_client_module->mock( get_user => sub {
        return $test->{user_ref};
    });

    $stream2_module->mock( 'increment_analytics' => sub {
        $increment_analytics = 1;
        return 1;
    });

    # Build a shortlist action and perform it.
    my $shortlist_action = Stream2::Action::ShortlistCandidate->new({
        stream2 => $stream2,
        candidate_object => $candidate,
        user_object => $user,
        ripple_settings => $user->ripple_settings(),
        advert_id => $test->{advert_id},
        advert_label => $test->{advert_label},
        list_name => $test->{shortlist_name},
        jobid => 'Blabla'
    });

    # Simulate using the cache
    $shortlist_action->instance_memory_cache()->{some_cache_key} = 1;

    if( $test->{ripple_settings}->{new_candidate_url} ){
        my $mocked_log = Test::MockModule->new( ref( $log ) );
        my $captured = 0;

        my $trace_mock = sub{
            my ($self, $message , @rest ) = @_;

            if( $message =~ /REQUEST POST/ ){
                $captured = 1;
                like( $message, qr/removed to reduce log size/ );
            }
            return $mocked_log->original('trace')->( $self, $message , @rest );
        };

        $mocked_log->mock('is_trace' => sub{ 1 ; } );
        $mocked_log->mock('tracef' => $trace_mock );
        $mocked_log->mock('trace' => $trace_mock );

        $shortlist_action->instance_perform();

        ok( $captured );
    }else{
        # Just do the action. Dont care about any logging testing.
        $shortlist_action->instance_perform();
    }

    {
        my $last_action = $stream2->factory('CandidateActionLog')->search(undef, { order_by => { -desc => 'id' } })->first();
        my %data = %{ $last_action->data() };

        if( $last_action->is_adcourier_shortlist() ){
            is( $last_action->adc_advert_id() , $test->{advert_id} );
        }else{
            # Not an adcourier shortlist no adcourier advert ID
            is( $last_action->adc_advert_id() , undef );
        }

        %data = map{ $_ => $data{$_} } grep { $_ =~ /^candidate_/ } keys %data;
        is_deeply( \%data,
                   {
                     $shortlist_action->candidate_object()->action_hash(),
                 });
    }


    # $t->post_ok( '/results/1/talentsearch/candidate/1/shortlist' => { Accept => 'text/json' },
    #     form => { q => JSON::encode_json({ advert_id => $test->{advert_id}, advert_label =>  }) }
    # )->status_is(200);
    ok( $increment_analytics, "we should always increment analytics on a shortlist" );
    ok( $shortlisted, "always shortlist" );

    is( $email_sent, $test->{should_email}, "sometimes email" );
    is( $imported, $test->{should_import}, "sometimes imported" );
    is( $ats_shortlisted, $test->{ats_shortlisted}, "sometimes shortlist via ats" );
}

{
    package test_ua;
    use base 'LWP::UserAgent';
    sub new { bless $_[1], $_[0] }
    # The shortlist action now uses request.
    sub request { my $cb = shift->{cb}; &$cb(@_); }
}

done_testing();

#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Stream2;

use Log::Any::Adapter;
#Log::Any::Adapter->set('Stderr');

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();

# Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");

{
    package My::Action;
    use Moose;
    has 'stream2' => ( is => 'ro' );
    with qw/Stream2::Role::Action::HasUser/;
    sub build_context{ {}; }
}

ok( my $action = My::Action->new({ stream2 => $stream2, user => { user_id => $user_id } , ripple_settings => {}  }) );
ok( $action->user_object() );

done_testing();

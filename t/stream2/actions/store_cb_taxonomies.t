use Test::Most;
use Test::MockModule;
use Test::Mock::LWP::Dispatch;
use HTTP::Response;

use Stream2;
use JSON;

use_ok('Stream2::Action::StoreCBTaxonomies');

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

my $stream2 = Stream2->new({ config_file => $config_file });
$stream2->stream_schema();
# Deploying schema
$stream2->stream_schema()->deploy();

my $data = {
    data => {
        en => [{ id => '12345', title => 'test title', normalized_name => 'normalized test title' }],
        de => [{ id => '58840', title => 'another test title' }]
    }
};

my $data_response = HTTP::Response->new(200, 'OK', [], JSON::encode_json($data));
$mock_ua->map('https://api.careerbuilder.com/core/taxonomy/skillsv4', $data_response);
$mock_ua->map('https://api.careerbuilder.com/core/taxonomy/carotenev3', $data_response);
$mock_ua->map('https://api.careerbuilder.com/core/taxonomy/onet17', $data_response);

my $mock_auth = Test::MockModule->new('Bean::OAuth::CareerBuilder');
$mock_auth->mock(get_token => sub { return ('mockytoken', 60); });

ok my $action = Stream2::Action::StoreCBTaxonomies->new({
    stream2 => $stream2,
}), 'StoreCBTaxonomies action was created';

ok my $tax_results = $action->instance_perform(), 'taxonomy action stored the mappings';
ok $tax_results->{skillsv4}, 'stored skills fine';
ok $tax_results->{carotenev3}, 'stored carotenes fine';
ok $tax_results->{onet17}, 'stored onet codes fine';

# check that we stored the results
ok my @rs = $stream2->factory('Taxonomy')->all(), 'got some results from taxonomy table';
is scalar @rs, 6, 'stored all the results in taxonomy table';

my $one_res = $rs[0];
is $one_res->field_type, 'skillsv4', 'stored the right type';
is $one_res->lang, 'en', 'stored the right lang';
is $one_res->taxonomy_id, '12345', 'stored id in key';
is $one_res->label, 'normalized test title', 'stored title in value';

done_testing();

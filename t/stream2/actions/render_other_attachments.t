#! perl -w

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;


use Test::Most;
use Test::MockModule;

use Test::Stream2;

use HTTP::Response;
use MIME::Base64;

use Stream2::Action::RenderOtherAttachments;
use Stream2::Action::DownloadOtherAttachment;

use Log::Any::Adapter;

# toggle errors to go to STDERR
# Log::Any::Adapter->set('Stderr');

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    die 'there was no config, this needs to be fixed';
}

ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ),
    "Ok can build stream2" );
ok( $stream2->deploy_test_db, "Ok can run deploy" );

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser'),
    "Ok can get resultset" );

my $users_factory = $stream2->factory('SearchUser');

my @user_ids;

my $user = $users_factory->new_result(
    {
        contact_name        => 'Scrooge McDuck',
        contact_email       => 'scrooge_mcduckmoo@DTinc.com',
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 0,
        group_identity      => 'acme',
        contact_details     => { username => 'scrooge@theteam.theoffice.acme' },
        subscriptions_ref   => {
            dummy => {
                nice_name   => "Dummy",
                auth_tokens => {
                    cvlibrary_username => 'dummy',
                    cvlibrary_password => 'pacifer'
                },
                type               => "internal",
                allows_acc_sharing => 0,
            }
        },
        ripple_settings => {},
    }
);

my $user_id = $user->save();

# Set up my Candidate
my $candidate = Stream2::Results::Result->new(
    {
        name             => 'Lanchpad McQuack',
        email            => 'lanchpadmcquak@Quackwerks.com',
        candidate_id     => '5318008',
        destination      => 'dummy',
        search_record_id => 1234,
    }
);

# we dont have a CV for this user, so use the build_forward_profile (which will need to be mocked)
$candidate->has_cv(0);
$candidate->has_profile(1);

# this tests that a user recieving a CV from cvlibrary user has a current subscription, an
{
    my $other_attachments = Stream2::Action::RenderOtherAttachments->new({
        jobid            => 'does not matter',
        stream2          => $stream2,
        user_object      => $user,
        ripple_settings  => $user->ripple_settings(),
        candidate_object => $candidate,
    });

    # run the instance
    my $result  = $other_attachments->instance_perform();
    is( @{ $result->{other_attachments} }, 3, 'there are 3 attachments' );

    # Time to download stuff.
    my $b64_response = Stream2::Action::DownloadOtherAttachment->new({
        jobid            => 'does not matter',
        stream2          => $stream2,
        user_object      => $user,
        ripple_settings  => $user->ripple_settings(),
        candidate_object => $candidate,
        attachment_key   => $result->{other_attachments}->[0]->{key},
    })->instance_perform();
    ok( $b64_response, "Ok got response");

    my $response = HTTP::Response->parse(
        MIME::Base64::decode_base64( $b64_response )
    );
    is( $response->content_type() , 'text/plain' );
    is( $response->content() , 'BLABLABLA' );
    is( $response->header('Content-Disposition') , 'attachment; filename="0_someubercoolfile\.txt";' );
}

LWP::UserAgent::Mockable->finished();
done_testing();

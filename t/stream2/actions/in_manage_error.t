#! perl -w

use Test::Most;
use Test::MockModule;
use Test::Stream2;
use Log::Any::Adapter;
#Log::Any::Adapter->set('Stderr');

use Qurious::Job;

my $s2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$s2->deploy_test_db;

subtest 'un-managed error response' => sub {
    {
        package My::Action;
        use Moose;
        has 'jobid' => ( is => 'ro', isa => 'Str' );
        has stream2 => ( is => 'ro', lazy_build => 1);
        with qw/
                Stream2::Role::Action::FastJob
                Stream2::Role::Action::InLogChunk
                Stream2::Role::Action::InManageError
            /;
        sub build_context{ {}; }
        sub _build_stream2 {
            return $s2;
        }
        sub instance_perform{
            die 'wow';
        }
    }

    my $job = Qurious::Job->new({ class => 'Whatever' });

    my $mock_job = Test::MockModule->new( ref( $job ));
    ok( my $action = My::Action->new({ jobid => 'blabla' }) );
    is_deeply( $action->instance_perform( $job ) , { error => { message => "An un-managed error has occured. Please quote 'Job blabla' to support\n", log_id => 'blabla' } });
};

subtest 'managed error response' => sub {
     {
        package My::Action2;
        use Moose;
        use Stream::EngineException;
        has 'jobid' => ( is => 'ro', isa => 'Str' );
        has stream2 => ( is => 'ro', lazy_build => 1);
        with qw/
                Stream2::Role::Action::FastJob
                Stream2::Role::Action::InLogChunk
                Stream2::Role::Action::InManageError
            /;
        sub build_context{ {}; }
        sub _build_stream2 {
            return $s2;
        }
        sub instance_perform{
            die Stream::EngineException::NoCV->new({ message => 'wow' });
        }
    }

    my $job = Qurious::Job->new({ class => 'Whatever' });

    my $mock_job = Test::MockModule->new( ref( $job ));
    ok( my $action = My::Action2->new({ jobid => 'blabla' }) );
    is_deeply( $action->instance_perform( $job ) , {
        error => {
            message => "No CV: wow\n",
            log_id => 'blabla',
            properties => {
                type => 'nocv'
            }
        }
    });
};

done_testing();

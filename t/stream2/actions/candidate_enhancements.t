use Test::Most;
use Test::MockModule;

use Stream2;
use Stream2::Results::Result;
use Stream2::Action::CandidateEnhancements;

use JSON;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

### Mocks ###

my $candidate_ref = {
    email       => 'wishwash@hungrybear.com',
    destination => 'dummy',
};

my $candidate_obj = Stream2::Results::Result->new($candidate_ref);

my $store_mock = Test::MockModule->new('Stream2::Results::Store');
$store_mock->mock(r_lindex => sub {
    return JSON::to_json($candidate_ref, {utf8 => 1});
});
$store_mock->mock(r_lset => sub {
    return 'OK'; # don't test to redis
});
$store_mock->mock(find => sub {
    return $candidate_obj;
});

# Test that the candidate result was updated with new fields
$store_mock->mock(update => sub {
    my ($self, $position, $fields) = @_;
    my $orig_result = $store_mock->original('update')->($self, $position, $fields);
    is( $orig_result->attr('result_enhanced') , 1, 'set an attribute for what was enhanced' );
    is( $orig_result->attr('location'), 'North Carolina', 'data was added to candidate from feeds callback' );
    is( $orig_result->attr('favourite_team'), 'Panthers', 'data was added to candidate from feeds callback' );
    return $orig_result;
});

### /Mocks ##

my $stream2 = Stream2->new({ config_file => $config_file });
$stream2->stream_schema();
# Deploying schema
$stream2->stream_schema()->deploy();

my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
    contact_email => 'hungrybear@porridge.co.uk',
    contact_name => 'Ali',
    subscriptions_ref => {
        dummy => {
            nice_name => 'Dummy',
            auth_tokens => {}
        }
    }
});

$memory_user->save();

ok my $action = Stream2::Action::CandidateEnhancements->new(
    stream2         => $stream2,
    results_id      => 1,
    candidate_idx   => $candidate_obj->id(),
    user            => $memory_user->to_hash(),
    ripple_settings => $memory_user->ripple_settings(),
    jobid           => 3409423,
), 'have built the action';

ok my $data = $action->instance_perform(testjob->new), 'performed job and received the enhancements data';

is_deeply $data, {
    enhancements => {
        result_enhanced => 1,
        location => 'North Carolina',
        favourite_team => 'Panthers',
    }
}, 'returned enhancement details ref';

{
    package testjob;
    sub new {
        return bless {}, $_[0];
    }
    sub parameters {
        return {
            enhancement_section => 'result',
            results_id          => 5,
        };
    }
    1;
}

done_testing;

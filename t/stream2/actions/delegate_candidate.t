#! perl -w

use Test::Most;
use Stream2::Action::DelegateCandidate;
use Test::MockModule;
use Stream2::Results::Result;
use HTTP::Response;

#use Log::Any::Adapter ('Stderr');

use Test::Stream2;

ok( my $stream2 = Test::Stream2->new( { config_file => './t/app-stream2.conf' } ), "Ok can build stream2" );

$stream2->deploy_test_db;

my $current_user = $stream2->factory('SearchUser')->new_result(
    {
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 123,
        group_identity      => 'acme',
        contact_details     => { username => 'clark.kent@metropolis.farm.dailyplanet' },
        contact_name        => 'Clark Kent',
        contact_email       => 'c.kent@dailyplanet.com',
    }
);
$current_user->save();

my $delegate_user = $stream2->factory('SearchUser')->new_result(
    {
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 456,
        group_identity      => 'acme',
        contact_details     => { username => 'louis.lane@metropolis.city.dailyplanet' },
        contact_name        => 'Louis Lane',
        contact_email       => 'l.lane@dailyplanet.com',
    }
);
$delegate_user->save();

my $candidate = Stream2::Results::Result->new({
    candidate_id    => 100,
    destination     => 'adcresponses',
});

my $action = Stream2::Action::DelegateCandidate->new(
    jobid               => 'kryptonite',
    stream2             => $stream2,
    delegate_to_userid  => $delegate_user->id,
    candidate_object    => $candidate,
    user_object         => $current_user,
    ripple_settings     => $current_user->ripple_settings,
);

my $advert_client_mock = Test::MockModule->new('Bean::AdvertService::Client');
my $bbcss_mock = Test::MockModule->new('Bean::CareerBuilder::CandidateSearch');
my $template_mock = Test::MockModule->new('Stream::Templates::adcresponses');

$advert_client_mock->mock(search_adverts => sub {
    return ();
});

like($action->instance_perform()->{error}->{message}, qr/does not have a requisition advert/, 'dies when no requisition ad for user');
ok(my $failure_action_log = $stream2->factory('CandidateActionLog')->search({ action => 'delegate_fail' })->first, 'delegation failure was stored in action logs');
is($failure_action_log->data->{delegate_to_userid}, $delegate_user->id, 'the delegated users search user_id is stored in failure log');

$advert_client_mock->mock(search_adverts => sub {
    return ({ company => 'acme', is_requisition_ad => 1, advert_id => 9000 });
});

$template_mock->mock(bbcss_customer => sub {
    return { careerbuilder_customer_key => 'dailyplanet_e48ez' };
});

$bbcss_mock->mock(update_document => sub {
    my ($self, $request_ref) = @_;
    is_deeply(
        $request_ref,
        {   document_id     => $candidate->candidate_id(),
            customer_key    => 'dailyplanet_e48ez',
            action          => 'DelegateCandidate',
            update_fields   => [
                { name => 'broadbean_adcuser_id',       value => $delegate_user->provider_id },
                { name => 'broadbean_adcidentity_path', value => $delegate_user->hierarchical_path },
                { name => 'broadbean_adcadvert_id',     value => 9000 },
            ],
        }
    );
    return HTTP::Response->new(200);
});

my $result = $action->instance_perform();
like($result->{message}, qr/candidate is being sent/i, 'completed the action successfully');
ok(my $action_log = $stream2->factory('CandidateActionLog')->search({ action => 'delegate' })->first, 'successful delegation was stored in action logs');
is($action_log->data->{delegate_to_userid}, $delegate_user->id, 'the delegated users search user_id was also stored in log');
is($action_log->data->{delegate_to_contact_name}, $delegate_user->contact_name, 'the delegated users contact name was also stored in log');
is($action_log->data->{delegate_from_contact_name}, $current_user->contact_name, 'the current users contact name was also stored in log');

subtest 'Should not delegate to same user' => sub {
    my $action = Stream2::Action::DelegateCandidate->new(
        jobid               => 'kryptonite',
        stream2             => $stream2,
        delegate_to_userid  => $current_user->id,
        candidate_object    => $candidate,
        user_object         => $current_user,
        ripple_settings     => $current_user->ripple_settings,
    );
    like($action->instance_perform()->{error}->{message}, qr/to yourself/, 'dies when trying to delegate to the same user');
};

done_testing();

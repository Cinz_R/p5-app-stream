use Test::Most;
use Test::Stream2;
use Test::MockModule;
use HTTP::Response;
use Email::Abstract;

BEGIN {
    $ENV{ LWP_UA_MOCK } ||= 'playback';
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
}

use_ok( 'Stream2::Action::BulkForwardCandidates' );
use LWP::UserAgent::Mockable;

my $config_file = q{t/app-stream2.conf};
my $stream2 = Test::Stream2->new({ config_file => $config_file });
$stream2->deploy_test_db;
my $user = _setup_user();
my $s2_mock = Test::MockModule->new('Stream2');

my $dcv_mock = _setup_downloadcv();
_setup_results( $s2_mock );

my $s3_mock = Test::MockModule->new('Bean::AWS::S3FileFactory');
$s3_mock->mock( create => sub {
    return TestObject->new({
        query_string_authentication_uri => 'http://www.example.com'
    });
});

my $provider_mock = Test::MockModule->new('App::Stream2::Plugin::Auth::Provider::Adcourier');
$provider_mock->mock( download_attachment => sub {
    return {
        filename => 'my-attachment.docx',
        content => 'I am irrelevant'
    }
});

my $forward_settings_mock = Test::MockModule->new( 'Bean::Juice::APIClient::Stream' );
$forward_settings_mock->mock( get_forward_email_settings => sub {
    return {
        subject => 'found on [nice_board_name]',
        bcc => 'patbcc@example.com'
    }
});

my $send_called = 0;
$s2_mock->mock( send_email => sub {
    $send_called = 1;
    my ( $self, $email ) = @_;

    my $text_body = $email->entity->{ME_Parts}->[0]->{ME_Parts}->[0]->{ME_Bodyhandle}->{'MBS_Data'};
    my $html_body = $email->entity->{ME_Parts}->[0]->{ME_Parts}->[1]->{ME_Bodyhandle}->{'MBS_Data'};
    my $head = $email->entity->head;

    like ( $text_body, qr/Juliet Mooney/ );
    like ( $html_body, qr/Juliet Mooney/ );
    like ( $head->get('Subject'), qr/found_on_Responses/ );
    is ( $head->get('To'), "patrick\@example.com\n" ); # new lines are added by Email::Header
    is ( $head->get('Bcc'), "patbcc\@example.com\n" );
    like ( $html_body, qr/This is a test/ );
    like ( $text_body, qr/This is a test/ );
    like ( $text_body, qr/expire 30/ );

    # keep return type consistent with the original (mocked) subroutine
    return Email::Abstract->new($email->entity);
});

my $log_mock = Test::MockModule->new( 'Stream2::CandidateActionLog' );

my $log_called = 0;
$log_mock->mock( insert => sub {
    my ( $self, $opt_ref ) = @_;
    is ( $opt_ref->{action}, 'forward' );
    $log_called++;
});

my $out = Stream2::Action::BulkForwardCandidates->new({
    user => { user_id => $user->id },
    candidate_idxs => [0,1],
    results_id => 'aa-11',
    message => 'This is a test',
    recipients_emails => [ 'patrick@example.com' ],
    recipients_ids => [ $user->id ],
    ripple_settings => {},
    stream2 => $stream2,
    stream2_base_url => 'http://127.0.0.1/',
    jobid => 555,
})->instance_perform();

is( $log_called, 2 );
ok( $send_called );

END {
    LWP::UserAgent::Mockable->finished();
    done_testing();
}

sub _setup_user {
    my $result = $stream2->factory('SearchUser')->new_result({
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 555,     # just needs to be unique
        contact_name        => 'Patrick Mooney',
        group_identity      => 'pat',
        contact_details     => {
            username => 'pat@pat.pat.pat'
        },
        subscriptions_ref => {
            adcresponses => {
                nice_name => 'Responses',
                auth_tokens => {}
            }
        }
    });
    $result->save;
    return $result;
}

sub _setup_results {
    my $s2_mock = shift;

    $s2_mock->mock ( find_results => sub {
        return Stream2::Results->new({
            results => _results()
        });
    });
}

sub _setup_downloadcv {
    my $downloadcv_mock = Test::MockModule->new('Stream2::Action::DownloadCV');
    $downloadcv_mock->mock(
        'instance_perform' => sub {
            my $response = HTTP::Response->new();
            $response->content( 'This is the CV' );
            $response->content_type( 'text/plain' );
            $response->header(
                'Content-Disposition' => sprintf(
                    "attachment; filename=%s",
                    'my-cv.txt'
                )
            );
            return $response;
        }
    );
    return $downloadcv_mock;
}

sub _results {
    return [
        {
            'can_downloadcv' => 1,
            'usertags' => [],
            'documents' => [
                             {
                               'document_file_name' => '1472283731-30332-Juliet_Mooney_-_CV.docx',
                               'document_file_url' => 'https://bbcss.adcourier.com/api/documentfile?customer_key=bbts_pat_R5cce04f848&document_file_id=4427409'
                             }
                           ],
            'canonical_name' => 'Juliet Mooney',
            'canonical_city' => 'London',
            'email' => 'patrick@broadbean.com',
            'current_position' => {
                                    'normalized_did' => '',
                                    'company_name' => 'Griffin Stone Moscrop & Co',
                                    'job_title' => 'Payroll Clerk / Assistant to the Partners',
                                    'carotene_v2' => '',
                                    'description' => 'Responsibilities past and present include:  * Ad hoc PA duties to the four partners.  * Secretarial duties such as answering telephones, greeting clients, booking meetings, preparing  meeting rooms, providing refreshments, booking couriers and typing dictation.  * Administrative duties such as creating and maintaining MS Word templates, implementing and  maintaining filing systems, updating office manuals, preparing and sending mailshots, sorting  and scanning incoming post, identifying and implementing procedural changes to improve  efficiency, updating website, ordering stationery and furniture, checking and entering staff  expenses, and setting-up new clients and employees on internal systems.  * Managing contracts such as cleaning services, office equipment, maintenance, fire safety and  recycling.  * Invoicing and credit control.  * Processing weekly, monthly and annual payrolls for clients including distributing payslips,  preparing P45s and P60s, completing statutory RTI reporting, performing year-end procedures  and identifying and monitoring Auto-Enrolment (AE) staging dates.  * Responding to clients\' payroll queries including explaining PAYE and NI deductions, tax code  implications and AE obligations.  * Liaising with and reporting to statutory bodies regarding various payroll and AE matters.  * Setting up NEST pension schemes on behalf of clients.  * Liaising with and submitting accounts to Companies House, the Charity Commission and the  Financial Conduct Authority.  * First level IT support / liaising with external IT providers and software support.  * Office first aider.  * Bookkeeping for clients using Sage and Excel.  * Key role in the logistics of an office move in 2013.  * Conducted telephone interviews in autumn 2014.',
                                    'normalized_company_name' => '',
                                    'company_naics' => '',
                                    'end_date' => '2016-10-19T00:00:00.000Z',
                                    'onet_17' => 'Office Clerks, General',
                                    'start_date_epoch' => 1199145600,
                                    'company_size' => 0,
                                    'start_date' => '2008-01-01T00:00:00.000Z',
                                    'is_current_position' => 1,
                                    'end_date_epoch' => 1476835200
                                  },
            'latitude' => 0,
            'education_list' => [
                                  {
                                    'normalized_school_did' => '',
                                    'degree_name' => '',
                                    'normalized_major_name' => '',
                                    'graduation_date_epoch' => 1558137600,
                                    'degree_code' => 'CE3',
                                    'normalized_major_did' => '',
                                    'school_name' => 'St John\'s Ambulance; The; Mercia',
                                    'graduation_date' => '2019-05-18T00:00:00.000Z',
                                    'major_name' => 'First Aid at Work qualification; Principles of Bookkeeping - Mercia Bookkeeping; Next Stage; A Levels',
                                    'normalized_school_name' => ''
                                  }
                                ],
            'broadbean_adcresponse_source_id' => '1',
            'longitude' => 0,
            'broadbean_adcresponse_attachments' => [
                                                     's3://broadbean-eu-files/afs/cvs/pat/C459CC38-6C29-11E6-95C7-6F0F9ACAE4DE'
                                                   ],
            'broadbean_adcresponse_email_body' => 's3://broadbean-eu-files/afs/emailbodies/pat/C4859674-6C29-11E6-95C7-6F0F9ACAE4DE',
            'name' => 'Juliet Mooney',
            'flagging_history' => {},
            'broadbean_adcresponse_attachement_cv' => 's3://broadbean-eu-files/afs/cvs/pat/C459CC38-6C29-11E6-95C7-6F0F9ACAE4DE',
            'idx' => 5,
            'county' => 'GB-LND',
            'results_id' => '454EC5E6-9AAB-11E6-89D8-F4D2CE8A660C',
            'advert_preview_url' => 'https://www.adcourier.com/view-vacancy.cgi?advert_id=131622',
            'can_forward' => 1,
            'broadbean_adcresponse_is_read' => 'True',
            'broadbean_employer_years' => 8,
            'cv_url' => 'https://bbcss.adcourier.com/api/documentfile?customer_key=bbts_pat_R5cce04f848&document_file_id=4427409',
            'messaging_uri' => 'email://?to=patrick%40broadbean.com',
            'country' => 'GB',
            'broadbean_adcresponse_forwarded' => 'False',
            'broadbean_adcuser_id' => '150780',
            'broadbean_adcresponse_name' => 'Juliet Mooney',
            'editable' => 1,
            'can_paymessage' => 1,
            'has_cv' => 1,
            'canonical_telephone' => '07545 877 858',
            'destination' => 'adcresponses',
            'skill_list' => [],
            'broadbean_adcboard_nicename' => 'Broadbean Test Board',
            'broadbean_adcresponse_tagged_time' => '2016-08-27T07:24:54.000Z',
            'broadbean_adcresponse_rank' => 1,
            'canonical_country' => 'GB',
            'total_years_experience' => '9.5',
            'broadbean_adcresponse_updated' => '2016-08-27T07:24:51.000Z',
            'broadbean_group_adcresponse_id' => 'pat:53544',
            'broadbean_employer_org_job_title' => 'Payroll Clerk / Assistant to the Partners',
            'has_profile' => 1,
            'broadbean_adcadvert_id' => '131622',
            'broadbean_employer_org_name' => 'Griffin Stone Moscrop & Co',
            'broadbean_adcresponse_progressed' => 'False',
            'broadbean_adcresponse_id' => '53544',
            'last_name' => 'Mooney',
            'broadbean_group_identity' => 'pat',
            'city' => 'London',
            'currently_employed' => 'True',
            'broadbean_adcadvert_title' => 'qa-test - QA TEST',
            'broadbean_adcresponse_subject' => 'Application for qa-test - QA TEST via Broadbean Test Board',
            'broadbean_adcresponse_date' => '2016-08-27T07:24:51.000Z',
            'candidate_id' => '2378478',
            'phone' => '07545 877 858',
            'broadbean_adcidentity_path' => '@pat@pat@pat@pat',
            'search_record_id' => 1,
            'broadbean_adcboard_id' => '999',
            'rank' => 0,
            'can_importcv' => 1,
            'first_name' => 'Juliet'
        },
        {
            'can_downloadcv' => 0,
            'usertags' => [],
            'documents' => [
                             {
                               'document_file_name' => '1472281952-75514-52-talentsearch_137134119.docx',
                               'document_file_url' => 'https://bbcss.adcourier.com/api/documentfile?customer_key=bbts_pat_R5cce04f848&document_file_id=4427410'
                             }
                           ],
            'canonical_name' => 'Claire Pinkney',
            'canonical_city' => 'London',
            'email' => 'clairep@broadbean.com',
            'current_position' => {
                                    'normalized_did' => '',
                                    'company_name' => 'Magic Ltd',
                                    'job_title' => 'Senior Client Support',
                                    'carotene_v2' => '',
                                    'description' => 'After leaving the world of Recruitment, I was asked to join a growing company called Broadbean. After meeting both the Founder and the MD my mind was made up, it was the perfect career opportunity and I have never looked back. I was taken on to become a relationship manager, but before that was told that I would have to spend some time learning about the product. I thoroughly enjoyed it, so much so I that I quickly learnt how to train people on the product. After that I became the main trainer, often training for literally 5-6 hours on the phone every day.  I have since progressed to being a senior member of our team and have currently been offered a couple of new roles within Broadbean that will enhance my career further.  Key responsibilities:   * Train all new and current clients on the whole Broadbean system regularly getting great  reviews.  * Responsible for all our Dutch set ups and support meaning I have to adapt my approach due to  the language difference.  * In charge of our latest project \'Stream\' making sure it worked correctly and passing that  knowledge to my colleagues.',
                                    'normalized_company_name' => '',
                                    'company_naics' => '',
                                    'end_date' => '2016-10-19T00:00:00.000Z',
                                    'onet_17' => 'Computer User Support Specialists',
                                    'start_date_epoch' => 1157068800,
                                    'company_size' => 0,
                                    'start_date' => '2006-09-01T00:00:00.000Z',
                                    'is_current_position' => 1,
                                    'end_date_epoch' => 1476835200
                                  },
            'latitude' => 0,
            'education_list' => [
                                  {
                                    'normalized_school_did' => '',
                                    'degree_name' => '',
                                    'normalized_major_name' => '',
                                    'graduation_date_epoch' => 615168000,
                                    'degree_code' => 'CE3',
                                    'normalized_major_did' => '',
                                    'school_name' => 'Brook Comprehensive School',
                                    'graduation_date' => '1989-06-30T00:00:00.000Z',
                                    'major_name' => 'GCSE\'s',
                                    'normalized_school_name' => ''
                                  },
                                  {
                                    'normalized_school_did' => '',
                                    'degree_name' => '',
                                    'normalized_major_name' => '',
                                    'graduation_date_epoch' => 678240000,
                                    'degree_code' => 'CE32',
                                    'normalized_major_did' => '',
                                    'school_name' => 'Loxley College',
                                    'graduation_date' => '1991-06-30T00:00:00.000Z',
                                    'major_name' => 'BTEC 1st Electrical Engineering',
                                    'normalized_school_name' => ''
                                  }
                                ],
            'broadbean_adcresponse_source_id' => '3',
            'longitude' => 0,
            'broadbean_adcresponse_attachments' => [
                                                     '1472281952-75514-52-talentsearch_137134119.docx',
                                                     's3://broadbean-eu-files/afs/cvs/pat/C459CC38-6C29-11E6-95C7-6F0F9ACAE4DE' # CV from above result
                                                   ],
            'name' => 'Claire Pinkney',
            'flagging_history' => {},
            'broadbean_adcresponse_attachement_cv' => '1472281952-75514-52-talentsearch_137134119.docx',
            'idx' => 6,
            'county' => 'GB-LND',
            'results_id' => '454EC5E6-9AAB-11E6-89D8-F4D2CE8A660C',
            'advert_preview_url' => 'https://www.adcourier.com/view-vacancy.cgi?advert_id=131620',
            'can_forward' => 1,
            'broadbean_adcresponse_is_read' => 'False',
            'broadbean_employer_years' => 10,
            'cv_url' => 'https://bbcss.adcourier.com/api/documentfile?customer_key=bbts_pat_R5cce04f848&document_file_id=4427410',
            'messaging_uri' => 'email://?to=clairep%40broadbean.com',
            'country' => 'GB',
            'broadbean_adcresponse_forwarded' => 'False',
            'broadbean_adcuser_id' => '150780',
            'broadbean_adcresponse_name' => 'Claire Pinkney',
            'editable' => 1,
            'can_paymessage' => 0,
            'has_cv' => 1,
            'canonical_telephone' => '07500 445 178',
            'destination' => 'adcresponses',
            'skill_list' => [],
            'broadbean_adcboard_nicename' => 'Talent Search',
            'broadbean_adcresponse_tagged_time' => '2016-08-27T07:12:35.000Z',
            'broadbean_adcresponse_rank' => 5,
            'canonical_country' => 'GB',
            'total_years_experience' => '20.83',
            'broadbean_adcresponse_updated' => '2016-08-27T07:12:32.000Z',
            'broadbean_group_adcresponse_id' => 'pat:53543',
            'broadbean_employer_org_job_title' => 'Senior Client Support',
            'has_profile' => 1,
            'broadbean_adcadvert_id' => '131620',
            'broadbean_employer_org_name' => 'Magic Ltd',
            'broadbean_adcresponse_progressed' => 'False',
            'broadbean_adcresponse_id' => '53543',
            'last_name' => 'Pinkney',
            'broadbean_group_identity' => 'pat',
            'city' => 'London',
            'currently_employed' => 'True',
            'broadbean_adcadvert_title' => 'test123 - This is a test',
            'broadbean_adcresponse_date' => '2016-08-27T07:12:32.000Z',
            'candidate_id' => '2378482',
            'phone' => '07500 445 178',
            'broadbean_adcidentity_path' => '@pat@pat@pat@pat',
            'search_record_id' => 1,
            'broadbean_adcboard_id' => '3286',
            'rank' => 0,
            'can_importcv' => 0,
            'first_name' => 'Claire'
        }
    ];
}

{
    # TestObject provides a getter for any attribute
    package TestObject;
    use AutoLoader;
    sub new { bless pop, shift; }
    sub AUTOLOAD {
        our $AUTOLOAD;
        my ( $method ) = $AUTOLOAD =~ m/\ATestObject::(.*)\z/;
        return $method eq 'DESTROY' ? () : shift->{$method};
    }
}

#! perl
use strict;
use warnings;
use Test::Most;

use Stream2::Action::PhoneCandidate;

use Log::Any::Adapter;

# Log::Any::Adapter->set('Stderr');

use Test::Stream2;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'" . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ), "Ok can build stream2" );

$stream2->deploy_test_db;

my $user = $stream2->factory('SearchUser')->new_result(
    {   provider            => 'demo',
        last_login_provider => 'demo',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_name        => 'Andy Jones',
        contact_email       => 'andy@broadbean.com',
        settings            => { responses => { bg_language_pack => 'au' } }
    }
);
$user->save();

my $candidate = Stream2::Results::Result->new(
    {   destination => 'talentsearch',
        name        => 'John Smith',
    }
);

my $call_candidate = Stream2::Action::PhoneCandidate->new(
    stream2          => $stream2,
    user_object      => $user,
    ripple_settings  => $user->ripple_settings(),
    candidate_object => $candidate,
    jobid            => "Calling Smithy",
);

# run the instance
my @result = $call_candidate->instance_perform();

my @all_actions = $stream2->factory('CandidateActionLog')->search()->all();

is( $all_actions[0]->action(), 'phonecall', 'OK log action is phonecall' );
is_deeply(
    $all_actions[0]->data(),
    {   s3_log_id        => 'Calling Smithy',
        'candidate_name' => 'John Smith',
    },
    'OK data looks correct'
);

done_testing();

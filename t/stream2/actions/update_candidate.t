#! perl -w

use Test::Most;
use Test::MockModule;


use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');

use Test::Stream2;
use Stream2::Action::UpdateCandidate;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'" . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ), "Ok can build stream2" );

$stream2->deploy_test_db;

my $user = $stream2->factory('SearchUser')->new_result(
    {   provider            => 'demo',
        last_login_provider => 'demo',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_name        => 'Andy Jones',
        contact_email       => 'andy@broadbean.com',
        settings            => {}
    }
);
$user->save();

my $candidate = Stream2::Results::Result->new(
    {   destination => 'adcresponses',
        name        => 'John Smith',
        candidate_id => '321',
    }
);

{
    my $update_candidate = Stream2::Action::UpdateCandidate->new(
        stream2          => $stream2,
        stream2_base_url => 'http://127.0.0.1/',
        user_object      => $user,
        ripple_settings  => $user->ripple_settings(),
        candidate_object => $candidate,
        candidate_idx    => 0,
        results_id       => 'abcd',
        jobid            => "Calling Smithy",
        fields           => { field1 => 'newvalue', field2 => 'anothernewvalue' }
    );

    my $gone_through = 0;
    my $mock_feed = Test::MockModule->new( ref( $update_candidate->feed() ) );
    $mock_feed->mock( 'update_candidate' => sub{
                          my ($self, $candidate , $fields ) = @_;
                          $gone_through = 1;
                          is_deeply( $fields, { field1 => 'newvalue', field2 => 'anothernewvalue' });
                      });
    $update_candidate->instance_perform();
    ok( $gone_through );
    ok( my $context = $update_candidate->build_context() , "Ok got context");
    my @context_keys = sort keys %$context;
    cmp_ok( scalar(@context_keys) , '>' , 1 , "Ok more than one key in context");
    is_deeply( $context->{fields}, { field1 => 'newvalue', field2 => 'anothernewvalue' } );
}



done_testing();

#! perl -w

use Test::Most;
use Test::MockModule;

use FindBin;
use Test::More;

use Stream2;
use Stream2::Results::Result;
use Stream2::Action::Search;
use Stream::Engine::API::REST;
use Stream2::Criteria;
use Redis;

# This is not the definitive test for the search action and should be improved upon
# for example, testing the inputs to the feed

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}


my $redis = Redis->new(server => 'whatever', no_auto_connect_on_new => 1);
my $stream2 = Stream2->new({ config_file => $config_file, qurious => Qurious->new( redis => $redis ) });
$stream2->stream_schema();
# Deploying schema
$stream2->stream_schema()->deploy();

my @results = (
    Stream2::Results::Result->new({
        candidate_id => 1,
        email => 'vladimir@thekremlin.com',
        destination => 'sausage',
        location_text => 'Isle of Man',
        name => 'Vladimir Putin',
    }),
    Stream2::Results::Result->new({
        candidate_id => 2,
        email => 'cameron@downingsteeet.com',
        destination => 'sausage',
        location_text => 'Tokyo',
        name => 'David Cameron'
    })
);


# blank feed which returns above results
my $feed_mock = Test::MockModule->new( 'Stream::Engine::API::REST' );
$feed_mock->mock( search => sub { push ( @{shift->results->results}, @results ) });
$feed_mock->mock( quota_guard => sub { pop->() } );

# results store that doesn't go to redis
my $job = MyJob->new();
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { $job->{results} = shift->results; } );
$results_mock->mock( default_results => sub { []; } );
$results_mock->mock( fetch_results => sub { @results } );
$results_mock->mock( total_results => sub{ 2 } );

# avoid some of the corollary actions
my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( increment_analytics => sub { 1; } );
$stream2_mock->mock( redis => sub { 1; } );

my $criteria = Stream2::Criteria->new( schema => $stream2->stream_schema() );
$criteria->save();

# Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
    contact_email => 'sausage@tofu.co.uk',
    contact_name => 'Patrick',
    subscriptions_ref => {
        sausage => {
            nice_name => 'Sausage Board',
            auth_tokens => {}
        }
    }
});
my $user_id = $memory_user->save();


my $qurious_mock = Test::MockModule->new( 'Qurious' );
subtest 'Search with followups' => sub {
    plan tests => 12;

    ok(
        my $action = Stream2::Action::Search->new({
            criteria_id => $criteria->id(),
            template_name    => 'sausage',
            stream2_base_url => 'http://127.0.0.1',
            stream2     => $stream2,
            user => { user_id => $user_id },
            ripple_settings => {},
            log_chunk_capture => 0,
            options => { candidate_action => { class => 'Test Class', options => { test => 'test-options' } }, results_per_page => 11 },
            jobid => 'doesnt matter',
            feed => Stream::Engine::API::REST->new(),
        }),
        "Can create search task",
    );

    $stream2_mock->mock( log_search_calendar => sub {
        my ( $self, $opts ) = @_;
        is ( $opts->{search_record_id}, 1, "we have a search_record_id" );
        return 1;
    });
    $qurious_mock->mock( create_job => sub {
        my ( $self, %stuff ) = @_;
        is ( $stuff{class}, 'Test Class' );
        is ( $stuff{parameters}->{options}->{test}, 'test-options' );
        return MyJob->new();
    });

    $feed_mock->mock( token_value => sub {
        $feed_mock->original( 'token_value' )->( @_ );
        my $self = shift;
        return if @_ == 1;
        my %to_set = @_;
        if ( $to_set{results_per_page} ) {
            is ( $to_set{results_per_page}, 11 );
        }
    });

    ok( $action->instance_perform( $job ), "performing task finishes" );
    is( scalar(@{$job->{results}}), 2, "results are stored in the job" );

    my $search_records = $stream2->factory('SearchRecord')->search(
        { board => 'sausage' }, {}
    );
    ok $search_records->count == 1,
        'Stored search record with board named \'sausage\'';
    is(  $search_records->first()->n_results() ,2 , "Ok got right number of total results");
    is(  $search_records->first()->remote_ip() , '127.0.0.1', "Ok stored the remote ip of user");
};

done_testing();

{
    package MyJob;
    sub new { bless {}, shift; }
    sub complete_job { shift->{meta} = pop; }
    sub guid { int(rand( 100 )); }
    sub enqueue { 1; }
}

#! perl -w

use Test::Most;
use Stream2::Action::UntagCandidate;
use Test::MockModule;
use Test::Stream2;

use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');

my $config_file = 't/app-stream2.conf';
my $stream2 = Test::Stream2->new( { config_file => $config_file } );
$stream2->deploy_test_db;

my $user = $stream2->factory('SearchUser')->new_result(
    {
        provider            => 'demo',
        last_login_provider => 'demo',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_name        => 'Andy Jones',
        contact_email       => 'andy@broadbean.com',
        settings            => { responses => { bg_language_pack => 'au' } }
    }
);
$user->save();

subtest "Fails when tag is not found" => sub {
    my $candidate = Stream2::Results::Result->new(
        {
            candidate_id => 555,
            destination => 'adcresponses',
            name        => 'John Smith',
        }
    );

    my $untag_action = Stream2::Action::UntagCandidate->new(
        stream2          => $stream2,
        user_object      => $user,
        candidate_object => $candidate,
        jobid            => "Tagging Smithy",
        ripple_settings  => $user->ripple_settings(),
        tag_name         => 'cholesterol'
    );

    ok(my $result = $untag_action->instance_perform(), 'ran the untag candidate action');
    like($result->{error}->{message}, qr/not found/, 'error message indicates tag is nonexistent');
    is($result->{error}->{properties}->{type}, 'taggingerror', 'error type is correct');

    ok( !$stream2->factory('CandidateActionLog')->find({ action => 'candidate_tag_delete' }), 'no action log created' );
};

subtest "Delete a candidate tag" => sub {
    my $candidate = Stream2::Results::Result->new(
        {
            candidate_id => 555,
            destination => 'iprofile',
            name        => 'John Smith',
        }
    );

    my $tag = $stream2->factory('Tag')->create({
        group_identity  => $user->group_identity,
        tag_name        => 'pizza',
        tag_name_ci     => 'pizza',
    });

    my $candidate_tag = $stream2->stream_schema->resultset('CandidateTag')->create({
        group_identity  => $user->group_identity,
        candidate_id    => $candidate->id(),
        tag_name        => $tag->tag_name,
    });

    $candidate->add_tag($candidate_tag->tag_name);
    my @tags = $candidate->tags;
    is_deeply(\@tags, ['pizza'], 'candidate tags has pizza');

    my $untag_action = Stream2::Action::UntagCandidate->new(
        stream2          => $stream2,
        user_object      => $user,
        candidate_object => $candidate,
        jobid            => "Tagging Smithy",
        ripple_settings  => $user->ripple_settings(),
        tag_name         => $candidate_tag->tag_name,
    );

    ok(my $result = $untag_action->instance_perform(), 'ran the untag candidate action');
    is($result->{message}, "Tag 'pizza' removed.", 'result message was provided');
    @tags = $candidate->tags;
    is_deeply(\@tags, [], 'candidate tags are empty');

    ok( ! $stream2->stream_schema->resultset('CandidateTag')->find({ tag_name => 'pizza' }), 'candidate tag removed from database');

    ok( my $action_log = $stream2->factory('CandidateActionLog')->find({ action => 'candidate_tag_delete' }), 'found action log' );
    is( $action_log->data->{tag_name}, 'pizza', 'included tag name in log action data');
};

done_testing();

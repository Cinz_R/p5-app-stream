#! perl -w

use Test::Most;
use Test::MockModule;


use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');

use Test::Stream2;
use Stream2::Action::DownloadProfile;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'" . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ), "Ok can build stream2" );

$stream2->deploy_test_db;

my $user = $stream2->factory('SearchUser')->new_result(
    {   provider            => 'demo',
        last_login_provider => 'demo',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_name        => 'Andy Jones',
        contact_email       => 'andy@broadbean.com',
        settings            => {}
    }
);
$user->save();

my $candidate = Stream2::Results::Result->new(
    {   destination => 'adcresponses',
        name        => 'John Smith',
        candidate_id => '321',
    }
);

{
    my $download_profile = Stream2::Action::DownloadProfile->new(
        stream2          => $stream2,
        stream2_base_url => 'http://127.0.0.1/',
        user_object      => $user,
        ripple_settings  => $user->ripple_settings(),
        candidate_object => $candidate,
        candidate_idx    => 0,
        results_id       => 'abcd',
        jobid            => "Calling Smithy",
    );

    my $mock_feed = Test::MockModule->new( ref( $download_profile->feed() ) );
    $mock_feed->mock( 'download_profile' => sub{
                          my ($self, $candidate) = @_;
                          $candidate->attr( 'cv_html' ,'<some>html</some>' );
                      });

    # run the instance
    my $candidate_ref = $download_profile->instance_perform();
    like( $candidate_ref->{cv_html} , qr/\<some\>html/ , "Ok just the html given by the feed");
    unlike( $candidate_ref->{cv_html} , qr/pdfjs/ , "OK no pdfjs rendering" );
    unlike( $candidate_ref->{cv_html} , qr/STOK/, "No session token here");
}


{
    my $download_profile = Stream2::Action::DownloadProfile->new(
        stream2          => $stream2,
        stream2_base_url => 'http://127.0.0.1/',
        user_object      => $user,
        ripple_settings  => $user->ripple_settings(),
        candidate_object => $candidate,
        candidate_idx    => 0,
        results_id       => 'abcd',
        jobid            => "Calling Smithy",
    );

    my $mock_feed = Test::MockModule->new( ref( $download_profile->feed() ) );
    $mock_feed->mock( 'download_profile' => sub{
                          my ($self, $candidate) = @_;
                          $candidate->attr( 'cv_mimetype' , 'application/pdf' );
                      });

    # run the instance
    my $candidate_ref = $download_profile->instance_perform();
    like( $candidate_ref->{cv_html} , qr/pdfjs/ , "OK an application/pdf profile is rendered using pdfjs" );
    unlike( $candidate_ref->{cv_html} , qr/STOK/, "No session token here");
}

{
    my $download_profile = Stream2::Action::DownloadProfile->new(
        stream2          => $stream2,
        stream2_base_url => 'http://127.0.0.1/',
        user_object      => $user,
        ripple_settings  => $user->ripple_settings(),
        candidate_object => $candidate,
        candidate_idx    => 0,
        results_id       => 'abcd',
        jobid            => "Calling Smithy",
        session_token    => 'saussage',
    );

    my $mock_feed = Test::MockModule->new( ref( $download_profile->feed() ) );
    $mock_feed->mock( 'download_profile' => sub{
                          my ($self, $candidate) = @_;
                          $candidate->attr( 'cv_mimetype' , 'application/pdf' );
                      });

    # run the instance
    my $candidate_ref = $download_profile->instance_perform();
    like( $candidate_ref->{cv_html} , qr/pdfjs/ , "OK an application/pdf profile is rendered using pdfjs" );
    like( $candidate_ref->{cv_html} , qr/STOK%3Dsaussage/, "Session token is there");
}


done_testing();

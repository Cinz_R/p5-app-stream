use Test::Most;
use HTTP::Response;

use Stream2;

use_ok('Stream2::Action::FeedCrawler');

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

my $stream2 = Stream2->new({ config_file => $config_file });
# Deploying schema
$stream2->stream_schema()->deploy();

ok my $action = Stream2::Action::FeedCrawler->new({
    stream2 => $stream2,
}), 'Feed crawling action was created';

done_testing();

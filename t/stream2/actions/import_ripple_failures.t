#! perl -w

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';
    $ENV{LWP_UA_MOCK} ||= $DEFAULT_UA_MOCK;
    $ENV{LWP_UA_MOCK_FILE} ||= __FILE__ . '-lwp-mock.out';
}
use LWP;
use LWP::UserAgent::Mockable;

use Test::Most;
use Test::MockModule;
use MIME::Base64;

use FindBin;

use Stream2;
use Stream2::Results::Result;
use Stream2::Action::ImportCandidate;

# use Log::Any::Adapter qw/Stderr/;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'" . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Stream2->new( { config_file => $config_file } ), "Ok can build stream2" );
ok( $stream2->stream_schema(), "Ok got schema" );

# Deploying schema
$stream2->stream_schema()->deploy();

# Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result(
    {   provider            => 'adcourier',
        last_login_provider => 'adcourier_ats',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_email       => 'sausage@tofu.co.uk',
        contact_name        => 'Patrick',
        settings            => { 'responses' => { bg_language_pack => 'uk' } },
        subscriptions_ref   => {
            sausage => {
                nice_name   => 'Sausage Board',
                auth_tokens => {}
            },
            indeed => {
                nice_name   => 'Indeed Board',
                auth_tokens => {}
            },
            talentsearch => {
                nice_name   => 'Talent Search',
                auth_tokens => {}
            }
        }
    }
);

ok( my $user_id = $memory_user->save(), "Ok can save this user" );

my %tests = (
    NoEmail => {
        destination      => 'sausage',
        location_text    => 'Isle of Man',
        name             => 'Vladimir Putin',
        search_record_id => 2345,
        cv_content       => 'with_email_cv_content',
        cv_filename      => 'with_email_cv_content.cv',
        enrichment_email => 'vladimir@thekremlin.ru',
        correct_email    => 'vladimir@thekremlin.ru',
        nice_name        => 'Sausage Board',
    },
);

foreach my $board_test ( keys %tests ) {

    my $test_params = $tests{$board_test};

    my $stream_mock = Test::MockModule->new('Stream2');
    $stream_mock->mock(
        find_results => sub {
            return $board_test;
        }
    );

    my $downloadcv_mock = Test::MockModule->new('Stream2::Action::DownloadCV');
    $downloadcv_mock->mock(
        'instance_perform' => sub {
            my $response = HTTP::Response->new();
            $response->content( $test_params->{cv_content} );
            $response->content_type('application/pdf');
            $response->header( 'Content-Disposition' => sprintf( "attachment; filename=%s", $test_params->{cv_filename} ) );
            return $response;
        }
    );

    # Mock the ts import API. This is because we want
    # the full user_import_candidate to be executed.
    my $tsimport_mock = Test::MockModule->new( ref( $stream2->tsimport_api() ) );
    $tsimport_mock->mock(
        import_cv_content => sub {
            my ( $self, %args ) = @_;
            is( $args{attributes}->{locale}, 'en_GB' );
            return { id => 123456 };
        }
    );

    my $encoded_content = MIME::Base64::encode_base64("woooo");
    my $hr_xml          = q|
        <Candidate>
            <CandidateProfile>
                <PersonalData>
                    <PersonName>
                        <GivenName>Jean Pierre</GivenName>
                        <FormattedName>Jean Pierre Marcel</FormattedName>
                        <FamilyName>Marcel</FamilyName>
                    </PersonName>
                    <Email>jean@pierre.com</Email>
                    <ChannelId>cvlibrary</ChannelId>
                    <ChannelName>CV library</ChannelName>
                    <Doc FileName="cvfilename.doc" DocumentType="Attachment" EncodingType="Base64" MimeType="text/xml">| . $encoded_content . q|</Doc>
                </PersonalData>
            </CandidateProfile>
        </Candidate>|;

    my $search_user_mock = Test::MockModule->new('Stream2::O::SearchUser');
    $search_user_mock->mock(
        parse_cv_with_locale => sub {
            return { email => $test_params->{enrichment_email}, hr_xml => $hr_xml };
        }
    );

    ok( my $action = Stream2::Action::ImportCandidate->new(
            {   stream2           => $stream2,
                user              => { user_id => $user_id },
                ripple_settings   => { new_candidate_url => 'https://www.here-is-an-error.com' },
                results_id        => 1,
                candidate_idx     => 1,
                log_chunk_id      => 12345,
                log_chunk_capture => 0,
                options           => { candidate_attributes => { test => 'armadillo' } }
            }
        )
    );

    ok( $action->candidate_object, 'there is a $action->candidate_object' );

    my $xml = q|<?xml version="1.0" encoding="UTF-8" ?>
    <StreamCandidateResponse>
        <Version>3.0</Version>
        <Status>ERROR</Status>
        <Message>Missing Required Field: Email
        </Message>
    </StreamCandidateResponse>|;

    my $lwp_useragent_mock = Test::MockModule->new('LWP::UserAgent');
    $lwp_useragent_mock->mock(
        request => sub {
            my $response = HTTP::Response->new( '200', 'OK' );
            $response->header( 'Content-Type' => 'text/xml' );
            $response->content($xml);
            return $response;
        }
    );

    ok( my $response = $action->instance_perform(), 'perform the action' );
    ok( $response->{error}->{message} =~ /Missing Required Field: Email/, 'Correct error message is included' );
}

done_testing();

END{
    LWP::UserAgent::Mockable->finished();
}

{

    package NoEmail;

    sub find {
        Stream2::Results::Result->new(
            {   destination      => $tests{NoEmail}->{destination},
                candidate_id     => 1234,
                location_text    => $tests{NoEmail}->{location_text},
                name             => $tests{NoEmail}->{name},
                search_record_id => $tests{NoEmail}->{search_record_id},
            }
        );
    }
}

#! perl -w

use Test::Most;
use Test::MockModule;

use Log::Any::Adapter;
# use Carp::Always;
# Log::Any::Adapter->set('Stderr');

use Test::Stream2;
use Stream2::Action::MessageCandidate;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'" . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Test::Stream2->new( { config_file => $config_file } ), "Ok can build stream2" );

$stream2->deploy_test_db;

my $user = $stream2->factory('SearchUser')->new_result(
    {   provider            => 'adcourier',
        last_login_provider => 'demo',
        provider_id         => 54321,
        group_identity      => 'cinzwonderland',
        contact_name        => 'Andy Jones',
        contact_email       => 'andy@broadbean.com',
        settings            => { responses => { bg_language_pack => 'au' } }
    }
);
$user->save();


# Mock the S3
{ package MockedS3Object; use base qw/Class::AutoAccess/; sub new{ bless $_[1], $_[0]; } 1; }
my $files_mock = Test::MockModule->new( ref( $stream2->factory('PrivateFile') ) );
$files_mock->mock(
    'create' => sub {
        return MockedS3Object->new( { key => 'abcdefg' } );
    }
);

# Mock this as the email service stores emails we send.
my $email_client = Test::MockModule->new('Bean::EmailService::Client');
$email_client->mock( store_email => sub {
    return;
});

my $email_module = Test::MockModule->new('Email::Sender::Simple');
my $template = $stream2->factory('EmailTemplate')->create(
    {   group_identity => $user->group_identity(),
        name           => 'supertemplate'
    }
);

my $cvp_client_module = Test::MockModule->new('Bean::CVP::Client');
$cvp_client_module->mock( 'parse_cv_contents' => sub {
    return { email => 'blacked@example.com' };
});

my $cand_api_module = Test::MockModule->new('Stream2::Action::DownloadCV');
$cand_api_module->mock(
    'instance_perform' => sub {
        my ($self) = @_;
        return HTTP::Response->new( 200, "OK", [ "Content-Type" => "text/xml", "Content-Disposition" => q{inline; filename="file.txt"} ],
           "TEST CV" );
    }
);

my $candidate = Stream2::Results::Result->new(
    {   destination => 'talentsearch',
        name        => 'John Smith',
        cv_html     => 'TEST CV',
        cv_link     => 'http://www.foo.com'
    }
);
$candidate->has_cv( 1 );

my $action_params =
    {
        stream2           => $stream2,
        base_url          => 'http://localhost.com/',
        message_bcc       => '',
        message_subject   => 'Whatever',
        message_content   => 'blablablabla',
        message_email     => 'john@example.com',
        subject_prefix    => 'Yeah',
        jobid             => 'blabla',
        candidate_object  => $candidate,
        user_object       => $user,
        ripple_settings   => $user->ripple_settings(),
        advert_id         => 123,
        email_template_id => $template->id,
    };

my $email_action = Stream2::Action::MessageCandidate->new( $action_params );

my $mocked_email_action = Test::MockModule->new( ref( $email_action ) );
$mocked_email_action->mock( get_advert => sub{ return {}; });
$mocked_email_action->mock( _advert_preview_url => sub { return '<a href="link">Advert title</a>'; } );

subtest "Sending an email template works" => sub {
    my $email_sent = 0;
    $email_module->mock(
        send => sub {
            my ( $self, $local_email ) = @_;
            $email_sent = 1;
            1;
        }
    );

    # send email once
    $email_action->instance_perform();

    my $last_action = $stream2->factory('CandidateActionLog')->search( undef, { order_by => { -desc => 'id' } } )->first();
    is( $last_action->action(), "message", "All arguments present and correct" );
    ok( $email_sent, "Email send was called" );
};

{
    # try it again
    my $result = $email_action->instance_perform();
    ok( $result->{error} );
    like( $result->{error}->{message} , qr/already emailed this/ );
    my $last_action = $stream2->factory('CandidateActionLog')->search( undef, { order_by => { -desc => 'id' } } )->first();
    is( $last_action->action(), "message_fail" );
}

{
    # Switch the setting on and try again.
    my $setting = $stream2->factory('Setting')->search({ setting_mnemonic => 'candidate-actions-message-template-resend-enabled'})->first();
    $setting->set_users_value( $user, [ $user->id() ] , 1 );
    $user = $stream2->find_user( $user->id() ); # Reload user so it gets its settings just fine
    # Same action params, fresh user object.
    my $email_action = Stream2::Action::MessageCandidate->new({ %{$action_params} , user_object => $user });
    my $result = $email_action->instance_perform();
    ok( ! $result->{error} , "Ok it works this time to send the same template twice");
}

done_testing();

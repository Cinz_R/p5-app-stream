#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::MockModule;

use Log::Any::Adapter;
#Log::Any::Adapter->set('Stderr');

use Log::Log4perl::MDC;

use Qurious::Job;

my $action_sequence = 0;

my $mdc = Test::MockModule->new( 'Log::Log4perl::MDC' );


{
    package My::Action;
    use Moose;
    use Log::Any qw/$log/;
    has 'jobid' => ( is => 'ro', isa => 'Str' );
    with qw/
               Stream2::Role::Action::FastJob
               Stream2::Role::Action::InLogChunk
           /;
    sub build_context{ {}; }
    sub instance_perform{
        $log->info("Whatever");
        return 'wow';
    }
}

{
    my $complete_action;
    my $chunk_action;

    my @chunkids = ();
    $mdc->mock('put' , sub{
                   my ($class, $key, $value) = @_;
                   if( $key eq 'chunk' ){
                       $chunk_action = $action_sequence++;
                       push @chunkids , $value;
                   }
               });


    my $job = Qurious::Job->new({ class => 'Whatever' });

    my $mock_job = Test::MockModule->new( ref( $job ));
    $mock_job->mock( 'complete_job' , sub{
                         my ($self, $value) = @_;
                         $complete_action = $action_sequence++;
                         is( $value , 'wow');
                     });
    ok( my $action = My::Action->new({ jobid => 'blabla' }) );
    is( $action->instance_perform( $job ) , 'wow' );
    is_deeply( \@chunkids , [ 'blabla', undef ], "Ok good chunk ids");
    is( $complete_action , 1 );
    is( $chunk_action , 2 , "Chunk was stored after completing the job");
}

done_testing();

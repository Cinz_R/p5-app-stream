use Test::Most;
use Test::MockModule;

use Stream2;
use Stream2::Results::Result;
use Stream2::Action::WatchdogLoad;
use Stream::Templates::RestMock;
use Stream2::Criteria;

# This is not a definitive test for the action 'WatchdogLoad' and should be improved

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

my $stream2 = Stream2->new({ config_file => $config_file });
$stream2->stream_schema();
# Deploying schema
$stream2->stream_schema()->deploy();

my @results = (
    Stream2::Results::Result->new({
        email => 'cameron@downingsteeet.com',
        destination => 'RestMock',
        location_text => 'Tokyo',
        name => 'David Cameron'
    })
);

my $feed_mock = Test::MockModule->new( 'Stream::Templates::RestMock' );
$feed_mock->mock( search => sub {
    my $self = shift;
    $self->total_results( scalar( @results ) );
    return @results;
});
$feed_mock->mock( quota_guard => sub { pop->() } );

my $job = MyJob->new();

# results store that doesn't go to redis
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { $job->{results} = shift->results; } );
$results_mock->mock( fetch_results => sub { @results } );

# avoid some of the corollary actions
my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( increment_analytics => sub { 1; } );
$stream2_mock->mock( log_search_calendar => sub { 1; } );
$stream2_mock->mock( redis => sub { MyRedis->new(); } );

my $qurious_mock = Test::MockModule->new( 'Qurious' );
$qurious_mock->mock( create_job => sub {
    my ( $self, %stuff ) = @_;
    return MyJob->new();
});

my $criteria = Stream2::Criteria->new( schema => $stream2->stream_schema() );
$criteria->add_token( 'keywords' , 'manager');
$criteria->add_token('cv_updated_within' , '3M' );
$criteria->save();

# Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
    contact_email => 'restmock@tofu.co.uk',
    contact_name => 'Patrick',
    subscriptions_ref => {
        RestMock => {
            nice_name => 'Rest Board',
            auth_tokens => {}
        }
    }
});
my $user_id = $memory_user->save();

# Create a watchdog
ok( my $subscription = $memory_user->subscriptions()->find({ board => 'RestMock' }) , "Ok can get subscription via row user relationship");
ok( my $wd = $stream2->watchdogs->create({ user_id => $user_id , name => 'wow watchdog', criteria_id => $criteria->id() }) , "Ok can create watchdog");

$wd->add_to_watchdog_subscriptions( { subscription => $subscription } );

ok( my $wd_sub = $wd->watchdog_subscriptions()->search({ subscription_id => $subscription->id() })->first(), 'got a wd sub' );

$wd->discard_changes();
is( $wd->last_viewed_datetime() , undef );

ok( my $results = Stream2::Action::WatchdogLoad->perform( $job ), "performing task finishes" );

$wd->discard_changes();
ok( $wd->last_viewed_datetime() , "Ok now have a last viewed datatime");

ok $results->{search_record_id}, 'search record id is set for watchdogs';

# SEAR-1363
my $search_records = $stream2->factory('SearchRecord')->search({ board => 'RestMock' }, {});
ok $search_records->count == 1, 'Stored search record with board named \'RestMock\' for a watchdog';
ok( defined( $search_records->first()->n_results() ) , "n_results stored in the search record is defined");
is( $search_records->first()->remote_ip(), 'i_know_where_you_live' , "stored remote ip of user in search record");

{
    package MyJob;
    sub new { bless { stream2 => $stream2 }, shift; }
    sub complete_job { shift->{meta} = pop; }
    sub guid { int(rand( 100 )); }
    sub enqueue { 1; }
    sub parameters {
        return {
            options => { page => 1 },
            watchdog_id => $wd->id,
            template_name => 'RestMock',
            subscription_id => $subscription->id,
            remote_ip => 'i_know_where_you_live',
            ripple_settings => {},
            user => { user_id => $user_id },
        };
    }
}

{
    package MyRedis;
    sub rpush { 1; }
    sub expire { 1; }
    sub new { return bless {}; }
}

done_testing();

#! perl -w

use Test::More;
use Test::MockModule;

use Test::Stream2;
use Stream2::Results::Result;

use Scalar::Util;

use Log::Any::Adapter;
#Log::Any::Adapter->set('Stderr');

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();

# Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");

# Time to create a candidate
my $candidate = Stream2::Results::Result->new();
$candidate->candidate_id('1234');
$candidate->destination('adcresponses');
$candidate->name('Gerard Mensoif');
$candidate->email('gerard@mensoif.com');

{
    package My::Action;
    use Moose;
    extends qw/Stream2::Action/;
    has 'stream2' => ( is => 'ro' );
    has 'stream2_interactive' => ( is => 'ro', default => 0 );

    with qw/Stream2::Role::Action::HasCandidate/;
    sub instance_perform{
        return {};
    }
}

ok( my $action = My::Action->new({ stream2 => $stream2, stream2_interactive => 1, candidate_object => $candidate, user => { user_id => $user_id }, ripple_settings => {} }) );
ok( $action->candidate_object() );
ok( $action->user_object() );
my $res = $action->instance_perform();
ok( $res->{echo_candidate} , "Ok got echo_candidate in res");


{
    # Use the context to build a new action.
    my $context = $action->build_context();
    # Scrape the context from blessed things.
    foreach my $key ( keys( $context ) ){
        if( Scalar::Util::blessed( $context->{$key} ) ){
            delete $context->{$key};
        }
    }
    my $new_action = My::Action->new({ stream2 => $stream2, %{$context} });
    is( $new_action->candidate_object()->id() , 'adcresponses_1234' );
    is( $new_action->candidate_object()->email() , 'gerard@mensoif.com' );
    is( $new_action->candidate_object()->name(), 'Gerard Mensoif' );
    ok( $new_action->user_object() );
    my $res = $new_action->instance_perform();
    ok( ! $res->{echo_candidate} , "Ok no candidate as its an action built from a context, not an interactive one");
}

{
    my $advertapi_mock = Test::MockModule->new('Bean::AdcApi::Client');
    $advertapi_mock->mock(request_json => sub {
        return {
            Advert => {
                JobReference    => 'jobref1234',
                JobTitle        => 'Test title',
                AdvertID        => 1234,
                Consultant      => 'whatever',
                AplitrakID      => 'blabla',
            }
        };
    });
    $candidate->set_attr(broadbean_adcadvert_id => 1234);
    $candidate->set_attr(broadbean_adcboard_id => 999);
    my $new_action = My::Action->new({ stream2 => $stream2, candidate_object => $candidate, user => $memory_user->to_hash(), ripple_settings => {} });
    my $stash = $new_action->get_template_stash();
    note('test some advert details for adcresponse candidates come through');
    is($stash->{jobref}, 'jobref1234', 'jobref for adcresponse candidate was set with advert');
    is($stash->{jobtitle}, 'Test title', 'jobtitle for adcresponse candidate was set with advert');
}

done_testing();

#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Log::Any::Adapter;
#Log::Any::Adapter->set('Stderr');


{
    package My::Action;
    use Moose;
    with qw/Stream2::Role::Action::HasRemoteIP/;
    sub build_context{ {}; }
    sub instance_perform{
        return $ENV{REMOTE_ADDR};
    }
}

{
    ok( my $action = My::Action->new() );
    is(  $action->remote_ip() , '127.0.0.1' );
    is( $action->instance_perform() , '127.0.0.1' );
}

{
    ok( my $action = My::Action->new({ remote_ip => '123.456.789.101' }) );
    is(  $action->remote_ip() , '123.456.789.101' );
    is( $action->instance_perform() , '123.456.789.101' );
    is( $action->build_context()->{remote_ip} , $action->remote_ip() );
}

done_testing();

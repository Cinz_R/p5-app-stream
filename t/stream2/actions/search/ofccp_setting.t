#! perl -w
use strict;
use warnings;

use Test::Most;
use Test::MockModule;

use FindBin;
use Test::More;

use Stream2;
use Stream2::Results::Result;
use Stream2::Action::Search;
use Stream::Templates::dummy;
use Stream2::Criteria;
use Redis;

# This is not the definitive test for the search action and should be improved upon
# for example, testing the inputs to the feed

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}


my $redis = Redis->new(server => 'whatever', no_auto_connect_on_new => 1);
my $stream2 = Stream2->new({ config_file => $config_file, qurious => Qurious->new( redis => $redis ) });

# Deploying schema
$stream2->stream_schema()->deploy();

# blank feed which returns above results
my $feed_mock = Test::MockModule->new( 'Stream::Templates::dummy' );
$feed_mock->mock( search => sub { return } );
$feed_mock->mock( quota_guard => sub { pop->() } );

my %tokens;
$feed_mock->mock( token_value => sub {
    my ($self, $key, $value) = @_;
    $tokens{$key} = $value;
});

# results store that doesn't go to redis
my $job = MyJob->new();
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { 1; } );

# avoid some of the corollary actions
my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( increment_analytics => sub { 1; } );
$stream2_mock->mock( log_search_calendar => sub { 1; } );
$stream2_mock->mock( redis => sub { 1; } );

my $criteria = Stream2::Criteria->new( schema => $stream2->stream_schema() );
$criteria->add_token('ofccp_context' => 'GJ30PS2');
$criteria->add_token('eksempel' => 'verdig');
$criteria->save();

# Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
    contact_email => 'sausage@tofu.co.uk',
    contact_name => 'Patrick',
    subscriptions_ref => {
        sausage => {
            nice_name => 'Sausage Board',
            auth_tokens => {}
        }
    }
});
my $user_id = $memory_user->save();

my $qurious_mock = Test::MockModule->new( 'Qurious' );

ok(
    my $action = Stream2::Action::Search->new({
        criteria_id         => $criteria->id(),
        template_name       => 'sausage',
        stream2_base_url    => 'http://127.0.0.1',
        stream2             => $stream2,
        user                => { user_id => $user_id },
        ripple_settings     => {},
        log_chunk_capture   => 0,
        jobid               => 'doesnt matter',
        feed                => Stream::Templates::dummy->new(),
    }),
    'Created search action for OFCCP exclusion',
);

ok( $action->instance_perform( $job ), "OFCCP excluded task finishes" );

is($tokens{eksempel}[0], 'verdig', 'generic token used');
ok(!$tokens{ofccp_context}, 'ofccp_context token omitted');

my $setting = $stream2->factory('Setting')->create({
  setting_mnemonic    => 'behaviour-search-use_ofccp_field',
  setting_description => 'testdesc',
});

$stream2->factory('UserSetting')->create({
  user_id => $user_id,
  setting_id => $setting->id,
  admin_user_id => $memory_user->id,
  boolean_value => 1,
});

ok(
    $action = Stream2::Action::Search->new({
        criteria_id         => $criteria->id(),
        template_name       => 'sausage',
        stream2_base_url    => 'http://127.0.0.1',
        stream2             => $stream2,
        user                => { user_id => $user_id },
        ripple_settings     => {},
        log_chunk_capture   => 0,
        jobid               => 'doesnt matter either',
        feed                => Stream::Templates::dummy->new()
    }),
    'Created action for OCFFP inclusion'
);

# Clear the tokens from the previous action
%tokens = ();
ok( $action->instance_perform( $job ), "OFCCP included task finishes" );

is($tokens{eksempel}[0], 'verdig', 'generic token used');
ok($tokens{ofccp_context}, 'ofccp_context included');

done_testing();

{
    package MyJob;
    sub new { bless {}, shift; }
    sub complete_job { shift->{meta} = pop; }
    sub guid { int(rand( 100 )); }
    sub enqueue { 1; }
}

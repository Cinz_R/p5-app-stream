#! perl

use strict;
use warnings;

# use Carp::Always;

use FindBin;
use Test::More;
use Test::Exception;

use Data::UUID;

use Stream2;

use Log::Log4perl qw/:easy/;
Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log::Log4perl');


my $config_file = 't/app-stream2.conf';

my $template_name = 'bullhorn';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $ug = Data::UUID->new();

my $users_factory = $stream2->factory('SearchUser');

# A test user.
my $user = $users_factory->new_result({provider => 'adcourier',
                                       last_login_provider => 'adcourier',
                                       provider_id => 21024, # This is a bullhorn adcourier test user.
                                       group_identity => 'acme',
                                      });
$user->save();

ok( my $template = $stream2->build_template($template_name, undef , { user_object => $user } ) , "Ok got template");

my $tests = [
             [ ' kitten ( ' => q|isDeleted:0 AND ( ((companyName:kitten OR description:kitten OR fileAttachments.description:kitten OR id:kitten OR name:kitten OR occupation:kitten OR type:kitten) OR  id:e59a8214adfa11e4a5b9c7d4c8ae23a0 )  ) | ],
             [ '' => 'isDeleted:0' ],
             [ 'kitten' => q|isDeleted:0 AND ( (companyName:kitten OR description:kitten OR fileAttachments.description:kitten OR id:kitten OR name:kitten OR occupation:kitten OR type:kitten)  ) |],
             [ '"kitten manager"' => q|isDeleted:0 AND ( (companyName:"kitten manager" OR description:"kitten manager" OR fileAttachments.description:"kitten manager" OR id:"kitten manager" OR name:"kitten manager" OR occupation:"kitten manager" OR type:"kitten manager")  ) | ],
             [ 'kitten OR manager' => q|isDeleted:0 AND ( ((companyName:kitten OR description:kitten OR fileAttachments.description:kitten OR id:kitten OR name:kitten OR occupation:kitten OR type:kitten) OR (companyName:manager OR description:manager OR fileAttachments.description:manager OR id:manager OR name:manager OR occupation:manager OR type:manager))  ) | ],
             [ '(sale* OR vend*) AND technology NOT (computers OR cars)' => q|isDeleted:0 AND ( (-((companyName:computers OR description:computers OR fileAttachments.description:computers OR id:computers OR name:computers OR occupation:computers OR type:computers) OR (companyName:cars OR description:cars OR fileAttachments.description:cars OR id:cars OR name:cars OR occupation:cars OR type:cars)) AND (((companyName:sale* OR description:sale* OR fileAttachments.description:sale* OR id:sale* OR name:sale* OR occupation:sale* OR type:sale*) OR (companyName:vend* OR description:vend* OR fileAttachments.description:vend* OR id:vend* OR name:vend* OR occupation:vend* OR type:vend*)) AND (companyName:technology OR description:technology OR fileAttachments.description:technology OR id:technology OR name:technology OR occupation:technology OR type:technology)))  ) | ],
            ];

foreach my $test ( @$tests ){
    is( $template->bullhorn_expand_query_string('isDeleted:0' , $test->[0]), $test->[1] , "Ok good expansion of '".$test->[0]."'");
}


# Tests from ali:
{
    my $ali_tests = [
                     {
                      input   =>  'C#',
                      output  =>  q|( (companyName:C# OR description:C# OR fileAttachments.description:C# OR id:C# OR name:C# OR occupation:C# OR type:C#)  ) |
                     },
                     {
                      input   =>  'You, me, with, cheese',
                      output  =>  q|( ((companyName:You OR description:You OR fileAttachments.description:You OR id:You OR name:You OR occupation:You OR type:You) OR (companyName:me OR description:me OR fileAttachments.description:me OR id:me OR name:me OR occupation:me OR type:me) OR (companyName:with OR description:with OR fileAttachments.description:with OR id:with OR name:with OR occupation:with OR type:with) OR (companyName:cheese OR description:cheese OR fileAttachments.description:cheese OR id:cheese OR name:cheese OR occupation:cheese OR type:cheese))  ) |
                     },
                     {
                      input   =>  '"Project Manager", cheesecake',
                      output  =>  q|( ((companyName:"Project Manager" OR description:"Project Manager" OR fileAttachments.description:"Project Manager" OR id:"Project Manager" OR name:"Project Manager" OR occupation:"Project Manager" OR type:"Project Manager") OR (companyName:cheesecake OR description:cheesecake OR fileAttachments.description:cheesecake OR id:cheesecake OR name:cheesecake OR occupation:cheesecake OR type:cheesecake))  ) |
                     },
                     {
                      input   =>  '"Pimp" with cheesecake',
                      output  =>  q|( ((companyName:"Pimp" OR description:"Pimp" OR fileAttachments.description:"Pimp" OR id:"Pimp" OR name:"Pimp" OR occupation:"Pimp" OR type:"Pimp") OR (companyName:with OR description:with OR fileAttachments.description:with OR id:with OR name:with OR occupation:with OR type:with) OR (companyName:cheesecake OR description:cheesecake OR fileAttachments.description:cheesecake OR id:cheesecake OR name:cheesecake OR occupation:cheesecake OR type:cheesecake))  ) |
                     },
                     {
                      input   =>  'cheesecake with "rabies"',
                      output  =>  q|( ((companyName:cheesecake OR description:cheesecake OR fileAttachments.description:cheesecake OR id:cheesecake OR name:cheesecake OR occupation:cheesecake OR type:cheesecake) OR (companyName:with OR description:with OR fileAttachments.description:with OR id:with OR name:with OR occupation:with OR type:with) OR (companyName:"rabies" OR description:"rabies" OR fileAttachments.description:"rabies" OR id:"rabies" OR name:"rabies" OR occupation:"rabies" OR type:"rabies"))  ) |
                     },
                     {
                      input   =>  '"Team foundation Server" and Administrator',
                      output  =>  q|( ((companyName:"Team foundation Server" OR description:"Team foundation Server" OR fileAttachments.description:"Team foundation Server" OR id:"Team foundation Server" OR name:"Team foundation Server" OR occupation:"Team foundation Server" OR type:"Team foundation Server") OR (companyName:and OR description:and OR fileAttachments.description:and OR id:and OR name:and OR occupation:and OR type:and) OR (companyName:Administrator OR description:Administrator OR fileAttachments.description:Administrator OR id:Administrator OR name:Administrator OR occupation:Administrator OR type:Administrator))  ) |
                  },
                ];

    foreach my $test ( @$ali_tests ){
        is( $template->bullhorn_expand_query_string( 'BLA' , $test->{input} ), 'BLA AND '.$test->{output} , "Ok good expansion of '".$test->{input}."'");
    }
}

{
    # A nasty query (SEAR-1199)
    my $query = '"accounts payable" OR accounts receivable" OR "payroll"';
    eval{ $template->bullhorn_expand_query_string('BLA' , $query ); };
    my $err = $@;
    like( $err->to_string() , qr/Please check quotes/ );
}


done_testing();

#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;

use DateTime;

use Log::Any::Adapter;

use Email::Simple;

use CHI;

# Log::Any::Adapter->set('Stderr');

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';

    plan skip_all => 'CV lib account may be in use' if $ENV{ TEST_ONLINE };
}

use LWP;
use LWP::UserAgent::Mockable;

use Stream2;
use Stream2::Criteria;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}


ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

$stream2->location_api()->chi( CHI->new(driver => 'Null') );
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();


# Import the andy company watchdogs

#$stream2->watchdogs->import_company('andy');

{
    ## Note: We need to load this before monkey patching
    ok( $stream2->watchdogs() , "Ok can load factory");
    ok( $stream2->stream_api(), , "Ok can load stream API");
    ok( !  $stream2->watchdogs()->count() , "Ok no watchdog");

    # Mock enqueing a run to intialise.
    no warnings;
    *Stream2::DBICWrapper::Watchdog::enqueue_watchdog_run= sub{
        my ($self, $watchdog, $params) = @_;
        is( $params->{real_run} , 0 , "Ok got blabla as real_run param");
    };

    ## Shortcut to the real thing.. We are testing the feature,
    ## not qurious
    *Stream2::DBICWrapper::WatchdogResult::enqueue_backfill_from
      = *Stream2::DBICWrapper::WatchdogResult::backfill_from;

    *Bean::Juice::APIClient::User::get_user = sub{
        return { timezone => 'Europe/Paris',
                 locale => 'fr'
               }
    };

    *Bean::Juice::APIClient::Stream::get_candidate = sub{
        my ($self, $candidate_id) = @_;

        if( $candidate_id =~ 2 ){
            die("ARGGGGGG SOME HORROR HAS OCCURED");
        }
        return { some_funky_properties => 'CANDIDATE '.$candidate_id };

    };
    use warnings;
    ## End of monkey patchin'


    my @deliveries = Email::Sender::Simple->default_transport->deliveries;

    is( scalar(@deliveries)  , 0 ,  "Ok NO Email delivered");

    is( $stream2->factory('SearchUser')->search({ last_login_provider => 'woopwoop'})->count() , 0 , "Ok no woopwoop users");

    $stream2->watchdogs->import_company('manpoweruk' , sub { return scalar( grep { $_[0]->{id} eq $_ }  qw/133823 6557 6558/); } , 'https://www.example.com' , { provider => 'woopwoop'} ) ;
    cmp_ok( $stream2->factory('SearchUser')->search({ last_login_provider => 'woopwoop'})->count() , '>' , 0 , "Ok some woopwoop users now");


    @deliveries = Email::Sender::Simple->default_transport->deliveries;

    cmp_ok( scalar(@deliveries)  , '>=', 1  ,  "Ok at least one Email delivered");


    $stream2->factory('WatchdogResult')->search(undef , { order_by => [ qw/watchdog_id subscription_id candidate_id/ ] })
      ->loop_through(sub{
                         my $result = shift;
                         # diag(Dumper($result->content()));
                     });

    my $has_failed = 0;
    my $has_succeeded = 0;


    foreach my $watchdog ( $stream2->watchdogs->all() ){
        # diag("GOT WATCHDOG ".$watchdog->name() . " ID = ".$watchdog->id());
        foreach my $wd_subscription ( $watchdog->watchdog_subscriptions()->all() ){
            # diag("  - SUBSCRIPTION ".$wd_subscription->subscription->board());
            # diag("      N RESULTS: ".$wd_subscription->watchdog_results()->count());
            my $results = $wd_subscription->watchdog_results();
            while( my $result = $results->next() ){
                ## Check the result has got either an v1_candidate_id AND and v1_backfill_error OR NONE OF THAT.
                my $content = $result->content();
                unless( ( $content->{v1_candidate_id} && $content->{v1_backfill_error} ) ||
                        ( ! $content->{v1_candidate_id} && ! $content->{v1_backfill_error} ) ){
                    fail("Ok candidate ".join("-", $result->id())." V1 backfilling state is consistent");
                }

                if( $content->{v1_backfill_error} ){
                    $has_failed = 1;
                }
                else{
                    $has_succeeded = 1;
                }
            }
        }
    }
    ok( $has_failed , "Result backfilling from v1 has failed at least once");
    ok( $has_succeeded , "Result backfilling form 1 has failed at least once");
}


ok( $stream2->stream_schema->resultset('Watchdog')->count(), "OK some watchdogs were created");
is( $stream2->stream_schema->resultset('Watchdog')->first()->base_url(), 'https://www.example.com',  "OK some watchdogs were created");
ok( $stream2->stream_schema->resultset('WatchdogSubscription')->count() , "Ok some watchdog subscriptions were created");
ok( $stream2->factory('WatchdogResult')->count(), "Ok some watchdog results were created");

use Data::Dumper;



foreach my $wd ( $stream2->stream_schema->resultset('Watchdog')->search(undef, { prefetch => 'user' } )->all() ){
    my $user = $wd->user();
    my $london_next_run = $wd->next_run_datetime()->clone()->set_time_zone('Europe/London');
    is( $london_next_run->hour() , 1 , "In Paris, when it's 2am, it's 1 am IN LONDON, because Paris and London both have summer time");
    ok( $user->contact_email() , "Ok user has got an email address");
    is( $user->timezone() , 'Europe/Paris' , "Ok user has got a timezone");
    is( $user->locale() , 'fr'  , "Ok user has got a language locale");
}

LWP::UserAgent::Mockable->finished();

done_testing();

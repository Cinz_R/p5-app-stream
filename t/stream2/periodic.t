#! perl -w
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;

use Test::Stream2;

use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');


my $stream2 = Test::Stream2->new();
$stream2->deploy_test_db();

# Try a simple periodic.
my $n_runs = 3;
$stream2->periodic->run({ finish_if => sub{ --$n_runs == 0 } });

ok( ! $n_runs , "Ok no runs are left");

done_testing();

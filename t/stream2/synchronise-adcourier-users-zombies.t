use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;
use Qurious;
use Data::Dumper;
use Stream2::Action::SynchroniseAdCourierUserSiblings;


my $t = Test::Mojo::Stream2->new();
$t->login_ok;
my $stream2 = $t->app->stream2;

my $user_api_mock = Test::MockModule->new( 'Bean::Juice::APIClient::User' );
my $users_factory = $stream2->factory('SearchUser');

my $logged_in_user = $users_factory->first();
$logged_in_user->provider_id( 1 ); $logged_in_user->save;
my $company = $logged_in_user->group_identity;

my $api_users = [
    {
        id => 10,
        name => "Johnny Winter",
        email => 'johnny.winter@example.com',
        company => $company,
        office => 'sausages',
    },
    {
        id => 11,
        name => "Muddy Waters",
        email => 'muddy.waters@example.com',
        company => $company,
        office => 'sausages'
    },
];

## add normal user
my $u1 = $users_factory->new_result({
        'contact_details'   => {
            contact_name    => "doesnt matter",
            contact_email   => "doesnt matter"
        },
        'provider'          => 'adcourier',
        'last_login_provider' => 'adcourier',
        'provider_id'       => 10,
        'group_identity'    => 'pat',
        'group_nice_name'   => 'pat',
        'settings'          => {
        },
        'subscriptions_ref' => {
            board1 => {
                nice_name => "Board1",
                auth_tokens => {},
                type => "social"
            },
            board2 => {
                nice_name => 'Board2',
                auth_tokens => {},
                type => 'external',
            },
        },
});
$u1->save;

## add user wot will be a zombie
my $u2 = $users_factory->new_result({
        'contact_details'   => {
            contact_name    => "doesnt matter",
            contact_email   => "doesnt matter"
        },
        'provider'          => 'adcourier',
        'last_login_provider' => 'adcourier',
        'provider_id'       => 11,
        'group_identity'    => 'pat',
        'group_nice_name'   => 'pat',
        'settings'          => {},
        'subscriptions_ref' => {
            board1 => {
                nice_name => "Board1",
                auth_tokens => {},
                type => "social"
            },
            board2 => {
                nice_name => 'Board2',
                auth_tokens => {},
                type => 'external',
            },
        },
});
$u2->save;

my $schema = $stream2->stream_schema();

# two different criterias
my $criteria1 = Stream2::Criteria->new( schema => $schema );
$criteria1->add_token( 'keywords' , 'manager');
$criteria1->add_token('cv_updated_within' , '3M' );
$criteria1->save();
my $criteria2 = Stream2::Criteria->new( schema => $schema );
$criteria2->add_token( 'keywords' , 'developer');
$criteria2->add_token('cv_updated_within' , '3M' );
$criteria2->save();

my $u1s1 = $u1->subscriptions()->find({ board => 'board1'});
my $u1s2 = $u1->subscriptions()->find({ board => 'board2'});
my $u2s1 = $u2->subscriptions()->find({ board => 'board1'});
my $u2s2 = $u2->subscriptions()->find({ board => 'board2'});

map {
    _create_watchdog( $stream2, @$_ );

} (
    # two watchdogs per user
    [ $u1->id, 'u1wd1', $criteria1->id(), [ $u1s1, $u1s2 ] ],
    [ $u1->id, 'u1wd2', $criteria2->id(), [ $u1s1, $u1s2 ] ],
    [ $u2->id, 'u2wd1', $criteria1->id(), [ $u2s1, $u2s2 ] ],
    [ $u2->id, 'u2wd2', $criteria2->id(), [ $u2s1, $u2s2 ] ]
);

$user_api_mock->mock( company_users => sub { return @$api_users; } );
my $SyncAdCusersibs = Stream2::Action::SynchroniseAdCourierUserSiblings->new(
                                            stream2 => $stream2,
                                            jobid   => 'doesnt matter',
                                            adc_id  => 1,
                                            company => 'pat'
                                            );

is( $u1->watchdogs->search({ active => 1 })->count(), 2, "user 1 has 2 active watchdogs" );
is( $u2->watchdogs->search({ active => 1 })->count(), 2, "user 2 has 2 active watchdogs" );

ok( $SyncAdCusersibs->instance_perform( ), "job ran successfully" );

is( $u1->watchdogs->search({ active => 1 })->count(), 2, "user 1 is still active after sync" );
is( $u2->watchdogs->search({ active => 1 })->count(), 2, "user 2 is still active after sync" );

# zombify user 2!
$api_users->[1]->{office} = 'zombie';

ok( $SyncAdCusersibs->instance_perform( ), "job ran successfully" );

is( $u1->watchdogs->search({ active => 1 })->count(), 2, "user 1 is still active" );
is( $u2->watchdogs->search({ active => 1 })->count(), 0, "user 2 is a zombie and wd have been deactivated" );

sub _create_watchdog {
    my ( $s2, $uid, $name, $cid, $subs ) = @_;
    my $wd = $s2->watchdogs->create({ user_id => $uid, name => $name, criteria_id => $cid, base_url => 'http://www.example.com/' });
    map { $wd->add_to_watchdog_subscriptions({ subscription => $_ }); } @$subs;
}

{
    package MyJob;
    use base 'Qurious::Job';
    sub enqueue {1;}
    sub save {1;}
}

done_testing();

use Test::Most;

use Test::MockModule;
use DateTime;
use Email::Simple;
use Test::Stream2;
use Stream2::Criteria;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();

my @results = (
    Stream2::Results::Result->new({
        candidate_id => 1,
        email => 'cameron@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Tokyo',
        name => 'David Cameron'
    }),
    Stream2::Results::Result->new({
        candidate_id => 2,
        email => 'may@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Djibouti',
        name => 'Theresa May'
    }),
    Stream2::Results::Result->new({
        candidate_id => 3,
        email => 'brown@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Sandringham',
        name => 'Gordon Brown'
    }),
    Stream2::Results::Result->new({
        candidate_id => 4,
        email => 'jezza@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Woodstock',
        name => 'Jeremy Corbyn'
    })
);

my $feed_mock = Test::MockModule->new( 'Stream::Templates::twitter_xray' );
$feed_mock->mock( search => sub {
    my $self = shift;
    $self->total_results( scalar( @results ) );
    return @results;
});
$feed_mock->mock( quota_guard => sub { pop->() } );

# results store that doesn't go to redis
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { shift->results; } );
$results_mock->mock( fetch_results => sub { @results } );

my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( increment_analytics => sub { 1; } );
$stream2_mock->mock( log_search_calendar => sub { 1; } );
$stream2_mock->mock( redis => sub { MyRedis->new(); } );

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser') , "Ok can get resultset");

my $schema = $stream2->stream_schema();

# Build an empty criteria
my $criteria = Stream2::Criteria->new( schema => $schema );

$criteria->add_token( 'keywords' , 'manager');
$criteria->add_token('cv_updated_within' , '3M' );

$criteria->save();



my $users_factory = $stream2->factory('SearchUser');

# Time to create a user.
my $user_id;
my $memory_user;
{
    $memory_user = $users_factory->new_result({  contact_email => 'woodstock@peanuts.com',
                                                 provider => 'dummy',
                                                 last_login_provider => 'dummy',
                                                 provider_id => 31415,
                                                 group_identity => 'acme',
                                                 subscriptions_ref => {
                                                                       flickr_xray => {
                                                                                       nice_name => "Flickr",
                                                                                       auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
                                                                                       type => "social",
                                                                                       allows_acc_sharing => 0,
                                                                                      },
                                                                       'whatever' => {
                                                                                      nice_name => 'Whatever',
                                                                                      auth_tokens => { whatever_login => 'Boudin', whatever_passwd => 'Blanc' },
                                                                                      type => 'social',
                                                                                     }
                                                                      }
                                              });
    ok( $user_id = $memory_user->save() , "Ok can save this user");
    is( $schema->resultset('CvSubscription')->search({ user_id => $user_id })->count() , 2 , "Ok two subscriptions in the DB");

    ok( ! $schema->resultset('CvSubscription')->search({ board => 'flickr_xray'} , { prefetch => 'board_object' } )->first()->board_object()->allows_acc_sharing() , "Ok saved the right Account sharing setting (flickr not allow)");
    ok( $schema->resultset('CvSubscription')->search({ board => 'whatever'})->first()->board_object->allows_acc_sharing() , "Ok saved the right Account sharing setting (whatever is allowed)");
}

ok( my $flickr = $memory_user->subscriptions()->find({ board => 'flickr_xray' }) , "Ok can get subscription via row user relationship");
is($memory_user->watchdog_email_address(), 'woodstock@peanuts.com', 'the users email address is woodstock@peanuts.com');

# Create the same user
{
    $memory_user = $users_factory->find({
                                         provider => 'dummy',
                                         provider_id => 31415,
                                        });
    $memory_user->subscriptions_ref({
                                     flickr_xray => {
                                                     nice_name => "Flickr",
                                                     auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
                                                     type => "social",
                                                     allows_acc_sharing => 0,
                                                    },
                                     'someother' => {
                                                     nice_name => 'someother',
                                                     auth_tokens => { whatever_login => 'Boudin', whatever_passwd => 'Blanc' },
                                                     type => 'social'
                                                    }
                                    }
                                   );

    is( $memory_user->save() , $user_id , "Ok can save this user with the same user id");
    is( $schema->resultset('CvSubscription')->search({ user_id => $user_id })->count() , 2 , "Ok two subscriptions in the DB Only (whatever is gone, someother is new)");
}


is( $memory_user->subscriptions()->find({ board => 'flickr_xray' })->id() , $flickr->id(),  "Ok flickr subscription ID did not change");
ok( my $someother = $memory_user->subscriptions()->find({ board => 'someother' }) , "Ok can get some other subscription");

is( $memory_user->watchdogs()->count() , 0 , "Ok this user has no watchdogs");


ok( $stream2->watchdogs() , "Can get watchdogs factory");

ok( $memory_user->can_add_watchdog() , "Ok user can add watchdogs");

ok( my $snoopy = $stream2->watchdogs->create({ user_id => $user_id , name => 'Snoopy and a very long name that will probably be truncated in the email subject', criteria_id => $criteria->id() }) , "Ok can create watchdog");

$snoopy->add_to_watchdog_subscriptions( { subscription => $flickr } );
$snoopy->add_to_watchdog_subscriptions( { subscription => $someother } );

is( $snoopy->subscriptions()->count() , 2 , "Ok snoopy has got two subscriptions");

{
    ## Re-import the user without the 'someother'. It should go away from the watchdog just fine.
    $memory_user = $users_factory->find({     provider => 'dummy',
                                              provider_id => 31415
                                        });
    $memory_user->subscriptions_ref({
                                     flickr_xray => {
                                                     nice_name => "Flickr",
                                                     auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
                                                     type => "social",
                                                    },
                                     'reed' => {
                                                      'nice_name' => 'Reed',
                                                      'auth_tokens' => {
                                                                        'reed_username' => 'aliz@broadbean.com',
                                                                        'reed_posting_key' => '98fac8fd-8920-42a6-8d3a-5bcdd7b9c7baWRONG'
                                                                       },
                                                      'type' => 'external',
                                                      'board_id' => '294',
                                                      'eza_number' => 314159,
                                                      allows_acc_sharing => 0,
                                                     },
                                     'iprofile' => {
                                                      'nice_name' => 'I-Profile',
                                                      'auth_tokens' => {
                                                                        'iprofile_demo_username' => 'username',
                                                                        'iprofile_demo_password' => 'password',
                                                                        'iprofile_demo_email' => 'recruiter_email'
                                                                       },
                                                      'iprofile_demo_security_key' => 'a_key',
                                                      'type' => 'external',
                                                      'board_id' => '479',
                                                      'eza_number' => 314159,
                                                      allows_acc_sharing => 0,
                                                     },
                                    });
    $memory_user->save();
}

ok( $memory_user->subscriptions_ref() , "Ok can get a subscriptions_ref from this user");
ok( my $reed = $memory_user->subscriptions()->find({ board => 'reed' }) , "Ok can get british medical subscription");


# Load the user again from memory and get its subscriptions_ref
{
    my $loaded_again = $users_factory->find($memory_user->id() );

    ok( $loaded_again->subscriptions_ref() , "Ok can load the subscription refs of this user");
    ok( $loaded_again->subscriptions_ref()->{reed}->{eza_number} , "Ok got eza_number for reed");
    is( $loaded_again->subscriptions_ref()->{reed}->{allows_acc_sharing} , 0 , "Ok good allows account sharing options");
}


is( $snoopy->subscriptions()->count() , 1 , "Ok snoopy has got one subscription only");

ok( $stream2->watchdogs->create({ user_id => $user_id , name => "Santa's little helper", criteria_id => $criteria->id(), base_url => 'http://www.example.com/' }) , "Ok can create watchdog");

ok( my $scoobidoo = $stream2->watchdogs->create({ user_id => $user_id , name => 'Scoobidoo and a very long name that will probably be truncated in the email subject', criteria_id => $criteria->id(),  base_url => 'http://www.example.com/' }) , "Ok can create watchdog");


ok( $stream2->watchdogs->create({ user_id => $user_id , name => 'Rantanplan', criteria_id => $criteria->id() }) , "Ok can create watchdog");

is( $scoobidoo->base_url() , 'http://www.example.com/', "Ok good base URL");

$scoobidoo->add_to_watchdog_subscriptions({ subscription => $flickr });
$scoobidoo->add_to_watchdog_subscriptions({ subscription => $reed });


ok( ! $scoobidoo->next_run_datetime() , "No next run datetime until initialized");

# Try to initialize it.
$stream2->watchdogs()->runner($scoobidoo)->initialize();



ok( $scoobidoo->next_run_datetime() , "Ok not we have a datetime");
ok( $scoobidoo->next_run_datetime()->subtract_datetime( DateTime->now() )->is_positive() , "Ok next run date is in the future");

ok( $scoobidoo->watchdog_subscriptions()->search({ subscription_id => $flickr->id() })->first()->last_run_datetime() , "Ok got last run datetime");
ok( ! $scoobidoo->watchdog_subscriptions()->search({ subscription_id => $flickr->id() })->first()->last_run_error() , "No error for flickr");
ok( $scoobidoo->watchdog_subscriptions()->search({ subscription_id => $reed->id() })->first()->last_run_error() , "Ok error for reed");

ok( my $iprofile_sub = $memory_user->subscriptions()->find({ board => 'iprofile' }) , "Ok can get iprofile subscription");
ok( my $iprofile_wd = $stream2->watchdogs->create({ user_id => $user_id , name => 'WD_with_iprofile' , criteria_id => $criteria->id(),  base_url => 'http://www.example.com/' }) , "Ok can create watchdog");

$iprofile_wd->add_to_watchdog_subscriptions({ subscription => $iprofile_sub });

ok( ! $iprofile_wd->next_run_datetime() , "No next run datetime until initialized (iprofile)");

# Try to initialize it.
$stream2->watchdogs()->runner($iprofile_wd)->initialize();

ok( $iprofile_wd->next_run_datetime() , "Ok not we have a datetime (iprofile)");
ok( $iprofile_wd->next_run_datetime()->subtract_datetime( DateTime->now() )->is_positive() , "Ok next run date is in the future and should be 5am (iprofile)");
isa_ok( $iprofile_wd->next_run_datetime()->time_zone() , 'DateTime::TimeZone::UTC');
is( $iprofile_wd->next_run_datetime()->clone()->set_time_zone('Europe/London')->hour, 5 , "Ok next run date is at 5am london time (iprofile)");


# Now try to initialize again with some correct credentials for reed.
{
    ## Re-import the user without the 'someother'. It should go away from the watchdog just fine.
    $memory_user = $users_factory->find({     provider => 'dummy',
                                              provider_id => 31415
                                        });
    $memory_user->contact_details({ contact_email => 'zorro@example.com',
                                    feedback_email => 'bernardo@example.com', # Zorro is too busy
                                    contact_name  => 'Zorro'."\N{U+262D}",
                                    locale => 'fr', # Zorro is french
                                  });

    $memory_user->subscriptions_ref({
                                     flickr_xray => {
                                                     nice_name => "Flickr",
                                                     auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
                                                     type => "social"
                                                    },
                                     'reed' => {
                                                      'nice_name' => 'Reed',
                                                      'auth_tokens' => {
                                                                        'reed_username' => 'aliz@broadbean.com',
                                                                        'reed_posting_key' => '98fac8fd-8920-42a6-8d3a-5bcdd7b9c7ba'
                                                                       },
                                                      'type' => 'external',
                                                      'board_id' => '294',
                                                      allows_acc_sharing => 0,
                                                     },
                                    }
                                   );
    $memory_user->save();
}


$stream2->watchdogs()->runner($scoobidoo)->initialize();

ok( $scoobidoo->watchdog_subscriptions()->search({ subscription_id => $flickr->id() })->first()->last_run_datetime() , "Ok got last run datetime");
ok( ! $scoobidoo->watchdog_subscriptions()->search({ subscription_id => $flickr->id() })->first()->last_run_error() , "No error for flickr");
ok( ! $scoobidoo->watchdog_subscriptions()->search({ subscription_id => $reed->id() })->first()->last_run_error() , "No error for reed");

foreach my $subscription ( $scoobidoo->watchdog_subscriptions()->all ){
    ok( $subscription->watchdog_results()->count(), "Subscription has results" );
    ok( $subscription->watchdog_results->first()->viewed(), "result is set as viewed" );
}


# Now we need to run this watchdog and see that an email has been sent.
# Fiddle twith the subscription results so two of each are new.
foreach my $subscription ( $scoobidoo->watchdog_subscriptions()->all ){
    $subscription->watchdog_results()->search(undef, { rows => 2 })->delete_all();
    ok( $subscription->watchdog_results()->count() , "We still have some results");
}


# Reload scoobidoo. This will make sure its associated user has got an email
$scoobidoo = $stream2->factory('Watchdog')->find($scoobidoo->id());

is( $scoobidoo->user()->locale() , 'fr' );

# Run the watchdog and check it sends an email
$stream2->watchdogs()->runner($scoobidoo)->run();


my @deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries)  , "1" ,  "Ok one email got delivered");
is( $deliveries[0]->{email}->get_header('X-Stream2-Transport') , 'static_transport' );

{
    my $string_email = $deliveries[0]->{email}->as_string();

    like( Encode::decode('MIME-Header' , $deliveries[0]->{email}->get_header('Subject') ) , qr/\[Watchdog\] Your watchdog "Scoobidoo and a very long name that will probably be trun\.\.\." Found \d+ new candidates/ );
    like( $string_email , qr/To: bernardo\@example.com/  , "Email to bernardo");
    like( $string_email , qr/Bonjour/ , "Email contains the french version of Hello {name} (new style translation with i18nsx)");
    like( $string_email , qr/Salaire/  , "Email contains a french word (this is bean::locale style translation with L)");
    like( $string_email , qr/nous avons trouv/  , "Email contains a french version of the french (this is new style translation)");
    like( $string_email , qr/nouveaux candidats/  , "Email contains a french version of the french (this is new style translation)");
}


# Reload scoobidoo. We dont want old stuff to stick in memory
$scoobidoo = $stream2->factory('Watchdog')->find($scoobidoo->id());

# Run it again right now, check that no email is delivered again
$stream2->watchdogs()->runner($scoobidoo)->run();
@deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries)  , "1" ,  "No new email has been delivered.");

# Change the user credentials to some rubbish ones. Check a new email containing
# some errors have been delivered.
{
    ## Re-import the user without the 'someother'. It should go away from the watchdog just fine.
    $memory_user = $users_factory->find({
                                              provider => 'dummy',
                                              provider_id => 31415,
                                         });
    $memory_user->subscriptions_ref({
                                     flickr_xray => {
                                                     nice_name => "Flickr",
                                                     auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
                                                     type => "social"
                                                    },
                                     'reed' => {
                                                      'nice_name' => 'reed',
                                                      'auth_tokens' => {
                                                                        'reed_username' => 'aliz@broadbean.com',
                                                                        'reed_posting_key' => '98fac8fd-8920-42a6-8d3a-5bcdd7bWRONG'
                                                                       },
                                                      'type' => 'external',
                                                      'board_id' => '294',
                                                      allows_acc_sharing => 0,
                                                     },
                                    },
                                );
    $memory_user->save();
}

# Make sure to reload scoobidoo
$scoobidoo = $stream2->factory('Watchdog')->find($scoobidoo->id());

{
    # Make sure we can export scoobidoo (mock the actual posting).
    my $exported = 0;
    no strict;
    no warnings;
    *{'Bean::Juice::APIClient::User::post_watchdog_toV1'} = sub{ $exported++; return { cv_watchdog_id => -1 }; };
    use strict;
    use warnings;
    $scoobidoo->user()->provider('adcourierFAKE');
    ok( $stream2->watchdogs->export_tov1( $scoobidoo ) , "Ok can export scoobidoo to v1");
    ok( $exported , "Ok watchdog was exported");
}



# Scoobidoo is back to its dummy provider
$scoobidoo->user()->provider('dummy');

$stream2->watchdogs()->runner($scoobidoo)->run();
@deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries)  , "2" ,  "There was a second delivery");
like( $deliveries[1]->{email}->as_string() , qr/produite lors de la recherche/ , "Ok this second email contain something about some error");

{
    # Check that if we set the right date, due watchdogs finds scoobidoo
    ok( ! $stream2->watchdogs()->fire_due_watchdogs() );

    $scoobidoo->next_run_datetime(DateTime->now());
    $scoobidoo->update();


    my $queued_runs = 0;

    {
        # Monkey patch so we dont actually enqueue anything
        no strict;
        no warnings;
        *{'Stream2::DBICWrapper::Watchdog::enqueue_watchdog_run'} = sub{ $queued_runs++; };
        use strict;
        use warnings;
    }

    is( $stream2->watchdogs()->fire_due_watchdogs() , 1 , "Ok fired one watchdog");
    ok( $queued_runs , "Ok queued runs is alright");
}

{
    my $four_months_ago = DateTime->now()->subtract( months => 4 );

    # Fiddle with watchdog results so clearing the watchdog results would work.
    ok( $stream2->factory('WatchdogResult')->count() , "Ok got some results");
    ok( $stream2->factory('WatchdogResult')->dbic_rs->update({ last_seen_datetime => $four_months_ago }));
    ok( $stream2->factory('WatchdogResult')->clean_old_results() , "OK can clean old results");
    ok(! $stream2->factory('WatchdogResult')->count() , "No results left after the cleanup");

}



{
    $memory_user->group_identity( 'khr' );
    ok( $memory_user->can_add_watchdog() , "KHR user is subject to their own limit as a minimum" );
    $memory_user->group_identity( 'acme' );
}

# $user_id has 5 watchdogs to this point. Create 6 more. Test the limit of 10.
for (1 .. 5) {
    ok(
        $stream2->watchdogs->create( {
            user_id     => $user_id ,
            name        => 'molecular monkey',
            criteria_id => $criteria->id(),
            base_url    => 'http://www.example.com/'
        } ),
        'created watchdog ok'
    );
}
ok( ! $memory_user->can_add_watchdog() , "Ok user cannot add watchdogs anymore");
dies_ok {  $stream2->watchdogs->create({ user_id => $user_id , name => 'Rantanplan', criteria_id => $criteria->id() }); } 'Ok dies creating a 10th wathdog';

$scoobidoo->delete();

ok( $memory_user->can_add_watchdog() , "Ok user can now add another watchdog");

$memory_user->delete();

is( $schema->resultset('Watchdog')->count() , 0 , "Ok no watchdog left in the DB following a user delete");

done_testing();

{
    package MyRedis;
    sub rpush { 1; }
    sub expire { 1; }
    sub new { return bless {}; }
}

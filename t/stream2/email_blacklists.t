#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;

use Stream2;

use Email::Simple;

# to deploy email_blacklist table
use Test::Mojo::Stream2;
my $t = Test::Mojo::Stream2->new();
$t->login_ok;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

ok( my $stream2 = $t->app->stream2, "Ok can build stream2" );

my $ebl = $stream2->factory('EmailBlacklist');

my @tests = (
    {   test_name         => 'Blacklisted email in TO',
        blacklisted_emails => ['jerome_spam@broadbean.com'],
        purpose           => 'delivery.spam',
        email             => {
            header => [
                To      => 'jerome_spam@broadbean.com',
                From    => 'pass_1@broadbean.com',
                Subject => 'Boudin blanc',
                Bcc     => 'pass_2@broadbean.com ; pass_3@broadbean.com',
            ],
            body => 'Some message sent'
        }
    },
    {   test_name         => 'Blacklisted email in BCC',
        blacklisted_emails => ['jerome_BCC_spam@broadbean.com'],
        purpose           => 'delivery.spam',
        email             => {
            header => [
                To      => 'pass_1@broadbean.com',
                From    => 'whatever@broadbean.com',
                Subject => 'Boudin blanc',
                Bcc => 'jerome_BCC_spam@broadbean.com ; pass_2@broadbean.com',
            ],
            body => 'Some message sent'
        }
    },
    {   test_name => 'Blacklisted email in TO and BCC',
        blacklisted_emails =>
            [ 'spam_to_two@broadbean.com', 'spam_bcc_two@broadbean.com' ],
        purpose => 'delivery.spam',
        email   => {
            header => [
                To      => 'spam_to_two@broadbean.com',
                From    => 'whatever@broadbean.com',
                Subject => 'Boudin blanc',
                Bcc => 'spam_bcc_two@broadbean.com ; pass_2@broadbean.com',
            ],
            body => 'Some message sent'
        }
    }
);

foreach my $test (@tests) {

    my $email = Email::Simple->create( %{ $test->{email} } );

    ok( $stream2->send_email($email), 'should send' );

    # create a black list and try and email again
    for my $blacklisted_email ( @{ $test->{blacklisted_emails} } ) {
        $ebl->create(
            {   email   => $blacklisted_email,
                purpose => $test->{purpose}
            }
        );
    }

    throws_ok(
        sub { $stream2->send_email($email); },
        'Stream::EngineException::MessagingError',
        $test->{test_name}
    );

}

#  this is the sub tests for the email black list database.

# create a black list and try and email again
$ebl->create( { email => 'jerome@broadbean.com', } );

ok( $ebl->email_blacklisted('jerome@broadbean.com') );
ok( !$ebl->email_blacklisted(
        'jerome@broadbean.com', { group_identity => 'blabla' }
    )
);
ok( !$ebl->email_blacklisted(
        'jerome@broadbean.com',
        { group_identity => 'blabla', purpose => 'whatever' }
    )
);

$ebl->create(
    { email => 'jerome@broadbean.com', group_identity => 'blabla' } );
ok( $ebl->email_blacklisted(
        'jerome@broadbean.com', { group_identity => 'blabla' }
    )
);

$ebl->create(
    {   email          => 'jerome@broadbean.com',
        group_identity => 'blabla',
        purpose        => 'whatever'
    }
);
ok( $ebl->email_blacklisted(
        'jerome@broadbean.com',
        { group_identity => 'blabla', purpose => 'whatever' }
    )
);

done_testing();

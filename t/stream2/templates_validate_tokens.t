#! perl
# Test a basic search (on the dummy search template).

use strict;
use warnings;

use FindBin;
use Test::More;

use Data::UUID;

use Stream2;

use Log::Any;
# use Log::Any::Adapter qw/Stderr/;


my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $ug = Data::UUID->new();

my $users_factory = $stream2->factory('SearchUser');

# Create a test user.
my $user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
$user->save();

ok( my $template = $stream2->build_template('dummy', undef , { user_object => $user }) , "Ok got dummy template");


# use Data::Dumper;
# diag( Dumper( $template->standard_tokens() ) );

# done_testing();
# exit(0);

ok( ! $template->invalid_token_values()  , "No invalid token values");
ok( ! $template->invalid_token_values({ some => [ 1 ] , stuff => [] , not_in_template => undef }) , "Ok no invalid tokens");
ok( ! $template->invalid_token_values({ dummy_activity => [ 1 ] } ), "Ok good real dummy token");
is_deeply( $template->invalid_token_values( { dummy_activity => [ 314 ] } ) , { dummy_activity => [ 314 ] } , "Ok got errors" );
is_deeply( $template->invalid_token_values( { dummy_activity => [ 314 , 1 ] } ) , { dummy_activity => [ 314 ] } , "Ok got errors" );
is_deeply( $template->invalid_token_values( { dummy_activity => [ 314 , 1 ] , not_there => [ 'a' , 'b' , 'c' ] } ) , { dummy_activity => [ 314 ] } , "Ok got errors" );
is_deeply( $template->invalid_token_values( { dummy_activity => [ 314 , 1 ] , dummy_activity_time => [  180, 'a' , 90, 'b' , 30 ,  'c' ] } )
           , { dummy_activity => [ 314 ],
               dummy_activity_time => [ 'a' , 'b' , 'c' ],
             } , "Ok got errors" );

is_deeply( $template->invalid_token_values( { dummy_place => [ 98 , 'Sillyvalue' , 'BBTECH_RESERVED_OPTGROUP' , 2 ] } ),
           { dummy_place => [ 'Sillyvalue'  , 'BBTECH_RESERVED_OPTGROUP' ] },
           "Ok got errors");


done_testing();

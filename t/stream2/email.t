#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Stream2;

use Email::Simple;

use Test::MockModule;

# to deploy email_blacklist table
use Test::Mojo::Stream2;
my $t = Test::Mojo::Stream2->new();
$t->login_ok;

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

ok( my $stream2 = $t->app->stream2 , "Ok can build stream2");
ok( my $email_factory = $stream2->factory('Email') );
is( $stream2->factory('Email') , $email_factory , "Singleton OK");
ok( $email_factory->email_transport(), "Ok got standard transport");
ok( $email_factory->static_email_transport() , "Ok got static email transport");


{
    # Send a normal email
    my $went_through = 0;
    my $mock_sender = Test::MockModule->new( 'Email::Sender::Simple' );
    $mock_sender->mock( 'send' => sub{
                            my ($self, $email , $options) = @_;
                            $went_through++;
                            is( $options->{transport} , $email_factory->email_transport() );
                            return $mock_sender->original('send')->( $self, $email , $options );
                        });

    my $email = Email::Simple->create(
        header => [ To => 'jerome@broadbean.com',
                    From => 'whatever@broadbean.com',
                    Subject => 'Boudin blanc',
                    Bcc => 'jeje@broadbean.com ; someother@broadbean.com',
                ],
        body => 'Some message sent'
    );
    ok( $stream2->send_email($email) , "Ok can send_email");
    is( $went_through , 3 , "Went through the send method with the right standard transport 3 times");

    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries)  , 3 ,  "Ok 3 deliveries (one for the To, and 2 for the BCCs");
    is( $deliveries[0]->{envelope}->{to}->[0] , 'jerome@broadbean.com' );
    is( $deliveries[1]->{envelope}->{to}->[0] , 'jeje@broadbean.com' );
    is( $deliveries[2]->{envelope}->{to}->[0] , 'someother@broadbean.com' );

    # headers to faciliate unique message identification must exist
    ok( $deliveries[0]->{email}->get_header('Message-ID'), 'Message-ID present' );
    ok( $deliveries[0]->{email}->get_header('X-MJ-CustomID'), 'Mailjet custom ID present' );
}

{
    # Send a static email
    my $went_through = 0;
    my $mock_sender = Test::MockModule->new( 'Email::Sender::Simple' );
    $mock_sender->mock( 'send' => sub{
                            my ($self, $email , $options) = @_;
                            $went_through++;
                            is( $options->{transport} , $email_factory->static_email_transport() );
                            return $mock_sender->original('send')->( $self, $email , $options );
                        });

    my $email = Email::Simple->create(
        header => [ To => 'jerome@broadbean.com',
                    Bcc => 'jerome.eteve@gmail.com ; someother@broadbean.com',
                    From => 'noreply@broadbean.net',
                    Subject => 'Boudin blanc',
                    'X-Stream2-Transport' => 'static_transport'
                ],
        body => 'Some message sent'
    );
    ok( $stream2->send_email($email) , "Ok can send_email");
    is( $went_through , 3 , "Went throught the sending with the right transport 3 times");

    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries)  , 6 ,  "Ok 6 deliveries (3 more)");
    is( $deliveries[3]->{envelope}->{to}->[0] , 'jerome@broadbean.com' );
}


done_testing();

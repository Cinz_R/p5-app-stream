#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Data::Dumper;
use Test::Stream2;
use Stream2::Action::UserAdoptData;

use Log::Any::Adapter;
# Log::Any::Adapter->set('Stderr');

# Testing data adoption from one user to another

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->deploy_test_db();


# Ok the setting are not empty
ok( $stream2->factory('Setting')->count() , "Ok got some settings");


# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser') , "Ok can get resultset");

my $users_factory = $stream2->factory('SearchUser');

# Time to create an adcourier user
my $adcourier_user = $users_factory->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 3141,
    group_identity => 'acme',
    subscriptions_ref => {
        flickr_xray => {
            nice_name => "Flickr",
            auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
            type => "social",
            allows_acc_sharing => 0,
        },
        'whatever' => {
            nice_name => 'Whatever',
            auth_tokens => { whatever_login => 'Boudin', whatever_passwd => 'Blanc' },
            type => 'social',
        }
    }
});
ok( my $adcourier_id = $adcourier_user->save() , "Ok can save this user");


# Then an ats user:
my $ats_user = $users_factory->new_result({
    provider => 'adcourier_ats',
    last_login_provider => 'adcourier_ats',
    provider_id => 3141,
    group_identity => 'acme',
    subscriptions_ref => {
        flickr_xray => {
            nice_name => "Flickr",
            auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
            type => "social",
            allows_acc_sharing => 0,
        },
        'sumthin' => {
            nice_name => 'Something',
            auth_tokens => {},
            type => 'social',
        }
    }
});

ok( my $ats_id = $ats_user->save() , "Ok can save this user");

# Alright, let's list all the relationships from a user and create enough data
# so we can check none are empty.

my $users_source =  $stream2->stream_schema->resultset('CvUser')->result_source();

my @relationships =  $users_source->relationships();
my @to_transfer = ();
foreach my $relationship ( @relationships ){
    my $rel_data = $users_source->relationship_info($relationship);
    # diag($relationship.':'.Dumper($rel_data));
    if( $rel_data->{attrs}->{accessor} eq 'multi' ){
        push @to_transfer , $relationship;
    }
}

# Now we need to add some data to both users, so we can test the consolidation
# after.

{
    # Add some settings.
    ok( my $setting = $stream2->factory('Setting')->find({ setting_mnemonic => 'candidate-actions-profile-enabled' }) , "Ok can get setting");
    ok( my $string_setting = $stream2->factory('Setting')->find({ setting_mnemonic => 'behaviour-downloadcv-use_internal'}) , "Ok can get string setting");
    ok( my $other_string_setting = $stream2->factory('Setting')->find({ setting_mnemonic => 'criteria-cv_updated_within-default' }) , "Ok can get string setting for cv_updated_within");

    $setting->set_users_value($adcourier_user, [ $adcourier_user->id(), $ats_user->id() ] , 1);
    $string_setting->set_users_value( $adcourier_user , [ $ats_user->id() ] , 'SomeStringOnlyInAts' );
    # Note that ats_user can also set stuff.
    $other_string_setting->set_users_value( $ats_user , [ $adcourier_user->id() ] , 'SomeStringOnlyInAdcourier' );
}

{
    # time to add some candidate tags
    my $saussage = $stream2->factory('Tag')->create({ tag_name => 'saussage', tag_name_ci => 'saussage', group_identity => $adcourier_user->group_identity() });
    my $beer     = $stream2->factory('Tag')->create({ tag_name => 'beer', tag_name_ci => 'beer',  group_identity => $adcourier_user->group_identity() });
    my $mash     = $stream2->factory('Tag')->create({ tag_name => 'mash', tag_name_ci => 'mash',  group_identity => $adcourier_user->group_identity() });

    # adcourier user adds saussage and beer to one candidate
    $stream2->factory('CandidateTag')->create({ group_identity => $adcourier_user->group_identity(),
                                                user_id  => $adcourier_user->id(),
                                                candidate_id => 'blabla',
                                                tag => $saussage->o() });
    $stream2->factory('CandidateTag')->create({ group_identity => $adcourier_user->group_identity(),
                                                user_id  => $adcourier_user->id(),
                                                candidate_id => 'blabla',
                                                tag => $beer->o() });
    # ats user adds mash to the same candidate
    $stream2->factory('CandidateTag')->create({ group_identity => $ats_user->group_identity(),
                                                user_id  => $ats_user->id(),
                                                candidate_id => 'blabla',
                                                tag => $mash->o() });
}

{
    # time to add some user tags.
    my $saussage   = $stream2->factory('Usertag')->create({ tag_name => 'saussage', tag_name_ci => 'saussage', user => $adcourier_user->o() });
    my $beer       = $stream2->factory('Usertag')->create({ tag_name => 'beer', tag_name_ci => 'beer', user => $adcourier_user->o()  });
    my $cheese     = $stream2->factory('Usertag')->create({ tag_name => 'cheese', tag_name_ci => 'cheese', user => $ats_user->o()  });
    my $other_beer = $stream2->factory('Usertag')->create({ tag_name => 'beer', tag_name_ci => 'mash', user => $ats_user->o()  });
}

{
    # Add some longlists
    $adcourier_user->longlists()->create({ user_id => $adcourier_user->id ,  name => 'My Longlist Adcourier'});
    $ats_user->longlists()->create({ user_id => $ats_user->id ,  name => 'My Longlist Ats'});
}

{
    # add some email templates
    my $email = $stream2->factory('EmailTemplate')->create({
        group_identity => $adcourier_user->group_identity,
        name => 'test template',
        mime_entity => 'sausage'
    });
    my $email2 = $stream2->factory('EmailTemplate')->create({
        group_identity => $adcourier_user->group_identity,
        name => 'test template 2',
        mime_entity => 'sausage'
    });
}

{
    # Add some recurring reports.
    my $now = DateTime->now();

    # For both users.
    my $r1 = $stream2->factory('RecurringReport')->create({
        next_run_datetime => $now,
        class => 'Stream2::Role::Report::Void',
        settings => { foo => 'bar' }});
    $r1->add_to_users($adcourier_user->o());
    $r1->add_to_users($ats_user->o());

    # Only for adcourier.
    my $r2 = $stream2->factory('RecurringReport')->create({
        next_run_datetime => $now,
        class => 'Stream2::Role::Report::Void',
        settings => { foo => 'bar' }});
    $r2->add_to_users($adcourier_user->o());

    # Only for ats
    my $r3 = $stream2->factory('RecurringReport')->create({
        next_run_datetime => $now,
        class => 'Stream2::Role::Report::Void',
        settings => { foo => 'bar' }});
    $r3->add_to_users($ats_user->o());
}

{
    # Search records
    my $criteria = Stream2::Criteria->new( schema => $stream2->stream_schema() );
    $criteria->save();

    $stream2->stream_schema->resultset('SearchRecord')->create({
        criteria_id => $criteria->id,
        user_id => $adcourier_user->id,
    });

    $stream2->stream_schema->resultset('SearchRecord')->create({
        criteria_id => $criteria->id,
        user_id => $ats_user->id,
    });

}

{
    # Create some watchdogs.
    # Note that the criteria doesnt matter much
    my $criteria = Stream2::Criteria->new( schema => $stream2->stream_schema() );
    $criteria->save();

    my @wds = ();
    # adcourier user
    {
        my $flickr = $adcourier_user->subscriptions()->find({ board => 'flickr_xray' });
        my $whatever = $adcourier_user->subscriptions()->find({ board => 'whatever' });
        my $snoopy = $stream2->watchdogs->create({ user_id => $adcourier_user->id() , name => 'Snoopy', criteria_id => $criteria->id() });
        $snoopy->add_to_watchdog_subscriptions( { subscription => $flickr } );
        $snoopy->add_to_watchdog_subscriptions( { subscription => $whatever } );
        push @wds , $snoopy;
    }
    # Ats user
    {
        my $flickr = $ats_user->subscriptions()->find({ board => 'flickr_xray' });
        my $something = $ats_user->subscriptions()->find({ board => 'sumthin' });
        my $snoopy = $stream2->watchdogs->create({ user_id => $ats_user->id() , name => 'Snoopy', criteria_id => $criteria->id() });
        $snoopy->add_to_watchdog_subscriptions( { subscription => $flickr } );
        $snoopy->add_to_watchdog_subscriptions( { subscription => $something } );
        push @wds , $snoopy;
    }

    # Create a few result for each wd subscription
    foreach my $wd ( @wds ){
        foreach my $wdsub (  $wd->watchdog_subscriptions()->all() ){
            foreach my $cid ( map{ 'candidate_'.$_ } ( 1..10 ) ){
                $stream2->factory('WatchdogResult')->create({
                    watchdog_id => $wdsub->watchdog_id(),
                    subscription_id => $wdsub->subscription_id(),
                    candidate_id => $cid,
                    viewed => 0,
                    content => { cid => $cid },
                });
            }
        }
    }
}

{
    # Saved searches.
    # Search records
    my $criteria = Stream2::Criteria->new( schema => $stream2->stream_schema() );
    $criteria->save();

    $stream2->factory('SavedSearch')->create({ user_id => $adcourier_user->id(),
                                               criteria => $criteria,
                                               active => 1 });
    $stream2->factory('SavedSearch')->create({ user_id => $ats_user->id(),
                                               criteria => $criteria,
                                               active => 1 });
}


# Action logs and sent emails
foreach my $user ( $adcourier_user , $ats_user ){
    my $candidate_action_log = $stream2->action_log->insert({
        action => 'message',
        candidate_id => 'whatever',
        destination  => 'dowecare',
        user_id      => $user->id(),
        group_identity => $user->group_identity(),
        data => {}
    });

    $stream2->stream_schema->resultset('SentEmail')
        ->create( {
            user_id       => $user->id,
            action_log_id => $candidate_action_log->id,
            sent_date     => \'NOW()',
            subject       => 'bla',
            recipient     => 'bla',
            aws_s3_key    => 'bla',
        } );
}


# Quality control.
my $pretransfer_adcourier_counts = {};
foreach my $rel ( @to_transfer ){
    # diag("Validating relation '$rel'");
    my $adcourier_count = $adcourier_user->$rel()->count();
    $pretransfer_adcourier_counts->{$rel} = $adcourier_count;
    ok( $adcourier_count , "Ok adcourier user has got some '$rel'");
    ok( $ats_user->$rel()->count() , "Ok ats_user has got some '$rel'");
}

# Ok now transfer all the data.
$adcourier_user->adopt_data_from($ats_user);

# And check that the ats user is now data free.
foreach my $rel ( sort @to_transfer ){
    if( $rel eq 'subscriptions' ){
        # Subscriptions are a bit special. They will go when
        # we delete the ats user anyway.
        next;
    }
    if( $rel eq 'user_settings' ){
        # User settings are a bit special. They will go when
        # we delete the ats user anyway, and the adcourier
        # user setting was the source of truth in the past.
        next;
    }
    # diag("Validating relation '$rel'");
    my $adcourier_count = $adcourier_user->$rel()->count();
    ok( $adcourier_count , "Ok some data in adcourier user");
    cmp_ok( $adcourier_count , '>' , $pretransfer_adcourier_counts->{$rel} , "There is more data in adcourier user than before the transfer for $rel");
    is( $ats_user->$rel()->count() , 0, "Ok ats_user does not have any '$rel'");
}

# Alright, check we can delete the ats_user.
# This should happily cascade delete the subscriptions (and the watchdog_subscriptions and results),
# and the user_settings;

$ats_user->delete();
# Check no user settings exists anymore with the ats_id
is( $stream2->factory('UserSetting')->search({ user_id => $ats_id })->count() , 0 );

# And that no subscriptions neither exists
is( $stream2->factory('CvSubscription')->search({ user_id => $ats_id })->count() , 0 );

done_testing();

#! /usr/bin/env perl
use strict;
use warnings;
use Test::More;

# Silence!
# use Log::Any::Adapter;
# use Log::Log4perl qw/:easy/;
# Log::Log4perl->easy_init($TRACE);
# Log::Any::Adapter->set('Log4perl');

use File::Temp;
use File::Slurp;

use Stream2::DirWatcher;
use Stream2::ForkManager;


# A file for processes to report in
my ($fh, $filename) = File::Temp::tempfile();
close($fh);

# A directory to watch.
my $temp_dir = File::Temp::tempdir( CLEANUP => 1 );

# Something that will be restarted when a file changes.
my $restart = sub{
    File::Slurp::write_file($filename, { append => 1 }, 'RESTART' );
};

# We need to run the dir watcher in a new process.
# so we can control and test it.
my $fm = Stream2::ForkManager->new({ main => sub{
                                         my $dr = Stream2::DirWatcher->new({ dir => $temp_dir,
                                                                             restart => $restart
                                                                           });
                                         $dr->watch();
                                     }
                                   });

# Ok, change one file in the dir and check the report file contains 2 RESTART
# (one for the first time, one for the restart).
{
    diag ("LOOK INTO $temp_dir");
    sleep(2);
    my ($fh2, $filename2 ) = File::Temp::tempfile( DIR => $temp_dir );
    #system('touch '.$filename2);
    File::Slurp::write_file($filename2, { append => 1 }, 'some stuff' );
    # That will cause the DirWatcher to restart the process '$restart'
#    sleep(1);
#    is( File::Slurp::read_file($filename) , 'RESTART' x 2 , "Dir watcher has ran 2 processes");
}

# Teardown Fork manager cleanly.
$fm->kill_all();
$fm->wait_children();

ok(1);
done_testing();

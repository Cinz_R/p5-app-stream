#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;

use Data::UUID;

use Log::Any::Adapter;

# Log::Any::Adapter->set('Stderr');

use Stream2;
use Stream2::Criteria;

no warnings;
*Stream2::log_search_calendar = sub {};
use warnings;


my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}


ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser') , "Ok can get resultset");

my $schema = $stream2->stream_schema();

# Build an empty criteria
my $criteria = Stream2::Criteria->new( schema => $schema );

$criteria->add_token( 'keywords' , 'manager');
$criteria->add_token('cv_updated_within' , '3M' );

$criteria->save();



my $users_factory = $stream2->factory('SearchUser');

# Time to create a user.
my $user_id;
my $memory_user;
{
    $memory_user = $users_factory->new_result({  provider => 'dummy',
                                                 last_login_provider => 'dummy',
                                                 provider_id => 31415,
                                                 group_identity => 'acme',
                                                 subscriptions_ref => {
                                                                      }
                                              });
    ok( $user_id = $memory_user->save() , "Ok can save this user");
}

# Create a longlist
ok( my $ll = $memory_user->longlists()->create({ user_id => $user_id ,  name => 'My Longlist'}) , "Ok can create longlist with a name" );
is( $memory_user->longlists()->count() , 1 , "Ok one longlist");

ok( $stream2->factory('LongList')->user_can_create_longlist($memory_user) , "Ok user can still create longlists");

foreach my $i ( 1..9 ){
    ok( my $ll = $memory_user->longlists()->create({ user_id => $user_id ,  name => 'My Longlist '.$i }) , "Ok can create longlist with a name" );
    is( $memory_user->longlists()->count() , 1 + $i , "Ok good number of longlist");
}

ok( ! $stream2->factory('LongList')->user_can_create_longlist($memory_user) , "Ok user cannot create longlist anymore");
dies_ok { $memory_user->longlists()->create({ user_id => $user_id ,  name => 'One too many' }) } 'Cannot create one more';

# Delete one an see that we can add some more again.
$memory_user->longlists()->first()->delete();
ok( $stream2->factory('LongList')->user_can_create_longlist($memory_user) , "Ok user can create longlists again");


# Lets perform a search and add each of the candidates in the longlist.
ok( my $template = $stream2->build_template('dummy', undef , { user_object => $memory_user }) , "Ok got dummy template");

my $ug = Data::UUID->new();

my $results_id = $ug->create_str();

ok( my $results_collector = Stream2::Results->new({
                                                   id          => $results_id,
                                                   destination => 'dummy',
                                                  }),
    "Ok can build a result collector");
# Inject the result collector in the template object.
$template->results($results_collector);
# And search!
$template->search({});

# Check we now have results
ok( my $n_results = scalar(@{$results_collector->results()}), "Ok got results in the results collector");

my $test_list = $memory_user->longlists()->first();

foreach my $candidate ( @{$results_collector->results()} ){
    $test_list->add_candidate($candidate);
}

is( $test_list->candidates()->count() , $n_results , "OK good number of candidates in there");

# Add the same candidates again
foreach my $candidate ( @{$results_collector->results()} ){
    $test_list->add_candidate($candidate);
}

# Number is still the same
is( $test_list->candidates()->count() , $n_results , "OK good number of candidates in there");

done_testing();

use Test::Most;
use Test::Stream2;

my $s2 = Test::Stream2->new({ config_file => './t/app-stream2.conf' });
$s2->deploy_test_db;

my $factory = $s2->factory('AdcresponsesCustomFlag');
my $gc_factory = $s2->factory('Group');
$gc_factory->create({
    identity => 'cinzwonderland', nice_name => 'Cinz'
});
$gc_factory->create({
    identity => 'pat', nice_name => 'Pat'
});

my $flag1 = $factory->create({
    colour_hex_code => '#00FF00',
    description => 'I am a sausage',
    group_identity => 'cinzwonderland'
});

my $flag2 = $factory->create({
    colour_hex_code => '#00FFFF',
    description => 'HELLO',
    group_identity => 'cinzwonderland'
});

my $flag3 = $factory->create({
    colour_hex_code => 'meh',
    description => 'another flag',
    group_identity => 'pat'
});

my $flag4 = $factory->create({
    colour_hex_code => 'blarg',
    description => 'yet another',
    group_identity => 'cinzwonderland'
});


my @results = $factory->search(
    {
        group_identity => 'cinzwonderland'
    },
    {
        order_by => { -asc => 'flag_id' }
    }
)->all();

my @settings = $s2->factory(
    'Groupsetting',
    {
        group_identity => 'cinzwonderland'
    },
)->search()->all();

is ( scalar( @settings ), 3 );
is ( $settings[0]->setting_mnemonic, 'adcresponses-custom-flag-20' );
my $flag1_desc = $flag1->description;
like ( $settings[0]->setting_description, qr/\Q$flag1_desc\E/ );
is ( $flag1->flag_id, 20 );
is ( $flag2->flag_id, 21 );
is ( scalar( @results ), 3 );
is ( $results[0]->flag_id, 20 );
is ( $results[1]->flag_id, 21 );
is ( $results[1]->description, 'HELLO' );

my @available = $factory->list_available_flags( 'cinzwonderland' );
is ( scalar(@available), 9 );

$flag1->delete();
is( $s2->factory('Groupsetting', { group_identity => 'cinzwonderland' })->count(), 2, "deleting the flag, deletes the setting" );

done_testing();

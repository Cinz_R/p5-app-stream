use Test::Most;
use Test::Stream2;
use JSON ();

my $config_file = 't/app-stream2.conf';
my $stream2 = Test::Stream2->new({ config_file => $config_file });
$stream2->deploy_test_db;

my $email_factory = $stream2->factory('EmailTemplate');
my $group_file_factory = $stream2->factory('GroupFile');
my $user_factory = $stream2->factory('SearchUser');

ok( my $attachment_factory = $stream2->factory('EmailTemplateAttachment'), 'We can build the email template attachment table' );

my $user = $user_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    group_identity => 'acme',
    settings => {
        "auto_download_cvs_on_preview" => JSON::XS::false,
        "some_other_random_setting" => JSON::true,
        'stream' => {
            'forward_to_tp2' => '0'
        },
    },
    provider_id => 31415,
});
$user->save();



my $email = $email_factory->create({
    name => 'test template',
    group_identity => $user->group_identity,
    mime_entity => 'whatever'
});

my $email2 = $email_factory->create({
    name => 'unrestricted test template',
    group_identity => $user->group_identity,
    mime_entity => 'whatever2'
});

my $group_file = $group_file_factory->create({
    mime_type => 'text/plain',
    public_file_key => 'B19024D9-377B-43D1-A336-41418EA96F76',
    name => 'kitten.txt',
    group_identity => $user->group_identity()
});

my $group_file2 = $group_file_factory->create({
    mime_type => 'text/plain',
    public_file_key => 'B19024D9-377B-43D1-A336-41418EA96F77',
    name => 'kitten2.txt',
    group_identity => $user->group_identity()
});

$email->create_related( 'email_template_attachments' => {
    group_file_id => $group_file->id
});
$email->create_related( 'email_template_attachments' => {
    group_file_id => $group_file2->id
});

is ( scalar( $email->email_template_attachments->all ), 2 );
is ( scalar( $email2->email_template_attachments->all ), 0 );

$email->delete();

is ( scalar( $attachment_factory->search()->all ), 0 );

done_testing();

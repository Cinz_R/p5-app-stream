use Test::Most;
use Test::Stream2;

my $config_file = 't/app-stream2.conf';
my $stream2 = Test::Stream2->new({ config_file => $config_file });
$stream2->deploy_test_db;

my $email_factory = $stream2->factory('EmailTemplate');
my $user_factory = $stream2->factory('SearchUser');
my $user_args = {
    provider => 'whatever',
    last_login_provider => 'whatever',
    group_identity => 'acme',
    settings => {
        "auto_download_cvs_on_preview" => JSON::XS::false,
        "some_other_random_setting" => JSON::true,
        'stream' => {
            'forward_to_tp2' => '0'
        },
    }
};

my $user = $user_factory->new_result({
    %$user_args,
    provider_id => 31415,
});
$user->save();

my $user2 = $user_factory->new_result({
    %$user_args,
    provider_id => 31416,
});
$user2->save();

my $user3 = $user_factory->new_result({
    %$user_args,
    provider_id => 31417,
});
$user3->save();

my $email =$email_factory->create({
    name => 'test template',
    group_identity => $user->group_identity,
    mime_entity => 'whatever'
});

my $email2 =$email_factory->create({
    name => 'restricted test template 2',
    group_identity => $user->group_identity,
    mime_entity => 'whatever3'
});

$email->permission_setting->set_users_value(
    $user, [ $user->id, $user3->id ], 1
);

$email2->permission_setting->set_users_value(
    $user, [ $user3->id ], 1
);

$email->permission_setting->set_users_value(
    $user, [ $user2->id ], 0
);

is ( scalar( $email->permission_setting->users_groupsetting->all ), 1 );
is ( scalar( $email2->permission_setting->users_groupsetting->all ), 0 );

is ( $user->settings_values_hash()->{groupsettings}->{$email->setting_mnemonic}, 1 );
is ( $user->settings_values_hash()->{groupsettings}->{$email2->setting_mnemonic}, 1 );
is ( $user2->settings_values_hash()->{groupsettings}->{$email->setting_mnemonic}, 0 );
is ( $user2->settings_values_hash()->{groupsettings}->{$email2->setting_mnemonic}, 1 );

done_testing();

#! perl
# Test a basic search (on the dummy search template).

use strict;
use warnings;

use utf8;
use FindBin;
use Test::More;

use Data::UUID;

use Test::Mojo::Stream2;

use Stream2;
use Stream2::Criteria;

use Data::Dumper;
use Encode;

my $t = Test::Mojo::Stream2->new();
$t->login_ok();

my $stream2 = $t->app()->stream2();

# don't want to actually queue the job as there is no redis
$t->app->helper( 'queue_job', sub { return MyJob->new; } );

my $search_keyword = "mark";
my $search_query = {
    "_searchOptions" => {},
    "board"          => "talentsearch",
    "criteria"       => {
        "location_within"       => 30,
        "cv_updated_within"     => "3M",
        "default_jobtype"       => "",
        "salary_cur"            => "",
        "salary_per"            => "",
        "salary_from"           => "",
        "salary_to"             => "",
        "keywords"              => undef,
        "talentsearch_order_by" => "Relevance",
    }
};


{
    my @keyword_searches = ('marketing executive', 'marketing director', 'marketing manager', 'market research analyst');

    foreach my $keyword_search (@keyword_searches) {
        # add new keywords to search
        $search_query->{criteria}->{keywords} = $keyword_search;
        record_search($t, $search_query);
    }

    # now query search keywords
    my $expected_json_result = {
        "suggestions" => [
            {"value" => "marketing executive",     "label" => "marketing executive"},
            {"value" => "marketing director",      "label" => "marketing director"},
            {"value" => "marketing manager",       "label" => "marketing manager"},
            {"value" => "market research analyst", "label" => "market research analyst"}
            ]
    };
    get_suggestions($t, $search_keyword, 200, $expected_json_result, "Correct keyword suggestions returned");
}

{
    # check that post to '/api/user/keyword-suggest' returns a 404 error
    $t->post_ok(
        '/api/user/keyword-suggest',
        form => {
            q => JSON::encode_json({search => $search_keyword })
        }
    )->status_is(404);
}

{
    $search_query->{criteria}->{keywords} = "   ";

    record_search($t, $search_query);
    get_suggestions($t, " ", 200, { suggestions => [] });
    get_suggestions($t, "", 200, { suggestions => [] }, "Checking searching for empty string returns no suggestions");
}

{
    $search_query->{criteria}->{keywords} = q{"Java Developer"};

    record_search($t, $search_query);
    get_suggestions($t, '"Java', 200, { suggestions => [{value=>q{"Java Developer"},label=>q{"Java Developer"}}] }, "Character escaping works correctly");
}

{ #utf8 test 1
    $search_query->{criteria}->{keywords} = "utf8 ¥§ª±µËÕßæöøý";
    record_search($t, $search_query);

    my $expected_json_result = {
        "suggestions" => [
            {"value" => "utf8 ¥§ª±µËÕßæöøý", "label" => "utf8 ¥§ª±µËÕßæöøý"},
        ]
    };

    # diag("Results containing UTF8 characters returned");
    $t->get_ok(
        '/api/user/keyword-suggest',
        form => {
            q => JSON::to_json({search => "utf8" })
        }
    )->status_is( 200 );

    my $json = JSON::decode_json($t->tx->res->content()->get_body_chunk(0));
    is_deeply(
        $json->{suggestions}->[0]->{value},
        $expected_json_result->{suggestions}->[0]->{value},
        "Results containing UTF8 characters returned in correct structure 1"
    );
}

{ #utf8 test 2
    $search_query->{criteria}->{keywords} = "¥§ª±µËÕßæöøý";
    record_search($t, $search_query);

    my $expected_json_result = {
        "suggestions" => [
            {"value" => "¥§ª±µËÕßæöøý", "label" => "¥§ª±µËÕßæöøý"},
        ]
    };

    # diag("Getting suggestions when supplying keyword search containing UTF8 characters");
    $t->get_ok(
        '/api/user/keyword-suggest',
        form => {
            q => JSON::to_json({search => "¥§ª±µ" })
        }
    )->status_is( 200 );

    my $json = JSON::decode_json($t->tx->res->content()->get_body_chunk(0));
    is_deeply(
         $json->{suggestions}->[0]->{value},
         $expected_json_result->{suggestions}->[0]->{value},
        "Results containing UTF8 characters returned in correct structure 2"
    );
}

done_testing();

sub get_suggestions {
    my ($t, $search_keyword, $status, $expected_json, $message) = @_;

    # diag( $message ) if $t;

    $t->get_ok(
        '/api/user/keyword-suggest',
        form => {
            q => JSON::to_json({search => $search_keyword })
        }
    )->status_is( $status )
    ->json_is( $expected_json );
}

sub record_search {
    my ($t, $search_query) = @_;
    # Build criteria object.
    my $criteria = Stream2::Criteria->from_query( $t->app->stream_schema(), $search_query->{criteria} );
    # Build the search record. That's all is needed to have the autocomplete
    my $search_record = $stream2->stream_schema()->resultset('SearchRecord')->create({
        criteria_id => $criteria->id,
        user_id => 1,
    });
}

{
    package MyJob;
    sub new { bless {}, shift; }
    sub guid { 1; };
}
1;

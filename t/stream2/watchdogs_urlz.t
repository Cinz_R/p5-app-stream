use Test::Most;

use DateTime;
use Email::Simple;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

use Test::Stream2;
use Stream2::Criteria;
use Stream::Templates::Default;
use Test::MockModule;

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();

my @results = (
    Stream2::Results::Result->new({
        candidate_id => 1,
        email => 'cameron@downingsteeet.com',
        destination => 'google_plus',
        location_text => 'Tokyo',
        name => 'David Cameron',
        picture_url => '//www.example.com/4.jpg'
    }),
    Stream2::Results::Result->new({
        candidate_id => 2,
        email => 'may@downingsteeet.com',
        destination => 'google_plus',
        location_text => 'Djibouti',
        name => 'Theresa May',
        picture_url => '//www.example.com/3.jpg'
    }),
    Stream2::Results::Result->new({
        candidate_id => 3,
        email => 'brown@downingsteeet.com',
        destination => 'google_plus',
        location_text => 'Sandringham',
        name => 'Gordon Brown',
        picture_url => '//www.example.com/1.jpg'
    }),
    Stream2::Results::Result->new({
        candidate_id => 4,
        email => 'jezza@downingsteeet.com',
        destination => 'google_plus',
        location_text => 'Woodstock',
        name => 'Jeremy Corbyn',
        picture_url => '//www.example.com/1.jpg'
    })
);

my $feed_mock = Test::MockModule->new( 'Stream::Templates::google_plus' );
$feed_mock->mock( search => sub {
    my $self = shift;
    $self->total_results( scalar( @results ) );
    $self->results->add_candidate( @results );
    return @results;
});
$feed_mock->mock( quota_guard => sub { pop->() } );

# results store that doesn't go to redis
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { shift->results; } );
$results_mock->mock( fetch_results => sub { @results } );

my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( increment_analytics => sub { 1; } );
$stream2_mock->mock( redis => sub { MyRedis->new(); } );

# Try making a query on a resultset. It should not go bang.
my $cv_users = $stream2->stream_schema->resultset('CvUser');

my $schema = $stream2->stream_schema();

# Build an empty criteria
my $criteria = Stream2::Criteria->new( schema => $schema );

$criteria->add_token('cv_updated_within' , '3M' );

$criteria->save();


my $users_factory = $stream2->factory('SearchUser');

# Time to create a user.
my $user_id;
my $memory_user;
{
    $memory_user = $users_factory->new_result({  provider => 'adcourier',
                                                 last_login_provider => 'adcourier',
                                                 provider_id => 31415,
                                                 group_identity => 'acme',
                                                 contact_email => 'zorro@example.com',
                                                 contact_details => {},
                                                 subscriptions_ref => {
                                                     google_plus => {
                                                         nice_name => 'Groogle pus',
                                                         auth_tokens => {},
                                                         type => "social",
                                                     },
                                                 }
                                             });
    ok( $user_id = $memory_user->save() , "Ok can save this user");
    is( $schema->resultset('CvSubscription')->search({ user_id => $user_id })->count() , 1 , "Ok one subscriptions in the DB");
    no warnings;
    # Any user ALWAYS exists remotely (for the sake of this test).
    *Stream2::O::SearchUser::exists_remotely = sub{ return 1; };
    use warnings;
}

my $snoopy = $stream2->watchdogs->create({ user_id => $user_id , name => 'Snoopy', criteria_id => $criteria->id() , base_url => 'http://boudin.net/'  });

my $google_plus = $memory_user->subscriptions()->find({ board => 'google_plus' });

$snoopy->add_to_watchdog_subscriptions( { subscription => $google_plus } );

my $err_cal_run = 0;
$stream2_mock->mock( log_search_calendar => sub {
    $err_cal_run = 1;
    my ( $self, $stuff ) = @_;
    is( $stuff->{stream2_base_url}, 'http://boudin.net/', 'Base URL is sent to error calendar' );
    1;
});

# Try to initialize it.
$stream2->watchdogs()->runner($snoopy)->initialize();
ok( $err_cal_run, "err calendar was called" );
$err_cal_run = 0;

foreach my $subscription ( $snoopy->watchdog_subscriptions()->all ){
    ok( $subscription->watchdog_results()->count(), "Subscription has results" );
    ok( $subscription->watchdog_results->first()->viewed(), "result is set as viewed" );
}

# Now we need to run this watchdog and see that an email has been sent.
# Fiddle twith the subscription results so two of each are new.
foreach my $subscription ( $snoopy->watchdog_subscriptions()->all ){
    $subscription->watchdog_results()->search(undef, { rows => 2 })->delete_all();
    ok( $subscription->watchdog_results()->count() , "We still have some results");
}

# Run the watchdog and check it sends an email
$stream2->watchdogs()->runner($snoopy)->run();

ok( $err_cal_run, "err calendar was called" );


my @deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries)  , "1" ,  "Ok one email got delivered");

{
    my $string_email = $deliveries[0]->{email}->as_string();
    like( $string_email , qr|src=3D"https://www\.example\.com/\d.jpg| , "No protocol less URLs for image sources please");
    like( $string_email, qr{http://boudin\.net/}, "We send the correct URLs in emails" );
}

done_testing();

{
    package MyRedis;
    sub rpush { 1; }
    sub expire { 1; }
    sub new { return bless {}; }
}

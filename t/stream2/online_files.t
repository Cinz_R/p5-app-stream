#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::MockModule;
use File::Touch;

use Log::Log4perl qw/:easy/;

# Check some emails.
BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $good_image_file = 'https://www.broadbean.com/images/logo.png';
my $bad_image_file  = 'https://www.broadbean.com/images/logo.pngz';

# Log::Log4perl->easy_init($TRACE);

# use Log::Any::Adapter q/Stderr/;

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{LWP_UA_MOCK} ||= $DEFAULT_UA_MOCK;
    $ENV{LWP_UA_MOCK_FILE} ||= __FILE__ . '-lwp-mock.out';
}

# use LWP;
use LWP::UserAgent::Mockable;

use Stream2;

my $config_file = 't/app-stream2.conf';
unless ( $config_file && -r $config_file ) {
    plan skip_all => "Cannot read config_file'"
        . ( $config_file || 'UNDEF' ) . "'";
}

ok( my $stream2 = Stream2->new( { config_file => $config_file } ),
    "Ok can build stream2" );

# Deploying schema
$stream2->stream_schema()->deploy();

# Test the OnlineFile factory
ok( my $onlinefiles = $stream2->factory(
        'OnlineFile',
        {   filesys_size_threshold  => 1000_000_000,
            filesys_inode_threshold => 100_000_000
        }
    ),
    "Ok got factory about online files with very high free file thresholds"
);
is( $stream2->factory('OnlineFile') . '',
    $stream2->factory('OnlineFile') . '', "Singleton!" );

ok( $onlinefiles->chi(), "Ok can build a CHI" );

# Delete all the keys
$onlinefiles->chi()->clear();

# as filesys_size_threshold and filesys_inode_threshold have been set to some
# ridiculous values, every time an on-line is pulled, it triggers tech report
# emails

{
    ok( my $file = $onlinefiles->find($bad_image_file),
        "Ok got file from URL" );
    is( $file->file_size(),   undef, 'a 404 has no content/length size.' );
    is( $file->has_disk_file, '',    'no file should downloaded' );

    # as this test fails we get a 404 which is text/plain
    is( $file->mime_type(), 'text/plain',
        'Got correct mime_type (text/plain)' );
    is( $file->has_error, 1, 'has this cached entry got an error? - YES' );

    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries), 2,
        "Got two emails. One about size, one about inodes" );
}

{
    ok( my $file = $onlinefiles->find($good_image_file),
        "Ok got file from URL" );

    is( $file->file_size(), 7342, 'size as reported by HEAD.' );
    is( $file->mime_type(), 'image/png',
        'Got correct mime_type (image/png)' );
    is( $file->file_size(),
        $file->_cached_entry->{file_size},
        'a the file size and cached entry file size are the same.'
    );
    is( $file->has_error, 0, 'has this cached entry got an error? - NO' );

    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries), 4,
        "Got two emails. One about size, one about inodes" );

# set the size from head to be incorrect to make sure the file size is reported from the cached_entry.
    is( $file->_file_size_from_head_request(10),
        10, 'setting the file size from to be incorrect' );
    is( $file->file_size(), 7342,
        'the size should be taken from the cached_entry' );

}

{
    # Same image again.
    ok( my $file = $onlinefiles->find($good_image_file),
        'file was downloaded again' );
    is( $file->mime_type(), 'image/png', 'the file was a png' );
}

# Reset the factory thresholds to a decent value
$onlinefiles->filesys_size_threshold(500_000);
$onlinefiles->filesys_inode_threshold(2000);

# test the cache clearing
{
    # A nice image
    ok( my $file = $onlinefiles->find( $good_image_file ), 'on-line file found something' );
    ok( -f $file->disk_file(), "Ok got disk file" );

    my $datetime_mock = Test::MockModule->new('DateTime');
    $datetime_mock->mock(
        now => sub {
            return DateTime->from_epoch( epoch => time )
                ->subtract( hours => 5 );
        }
    );

    ok( my $oldfile = $onlinefiles->find( $bad_image_file ), 'find and old file' );
    ok( -f $oldfile->disk_file(), "Ok got disk file" );

    my $dir = $oldfile->disk_file();
    $dir =~ s/\/stream2_onlinefiles\/(\d{10})\/.+/\/stream2_onlinefiles\/$1/;

    my $four_hours_ago = time - ( 4 * 60 * 60 );
    my $touch = File::Touch->new(
        mtime => $four_hours_ago,
        atime => $four_hours_ago
    );
    $touch->touch($dir);

    $onlinefiles->clear_old_cache();

    ok( !( -f $oldfile->disk_file() ), "Ok old file is gone" );
    ok( -f $file->disk_file(),         "Ok got disk file" );
}

LWP::UserAgent::Mockable->finished();
done_testing();

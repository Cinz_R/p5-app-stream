#! perl
use Test::Most;
# use Log::Any::Adapter qw/Stderr/;

use DateTime;
use Stream2;
use Stream2::Criteria;
use Test::MockModule;

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}
my $now = DateTime->now();
my $stream2 = Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->stream_schema()->deploy();

# Time to create some users

my $ts_subscriptions_ref = {
talentsearch => {
    nice_name   => "Talentsearch",
    auth_tokens => {
        talentsearch_username => 'smd',
        talentsearch_password => 'smdpass'
    },
    type               => "moo",
    allows_acc_sharing => 1,
}};
my $search_user_factory = $stream2->factory('SearchUser');
my $companies_users;
my $N_users = 20;
{
    for( my $i = 0; $i < $N_users ; $i++){
        my $company_name = 'acme'.( $i % 3 );
        my $memory_user = $search_user_factory->new_result({
            provider => 'adcourier',
            last_login_provider => 'adcourier',
            provider_id => 31415 + $i,
            group_identity => $company_name,
            timezone => (0 == $i % 6) ? 'Sol/Earth' : 'Sol/Mars',
            subscriptions_ref => (0 == $i % 2) ? $ts_subscriptions_ref : {},
        });
        ok( my $user_id = $memory_user->save() , "Ok can save this user");
        # generate a list of company users
        push @{$companies_users->{$company_name}},
            {
                id => 31415 + $i,
                name => "$user_id-$company_name",
                email => "$user_id\@$company_name.com",
                company => $company_name
            }
    }
}

my @users = $search_user_factory->all();

# generate some action types
my @actions = qw(download cv_preview shortlist);
my $N_actions = scalar @actions;
for( my $i = 0 ; $i < $N_actions; $i++ ){
    $stream2->factory('CandidateActions')->create(
        { action => ( $actions[int( $i % $N_actions)] ) });
}

# 2200 over three months = one per hour
my $N_action_logs = 2200;
{
    my $date = DateTime->now()->subtract( months => 3 );

    for( my $i = 0 ; $i < $N_action_logs; $i++ ){
        my $user =  $users[$i % $N_users];
        $stream2->factory('CandidateActionLog')->create({
            action => $actions[int( $i % $N_actions)],
            user_id => $user->id(),
            destination => 'doesntmatter',
            group_identity => $user->group_identity(),
            data => {},
            candidate_id => 'candidate_'.$i,
            insert_datetime => $date->iso8601(),
        });
        $date->add(hours => 1);
    }
}

# Then create some criteria
my $criteria = Stream2::Criteria->new( schema => $stream2->dbic_schema() );
ok( $criteria->save(), 'Saved empty criteria' );
ok( $criteria->id(), "Ok got ID");
ok( $criteria->is_empty() , "Ok This criteria is also logically empty");


# Search records
my $N_searches = 2200;
{
    my $date = DateTime->now()->subtract( months => 3 );

    for( my $i = 0 ; $i < $N_searches; $i++ ){
        my $user =  $users[$i % $N_users];
        $stream2->factory('SearchRecord')->create({
            user_id => $user->id(),
            criteria_id => $criteria->id(),
            insert_datetime => $date->iso8601(),
        });
        $date->add(hours => 1);
    }
}

# generate some unsubcribed candidates.
my $N_blacklisted_emails = 2200;
{
    my $date = DateTime->now()->subtract( months => 3 );

    for( my $i = 0 ; $i < $N_blacklisted_emails; $i++ ){
        my $user =  $users[$i % $N_users];
        $stream2->factory('EmailBlacklist')->create({
            comments => 'I am a comment',
            email => 'cand' . $i,
            group_identity => $user->group_identity(),
            insert_datetime => $date->iso8601(),
            purpose => 'message_candidate',
        });
        $date->add(hours => 1);
    }
}


# generate some mocked juice company data and mock how it is served
my $juice_companies;
for my $id (0..2)
{
    $juice_companies->{'acme'.( $id % 3 )} =
        {
            billable => (0 == $id % 2) ? 1 : 0,
            account_type => (0 == $id % 2) ? 'agency' : 'employer',
        }
}

my $company_api_client_module = Test::MockModule->new('Bean::Juice::APIClient::Company');
    $company_api_client_module->mock( get_company => sub {
        my ($self,$company) = @_;
        return $juice_companies->{$company} ? $juice_companies->{$company} : undef;
});

my $user_api_client_module = Test::MockModule->new('Bean::Juice::APIClient::User');
    $user_api_client_module->mock( company_users => sub {
        my ($self,$company) = @_;
        return $companies_users->{$company} ? @{$companies_users->{$company}} : undef;
});



# Run a full report
{
    ok( $stream2->factory('RecurringReport') , "Ok got recurring report factory");
    ok( my $report = $stream2->factory('RecurringReport')->create({
        next_run_datetime => $now,
        class => 'Stream2::Role::Report::UsageReport',
        settings => {ignore_actions => { download => 1}},
    }) , "Ok can create a report");

    ok( my $files = $report->generate_files(), 'the reports ran.' );
    is( $files->[0]->mime_type() , 'application/json', "Ok first file returned is a json file");
    is( $files->[1]->mime_type() , 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' , "Ok second file is excel");
    my $json = $stream2->json()->decode($files->[0]->binary_content);
    for my $report_company (qw(acme0 acme1 acme2)){
        ok($json->{$report_company},
            "$report_company has a record.")
    }
}

# Run a report that filters actions
{
    ok( $stream2->factory('RecurringReport') , "Ok got recurring report factory");
    ok( my $report = $stream2->factory('RecurringReport')->create({
        next_run_datetime => $now,
        class => 'Stream2::Role::Report::UsageReport',
        settings => {ignore_actions => { download => 1}},
    }) , "Ok can create a report");

    ok( my $files = $report->generate_files(), 'the reports ran.' );
    is( $files->[0]->mime_type() , 'application/json', "Ok first file returned is a json file");
    is( $files->[1]->mime_type() , 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' , "Ok second file is excel");
    my $json = $stream2->json()->decode($files->[0]->binary_content);
    for my $report_company (qw(acme0 acme1 acme2)){
        is($json->{$report_company}->{actions}->{download},
            undef,"$report_company does not have a download action")
    }
}

#  Run a report that filters companies
{
    ok( $stream2->factory('RecurringReport') , "Ok got recurring report factory");
    ok( my $report = $stream2->factory('RecurringReport')->create({
        next_run_datetime => $now,
        class => 'Stream2::Role::Report::UsageReport',
        settings => {filter_company_on => {billable => 1}},
    }) , "Ok can create a report");

    ok( my $files = $report->generate_files(), 'the reports ran.' );
    is( $files->[0]->mime_type(),
        'application/json',
        "Ok first file returned is a json file");
    is( $files->[1]->mime_type(),
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        "Ok second file is excel");
    my $json = $stream2->json()->decode($files->[0]->binary_content);
    is($json->{acme1},undef,'Acme1 has been filtered out');
}

#  Run a report that filters out all the company data, as there are no zombies in
#  my data set looking for companies with one zombie will filter out all the companies
{
    ok( $stream2->factory('RecurringReport') , "Ok got recurring report factory");
    ok( my $report = $stream2->factory('RecurringReport')->create({
        next_run_datetime => $now,
        class => 'Stream2::Role::Report::UsageReport',
        settings => {filter_company_on => {zombies => 1}},
    }) , "Ok can create a report");

    ok( my $files = $report->generate_files(), 'the reports ran.' );
    is( $files->[0]->mime_type(),
        'application/json',
        "Ok first file returned is a json file");
    is( $files->[1]->mime_type(),
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ,
        "Ok second file is excel");
    my $json = $stream2->json()->decode($files->[0]->binary_content);
    is($json->{error},
        'It seems no data usage data was available when generating a usage report.',
        'has an error, as all comapanies have been filtered.');
}

done_testing();

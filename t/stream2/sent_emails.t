use Test::Most;
use Test::MockModule;
use Class::MOP;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

use Stream2;
### tests for Stream2::DBICWrapper::SentEmail

# deploy schema for duration of this test
my $config_file = 't/app-stream2.conf';
my $stream2     = Stream2->new( { config_file => $config_file } );
my $schema      = $stream2->stream_schema;
$schema->deploy;

### create test data
my $emails_factory = $stream2->factory('SentEmail');
my $users_factory  = $stream2->factory('SearchUser');

# create 2 acme users, 1 samsung user
my %users;
for ( [ acme => 'Annie' ], [ acme => 'Arnie' ], [ samsung => 'Rav' ] ) {
    my $result = $users_factory->new_result( {
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => keys(%users) + 1,     # just needs to be unique
        contact_name        => $_->[1],
        group_identity      => $_->[0],
    } );
    $result->save;
    $users{ $_->[1] } = $result;
}

my $messages = [
    {
        user           => $users{Annie},
        sent_date      => \'NOW()',
        subject        => 'Job Opportunity',
        recipient      => 'jsmith@example.com',
        recipient_name => 'John Smith',
        aws_s3_key     => 'an1',
        plain_body     => 'it has been a lovely day',
    },
    {
        user           => $users{Annie},
        sent_date      => \'NOW()',
        subject        => 'LovElY Job Opportunity',
        recipient      => 'pb123@example.com',
        recipient_name => 'Paula Bastable',
        aws_s3_key     => 'an2',
        plain_body     => 'Hello. Please call if interested.',
    },
    {
        user           => $users{Annie},
        sent_date      => \'NOW()',
        subject        => 'Free to Discuss?',
        recipient      => 'tm@example.com',
        recipient_name => 'Ted Maul',
        aws_s3_key     => 'an3',
        plain_body     => 'Let\'s speak at 14:45'
    },
    {
        user           => $users{Arnie},
        sent_date      => \'NOW()',
        subject        => 'Positions Ahoy',
        recipient      => 'ddavis3@example.com',
        recipient_name => 'Dave Davis',
        aws_s3_key     => 'ar1',
        plain_body     => 'Hello. Paula mentioned you.'
    },
    {
        user           => $users{Rav},
        sent_date      => \'NOW()',
        subject        => 'New Positions',
        recipient      => 'pb123@example.com',
        recipient_name => 'Paula Bastable',
        aws_s3_key     => 'ra1',
        plain_body     => 'I think you\'ll be great at Octagon'
    },
];

my %emails;
for my $message ( @{$messages} ) {

    my $sender = $message->{user};

    # each SentEmail must have a CandidateActionLog (FK constraint)
    my $action = $stream2->factory('CandidateActionLog')->create({
        action         => 'message',
        user_id        => $sender->id,
        candidate_id   => 1,
        destination    => "sausage",
        group_identity => $sender->group_identity
    });

    my $msg_result = $stream2->factory('SentEmail')->create({
        user_id        => $sender->id,
        action_log_id  => $action->id,
        sent_date      => $message->{sent_date},
        subject        => $message->{subject},
        recipient      => $message->{recipient},
        recipient_name => $message->{recipient_name},
        aws_s3_key     => $message->{aws_s3_key},
        plain_body     => $message->{plain_body},
    });

    my @msg_per_sender = $emails{ $sender->contact_name } //= ();
    push @msg_per_sender, $msg_result;
}

# tests for search_user_emails()
my $emails_rs = $emails_factory->search_user_emails(
                    $users{Annie}, undef, { page => 1, rows => 10 } );
cmp_ok $emails_rs->count, '==', 3, 'Annie has sent 3 emails';

$emails_rs = $emails_factory->search_user_emails(
                    $users{Rav}, undef, { page => 1, rows => 10 } );
cmp_ok $emails_rs->count, '==', 1, 'Rav has sent 1 email';

my $keywords = 'lovely';
$emails_rs = $emails_factory->search_user_emails(
                    $users{Annie}, $keywords, { page => 1, rows => 10 } );
cmp_ok $emails_rs->count, '==', 2,
    "Annie sent 2 emails matching '$keywords'";

# tests for search_group_emails()
$emails_rs = $emails_factory->search_group_emails(
                    $users{Arnie}, undef, { page => 1, rows => 10 } );
cmp_ok $emails_rs->count, '==', 4, "Arnie's company has sent 4 emails";

$emails_rs = $emails_factory->search_group_emails(
                    $users{Rav}, undef, { page => 1, rows => 10 } );
cmp_ok $emails_rs->count, '==', 1, "Rav's company has sent 1 email";

$keywords = 'paula';
$emails_rs = $emails_factory->search_group_emails(
                    $users{Annie}, $keywords, { page => 1, rows => 10 } );
cmp_ok $emails_rs->count, '==', 2,
    "Annie's company has sent 2 emails matching '$keywords'";

done_testing();

#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Log::Log4perl qw/:easy/;
Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
Log::Any::Adapter->set('Log::Log4perl');

use Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

unless( $ENV{TEST_ORIGINAL_FEEDS} ){
    plan skip_all => 'No ENV TEST_ORIGINAL_FEEDS . Skipping';
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

# Deploying schema
$stream2->stream_schema()->deploy();

ok( -d $stream2->original_templates_dir() , "Ok can access the original templates dir");
ok( $stream2->original_templates->clear_all() , "Ok can clear all");

ok( $stream2->original_templates() , "Ok can get original templates object");
ok( $stream2->original_templates()->packages_dir() , "Ok packages dir");
ok( $stream2->original_templates()->results_dir() , "Ok results dir");
ok( $stream2->original_templates()->profiles_dir() , "Ok profiles dir");

ok( $stream2->original_templates->slurp() , "Ok can slurp original templates");

ok( $stream2->original_templates->clear_all() , "Ok can clear all");

done_testing();

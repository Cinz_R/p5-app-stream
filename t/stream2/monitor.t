#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;

use Stream2;

use Log::Any::Adapter;
#Log::Any::Adapter->set('Stderr');

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();


# Try the backend locking mechanism
my $schema = $stream2->stream_schema();

ok( $schema->try_acquiring_lock('funky_lock') , "Ok can acquire funky lock");

dies_ok { $schema->try_acquiring_lock('any_other_stuff') ; }  "Ok dies trying to acquire another lock in the same session";
# Try another connection

my $clone = $stream2->clone();
ok( ! $clone->stream_schema->try_acquiring_lock('funky_lock') , "Ok can NOT acquire funky lock");


ok( $schema->release_lock('funky_lock') , "Ok can release this lock");

ok( $clone->stream_schema->try_acquiring_lock('funky_lock') , "Ok a clone can now acquire it");

ok( ! $schema->try_acquiring_lock('funky_lock') , "The current one cannot acquire it");

ok( $clone->stream_schema->release_lock('funky_lock') , "Ok clone releases it");
ok( $schema->try_acquiring_lock( 'funky_lock') , "Main object can acquire it");
ok( $schema->release_lock('funky_lock') , "Ok schema can release");


# Now try the upper level (monitor)
{
    my $executed;
    $stream2->monitor()->try_mutex('my_key' , sub{ return $executed++ ;} , sub{ die "Alternative should not be run"; });
    ok( $executed );
}

{
    # Try something that dies.
    dies_ok { $stream2->monitor()->try_mutex('my_key' , sub{ die "Some error" ;} , sub{ die "Alternative should not be run"; }); } "Ok dies";

    my $executed;
    $stream2->monitor()->try_mutex('my_key' , sub{ return $executed++ ;} , sub{ die "Alternative should not be run"; });
    ok( $executed, "And the key was released just fine");

    # Now use the clone to lock the key
    ok( $clone->stream_schema->try_acquiring_lock('my_key') , "Ok clone can acquire key");

    # Now try the mutex
    my $alt_executed = 0;
    $stream2->monitor()->try_mutex('my_key' , sub{ die "Should not be executed (my_key is taken)" ;} , sub{ $alt_executed++ });
    ok( $alt_executed , "Ok the alternative was executed");

    ok( $clone->stream_schema->release_lock('my_key') , "Ok clone can release the lock");

}


done_testing();

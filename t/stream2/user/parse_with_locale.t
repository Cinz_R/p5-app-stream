use Test::Most;
use Test::MockModule;
use Test::Mojo::Stream2;

my $t = Test::Mojo::Stream2->new();

my $cvp_mock = Test::MockModule->new('Bean::CVP::Client');

my $u = $t->app->stream2->factory('SearchUser')->new_result({
    settings => {
        responses => {
            bg_language_pack => 'us'
        }
    },
    provider => 'dummy',
    last_login_provider => 'dummy',
    provider_id => 31415,
    group_identity => 'acme',
    subscriptions_ref => {}
});

subtest 'locale is heeded and return is propagated' => sub {
    plan tests => 2;
    $cvp_mock->mock('parse_cv_contents' => sub {
        my ( $self, $content, $options ) = @_;
        is ( $options->{tagger_locale}, 'en_us' );
        return { a => 1, b => 2 };
    });
    is_deeply( $u->parse_cv_with_locale( 'test' ), { a => 1, b => 2 } );
};

subtest 'locale is not mandatory' => sub {
    plan tests => 2;
    delete $u->settings()->{responses}->{bg_language_pack};
    $cvp_mock->mock('parse_cv_contents' => sub {
        my ( $self, $content, $options ) = @_;
        is_deeply( $options, { conversion => 'hr_xml' } );
        return 'string';
    });
    is( $u->parse_cv_with_locale( 'test' ), 'string' );
};

subtest 'locale is unknown' => sub {
    plan tests => 2;
    $u->settings()->{responses}->{bg_language_pack} = 'made_up';
    $cvp_mock->mock('parse_cv_contents' => sub {
        my ( $self, $content, $options ) = @_;
        is_deeply( $options, { conversion => 'hr_xml' } );
        return 'string';
    });
    is_deeply( $u->parse_cv_with_locale( 'test' ), 'string' );
};

subtest 'conversion is set from ripple_settles' => sub {
    plan tests => 2;
    $u->ripple_settings->{tagged_doc_type} = 'BGT';
    $cvp_mock->mock('parse_cv_contents' => sub {
        my ( $self, $content, $options ) = @_;
        is_deeply( $options, { conversion => 'bg_xml' } );
        return 'string';
    });
    is_deeply( $u->parse_cv_with_locale( 'test' ), 'string' );
};

done_testing;

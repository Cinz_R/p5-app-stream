#! perl

use Test::Most;
use Test::MockModule;

use Data::Dumper;

use Stream2;

use Stream2::Action::SynchroniseAdCourierUserSiblings;

# use Log::Any::Adapter qw/Stderr/;


my $config_file = 't/app-stream2.conf';
ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();

my $user_api_mock = Test::MockModule->new( 'Bean::Juice::APIClient::User' );

my $users_factory = $stream2->factory('SearchUser');

my $logged_in_user = $users_factory->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 1,
    group_identity => 'acme',
});
$logged_in_user->save();


my $company = $logged_in_user->group_identity;

my $test_prototype = {
        users => [
            {
                id => 2,
                name => "Johnny Winter",
                email => 'johnny.winter@example.com',
                company => $company,
            },
            {
                id => 3,
                name => "Muddy Waters",
                email => 'muddy.waters@example.com',
                company => $company,
            },
            {
                id => 4,
                name => "Eric Clapton",
                email => 'eric.clapton@example.com',
                company => $company,
            }
        ],
        parameters => {
            adc_id  => 1,
            company => $company,
        },
        expected_ids => [qw/2 3 4/],
};

my @tests = (
    {
        # test 1
        testname => 'normal sync',
        add_users => [qw/5 6 7/], # add old users
        expected_ids => [qw/1 2 3 4 5 6 7/] # don't remove old users as this is not an admin
    },
    {
        # test 2 - remove extras
        testname => 'removed extras',
        parameters => {
            adc_id  => 1,
            company => $company,
            clean_missing => 1,
        },
        add_users => [qw/2 3 4 5 6 7 8 9 10/], # add old users
        expected_ids => [qw/1 2 3 4 5 6 7 8 9 10/], # only these should exist at the end
    }
);

foreach my $test ( @tests ){
    $test = { %$test_prototype, %$test };
    diag($test->{testname});
    $user_api_mock->mock( company_users => sub { return @{$test->{users}}; } );

    my $SyncAdCusersibs = Stream2::Action::SynchroniseAdCourierUserSiblings->new(
                                                stream2     => $stream2,
                                                jobid       => 'doesnt matter',
                                                %{$test->{parameters}}
                                                );
    if ( $test->{add_users} ){
        # delete all existing cached users
        $users_factory->search({ id => { '!=' => 1 }})->delete();

        foreach my $add_id ( @{$test->{add_users}} ){
            my $u = $users_factory->new_result({
                    'contact_details'   => {
                        contact_name    => "doesnt matter",
                        contact_email   => "doesnt matter"
                    },
                    'provider'            => 'adcourier',
                    'last_login_provider' => 'adcourier',
                    'provider_id'       => $add_id,
                    'group_identity'    => $test->{parameters}->{company},
                    'group_nice_name'   => $test->{parameters}->{company},
                    'settings'          => {},
                    'subscriptions_ref' => {},
            });
            $u->save;
        }

        my $ids_should_be_there_rs = $users_factory->search({
            group_identity  => $test->{parameters}->{company},
            id              => { '!=' => 1 }
        });

        is(
            $ids_should_be_there_rs->count,
            scalar( @{$test->{add_users}} ),
            "users have been setup for test"
        );
    }

    ok( $SyncAdCusersibs->instance_perform(), "job ran successfully" );

    is ( $users_factory->count(), scalar( @{$test->{expected_ids}} ), "the correct amount of users exist" );

    foreach my $user ( @{$test->{users}} ){

        my $db_user = $users_factory->search({ contact_name => $user->{name} })->first();

        is( $db_user->get_column('contact_name'), $user->{name}, "contact detals are correct" );
        is( $db_user->get_column('contact_email'), $user->{email}, "contact detals are correct" );
        is( $db_user->get_column('group_identity'), $user->{company}, "group identity is correct" );
        is( $db_user->get_column('provider_id'), $user->{id}, "provider id is correct" );
    }

    if ( $test->{add_users} ){
        my $ids_shouldnt_be_there_rs = $users_factory->search({
            group_identity => $company,
            provider_id => { -not_in => $test->{expected_ids} }
        });
        is ( $ids_shouldnt_be_there_rs->count, 0, "clean all missing ids" );

        my $ids_should_be_there_rs = $users_factory->search({
            group_identity => $company
        });
        is(
            $ids_should_be_there_rs->count,
            scalar( @{$test->{expected_ids}} ),
            "the correct amount of users are in the DB"
        );
    }
}

done_testing();

use Test::Most;

use Test::MockModule;
use Log::Any::Test;
use Log::Any qw($log);
use Email::Simple;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

use Test::Stream2;
use Stream2::Criteria;
use Stream::Templates::Default;

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();

my @results = (
    Stream2::Results::Result->new({
        candidate_id => 1,
        email => 'cameron@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Tokyo',
        name => 'David Cameron'
    }),
    Stream2::Results::Result->new({
        candidate_id => 2,
        email => 'may@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Djibouti',
        name => 'Theresa May'
    }),
    Stream2::Results::Result->new({
        candidate_id => 3,
        email => 'brown@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Sandringham',
        name => 'Gordon Brown'
    }),
    Stream2::Results::Result->new({
        candidate_id => 4,
        email => 'jezza@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Woodstock',
        name => 'Jeremy Corbyn'
    })
);

my $feed_mock = Test::MockModule->new( 'Stream::Templates::google_plus' );
$feed_mock->mock( search => sub {
    my $self = shift;
    $self->total_results( scalar( @results ) );
    $self->results->add_candidate( @results );
    return @results;
});
$feed_mock->mock( quota_guard => sub { pop->() } );

# results store that doesn't go to redis
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { shift->results; } );
$results_mock->mock( fetch_results => sub { @results } );

my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( increment_analytics => sub { 1; } );
$stream2_mock->mock( log_search_calendar => sub { 1; } );
$stream2_mock->mock( redis => sub { MyRedis->new(); } );


my $mocked_search_user = Test::MockModule->new('Stream2::O::SearchUser');
$mocked_search_user->mock( exists_remotely => sub { return 1; } );

$stream2->factory('EmailBlacklist')->create(
    {
        email   => 'Gertrude@sugarplumfairy.com',
        purpose => 'delivery.spam'
    }
);

my $schema = $stream2->stream_schema();

# Build an empty criteria
my $criteria = Stream2::Criteria->new( schema => $schema );
$criteria->add_token( 'cv_updated_within', '3M' );
$criteria->save();


# Time to create a user.
my $users_factory = $stream2->factory('SearchUser');

my $user_id;
my $memory_user;
{
    $memory_user = $users_factory->new_result(
        {
            provider            => 'adcourier',
            last_login_provider => 'adcourier_ats',
            provider_id         => 31415,
            group_identity      => 'acme',
            contact_email       => 'Gertrude@sugarplumfairy.com',
            contact_details     => {},
            subscriptions_ref   => {
                google_plus => {
                    nice_name   => 'Groogle plus',
                    auth_tokens => {},
                    type        => "social",
                },
            }
        }
    );
    ok( $user_id = $memory_user->save(), "Ok can save this user" );
    is(
        $schema->resultset('CvSubscription')->search( { user_id => $user_id } )
          ->count(),
        1,
        "Ok one subscriptions in the DB"
    );
}

my $snoopy = $stream2->watchdogs->create(
    {
        user_id     => $user_id,
        name        => 'Snoopy',
        criteria_id => $criteria->id(),
        base_url    => 'http://boudin.net/'
    }
);

my $google_plus =
  $memory_user->subscriptions()->find( { board => 'google_plus' } );

$snoopy->add_to_watchdog_subscriptions( { subscription => $google_plus } );

# Try to initialize it.
$stream2->watchdogs()->runner($snoopy)->initialize();

foreach my $subscription ( $snoopy->watchdog_subscriptions()->all ) {
    ok( $subscription->watchdog_results()->count(),
        "Subscription has results" );
    ok( $subscription->watchdog_results->first()->viewed(),
        "result is set as viewed" );
}

# # Now we need to run this watchdog and see that an email has been sent.
# # Fiddle twith the subscription results so two of each are new.
foreach my $subscription ( $snoopy->watchdog_subscriptions()->all ) {
    $subscription->watchdog_results()->search( undef, { rows => 2 } )
      ->delete_all();
    ok( $subscription->watchdog_results()->count(),
        "We still have some results" );
}
use Stream2::Action::WatchdogRun;
my $job = MyJob->new(
    {
        class      => 'Stream2::Action::WatchdogRun',
        parameters => {
            watchdog_id => $snoopy->id,
            real_run    => 1
        }
    }
);
$job->{stream2} = $stream2;

# lets se tthe job off and see what happens
ok( Stream2::Action::WatchdogRun->perform($job), "job is completed" );

#  no emails should be sent
my @deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries), 0, 'there should be no emails' );
$log->contains_ok(
qr/An error occured while running watchdog 1: Messaging error: The recipient \(Gertrude/,
    "There was no email as Gertrude's email is marked as spam"
);

done_testing();

{
    package MyJob;
    use base qw/Qurious::Job/;
    sub save { 1; }
}

{
    package MyRedis;
    sub rpush { 1; }
    sub expire { 1; }
    sub new { return bless {}; }
}

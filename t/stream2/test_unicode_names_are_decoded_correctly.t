#! perl
use strict;
use warnings;

use utf8;
use FindBin;
use Test::More;

use Stream2;

use Log::Any::Adapter;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");
# Deploying schema
$stream2->stream_schema()->deploy();

my $users_factory = $stream2->factory('SearchUser');

# Time to create a user.
my $memory_user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
    contact_name => 'Cinz PoŁAnd'
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");

my $user = $stream2->find_user($user_id);

ok( utf8::is_utf8( $user->contact_name));
is( $stream2->translations()->__x('Hello {name}', name => $user->contact_name),'Hello Cinz PoŁAnd', "Check to see if unicode names are decoded correctly");



done_testing;
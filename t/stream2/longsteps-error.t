#! perl -w

use strict;
use warnings;

use Test::More;

# use Log::Any::Adapter qw/Stderr/;

use Test::Stream2;

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}


{
    package My::TestProcess;
    use Moose;
    use DateTime;
    extends qw/Stream2::Process/;

    sub build_first_step{
        my ($self) = @_;
        return $self->new_step({ what => 'do_stuff1' , run_at => DateTime->now() });
    }
    sub do_stuff1{
        my ($self) = @_;
        die "SOMETHING HORRIBLE HAPPENS";
    }
}


my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

ok( $stream2->deploy_test_db, "Ok can run deploy" );

ok( my $longstep = $stream2->longsteps(), "Ok can build longsteps");

ok( my $p = $longstep->instantiate_process('My::TestProcess',
                                           { stream2 => $stream2 },
                                       )
        , "Ok can instantiate process");

$longstep->run_due_processes({ stream2 => $stream2 });

$p->discard_changes();
like( $p->error() , qr/SOMETHING HORRIBLE/ );

my @deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries)  , 1,  "Ok error email delivered");


done_testing();

#! perl
# Test a basic search (on the dummy search template).

use strict;
use warnings;

use FindBin;
use Test::More;

use Data::UUID;

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

use Test::Stream2;
use Email::Sender::Simple;

# use Log::Any::Adapter qw/Stderr/;

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();

my $ug = Data::UUID->new();

my $users_factory = $stream2->factory('SearchUser');

# Create a test user.
my $user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
$user->save();

ok( my $template = $stream2->build_template('dummy', undef , { user_object => $user }) , "Ok got dummy template");

my $results_id = $ug->create_str();

ok( my $results_collector = Stream2::Results->new({
                                                   id          => $results_id,
                                                   destination => 'dummy',
                                                  }),
    "Ok can build a result collector");

ok( $results_collector->add_notice('Some traditional notice') );
is_deeply( $results_collector->notices() , [ { level => 'warning', message => 'Some traditional notice' } ]);
ok( $results_collector->add_notice({ level => 'info' , message => 'Some extra information' } ) );
is_deeply( $results_collector->notices() , [ { level => 'warning', message => 'Some traditional notice' },
                                             { level => 'info' , message => 'Some extra information' }
                                             ]);

# Inject the result collector in the template object.
$template->results($results_collector);

# Set some user value.
ok( $template->account_value('somekey' , 'some value') , "Ok can set some value against the user account through the engine");
is( $template->account_value('somekey') , 'some value' , "Ok can retrieve the same value");


# And search!
$template->search({});

# Check we now have results
ok( scalar(@{$results_collector->results()}), "Ok got results in the results collector");

# Build a MIME::Entity for the first result.
ok( my $email = $results_collector->find(0)->build_target_entity() , "Ok got email");
$email->head->replace( From => 'jerome@broadbean.com' );

ok( $stream2->send_email($email) , "Ok can send_email");

my @deliveries = Email::Sender::Simple->default_transport->deliveries;
ok( scalar(@deliveries)  , "Ok got deliveries");


{
    # Tag this candidate through a template's method.
    # Note that the template already knows the current user_object.
    my $candidate = $results_collector->find(0);
    ok( ! $candidate->make_email_up( $user ) , "Ok making an email up has no effect");
    # Void the email address
    $candidate->email('');
    ok( $candidate->make_email_up( $user ) , "This time we can make one up");

    ok( $template->mark_candidate_contacted($candidate) , "Ok marking a candidate contacted");
    ok( $template->tag_candidate($candidate, create_tag($candidate, 'shiny tag')) , "Ok can tag");
    ok( $template->tag_candidate($candidate , create_tag($candidate, '25% yogurt')) , "Ok can tag with a %");
    is( $stream2->factory('Tag')->autocomplete('acme', 'shi')->count() , 1 , "Ok tag autocomplete count is 1");
    is( $stream2->factory('Tag')->autocomplete('acme', '25% y')->count() , 1 , "Ok tag autocomplete count is 1");

    ok( grep { $_ eq 'shiny tag' } $candidate->tags() , "Ok got shiny tag in object too");

    ok( ! $template->untag_candidate($candidate, find_tag('not so shiny')) , "Ok can untag a non-existing tag, returns nothing");
    ok( $template->untag_candidate($candidate, find_tag('shiny tag')) , "Ok can untag an existing tag. Returns the removed tag");
    ok( ! grep { $_ eq 'shiny tag' } $candidate->tags() , "Ok no shiny tag left against the candidate");


    # Tag the second candidate with something completely different.
    my $second_candidate = $results_collector->find(1);
    ok( $template->tag_candidate($second_candidate , create_tag($second_candidate, 'cheap')), "Ok tagged second candidate");
    ok( $template->tag_candidate($second_candidate , create_tag($second_candidate, 'as')), "Ok tagged second candidate");
    ok( my $cand_tag =  $template->tag_candidate($second_candidate , create_tag($second_candidate, 'chipss')), "Ok tagged second candidate");
    ok( my $chips_tag = $cand_tag->tag() , "Ok the candidate tag has got a tag");
    ok(! $chips_tag->flavour() , "Ok this tag does not have a flavour");

    # Set a flavour against the chipss.
    my $flavour = $stream2->stream_schema->resultset('TagFlavour')->find_or_create({ group_identity => 'acme' , name => 'hotlist' });
    $chips_tag->flavour($flavour);
    $chips_tag->update();
    ok( $chips_tag->flavour() , "Ok the chipss tag has got a flavour");

    {
        # Check we can separate hotlist ones from normal ones.
        my ($hot, $normal) = $stream2->factory('Tag')->split_hotlist_tags('acme' , [ 'fish', 'and', 'chipss' ]);
        is_deeply($hot, [ 'chipss' ]);
        is_deeply($normal, [ 'fish' , 'and' ]);
    }

    # Delete a flavour from a tag.
    $chips_tag->flavour(undef);
    $chips_tag->update();
    ok( ! $chips_tag->flavour(), "Ok no flavour in chips_tag");
}


{
    # Search for candidates again and check enriching them with tags just work.
    my $results_id = $ug->create_str();

    ok( my $results_collector = Stream2::Results->new({
                                                       id          => $results_id,
                                                       destination => 'dummy',
                                                      }),
        "Ok can build a result collector");
    # Inject the result collector in the template object.
    $template->results($results_collector);
    $template->search({});

    my $candidates = $results_collector->results();

    is( $candidates->[0]->{tags} , undef , "Ok no tags in candidate 0");
    is( $candidates->[1]->{tags} , undef , "Ok no tags in candidate 1");

    # Put the itchy and the scratchy tag in candidate 2.
    $candidates->[2]->{tags} = [ 'itchy' , 'scratchy' ];

    ok(! $stream2->stream_schema->resultset('Tag')->find({ group_identity => 'acme' , tag_name => 'itchy' }) , "Ok no itchy tag");
    ok(! $stream2->stream_schema->resultset('Tag')->find({ group_identity => 'acme' , tag_name => 'scratchy' }) , "Ok no scratchy tag");

    my $flavour = $stream2->stream_schema->resultset('TagFlavour')->find_or_create({ group_identity => 'acme' , name => 'Hot' });
    my $chips_tag = $stream2->stream_schema->resultset('Tag')->find({ group_identity => 'acme' , tag_name => 'chipss' });
    $chips_tag->flavour($flavour);
    $chips_tag->update();

    # the board has returned a tags attribute for the first candidate!
    # we should add this to the DB and then add other tags from the DB
    $candidates->[0]->{tags} = [{ value => 'not in the database', flavour => undef, label => 'not in the database' }];

    # Create the itchy tag. That should prevent it to be added to the database for the candidate 2.
    $stream2->stream_schema->resultset('Tag')->create({ group_identity => 'acme' , tag_name => 'itchy' , tag_name_ci => 'itchy' });

    $stream2->factory('Tag')->enrich_candidates($candidates , { user => $user } );

    is_deeply( $candidates->[0]->{tags} , [{ value  => '25% yogurt' , flavour => undef, label => '25% yogurt' }, { value => 'not in the database', flavour => undef, label => 'not in the database' }]  , "Ok no tags in candidate 0");
    is_deeply( $candidates->[1]->{tags} , [{ value => 'as' , flavour => undef, label => 'as' } , { value => 'cheap' , flavour => undef, label => 'cheap' },
                                           { value => 'chipss' , flavour => 'Hot', label => 'chipss' } ] , "candidate has appropriate tags as cheap as chipss");
    ok( $stream2->factory('CandidateTag')->search({ tag_name => 'itchy', candidate_id => $candidates->[2]->id() })->first() , "Ok can find itchy tag against the candidate");
    is_deeply( $candidates->[2]->{tags} , [{ value => 'itchy' , flavour => undef, label => 'itchy' },
                                           { value => 'scratchy' , flavour => undef, label => 'scratchy' }] , "Tags were enriched even if they were not in the DB.");
    ok( $stream2->stream_schema->resultset('Tag')->find({ group_identity => 'acme' , tag_name => 'scratchy' }) , "Ok scratchy tag is there now");
    ok( my $tag = $stream2->stream_schema->resultset('Tag')->find({ group_identity => 'acme' , tag_name => 'not in the database', label => 'not in the database' }), "Tag which wasn't previously in the DB is now in the database as it existed in the candidate on the board");
    is ( $tag->candidate_tags->count(), 1, "only one candidate has this new tag attributed to it" );
}

sub create_tag {
    my ($candidate, $tag_name) = @_;
    my $stuff = sub {
        my $tag = $stream2->factory('Tag')->find_or_create({
            group_identity  => $user->group_identity(),
            tag_name        => $tag_name,
            # Yes this is rididulous,
            # as we have a BEFORE INSERT trigger that calculates
            # this thing for us. So we should not need to give it.
            # However, MySQL being MySQL, it checks that this is NOT NULL,
            # BEFORE the BEFORE trigger has been triggered. Lost 2 hours
            # figuring that out. Thanks MySQL.
            tag_name_ci     => $tag_name,
        });

        my $candidate_tag = $stream2->stream_schema->resultset('CandidateTag')->find_or_create({
            group_identity  => $user->group_identity(),
            candidate_id    => $candidate->id(),
            tag_name        => $tag->tag_name,
        });
        $candidate_tag->user_id($user->id);
        $candidate_tag->update();

        $candidate->add_tag($tag_name);
        return $candidate_tag;
    };
    return $stream2->stream_schema->txn_do($stuff);
}

sub find_tag {
    my ($tag_name) = @_;
    return $stream2->stream_schema->resultset('CandidateTag')->find({
        group_identity  => $user->group_identity(),
        tag_name        => $tag_name
    });
}

done_testing();

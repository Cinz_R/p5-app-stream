#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Log::Any;
# use Log::Any::Adapter qw/Stderr/;

use Stream2;

use Email::Simple;

# to deploy email_blacklist table
use Test::Mojo::Stream2;
my $t = Test::Mojo::Stream2->new();
$t->login_ok;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file:'".( $config_file || 'UNDEF')."'";
}

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

ok( my $stream2 = $t->app->stream2 , "Ok can build stream2");


is( $stream2->report_tech([], sub{ return 'A scalar'; }) , 'A scalar' , "Ok works for working stuff");

{
    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries)  , 0 ,  "No email sent");
}

like( $stream2->report_tech([], sub{ my ($s2) = @_ ; return $s2->json->encode(['boudin_blanc']); }) , qr/boudin_blanc/ , "Ok can use s2 inside the code");

{
    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries)  , 0 ,  "No email sent");
}


is( $stream2->report_tech([ 'boudin_blanc' ] , sub{ my ($s2 , $str) = @_; return $str ; }) , 'boudin_blanc' , "Ok passing arguments works");

{
    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries)  , 0 ,  "No email sent");
}

is( $stream2->report_tech([123456], sub{ my ($s2); die "Hahaha"; }) , undef  , "A crash leads to undef");

{
    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries)  , 1 ,  "One email sent");
    like( $deliveries[0]->{email}->get_body() , qr/fix me/ , "Body contains fixme");
    like( $deliveries[0]->{email}->get_body() , qr/Hahaha/ , "Body contains the error");
    like( $deliveries[0]->{email}->get_header('Subject') , qr/123456/ , "Subject contains argument");
}

done_testing();

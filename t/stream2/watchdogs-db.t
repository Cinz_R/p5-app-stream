use Test::Most;

use Test::Stream2;
use Test::MockModule;
use Stream2::Criteria;
use Stream2::Action::WatchdogLoad;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();

my @results = (
    Stream2::Results::Result->new({
        candidate_id => 1,
        email => 'cameron@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Tokyo',
        name => 'David Cameron'
    }),
    Stream2::Results::Result->new({
        candidate_id => 2,
        email => 'may@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Djibouti',
        name => 'Theresa May'
    }),
    Stream2::Results::Result->new({
        candidate_id => 3,
        email => 'brown@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Sandringham',
        name => 'Gordon Brown'
    }),
    Stream2::Results::Result->new({
        candidate_id => 4,
        email => 'jezza@downingsteeet.com',
        destination => 'twitter_xray',
        location_text => 'Woodstock',
        name => 'Jeremy Corbyn'
    })
);

my $feed_mock = Test::MockModule->new( 'Stream::Templates::twitter_xray' );
$feed_mock->mock( search => sub {
    my $self = shift;
    $self->total_results( scalar( @results ) );
    $self->results->add_candidate( @results );
    return @results;
});
$feed_mock->mock( quota_guard => sub { pop->() } );

# results store that doesn't go to redis
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { shift->results; } );
$results_mock->mock( fetch_results => sub { @results } );

my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( increment_analytics => sub { 1; } );
$stream2_mock->mock( log_search_calendar => sub { 1; } );
$stream2_mock->mock( redis => sub { MyRedis->new(); } );


my $schema = $stream2->stream_schema;
my $criteria = Stream2::Criteria->new( schema => $schema );

$criteria->add_token( 'keywords' , 'php developer');
$criteria->add_token('cv_updated_within' , 'TODAY' );

$criteria->save();

# Time to create a user.
my $user_id;
my $memory_user;
{
    $memory_user = $stream2->factory('SearchUser')->new_result({
        provider            => 'dummy',
        last_login_provider => 'dummy',
        provider_id         => 31415,
        group_identity      => 'acme',
        subscriptions_ref   => {
            twitter_xray    => {
                nice_name   => "Twitter",
                type        => "social",
                auth_tokens => {}
            }
        }
    });
    ok( $user_id = $memory_user->save() , "Ok can save this user");
}

my $snoopy = $stream2->watchdogs->create({ user_id => $user_id , name => 'Snoopy', criteria_id => $criteria->id(), base_url => 'http://www.example.com' });
my $twitter = $memory_user->subscriptions()->find({ board => 'twitter_xray' });
$snoopy->add_to_watchdog_subscriptions( { subscription => $twitter } );
my $wd_sub = $snoopy->watchdog_subscriptions()->first();

# twitter doesn't support cv_updated_within
$stream2->watchdogs()->runner($snoopy)->initialize();

{
    ok( my $result = $wd_sub->watchdog_results->first(), "there are results for twitter" );
    ok( my $insert_datetime = $result->insert_datetime, "The insert datetime has been set" );
    ok( ! ( my $previous_datetime = $result->previously_seen_datetime ), "the candidate has not been seen previously" );
    ok( my $last_seen_datetime = $result->last_seen_datetime, "the candidate has been seen last" );
    is( $insert_datetime."", $last_seen_datetime."", "The insert datetime is the same as the last seen as this is init" );
    is ( ref( $result->content ), "HASH", "the content column is being inflated correctly" );
}

# wait for 1 second to make sure the datetime has a difference
sleep( 1 );

# do some tests to ensure candidate data exists
$stream2->watchdogs()->runner($snoopy)->run();
{
    ok ( my $result = $wd_sub->watchdog_results->first(), "results still exist after first nightly run" );
    my $insert_datetime = $result->insert_datetime;
    ok ( my $previous_datetime = $result->previously_seen_datetime, "there is now a previous datetime" );
    is ( $insert_datetime."", $previous_datetime."", "previously_seen_datetime is equal to last_seen_datetime ... previously" );
    isnt ( $result->last_seen_datetime."", $previous_datetime."" );
}

{
    my $first = $wd_sub->watchdog_results->search( undef, 
        {
            order_by    =>  [ \'COALESCE( last_seen_datetime, insert_datetime ) DESC', { -asc => [qw/candidate_id/] } ]
        }
    )->first();

    my $wd_load = Stream2::Action::WatchdogLoad->new({
        stream2                 => $stream2,
        template_name           => 'twitter_xray',
        ripple_settings         => {},
        watchdog_subscription   => $wd_sub
    });

    my $result_set = $wd_load->fetch_results();
    is ( $result_set->first->candidate_id, $first->candidate_id, "the watchdog load routine returns the first result correctly" );
    $first->content(undef); $first->update();
    $result_set = $wd_load->fetch_results( $wd_sub );
    my $second_first = $result_set->first;
    isnt ( $result_set->first->candidate_id, $first->candidate_id, "the watchdog load routine ignores results with empty content" );
}

done_testing();

{
    package MyRedis;
    sub rpush { 1; }
    sub expire { 1; }
    sub new { return bless {}; }
}

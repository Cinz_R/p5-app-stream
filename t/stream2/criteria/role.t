#!/usr/bin/env perl

use Test::Most;

{
   package Criteria;

   use Moose;

   has '_id' => ( is => 'rw', isa => 'Str' );
   has '_tokens' => ( is => 'ro', isa => 'HashRef', default => sub { {} } );

   with 'Stream2::Criteria::Role';
};

subtest "All criteria have an ID" => sub {
  my $empty_sha1 = 'vyGp6PvFo4RvsFtPoIWeCReyIC8';

  my $criteria = Criteria->new();
  is $criteria->id, $empty_sha1;
};

subtest "Add / Get / Remove tokens" => sub {
  my $crit = Criteria->new();

  $crit->add_token( token => 'hello' );
  is $crit->get_token('token'), 'hello', "Can fetch an added token";

  $crit->remove_token('token');
  is $crit->get_token('token'), undef, "Can't fetch a token once it has been removed";
  is_deeply [$crit->get_token('token')], [], "Return an empty array if the token doesn't exist";
};

subtest "Changing tokens changes the ID" => sub {
  my $crit = Criteria->new();

  my $empty_id = $crit->id();
  $crit->add_token( token => 'hello' );
  isnt $crit->id, $empty_id, "Adding tokens change the ID";

  $crit->remove_token( 'token' );
  is $crit->id, $empty_id, "Removing tokens changes the ID back";
};

subtest "The same criteria should generate the same ID" => sub {
  my $crit = Criteria->new();
  my $crit2 = Criteria->new();

  $crit->add_token( token => 'value1' );

  isnt $crit->id, $crit2->id, "Different criteria, different ID";

  $crit2->add_token( token => 'value1' );

  is $crit->id, $crit2->id, "Same criteria, same ID";
};

subtest "Exceptions" => sub {
  throws_ok { Criteria->new()->id("not allowed") } qr/readonly/, 'id is readonly';
};

subtest "remove_tokens_starting" => sub {
    plan tests => 3;

    my $crit = Criteria->new();
    $crit->add_tokens(
        board              => 'talentsearch',
        ts_industry        => 'Accountancy',
        totaljobs_industry => 'Accountancy',
    );

    $crit->remove_tokens_starting('ts');

    is($crit->get_token('board'), 'talentsearch', 'Keep global tokens');
    is($crit->get_token('ts_industry'), undef, 'Wipe matching board specific tokens');
    is($crit->get_token('totaljobs_industry'), 'Accountancy', 'Keep other board specific tokens');
};

subtest "all ids must be url safe" => sub {
    my $URL_SAFE_CHARS = qr/[A-Za-z0-9\-_]/;

    my $crit = Criteria->new();

    # generate lots of ids
    my $invalid_ids = 0;
    foreach my $token ( 1 .. 100 ) {
        $crit->add_tokens( $token => 'value' );

        if ( $crit->id() !~ m/^$URL_SAFE_CHARS+$/ ) {
            $invalid_ids++;
        }
    }

    ok( !$invalid_ids, "Every id generates is url safe" );
};

done_testing;

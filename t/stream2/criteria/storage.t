#!/usr/bin/env perl

use utf8;

use Test::Most;

my $EMPTY_CRITERIA_ID = 'vyGp6PvFo4RvsFtPoIWeCReyIC8';

use_ok('Stream2');
use_ok('Stream2::Criteria');

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

sub test_schema{
    my $stream2 = Stream2->new({ config_file => $config_file });
    $stream2->stream_schema()->deploy();
    return $stream2->stream_schema();
}



subtest "saving criteria generates the id" => sub {
  my $schema = test_schema();
  my $criteria = Stream2::Criteria->new( schema => $schema );

  ok( $criteria->save(), 'Saved empty criteria' );
  is( $criteria->id(), $EMPTY_CRITERIA_ID, 'And auto generated the correct id' );
  ok( $criteria->is_empty() , "Ok This criteria is also logically empty");

};

subtest "loading criteria" => sub {
  my $tokens_ref = { key => [ 'value' ] };

  my $schema = test_schema();
  # create an empty criteria
  my $new_criteria = Stream2::Criteria->new( schema => $schema, tokens => $tokens_ref );
  ok( ! $new_criteria->is_empty() , "Ok This criteria is NOT empty");
  $new_criteria->save();

  # and fetch it
  my $criteria = Stream2::Criteria->new(
    schema => $schema,
    id     => $new_criteria->id(),
  )->load();

  ok( ! $criteria->is_empty() , "Ok This criteria is NOT empty");

  is( $criteria->id(), $new_criteria->id(), 'Loaded the criteria' );
  is_deeply( { $criteria->tokens }, $tokens_ref, 'And it has the correct tokens' );
};

subtest "saving the same criteria only creates one db row" => sub {
  my $schema = test_schema();

  # Try and create the same criteria multiple times
  for ( 1 .. 2 ) {
    Stream2::Criteria->new( schema => $schema )->save();
  }

  is( $schema->resultset('Criteria')->count(), 1, 'Criteria is unique, saving duplicates of the same criteria does not create multiple criteria in the store' );
};

subtest "editing new criteria should result in a new db row" => sub {
  my $schema = test_schema();

  my $criteria = Stream2::Criteria->new( schema => $schema );
  $criteria->save();

  ok( $criteria->is_empty() , "Ok This criteria is empty");

  # this should create a new row under a new id
  # instead of updating the existing one
  $criteria->add_token( 'a' => 'token' );
  ok( ! $criteria->is_empty() , "Ok criteria is not empty");
  $criteria->save();

  is( $schema->resultset('Criteria')->count(), 2, 'Saved 2 different criteria from the same object' );
};

subtest "cloning a criteria and editing it should result in a new db row" => sub{
    my $schema = test_schema();

    my $criteria = Stream2::Criteria->new( schema => $schema );
    $criteria->save();
    $criteria->add_token( 'a' => 'token' );
    $criteria->save();

    is( $schema->resultset('Criteria')->count(), 2, 'Saved 2 different criteria from the same object' );

    my $clone = $criteria->clone({ a => 'some other' });
    $clone->add_token('b' , [ 'whatever' ]);
    $clone->save();
    is( $schema->resultset('Criteria')->count(), 3 , 'Now we have 3 different criteria in the DB' );

    {
        my $other_clone = Stream2::Criteria->new(
                                                 id => $clone->id(),
                                                 schema => $schema,
                                                )->load();
        is_deeply( { $other_clone->tokens() } , { a => [ 'some other' ], b => [ 'whatever' ] } , "Ok good saved clone");
    }

};

subtest "The token order does not matter" => sub {
  my $schema = test_schema();

  my %tokens = (
    location_id => '',
    salary_per  => 'annum',
    salary_from => '',
    cvupdatedwithin => 'ALL', # TODO: this should be cv_updated_within
    location_within => '',
    keywords => '',
    salary_to => '',
    salary_cur => 'USD', # TODO: should be GBP
    jobtype => '',
    board => 'talentsearch',
    location => '',
  );

  foreach my $i ( 1 .. 100 ) {
    my $criteria = Stream2::Criteria->new(
      schema => $schema,
    );

    while ( my ($token, $value) = each %tokens ) {
      $criteria->add_token( $token => $value );
    }

    $criteria->save();
  }

  is( $schema->resultset('Criteria')->count(), 1 );
};

subtest "Query Cleaning to get rid of unwanted nonsense" => sub {
    my $schema = test_schema();
    my %tokens = (
        location_id => ['12345'],
        salary_to   => [50000],
        keywords    => ['Do the monster mash 1234!']
    );
    my $criteria = Stream2::Criteria->new(
        schema => $schema,
    );
    while ( my ($token, $value) = each %tokens ) {
      $criteria->add_token( $token => $value );
    }
    $criteria->save();

    is_deeply( {$criteria->tokens}, {%tokens}, "OK tidying normal keywords doesn't break anything" );

    $criteria->remove_token( 'keywords' );
    $criteria->add_token( keywords => 'hello em—dash “blah” …' );
    $criteria->save();

    my $criteria_ref = { $criteria->tokens };

    my $safe_string = 'hello em-dash "blah" ...';

    is( $criteria_ref->{keywords}->[0], $safe_string, "We strip out unwanted characters from the keywords" );

    my $row = $schema->resultset('Criteria')->find( $criteria->id );
    my $row_ref = $row->tokens;

    is ( $row_ref->{keywords}->[0], $safe_string, "We store it nicely in the table as well" );
};

done_testing();

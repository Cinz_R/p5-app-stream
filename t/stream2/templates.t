#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Stream2;

use Email::Simple;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file:'".( $config_file || 'UNDEF')."'";
}

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

is( $stream2->templates->render_simple('boudin [color] [color] tastes [taste]' , { color => 'blanc', taste => 'good' }),
    'boudin blanc blanc tastes good' , "Ok simple template works");

ok( !$stream2->templates->validate_simple('boudin [color] [color] tastes [taste]' , { color => 'blanc', taste => 'good' }) , "Ok good template doesnt trigger errors");
ok(  $stream2->templates->validate_simple('boudin [color] [colo/r] tastes [taste]' , { color => 'blanc', taste => 'good' }) , "Ok bad template gives an error");

done_testing();

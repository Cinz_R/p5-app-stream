#! perl -w

use Test::Most;
use Test::Mojo::Stream2;

use Test::MockDateTime;

use Log::Any qw/$log/;
use Log::Any::Adapter;

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $t = Test::Mojo::Stream2->new();
$t->login_ok;
my $stream2 = $t->app->stream2;

## add normal user
my $u1 = $stream2->factory('SearchUser')->new_result({
        'contact_details'   => {
            contact_name    => "doesnt matter",
            contact_email   => 'jerome1@broadbean.com'
        },
        'provider'          => 'adcourier',
        'last_login_provider' => 'adcourier',
        'provider_id'       => 10,
        'group_identity'    => 'pat',
        'group_nice_name'   => 'pat',
        'settings'          => {
        },
        'subscriptions_ref' => {
            board1 => {
                nice_name => "Board1",
                auth_tokens => {},
                type => "social"
            },
            board2 => {
                nice_name => 'Board2',
                auth_tokens => {},
                type => 'external',
            },
        },
});
$u1->save;

my $schema = $stream2->stream_schema();

# create a criteria
my $criteria1 = Stream2::Criteria->new( schema => $schema );
$criteria1->add_token( 'keywords' , 'manager');
$criteria1->add_token('cv_updated_within' , '3M' );
$criteria1->save();

# create some WDs.
my $wd1 = _create_watchdog( $stream2, $u1, 'u1wd1', $criteria1->id(), [ 'board1' ] );
my $wd2 = _create_watchdog( $stream2, $u1, 'u1wd2', $criteria1->id(), [ 'board1' ] );

is ( $stream2->watchdogs->search()->count(), 2, "2 watchdogs exist" );

# Reload wd1 and wd2 from the DB
$wd1->discard_changes();
$wd2->discard_changes();

# now run the test with jerome1 on the group idenetity list
$stream2->factory('EmailBlacklist')->create({
    email => 'jerome1@broadbean.com',
    purpose => 'spam',
    group_identity => "pat"
});

run_test($stream2,$wd1);

# now we remove the Jerome1 off the Group Identity black list to the global one.
$schema->resultset('EmailBlacklist')->search({
    email => 'jerome1@broadbean.com'
})->delete;

$schema->resultset('EmailBlacklist')->create({
    email => 'jerome1@broadbean.com',
    purpose => 'spam',
});

run_test($stream2,$wd2);

sub run_test {
    my ($s2,$test_wd) = @_;

    is( $test_wd->last_viewed_datetime() , undef, 'new wd viewed is undef' );

    #
    # Set only one watchdog to be last_viewed more than 3 weeks ago
    #
    #
    $test_wd->last_viewed_datetime( DateTime->now()->subtract( days => 7 * 3 + 1 ) );
    $test_wd->update();

    #
    # Now run the inititiate_abandonned_destruction
    #
    is( $s2->watchdogs()->initiate_abandonned_destruction() , 1 ,  "Ok created one desctruction process");

    # Reload the watchdog and do a few checks.
    $test_wd->discard_changes();
    ok( $test_wd->destruction_process_id() , "Ok the first one has got a destruction process running against it");


    # We need to inspect the process as we go.
    my $process = $s2->factory('LongStepProcess')->find( $test_wd->destruction_process_id() );

    {
        # Run the due processes and check an email has been sent
        $s2->longsteps()->run_due_processes( { stream2 => $s2 } );
        $process->discard_changes();

        # failure should not advance the delete
        is( $process->what() , 'do_send_warning_email', 'failure should not advance the delete' );

        my @deliveries = Email::Sender::Simple->default_transport->deliveries;
        is( scalar(@deliveries)  , 0 ,  "Ok no email delivered due to blacklist");
        $test_wd = $s2->watchdogs()->find($test_wd->id());
        is($test_wd->active, 0, 'Watchdog was deactivated due to email address being blacklisted');
    }

}

sub _create_watchdog {
    my ( $s2, $user, $name, $cid, $subs ) = @_;

    my $uid = $user->id;

    my $wd = $s2->watchdogs->create({ user_id => $uid, name => $name, criteria_id => $cid, base_url => 'http://www.example.com/' });

    for my $board_name (@$subs) {
        my $sub = $user->subscriptions()->find({ board => $board_name});
        $wd->add_to_watchdog_subscriptions({ subscription => $sub })
    }

    return $wd;
}

done_testing();

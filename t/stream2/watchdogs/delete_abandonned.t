#! perl -w

use Test::Most;
use Test::Mojo::Stream2;

use Test::MockDateTime;

use Log::Any qw/$log/;
use Log::Any::Adapter;

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $t = Test::Mojo::Stream2->new();
$t->login_ok;
my $stream2 = $t->app->stream2;

## add normal user
my $u1 = $stream2->factory('SearchUser')->new_result({
        'contact_details'   => {
            contact_name    => "doesnt matter",
            contact_email   => 'jerome@broadbean.com'
        },
        'provider'          => 'adcourier',
        'last_login_provider' => 'adcourier',
        'provider_id'       => 10,
        'group_identity'    => 'pat',
        'group_nice_name'   => 'pat',
        'settings'          => {
        },
        'subscriptions_ref' => {
            board1 => {
                nice_name => "Board1",
                auth_tokens => {},
                type => "social"
            },
            board2 => {
                nice_name => 'Board2',
                auth_tokens => {},
                type => 'external',
            },
        },
});
$u1->save;

my $schema = $stream2->stream_schema();

# two different criterias
my $criteria1 = Stream2::Criteria->new( schema => $schema );
$criteria1->add_token( 'keywords' , 'manager');
$criteria1->add_token('cv_updated_within' , '3M' );
$criteria1->save();
my $criteria2 = Stream2::Criteria->new( schema => $schema );
$criteria2->add_token( 'keywords' , 'developer');
$criteria2->add_token('cv_updated_within' , '3M' );
$criteria2->save();

my $u1s1 = $u1->subscriptions()->find({ board => 'board1'});
my $u1s2 = $u1->subscriptions()->find({ board => 'board2'});

my $wd1 = _create_watchdog( $stream2, $u1->id, 'u1wd1', $criteria1->id(), [ $u1s1, $u1s2 ] );
my $wd2 = _create_watchdog( $stream2, $u1->id, 'u1wd2', $criteria2->id(), [ $u1s1, $u1s2 ] );

is ( $stream2->watchdogs->search()->count(), 2, "2 watchdogs exist" );

# Reload wd1 and wd2 from the DB
$wd1->discard_changes();
$wd2->discard_changes();

is( $wd1->last_viewed_datetime() , undef );
is( $wd2->last_viewed_datetime() , undef );

#
# Set only one watchdog to be last_viewed more than 3 weeks ago
#
#
$wd1->last_viewed_datetime( DateTime->now()->subtract( days => 7 * 3 + 1 ) );
$wd1->update();

#
# Now run the inititiate_abandonned_destruction
#
is( $stream2->watchdogs()->initiate_abandonned_destruction() , 1 ,  "Ok created one desctruction process");

# Reload the watchdog and do a few checks.
$wd1->discard_changes(); $wd2->discard_changes();
ok( $wd1->destruction_process_id() , "Ok the first one has got a destruction process running against it");
ok( !$wd2->destruction_process_id() , "Ok second one doesnt have a destruction process running against it");


is( $stream2->watchdogs()->initiate_abandonned_destruction() , 0 ,  "Ok no destruction process created");


# We need to inspect the process as we go.
my $process = $stream2->factory('LongStepProcess')->find( $wd1->destruction_process_id() );

{
    # Run the due processes and check an email has been sent
    $stream2->longsteps()->run_due_processes({ stream2 => $stream2 });

    $process->discard_changes();

    is( $process->what() , 'do_send_final_warning' );

    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries)  , 1 ,  "Ok 1 email delivered");
    is( $deliveries[0]->{envelope}->{to}->[0] , 'jerome@broadbean.com' );
    is( $deliveries[0]->{email}->get_header('X-Stream2-Transport') , 'static_transport' );
}

my $few_days_later = DateTime->now()->add( days => 3 );
$wd1->last_viewed_datetime( $few_days_later );
$wd1->update();

on $process->run_at()->clone()->add( hours => 1 ) => sub{
    $stream2->longsteps()->run_due_processes({ stream2 => $stream2 });
    $process->discard_changes();
    # Because we viewed the watchdog in the meanwhile, the next thing to
    # be done is to send another warning email in three weeks
    is( $process->what() , 'do_send_warning_email' );
    is( DateTime->compare( DateTime->now()->add( days => 14 ) , $process->run_at() ) , -1 , "2 weeks from now is before the next run_at");
};

# Now lets say that no one views the watchdog anymore
on $process->run_at()->clone()->add( hours => 1 ) => sub{
    $stream2->longsteps()->run_due_processes({ stream2 => $stream2 });
    $process->discard_changes();
    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries)  , 2 ,  "Ok a second email delivered");
    is( $process->what() , 'do_send_final_warning');
};

on $process->run_at()->clone()->add( hours => 1 ) => sub{
    $stream2->longsteps()->run_due_processes({ stream2 => $stream2 });
    $process->discard_changes();
    my @deliveries = Email::Sender::Simple->default_transport->deliveries;
    is( scalar(@deliveries)  , 3 ,  "Ok a third email delivered");
    like( $deliveries[2]->{email}->get_body() , qr/friendly reminder/ );

    is( $process->what() , 'do_delete_watchdog');
};


on $process->run_at()->clone()->add( hours => 1 ) => sub{
    is( $stream2->watchdogs()->initiate_abandonned_destruction() , 0 ,  "Ok no extra destruction process created");

    $stream2->longsteps()->run_due_processes({ stream2 => $stream2 });
    $process->discard_changes();
    is( $process->what() , 'do_delete_watchdog', "Good name of last function executed");
    is( $process->status() , 'terminated');
    is( scalar( @{ $process->state()->{warnings_sent} } ) , 3 , "Recorded 3 warnings sent" );
    ok( ! $stream2->factory('Watchdog')->find( $wd1->id() ) , "Ok cannot find the watchdog anymore");
};

sub _create_watchdog {
    my ( $s2, $uid, $name, $cid, $subs ) = @_;
    my $wd = $s2->watchdogs->create({ user_id => $uid, name => $name, criteria_id => $cid, base_url => 'http://www.example.com/' });
    map { $wd->add_to_watchdog_subscriptions({ subscription => $_ }); } @$subs;

    return $wd;
}

done_testing();

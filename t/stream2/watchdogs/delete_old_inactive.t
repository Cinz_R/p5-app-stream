use Test::Most;
use Test::Mojo::Stream2;

my $t = Test::Mojo::Stream2->new();
$t->login_ok;
my $stream2 = $t->app->stream2;

## add normal user
my $u1 = $stream2->factory('SearchUser')->new_result({
        'contact_details'   => {
            contact_name    => "doesnt matter",
            contact_email   => "doesnt matter"
        },
        'provider'          => 'adcourier',
        'last_login_provider' => 'adcourier',
        'provider_id'       => 10,
        'group_identity'    => 'pat',
        'group_nice_name'   => 'pat',
        'settings'          => {
        },
        'subscriptions_ref' => {
            board1 => {
                nice_name => "Board1",
                auth_tokens => {},
                type => "social"
            },
            board2 => {
                nice_name => 'Board2',
                auth_tokens => {},
                type => 'external',
            },
        },
});
$u1->save;

my $schema = $stream2->stream_schema();

# two different criterias
my $criteria1 = Stream2::Criteria->new( schema => $schema );
$criteria1->add_token( 'keywords' , 'manager');
$criteria1->add_token('cv_updated_within' , '3M' );
$criteria1->save();
my $criteria2 = Stream2::Criteria->new( schema => $schema );
$criteria2->add_token( 'keywords' , 'developer');
$criteria2->add_token('cv_updated_within' , '3M' );
$criteria2->save();

my $u1s1 = $u1->subscriptions()->find({ board => 'board1'});
my $u1s2 = $u1->subscriptions()->find({ board => 'board2'});

my $wd1 = _create_watchdog( $stream2, $u1->id, 'u1wd1', $criteria1->id(), [ $u1s1, $u1s2 ] );
my $wd2 = _create_watchdog( $stream2, $u1->id, 'u1wd2', $criteria2->id(), [ $u1s1, $u1s2 ] );

is ( $stream2->watchdogs->search()->count(), 2, "2 watchdogs exist" );

# two new active fresh watchdogs
$stream2->watchdogs->clear_old_inactve();
is ( $stream2->watchdogs->search()->count(), 2, "2 watchdogs still exist" );

# two new, 1 inactive fresh watchdogs
$wd1->update({ active => 0 });
$stream2->watchdogs->clear_old_inactve();
is ( $stream2->watchdogs->search()->count(), 2, "2 watchdogs still exist" );

# one new fresh watchdogs, 1 old inactive watchdog
$wd1->update({ update_datetime => \"date(NOW() , '-3 month')" });
$stream2->watchdogs->clear_old_inactve();
is ( $stream2->watchdogs->search()->count(), 1, "1 watchdog has been cleared" );

sub _create_watchdog {
    my ( $s2, $uid, $name, $cid, $subs ) = @_;
    my $wd = $s2->watchdogs->create({ user_id => $uid, name => $name, criteria_id => $cid, base_url => 'http://www.example.com/' });
    map { $wd->add_to_watchdog_subscriptions({ subscription => $_ }); } @$subs;

    return $wd;
}

done_testing();

use Test::Most;

use Test::Stream2;
use Stream2::Criteria;
use Test::MockModule;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();
my $schema = $stream2->stream_schema();

# results store that doesn't go to redis
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { shift->results; } );
$results_mock->mock( fetch_results => sub { } );

my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( increment_analytics => sub { 1; } );
$stream2_mock->mock( log_search_calendar => sub { 1; } );
$stream2_mock->mock( redis => sub { MyRedis->new(); } );

my $criteria = Stream2::Criteria->new( schema => $schema );
$criteria->add_token( 'ofccp_context' , 'A1B2CDEF99G' );
$criteria->save();

my $memory_user = $stream2->factory('SearchUser')->new_result({
    contact_email => 'woodstock@peanuts.com',
    provider => 'dummy',
    last_login_provider => 'dummy',
    provider_id => 31415,
    group_identity => 'acme',
    subscriptions_ref => {
        dummy => {
            nice_name => "Dummy",
            auth_tokens => {
                flickr_username => 'Shake',
                flickr_password => 'When the walls fell'
            },
            type => "social",
            allows_acc_sharing => 0,
        },
    }
});

my $user_id = $memory_user->save();

# Capture the tokens given to the feed.
my $kipper = $stream2->watchdogs->create({
    user_id => $user_id,
    name => 'Kipper',
    criteria_id => $criteria->id(),
    base_url => 'http://example.com'
});

my $subscription = $memory_user->subscriptions()->find({ board => 'dummy' });
$kipper->add_to_watchdog_subscriptions({
    subscription => $subscription
});

my $mock_feed = Test::MockModule->new('Stream::Templates::dummy');
$mock_feed->mock( search => sub { return 1 } );

my %tokens;
$mock_feed->mock( token_value => sub {
    my ($self, $key, $value) = @_;
    $tokens{$key} = $value;
});

$stream2->watchdogs()->runner( $kipper )->run();

ok( ! $tokens{ofccp_context}, 'OFCCP disabled, not passed to feed');

my $setting = $stream2->factory('Setting')->find_or_create({
  setting_mnemonic    => 'behaviour-search-use_ofccp_field',
  setting_description => 'bobtimandgeorge',
});

$stream2->factory('UserSetting')->create({
  user_id => $memory_user->id,
  setting_id => $setting->id,
  admin_user_id => $memory_user->id,
  boolean_value => 1,
});

%tokens = ();

# Reload and run the watchdog
$stream2->watchdogs()->runner( $kipper )->run();

is($tokens{ofccp_context}->[0], 'A1B2CDEF99G', 'OFCCP enabled, passed to feed');

done_testing();

{
    package MyRedis;
    sub rpush { 1; }
    sub expire { 1; }
    sub new { return bless {}; }
}

#! perl
# Test some quota stuff.

use strict;
use warnings;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

use Test::More;
use Test::Fatal qw/dies_ok lives_ok exception/;

use Test::Stream2;

#use Log::Any::Adapter;
#Log::Any::Adapter->set('Stderr');

my $config_file = 't/app-stream2.conf';

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->deploy_test_db();

my $users_factory = $stream2->factory('SearchUser');

# Create a test user. This is dutch pat
my $user = $users_factory->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 183807,
    group_identity => 'pat',
    contact_details => { username => 'pat@pat.pat.pat' }
});
$user->save();

my $quota_object = $stream2->quota_object;
$quota_object->create(
    {
        board => 'talentsearch',
        type => 'search-cv',
        quota => [
            {
                path => '/pat/pat/pat/pat',
                remaining => 2,
            }
        ]
    },
    {
        actioned_by => 'batman',
        remote_addr => '127.0.0.1',
    }
);

ok( my $template = $stream2->build_template('talentsearch', undef , { user_object => $user }) , "Ok got dummy template");

dies_ok { $template->quota_guard('blabla' , sub{ 1; }); }  "OK dies with bad type";

{
    my $except = exception{ $template->quota_guard([ 'cv' ] , sub{ die 'SPECIFIC EXCEPTION'; } ) };
    like( $except , qr/SPECIFIC EXCEPTION/ , "Ok got original exception");
}

# We can do it two times because of the refund mechanism.
is( $template->quota_guard([ 'cv' ] , sub{ return 'stuff1'; }) , 'stuff1' , "Ok returned stuff one time");
is( $template->quota_guard([ 'cv' ] , sub{ return 'stuff2'; }) , 'stuff2' , "Ok returned stuff two times");

Email::Sender::Simple->default_transport->clear_deliveries; # clear the deliveries

{
    my $except = exception { $template->quota_guard([ 'cv' ], sub{ die 'SPECIFIC EXCEPTION' }) };
    like( $except , qr/Stream::EngineException::QuotaLimitExceeded/,
          "This time we cannot do it because of overspent quota");
}

{
    my $except = exception { $template->quota_guard([ 'cv' ], sub{ return 'something specific' }) };
    like( $except , qr/Stream::EngineException::QuotaLimitExceeded/,
          "No matter how hard we try");
}

my @deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries), 0,  "Ok, no deliveries after no quota errors");

## That was a quota aware board (talentsearch).
## Now try a quota free board (bitbucket_xray)
my $bitbucket = $stream2->build_template('bitbucket_xray', undef , { user_object => $user });
foreach my $i ( 1..10 ){
    is( $bitbucket->quota_guard([ 'cv' ] , sub{ return 'VALUE'.$i; }) , 'VALUE'.$i , "Ok good value" );
}

# Test the profile-or-search quota
my $profile_or_cv_quota_object = $stream2->quota_object;
$profile_or_cv_quota_object->create(
    {
        board => 'talentsearch',
        type => 'search-profile-or-cv',
        quota => [
            {
                path => '/pat/pat/pat/pat',
                remaining => 3,
            }
        ]
    },
    {
        actioned_by => 'batman',
        remote_addr => '127.0.0.1',
    }
);

is( $template->quota_guard([ 'profile-or-cv' ], sub{ return 'stuff1'; }) , 'stuff1' , "Ok returned stuff one time");

my $quota_summary = $profile_or_cv_quota_object->fetch_quota_summary(
      {   path => '/pat/pat/pat/pat',
          board => 'talentsearch',
          type => 'profile-or-cv' ,
      }
);

is( $quota_summary->{ talentsearch }->{ 'search-profile-or-cv' }, 2, 'Ok 1 quota was deducted');

$quota_object->create(
    {
        board => 'talentsearch',
        type => 'search-cv',
        quota => [
            {
                path => '/pat/pat/pat/pat',
                remaining => 1,
            }
        ]
    },
    {
        actioned_by => 'batman',
        remote_addr => '127.0.0.1',
    }
);

is( $template->quota_guard([ 'profile-or-cv', 'cv' ], sub{ return 'stuff2'; }) , 'stuff2' , "Ok returned stuff two times");

$quota_summary = $profile_or_cv_quota_object->fetch_quota_summary(
      {   path => '/pat/pat/pat/pat',
          board => 'talentsearch',
          type => 'profile-or-cv' ,
      }
);

is( $quota_summary->{ talentsearch }->{ 'search-profile-or-cv' }, 1, 'Ok 1 quota was deducted from profile-or-cv');
is( $quota_summary->{ talentsearch }->{ 'search-cv' }, 0, 'Ok 1 was also removed from cv');

{
    my $except = exception { $template->quota_guard([ 'cv' ], sub{ die 'SPECIFIC EXCEPTION' }) };
    like( $except , qr/Stream::EngineException::QuotaLimitExceeded/,
          "OK Quota exceeded for cv since it's 0 even though we have a profile-or-cv quota");
}

done_testing();

#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Encode;
use Test::Stream2;

my $config_file = 't/app-stream2.conf';

my $stream2 = Test::Stream2->new( { config_file => $config_file } );
$stream2->deploy_test_db;

my $users_factory = $stream2->factory('SearchUser');

my $memory_user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");

ok( my $group = $stream2->factory('Group')->find('acme') , "Ok found acme group");
ok( my $template = $stream2->factory('EmailTemplate')->create({ groupclient => $group,
                                                                name => 'My Shiny Template'
                                                              }) , "Ok can create template");

ok( $stream2->factory('EmailTemplate')->create({ groupclient => $group,
                                                                name => 'łövę tô šēñd émåïlś'
                                                              }) , "Ok can create template with non-english unicode characters");

my $mime = $stream2->factory('Email')->build([
        From => Encode::encode('MIME-Q', "whatever \N{U+00CA}")."bla\@broadbean.net",
        To => Encode::encode('MIME-Q', "whatever\N{U+00CA}")."bla\@whatever.com",
        Type => 'multipart/mixed',
        Subject =>
          Encode::encode( 'MIME-Q', "Some \N{U+00CA}subject [kitten]" ),
    ]);

{
    # The container for alternative text or html
    my $container = $mime->attach( Type => 'multipart/alternative' );

    # text version of candidate html body
    $container->attach(
                       Type        => 'text/plain; charset=UTF-8',
                       Encoding    => 'quoted-printable',
                       Disposition => 'inline',
                       Data        => [ Encode::encode('UTF-8' , "Some raw \N{U+00CA}text [kitten]" ) ]
                      );

    # html version of candidate html body
    $container->attach(
                       Type        => 'text/html; charset=UTF-8',
                       Encoding    => 'quoted-printable',
                       Data        => [ Encode::encode('UTF-8', "<p>Some html \N{U+00CA} [kitten]</p>") ]
                      );
}


ok( $template->mime_entity_object($mime) , "Ok can set mime object");

$template->update();

## Load the good template again, check that the mime entity is the same.
ok( my $reloaded = $stream2->factory('EmailTemplate')->find( $template->id() ) , "Ok can reload the same one");
is( $reloaded->mime_entity_object()->stringify() , $mime->stringify() , "Ok same templates");

{
    my $processed = $reloaded->process_with_stash({ kitten => 'beerpie><' });
    $processed->append_html('<p>FunkyBottomFooter</p>');

    my $head = $processed->head();


    foreach my $header ( 'Subject' ){
        like( Encode::decode('MIME-Header' , scalar($head->get($header)) ) , qr/beerpie></ , "beerpie in $header");
    }
    $processed->walk_through_text_bodies(sub{
                                             my ($entity, $string) = @_;
                                             if( $entity->effective_type() =~ /html/ ){
                                                 like( $string , qr/beerpie&gt;&lt;/ );
                                             }else{
                                                 like( $string , qr/beerpie></ );
                                             }
                                             return $string;
                                         });

    # Check the one html body actually works.
    like( $processed->one_html_body() , qr/FunkyBottomFooter/ );
    like( $processed->one_html_body() , qr/beerpie&gt;&lt/ );
    like( $processed->one_header_value('From') , qr/bla\@broadbean\.net/ );
    like( $processed->one_header_value('To') , qr/bla\@whatever\.com/ );
    like( $processed->one_header_value('Subject') , qr/beerpie/ );
}

{
    # Check a badly formatted entity will die on processing.
    my $mime = $stream2->factory('Email')->build([
            From => Encode::encode(
                'MIME-Q', "whatever \N{U+00CA}\@broadbean.net [kitten]"
            ),
            To => Encode::encode(
                'MIME-Q', "whatever\N{U+00CA}\@whatever.com [kitten]"
            ),
            Type => 'multipart/mixed',
            Subject =>
              Encode::encode( 'MIME-Q', "Some \N{U+00CA}subject [kitten]" ),
    ]);

    {
        # The container for alternative text or html
        my $container = $mime->attach( Type => 'multipart/alternative' );

        # text version of candidate html body
        $container->attach(
            Type        => 'text/plain; charset=UTF-8',
            Encoding    => 'quoted-printable',
            Disposition => 'inline',
            Data        => [ Encode::encode('UTF-8' , "Some raw \N{U+00CA}text [kitten / ]" ) ]
        );

        # html version of candidate html body
        $container->attach(
            Type        => 'text/html; charset=UTF-8',
            Encoding    => 'quoted-printable',
            Data        => [ Encode::encode('UTF-8', "<html><body>Some html \N{U+00CA} [kitten]</body></html>") ]
        );
    }
    eval{
        # there should be no error with estimating the eneity size
        $mime->entity_size();
        $mime->clone()->process_with_stash({ kitten => 'beerpie><' });
    };
    my $err = $@;
    ok( $err );
    like( $err , qr/parse error/ );

    # try and build again but have the estimate_entity_size die.
    eval{
        $mime->entity_size(1);
        $mime->clone()->process_with_stash({ kitten => 'beerpie><' });
    };
    $err = $@;
    ok( $err, 'estimate_entity_size throws an error' );
    like( $err , qr/Email exceeds the maximum size/, 'email exceeded it max size.' );
}


done_testing();

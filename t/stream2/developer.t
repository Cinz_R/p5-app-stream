#! perl -w

use strict;
use warnings;

use Test::Most;

# use Log::Any::Adapter qw/Stderr/;

use Test::Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

ok( my $dev = $stream2->developer(), "Ok can get developer" );
ok( $dev->sqitch() , "Ok can get sqitch in developer");
ok( my $split_dbconf = $dev->split_dbconfig() , "Ok can split the DB Config into a hash" );
ok( $split_dbconf->{database} , "Ok there is a database" );
ok( exists $split_dbconf->{password} , "Ok there is a password key" );

ok( $dev->run_sqitch([ 'deploy' ]) , "Ok can run deploy" );
ok( $dev->run_sqitch([ 'verify' ]) , "Ok can run verify" );

ok( $stream2->stream_schema() , "Ok can get stream schema");
my @source_names = sort $stream2->stream_schema()->sources();

ok( scalar( @source_names ) > 8 , "At least 8 sources (this is arbitrary)");
foreach my $source ( @source_names ){
    ok( defined( $stream2->stream_schema()->resultset($source)->search()->count() ), "Ok can count from $source");
    lives_ok(
        sub{ $stream2->stream_schema()->resultset($source)->search(undef , { rows => 1 } )->first(); },
        "Ok can select all columns from DB");
}

done_testing();

use Test::Most;

use Stream2::FeedTokens;
use Stream2::Criteria;

{
  package Stream::Templates::Example;

  use base 'Stream::Templates::Default';

  our %standard_tokens = (
    text_token      => { Type => 'Text' },
    list_token      => { Type => 'List' },
    multilist_token => { Type => 'MultiList' },
  );
};

subtest "Text and list tokens store the selected values in the Value key" => sub {
  my $criteria = Stream2::Criteria->new();
  $criteria->add_token('Example_text_token' => 'Entered Text');
  $criteria->add_token('Example_list_token' => 'Option 2 Selected');

  my $tokens = merge_facets_with_criteria($criteria);

  my $text_token = get_token($tokens, 'text_token');
  is($text_token->{Value}, 'Entered Text', 'Text tokens store the selected value in the Value key');

  my $list_token = get_token($tokens, 'list_token');
  is($list_token->{Value}, 'Option 2 Selected', 'Selected list option');
};

{
  my $criteria = Stream2::Criteria->new();
  $criteria->add_token('Example_multilist_token' => ['Option 1 Selected','Option 3 Selected']);

  my $tokens = merge_facets_with_criteria($criteria);

  my $multilist_token = get_token($tokens, 'multilist_token');
  my %expected_selection = (
    'Option 1 Selected' => 1,
    'Option 3 Selected' => 1,
  );
  is_deeply($multilist_token->{SelectedValues}, \%expected_selection, "Multilist tokens store selected values in the SelectedValues key")
    or diag("MultiList token contains:" . explain($multilist_token));
};

subtest "Empty string is different to undef" => sub {
  my $criteria = Stream2::Criteria->new();
  $criteria->add_token('Example_list_token' => "");

  my $tokens = merge_facets_with_criteria($criteria);

  my $text_token = get_token($tokens, 'text_token');
  my $list_token = get_token($tokens, 'list_token');
  is($text_token->{Value}, undef, "Token value is undef when not set");
  is($list_token->{Value}, "",    "Empty string means the token has a value");
};

done_testing();

sub merge_facets_with_criteria {
  my $criteria = shift;

  my $tokens = Stream2::FeedTokens->new( board => 'Example' );
  return $tokens->merge_criteria($criteria);
}

sub get_token {
    my ($tokens, $token_name) = @_;
    foreach my $token ( @{$tokens->tokens()} ) {
        return $token if $token->{Name} eq $token_name;
    }
    die "Could not find '$token_name' in " . explain([ $tokens->tokens ]);
}

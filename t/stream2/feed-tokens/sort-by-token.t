use Test::Most;

use Stream2::FeedTokens;
use Stream2::Criteria;

{
  package Stream::Templates::Example;

  use base 'Stream::Templates::Default';

  our %standard_tokens = (
    list_token => { Type => 'List' },
    sort_token => { Type => 'Sort' },
  );
};

my $criteria = Stream2::Criteria->new();
$criteria->add_token('Example_sort_token' => 'Relevance');

my $tokens = Stream2::FeedTokens->new( board => 'Example' );
$tokens->merge_criteria($criteria);

my $sort_by_token = $tokens->sort_by_token();
ok $sort_by_token, "Found a sort by token";
is $sort_by_token->{Name}, "sort_token", "... and it is the right one";
is $sort_by_token->{Value}, "Relevance", "... and the correct value is selected";

done_testing();

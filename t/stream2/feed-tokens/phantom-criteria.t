use Test::Most tests => 3;
use List::Util 'first';
use Stream2::Criteria;

use_ok('Stream2::FeedTokens');

use Carp;
$SIG{__DIE__} = \&Carp::confess;

subtest "Selected criteria is retained" => sub {
  my $banking_criteria = Stream2::Criteria->new();
  $banking_criteria->add_token('talentsearch_advert_industry' => 'Banking');

  my $industry = merge_with_criteria($banking_criteria);
  ok($industry->{SelectedValues}->{Banking}, "The industry facet has banking selected");
};

subtest "The facets are not affected by previous calls" => sub {
  my $empty_criteria = Stream2::Criteria->new();
  my $industry = merge_with_criteria($empty_criteria);
  ok(!$industry->{SelectedValues}->{Banking});
};

done_testing();

sub merge_with_criteria {
    my ($criteria) = @_;

    my $tokens = Stream2::FeedTokens->new( board => q{talentsearch} );
    $tokens->merge_criteria($criteria);
    return first { $_->{Id} eq 'talentsearch_advert_industry' } @{ $tokens->tokens() };
}

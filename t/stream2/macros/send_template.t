#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;
use Test::MockModule;

use Log::Log4perl qw/:easy/;
#Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
#Log::Any::Adapter->set('Log::Log4perl');
use Test::Stream2;

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

$stream2->deploy_test_db();

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
    contact_details => {
        username => 'jeje@theteam.theoffice.acme',
    },
    subscriptions_ref   => {
            cvlibrary => {
                nice_name   => "CV library",
                auth_tokens => {
                    cvlibrary_username => 'smd',
                    cvlibrary_password => 'smdpass'
                },
                type               => "social",
                allows_acc_sharing => 0,
            }
        }
});
my $user_id = $user->save();

my $advertapi_mock = Test::MockModule->new('Bean::AdcApi::Client');
$advertapi_mock->mock(request_json => sub {
    return {
        Advert => {
            JobReference    => 'jobref1234',
            JobTitle        => 'Test title',
            AdvertID        => 1234,
            Consultant      => 'whatever',
            AplitrakID      => 'blabla',
        }
    };
});
# sending an email template to a candidate
{
    my $candidate = Stream2::Results::Result->new();
    $candidate->set_attr(broadbean_adcadvert_id => 1234);
    $candidate->set_attr(broadbean_adcboard_id => 999);
    $candidate->destination('adcresponses');
    $candidate->email('jean@pierre.com');

    my $mime_entity = $stream2->factory('Email')->build([
        Type     => 'text/html; charset=UTF-8',
        Encoding => 'quoted-printable',
        Subject  => "Check that",
        'Reply-To' => 'reply@example.com',
        Bcc => 'bcc@example.com',
        Disposition => 'inline',
        Data        => [ '<html><body><h1>Hello template</h1></body></html>' ]
    ]);

    my $template = $stream2->factory('EmailTemplate')->create(
        {   group_identity => $user->group_identity(),
            name           => 'supertemplate'
        }
    );
    $template->mime_entity_object( $mime_entity );
    $template->update();



    my $macro = $user->groupclient()->add_to_macros(
        {   name        => 'send_template_candidate',
            lisp_source => q|( s2#send-email-template |.$template->id().q| )|
        }
    );
    $macro = $stream2->factory('GroupMacro')->wrap($macro);
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            candidate_object => $candidate,
            ripple_settings  => {},
            results_id       => 'foo',
            candidate_idx    => 'bar',
        },
    };

    # The template permission setting.
    my $setting = $template->permission_setting();

    {
        # Allow the user to access the template (default)
        $setting->set_users_value( $user, [ $user->id() ] , '1' );
        my $result = $macro->execute_this( $context );
        is( $result->value(), "true" );
        my @deliveries = Email::Sender::Simple->default_transport->deliveries();
        is( scalar(@deliveries) , 2 , "One normal delivery, and one BCC" );
    }
    {
        # Forbid the user to access the template
        $setting->set_users_value( $user, [ $user->id() ] , '0' );

        # Make sure the user is reloaded and freshly injected in the context.
        $context->{user} =  $stream2->find_user( $user->id() );
        $context->{action_context}->{user_object} = $context->{user};

        my $result = eval{ $macro->execute_this( $context ); };
        my $err = $@;
        ok( ! $result );
        like( $err , qr/restricted/, "Template use is now restricted" );
    }
}


done_testing();

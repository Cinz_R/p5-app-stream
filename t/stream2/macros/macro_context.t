#! perl -w
use strict;
use warnings;

use FindBin;
use Test::More;

# use Log::Any::Adapter qw/Stderr/;

use Test::Stream2;

{
    package My::Action;
    use Moose;
    extends qw/Stream2::Action/;
    has 'stream2' => ( is => 'ro' );
    with qw/Stream2::Role::Action::HasUser/;
}


my $stream2 = Test::Stream2->new();
$stream2->deploy_test_db();

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
    contact_details => {},
    subscriptions_ref   => {}
});
my $user_id = $user->save();

my $action = My::Action->new({ stream2 => $stream2 ,  user => { user_id => $user_id } , ripple_settings => {}  });


ok( my $context = Stream2::O::MacroContext->new({ stream2 => $stream2 , user => $user , action_context => $action->build_context() }) );

ok( my $c_hash = $context->to_hash() );
ok( my $b64c = $context->freeze_to_b64() );

# The context hash can now be used to defrost a context.
{
    ok( my $defrost_context = Stream2::O::MacroContext->new_from_frozen_b64( $b64c , { stream2 => $stream2 } ) );
    # Try to build a My::Action from this context action context.
    my $new_action = My::Action->new({ stream2 => $stream2, %{$defrost_context->action_context()} } );
    is( $new_action->user_object()->id() , $user_id );

    # Now try to use the context evaler to execute some lisp
    my $res = $defrost_context->evaler()->eval(q|( s2#about )|);
    is( $res->value() , "This is Stream2::Lisp::Core with built-in pure functions.");
}

done_testing();

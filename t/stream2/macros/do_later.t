#! perl -w
use strict;
use warnings;

use Test::More;
use Test::Exception;
use Test::MockModule;

# use Carp::Always;

# use Log::Any::Adapter qw/Stderr/;
use Test::Stream2;

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}


my $stream2 = Test::Stream2->new();

$stream2->deploy_test_db();

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
    contact_details => {
        username => 'jeje@theteam.theoffice.acme',
    },
    subscriptions_ref   => {}
});
my $user_id = $user->save();

my $mock_forward_candidate = Test::MockModule->new('Stream2::Action::ForwardCandidate');
$mock_forward_candidate->mock(instance_perform => sub {
    my ($self) = @_;
    is($self->recipients_emails->[0],'gluttony@seven.sins', 'have the correct email address');
    return;
});

# Test forwarding a candidate later.
{
    my $candidate = Stream2::Results::Result->new();
    $candidate->destination('adcresponses');
    $candidate->email('jean@pierre.com');
    $candidate->candidate_id('12345');

    my $macro = $user->groupclient()->add_to_macros(
        {   name        => 'forward-candidate',
            lisp_source => q|( s2#do-later 1440 ( s2#forward-candidate "gluttony@seven.sins" ) )|
        }
    );
    $macro = $stream2->factory('GroupMacro')->wrap($macro);
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            candidate_object => $candidate,
            candidate_id => $candidate->candidate_id(),
            candidate_content => $candidate->to_ref(),
            ripple_settings  => {},
        }
    };

    my $result = $macro->execute_this( $context );
    is( $result->value(), 1 , 'returns the value of a schedule longstep process ID' );

    ok( my $action = $stream2->factory('CandidateActionLog')->search({ candidate_id => $candidate->id() })->first() );

    # And cancel the action.
    ok( $action->cancel() );
    ok( ! $action->cancel() , "Action is already cancelled");
}

# Test messageing a candidate later.
{
    my $candidate = Stream2::Results::Result->new();
    $candidate->destination('adcresponses');
    $candidate->email('jean@pierre.com');
    $candidate->candidate_id('12345');

    my $mime_entity = $stream2->factory('Email')->build([
        Type     => 'text/html; charset=UTF-8',
        Encoding => 'quoted-printable',
        Subject  => "Check that",
        'Reply-To' => 'reply@example.com',
        Bcc => 'bcc@example.com',
        Disposition => 'inline',
        Data        => [ '<html><body><h1>Hello template</h1></body></html>' ]
    ]);

    my $template = $stream2->factory('EmailTemplate')->create(
        {   group_identity => $user->group_identity(),
            name           => 'supertemplate'
        }
    );
    $template->mime_entity_object( $mime_entity );
    $template->update();

    my $macro = $user->groupclient()->add_to_macros(
        {   name        => 'later-message-candidate',
            lisp_source => q|( s2#do-later 1440 ( s2#send-email-template "|.$template->id().q|" ) )|
        }
    );
    $macro = $stream2->factory('GroupMacro')->wrap($macro);
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            candidate_object => $candidate,
            candidate_id => $candidate->candidate_id(),
            candidate_content => $candidate->to_ref(),
            ripple_settings  => {},
        }
    };

    my $result = $macro->execute_this( $context );
    is( $result->value(), 2 , 'returns the value of a schedule longstep process ID' );

    ok( my $action = $stream2->factory('CandidateActionLog')->search({ candidate_id => $candidate->id() , action => 'candidate_future_message' })->first() );

    is( $action->data()->{template_name} , 'supertemplate' );
    # And cancel the action.
    ok( $action->cancel() );
    ok( ! $action->cancel() , "Action is already cancelled");
}



done_testing();

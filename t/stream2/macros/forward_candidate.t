#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;
use Test::MockModule;

use Log::Log4perl qw/:easy/;
# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log::Log4perl');
use Test::Stream2;

BEGIN{
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

$stream2->deploy_test_db();

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
    contact_details => {
        username => 'jeje@theteam.theoffice.acme',
    },
    subscriptions_ref   => {
            cvlibrary => {
                nice_name   => "CV library",
                auth_tokens => {
                    cvlibrary_username => 'smd',
                    cvlibrary_password => 'smdpass'
                },
                type               => "social",
                allows_acc_sharing => 0,
            }
        }
});
my $user_id = $user->save();

my $mock_forward_candidate = Test::MockModule->new('Stream2::Action::ForwardCandidate');
$mock_forward_candidate->mock(instance_perform => sub {
    my ($self) = @_;
    is($self->recipients_emails->[0],'gluttony@seven.sins', 'have the correct email address');
    return;
});

# sending an email template to a candidate
{
    my $candidate = Stream2::Results::Result->new();
    $candidate->destination('adcresponses');
    $candidate->email('jean@pierre.com');

    my $macro = $user->groupclient()->add_to_macros(
        {   name        => 'forward-candidate',
            lisp_source => q|( s2#forward-candidate "gluttony@seven.sins" )|
        }
    );
    $macro = $stream2->factory('GroupMacro')->wrap($macro);
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            candidate_object => $candidate,
            ripple_settings  => {},
            results_id       => 'foo',
            candidate_idx    => 'bar',
        },
    };

        my $result = $macro->execute_this( $context );
        is( $result->value(), "true", 'returns the correct value on success' );
}


done_testing();

use Test::Most;
use Stream2;
use Stream2::FeedTokens;
use utf8;

my $s2 = Stream2->new( { config_file => 't/config/app-stream2.conf' } );

# I use the real translation files to test, make sure that
# the keys here exist

my $t1 = 'Clear criteria';
my $t2 = 'Fluent Languages';

is ( $s2->translations->__( $t1 ), $t1, "Should not be translated when english" );
is ( $s2->template_translations->__( $t2 ), $t2, "Should not be translated when english" );

$s2->translations->set_language( 'de' );

is ( $s2->translations->__( $t1 ), 'Kriterien löschen', "Translation for clear criteria exists in app translations" );
is ( $s2->template_translations->__( $t2 ), 'Sprachen, fließend', "Fluent Languages exists as a key in template translations" );
is ( $s2->translations->__( $t2 ), $t2, "Fluent Languages does not exist in app translations" );
is ( $s2->template_translations->__( $t1 ), $t1, "Clear criteria does not exist in template translations" );

subtest "Translate template using correct domain" => sub {
    my $feed_tokens = Stream2::FeedTokens->new({
        board => 'monsterxml',
        locale => 'de',
        translations => $s2->template_translations
    });
    my ( $job_title_token ) = grep { $_->{Id} eq 'monsterxml_qtjt' } @{$feed_tokens->tokens};
    is ( $job_title_token->{Label}, 'Gewünschte Stellenbezeichnung', "We translate feed tokens" );
    my ( $industry_token ) = grep { $_->{Id} eq 'monsterxml_tcc' } @{$feed_tokens->tokens};
    my ( $test_option ) = grep { $_->[1] == 11724 } @{$industry_token->{Options}};
    is ( $test_option->[0], 'Wirtschaftsprüfung/Revision', "We translate feed options" );
};

subtest "Translate template using wrong domain" => sub {
    my $feed_tokens = Stream2::FeedTokens->new({
        board => 'monsterxml',
        locale => 'de',
        translations => $s2->translations
    });
    my ( $job_title_token ) = grep { $_->{Id} eq 'monsterxml_qtjt' } @{$feed_tokens->tokens};
    is ( $job_title_token->{Label}, 'Target Job Title', "We translate feed tokens" );
    my ( $industry_token ) = grep { $_->{Id} eq 'monsterxml_tcc' } @{$feed_tokens->tokens};
    my ( $test_option ) = grep { $_->[1] == 11724 } @{$industry_token->{Options}};
    is ( $test_option->[0], 'Audit', "We translate feed options" );
};

done_testing();

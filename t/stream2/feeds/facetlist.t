use Test::More;

use Stream2::Feeds::FacetList;
use Stream2::Feeds::Facet::Text;

my $list = Stream2::Feeds::FacetList->new();
ok($list, 'Created facet list');

my @default_facets = qw(
    cv_updated_within
    default_jobtype
    distance_unit
    include_unspecified_salaries
    keywords
    location_within
    salary_cur
    salary_from
    salary_per
    salary_to
    salary_unknown
);
subtest 'Default facets' => sub {
    foreach my $key (@default_facets) {
        my $default_facet = $list->get($key);
        ok($default_facet, 'Found default facet "' . $key . '"');
        $default_facet->set_token(['MyGreat' . $key . 'Value']);
        is(
            $default_facet->value,
            'MyGreat' . $key . 'Value',
            'Set and get value for default facet "' . $key . '"'
        );
    }
};

$list->add_facets(
    name => Stream2::Feeds::Facet::Text->new(
        label => 'My Text Facet',
        value => 'Jimbob McGregorson II'
    )
);

ok( ! $list->get('richquick'), 'Can\'t get nonexistant facet');
is($list->get('name')->value, 'Jimbob McGregorson II', 'Retrieved facet by key');
is($list->value_of('name'), 'Jimbob McGregorson II', 'value of is correct');

is_deeply(
    $list->to_standard_tokens,
    {
        name => {
            Name => 'name',
            Id => 'My Text Facet',
            Label => 'My Text Facet',
            Value => 'Jimbob McGregorson II',
            Type => 'text',
            SortMetric => 999

        }
    }
);
done_testing;

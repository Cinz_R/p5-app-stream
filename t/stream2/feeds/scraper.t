use Test::Most;
use File::Slurp;
use HTML::Scrubber;

use Stream2::Feeds::Scraper;

my $html = File::Slurp::read_file('t/stream2/feeds/scraper.t.html');
my $html_dom = XML::LibXML->load_html(
    string => $html,
    recover => 2
);

my $scrubber = HTML::Scrubber->new(
    allow => ['p', 'br', 'b']
);

my $scraper = Stream2::Feeds::Scraper->new();

my $candidate_xpath = 'id("main")/ul/li';

my $instructions = {
    _path => $candidate_xpath,
    name => 'normalize-space(./h3/text())',
    bio => 'normalize-space(./p)',
    skills => './strong[contains(text(), "Skill Tags")]/following-sibling::ul/li',
    cv_filename => 'normalize-space(id("cv_download"))',
    educations => {
        _path => './/div[@class="education"]/div',
        institution => 'normalize-space(./strong[contains(text(), "Institution")]/following-sibling::text())',
        subject     => 'normalize-space(./strong[contains(text(), "Subject")]/following-sibling::text())',
        grade       => 'normalize-space(./strong[contains(text(), "Grade")]/following-sibling::text())',
        date        => 'normalize-space(./strong[contains(text(), "Date")]/following-sibling::text())',

        subjects    => {
            _path => './strong[contains(text(),  "Subjects")]/following-sibling::div[1]/div',
            title => 'normalize-space(./strong[1]/text())',
            grade => 'normalize-space(./strong[contains(text(), "Grade")]/following-sibling::text())',
            date  => 'normalize-space(./strong[contains(text(), "Date")]/following-sibling::text())',
        }
    },
    cv_preview_html => 'id("cv_preview_html")'
};


my $results = $scraper->scrape_node($html_dom, $instructions, ['CANDIDATE']);

my $result = $results->[0];

ok($result->{name}->isa('XML::LibXML::Literal'), 'Return literal' );
is($result->{name}, 'Henry Tool', 'Name scraped');

ok($result->{skills}->isa('XML::LibXML::NodeList'), 'Return NodeList');
is($result->{skills}->size, 5, 'Return all skills');

$scraper->br2nl_coderef()->($_) for $result->{skills};

ok($result->{cv_filename}->isa('XML::LibXML::Literal'), 'Normalize returns literal for nested elements');
is($result->{cv_filename}, 'henry_tool.pdf', 'Normalised nested elements');

is(ref $result->{educations}, 'ARRAY', 'Nested instructions return ArrayRef');

my $non_hashes = grep { ! (ref $_ eq 'HASH') } @{$result->{educations}};

ok( ! $non_hashes, 'Nested instructions ArrayRef[HashRef] ' );

my $clean_html = $scrubber->scrub($result->{cv_preview_html}->[0]);
$result->{cv_preview_html} = $clean_html;

my $attributes = $scraper->finalize($result);

is_deeply(
    $attributes,
    {
        name => 'Henry Tool',
        cv_filename => 'henry_tool.pdf',
        bio => 'This is my short biography which includes Bold and Line breaks all over the place',
        skills => [
            'Skill1',
            'Skill2 Is the best',
            'Skill3',
            'Skill4',
            'Skill5',
        ],
        educations => [
            {
                institution => 'University of Example',
                subject => 'Example studies',
                grade => 'E for example',
                date => '10/12/2003',
                subjects => [],
            },
            {
                institution => 'University of Example',
                subject => 'Post graduate Example studies',
                grade => 'Pass',
                date => '10/12/2006',
                subjects => [],
            },
            {
                institution => 'Example College',
                subject => '',
                grade => '',
                date => '',
                subjects => [
                    {
                        title => 'Example A Level',
                        grade => 'E',
                        date => '10/12/2001',
                    },
                    {
                        title => 'For Instance A Level',
                        grade => 'F',
                        date => '10/12/2001',
                    },
                    {
                        title => 'Such as A Level',
                        grade => 'S',
                        date => '10/12/2001',
                    }
                ],
            },
        ],
        cv_preview_html => 'This is a div<p>Paragraph</p>'
                            
    },
    'Finalizing results good'
);

done_testing();
use Test::Most;
use Test::Exception;

use Stream2::Feeds::Facet::Checkbox;

ok(my $checkbox = Stream2::Feeds::Facet::Checkbox->new(), 'Got checkbox');
is($checkbox->type, 'checkbox', 'Correct type');
ok($checkbox->checked_value, 'Default checked value truthy');
ok( ! $checkbox->unchecked_value, 'Default unchecked value falsy');
ok(! $checkbox->checked, 'Defaults to unchecked');
is($checkbox->value, $checkbox->unchecked_value, 'Returns unchecked value');
$checkbox->checked(1);
is($checkbox->value, $checkbox->checked_value, 'Returns checked value');

$checkbox->checked_value('checked123');
$checkbox->unchecked_value('unchecked_value123');

is($checkbox->value, 'checked123', 'Custom checked value');
$checkbox->checked(0);
is($checkbox->value, 'unchecked_value123', 'Custom unchecked value');

$checkbox->label('Test Checkbox');

is_deeply(
    $checkbox->to_token(),
    {
        Label => 'Test Checkbox',
        Id => 'Test Checkbox',

        Type => 'list',
        Options => [
            ['Yes', 'checked123'],
            ['No', 'unchecked_value123'],
        ],
        SelectedValues => {
            'unchecked_value123' => 1
        },
        SortMetric => 999
    },
    'to_token correct'
);

$checkbox->set_token( 'checked123' );

ok($checkbox->checked, 'set_token with checked_value');

$checkbox->set_token( 'unchecked_value123' );

ok( ! $checkbox->checked, 'set_token with unchecked_value');

dies_ok(
    sub {
        $checkbox->set_token('unknown value');
    },
    'Dies from set_token with unkonwn value'
);

my $default_checkbox = Stream2::Feeds::Facet::Checkbox->new();
is_deeply(
    $default_checkbox->to_token->{SelectedValues},
    { $default_checkbox->unchecked_value => 1},
    'Defaults to \'No\''
);


done_testing();

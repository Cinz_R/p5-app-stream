use Test::Most;

use Stream2::Feeds::Facet::List;
use Stream2::Feeds::Facet::List::Option;
my $list = Stream2::Feeds::Facet::List->new();

ok($list, 'Created list facet');
ok(! @{$list->options}, 'No default list options');


my $test_options = [
    ['Label Alpha', 'alpha', 123],
    Stream2::Feeds::Facet::List::Option->new(
        label => 'Test label',
        value => 'Test value',
        count => 2
    )
];

$list->options($test_options);

ok(
    $list->options->[0]->isa('Stream2::Feeds::Facet::List::Option'),
    'List option inflated'
);

is( scalar @{$list->options}, 2, 'Both list options inserted');



ok( $list->options->[0]->isa('Stream2::Feeds::Facet::List::Option') );

dies_ok(
    sub {
        $list->options('not an options list')
    },
    'Dies on bad options'
);

dies_ok(
    sub {
        $list->options([[0, 1, 2], 'not an option'])
    },
    'Dies on bad options'
);

$list->options->[1]->selected(1);
$list->label('Test List');
is_deeply(
    $list->to_token,
    {
        Label => 'Test List',
        Id => 'Test List',
        Options => [
            ['Label Alpha', 'alpha', 123],
            ['Test label', 'Test value', 2]
        ],
        Type => 'list',
        SelectedValues => {
            'Test value' => 1
        },
        SortMetric => 999
    }
);

$list->max_select(2);
is($list->to_token->{Type}, 'multilist', 'Multilist when max_select = 2');

my $list2 = Stream2::Feeds::Facet::List->new(
    label => 'From hashref',
    options => [
        ['label', 'value', 1337]
    ]
);

ok($list2, 'Shorthand list creation');

$list->set_token('alpha');

is(scalar $list->selected_options, 1, 'Set two options');
is( ($list->selected_options)[0]->label, 'Label Alpha', 'Set correct option');

$list->set_token(['alpha', 'Test value']);

is(scalar $list->selected_options, 2, 'Set both options');

done_testing();

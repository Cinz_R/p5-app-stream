#! perl -w
use Test::More;
use Stream2::Feeds::Facet::Text;

ok(my $facet = Stream2::Feeds::Facet::Text->new(), 'created facet');
is($facet->type, 'text', 'Type: "text"');
ok(! defined $facet->value, 'Undefined value (not empty string');

$facet->value('Hello World');
$facet->label('My phrase');
$facet->sortmetric(123);
is_deeply(
    $facet->to_token,
    {
        Id => 'My phrase',
        Value => 'Hello World',
        Label => 'My phrase',
        Type => 'text',
        SortMetric => 123
    
    },

    'to_token()'    
);

$facet->set_token('oioi hello');
is($facet->value, 'oioi hello', 'set_token sets values');
is($facet->to_token->{Value}, 'oioi hello', 'set_token reflectede in to_token');

done_testing();
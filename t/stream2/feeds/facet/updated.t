#! perl -w
use Test::More;
use Test::MockModule;

my $year = 
my $mock_datetime = Test::MockModule->new('DateTime');
my $now = DateTime->new(
      year       => 2017,
      month      => 7,
      day        => 7,
      hour       => 7,
      minute     => 7,
      second     => 7,
      nanosecond => 7,
      time_zone  => 'Europe/London',
);
$mock_datetime->mock(now => sub { return $now->clone });

use Stream2::Feeds::Facet::Updated;
ok(my $updated = Stream2::Feeds::Facet::Updated->new(), 'Created facet');

# [value, strftime]
my @test_values = (
    ['TODAY'     => '2017-07-06T07:07:07'],
    ['YESTERDAY' => '2017-07-05T07:07:07'],
    ['3D'        => '2017-07-04T07:07:07'],
    ['1W'        => '2017-06-30T07:07:07'],
    ['2W'        => '2017-06-23T07:07:07'],
    ['1M'        => '2017-06-07T07:07:07'],
    ['2M'        => '2017-05-07T07:07:07'],
    ['3M'        => '2017-04-07T07:07:07'],
    ['6M'        => '2017-01-07T07:07:07'],
    ['1Y'        => '2016-07-07T07:07:07'],
    ['2Y'        => '2015-07-07T07:07:07'],
    ['3Y'        => '2014-07-07T07:07:07']
);

foreach my $values (@test_values) {
    diag @{$values->[0]};
    $updated->set_value($values->[0]);
    is(
        $updated->datetime->strftime('%FT%T'),
        $values->[1],
        $values->[0] . ' duration'
    );
}

$updated->set_value('3M');
ok( my $datetime = $updated->datetime, 'Got datetime' );
ok( $datetime->isa('DateTime'), 'Is a Datime');
is( $datetime->month, 4 );

ok( my $duration = $updated->duration, 'Got durection' );
ok( $duration->isa('DateTime::Duration'), 'Isa a DateTime::Duration' );

$updated->set_value('ALL');
ok( ! defined $updated->duration, '"All" value returns undefined duration');
done_testing();
use Test::Most;

use Stream2::Feeds::Facet::List::Option;

my $option = Stream2::Feeds::Facet::List::Option->new(
    label => 'lobol',
    value => 'voloo',
    count => '2'    
);

is($option->label, 'lobol', 'Option has label');
is($option->value, 'voloo', 'Option has value');
is($option->count, 2, 'Option has count');

ok($option, 'created option');

my $arrayref_option = Stream2::Feeds::Facet::List::Option->from_arrayref(
    ['lobol', 'voloo', 2]
);

ok($arrayref_option, 'Created option from arrayref');

is_deeply($option, $arrayref_option, 'Arrayref option matches regular option');

foreach my $count ({}, [], 1.2, 'four', '') {
    dies_ok(
        sub {
            Stream2::Feeds::Facet::List::Option->new(
                label => '',
                value => '',
                count => $count
            );
        },
        'Count must be Int, not "' . $count . '"'
    );
}

is_deeply( $option->to_arrayref, ['lobol', 'voloo', 2], 'to_arrayref matches');

done_testing();
#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Stream2;

use Test::Mojo::Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

my $t = Test::Mojo::Stream2->new();
$t->login_ok();

ok( my $stream2 = $t->app->stream2 , "Ok can build stream2");
ok( $stream2->stream_schema() , "Ok got schema");

ok( my $users_factory = $stream2->factory('SearchUser'), 'can get user factory' );

ok( my $memory_user = $users_factory->find(1), 'can find test user' );

{
    $memory_user->contact_details({
                                   contact_email => 'yadayada@example.com' ,
                                   contact_name => 'Yada Yada',
                                   locale => 'en',
                                   timezone => 'Europe/London',
                                   currency => 'GBP',
                                   distance_unit => 'miles',
                                  });
}

is( $memory_user->locale(), 'en', "locale defined on user object is 'en'" );

$t->app->helper( current_user => sub { return $memory_user } );

is( $t->app->support_email(), $t->app->config->{support_email}, 'support email should be as defined in config' );

{
    $memory_user->contact_details({
                                   contact_email => 'yabadaba@example.com' ,
                                   contact_name => 'Yaba Daba',
                                   locale => 'en_US',
                                   timezone => 'America/Los_Angeles',
                                   currency => 'USD',
                                   distance_unit => 'miles',
                                  });
    $memory_user->save();
}

is( $memory_user->locale(), 'en_US', "locale defined on user object is 'en_US'" );

$t->app->helper( current_user => sub { return $memory_user } );

is( $t->app->support_email(), 'supportus@broadbean.com', 'support email changed to the US office for clients in the US' );

done_testing();

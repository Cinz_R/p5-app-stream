#! perl
use strict;
use warnings;

use FindBin;
use Test::More;

use Log::Log4perl qw/:easy/;

# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log::Log4perl');

use Stream2;
use Stream2::Criteria;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

# Deploying schema
$stream2->stream_schema()->deploy();


my $user_factory = $stream2->factory('SearchUser');

## Building a user.
my $memory_user = $user_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
    subscriptions_ref => {
        flickr_xray => {
            nice_name => "Flickr",
            auth_tokens => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
            type => "social",
            allows_acc_sharing => 0,
        },
        'whatever' => {
            nice_name => 'Whatever',
            auth_tokens => { whatever_login => 'Boudin', whatever_passwd => 'Blanc' },
            type => 'social',
        }
    }
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");

## We have a user.

## Now building some criteria
# Build an empty criteria

my $criteria = Stream2::Criteria->new( schema => $stream2->dbic_schema );

$criteria->add_token( 'keywords' , 'manager');
$criteria->add_token('cv_updated_within' , '3M' );
$criteria->save();


## Now is the time to create a saved search.

ok( my $saved_search = $stream2->factory('SavedSearch')->create({ user_id => $user_id,
                                                                  criteria => $criteria,
                                                                  active => 1 }), "Ok can create saved search");
ok( $saved_search->to_raw_hashref() , "Ok calling to_raw_hashref");

$saved_search->unsave();

ok( ! $saved_search->active() , "This saved search is not active anymore");


done_testing();

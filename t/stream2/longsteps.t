#! perl -w

use strict;
use warnings;

use Test::More;

# use Log::Any::Adapter qw/Stderr/;

use Test::Stream2;

{
    package My::TestProcess;
    use Moose;
    use DateTime;
    extends qw/Schedule::LongSteps::Process/;
    has 'stream2' => ( is => 'ro', isa => 'Stream2' , required => 1);
    sub build_first_step{
        my ($self) = @_;
        return $self->new_step({ what => 'do_stuff1' , run_at => DateTime->now() });
    }
    sub do_stuff1{
        my ($self) = @_;
        $self->stream2()->stream2_cache()->set( $self->state()->{the_key} , $self->state()->{the_value} );
        return $self->final_step({ state => { the => 'final', state => 1 , %{$self->state()} } });
    }
}



my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();

my $longstep = $stream2->longsteps();

ok( my $p = $longstep->instantiate_process('My::TestProcess',
                                           { stream2 => $stream2 },
                                           { the_key => 'somekey', the_value => "boudin aux pommes\N{U+262D}" })
        , "Ok can instantiate process");

ok( ! $stream2->stream2_cache()->get('somekey') );

$longstep->run_due_processes({ stream2 => $stream2 });

# Note that cache returns binary data. (See CHI, getting and setting).
is( $stream2->stream2_cache()->get('somekey'), "boudin aux pommes\N{U+262D}");

done_testing();

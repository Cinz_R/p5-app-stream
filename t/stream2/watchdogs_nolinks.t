use Test::Most;

use Email::Simple;

use Test::Stream2;
use Stream2::Criteria;
use Test::MockModule;
use Carp::Always;

BEGIN {
    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}

my $stream2 = Test::Stream2->new({ config_file => 't/app-stream2.conf' });
$stream2->deploy_test_db();

my @results = (
    Stream2::Results::Result->new({
        candidate_id => 1,
        email => 'cameron@downingsteeet.com',
        destination => 'flickr_xray',
        location_text => 'Tokyo',
        name => 'David Cameron'
    }),
    Stream2::Results::Result->new({
        candidate_id => 2,
        email => 'may@downingsteeet.com',
        destination => 'flickr_xray',
        location_text => 'Djibouti',
        name => 'Theresa May'
    }),
    Stream2::Results::Result->new({
        candidate_id => 3,
        email => 'brown@downingsteeet.com',
        destination => 'flickr_xray',
        location_text => 'Sandringham',
        name => 'Gordon Brown'
    }),
    Stream2::Results::Result->new({
        candidate_id => 4,
        email => 'jezza@downingsteeet.com',
        destination => 'flickr_xray',
        location_text => 'Woodstock',
        name => 'Jeremy Corbyn'
    })
);

my $feed_mock = Test::MockModule->new( 'Stream::Templates::flickr_xray' );
$feed_mock->mock( search => sub {
    my $self = shift;
    $self->total_results( scalar( @results ) );
    $self->results->add_candidate( @results );
    return @results;
});
$feed_mock->mock( quota_guard => sub { pop->() } );

# results store that doesn't go to redis
my $results_mock = Test::MockModule->new( 'Stream2::Results::Store' );
$results_mock->mock( save => sub { shift->results; } );
$results_mock->mock( fetch_results => sub { @results } );

my $stream2_mock = Test::MockModule->new( 'Stream2' );
$stream2_mock->mock( increment_analytics => sub { 1; } );
$stream2_mock->mock( log_search_calendar => sub { 1; } );
$stream2_mock->mock( redis => sub { MyRedis->new(); } );

# Try making a query on a resultset. It should not go bang.
ok( my $cv_users = $stream2->stream_schema->resultset('CvUser') , "Ok can get resultset");

my $schema = $stream2->stream_schema();

# Build an empty criteria
my $criteria = Stream2::Criteria->new( schema => $schema );
$criteria->add_token('cv_updated_within' , '3M' );
$criteria->save();

my $users_factory = $stream2->factory('SearchUser');

# Time to create a user.
my $user_id;
my $memory_user;
{
    $memory_user = $users_factory->new_result({
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_email       => 'zorro@example.com',
        contact_details     => {},
        subscriptions_ref   => {
            flickr_xray => {
                nice_name           => "Flickr",
                auth_tokens         => { flickr_username => 'shaun' , flickr_password => 'the_sheep' },
                type                => "social",
                allows_acc_sharing  => 1,
            },
        }
    });
    ok( $user_id = $memory_user->save() , "Ok can save this user");
    is( $schema->resultset('CvSubscription')->search({ user_id => $user_id })->count() , 1 , "Ok one subscriptions in the DB");
    no warnings;
    # Any user ALWAYS exists remotely (for the sake of this test).
    *Stream2::O::SearchUser::exists_remotely = sub{ return 1; };
    use warnings;
}

is( $memory_user->watchdogs()->count() , 0 , "Ok this user has no watchdogs");
ok( my $snoopy = $stream2->watchdogs->create({ user_id => $user_id , name => 'Snoopy', criteria_id => $criteria->id() , base_url => 'http://boudin.net/'  }) , "Ok can create watchdog");

ok( my $flickr = $memory_user->subscriptions()->find({ board => 'flickr_xray' }) , "Ok can get subscription via row user relationship");

$snoopy->add_to_watchdog_subscriptions( { subscription => $flickr } );

is( $snoopy->subscriptions()->count() , 1 , "Ok snoopy has got one subscriptions");


# Try to initialize it.
$stream2->watchdogs()->runner($snoopy)->initialize();

foreach my $subscription ( $snoopy->watchdog_subscriptions()->all ){
    ok( $subscription->watchdog_results()->count(), "Subscription has results" );
    ok( $subscription->watchdog_results->first()->viewed(), "result is set as viewed" );
}

# Now we need to run this watchdog and see that an email has been sent.
# Fiddle twith the subscription results so two of each are new.
foreach my $subscription ( $snoopy->watchdog_subscriptions()->all ){
    $subscription->watchdog_results()->search(undef, { rows => 2 })->delete_all();
    ok( $subscription->watchdog_results()->count() , "We still have some results");
}


# Run the watchdog and check it sends an email
$stream2->watchdogs()->runner($snoopy)->run();

my @deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries)  , "1" ,  "Ok one email got delivered");

{
    my $string_email = $deliveries[0]->{email}->as_string();
    like( $string_email , qr/To: zorro\@example.com/  , "Email to zorro"); 
    like( $string_email , qr|http://boudin.net| , "Email contains links to boudin.net");
}

# Change the user last_login_provider, and run the whole thing again.
$memory_user->last_login_provider('adcourier_ats'); $memory_user->update();
foreach my $subscription ( $snoopy->watchdog_subscriptions()->all ){
    $subscription->watchdog_results()->search(undef, { rows => 2 })->delete_all();
    ok( $subscription->watchdog_results()->count() , "We still have some results");
}
# Reload from the DB
$snoopy = $stream2->factory('Watchdog')->find($snoopy->id());

# Run again
$stream2->watchdogs()->runner($snoopy)->run();
@deliveries = Email::Sender::Simple->default_transport->deliveries;
is( scalar(@deliveries)  , "2" ,  "Ok two email got delivered");
{
    my $string_email = $deliveries[1]->{email}->as_string();
    # diag($string_email);
    unlike( $string_email , qr|http://boudin.net| , "Email contains links to boudin.net");
}

{
    package MyRedis;
    sub rpush { 1; }
    sub expire { 1; }
    sub new { return bless {}; }
}

done_testing();

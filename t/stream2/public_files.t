#! perl
use strict;
use warnings;

use Log::Log4perl qw/:easy/;

# Log::Log4perl->easy_init($TRACE);
# use Log::Any::Adapter q/Stderr/;

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';

    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';
}
use LWP;
use LWP::UserAgent::Mockable;

use FindBin;
use Test::More;
use Test::MockModule;
use File::Touch;

use Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
# Deploying schema
$stream2->stream_schema()->deploy();

ok( my $pubfiles =  $stream2->factory('PublicFile') , "Ok got a factory about cloudy public files");
ok( my $other_pubfiles = $stream2->factory('PublicFile') );

{
    ok( $pubfiles->s3() , "OK s3");
    ok( $pubfiles->bucket() , "Ok got bucket");
    ok( $pubfiles == $other_pubfiles, '$pubfiles is a singleton' );
}


# Time to create a user.
my $memory_user = $stream2->factory('SearchUser')->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");

# Now lets create a file in this users group

{

    ok( my $file = $stream2->factory('GroupFile')->create({ content_type => 'text/plain', content => 'Kittens Beer Pie',
                                                            name => 'kitten.txt',
                                                            group_identity => $memory_user->group_identity()
                                                        }), "Ok can create a file");
    ok( $file->uri() , "Ok can get URI for this public group file");
    ok( $file->mime_type() , "OK can get mimetype for this public group file");
}

LWP::UserAgent::Mockable->finished();
done_testing();

#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;
use Test::MockModule;

use Log::Log4perl qw/:easy/;
# Log::Log4perl->easy_init($TRACE);

use Log::Any::Adapter;
# Log::Any::Adapter->set('Log::Log4perl');
use Test::Stream2;

my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}

ok( my $stream2 = Test::Stream2->new({ config_file => $config_file }) , "Ok can build stream2");

$stream2->deploy_test_db();

my $user = $stream2->factory('SearchUser')->new_result({
    provider => 'adcourier',
    last_login_provider => 'adcourier',
    provider_id => 31415,
    group_identity => 'acme',
    contact_details => {
        username => 'jeje@theteam.theoffice.acme',
    },
    subscriptions_ref   => {
            cvlibrary => {
                nice_name   => "CV library",
                auth_tokens => {
                    cvlibrary_username => 'smd',
                    cvlibrary_password => 'smdpass'
                },
                type               => "social",
                allows_acc_sharing => 0,
            }
        }
});

my $user_id = $user->save();

is( $user->hierarchical_path() , '/acme/theoffice/theteam/jeje' );

ok( $user->groupclient()->macros() , "Ok got macros in the user group");

# Define a macro and execute it!
{
    my $macro = $user->groupclient()->add_to_macros({ name => 'justabout',
                                                      lisp_source => q|( s2#about )| });
    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    ok(! $macro->email_template() , "Ok this macro is not associated with any email template");

    my $res = $macro->execute_this( { user => $user } );
    like( $res->value() , qr/Stream2::Lisp::Core with built/ );
}

# Another one that will return a users company
{
    my $macro = $user->groupclient()->add_to_macros({ name => 'user_company',
                                                      lisp_source => q|( s2#user-in-company "acme" )| });

    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    my $context = { user => $user };
    my $res =  $macro->execute_this( $context );
    is( $res->value() , "true" );

    $macro->lisp_source(q|( if ( s2#user-in-company "acme" ) "HOYEAH" "NOPE")|);
    $res = $macro->execute_this( $context );
    is( $res->value() , 'HOYEAH' );

    $macro->lisp_source(q|( if ( s2#user-in-company "bladibla" ) "HOYEAH" "NOPE")|);
    $res = $macro->execute_this( $context );
    is( $res->value() , 'NOPE' );
}

# Another one with office
{
    my $macro = $user->groupclient()->add_to_macros({ name => 'user_office',
                                                      lisp_source => q|( s2#user-in-office "theoffice" )| });
    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    my $context = { user => $user };
    my $res = $macro->execute_this( $context );
    is( $res->value() , "true" );

    $macro->lisp_source(q|( if ( s2#user-in-office "theoffice" ) "HOYEAH" "NOPE")|);
    $res = $macro->execute_this( $context );
    is( $res->value() , 'HOYEAH' );

    $macro->lisp_source(q|( if ( s2#user-in-office "bladibla" ) "HOYEAH" "NOPE")|);
    $res = $macro->execute_this( $context );
    is( $res->value() , 'NOPE' );
}

# Another one with team
{
    my $macro = $user->groupclient()->add_to_macros({ name => 'user_team',
                                                      lisp_source => q|( s2#user-in-team "theteam" )| });
    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    my $context = {  user => $user };
    my $res =  $macro->execute_this( $context );
    is( $res->value() , "true" );

    $macro->lisp_source(q|( if ( s2#user-in-team "theteam" ) "HOYEAH" "NOPE")|);
    $res =  $macro->execute_this( $context );
    is( $res->value() , 'HOYEAH' );

    $macro->lisp_source(q|( if ( s2#user-in-team "bladibla" ) "HOYEAH" "NOPE")|);
    $res = $macro->execute_this( $context );
    is( $res->value() , 'NOPE' );
}


{
    my $macro = $user->groupclient()->add_to_macros({ name => 'candidate_in_destination',
                                                      lisp_source => q|( s2#candidate-in-destination "boudinblanc" )| });
    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    my $candidate = Stream2::Results::Result->new();
    $candidate->destination('cvlibrary');
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            ripple_settings  => {},

            candidate_object => $candidate,
        },
    };

    {
        my $res = $macro->execute_this( $context );
        is( $res->value() , "false" );
    }
    $candidate->destination('boudinblanc');
    {
        my $res =  $macro->execute_this( $context );
        is( $res->value() , "true" );
    }
}

{
    my $macro = $user->groupclient()->add_to_macros({ name => 'candidate_is_response',
                                                      lisp_source => q|( s2#candidate-is-response )| });
    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    my $candidate = Stream2::Results::Result->new();
    $candidate->destination('adcresponses');
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            ripple_settings  => {},

            candidate_object => $candidate,
        },
    };
    my $res =  $macro->execute_this( $context );
    is( $res->value() , "true" );
}

# candidate-updated-with-value
{
    my $macro = $user->groupclient()->add_to_macros({ name => 'candidate_updated_with_value_response',
                                                      lisp_source => q|( s2#candidate-updated-with-value "strawberry_field" 2 )| });
    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            ripple_settings  => {},
        },
    };
    my $res =  $macro->execute_this( $context );
    is( $res->value() , "false" );
}

{
    my $macro = $user->groupclient()->add_to_macros({ name => 'candidate_updated_with_value_response-good-field',
                                                      lisp_source => q|( s2#candidate-updated-with-value "strawberry_field" 2 )| });
    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            ripple_settings  => {},

            fields => { strawberry_field => 3 }
        },
    };
    my $res =  $macro->execute_this( $context );
    is( $res->value() , "false" );
}

{
    my $macro = $user->groupclient()->add_to_macros({ name => 'candidate_updated_with_value_response-good-value',
                                                      lisp_source => q|( s2#candidate-updated-with-value "strawberry_field" 3 )| });
    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            ripple_settings  => {},

            fields => { strawberry_field => 3 }
        },
    };
    my $res = $macro->execute_this( $context );
    is( $res->value() , "true" );
}

# sending candidate xml data to url
{
    my $candidate = Stream2::Results::Result->new();
    $candidate->destination('cvlibrary');

    my $mock_parse = Test::MockModule->new('Stream2::O::SearchUser');
    $mock_parse->mock(
        'parse_cv_with_locale' => sub {
            return {foo => 'bar'};
        }
    );

    $candidate->enrich_with_cvparsing(
        {   first_name => 'Jean Pierre',
            last_name  => 'Marcel',
            email      => 'jean@pierre.com',
            bg_xml     => q|<?xml version='1.0' encoding='iso-8859-1'?>
                <ResDoc><resume canonversion='2' dateversion='2' iso8601='2014-12-16' present='735585' xml:space='preserve'><contact><name><givenname>John</givenname> <surname>Smith</surname></name>
                    <address><street>Blah Street</street>
                    P.O. Box <street>123
                    Ya Ya Ya ROad
                    The Netherlands</street></address>

                    Phone:  (00) <phone>1234 567 8910</phone>
                    Linkldnk: <website>http://www.example.com</website>
                    E-mail: <email>emaill\@address.com</email></contact>
                    </resume>
                </ResDoc>|
        }
    );

    my $mock_download = Test::MockModule->new('Stream2::Action::DownloadCV');

    my $content = 'reallycoolcv';
    my $http_response = HTTP::Response->new(
                    200,
                    'OK',
                    [   'Content-Type'     => 'text/xml',
                        'Content-Location' => 'cvfilename.doc'
                    ],
                    'reallycoolcv',
                );
    my $encoded_content = MIME::Base64::encode_base64($http_response->content . "\n"); # newline gets added

    $mock_download->mock(
        'instance_perform' => sub {
            return  MIME::Base64::encode_base64( $http_response->as_string );
        }
    );

    my $mock_ua = Test::MockModule->new('LWP::UserAgent');
    my $response;
    $mock_ua->mock(
        'request' => sub {
            $response = $mock_ua->original('request')->(@_);
            return $response;
        }
    );

    my $macro = $user->groupclient()->add_to_macros(
        {   name        => 'send_candidate',
            lisp_source => q|( s2#send-candidate "http://www.zombo.com" )|
        }
    );
    $macro = $stream2->factory('GroupMacro')->wrap($macro);
    my $context = {
        user => $user,
        action_context => {
            user_object      => $user,
            ripple_settings  => {},

            candidate_object => $candidate,
            results_id       => 'foo',
            candidate_idx    => 'bar'
        },
    };

    my $result = $macro->execute_this( $context );

    is( $result->value(), "true" );
    is( $response->request->uri->as_string, 'http://www.zombo.com' );
    like( $response->request->content,
        qr/<Candidate><Name>Jean Pierre Marcel<\/Name><Email>jean\@pierre.com<\/Email><ChannelId>cvlibrary<\/ChannelId><ChannelName>CV library<\/ChannelName><Doc FileName="cvfilename.doc" DocumentType="Attachment" EncodingType="Base64" MimeType="text\/xml">$encoded_content<\/Doc><\/Candidate><\/SearchCandidate>/
    );
}


{
    my $macro = $user->groupclient()->add_to_macros({ name => 'infinite',
                                                      lisp_source => q|
(while true
 (println true))
| });
    $macro = $stream2->factory('GroupMacro')->wrap( $macro );
    throws_ok { $macro->execute_this( { user => $user } ); } qr/while/ , 'Ok while is not a function';
}

done_testing();

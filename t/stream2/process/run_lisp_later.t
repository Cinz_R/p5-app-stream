#! perl -w

use strict;
use warnings;

use Test::More;
use Test::Exception;
use Test::MockDateTime;

use Test::Stream2;

# use Log::Any::Adapter qw/Stderr/;

my $stream2 = Test::Stream2->new();
$stream2->deploy_test_db();

my $user = $stream2->factory('SearchUser')->new_result(
    {
        provider            => 'adcourier',
        last_login_provider => 'adcourier',
        provider_id         => 31415,
        group_identity      => 'acme',
        contact_details     => {
            username => 'jeje@theteam.theoffice.acme',
        },
        subscriptions_ref => {}
    }
);
my $user_id = $user->save();

my $macro_context = Stream2::O::MacroContext->new(
    {
        stream2        => $stream2,
        user           => $user,
        action_context => {}
    }
);

ok(
    my $stored_process = $stream2->longsteps()->instantiate_process(
        'Stream2::Process::RunLispLater',
        { stream2 => $stream2 },
        {
            lisp_code     => '( s2#about )',
            macro_context => $macro_context->freeze_to_b64(),
            in_minutes    => 10
        }
    )
);

my $in_1_minutes = DateTime->now()->add( minutes => 1 );
on $in_1_minutes => sub {
    $stream2->longsteps()->run_due_processes( { stream2 => $stream2 } );
};

my $in_11_minutes = DateTime->now()->add( minutes => 11 );
on $in_11_minutes => sub {
    $stream2->longsteps()->run_due_processes( { stream2 => $stream2 } );
};

$stored_process->discard_changes();

is(
    $stored_process->state()->{result},
    'This is Stream2::Lisp::Core with built-in pure functions.',
    "The lisp code was run after 10 minutes"
);

done_testing();

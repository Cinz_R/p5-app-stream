#! perl -w

use strict;
use warnings;

use Test::More;
use Test::Exception;
use Test::MockModule;
use Test::MockDateTime;

use Test::Stream2;
use Stream2::Results::Result;

#use Log::Any::Adapter qw/Stderr/;

my $stream2 = Test::Stream2->new({ config_file => './t/app-stream2.conf' });
$stream2->deploy_test_db();

my $users_factory = $stream2->factory('SearchUser');
my $memory_user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
    settings => {
        "auto_download_cvs_on_preview" => JSON::XS::false,
        "some_other_random_setting" => JSON::true,
        'stream' => {
            'forward_to_tp2' => '0'
        },
    }
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");


my $tag = $stream2->factory('Tag')->create({
    group_identity  => $memory_user->group_identity,
    tag_name        => 'sausage',
    tag_name_ci     => 'sausage'
});
my $cand_tag = $stream2->stream_schema->resultset('CandidateTag')->create({
    group_identity  => $memory_user->group_identity,
    candidate_id    => 'talentsearch_142802203',
    tag_name        => 'sausage'
});

ok( my $stored_process =
        $stream2->longsteps()->instantiate_process(
            'Stream2::Process::TransferResponseTag',
            {
                stream2 => $stream2
            },
            {
                user_id => $user_id,
                adcresponses_id => 2377756, # Real example data from live BBCSS.
                tsimport_id => 13758279,
                search_record_id => undef,
                tag_name => 'sausage'
            }
        )
);

my $s2_mock = Test::MockModule->new( 'Stream2' );
my $in_2_minutes = DateTime->now()->add( minutes => 2 );
on $in_2_minutes => sub{
    # Mock the $self->response_candidate->get_internal_db_ids();
    my $mock_result = Test::MockModule->new( 'Stream2::Results::Result' );
    $mock_result->mock( 'get_internal_db_ids' => sub{ 'INTERNAL_DBS_PLEASE_WAIT' } );

    $s2_mock->mock( build_template => sub {
        fail( "Build template should not be called" );
    });

    $stream2->longsteps()->run_due_processes({ stream2 => $stream2 });
};
$s2_mock->unmock_all();

my $in_10_minutes = DateTime->now()->add( minutes => 10 );
on $in_10_minutes => sub{
    # Mock the $self->response_candidate->get_internal_db_ids();
    # so this time it returns something.
    my $mock_result = Test::MockModule->new( 'Stream2::Results::Result' );
    $mock_result->mock( 'get_internal_db_ids' => sub {
        +{
            'Artirix' => '142802203'
        };
    });

    my $ts_called = 0;
    my $ts_mock = Test::MockModule->new( 'Bean::TSImport::Client' );
    $ts_mock->mock( make_request => sub {
        my ( $self, %args ) = @_;
        $ts_called = 1;
        is( $args{path}, sprintf( '/company/%s/candidate/Artirix/%s/tags', 'acme', '142802203' ), "call to artirix was made" );
        is_deeply( $args{arguments}, { tag_name => 'sausage' } );
    });

    $stream2->longsteps()->run_due_processes({ stream2 => $stream2 });

    is( $stream2->factory('CandidateActionLog')->count() , 1 , "Ok a candidate action has been recorded");
    my $tag_action = $stream2->factory('CandidateActionLog')->first();
    is( $tag_action->action(), 'candidate_tag_add' );
    is( $tag_action->user_id() , $user_id );
    is( $tag_action->group_identity() , $memory_user->group_identity() );
    is( $tag_action->candidate_id() , 'talentsearch_142802203' );
    is( $tag_action->destination() , 'talentsearch' );
    is_deeply( $tag_action->data() , { tag_name => 'sausage' } );
    ok( $ts_called, "Talentsearch was called" );
};


done_testing();

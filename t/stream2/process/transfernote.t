#! perl -w

use strict;
use warnings;

use Test::More;
use Test::Exception;
use Test::MockModule;
use Test::MockDateTime;

use Test::Stream2;
use Stream2::Results::Result;

# use Log::Any::Adapter qw/Stderr/;

my $stream2 = Test::Stream2->new({ config_file => './t/app-stream2.conf' });
$stream2->deploy_test_db();

my $users_factory = $stream2->factory('SearchUser');
my $memory_user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
    settings => {
        "auto_download_cvs_on_preview" => JSON::XS::false,
        "some_other_random_setting" => JSON::true,
        'stream' => {
            'forward_to_tp2' => '0'
        },
    }
});
ok( my $user_id = $memory_user->save() , "Ok can save this user");


ok( my $stored_process =
        $stream2->longsteps()->instantiate_process('Stream2::Process::TransferResponseNote',
                                                   { stream2 => $stream2 },
                                                   { user_id => $user_id,
                                                     adcresponses_id => 2377756, # Real example data from live BBCSS.
                                                     tsimport_id => 13758279,
                                                     search_record_id => undef,
                                                     note_data => { some => 'cool' , stuff => 1 },
                                                 }
                                               ) );
my $in_2_minutes = DateTime->now()->add( minutes => 2 );
on $in_2_minutes => sub{
    # Mock the $self->response_candidate->get_internal_db_ids();
    my $mock_result = Test::MockModule->new( 'Stream2::Results::Result' );
    $mock_result->mock( 'get_internal_db_ids' => sub{ 'INTERNAL_DBS_PLEASE_WAIT' } );

    $stream2->longsteps()->run_due_processes({ stream2 => $stream2 });
    ok(!$stream2->factory('CandidateActionLog')->count() , "Ok no candidate action (no note action) has been filed");
};

my $in_10_minutes = DateTime->now()->add( minutes => 10 );
on $in_10_minutes => sub{
    # Mock the $self->response_candidate->get_internal_db_ids();
    # so this time it returns something.
    my $mock_result = Test::MockModule->new( 'Stream2::Results::Result' );
    $mock_result->mock( 'get_internal_db_ids' => sub{
                            {
                                'Artirix' => '142802203'
                            };
                        } );
    $stream2->longsteps()->run_due_processes({ stream2 => $stream2 });
    is( $stream2->factory('CandidateActionLog')->count() , 1 , "Ok a candidate action has been recorded");
    my $note_action = $stream2->factory('CandidateActionLog')->first();
    is( $note_action->action() ,'note');
    is( $note_action->user_id() , $user_id );
    is( $note_action->group_identity() , $memory_user->group_identity() );
    is( $note_action->candidate_id() , 'talentsearch_142802203' );
    is( $note_action->destination() , 'talentsearch' );
    is_deeply( $note_action->data() , { some => 'cool', stuff => 1 } );
};


done_testing();

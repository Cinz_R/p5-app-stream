#! perl
# Test a basic search (on the dummy search template).


# If you run your test normally, you should have to change this.
# In the case you use LWP_UA_MOCK=record more than 24 hours
# after the last LWP_UA_MOCK 'record', you will have to update
# this value with the session ID that will be displayed at the end
# of this test.

my $PERSISTENT_SESSION_ID = '145245wnbL81ONc2AJkXzSneIXwigT';

#

use strict;
use warnings;

use FindBin;
use Test::More;

use Data::UUID;
BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';

    # TEST_ONLINE set by jenkins
    plan skip_all => 'CV lib account may be in use' if $ENV{ TEST_ONLINE };
}

use LWP;
use LWP::UserAgent::Mockable;

use Stream2;

use Log::Log4perl qw/:easy/;
Log::Log4perl->easy_init($ERROR);

use Log::Any::Adapter;
Log::Any::Adapter->set('Log::Log4perl');


my $config_file = 't/app-stream2.conf';

my $template_name = 'cvlibrary';

ok( my $stream2 = Stream2->new({ config_file => $config_file }) , "Ok can build stream2");
$stream2->stream_schema->deploy();

my $ug = Data::UUID->new();

my $users_factory = $stream2->factory('SearchUser');

# A test user.
my $user = $users_factory->new_result({
    provider => 'whatever',
    last_login_provider => 'whatever',
    provider_id => 31415,
    group_identity => 'acme',
});
$user->save();



ok( my $template = $stream2->build_template($template_name, undef , { user_object => $user } ) , "Ok got template");

my $results_id = $ug->create_str();

ok( my $results_collector = Stream2::Results->new({
                                                   id          => $results_id,
                                                   destination => $template_name,
                                                  }),
    "Ok can build a result collector");

# Inject the result collector in the template object.
$template->results($results_collector);

# This is needed for Stream::SharedUtils
# to know about the IP.
$ENV{REMOTE_ADDR} = '127.0.0.1';

# Set the authentication tokens.
$template->token_value( cvlibrary_username => 'bbstream' );
$template->token_value( cvlibrary_password => 'wisoxi685' );
$template->token_value( cvlibrary_registered_email => 'john@doe.com' );

my $browserTag = 'imaspecialsnowflake';
$template->token_value( browser_tag => $browserTag );

my $session_id_key = $browserTag.'session_id';
$template->account_value($session_id_key, $PERSISTENT_SESSION_ID );

# we need to give some keywords
$template->token_value( keywords => 'perl' );

# And search!
$template->search({});

ok( $template->account_value($session_id_key) , "Ok can get stored session_id");
diag("You might want to hit http://www.cv-library.co.uk/cgi-bin/umid.gif?sessionID=". $template->account_value($session_id_key)." so this test runs again (for devs only)" );

$stream2->user_agent()->get("http://www.cv-library.co.uk/cgi-bin/umid.gif?sessionID=". $template->account_value($session_id_key));

# Check we now have results
ok( scalar(@{$results_collector->results()}), "Ok got results in the results collector");
ok( scalar( @{ $results_collector->notices() } ) , "Ok got some notice from this template");

unlike ( $template->search_build_xml(), qr/watchdog>1/, "THis isn't a watchdog so why should we say that it is" );
$template->is_watchdog( 1 );
like ( $template->search_build_xml(), qr/watchdog>1/, "This is a watchdog, so tell cvlibrary it is" );
$template->is_watchdog( 0 );

END{
  LWP::UserAgent::Mockable->finished();
}

done_testing();

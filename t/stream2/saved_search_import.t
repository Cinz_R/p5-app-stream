#! perl
use strict;
use warnings;

use FindBin;
use Test::More;
use Test::Exception;

use DateTime;

use Log::Any::Adapter;

use Email::Simple;

#Log::Any::Adapter->set('Stderr');

BEGIN {
    my $DEFAULT_UA_MOCK = 'playback';

    $ENV{ LWP_UA_MOCK } ||= $DEFAULT_UA_MOCK;
    $ENV{ LWP_UA_MOCK_FILE } ||= __FILE__.'-lwp-mock.out';

    $ENV{EMAIL_SENDER_TRANSPORT} = 'Test';

}

use LWP;
use LWP::UserAgent::Mockable;

use Stream2;
use Stream2::Criteria;


my $config_file = 't/app-stream2.conf';
unless( $config_file && -r $config_file ){
    plan skip_all => "Cannot read config_file'".( $config_file || 'UNDEF')."'";
}


my $stream2 = Stream2->new({ config_file => $config_file });
$stream2->stream_schema()->deploy();

my $user_id = '150780';

my $previous_search;
my @prev_searches = $stream2->user_api->get_stream_v1_previous_searches( $user_id );
foreach my $ps ( @prev_searches ) {
    if ( $ps->{name} ){
        $previous_search = $ps;
    }
}

{
    # No woopwoop users before importing
    is( $stream2->factory('SearchUser')->search({ provider => 'woopwoop' })->count() , 0 , "No user in DB before importing");

    $stream2->factory('SavedSearch')->import_company('pat',
                                                     sub { return scalar( grep { $_[0]->{id} eq $_ }  qw/150780/); },
                                                     'https://www.example.com',
                                                     { provider => 'woopwoop' }
                                                 );
    # Some woopwoop users are there.
    cmp_ok( $stream2->factory('SearchUser')->search({ last_login_provider => 'woopwoop' })->count(), '>',  0 , "Some users with the woopwoop provider are there");

    ok( $stream2->stream_schema->resultset('SavedSearches')->count(), "OK some saved searches were created");
    my $saved_search = $stream2->stream_schema->resultset('SavedSearches')->find({ name => $previous_search->{name} });

    ok( $saved_search, "one of the previous searches has been successfully stored in the DB" );

    my $criteria = Stream2::Criteria->new(
        id => $saved_search->criteria_id,
        schema => $stream2->stream_schema()
    )->load;

    my $tokens = { $criteria->tokens };
    foreach my $token ( keys %$tokens ){
        is_deeply ( $tokens->{$token}, $previous_search->{attributes}->{$token}, "$token matches that of v1" );
    }
}

LWP::UserAgent::Mockable->finished();

done_testing();

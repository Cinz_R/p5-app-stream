package Bean::Locale::en_NZ;
use base qw(Bean::Locale::en_AU);

use strict;
### MARK BEGIN LEXICON
our %Lexicon = (
'Utilities' => 'Utilities (Energy, Oil, Gas, Mining)',
'_DEFAULT_SALARY' => 'NZD',
'_DEFAULT_SPELLCHECK_LANG' => 'en_AU',
'_LOCALE' => 'en_NZ,en_NZ.ISO8859-1',
'_TP_DISTANCE_UNIT' => 'km',
);
### MARK END LEXICON
1;

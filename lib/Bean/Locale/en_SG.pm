package Bean::Locale::en_SG;
use base qw(Bean::Locale::en);

use strict;
### MARK BEGIN LEXICON
our %Lexicon = (
'Utilities' => 'Utilities (Energy, Oil, Gas, Mining)',
'_DEFAULT_SALARY' => 'SGD',
'_DEFAULT_SPELLCHECK_LANG' => 'en',
'_LOCALE' => 'en_SG,en_SG.ISO8859-1',
'_TP_DISTANCE_UNIT' => 'km',
);
### MARK END LEXICON
1;

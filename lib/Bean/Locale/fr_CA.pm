package Bean::Locale::fr_CA;
use base qw(Bean::Locale::fr);

use strict;
### MARK BEGIN LEXICON
our %Lexicon = (
'_DEFAULT_SALARY' => 'CAD',
'_DEFAULT_SPELLCHECK_LANG' => 'fr',
'_LOCALE' => 'fr_CA,fr_CA.ISO8859-1',
);
### MARK END LEXICON
1;


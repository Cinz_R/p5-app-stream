package Bean::Locale::en_HK;
use base qw(Bean::Locale::en);

use strict;
### MARK BEGIN LEXICON
our %Lexicon = (
'Utilities' => 'Utilities (Energy, Oil, Gas, Mining)',
'_DEFAULT_SALARY' => 'HKD',
'_DEFAULT_SPELLCHECK_LANG' => 'en',
'_LOCALE' => 'en_HK,en_HK.ISO8859-1',
'_TP_DISTANCE_UNIT' => 'km',
);
### MARK END LEXICON
1;

package Bean::Locale;

# Localisation module for Adcourier
#  the language handler is automatically created during the first call to translate some text
#  (not when the module gets loaded)

use strict;
use warnings;

# used to determine username
use CGI;
use Carp;
use DateTime;
use DateTime::TimeZone;

#use to switch locale
use POSIX qw(setlocale LC_COLLATE LC_CTYPE);

# we aren't subclassing exporter (because we are a Locale::Maketext subclass) but we still want to export some subroutines
use Exporter qw( import );
our @EXPORT = qw(&localise &localise_in &lang &PT &L &L_JS_SAFE &L_NOTATION_SAFE &html_translate protect_translate);

# determine devuser type for debugging
#use Bean::Internal; # do not load Bean::Internal in this module

# just in case we ever need utf8 output
my $ENCODING = ''; # no encoding by default

BEGIN {
    if ( $ENCODING && $ENCODING eq 'utf8' ) {
        require Encode;
    }
};

use I18N::LangTags;

# by default, I18N::LangTags autodetection prefers French instead of English for Italians
# this ugly hack forces it to try English instead
my %OurPanic = (
    # force languages like Italian to return English instead of French
#    'pt' => [qw(en)], # Engrish Portuguese, Spanish, Catalan, Italian, French
    'ca' => [qw(es pt en it fr)],
    'es' => [qw(ca en it fr pt)],
    'it' => [qw(en es fr ca pt)],
);

@I18N::LangTags::Panic{ keys %OurPanic } = values %OurPanic;

# localisation framework
use Locale::Maketext;
use base qw(Locale::Maketext);

# http://search.cpan.org/~neilb/Locale-Codes-2.07/
# Module is already installed on monsoon so I guess it is part of the perl core

# Using Maketext framework (it is part of perl core)

# Here are a couple of good articles about localisation & perl
# - guide to Maketext (examples are web orientated)
#    http://search.cpan.org/src/AUTRIJUS/Locale-Maketext-Lexicon-0.62/docs/webl10n.html
# - introduction to Maketext (a perl replacement for gettext)
#    http://search.cpan.org/~jhi/perl-5.8.1/lib/Locale/Maketext/TPJ13.pod

# NB. Some tutorials recommend naming the translation sub _ (e.g. print _("Hi!"))
# but this isn't a good idea (see http://hop.perl.plover.com/errata/errata-s-2.html)
# _ is difficult to export nicely as well

my %cached_handlers;


# supported languages (will be useful when we need to display a dropdown of languages)
my %available_languages = (
    # most UK browsers request "en-us,en", so it defaults to en_us which isn't the plan
    # so we will skip autodetecting the language and force GB english
    # to enable US locales in an account either:
    #    - set locale to 'en_usa' in the company table
    #    - update storable (either office,team,user) elements to have locale="en_usa"
    'English (GB)'   => 'en',
    'English (US)'   => 'en_usa',
    'English (AU)'   => 'en_AU',
    'English (HK)'   => 'en_HK',
    'English (NZ)'   => 'en_NZ',
    'English (SG)'   => 'en_SG',
    'French (Custom)'   => 'fr',
    'French (Standard)' => 'fr_CH',
    'French (Canada)'=> 'fr_CA',
    'Dutch'          => 'nl',
    'Spanish'        => 'es',
    'Danish'         => 'da',
    'Japanese'       => 'ja',
    'Chinese'        => 'zh',
    'Italian'        => 'it',
    'German'         => 'de',
    'Swedish'        => 'sv',
    'Portuguese (Brazil)'=> 'pt_BR',
);

sub get_handle {
    my $class = shift;
    if ( @_ == 1 && $_[0] =~ m/^(?:en_usa|en_(?:AU|HK|NZ|SG)|en_translated|fr_CH|fr_CA|pt_BR)$/ ) {
        local $Locale::Maketext::USING_LANGUAGE_TAGS;
        $Locale::Maketext::USING_LANGUAGE_TAGS = 0;
        return $class->SUPER::get_handle(@_);
    }

    return $class->SUPER::get_handle(@_);
}

# init gets automatically called by Locale::Maketext constructor
# we subclass it so we can cache the language handlers & install
# a method to help us debug translations
sub init {
    my $self = shift;

    $self->SUPER::init();

    $self->fail_with( \&fail_method );

    $cached_handlers{ $self->language_tag() } = $self;

    return $self;
}

# get the raw, unprocessed translation
sub raw_translation {
    my $language = shift;
    my $phrase   = shift;

    my $handle = get_language_handle( $language );
    foreach my $h_r ( @{ $handle->_lex_refs } ) {
        if(exists $h_r->{$phrase}) {
            return $h_r->{$phrase};
        }
        elsif($phrase !~ m/^_/s and $h_r->{_AUTO} ) {
            # using AUTO lexicon
            return $phrase;
        }
    }

    return undef;
}

# returns true if the language has an auto lexicon (ie. we return the key as the translation if it doesn't exist)
sub supports_auto {
    my $language = shift;

    my $handle = get_language_handle( $language ) or return 0;
    foreach my $h_r ( @{ $handle->_lex_refs } ) {
        if ( exists($h_r->{_AUTO}) ) {
            return 1;
        }
    }

    return 0;
}

# localise the text in a specific language
sub localise_in {
    my $language = shift;
    return get_language_handle( $language )->maketext( @_ );
}

my $_locale_set;
sub setup_locale {
    my $_locale = L('_LOCALE');

    if ( $_locale =~ m/_LOCALE|missing/ ) {
        return 1;
    }

    LOCALE:
    foreach my $locale (split (/,/,L('_LOCALE'))){
        if (!$locale) {
            next LOCALE;
        }

        if (setlocale(LC_COLLATE, $locale)) {
            $_locale_set = $locale;
            # loaded a locale, make sure the locale can sort correctly
            determine_locale_sort();
            return $locale;
        };
    }

    return 1;
}

my ($_ropey_sort);
sub determine_locale_sort {
    # high level overview:
    #  1. UK/US uses the normal perl ASCII sort
    #  2. Monsoon uses locale + perl sort
    #  3. Live servers use locale + my hacky sort

    # why it isn't as simple as it should be:
    #  1. monsoon locale sorts -- to the top of a list, live servers locale sorts ignore
    #     the hyphen (and spaces too)
    #  2. setting LC_ALL on monsoon causes perl to die horribly
    #  3. locale names are not portable
    #     (e.g. freebsd you want: fr_FR.ISO8859-1, slack you want fr_FR)

    # onto some notes about "use locale;"
    #  * sort is an ASCII sort
    #  * under "use locale", sort checks LC_COLLATE to determine sort order
    #  * the sort under locale is equivalent to:
    #       sort { $a locale cmp $b or $a ascii cmp $b } @_
    #    so -- Please select, would come after Accountancy, but before Public Sector
    #    if the locale ignores hyphens
    #  * a lot of the slack/debian locales ignore hyphens and see them as a separator instead
    #  * regex character classes like [[:alpha:]] checks LC_CTYPE.
    #  * on monsoon you can set LC_COLLATE and LC_CTYPE separately, setting LC_ALL causes bad things(tm)
    use locale;
    if ( ('-A' cmp 'A' ) == -1 && ('-P' cmp 'A') == 1 ) {
        # -A comes before A because cmp does a locale based cmp and then falls back to
        # an ASCII sort if the locale cmp returns 0
        # '-P' cmp 'A' will test with locale first, and will compare P vs A if the locale
        # doesn't check hyphens (ala slack & debian locales)
        # the locale sort ignores hyphens so we need our own (albeit ropey & less efficient)
        setlocale(LC_CTYPE, $_locale_set);
        $_ropey_sort = 1;
    }
}

sub locale_sort {
    if ( $_locale_set ) {
        if ( $_ropey_sort ) {
            return _locale_sort { $_[0] } @_;
        }

        use locale;
        return sort { $a cmp $b } @_;
    }

    # locale doesn't need a clever sort
    return sort { $a cmp $b } @_;
}

sub locale_sort_block (&@) {
    if ( $_ropey_sort ) {
        goto &_locale_sort;
    }

    my $codeblock_ref = shift;
    if ( $_locale_set ) {
        use locale;
        return sort { $codeblock_ref->($a) cmp $codeblock_ref->($b) } @_;
    }

    return sort { $codeblock_ref->($a) cmp $codeblock_ref->($b) } @_;
}

sub _locale_sort (&@) {
    # not the world's greatest sort (understatement)
    # it doesn't sort hyphens in the middle of the string correctly
    # I have some code that does sort correctly but this hacky version
    # is enough for our needs and is 4000% quicker than the correct code
    use locale;

    my @sort_normally;
    my @sort_to_top;

    my $codeblock_ref = shift; # should return the value to sort
    foreach ( @_ ) {
        my $value = $codeblock_ref->($_);
        if ( $value =~ m/^[[:alnum:]]/ ) {
            push @sort_normally, $_;
        }
        else {
            push @sort_to_top, $_;
        }
    }

    return (
        (sort { $codeblock_ref->($a) cmp $codeblock_ref->($b) } @sort_to_top),
        (sort { $codeblock_ref->($a) cmp $codeblock_ref->($b) } @sort_normally),
    );
}

sub locale_sort_lists {
    # used for sorting lists/multilists on bs2/bs3. It sorts plain lists and
    # hierarchy lists (currently only two levels).
    my ($has_please_select,@menu) = @_;
    my $parent;
    my @child_list;
    my @parent_list;
    my $place_holder_ref;
    my $sorted_list_ref;
    my @temp_child;
    my @final_list;
    my $array_element = 0;
    my $please_select_text = '';
    my $please_select_value = '';
    my $please_select_sep = '';
    
    if ($menu[ 0 ] =~ m/^-{1,}/){
        $has_please_select = 1; # Incase the first option starts with one or more hyphens.
    }

    if ($has_please_select){
        my $pleaseselect = shift(@menu);
        $please_select_text = $pleaseselect;

        if ($pleaseselect =~ m/=/){
           ($please_select_text, $please_select_value) = split(/=/, $pleaseselect);
            $please_select_sep = '=';
            
        }elsif ($pleaseselect =~ m/!!/){
           ($please_select_text, $please_select_value) = split(/!!/, $pleaseselect);
            $please_select_sep = '!!';

        }elsif ($pleaseselect =~ m/!/){
           ($please_select_text, $please_select_value) = split(/!/, $pleaseselect);
            $please_select_sep = '!';
        }else{
            $please_select_value = $please_select_text;
            $please_select_sep = '!!';
        }
        $please_select_text = L($please_select_text);
    }

    foreach my $item (@menu){
        my ($menu_val) = $item;
        $array_element++;
        my $lone_parent = '';

        if ( $item =~ /=/ ) {
            ( $item, $menu_val ) = split( /=/, $item );
        }
        elsif ( $item =~ /!!/ ) {
            ( $item, $menu_val ) = split( /!!/, $item );
        }
        elsif ( $item =~ /!/ ) {
            ( $item, $menu_val ) = split( /!/, $item );
        }

        next if( $item =~ m/^-{3,}/ );
        $item =~ s/(^\s?)//;
        $item =~ s/^-+\s?/- /;

        if ( $item !~ m/^-\s/ ){
            $parent = L($item);
            $parent =~ s/(^\s?)|(\s?$)//g;

            push( @parent_list, { entry => $parent, value => $menu_val } );
            
            if ( ! defined($menu[ $array_element ]) || $menu[ $array_element ] !~ m/^-\s?/ ){
                $lone_parent = $parent;
                $place_holder_ref->{$parent}->{'no_children'} = $menu_val;
            }
        }elsif ( $item =~ m/^-\s/){
            my $new_item = $item;
            $new_item =~ s/^-\s//g;
            $new_item =~ s/(^\s?)|(\s?$)//g;
            my $nice_child = L($new_item); 
            $place_holder_ref->{$parent}->{$nice_child} = { entry => $nice_child, value => $menu_val };
            push( @child_list, { entry => $nice_child, value => $menu_val} ); 
        }
    }
    my @sort_parent = locale_sort_block { $_[0]->{'entry'} } @parent_list;

    foreach my $parent (@sort_parent){
        my $is_loner;
        my $parent_name = $parent->{'entry'};
        my $parent_value = $parent->{'value'};
        my $parent_string = "$parent_name!!$parent_value";
                
        foreach my $child (sort keys %{$place_holder_ref->{$parent_name}}){
            if ( $child eq 'no_children' ) {
                $is_loner = 1;
                next;
            }
            my $entry = $place_holder_ref->{$parent_name}->{$child}->{'entry'};
            my $value = $place_holder_ref->{$parent_name}->{$child}->{'value'};
            push( @temp_child, { entry => $entry, value => $value } );
        }
        my @sort_child = locale_sort_block { $_[0]->{'entry'} } @temp_child;
        
        foreach my $childl (@sort_child){
            my $child_entry = $childl->{'entry'};
            my $child_value = $childl->{'value'};
                    
            $sorted_list_ref->{$parent_string}->{$child_entry} = $child_value if( !$is_loner );
        }
        $sorted_list_ref->{$parent_string}->{'no_children'} = 'no_value' if( $is_loner );
        @temp_child = ();
    }

    foreach my $parent ( sort keys %{$sorted_list_ref}){
        push( @final_list, $parent );
        foreach my $child ( sort keys %{$sorted_list_ref->{$parent}}){
            next if( $child eq 'no_children' );
            my $ch_label = "- $child!!$sorted_list_ref->{$parent}->{$child}";
            push( @final_list, $ch_label );
        }
    }
    $please_select_text = "${please_select_text}${please_select_sep}${please_select_value}";
    unshift(@final_list, $please_select_text) if ($has_please_select);
    return @final_list;
}

# html translate
sub html_translate {
    my ($translated_text) = localise(@_);

    $translated_text =~ s/</&lt;/g;
    $translated_text =~ s/>/&gt;/g;
    $translated_text =~ s/\n/<br>/g;

    return $translated_text;
}

sub make_notation_safe{
    my $text = shift;

    $text =~ s/(\[|\])/~$1/g;
    return $text;
}
*L_NOTATION_SAFE = \&make_notation_safe;

# returns a hash of supported languages
sub available_languages {
    return %available_languages;
}

#
# Utility routines (not exported)
#

# check the users profile and fetch any saved language preferences
sub determine_language { $ENV{LANGUAGE} || $ENV{LANG} || 'en'; }
sub determine_timezone { $ENV{BB_TIMEZONE} || 'Europe/London'; }

# cache language handlers
sub get_language_handle {
    my $language = shift;

    return $cached_handlers{ $language } || __PACKAGE__->get_handle( $language );
}

# METHODS - these apply to the language objects ###############################
sub encoding_maketext {
    my $self = shift;

    my $value = $self->_maketext( @_ );

    return Encode::encode($ENCODING, $value);
}

*maketext = \&_maketext;
if ( $ENCODING ) { 
    *maketext = \&encoding_maketext;
}

sub _maketext {
    my $self = shift;
    my $value = $self->SUPER::maketext(@_);
    if( $value ) {
        $value =~ s/BBTECH_RESERVED_LEFT_BRACKET/[/g;
        $value =~ s/BBTECH_RESERVED_RIGHT_BRACKET/]/g;
    }

    # can also handle encoding here too

    return $value;
}

sub default_date_format {
    return '%d %b %Y';
}

# diagnostic method that gets called when maketext cannot translate a key
# if a devuser it returns a warning to help debug translations otherwise
# it returns the key (with some params substituted)
sub fail_method {
    my $self = shift;
    my $key  = shift;
    my @params = @_;

    my ($method, $args_list) = $self->extract_method( $key );

    if ( $method && $self->can($method) ) {
        # we can always translate date only keys
        my (@args) = map {
            m/^_(\d+)$/ ? $params[$1-1] : $_;
        } split /,/, $args_list;
        return $self->$method( @args );
    }

    $key =~ s/\[_(\d+)\]/$params[$1-1]/eg;

    return $key;
}

sub extract_method {
    if ( $_[1] && $_[1] =~ m/^\[([^]^[^,]+),([^\]]+)\]$/ ) {
        return ($1, $2);
    }

    return 0;
}

sub localise_or_fallback {
    my $self = shift;
    my $format = shift;

    my $first_attempt;
    if ( ref( $self ) ) {
        my $first_attempt = eval {
            $self->maketext( $format );
        };

        if ( $first_attempt && !$@ && $first_attempt !~ m/_DATEFORMAT/) {
            return $first_attempt;
        }
    }

    # fallback to english
    return localise_in('en', $format );
}

=head1 Utility Methods

These methods are interpolated within translation keys and are used within the context of a specifil language + string

=head2 dateformat

This method is used within a translation key

Usage:
    [% L('[datefomat,1234567890,_DATETIME_FORMAT_DATE]') %]

Out:
    Mon 1st Dec, 2015

=cut

sub dateformat {
    my ( $self, $epoch, $format_key ) = @_;

    if ( !$epoch || $epoch !~ m/^\d+$/ ) {
        # for backwards compatibility in Stream templates
        # old templates store the date string instead of an epoch
        return $epoch;
    }

    my $format_str = $self->maketext( $format_key );
    if ( $format_str =~ m/^_DATEFORMAT_/ ) {
        $format_str = $self->localise_or_fallback( uc($format_str) );
    }

    my $nice_locale = $self->maketext('_DEFAULT_SPELLCHECK_LANG');
    if( $nice_locale eq '_DEFAULT_SPELLCHECK_LANG' ){
        $nice_locale = 'en_GB';
    }

    my $timezone = $self->determine_timezone(); # default is Europe/London

    my $dt = DateTime->from_epoch( epoch => $epoch, locale => $nice_locale, time_zone => $timezone );
    return $dt->strftime( $format_str );
}

sub casual_time {
    my $self  = shift;
    my $epoch = shift;

    my $current_time = time;

    my $secs_diff = $current_time - $epoch;
    if ( $secs_diff < (30*60) ) {
        return $self->maketext("Just now");
    }
 
    my $today = DateTime->today( time_zone => $self->determine_timezone() );

    if ( $epoch > $today->epoch() ) {
        return $self->maketext("Today");
    }
    my $yesterday = $today->clone()->subtract( days => 1 );
    if ( $epoch > $yesterday->epoch() ) {
        return $self->maketext("Yesterday");
    }

    my $their_dt = DateTime->from_epoch( epoch => $epoch );

    my $this_week = $today->clone();
    if ( $this_week->day_of_week_0() ) {
        $this_week->subtract( days => $this_week->day_of_week_0() );
    }
    if ( $epoch > $this_week->epoch() ) {
        return $self->maketext($their_dt->day_name); #'This week';
    }

    my $last_week = $this_week->clone()->subtract( weeks => 1 );
    if ( $epoch > $last_week->epoch() ) {
        return $self->maketext($their_dt->day_name() . " last week");
    }

    my $this_month = $today->clone()->set_day(1);
    if ( $epoch > $this_month->epoch() ) {
        return $self->maketext("This month");
    }

    my $last_month = $this_month->clone()->subtract( months => 1 );
    if ( $epoch > $last_month->epoch() ) {
        return $self->maketext("Last month");
    }

    return $self->dateformat($epoch => "_DATEFORMAT_DATE");
}

sub default_dateformat {
    my $self = shift;
    my $format = uc(shift);

    return $self->localise_or_fallback( $format );
}

sub either_or {
    my $self = shift;
    if ( scalar(@_) <= 1 ) {
        return @_;
    }

    my $last_one = pop @_;

    return join(', ', @_) . L(' or ') . $last_one;
}

sub with_and {
    my $self = shift;
    if ( scalar(@_) <= 1 ) {
        return @_;
    }

    my $last_one = pop @_;

    return join(', ', @_) . L(' and ') . $last_one;
}

sub html {
    my $self = shift;
    my @raw_text = @_;

    my @escaped_text = map {
        if ( $_ ) {
            s/&/&amp;/g;
            s/</&lt;/g;
            s/>/&gt;/g;
            s/"/&quot;/g;
        }
        $_;
    } @raw_text;

    return wantarray ? @escaped_text : $escaped_text[0];
}

# override this in your module
sub ord {
    my $self = shift;

    return @_;
}

sub quant_sprintf {
    my ($lex)   = shift;
    my ($num)   = shift;
    my (@forms) = @_;  # $forms[0] is zeroth case, $forms[1] is singular, $forms[2] is plural

    my $key = $lex->numerate( $num, @forms );

    return $lex->sprintf( $key, $lex->numf($num) );
}
1;

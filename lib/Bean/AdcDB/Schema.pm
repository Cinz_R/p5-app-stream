use utf8;
package Bean::AdcDB::Schema;

our $VERSION = 1;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-06-23 12:28:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:fVRR1ODGrj6KZjZk/jq+Eg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

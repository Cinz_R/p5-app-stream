use utf8;
package Bean::AdcDB::Schema::Result::Location;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bean::AdcDB::Schema::Result::Location

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<locations>

=cut

__PACKAGE__->table("locations");

=head1 ACCESSORS

=head2 type

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 15

=head2 id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  is_nullable: 0
  size: 45

=head2 parent_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 mapping_id

  data_type: 'integer'
  is_nullable: 1

=head2 other

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 25

=head2 clicks

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 latitude

  data_type: 'float'
  is_nullable: 1
  size: [9,6]

=head2 longitude

  data_type: 'float'
  is_nullable: 1
  size: [9,6]

=head2 lat_in_radians

  data_type: 'float'
  is_nullable: 1
  size: [9,8]

=head2 lon_in_radians

  data_type: 'float'
  is_nullable: 1
  size: [9,8]

=head2 responses

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 is_place

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 is_capital

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 n_children

  data_type: 'smallint'
  default_value: 0
  is_nullable: 0

=head2 created_on

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: '0000-00-00 00:00:00'
  is_nullable: 0

=head2 last_updated

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  default_value: current_timestamp
  is_nullable: 0

=head2 retired

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "type",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 15 },
  "id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 45 },
  "parent_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "mapping_id",
  { data_type => "integer", is_nullable => 1 },
  "other",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 25 },
  "clicks",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "latitude",
  { data_type => "float", is_nullable => 1, size => [9, 6] },
  "longitude",
  { data_type => "float", is_nullable => 1, size => [9, 6] },
  "lat_in_radians",
  { data_type => "float", is_nullable => 1, size => [9, 8] },
  "lon_in_radians",
  { data_type => "float", is_nullable => 1, size => [9, 8] },
  "responses",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "is_place",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "is_capital",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "n_children",
  { data_type => "smallint", default_value => 0, is_nullable => 0 },
  "created_on",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => "0000-00-00 00:00:00",
    is_nullable => 0,
  },
  "last_updated",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    default_value => \"current_timestamp",
    is_nullable => 0,
  },
  "retired",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-06-23 12:28:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:AvxX8//TTaLkQBL0BrKvYg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

use utf8;
package Bean::AdcDB::Schema::Result::LocationPostcode;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bean::AdcDB::Schema::Result::LocationPostcode

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<location_postcodes>

=cut

__PACKAGE__->table("location_postcodes");

=head1 ACCESSORS

=head2 major_area

  data_type: 'varchar'
  is_nullable: 0
  size: 4

=head2 minor_area

  data_type: 'varchar'
  is_nullable: 0
  size: 3

=head2 location_id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 latitude

  data_type: 'float'
  is_nullable: 1
  size: [9,6]

=head2 longitude

  data_type: 'float'
  is_nullable: 1
  size: [9,6]

=cut

__PACKAGE__->add_columns(
  "major_area",
  { data_type => "varchar", is_nullable => 0, size => 4 },
  "minor_area",
  { data_type => "varchar", is_nullable => 0, size => 3 },
  "location_id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "latitude",
  { data_type => "float", is_nullable => 1, size => [9, 6] },
  "longitude",
  { data_type => "float", is_nullable => 1, size => [9, 6] },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<majorarea>

=over 4

=item * L</major_area>

=item * L</minor_area>

=back

=cut

__PACKAGE__->add_unique_constraint("majorarea", ["major_area", "minor_area"]);


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-06-23 12:28:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:LK+0jtZVt/mBViuM6Q2SCQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

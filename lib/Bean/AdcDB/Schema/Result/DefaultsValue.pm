use utf8;
package Bean::AdcDB::Schema::Result::DefaultsValue;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bean::AdcDB::Schema::Result::DefaultsValue

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<defaults_value>

=cut

__PACKAGE__->table("defaults_value");

=head1 ACCESSORS

=head2 id_defaults_type

  data_type: 'integer'
  is_nullable: 0

=head2 id_board

  data_type: 'varchar'
  is_nullable: 0
  size: 4

=head2 consultant

  data_type: 'varchar'
  is_nullable: 0
  size: 32

=head2 team

  data_type: 'varchar'
  is_nullable: 0
  size: 32

=head2 office

  data_type: 'varchar'
  is_nullable: 0
  size: 32

=head2 company

  data_type: 'varchar'
  is_nullable: 0
  size: 32

=head2 value

  data_type: 'varchar'
  is_nullable: 0
  size: 128

=head2 last_changed

  data_type: 'integer'
  is_nullable: 1

=head2 changed_by

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=cut

__PACKAGE__->add_columns(
  "id_defaults_type",
  { data_type => "integer", is_nullable => 0 },
  "id_board",
  { data_type => "varchar", is_nullable => 0, size => 4 },
  "consultant",
  { data_type => "varchar", is_nullable => 0, size => 32 },
  "team",
  { data_type => "varchar", is_nullable => 0, size => 32 },
  "office",
  { data_type => "varchar", is_nullable => 0, size => 32 },
  "company",
  { data_type => "varchar", is_nullable => 0, size => 32 },
  "value",
  { data_type => "varchar", is_nullable => 0, size => 128 },
  "last_changed",
  { data_type => "integer", is_nullable => 1 },
  "changed_by",
  { data_type => "varchar", is_nullable => 1, size => 50 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<defaults_value_uq>

=over 4

=item * L</id_defaults_type>

=item * L</id_board>

=item * L</consultant>

=item * L</team>

=item * L</office>

=item * L</company>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "defaults_value_uq",
  [
    "id_defaults_type",
    "id_board",
    "consultant",
    "team",
    "office",
    "company",
  ],
);


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-06-23 12:28:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:b9rhTp1U9fL1TRzEGi6+fw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

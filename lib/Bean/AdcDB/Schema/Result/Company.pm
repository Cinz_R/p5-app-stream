use utf8;
package Bean::AdcDB::Schema::Result::Company;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bean::AdcDB::Schema::Result::Company

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<company>

=cut

__PACKAGE__->table("company");

=head1 ACCESSORS

=head2 name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 50

=head2 nice_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 100

=head2 admin_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 50

=head2 admin_email

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 100

=head2 aplitrak

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 aplitrak_domain

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 address

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 150

=head2 ip_restrictions

  data_type: 'varchar'
  is_nullable: 1
  size: 1024

=head2 salesperson

  data_type: 'varchar'
  default_value: 'Unassigned'
  is_nullable: 0
  size: 20

=head2 status

  data_type: 'varchar'
  default_value: 'Live'
  is_nullable: 0
  size: 8

=head2 locked

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 banned_words

  data_type: 'text'
  is_nullable: 0

=head2 response_subject_line

  data_type: 'varchar'
  default_value: 'Ad response to: [jobref]/[subject] from [nice_board_name]'
  is_nullable: 0
  size: 150

=head2 default_cc_to

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 user_limit

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 4

=head2 slot_limit

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 4

=head2 phone

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 40

=head2 fax

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 40

=head2 date_added

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 added_by

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 20

=head2 account_manager

  data_type: 'varchar'
  default_value: 'Lee'
  is_nullable: 0
  size: 20

=head2 send_autoresponse

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 eaa_tag

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 confirm_location

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 use_applyonline

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 use_salarybanding

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 allow_html_in_desc

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 allow_contacts_in_desc

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 allow_allsites

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 use_delete_function

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 office_level_reports

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 account_type

  data_type: 'enum'
  default_value: 'agency'
  extra: {list => ["agency","employer"]}
  is_nullable: 0

=head2 posting_engine

  data_type: 'varchar'
  default_value: 'hiens_queue'
  is_nullable: 0
  size: 11

=head2 parent_group_id

  data_type: 'smallint'
  default_value: 0
  is_nullable: 0

=head2 use_multi_delete

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 email_cons_passwd

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 allow_instant_rebroadcast

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 allow_regular_rebroadcast

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 allow_multi_ir

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 forward_to_iprofile

  data_type: 'enum'
  default_value: 'notset'
  extra: {list => ["notset","enabled","disabled"]}
  is_nullable: 0

=head2 requires_authorisation

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 email_expiry_reports

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 force_spellcheck

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 lock_jobref

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 slot_life_days

  data_type: 'integer'
  default_value: 30
  is_nullable: 1

=head2 slots_used_recently

  data_type: 'integer'
  default_value: 0
  is_nullable: 1

=head2 can_fw_cvs

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 send_passwd

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 tl_can_post

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 tl_can_del

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 tl_can_rebroadcast

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 tl_can_mod_users

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 responses_on_behalf_of

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 post_company_not_contactname

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 multi_applyonline

  data_type: 'enum'
  default_value: 'inherit'
  extra: {list => ["inherit",0,1]}
  is_nullable: 0

=head2 has_bg

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 auto_delete

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 auto_jobref

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 user_level_report

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 auto_refresh

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 date_expires

  data_type: 'varchar'
  is_nullable: 1
  size: 11

=head2 locale

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 6

=head2 fw_progressed_cvs

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 client_survey_emailed

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 client_survey_completed

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 is_sla_client

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 send_responses

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 1

=head2 send_notifications

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 send_notifications_exec

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 send_notifications_without_links

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 response_extra_headers

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 stream

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 stream_trial

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 tp_auto_search

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 adcourier

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 stream_expiry

  data_type: 'integer'
  is_nullable: 1

=head2 adcourier_expiry

  data_type: 'integer'
  is_nullable: 1

=head2 aplitrak_expiry

  data_type: 'integer'
  is_nullable: 1

=head2 talentpod_expiry

  data_type: 'integer'
  is_nullable: 1

=head2 use_html_description

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 paginator_responses

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 can_fw_cvs_in_responses

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 timezone

  data_type: 'varchar'
  default_value: 'Europe/London'
  is_nullable: 0
  size: 50

=head2 realname_order

  data_type: 'enum'
  default_value: 'first_last'
  extra: {list => ["first_last","last_first"]}
  is_nullable: 0

=head2 twitter_post

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_contact_name

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_contact_email

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_contact_telephone

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_contact_fax

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_contact_address

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 hide_contact_name

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 hide_contact_email

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 hide_contact_telephone

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 hide_contact_fax

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 hide_contact_address

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 board_grouping

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 talentmine

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 talentmine_expiry

  data_type: 'integer'
  is_nullable: 1

=head2 auth_reminder

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 allow_edit

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 custom_css

  data_type: 'varchar'
  is_nullable: 1
  size: 250

=head2 salary_keys

  data_type: 'text'
  is_nullable: 1

=head2 can_hide

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 can_free_slots

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 billable

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 use_share_tracker

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 mandatory_apply_url

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 hide_jobtype

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 hide_duration

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 bg_type

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 send_failure_emails

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 mr_ted_report

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 talentpod_two

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 talentpod_two_expiry

  data_type: 'integer'
  is_nullable: 1

=head2 use_campaign_emails

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 applyonline_no_edit

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 quota_set_hierarchy

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 allow_brand

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 allow_to_expand_users_list

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 50 },
  "nice_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 100 },
  "admin_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 50 },
  "admin_email",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 100 },
  "aplitrak",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "aplitrak_domain",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "address",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 150 },
  "ip_restrictions",
  { data_type => "varchar", is_nullable => 1, size => 1024 },
  "salesperson",
  {
    data_type => "varchar",
    default_value => "Unassigned",
    is_nullable => 0,
    size => 20,
  },
  "status",
  {
    data_type => "varchar",
    default_value => "Live",
    is_nullable => 0,
    size => 8,
  },
  "locked",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "banned_words",
  { data_type => "text", is_nullable => 0 },
  "response_subject_line",
  {
    data_type => "varchar",
    default_value => "Ad response to: [jobref]/[subject] from [nice_board_name]",
    is_nullable => 0,
    size => 150,
  },
  "default_cc_to",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "user_limit",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 4 },
  "slot_limit",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 4 },
  "phone",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 40 },
  "fax",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 40 },
  "date_added",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "added_by",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 20 },
  "account_manager",
  {
    data_type => "varchar",
    default_value => "Lee",
    is_nullable => 0,
    size => 20,
  },
  "send_autoresponse",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "eaa_tag",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "confirm_location",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "use_applyonline",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "use_salarybanding",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "allow_html_in_desc",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "allow_contacts_in_desc",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "allow_allsites",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "use_delete_function",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "office_level_reports",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "account_type",
  {
    data_type => "enum",
    default_value => "agency",
    extra => { list => ["agency", "employer"] },
    is_nullable => 0,
  },
  "posting_engine",
  {
    data_type => "varchar",
    default_value => "hiens_queue",
    is_nullable => 0,
    size => 11,
  },
  "parent_group_id",
  { data_type => "smallint", default_value => 0, is_nullable => 0 },
  "use_multi_delete",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "email_cons_passwd",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "allow_instant_rebroadcast",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "allow_regular_rebroadcast",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "allow_multi_ir",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "forward_to_iprofile",
  {
    data_type => "enum",
    default_value => "notset",
    extra => { list => ["notset", "enabled", "disabled"] },
    is_nullable => 0,
  },
  "requires_authorisation",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "email_expiry_reports",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "force_spellcheck",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "lock_jobref",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "slot_life_days",
  { data_type => "integer", default_value => 30, is_nullable => 1 },
  "slots_used_recently",
  { data_type => "integer", default_value => 0, is_nullable => 1 },
  "can_fw_cvs",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "send_passwd",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "tl_can_post",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "tl_can_del",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "tl_can_rebroadcast",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "tl_can_mod_users",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "responses_on_behalf_of",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "post_company_not_contactname",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "multi_applyonline",
  {
    data_type => "enum",
    default_value => "inherit",
    extra => { list => ["inherit", 0, 1] },
    is_nullable => 0,
  },
  "has_bg",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "auto_delete",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "auto_jobref",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "user_level_report",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "auto_refresh",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "date_expires",
  { data_type => "varchar", is_nullable => 1, size => 11 },
  "locale",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 6 },
  "fw_progressed_cvs",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "client_survey_emailed",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "client_survey_completed",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "is_sla_client",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "send_responses",
  { data_type => "tinyint", default_value => 1, is_nullable => 1 },
  "send_notifications",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "send_notifications_exec",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "send_notifications_without_links",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "response_extra_headers",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "stream",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "stream_trial",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "tp_auto_search",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "adcourier",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "stream_expiry",
  { data_type => "integer", is_nullable => 1 },
  "adcourier_expiry",
  { data_type => "integer", is_nullable => 1 },
  "aplitrak_expiry",
  { data_type => "integer", is_nullable => 1 },
  "talentpod_expiry",
  { data_type => "integer", is_nullable => 1 },
  "use_html_description",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "paginator_responses",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "can_fw_cvs_in_responses",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "timezone",
  {
    data_type => "varchar",
    default_value => "Europe/London",
    is_nullable => 0,
    size => 50,
  },
  "realname_order",
  {
    data_type => "enum",
    default_value => "first_last",
    extra => { list => ["first_last", "last_first"] },
    is_nullable => 0,
  },
  "twitter_post",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_contact_name",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_contact_email",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_contact_telephone",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_contact_fax",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_contact_address",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "hide_contact_name",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "hide_contact_email",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "hide_contact_telephone",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "hide_contact_fax",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "hide_contact_address",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "board_grouping",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "talentmine",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "talentmine_expiry",
  { data_type => "integer", is_nullable => 1 },
  "auth_reminder",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "allow_edit",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "custom_css",
  { data_type => "varchar", is_nullable => 1, size => 250 },
  "salary_keys",
  { data_type => "text", is_nullable => 1 },
  "can_hide",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "can_free_slots",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "billable",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "use_share_tracker",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "mandatory_apply_url",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "hide_jobtype",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "hide_duration",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "bg_type",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "send_failure_emails",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "mr_ted_report",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "talentpod_two",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "talentpod_two_expiry",
  { data_type => "integer", is_nullable => 1 },
  "use_campaign_emails",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "applyonline_no_edit",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "quota_set_hierarchy",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "allow_brand",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "allow_to_expand_users_list",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->set_primary_key("name");


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-06-23 12:28:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ReGQDT1UAl04Wx54sEbETw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

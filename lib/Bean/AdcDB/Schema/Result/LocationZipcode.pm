use utf8;
package Bean::AdcDB::Schema::Result::LocationZipcode;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bean::AdcDB::Schema::Result::LocationZipcode

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<location_zipcodes>

=cut

__PACKAGE__->table("location_zipcodes");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 zipcode

  data_type: 'varchar'
  is_nullable: 0
  size: 10

=head2 country_id

  data_type: 'integer'
  is_nullable: 0

=head2 location_id

  data_type: 'integer'
  is_nullable: 0

=head2 latitude

  data_type: 'float'
  is_nullable: 1
  size: [9,6]

=head2 longitude

  data_type: 'float'
  is_nullable: 1
  size: [9,6]

=head2 insert_datetime

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=head2 update_datetime

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "zipcode",
  { data_type => "varchar", is_nullable => 0, size => 10 },
  "country_id",
  { data_type => "integer", is_nullable => 0 },
  "location_id",
  { data_type => "integer", is_nullable => 0 },
  "latitude",
  { data_type => "float", is_nullable => 1, size => [9, 6] },
  "longitude",
  { data_type => "float", is_nullable => 1, size => [9, 6] },
  "insert_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
  "update_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-06-23 12:28:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:xy4kjKVhbiSwKoMAac2+gw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

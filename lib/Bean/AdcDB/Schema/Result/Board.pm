use utf8;
package Bean::AdcDB::Schema::Result::Board;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bean::AdcDB::Schema::Result::Board

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<board>

=cut

__PACKAGE__->table("board");

=head1 ACCESSORS

=head2 id

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 50

=head2 nice_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 100

=head2 public

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 logo

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 frontpage

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 notes

  data_type: 'blob'
  is_nullable: 1

=head2 industries

  data_type: 'blob'
  is_nullable: 1

=head2 short_desc

  data_type: 'blob'
  is_nullable: 1

=head2 type

  data_type: 'varchar'
  default_value: 'http'
  is_nullable: 0
  size: 20

=head2 delete_function

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 applyonline

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 template

  data_type: 'longtext'
  is_nullable: 0

=head2 changelog

  data_type: 'text'
  is_nullable: 0

=head2 adverts_posted

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 responses_received

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 spam_received

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 template_changed

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 expired

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 aplitrak_adverts_posted

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 contact_email

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 50

=head2 telephone

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 30

=head2 domain_acl

  data_type: 'text'
  is_nullable: 1

=head2 free_to_post

  data_type: 'smallint'
  default_value: 0
  is_nullable: 0

=head2 login_notes

  data_type: 'text'
  is_nullable: 0

=head2 general_notes

  data_type: 'text'
  is_nullable: 0

=head2 skip_in_queue

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 last_slow_query

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 use_replyto_instead

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 permanent

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 contract

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 temporary

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 allow_dupes_from

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 added_by

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 updated_by

  data_type: 'varchar'
  is_nullable: 1
  size: 50

=head2 tech_contact

  data_type: 'varchar'
  is_nullable: 1
  size: 255

=head2 account_type

  data_type: 'enum'
  default_value: 'agency'
  extra: {list => ["agency","employer","both"]}
  is_nullable: 0

=head2 board_survey_emailed

  data_type: 'varchar'
  default_value: 0
  is_nullable: 0
  size: 1

=head2 board_survey_completed

  data_type: 'varchar'
  default_value: 0
  is_nullable: 0
  size: 1

=head2 template_email_address

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 100

=head2 salesforce_email_address

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 100

=head2 other_email_address

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 1
  size: 100

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 100

=head2 template_group_id

  data_type: 'varchar'
  default_value: 0
  is_nullable: 0
  size: 4

=head2 refreshable

  data_type: 'smallint'
  default_value: 0
  is_nullable: 0

=head2 cvsearch

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 cvsearch_domain_acl

  data_type: 'text'
  is_nullable: 0

=head2 fill_colour

  data_type: 'varchar'
  default_value: '#FFFFFF'
  is_nullable: 0
  size: 10

=head2 sla

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 time_created

  data_type: 'integer'
  is_nullable: 1

=head2 time_changed

  data_type: 'integer'
  is_nullable: 1

=head2 email_feed_type

  data_type: 'enum'
  default_value: 'job_board'
  extra: {list => ["job_board","manual","manual_free","media_buy"]}
  is_nullable: 0

=head2 preview_application

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

used to preview job

=head2 cvsearch_type

  data_type: 'enum'
  extra: {list => ["api","robot"]}
  is_nullable: 1

=head2 cvsearch_trial

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 cvsearch_free

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 allows_acc_sharing

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 edit_jobref

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_jobtitle

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_startdate

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_enddate

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_duration

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_skills

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_description

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_client_details

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_profile

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_summary

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_short_summary

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 edit_brief_description

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 editable_time

  data_type: 'varchar'
  default_value: 0
  is_nullable: 0
  size: 10

=head2 temp_disabled

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 board_type

  data_type: 'enum'
  default_value: 'external'
  extra: {list => ["internal","external","social","public"]}
  is_nullable: 0

=head2 can_mailshot

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 contact_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 100

=head2 cvsearch_keywords_required

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 email_from_subject

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 use_eaa_tags

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 checked_merged_fields

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 instant_resend

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 0

=head2 cvsearch_display_name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 100

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 50 },
  "nice_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 100 },
  "public",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "logo",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "frontpage",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "notes",
  { data_type => "blob", is_nullable => 1 },
  "industries",
  { data_type => "blob", is_nullable => 1 },
  "short_desc",
  { data_type => "blob", is_nullable => 1 },
  "type",
  {
    data_type => "varchar",
    default_value => "http",
    is_nullable => 0,
    size => 20,
  },
  "delete_function",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "applyonline",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "template",
  { data_type => "longtext", is_nullable => 0 },
  "changelog",
  { data_type => "text", is_nullable => 0 },
  "adverts_posted",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "responses_received",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "spam_received",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "template_changed",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "expired",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "aplitrak_adverts_posted",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "contact_email",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 50 },
  "telephone",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 30 },
  "domain_acl",
  { data_type => "text", is_nullable => 1 },
  "free_to_post",
  { data_type => "smallint", default_value => 0, is_nullable => 0 },
  "login_notes",
  { data_type => "text", is_nullable => 0 },
  "general_notes",
  { data_type => "text", is_nullable => 0 },
  "skip_in_queue",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "last_slow_query",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "use_replyto_instead",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "permanent",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "contract",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "temporary",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "allow_dupes_from",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "added_by",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "updated_by",
  { data_type => "varchar", is_nullable => 1, size => 50 },
  "tech_contact",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "account_type",
  {
    data_type => "enum",
    default_value => "agency",
    extra => { list => ["agency", "employer", "both"] },
    is_nullable => 0,
  },
  "board_survey_emailed",
  { data_type => "varchar", default_value => 0, is_nullable => 0, size => 1 },
  "board_survey_completed",
  { data_type => "varchar", default_value => 0, is_nullable => 0, size => 1 },
  "template_email_address",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 100 },
  "salesforce_email_address",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 100 },
  "other_email_address",
  { data_type => "varchar", default_value => "", is_nullable => 1, size => 100 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "template_group_id",
  { data_type => "varchar", default_value => 0, is_nullable => 0, size => 4 },
  "refreshable",
  { data_type => "smallint", default_value => 0, is_nullable => 0 },
  "cvsearch",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "cvsearch_domain_acl",
  { data_type => "text", is_nullable => 0 },
  "fill_colour",
  {
    data_type => "varchar",
    default_value => "#FFFFFF",
    is_nullable => 0,
    size => 10,
  },
  "sla",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "time_created",
  { data_type => "integer", is_nullable => 1 },
  "time_changed",
  { data_type => "integer", is_nullable => 1 },
  "email_feed_type",
  {
    data_type => "enum",
    default_value => "job_board",
    extra => { list => ["job_board", "manual", "manual_free", "media_buy"] },
    is_nullable => 0,
  },
  "preview_application",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "cvsearch_type",
  {
    data_type => "enum",
    extra => { list => ["api", "robot"] },
    is_nullable => 1,
  },
  "cvsearch_trial",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "cvsearch_free",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "allows_acc_sharing",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "edit_jobref",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_jobtitle",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_startdate",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_enddate",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_duration",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_skills",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_description",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_client_details",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_profile",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_summary",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_short_summary",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "edit_brief_description",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "editable_time",
  { data_type => "varchar", default_value => 0, is_nullable => 0, size => 10 },
  "temp_disabled",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "board_type",
  {
    data_type => "enum",
    default_value => "external",
    extra => { list => ["internal", "external", "social", "public"] },
    is_nullable => 0,
  },
  "can_mailshot",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "contact_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 100 },
  "cvsearch_keywords_required",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "email_from_subject",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "use_eaa_tags",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "checked_merged_fields",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "instant_resend",
  { data_type => "tinyint", default_value => 0, is_nullable => 0 },
  "cvsearch_display_name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 100 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<id>

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->add_unique_constraint("id", ["id"]);

=head2 C<name>

=over 4

=item * L</name>

=back

=cut

__PACKAGE__->add_unique_constraint("name", ["name"]);


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-06-23 12:28:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kAlHWQeRoRjr/C+9mJf/vA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

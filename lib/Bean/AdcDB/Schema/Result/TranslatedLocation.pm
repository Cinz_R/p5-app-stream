use utf8;
package Bean::AdcDB::Schema::Result::TranslatedLocation;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bean::AdcDB::Schema::Result::TranslatedLocation

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<translated_locations>

=cut

__PACKAGE__->table("translated_locations");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 45

=head2 lang

  data_type: 'varchar'
  default_value: 'DEF'
  is_nullable: 1
  size: 7

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "name",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 45 },
  "lang",
  { data_type => "varchar", default_value => "DEF", is_nullable => 1, size => 7 },
);


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-06-23 12:28:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:iOWCwl1Wt3+pBJCouCeRrw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

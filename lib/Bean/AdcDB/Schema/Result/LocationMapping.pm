use utf8;
package Bean::AdcDB::Schema::Result::LocationMapping;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Bean::AdcDB::Schema::Result::LocationMapping

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<location_mappings>

=cut

__PACKAGE__->table("location_mappings");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  default_value: 0
  is_nullable: 0

=head2 board

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 50

=head2 value

  data_type: 'varchar'
  is_nullable: 1
  size: 500

=head2 mapping_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", default_value => 0, is_nullable => 0 },
  "board",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 50 },
  "value",
  { data_type => "varchar", is_nullable => 1, size => 500 },
  "mapping_id",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=item * L</board>

=back

=cut

__PACKAGE__->set_primary_key("id", "board");


# Created by DBIx::Class::Schema::Loader v0.07035 @ 2013-06-23 12:28:28
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:PZjbXr6rgRH1oaahwKqDKw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

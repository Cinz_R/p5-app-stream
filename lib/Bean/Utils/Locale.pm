package Bean::Utils::Locale;

# Purpose:
#   Provides functions to Bean::Locale and Bean::Utils

use strict;

use Exporter ();
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

use DateTime;
use Carp;

$VERSION = 1.00;

@ISA	= qw(Exporter);
@EXPORT = qw( &bean_time_timezone &bean_time_strftime &bean_time_epoch );
%EXPORT_TAGS = ();
@EXPORT_OK   = qw( &lutoc );


=head2 bean_time_timezone

Returns the current value of $ENV{BB_TIMEZONE} or the default timezone 'Europe/London'

=cut

sub bean_time_timezone {
    my $time_zone = $ENV{BB_TIMEZONE} // 'Europe/London'; # Default
    my $server_tz='Europe/London'; # Time Zone of the server

    return wantarray ? ($time_zone, $server_tz) : $time_zone;
}

sub bean_time_strftime {
    my (  $binned_username , $epoch , $strftime , $locale ) = @_;

    $locale ||= 'en_US';
	$epoch ||= time();

	my ($time_zone, $server_tz) = bean_time_timezone();

    if ( $locale eq 'en_USA' ) {
        $locale = 'en_US';
    }

    # Protect against silly epochs.
    unless( $epoch =~ /^\d+$/ ){
        return '-';
    }

	# Now return the time in the format provided in the timezone wanted
    ## This was using  time_zone => 'Europe/London'. If you find bugs about
    ## epoch times not being quite right, re-instate that.
    ## If our data is not broken, we should not need that.
    ## See http://en.wikipedia.org/wiki/Unix_time

    my $time = DateTime->from_epoch( epoch => $epoch, locale => $locale );
    $time->set_time_zone($time_zone);

    return $time->strftime($strftime); # Returns the time in the desired timezone
}

sub bean_time_epoch {
	my ($binned_username,$year,$month,$day,$hour,$minute,$second) = @_;

    my ($time_zone, $server_tz) = bean_time_timezone();

	# Now return the epoch for the time in their time zone
	my $dt = DateTime->new( year => $year, month => $month, day => $day, hour => $hour, minute => $minute, second => $second, time_zone => $time_zone);
	return $dt->epoch;
}

sub lutoc {
    my($login)=$_[0];
    my ( $user, $domain ) = split '@', $login;
    my ( $company, $office, $team ) = reverse split /\./, $domain;
    return ( $user, $team, $office, $company );
}

1;

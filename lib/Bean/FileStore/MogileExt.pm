#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id: MogileFS.pm 20562 2009-06-15 07:28:26Z andy $

package Bean::FileStore::MogileExt;

use strict;

use base 'Bean::FileStore::MogileFS';

# Bean::FileStore::Mogile that will list every single key
# and probably crash the server :(

sub list_keys {
    my $pkg      = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my $mogc = $pkg->Mogc() or return;
    my $filename = Bean::FileStore::MogileFS::_build_filename( $key, $class );

    my @keys;
    $mogc->foreach_key(
        prefix => $filename,
        sub {
            push @keys, $_[0];
        },
    );

    return map { s/^\Q$class\E\-//; $_ } @keys;
}

sub foreach_key {
    my $pkg      = shift;
    my $key      = shift;
    my $class    = shift;
    my $code_ref = shift;

    my $mogc = $pkg->Mogc() or return;
    my $filename = Bean::FileStore::MogileFS::_build_filename( $key, $class );

    $mogc->foreach_key(
        prefix => $filename,
        sub {
            my $key = shift;
            $key =~ s/^\A$class\E\-//;
            $code_ref->( $key );
            
        },
    );
}

1;

#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id: MogileFS.pm 54406 2011-06-18 16:04:46Z pete $

package Bean::FileStore::MogileFS;

=head1 NAME

Bean::FileStore::MogileFS - file abstraction layer around MogileFS

=head1 OVERVIEW

Check the MogileFS::Client docs for more details

=head2 GOTCHAS

 class = determines replication policy (ie. how many copies mogilefs makes)
         The class must already be setup on MogileFS (mogadm class add CLASSNAME)

=cut

use strict;

use base 'Bean::FileStore::Base';

use Storable;
use Sys::Hostname;
use Encode;

CHECK {
    # we need this module installed but only load it when we use it
    require MogileFS::Client;
};

sub MinDevCount {
    return 2;
}

sub Domain {
    return 'adcourier.com::cvsearch';
}

my $Trackers_ref;
sub Trackers {
    if ( !$Trackers_ref ) {
        my $hostname = Sys::Hostname::hostname();

        if ( $hostname =~ m/broadbean.priv|monsoon|earthquake|cyclone/ ) {
            $Trackers_ref = ['192.168.1.6:6001'];
        }
        else {
            $Trackers_ref = [
                 '46.254.116.47:6001', # blade15.gs
                 '46.254.116.48:6001', # blade16.gs
#                '46.254.116.114:6001', # mog0.gs
#                '46.254.116.115:6001', # mog1.gs
            ];
        }
    }

    return $Trackers_ref;
}

my $Mogc; # singleton
sub Mogc {
    my $pkg = shift;
    if ( !$Mogc ) {
        # connect when needed
        require MogileFS::Client;
        require LWP::UserAgent;
        $Mogc = MogileFS::Client->new(
            domain => Domain(),
            hosts  => Trackers(), # address of mogilefsd (aka tracker)
        );

        if ( !$Mogc ) {
            $pkg->fail( "Unable to connect to MogileFS" );
        }
    }
    return $Mogc;
}

my $Mogadm; # MogileFS::Admin singleton
sub Mogadm {
    my $pkg = shift;
    if ( !$Mogadm ) {
        # MogileFS::Admin is used to create classes (that is it!)
        require MogileFS::Admin;
        $Mogadm = MogileFS::Admin->new(
            hosts => Trackers(),
        );

        if ( !$Mogadm ) {
            $pkg->fail( "Unable to connect to MogileFS::Admin" );
        }
    }
    return $Mogadm;
}

sub new_file {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;

    my $mogc = $pkg->Mogc();

    my $filename = _build_filename( $key, $class );
    return $mogc->new_file( $filename, $class );

    my $fh;
    {
        # doesn't work!!!
        use open IO => ':raw';
        $fh = $mogc->new_file( $key, $class );
    };

#    if ( $fh ) {
#
#    }

    return $fh;
}

sub get_file_data {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my $mogc = $pkg->Mogc();

    my $filename = _build_filename( $key, $class );
    my $dataref = $mogc->get_file_data( $filename );

    if ( $dataref ) {
        return $$dataref;
    }

    $pkg->fail( "Unable to retrieve '$key'" );
}

sub store_content {
    my $pkg = shift;
    my $key       = shift;
    my $class     = shift; # determines replication policy
    my $content   = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my %headers;
    if ( $options_ref->{content_type} ) {
        $headers{content_type} = delete $options_ref->{content_type};
    }
    if ( $options_ref->{filename} ) {
        my $filename = delete $options_ref->{filename};
        $headers{content_disposition} = qq(attachment;filename=$filename);

        if ( !$headers{content_type} ) {
            $headers{content_type} = $pkg->identify_mime_type( $filename );
        }
    }

    if ( %headers ) {
        $headers{content_type} ||= delete $options_ref->{fallback_content_type};

        require HTTP::Headers;
        my $h = HTTP::Headers->new( %headers );

        my $eol = "\015";
        my $header = "X-FileStore-Header: Inline\n".$h->as_string();

        $header =~ s/\n?\z/\n\n/;

        if ( Encode::is_utf8( $content ) == Encode::is_utf8( $header ) ) {
#             print STDERR "Headers and content have the smae encoding\n";
            $content = $header . $content;
        }
        elsif ( Encode::is_utf8( $content ) ) {
            # we don't want to "taint" the content by making a binary string utf8
#             print STDERR "Encoding utf8 content\n";
            $content = $header . Encode::encode_utf8( $content );
        }
        elsif ( Encode::is_utf8( $header ) ) {
#             print STDERR "UTF8 header: " . length( $header ) . "\n";
#             print STDERR "Byte header: " . length( Encode::encode_utf8( $header ) ) . "\n";

#             my $utf8_header = Encode::encode_utf8( $header );
#             my @chars = split //, $header;
#             my @bytes = split //, $utf8_header;
#
#             print STDERR "char\t=>\tbyte\n";
#             while ( @chars || @bytes ) {
#                 my $char = shift @chars;
#                 my $byte = shift @bytes;
#
#                 print STDERR "$char\t=>\t$byte\n";
#             }
#             print STDERR "===============\n";
#
#             print STDERR "Encoding utf8 header\n";
            $content = Encode::encode_utf8( $header ) . $content;
        }
    }

    my $mogc = $pkg->Mogc();
    my $filename = _build_filename( $key, $class );

    if ( Encode::is_utf8( $content ) ) {
        # encode from characters to octets
        # otherwise syswrite will croak
#         print STDERR "Encoding utf8 headers-content\n";
        $content = Encode::encode_utf8( $content );
    }

    my $bytes_stored = $mogc->store_content($filename,$class, $content );

    if ( defined( $bytes_stored ) ) {
        return 1;
    }

    $pkg->fail( "Unable to store '$key' in '$class'" );
}

sub list_keys {
    my $pkg      = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my $mogc = $pkg->Mogc() or return;
    my $filename = _build_filename( $key, $class );
    my $keys_ref = $mogc->list_keys( $filename );

    if ( !$keys_ref ) {
        return;
    }

    return map { s/^\Q$class\E\-//; $_ } @$keys_ref;
}

sub error {
    my $mogc = $Mogc or return;

    my $errcode = $mogc->errcode;

    if ( !$errcode ) {
        return undef;
    }

    return $errcode . ': ' . $mogc->errstr;
}

sub create_class {
    my $pkg = shift;
    my $class = shift;

    my $mogadm = $pkg->Mogadm();
    my $domains_ref = eval {
        $mogadm->get_domains();
    };

    if ( $@ ) {
        $pkg->fail( "Unable to check domains: $@" );
    }

    my $domain = $pkg->Domain();

    die "Domain not found." unless $domains_ref && $domains_ref->{$domain};
    die "Class already exists." if $domains_ref->{$domain}->{$class};

    $mogadm->create_class($domain, $class, $pkg->MinDevCount() );

    if ( $mogadm->err ) {
        die "Error creating class: ". $mogadm->errstr;
    }

    return 1;
}

sub list_classes {
    my $pkg = shift;
    my $mogadm = $pkg->Mogadm();
    my $domains_ref = eval {
        $mogadm->get_domains();
    };

    if ( $@ ) {
        $pkg->fail( "Unable to check domains: $@" );
    }

    my $domain = $pkg->Domain();

    die "Domain not found." unless $domains_ref && $domains_ref->{$domain};

    return sort keys %{ $domains_ref->{$domain} };
}

sub serve_file {
    my $pkg      = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache   = $options_ref->{use_cache};

    my $mogc = $pkg->Mogc() or return;
    my $filename = _build_filename( $key, $class );

    my @urls = $mogc->get_paths( $filename );

    if ( !@urls ) {
        $pkg->fail( "Unable to find file: $key" );
    }

    return $pkg->serve_urls( $options_ref, @urls );
}

sub file_exists {
    my $pkg = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache   = $options_ref->{use_cache};

    my $mogc = $pkg->Mogc() or return;
    my $filename = _build_filename( $key, $class );

    my @urls = $mogc->get_paths( $filename );

    if ( @urls ) {
        return 1;
    }

    return 0;
}

sub delete_file {
    my $pkg   = shift;
    my $key   = shift;
    my $class = shift;

    my $mogc = $pkg->Mogc() or return;
    my $filename = _build_filename( $key, $class );

    return $mogc->delete( $filename );
}

sub _build_filename {
           # class  -    key
    return $_[1] . '-' . $_[0];
}

=head1 SEE ALSO

L<MogileFS::Client>, L<mogadm>

=cut
1;

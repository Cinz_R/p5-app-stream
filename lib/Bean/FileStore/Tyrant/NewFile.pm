#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id$

package Bean::FileStore::Tyrant;
# Virtually a copy and paste of MogileFS::NewHTTPFile
# NOTE: Meant to be used within IO::WrapTie...
# 

use strict;

use base qw(Bean::FileStore::NewFile);

sub CLOSE {
    my $self = shift;

    my $key = $self->key();
    my $class = $self->class();

    Bean::FileStore::Tyrant->store_content(
        $key,
        $class,
        $self->{data},
    ) or Bean::FileStore::Tyrant->fail( "Unable to store file '$key' in bucket '$class'" );

   return 1;
}

*close = *CLOSE;

1;

#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id$

package Bean::FileStore::NativeTyrant;

=head1 NAME

Bean::FileStore::NativeTyrant - file abstraction layer around Tokyo Tyrant

=head1 OVERVIEW

see L<TokyoTyrant> and http://1978th.net/tokyotyrant/perldoc/

This uses the Tyrant binary protocol instead of Memcached.

Pros:
 - We can check if we managed to store the value in Tyrant
 - Binary protocol should be more efficient

Cons:
 - TokyoTyrant module not on CPAN

=head2 GOTCHAS

None

=head2 TERMINOLOGY

 key   = unique identifier for filename
 class = prefix to the filename

=cut

use strict;

use base 'Bean::FileStore::Base';

use Storable;
use Encode;
use TokyoTyrant;
use IO::WrapTie;

use Bean::FileStore::NativeTyrant::NewFile;

my $MAX_RETRIES = 2;
my %TYRANT;
sub C {
    my $class = shift;
    return $TYRANT{$class} ||= $class->tyrant_connect();
}

sub C_if_connected {
    my $class = shift;
    return $TYRANT{$class};
}

sub tyrant_disconnect {
    my $class = shift;

    my $c = delete $TYRANT{$class};
    if ( $c ) {
        return $c->close();
    }

    return undef;
}


sub master {
    return '46.254.116.35:1978';
}

sub backup {
    return '46.254.116.36:1978';
}

sub mem_key {
    my $key = shift;
    my $class = shift;

    return $class . '::' . $key;
}

sub tyrant_connect {
    my $pkg = shift;
    my $tyrant = TokyoTyrant::RDB->new();

    my $server = $pkg->master();
    my $timeout = $pkg->timeout();

    my ($master_ip, $master_port) = split_tyrant_port( $pkg->master() );

    if ( !$tyrant->open( $master_ip, $master_port, $timeout ) ) {
        warn qq{Unable to connect to "$master_ip", port "$master_port"};
        
        my ($backup_ip, $backup_port) = split_tyrant_port( $pkg->backup() );
        if ( !$tyrant->open( $backup_ip, $backup_port, $timeout ) ) {
            warn qq{Unable to connect to "$backup_ip", port "$backup_port"};
            return $pkg->fail( qq{Unable to connect to Tyrant} );
        }
    }

    return $tyrant;
}

sub timeout {
    return 2; # in seconds
}

sub split_tyrant_port {
    my $server = shift;
    my ($server_ip, $port) = split /:/, $server;
    if ( !$port ) {
        my $default_tyrant_port = 1978;
        $port = $default_tyrant_port;
    }
    return ($server_ip, $port);
}

sub new_file {
    shift;
    my $key = shift;
    my $class = shift;

    return IO::WrapTie::wraptie('Bean::FileStore::NativeTyrant::NewFile',
        key   => $key,
        class => $class,
    );
}

sub get_file_data {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $options_ref = $pkg->_options_or_cache(@_);

    return $pkg->_get_key( $key, $class );
}

sub store_content {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $content = shift;
    my $options_ref = $pkg->_options_or_cache(@_);

    my %headers;
    if ( $options_ref->{content_type} ) {
        $headers{content_type} = delete $options_ref->{content_type};
    }
    if ( $options_ref->{filename} ) {
        my $filename = delete $options_ref->{filename};
        $headers{content_disposition} = qq(attachment;filename=$filename);

        if ( !$headers{content_type} ) {
            $headers{content_type} = $pkg->identify_mime_type( $filename );
        }
    }

    if ( %headers ) {
        $headers{content_type} ||= delete $options_ref->{fallback_content_type};

        require HTTP::Headers;
        my $h = HTTP::Headers->new( %headers );

        my $eol = "\015";
        my $header = "X-FileStore-Header: Inline\n".$h->as_string();
        $header =~ s/\n?\z/\n\n/;

        if ( Encode::is_utf8( $content ) == Encode::is_utf8( $header ) ) {
            $content = $header . $content;
        }
        elsif ( Encode::is_utf8( $content ) ) {
            $content = $header . Encode::encode_utf8( $content );
        }
        else {
            $content = Encode::encode_utf8( $header ) . $content;
        }
    }


    if ( Encode::is_utf8( $content ) ) {
        # encode from characters to octets
        # otherwise syswrite will croak
        $content = Encode::encode_utf8( $content );
    }

    return $pkg->_set_key( $key, $class => $content );
}

sub list_keys {
    die "list_keys() not supported\n";
}

sub error {
}

sub create_class {
    my $pkg = shift;
    my $class = shift;

    warn "create_class: null op\n";
    return 1;
}

sub list_classes {
    die "list_classes() not supported\n";
}

sub serve_file {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $options_ref = $pkg->_options_or_cache(@_);

    my $data = $pkg->_get_key( $key, $class );

    if ( defined($data) ) {
        print $data;
        return 1;
    }

    return 0;
}

sub file_exists {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $options_ref = $pkg->_options_or_cache(@_);

    my $mem_key = mem_key( $key, $class );
    my $size = $pkg->tyrant_call( 'vsiz', $mem_key );
    if ( $size > 0 ) {
        return 1;
    }

    return 0;
}

sub delete_file {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $options_ref = $pkg->_options_or_cache(@_);

    my $mem_key = mem_key( $key, $class );
    my $response = $pkg->tyrant_call('out', $mem_key );

    if ( defined( $response ) ) {
        return $response;
    }
   
    $pkg->fail("Unable to delete '$key' in '$class'" );
}

sub _get_key {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;

    my $mem_key = mem_key( $key, $class );
    my $data_ref = $pkg->tyrant_call('get', $mem_key );

    if ( defined( $data_ref ) ) {
        return $data_ref;
    }

    $pkg->fail("Unable to retrieve '$key' in '$class'" );
}

sub _set_key {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $content = shift;

    my $mem_key = mem_key( $key, $class );
    $pkg->tyrant_call( 'put', $mem_key, $content )
        or $pkg->fail("Unable to store '$key' in '$class'" );
}

# tyrant_call()
#      Calls a method on Tyrant
#      Will reconnect if it thinks there is a problem
#
# Parameters:
#      $class->tyrant_call( 'tyrant_method', @arguments );
#
# Returns:
#      Success: returns response
#      Failure: returns undef
sub tyrant_call {
    my $pkg = shift;
    my $method = shift;
    my @arguments = (@_);

    if ( !$method ) {
        die "Missing method: $method";
    }

    my $retries_remaining = $MAX_RETRIES;

    RETRY:
    while ( $retries_remaining ) {
        my $c = $pkg->C();

        my $response = $c->$method(@arguments);

        # put returns true or false
        # get returns value or undefined
        my $ok = $method eq 'get' ? defined( $response ) : $response;

        if ( $ok ) {
            return $response;
        }

        # ESEND is an error in sending
        if ( $c->ecode() == $c->ESEND() || $c->ecode() == $c->EINVALID() || $c->ecode() == $c->EMISC() ) {
            # our tyrant handle is probably stale
            # if we reconnect and retry it will likely work again
            $pkg->tyrant_disconnect();

            $retries_remaining--;

            next RETRY;
        }

        # if we get this far, retrying the action is unlikely to help
        # so we won't.
        # usual suspects are host not found or connection refused
        return undef;
    }

    return undef;
}

{   no warnings "redefine";
    sub error {
        my $pkg = shift;
        my $C = $pkg->C_if_connected();

        return $C ? $C->errmsg() : '';
    }
}

1;

#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id$

package Bean::FileStore::NewFile;
# Virtually a copy and paste of MogileFS::NewHTTPFile
# NOTE: Meant to be used within IO::WrapTie...
# 

use strict;

sub CLOSE {
    my $self = shift;

    my $key = $self->key();
    my $class = $self->class();

    die "Subclass me!";

   return 1;
}

*close = *CLOSE;

# the rest should be generic enough
sub class { _getset(shift, 'class', @_); }
sub key   { _getset(shift, 'key',   @_); }

sub TIEHANDLE {
    my $class = shift;
    my $args_ref = @_ == 1 ? shift : { @_ };

    my $self = bless {
        data  => delete $args_ref->{data},
        pos   => 0,
        class => delete $args_ref->{class},
        key   => delete $args_ref->{key},
    }, $class;

    _fail("class is mandatory") if ( !$self->{class} );
    _fail("key is mandatory") if ( !$self->{key} );
    
    $self->{length} = length( $self->{data} );

    return $self;
}

*new = *TIEHANDLE;

sub PRINT {
    my $self = shift;

    my $data = shift;
    my $newlen = length $data;

    $self->{pos} += $newlen;

    $self->{data} .= $data;
    $self->{length} += $newlen;
}

*print = *PRINT;

sub TELL {
    # return our current pos
    reutrn $_[0]->{pos};
}

*tell = *TELL;

sub SEEK {
    # simply set pos...
    _fail("seek past end of file") if $_[1] > $_[0]->{length};
    $_[0]->{pos} = $_[1];
}
*seek = *SEEK;

sub EOF {
    return ($_[0]->{pos} >= $_[0]->{length}) ? 1 : 0;
}
*eof = *EOF;

sub BINMODE {
    # no-op, we're always in binary mode
}
*binmode = *BINMODE;

sub READ {
    my $self = shift;
    my $count = $_[1] + 0;

    my $max = $self->{length} - $self->{pos};
    $max = $count if $count < $max;

    $_[0] = substr($self->{data}, $self->{pos}, $max);
    $self->{pos} += $max;

    return $max;
}
*read = *READ;

# class methods
sub _fail {
    require Carp;
    Carp::croak( __PACKAGE__ .": $_[0]");
}

sub _debug {
}

sub _getset {
    my $self = shift;
    my $what = shift;

    if (@_) {
        # note: we're a TIEHANDLE interface, so we're not QUITE like a
        # normal class... our parameters tend to come in via an arrayref
        my $val = shift;
        $val = shift(@$val) if ref $val eq 'ARRAY';
        return $self->{$what} = $val;
    }
    else {
        return $self->{$what};
    }
}

1;

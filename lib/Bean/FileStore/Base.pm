#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id: FileStore.pm 19507 2009-05-18 08:26:13Z andy $

package Bean::FileStore::Base;

use strict;

#use Bean::MyCache;
our $C;
use Storable;

=head1 NAME

Bean::FileStore::Base - file abstraction layer for the CV search

=head1 OVERVIEW

File store abstraction layer so we can store in Amazon S3, MogileFS or
the current flavour of the month.

Currently supports Amazon S3 and Mogile FS

=head2 CACHING

Most of these routines accept an optional $can_cache/$use_cache parameter.

If missing or 0, then we will go directly to the file store (and we will not cache it)

If 1, we may cache it before going to the file store

=head2 TERMINOLOGY

=over

=item *

B<key> - unique identifier for filename

=item *

B<class> - similar to a directory

=back

=head2 SUBCLASSING

Subclasses must provide the follow methods:

=over 4

=item *

new_file

=item *

get_file_data

=item *

store_content

=item *

list_keys

=item *

file_exists

=item *

delete_file

=back

=head2 CLASS METHODS

=head3 new_file

 my $fh = Bean::FileStore->new_file( $key, $class );
 print $fh "file contents\n";
 $fh->close() or die "Unable to store '$key': ". Bean::FileStore->error();

=cut

sub new_file {
    die 'Stub';
}

=head3 file_exists

 my $file_exists = Bean::FileStore->file_exists( $key, $class, [ $use_cache ] );

=cut

sub file_exists {
    die 'Stub';
}

=head3 get_file_data

 my $file_contents = Bean::FileStore->get_file_data( $key, $class, [ $use_cache ] );

=cut

sub get_file_data {
    die 'Stub';
}

=head3 store_content

 my $ok = Bean::FileStore->store_content( $key, $class, $content, [ $can_cache ] );

=cut

sub store_content {
    die 'Stub';
}

=head3 save_reference

 Bean::FileStore->save_reference( $key, $class, $datastructure_ref, [ $can_cache ] );

=cut

sub save_reference {
    my $pkg       = shift;
    my $key       = shift;
    my $class     = shift; # determines replication policy
    my $reference = shift;
    my $can_cache = shift;

    my $serialised_reference = Storable::nfreeze( $reference );
    if ( $can_cache ) {
        $pkg->store_in_cache( $key, $class, $serialised_reference );
    }

    return $pkg->store_content( $key, $class, $serialised_reference, $can_cache );
}

=head3 load_reference

 my $datastructure_ref = Bean::FileStore->load_reference( $key, $class, [ $use_cache ] );

=cut

sub load_reference {
    my $pkg   = shift;
    my $key   = shift;
    my $class = shift;
    my $use_cache = shift;

    my $content;
    if ( $use_cache ) {
        $content = $pkg->load_from_cache( $key, $class );
    }

    # fetch from backend storage if we don't have a cached version
    $content ||= $pkg->get_file_data( $key, $class, $use_cache );

    if ( $content ) {
        if ( ref( $content ) ) {
            # storable will barf
            require Data::Dumper;
            die "Not a scalar: " . Data::Dumper::Dumper( $content );
        }

        return Storable::thaw( $content );
    }

    _fail("Unable to read '$key': " . $pkg->error);
}

=head3 delete

 my $success = Bean::FileStore->delete( $key, $class );

** Don't call C<Bean::FileStore->delete_file()> directly
because it doesn't clear memcached **

=cut

sub delete {
    my $pkg   = shift;
    my $key   = shift;
    my $class = shift;

    $pkg->delete_from_cache(
        $key, $class,
    );

    return $pkg->delete_file(
        $key, $class,
    );
}

=head3 delete_file (PRIVATE)

 my $success = Bean::FileStore->delete_file( $key, $class )

** hint: use Bean::FileStore->delete instead **

=cut

sub delete_file {
    die 'Stub';
}

=head3 list_keys

 # kind of like doing `ls $key*`
 my $file_contents = Bean::FileStore->list_keys( $key, $classs, [ $use_cache ] );

=cut

sub list_keys {
    die 'Stub';
}

=head3 list_class

 # kind of like doing `cd $class; ls *`
 my $file_contents = Bean::FileStore->list_class( $classs, [ $use_cache ] );

=cut

sub list_class {
    my $pkg = shift;
    my $class = shift;
    my $use_cache = shift;

    return $pkg->list_keys( '', $class, $use_cache );
}

=head3 create_class

Bean::FileStore->create_class( $class );

=cut

sub create_class {
    die 'Stub';
}

=head3 serve_file

Bean::FileStore->serve_file( $key, $class, [ $use_cache ] );

=cut

sub serve_file {
    die 'Stub';
}

=head3 serve_urls

Bean::FileStore->serve_urls({ %options }, @urls);

Proxy the first working url from a list of urls. Good for serving
large files because we don't read the entire content into memory

=cut

sub serve_urls {
    my $class = shift;
    my $options_ref = ref($_[0]) ? shift : {};
    my @urls = @_;

    my $filename;
    my $timeout = exists $options_ref->{timeout} ? $options_ref->{timeout} : $class->default_timeout();

    URL:
    foreach my $url ( @urls ) {
        next URL unless $url;

        my $ua = LWP::UserAgent->new();
        $ua->timeout( $timeout );

        my $printed_header;
        my $res = $ua->get($url, ':content_cb' => sub {
            my $data = shift;
            my $res  = shift;
            my $ua   = shift;

            unless ( $printed_header ) {
                if ( $data =~ s/^X-FileStore-Header: Inline\n// ) {
                    # data includes the http headers so we don't need to do anything
                    # this is my hacky workaround for mogilefs
                }
                else {
                    my $content_type = $res->header('Content-Type');
                    my $newline = "\n"; #\015";
                    print 'Content-Type: ' .  $content_type . $newline;
                    if ( $filename ) {
                        print 'Content-Disposition:attachment;filename="'.$filename.'"'.$newline;
                    }
                    print $newline;
                }

                $printed_header++;
            }

            print $data;
        });

        if ( $res->is_success ) {
            return 1;
        }
    }

    return 0;
}

sub default_timeout {
    return 10;
}

=head3 store_in_cache

Bean::FileStore->store_in_cache( $key, $class, $serialised_content );

Store a string of characters in memcached

Called automatically by C<store_reference()> if needed

=cut
sub store_in_cache {
    return unless $C;

    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $content = shift;

    my $cache_key = $pkg->cache_key( $key, $class );

    $C->set( $cache_key, $content );
}

=head3 load_from_cache

Bean::FileStore->load_from_cache( $key, $class );

Fetches a string of characters in memcached (you need to deserialise it)

Called automatically by C<load_reference()> if needed

=cut
sub load_from_cache {
    return unless $C;

    my $pkg = shift;
    my $key = shift;
    my $class = shift;

    my $cache_key = $pkg->cache_key( $key, $class );

    return $C->get( $cache_key );
}

sub delete_from_cache {
    return unless $C;

    my $pkg = shift;
    my $key = shift;
    my $class = shift;

    my $cache_key = $pkg->cache_key( $key, $class );

    return $C->delete( $cache_key );
}

sub cache_key {
    my $pkg = shift;

    return join(':', 'ref', shift, shift );
}

sub error {
    die 'Stub';
}

sub fail {
    my $pkg = shift;
    my $err = shift;
    my $backend_error = $pkg->error();
    if ( $backend_error ) {
        $err .= " - $backend_error";
    }

    require Carp;
    Carp::croak( $pkg . ':' . $err );
}

####### PRIVATE ########
#
# input: undef,0,1 or hashref
# output: $hashref or { use_cache => 0,1,undef }
sub _options_or_cache {
    shift;
    return $_[0] if ref($_[0]);
    my $options_ref = {};
    if ( defined($_[0]) ) {
        $options_ref->{use_cache} = shift;
    }

    return $options_ref;
}

sub identify_mime_type {
    shift;
    my $filename = shift;

    require Stream::DetectFileType;
    return Stream::DetectFileType->guess_mime_type_from_filename( $filename );
}

=head1 TODO

rename methods (I haven't needed them yet)

=head1 SEE ALSO

L<Bean::FileStore::MogileFS>, L<Bean::FileStore::S3>

=cut

1;

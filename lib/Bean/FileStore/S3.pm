#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id$

package Bean::FileStore::S3;

=head1 NAME

Bean::FileStore::S3 - file abstraction layer around S3

=head1 OVERVIEW

Check the Net::Amazon::S3 docs for more details

=head2 GOTCHAS

You must create the bucket first!

 Bean::FileStore->create_class( $class );

The amazon bucket namespace is global (ie. you can only create a new bucket if somebody hasn't beat you to it)

This module transparently prefixes all buckets with $AWS_BUCKET_PREFIX. You don't have to worry about thisunless you are updating this module.

=head2 TERMINOLOGY

 key   = unique identifier for filename
 class = what Amazon calls a bucket

=cut

use strict;

use base 'Bean::FileStore::Base';

use Storable;
use Encode;
use IO::WrapTie;

use Bean::FileStore::S3::NewFile;

use Net::Amazon::S3;

sub AWS_ACCESS_KEY_ID {
    return '0D4C6D19XJAR7MZ9Q482';
}
sub AWS_SECRET_ACCESS_KEY {
    return '/Svf4sV+uhR5m2erONdtYnqIj4Z/rQd41VQp40mb';
}
sub AWS_RETRY {
    return 1;
}

=head2 AWS_HOST

The Host to use to access the bucket. Buckets SHOULD now their
region and you should set the host according to

L<http://docs.aws.amazon.com/general/latest/gr/rande.html#s3_region>

=cut

sub AWS_HOST{
    return 's3.amazon.com';
}

my $HAS_TEXT_UNACCENT;

BEGIN {
    # We store the CV filename in the S3 meta information
    # S3 meta info can only contain Ascii characters
    # so we remove the other characters using Text::Unaccent from CPAN
    # If Text::Unaccent is unavailable we use Bean::Hacks
    # see clean_filename() for more info
    $HAS_TEXT_UNACCENT = eval "use Text::Unaccent; 1";
};

# S3 uses REST (so buckets = url, buckets can't contain characters like / and ?)
# Nor can this prefix
sub AWS_BUCKET_PREFIX {
    return 'ADC_CVS-';
}

my $S3;
sub S3 {
    if ( !$S3 ) {
        my $class = shift || __PACKAGE__;
        $S3 = $class->new_s3();
    }

    return $S3;
}

sub new_s3 {
    my $class = shift;
    return Net::Amazon::S3->new({
        aws_access_key_id     => $class->AWS_ACCESS_KEY_ID(),
        aws_secret_access_key => $class->AWS_SECRET_ACCESS_KEY(),
        retry                 => $class->AWS_RETRY(),
        host                  => $class->AWS_HOST(),
    });
}

sub _to_bucket {
    my $pkg = shift;
    my $aws_class = shift or return;

    return $pkg->AWS_BUCKET_PREFIX() . $aws_class;
}

sub _to_class {
    my $pkg = shift;
    my $bucket = shift or return;

    my $aws_bucket_prefix = $pkg->AWS_BUCKET_PREFIX();
    $bucket =~ s/^\Q$aws_bucket_prefix\E//;

    return $bucket;
}

sub new_file {
    my $pkg = shift;
    my $key = shift;
    my $aws_class = shift;

    my $bucket = $pkg->_to_bucket($aws_class);
    return IO::WrapTie::wraptie('Bean::FileStore::S3::NewFile',
        key      => $key,
        class    => $bucket,
        s3_class => $pkg,
    );
}

sub get_file_data {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my $data_ref = $pkg->s3_get_key( $key, $class );

    if (defined( $data_ref ) ) {
        return $data_ref->{value};
    }

    $pkg->fail("Unable to retrieve '$key' in '$class'");
}

sub store_content {
    my $pkg       = shift;
    my $key       = shift;
    my $class     = shift; # determines replication policy
    my $content   = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my %headers;
    if ( $options_ref->{content_type} ) {
        $headers{content_type} = delete $options_ref->{content_type};
    }
    if ( $options_ref->{filename} ) {
        my $filename = delete $options_ref->{filename}; 

        # Amazon don't like non-ASCII characters in the headers
        # If you include non-ASCII characters in the headers,
        # you will get a "SignatureDoesNotMatch" error
        $filename = clean_filename( $filename );

        # check if there is a file extension (very crude but good enough for now)
        my $has_fileext = $filename =~ m/\./;

        # set the content type from the filename if there isn't one
        if ( $has_fileext && !$headers{content_type} ) {
            $headers{content_type} = $pkg->identify_mime_type( $filename );
        }

        # use the fallback content type if we still don't have one
        $headers{content_type} ||= delete $options_ref->{fallback_content_type};

        # test case: stream/backend/zz-s3-add-file-extension.t
        # be nice and try to add an extension if we know the mime type
        # and it is missing the extension
        # Indeed do not add a file extension to their PDF CVs
        if ( !$has_fileext && $headers{content_type} ) {
            require Stream::DetectFileType;
            my %mime_types = Stream::DetectFileType->known_mime_types();
            MIME_TYPE:
            while ( my ($extension, $mime_type) = each %mime_types ) {
                if ( $headers{content_type} =~ m/^\Q$mime_type/ ) {
                    $filename .= ".$extension";
                    last MIME_TYPE;
                }
            }
        }

        # http://docs.amazonwebservices.com/AmazonS3/latest/index.html?UsingMetadata.html
        # prefix custom metadeta with 'x-amz-meta-'
        $headers{'x-amz-meta-filename'} = $filename;
    }

    if ( %headers ) {
        $headers{'x-amz-meta-content-type'} = $headers{content_type};
    }

    # test case: stream/backend/zz-s3-pdf-content-type-charset.t
    # workaround for bug https://rt.cpan.org/Public/Bug/Display.html?id=76602
    # that is triggered when S3 do not return a content-type
    # remove the charset from file types that should not have one specified
    # as they are binary data
    # this is required to downloading CVs from Indeed
    # as they label their PDFs as charset=UTF8 which is nonsense
    if ( $headers{content_type} =~ m{application/pdf} && $headers{content_type} =~ m/charset/ ) {
        my (@ct) = split(/;\s*/, $headers{content_type}, 2);
        my $stripped_ct = lc($ct[0]);
        $stripped_ct =~ s/\s+//g;
        $headers{content_type} = $stripped_ct;
    }

    if ( Encode::is_utf8( $content ) ) {
        # encode from characters to octets
        # otherwise syswrite will croak
        $content = Encode::encode_utf8( $content );
    }

    $pkg->s3_add_key( $key, $class, $content, \%headers )
        or $pkg->fail("Unable to store '$key' in '$class'");

    return 1;
}

my $_used_bean_hacks_warning = 0;
sub clean_filename {
    my $filename = shift;

    # We need to remove any non-ascii characters from the filename
    # First try to remove accents using Text::Unaccent
    # (if Bean::Hacks isn't good enough, Text::Unaccent from CPAN is very good)

    # small complication - _MockResponse objects may store the filename as UTF8 characters
    #                      HTTP::Response objects always store the filename as bytes
    # Bean::Hacks only works with UTF8 characters so we need to convert the bytes to characters
    # before calling it
    # test case for this is in ~/tests/stream/backend/zz-s3-utf8-filename.t

    # Text::Unaccent only removes accents from text
    # We have to strip things like emdashes and quotes manually :(
    # Bean::Hacks expects characters not bytes
    my ($filename_chars) = Encode::is_utf8( $filename ) ? $filename : Encode::decode_utf8($filename);
    $filename_chars =~ s/[\xA0\x{2002}\x{2003}\x{2009}]/ /g;   # no breaking space
    $filename_chars =~ s/[\x{2018}\x{2019}\xB4]/'/g; # single quotes
    $filename_chars =~ s/[\x{201C}\x{201D}]/"/g; # double quotes
    $filename_chars =~ s/[\x{2013}\x{2014}]/-/g; # e[nm]dashes

    # prefer Text::Unaccent because it is quick and picks up more characters
    if ( $HAS_TEXT_UNACCENT ) {
        # Text::Unaccent needs bytes not characters
        my $filename_bytes = Encode::encode_utf8($filename_chars);

        my $new_filename = Text::Unaccent::unac_string( 'UTF-8', $filename_bytes );
        if ( $new_filename ) {
            # we removed the funny characters
            return $new_filename;
        }

        # we couldn't remove the characters
        return $filename;
    }

    # fallback to Bean::Hacks which is slow and not exhaustive
    if ( !$_used_bean_hacks_warning ) {
        warn "Text::Unaccent is unavailable so falling back to Bean::Hacks. Consider installing Text::Unaccent because it is faster and handles more funky characters";
        $_used_bean_hacks_warning++;
    }
    require Bean::Hacks;

    # Bean::Hacks expects characters not bytes
    return Bean::Hacks::strip_accented_characters( $filename_chars );
}

sub list_keys {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my $s3 = $pkg->S3();

    if ( $class ) {
        my $bucket = $pkg->_to_bucket($class);
        return map { $_->{key} }
            $pkg->s3_return_key($s3->list_bucket({ bucket => $bucket, prefix => $key }), 'keys');
    }

    my @keys_found;
    foreach my $bucket ( $pkg->s3_return_key($s3->buckets(), 'buckets') ) {
        my @keys_in_bucket = $pkg->s3_return_key( $bucket->list({ prefix => $key }), 'keys' );

        push @keys_found, @keys_in_bucket if scalar(@keys_in_bucket);
    }

    return map { $_->{key} } @keys_found;
}

sub s3_return_key {
    my $pkg = shift;
    my $response = shift or return;
    my $key      = shift or $pkg->fail("Missing key");

    my $reftype = ref( $response->{$key} );
    if ( $reftype eq 'ARRAY' ) {
        return @{ $response->{$key} };
    }
    if ( $reftype eq 'HASH' ) {
        return %{ $response->{$key} };
    }
    return $response->{$key};
}

sub error {
    if ( $S3 ) {
        my (@parts) = grep { $_ } ($S3->err, $S3->errstr);
        if ( @parts ) {
            return join(': ', @parts);
        }
    }

    return undef;
}

sub create_class {
    my $pkg = shift;
    my $class = shift;

    my $s3 = $pkg->S3();

    my $bucket = $pkg->_to_bucket( $class );
    $s3->add_bucket({ bucket => $bucket })
        or $pkg->fail("Unable to create bucket '$class'");
}

sub list_classes {
    my $pkg = shift;
    my $s3 = $pkg->S3();

    my @classes = $pkg->s3_return_key($s3->buckets(), 'buckets' );

    if ( @_ ) {
        return map { $_->bucket() } @classes;
    }

    return map { $pkg->_to_class($_->bucket()) } @classes;
}

sub file_details {
    my $pkg = shift;
    my $key      = shift;
    my $class    = shift;

    my $response = $pkg->s3_get_key( $key, $class )
            or $pkg->fail("Unable to get key '$key'");

    my $filename = $response->{filename} || $response->{'x-amz-meta-filename'} || $key;
    my $content_type = $response->{'x-amz-meta-content-type'} || $response->{content_type} || 'text/plain';
    my $content = $response->{value};
    
    return ($content_type, $content, $filename);
}

sub serve_file {
    my $pkg = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my $response = $pkg->s3_get_key( $key, $class )
        or $pkg->fail("Unable to get key '$key'");

    my $filename = $response->{filename} || $response->{'x-amz-meta-filename'} || $key;
    my $content_type = $response->{'x-amz-meta-content-type'} || $response->{content_type} || 'text/plain';
    
    if ( $content_type eq 'text/html' ) {
        if ( $response->{value} !~ m/charset/ && $response->{value} =~ m/\xA0/ ) {
            $content_type = 'text/html; charset=UTF-8';
        }
    }

    #my $newline = "\012\015";
    my $newline = "\n";
    print 'Content-Type: ' .  $content_type . $newline;
    print 'Content-Disposition:attachment;filename="'.$filename.'";'.$newline;

    print $newline;

    my $value = $response->{value};

    # monster CVs are UTF8. when they are saved to the desktop that information is lost
    if ( $content_type =~ m/html/ && $value !~ m/<meta http-equiv="content-type"/i ) {
        $value =~ s{(</title>)}{$1<meta http-equiv="content-type"content="$content_type">}i;
    }
            
    print $value;

    return 1;
}

sub file_exists {
    my $pkg      = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);

    my $use_cache = $options_ref->{use_cache};

    my $response = $pkg->s3_head_key( $key, $class )
        or return 0;

    return 1;
}

sub delete_file {
    my $pkg      = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);

    my $use_cache = $options_ref->{use_cache};

    $pkg->s3_delete_key( $key, $class )
        or return 0;

    return 1;
}

# Low level interactions with S3
sub s3_head_key {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;

    my $bucket = $pkg->s3_get_bucket( $class );
    return $bucket->head_key( $key )
}

sub s3_get_key {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;

    my $bucket = $pkg->s3_get_bucket( $class );
    return $bucket->get_key( $key );
}

sub s3_add_key {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $content = shift;
    my $headers_ref = shift;

    my $bucket = $pkg->s3_get_bucket( $class );
    return $bucket->add_key( $key, $content, $headers_ref );
}

sub s3_delete_key {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;

    my $bucket = $pkg->s3_get_bucket( $class );
    return $bucket->delete_key( $key )
}

sub s3_get_bucket {
    my $pkg = shift;
    my $class = shift;

    my $s3 = $pkg->S3() or return;
    my $bucket = $pkg->_to_bucket( $class );
    return $s3->bucket( $bucket );
}

1;

#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id$

package Bean::FileStore::Tyrant;

=head1 NAME

Bean::FileStore::Tyrant - file abstraction layer around Tokyo Tyrant

=head1 OVERVIEW

see L<Cache::Memcached> and http://tokyocabinet.sourceforge.net/tyrantdoc/

=head2 GOTCHAS

None

=head2 TERMINOLOGY

 key   = unique identifier for filename
 class = prefix to the filename

=cut

use strict;

use base 'Bean::FileStore::Memcached';

use Storable;
use Encode;
use Cache::Memcached;
use IO::WrapTie;

use Bean::FileStore::Tyrant::NewFile;

my %TYRANT;
sub C {
    my $class = shift;
    return $TYRANT{$class} ||= $class->tyrant_connect();
}

sub master {
    return ['46.254.116.35:1978'];
}

sub tyrant_connect {
    my $class = shift;
    my $tyrant = Cache::Memcached->new();

    my $servers_ref = $class->master();

    # master
    my @servers = @$servers_ref;
    $tyrant->set_servers($servers_ref);

    # backup
    #$tyrant->set_servers(['213.165.2.164:1978']);

    return $tyrant;
}

sub new_file {
    shift;
    my $key = shift;
    my $class = shift;

    return IO::WrapTie::wraptie('Bean::FileStore::Tyrant::NewFile',
        key   => $key,
        class => $class,
    );
}

1;

#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id$

package Bean::FileStore::S3::NewFile;
# Virtually a copy and paste of MogileFS::NewHTTPFile
# NOTE: Meant to be used within IO::WrapTie...
# 

use strict;

sub class    { _getset(shift, 'class', @_); }
sub key      { _getset(shift, 'key',   @_); }
sub s3_class { _getset(shift, 's3_class',   @_); }

sub TIEHANDLE {
    my $class = shift;
    my $args_ref = @_ == 1 ? shift : { @_ };

    my $s3_class = delete $args_ref->{s3_class};
    my $self = bless {
        data     => delete $args_ref->{data},
        pos      => 0,
        s3_class => $s3_class || 'Bean::FileStore::S3',
        class    => delete $args_ref->{class},
        key      => delete $args_ref->{key},
    }, $class;

    _fail("class is mandatory") if ( !$self->{class} );
    _fail("key is mandatory") if ( !$self->{key} );
    
    $self->{length} = length( $self->{data} );

    return $self;
}

*new = *TIEHANDLE;

sub PRINT {
    my $self = shift;

    if( $self->{length} > $self->MAX_BUFFER_LENGTH() ) {
        warn "Exceeded buffer limit " . $self->MAX_BUFFER_LENGTH() 
            . "... dumping to temporary file";

        $self->CLOSE();
        my $base_key = $self->{base_key} ||= $self->{key};
        $self->{key} = $base_key . '-' . ++$self->{key_counter};
        $self->_flush_buffer(); 
    }


    my $data = shift;
    my $newlen = length $data;

    $self->{pos} += $newlen;

    $self->{data} .= $data;
    $self->{length} += $newlen;
}

*print = *PRINT;

sub MAX_BUFFER_LENGTH {
    return $ENV{'S3::NewFile::MAX_BUFFER_LENGTH'} 
        || 10 * 1024 * 1024; #5Mb
}

sub _flush_buffer {
    my $self = shift;

    #then flush the data buffer
    $self->{data} = undef;
    $self->{length} = 0;
    $self->{pos} = 0;

    return;
}

sub CLOSE {
    my $self = shift;
    
    #Last entry on multi log files might have nothing
    #so we can ignore
    return if !$self->{data} && $self->{key_counter};

    my $key = $self->key();
    my $class = $self->class();

    my $s3_class = $self->s3_class();

    my $s3 = $s3_class->S3();

    my $bucket = $s3->bucket($class)
        or $s3_class->fail( "Unable to fetch bucket '$class'" );

    $bucket->add_key( $key => $self->{data} )
        or $s3_class->fail( "Unable to store file '$key' in bucket '$class'" );

    return 1;
}

*close = *CLOSE;

sub TELL {
    # return our current pos
    return $_[0]->{pos};
}

*tell = *TELL;

sub SEEK {
    # simply set pos...
    _fail("seek past end of file") if $_[1] > $_[0]->{length};
    $_[0]->{pos} = $_[1];
}
*seek = *SEEK;

sub EOF {
    return ($_[0]->{pos} >= $_[0]->{length}) ? 1 : 0;
}
*eof = *EOF;

sub BINMODE {
    # no-op, we're always in binary mode
}
*binmode = *BINMODE;

sub READ {
    my $self = shift;
    my $count = $_[1] + 0;

    my $max = $self->{length} - $self->{pos};
    $max = $count if $count < $max;

    $_[0] = substr($self->{data}, $self->{pos}, $max);
    $self->{pos} += $max;

    return $max;
}
*read = *READ;

# class methods
sub _fail {
    require Carp;
    Carp::croak( __PACKAGE__ .": $_[0]");
}

sub _debug {
}

sub _getset {
    my $self = shift;
    my $what = shift;

    if (@_) {
        # note: we're a TIEHANDLE interface, so we're not QUITE like a
        # normal class... our parameters tend to come in via an arrayref
        my $val = shift;
        $val = shift(@$val) if ref $val eq 'ARRAY';
        return $self->{$what} = $val;
    }
    else {
        return $self->{$what};
    }
}

1;

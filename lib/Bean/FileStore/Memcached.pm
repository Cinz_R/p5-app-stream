#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id$

package Bean::FileStore::Memcached;

=head1 NAME

Bean::FileStore::S3 - file abstraction layer around S3

=head1 OVERVIEW

Check the Net::Amazon::S3 docs for more details

=head2 GOTCHAS

You must create the bucket first!

 Bean::FileStore->create_class( $class );

The amazon bucket namespace is global (ie. you can only create a new bucket if somebody hasn't beat you to it)

This module transparently prefixes all buckets with $AWS_BUCKET_PREFIX. You don't have to worry about thisunless you are updating this module.

=head2 TERMINOLOGY

 key   = unique identifier for filename
 class = what Amazon calls a bucket

=cut

use strict;

use base 'Bean::FileStore::Base';

use Storable;
use Encode;
use Bean::MyCache;
use IO::WrapTie;

use Bean::FileStore::Memcached::NewFile;

sub C {
    return $C;
}

sub mem_key {
    my $key   = shift;
    my $class = shift;

    return $class . '::' . $key;
}

sub new_file {
    shift;
    my $key = shift;
    my $class = shift;

    return IO::WrapTie::wraptie('Bean::FileStore::Memcached::NewFile',
        key   => $key,
        class => $class,
    );
}

sub get_file_data {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my $c = $pkg->C();

    my $mem_key = mem_key( $key, $class );

    my $data_ref = $c->get( $mem_key );

    if (defined( $data_ref ) ) {
        return $data_ref;
    }

    $pkg->fail("Unable to retrieve '$key' in '$class'");
}

sub store_content {
    my $pkg       = shift;
    my $key       = shift;
    my $class     = shift; # determines replication policy
    my $content   = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my %headers;
    if ( $options_ref->{content_type} ) {
        $headers{content_type} = delete $options_ref->{content_type};
    }
    if ( $options_ref->{filename} ) {
        my $filename = delete $options_ref->{filename}; 
        $headers{content_disposition} = qq(attachment;filename=$filename);

        if ( !$headers{content_type} ) {
            $headers{content_type} = $pkg->identify_mime_type( $filename );
        }
    }

    if ( %headers ) {
        $headers{content_type} ||= delete $options_ref->{fallback_content_type};

        require HTTP::Headers;
        my $h = HTTP::Headers->new( %headers );

        my $eol = "\015";
        my $header = "X-FileStore-Header: Inline\n".$h->as_string();
        $header =~ s/\n?\z/\n\n/;

        if ( Encode::is_utf8( $content ) == Encode::is_utf8( $header ) ) {
            $content = $header . $content;
        }
        elsif ( Encode::is_utf8( $content ) ) {
            $content = $header . Encode::encode_utf8( $content );
        }
        else {
            $content = Encode::encode_utf8( $header ) . $content;
        }
    }

    if ( Encode::is_utf8( $content ) ) {
        # encode from characters to octets
        # otherwise syswrite will croak
        $content = Encode::encode_utf8( $content );
    }

    return $pkg->_set_key( $key, $class => $content );
}

sub _set_key {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $content = shift;

    my $c = $pkg->C();
    my $mem_key = mem_key( $key, $class );

    $c->set( $mem_key => $content )
        or $pkg->fail("Unable to store '$key' in '$class'");

    return 1;
}

sub list_keys {
    my $pkg = shift;
    my $key = shift;
    my $class = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    # not available
    die "list_keys() not supported\n";
}

sub error {
#    if ( $S3 ) {
#        return $S3->err . ": " . $S3->errstr;
#    }
#
    return undef;
}

sub create_class {
    my $pkg = shift;
    my $class = shift;

    warn "create_class: null op\n";
    return 1;
}

sub list_classes {
    my $pkg = shift;

    die "list_classes() not supported\n";
}

sub serve_file {
    my $pkg = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);
    my $use_cache = $options_ref->{use_cache};

    my $c = $pkg->C() or return;

    my $mem_key = mem_key( $key, $class );

    my $data = $c->get( $mem_key );

    print $data;

    if ( defined($data) ) {
        return 1;
    }

    return 0;
}

sub file_exists {
    my $pkg      = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);

    my $use_cache = $options_ref->{use_cache};

    my $c = $pkg->C() or return;
    my $mem_key = mem_key( $key, $class );

    # TODO
    my $data_ref = $c->get( $mem_key );

    if (defined( $data_ref ) ) {
        return 1;
    }

    return 0;
}

sub delete_file {
    my $pkg      = shift;
    my $key      = shift;
    my $class    = shift;
    my $options_ref = $pkg->_options_or_cache(@_);

    my $use_cache = $options_ref->{use_cache};

    my $c = $pkg->C() or return;
    my $mem_key = mem_key( $key, $class );

    $c->delete_key( $mem_key )
        or return 0;

    return 1;
}

1;

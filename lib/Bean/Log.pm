#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: Log.pm 85107 2012-10-15 15:44:34Z robert.marlton $

package Bean::Log;

use strict;

use utf8;

use base qw(Class::Accessor::Fast);

use Encode;

use Stream::SharedUtils
    qw(current_user current_devuser remote_addr hostname);

BEGIN {
    my $HAS_HIRES_TIME = eval { require Time::HiRes; 1 };

    if ( $HAS_HIRES_TIME ) {
        *_time = \&Time::HiRes::time;
    }
    else {
        *_time = \&CORE::time;
    }

    __PACKAGE__->mk_accessors( qw( id cancel ) );
};

sub new {
    my ($class, $args_ref) = @_;

    my $self = $class->SUPER::new( $args_ref );

    $self->say_hello();

    $self->{log_started} = _time();

    return $self;
}

# returns a unique key to let us find the log again later
sub key {
    return $_[0]->id();
}

sub say_hello {
    my $self = shift;

    my ($remote_addr) = remote_addr();
    my ($username)    = current_user();
    my ($devuser)     = current_devuser();
    my ($hostname)    = hostname();

    $self->info(
        'Log created at %s by %s (username: %s, devuser: %s). This server is %s',
        scalar(localtime()),
        $remote_addr,
        $username,
        $devuser,
        $hostname,
    );
}

sub mark {
    my $self = shift;
    return $self->log('MARK', @_);
}

sub DESTROY {
    my $self = shift;

    return $self->finish_log(); 
}

sub finish_log {
    my $self = shift;

    $self->record_time_of_last_mark();
    $self->record_time_since_start();
}

sub record_time_of_last_mark {
    my $self = shift;
    my $time_of_last_mark = delete $self->{_last_mark_time};
    my $time_of_this_mark = _time();
    if ( $time_of_last_mark ) {
        $self->debug('Last mark took %.5f seconds', $time_of_this_mark - $time_of_last_mark );
    }

    return $time_of_this_mark;
}

sub record_time_since_start {
    my $self = shift;

    my $time_started = $self->{log_started}
        or return;
    my $time_now     = _time();

    $self->debug('From start to finish took %.5f seconds', $time_now - $time_started );
}

sub debug {
    shift->log('DEBUG', @_);
}

sub info {
    shift->log('INFO', @_);
}

sub warn {
    shift->log('WARN', @_);
}

sub error {
    shift->log('ERROR', @_);
}

sub request {
    my $self = shift;
    my $request = shift;
    $self->log('REQUEST', $request->as_string());
}

sub response {
    my $self = shift;
    my $response = shift;

    if ( $response->header('Content-Encoding') =~ m/\bgzip\b/ ) {
        $self->log('RESPONSE', $response->headers_as_string() . "\n"
                   . $response->decoded_content( raise_error => 0 ) );
    }
    else {
        $self->log('RESPONSE', $response->as_string() );
    }
}

sub _quote {
    # pinched from DBI.pm
    my ($str) = shift;
    return 'NULL' unless defined $str;
    $str =~ s/'/''/g; # ISO SQL2
    return qq('$str');
}

sub debug_sql {
    my $self = shift;
    my $sql  = shift;
    my (@binds) = @_;

    if ( ref($sql) ) {
        $sql = $sql->{Statement};
    }

    foreach my $bind ( map { _quote( $_ ) } @binds ) {
        $sql =~ s/\?/$bind/;
    }

    $self->log('SQL', $sql);
}

# Handy shortcut around calling request(), response()
sub transaction {
    my $self = shift;
    my $final_response = shift;

    if ( !$final_response ) {
        return $self->warn('Tried to log a request/response but response is empty');
    }

    my @previous_responses = ( $final_response );

    my $this_response = $final_response;

    RESPONSE:
    while ( my $previous_response = $this_response->previous ) {
        if ( $previous_response ) {
            unshift @previous_responses, $previous_response;
            $this_response = $previous_response;
        }

        last RESPONSE if !$this_response;
    }

    foreach my $response ( @previous_responses ) {
        $self->request( $response->request() );
        $self->response( $response );
    }
}

sub log {
    my $self = shift;
    my $msg  = $self->_prepare_msg( @_ );

    if ( $_[0] eq 'MARK' ) {
        $self->{_last_mark_time} = $self->record_time_of_last_mark();
    }

    $self->log_msg( $_[0], Encode::encode( 'utf8', $msg ) );
}

sub log_msg {
    my $self  = shift;
    my $level = shift;
    my $msg   = shift;

    $self->_print( $msg );
}

# need to watch this
# because we are using sprintf, you may need to escape %
sub _prepare_msg {
    my $self = shift;

    my $level = shift;
    my $msg   = @_ == 1 ? shift : sprintf(shift, map { ref($_) eq 'ARRAY' ? _array_to_string($_) : $_ } @_);

    return sprintf( "%s %s %s\n", $level, _time(), $msg );

}

sub _print {
    shift;
    print @_;
}

sub _array_to_string {
    my $ref = shift;
    return $ref->[0] if @$ref == 1;
    return '['.join(',',@$ref).']';
}

1;

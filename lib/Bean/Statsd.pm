package Bean::Statsd;


die "DO NOT USE THAT FOR NOW. Note that We are lucky it uses UDP";

use strict;

# a perl clone of https://github.com/etsy/statsd/blob/master/php-example.php

=head1

StatsD - Sends statistics to the stats daemon over UDP

=cut

use IO::Socket;
use Time::HiRes;

=head2 timing

Log timing information

 @param string $stats The metric to in log timing info for.
 @param float $time The elapsed time (ms) to log
 @param float|1 $sampleRate the rate (0-1) for sampling.

=cut

sub timing {
    my ($class, $stat, $time, $sample_rate) = @_;
    if ( ref( $time ) eq 'ARRAY' ) {
        # assume $time is the time from Time::HiRes::gettimeofday
        $time = Time::HiRes::tv_interval( $time ) * 1000;
    }
    return $class->send( { $stat => "$time|ms" }, $sample_rate );
}

=head2 average

Calculates the average. Similar to timing but only records the average in graphite

 @param string $stats The metric to in log timing info for.
 @param float $time The elapsed time (ms) to log
 @param float|1 $sampleRate the rate (0-1) for sampling.

=cut

sub average {
    my ($class, $stat, $value, $sample_rate) = @_;
    return $class->send( { $stat => "$value|a" }, $sample_rate );
}

=head2 increment

Increments one or more stats counters

 @param string|array $stats The metric(s) to increment.
 @param float|1 $sampleRate the rate (0-1) for sampling.
 @return boolean

=cut

sub increment {
    my ($class, $stats, $sample_rate) = @_;
    return $class->update_stats( $stats, 1, $sample_rate );
}

=head2 decrement

Decrements one or more stats counters.

 @param string|array $stats The metric(s) to decrement.
 @param float|1 $sampleRate the rate (0-1) for sampling.
 @return boolean

=cut

sub decrement {
    my ($class, $stats,$sample_rate);
    return $class->update_stats( $stats, -1, $sample_rate );
}

=head2 update_stats

Updates one or more stats counters by arbitrary amounts

 @param string|array $stats The metric(s) to update. Should be either a string or array of metrics.
 @param int|1 $delta The amount to increment/decrement each metric by.
 @param float|1 $sampleRate the rate (0-1) for sampling.
 @return boolean

=cut

sub update_stats {
    my ($class, $stats_ref, $delta, $sample_rate) = @_;

    # set some defaults
    $delta       ||= 1;

    my (@stats) = ref($stats_ref) ? @$stats_ref : $stats_ref;

    my %data;
    foreach my $stat ( @stats ) {
        $data{$stat} = "$delta|c";
    }

    $class->send( \%data, $sample_rate );
}

=head2 send

Squirt the metrics over UDP

=cut

sub send {
    my ($class, $data_ref, $sample_rate) = @_;

    # TODO: move to config
    my $STATSD_ENABLED = 1;
    if ( !$STATSD_ENABLED ) {
        return;
    }

    if ( !$data_ref ) {
        return;
    }

    # default the sample rate
    $sample_rate ||= 1;

    # sampling support
    my %sampled_data;
    if ( $sample_rate < 1 ) {
        while ( my ($stat, $value) = each %$data_ref  ) {
            if ( rand() <= $sample_rate ) {
                $sampled_data{ $stat } = "$value|\@$sample_rate";
            }
        }
    }
    else {
        %sampled_data = %$data_ref;
    }

    if ( !%sampled_data ) {
        return;
    }

    eval {
        my $sock = $class->connect()
            or return;

        while ( my ($stat, $value) = each %sampled_data ) {
            print $sock "$stat:$value";
        }
        close($sock);
    };

    if ( $@ ) {
        # don't care - siliently ignore any failures
    }
}

sub connect {
    my $class = shift;

    # TODO: move to config
    my $server = shift || '46.254.116.3';
    my $port   = shift || '8125';

    return IO::Socket::INET->new(
        PeerAddr => $server,
        PeerPort => $port,
        Proto    => 'udp',
    );
}

1;

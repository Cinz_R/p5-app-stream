#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id$

package Bean::Log::File;

use strict;

use utf8; # source file is utf8!

use base qw(Bean::Log);

use Bean::FileStore;

sub default_class {
    return 'logs';
}

sub filestore_class {
    return 'Bean::FileStore';
}

sub _new_fh {
    my $self = shift;

    my $key = $self->key();

    return undef unless $key;

    _debug("Creating log file: $key");

#     use IO::File;
#     return IO::File->new('/tmp/'.$key, 'w') or die "Unable to create /tmp/$key: $!";

    my $filestore_class = $self->filestore_class();
    return $filestore_class->new_file( $key, $self->class() );
}

sub last_error {
    my $class = shift;
    my $filestore_class = $class->filestore_class();
    return $filestore_class->error();
}

sub _print {
    my $self = shift;
    if ( !@_ && !$self->{buffer} ) {
        return;
    }

    my $fh = $self->fh();

    if ( $fh ) {
        if ( $self->{buffer} ) {
            my $buffer_ref = delete $self->{buffer};

            foreach my $buffer ( @$buffer_ref ) {
                print $fh $buffer;
            }
        }

        foreach my $line ( @_ ) {
            print $fh $line;
        }

        return;
    }

    # no fh available yet so buffer up output
    my $buffer_ref = $self->{buffer} ||= [];

    push @$buffer_ref, @_;
}

sub fh {
    my $self = shift;

    return $self->{fh} ||= $self->_new_fh();
}

sub key {
    my $id = $_[0]->id();

    return undef unless $id;
    if ( $id eq $_[0]->{_id} ) {
        return $_[0]->{key};
    }

    $_[0]->{_id} = $id;
    return $_[0]->{key} = 'log-' . $id . '-'. time() . '-' . $$;
}

sub class {
    if ( @_ == 1 ) {
        return $_[0]->{class} || $_[0]->default_class();
    }

    return $_[0]->{class} = $_[1];
}

sub close {
    my $self = shift;

    eval {
        $self->_print();
    };

    if ( $@ ) {
        _debug("error: " . $@ );
    }

    $self->close_fh();
}

sub close_fh {
    my $self = shift;
    my $retry = shift;

    my $fh = $self->{fh};

    if ( !$self->{fh} ) {
        return;
        return warn "No open files to close";
    }

    if ( !$fh->close() ) {
        _debug("Unable to close file");
        my $last_error = $self->last_error();
        _debug("Error message was: " . $last_error );

        if ( $retry > 0 ) {
            # mainly for mogilefs - let it retry the close
            return $self->close_fh( $retry - 1 );
        }

        die "Error writing file: " . $last_error;
    }

    $self->{fh} = undef;

    _debug("Saved log file: " . $self->key() );
}

sub DESTROY {
    my $self = shift;

    $self->SUPER::DESTROY();

#    _debug( "Destroying logfile " . $self->key() );

    if ( $self->{cancel} ) {
        return;
    }

#    _debug( "Closing logfile " . $self->key() );
    eval {
        $self->close(1);
#        _debug( "Closed logfile " . $self->key() );
    };
    if ( $@ ) {
        _debug( "Error closing logfile: " . $@ );
    }
}

sub _debug {
    if ( exists($ENV{'HTTP_HOST'}) ) {
        print STDERR "\n\n#### " . scalar(localtime()) . "\n\t", join( "\n\t", @_ ) . "\n####\n\n";
    }
    if ( exists( $INC{'Stream/Log.pm'} ) ) {
        Stream::Log->debug( @_ );
    }
}

1;

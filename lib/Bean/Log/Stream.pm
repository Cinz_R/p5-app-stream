#!/usr/bin/perl

# vim: tabstop=4:expandtab:encoding=utf-8

# $Id: Stream.pm 37778 2010-07-13 13:39:22Z andy $

package Bean::Log::Stream;

use strict;

use base qw(Bean::Log::File);

use Stream::FileStore;

sub list_thread_logs {
    my $pkg = shift;
    my $thread_id = shift;

    my $class = $pkg->class_for( $thread_id );
    my $filestore_class = $pkg->filestore_class();

    return $filestore_class->list_keys(
        'log-' . $thread_id . '-',
        $class,
    );
}

sub load_log {
    my $pkg = shift;
    my $logfile = shift;

    my $class = $pkg->class_for( $logfile );

    require Bean::Log::Parser;
    
    my $filestore_class = $pkg->filestore_class();
    return Bean::Log::Parser->new_from_key( $logfile, $class, $filestore_class );
}

sub filestore_class {
    return 'Stream::FileStore';
}

sub class {
    if ( @_ == 1 ) {
        if ( $_[0]->{class} ) {
            return $_[0]->{class};
        }

        # determine the log class from the id
        my $id = $_[0]->id();

        return $_[0]->class_for( $id );
    }

    return $_[0]->{class} = $_[1];
}

sub class_for {
    my $self = shift;
    my $id = shift || return $self->default_class();

    if ( $id =~ m/candidate/ ) {
        return 'streamcandidate';
    }
    elsif ( $id =~ m/^\d+$/ || $id =~ m/^log-\d+-\d+$/ ) {
        return 'streamsearch';
    }

    return $self->default_class();
}

1;

package Bean::Log::Stderr;

use strict;

use base 'Bean::Log';

sub _print {
    shift;
    print STDERR @_;
}

1;

#!/usr/bin/perl

# vim:expandtab:tabstop=4:encoding=utf-8

# $Id: DevNull.pm 33260 2010-04-26 15:08:20Z andy $

package Bean::Log::DevNull;

use strict;

use base qw(Bean::Log);

BEGIN {
    unless ( exists( $ENV{'TEST_VERBOSE'} ) && $ENV{'TEST_VERBOSE'} ) {
        *log = sub {};
    }
};

sub key {
    return;
}

sub _print {
    shift;
    Test::More::diag( @_ );
}

sub DESTROY  {}

1;

package App::Stream2;

use strict;
use warnings;

use Mojo::Base 'Mojolicious';
use App::Stream2::Controller;
use Stream2;

use JSON;
use Parse::HTTP::UserAgent;
use Log::Log4perl::MDC;
use Log::Log4perl::NDC;

use CHI;

use Mojolicious::Sessions::ThreeS;
use Mojolicious::Sessions::ThreeS::Storage::CHI;

use App::Stream2::Sessions3S::State;

=head1 NAME

App::Stream2 - The Mojolicious Stream2 Web application.

=head1 PROPERTIES

=head2 stream2

The L<Stream2> application.

=cut

## In dev, this is very helpful. Sometimes.
# use Carp::Always;

has stream2_class => sub {
    return 'Stream2';
};

# The Stream2 application
has 'stream2' => sub{
    my ($self) = @_;
    my $file = $ENV{MOJO_CONFIG} || $self->moniker . '.conf';
    my $stream2_class = $self->stream2_class;
    return $stream2_class->new({ config_file => $self->home->rel_file( $file ) });
};

# Proxy accessors for compatibility of code using the app
has 'stream_schema' => sub { $_[0]->stream2->stream_schema() };
has '_redis' => sub { $_[0]->stream2->redis(); };
has '_queue' => sub{ $_[0]->stream2->qurious(); };

sub startup {
    my $self = shift; # the app

    $self->setup_mojo_defaults();
    $self->setup_commands();
    $self->setup_plugins();
    $self->setup_session();
    $self->setup_routes();
    $self->setup_helpers();

    Log::Log4perl::MDC->put('sentry_tags' , { subsystem => 'webapp' });

    $self;
}

sub setup_mojo_defaults {
    my $self = shift;
    $self->controller_class('App::Stream2::Controller');
}

sub setup_commands {
    my ($self) = @_;

    push @{$self->commands->namespaces}, 'App::Stream2::Command';

    $self;
}

sub setup_plugins {
    my ($self) = @_;

    push @{$self->plugins->namespaces}, 'App::Stream2::Plugin';

    $self->setup_config_plugin();

    ## NOTE: This is neutered until we find how to install SAML2 properly with carton
    # $self->plugin('Bean::Mojolicious::Plugin::SAML2' => $self->config()->{saml2} );

    # Build the stream2 application instance
    # right after the config is loaded.
    # This is to avoid the singleton Stream2->instance()
    # to be null for other plugins.
    my $s2 = $self->stream2();

    $self->setup_other_plugins();

    $self;
}

sub setup_helpers {
    my $self = shift;

    # accepts action_name, <Stream2::Results::Result>, <HashRef>
    # adds an entry to candidate_action_log
    $self->helper(
        log_action => sub {
            my $controller = shift;
            my ( $action, $candidate, $data ) = @_;
            my $user = $controller->current_user();

            my $schema = $controller->app()->stream2->stream_schema;

            return $schema->txn_do(
                sub {
                    # Some auth providers would just find an action
                    # some others would autovivify it.
                    # This should die if the action is invalid.
                    my $action_o = $controller->auth_provider()
                      ->find_action( $user, $action );
                    $controller->app->stream2()->action_log()->insert(
                        {
                            action           => $action,
                            candidate_id     => $candidate->id,
                            destination      => $candidate->destination,
                            user_id          => $user->id,
                            group_identity   => $user->group_identity,
                            search_record_id => $candidate->search_record_id(),
                            data             => {
                                %{ $data || {} },
                                $candidate->action_hash()
                            }
                        }
                    );
                }
            );
        }
    );

    $self->helper(retrieve_notes => sub {
        my $controller = shift;
        return $controller->app->stream2()->action_log()->get_notes(
            $controller->current_user()->id(),
            $controller->current_user()->group_identity(),
            @_
        );
    });
    $self->helper(useragent => sub {
        my $c = shift;
        unless ( $c->stash('useragent') ){
            my $headers = $c->tx->req->headers;
            if( my $useragent_str = $headers->header('User-Agent') ){
                my $ua = Parse::HTTP::UserAgent->new( $useragent_str );
                $c->stash( useragent => $ua );
            }
        }
        return $c->stash( 'useragent' );
    });
    $self->helper(support_email => sub {
        my $controller = shift;
        my $locale = $controller->current_user()->locale;
        if ( $locale =~ m/^en_usa|en_US$/i ) {
            return 'supportus@broadbean.com';
        }
        return $self->app->config->{support_email};
    });
}

sub setup_config_plugin {
    my ($self) = @_;

    $self->defaults( config => $self->config )->config( $self->stream2->config );
    $self->secrets([$self->config->{secret_key}]);

    $self;
}

sub setup_other_plugins {
    my ($self) = @_;

    # Log before anything.
    $self->plugin('log', $self->config);
    $self->plugin('textdomain', $self->config->{translations} || {} );
    $self->plugin('basic_auth');

    # this is now the unified templates directory
    my $template_directory = $self->stream2->templates->dir();
    # render templates with the template toolkit plugin
    $self->plugin(
        'tt_renderer' => {
            template_options => {
                INCLUDE_PATH => $template_directory,
                FILTERS      => {
                    'em_highlight' => sub {
                        my $text = shift;

                        # NON GREEDY substitution of <em></em> with
                        # non html escaped <em></em>
                        $text =~ s/&lt;em&gt;(.*?)&lt;\/em&gt;/<em>$1<\/em>/g;
                        return $text;
                    }
                },
            }
        }
    );


    $self->renderer->default_handler('tt');

    $self->plugin('auth', sub { $self->config->{plugins}->{auth} });
    $self->plugin('queue');

    # Some utilities, like remote_ip()
    # Lives in lib/Mojolicious/Plugin/Utils.pm
    $self->plugin('utils');

    $self;
}

sub setup_session {
    my ($self) = @_;

    my $storage = Mojolicious::Sessions::ThreeS::Storage::CHI->new({
        chi => CHI->new( driver => 'Redis',
                         namespace => 'websessions',
                         redis => $self->stream2()->redis()
                     )
    });
    $self->plugin( Sessions3S => { storage => $storage,
                                   state => App::Stream2::Sessions3S::State->new()
                               } );

    my $sessions = $self->sessions;
    $sessions->cookie_name($self->moniker);
    if ( defined( my $default_expiration = $self->config->{default_expiration} ) ) {
        $sessions->default_expiration( $default_expiration );
    }
    if ( my $cookie_domain = $self->config->{cookie_domain} ) {
        $sessions->cookie_domain($cookie_domain);
    }

    $self;
}

sub setup_routes {
    my $self = shift;

    # Prefetch busting. it looks like chrome does a lot of pre fetching
    # which sparks an entire search - look in app/scripts/stream2.js for chrome
    $self->hook(before_dispatch => sub {
        my $c = shift;

        # Reset the NDC.
        Log::Log4perl::NDC->remove();
        my $headers = $c->tx->req->headers;

        # safari
        if ( $headers->header('X-Purpose') && $headers->header('X-Purpose') eq 'preview' ) {
            return $c->reply->not_found;
        }
        # firefox
        if ( $headers->header('X-moz') && $headers->header('X-moz') eq 'prefetch' ) {
            return $c->reply->not_found;
        }

        # Don't do checks for static resources
        return if ( $c->tx->req->url =~ m/\A\/(?:static|healthcheck)\/?/ );

        # What this f**k is this? well... nginx is translating the traffic coming in from https to http
        # apparently the X-Forwarded-Proto header mentioned in the mojolicious docs for nginx is insuffient
        # for mojo to automaticallty generate URLS with an https scheme!
        # http://mojolicio.us/perldoc/Mojolicious/Guides/Cookbook
        # proxy => 1 is in the config but it doesnt seem to do anything.
        # to be continued
        # this is working in newer MOJO 5.05
        # if ( $c->tx->req->headers->header('X-Forwarded-Proto') eq 'https' ){
        #     $c->tx->req->url->base->scheme('https');
        # }

        # Put some request data in the Log4perl MDC (for sentry)
        my $req = $c->tx->req();
        my $request_body = $req->content->is_multipart() ? 'multipart-data' : $req->body();
        Log::Log4perl::MDC->put('sentry_http', { url => $req->url().'',
                                                 method => $req->method(),
                                                 data => $request_body.'',
                                                 env => \%ENV
                                               });

        my $X_Requested_With = $headers->header('X-Requested-With');
        if( ( $X_Requested_With // '' ) eq 'XMLHttpRequest' ){
            # This is an ajax request.

            # A bit of cache control would be nice.
            # See http://support2.microsoft.com/default.aspx?scid=KB;EN-US;Q234067
            $c->res->headers->cache_control('max-age=1, no-cache');


            # Check the current front end version is the same as the received one.
            my $received_frontend_version = $headers->header('X-Stream2-VERSIONFRONTEND') // 'unknown';
            unless( $c->app->stream2()->VERSION_FRONTEND() eq $received_frontend_version ){
                return $c->render( status => 409, # 409 is CONFLICT
                                   exception => 'A new version of search is available.' );
            }

            # Check the current_user is the same as the received user.

            my $current_user_id = $headers->header('X-Stream2-CurrentUserId');
            if( my $current_user = $c->current_user() ){
                unless( $current_user->id() == $current_user_id ){
                    return $c->render( status => 409, # 409 is CONFLICT
                                       exception => 'We detected you signed-in as a different user.' );
                }
            }

            # No current user. what do we do? Continue for now just as usual
        }

        if ( my $json = $c->param('q') ){
            my $json_ref = {};
            eval { $json_ref = JSON::from_json( $json ); };
            $c->stash( 'query_ref' => $json_ref );
        }

        if (my $user =  $c->current_user ){
            $c->app->stream2->translations->set_language( $user->locale() || 'en' );
            $c->app->stream2->translations->set_timezone( $user->timezone() || 'Europe/London' );
        }else{
            $c->app->stream2->translations->set_language( 'en' );
            $c->app->stream2->translations->set_timezone( 'Europe/London' );
        }
    });

    $self->hook('after_dispatch' => sub{
                    my $c = shift;

                    my $req_headers = $c->tx->req->headers();
                    my $X_Requested_With = $req_headers->header('X-Requested-With');

                    unless( ( $X_Requested_With // '' ) eq 'XMLHttpRequest' ){
                        # NOT an ajax request. Emit extra headers.
                        $c->res->headers->add('P3P' => 'policyref="/w3c/p3p.xml", CP="CURa ADMa DEVa OUR IND DSP ALL COR"');
                    }
                });

    $self->hook( 'before_render' => sub {
        my ( $c, $args ) = @_;

        if ( $args->{status} && $args->{status} != 200 ){
            # Mojo's built in exception handling has two modes, 404 or 500.
            # We also need to cater for other types of errors and formats ( i.e. 400 xml )
            # If a route returns $self->render({ exception => 'Bad request', status => 400 }) we can better handle it
            # with this added logic

            # Display a page not found text error
            if ( $args->{status} == 404 ){
                $args->{exception} = "Page not found";
            }

            # Ensure exception is correct format
            if ( $args->{exception} ) {
                my $exception = $args->{exception} . '';
                chomp( $exception );
                $args->{exception} = Mojo::Exception->new( $exception );
            }

            if ( $c->accepts('', 'json') ) {
                my $error = $args->{json}->{error};

                # some legacy checks
                unless ( ref($error) eq 'HASH' ) {
                    $error = { message => $error };
                }
                unless ( $error->{message} ) {
                    my $message = $args->{json}->{message} || $args->{exception};
                    $error->{message} = $message;
                }

                # our json api should always respond with an error object
                $args->{json}->{error} = $error;
            }
            elsif ( $c->accepts('', 'xml') ){
                # data is like text but doesn't apply encoding to UTF-8, XML::LibXML already does this at toString
                $args->{data} = $c->generate_xml_exception( $args );
                $c->res->headers->content_type( 'text/xml;charset=UTF-8' );
            }
            else {
                # default for */* - use the default exception template
                $args->{format} = 'html';
                $args->{template} //= 'exception.production';
            }
        }
    });

    my $r = $self->routes();

    # TODO: tmp hack to redirect logo 404s to placeholder
    $r->get('/static/board_logos/:logo')->to( cb => sub {
        shift->redirect_to('http://placehold.it/150x70');
    });

    $r->get('/login')->to('web-auth#login');
    $r->post('/login')->to('web-auth#authenticate');

    $r->get('/login/as/:adc_user_id')->to('web-auth#login_as');
    $r->get('/login/popuser')->to('web-auth#popuser');

    $r->post('/external/oneiam_sso/verifycb')->to('web-feed_o_auth#verify');
    $r->get('/external/oneiam_sso/logout')->to('web-auth#logout_sso');

    $r->get('/auth/:provider')->to('web-auth#authenticate');
    $r->post('/auth/:provider')->to('web-auth#authenticate');

    $r->get('/logout')->to('web-auth#logout');

    my $search_json = $r->any('/search_json/:id');
    $search_json->any('criteria')->to('api-search#criteria');

    $search_json->any('resume/:board/page/:page_id')->to('api-search#resume');
    $search_json->any('run')->to('api-search#run');
    $search_json->any('page/:page_id')->to('api-search#results');
    $search_json->post('bulk_forward')->to('api-search#bulk_forward_candidates');

    #Saved searches

    $r->any('/search_json/run')->to('api-search#run');

    my $api = $r->any('/api');

    # Error code on demand (for tests)
    # see Mojo::Message::Response for vaild codes.
    # try to stick to:
    # 200 => 'OK',  403 => 'Forbidden', 404 => 'Not Found',
    # 500 => 'Internal Server Error',   502 => 'Bad Gateway',
    # 503 => 'Service Unavailable',     504 => 'Gateway Timeout',
    $api->any('/errorcode/:code')
        ->to(
            cb => sub{
                my $c = shift;
                my $code = $c->stash('code');
                return $c->reply->not_found unless $code =~ /^\d{3}$/;
                $c->render(
                    status => $code + 0 ,
                    text => 'This is code '.$code
                );
            });

    # Async Job Control
    my $asyncjob = $api->any('/asyncjob');
    $asyncjob->get('/:id/status')->to('Api::AsyncJob#status');


    my $tinymce = $api->any('/tinymce');
    $tinymce->post('/spellcheck')->to('api-tinymce#spellcheck');
    $tinymce->get('/images')->to('api-tinymce#images');
    $tinymce->post('/upload')->to('api-tinymce#image_upload');
    $tinymce->delete('/image')->to('api-tinymce#delete_image');

    $api->post('create_criteria')->to('api-search#create_criteria');

    $api->post('/savedsearches')->to('api-savedsearches#save');
    $api->get('/savedsearches')->to('api-savedsearches#list');
    $api->delete('/savedsearches')->to('api-savedsearches#unsave');

    my $boards_json = $api->any('/boards');
    $boards_json->any('active')->to('api-boards#active');
    $boards_json->post('/set-positions')->to('api-boards#set_positions');
    $api->get('board/:board_name')->to('api-boards#board_meta_information');


    # API routes
    $api->get('/settings/boardaccounts')->to('api-settings#boardaccounts');
    $api->any('/locations/suggest')->to('api-locations#suggest');

    $api->any('/cheesesandwiches/:list_name')->to('api-adverts#search');
    $api->get('/cheesesandwiches/:list_name/:advert_id/preview/:board_id')->to('api-adverts#previewadvert');

    # Admin under (under '/api/')
    my $admin_under = $api->under('admin/')->to(controller => 'api-admin', action => 'load_user' );

    $admin_under->get('response-flags')->to('api-admin#list_response_flags');
    $admin_under->post('response-flags')->to('api-admin#add_response_flag');
    $admin_under->put('response-flags/:id')->to('api-admin#update_response_flag');
    $admin_under->post('add-flag-rejection-reason')->to( controller => 'api-admin', action => 'add_flag_rejection_reason' );
    $admin_under->post('delete-flag-rejection-reason')->to( controller => 'api-admin', action => 'delete_flag_rejection_reason' );

    $admin_under->get('user_hierarchy')->to( controller => 'api-admin' , action => 'get_user_hierarchy' );

    # Macros
    $admin_under->get('group_macros')->to( controller => 'api-admin', action => 'list_group_macros' );
    $admin_under->get('group_macros/:macro_id')->to( controller => 'api-admin', action => 'get_group_macro' );
    # No macro_id -> new
    $admin_under->post('group_macros/')->to( controller => 'api-admin', action => 'set_group_macro' );
    # With macro_id ->edit
    $admin_under->post('group_macros/:macro_id')->to( controller => 'api-admin', action => 'set_group_macro' );
    $admin_under->delete('group_macros/:macro_id')->to( controller => 'api-admin', action => 'delete_group_macro' );

    # Global product settings.
    $admin_under->get('available_settings')->to( controller => 'api-admin' , action => 'get_available_settings' );
    # by ID
    $admin_under->get('user_settings/:setting_id')->to( controller => 'api-admin', action => 'get_user_settings' );
    $admin_under->post('user_settings/:setting_id')->to( controller => 'api-admin', action => 'set_user_settings' );
    # by setting_mnemonic
    $admin_under->get( 'user_settings_by_mnemonic/:setting_mnemonic')->to( controller => 'api-admin', action => 'get_user_settings_by_mnemonic' );
    $admin_under->post('user_settings_by_mnemonic/:setting_mnemonic')->to( controller => 'api-admin', action => 'set_user_settings_by_mnemonic' );

    # Group specific settings
    $admin_under->get('user_groupsettings/:setting_mnemonic')->to( controller => 'api-admin', action => 'get_user_groupsettings' );
    $admin_under->post('user_groupsettings/:setting_mnemonic')->to( controller => 'api-admin', action => 'set_user_groupsettings' );

    $admin_under->get('email-templates')->to( 'api-admin#list_emailtemplates' );
    $admin_under->post('email-templates')->to( controller => 'api-admin' , action => 'create_emailtemplate' );
    $admin_under->post('email-templates/sample')->to( controller => 'api-admin' , action => 'send_emailtemplate_sample' );

    $admin_under->delete('email-templates/:template_id')->to( controller => 'api-admin' , action => 'delete_emailtemplate' );
    $admin_under->put('email-templates/:template_id')->to( controller => 'api-admin' , action => 'set_emailtemplate' );

    $admin_under->get('savedsearches')->to( controller => 'api-admin', action => 'list_savedsearches' );
    $admin_under->post('tags/tag/set_flavour')->to(controller => 'api-admin', action => 'set_tag_flavour' );
    $admin_under->post('tags/tag')->to(controller => 'api-admin', action => 'add_tag' );
    $admin_under->get('tags/tag/info')->to( controller => 'api-admin' , action => 'get_tag_info' );
    $admin_under->post('tags/tag/delete')->to( controller => 'api-admin', action => 'delete_tag' );
    $admin_under->get('tags/:page_num')->to( controller => 'api-admin', action => 'get_tags' );
    $admin_under->get('watchdogs')->to( controller => 'api-admin', action => 'list_watchdogs' );
    $admin_under->get('previous-searches')->to('api-admin#previous_searches');

    $admin_under->get('/emails/search/')->to('api-emails#search_group_emails');

    $admin_under->get('/user_quota')->to('api-admin#get_quota');
    $admin_under->post('/user_quota')->to('api-admin#set_quota');

    #Watchdog routes
    # Watchdogs

    # The api

    # Actions on watchdogs set
    $api->get('/watchdogs')->to('api-watchdogs#list');


    $api->delete('/watchdogs')->to('api-watchdogs#delete');
    $api->post('/watchdogs')->to('api-watchdogs#create');

    # One watchdog
    my $one_watchdog_under = $api->under('watchdog/:watchdog_id')->to( controller => 'api-watchdogs' , action => 'load_one' );

    $one_watchdog_under->delete('viewed_results')->to('api-watchdogs#clear_all_viewed_results');

    $one_watchdog_under->delete('view/board/:board/viewed_results')->to('api-watchdogs#clear_viewed_results' );
    $one_watchdog_under->get('view/board/:board/page/:page')->to('api-watchdogs#view');

    $one_watchdog_under->post('')->to('api-watchdogs#edit');
    $one_watchdog_under->post('criteria_id')->to('api-watchdogs#edit_criteria_id');

    # $api->post('/watchdog/:watchdog_id/criteria_id')->to('api-watchdogs#edit_criteria');

    # Ripple API
    $r->post('/ripple')->to('ripple#index');
    $r->post('/ripple/search')->to('ripple-json#search');
    $r->post('/ripple/search-results')->to('ripple-json#search_and_wait');
    $r->post('/ripple/login')->to('ripple-json#login');
    $r->post('/ripple/update_session')->to('ripple-json#update_session');
    $r->get('/ripple/status')->to('ripple-json#status');
    $r->get('/ripple/results')->to('ripple-json#results');

    $r->get('/')->to( 'web#force_connect' );

    $r->get('/connectas/:adc_user_id')->to('web#connect_as');

    # Let ember handle all other routes
    $r->get( $_ )->to('web#index') for (
        '/search', 'welcome-admin', '/longlists/*rest',
        '/search/*rest', '/admin', '/admin/*rest', '/emails*', '/watchdogs/*rest'
    );

    $r->get('/exit-responses')->to(cb => sub {
        delete $_[0]->session()->{manage_responses_view};
        $_[0]->render( json => { ok => 1 } );
    });

    # /external/:board/oauth/callback: A callback route for feeds that require an OAuth process
    $r->get('/external/twitter/oauth/callback')->to( 'Web::FeedOAuth#twitter' );

    # the Unsubscribe routes.
    $r->get('/external/unsubscribe/:id/:auto' => [auto => [qw(auto)]])->to( 'web-unsubscribe#index', auto=>undef, id=>undef );
    $r->post('/external/unsubscribe/:id')->to( 'web-unsubscribe#submit', id=>undef );

    # The AWS SES Notification through AWS SNS
    $r->post('/external/ses_notification')->to( 'Web::SESNotification#receive_sns_notification' );
    $r->post('/external/mailjet_notification')->to( 'Web::MailJetNotification#receive_notification' ); # Legacy route - use with :transport_method instead; remove this after next deploy
    $r->post('/external/mailjet_notification/:transport_method')->to( 'Web::MailJetNotification#receive_notification' );

    # The new responses SNS notification
    $r->post('/external/sns_bbcss_responses')->to( 'Web::SNSNotification#receive_bbcss_response' );

    #SAML2
    $r->post('/external/okta_saml2')->to(
        cb => sub{
            my ($c) = @_;
            my $assertion_string = $c->param('SAMLResponse');
            my $assertion = $c->app()->saml2()->decode_assertion( $assertion_string );
            $c->session()->{'saml2_nameid'} = $assertion->nameid();
            $c->redirect_to( $c->session( 'saml_redirect_path' ) );
        }
    );

    my $dev_user_under = $r->under('/devuser')->to( 'web-devuser#devuser_under' );
    $dev_user_under->get('/email_notifications')->to( 'web-devuser#view_notifications' );
    $dev_user_under->get('/email_notification_extra_information')->to( 'web-devuser#view_extra_notification_information' );
    $dev_user_under->get('/view_blacklisted_emails')->to( 'web-devuser#view_blacklisted_emails' );
    $dev_user_under->get('/view_response_log/:response_id')->to( 'web-devuser#response_log' );

    # Search log route
    $r->get('/searchrecords/:record_id')->to( controller => 'web-log' , action => 'show_search_record' );
    $r->get('/searchrecords/by-log-id/:log_id')->to( controller => 'web-log' , action => 'show_search_record' );
    $r->get('/searchrecords/by-s3log-id/:s3_log_id')->to( controller => 'web-log' , action => 'show_search_record_s3_log' );
    $r->get('/stats')->to( controller => 'web-internal' , action => 'qurious_stats' );
    $r->get('/candidateactions/:action_id')->to( controller => 'web-log', action => 'show_candidate_action', action_id => undef );
    $r->get('/show_html/:s3_log_id/:request_num')->to( controller => 'web-log' , action => 'show_html' );

    $r->get('/sandbox')->to( controller => 'web-sandbox' , action => 'index' );

    $r->get('/usage-report')->to( controller => 'web', action => 'usage_report_form' );
    $r->post('/usage-report')->to( controller => 'web', action => 'usage_report' );

    my $user_route = $api->any('user');
    $user_route->get('/cache/:key')->to('api-user#getcache');
    $user_route->get('/suggest_usertag')->to('api-user#suggest_usertag');
    $user_route->get('/message-defaults')->to('api-user#messagedefaults');
    $user_route->get('/custom_lists')->to('api-user#get_custom_lists');
    $user_route->get('/details')->to('api-user#get_details');
    $user_route->get('/isadmin')->to('api-user#is_admin');
    $user_route->get('/oversees')->to('api-user#oversees');
    $user_route->get('/keyword-suggest')->to('api-user#autocomplete_keywords');
    $user_route->delete('/longlists/:id')->to('api-user#delete_longlist');
    $user_route->get('/adcresponse_flags')->to('api-user#get_available_adcresponse_flags');
    $user_route->get('/quota')->to('api-user#get_quota_summary');
    $user_route->get('/activity_report')->to('api-user#activity_report');
    $user_route->get('/actions')->to('api-user#actions');

    # Candidate under. Factorizes loading one candidate.
    my $cand_under = $r->under('/results/:results_id/:board/candidate/:candidate_idx')
      ->to( controller => 'api-candidate', action => 'load_candidate' );

    $cand_under->post('/shortlist')->to('api-candidate#shortlist');
    $cand_under->post('/message')->to('api-candidate#message');
    $cand_under->post('/delete_candidate')->to('api-candidate#delete_candidate');
    $cand_under->post('/phonecall')->to('api-candidate#phone_candidate');

    #search tags
    $user_route->get('/tags')->to('api-user#autocomplete_tags');
    $user_route->get('/colleagues')->to('api-user#autocomplete_colleagues');

    $user_route->get('/email-templates')->to( 'api-user#list_emailtemplates' );
    $user_route->get('/email-templates/:template_id')->to( 'api-user#get_emailtemplate' );

    $user_route->post('/share')->to('api-user#share_search_criteria');

    # retrieve sent email messages
    $user_route->get('/emails/search/')->to('api-emails#search_user_emails');
    $user_route->get( '/emails/:id' => [id => qr/\d+/] )->to('api-emails#get_email');

    # Note: this is the same as ( controller => 'api-candidate' , action => 'downloadcv' )
    $cand_under->get('/cv')->to('api-candidate#downloadcv');

    # Forward is an action. Prefer a POST
    $cand_under->post('/delegate')->to('api-candidate#delegate_candidate');
    $cand_under->post('/forward')->to('api-candidate#forward_candidate');
    $cand_under->get('/profile')->to('api-candidate#downloadprofile');
    $cand_under->get('/import')->to( controller => 'api-candidate', action => 'import_candidate' );
    $cand_under->get('/application_history')->to('api-candidate#application_history');
    $cand_under->post('/longlist')->to('api-candidate#longlist_candidate');
    $cand_under->get('/longlist')->to('api-candidate#get_longlist_candidate');

    $cand_under->get('/history')->to( controller => 'api-candidate', action => 'history_log' );
    $cand_under->get('/profile_history')->to( controller => 'api-candidate', action => 'profile_history' );
    $cand_under->get('/email_history')->to( controller => 'api-candidate', action => 'email_history' );
    $cand_under->get('/other_attachments')->to( controller => 'api-candidate', action => 'other_attachments' );
    $cand_under->get('/download_other_attachment')->to( controller => 'api-candidate', action => 'download_other_attachment' );
    $cand_under->get('/enhancements')->to( controller => 'api-candidate', action => 'enhancements' );

    # Cancelling future actions
    $cand_under->delete('/future_action/:action_id')->to('api-candidate#delete_future_action');

    # Post { tag_name => 'new tag' }
    $cand_under->post('/tags')->to( controller => 'api-candidate' , action => 'tag_candidate');
    # Dont forget to URI::Escape the tag name.
    $cand_under->delete('/tags')->to( controller => 'api-candidate' ,  action => 'untag_candidate' );

    $cand_under->put('/usertags')->to( controller => 'api-candidate' ,  action => 'set_usertags' );

    $cand_under->post('/property/:field_name')->to('api-candidate#property');
    $cand_under->post('/properties')->to('api-candidate#properties');

    $cand_under->post('/note')->to('api-candidate#add_note');
    $cand_under->delete('/note/:note_id')->to('api-candidate#delete_note');

    ## End of candidate resources.

    $r->get('/healthcheck')->to( cb => sub { shift->render( text => "OK" ); } );

    $self;
}

1;


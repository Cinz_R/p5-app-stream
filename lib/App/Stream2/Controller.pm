use strict;
use warnings;

package App::Stream2::Controller;
use Mojo::Base 'Mojolicious::Controller';

use Data::Dumper;

sub default_format { 'html'; }

sub on_exception {
    my $self = shift;
    my $e = shift;

    $self->app->log->error($e);
}

sub extend_timeout {
    my $self = shift;
    my $time = shift;
    my $cb = shift || sub {
        $self->render( exception => "Timout", status => 504 );
    };

    Mojo::IOLoop->stream($self->tx->connection)->timeout( $time + 1 );
    my $id = Mojo::IOLoop->timer( $time => $cb );

    $self->on('finish', sub { Mojo::IOLoop->remove($id); });
}

=head2 classify_ids

Given a list of ids of the form [ 'class1:1' , 'class1:2' , 'class1:4' , 'class2:bla' , 'foo' ] , returns a structure like:

{

  '' => [ 'foo' ],
  'class1' => [ 1 ,2 , 4 ],
  'class2' => [ 'bla' ]
}

=cut

sub classify_ids{
    my ($self, $ids ) = @_;

    my $classified = {};

    foreach my $class_id  ( @{ $ids } ){
        my ($class, $id) = ( $class_id =~ /^(\w+):(\S+)/ );
        unless( $class ){
            $class = '' ; $id = $class_id;
        }
        $classified->{$class} //= [];
        push @{ $classified->{$class} } , $id;
    }
    return $classified;
}



sub generate_xml_exception {

    my ( $self, $args ) = @_;
    my $err = $args->{exception};
    my $code = $args->{status};

    chomp( $err );

    my $now = DateTime->now();
    my $xml_response = XML::LibXML::Document->createDocument('1.0', 'UTF-8');

    my $version = $self->stash('ripple_version') || '3.0';
    if ( $version ne '3.0' ){
        my $xsearch = $xml_response->createElement('CV_Search_Query');
        $xml_response->setDocumentElement($xsearch);

        $xsearch->appendChild($xml_response->createElement('Information'));
        my $error = $xsearch->appendChild($xml_response->createElement('Errors'))
            ->appendChild($xml_response->createElement('Error'));

        $error->appendChild($xml_response->createElement('Code'))->appendText('INVALID XML');
        $error->appendChild($xml_response->createElement('Detail'))->appendText($err);
    }
    else {
        my $xsearch = $xml_response->createElement('Search');
        $xml_response->setDocumentElement($xsearch);

        $xsearch->appendChild($xml_response->createElement('TimeNow'))->appendText($now->iso8601());

        my $xsearch_response = $xsearch->appendChild($xml_response->createElement('SearchResponse'));
        my $xstatus = $xsearch_response->appendChild($xml_response->createElement('Status'));

        $xstatus->setAttribute('code', $code);
        $xstatus->appendText($err);
    }

    return $xml_response->toString(2);
}

=head2 pager_to_hashref

Convenience method. Turns a L<Data::Page> object into a hash suitable to
put in a JSON response for instance.

This mainly exists due to the lack of such a feature in the Data::Page object.

TODO:
pager_to_hashref should provide absolute URLs in the future for specific contexts

Usage:

  my $page_hash = $this->pager_to_hashref($pager_object);

=cut

{
    my @PAGER_PROPERTIES = qw/total_entries entries_per_page current_page entries_on_this_page
                              first_page last_page
                              first last
                              previous_page
                              next_page
                             /;
    sub pager_to_hashref{
        my ($self, $pager) = @_;
        return { map { $_ , $pager->$_() } @PAGER_PROPERTIES };
    }
}
1;

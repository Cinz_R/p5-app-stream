package App::Stream2::Log::Any::Proxy;

# Modeled on https://metacpan.org/source/DAGOLDEN/Log-Any-1.03/lib/Log/Any/Proxy/Test.pm
# Implement some Mojolicious specific methods that are needed to avoid mojolicious going bang
# in your face.

use base qw/Log::Any::Proxy/;


# Mojolicious required functions:

sub log{ return shift->info(@_); }
sub history{ return[] }


1;

use strict;
use warnings;

package App::Stream2::Api::Boards;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';
use Stream2::FeedTokens;
use List::Util ();
use JSON ();

sub active {
    my $self = shift;

    my @boards = _active_boards_for( $self->current_user )
        or return $self->reply->not_found;

    $self->render(
        json => {
            boards => [ $self->_sort_boards( \@boards ) ],
        },
    );
}

sub _active_boards_for {
    my $user = shift;

    my @boards;
    my $boards_ref = $user->subscriptions_ref;
    my $quota = $user->quota_summary();
    while ( my ($name,$board) = each %$boards_ref ) {

        my $cost_warning = undef;

        # Yes this is a horrible thing, but look at
        # https://www.pivotaltracker.com/story/show/84016314
        if( $name eq 'jobserve_uk' ){
            $cost_warning = 'This will take 1 credit from Jobserve';
        }

        my $disabled = $board->{disabled} || 0;
        if(  ( $ENV{MOJO_MODE} // '' ) eq 'development' ){
            # Disabled is ALWAYS false in development mode
            $disabled = 0;
        }

        push @boards, {
            name            => $name,
            nice_name       => $board->{nice_name} || '',
            type            => $board->{type} || '',
            cv_cost_warning => $cost_warning,
            disabled        => $disabled,
            $quota->{$name} ? ( user_quotas => $quota->{$name} ) : ()
        };
    }

    return @boards;
}

# Returns an array of boards the user is permitted to see. The boards will be
# ordered by the user's UI preference - the order in which they chose to drag
# n drop them in the UI - or, if no such order exists, by board type.
# at the end.
sub _sort_boards {
    my ($self, $allowed_boards) = @_;

    my $by_name_index = { map{ $_->{name} => $_ } @{$allowed_boards} };

    # add the sort_index metric to each board
    my $i = 0;
    do { 
        $by_name_index->{$_}->{sort_index} = $i++ if $by_name_index->{$_}
    } for @{$self->_get_boards_by_position()};

    # Sort and push the remaining.
    return _sort_boards_by_type( values %$by_name_index );
}

# Sort by:
#   Internal first always
#   User preference
#   Board type
#   Nice name
sub _sort_boards_by_type {
    my %type_priority = (
        internal    => 1,
        external    => 2,
        social      => 3,
        public      => 4,
    );

    return sort {
        # Internal boards are ALWAYS smaller than anything else.
        # And anything else is always greater than talentsearch.
        foreach my $internal_board ( qw/adcresponses talentsearch cb_mysupply candidatesearch/ ){
            if ( $a->{name} eq $internal_board ){ return -1; }
            if ( $b->{name} eq $internal_board ){ return 1; }
        }

        return ($a->{sort_index} // 100) <=> ($b->{sort_index} // 100)
          || $type_priority{$a->{type}} <=> $type_priority{$b->{type}}
          || $a->{nice_name} cmp $b->{nice_name}
    } @_;
}

# Returns an array of board names in the user's preferred order. A board's
# position is where the user has spatially dragged it in relation to other
# boards they subscribe to.
sub _get_boards_by_position {
    my ( $self ) = @_;

    my $user        = $self->current_user;
    my $ui_settings = $user->ui_settings;

    return $ui_settings
        ? JSON::decode_json( $ui_settings )->{boards}
        : [];
}

=head2 set_positions

Handles POST /api/boards/set-positions

Sets the position/order of the current user's job board buttons. Saves this
order to the cv_users.ui_settings column. Receives POSTed JSON like this:

    { "boards": [ "boardName1", "boardName2", ... ] }

=cut

sub set_positions {
    my ($self) = @_;

    my $boards_str = $self->req->param('q');
    my $user       = $self->current_user;
    $user->ui_settings( $boards_str );
    $user->update;

    # When the Stream2.request() JavaScript function calls an endpoint like
    # this, it requires a JSON response, so we can't return a 204
    #$self->respond_to;      # sends an empty 204/No Content response
    $self->render( json => {}, status => 200 );
}

=head2 board_meta_information

Fetch details and the fields associated with a specified board, requires a user session

Usage:

    GET /api/board/totaljobs?locale=en

Out:

    {
        "data": {
            "name": "totaljobs",
            "nice_name": "Total Jobs",
            "sort_by_token": {
                ...
            },
            "auth_tokens": [
                {
                    ...
                },
                ...
            ],
            "tokens": [
                {
                    ...
                },
                ...
            ]
        }
    }

=cut

sub board_meta_information {
    my ( $self ) = @_;
    my $board_name = $self->param('board_name');
    my $locale = $self->param('locale') || $self->current_user->locale();

    my $board_row = $self->app->stream2->factory('Board')->fetch({ name => $board_name })
        or return $self->render( json => { error => { message => sprintf('board name "%s" not found', $board_name) } }, status => 404 );

    unless ( List::Util::first { $board_name eq $_ } keys %{$self->current_user->subscriptions_ref} ){
        return $self->render( exception => "User has not got a subscription to this board", status => 403 );
    }

    my $feed = Stream2::FeedTokens->new( locale => $locale, board => $board_name );
    my $details_ref = {
        name        => $board_name,
        nice_name   => $board_row->nice_name,
        auth_tokens => $feed->auth_tokens,
        tokens      => $feed->tokens,
    };

    # don't want the sort by token to be sent twice
    if ( my $sort_by_token = $feed->sort_by_token ){
        $details_ref->{sort_by_token} = $sort_by_token;
        $details_ref->{tokens} = [ grep { $_->{Id} ne $sort_by_token->{Id} } @{$details_ref->{tokens}} ];
    }

    return $self->render( json => {
        data => $details_ref
    });
}

1;

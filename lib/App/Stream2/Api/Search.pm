use strict;
use warnings;

package App::Stream2::Api::Search;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';

use Bean::Locale;

use Stream2::Criteria;
use Stream2::FeedTokens;
use Stream2::Results::Store;

# Not just JSON, Mojo::JSON
use Mojo::JSON;
use JSON;

use Carp;

=head2 criteria

Gets the criteria full expanded tokens from the given criteria id

=cut

sub criteria{
    my ($self) = @_;
    my $criteria_id = $self->param('id');

    my $criteria = Stream2::Criteria->new(
                                          id => $criteria_id,
                                          schema => $self->app->stream_schema,
                                         )->load;

    my %response = $criteria->tokens;
    $response{_search_terms} = [ $criteria->search_terms() ];

    return  $self->render( json => { %response } );
}

=head2 create_criteria

Vivifies a criteria ID from the given criteria tokens.

Renders:

=over

=item "id"

The newly created criterias ID

=item "search_url"

Search URL containing the criteria. Pass "board" for
a board specific search url.

=back

=cut

sub create_criteria{
    my ($self) = @_;
    my $json_ref = $self->stash('query_ref');

    my $criteria_hash = $json_ref->{criteria} || confess("No criteria hash given");
    return unless $self->_valid_query( $criteria_hash );

    my $criteria = Stream2::Criteria->from_query( $self->app->stream_schema(), $criteria_hash);

    my $url = $self->url_for('/search/' . $criteria->id() . '/');
    if ( my $destination = $json_ref->{board} ) {
        my $page = $json_ref->{page_id} // 1;
        $url->path("$destination/page/$page");
    }

    my $session_state = $self->app->sessions->state();
    return $self->render(
        json => {
            data => {
                id => $criteria->id(),
                search_url => $session_state->sessionized_url($self, $url, 360)->to_abs()->to_string(),
            }
        }
    );
}

=head2 resume

Same as running a new search except we are given a criteria_id instead of the criteria

=cut

sub resume {
    my $self = shift;

    my $json_ref = $self->stash('query_ref');

    my $watchdog_id = $json_ref->{watchdog_id} // undef;

    # Run a search on it
    my $board = $self->stash('board');
    my $config_ref = { page => $self->stash('page_id') || 1,
                       cb_oneiam_auth_code => $json_ref->{cb_oneiam_auth_code},
                   };

    # Load criteria from ID
    my $criteria = Stream2::Criteria->new(
        id => $self->stash('id'),
        schema => $self->app->stream_schema,
    )->load;

    my %response = (
        criteria_id => $criteria->id(),
        board       => $board,
        page_id     => $config_ref->{page},
    );

    $response{status_id} = $self->run_search($board, $criteria, $config_ref , $watchdog_id);
    $response{criteria} = { $criteria->tokens };

    my $feed = Stream2::FeedTokens->new( locale => $self->current_user->locale(), board => $board );
    $feed->merge_criteria($criteria);

    my $sort_by_token = $feed->sort_by_token;
    my $tokens = $feed->tokens;
    # don't want the sort by token to be sent twice
    if ( $sort_by_token ){
        $response{sort_by_token} = $sort_by_token;
        $tokens = [ grep { $_->{Id} ne $sort_by_token->{Id} } @$tokens ];
    }
    $response{tokens} = $tokens;
    $response{_search_terms} = [ $criteria->search_terms() ];

    $self->render( json => \%response );
}

=head2 run

Runs a search from <to be defined> and returns:

 {
   criteria_id => 'oifoj5809385490ojfo',
   status_id => '5989038oujfoweijfoiwej048084', # The state of the search in the redis storage.
   sort_by_token => To be defined,
   tokens => To be defined
 };

=cut

sub run {
    my $self = shift;
    my $json_ref = $self->stash('query_ref');

    my $query = $json_ref->{criteria} || {}; # Not mandatory?
    my $config_ref = $json_ref->{_searchOptions} || {};

    my $watchdog_id = $json_ref->{watchdog_id} // undef;


    my $board = $json_ref->{board}
        or die "Board is required for search";

    return unless $self->_valid_query( $query );

    my $criteria = Stream2::Criteria->from_query( $self->app->stream_schema(), $query );
    my %response = (
        criteria_id => $criteria->id,
    );
    $response{status_id} = $self->run_search( $board, $criteria, $config_ref , $watchdog_id );

    if ( $config_ref->{include_facets} ) {
        my $feed = Stream2::FeedTokens->new( locale => $self->current_user->locale(), board => $board );
        $feed->merge_criteria($criteria);

        my $sort_by_token = $feed->sort_by_token;
        my $tokens = $feed->tokens;
        # don't want the sort by token to be sent twice
        if ( $sort_by_token ){
            $response{sort_by_token} = $sort_by_token;
            $tokens = [ grep { $_->{Id} ne $sort_by_token->{Id} } @$tokens ];
        }
        $response{tokens} = $tokens;
    }

    $response{_search_terms} = [ $criteria->search_terms() ];

    $self->render( json => \%response );
}

sub run_search {
    my ($self, $board_name, $criteria, $config_ref, $watchdog_id) = @_;

    my $user = $self->current_user;
    my $user_ref = {
        user_id         => $user->id,
        group_identity  => $user->group_identity,
        auth_tokens     => $user->subscriptions_ref->{$board_name}->{auth_tokens} || {},
        locale          => $user->locale || 'en',
        cb_oneiam_auth_code => $config_ref->{cb_oneiam_auth_code},
        browser_tag         => $self->cookie('BrowserTag'),
    };

    my $action_class = 'Stream2::Action::Search';
    my $parameters = {
        criteria_id         => $criteria->id(),
        template_name       => $board_name,
        options             => $config_ref,
        user                => $user_ref,
        original_user_id    => scalar( $self->session('original_user') ),
        ripple_settings     => $user->ripple_settings(),
        remote_ip           => $self->remote_ip(),
        stream2_base_url    => $self->url_for('/')->to_abs()->to_string()
    };

    if ( $watchdog_id ) {
        $parameters->{watchdog_id} = $watchdog_id;
        $action_class = 'Stream2::Action::WatchdogLoad';
    }

    return $self->queue_job(
        queue       => 'Stream2ActionSearch',
        class       => $action_class,
        parameters  => $parameters
    )->guid;
}

sub results {
    my $self = shift;

    my $results_id = $self->param('id')
        or die "Results ID required";
    my $page_id = $self->param('page_id') || 1;
    my $query_ref = $self->stash('query_ref') || {};

    my $results = Stream2::Results::Store->new({ id => $results_id,
                                                 store => $self->app->_redis
                                             });
    unless( $results->fetch() ){
        confess("Result $results_id is GONE");
    }

    my $stream2 = $self->app()->stream2();
    my $current_user = $self->current_user();

    my $result_arr = $results->results();
    my @candidate_ids = map { $_->id } @$result_arr;

    my $candidate_action_ref = $self->app()->stream2()->action_log()->get_all_actions(
        $current_user->id(),
        $current_user->group_identity(),
        \@candidate_ids
    );

    my $candidate_notes_ref = $self->retrieve_notes(\@candidate_ids);
    my $facets = $self->_enrich_facets( $results->facets() );

    my $name_arr = [];

    $stream2->factory('Tag')->enrich_candidates($result_arr, { user => $current_user });

    # Should result be able to be imported?
    my $auth_provider = $self->auth_provider();
    $self->app->log->info("Auth provider is ".$auth_provider);

    # Note: this is necessary but not sufficient to show the 'Save' (import) button. (has_cv is another req.)
    my $can_importcv = undef;
    # In case the current user has got this destination as a subscription, we can calculate this for the whole set of
    # results. Other wise this will be calculated at each result level
    # A case where the user wouldnt have a subscription is when the global destination is longlist for instance.
    if( $current_user->has_subscription($results->destination()) ){
        $can_importcv = $auth_provider->user_can_import_from($current_user, $results->destination()) ? Mojo::JSON->true() : Mojo::JSON->false();
    }

    # Note: this is a necessary but not sufficient to show the 'Download' and 'Forward'. (has_cv is another req.)
    my $can_downloadcv =
      $auth_provider->user_can_download_from($current_user, $results->destination()) ? Mojo::JSON->true() : Mojo::JSON->false();

    ## Horrible hack to output CV Library
    ## Session liberation 1px images (look in the CV library template).
    my $cvlibrary_session_id = undef;
    if( $results->destination() eq 'cvlibrary' ){
        # This thing would have been set against the user by the backend cvlibrary async template job.
        $cvlibrary_session_id = $current_user->get_cached_value('cvlibrary_session_id');
    }


    # Candidate application history
    # foreach email address get a response row and its corresponding advert row
    my $board_details = $current_user->has_subscription($results->destination());

    # REFACTOR ME - we don't want flagging history for the adcresponses feed
    if ( $results->destination() ne 'adcresponses' && ( $results->destination() eq 'longlist'
         || $board_details->{type} eq 'internal' )
       ){
        $auth_provider->flagging_history( $current_user, $result_arr );
    }

    my $now_dt = DateTime->now();

    # In very special instances we have re-created the result body for some boards
    # using handlebars, this means the properties are controlled by the result object
    my $handlebars_template = sprintf('board/result/%s',$results->destination);
    my $hb_file = $self->app->home->rel_file( sprintf('/app/templates/%s.hbs', $handlebars_template) );
    if ( ! -e $hb_file ){
        $handlebars_template = '';
    }

    foreach my $result ( @$result_arr ){
        my $template = "stream/results/body";
        my $result_ref = $result->to_ref({ candidate_action_ref => $candidate_action_ref->{$result->id()} });
        my @notes = map {
            {
                content         => $_->data->{content},
                privacy         => $_->data->{privacy},
                insert_epoch    => $_->insert_datetime->epoch, # let their javascript show the time
                id              => $_->id,
                author_id       => $_->user_id().'', # user_id is a string in javascript
                author          => $_->data->{author} || ''
            }
        } @{$candidate_notes_ref->{$result->id}};

        unless ( $handlebars_template ){
            $result_ref->{html_body} = $self->app->stream2->templates->result_body(
                {
                    L           => sub { Bean::Locale::localise_in( $current_user->locale(), @_ ); },
                    candidate   => $result_ref,
                    ( $cvlibrary_session_id ? ( cvlibrary_session_id => $cvlibrary_session_id ) : () ),
                },
            );
            delete $result_ref->{cv_text};
        }

        $result_ref->{notes} = \@notes;
        $result_ref->{can_importcv} = $can_importcv // # If this is not defined, it means we HAVE to calculate it at individual level.
                                                       # That would happen only for 'virtual' destination (like longlist)
          (  $auth_provider->user_can_import_from( $current_user, $result->destination() )  ? Mojo::JSON->true() : Mojo::JSON->false() );
        $result_ref->{can_downloadcv} = $can_downloadcv;
        $result_ref->{can_paymessage} = $can_downloadcv;

        ## Always a flagging_history please.
        $result_ref->{flagging_history} //= {};

        push @{$name_arr}, $result_ref;
    }

    # Try to keep the results in the order we found them
    my $watchdog_id = $query_ref->{watchdog_id};
    if ( $watchdog_id ) {
        my @tmp_arr = sort {
            ( $b->{time_found} cmp $a->{time_found} ) || ( $b->{candidate_id} cmp $a->{candidate_id} )
        } @{$name_arr};
        $name_arr = \@tmp_arr;
    }

    $self->render(
        json => {
            notices => $results->notices(),
            facets  => $facets,
            results => $name_arr,
            $results->total_results() ? (total_results => $results->total_results()) : (),
        },
    );
}

=head2 _enrich_facets

So we can apply our own attributes to facets returned from the board
At the moment this only applies to tags from talentsearch to which we apply
a 'flavour'

=cut

sub _enrich_facets {
    my ( $self, $facets ) = @_;
    my $stream2 = $self->app()->stream2();
    my $current_user = $self->current_user();

    # Facets are board tokens which are made available only after the search has been completed
    # Tags are a special sort of facet to which we assign a 'flavour' which is stored locally
    # This flavour is currently default or hotlist

    if ( my @tag_facets = grep { $_->{Type} eq 'tag' } @$facets ){
        # For each facet which is of type tag we want to split them into further facets by tag flavour
        foreach my $facet ( @tag_facets ) {
            my $tag_options = {};

            # These are the tags which artirix have
            foreach my $option ( @{$facet->{Options}} ){
                $tag_options->{$option->[1]} = {
                    value   => $option->[1],
                    label   => $option->[0],
                    count   => $option->[2],
                    flavour => 'default'
                }
            }
            my @tag_names = keys %$tag_options;

            $facet->{Options} = {
                default => {
                    # Shouldn't this default be defined somewhere else?
                    Label => $self->__('Tags'),
                    Options => []
                }
            };

            # for each tag, see if we have a record for it and a flavour
            while ( my @tags = splice( @tag_names, 0, 500 ) ){
                my $rs = $stream2->factory('Tag')->search({
                    'me.group_identity' => $current_user->group_identity,
                    'me.tag_name_ci' => { -in => [ @tags ] }
                }, { prefetch => 'flavour' });
                map {
                    if ( my $tag_ref = delete( $tag_options->{$_->tag_name_ci} ) ){
                        my $flavour = 'default';
                        if ( $_->flavour ) {
                            $flavour = $_->flavour->name;
                            $tag_ref->{flavour} = $flavour;

                            # If a tag is of type shortlist, the label shouldn't contain the ID of the shortlist
                            if ( $flavour eq 'shortlist' ){
                                $tag_ref->{label} =~ s/\A.*?;;//;
                            }

                            ## We want to pluralize flavour names. (like Hotlists)
                            $facet->{Options}->{$flavour}->{Label} = ucfirst( $flavour ).'s';
                        }
                        push ( @{$facet->{Options}->{$flavour}->{Options}}, $tag_ref );
                    }
                } $rs->all;
            }

            # These tags exist in artirix but not in search 2 - default them to default flavour
            foreach my $remaining ( values( %$tag_options ) ){
                push ( @{$facet->{Options}->{default}->{Options}}, $remaining );
            }

            my $sort_order = 1;
            foreach my $flavour ( keys %{$facet->{Options}} ) {
                if ( $flavour eq 'default' ) {
                    $facet->{Options}->{$flavour}->{SortOrder} = $sort_order;
                } else {
                    $facet->{Options}->{$flavour}->{SortOrder} = $sort_order++;
                }
            }
        }
    }

    if ( my ($advert_facet) = grep { $_->{Type} eq 'advert' } @$facets ) {
        my $options = $advert_facet->{Options};
        my $grouped_options = {};
        foreach my $option ( @$options ) {
            # a facet normally contains three indexes (label/value/count)
            # you can specify a fourth index to include it's grouping,
            # this code below changes the data structure anyway
            my $group = $option->[3] // 'default';
            my $label = $group eq 'default' ? 'live' : $group;
            $grouped_options->{$group}->{Label} = $self->__(join(' ', map { ucfirst($_) } split(/_/, $label) ));
            push @{$grouped_options->{$group}->{Options}}, {
                label   => $option->[0],
                value   => $option->[1],
                count   => $option->[2],
            };
        }
        $advert_facet->{Options} = $grouped_options;
        $advert_facet->{isSorted} = Mojo::JSON->true();

        my $sort_order = 1;
        foreach my $group ( qw/general_submissions default archived/ ) {
            if ( $advert_facet->{Options}->{$group} ) {
                $advert_facet->{Options}->{$group}->{SortOrder} = $sort_order;
                $sort_order++;
            }
        }
    }

    # we don't want any part of tags or adverts to be translated as they are generated by the user
    if ( my @translatable_facets = grep { $_->{Type} ne 'tag' && $_->{Type} ne 'advert' } @$facets ){
        foreach my $token ( @translatable_facets ) {
            $token->{'Label'} = Bean::Locale::localise_in( $self->current_user->locale, $token->{'Label'} );
            if ( my $options = $token->{'Options'} ){
                map {
                    my $string = $_->[0] // 'undefined';
                    # localise_in is Locale::MakeText::make_text under the hood.
                    # It needs to have [] things that are not proper make text placeholders
                    # to be escaped with ~. See https://broadbean.atlassian.net/browse/SEAR-742
                    $string =~ s/([\[\]])/~$1/g;
                    $_->[0] = Bean::Locale::localise_in( $self->current_user->locale, $string );
                } @{$options};
            }
        }
    }

    return $facets;
}

=head2 _valid_query

    check the search criteria for any immediate errors before sending it to the engine

=cut

sub _valid_query {
    my ( $self, $q ) = @_;
    if ( $q->{salary_from} && $q->{salary_to} && ( $q->{salary_from} > $q->{salary_to} ) ){
        $self->render(
            status => 400,
            json => {
                error => {
                    reason => $self->__( "Salary-from must be lower than salary-to." )
                }
            }
        );
        return 0;
    }

    return 1;
}

=head2 bulk_forward_candidates

Allows the forwarding of candidate details in a single email to selected users/emails

This is meant to be used asynchronously, so it returns only { status_id => 'jobid' }

=cut

sub bulk_forward_candidates {
    my ( $self ) = @_;
    my $query_ref = $self->stash('query_ref');

    # After 60 seconds, respond with an error
    $self->extend_timeout( 60 , sub { return $self->render( json => { error => { message => $self->__("CV Forward has timed out") } }, status => 500 ); } );

    # Separate recipients by 'userid:' and 'email:'
    my $classified_ids = $self->classify_ids( $query_ref->{recipients} // [] );
    my @user_ids = ( @{ $classified_ids->{userid} // []  } , @{ $classified_ids->{''} // [] } );
    my @emails = @{ $classified_ids->{email} // []  };

    my $user = $self->current_user;
    my $destination = $query_ref->{board};
    my $job_user_ref = {
        user_id         => $user->id,
        group_identity  => $user->group_identity,
        auth_tokens     => $user->subscriptions_ref->{$destination}->{auth_tokens} || {},
        locale          => $user->locale(),
    };
    if( $query_ref->{cb_oneiam_auth_code} ) {
        $job_user_ref->{cb_oneiam_auth_code} = $query_ref->{cb_oneiam_auth_code};
    }

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::BulkForwardCandidates',
        parameters => {
            stream2_base_url  => $self->url_for('/')->to_abs()->to_string(),
            results_id        => $self->param('id'),
            candidate_idxs    => $query_ref->{candidate_idxs},
            user              => $job_user_ref,
            ripple_settings   => $self->current_user->ripple_settings(),
            remote_ip         => $self->remote_ip(),
            recipients_emails => \@emails,
            recipients_ids    => \@user_ids,
            message           => $query_ref->{message},
            destination       => $destination
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}


1;

use strict;
use warnings;
package App::Stream2::Api::Adverts;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';

sub search {
    my $self = shift;

    my $list_name = $self->param('list_name');

    my $query_ref = $self->stash('query_ref');

    my $keywords = $query_ref->{'search'};

    my $ui_origin = $query_ref->{'ui_origin'} // '';

    my $grouping = $query_ref->{'group_sandwiches'}; # boolean - group the adverts by something or not

    my $filter_out_archived = $query_ref->{'filter_out_archived'}; # boolean - filter adverts on their visibility

    my $current_user = $self->current_user();

    my $options = {
        list_name       => $list_name,
        keywords        => $keywords,
        limit           => 20,
        group_adverts   => $grouping,
        user_id         => $current_user->provider_id(), # only ever consider the current users adverts
        $filter_out_archived ? ( visible => 1 ) : (), # visible is the database col name
    };

    # https://broadbean.atlassian.net/browse/SEAR-1557
    # admins should be able to view all adverts in shortlists
    if ( $self->original_user() && $ui_origin eq 'shortlist' ) {
        delete $options->{user_id};
    }

    if ( $current_user->settings_values_hash()->{'show-all-companies-adverts'} && $ui_origin eq 'message' ) {
        delete $options->{user_id};
    }
    ## Get the adverts according to the current provider and the current user
    # auth_provider is the same as login_provider
    my $adverts_ref = $self->auth_provider()->get_adverts($current_user, $options);

    my $json_ref;

    if ( $grouping ) {
        my $grouped_adverts = $adverts_ref->{adverts};
        while ( my ($group, $adverts) = each %$grouped_adverts ) {
            $json_ref->{suggestions}->{$group} = $self->_normalize_adverts($list_name, $adverts);
        }
        $json_ref->{groups} = $adverts_ref->{groups};
    }
    else {
        $json_ref->{suggestions} = $self->_normalize_adverts($list_name, $adverts_ref);
    }

    $self->render(
        json => $json_ref,
    );
}

sub previewadvert {
    my $self = shift;
    my $advert_id = $self->param('advert_id')
        or return $self->render( exception => 'no advert id provided' );
    my $board_id = $self->param('board_id') || '999'; # broadbean
    my $list_name = $self->param('list_name');


    my $advert_user = $self->original_user() || $self->current_user();

    my $advert = $self->auth_provider->find_advert($advert_user ,  $advert_id , { list_name => $list_name } )
      or return $self->reply->not_found;

    my $url = $self->auth_provider->advert_preview_url($advert,  $advert_user , { board_id => $board_id });
    unless( $url ){
        return $self->reply->not_found;
    }

    return $self->redirect_to( $url );
}

# takes the adverts returned from a login provider
# and returns an arrayref of the data we want for
# each list option
sub _normalize_adverts {
    my ($self, $list_name, $adverts) = @_;

    my @normalized_adverts = ();

    push @normalized_adverts,
        {  label => $_->{'label'},
            value => $_->{'advert_id'},
            list => $list_name
        }
        for @$adverts;

    return \@normalized_adverts;
}

1;

use strict;
use warnings;

package App::Stream2::Api::Watchdogs;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';

use Stream2::Criteria;
use Stream2::Watchdogs::Results;
use Data::Dumper;

use Carp;

sub list {
    my ( $self ) = @_;

    my $user_id = $self->current_user->id;

    my @subscriptions = $self->app->stream2->stream_schema->resultset('WatchdogSubscription')->search(
        {
            'watchdog.user_id'   => $user_id,
        },
        {
            '+select'   => [
                'watchdog.name',
                'watchdog.criteria_id',
                'watchdog.id',
                'new_result_count',
                'subscription.board',
                'subscription.board_nice_name'
            ],
            '+as'       => [qw/watchdog_name criteria_id watchdog_id new_results board board_nice_name/],
            join        => [qw/watchdog subscription/]
        }
    );

    my $watchdog_ref = {};
    foreach my $sub ( @subscriptions ){

        $watchdog_ref->{$sub->watchdog_id}->{name} //= $sub->get_column('watchdog_name');
        $watchdog_ref->{$sub->watchdog_id}->{id} //= $sub->get_column('watchdog_id') + 0; # Force this to be a numeric ID, not a string.
        $watchdog_ref->{$sub->watchdog_id}->{new_results} //= 0;
        $watchdog_ref->{$sub->watchdog_id}->{new_results} += $sub->get_column('new_results') * 1;

        map {
            $watchdog_ref->{$sub->watchdog_id}->{$_} //= $sub->get_column($_);
        } qw/criteria_id insert_datetime/;

        $watchdog_ref->{$sub->watchdog_id}->{boards} //= [];
        push @{$watchdog_ref->{$sub->watchdog_id}->{boards}}, { 
            name                => $sub->get_column('board'),
            count               => $sub->get_column('new_results') * 1,
            nice_name           => $sub->get_column('board_nice_name'),
            last_run_datetime   => $sub->get_column('last_run_datetime'),
            last_run_error      => $sub->get_column('last_run_error')
        };
    }

    # Make sure subscription-less watchdogs are there too.
    # Note that this query will only yield the subscription-less ones.
    my $all_watchdogs = $self->app()->stream2()->stream_schema()
        ->resultset('Watchdog')->search({ user_id => $user_id,
                                          id => { -not_in => [ keys %{$watchdog_ref} ] }
                                      });
    while( my $wd = $all_watchdogs->next() ){
        my $wd_id = $wd->id();
        $watchdog_ref->{$wd_id} = {
            name => $wd->name(), id => $wd_id + 0,
            new_results => 0,
            criteria_id => $wd->criteria_id(),
            insert_datetime => $wd->insert_datetime().'',
            boards => []
        };
    }

    ## Ensure consistent sorting. Latest first please.
    return $self->render( json => { watchdogs => [ sort { $b->{id} <=> $a->{id} } values %$watchdog_ref ] } );
}


sub create{
    my ($self) = @_;
    my $json_q = $self->stash('query_ref');

    # accept a serialised criteria object instead of a criteria_id
    my $query = $json_q->{criteria} || {};
    my $criteria = Stream2::Criteria->new(
        schema => $self->app->stream_schema(),
    );
    foreach my $field ( keys %$query ) {
        $criteria->add_token($field => $query->{$field});
    }
    $criteria->save();
    my $criteria_id = $criteria->id();



    my $name = $json_q->{name} // confess("Missing name");
    my $boards = $json_q->{boards};
    unless( @$boards ){ confess("Missing at least one board"); }

    my $stream2 = $self->app()->stream2();
    my $user = $self->current_user();

    unless( $user->can_add_watchdog() ){
        return $self->render( json => { error => { message => 'You have reached the maximum number of Watchdogs. Please remove one before adding a new one' } } , status => 400 );
    }

    my $stuff = sub{
        # Ok lets do it.
        # Protect against VERRRRY long names
        if(length($name) > 255 ){
            $name = substr($name, 0 , 252) . '...';
        }

        my $watchdog = $stream2->watchdogs->create({ user_id => $user->id , name => $name , criteria_id => $criteria_id,
                                                     base_url => $self->url_for('/')->to_abs()->to_string()
                                                   });
        foreach my $board ( @$boards ){
            if( my $subscription = $user->subscriptions()->find({ board => $board }) ){
                $watchdog->add_to_watchdog_subscriptions( { subscription => $subscription } );
            }
        }

        return $watchdog;
    };
    my $watchdog =  $stream2->stream_schema->txn_do($stuff);
    # We queue the job AFTER the watchdog was created completely in the DB
    # This is to avoid https://broadbean.atlassian.net/browse/SEAR-862
    $self->queue_job(
        class => 'Stream2::Action::WatchdogRun',
        queue => 'BackgroundJob',
        parameters => {
            watchdog_id =>  $watchdog->id()
        }
    );
    return $self->render(
        json => { 
            message => 'Your Watchdog was successfully created',
            criteria_id => $criteria_id,
            watchdog => {
                name => $watchdog->name(),
                id => $watchdog->id()
            }
        }
    );
}



=head2 delete

Deletes a watchdog.

=cut

sub delete{
    my ($self) = @_;

    my $json_q = $self->stash('query_ref');

    my $wd_id = $json_q->{id} // confess("Missing id in the query");

    $self->current_user()->watchdogs()->find($wd_id)->delete();

    $self->render( json => { message => $self->__('Successfully deleted this watchdog') });
}

=head2 load_one

Load one watchdog on the stash (or return an 404 with an error message)

=cut

sub load_one{
    my ($self) = @_;
    my $json_q = $self->stash('query_ref');
    my $watchdog_id = $self->param('watchdog_id')
        or confess( 'watchdog id required' );

    my $watchdog = $self->current_user()->watchdogs->find( $watchdog_id );

    unless( $watchdog ){
        $self->render( json => { error => { message => 'This watchdog could not be found' } },
                              status => 404 );
        return;
    }

    $self->stash( 'watchdog' => $watchdog );
    1;
}

=head2 clear_all_viewed_results

Clear ALL the viewed results of this watchdog

=cut

sub clear_all_viewed_results{
    my ($self) = @_;
    my $status_id = $self->queue_job(
        queue => 'BackgroundJob',
        class => 'Stream2::Action::ClearWatchdogResults',
        parameters => {
            watchdog_id => $self->stash('watchdog')->id(),
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}


=head2 clear_viewed_results

Clear the viewed results for the given board watchdog subscription.

=cut

sub clear_viewed_results{
    my ($self) = @_;
    my $status_id = $self->queue_job(
        queue => 'BackgroundJob',
        class => 'Stream2::Action::ClearWatchdogResults',
        parameters => {
            watchdog_id => $self->stash('watchdog')->id(),
            board_name => scalar($self->param('board'))
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}

=head edit_criteria_id

Just sets a new criteria ID against the stashed watchdog. If these criteria is not correct
for the watchdog subscriptions, this will scrub the incorrect values from the criteria and
save a correct version.

=cut

sub edit_criteria_id{
    my ($self) = @_;
    my $watchdog = $self->stash('watchdog');

    my $app = $self->app();
    my $stream2 = $app->stream2();

    my $json_q = $self->stash('query_ref');

    my $new_criteria_id = $json_q->{criteria_id} || confess("Missing criteria_id");

    my $new_criteria_please = 0;

    my %candidate_tokens = $stream2->find_criteria($new_criteria_id)->tokens();

    foreach my $subscription ( $watchdog->subscriptions() ){
        $app->log->info("Checking valid criteria for board ".$subscription->board());
        my $template = eval{ $stream2->build_template($subscription->board()); };

        if( my $err = $@ ){
            # Skip unloadable templates..
            $app->log->error("Could not load ".$subscription->board().': '.$err);
            next;
        }

        if( my $invalid_values = $template->invalid_token_values( \%candidate_tokens ) ){
            $new_criteria_please = 1;
            $app->log->warn("Found invalid values for board ".$subscription->board().": ".Dumper($invalid_values));

            ## Scrub those invalid values from the tokens.
            foreach my $invalid ( keys %$invalid_values ){
                $app->log->info("Invalid values for $invalid are: ".Dumper($candidate_tokens{$invalid}));

                my %invalid_map = map{ $_ => 1 } @{ $invalid_values->{$invalid} };

                $candidate_tokens{$invalid} = [ grep{ ! $invalid_map{$_} } @{ $candidate_tokens{$invalid} } ];

                $app->log->info("Valid values for $invalid are now: ".Dumper($candidate_tokens{$invalid}));
            }
        }
    }


    my $criteria = $stream2->find_criteria($new_criteria_id);

    if( $new_criteria_please ){
        $criteria = $criteria->clone(\%candidate_tokens);
        $criteria->save();
        $new_criteria_id = $criteria->id();
        $app->log->warn("Will replace criteria with new criteria id=$new_criteria_id");
    }


    $watchdog->criteria_id($new_criteria_id);
    $watchdog->update();

    return $self->render( json => { message => 'Your Watchdog search criteria were successfully updated' ,
                                    criteria_id => $new_criteria_id,
                                    criteria => { $criteria->tokens() },
                                  } );
}

=head2 edit

Just edits the boards and the name.

Expects a watchdog on the stash

=cut

sub edit {
    my ( $self ) = @_;

    my $user = $self->current_user();
    my $watchdog = $self->stash('watchdog');

    my $json_q = $self->stash('query_ref');

    my @boards = @{$json_q->{boards} || []};

    if ( @boards ) {
        my %board_map = map { $_ => 1 } @boards;
        my $new_subscription_added = 0;

        foreach my $active_sub ( $watchdog->watchdog_subscriptions ) { #WatchdogSubscriptions
            # un-subscribe
            if ( ! defined $board_map{$active_sub->subscription->board} ){
                $watchdog->delete_related( 'watchdog_subscriptions',{
                    subscription_id => $active_sub->subscription_id
                });
            }
            # no change - we already have a subscription
            else {
                delete $board_map{$active_sub->subscription->board};
            }
        }
        # Some new boards
        foreach my $new_sub_board ( keys %board_map ){
            if( my $subscription = $user->subscriptions()->find({ board => $new_sub_board }) ){
                $new_subscription_added = 1;
                $watchdog->add_to_watchdog_subscriptions( { subscription => $subscription } );
            }
        }
        # Only re-initialise if subs have been added
        if ( $new_subscription_added ){
            $self->queue_job(
                class => 'Stream2::Action::WatchdogRun',
                queue => 'BackgroundJob',
                parameters => {
                    watchdog_id =>  $watchdog->id()
                }
            );
        }
    }

    if ( my $name = $json_q->{name} ){

        # Protect against VERRRRY long names
        if(length($name) > 255 ){
            $name = substr($name, 0 , 252) . '...';
        }

        # name must be unique
        if ( $self->_name_already_exists( $name, $watchdog->id ) ) {
            $self->app->log->error(
                "Rename watchdog failed. Name '$name' already exists.");
            $self->render(
                json   => { error => { message => "Watchdog '$name' already exists." } },
                status => 400
            );
            return;
        }

        $watchdog->name( $name );
        $watchdog->update();
    }

    return $self->render( json => { message => 'Your Watchdog was successfully updated' } );
}

# returns true if the current user already has a watchdog named $name
# If a watchdog Id is given this is ignored when searching.
sub _name_already_exists {
    my ( $self, $name, $watchdog_id) = @_;

    return $self->app->stream2
                     ->stream_schema->resultset('Watchdog')->search(
        {
            user_id => $self->current_user->id,
            name    => $name,
            $watchdog_id ? ( -not => { 'id' => $watchdog_id } ) : ()
        },
        {}
    )->count;
}

1;

use strict;
use warnings;
package App::Stream2::Api::Locations;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';

use Log::Any qw/$log/;

sub suggest {
    my $self = shift;

    my $query_ref = $self->stash('query_ref');

    my $search = $query_ref->{'search'};

    # Note that for USA. this HAS to be en_usa (the broadbean version)
    my $lang = $self->current_user()->locale();
    if( $lang eq 'en_US' ){
        $lang = 'en_usa';
    }

    $log->info("Expanding '$search' in '$lang'");

    my (@locations) = eval {
        $self->app->stream2->location_api->safe_search({
                                                        search => $search,
                                                        lang   => $lang,
                                                       }, { expand => [ 'fullpath', 'specific_path' ] });
    };

    my $max_locations = 100;
    if ( scalar(@locations) > $max_locations ) {
      $#locations = $max_locations - 1;
    }

    my $json_ref = [];

    # The description will be diplayed by select2, the text is what will be set in the selected display
    # the id is what will be stored in the field.
    map { push @{$json_ref}, { description => $_->fullpath() , text => $_->specific_path(), id => $_->id() } } @locations;

    $self->render(
        json => {suggestions => $json_ref},
    );
}

1;

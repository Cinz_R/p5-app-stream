package App::Stream2::Api::AsyncJob;

use strict;
use warnings;

use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';

=head2 status

Returns the status of the job which ID is on the stash

=cut

sub status {
    my $self = shift;

    my $job = $self->retrieve_job($self->stash('id'));

    return $self->render( json => { error => { message => $self->__('Sorry this action has expired.') } },
        status => 404 ) unless $job;

    my $result_status = $job->status;
    my $result = $job->result ? $job->result : {};

    $result = $self->_fixup_result_error( $job, $result );

    return $self->render(
        json => { status => $result_status, context => $result },
    );
}

sub _fixup_result_error {
    my $self = shift;
    my ( $job, $result ) = @_;

    if ( my $error = $result->{error} ){
        my $job_id = $job->guid();
        if ( $error =~ /\Q$job_id\E/ ){
            $result->{is_unmanaged} = 1;
        }
    }

    return $result;
}

1;

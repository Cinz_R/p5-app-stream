use strict;
use warnings;

package App::Stream2::Api::Savedsearches;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';

use List::Util ();
use Stream2::Criteria;

use JSON qw//;

=head2

Fetch the saved searches for the current user

Params:

    rows - number of results per page
    page - base 1, rows * page = offset
    prefetch - only availle is "tokens" which will inflate the crtieria associated with the saved search

Usage:

    GET /api/savedsearches?rows=10&page=1&prefetch=tokens

Out:

    {
        "pager":{
           "first":1,
           "last_page":1,
           "current_page":1,
           "entries_per_page":1,
           "last":1,
           "entries_on_this_page":1,
           "next_page":null,
           "previous_page":null,
           "total_entries":1,
           "first_page":1
        },
        "data":{
           "searches":[
              {
                 "insert_datetime":"2016-05-09 16:52:20",
                 "tokens":"{\"cv_updated_within\":[\"3M\"],\"default_jobtype\":[\"\"],\"distance_unit\":[\"miles\"],\"include_unspecified_salaries\":[true],\"keywords\":[\"\"],\"location_within\":[30],\"salary_cur\":[\"GBP\"],\"salary_from\":[\"\"],\"salary_per\":[\"annum\"],\"salary_to\":[\"\"]}",
                 "active":"1",
                 "name":"tttt",
                 "criteria_id":"M0k-U2ZvWMzLSL1t2QW-krFjZV0",
                 "update_datetime":"2016-05-09 16:52:20",
                 "id":"1",
                 "user_id":"18"
              }
           ]
        }
    }

=cut

sub list {
    my $self = shift;
    my $s2 = $self->app->stream2();

    my ( $prefetch_criteria ) = ( List::Util::first { $_ eq 'tokens' } @{$self->every_param('prefetch') // []} )
        || 0;

    my $saved_search_ref = $s2->factory('SavedSearch')->list(
        $self->current_user->id(),
        {
            prefetch_criteria => $prefetch_criteria,
            rows => $self->param('rows') // 100,
            page => $self->param('page') || 1
        }
    );

    $self->render( json => {
        data => {
            searches => [ sort { $b->{insert_datetime} cmp $a->{insert_datetime} } @{$saved_search_ref->{searches}} ]
        },
        pager => $self->pager_to_hashref( $saved_search_ref->{pager} ),
    });
}

sub save {
    my $self = shift;
    my $json_ref = $self->stash('query_ref');

    my $s2 = $self->app->stream2();
    my $saved_searches = $s2->factory('SavedSearch');

    my $criteria = $self->_load_criteria_from_tokens($json_ref->{'criteria'});

    my $name = $json_ref->{'name'} || $self->__('Empty Search');
    my $flavour = $json_ref->{'flavour'} || 'search';

    return $s2->stream_schema->txn_do( sub {

        my $saved_search_ref = {
            user_id     => $self->current_user->id(),
            criteria_id => $criteria->id(),
            active      => 1,
            flavour     => $flavour
        };

        if( my $saved_search = $saved_searches->search( $saved_search_ref )->first() ){
            if( $name ){
                # Update the name
                $saved_search->name($name);
                $saved_search->update();
            }
            return $self->render(json => {
                success     => 1,
                warnings    => 'This search has already been saved!',
                search      => $saved_search->to_raw_hashref()
            });
        }

        my $saved_search = $saved_searches->create(
            {
                %$saved_search_ref,
                criteria    => $criteria,
                name        => $name,
            }
        );
        return $self->render( json => { search => $saved_search->to_raw_hashref() });
    });
}

sub unsave {
    my $self = shift;
    my $json_ref = $self->stash('query_ref');
    my $saved_search_id = $json_ref->{'id'} || die "ID is mandatory";

    my $s2 = $self->app->stream2();

    my $saved_search = $s2->factory('SavedSearch')->find($saved_search_id);

    $saved_search->unsave();

    $self->render( json => { 'success' => 1, criteria_id => $saved_search->criteria_id } );
}

sub _load_criteria_from_tokens {
    my $self = shift;
    my $tokens_ref = shift || {};

    require Stream2::Criteria;

    my $criteria = Stream2::Criteria->new(
        schema => $self->app->stream_schema(),
    );

    foreach my $field ( keys %$tokens_ref ) {
        $criteria->add_token($field => $tokens_ref->{$field});
    }

    $criteria->save();

    return $criteria;
}

1;

use strict;
use warnings;
package App::Stream2::Api::Admin;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';
use Mojo::JSON;
use Mojo::Util;
use Scalar::Util qw();
use List::Util;

use Stream2::Results;
use Data::Page;
use DateTime;
use Stream2::Criteria;
use Stream2::Data::JSON;
use Stream2::FeedTokens;
use Encode;

use Carp;

use Log::Any qw/$log/;

=head2 load_user

Makes sure the current user is an admin (403 if not).

=cut

sub load_user{
    my ($self) = @_;

    my $user = $self->current_user();
    unless( $user->is_overseer() ){
        $self->render( json => { error => { message => 'Only overseers can access this' } },
                       status => 403 );
        return 0; # Unauthorized
    }

    return 1; # Authorized.
}

=head2 get_user_hierarchy

Gets the current user overseen hierarchy

=cut

sub get_user_hierarchy{
    my ($self) = @_;
    return $self->render( json => $self->current_user->overseen_hierarchy() );
}

sub list_group_macros{
    my ($self) = @_;
    my $user = $self->current_user();
    my $query_ref = $self->stash('query_ref');
    my $page = $query_ref->{page} || 1 ;
    my $stream2 = $self->app->stream2();
    # lets protect against stupidity and negative numbers.

    $page = 1 if $page =~ /\D/;
    my $macros  = $stream2->factory('GroupMacro')->search(
        {
            group_identity => $user->groupclient->identity()
        },
        { page => $page , order_by => 'name', rows => 10 }
    );

    return $self->render(
        json => {
            macros => [ map { +{ $_->get_columns() } } $macros->all ],
            pager => $self->pager_to_hashref( $macros->pager ),
        }
    );

}


=head2 get_group_macro

Gets a group_macro according to the given macro_id (in the params)

=cut

sub get_group_macro{
    my ($self) = @_;
    my $stream2 = $self->app->stream2();
    my $macro = $stream2->factory('GroupMacro')->search({ id => $self->param('macro_id'),
                                                          group_identity => $self->current_user->group_identity() })->first();
    unless( $macro ){
        return $self->render( json => { error => { message => 'not found' } }, status => 404 );
    }
    return $self->render( json => { data => { $macro->get_columns() } } );
}

=head2 delete_group_macro

Deletes the targetted macro.

=cut

sub delete_group_macro{
    my ($self) = @_;
    my $stream2 = $self->app->stream2();
    my $query = $self->stash('query_ref');
    my $macro = $stream2->factory('GroupMacro')->search({ id => $self->param('macro_id'),
                                                          group_identity => $self->current_user->group_identity() })->first();
    unless( $macro ){
        return $self->render( json => { error => { message => $self->__('Macro not found') } }, status => 404 );
    }
    $macro->delete();
    return $self->render( json => { message => $self->__('Success') });
}

sub set_group_macro{
    my ($self) = @_;
    my $stream2 = $self->app->stream2();
    my $macro;
    my $query = $self->stash('query_ref');

    my $group_macros = $stream2->factory('GroupMacro')->search({ group_identity => $self->current_user()->group_identity() });

    if( $self->param('macro_id') ){
        $macro = $group_macros->search({ id => $self->param('macro_id')})->first();
        unless( $macro ){
            return $self->render( json => { error => { message => 'not found' } }, status => 404 );
        }
        if( ( $query->{name} ne $macro->name() ) &&
                $group_macros->search({ id => { '<>' => $macro->id() }, name => $query->{name} })->first() ){
            return $self->render( json => { error => { message => $self->__x('Another macro named "{name}" already exists in your account.' , name => $query->{name} ) } }, status => 400 );
        }
        $macro->name( $query->{name} );
        $macro->on_action( $query->{on_action} );
        $macro->lisp_source( $query->{lisp_source} );
        $macro->blockly_xml( $query->{blockly_xml} );
        $macro->update();
    }else{
        if( $group_macros->search({ name => $query->{name} })->first() ){
            return $self->render( json => { error => { message => $self->__x('Another macro named "{name}" already exists in your account.' , name => $query->{name} ) } }, status => 400 );
        }
        $macro = $stream2->factory('GroupMacro')->create({
            group_identity => $self->current_user->group_identity(),
            name => $query->{name},
            on_action => $query->{on_action},
            lisp_source => $query->{lisp_source},
            blockly_xml => $query->{blockly_xml}
        });
    }
    return $self->render( json => { message => $self->__('Success') , macro => { id => $macro->id() * 1 } } );
}

=head2 get_user_settings

Returns a user settings structure for the setting panel.

=cut

sub get_user_settings{
    my ($self) = @_;
    my $setting_id_param = $self->param('setting_id');
    return $self->_get_user_settings( $setting_id_param );
}

=head2 get_user_settings_by_mnemonic

Returns a user settings structure for a given setting mnemonic.

=cut

sub get_user_settings_by_mnemonic{
    my ($self) = @_;
    my $setting_id_param = $self->param('setting_mnemonic');
    return $self->_get_user_settings( { setting_mnemonic => $setting_id_param } );
}

sub _get_user_settings{
    my ($self,$setting_id) = @_;
    my $stream2 = $self->app->stream2();

    my $setting = $stream2->factory('Setting')->find($setting_id);

    my $current_user = $self->current_user();

    my $user_settings = $setting->get_group_identity_values( $current_user->group_identity() , $current_user->provider() );
    # Massage to replace boolean with actual booleans
    $user_settings->{default}->{boolean_value} =  $user_settings->{default}->{boolean_value} ? Mojo::JSON->true() : Mojo::JSON->false();
    foreach my $map ( @{$user_settings->{users_map}} ){
        $map->{value}->{boolean_value} = $map->{value}->{boolean_value} ? Mojo::JSON->true() : Mojo::JSON->false();
    }
    return $self->render( json => $user_settings );
}

=head2 set_user_settings_by_mnemonic

Sets a user settings structure for a given setting id.

=cut

sub set_user_settings {
    my ($self) = @_;
    my $setting_id_param = $self->param('setting_id');
    return $self->_set_user_settings(
        $setting_id_param,
        $self->stash('query_ref')
    );
}

=head2 set_user_settings_by_mnemonic

Sets a user settings structure for a given setting mnemonic.

=cut

sub set_user_settings_by_mnemonic {
    my ($self) = @_;
    my $setting_id_param = $self->param('setting_mnemonic');
    return $self->_set_user_settings(
        { setting_mnemonic => $setting_id_param },
        $self->stash('query_ref')
    );
}


sub _set_user_settings{
    my ($self,$setting_id, $query) = @_;

    my $stream2 = $self->app()->stream2();
    my $setting_id_param = $self->param('setting_id');

    my $setting = $stream2->factory('Setting')->find($setting_id);

    $setting->set_users_value($self->current_user(), $query->{users} , $query->{value}.'' );
    return $self->render( json => { message => $self->__('Success') } );
}



=head2 get_available_settings

=cut

sub get_available_settings{
    my ($self) = @_;
    my @all_settings = $self->app->stream2->factory('Setting')->search({
                                                                       },
                                                                       {
                                                                        order_by => 'setting_mnemonic',
                                                                       })->all();

    my $tr = $self->app->stream2()->translations();

    my %with_non_defaults = ();
    # Select the settings that have a non default value
    $self->app->stream2->stream_schema->storage->dbh_do(sub{
                                                            my ($storage, $dbh) = @_;
                                                            my $sth = $dbh->prepare(q{SELECT DISTINCT(setting_id)
                                                                                      FROM user_setting JOIN cv_users ON user_id = cv_users.id
                                                                                      WHERE group_identity = ?});
                                                            $sth->execute($self->current_user()->group_identity());
                                                            while( my $row = $sth->fetch() ){
                                                                $with_non_defaults{$row->[0]} = 1;
                                                            }
                                                        });

    my @options_to_show = map {
        {
            label      => $tr->__($_->setting_description),
            rich_label => '<span style="'.( $with_non_defaults{$_->id()}  ? " color:orange; font-style: italic" : '' ).'">'.Mojo::Util::xml_escape($tr->__($_->setting_description)).'</span>',
            id         => $_->id(),
            mnemonic   => $_->setting_mnemonic(),
            type       => $_->setting_type(),
            meta_data  => $_->setting_meta_data()
        }
    } @all_settings;

    # disable some options in New Manage Responses view (SEAR-1973):
    if ( $self->session()->{manage_responses_view} ) {
        @options_to_show = grep {
            $_->{mnemonic} !~ /^(?:
                candidate-actions-import-enabled   |
                candidate-actions-update-enabled   |
                behaviour-downloadcv-use_internal  |
                criteria-cv_updated_within-default |
                email-subject-prefix               |
            )$/x
        } @options_to_show;
    }
    else{ # disable in search..
        @options_to_show = grep {
            $_->{mnemonic} !~ /^(?:
                responses-email_subject_prefix   |
            )$/x
        } @options_to_show;
    }
    return $self->render( json => \@options_to_show );
}

=head2 get_usergroupsettings

Gets groupsettings by mnemonic for all users of the group.

=cut

sub get_user_groupsettings{
    my ($self) = @_;
    my $stream2 = $self->app->stream2();
    my $current_user = $self->current_user();

    my $setting = $stream2->factory('Groupsetting',
                                    { group_identity => $current_user->group_identity() }
                                )->find({
                                    group_identity => $current_user->group_identity(),
                                    setting_mnemonic => $self->param('setting_mnemonic')
                                });

    my $user_settings = $setting->get_group_identity_values( $current_user->group_identity() , $current_user->provider() );
    # Massage to replace boolean with actual booleans
    $user_settings->{default}->{boolean_value} =  $user_settings->{default}->{boolean_value} ? Mojo::JSON->true() : Mojo::JSON->false();
    foreach my $map ( @{$user_settings->{users_map}} ){
        $map->{value}->{boolean_value} = $map->{value}->{boolean_value} ? Mojo::JSON->true() : Mojo::JSON->false();
    }
    return $self->render( json => $user_settings );
}

=head2 set_user_groupsettings

Sets a groupsetting by mnemonic. Accepts the same request
as the set_user_settings:

# For instance (from tests):

    $t->post_ok("/api/admin/user_groupsettings/".$setting->setting_mnemonic() => { Accept => 'text/json' },
                form => {
                    q => JSON::encode_json({
                        value => JSON::false, # Set the value to non default.
                        users => [$user->id],
                    }),
                    ts => time
                }
            )->status_is(200)->content_like(qr({"message":"Success"}));

=cut

sub set_user_groupsettings{
    my ($self) = @_;

    my $stream2 = $self->app()->stream2();
    my $current_user = $self->current_user();

    my $setting = $stream2->factory('Groupsetting',
                                    { group_identity => $current_user->group_identity() }
                                )->find({
                                    group_identity => $current_user->group_identity(),
                                    setting_mnemonic => $self->param('setting_mnemonic')
                                });

    my $query = $self->stash('query_ref');
    $setting->set_users_value( $self->current_user(), $query->{users} , $query->{value}.'' );
    return $self->render( json => { message => $self->__('Success') } );
}




=head2 delete_emailtemplate

Deletes the email template given by the template_id (in the path).

Returns:

 { message: ... }

=cut

sub delete_emailtemplate{
    my ($self) = @_;
        my $user = $self->current_user();
    my $id = $self->param('template_id');
    my $stream2 = $self->app->stream2();

    my $template = $stream2->factory('EmailTemplate')->search({ id => $id , group_identity => $user->group_identity()})->first();
    my $name = $template->name();
    $template->delete();
    return $self->render( json => { message => $self->__x('The template {template_name} has been successfully deleted.' , template_name => $name ) } );
}

=head2 _build_emailtemplate_entity

Builds an email Stream2::O::Email email template
from the query_ref (on stash)

=cut

sub _build_emailtemplate_entity{
    my ($self) = @_;
    my $query_ref = $self->stash('query_ref');
    my $bodyHtml = $query_ref->{emailBody} || confess("Missing emailBody");
    my $subject  = $query_ref->{emailSubject} || confess("Missing templateSubject");
    my $bcc      = $query_ref->{emailBcc};
    my $replyto  = $query_ref->{emailReplyto};

    # use Devel::Peek;
    # Dump($bodyHtml);

    # Parse email body as an HTML document. This is to garantee
    # its validity
    my $stream2 = $self->app->stream2();
    my $xbody = $stream2->html_parser()->load_html( string => Encode::encode('UTF-8', $bodyHtml) ,  encoding => 'UTF-8' );
    $xbody->setEncoding('UTF-8');

    return $stream2->factory('Email')->build(
        [
            Type     => 'text/html; charset=UTF-8',
            Encoding => 'quoted-printable',
            Subject  => Encode::encode( 'MIME-Q', $subject ),
            (
                $replyto
                ? ( 'Reply-To' => $replyto )
                : ()
            ),
            ( $bcc ? ( Bcc => $bcc ) : () ),
            Disposition => 'inline',
            Data        => [ $xbody->toStringHTML() ]
        ]
    );


}

#
# Update the template rules against the given template from
# the given templateRules data structure.
#
sub _update_template_rules{
    my ($self, $template, $templateRules) = @_;
    {
        my @existing_rules = $template->rules()->all();
        my %existing_rules = map{ $_->id() => $_ } @existing_rules;
        my @param_rules    = @$templateRules;
        # the param is driving
        foreach my $param_rule ( @param_rules ){
            if( $param_rule->{id} ){
                my $db_rule = delete ( $existing_rules{$param_rule->{id}} ) // confess("Rule is gone from database. Cannot edit");
                $self->app()->log()->info("Updating email template rule ID=".$db_rule->id());
                foreach my $property ( keys %$param_rule ){
                    $db_rule->$property( $param_rule->{$property} );
                    $db_rule->update();
                }
            }else{
                # This is a new rule.
                $self->app()->log()->info("Creating new rule of role ".$param_rule->{role_name});
                $template->add_rule( $param_rule->{role_name}, $param_rule );
            }
        }
        # Delete the existing rules that were not mentioned in the parameters.
        map{
            $self->app()->log()->info("Deleting rule ID= ".$_->id());
            $_->delete();
        } values %existing_rules;
    }
    return $template;
}

=head2 send_emailtemplate_sample

A bit like 'set_email_template', but sends a sample to the users email address.

=cut

sub send_emailtemplate_sample{
    my ($self) = @_;

    my $user = $self->current_user();
    my $id = $self->param('template_id');

    my $recipient = $user->contact_email();
    unless( $recipient ){
        return $self->render( json => { error => { message => $self->__('You do not have a contact email address to send this sample to') }},
                       status => 400 );
    }

    my $stream2 = $self->app->stream2();

    my $stash = {
                 advert_link => 'http://www.example.com/samplead',
                 advert_email => 'sampleadvert@example.com',
                 advert_title => $self->__('Sample Advert Title'),
                 board_nice_name => $self->__('Sample Job Board Name'),
                 candidate_name => $self->__('Sample Candidate Name'),
                 contactname => $self->__('Sample Contact Name'),
                 consultant_email => 'sampleconsultant@example.com'
                };

    # All is cool. Built a Stream2::O::Email, process the sample stash and send it goddammit!
    my $mime = $self->_build_emailtemplate_entity();
    $mime->head()->replace('To', $recipient );
    $mime->head()->add('From', 'noreply@broadbean.net' );

    # We want this to go through our static IP transport.
    $mime->head()->add('X-Stream2-Transport', 'static_transport');

    eval{
        $mime->process_with_stash( $stash );
    };
    if( my $err = $@ ){
        return $self->render( json => { error => { message => $self->__('This template is not valid: ').$err } },
                              status => 400 );
    }

    # Solve the images
    $mime = $mime->resolve_images(sub{
                                      my ($url) = @_;
                                      my $onlineimage = $stream2->factory('OnlineFile')->find($url);
                                      $self->app->log->info("Local file: ".$onlineimage->disk_file());
                                      return { local_file => $onlineimage->disk_file(),
                                               mime_type  => $onlineimage->mime_type()
                                             };
                                  });

    eval { $stream2->send_email( $mime->entity() ) };
    if ($@) {
        my $error = $@;

        # general cover just in case the error is a string.
        $error = $error->message
            if Scalar::Util::blessed $error
            && $error->isa('Stream::EngineException');
        return $self->render(
            json   => { error => { message => $error } },
            status => 400
        );
    }


    return $self->render(
                         json => { message =>
                                   $self->__x('A Sample email was send to {email_recipient}. Check your inbox!', email_recipient => $recipient ) }
                        );
}

=head2 set_emailtemplate

Updates (aka set) the email template (given by the template_id in a URL fragment) with the PUT'd properties.

Returns the same stuff as create.

=cut

sub set_emailtemplate{
    my ($self) = @_;
    my $user = $self->current_user();
    my $id = $self->param('template_id');
    my $query_ref = $self->stash('query_ref');
    my $name = $query_ref->{'templateName'} || confess("Missing templateName");
    my $permission_changes = $query_ref->{'permissionChanges'};
    my $templateRules = $query_ref->{templateRules} || confess("Missing templateRules");

    my $stream2 = $self->app->stream2();

    my $error = '';
    my $stuff = sub{
        # 1 - Build a MIME entity. Note that we go the simple
        # route, which is a single body entity with some headers.


        my $mime_entity = $self->_build_emailtemplate_entity();

        eval{
            my $max_template_size
                = $self->app->config()->{max_email_template_size}
                // 1024 * 1024 * 2;    # default to 2MB

            $mime_entity->entity_size( $max_template_size );

            # Note that we need to clone to validate, cause this has a side effect
            # and we dont want to save the processed template.
            $mime_entity->clone()->process_with_stash( {} );
        };
        if( my $err = $@ ){
            $error = $err;
            return undef;
        }

        # 2 -  Create a row and set the entity against it.
        my $template = $stream2->factory('EmailTemplate')->search({ group_identity => $user->group_identity(),
                                                                    id => $id
                                                                  })->first();
        $template->name( $name );
        $template->mime_entity_object( $mime_entity );
        $template->update();

        # Do the rulez part
        $self->_update_template_rules( $template, $templateRules );

        return $template;
    };
    my $template = $stream2->stream_schema->txn_do($stuff);
    unless( $template ){
        $error = $error->message
            if Scalar::Util::blessed $error
            && $error->isa('Stream::EngineException');
        return $self->render( json => { error => { message => $self->__('This template is not valid: ').$error } },
                              status => 400 );
    }

    my $setting = $template->permission_setting();
    $setting->set_users_value( $user, $permission_changes->{allow} , '1' );
    $setting->set_users_value( $user, $permission_changes->{deny}  , '0' );

    return $self->render(
                         json => { message => $self->__x('Your template "{template_name}" has been updated' , template_name => $name ),
                                   id => $template->id(),
                                 }
                        );
}

=head2 list_emailtemplates

Lists all the email templates accessible to the company

Returns something like:

 {
   emailtemplates: [ { id: .. , name: ... }, ... ],
   pager: { .. usual pager things .. }
 }

=cut

sub list_emailtemplates{
    my ($self) = @_;
    my $user = $self->current_user();
    my $query_ref = $self->stash('query_ref');
    my $page = $query_ref->{page} || 1 ;
    my $stream2 = $self->app->stream2();
    # lets protect against stupidity and negative numbers.

    $page = 1 if $page =~ /[^0-9]/;
    my $templates  = $stream2->factory('EmailTemplate')->search(
        {
            group_identity => $user->groupclient->identity()
        },
        { page => $page , select => [ 'id' , 'name', 'insert_datetime' ] , order_by => 'name', rows => 10 }
    );

    return $self->render(
        json => {
            emailtemplates => [ map { +{ id => $_->id(),
                                         name => $_->name,
                                         insert_datetime => $_->insert_datetime()->iso8601().'Z',
                                         rules_data => [ map{  $_->to_hash() } $_->rules()->all() ],
                                     } } $templates->all() ],
            pager => $self->pager_to_hashref( $templates->pager ),
        }
    );

}

=head2 create_emailtemplate

Creates a new email template from the given params

templateName and emailBody

Returns:

 { message: 'Template ... created ...' , id: .. }

=cut

sub create_emailtemplate{
    my ($self) = @_;
    my $user = $self->current_user();
    my $stream2 = $self->app->stream2();


    my $query_ref = $self->stash('query_ref');
    my $name = $query_ref->{'templateName'} || confess("Missing templateName");
    my $templateRules = $query_ref->{templateRules} || confess("Missing templateRules");

    my $permission_changes = $query_ref->{'permissionChanges'};

    my $error = '';
    my $stuff = sub{
        # 1 - Build a MIME entity. Note that we go the simple
        # route, which is a single body entity with some headers.

        my $mime_entity = $self->_build_emailtemplate_entity();

        eval{
            my $max_template_size
                = $self->app->config()->{max_email_template_size}
                // 1024 * 1024 * 2;    # default to 2MB

            $mime_entity->entity_size( $max_template_size );

            # Note that we need to clone to validate, cause this has a side effect
            # and we dont want to save the processed template.
            $mime_entity->clone()->process_with_stash( {} );
        };
        if( my $err = $@ ){
            $error = $err;
            return undef;
        }

        # Create a row and set the entity against it.
        my $template = eval {
            $stream2->factory('EmailTemplate')->create(
                {   group_identity => $user->group_identity(),
                    name           => $name
                }
            );
        };
        if ( my $err = $@ ) {
            $error = $err;

            # becuase SQLite and MySQL output differnt errors messages for the
            # same issue I have to cover both bases.
            $error
                = $self->__x(
                'You already have a template called "{template_name}".',
                template_name => $name )
                if $error =~ /(?:Duplicate entry|UNIQUE constraint failed)/;

            return undef;
        }


        $template->mime_entity_object( $mime_entity );

        $self->_update_template_rules( $template , $templateRules );
        $template->update();

        return $template;
    };

    my $template = $stream2->stream_schema->txn_do($stuff);
    unless( $template ){
        # general cover just in case the error is a string.
        $error = $error->message
            if Scalar::Util::blessed $error
            && $error->isa('Stream::EngineException');
        return $self->render( json => { error => { message => $self->__('This template is not valid: '). $error } },
                              status => 400 );
    }

    my $setting = $template->permission_setting();
    $setting->set_users_value( $user, $permission_changes->{allow} , 1 );
    $setting->set_users_value( $user, $permission_changes->{deny}  , 0 );



    my $mime_entity = $template->mime_entity_object();

    return $self->render(
                         json => { message => $self->__x('Your new email template "{template_name}" has been created' , template_name => $name ),
                                   id => $template->id(),
                                   templateName => $template->name(),
                                   templateRules => [ map{ $_->to_hash() } $template->rules()->all() ],
                                   emailBody => $mime_entity->one_html_body(),
                                   emailSubject => $mime_entity->one_header_value('Subject'),
                                   emailReplyto => $mime_entity->one_header_value('Reply-To'),
                                   emailBcc     => $mime_entity->one_header_value('Bcc'),
                                 }
                        );
}

=head2 list_savedsearches

List all the savedsearches this user can see as an administrator.

Returns a JSON like:

   {
       "searches":[
          {
             "id":123,
             "name":"Ruby in Sydney",
             "criteria_id":"SURH_L2_Gkp_kLsvfGf5Pn95UPE",
             "user":"Keith Broughton"
          }
       ],
       "pager":{
          "current_page":1,
          "entries_on_this_page":5,
          "entries_per_page":5,
          ....
          "total_entries":14
       }
   }

=cut

sub list_savedsearches {
    my ( $self ) = @_;

    my $user = $self->current_user();

    my @oversees_ids = ( $user->id() );
    if ( my $user_rs = $user->identity_provider()->user_oversees( $user ) ){
        push @oversees_ids, $user_rs->get_column('id')->all();
    }

    my $page = $self->param('page') // 1;
    my $stem = $self->param('textSearch') // '';

    my $stream2 = $self->app()->stream2;
    my $escaped_stem = $stream2->stream_schema->escape_like($stem);
    my $searches = $stream2->factory('SavedSearch')->search(
        {
            'user.group_identity' => $user->group_identity,
            'user.id' => { -in => \@oversees_ids },
            -or => [
                'me.name' => { -like => $escaped_stem.'%' },
                'user.contact_name' => { -like => $escaped_stem.'%' },
                'user.contact_email' => { -like => $escaped_stem.'%' },
            ],
        },
        {
            page => $page,
            rows => 5,
            order_by => [ 'user.contact_name' , 'me.id' ],
            join => [ 'user' ],
            prefetch => [ 'user' ],
        },
    );

    my ( @searches, %searches_by_id );
    while( my $s = $searches->next ) {
        $searches_by_id{$s->id} = {
            name => $s->name,
            id => $s->id,
            criteria_id => $s->criteria_id,
            user => ( $s->user->contact_name || $s->user->contact_email ),
            insert_datetime => $s->get_column('insert_datetime'),
        };

        push @searches, $searches_by_id{$s->id};
    }

    return $self->render(
        json => {
            searches => \@searches,
            pager => $self->pager_to_hashref( $searches->pager ),
        },
    );
}

=head2 list_watchdogs

List all the watchdogs this user can see as an administrator.

Returns a json like:

  {
    watchdogs:
    [
     {
       name: 'blabla',
       id: 1234
     }
    ],
    pager: {

    }
  }

=cut

sub list_watchdogs{
    my ($self) = @_;

    my $user = $self->current_user();

    my @oversees_ids = ( $user->id() );
    if ( my $user_rs = $user->identity_provider()->user_oversees( $user ) ){
        push @oversees_ids, $user_rs->get_column('id')->all();
    }

    my $page = $self->param('page') // 1;
    my $stem = $self->param('textSearch') // '';

    my $stream2 = $self->app()->stream2();
    my $schema = $stream2->stream_schema();

    my $escaped_stem = $schema->escape_like($stem);


    my $watchdogs  = $stream2->factory('Watchdog')->search({ 'user.group_identity' => $user->group_identity,
                                                            'user.id' => { -in => \@oversees_ids },
                                                            'me.active' => 1,
                                                            -or => [
                                                                    'me.name' => { -like => $escaped_stem.'%' },
                                                                    'user.contact_name' => { -like => $escaped_stem.'%' },
                                                                    'user.contact_email' => { -like => $escaped_stem.'%' }
                                                                   ],
                                                           },
                                                           { page => $page,
                                                             rows => 5,
                                                             order_by => [ 'user.contact_name' , 'me.id' ],
                                                             join => [ 'user' ],
                                                             prefetch => [ 'user' ],
                                                           });

    ## Base watchdogs.
    my @watchdogs = ();
    my %watchdogs_byid = ();
    while( my $wd = $watchdogs->next() ){
        $watchdogs_byid{$wd->id()} = {
                                      name => $wd->name(),
                                      id => $wd->id(),
                                      criteria_id => $wd->criteria_id(),
                                      user => ( $wd->user()->contact_name() || $wd->user()->contact_email() )
                                     };

        push @watchdogs , $watchdogs_byid{$wd->id()};
    }


    # ## Watchdogs enrichment with individual subscriptions (with N Results and different statuses)
    my $subscriptions = $stream2->factory('WatchdogSubscription')
      ->search(
               {
                'me.watchdog_id'   => { -in => [ keys %watchdogs_byid ] },
               },
               {
                '+select'   => [
                                'new_result_count',
                                'subscription.board',
                                'subscription.board_nice_name'
                               ],
                '+as'       => [qw/new_results board board_nice_name/],
                join        => [qw/subscription/],
               }
              );

    while( my $sub  = $subscriptions->next() ){
        $watchdogs_byid{ $sub->watchdog_id() }->{boards} //= [];
        push @{ $watchdogs_byid{$sub->watchdog_id}->{boards} },
          {
           name                => $sub->get_column('board'),
           count               => $sub->get_column('new_results') * 1,
           nice_name           => $sub->get_column('board_nice_name'),
           last_run_datetime   => $sub->get_column('last_run_datetime'),
           last_run_error      => $sub->get_column('last_run_error')
          };
    }


    return $self->render( json => { watchdogs => \@watchdogs , pager => $self->pager_to_hashref( $watchdogs->pager() ) });
}


=head2 delete_tag

Deletes a tag (given by tag_name) from the DB and from talent search.

(If such a thing exists).

=cut

sub delete_tag{
    my ($self) = @_;
    my $query = $self->stash('query_ref');
    my $tag_name = $query->{tag_name};
    my $user = $self->current_user();
    my $stream2 = $self->app->stream2();

    my $stuff =
      sub{
          my $tag = $stream2->factory('Tag')->find({
              group_identity => $user->group_identity(),
              tag_name => $tag_name
          }) or return $self->reply->not_found;

          $tag->candidate_tags()->delete();
          $tag->delete();

          if( $user->has_subscription('talentsearch') ){
              $self->queue_job(
                               class => 'Stream2::Action::TalentSearchDeleteTag',
                               queue => 'BackgroundJob',
                               parameters => {
                                              user_id => $user->id(),
                                              tag_name => $tag_name
                                             }
                              );
              return $self->render( json => { message => $self->__x('Your tag "{tag_name}" is now scheduled to be deleted. It might take a little while to delete it from your Talent Search', tag_name => $tag_name) } );
          }

          return $self->render( json => { message => $self->__x('Your tag "{tag_name}" has been deleted.', tag_name => $tag_name)} );
      };
    return $stream2->stream_schema->txn_do($stuff);
}

=head2 get_tag_info

Get extended information about a tag.

Returns something like:

  { tag => { flavour => ...
             label => ...,
             value => ....,
             n_candidates => ...
           }
  }

=cut

sub get_tag_info{
    my ($self) = @_;
    my $query = $self->stash('query_ref');
    my $tag_name = $query->{tag_name};

    my $user = $self->current_user();
    my $stream2 = $self->app->stream2();

    my $tag = $stream2->factory('Tag')->find({
        group_identity => $user->group_identity(),
        tag_name => $tag_name
    }) or return $self->reply->not_found;

    # Calculate N candidates
    # For standard third party candidate, thats
    # easy. When the candidate is in talent search, thats
    # a bit more complex.

    my $third_party_n = $tag->candidate_tags()->search({ candidate_id => { -not_like => 'talentsearch_%' }})->count();
    my $talentsearch_n = 0;
    if( $user->has_subscription('talentsearch') ){
        # Right, we need to make a talent search query.
        my $results_collector = Stream2::Results->new({destination => 'talentsearch'});
        my $ts_feed = $stream2->build_template('talentsearch',
                                               undef,
                                               {company => $user->group_identity(),
                                                user_object => $user
                                               }
                                              );

        $ts_feed->results($results_collector);

        $ts_feed->token_value('applicant_tags' , $tag_name );

        $ts_feed->run_helpers();
        $ts_feed->search({});
        ## Search as run
        $talentsearch_n = $results_collector->total_results();
    }


    $self->render( json =>
                   { tag => { label => $tag->tag_name(),
                              value => $tag->tag_name(),
                              flavour => $tag->flavour ? $tag->flavour->name() : undef,
                              n_candidates => $third_party_n + $talentsearch_n + 0,
                            }
                   });
}

=head2 set_tag_flavour

From

  q={ tag_name => '..'  , flavour_name => '..' }

Sets the tag's flavour (give an empty one to unset it).

Replies with

  { message => 'Some nice message' }

=cut

sub set_tag_flavour{
    my ($self) = @_;
    my $query_ref = $self->stash('query_ref');

    my $user = $self->current_user();

    my $tag_name = $query_ref->{tag_name} // confess("No tag_name given");
    my $flavour_name = $query_ref->{flavour_name} // confess("No flavour_name given");

    my $tag = $self->app->stream2->factory('Tag')->find({
        group_identity => $user->group_identity,
        tag_name => $tag_name
    }) or return $self->reply->not_found;

    $tag->change_flavour( $flavour_name );

    return $self->render( json => { message => 'Successfully update "'.$tag_name.'" as "'.$flavour_name.'"' });
}

=head2 get_tags

From param = page_num and q={ search => ... }

Returns something like:

 { tags => [ { label => ... , value => ... , flavour => ... }, .... ],
   pager => { current_page => .. , first_page => .. , # ETC
 }

=cut

sub get_tags{
    my ($self) = @_;
    my $user = $self->current_user();

    my $page_num = $self->param('page_num');

    my $query_ref = $self->stash('query_ref');

    my $stem = $query_ref->{search} // '';
    my $flavour = $query_ref->{flavour} // '';

    my $tags = $self->app->stream2->factory('Tag')
                ->autocomplete($user->group_identity() , $stem )
                ->search(
                    {
                        $flavour ? ( "flavour.name" => $flavour ) : ()
                    },
                    {
                        page => $page_num,
                        rows => 20,
                        order_by => { -asc => 'tag_name' },
                        prefetch => 'flavour',
                        join => 'flavour'
                    }
                );

    my $pager = $tags->pager();

    $self->render(
                  json => {tags =>  [ map {
                                        +{ label => $_->tag_label() , value => $_->tag_name(), flavour => $_->flavour_name }
                                      }
                                      $tags->all() ],
                           pager => $self->pager_to_hashref($pager)
                          },
                 );
}

sub add_tag {
    my ($self) = @_;
    my $user = $self->current_user();

    my $query_ref = $self->stash('query_ref');

    my $tag_name = $query_ref->{value}
        or $self->render(json => { error => { message => $self->__('Tag value required') } }, status => 400);

    my $tag = $self->app->stream2()->factory('Tag')->find_or_new({
        group_identity => $user->group_identity(),
        tag_name => $tag_name,
        tag_name_ci => $tag_name,
    });

    unless( $tag->in_storage ){
        $tag->insert;
        return $self->render(
            json => { new => Mojo::JSON->true }
        );
    }

    return $self->render(
        json => {
            new => Mojo::JSON->false
        }
    );
}

=head2 list_response_flags

Fetches a list of all adcresponse flags available to
the admin's company

Usage:

    GET /api/admin/response-flags

Out:

    {
        "data": {
            "flags": [
                {
                    "flag_id": 27,
                    "group_identity": "cinzwonderland",
                    "colour_hex_code": "#55ff33",
                    "description": "My shiny flag"
                },
                ...
            ]
        }
    }

=cut

sub list_response_flags {
    my ( $self ) = @_;
    my $flag_factory = $self->app->stream2->factory('AdcresponsesCustomFlag');
    my @flags = $flag_factory->search({
        group_identity => $self->current_user->group_identity
    })->all();

    my $flags = [
        map { {%$_, flag_id => ($_->{flag_id} * 1)} } $flag_factory->standard_flags,
        map { { $_->get_columns, setting_mnemonic => $_->setting_mnemonic, type => 'custom' } } @flags
    ];

    my $stream2   = $self->app->stream2();
    my $templates = $stream2->factory('EmailTemplate')->search(
        {
            group_identity => $self->current_user->group_identity
        },
        { select => [ 'id', 'name', 'insert_datetime' ], order_by => 'name' }
    );

    # check for an assocation between a flagging event and email template
    my $flags_associated_to_email_templates;
    for my $email_template ( $templates->all() ) {
        for my $email_template_rule ( $email_template->rules()->all() ) {
            if ($email_template_rule->role_name eq 'ResponseFlagged') {
                $flags_associated_to_email_templates
                    ->{ $email_template_rule->{flag_id} }++;
            }
        }
    }

    # fill in the missing information
    $flags = [
        map {
            +{
                %{$_},
                $flags_associated_to_email_templates->{$_->{flag_id}} ?
                    (has_template_association => 1) : (),
            }
        } @$flags
      ];

    return $self->render( json => {
        data => {
            flags => $flags
        }
    });
}

=head2 add_response_flag

Allows the addition of new adcresponse flags

Usage:

    POST /api/admin/response-flags
    Content-Type: application/json

    {
        "colour_hex_code": "#55ff33",
        "description": "My shiny flag"
    }

Out:

    {
        "data": { "id": 27 }
    }

=cut

sub add_response_flag {
    my ( $self ) = @_;
    my $query_ref = $self->stash('query_ref');

    # abort flag creation if company already has the maximum allowed number
    my $flags_factory  = $self->app->stream2->factory('AdcresponsesCustomFlag');
    my $group_identity = $self->current_user->group_identity;
    my $company_flags  = $flags_factory->search(
        { group_identity => $group_identity }
    )->count() || 0;

    if ( $company_flags >= $self->app->config()->{custom_flags_limit} ) {

        # num_flags: the "+ 3" includes global Red, Green and Amber
        return $self->render(
            json   => { error => { message => $self->__x("Sorry! {num_flags} are already in use and that's the limit. To add a new flag you'll need to delete one.", num_flags => $company_flags + 3) } },
            status => 400
        );
    }

    my $description = $query_ref->{description}
        or return $self->render( json => { error => { message => $self->__x("description is required") } }, status => 400 );
    my $colour_hex_code = $query_ref->{colour_hex_code}
        or return $self->render( json => { error => { message => $self->__x("colour_hex_code is required") } }, status => 400 );

    my $flag_row = $flags_factory->create({
        group_identity => $group_identity,
        description => $description,
        colour_hex_code => $colour_hex_code
    });

    return $self->render(
        json => {
            data => {
                $flag_row->get_columns,
                setting_mnemonic => $flag_row->setting_mnemonic,
                type => 'custom'
            }
        }
    );
}

=head2 update_response_flag

Update an existing adcresponse flag

Usage:

    PUT /api/admin/response-flags/27
    Content-Type: application/json

    {
        "colour_hex_code": "#55ff33",
        "description": "My shiny flag"
    }

Out:

    {
        "data": { "id": 27 }
    }

=cut

sub update_response_flag {
    my ( $self ) = @_;
    my $query_ref = $self->stash('query_ref');
    my $id = $self->param('id');

    my $description = $query_ref->{description}
        or return $self->render( json => { error => { message => "description is required" } }, status => 400 );
    my $colour_hex_code = $query_ref->{colour_hex_code}
        or return $self->render( json => { error => { message => "colour_hex_code is required" } }, status => 400 );

    my $flag_row = $self->app->stream2->factory('AdcresponsesCustomFlag')->search({
        group_identity => $self->current_user->group_identity,
        flag_id => $id
    })->first();

    unless ( $flag_row ) {
        return $self->render( json => { error => { message => 'not found' } }, status => 404 );
    }

    $flag_row->update({
        description => $description,
        colour_hex_code => $colour_hex_code
    });

    return $self->render(
        json => {
            data => {
                $flag_row->get_columns,
                setting_mnemonic => $flag_row->setting_mnemonic,
                type => 'custom'
            }
        }
    );
}

=head2 previous_searches

    interacts with our error-calendar to provide historical search data
    returns results and facets

=cut

sub previous_searches {
    my ( $self ) = @_;
    my $query_ref = $self->stash('query_ref');
    my $current_page = $query_ref->{'page'} || 1;
    my $per_page = 25;

    # Filters:
    #   company: always!
    #   stream2_base_url: make sure you only see logs from the current environment
    #   watchdog: 0
    #   user_id: selectable facet
    #   destination: " "
    my $filter = eval{ $self->_build_filter(); };
    if( my $err = $@ ){
        return $self->render(
            exception => $err,
            status => 400
        );
    }

    $self->extend_timeout( 60, sub { $self->render( text => $self->__("Getting search history has timed-out") ); } );

    $self->app->stream2->aws_elastic_search_async->search(
        index => 'threads',
        type  => 'stream2search',
        body  => {
            query => $filter,
            size => $per_page,
            from => ( $current_page - 1 ) * 25,
            sort => [
                { time_completed =>  { order =>  "desc" } }
            ],
            facets => {
                user => {
                    terms => {
                        field => "user_stream2_id",
                        size => 1000,
                    },
                    facet_filter => $filter
                },
                destination => {
                    terms => {
                        field => "destination",
                        size => 500,
                    },
                    facet_filter => $filter
                }
            }
        }
    )->then( sub {
        my $json_ref = shift;

        my $return_ref;
        eval{
            $return_ref = $self->_enrich_previous_searches( $json_ref );

            my $total = $json_ref->{hits}->{total};
            my $pager = Data::Page->new( $total, $per_page, $current_page );
            $return_ref->{pager} = $self->pager_to_hashref( $pager );
        };
        if( my $e = $@ ){
            $self->app->log->error( $e );
            return $self->render( json => { error => { message => 'Request failed: '.$e } }, status => 500 );
        }
        return $self->render( json => $return_ref );
    }, sub {
        my $e = shift;
        $self->app->log->error( $e );
        return $self->render( json => { error => { message => 'Request failed: '.$e } }, status => 500 );
    });
}

=head2 _enrich_previous_searches

    given the little data we store per record in our error-calendar
    enrich each result using corresponding information from our DB/templates

    e.g. mapping a board specific selected value to its corresponding label

        itjobboard_industry: 14 -> Industry: Accountancy

=cut

sub _enrich_previous_searches {
    my ( $self, $json_ref ) = @_;

    $self->app()->log()->info("Enriching the results using Database data");

    my $user = $self->current_user;

    # mass fetch criteria based on the hits
    my %seen = ();
    my @ids = grep{ ! $seen{$_}++ }
              map { $_->{_source}->{criteria_id} }
              @{$json_ref->{hits}->{hits}};
    my @criterias = $self->app->stream_schema->resultset('Criteria')->search({ id => { -in => \@ids } })->all;
    my %criteria_map = map {
        $_->id => {
            $_->get_columns,
            tokens => $_->tokens
        };
    } @criterias;

    # mass fetch users based on this hits and facets
    %seen = ();
    @ids = grep{ ! $seen{$_}++ }
           map { $_->{term} }
           @{$json_ref->{facets}->{user}->{terms}};
    my @users = $self->app->stream_schema->resultset('CvUser')->search({ id => { -in => \@ids }, group_identity => $user->group_identity })->all;
    my %user_map = map {
        $_->id => { $_->get_columns };
    } @users;

    my @boards = $self->app->stream_schema->resultset('Board')->search({ name => { -in => [ map { $_->{term} } @{$json_ref->{facets}->{destination}->{terms}} ] } })->all;
    my %board_map = map {
        $_->name => { $_->get_columns };
    } @boards;

    my @results = ();
    my %templates = ();
    foreach my $result_ref ( @{$json_ref->{hits}->{hits}} ){
        my $s = $result_ref->{_source};
        next unless ( defined ( $user_map{$s->{user_stream2_id}} ) );
        next unless ( defined ( $criteria_map{$s->{criteria_id}} ) );

        my $tokens = $criteria_map{$s->{criteria_id}}->{tokens} // {};
        my $destination = $s->{destination};

        # board specific options need to be properly labeled and the selected values need to be swapped
        # out for the corresponding option-value ... complicated but necessary
        my @board_specific = ();
        while ( my ( $key, $value ) = each( %$tokens ) ){
            next unless ( $key =~ m/\A${destination}_/ );

            unless ( $templates{$destination} ){
                my $feed = Stream2::FeedTokens->new( locale => $self->current_user->locale(), board => $destination );
                $templates{$destination} = $feed->tokens;
            }

            my ( $token ) = grep { $_->{Id} eq $key } @{$templates{$destination}};
            if ( $token ){
                my $label = $token->{Label} || $token->{Name};
                $label =~ s/_/ /g;
                $label =~ s/\b(\w)/\U$1/g;

                if ( $token->{Options} ){
                    my $list_values = $self->_extract_list_values( $token, $value );
                    $value = join ( ', ', @$list_values);
                }

                push @board_specific, {
                    id => $key,
                    label => $label,
                    value => $value
                };
            }
        }

        $s->{n_results} ||= 0;
        push @results, {
            user            => {
                id      => $s->{user_stream2_id},
                name    => $user_map{$s->{user_stream2_id}}->{contact_name},
                email   => $user_map{$s->{user_stream2_id}}->{contact_email},
                label   => $user_map{$s->{user_stream2_id}}->{contact_name}
                            || $user_map{$s->{user_stream2_id}}->{contact_email}
                            || $s->{user_stream2_id}
            },
            tokens          => ( $criteria_map{$s->{criteria_id}} // {} ),
            board_specific  => [ @board_specific ],
            %$s,
            nice_name => ( $board_map{$destination}->{nice_name} // $destination ),
            _id => $result_ref->{_id}
        };
    }

    # Enrich the facet labels and remove any invalid facets ( no count )
    my $facets;
    $facets->{user} = [ grep { $_->{count} > 0 && defined( $user_map{$_->{term}} ) } map {
        {
            %$_,
            label => $user_map{$_->{term}}->{contact_name} || $user_map{$_->{term}}->{contact_email} || $_->{term}
        }
    } @{$json_ref->{facets}->{user}->{terms}} ];
    $facets->{destination} = [ grep { $_->{count} > 0 } map {
        {
            %$_,
            label => $board_map{$_->{term}}->{nice_name} // $_->{term}
        }
    } @{$json_ref->{facets}->{destination}->{terms}} ];

    return { results => [@results], facets => $facets };
}

=head2 _build_filter

    extract params from request and translate into an elasticsearch style query

=cut

sub _build_filter {
    my ( $self ) = @_;

    my $query_ref = $self->stash('query_ref');

    # current user should only see users he oversees
    my $user = $self->current_user;
    my @oversees_ids;
    if ( my $user_rs = $user->identity_provider->user_oversees( $user ) ) {
        push @oversees_ids, $user_rs->get_column('id')->all;
    }
    else {
        # let user at least see themselves, as a fallback
        @oversees_ids = ( $user->id );
    }

    # get the searches of one specific user xor those of all the users the
    # current user oversees
    my $user_sub_filter;
    if ( my $user_id = $query_ref->{user_id} ) {
        if ( List::Util::any { $_ == $user_id } @oversees_ids ) {
            $user_sub_filter = { term => { user_stream2_id => $user_id } };
        }
        else {
            die  "You do not have permission to see user\n";
        }
    }
    elsif( ( $user->contact_details->{user_role} // '' ) eq 'SUPERADMIN' ){
        # Super admins just need company filtering
        $log->info("User is superadmin. Just filtering on company");
        $user_sub_filter = { term => { company => $user->group_identity() } };
    }
    else {
        $user_sub_filter = { terms => { user_stream2_id => \@oversees_ids } };
    }

    my $filter = {
        bool => {
            must => [
                {
                    term => {
                        stream2_base_url => $self->url_for('/')->to_abs()
                                                               ->to_string
                    }
                },
                {
                    term => { watchdog => 0 }
                },
                {
                    range => {
                        time_completed => {
                            gte => DateTime->now
                                           ->subtract( months => 3 )->iso8601
                        }
                    }
                },
                $user_sub_filter,
            ]
        }
    };

    if ( my $destination = $query_ref->{destination} ){
        push @{$filter->{bool}->{must}}, { term => { "destination" => $destination } };
    }

    return $filter;
}

sub _extract_list_values {
    my ( $self, $token, $value ) = @_;

    my $values = [ ref( $value ) ? @$value : $value ];
    my $options = $token->{Options};

    if ( $token->{Type} && $token->{Type} eq 'groupedmultilist' ){
        foreach my $sub_option ( @$options ){
            foreach my $val ( @$values ) {
                my ( $val_label ) = grep { $_->[1] eq $val } @{$sub_option->{Options}};
                if ( $val_label ) {
                    $val = $sub_option->{Label} . " - " . $val_label->[0];
                }
            }
        }
    }
    else {
        foreach my $val ( @$values ) {
            my ( $val_label ) = grep { $_->[1] eq $val } @$options;
            if ( $val_label ){
                $val = $val_label->[0];
            }
        }
    }

    return $values;
}

sub get_quota {
    my ( $self ) = @_;
    my $board = $self->param('board')
        or return $self->render( json => { error => { message => $self->__('board is a required parameter') } }, status => 400 );
    my $type = $self->param('type')
        or return $self->render( json => { error => { message => $self->__('type is a required parameter') } }, status => 400 );

    my $path = $self->current_user->oversees_hierarchical_path();
    my $settings = $self->app->stream2->quota_object->fetch_settings(
        {   path => $path,
            board => $board,
            type => $type,
        }
    );

    my %setting_map = map { $_->{path} => $_ } @$settings;
    return $self->render( json => { quota => \%setting_map } );
}

sub set_quota {
    my ( $self ) = @_;
    my $query = $self->stash('query_ref');
    my $current_user = $self->current_user;

    # auth check, shouldnt be able to access other company quota
    foreach my $quota ( @{$query->{quota}} ){
        my $path = $quota->{path};
        $path =~ s/\A\///;
        my ( $company ) = split( /\//, $path );
        unless ( $company eq $current_user->contact_details()->{company} ){
            return $self->render( json => { error => { message => $self->__('You do not have permission to set quotas') } }, status => 401 );
        }
    }

    my $response = $self->app->stream2->quota_object->create(
        $query,
        {   actioned_by => $current_user->id,
            remote_addr => $self->remote_ip()
        }
    );

    return $self->render(json => { success => 1 });
}

=head2 add_flag_rejection_reason

allows for a rejection reason to be added

Usage:

    POST /api/admin/add-flag-rejection-reason
    Content-Type: application/json

    {
        "reason": {"label":"asdasdasd"},
        "flagId": flagId,
    }

Out:

    {
        "reasonId": UUID
    }

=cut

sub add_flag_rejection_reason {
    my ($self) = @_;
    my $query_ref = $self->stash('query_ref');

    my $rejection_reason = $query_ref->{reason}
      or return $self->render(
        json => {
            error => { message => $self->__('A rejection reason is required.') }
        },
        status => 400
      );

    my $rejection_reason_length = length($rejection_reason->{label});
    if ( 0 == $rejection_reason_length || $rejection_reason_length > 50 ) {
        return $self->render(
            json => {
                error => {
                    message => $self->__(
                        'The rejection reason needs to be less than 50 charactors long.')
                }
            },
            status => 400
        );
    }

    # we only allow rejection reasons on red (1) and amber (3) flags
    my $flag_id = $query_ref->{flagId} || '';
    if ( $flag_id != 1 && $flag_id != 3 ) {
        return $self->render(
            json => {
                error => {
                    message => $self->__(
                        'You can only set flag reasons on Red or Amber flags.')
                }
            },
            status => 400
        );
    }

    my $stream2        = $self->app->stream2();
    my $group_identity = $self->current_user->group_identity;

    # $flag is sainitied, as it can only be a 1 or 3.
    my $setting = $stream2->factory(
        'Groupsetting',
        {
            group_identity => $group_identity
        }
      )->find_or_create(
        {
            setting_mnemonic     => 'flag-rejection-' . $flag_id,
            setting_description  => 'flag rejection for '. $flag_id,
            setting_type         => 'string',
            default_string_value => '{"reasons":[]}',
            setting_meta_data    => '{"string_type": "json"}',
            group_identity       => $group_identity,
        }
      );

    # push the reason into the meta data, and update
    my $setting_data = Stream2::Data::JSON->inflate_from_json($setting->default_string_value());
    if ( $setting_data->{reasons} && 10 == scalar @{ $setting_data->{reasons} } ) {
        return $self->render(
            json => {
                error => {
                    message =>
                      $self->__('You have reached the limit rejection reasons.')
                }
            },
            status => 400
        );
    }
    # add a UUID to the reason
    $rejection_reason->{id} = $stream2->uuid->create_str;
    # make sure its actully a string
    $rejection_reason->{label} = $rejection_reason->{label} .'';
    push @{ $setting_data->{reasons} }, $rejection_reason;
    $setting->default_string_value(Stream2::Data::JSON->deflate_to_json($setting_data));
    $setting->update();

    return $self->render( json => { reasonId => $rejection_reason->{id} } );
}

=head2 delete_flag_rejection_reason

allows for a rejection reason to be deleted

Usage:

    POST /api/admin/delete-flag-rejection-reason
    Content-Type: application/json

    {
        "flagId": flagId,
        "reasonId": reasonId,
    }

Out:

    {
        "success": 1
    }

=cut

sub delete_flag_rejection_reason {
   my ($self) = @_;
    my $query_ref = $self->stash('query_ref');

    my $rejection_reason_id = $query_ref->{reasonId}
      or return $self->render(
        json =>
          { error => { message => $self->__('A rejection reason id are required') } },
        status => 400
      );

    # we only allow rejection reasons on red (1) and amber (3) flags
    my $flag_id = $query_ref->{flagId} || '';
    if ( $flag_id != 1 && $flag_id != 3 ) {
        return $self->render(
            json => {
                error => {
                    message => $self->__(
                        'You can only set flag reasons on Red or Amber flags.')
                }
            },
            status => 400
        );
    }

    my $stream2        = $self->app->stream2();
    my $group_identity = $self->current_user->group_identity;

    my $setting = $stream2->factory(
        'Groupsetting',
        {
            group_identity => $group_identity
        }
      )->find(
        {
            setting_mnemonic    => 'flag-rejection-' . $flag_id,
            group_identity      => $group_identity,
        }
      );

    unless ($setting) {
        return $self->render(
            json => {
                error => {
                    message => $self->__(
                        'You can only set flag reasons on Red or Amber flags.')
                }
            },
            status => 400
        );
    }


    my $setting_data = Stream2::Data::JSON->inflate_from_json($setting->default_string_value());
    $setting_data->{reasons} = [grep { $_->{id} ne $rejection_reason_id }
          @{ $setting_data->{reasons} }];
    $setting->default_string_value(Stream2::Data::JSON->deflate_to_json($setting_data));
    $setting->update();

    return $self->render( json => { success => 1 } );
}


1;

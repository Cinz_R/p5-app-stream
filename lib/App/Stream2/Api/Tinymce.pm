use strict;
use warnings;
package App::Stream2::Api::Tinymce;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';
use Carp;
use JSON;

use Encode;
use Text::Aspell;

use File::Slurp qw/read_file write_file/;

=head2 spellcheck

Copied from adc code base, site/ajax/aspell.cgi
Carries out spell checking on provided text, returns suggested alterations.
To be used via the TinyMCE spellcheck plugin

=cut

sub spellcheck {
    my ( $self ) = @_;

    my $content = $self->param('text');
    my $lang = $self->param('lang');

    if ( ! $content ) {
        return $self->render( json => {} );
    }
    if ( ! $lang ) {
        $lang = $self->current_user->locale();
    }

    my $results = {};

    my $speller = Text::Aspell->new;
    $speller->set_option('lang',$lang);
    $speller->set_option('sug-mode','fast');

    # The split here would be on /\s+/ but that doesn't work in Perl 5.8.8!
    # It's fixed in 5.8.9 onwards... This works in 5.8.8 too :)
    foreach my $word ( split ( /[\s\.,-\/#!£$%\^&\*;:{}=\-_`~()]+/, $content ) ) {
        if ( ! $speller->check( $word ) ) {
            my @suggestions = $speller->suggest( $word );
            $results->{$word} = [ map { Encode::decode('UTF-8', $_) } @suggestions ];
        }
    }

    return $self->render( json => $results );

}

=head2 images

list uploaded images available for the current user's group

=cut

sub images {
    my ( $self ) = @_;
    my $user = $self->current_user();
    my $images = $self->app->stream2->factory('GroupFile')->search({
        group_identity => $user->group_identity
    });
    
    my @urls = map { 
        { url => $_->uri, id => $_->id, name => $_->name }
    } 
    sort { # in order of date uploaded
        $a->id <=> $b->id
    }
    grep { # images only
        $_->mime_type =~ m|\Aimage/|i
    } $images->all();

    return $self->render(json => { images => \@urls });
}

=head2 image_upload

accepts image data and stores a record in the DB and the file in S3

=cut

sub image_upload {
    my ( $self ) = @_;
    my $file = $self->req->upload('image')
        or return $self->render( json => { error => { message => $self->__('Image is a required parameter') } }, status => 400 );

    my $content_type = $file->headers->content_type();
    if ( ! $content_type ) { # No content_type
        $self->app->log->errorf( 'File uploaded without content_type: %s', $file->headers->to_string );
        return $self->render( json => { error => { message => $self->__('Your image has failed to upload') } }, status => 400 );
    }
    unless ( $content_type =~ m/\Aimage\//i ) { # File is not an image probably
        $self->app->log->errorf( 'File uploaded is not an image with content_type: %s', $content_type );
        return $self->render( json => { error => { message => $self->__('Invalid file type, only images are acceptable') } }, status => 400 );
    }

    my $filename = $file->filename();
    if ( ! $filename ) { # No filename
        $self->app->log->errorf( 'File uploaded without filename: %s', $file->headers->to_string );
        return $self->render( json => { error => { message => $self->__('Your image has failed to upload') } }, status => 400 );
    }

    my $size = $file->size();
    my $max_upload_size = $self->config->{max_image_upload_size_bytes} || ( 100 * 1024 );
    if( $size > $max_upload_size ){ # File is too big
        $self->app->log->errorf( 'File uploaded exceeds max size ( %s > %s )', $file->size(), $max_upload_size );
        return $self->render( json => { error => { message => $self->__x('Your image size must not exceed {KB}', KB => sprintf('%skb',int( $max_upload_size / 1024 ))) } }, status => 400 );
    }

    my $user = $self->current_user();
    my $file_postfix = 0;
    my $group_file;
    my $new_filename;

    do {
        $new_filename = $new_filename ? $self->_postfix_filename( $filename, $file_postfix ) : $filename;
        $file_postfix++;

        $group_file = $self->app->stream2->factory('GroupFile')->search({
            name            => $new_filename,
            group_identity  => $user->group_identity()
        })->first();
    } while( $group_file );

    $self->app->stream2->factory('GroupFile')->create({
        name            => $new_filename,
        group_identity  => $user->group_identity(),
        content_type    => $content_type,
        content         => $file->slurp()
    });

    return $self->render( json => { message => 'OK' } );
}

=head2 _postfix_filename

if the filename contains an extension then stick the postfix before the extension, otherwise
paste it after

=cut

sub _postfix_filename {
    my ( $self, $filename, $postfix ) = @_;

    if ( $filename =~ m/\.[^\.]+\z/ ){
        $filename =~ s/\.([^\.]+)\z/\-$postfix\.$1/;
        return $filename;
    }
    return sprintf( '%s-%s', $filename, $postfix );
}

=head2

finds the given image ID against the current user and deletes the record from DB and the physical data from S3

=cut

sub delete_image {
    my ( $self ) = @_;
    my $image_id = $self->param('id')
        or return $self->render( json => { error => { message => 'id is a required parameter' } }, status => 400 );
    my $user = $self->current_user();

    my $image = $self->app->stream2->factory('GroupFile')->search({
        group_identity => $user->group_identity,
        id             => $image_id
    })->first();
    if ( $image ) {
        $image->delete();
        return $self->render( text => 'OK' );
    }

    return $self->render( json => { error => { message => 'image not found' } }, status => 404 );
}

1;

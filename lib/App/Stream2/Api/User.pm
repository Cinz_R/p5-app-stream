package App::Stream2::Api::User;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';
use Mojo::JSON;
use Encode ();

use HTML::Entities;

use Scalar::Util;
use Carp;

use DateTime::Format::ISO8601;

sub getcache {
    my $self = shift;
    my $key = $self->param('key');
    $self->app->log->info( $key );
    my $value = $self->current_user->get_cached_value( $key );
    return $self->render( json => { value => $value } );
}

=head2 delete_longlist

Served like DELETE /api/user/longlists/123

Returns a { message => ... }

=cut

sub delete_longlist{
    my ($self) = @_;
    my $user = $self->current_user();
    my $longlist = $self->current_user->longlists()->find($self->param('id'));
    $longlist->delete();
    return $self->render( json => { message  => $self->app->__("Longlist deletion successful") } );
}

=head2 autocomplete_tags

From

  q={ search => '...'  }


Returns a collection of tags like:

   { suggestions => [ { label => ... , value => ... , flavour => ... } ] }

=cut

sub autocomplete_tags{
    my ($self) = @_;
    my $user = $self->current_user();

    my $query_ref = $self->stash('query_ref');

    my $stem = $query_ref->{search} // '';

    my $tags = $self->app->stream2->factory('Tag')->autocomplete($user->group_identity() , $stem )->search(undef, { rows => 100 });

    $self->render(
                  json => {suggestions =>  [ map { +{ label => $_->tag_name() , value => $_->tag_name(),
                                                      flavour => ( $_->flavour ? $_->flavour->name(): undef ) } }
                                             $tags->all() ] },
                 );
}


sub suggest_usertag {
    my ($self)      = @_;
    my $query_ref   = $self->stash('query_ref');
    my $user        = $self->current_user();
    my $search_term = $query_ref->{'search'};

    my $tags = $self->app()->stream2()->factory('Usertag')
      ->autocomplete( $user, $search_term )->search( undef, { rows => 100 , order_by => { -asc => 'tag_name'  } } );
    $self->render(
        json => {
            suggestions => [
                (
                    map {
                        +{
                            id   => $_->tag_name(),
                            text => $_->tag_name()
                         }
                    } $tags->all()
                ),
                $search_term ? {
                    id   => $search_term, # Note that his is the id for select2, which is just the name of the tag.
                    text => $search_term,
                    description => ' - Create New Folder',
                } : (),

            ]
        },
    );
}


=head2 list_emailtemplates

Lists all the email templates accessible to the current user
(so the ones in the same company).

Returns something like:

 {
   emailtemplates: [ { id: .. , name: ... }, ... ],
 }

=cut

sub list_emailtemplates{
    my ($self) = @_;
    my $user = $self->current_user();
    my $query_ref = $self->stash('query_ref');
    my $keywords = $query_ref->{search};
    my $stream2 = $self->app->stream2();

    my $templates  = $stream2->factory('EmailTemplate')->search(
        {
            group_identity => $user->groupclient->identity(),
            ( $keywords ? ( name => { -like => $stream2->stream_schema->escape_like($keywords).'%' } ) : () )
        },
        { select => [ 'id' , 'name', 'insert_datetime' ] , order_by => 'name' }
    );

    my $user_settings = $user->settings_values_hash()->{groupsettings};

    my @templates =
      grep { $user_settings->{ $_->setting_mnemonic } }
      $templates->all;

    # Limit by 10 based on all active user templates
    @templates = splice(@templates, 0, 10);

    return $self->render(
        json => {
            emailtemplates => [ map { +{ id => $_->id() , name => $_->name , insert_datetime => $_->insert_datetime()->iso8601().'Z' } } @templates ],
        }
    );

}

=head2 get_emailtemplate

Retrieves some template data from the
given template_id (in the path).

Returns something like:

 {  id: ..
    templateName: ..
    emailBody: ..
    templateRules: [...]
 }

=cut

sub get_emailtemplate{
    my ($self) = @_;
    my $user = $self->current_user();
    my $id = $self->param('template_id');
    my $stream2 = $self->app->stream2();

    my $template = $stream2->factory('EmailTemplate')->search({ id => $id , group_identity => $user->group_identity()})->first();

    my $entity = $template->mime_entity_object;
    my %response = ( id => $template->id(),
                     templateName => $template->name(),
                     emailBody => $template->one_html_body(),
                     emailReplyto => $entity->one_header_value('Reply-To') // '', # Avoid later chomp warnings
                     emailBcc     => $entity->one_header_value('Bcc') // '',
                     emailSubject => $entity->one_header_value('Subject'),
                     templateRules => [
                         map { $_->to_hash() }
                             $template->rules()->all()
                         ]
                   );

    return $self->render(
                         json => \%response
                        );
}



=head2 autocomplete_colleagues

From:

 q = { search => '...', includeZombies => BOOL}

Autocomplete colleagues by name or email address returns something like:

  { suggestions => [ { id: 'user_id:123' , text: 'Bla Bla' } , { id: 'email:jerome@broadbean.com' , text: 'jerome@broadbean.com'  ] }

Note that the second example (the one with email:..) is only valid when the current user settings (from juice) have
C<can_forward_to_any_email_address>  set to a true value, unless they are in the responses channel.

Zombies will only be included in the returned results if includeZombies is true.

=cut

sub autocomplete_colleagues{
    my ($self) = @_;

    my $user = $self->current_user();
    my $query_ref = $self->stash('query_ref');
    my $stem = $query_ref->{search} // '';
    my $is_responses_channel = $query_ref->{isResponses};

    my $include_zombies = $query_ref->{includeZombies};
    my $exclude_current_user = $query_ref->{excludeCurrentUser};

    my @users = $user->identity_provider()->autocomplete_colleagues( $user, $stem );

    unless ($include_zombies) {
        @users = grep { !$_->is_zombie() } @users;
    }

    if ($exclude_current_user) {
        @users = grep { $_->id ne $user->id } @users;
    }

    my $well_formed_forwarding_email = $user->parse_email_address_for_forwarding( $stem,
        { is_responses_channel => $is_responses_channel } );

    $self->render(
        json => {
            suggestions => [
                (
                    $well_formed_forwarding_email
                    ? (
                        {
                            id   => 'email:' . $well_formed_forwarding_email,
                            text => $stem
                        }
                      )
                    : ()
                ),
                map {
                    +{
                        id   => 'userid:' . $_->id(),
                        text => sprintf( '%s (%s)',
                            ( $_->contact_name() || $_->contact_email() ),
                            $_->contact_email() )
                      }
                } @users
            ]
        }
    );

}

sub share_search_criteria {
    my $self = shift;

    my $json            = $self->stash('query_ref');
    my $recipients_ids  = $json->{recipients};
    my $extra_message   = $json->{message};
    my $search_url      = $json->{searchURL};
    my $app             = $self->app;
    my $stream2         = $app->stream2();



    my $classified_ids = $self->classify_ids( $recipients_ids );
    my @user_ids = (  @{ $classified_ids->{userid} // []  } , @{ $classified_ids->{''} // [] } );
    my @emails = @{ $classified_ids->{email} // []  };


    my @recipients =  $stream2->factory('SearchUser')->search({id => {-in => \@user_ids }})->all;
    my @recipients_emails = map {$_->contact_email()} @recipients;
    push @recipients_emails , @emails;


    die $self->__('No recipients were specified. Please choose at least one email address to share the search criteria with.') . "\n"
        unless @recipients_emails;

    # generate email message (headers and body)
    my $email = eval {
        my $recipients_string = join ', ', @recipients_emails;
        $app->log->info(
            "Share search criteria: recipients: '$recipients_string'");
        $app->log->debug("Shared search criteria URL: $search_url");

        my $user       = $self->current_user;
        my $user_name  = $user->contact_name;
        my $user_email = $user->contact_email;
        my $subject    = "$user_name has shared Candidate Search Criteria "
                       . 'with you';
        my $body = "Check this out:\n\n  $search_url\n\n$extra_message";

        my $container = $stream2->factory('Email')->build(
            [
                From => 'noreply@broadbean.net',
                'Reply-To' => $user_email,
                To         => $recipients_string,
                Type       => 'text/plain; charset=UTF-8',
                Encoding   => 'quoted-printable',
                Subject    => Encode::encode( 'MIME-Q', $subject ),
                Data       => Encode::encode( 'UTF-8', $body ),
                'X-Stream2-Transport' => 'static_transport' , # We want this to go through the static IP transport
            ]
        );


        $container;
    };
    if ( my $exception = $@ ) {
        $app->log->error("Error building email: $exception");
        die $self->__('Failed to build email message') . "\n";
    }

    # send email message
    $app->log->info('Sharing search criteria URL: sending email');
    eval {
        $stream2->send_email( $email );
    };
    if ( my $exception = $@ ) {
        $app->log->error("Error sending email: $exception");
        die $self->__('Failed to send email for search criteria') . "\n";
    }

    return $self->render( json => { message => $self->__('success') } );
}

=head2 autocomplete_keywords

For the current API user, get a list of stored keywords that match the supplied keyword as a prefix.

From

  q={ search => '...'  }


Returns a collection of matched keywords like:

   { suggestions => [ { label => ..., value => ... ] }

=cut

sub autocomplete_keywords{
    my ($self) = @_;
    my $user = $self->current_user();

    my $query_ref = $self->stash('query_ref');

    my $kw = $query_ref->{search} // '';

    $kw =~ s/\s+/ /g;

    my $suggestions = $kw && $kw ne ' ' ? $user->autocomplete_keywords($kw) : [];

    $self->render( json => { suggestions =>  [ map { +{ label => $_, value => $_ } } @$suggestions ] }, );
}

=head2 oversees

Any user can have others working under them, get a list of such users
Will only return users with a 'user_role' of 'USER'

=cut

sub oversees {
    my ($self) = @_;

    my $overseen = [];
    if( $self->session('is_overseer') && $self->current_user()->provider() =~ /^adcourier/ ){
        $overseen = [
            grep { $_->{user_role} eq 'USER' } @{
                $self->app->stream2->user_api->oversees(
                    $self->current_user()->provider_id(),
                    {
                        active => 1,
                        exclude_zombies => !$self->session()->{manage_responses_view}
                    },
                )
            }
        ];
    }

    return $self->render( json => { overseen_users => $overseen } );
}

sub messagedefaults {
    my $self = shift;

    my $query_ref = $self->stash('query_ref');

    # The destination is important to calculate the default subject.
    my $destination = $query_ref->{destination} or die "Missing destination in query";

    my $current_user = $self->current_user();

    my %hash = map {
        $_ => $current_user->get_cached_value( "message-$_" );
    } qw/content subject email/;
    if ( ! defined( $hash{email} ) ){
        $hash{email} = $self->current_user->contact_details->{contact_email};
    }

    # This is just for adcourier for now.
    # Note that this depends on the login provider (aka auth_provider)
    if ( $self->auth_provider->show_advert_preview() ){
        $hash{show_advert_preview} = Mojo::JSON->true();
    }

    $hash{from} = $current_user->contact_email();
    $hash{subject_prefix} = $current_user->subject_prefix_for( $destination );

    return $self->render( json => \%hash );
}

=head2 feature_exclusions

Extracts the feature exclusion settings from the Ripple settings.

=cut

sub feature_exclusions {
    my ( $self ) = @_;

    my %ripple_settings = %{ $self->current_user()->ripple_settings() };

    my @feature_exclusion_settings = grep{ defined $ripple_settings{$_} }
        qw/ no_adcourier_shortlist hide_forward
            hide_download          hide_save
            hide_message           hide_chargeable_message
            hide_favourite         hide_tagging
            hide_save_internal/;

    my %feature_exclusions;
    @feature_exclusions{@feature_exclusion_settings} = @ripple_settings{@feature_exclusion_settings};
    return \%feature_exclusions;
}

=head2 get_custom_lists

Gets the custom_lists part of the ripple settings against the user.

=cut

sub get_custom_lists{
    my ($self) = @_;
    my @custom_lists = @{ $self->current_user->ripple_settings()->{custom_lists} // [] };

    my $iconclass_num=0;
    return $self->render( json => [ map{ +{ icon_class => 'icon-shortlist'. $iconclass_num++  ,  %$_ } } @custom_lists ] );
}

sub is_admin {
    my ($self) = @_;
    return $self->render( json => { is_admin => $self->current_user->is_admin() } );
}

=head2 get_details

Gets the current user details hash.

=cut

sub get_details{
    my ($self) = @_;
    my $user_ref = $self->current_user->to_hash();

    # On load the app gets the user's details, inform the app that this is the user's first time
    unless( $self->cookie('SearchTourist') ){
        $user_ref->{first_time} = 1;
        my $domain = $self->req->url->host;
        my $five_years = 5 * 365 * 86400;
        $self->cookie(SearchTourist => 1, { domain => $domain, path => '/', expires => time + $five_years });
    }

    # On load the app gets the user's details, lets tag the browser
    unless( $self->cookie('BrowserTag') ){
        my $domain = $self->req->url->host;
        # something nice and random
        my $sid = $self->app()->sessions()->sidgen()->generate_sid();
        $self->app->log->info( "Setting BrowserTag $sid" );
        my $five_years = 5 * 365 * 86400;
        $self->cookie(BrowserTag => $sid, { domain => $domain, path => '/', expires => time + $five_years });
    }

    # Load the user long lists
    my $ll_factory = $self->current_user->longlists();
    my @long_lists = $ll_factory->search({} , { order_by => 'name' })->all();
    my $long_lists_limit = $ll_factory->longlists_limit();

    $user_ref->{long_lists} = [ map{ +{  $_->get_columns() } } @long_lists ];
    $user_ref->{long_lists_limit} = $long_lists_limit * 1;
    $user_ref->{currencies} = $self->session()->{currencies};
    $user_ref->{search_mode} = $self->session()->{manage_responses_view} ? "responses" : "search";
    $user_ref->{adcresponse_flags} = $self->current_user->response_flags();
    $user_ref->{ui_config}->{maxWatchdogs} = $self->app->stream2->factory('Watchdog')->watchdogs_limit();

    #
    # Quality control of settings. Avoid
    # L<https://broadbean.atlassian.net/browse/SEAR-1549>
    #
    # We dont want any setting to be a blessed object,
    # as Mojo::JSON is not capable of serializing all flavours
    # of JSON Booleans.
    # See L<https://github.com/kraih/mojo/issues/972>
    #
    my $settings = $user_ref->{settings} // {};
    foreach my $setting ( keys %$settings ){
        my $value = $settings->{$setting};
        if( Scalar::Util::blessed( $value ) ){
            confess("Setting $setting has got a blessed value $value. We cannot count on Mojo::JSON to serialize it correctly");
        }
    }


    return $self->render( json => { details => { %$user_ref,
                                                 original_user => $self->session()->{original_user},
                                                 feature_exclusions => $self->feature_exclusions,
                                               }
                                  });
}

=head2 get_available_response_flags

Returns an array of available response flags for a candidate

Usage:

    GET /api/user/adcresponse_flags
    x-ripple-session-id: xxxyyyyzzzz

Out:

    {
        "data": {
            "adcresponse_flags": [
                {
                    type => 'standard', # or custom
                    flag_id => 7,
                    setting_mnemonic => '....',
                    group_identity => '...',
                    description => 'Candidate is good',
                    colour_hex_code => '#ff00ee'
                },
                ...
            ]
        }
    }

=cut

sub get_available_adcresponse_flags {
    my ( $self ) = @_;
    my $response_flags_ref = $self->current_user->response_flags();
    return $self->render(
        json => {
            data => {
                adcresponse_flags => $response_flags_ref
            }
        }
    );
}

=head2 get_quota_summary

Gets a summary of the quota limits. Provide a board for single context.

Out:

    {
        "careerbuilder": {
            "search-search": 10,
            "search-cv": 5,
            "search-profile": 5
        },
        ...
    }

=cut

sub get_quota_summary {
    my ($self) = @_;
    my $query_ref = $self->stash('query_ref');
    my $board = $query_ref->{board};

    my $quota_summary = $self->current_user->quota_summary($board);
    unless ( $quota_summary ) {
        return $self->render(json => { error => { message => 'No quotas available' } }, status => 404);
    }

    return $self->render(json => { %$quota_summary });
}

=head2 activity_report

Spawns a ReportActivity action in the background.

=cut

sub activity_report{
    my ($self) = @_;

    my $user = $self->current_user();
    my $query = $self->stash('query_ref');

    # What FROM means is <day>T00:00:00
    # What TO   means is <day>T:23:59:59 (for instance up to the 30th of march should be INCLUSIVE of the 30th of march

    my $from_str = $query->{from_str}.'T00:00:00';
    my $to_str   = $query->{to_str}.'T23:59:59';

    my $from_date = DateTime::Format::ISO8601->parse_datetime($from_str);
    my $to_date   = DateTime::Format::ISO8601->parse_datetime($to_str);

    # Those are in floating timezone. We need to fix them in the time zone
    # of the user.
    $from_date->set_time_zone( $user->timezone() || 'UTC' );
    $to_date->set_time_zone( $user->timezone() || 'UTC' );

    # Then transform into UTC for correct selection
    $from_date->set_time_zone('UTC');
    $to_date->set_time_zone('UTC');

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::ReportActivity',
        parameters => {
            from_str => $from_date->iso8601().'Z', # IN Z (UTC timezone)
            to_str   => $to_date->iso8601().'Z',
            user => {
                user_id         => $user->id,
                group_identity  => $user->group_identity,
                locale          => $user->locale(),
            },
            ripple_settings   => $user->ripple_settings(),
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );

}

=head2 actions

From a given user_id, an action type, a board and
max_action_id (excluded) and min_action_id (included),
return all the action rows.

=cut

sub actions{
    my ($self) = @_;
    my $user = $self->current_user();
    my $query = $self->stash('query_ref');

    my $user_id = $query->{user_id};

    # only admins can omit the user_id param. If they do, all users from their
    # company are selected
    my %user_filter;
    my $stream2 = $self->app()->stream2();
    if ( !$user_id ) {
        if ( $user->is_admin ) {
            %user_filter = ( group_identity => $user->group_identity() );
        }
        else {
            return $self->render(
                json => {
                    error => { message => 'A user_id param is required' }
                },
                status => 400
            )
        }
    }
    else {
        my $reported_user = $stream2->factory('SearchUser')->search({ group_identity => $user->group_identity(), id => $user_id })->first();
        unless( $reported_user ){
            return $self->render(json => { error => { message => 'Unauthorized user ID' } }, status => 403);
        }
        %user_filter = ( user_id => $user_id * 1 );
    }

    my $actions_rs = $stream2->factory('CandidateActionLog')->search({
        %user_filter,
        action  => $query->{action},
        destination => $query->{board},
        -and => [ 'me.id' => { '>=' => $query->{min_action_id} * 1 },
                  'me.id' => { '<'  => $query->{max_action_id} * 1 }
              ]
    }, { order_by => { -desc => 'me.id' } });

    my @actions = ();
    while( my $action = $actions_rs->next() ){
        push @actions , {
            $action->get_columns(),
            data => $action->data()
        };
    }
    return $self->render( json => { actions => \@actions } );
}

1;

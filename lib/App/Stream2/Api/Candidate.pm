package App::Stream2::Api::Candidate;
use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';

use Bean::Locale;

use Carp;
use Data::Dumper;
use DateTime;
use Encode;

use File::Slurp;

use HTTP::Response;

use Log::Any qw/$log/;

use MIME::Base64 qw/ decode_base64 /;
use Mojo::JSON;

use HTML::Entities;
use HTML::FormatText;

use Log::Log4perl::NDC;

use Stream2::Results::Result;
use Stream2::O::File;

use URI::Escape;

use JSON;
use List::Util ();


=head2 load_candidate

Loads a candidate, the current_user and the job_user_ref (special user hash structure for do_job)
on the stash.

=cut

sub load_candidate{
    my ($self) = @_;

    # Results ID (the search ID).
    my $results_id = $self->param('results_id');

    # Index of candidate in the result.
    my $candidate_idx = $self->param('candidate_idx');

    # Board to look into for user_ref auth_tokens
    my $board_name = $self->param('board');

    my $candidate = eval{ $self->app()->stream2->find_results($results_id)->find($candidate_idx); };

    unless( $candidate ){
        # 404! do not go further.
        $self->render (
            json => {
                error => {
                    message => "Could not find any candidate for Results:$results_id, index:$candidate_idx",
                }
            },
            status => 404
        );
        return;
    }

    # Put the candidate on the stash.
    $self->stash('candidate' => $candidate);

    # Put the 'job_user_ref' too, we'll need it.
    # By the way, the fact that this extends App::Stream2::Controller::Authenticated
    # means the current user will ALWAYS be there.
    my $user = $self->current_user;

    # We also need the subscription (board) info. This is FROM THE LOADED CANDIDATE
    # and might not be equal to the board from the param, specially when the user
    # is looking at a board that 'mixes' candidates from different sources.
    my $real_board_name = $candidate->destination() || $board_name;
    $self->app()->log->debug("Real board name is '$real_board_name'");
    $self->stash('board_name' , $real_board_name );
    Log::Log4perl::NDC->push( $real_board_name );
    Log::Log4perl::NDC->push( $candidate->id() );
    Log::Log4perl::NDC->push( $candidate->email() );
    Log::Log4perl::NDC->push( $candidate->name() );

    # User ref will be used for any background job. This
    # is why it is called 'job_user_ref' on the stash
    my $user_ref = {
                    user_id         => $user->id,
                    group_identity  => $user->group_identity,
                    # Note that we want the auth_tokens of the real_board_name, not just the board_name
                    auth_tokens     => $user->subscriptions_ref->{$real_board_name}->{auth_tokens} || {},
                    locale          => $user->locale(),
                    browser_tag     => $self->cookie('BrowserTag'),
                   };
    # Also stash the current user, as calling 'current_user' is expensive.
    $self->stash('current_user' , $user );
    $self->stash('job_user_ref' , $user_ref);

    $self->stash('subscription_info' ,  ( $user->subscriptions_ref->{$real_board_name} // {} ) );

    $self->app()->log()->debug("Candidate is loaded");

    1; # It is a succcess.
}

=head2 _with_cv

Download this candidate CV and returns what the first sub returns
in case of success, or a standard error message in case of error.

The response is an HTTP::Response object.

Usage:

  $this->with_cv(sub{
     my ($response) = @_;

     my $content = $response->content; # That's the CV binary.
     my $content_filename = $response->filename;
     my $content_type = $response->content_type || 'text/plain';

     return $self->render(...);
  });

=cut

sub _with_cv{
    my ($self, $success) = @_;

    $success // confess("Missing success callback");

    unless( defined( $self->param('results_id') ) &&
            defined( $self->param('candidate_idx') ) &&
            $self->stash('job_user_ref') ){
        confess("Missing at least one of results_id , candidate_idx or job_user_ref. Are you sure you are calling that from the right place?");
    }

    my $candidate = $self->stash('candidate') // confess("No candidate on the stash");
    my $user_ref = $self->stash('job_user_ref');
    if( $self->param('cb_oneiam_auth_code') ) {
        $user_ref->{cb_oneiam_auth_code} = $self->param('cb_oneiam_auth_code');
    }
    return $self->do_job(
                         class => 'Stream2::Action::DownloadCV',
                         parameters      => {
                                             results_id      => $self->param('results_id'),
                                             candidate_idx   => $self->param('candidate_idx'),
                                             user            => $user_ref,
                                             ripple_settings => $self->current_user->ripple_settings(),
                                             remote_ip       => $self->remote_ip()
                                            }
                        )
      ->then(sub{
                 my ($job) = @_; # This is there thanks to App::Stream2::Plugin::Queue::do_job
                 my $response = $self->parse_response( $job );

                 $self->app->log->info("Got response from parse CV: ".$response);

                 if( my $candidate_email = $response->header('X-Candidate-Email') ){
                     $self->app->log->info("CV download also returned a candidate email = '$candidate_email' . Setting it against the candidate");
                     $candidate->email($candidate_email);
                 }

                 my $res = eval{ &$success($response); };
                 if( my $err = $@ ){
                     return $self->render( json => { error => { message => $err } }, status => 500 );
                 }
                 return $res;
             },
             sub{
                 my $job = shift;
                 return $self->render( json => { error => $job->result->{error} }, status => 500 );
             });
}

=head2 _with_feed_template

Execute (and returns) some code in the context of the feed template.

Usage:

 return $self->_with_feed_template(sub{ my ($template) = @_ ; .. return $self->render.. });

=cut

sub _with_feed_template{
    my ($self, $code) = @_;
    my $user = $self->stash('current_user') // confess("No user on the stash");
    my $board_name = $self->stash('board_name') // confess("No board_name on the stash");

    return $self->app->stream2->with_feed_template( $user, $board_name, $code );
}

sub tag_candidate{
    my ($self) = @_;
    my $query = $self->stash('query_ref');
    my $tag_name = $query->{tag_name} // confess("Missing tag_name in query");

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::TagCandidate',
        parameters => {
            results_id        => $self->param('results_id'),
            candidate_idx     => $self->param('candidate_idx'),
            user              => $self->stash('job_user_ref'),
            ripple_settings   => $self->current_user->ripple_settings(),
            tag_name          => $tag_name
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}

sub untag_candidate{
    my ($self) = @_;
    my $query = $self->stash('query_ref');
    my $tag_name = $query->{'tag_name'} // confess("Tag name required");

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::UntagCandidate',
        parameters => {
            results_id        => $self->param('results_id'),
            candidate_idx     => $self->param('candidate_idx'),
            user              => $self->stash('job_user_ref'),
            ripple_settings   => $self->current_user->ripple_settings(),
            tag_name          => $tag_name
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}

sub set_usertags{
    my ($self) = @_;
    my $candidate = $self->stash('candidate');
    unless( $candidate->destination() eq 'adcresponses' ){
        confess("Can only use usertags on adcresponses candidate");
    }
    my $user = $self->current_user();

    my $query = $self->stash('query_ref');
    my $tag_names = $query->{tag_names} // confess("Missing tag_names param");
    $self->app->log->info("Will set usertags = ".join(', ' , @$tag_names) );

    my $user_tags = $self->app->stream2()->factory('Usertag');

    # Vivify usertag objects.
    my $already_tags = { map{ $_->tag_name() => $_ }
                             $user_tags->search({ user_id => $user->id(),
                                                            tag_name => { -in => $tag_names }
                                                        })->all()
                                                    };
    # Vivify tags not already there.
    foreach my $tag_name ( @$tag_names ){
        if( $already_tags->{$tag_name} ){ next; }
        $already_tags->{$tag_name} = $user_tags->create({ user_id => $user->id(),
                                                          tag_name => $tag_name,
                                                          tag_name_ci => 'Needs to be there. Trigger will fix this'
                                                      });
    }

    # Now is the time to store those tags against this candidate using the feed.
    $self->_with_feed_template(sub{
                                   shift->set_usertags($candidate , [ values %$already_tags ]);
                               });
    return $self->render( json => { message => "My Folders set" });
}


sub shortlist {
    my ($self) = @_;

    my $query = $self->stash('query_ref');

    my $advert_id = $query->{advert_id};
    my $list_name = $query->{list_name};
    my $advert_label = $query->{advert_label};
    my $candidate = $self->stash('candidate');
    my $user = $self->stash('current_user');
    my $app = $self->app();
    my $user_ref = $self->stash('job_user_ref');

    if( $query->{cb_oneiam_auth_code} ) {
        $user_ref->{cb_oneiam_auth_code} = $query->{cb_oneiam_auth_code};
    }
    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::ShortlistCandidate',
        parameters => {
            results_id        => $self->param('results_id'),
            candidate_idx     => $self->param('candidate_idx'),
            user              => $user_ref,
            ripple_settings   => $user->ripple_settings(),
            advert_label      => $advert_label,
            advert_id         => $advert_id,
            list_name         => $list_name,
            original_user_id =>  scalar($self->session('original_user')),
            remote_ip         => $self->remote_ip(),
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}



=head2 import_candidate

AKA 'Save'. This is the 'Save' button action.

=cut

sub import_candidate{
    my ($self) = @_;
    my $query = $self->stash('query_ref');

    my $user = $self->stash('current_user');
    my $user_ref = $self->stash('job_user_ref');

    if( $query->{cb_oneiam_auth_code} ) {
        $user_ref->{cb_oneiam_auth_code} = $query->{cb_oneiam_auth_code};
    }


    # If the user doesn't have a subscription to talentsearch or cb_mysupply, we cannot import them anywhere
    if( $user->ripple_settings()->{login_provider} eq 'adcourier' ){
        # Note that the case of other login providers
        # is already handled by the implementation of 'user_import_candidate' itself
        unless ( $user->has_adcourier_search() ) {
            return $self->render( json => { error => { message => $self->__("You do not have any internal database to import this candidate into") } }, status => 400 );
        }
    }

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::ImportCandidate',
        parameters => {
            results_id        => $self->param('results_id'),
            candidate_idx     => $self->param('candidate_idx'),
            user              => $user_ref,
            ripple_settings   => $user->ripple_settings(),
            remote_ip         => $self->remote_ip(),
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}

sub get_longlist_candidate{
    my ($self) = @_;
    my $candidate = $self->stash('candidate');
    my $query = $self->stash('query_ref');
    my $user  = $self->current_user();

    my $stream2 = $self->app->stream2();

    my $memberships = $stream2->factory('LongListCandidate')->search({ 'longlist.user_id' => $user->id(),
                                                                       candidate_id => $candidate->id(),
                                                                     }, {select => 'longlist_id', # Only select that. That does speed things up cause it gets
                                                                                                  # the data from the index directly
                                                                         join => 'longlist' });
    my $longlist_membership = {};
    while( my $membership = $memberships->next() ){
        $longlist_membership->{$membership->longlist_id()} = Mojo::JSON->true();
    }
    return $self->render( json => { longlist_membership => $longlist_membership  });
}

=head2 longlist_candidate

Implement adding this candidate to the given new longlist name, or toggling a candidate from a longlist.

The given longlist is either given with an 'id' or with a potential 'new_long_list_name'

=cut

sub longlist_candidate{
    my ($self) = @_ ;
    my $candidate = $self->stash('candidate');
    my $query = $self->stash('query_ref');

    my $user = $self->current_user();
    my $should_remove = $query->{remove} // 0;
    my $user_longlists = $user->longlists();

    my $longlist;
    if ( my $long_list_id = $query->{id} ){
        $longlist = $user_longlists->find($long_list_id);
    }
    elsif ( my $new_list_name = $query->{new_long_list_name} ) {
        # Create the long list
        $longlist = $user_longlists->search({ user_id => $user->id() ,  name => $new_list_name })->first()
            || $user_longlists->create({ user_id => $user->id() ,  name => $new_list_name });

        # We shall NEVER remove on creation of a long list.
        $should_remove = 0;
    }
    else {
        confess("Missing new_list_name");
    }

    my $stuff = sub{};
    unless( $should_remove ){
        $stuff = sub{
            # Add the candidate to the Londlist and reply with a nice message.
            $self->log_action( "longlist_add", $candidate ,{ longlist_id => $longlist->id() , longlist_name => $longlist->name() });
            $longlist->add_candidate($candidate);
            return $self->render( json => { message => $self->__x('Candidate has been added to {longlist_name}.' , longlist_name => $longlist->name()),
                                            longlist => { $longlist->get_columns(), id => $longlist->id() * 1 }
                                          } );
        }
    }else{
        $stuff = sub{
            $self->log_action( "longlist_remove", $candidate , { longlist_id => $longlist->id() , longlist_name => $longlist->name() });
            $longlist->candidates()->search({ candidate_id => $candidate->id() })->delete();
            return $self->render( json => { message => $self->__x('Candidate has been removed from {longlist_name}.' , longlist_name => $longlist->name()),
                                            longlist => { $longlist->get_columns(), id => $longlist->id() * 1 }
                                          } );
        }
    }
    return $self->app->stream2->stream_schema->txn_do($stuff);
}

=head2 delegate_candidate

Sends an internal database candidate from one users database to another users database.

=cut

sub delegate_candidate {
    my ($self) = @_;
    my $query_ref = $self->stash('query_ref');
    my $user_ref = $self->stash('job_user_ref');

    # After 60 seconds, respond with an error
    $self->extend_timeout( 60 , sub { return $self->render( json => { error => { message => $self->__("Candidate delegation timed out") } }, status => 500 ); } );

    my $classified_ids = $self->classify_ids([$query_ref->{colleague}->{id}]);
    my $delegate_to_userid = @{$classified_ids->{userid}}[-1];

    unless ( $delegate_to_userid ) {
        return $self->render(json => { error => { message => $self->__('No user provided to delegate this candidate to') } }, status => 500);
    }

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::DelegateCandidate',
        parameters => {
            results_id          => $self->param('results_id'),
            candidate_idx       => $self->param('candidate_idx'),
            user                => $user_ref,
            ripple_settings     => $self->current_user->ripple_settings(),
            remote_ip           => $self->remote_ip(),
            delegate_to_userid  => $delegate_to_userid,
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}

=head2 forward_candidate

Allows the forwarding of candidate details to selected users/emails

This is meant to be used asynchronously, so it returns only { status_id => 'jobid' }

=cut

sub forward_candidate {
    my ( $self ) = @_;
    my $query_ref = $self->stash('query_ref');

    my $user_ref = $self->stash('job_user_ref');
    if( $query_ref->{cb_oneiam_auth_code} ) {
        $user_ref->{cb_oneiam_auth_code} = $query_ref->{cb_oneiam_auth_code};
    }
    # After 60 seconds, respond with an error
    $self->extend_timeout( 60 , sub { return $self->render( json => { error => { message => $self->__("CV Forward has timed out") } }, status => 500 ); } );

    # Separate recipients by 'userid:' and 'email:'
    my $classified_ids = $self->classify_ids( $query_ref->{recipients} // [] );
    my @user_ids = ( @{ $classified_ids->{userid} // []  } , @{ $classified_ids->{''} // [] } );
    my @emails = @{ $classified_ids->{email} // []  };

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::ForwardCandidate',
        parameters => {
            results_id        => $self->param('results_id'),
            candidate_idx     => $self->param('candidate_idx'),
            user              => $user_ref,
            ripple_settings   => $self->current_user->ripple_settings(),
            remote_ip         => $self->remote_ip(),
            recipients_emails => \@emails,
            recipients_ids    => \@user_ids,
            message           => $query_ref->{message},
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}

=head2 downloadprofile

Implements the preview/profile feature

Asynchonously download the Profile, render the HTML using board templates

=cut

sub downloadprofile {
    my $self = shift;
    my $query = $self->stash('query_ref');

    my $user_ref = $self->stash('job_user_ref');
    if( $query->{cb_oneiam_auth_code} ) {
        $user_ref->{cb_oneiam_auth_code} = $query->{cb_oneiam_auth_code};
    }

    # After 60 seconds, respond with an error
    $self->extend_timeout( 60, sub { $self->render( text => $self->__("Profile download has timed out") ); } );
    my $is_frontend = !! $self->tx->req->headers->header('X-Stream2-VERSIONFRONTEND');

    $self->do_job(
        class           => 'Stream2::Action::DownloadProfile',
        parameters      => {
            results_id      => $self->param('results_id'),
            candidate_idx   => $self->param('candidate_idx'),
            user            => $user_ref,
            ripple_settings => $self->current_user->ripple_settings(),
            remote_ip       => $self->remote_ip(),
            # Leave 6 minutes to the backend process to generate URLs. That should be
            # more than enough.
            # Note that we do that only when we are not in the front end,
            # because when we are in the front end, the link to download the CV rendered by the DownloadProfile
            # action just works (tm).
            ( $is_frontend ? () : ( session_token => $self->app()->sessions()->state()->session_token( $self, 360 ) ) )
        }
    )->then(
        sub {
            my $job = shift;
            my $candidate = Stream2::Results::Result->new( $job->result()->{result} );

            my $response_ref = $candidate->to_ref({
                # Send profile-specific candidate attributes
                attributes => $candidate->profile_keys()
            });
            $response_ref->{has_other_attachments_name} ||= $self->__('Attachments');
            $response_ref->{has_other_attachments} ||= 0;
            $response_ref->{has_other_attachments} *= 1;
            $response_ref->{warnings} ||= [];

            # the ember front end has it's own version of the adcresponses profile
            if ( $candidate->destination eq 'adcresponses' && $is_frontend ){
                $response_ref->{profile_body} = $candidate->attr('cv_html');
                $response_ref->{email_body} = $candidate->attr('broadbean_adcresponse_email_body_html');
            }
            else {
                $response_ref->{profile_body} = $self->app->stream2->templates->profile_body(
                    {
                        L         => sub { Bean::Locale::localise_in( $self->current_user->locale(), @_ ); },
                        candidate => $candidate,
                    }
                );
            }

            return $self->render( json => { data => $response_ref } );
        },
        sub {
            my $job = shift;
            my $result = $job->result;
            my $error = $result->{error};

            return $self->render( json => { error => $error }, status => 500 );
        }
    );
}


=head2 downloadcv

Implements the download CV feature.

Asynchronously download the CV, then renders the file to download (mojolicious promise style)

=cut

sub downloadcv {
    my $self = shift;
    # After 60 seconds, respond with an error
    $self->extend_timeout( 60, sub { $self->render( text => $self->__("CV download has timed out") ); } );

    return $self
      ->_with_cv(
                 sub {
                     my ( $response ) = @_;

                     # $.fileDownload needs this cookie set in the iframe to show success
                     $self->cookie(fileDownload => 'true', { path => '/' });
                     return $self->render_file_download( $response );
                 });
}

sub render_file_download {
    my $self = shift;
    my $response = shift;

    my $content_type = $response->content_type || 'text/plain';
    my $content_encoding = $response->content_encoding || '';

    $self->res->headers->from_hash({
        ( $content_encoding ? ( 'Content-Encoding'    => $content_encoding ) : () ),
        'Content-Type'        => $content_type,
        'Content-Disposition' => 'attachment; filename="' . quotemeta($response->filename) . '";'
    });

    return $self->render(
        data => $response->content
    );
}

sub parse_response {
    my $self = shift;
    my $job = shift;

    my $response = HTTP::Response->parse( 
        decode_base64( $job->result->{result} )
    );

    return $response;
}


=head2 application_history

    get info on any applications made to this company's adverts by a candidate

=cut

sub application_history {
    my $self = shift;
    my $candidate = $self->stash('candidate');
    # That depends on the authentication provider.
    my $application_ref = $self->auth_provider->application_history( $self->original_user() || $self->current_user , $candidate );
    return $self->render(
        json => { application_history => $application_ref }
    );
}

=head2 message

send text content via email to a selected candidate
also has some token replacement capabilities, only really used for advert_link
at the moment. The advert link also takes it upon itself to set the From address
as the aplitrak email address so any responses will go into adcourier... which is nice

=cut

sub message {
    my $self = shift;
    my $query = $self->stash('query_ref');

    my $advert_id = $query->{'advertId'};
    my $advert_list = $query->{'advertList'};
    my $email_template_id = $query->{'emailTemplateId'};
    my $email_template_name = $query->{'emailTemplateName'};
    my $user = $self->stash('current_user');
    my $candidate = $self->stash('candidate');

    my $user_ref = $self->stash('job_user_ref');
    if( $query->{cb_oneiam_auth_code} ) {
        $user_ref->{cb_oneiam_auth_code} = $query->{cb_oneiam_auth_code};
    }
    # if the candidate is in NMR they will certainly have an advert
    # already attributed to them, use this in the absence of a selected advert
    if ( $candidate->destination eq 'adcresponses' && !$advert_id ){
        $advert_id = $candidate->attr('broadbean_adcadvert_id');
        $advert_list = 'adc_shortlist';
    }

    my $message_content = $query->{'messageContent'}
        or die "Message content is required\n";

    $user->set_cached_value( "message-content" => $message_content );

    my $message_subject = $query->{'messageSubject'}
        or die "Message subject is required\n";
    $user->set_cached_value( "message-subject" => $message_subject );

    my $subject_prefix = $query->{messageSubjectPrefix} or die "No subject prefix given";

    my $message_email = $query->{'messageEmail'}
      or die "Sender email address is required\n";

    my $message_bcc = $query->{'messageBcc'} // '';

    # Note we only check for blacklisting if the candidate
    # has got an email address.
    if( $candidate->email() ){
        if( $self->app()->stream2->factory('EmailBlacklist')
                ->email_blacklisted($candidate->email(),
                                    {
                                        group_identity => $user->group_identity(),
                                        purpose => 'candidate.message'
                                    })){
            return $self->render( json => { error => { message => $self->__('Sorry, but this candidate has unsubscribed from these emails') } } , status => 400 );
        }
    }

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::MessageCandidate',
        parameters => {
            results_id      => $self->param('results_id'),
            candidate_idx   => $self->param('candidate_idx'),
            user            => $user_ref,
            ripple_settings => $self->current_user->ripple_settings(),
            remote_ip       => $self->remote_ip(),
            advert_id       => $advert_id,
            list_name       => $advert_list,
            message_email   => $message_email,
            message_bcc     => $message_bcc,
            message_content => $message_content,
            message_subject => $message_subject,
            subject_prefix  => $subject_prefix,
            base_url        => $self->url_for('/')->to_abs()->to_string(),
            $email_template_name ? (email_template_name => $email_template_name) : (),
            $email_template_id   ? (email_template_id   => $email_template_id) : (),
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}

sub property {
    my $self = shift;
    my $query = $self->stash('query_ref');
    my $field_name = $self->param('field_name')
        or die "field name required";

    $self->stash('query_ref', { $field_name => $query->{value} });

    return $self->update();
}


sub delete_candidate{
    my ($self) = @_;
    my $query = $self->stash('query_ref');

    my $candidate = $self->stash('candidate');

    my $devuser = $self->current_user()->contact_details()->{contact_devuser};

    unless( $devuser ){
        confess("Do NOT call that as a non devuser (logged into Broadban juice)");
    }


    $self->_with_feed_template(sub{
                                   shift->delete_candidate($self->stash('candidate'));
                                   $self->log_action( "delete", $candidate , { devuser => $devuser,
                                                                               candidate_name => $candidate->name(),
                                                                               candidate_email => $candidate->email()
                                                                             });
                               });
    return $self->render( json => { message => "Candidate ".$candidate->candidate_id()." has been deleted" } );
}

sub phone_candidate{
    my $self = shift;

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class => 'Stream2::Action::PhoneCandidate',
        parameters => {
            results_id        => $self->param('results_id'),
            candidate_idx     => $self->param('candidate_idx'),
            user              => $self->stash('job_user_ref'),
            ripple_settings   => $self->current_user->ripple_settings(),
            remote_ip         => $self->remote_ip(),
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}

=head properties

Another more RESTfull name for update

=cut

sub properties {
    my $self = shift;
    return $self->update();
}

=head2 update

updates multiple properties for a candidate

Usage:

    POST /results/<UUID>/<BOARD>/candidate/<CANDIDATE_IDX>/properties
    Content-Type: application/json
    {
        q:{
            broadbean_adcresponse_rank :1,
            broadbean_adcresponse_rank_reason:"too awesome"
        }
    }

Out:

    {
        "message": "Your change to this candidate has been submitted"
    }

=cut

sub update {
    my $self = shift;
    my $query = $self->stash('query_ref');

    my $candidate = $self->stash('candidate');
    my $user = $self->stash('current_user');

    $self->extend_timeout( 60, sub { $self->render( text => $self->__("Candidate update has timed out") ); } );

    my $status_id = $self->queue_job(
        queue => 'ForegroundJob',
        class           => 'Stream2::Action::UpdateCandidate',
        parameters      => {
            results_id      => $self->param('results_id'),
            candidate_idx   => $self->param('candidate_idx'),
            fields          => $query,
            user            => $self->stash('job_user_ref'),
            ripple_settings => $self->current_user->ripple_settings(),
        }
    )->guid();
    return $self->render( json => { status_id => $status_id } );
}

sub add_note {
    my $self = shift;
    my $query = $self->stash('query_ref');

    my $candidate = $self->stash('candidate');
    my $user = $self->stash('current_user');

    if ( $query->{id} ){
        # note update
        return;
    }

    my $identity = $user->contact_name() || $user->contact_email();
    if ( $identity ) {
        $query->{author} = $identity;
    }

    if( $query->{privacy} eq 'user' ){
        confess("No user level notes from now on please");
    }

    my $stuff = sub{
        my $note_action = $self->log_action( "note", $candidate, $query );

        # The case of a Response. Notes MUST also be added
        # to mapping candidates.
        if( $note_action && (  $candidate->destination() eq 'adcresponses'  ) ){
            my $internal_mapping = $candidate->get_internal_db_ids();
            if( ( $internal_mapping // '' ) eq 'INTERNAL_DBS_PLEASE_WAIT' ){
                $log->info("Will file a Schedule Longstep to try that later on");
                my $stream2 = $self->app()->stream2();
                $stream2->longsteps()->instantiate_process('Stream2::Process::TransferResponseNote',
                                                           {
                                                               stream2 => $stream2
                                                           },
                                                           {
                                                               user_id => $note_action->user_id(),
                                                               adcresponses_id => $candidate->candidate_id(),
                                                               tsimport_id => $candidate->attr('tsimport_id'),
                                                               search_record_id => $note_action->search_record_id(),
                                                               note_data => $note_action->data(),
                                                           }
                                                       );
            }elsif( ref( $internal_mapping // '' ) eq 'HASH' ){
                $log->info("Got internal candidate mapping ".Dumper( $internal_mapping ));
                my @siblings = $candidate->get_internal_siblings( $internal_mapping );
                foreach my $sibling ( @siblings ){
                    $self->log_action("note" ,$sibling, $query);
                }
            }else{
                $log->info("No mapping to internal candidate possible for response ".$candidate->id().
                               " GOT: ".( $internal_mapping // '[UNDEF]' ));
            }
        }

        return $note_action ? $self->render( json => { id => $note_action->id } )
            : $self->render( json => { error => { message => $self->__("adding note failed") } }, status => 500 );
    };

    return $self->app->stream2->stream_schema->txn_do($stuff);
}

=head2 history_log

Retrieves the action log for the current candidate and returns a JSON array of hashes with the following keys mapped to the appropriate values:

=over 4

=item B<action>

The action performed on the candidate (could be one of the following: download, forward, profile, note).

=item B<user>

The actual name of the user who performed the action.

=item B<datetime>

The date and time the action was performed in ISO-8601 format.

=item B<data>

A string containing a JSON object of data related to the action (could be empty depending on the action performed).

=back

Accepts no arguments.

=cut

my %FAIL_ACTIONS = (
                    'profile_fail' => 1,
                    'download_fail' => 1,
                   );

sub history_log {
    my $self = shift;
    my $cand = $self->stash('candidate');
    my $current_user = $self->stash('current_user');

    my @actions = map {+{
                         id       => $_->id(),
                         action   => $_->action,
                         user     => $_->cv_users->contact_name || $_->cv_users->contact_email,
                         datetime => $_->insert_datetime->iso8601 . 'Z',
                         data     => $_->data,
                     }} @{ $self->app()->stream2->action_log->list_all( $current_user->group_identity(),
                                                                        [  $cand->id() ] ) };

    unless( $self->current_user()->contact_details()->{contact_devuser} ){
        # Scrape the failures.
        @actions = grep { ! $FAIL_ACTIONS{ $_->{action} } } @actions;
    }

    return $self->render( json => { history => \@actions });
}


=head profile_history

Returns who has done what for the Candidate profile centric
history.

=cut

sub profile_history{
    my ($self) = @_;
    my $candidate = $self->stash('candidate');
    my $current_user = $self->stash('current_user');

    my $query = $self->stash('query_ref');
    my $after_id = $query->{after_id};

    my @action_logs = $self->app->stream2->factory('CandidateActionLog')->search(
        {
            'me.candidate_id'    => $candidate->id(),
            'me.group_identity'  => $current_user->group_identity(),
            'me.action' => { '<>' => 'profile' },
        },
        {
            order_by => [ { -desc => 'me.id' } ],
            prefetch => "cv_users",
        }
    )->all();

    # This will contain a map of
    # { adc_advert_id => the shortlisting action }
    my %adc_shortlist_advert_ids = map{ $_->adc_advert_id() => $_  } grep { $_->is_adcourier_shortlist() } @action_logs;

    # Lets attach the sent email objects to the appropriate actions.
    my @email_action_ids = map{ $_->id() } grep{ $_->action() =~ /^message/ } @action_logs;

    my %action_emails = ();

    map{ $action_emails{$_->action_log_id()} = $_->to_hash() }
        $self->app->stream2()->factory('SentEmail')->search({
            action_log_id => { -in => \@email_action_ids }
        })->all();

    my @recruiter_actions = map{
        +{
            # This id is like that because front end history items can also be applications. not only actions:
            id        => 'action-'.$_->id(),
            action_id => $_->id(),
            action    => $_->action,
            user      => ( $_->cv_users->id() == $current_user->id() ) ? $self->__('You!') : ( $_->cv_users->contact_name || $_->cv_users->contact_email ),
            datetime  => $_->insert_datetime->iso8601 . 'Z',
            data      => $_->data,
            ( $action_emails{$_->id()} ? ( sent_email => $action_emails{$_->id()} ) : () )
        }
    } @action_logs;

    my @candidate_applications = ();

    # Only internal candidate can have an application history
    # For the right hand side display, this is if'ed via
    # the candidate.isEditable (meaning it's an internal one).
    if( List::Util::any { $candidate->destination eq $_ } $self->app->stream2->factory('Board')->list_internal_databases() ) {

        my @application_history = @{ $self->auth_provider->application_history( $self->original_user() || $self->current_user , $candidate ) };
        # Go through them and enrich with action_source and action_title the ones that
        # are about the same advert as the shortlist actions.
        foreach my $history_item ( @application_history ){
            my $shortlist_action = $adc_shortlist_advert_ids{$history_item->{advert_id}};
            unless( $shortlist_action ){ next; }
            $history_item->{was_shortlisted} = 1;
            $history_item->{action_source} = $shortlist_action->action();
            $history_item->{action_source_action_id} = $shortlist_action->id();
            $history_item->{label} = $self->__('Candidate Shortlisted');
            $history_item->{user} = ( $shortlist_action->cv_users->id() == $current_user->id() )
                ? $self->__('You!') : ( $shortlist_action->cv_users->contact_name || $shortlist_action->cv_users->contact_email );
        }

        @candidate_applications =
            map{+{
                id => 'application-'.$_->{application_time},
                action => 'candidate_application',
                datetime => DateTime->from_epoch( epoch => $_->{application_time} )->iso8601 . 'Z',
                data => $_,
                user => $_->{user} || $candidate->name() || $candidate->email()
            }} @application_history;
    }

    my @actions =( @recruiter_actions , @candidate_applications );
    @actions =  sort{ $b->{datetime} cmp $a->{datetime} } @actions;

    return $self->render( json => { data => { actions => \@actions } });
}

=head email_history

Returns a JSON-ified list of all emails sent by the group (company) of the
current user to the specified candidate

=cut

sub email_history {
    my $self         = shift;
    my $candidate    = $self->stash('candidate');
    my $current_user = $self->stash('current_user');

    my ($DEFAULT_PAGE, $DEFAULT_ITEMS_PER_PAGE) = (1, 10);
    my ($page, $items_per_page) = (
        $self->param('page')           || $DEFAULT_PAGE,
        $self->param('items_per_page') || $DEFAULT_ITEMS_PER_PAGE
    );

    my $rs = $self->app->stream2->factory('SentEmail')->search_rs(
        {
            'user.group_identity'     => $current_user->group_identity,
            'action_log.candidate_id' => $candidate->id,
            'action_log.action'       => 'message'
        },
        {
            join     => ['user', 'action_log'],
            collapse => 1,
            columns  => [ qw/me.id me.sent_date me.subject me.recipient 
                             me.recipient_name user.contact_name/ ],
            order_by => { -desc => ['me.id'] },
            page     => $page,
            rows     => $items_per_page,
        }
    );

    my @email_summaries = map {
        {
            id            => $_->id,
            date          => $_->sent_date =~ tr/T/ /r,
            subject       => $_->subject,
            from          => $_->user->contact_name,
            recipient     => $_->recipient,
            recipientName => $_->recipient_name,
        }
    } $rs->all;
    my $json_hashref = {
        emails => \@email_summaries,
        pager  => $self->pager_to_hashref( $rs->pager )
    };

    $self->render( json => $json_hashref );
}


sub delete_note {
    my $self = shift;

    my $note_id = $self->param('note_id');
    $note_id // die "Note ID is required";

    # When a user deletes a note, change them to the owner and set it as action=delete_note
    my $note = $self->app()->stream2()->stream_schema()->resultset('CandidateActionLog')->find(
        {
            id              => $note_id,
            action          => 'note',
            candidate_id    => $self->stash('candidate')->id(),
            group_identity  => $self->stash('current_user')->group_identity(),
        }
    );
    $note->update({
        user_id         => $self->stash('current_user')->id(),
        action          => 'delete_note',
        insert_datetime => \'NOW()'
    });

    return $self->render( json => { message => $self->__("success") } );
}

=head2 download_other_attachment

Downloads a generic other attachment, a bit like downloadcv.
Specify the file to download with the 'key' param

=cut

sub download_other_attachment {
    my ($self) = @_;

    my $attachment_key = $self->param('key');

    $self->app()->log()->info("Will download attachment '$attachment_key'");

    $self->render_later();

    return $self->do_job(
        queue      => 'ForegroundJob',
        class      => 'Stream2::Action::DownloadOtherAttachment',
        parameters => {
            results_id      => $self->param('results_id'),
            candidate_idx   => $self->param('candidate_idx'),
            user            => $self->stash('job_user_ref'),
            remote_ip       => $self->remote_ip(),
            ripple_settings => $self->current_user->ripple_settings(),
            attachment_key  => $attachment_key,
        }
    )->then(
        sub{
            my ($job) = @_; # This is there thanks to App::Stream2::Plugin::Queue::do_job
            $self->app()->log()->info("Parsing response");
            my $response = $self->parse_response( $job );
            # $.fileDownload needs this cookie set in the iframe to show success
            $self->cookie(fileDownload => 'true', { path => '/' });
            return $self->render_file_download( $response );
        },
        sub{
            my ($job) = @_;
            $self->render( json => { error => { message => "Error downloading attachment - Quote ".$job->id() } }, status => 500 )
        }
    );

}

sub file_test {
    return shift->render( json => { text => "OK" } );
}

=head2 other_attachments

Spawns the rendering of this candidate other attachments

=cut

sub other_attachments {
    my ($self) = @_;

    my $query     = $self->stash('query_ref');
    my $status_id = $self->queue_job(
        queue      => 'ForegroundJob',
        class      => 'Stream2::Action::RenderOtherAttachments',
        parameters => {
            results_id      => $self->param('results_id'),
            candidate_idx   => $self->param('candidate_idx'),
            user            => $self->stash('job_user_ref'),
            ripple_settings => $self->current_user->ripple_settings(),
        }
    )->guid;

    return $self->render( json => { status_id => $status_id } );
}

=head2 enhancements

Queues a job to retrieve extra information for a candidate.

See the action class for details

=cut

sub enhancements {
    my ($self) = @_;

    my $section = $self->param('section')
        or return $self->render(json => { error => { message => 'Section required' } }, status => 400);

    my $status_id = $self->queue_job(
        queue      => 'ForegroundJob',
        class      => 'Stream2::Action::CandidateEnhancements',
        parameters => {
            results_id          => $self->param('results_id'),
            candidate_idx       => $self->param('candidate_idx'),
            user                => $self->stash('job_user_ref'),
            ripple_settings     => $self->current_user->ripple_settings(),
            enhancement_section => $section,
        }
    )->guid;

    return $self->render( json => { status_id => $status_id });
}

sub delete_future_action{
    my ($self) = @_;

    my $candidate = $self->stash()->{candidate};
    my $action_id = $self->param('action_id');
    my $user = $self->current_user();

    my $stream2 = $self->app()->stream2();
    my $action = $stream2->factory('CandidateActionLog')->search(
        {
            id => $action_id,
            group_identity => $user->group_identity(),
            candidate_id => $candidate->id(),
            action => { -like => 'candidate_future%' }
        }
    )->first();
    unless( $action ){
        return $self->render(
            json => { error => { message => $self->__("Cannot find action or process to delete") }},
            status => 404
        );
    }

    $action->cancel();
    return $self->render( json => { message => $self->__("Future action cancelled"),
                                    new_data => $action->data()
                                } );
}

1;

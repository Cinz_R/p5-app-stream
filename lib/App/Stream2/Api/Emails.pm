package App::Stream2::Api::Emails;

use Mojo::Base 'App::Stream2::Controller::Authenticated::JSON';
use Log::Any qw/$log/;
use Carp qw/confess/;


=head1 NAME

App::Stream2::Api::Emails - a controller that shows current user's sent emails

=cut

my $DEFAULT_ITEMS_PER_PAGE = 10;

# returns the page number and number of items per page sought
sub _get_pagination_params {
    my $self = shift;
    return ( $self->param('page') || 1,
             $self->param('items_per_page') || $DEFAULT_ITEMS_PER_PAGE );
}

# returns a JSON representation of the email message summaries in $rs
sub _jsonify_resultset {
    my ( $self, $rs ) = @_;

    my @email_summaries = map {
        {
            id            => $_->id,
            date          => $_->sent_date =~ tr/T/ /r,
            subject       => $_->subject,
            from          => $_->user->contact_name,
            recipient     => $_->recipient,
            recipientName => $_->recipient_name,
        }
    } $rs->all;

    return {
        emails => \@email_summaries,
        pager  => $self->pager_to_hashref( $rs->pager )
    };
}

=head2 search_user_emails

Is hit by the email summary view while logged in as a user,
accepts keywords and paging options

=cut

sub search_user_emails {
    my $self     = shift;

    my $keywords                = $self->param('keywords');
    my ($page, $items_per_page) = $self->_get_pagination_params;
    my $user                    = $self->current_user;

    my $rs = $self->app->stream2->factory('SentEmail')->search_user_emails(
        $user, $keywords, { page => $page, rows => $items_per_page }
    );
    my $json = $self->_jsonify_resultset( $rs );

    $self->render( json => $json );
}

=head2 search_group_emails

Used by admin email summary page, returns a list of emails
sent by any user in current user's group

=cut

sub search_group_emails {
    my $self     = shift;

    my $keywords                = $self->param('keywords');
    my ($page, $items_per_page) = $self->_get_pagination_params;
    my $user                    = $self->current_user;

    my $rs = $self->app->stream2->factory('SentEmail')->search_group_emails(
        $user, $keywords, { page => $page, rows => $items_per_page }
    );
    my $json = $self->_jsonify_resultset( $rs );

    $self->render( json => $json );
}

sub get_email {
    my $self   = shift;
    my $msg_id = $self->stash('id');

    my $user  = $self->current_user;

    ## The candidate history tab doesnt impose to see
    ## only your own emails. Those of anyone in your company is alright.
    ## So no is_admin business for now
    ##
    # my $email = $user->is_admin() ?
    #     $user->groupclient()->users()->search_related('sent_emails')
    #          ->find( $msg_id ) :
    #     $user->sent_emails()->find( $msg_id );
    my $email = $user->groupclient()->users()->search_related('sent_emails')
        ->find( $msg_id );

    if ( !$email ) {
        return $self->render( text => "You have not sent an email message "
            . "identified by ID $msg_id", status => 404 );
    }

    # wrap Result::SentEmail instance
    $email = $self->app->stream2->factory('SentEmail')->wrap($email);
    my $html_body = $email->mime_entity->one_html_body;

    $self->render( text => $html_body );
}

1;

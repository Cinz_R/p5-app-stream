package App::Stream2::Ripple;

use strict;
use warnings;

use Carp;

use Mojo::Base 'App::Stream2::Controller';
use Log::Log4perl;

use Log::Any qw/$log/;

use Data::Dumper;

use Stream2::Criteria;

=head1 NAME

App::Stream2::Ripple - Ripple API (see http://api.adcourier.com/docs/index.cgi?page=ripple_docs) end points.

=head2 index

Accepts a POST with some XML (see 'A Simple Example' in the ripple API doc)
and responds with something like:

  <?xml version="1.0" encoding="UTF-8"?>
  <Search version="3.0">
    <TimeNow>2012-03-29T09:47:36</TimeNow>
    <SearchResponse>
      <Status code="">$message</Status> // 200 OK 401 Authentication Failure
      <URL>http://www.adcourier.com/cvsearch/ripple.cgi?stored_id=123456abcdef</URL>
    </SearchResponse>
  </Search>

Example of request:

   curl -k -X POST https://192.168.1.228:3001/ripple/ -d '<?xml version="1.0" encoding="UTF-8" ?><Search>
    <Version>3.0</Version>
    <APIKey>XXXXXXXXXXXX</APIKey>
    <Account>
      <UserName>cinzia.second@team_one.London.cinzwonderland</UserName>
      <Password>chenin200</Password>
    </Account>
    <Query>
      <Keywords>Project Manager</Keywords>
      <Location>
        <Postcode>SW12 2AW</Postcode>
        <Country>UK</Country>
      </Location>
    </Query>
    <Settings>
      <StyleSheet>/static/stylesheets/custom-demo.css</StyleSheet>
      <Shortlist name="cheese_sandwiches" allowPreview="1">
           <Label>Cheese Sandwich</Label>
           <URL>_this_is_dummy_</URL>
      </Shortlist>
      <Shortlist name="beers">
          <Label>Beer</Label>
          <URL>_this_is_dummy_</URL>
     </Shortlist>
     <ImportCandidateURL>_this_is_dummy_</ImportCandidateURL>
    </Settings>
    <CustomFields>
      <CustomField name="myInternalUserId">123</CustomField>
      <CustomField name="myInternalJobId">321</CustomField>
    </CustomFields>
  </Search>
'

A bullhorn user, (nice to test bullhorn integration:)

luke@test.test.bh_test
broadbean11

The custom stylesheet should be something like

  @import url('https://192.168.1.228:3001/static/stylesheets/app.css');
  * {
      color: #FF00FF  !important;
  }

=cut

sub index{
    my ($self) = @_;

    my $request = $self->req();
    my $stream2 = $self->app->stream2();

    # Parse the XML bytes.
    my $xml_parser = $self->app->stream2()->xml_parser();
    my $body = $self->param('xml') || $request->body();

    my $xreq = eval{
        $xml_parser->load_xml( string => $body );
    };
    if ( $@ ){
        $self->stash( 'ripple_version' => _scrape_version( $body ) );
        return $self->render( exception => 'Malformed Query: '.$@, status => 400 );
    }

    my $version = $xreq->findvalue('//Version') // '';
    $self->stash('ripple_version' => $version);

    if ( $version eq '3.0' ){
        eval {
            $xreq->validate( $stream2->dtd('ripple/ripplev3.dtd') );
        };
        if ( $@ ) {
            return $self->render( exception => "Invalid Query: $@", status => 400 );
        }
    }
    elsif ( $version =~ m/\A1\.0/ ) {
        eval {
            $xreq->validate( $stream2->dtd('ripple/ripplev2.dtd') );
        };
        if ( $@ ) {
            return $self->render( exception => "Invalid Query: $@", status => 400 );
        }

        # Just need to rename some of the fields on the old style XML to make it compatible with the new
        map { $_->setNodeName('Search'); } $xreq->findnodes('/CV_Search_Query');
        map { $_->setNodeName('Query'); } $xreq->findnodes('/Search/Stream_Query');
        map { $_->setNodeName('Query'); } $xreq->findnodes('/Search/Search_Query');
        map { $_->setNodeName('Settings'); } $xreq->findnodes('/Search/Config');
    }
    else {
        return $self->render( exception => 'Invalid Version value.', status => 400 );
    }

    my $api_key = $xreq->findvalue('/Search/APIKey');
    my $username = $xreq->findvalue('/Search/Account/UserName');
    my $password = $xreq->findvalue('/Search/Account/Password');

    my $signature_hash = $xreq->findvalue('/Search/Account/Signature/Hash');
    my $signature_time = $xreq->findvalue('/Search/Account/Signature/Time');

    $self->stash('api_key' => $api_key);

    my $grant_params = { api_key => $api_key, username => $username };

    if( $password ){
        $grant_params->{password} = $password;
    }else{
        $grant_params->{signature} = $signature_hash;
        $grant_params->{time_ms} = $signature_time;
    }

    # Right, we do have some credentials in $grant_params
    # We need to use the oauth client to get some user info and other magic.
    $self->render_later;

    return $self->auth_provider({ provider => 'adcourier_ats'})->oauth_client->credentials_grant( %$grant_params )->then(
        sub {
            my $grant_response = shift;
            $log->info("Grant response: ".Dumper($grant_response));

            my $session_url = $self->_create_session( $xreq, $grant_response );
            my $xml_response = $version eq '3.0' ?
                $self->_render_v3( $session_url ) : $self->_render_v2( $session_url );

            $log->info("Successful ripple authentication: '".$xml_response->toString(2).'"');
            return $self->render( data => $xml_response->toString(2), status => 200 );
        },
        sub{
            my $error = shift // 'unk error';
            return $self->render( exception => $error, status => 401 );
        }
    )
}

sub _render_v2 {
    my ( $self, $session_url ) = @_;
    my $xml_response = XML::LibXML::Document->createDocument('1.0', 'UTF-8');

    my $now = DateTime->now();
    my $xsearch = $xml_response->createElement('CV_Search_Query');
    $xml_response->setDocumentElement($xsearch);
    $xsearch->setAttribute('version', '1.0.0-r118179');
    $xsearch->appendChild($xml_response->createElement('TimeNow'))->appendText($now->iso8601());
    $xsearch->appendChild($xml_response->createElement('Information'));
    $xsearch->appendChild($xml_response->createElement('Stream_Query'))
        ->appendChild($xml_response->createElement('StreamURL'))
        ->appendText($session_url->to_abs());

    return $xml_response;
}

sub _render_v3 {
    my ( $self, $session_url ) = @_;
    my $xml_response = XML::LibXML::Document->createDocument('1.0', 'UTF-8');
    my $now = DateTime->now();
    my $xsearch = $xml_response->createElement('Search');
    $xml_response->setDocumentElement($xsearch);
    $xsearch->setAttribute('version', '3.0');
    $xsearch->appendChild($xml_response->createElement('TimeNow'))->appendText($now->iso8601());

    my $xsearch_response = $xsearch->appendChild($xml_response->createElement('SearchResponse'));
    $xsearch_response->appendChild($xml_response->createElement('URL'))
        ->appendText($session_url->to_abs());

    my $xstatus = $xsearch_response->appendChild($xml_response->createElement('Status'));
    $xstatus->setAttribute('code', '200');
    $xstatus->appendText("Success");

    return $xml_response;
}

=head2 _create_session

    - XML is valid
    - OAuth is successful

    create a session URL and add the row to the table

=cut

sub _create_session {
    my ( $self, $xreq, $grant_response ) = @_;
    my $stream2 = $self->app->stream2();

    # All is cool. We can build some criteria from the document.
    my $criteria = $self->_criteria_from_xml($xreq, $grant_response);

    # Building some ripple settings if they are there.
    my $ripple_settings = {};
    if( my $xSettings = $xreq->find('/Search/Settings') ){

        my $stylesheet_url;
        if( my $xstylesheet = $xreq->find('/Search/Settings/StyleSheet')->pop() ){
            $stylesheet_url = $xstylesheet->textContent();
        }

        my $shortlists = [];
        my @xShortlists = $xreq->findnodes('/Search/Settings/Shortlist');
        foreach my $xShortlist ( @xShortlists ){
            my $name = $xShortlist->getAttribute('name');
            my $allowPreview = $xShortlist->getAttribute('allowPreview');
            my $label = $xShortlist->find('Label')->pop()->textContent();
            my $url   = $xShortlist->find('URL')->pop()->textContent();
            push @$shortlists , { name => $name, label => $label,  options_url => $url , allowPreview => $allowPreview ? 1 : 0 };
        }

        my $new_candidate_url;
        my $candidate_tag_doc;
        if( my $xnew_candidate_url = $xreq->find('/Search/Settings/ImportCandidateURL')->pop() ){
            $new_candidate_url = $xnew_candidate_url->textContent();
            $candidate_tag_doc = $xnew_candidate_url->getAttribute('tagged_doc') || 'BGT';
        }

        $ripple_settings = { custom_lists => $shortlists ,
                             $stylesheet_url ? ( stylesheet_url => $stylesheet_url ) : () ,
                             $new_candidate_url ? ( new_candidate_url => $new_candidate_url ) : (),
                             $new_candidate_url ? ( tagged_doc_type => $candidate_tag_doc ) : (),
                           };

        my %valid_settings = (
            HideShortlist => 'no_adcourier_shortlist',
            HideForward => 'hide_forward',
            HideDownload => 'hide_download',
            HideSave => 'hide_save',
            HideMessage => 'hide_message',
            HideChargeableMessage => 'hide_chargeable_message',
            HideFavourite => 'hide_favourite',
            HideTagging => 'hide_tagging'
        );
        while ( my ( $k, $v ) = each %valid_settings ) {
            if ( my $x = $xreq->find("/Search/Settings/$k")->pop ) {
                $ripple_settings->{$v} = 1 if $x->textContent ne 'false';
            }
        }
    }

    # use adc standard shortlist feature as a fallback
    my $has_no_shortlists    = !defined( $ripple_settings->{custom_lists} )
        || @{ $ripple_settings->{custom_lists} } == 0;
    my $use_default_shortlist = !$ripple_settings->{no_adcourier_shortlist};
    if ( $has_no_shortlists && $use_default_shortlist ) {
        $ripple_settings->{custom_lists} =
            [ { name => 'adc_shortlist', label => 'Shortlist' } ];
    }

    my $webhooks = $stream2->user_api->provider_settings(
        $grant_response->{user}->{user_id},
        $self->stash('api_key')
    )->{webhooks};
    if( $webhooks && scalar(@$webhooks) ) {
        $ripple_settings->{webhooks} = $webhooks;
    }

    $log->info("webhooks: " . Dumper($webhooks));

    # Also build the custom fields.
    my $custom_fields = [];
    my @custom_xfields = $xreq->findnodes('/Search/CustomFields/CustomField');
    foreach my $custom_xfield ( @custom_xfields ){
        my $field = { name => $custom_xfield->getAttribute('name'),
                      asHttpHeader => ( ( $custom_xfield->getAttribute('asHttpHeader') // 'false' ) eq 'true' ? 1 : 0 ),
                      content => $custom_xfield->textContent(),
                    };
        push @$custom_fields , $field;
    }

    my $user            = $grant_response->{user};
    my $contact_details = $user->{read_contact_details};
    my $subscriptions   = { %{$user->{read_stream_subscriptions} // {} } };
    my $user_role       = $user->{user_role} || 'USER';
    my $is_admin        = $user_role eq 'SUPERADMIN';
    my $is_overseer     = $user_role =~ /^(?:(?:SUPER)?ADMIN|TEAMLEADER)$/;
    $self->user_sign_in( {
        provider => {
            user_id           => $user->{user_id},
            identity_provider => 'adcourier',
            login_provider    => 'adcourier_ats',
            group_identity    => $contact_details->{company},
            group_nice_name   => $contact_details->{company_niceName},
            navigation        => [],
            is_admin          => $is_admin,
            is_overseer       => $is_overseer,
        },
        ripple_settings => $ripple_settings,
        settings        => delete $contact_details->{settings} // {},
        contact_details => $contact_details,
        subscriptions   => $subscriptions,
        custom_fields   => $custom_fields,
        currencies      => $user->{read_currencies} || [ qw/GBP USD EUR/ ],
    } );

    # combine Search and Adcourier_ats data about this user
    $self->current_user->identity_provider
         ->after_sign_in( $self->current_user );

    my $route;
    if ( $is_overseer ) {
        $route = '/welcome-admin';
    }
    else {
        $route = '/search' . ( $criteria->id ? '/' . $criteria->id : '' );
    }
    my $session_state = $self->app->sessions->state;
    return $session_state->sessionized_url(
        $self,
        $self->url_for( $route )->to_abs,
        $grant_response->{expires_in}    # typically 43200 secs (12 hours)
    );
}


## Build a new Stream2::Criteria from the given xdoc
sub _criteria_from_xml{
    my ($self, $xdoc, $grant_response) = @_;

    my $stream2 = $self->app->stream2();
    $self->app->log->info( $xdoc->toString(2) );

    my $criteria = Stream2::Criteria->new( schema => $stream2->stream_schema() );

    if( my $keywords = $xdoc->findvalue('/Search/Query/Keywords') ){
        $criteria->add_token('keywords' => $keywords );
    }

    if( my $ofccp_context = $xdoc->findvalue('/Search/Query/OfccpContext') ){
        $criteria->add_token('ofccp_context' => $ofccp_context );
    }

    my $location;
    # Resolve location
    if( my $location_id = $xdoc->findvalue('/Search/Query/LocationId') ){
        $location = $stream2->location_api->find($location_id);
    } elsif( my $postcode = $xdoc->findvalue('/Search/Query/Location/Postcode') ){
        $location = $stream2->location_api->find({ postcode => $postcode });
    } elsif( my $latitude = $xdoc->findvalue('/Search/Query/Location/Latitude') ){
        # Longitude always go with latitude as per DTD
        $location = $stream2->location_api->find({ latitude => $latitude, longitude => scalar($xdoc->findvalue('/Search/Query/Location/Longitude')) });
    }

    if( $location ){
        $criteria->add_token('location' => $location->specific_path());
        $criteria->add_token('location_id' => $location->id() );
    }

    # <!ELEMENT Radius          (#PCDATA)><!-- optional, in miles -->
    if( defined( my $radius = $xdoc->findvalue('/Search/Query/Radius') ) ){
        if( length($radius) ){ # If this radius is something.

            # Only radius in miles can be expressed through a ripple request
            # if the user uses KM we need to do some convertion and ensure the radius
            # is adequately represented in the criteria
            my $distance_unit = $grant_response->{user}->{read_contact_details}->{distance_unit} || 'miles';
            if ( $distance_unit eq 'km' ){
                $radius = ($radius || 0) * 1.609;
            }
            $criteria->add_token('distance_unit' => $distance_unit);
            $criteria->add_token('location_within' => int(( $radius || 0 ) * 1) );

        }
    }

    # <!ELEMENT ContractType    (#PCDATA)><!-- optional -->
    if( my $contract_type = $xdoc->findvalue('/Search/Query/ContractType') ){
        $criteria->add_token('default_jobtype' , $contract_type );
    }

    # <!ELEMENT SalaryCur       (#PCDATA)><!-- optional: Any ISO 4217 currency code: see http://en.wikipedia.org/wiki/ISO_4217 -->
    if( my $salary_cur = $xdoc->findvalue('/Search/Query/SalaryCur') ){
        $criteria->add_token('salary_cur' => $salary_cur );
    }

    # <!ELEMENT SalaryMin       (#PCDATA)><!-- optional -->
    # <!ELEMENT SalaryMax       (#PCDATA)><!-- optional -->
    # <!ELEMENT SalaryRate      (#PCDATA)><!-- optional: annum/month/week/day/hour -->
    if( defined( my $salary_min = $xdoc->findvalue('/Search/Query/SalaryMin') ) ){
        if( length($salary_min) ){
            $criteria->add_token('salary_from' => $salary_min );
        }
    }
    if( defined( my $salary_max = $xdoc->findvalue('/Search/Query/SalaryMax') ) ){
        if( length($salary_max) ){
            $criteria->add_token('salary_to' => $salary_max );
        }
    }
    if( my $salary_rate = $xdoc->findvalue('/Search/Query/SalaryRate') ){
        $criteria->add_token('salary_per' => $salary_rate );
    }

    # <!ELEMENT UpdatedWithin   (#PCDATA)><!-- optional: 'TODAY','YESTERDAY','3D','1W','2W','1M','2M','3M','6M','1Y','2Y','3Y','ALL' -->
    if( my $updated_within = $xdoc->findvalue('/Search/Query/UpdatedWithin') ){
        $criteria->add_token('cv_updated_within' => $updated_within );
    }

    # <!ELEMENT IncludeUnspecifiedSalaries (#PCDATA)><!-- optional: 1 (true) or 0 (false), default 0 -->
    my $include_unspecified = $xdoc->findvalue('/Search/Query/IncludeUnspecifiedSalaries');
    if( defined( $include_unspecified ) && $include_unspecified ne "" ){
        $criteria->add_token('include_unspecified_salaries' => $include_unspecified);
    }
    else {
        $criteria->add_token('include_unspecified_salaries' => 1);
    }

    # Define channel specific criteria
    # <ChannelList><Channel><ChannelId>talentsearch</ChannelId><Extension name="tag">Test</Extension></Channel></ChannelList>
    my @xChannels = $xdoc->findnodes('/Search/ChannelList/Channel');
    foreach my $xChannel ( @xChannels ){
        my $channel_id = $xChannel->find('ChannelId')->pop()->textContent();
        my @xExtensions = $xChannel->findnodes('./Extension');
        foreach my $xExtension ( @xExtensions ){
            my $extension_name = $xExtension->getAttribute('name');
            my $extension_value = $xExtension->textContent();

            unless ( $extension_name =~ m/\A\Q$channel_id\E_/ ){
                $extension_name = join( '_', $channel_id, $extension_name );
            }

            $criteria->add_token( $extension_name => $extension_value );
        }
    }

    $criteria->save();
    return $criteria;
}

=head2 _scrape_version

    The parsing hasn't gone well... at all
    We need to try and get hold of the ripple version they are using

=cut

sub _scrape_version {
    my $body = shift;
    if ( $body =~ m/<Version>([^<]+)/ ){
        return $1;
    }
    return '3.0';
}

1;

package App::Stream2::Sessions3S::State;

use Mojo::Base qw/Mojolicious::Sessions::ThreeS::State::Cookie/;
use Mojo::Util qw//;

use Crypt::JWT qw//;

use Carp;

has 'session_token_param' => 'STOK';

=head1 NAME

App::Stream2::Sessions3S::State - The way search manages states for the Sessions3S plugin

=cut

=head2 get_session_id

On top of the session ID living in a cookie (like in the superclass
L<Mojolicious::Sessions::ThreeS::State::Cookie>), this will also
check for session IDs at different places. Like the x-ripple-session-id
header, or the magic GET parameters.

=cut

=head2 sign_value

Given a value, generate a signed version of it the mojo way. Beware that
the given value CANNOT contain any '--' string.

Usage:

 my $signed_value = $this->sign_value( $controller, $value );

=cut

sub sign_value{
    my ( $self, $controller , $value) = @_;
    return $value.'--'.Mojo::Util::hmac_sha1_sum($value, $controller->app->secrets->[0]);
}

=head2 check_value

Checks a signed value (the mojo way) is correct and returns it (or undef if the signature is invalid).

Usage:

 if( my $value = $this->checked_value( $controller , $signed_value ) ){
    ...
 }

=cut

sub checked_value{
    my ($self , $controller, $signed_value ) = @_;
    unless( $signed_value ){ return; }
    my ($value , $signature) = split( '--' , $signed_value );
    unless( $signature && $value ){ return ; }

    my $candidate = Mojo::Util::hmac_sha1_sum($value, $controller->app->secrets->[0]);
    if( Mojo::Util::secure_compare( $candidate , $signature ) ){
        return $value;
    }

    return undef;
}

=head2 get_session_id

See superclass.

=cut

sub get_session_id{
    my ( $self, $controller ) = @_;
    my $req = $controller->req();

    # A GET URL might have a session_token_param magic parameter.
    # In this case, we want to inject this in the Response and redirect to an URL
    # without this token.
    if( ( my $signed_token = $req->param( $self->session_token_param() ) ) &&  ( $req->method() eq 'GET' ) ){
        my $data = Crypt::JWT::decode_jwt( token => $signed_token ,
                                           key => $controller->app()->secrets()->[0]
                                       );
        if( $data ){

            my $sid = $data->{sid};

            my $stripped_url = $req->url()->clone();
            $stripped_url->query([ $self->session_token_param() => undef ] );

            # Inject the session ID as it should be and
            # make the cookie expire when we decided the link should not
            # be valid anymore.
            $self->set_session_id( $controller  , $sid  , { expires => $data->{exp} } );
            $controller->app()->log()->info("Will redirect to $stripped_url");
            $controller->redirect_to( $stripped_url );
            $controller->finish();

            return $sid;
        }
    } # End of magic GET

    if( my $sid = $self->SUPER::get_session_id( $controller ) ){
        return $sid;
    }

    # Session ID can also live in $headers->header('x-ripple-session-id')
    if( my $sid = $self->checked_value( $controller , ''.( $req->headers()->header('x-ripple-session-id') // '' )  )  ){
        return $sid;
    }

    return;
}

=head2 sessionized_url

Given a L<Mojo::URL>, turn it into a magic session injecting one valid until the
given time.

Usage:

  my $good_for_5minute = $this->sessionize_url( $controller , $controller->url_for('/blabla') , time() + 5 * 60 );

=cut

sub sessionized_url{
    my ($self, $controller, $url, $expires_in ) = @_;
    return $self->sessionize_url(
        $controller,
        $url,
        $self->session_token( $controller, $expires_in )
    );
}

=head2 sessionize_url

Accepts an already constructed signed token to sessionize a URL

Usage:

    my $STOK = $this->session_token( $controller, 360 );
    my $sessionized_url = $this->sessionize_url(
        $controller,
        $url,
        $STOK
    );

=cut

sub sessionize_url{
    my ( $self, $controller, $url, $signed_token ) = @_;

    my $clone = $url->clone();
    $clone->query->merge( $self->session_token_param() => $signed_token );

    return $clone;
}

=head2 set_session_id

See superclass.

=cut

sub set_session_id{
    my ( $self, $controller, @rest  ) = @_;
    if( delete $controller->stash()->{__PACKAGE__.'no-cookie-please'} ){
        return;
    }
    return $self->SUPER::set_session_id( $controller , @rest );
}

=head2 no_cookie_please

Marks the current request to avoid emitting the session cookie.

Usage:

 $this->no_cookie_please( $controller ));

=cut

sub no_cookie_please{
    my ($self, $controller) = @_;
    $controller->stash()->{__PACKAGE__.'no-cookie-please'} = 1;
}

=head2 session_token

Given the controller and an expiry time in seconds, return a magic Session Token
(STOK) one can use to build auto-login URLs.

Usage:

 my $STOK = $this->session_token( $controller, 360 ); # Gives a 6 minuts valid STOK

 .. redirect to /whatever/search/url?STOK=$STOK for auto-magic login. ..

=cut

sub session_token {
    my ( $self, $controller, $expires_in ) = @_;

    my $session_id = $controller->session_id();
    my $payload = { sid => $session_id };

    return Crypt::JWT::encode_jwt( payload => $payload,
        alg => 'PBES2-HS256+A128KW',
        enc => 'A128GCM',
        relative_exp => $expires_in,
        key => $controller->app()->secrets()->[0]
    );
}

1;

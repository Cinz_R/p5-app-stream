use strict;
use warnings;

package App::Stream2::Web::SNSNotification;

use Mojo::Base 'Mojolicious::Controller';

use Data::Dumper;

=head2 with_notification

Takes care of the subscription/unsubscription part of SNS notifications and let you deal
with the notification message only.

Returns whatever the given sub returns.

Usage:

 $this->with_notification(sub{
     my ($message) = @_;
     my $expected_topic = $stream2->config()->{bean_pubsub_client}->{bbcss_response_arn};
     unless( $message->{TopicArn} eq  $expected_topic ){
        return $self->render(..)
     }
     ...
 });

=cut

sub with_notification{
    my ($self, $stuff) = @_;

    my $app = $self->app();

    my $sns_message_type = $self->req()->headers()->header('x-amz-sns-message-type');
    $app->log()->info("Got message type=".$sns_message_type);

    my $message = $self->req()->json();
    $app->log()->info("Got message content=".Dumper($message));

    if( $sns_message_type eq 'SubscriptionConfirmation' ){
        my $subscribe_url = $message->{SubscribeURL};
        $app->log->info("Hitting $subscribe_url");
        my $response = $app->stream2()->user_agent->get( $subscribe_url );
        $app->log->info("Got response ".$response->as_string("\n"));
        unless( $response->is_success() ){
            die "Failed to hit $subscribe_url: ".$response->as_string("\n");
        }
        return $self->render( json => { message => 'OK' } );
    }

    if( $sns_message_type eq 'Notification' ){
        return $stuff->( $message );
    }

    if( $sns_message_type eq 'UnsubscribeConfirmation' ){
        return $self->render( json => { message => 'OK' } );
    }

    return $self->render( status => 400, message => 'Unsupported' );
}

=head1 DESCRIPTION

Contains the end point for receiving Amazon SNS notifications of various natures.

=cut

sub receive_bbcss_response {
    my ( $self ) = @_;
    my $app = $self->app();
    my $stream2 = $app->stream2();

    return $self->with_notification(
        sub{
            my ($message) = @_;
            my $expected_topic = $stream2->config()->{bean_pubsub_client}->{bbcss_response_arn};
            unless( $message->{TopicArn} eq  $expected_topic ){
                $app->log()->warn("We are not interested in ".$message->{TopicArn}." we are interested in ".$expected_topic);
                return $self->render( json => { message => 'Unsupported topic' }, status => 400 );
            }
            ## Ok we have the right notification.
            my $notification = $stream2->pubsub_client->read( $message->{Message} );
            my $status_id = $self->queue_job(
                queue => 'BackgroundJob',
                class => 'Stream2::Action::NewBBCSSResponse',
                parameters => {
                    %{$notification},
                }
            )->guid;
            return $self->render( json => { message => 'OK', status_id => $status_id } );
        });
}

1;

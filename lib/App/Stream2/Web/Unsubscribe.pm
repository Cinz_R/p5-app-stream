package App::Stream2::Web::Unsubscribe;
use Mojo::Base 'Mojolicious::Controller';

use Encode;
use Mojo::IOLoop;

=head1 DESCRIPTION

Unsubscribe is a means for candidates to elect out of emails from specific clients via a link in the foot of an email
or Via the the List-Unsubscribe header RFC 2369.

=head2 index

Display the unsubscribe confirmation page with an optional comments input field. To working with the list-unsubscribe
header a suffice path of 'auto' is used to perform the unsubscribe.

The valid routes into this this are can look like:

1) https://SERVER/external/unsubscribe/?email=USEREMAIL&group_identity=G_ID&group_nice_name=C_NAME&purpose=somemessage&rec=ID&sign=SIG
2) https://SERVER/external/unsubscribe/C6EE6E02-9F6C-11E5-82FB-E2B97BCA88A4
3) https://SERVER/external/unsubscribe/C6EE6E02-9F6C-11E5-82FB-E2B97BCA88A4/auto

=cut

sub index {
    my ( $self ) = @_;

    my $s2 = $self->app->stream2;
    # warm the stash with the translation goodness.
    $self->stash(
        i18n    => sub { $s2->translations->__(@_) },
        i18nx   => sub { $s2->templates->i18nx(@_) }
    );

    # if id exists then it is a UUID based Unsubscribe URL, else fall back to
    # the older url scheme.
    my $results = $self->param('id') ? $self->_validate_uuid_urls()
        : $self->_validate_get_param_urls();

    # if validation of the url fails we render the invalidurl page.
    # This is a departure form the older way to do it, as there were individual
    # errors associated with every params, which is kinda necessary.

    unless ($results){
        return $self->render( template => 'unsubscribe/invalidurl', status => 400);
    }


    # if this is a auto unsubscribe we do some fun things.
    if ($self->param('auto')){
        $results->{comments} = "The candidate unsubscribed via their email provider.";
        if ( $self->_do_the_unsubscribe($results)){
            # if the candidate has been successfully unsubscribe then we can send the
            # email to the recruiter at our leisure.
            Mojo::IOLoop->next_tick(sub {$self->_send_recruiter_email($results)});
            return $self->render( 'unsubscribe/successful' );
        }
        return $self->render( 'unsubscribe/invalidurl', status => 400 );
    }
    # Stash the results and render
    $self->stash(%{$results});
    return $self->render( 'unsubscribe' );
}

=head2 submit

Verifies the submitted form data, and  does the unsubscribe, if there is an
error, render the invalid URL
page.

=cut

sub submit {
    my ( $self ) = @_;

    my $s2 = $self->app->stream2;
    $self->stash(
        i18n    => sub { $s2->translations->__(@_) },
        i18nx   => sub { $s2->templates->i18nx(@_) },
    );
    unless( $self->_verified_request_signature() || $self->_validate_get_param_urls ){
        return $self->render(template => 'unsubscribe/invalidurl', status => 400);
    }

    # As all the params have been already validated against in validation step we can use them
    # without any problems,
    my $params = {
        email           => $self->param('email'),
        group_identity  => $self->param('group_identity'),
        purpose         => $self->param('purpose') || 'candidate.message',
        rec             => $self->param('rec'),
    };


    my $company = $s2->factory('Group')->find({
        identity => $params->{group_identity}
    }) or return $self->render( 'unsubscribe/invalidurl', status => 400 );

    $params->{company}  = $company->nice_name || $params->{group_identity};
    $params->{comments} = $self->param('comments') ? substr( $self->param('comments'), 0, 255 ) : undef;

    return $self->render( 'unsubscribe/invalidurl', status => 400 ) unless $self->_do_the_unsubscribe($params);
    # if the candidate has been successfully unsubscribe then we can send the
    # email to the recruiter at our leisure.
    Mojo::IOLoop->next_tick(sub {$self->_send_recruiter_email($params)});
    return $self->render( 'unsubscribe/successful' );
}


# private methods are below


#  _validate_uuid_urls
#
# validate and returns are data needed to perform the unsubscribe.
#

sub _validate_uuid_urls {
    my ( $self ) = @_;

    my $app = $self->app;
    my $s2  = $app->stream2;

    # get_email_by_s3 does a regex  on the UUID, no SQL-injections here.
    my $sent_email = $s2->factory('SentEmail')->get_email_by_s3($self->param('id'));


    unless( $sent_email ){
        $app->log->warn('Invalid un-subscribe URL parameters' .$self->param('id') );
        return;
    }

    my $user           = $sent_email->user;
    my $group_identity = $user->group_identity ;
    my $company        = $s2->factory('Group')->find({ identity => $group_identity }) or return;
    # to play nice with the older code, we need to sign the data.
    my $signature = $s2->sign_hash({
        email => $sent_email->recipient,
        group_identity => $group_identity,
        purpose => 'candidate.message',
        rec => $user->id(),
    });

    return {
        group_identity  => $group_identity,
        group_nice_name => $company->nice_name || $group_identity,
        email           => $sent_email->recipient,
        purpose         => 'candidate.message',
        sign            => $signature,
        rec             => $user->id()
    };
}

# _validate_get_param_urls
#
# validate and returns are data needed to perform the unsubscribe.
#

sub _validate_get_param_urls{
    my ($self) = @_;

    my $s2 = $self->app->stream2;

    my $email = $self->param('email') or return;
    my $group_identity = $self->param('group_identity') or return;

    my $company = $s2->factory('Group')->find({ identity => $group_identity }) or return;
    my $purpose = $self->param('purpose');


    unless( $self->_verified_request_signature() ){
        return;
    }

    my $recruiter = $s2->factory('SearchUser')->search({ group_identity => $group_identity,
                                                         id => scalar( $self->param('rec') )
                                                     })->first() or return;
    my $results = {
        group_identity  => $group_identity,
        group_nice_name => $company->nice_name || $group_identity,
        email           => $email,
        purpose         => $purpose,
        sign            => scalar( $self->param('sign') ),
        rec             => $recruiter->id(),
    };
    return $results ;
}

=head2 _validate_get_param_urls

validate form information by checking the data signature.

=cut

sub _verified_request_signature{
    my ($self) = @_;
    my $app = $self->app();
    my $s2 = $app->stream2;

    my $to_hash = {
        email => scalar($self->param('email')),
        group_identity => scalar($self->param('group_identity')),
        purpose => scalar($self->param('purpose')),
        rec => scalar($self->param('rec')),
    };

    my $signature = $self->param('sign') // '';
    my $candidate = $s2->sign_hash($to_hash);
    unless( $signature eq $candidate ){
        $app->log->warn("Invalid unsubscribe URL parameters");
        return; # False. This request does not pass verification
    }
    return 1;
}


=head2 _do_the_unsubscribe

Add the candidate to the Blacklist email, and email the recruiter about the unsubscribe event.

=cut

sub _do_the_unsubscribe{

    my ($self,$params) = @_;

    # If update_or_create fails, don't show a nasty dbic error
    return $self->app->stream2->factory('EmailBlacklist')->find_or_create({
        email           => $params->{email},
        group_identity  => $params->{group_identity},
        purpose         => $params->{purpose},
        comments        => $params->{comments},
    });
}


=head2 send_recruiter_email

Sends an email to the recruiter to tell them a candidate has un-subscribed
=cut

sub _send_recruiter_email {

    my ($self,$params) = @_;
    my $app = $self->app;
    my $s2 = $app->stream2;
    my $recruiter = $s2->factory('SearchUser')->search(
        {
            id             => $params->{rec},
            group_identity => $params->{group_identity},
        }
    )->first();

    unless ($recruiter) {
        $app->log->errorf(
            "No recruiter with ID: %s at %s",
            $params->{rec},
            $params->{group_identity}
        );
        return;
    }

    my $contact_email = $recruiter->contact_email();
    unless ($contact_email) {
        $app->log->errorf(
            "Recruiter with ID: %s at %s has no email address",
            $params->{rec},
            $params->{group_identity}
        );
        return;
    }

    # check to make sure the $recruiter->contact_email() is not on the blacklist
    if( $s2->factory('Email')->email_blacklisted( $contact_email ) ){
        $app->log->errorf(
            'Unsubscribe email to %s cannot be sent as they are on the email blacklist.',
            $contact_email
        );
        return;
    }

    $app->log->info("Notifying the recruiter with an email");
    # translate the comment into the recruiters language (if it exists);
    my $translated_comments = $s2->translations->in_language( $recruiter->locale, sub { $s2->__($params->{comments}); } );
    my $entity = $s2->factory('Email')->build([
        From        => 'noreply@broadbean.net',
        To          => $contact_email,
        Type        => "text/plain; charset=UTF-8",
        Encoding    => "quoted-printable",
        'X-Stream2-Transport' => 'static_transport', # We want this to go through the static IP transport
        Subject     => Encode::encode('MIME-Q', $s2->__x("Candidate {email} unsubscribed from your Broadbean Search emails", email => $params->{email} )),
        Data        => [
            Encode::encode('UTF-8',
                $s2->__x("Hi {name},\n\nA candidate ({email}) has unsubscribed from receiving emails from you through Broadbean Search.\n",
                    name => $recruiter->contact_name(),
                    email => $params->{email}
                )
            ),
            $params->{comments} ?
                Encode::encode('UTF-8',
                    $s2->__x("\nThe candidate commented '{comment}'\n",
                        name    => $recruiter->contact_name(),
                        comment => $translated_comments )
                ) : ()
        ]
    ]);

    $s2->send_email($entity);
    return;
}

1;

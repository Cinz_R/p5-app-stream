package App::Stream2::Web::Devuser;
use Mojo::Base 'Mojolicious::Controller';
use Email::Valid;

=head2 devuser_under

To authenticate via OKTA - requires SAML2 authenticaion paramters in the config..
Will post to /external/okta_saml2 to authenticate and then redirect to requested url.

=cut

sub devuser_under {
    my ( $self ) = @_;

    ## NOTE: SAML2 breaks the build. Neutering it for the time being.
    # $self->session( saml_redirect_path => $self->req->url->path->to_string );

    # unless( $self->session()->{'saml2_nameid'}){
    #     $self->redirect_to( $self->app()->saml2()->authentication_url() );
    #     return undef;
    # }
    #    return 1;


    # This is the old code:
    unless( $self->current_user() ){
        $self->render( text => 'Access Denied.',
                       status => 403 );
        return;
    }
    return !! $self->current_user()->contact_details()->{contact_devuser};
}

=head2 view_notifications

For internal users to see/search email notifications

=cut

sub view_notifications {
    my ( $self ) = @_;

    my $email = $self->param('email');
    my $results_per_page = 10;

    # the page should be a positive int above 0
    my $page = $self->param('page');
    $page = 1 unless ( $page &&  $page =~ /^[1-9][0-9]*$/ );

    my $rs = $self->app->stream2->factory('EmailNotification')->search(
        {
            ( $email ? ( email => $email ) : () )
        },
        {
            page        => $page,
            rows        => $results_per_page,
            order_by    => { -desc => 'id' }
        }
    );
    my @notifications = $rs->all();
    $self->stash( notifications => \@notifications );

    if ( my $total = $rs->count() ) {
        $self->stash( pager => $rs->pager() );
        $self->stash( email => $self->param('email') );
    }

    return $self->render( 'email-notifications' );
}


=head2 view_extra_notification_information

For internal users to see more information about an email notifications.

This will render an error:
    1) if the email_notitifcation_id is not a positive number
    2) if the email_notitifcation_id somehow is not in the database.
    3) if MessageID of the email_notification is not a positive int (MJ only uses these)
    4) if in getting the extra information from MJ fails.

=cut

sub view_extra_notification_information {
    my ( $self ) = @_;

    my $notification_id  = $self->param('notification_id');
    my $from_page = $self->param('from_page');

    # this template will be used regardless.
    $self->stash( template => 'email-notifications-extra-info' );

    # a little bit of protection, id and page should be positive int above 0
    $from_page = 1 unless ( $from_page &&  $from_page =~ /^[1-9][0-9]*$/ );
    $self->stash( from_page => $from_page );
    return $self->render(
        error => sprintf(
            "The id (%s) provided seems to be incorrect",
            $notification_id
        ))
        unless ( $notification_id && $notification_id =~ /^[1-9][0-9]*$/ );

    my $notification = $self->app->stream2->factory('EmailNotification')->search(
        {
            id => $notification_id
        }
    )->first;

    return $self->render( error => "A notification with the id $notification_id was not found" )
        unless ( $notification );

    my $notification_body = $notification->notification_body;
    my $notification_message_id = $notification_body->{MessageID};

    # this is the information that will be displayed at the top of the page, for
    # skimming
    my $tldrs;
    $tldrs = {
        ($notification_body->{error} ? ("error" => $notification_body->{error}) : ()),
        ($notification_body->{error_related_to} ? ("error_related_to" => $notification_body->{error_related_to}) : ()),
    };

    # There is currently two problems with the email notifications..
    # 1) there are two providers of the notifications,
    # 2) some of the messageID have been stomped on, eg. 2.0547696900102e+16
    return $self->render( error => 'This notification does not seem to be a valid Mailjet messageID' )
        unless ( $notification_message_id && $notification_message_id =~ /^\d+$/ );

    # check we are able to reach out to our service provider.
    my $extra_information = $self->app->stream2->mailjet_api->get_message_stats(
        mailjet_id => $notification_message_id,
        $notification->transport_method
    );

    if ( $extra_information ) {
        $tldrs->{BounceReason} = $extra_information->{BounceReason} if $extra_information->{BounceReason};
        $self->stash(
            email => $notification,
            tldrs => $tldrs,
            extra_information => $extra_information
        );
        return $self->render( 'email-notifications-extra-info' );
    }

    return 1;
}


=head2 view_blacklisted_emails

For internal users to see/search blacklisted email addresses, and delete
soft-bounce-blocks.

=cut

sub view_blacklisted_emails {
    my ( $self ) = @_;

    my $results_per_page = 15;
    my $notification;
    if ( $self->param('delete_id') ) {
        $self->stash(notification => $self->_remove_blacklisted_emails)
    }

    my $email = $self->param('email');
    if ( $email && !defined( Email::Valid->address($email) ) ) {
        return $self->render(
            text   => qq|Invalid email address: <code>"$email"</code>|,
            status => 400
        );
    }

    # we only show the delivery based blacklists
    my $rs = $self->app->stream2->factory('EmailBlacklist')->search(
        {
            ( $email ? ( email => $email ) : () ),
            purpose => { like => 'delivery%' }
        },
        {
            page        => $self->param('page') || 1,
            rows        => $results_per_page,
            order_by    => { -desc => 'id' }
        }
    );
    my @notifications = $rs->all();
    $self->stash( notifications => \@notifications );

    if ( my $total = $rs->count() ) {
        $self->stash( pager => $rs->pager() );
        $self->stash( email => $self->param('email') );
        $self->stash( results_per_page => $results_per_page );
    }

    return $self->render( 'email-blacklist' );
}

# This deletes the given id from the database, unless it is:
# * invalid (not a number or not in the database)
# * it is not a delivery.softbounceblock

sub _remove_blacklisted_emails {
    my ( $self ) = @_;

    my $email_blacklist_id_to_delete = $self->param('delete_id');

    return { error => 'Invalid Id.' }
        unless (
            $email_blacklist_id_to_delete &&
            $email_blacklist_id_to_delete =~ /^[1-9][0-9]*$/
        );

    my $blacklisted_email_record = $self->app->stream2->factory('EmailBlacklist')->search(
        {
            id => $email_blacklist_id_to_delete,
        }
    )->first;

    return { error =>
        sprintf(
            "There is no record with this ID (%s), it may have been already deleted"
            ,$email_blacklist_id_to_delete )
        } unless $blacklisted_email_record;
    my $dev_username = $self->current_user()->contact_details()->{contact_devuser} // 'UNK';
    my $blacklisted_email_address = $blacklisted_email_record->email;
    # this may set to change but there should be more pain for the other types of blocking
    unless ( $blacklisted_email_record->purpose eq 'delivery.softbounceblock' ) {
        $self->app->log->errorf(
            "%s attempted to delete %s from the database but was not a softbounceblock but %s.",
            $dev_username,
            $blacklisted_email_address,
            $blacklisted_email_record->purpose
        );
        return { error => 'Soft bounce blocks are currently the only thing that can be removed here' }
    };

    $self->app->log->infof(
        "%s deleted %s from the database.",
        $dev_username,
        $blacklisted_email_address
    );

    # temporary store before we delete the data.
    my $deleted_email_information = {
        email => $blacklisted_email_address
    };

    $blacklisted_email_record->delete;
    if ( $self->param('delete_notifications') ) {

        $self->app->log->infof(
            "%s deleted the email notification for %s from the database.",
            $dev_username,
            $blacklisted_email_address
        );
        my $email_blacklist_record = $self->app->stream2->factory('EmailNotification')->search(
            {
               %$deleted_email_information,
               notification => 'bounce.soft',
            }
        );
        # loop through and delete them suckers.
        $email_blacklist_record->fast_loop_through(
            sub {
                my ($notification_record) = @_;
                $notification_record->delete;
            }
        );
    }

    return $deleted_email_information
}

=head2 response_log

Internal route for viewing the parse.pl log for a response.
For the time being ( until we redo the logs into S3 ) the log is only
available after 2 days ( after it has been archived )

=cut

sub response_log {
    my ( $self ) = @_;

    my $response_id = $self->param('response_id');
    my $company = $self->current_user->group_identity;

    if( my $s3_url = $self->app()->stream2()->factory('PParseLog')->get_response_log_url( $company, $response_id ) ){
        return $self->redirect_to( $s3_url );
    }

    my $api = $self->app->stream2->candidate_api;

    $self->render_later;
    return $api->request_json_async(
        'GET',
        $api->url('/candidate/response', { company => $company, response_id => $response_id })
    )->then(
        sub {
            my $data = shift;
            my $logfile = $data->{response}->{logfile};
            return $self->redirect_to(
                sprintf('https://www.adcourier.com/manage/queues/search_by_board/extract-log.cgi?log=%s',$logfile)
            );
        },
        sub {
            my $err = shift;
            return $self->reply->exception($err);
        }
    );
}

1;

package App::Stream2::Web::Log;

=head1 NAME

App::Stream2::Web::Log

=head1 DESCRIPTION

Contains methods concerning log viewing via the application

=cut

use Mojo::Base 'App::Stream2::Controller::Authenticated::HTML';
use Log::Log4perl;

use Data::Dumper;

use Log::Any qw/$log/;

use DateTime;

use HTTP::Request;
use HTTP::Response;

use HTML::Scrubber;
use Template;

=head2 show_candidate_action

Show a candidate action (from action_id) in captured URL bit.

=cut

sub show_candidate_action{
    my ($self) = @_;

    unless( $self->current_user()->contact_details()->{contact_devuser} ){
        return $self->render( text => "Page restricted to internal Broadbean users.", status => 403);
    }

    my $action_id = $self->param('action_id');
    my $stream2 = $self->app()->stream2();

    # action id should not be confused with qurious job id
    if ( $action_id && $action_id =~ m/[^\d]/ ){
        $action_id = undef;
        $self->stash( error => 'Invalid Action ID should be numeric only' );
    }
    # allow the manual entry of a candidate action id
    if ( ! $action_id ) {
        return $self->render('candidate-action-form');
    }

    my $action = $stream2->factory('CandidateActionLog')->find($action_id);
    unless( $action ){
        $log->warn("Could not find action for action_id = '$action_id'");
        $self->stash( error => sprintf('Action not found: %s', $action_id) );
        return $self->render('candidate-action-form');
    }

    # # Lookup the 'chunk' appender of Log4perl. It
    # # contains the s3 storage bucket for our log chunks.
    my $appender = Log::Log4perl->appender_by_name('SearchLogChunk');
    unless( $appender ){
        $log->warn("No appender 'Chunk' found in log4perl config");
    }else{
        # We have an appender.
        my $s3_log_id = $action->data()->{s3_log_id};
        if( $s3_log_id ){

            my $bucket = $appender->store()->bucket();
            my $object = $bucket->object( key => $s3_log_id );

            unless( $object->exists() ){
                $log->warn("No chunk ID ".$s3_log_id)
            }else{
                $self->stash('log_lines' , $self->_log_chunk_to_lines($object->get()) );
            }
        }else{
            $log->warn("No chunk ID In action data");
        }
    }

    # Report the sent status of emails
    if ( my $custom_id = $action->data->{mailjet_custom_id} ) {
        my $json = $stream2->mailjet_api->get_message_stats(
            custom_id => $custom_id, $action->data->{mailjet_account} );
        $self->stash( sent_status => $json );
    }

    $self->stash( 'action' => $action );
    $self->render('show_candidate_action');
}

=head2 show_html

Show the body of the Nth HTML response from the $s3_log_id log chunk, where N
is $request_num, a zero based index

=cut

sub show_html{
    my ($self) = @_;

    my $s3_log_id   = $self->param('s3_log_id');
    my $request_num = $self->param('request_num');

    my $chunk_ref = $self->_get_log_chunk( $s3_log_id );
    return $self->render(
        text   => "No log chunk identified by '$s3_log_id' was found",
        status => 404
    ) unless $chunk_ref;

    # render the $request_num'th response from $$chunk_ref
    my $chunk_lines = $self->_log_chunk_to_lines( ${$chunk_ref} );
    for my $line ( @{$chunk_lines} ) {
        if ( exists $line->{request_num}
                && $line->{request_num} == $request_num ) {

            # inject a <base> tag into the response so images will be resolved
            # relative to the originally requested URL, and not the URL of
            # this current request
            my $base_href = $line->{http_response}->header('Base');
            my $base_tag  = qq/<base href="$base_href">/;
            my $html_str  = $line->{http_response}->decoded_content;
            $html_str     =~ s|<head>|<head>$base_tag|;

            return $self->render( text => $html_str );
        }
    }

    $self->render(
        text   => "No request number $request_num was found in log chunk '" .
                  $s3_log_id . "'.",
        status => 404
    );
}

=head2 show_search_record

Shows an aws_elastic_search , index threads search record.

=cut

sub show_search_record{
    my ($self) = @_;

    my $record_id = $self->param('record_id');
    my $log_id =  $self->param('log_id');
    my $stream2 = $self->app()->stream2();

    my $record;

    if ( $record_id ){
        $record = $stream2->aws_elastic_search->get( index => 'threads',
                                                     type => 'stream2search',
                                                     id => $record_id );
    }
    elsif ( $log_id ){
        my $results = $stream2->aws_elastic_search->search( index => 'threads',
                                                     type => 'stream2search',
                                                     body =>{
                                                        query => {
                                                            match => { s3_log_id => $log_id }
                                                        }
                                                     });

        $record = $results->{ hits }->{ hits }->[0];
    }

    unless( $record ){
        return $self->reply->not_found;
    }

    unless( $self->current_user()->contact_details()->{contact_devuser} ){
        my $req_url = $self->req->url;
        return $self->render(
            text   => '<h3>This page is restricted to internal Broadbean users.</h3>'
                    . "<p>You tried to access <a href=\"$req_url\">a log record</a> without an internal user cookie. "
                    . 'Please login to <a href="http://juice.adcourier.com" target="_blank">Juice</a> first.</p>'
                    . "<p>And then continue <a href='/?redirect_url=$req_url'>here</a></p>",
            status => 403
        );
    }

    $self->stash( record => $record );
    $self->stash( search_link => $self->url_for('/search/'.$record->{_source}->{criteria_id}.'/'.$record->{_source}->{destination} ) );


    # Try to get the full chunk from the appender.

    # # Lookup the 'chunk' appender of Log4perl. It
    # # contains the s3 storage bucket for our log chunks.
    my $chunk_ref = $self->_get_log_chunk( $record->{_source}->{s3_log_id} );
    if ( $chunk_ref ) {

        # so we can refer to hash entries (keys) starting with an underscore
        # in tt files
        $Template::Stash::PRIVATE = undef;
        $self->stash( 'log_lines',
            $self->_log_chunk_to_lines( ${$chunk_ref} ) );
    }

    $self->render('show_record');

}

sub show_search_record_s3_log {
    my ($self) = @_;
    my $s3_log_id = $self->param('s3_log_id');

    my $appender = Log::Log4perl->appender_by_name('SearchLogChunk');
    if ( $appender ) {
        my $bucket = $appender->store->bucket;
        my $object = $bucket->object(
            key         => $s3_log_id,
            expires     => DateTime->now()->add({ days => 1 })->ymd()
        );
        if ( $object->exists ) {
            return $self->redirect_to($object->query_string_authentication_uri());
        }
    }
    return $self->reply->not_found();
}

# given an S3 log chunk ID, returns a reference to a string of the contents
# of that S3 object, or undef if the log chunk can't be found
sub _get_log_chunk {
    my ($self, $s3_log_id) = @_;

    my $appender = Log::Log4perl->appender_by_name('SearchLogChunk');
    if ( $appender ) {
        my $bucket = $appender->store->bucket;
        my $object = $bucket->object( key => $s3_log_id );
        return \( $object->get() ) if $object->exists;
        $log->warn("No chunk ID: $s3_log_id");
    }
    else {
        $log->warn('No appender "SearchLogChunk" found in log4perl config');
    }
    return undef;
}

## Turns a log chunk into an arrayref of lines:
#
# [ { type => 'TRACE' , line => '....' }  , { type => ... , line => ... }, ... ]
#
#
#
# Lines can have extra properties:
#
#
# request_num: A request number in the log sequence.
#
# http_request: A HTTP::Request object.
#
# http_response: A HTTP::Response object.
#
#
sub _log_chunk_to_lines{
    my ($self, $chunk) = @_;

    my $request_num = 0;
    my $content_id = 0;

    my @lines = split(/^(TRACE|DEBUG|INFO|WARN|ERROR|FATAL|CRITICAL)/m , $chunk);
    my @lines_o = (); # Lines Hashes { type => ... , line => ... }


    # A first match is always empty (the chunk always starts with a priority marker).
    shift @lines;

    my $previous_type;
    while( 1 ) {
        my $type = shift @lines;
        my $line = shift @lines;
        unless( $type ){ last; }
        chomp($line);

        $type //= $previous_type;

        my $line_object = { type => $type , line => $type.$line };

        my ( $request_str , $response_str ) = ( $line =~ /=====\sREQUEST\s=====$(.+?)\n\n=====\sRESPONSE\s=====$(.+)/ms );
        if( $request_str && $response_str ){
            $log->info("GOT REQUEST/RESPONSE!");
            $line_object->{request_num} = $request_num++;

            my $http_request;
            my $http_response;

            eval{
                # trim initial newlines
                $request_str =~ s/^\n+//;
                $response_str =~ s/^\n+//;

                # $log->info("PARSING '$request_str'");
                $http_request = HTTP::Request->parse($request_str);

                # the content appears in the log as already un-gzipped so this header is superfluous
                $response_str =~ s/Content-Encoding: gzip\n//gi;

                $log->info("PARSING '".substr($response_str , 0 , 500 )."'");
                $http_response = HTTP::Response->parse($response_str);

                # Inject base.
                unless( $http_response->base() ){
                    $http_response->headers->header('Base' => $http_request->uri() );
                }

                if ( my $content_type = scalar($http_response->headers()->header('Content-Type')) ){
                    # Set the X-Content-Type to be one of html or xml or json
                    my ( $ctype_header ) = split(q/;\s*/ , $content_type);
                    $log->info("Response type is '$ctype_header'");
                    $ctype_header =~ s/\W/_/;
                    $http_response->headers()->header('X-Stream2-Content-Type' , $ctype_header );
                }
                $http_response->headers()->header('X-Stream2-Content-Id' , $content_id++ );
            };
            if( my $err = $@ ){
                $log->warn("Error building request/response objects: $err");
            }else{
                $line_object->{http_request} = $http_request;
                $line_object->{http_response} = $http_response;
                delete $line_object->{line};
            }

        }

        push @lines_o , $line_object;

        $previous_type = $type;
    }
    return \@lines_o;
}

1;

use strict;
use warnings;

package App::Stream2::Web::Auth;
use Mojo::Base 'Mojolicious::Controller';

use Log::Any qw/$log/;

sub login {
    my $self = shift;

    if ( $self->param('logged_out') ) {
        $self->stash('logged_out' => 1);
    }

    my $redirect_url = $self->session->{redirect_url};
    if ( $redirect_url && $redirect_url ne $self->url_for('/') ) {
        $self->stash('show_login_prompt' => 1);
    }

    $self->render('auth/login');
}

sub authenticate {
    my ($self) = @_;

    my $provider = $self->param('provider');
    unless( $provider ){
        return $self->render( status => 400, message => 'Missing provider parameter');
    }

    $log->info("In $self authenticate . Provider is $provider");
    my $login_url = $self->login_url({ provider => $provider });

    my $authenticate_user_promise = $self->authenticate_user({ provider => $provider });

    if ( $authenticate_user_promise ) {
        $self->render_later();
        return $authenticate_user_promise->then(
            sub {
                my $user_ref = shift;
                $self->user_sign_in($user_ref);

                my $redirect_url = delete $self->session->{redirect_url};
                return $self->redirect_to( $redirect_url || '/search' ); # Default entry point is search. The '/' would reconnect.
            },
            sub {
                $self->app->log->errorf( "Authentication failed: %s", shift."" );
                if ( $self->retry_login({ provider => $provider }) ){
                    $self->stash(login_error => 1);
                    return $self->redirect_to( $login_url );
                }
                my $s2 = $self->app->stream2;
                $self->stash(
                    i18n => sub { $s2->translations->__(@_) }
                );
                return $self->render( template => 'oauth-error', status => 500 );
            }
        )->then(sub{ return shift ; },
                sub{
                    $self->app->log->errorf( "Signing user in failed: %s", shift."" );
                    return $self->render( template => 'oauth-error', status => 500 );
                });
    }

    if ( $self->retry_login( { provider => $provider } ) ){
        $self->stash(login_error => 1);
        return $self->redirect_to( $login_url );
    }

    return $self->render( text => "Authentication failed" );
}

=head2 popuser

Pops the current user, going back to the original admin/overseer user.

=cut

sub popuser{
    my ($self) = @_;
    my $original_user = $self->original_user() // die "No original user to pop to";
    unless( $original_user->provider() eq 'adcourier' ){
        die "We can only pop to an adcourier identity provider user. We have ".$original_user->provider();
    }

    $self->become_adcourier_user( $original_user->provider_id() , { poping_user => 1 } );

    $self->redirect_to($self->url_for('/welcome-admin'));
}

sub login_as{
    my ($self)  = @_;
    my $adc_user_id = $self->param('adc_user_id');
    my $redirect_url = $self->param('redirect_to') || $self->url_for('/search').'';

    # This will set the session to this new user.
    eval {
        $self->become_adcourier_user($adc_user_id);
    };
    if ( $@ ){

        $self->app->log->errorf( "Admin user switch failed: %s", $@."" );

        my $s2 = $self->app->stream2;
        my $current_user = $self->current_user;
        $self->stash( # maybe this should be moved to before_render if the output is HTML
            i18n                => sub { $s2->translations->__(@_) },
            i18nx               => sub { $s2->templates->i18nx(@_) },
            new_user_id         => $adc_user_id,
            current_user_id     => $current_user->id,
            current_user_name   => $current_user->contact_name
        );
        return $self->render( 'invalid-session' );
    }

    $self->redirect_to($redirect_url);
}



sub logout {
    my $self = shift;
    $self->user_sign_out();
    my $logout_url = $self->logout_url();
    $self->redirect_to($logout_url);
}

# we need to logout the user without redirecting to our login page.
# we do not want the user to see the search/adcourier login page WITHIN CSP
sub logout_sso {
    my $self = shift;
    $self->user_sign_out();
    return $self->render(json => { message => 'User logged out from Search'}, status => 200);
}

1;

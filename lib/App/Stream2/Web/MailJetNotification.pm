package App::Stream2::Web::MailJetNotification;


use Mojo::Base 'Mojolicious::Controller';

# https://dev.mailjet.com/guides/#event-api-real-time-notifications

=head2 DESCRIPTION

To handle push notifications from MailJet about the emails we send out. You
can get more detailed information about mail transactions by going to:

    curl -XGET "https://api.mailjet.com/v3/REST/messagesentstatistics/<MessageID>?ShowExtraData=1" --user '.....'


=head2 receive_notification

receives the mail-jet notification, returns quickly and defers all the
logic database stuff so mail-jet is not hanging around for a reply

=cut

sub receive_notification {
    my ( $self ) = @_;
    my $transport_method = $self->param('transport_method') // 'transport';

    my $config = $self->config();
    unless ( grep { $transport_method eq $_ } keys(%{$config->{email}}) ) {
        return $self->render( json => { message => 'Unknown transport method' }, status => 400 );
    }

    # NOTE We have to break $request_ref out, as the underlying TX object is
    # usually a weak ref, meaning if the connection closes before the next tick
    # starts things will break.

    # use our stream2 json to decode the message otherwise the messageid gets
    # truncated into exponential notation
    my $request_ref = $self->app->stream2->json->decode( $self->req->body() );
    Mojo::IOLoop->next_tick(sub {
        return $self->process_notification( $transport_method, $request_ref );
    });

    return $self->render( json => { message => 'OK' }, status => 200 );
}

=head2 process_notification

processes the mail-jet notification.

Currently we are only interested in bounce spam blocked events, all other
requests are ignored.

Spam and blocked are automatically added to the email blacklist,
whereas bounces and preblocks are only added to the block list once they hit a given
threshold check the config, defaults are five or more in seven days.

=cut

sub process_notification {
    my ( $self, $transport_method, $request_ref ) = @_;
    my $app = $self->app();

    # return fast if we are not interested in the current event.
    my ( $event ) = grep { $request_ref->{event} eq $_ } qw/bounce spam blocked/ or return;
    my $config = undef;

    if ( $event eq 'bounce' && ! $request_ref->{hard_bounce} ) {
        $config->{ notification } = 'bounce.soft';
        my $bounce_policy = $app->config->{email}->{bounce_policy};
        $config->{ max_attempts } = $bounce_policy->{max_bounces_within_bounce_period} // 5;
        $config->{ time_period  } = $bounce_policy->{bounce_period} // { days => 7 };
    }
    elsif ( $request_ref->{error} =~ /preblock/ ){
        $config->{ notification } = 'preblock';
        my $bounce_policy = $app->config->{email}->{preblock_policy};
        $config->{ max_attempts } = $bounce_policy->{max_bounces_within_bounce_period} // 5;
        $config->{ time_period  } = $bounce_policy->{bounce_period} // { days => 7 };
    }

    $config->{ latest_email_notification } = $app->stream2()->factory('EmailNotification')->create({
        email               => $request_ref->{email},
        notification        => $config->{notification} // $event,
        notification_body   => $request_ref,
        transport_method    => $transport_method
    });

    if ( ( $config->{ notification } // 'UNDEF' )  =~ /^preblock|^bounce\.soft/ ) {
        $self->check_against_limit( $request_ref, $config );
    }
    else {
        my $message_id = $request_ref->{MessageID} // 'UNDEF';
        $app->log()->warnf("Email notification %s (%s): %s %s",
            $event,
            $request_ref->{CustomID} // $message_id,
            $request_ref->{error} // $event,
            $request_ref->{email} // 'NO EMAIL'
            );

        $app->stream2->factory('EmailBlacklist')->find_or_create({
            email   => $request_ref->{email},
            purpose => sprintf("delivery.$event")
        });
    }

    return;
}

=head2 check_against_limit

Both soft bounces and preblocks are ignored untill they reach a defined limit.
This checks against that limit and only adds to blacklist when reached.

=cut

sub check_against_limit {
    my ( $self, $request_ref, $config ) = @_;
    my $app = $self->app();

    my $notification   = $config->{notification};
    my $max_attempts   = $config->{max_attempts};
    my $time_period    = $config->{time_period};
    my $latest_email_notification = $config->{latest_email_notification};
    my $attempts_count = 0;

    my $time_period_datetime = $latest_email_notification->insert_datetime->subtract(
        %{$time_period}
    );

    my $attempts = $app->stream2()->factory('EmailNotification')->search(
        {
            email => $request_ref->{email},
            notification => $notification,
        },
        {
            rows => $max_attempts,
            order_by => { -desc => 'id' }
        }
    );

    while ( my $row = $attempts->next ){
        $attempts_count++ if
            1 == $row->insert_datetime->compare( $time_period_datetime );
    }

    if ( $attempts_count >= $max_attempts ){
         $app->log()->warnf( "%s has been blocked because of too many $notification".'s',
            $request_ref->{email}
        );

        $app->stream2->factory('EmailBlacklist')->find_or_create({
            email           => $request_ref->{email},
            purpose         => $notification eq 'bounce.soft' ? 'delivery.softbounceblock' : 'delivery.'.$notification,
        });
    }
}   

1;

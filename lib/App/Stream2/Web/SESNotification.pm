use strict;
use warnings;

package App::Stream2::Web::SESNotification;

use Mojo::Base 'App::Stream2::Web::SNSNotification';

use Data::Dumper;

=head1 DESCRIPTION

SESNotification contains the end point for receiving Amazon SES Notification SNS Messages over HTTP(s)

See L<http://docs.aws.amazon.com/sns/latest/dg/SendMessageToHttp.html> (for the SNS messages)

L<http://docs.aws.amazon.com/ses/latest/DeveloperGuide/configure-sns-notifications.html> (For the SES notification over SNS)

=cut

sub receive_sns_notification {
    my ( $self ) = @_;
    my $app = $self->app();
    return $self->with_notification(
            sub{
                my $message = shift;
                my $email_notification = $app->stream2()->json()->decode( $message->{Message} );
                my $notification_type = $email_notification->{notificationType};
                if( grep{ $notification_type eq $_ } ( 'Bounce' , 'Complaint' ) ){
                    # Save notifications in the table
                    my $recipients = $email_notification->{bounce}->{bouncedRecipients} ||
                        $email_notification->{complaint}->{complainedRecipients} || [];
                    foreach my $email ( map{ $_->{emailAddress}; } @{$recipients} ){
                        $app->stream2()->factory('EmailNotification')->create({ email => $email,
                                                                                notification => $notification_type,
                                                                                notification_body => $email_notification
                                                                            });
                    }
                    $app->log()->error("Email notification $notification_type: ".$message->{Message});
                }
                # Note we dont do anything special about normal notification delivery
                return $self->render( json => { message => 'OK' } );
            });
}

1;

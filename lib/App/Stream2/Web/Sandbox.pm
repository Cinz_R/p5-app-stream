package App::Stream2::Web::Sandbox;

use strict;
use warnings;

use Carp;

use Mojo::Base 'App::Stream2::Controller';
use Log::Any qw/$log/;

use JSON;

sub index{
    my ($self) = @_;
    return $self->render('sandbox');
}

1;

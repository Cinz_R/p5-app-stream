use strict;
use warnings;

package App::Stream2::Web::FeedOAuth;
use Mojo::Base 'Mojolicious::Controller';

use Crypt::JWT ();
use Stream2::Criteria;

use Log::Any qw/$log/;

=head1 NAME

App::Stream2::Web::FeedOAuth

=head1 DESCRIPTION

Controller for feeds to specify oauth routes (e.g. callbacks to store tokens).

=head1 SYNOPSIS

    $self->session('user_id' => $self->param('current_user'));
    my $current_user = $self->current_user();

    my $code = $self->param('code');

    $current_user->set_cached_value('feed_temporary_code', $code, 60);
    return $self->render( template => 'path/to/landing_page' );

=head2 twitter

Processes the request to allow access into Twitters API in the given users context.

Acts as the callback url route for the Twitter OAuth process from AdCourier. Receives the
access tokens given from Twitter and attempts to update them in the twitter oauth table
in AdCourier.

Don't call this route unless you've received a response from Twitter indicating the
users token has expired, otherwise you will be wasting everyones time.

=cut

sub twitter {
    my ($self)          = @_;
    my $redirect_route  = "/search/%s/twitter/page/%d";
    my $jwt             = $self->param('jwt');
    my $claims          = eval { Crypt::JWT::decode_jwt( token => $jwt, key => 'stream' ); };
    if ( $@ ) {
        # ease the users struggle to re-authenticate by providing a link back to search on twitter
        my $criteria = Stream2::Criteria->new( schema => $self->app->stream2->stream_schema );
        $criteria->save();
        my $redirect_url = $self->url_for( sprintf($redirect_route, $criteria->id(), 1) )->to_abs;
        return $self->render(
            exception => $self->__("Could not validate request, visit the following url to try again:"),
            link => { src => $redirect_url },
            status => 400
        );
    }

    my $page         = $claims->{page} // 1;
    my $provider_id  = $claims->{provider_id};
    my $criteria_id  = $claims->{criteria_id};
    my $access_token = $self->param('twitter_access_token');
    my $token_secret = $self->param('twitter_access_token_sec');

    # do the update in the source, doesn't return anything special
    $self->app->stream2->stream_api->update_oauth_tokens(
        $provider_id,
        'twitter',
        {
            access_token => $access_token,
            access_token_secret => $token_secret,
        }
    );

    my $redirect_url = $self->url_for( sprintf($redirect_route, $criteria_id, $page) )->to_abs;
    return $self->redirect_to($redirect_url);
}

sub verify {
    my $self = shift;

    my $json_ref = $self->stash('query_ref');
    my $claim = Crypt::JWT::decode_jwt(token => $json_ref->{token}, key => $self->config->{cb_oneiam}->{client_secret});

    $self->app()->log()->info("Testing ripple_user=".$json_ref->{ripple_user}."-VS-".$claim->{email}.".");

    # Note: Case should not matter.
    unless( lc( $claim->{email} ) eq lc( $json_ref->{ripple_user} ) ){
        return $self->render( json => { message => 'CB user does not match the currently logged in user.' }, status => 500 );
    }

    return $self->render(json => { message => 'User Verified'}, status => 200);
}

1;

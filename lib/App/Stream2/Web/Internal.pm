package App::Stream2::Web::Internal;

use strict;
use warnings;

use Carp;

use Mojo::Base 'App::Stream2::Controller';
use Log::Any qw/$log/;

use JSON;

sub requires_basic_auth { 1; }

sub qurious_stats {
    my ( $self ) = @_;

    my @queues = map {
        { name => $_, count => $self->app->_redis->llen( $self->app->_queue->queue_key($_) ) }
    } $self->_get_qurious_queues();

    return $self->render( 
        data => to_json (
            {
                queues => \@queues,
                redis_keys => scalar( $self->app->_redis->keys('*') ),
                redis_info => $self->app->_redis->info()
            },
            {
                pretty => 1
            }
        ),
        format => 'json'
    );
}

sub _get_qurious_queues {
    my ( $self ) = @_;
    my %queues;
    foreach my $worker ( @{$self->app->config()->{workers_arguments}} ){
        my $queues = $worker->[0];
        map { $queues{$_} = 1 } @{$queues};
    }
    return keys %queues;
}


1;

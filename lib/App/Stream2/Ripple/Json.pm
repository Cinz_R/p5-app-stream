package App::Stream2::Ripple::Json;

use strict;
use warnings;

use Carp;

use Mojo::Base 'App::Stream2::Controller';
use Log::Log4perl;

use Bean::Locale;

use Data::Dumper;
use Digest::MD5;

use List::Util;
use Log::Any qw/$log/;

use Stream2::Criteria;
use Stream2::FeedTokens;
use Stream2::Types;

use Crypt::CBC;
use MIME::Base64;

use AnyEvent::Promises;

use JSON;

has 'valid_settings' => sub {
    return [qw/
        no_adcourier_shortlist
        show_adcourier_navigation
        hide_forward
        hide_download
        hide_save
        hide_message
        hide_chargeable_message
        hide_favourite
        hide_tagging
        hide_save_internal
        cb_oneiam_user
    /];
};

=head2 status

    Check on the status using your given job_id

    GET /ripple/status
    Accept: application/json
    Content-Type: application/json
    x-api-key: 1097103005
    x-ripple-session-id: U2FsdGVkX1/+jbVzDcdgBLHqHcCdc8cI51vbUZcSALw5vEo183soueGzxcAe87H0B5k5EFdE43CjpbicPr2omw==

=cut

sub status {
    my ( $self ) = @_;

    # Do not emit cookie, the session
    # management will rely solely on
    # the x-ripple-session-id headers.
    $self->app()->sessions()->state()->no_cookie_please( $self );

    unless( $self->session( 'json_ripple' ) ){
        return $self->render( json => { error => "Invalid Request", message => "Session is invalid for this type of request" }, status => 400 );
    }

    my $job_id = $self->session('json_ripple')->{job_id}
        or confess "job_id not found in session.json_ripple";

    my $job = $self->retrieve_job($job_id);
    my $result_status = $job->status;

    if ( my $result = $job->result ) {

        # only return FeedTokens if there was no error.
        unless ( exists($result->{error} )) {

            # GET /api/board/totaljobs returns static facets as part of its
            # response. Let's avoid calling it and *just* get those static facets
            my $board_name = $job->parameters->{template_name};
            my $feed       = Stream2::FeedTokens->new(
                locale => $self->current_user->locale, board => $board_name
            );
            my $static_facets = $feed->tokens;

            # merge static facets in with the dynamic ones
            my $dynamic_facets = $result->{result}->{facets} //= [];
            for my $sf ( @{$static_facets} ) {
                unless ( List::Util::any { $_->{Id} eq $sf->{Id} } @{$dynamic_facets} ) {
                    push @{$dynamic_facets}, $sf;
                }
            }
        }

        $self->render(
            json => { status => $result_status, context => $result }
        );
    }
    else {
        $self->render( json => { status => $result_status, context => {} } );
    }
}

=head2 results

    Retrieve results associated with a job_id

    GET /ripple/results
    Accept: application/json
    Content-Type: application/json
    x-api-key: 1097103005
    x-ripple-session-id: U2FsdGVkX1/+jbVzDcdgBLHqHcCdc8cI51vbUZcSALw5vEo183soueGzxcAe87H0B5k5EFdE43CjpbicPr2omw==

=cut

sub results {
    my ( $self ) = @_;

    unless( $self->session( 'json_ripple' ) ){
        return $self->render( json => { error => "Invalid Request", message => "Session is invalid for this type of request" }, status => 400 );
    }

    my $job_id = $self->session('json_ripple')->{job_id}
        or confess "job_id not found in session.json_ripple";

    my $results_ref = $self->_fetch_results( $job_id );

    return $self->render(
        json => $results_ref
    );
}

sub _fetch_results {
    my ( $self, $job_id ) = @_;

    # Do not emit cookie, the session
    # management will rely solely on
    # the x-ripple-session-id headers.
    my $state = $self->app()->sessions()->state();
    $state->no_cookie_please( $self );

    my $results = Stream2::Results::Store->new( id => $job_id, store => $self->app->_redis );
    unless( $results->fetch() ){
        confess("Result $job_id is GONE");
    }

    my $render_results = $self->param('render_results');

    my $url_lifetime = $self->param('url_lifetime') || 900; # Default to 15 minutes.

    my $results_url =
      $self->url_for( '/search/'
          . ( $self->session('json_ripple')->{criteria_id} // '' ) . '/'
          . ( $results->destination() // '' )
          . '/page/1' );
    $results_url->query( 'search_id' => $job_id );

    my @results_arr = ();
    my $session_token = $state->session_token( $self, $url_lifetime );

    foreach my $result ( @{ $results->results() } ) {
        # fixup the candidate
        $result->fixup;

        my $profile_url = $results_url->clone();

        $profile_url->query()->merge( candidate_idx => $result->idx() );
        $profile_url = $state->sessionize_url(
            $self,
            $profile_url,
            $session_token
        )->to_abs()->to_string();

        my %actions = map {
            $_ => $state->sessionize_url(
                $self,
                $self->url_for( sprintf('/results/%s/%s/candidate/%s/%s', $job_id, $results->destination, $result->idx(), $_) ),
                $session_token
            )->to_abs()->to_string();
        } qw/profile profile_history cv/;

        push @results_arr, {
            %{ $result->to_ref },
            profile_url => $profile_url,
            actions => \%actions,
            (
                $render_results
                ? ( html_result => $self->_render_candidate_html($result) )
                : ()
            )
        };
    }

    return {
        facets      => $results->facets,
        results     => \@results_arr,
        results_url => $state->sessionize_url( $self, $results_url, $session_token )->to_abs()->to_string()
    };
}

sub _render_candidate_html {
    my ( $self, $candidate ) = @_;
    my $template     = "stream/results/body";
    my $current_user = $self->current_user();

    ## Horrible hack to output CV Library
    ## Session liberation 1px images (look in the CV library template).
    my $cvlibrary_session_id = undef;
    if ( $candidate->destination() eq 'cvlibrary' ) {
        # This thing would have been set against the user by the backend
        # cvlibrary async template job.
        $cvlibrary_session_id =
          $current_user->get_cached_value('cvlibrary_session_id');
    }

    return $self->app->stream2->templates->result_body(
        {
            L =>
              sub { Bean::Locale::localise_in( $current_user->locale(), @_ ); },
            candidate => $candidate->to_ref(),
            (
                $cvlibrary_session_id
                ? ( cvlibrary_session_id => $cvlibrary_session_id )
                : ()
            ),
        },
    );

}

=head2 login

For creating a Search session which will allow further calls to the Search API without having to provide login details

Usage

    POST /ripple/login

    Accept: application/json
    Content-Type: application/json

    {
        "api_key":"1097103005",
        "account": {
            "username":"pat@pat.pat.pat",
            "signature": {
                "hash": "39ce30deacac4ba833db75771244ad3fdc26d09bac422d9f5c9ead16898173e8",
                "time": 1424446101000
            },
            "oneiam": "cbuser@careerbuilder.com",
        },

        # Will save a session value to indicate you are in the 'responses' search_mode.
        # *For adcourier providers only
        manage_responses_view => 1,

        # If the user has a current session, this parameter will log them in again
        # to a new session. Useful for switching between responses/search modes.
        refresh_session => 1,
    }

Out:

    Content-Type: application/json

    {
        status => "OK",
        ripple_session_id => 'xxxxxxxxx'
    }

=cut

sub login {
    my ( $self ) = @_;
    # Do not emit cookie, the session
    # management will rely solely on
    # the x-ripple-session-id headers.
    $self->app()->sessions()->state()->no_cookie_please( $self );

    my $json = $self->req->body;
    my $stream2 = $self->app->stream2;

    my $request_ref = eval { $self->app->stream2->json->decode( $json ); };
    if ( $@ ) { return $self->render( json => { error => "Invalid JSON", message => "$@" }, status => 400 ); }

    my $ripple_login_promise = $self->_ripple_login( $request_ref )
        or return;
    $self->render_later();
    return $ripple_login_promise->then(
        sub {
            my $session_state = $self->app()->sessions()->state();
            my $stok = $session_state->session_token( $self, 360 );
            return $self->render( json => {
                status              => "OK",
                session_url         => $session_state->sessionize_url( $self, $self->url_for('/search') , $stok )->to_abs()->to_string(),
                STOK                => $stok,
                ripple_session_id   => $session_state->sign_value( $self, $self->session_id() ),
            });
        },
        sub {
            my $error = shift // 'Unknown error';
            return $self->render( json => { error => { status => "Unauthorised", message => $error } }, status => 401 );
        }
    );
}


#
# Return a promise resolved with the logged in user, and rejected with any error.
#
# OR returns undef when some parameter is missing. Note that in this
# case, the appropriate error message would already have been output to
# mojolicious. So you HAVE to test for this function's return value.
#
sub _ripple_login {
    my ( $self, $request_ref ) = @_;

    unless ( $request_ref->{refresh_session} ) {
        if ( my $current_user = $self->current_user() ){
            my $d = AnyEvent::Promises::deferred;
            $d->resolve($current_user);
            return $d->promise;
        }
    }

    # JSON Validation
    my $api_key = $request_ref->{api_key} or return $self->_missing_field( 'api_key' );
    my $account = $request_ref->{account} or return $self->_missing_field( 'account' );

    my $provider = $account->{provider} // 'adcourier_ats';
    my $login_with_provider  = '_login_with_'.$provider;

    $log->info("Will JRipple login using method ".$login_with_provider);

    return $self->$login_with_provider( $request_ref );
}

=head2 update_session

Same user, new search settings. This route is mainly to cut down
on the overhead of using the AdC OAuth service unnecessarily each time
the API consumer wishes to change the Search context.

Usage:

    POST /ripple/update_session
    x-ripple-session-id: <session_id>
    Content-Type: application/json

    {
        config => {
            .. see apiary docs for more info ..
        },
        custom_fields => [
            {
                name => 'custom_field_1',
                content => 'abc'
            },
            {
                name => 'custom_field_2',
                content => 123
            }
        ]
    }

=cut

sub update_session {
    my ( $self ) = @_;

    my $current_user = $self->current_user();
    unless( $current_user ){
        return $self->render( json => { error => "Unauthorised" }, status => 401 );
    }

    my $json = $self->req->body;
    my $request_ref = eval { $self->app->stream2->json->decode( $json ); };

    if ( my $config = $request_ref->{config} ) {
        my $ripple_settings = {
            custom_lists => $config->{shortlists} // [],
            ( $config->{new_candidate_url} ? ( new_candidate_url => $config->{new_candidate_url} ) : () ),
            ( $config->{stylesheet_url} ? ( stylesheet_url => $config->{stylesheet_url} ) : () ), 
            ( $config->{tagged_doc_type} ? ( tagged_doc_type => $config->{tagged_doc_type} ) : () ),
        };

        foreach my $valid_setting ( @{$self->valid_settings} ){
            if( defined ( $config->{$valid_setting} ) ){
                $ripple_settings->{$valid_setting} = $config->{$valid_setting};
            }
        }

        $self->session->{ripple_settings} = $ripple_settings;
    }

    if ( my $custom_fields = $request_ref->{custom_fields} ) {
        $self->session->{ripple_settings} //= {};
        $self->session->{ripple_settings}->{custom_fields} = $custom_fields;
    }

    return $self->render( data => '' );
}

sub _login_with_third_party{
    my ($self, $request_ref) = @_;

    my $api_key = $request_ref->{api_key};
    my $account = $request_ref->{account};

    my $jwt_token = $account->{jwt_token} or return $self->_missing_field( 'account/jwt_token' );

    my $provider = $self->auth_provider({ provider => 'third_party'});

    my $secret_key = $provider->secret_key_for_api_key( $api_key ) or return $self->_missing_field('api_key', 'Invalid or inactive api_key');

    my $decoded_content = Crypt::JWT::decode_jwt( token => $jwt_token,
                                                  key => $secret_key,
                                              );
    # Merge the decoded_content in the account.
    $account = { %$account , %$decoded_content };

    my $provider_id    = $account->{provider_id} or return $self->_missing_field( 'account/provider_id' );
    my $group_identity = $account->{group_identity} or return $self->_missing_fied( 'account/group_identity' );
    if( length( $group_identity ) > 40 ){
        return $self->_missing_field('account/group_identity' , 'Too long. Should be <= 40 chars');
    }
    my $group_nice_name = $account->{group_nice_name} // $group_identity;

    # Prefix the group identity AND the provider_id with the Prefix TP (Third Party) and 6 chars of the API Key.
    # This is to avoid different API applications to collide on their group_identity and users.
    my $prefix = 'TP'.substr( Digest::MD5::md5_hex( $api_key ), 0 , 6 );

    $group_identity = $prefix.'-'.$group_identity;
    $provider_id    = $prefix.'-'.$provider_id;

    # Just so we know for the future.
    # Possible contact details keys are qw/contact_name contact_email locale timezone currency distance_unit/
    my %contact_details = map {
        $account->{$_} ? ( $_ => $account->{$_} ) : ()
    } qw/contact_name contact_email locale timezone currency distance_unit/;

    my $subscriptions_ref = $account->{subscriptions} // {};

    # remove internal subscriptions
    delete $subscriptions_ref->{$_} && $log->warnf("Delete %s from third_party subscripitons",$_)
        for $self->app->stream2->factory('Board')->list_internal_boards();

    return
        AnyEvent::Promises::make_promise(
            sub{
                $self->user_sign_in({
                    provider        => {
                        user_id         => $provider_id,
                        identity_provider => 'third_party',
                        login_provider  => 'third_party',
                        group_identity  => $group_identity,
                        group_nice_name => $group_nice_name,
                        navigation      => [], # No tabs please.
                    },
                    ripple_settings => { api_key => $api_key },
                    settings => { "can_forward_to_any_email_address" => JSON->true(),
                                  "has_adcourier_v5" => JSON->true()
                              },
                    contact_details => \%contact_details,
                    subscriptions   => $subscriptions_ref,
                });
                $self->app()->log()->info("Session now contains a user");
                return $self->current_user;
            });
}

#
# Returns what _ripple_login should return, but only when request.account.provider = adcourier_ats
#
sub _login_with_adcourier_ats {
    my ($self, $request_ref) = @_;

    # Those two things presence are already enforced by _ripple_login.
    my $api_key = $request_ref->{api_key};
    my $account = $request_ref->{account};

    my $username = $account->{username} or return $self->_missing_field( 'account/username' );

    my $signature = $account->{signature};
    my $password = $account->{password};

    return $self->_missing_field( 'account/signature OR account/password' ) unless ( defined $signature || defined $password );

    if ( $signature ) {
        $signature->{hash} or return $self->_missing_field( 'account/signature/hash' );
        $signature->{time} or return $self->_missing_field( 'account/signature/time' );
    }

    my $grant_params = {
        api_key     => $api_key,
        username    => $username,
    };

    if ( $password ) {
        $log->info('Will attempt login using username/password');
        $grant_params->{password} = $password;
    } else {
        $log->info('Will attempt login using username/signature');
        $grant_params->{signature} = $signature->{hash};
        $grant_params->{time_ms} = $signature->{time};
    }

    # Only adcourier providers are able to login to Responses
    my $in_responses_view = $request_ref->{manage_responses_view} // 0;

    # we want to dynamically get the navigation bars for a user, we need to ask
    # for them, and not assume we have asked for them previously.
    my $oauth_client = $self->auth_provider({ provider => 'adcourier_ats'})->oauth_client;

    if ( $request_ref->{config}->{show_adcourier_navigation} ) {
        my @scopes = split /,/, $oauth_client->scope_string;
        unless ( List::Util::any { $_ eq 'read_navigation_tabs' } @scopes ) {
            push @scopes, 'read_navigation_tabs';
        }
        $grant_params->{scope} = join( ',', @scopes );
    }

    # Right, we do have some credentials in $grant_params
    # We need to use the oauth client to get some user info and other magic.
    return $oauth_client->credentials_grant( %$grant_params )->then(
        sub{
            my $grant_response = shift;
            my $config = $request_ref->{config} // {};
            my $ripple_settings = { custom_lists => $config->{shortlists} // [],
                                    ( $config->{new_candidate_url} ? ( new_candidate_url => $config->{new_candidate_url} ) : () ),
                                    ( $config->{stylesheet_url} ? ( stylesheet_url => $config->{stylesheet_url} ) : () ),
                                    ( $config->{tagged_doc_type} ? ( tagged_doc_type => $config->{tagged_doc_type} ) : () ),
                                };
            my $custom_fields = $request_ref->{custom_fields} // [];

            foreach my $valid_setting ( @{$self->valid_settings} ){
                if( defined ( $config->{$valid_setting} ) ){
                    $ripple_settings->{$valid_setting} = $config->{$valid_setting};
                }
            }

            my $subscriptions = {  %{ $grant_response->{user}->{read_stream_subscriptions} || {} } };
            my $navigation = []; # no tabs please

            if ( $ripple_settings->{show_adcourier_navigation} ) {
                $navigation = $self->auth_provider({ provider => 'adcourier' })->_filter_tabs(
                    $grant_response->{user}->{read_navigation_tabs},
                    {
                        manage_responses_view => $in_responses_view
                    }
                );
            }

            my $currencies = $grant_response->{user}->{read_currencies} || [ qw/GBP USD EUR/ ];
            $self->user_sign_in({
                    provider        => {
                        user_id         => $grant_response->{user}->{user_id},
                        identity_provider => 'adcourier',
                        login_provider  => 'adcourier_ats',
                        group_identity  => $grant_response->{user}->{read_contact_details}->{company},
                        navigation      => $navigation,
                    },
                    ripple_settings => $ripple_settings,
                    custom_fields   => $custom_fields,
                    settings => ( delete $grant_response->{user}->{read_contact_details}->{settings} ) // {},
                    contact_details => $grant_response->{user}->{read_contact_details},
                    subscriptions   => $subscriptions,
                    manage_responses_view => $in_responses_view,
                    currencies => $currencies,
                });
            $self->app()->log()->info("Session now contains a user");
            return $self->current_user;
        },
        sub {
            # pass on the error, a die is needed to prevent this being resolved
            # in the next 'then' block
            die( shift )
        }
    );

}

=head2 search

    can be called by sending a request to /ripple with content type json

    POST /ripple/search

    Accept: application/json
    Content-Type: application/json

    {
        "api_key":"1097103005",
        "account": {
            "username":"pat@pat.pat.pat",
            "signature": {
                "hash": "39ce30deacac4ba833db75771244ad3fdc26d09bac422d9f5c9ead16898173e8",
                "time": 1424446101000
            },
            "oneiam": "cbuser@careerbuilder.com"
        },
        "channel": {
            "type": "external",
            "name": "talentsearch"
        },

        # Will load Search in Responses mode
        # NOTE: this will replace your channel(s) with the Responses channel only
        manage_responses_view => 1,

        # Logs the user in again even if they have a valid session
        refresh_session => 1,
    }

=cut

sub search {
    my ( $self ) = @_;

    # if no promise has been returned then we have
    # to assume that an error has been rendered
    my $promise = $self->_init_search()
        or return;

    $promise->then(
        sub {
            my $search_ref = shift;
            my ( $job, $criteria ) = @{$search_ref}{qw/job criteria/};

            $self->session( json_ripple => { job_id => $job->guid , criteria_id => $criteria->id()  } );
            my $session_state = $self->app()->sessions()->state();

            return $self->render(
                json => {
                    ripple_session_id   => $session_state->sign_value( $self, $self->session_id() ),
                    # A link to the same search  that's only valid for 6 minutes ( 6 * 60 secs )
                    search_url => $session_state->sessionized_url(
                        $self,
                        $self->url_for('/search/'.$criteria->id().'/'.$job->parameters->{template_name}),
                        360
                    )->to_abs()->to_string(),
                    # A link to the status of this search that's only valid for one minute.
                    status_url => $session_state->sessionized_url(
                        $self,
                        $self->url_for('/ripple/status'),
                        60
                    )->to_abs()->to_string()
                }
            );
        },
        sub {
            my $error = shift // 'Unknown error';
            return $self->render( json => { error => { status => "Unauthorised", message => $error } }, status => 401 );
        }
    );
}

# Logs the user in, sets up the criteria and initialises the
# backend job. Note, should not return anything but the promise,
# as it handles pre-search errors itself.
sub _init_search {
    my ( $self ) = @_;

    # Do not emit cookie, the session
    # management will rely solely on
    # the x-ripple-session-id headers.
    $self->app()->sessions()->state()->no_cookie_please( $self );

    my $json = $self->req->body;
    my $stream2 = $self->app->stream2;

    my $request_ref = eval { $self->app->stream2->json->decode( $json ); };
    if ( $@ ) { $self->render( json => { error => { status => "Invalid JSON", message => "$@" } }, status => 400 ) and return; }

    # _ripple_login does a fair amount of param checking and deals with its own rendering of errors.
    my $ripple_login_promise = $self->_ripple_login( $request_ref )
        or return;

    $self->render_later();
    return $ripple_login_promise->then(
        sub {
            my $user = shift;
            # Channel, should either be type=internal or a name of an external board
            my $channel = $request_ref->{channel} or return $self->_missing_field( 'channel' );
            my $channel_type = $channel->{type} || 'external'; # Default channel type is external.

            if ( $self->session('manage_responses_view') ) {
                $log->debugf('Overwriting channel to be adcresponses as user is in manage_responses_view, ' .
                    'original channel and channel_type were "%s" and "%s" respectively', $channel, $channel_type);
                $channel_type = 'internal'; # don't need to guess by channel type, we know it is internal
                $channel->{name} = 'adcresponses';
            }

            # Setting the ripple settings so feeds that need access to a custom_list will have
            # the adc_shortlist one by default. No other ones are implemented so far.
            # This is to fix https://broadbean.atlassian.net/browse/SEAR-934

            unless( defined $request_ref->{query}->{cv_updated_within} ){
                $request_ref->{query}->{cv_updated_within} = $user->settings_values_hash()->{'criteria-cv_updated_within-default'};
                $log->info("Falling back to cv_updated_within ='".$request_ref->{query}->{cv_updated_within}."'");
            }
            unless( defined $request_ref->{query}->{include_unspecified_salaries} ) {
                $request_ref->{query}->{include_unspecified_salaries} = 1;
                $log->info("Setting include_unspecified_salaries to true as is the default for the front-end");
            }

            my $criteria = $self->_criteria_from_json( $request_ref->{query}, $user );
            my $search_options = $request_ref->{config} || {};

            # One can only search all channels if a corollary candidate action is provided.
            if ( $channel_type eq 'all' && $request_ref->{candidate_action}->{class} ){
                return $self->automatic_search( $criteria, $user, $request_ref, $search_options );
            }

            my $job = $self->single_search( $criteria, $user, $request_ref, $search_options );
            return {
                job => $job,
                criteria => $criteria
            };
        },
        sub{
            die ( shift );
        }
    );
}

=head2 automatic_search

Run a search accross all subscribed boards, should have a follow up action

Usage:

    $this->automatic_action(
        $criteria_obj,
        $user_obj,
        {
            candidate_action => {
                class => 'Stream2::Action::ImportCandidate',
                options => {
                    candidate_attributes => {
                        job_requisition_ids => [ 'csp_job_id:12345' ]
                    }
                }
            }
        }
    );

=cut

sub automatic_search {
    my ( $self, $criteria, $user, $request_ref, $search_options ) = @_;

    my $candidate_action = $request_ref->{candidate_action} or return $self->_missing_field('candidate_action');

    my $remote_ip = $self->remote_ip();
    my $base_url = $self->url_for('/')->to_abs()->to_string();
    my $guid_ref = {};
    do {
        $guid_ref->{$_} = $self->queue_job(
            class => 'Stream2::Action::Search',
            parameters   => {
                criteria_id     => $criteria->id(),
                template_name   => $_,
                options         => {
                    %{$search_options},
                    candidate_action => $candidate_action,
                },
                user        => {
                    user_id         => $user->id,
                    group_identity  => $user->group_identity,
                    auth_tokens     => $user->subscriptions_ref->{$_}->{auth_tokens} || {},
                    locale          => $user->locale || 'en',
                },
                ripple_settings => $user->ripple_settings(),
                remote_ip   => $remote_ip,
                stream2_base_url => $base_url,
            }
        )->guid;
    } for grep { $user->subscriptions_ref->{$_}->{type} ne 'internal' } keys %{$user->subscriptions_ref};

    $log->infof( "Running automatic search for user %s - keys: ", $user->id, $guid_ref );

    return $self->render( json => { job_guids => $guid_ref }, status => 202 );
}

sub single_search {
    my ( $self, $criteria, $user, $request_ref, $search_options ) = @_;
    my $stream2 = $self->app->stream2;

    my $channel_name;
    my %internal_boards = map {
        $_ => 1
    } $stream2->factory('Board')->list_internal_databases();

    if ( ( $request_ref->{channel}->{type} // 'external' ) eq 'internal' ){
        ( $channel_name ) = grep {
            $user->subscriptions_ref->{$_}->{type} eq 'internal'
        } grep { $internal_boards{$_} } keys %{$user->subscriptions_ref}
            or return $self->render(
            json => { error => { status => "Bad Request", message => "No internal boards available" } },
            status => 400
        );
    }
    else {
        $channel_name = $request_ref->{channel}->{name} or return $self->_missing_field( 'channel/name' );
    }

    my $user_ref = {
        user_id         => $user->id,
        group_identity  => $user->group_identity,
        auth_tokens     => $user->subscriptions_ref->{$channel_name}->{auth_tokens} || {},
        locale          => $user->locale || 'en',
    };

    my $search_job = $self->queue_job(
      class => 'Stream2::Action::Search',
      parameters   => {
        criteria_id         => $criteria->id(),
        template_name       => $channel_name,
        options             => $search_options,
        user                => $user_ref,
        ripple_settings     => $user->ripple_settings(),
        remote_ip           => $self->remote_ip(),
        stream2_base_url    => $self->url_for('/')->to_abs()->to_string(),
      }
    );

    return $search_job;
}

sub _criteria_from_json {
    my ( $self, $query_ref, $user ) = @_;
    $query_ref //= {};
    my $stream2 = $self->app->stream2;

    my $location;
    # Resolve location
    if( my $location_id = delete ( $query_ref->{location_id} ) ){
        $location = $stream2->location_api->find($location_id);
    }
    elsif ( my $location_ref = delete ( $query_ref->{location} ) ){

        if ( my $postcode = $location_ref->{postcode} ){
            my $country = $location_ref->{country};
            return $self->_missing_field( 'query/location/country' ) if ( $postcode && ! $country );
            return $self->_missing_field( 'query/location/postcode' ) if ( ! $postcode && $country );

            $location = $stream2->location_api->find({ postcode => $postcode, country => $country });
        }
        elsif ( my $latitude = $location_ref->{latitude} ){
            my $longitude = $location_ref->{longitude};
            return $self->_missing_field( 'query/location/longitude' ) if ( defined( $latitude ) && ! defined( $longitude ) );
            return $self->_missing_field( 'query/location/latitude' ) if ( ! defined( $latitude ) && defined( $longitude ) );

            $location = $stream2->location_api->find({ latitude => $latitude, longitude => $longitude });

            if( $location->id() == 70881 ){
                # see https://broadbean.atlassian.net/browse/SEAR-2654
                # THIS IS TO BE REMOVED WHEN https://broadbean.atlassian.net/browse/IS-229 is resolved.
                # This is just a quick hack to help text kernel.
                $log->warn("JSON Ripple does NOT like north east london (locID = 70881). Replacing by plain 'London' ID = 39550'");
                $location = $stream2->location_api()->find_by_id( 39550 );
            }
        }

    }
    if( $location ){
        $query_ref->{location} = $location->specific_path();
        $query_ref->{location_id} = $location->id();
    }

    if( defined( my $radius = delete($query_ref->{radius}) ) ){
        if( length($radius) ){ # If this radius is something.
            # Only radius in miles can be expressed through a ripple request
            # if the user uses KM we need to do some convertion and ensure the radius
            # is adequately represented in the criteria
            my $distance_unit = $user->distance_unit();
            if ( $distance_unit eq 'km' ){
                $radius = ($radius || 0) * 1.609;
            }
            $query_ref->{distance_unit} = $distance_unit;
            $query_ref->{location_within} = int(( $radius || 0 ) * 1);
        }
    }

    my $criteria = Stream2::Criteria->from_query( $stream2->stream_schema(), $query_ref );
    return $criteria;
}

sub _missing_field {
    my ( $self, $field , $extra_mess ) = @_;
    $extra_mess //= '';
    my $message = "required field $field missing or invalid. $extra_mess";
    $self->render( json => { error => { status => "Invalid request", message => $message } }, status => 400 );
    return;
}

=head2 search_and_wait

A "blocking" route which will execute the whole search before returning results.

Usage: see L<#search>

=cut

sub search_and_wait {
    my ( $self ) = @_;

    # The inactivity timeout is NOT the heartbeat timeout.
    # What it allows is clients to seat and wait for the connection
    # to return something. The default is 15 seconds which is much too
    # short for this. The nginx reverse proxies are set to 120 seconds
    # to receive data, so this will follow the same time.
    #
    # See http://mojolicious.org/perldoc/Mojolicious/Guides/FAQ#What-does-Inactivity-timeout-mean
    #
    $self->inactivity_timeout(120);

    $self->render_later();

    # if no promise has been returned then we have
    # to assume that an error has been rendered
    my $promise = $self->_init_search()
        or return;

    $promise->then(
        sub {
            my $search_ref = shift or return;

            my ( $job, $criteria ) = @{$search_ref}{qw/job criteria/};
            my $promise = $self->wait_job( $job );

            $self->session( json_ripple => { job_id => $job->guid , criteria_id => $criteria->id()  } );
            my $session_state = $self->app()->sessions()->state();

            my $ripple_session_id = $session_state->sign_value( $self, $self->session_id() );
            my $search_url = $session_state->sessionized_url(
                $self,
                $self->url_for('/search/'.$criteria->id().'/'.$job->parameters->{template_name}),
                360
            )->to_abs()->to_string();

            my $board_name = $job->parameters->{template_name};
            my $feed       = Stream2::FeedTokens->new(
                locale => $self->current_user->locale, board => $board_name
            );
            my $static_facets = $feed->tokens;

            $promise->then(
                sub {
                    my $result = $self->_fetch_results( $job->guid );

                    # merge static facets in with the dynamic ones
                    my $dynamic_facets = $result->{facets} //= [];
                    for my $sf ( @{$static_facets} ) {
                        unless ( List::Util::any { $_->{Id} eq $sf->{Id} } @{$dynamic_facets} ) {
                            push @{$dynamic_facets}, $sf;
                        }
                    }

                    # context about the search such as total results
                    my %search_context = map {
                        $_ => $job->result->{result}->{$_}
                    } qw/max_pages per_page destination current_page notices total_results expires_after search_record_id id/;

                    return $self->render(
                        json => {
                            data => $result,
                            context => \%search_context,
                            ripple_session_id => $ripple_session_id,
                            search_url => $search_url
                        }
                    );
                },
                sub {
                    my $job = shift;
                    my $error = $job->result->{error} // {};
                    my %error_data = (
                        status      => $job->status,
                        message     => $error->{message} // $job->result->{result},
                        error_type  => $error->{properties}->{type} // 'unknown',
                        log_id      => $error->{log_id},
                    );
                    return $self->render( json => { error => \%error_data }, status => 400 );
                }
            );
        },
        sub {
            my $error = shift;
            return $self->render( json => { error => { status => "Unauthorised", message => $error } }, status => 401 );
        }
    );
}

1;

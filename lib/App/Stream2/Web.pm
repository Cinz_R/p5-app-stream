package App::Stream2::Web;

use strict;
use warnings;

use Carp;

use Mojo::Base 'App::Stream2::Controller::Authenticated::HTML';
use Log::Log4perl;

use Data::Dumper;

use Log::Any qw/$log/;

use JSON;
use Stream2::Report::Usage;
use DateTime;

=head2 force_connect

Force a new connection. Will redirect
to /search right after.

=cut

sub force_connect{
    my ($self) = @_;
    my $url = $self->param('redirect_url') || $self->url_for('/search').'';
    $self->session( redirect_url => $url );
    return $self->handle_unauthorised_user($url);
}

sub index {
    my ($self) = @_;
    $self->stash( i18n  => sub{ return $self->app->stream2->translations->__(shift) } );
    shift->render('index');
}

sub usage_report_form {
    my ( $self ) = @_;

    unless( $self->current_user()->contact_details()->{contact_devuser} ){
        return $self->render( text => "Page restricted to internal Broadbean users.", status => 403);
    }

    my $dt = DateTime->now();
    $self->stash( 'n_year' => $dt->year );
    $self->stash( 'n_month' => $dt->month );
    $self->stash( 'n_day' => $dt->day );
    return $self->render('usage-report');
}

sub usage_report {
    my ( $self ) = @_;

    unless( $self->current_user()->contact_details()->{contact_devuser} ){
        return $self->render( text => "Page restricted to internal Broadbean users.", status => 403);
    }

    my @companies = map {
        my $c = $_;
        chomp($c);
        $c =~ s/\s*\z//g;
        $c =~ s/\A\s*//g;
        lc($c);
    } split( "\n", $self->param('companies') );

    my $time_from = join('-', map { $self->param($_) } qw/f_year f_month f_day/ ) . "T00:00:00";
    my $time_to = join('-', map { $self->param($_) } qw/t_year t_month t_day/ ) . "T23:59:59";

    my $report = Stream2::Report::Usage->new({
        stream2 => $self->app->stream2,
        companies => \@companies,
        time_from => $time_from,
        time_to => $time_to
    });
    my $out = $report->generate();

    my @headers = qw/success error total/;
    my @lines = join( ',', 'company', @headers );
    foreach my $company ( @companies ){
        push @lines, join( ',', $company, map { $out->{$company}->{$_} || 0 } @headers );
    }

    my $data = join( "\n", @lines );

    $self->res->headers->from_hash({
        'Content-Type'        => 'text/csv',
        'Content-Disposition' => 'attachment; filename=report.csv'
    });

    return $self->render(
        data => $data
    );
}

1;

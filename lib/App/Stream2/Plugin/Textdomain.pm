package App::Stream2::Plugin::Textdomain;

=head1 Mojolicious::Plugin::Textdomain

    from a bitbucket repo
    https://bitbucket.org/nuclon/mojolicious-plugin-textdomain
    doing the "import" on TextDomain should only be done once or everywhere, this does it once in one location
    and adds helpers to allow translations throughout the app and also in templates

=cut

use warnings;
use strict;

use base 'Mojolicious::Plugin';

use File::Spec;
use Encode;
use I18N::LangTags qw(implicate_supers);
use List::Util qw(first);

our $VERSION = '0.01';

sub register {
    my ($self, $app, $conf) = @_;

    {
        no strict 'refs';
        for my $method ( qw( __ __x __n __nx __xn __p __px __np __npx) ) {
            $app->renderer->add_helper(
                $method => sub {
                        my $self = shift;
                        $self->app->stream2()->translations()->$method(@_);
            });
        }
    }

    $app->renderer->add_helper(
        set_language => sub {
                my ($self, $lang) = @_;
                $self->app->stream2()->translations()->set_language($lang);
            });

    $app->renderer->add_helper(
        detect_language => sub {
            my ($self, $available_languages, $default_language) = @_;

            $available_languages ||= $conf->{'available_languages'};
            $default_language    ||= $conf->{'default_language'};
            my $accept_language = $self->req->headers->accept_language;

            my @langtags = 
                map { $_->[0] } #keep just lang tag
                sort { $b->[1] <=> $a->[1] } # sort by priority desc
                map { /([a-zA-Z]{1,8}(?:-[a-zA-Z]{1,8})?)\s*(?:;\s*q\s*=\s*(1|0\.[0-9]+))?/ ; [$1, $2||1] } #parse lang + priority
                split /\s*,\s*/, $accept_language; # split be comma
            @langtags = implicate_supers(@langtags);

            my $rv;
            for my $lang (@langtags) {
                if (first { $_ eq $lang } @{ $available_languages }) {
                    $rv = $lang;
                    last;
                }
            }

            $rv = $default_language if !$rv;
            $rv;

        });
}

1;

__END__

=head1 NAME

Mojolicious::Plugin::Textdomain - Locale::TextDomain plugin for Mojolicious

=head1 SYNOPSYS

code:

    sub startup {
        ...
        $self->plugin('textdomain', {
            'available_languages' => ['en', 'ru', 'uk'],
            'default_language'    => 'en',
        });
        ...

        my $r = $self->routes;
        $r->route('/')->to(cb => sub {
                my $self = shift;
                my $lang = $self->detect_language;
                $self->redirect_to('index', lang => $lang);
            });

        my $lang_bridge = $r->bridge('/:lang')->to(cb => sub {
                my $self = shift;
                $self->set_language( $self->stash('lang') );
                1;
            });
        $lang_bridge->route('/')->name('index')->to('root#index');
        ...
    }


=head1 DESCRIPTION

L<Mojolicious::Plugin::Textdomain> is L<Locale::TextDomain> plugin for Mojolicious.
You can read advantages of L<Locale::TextDomain> over <Locale::Maketext> solution used in Mojolicious::Plugin::I18n here: L<http://rassie.org/archives/247>

=head2 Options

=over 4

=item domain

=item search_dirs

=item codeset

=item available_languages

=item default_language

=back

=head2 Helpers

All __ methods from L<Locale::TextDomain>, e.g. <%= __ 'Message' %>, <%= __nx 'one apple', '{count} apples', $n, count => $n %> etc.
Plus 

=over 4

=item detect_language

=item set_language

=back


=cut

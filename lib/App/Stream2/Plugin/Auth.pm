package App::Stream2::Plugin::Auth;
use Mojo::Base 'Mojolicious::Plugin';
use Mojo::Log;

use Data::Dumper;
use Digest::SHA qw/sha256_base64/;

use Log::Any qw/$log/;

use Log::Log4perl::MDC;
use Log::Log4perl::NDC;

use Carp;

use Sys::Hostname qw//;

# attributes
has 'providers' => sub {
  require App::Stream2::Plugin::Auth::Providers;
  return App::Stream2::Plugin::Auth::Providers->new();
};

has default_provider_class => sub {
  my ($self) = @_;

  $self->log->debug('Your default auth provider needs to be changed!!!');

  return 'demo';
};

has log => sub { Mojo::Log->new() };

sub config {
  my ($self) = @_;

  my $config = $self->{config};
  $config = $config->() || {} if ref($config) eq 'CODE';
  return $config || {};
}

sub provider_config {
  my ($self, $provider) = @_;
  $self->config->{providers}->{$provider} || ();
}

# methods
sub register {
  my ($self, $app, $config) = @_;

  $self->{config} = $config if $config;
  $self->log($app->log);

  $self->register_helpers($app);
  $self->register_auth_per_controller($app);
}

sub register_helpers {
  my ($self, $app) = @_;

  $app->helper(auth_providers => sub { $self });

  foreach my $name ( $self->auth_helpers() ) {
    $app->helper( $name => sub { $self->$name(@_) } );
  }
}

sub register_auth_per_controller {
  my ($self, $app) = @_;

  $app->hook(around_action => sub {
    my ($next, $c, $action, $last) = @_;

    if ( $c->can('requires_auth') && $c->requires_auth($action) ) {
      if ( !$c->user_signed_in ) {
        return $c->handle_unauthorised_user();
      }
    }
    elsif ( $c->can('requires_basic_auth') && $c->requires_basic_auth($action) ) {
        # the basic_auth plugin basically reads the parts from the Authorization header for us.
        # the hash version of the password is stored in this plugins config and we make a comparison
        # if the authentication is not successful, the request is canceled
        unless ( $c->basic_auth (
            realm => sub {
                my ( $username, $password ) = @_;
                my $config = $self->config->{basic_auth};
                return 0 unless $username && $password;
                if ( $config && $username eq $config->{username} ) {
                    my $password_hash = sha256_base64( $password );
                    if ( $password_hash eq $config->{password_hash} ){
                        return 1;
                    }
                }

                return 0;
            }
        ) ){
            $c->stash(status => 401);
            return $c->respond_to(
                json => { json => { error => 'Not authenticated' } },
                any  => sub {
                    $c->res->headers->www_authenticate( 'Basic' );
                    return { text => 'Not authenticated' };
                }
            );
        }
    }

    return $next->();
  });
}

# plugin helpers
sub auth_helpers {
    return qw/original_user
              current_user
              user_signed_in
              authenticate_user
              user_sign_in
              user_sign_out
              become_adcourier_user
              login_url
              logout_url
              retry_login
              auth_provider/;
}

=head2 original_user

Returns the original search user (as a Stream2::SearchUser) if such a thing exists in the session.

Usage:

  if( my $original_user = $self->original_user() ){
     ...
  }

=cut

sub original_user{
    my ($self, $controller) = @_;

    # Use the stash as a cache.
    if( my $stashed_user = $controller->stash('original_user') ){
        return $stashed_user;
    }

    my $original_user_id = $controller->session('original_user');
    unless( $original_user_id ){ return undef; }

    my $stream2 = $controller->app->stream2();

    my $search_user = $stream2->factory('SearchUser')->find( $original_user_id , { ripple_settings => $controller->session()->{ripple_settings} } );

    if( $search_user ){
        $controller->stash( 'original_user' , $search_user );
        return $search_user;
    }
    return undef;
}

sub current_user {
  my ($self, $controller) = @_;

  if ( my $stashed_user = $controller->stash('current_user') ){
      # A user object has been stashed before in this query. Use that
      # instead of building a new one over and over again.
      return $stashed_user;
  }

  my $uid = $controller->session('user_id')
    or return;

  ## All sorts of stuff are in the session, and not in the DB.
  ## Building the user object from this.
  my $stream2 = $controller->app->stream2();
  my $user = $stream2->factory('SearchUser')->find( $uid  , { ripple_settings => $controller->session()->{ripple_settings} // {} } );
  ## Stash the current user for the rest of the current query.
  $controller->stash( current_user => $user );

  # Return undef when no such thing is found.
  unless( $user ){
      return;
  }

  # Since Late Feb 2015, we
  # grab the contact details from the session ONLY if this user does not have
  # contact details. This is to be compatible with old sessions where users dont have
  # contact_details yet and will wear out eventually when users login again.
  unless( keys %{ $user->contact_details() // {} } ){
      # $controller->app->log->debug("User ".$user->id()." has not NO contact_details. Grabbing from session. This will go away in the future");
      $user->contact_details( $controller->session('contact_details') //  {} );
  }else{
      # $controller->app->log->debug("User has contact details from the DB");
      # Cleanup the session from any contact details. That will shorten it :)
      # REMOVE THIS AFTER A LITTLE WHILE (End of March 2015), cause all the sessions would
      # have been cleaned up.
      $controller->session('contact_details' => undef );
  }

  # Inject session stuff in this user..
  map {
      $user->$_($controller->session($_));
  } qw/is_overseer
       navigation
       is_admin
      /;

  Log::Log4perl::MDC->put('sentry_user' , $user->short_info_hash() );
  # Time to build a stack. company , user email
  Log::Log4perl::NDC->push( $user->group_identity() );
  Log::Log4perl::NDC->push( $user->contact_email() );

  return $user;
}


sub user_sign_in {
  my ($self, $controller, $user_ref) = @_;

  my $stream2 = $controller->app->stream2();
  my $stream_user = $self->create_user( $controller, $stream2, $user_ref );

  # Save this latest connection IP against the user.
  # Some watchdogs need that (like cvlibrary).
  $stream_user->last_ip($controller->remote_ip());
  $stream_user->update();

  # these details will be kept in the session
  $controller->session('user_id' => $stream_user->id);

  # we are logging in a new user, ensure we replace the user stash
  $controller->stash('current_user' => $stream_user);

  my %session_vars = (
    'navigation'        => $user_ref->{provider}->{navigation} || [],
    'is_admin'          => $user_ref->{provider}->{is_admin} || 0,
    'is_overseer'       => $user_ref->{provider}->{is_overseer} || 0,
    'currencies'        => $user_ref->{currencies} || [],
    'manage_responses_view'    => $user_ref->{manage_responses_view} || 0
  );

  # Only inject in the session what is not stored in the DB (yet)
  $controller->session( %session_vars );


  # Time to create a login record
  my $login_record = $stream2->factory('LoginRecord')->create({
      user_id => $stream_user->id(),
      remote_ip => $stream_user->last_ip(),
      login_provider => $stream_user->last_login_provider(),
      hostname => Sys::Hostname::hostname()
  });
  $stream_user->ripple_settings()->{login_record_id} = $login_record->id();
  $controller->auth_provider->after_sign_in( $stream_user );
}

sub create_user {
    my ( $self, $controller,  $stream2, $user_ref ) = @_;

    my $ripple_settings = $user_ref->{ripple_settings};
    unless ($ripple_settings) {
        confess("Missing ripple_settings");
    }

    my $login_provider = $user_ref->{provider}->{login_provider}
        || confess("Missing login_provider");
    $ripple_settings->{login_provider} = $login_provider;

    # store user and subscription details
    my %user_vars = (
        'contact_details' => {
            timezone      => 'Europe/London',    # Sensible defaults.
            distance_unit => 'miles',
            locale        => 'en',
            currency      => 'GBP',
            %{ $user_ref->{contact_details} }
        },

        # provider === 'identity_provider'
        'provider' => $user_ref->{provider}->{identity_provider}
          || confess("Missing identity_provider"),
        'provider_id'         => $user_ref->{provider}->{user_id},
        'group_identity'      => $user_ref->{provider}->{group_identity},
        'group_nice_name'     => $user_ref->{provider}->{group_nice_name},
        'settings'            => $user_ref->{settings} || {},
        'ripple_settings'     => $ripple_settings,
        'last_login_provider' => $login_provider, # Note that this is Deprecated. Only there for legacy reasons.
    );

    $user_vars{contact_details}->{locale} =
        _fix_locale( $stream2, $user_vars{contact_details}->{locale} );

  my $user_factory = $stream2->factory('SearchUser');

  my $stream_user = $user_factory->find(
      { provider => $user_vars{provider}, # Note that this is the identity provider.
        provider_id => $user_vars{provider_id},
    },
      { ripple_settings => $ripple_settings }
  );
  if( $stream_user ){
      foreach my $key (keys %user_vars ){
          $stream_user->$key( $user_vars{$key} );
      }
  }
  else {
      $stream_user = $stream2->factory('SearchUser')->new_result(\%user_vars );
  }


  # Make sure we have the dummy subscription here.
  if( ( $ENV{MOJO_MODE} // '' ) eq 'development' ){
      $user_ref->{subscriptions}->{dummy} = { nice_name => 'Dummy',
                                              auth_tokens => {},
                                              type => 'social',
                                              board_id => -314159,
                                              eza_number => -654321,
                                            };
  }

  # Inject subscriptions always
  $stream_user->subscriptions_ref( $user_ref->{subscriptions} );

  my $user_is_brand_new = ! $stream_user->id();
  $stream_user->save();

  $self->log->info("User has been saved under Stream2 ID = ".$stream_user->id());

  if( $user_is_brand_new ){
      $self->log->info("User has never been seen in Search2 before Will conform to sibbling settings");
      $stream_user->conform_to_sibling_settings();
  }

  my $custom_fields = $user_ref->{custom_fields} // [];

  $ripple_settings->{custom_fields} = $custom_fields;
  $controller->session->{ripple_settings} = $ripple_settings;

  return $stream_user;
}

=head2 become_adcourier_user

Allows the current user to become the given adcourier user id.

Usage:

  $this->become_adcourier_user($controller, $adcourier_userid );


Or if you are poping a user and want to skip the hiarchy test, and reset the
original user to undef, you can give the option 'poping_user':

  $this->become_adcourier_user( $controller , $adcourier_userid , { poping_user => 1 } );

=cut

sub become_adcourier_user{
    my ($self, $controller, $adcourier_userid , $opts  ) = @_;
    my $current_user = $self->current_user($controller) or confess("Missing user in session");

    # zombie users can be logged in to the Responses view
    my $include_zombies = $controller->session->{manage_responses_view};
    $opts //=  {};

    unless( $opts->{poping_user} ){
        # When poping a user, we dont need to check the overseeing.
        # A bit of security:
        my @oversees = ();
        if ( my $user_rs = $controller->auth_provider->user_oversees( $current_user, $include_zombies ) ){
            @oversees = $user_rs->get_column('provider_id')->all();
        }

        unless( grep { $_ == $adcourier_userid } @oversees ){

            # zombie users probably don't exist in the cv_users table: give
            # them a chance
            die "Session invalid" unless $include_zombies;
        }
    }

    my $stream2 = $controller->app->stream2();

    # die if user doesn't exist, even as a zombie
    my $new_user_details = $stream2->user_api->get_user($adcourier_userid)
        or die "Session invalid - Cannot user_api.get_user for adc userID = $adcourier_userid";

    # Figure out roles.
    my $is_overseer = 0;
    my $is_superadmin = 0;
    my $user_role = $new_user_details->{user_role} || 'USER';
    if ( $user_role =~ m/\A(?:(?:SUPER)?ADMIN|TEAMLEADER)\z/ ){
        $is_overseer = 1;
    }
    if ( $user_role eq 'SUPERADMIN' ) {
        $is_superadmin = 1;
    }


    # Ok the current user oversees the requested user.
    # We need to build something that can be given to the
    # above method 'user_sign_in'
    my $inherited_ripple_settings = $controller->session()->{'ripple_settings'} // {};
    my $sign_in_properties = {
        provider => {
            # Same identity and login providers.
            identity_provider => $current_user->provider(),
            login_provider    => $current_user->last_login_provider(),
            user_id => $adcourier_userid,
            # Same group identity and nice name
            group_identity => $current_user->groupclient->identity,
            group_nice_name => $current_user->groupclient->nice_name(),
            # No navigation for now, as jumping to a different product would
            # be confusing.
            navigation => scalar( $controller->session()->{navigation}  // [] ),
            # Admin and is_overseer
            is_admin => $is_superadmin,
            is_overseer => $is_overseer
        },
        subscriptions => undef,
        # Same ripple settings as parent user.
        # In case this is used from inside ripple.
        ripple_settings => $inherited_ripple_settings,

        manage_responses_view => $controller->session()->{'manage_responses_view'} || 0,
        custom_fields => $inherited_ripple_settings->{'custom_fields'} // []
    };

    # We need to figure out the subscriptions
    $sign_in_properties->{subscriptions} = $stream2->stream_api->get_active_subscription_tokens( $adcourier_userid );

    # We need to figure out the contact details.
    my @currencies = $stream2->user_api->currencies($adcourier_userid);
    $sign_in_properties->{currencies} = scalar( @currencies ) ? \@currencies : [ qw/GBP USD EUR/ ];

    $sign_in_properties->{contact_details} =
      {
       contact_name => $new_user_details->{name},
       contact_telephone => $new_user_details->{telephone},
       contact_email => $new_user_details->{email},
       feedback_email => $new_user_details->{feedback_email},
       contact_devuser => $current_user->contact_details()->{contact_devuser},
       currency => $new_user_details->{currency},
       locale => $new_user_details->{locale},
       timezone => $new_user_details->{timezone},
       distance_unit => $new_user_details->{distance_unit},
       company => $current_user->groupclient->identity(), # Obviously.
       company_nice_name => $current_user->groupclient->nice_name(),
       ( map { $_ => $new_user_details->{$_} } qw/username office team consultant/ ),
       ( map { $_ => $new_user_details->{$_} // undef } qw/admin_phone admin_name admin_email/ )
      };

    $sign_in_properties->{settings} = $new_user_details->{settings} || {};

    if( $opts->{poping_user} ){
        $controller->session( original_user => undef );
    } else {
        $controller->session( original_user => $current_user->id() );
    }
    return $self->user_sign_in($controller, $sign_in_properties);
}

sub user_sign_out {
  my ($self, $controller) = @_;
  $controller->session(expires => 1 );
}

=head2 user_signed_in

Is there a signed in user?

Usage:

 if( $this->user_signed_in() ){
   ...
 }

=cut

sub user_signed_in {
  my ($self, $controller) = @_;
  return $controller->current_user() ? 1 : 0;
}

=head2 auth_provider

Returns an instance of L<App::Stream2::Plugin::Auth::Provider> according to the given option ref
(or the one of the current user, or the default one).

Note that this is the LOGIN provider of the user. If you want the identity provider
of the user, use $self->current_user()->identity_provider();

Example:

  my $current_provider = $controller->auth_provider();
  my $specific_provider = $controller->auth_provider({ provider => 'adcourier' );
  # Returns the singleton instance of L<App::Stream2::Plugin::Auth::Provider::Adcourier.pm>

=cut

sub auth_provider {
  my ($self, $controller, $options_ref) = @_;

  # It is important that we maintain the origin of the user
  my $current_provider;
  if ( defined( $options_ref->{provider} ) ) {
      # The provider name may be passed in the URL
      $current_provider = $options_ref->{provider};
  }
  elsif ( defined( $controller->current_user ) ) {
      # The provider name is the one of the last login one.
      # cause we are 
      $current_provider = $controller->current_user->last_login_provider() // confess("No last login provider");
  }

  my $provider =
      $current_provider
      ? $self->new_provider( $current_provider )
      : $self->default_provider();
  return $provider;

}

# Provider methods

=head2 authenticate_user

Example:

  $controller->authenticate_user({ provider => 'adcourier' });
  $controller->authenticate_user({ provider => 'adcourier_ats' , username => '..' 

=cut

sub authenticate_user {
  my $self = shift;
  return $self->auth_provider( @_ )->authenticate( @_ );
}

=head2 login_url

A login URL to force the user to log in.

You MUST specify the provider when you use that.

Usage:

 $controller->login_url({ provider => 'adcourier' });

=cut

sub login_url {
    my  ( $self, $controller, $params, @rest) = @_;
    $params //= {};
    unless( $params->{provider} ){
        confess("Missing provider");
    }
    return $self->auth_provider( $controller, $params, @rest )->login_url( $controller, $params, @rest  );
}
sub logout_url {
  my $self = shift;
  return $self->auth_provider( @_ )->logout_url( @_ );
}
sub retry_login {
  my $self = shift;
  return $self->auth_provider( @_ )->retry_login( @_ );
}


# plugin methods
sub default_provider {
  my ( $self ) = @_;

  my $default_provider = $self->config->{default_provider} || $self->default_provider_class();
  return $self->new_provider( $default_provider );
}

sub new_provider {
  my ($self, $provider_name ) = @_;

  return $self->providers->register_provider($provider_name,
                                             $self->provider_config($provider_name) // {},
                                         );
}

sub _fix_locale {
    my ( $s2, $locale ) = ( shift, shift // 'en' );

    # statically fix $locale if we can
    my %locale_map = (
        en_usa => 'en_US'
    );
    $locale = $locale_map{ $locale } if $locale_map{ $locale };

    # Dynamic language fallback
    # E.g. $locale eq 'fr_CH'. Try to serve 'fr_CH', 'fr', 'en' in descending
    # order of preference.
    my @lang_prefs = ($locale);                                     # 'fr_CH'
    push @lang_prefs, $locale =~ /^([a-z]+)_/ if $locale =~ /_/;    # 'fr'
    push @lang_prefs, 'en' unless $locale =~ /^en/;                 # 'en'

    my $supported_languages = $s2->translations->supported_languages;
    for my $lang_sought ( @lang_prefs ) {
        my ($matched_lang)
            = grep { $_ eq $lang_sought } @{$supported_languages};
        return $matched_lang if $matched_lang;
    }
}

1;

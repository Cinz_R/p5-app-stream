package App::Stream2::Plugin::Log;
use Mojo::Base 'Mojolicious::Plugin';
use Log::Log4perl;
use Log::Any::Adapter;
use Log::Any::Adapter::Util;

# This Log::Any proxy class contains Mojolicious required methods.
# This is since Log::Any >0.15
use Log::Any qw/$log/, proxy_class => '+App::Stream2::Log::Any::Proxy';

use Carp;

sub register {
  my ($self, $app, $config) = @_;

  my $log_out = $config->{logger} || 'Stderr';

  my $home = $app->home;
  if ( $log_out eq 'Log4perl' ){


      ## Build a log4perl config file
      my $log4perl_file = $home . "/" . (  $config->{logger_l4p} // 'log4perl.conf' );

      if ( -e $log4perl_file ){
          ## Avoid blating any initialization someone might have done at a higher level
          unless( Log::Log4perl->initialized() ){
              ## Removed that in an attempt to have
              ## the restart working via the mashboard.
              ## warn "INITIALIZING Log4perl from $log4perl_file. Will check for changes every 5s\n";
              Log::Log4perl::init_and_watch( $log4perl_file , 5 );
          }
      }else{
          confess("Cannot find $log4perl_file");
      }
  }

  Log::Any::Adapter->set($log_out);


  $app->log( $log );
}

1;

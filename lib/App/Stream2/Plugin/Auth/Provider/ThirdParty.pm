package App::Stream2::Plugin::Auth::Provider::ThirdParty;

use Moose;
extends qw/App::Stream2::Plugin::Auth::Provider::Base/;

use Stream2;
use Log::Any qw/$log/;

has 'keys' => ( is => 'ro', isa => 'HashRef' , required => 1 );

has '+user_hierarchy_header' => ( default => sub{ [ 'Group Identity', 'Provider ID' ] } );

=head1 NAME

App::Stream2::Plugin::Auth::Provider::ThirdParty - Trusted thirdparty managed authentication.

=cut

=head2 secret_key_for_api_key

Given a third party API key, returns the associated secret key.

=cut

sub secret_key_for_api_key{
    my ($self, $api_key) = @_;
    my $key_data = $self->keys()->{$api_key};
    unless( $key_data ){
      $log->warn("No data in config for key = $api_key");
      return;
    }
    return $key_data->{secret_key};
}


=head2 user_can_import_from

See superclass. Always false here. Not a broadbean client.

=cut

sub user_can_import_from{0;}

=head2 user_can_import_from

If there is a CV, users can always download why not.

=cut

sub user_can_download_from{1;}

=head2 flagging_history

Does nothing

=cut

sub flagging_history{};

=head2 application_history

Does nothing

=cut

sub application_history{ [] };


=head2 user_siblings_ids

Textkernely implementation. See Superclass.

Returns empty.

=cut

sub user_siblings_ids{
    my ($self, $user) = @_;
    return ();
}

=head2 get_hierarchical_identifier

Returns something that will never match for now.

=cut

sub get_hierarchical_identifier {
    my ( $self, $user ) = @_;
    return '/'.$user->group_identity().'/'.$user->provider_id();
}

=head2 get_oversees_hierarchical_identifier


Returns something that will never match for now.

=cut

sub get_oversees_hierarchical_identifier {
    my ( $self, $user ) = @_;
    return $self->get_hierarchical_identifier( $user );
}

__PACKAGE__->meta->make_immutable;

package App::Stream2::Plugin::Auth::Provider::Adcourier;

use Moose;
extends qw/App::Stream2::Plugin::Auth::Provider::Base/;

use Stream2;
use Bean::OAuth::AsyncClient;
use Stream2::Criteria;
use Log::Any qw/$log/;

use JSON;
use URI;
use Data::Dumper;
use Mojo::URL;
use MIME::Base64;

use Carp;

=head1 NAME

App::Stream2::Plugin::Auth::Provider::Adcourier - Adcourier/broadbean oauth authentication for Mojolicious.

=cut

has 'oauth_client' => (
  is       => 'ro',
  isa      => 'Bean::OAuth::AsyncClient',
  lazy     => 1,
  builder  => '_build_oauth_client'
);

has '+user_hierarchy_header' => ( default => sub{ [ 'Company', 'Office', 'Team', 'User' ] } );

sub login_url {
    my ( $self, $controller ) = @_;
    my $query = $controller->req->params->to_hash();

    # these are used by the front end and are of no interest here
    delete $query->{$_} for qw/search_id candidate_idx redirect_url/;

    # if search parameters have been passed in the URL, remember them
    # by passing them as part of the oauth request as the 'state' param

    my $state = {};
    do { # string options
        if ( exists($query->{$_}) ){
            $state->{$_} = delete $query->{$_} ;
        }
    } for qw/consultant_id/;
    do { # bool options
        if ( exists($query->{$_}) && delete $query->{$_}){
            $state->{$_} = 1;
        }
    } for qw/manage_responses_view/;

    # In case the query DOES NOT Contain anything, make sure we will not redirect
    # to /search/<criteria id> but to just '/search' after the oauth dance.
    if( scalar(%$query) ){
        # SEAR-2046 - always defaults to ALL if not set
        if ( ! exists($query->{cv_updated_within}) && $state->{manage_responses_view} ){
            $query->{cv_updated_within} = 'ALL';
        }
        my $criteria = Stream2::Criteria->from_query( $controller->app->stream_schema(), $query );
        $state->{criteria_id} = $criteria->id;
    }

    return $_[0]->oauth_client->auth_url( JSON::encode_json($state) );
}

sub logout_url {
    return "//www.adcourier.com/logout.cgi";
}
sub retry_login { 0; }
sub show_advert_preview { 1; }

=head2 get_adverts

Look at L<App::Stream2::Plugin::Auth::Role::Provider>

Usage

    my $adverts = $c->auth_provider()->get_adverts($user, $opts);

Where the $opts hashref can include:

=over 5

=item user_id

The user to filter adverts by, aka provider_id

=item keywords

Search term to further filter adverts

=item limit

Number of adverts to return per request

=item offset

Offset of adverts returned for paging

=item list_name

The shortlist name

=back

=cut

sub get_adverts{
    my ($self, $user, $opts) = @_;
    $opts //= {};

    my $group_adverts = $opts->{group_adverts} // 0;

    # Provider is adcourier
    # the following code would have to move to a provider specific place.
    my $advert_api = $user->stream2->advert_api();

    my $query = {
        company     => $user->group_identity,
        keywords    => $opts->{keywords},
        limit       => $opts->{limit} // 500,
        offset      => $opts->{offset} // 0,
        sort        => '-time', # always sort by most recent - SEAR-1587
        is_requisition_ad => 0, # NO REQUISITION ADS IN SEARCH.
        $opts->{visible} ? (visible => 1) : (),
    };

    my $user_id = $opts->{user_id};
    $query->{user_id} = $user_id if $user_id;

    my @adverts = $advert_api->search_adverts($query);

    unless( scalar(@adverts) ){
        $log->error("Undefined adverts response found for adcourier user ".$user->provider_id());
        return [];
    }

    $log->trace("Got response: ".Dumper(\@adverts));

    $_->{label} = $self->_build_advert_label( $_ ) for @adverts;

    if ( $group_adverts ) {
        my $adverts_ref = {};
        foreach my $advert ( @adverts ) {
            my $visibility = $advert->{visible} // '';
            my $group = $visibility eq '1' ? 'live' : 'archived';
            push @{$adverts_ref->{adverts}->{ucfirst $group}}, $advert;
        }
        $adverts_ref->{groups} = ['Live', 'Archived'];
        return $adverts_ref;
    }

    return \@adverts;
}

sub _build_advert_label {
    my ( $self, $advert ) = @_;

    my @label = ( $advert->{jobref}, $advert->{jobtitle} );
    my @location = ();

    # location_2 is usually the city or place ( sometimes with dashes at the start for some reason )
    if ( my $location_2 = $advert->{location_2} ) {
        $location_2 =~ s/\A\-*//g;
        push( @location, $location_2 );
    }

    # location_1 is usually the country
    if ( my $location_1 = $advert->{location_1} ) {
        $location_1 =~ s/\A\-*//g;
        push( @location, $location_1 );
    }

    return join( ' - ', @label, join( ", ", @location ) );
}

=head2 find_advert

Look at L<App::Stream2::Plugin::Auth::Role::Provider>

=cut

sub find_advert{
    my ($self, $user , $advert_id , $opts ) = @_;
    $opts //= {};

    $log->info("Finding advert for advert_id='$advert_id', opts = ".Dumper($opts));

    my $board_id = $opts->{'board_id'} || '999'; # broadbean
    my $adcapi = $user->stream2->adcourier_api();
    my $response = $adcapi->request_json( GET =>
                                              $adcapi->url('/adcourier/users_byid/'.$user->provider_id().'/adverts/'.$advert_id));
    # Make sure we complie with the signature.
    # Bloddy auto-vivification..
    unless( $response ){
        return;
    }
    my $advert = $response->{Advert};
    # Note we transfer the list_name and advert_id back to the advert.
    # This is consistent with how AdcourierAts provider does it too.
    $advert->{list_name} = $opts->{list_name};
    $advert->{advert_id} = $advert_id;
    $advert->{stream2_jobref} = $advert->{JobReference};
    $advert->{stream2_jobtitle} = $advert->{JobTitle};
    return $advert;
}

=head2 user_exists

Does this user exists in adcourier?

=cut

sub user_exists{
    my ($self, $provider_id) = @_;
    my $user = Stream2->instance->user_api->get_user($provider_id);

    # doing a lc and eq is 39% faster than a case insensitive regex says mhawkes
    return if ( $user->{office} && lc( $user->{office} ) eq 'zombie' );

    return $user && $user->{username};
}


=head2 advert_preview_url

See L<App::Stream2::Plugin::Auth::Role::Provider>

=cut

sub advert_preview_url{
    my ($self, $advert, $user, $opts) = @_;

    my $adcapi = $user->stream2->adcourier_api();
    my $api_url = $adcapi->url(
        join('/', '', $user->group_identity, 'adverts', $advert->{AdvertID}, 'applyonline' ),
        {
            board_id    => $opts->{board_id},
            variables   => JSON::encode_json( $opts )
        }
    );
    my $response = $adcapi->request_json( GET => $api_url );

    if ( my $apply_online = $response->{applyonline} ){
        $log->info("Will use returned applyonline URL ( $apply_online )");
        return $apply_online;
    }

    $log->info("Will generate aplitrack URL from parts");
    my $url = $user->stream2->gen_aplitrak_url_from_parts(
                                                          $advert->{Consultant},
                                                          $advert->{AplitrakID},
                                                          $opts->{board_id},
                                                          $user->group_identity()
                                                         );
    return $url;
}

=head2 application_history

Peruse L<App::Stream2::Plugin::Auth::Role::Provider>

=cut

sub application_history {
    my ($self, $user, $candidate) = @_;
    if ( my $email = $candidate->email ) {
        my $apps_ref = $user->stream2->candidate_api()->candidate_applications( $user->provider_id, $email );

        my $six_months_ago = time() - ( 86400 * 28 * 6 );

        my @applications;
        foreach my $app_ref ( @$apps_ref ){
            my $response    = $app_ref->{response};
            my $advert      = $app_ref->{advert};
            my $ad_url      = $advert->{responses_url};

            my @attachments;
            # only give links to files younger than 6 months as we cannot garuntee their availability
            if ( $response->{time} > $six_months_ago ){
                @attachments = split (/\|/, $response->{attachments} || '');
            }

            unless( $ad_url ){
                $ad_url = $advert->{apply_url};
            }else{
                if( $response->{rank} == 5 ){ # green
                    # Green ads are in view_shortlist
                    $ad_url =~ s/script_action=\w+/script_action=view_shortlist/;
                }

                # We can concatenate #response_{respid} to the responses url
                $ad_url .= '#response_'.$response->{response_id};
            }
            push @applications, {
                application_time => $response->{time},
                attachments      => \@attachments,
                job_title        => $advert->{jobtitle},
                advert_link      => $ad_url,
                advert_id        => $advert->{advert_id},
                rank             => $response->{rank},
                adcresponse_id   => $response->{response_id}
            };
        }
        return [@applications];
    }
    return [];
}

=head2 flagging_history

Cast thyn eye o'er L<App::Stream2::Plugin::Auth::Role::Provider>

=cut

sub flagging_history {
    my ( $self, $current_user, $result_arr ) = @_;
    my @email_addresses = ();
    # get a list of email addresses
    foreach my $result ( @$result_arr ) {
        if ( my $email = $result->email ){
            push @email_addresses, $email;
        }
    }
    if ( scalar( @email_addresses ) ){
        # request flagging for each email address
        my $flagging_ref = $current_user->stream2->candidate_api()->flagging_history(
            $current_user->provider_id(),
            @email_addresses
        );

        my $flagging_ref_by_email = {};
        foreach my $email ( keys %$flagging_ref ){

            # ensure we normalise the email address, this is to ensure maximum candidate email matchness
            # example adamlucas12@hotmail.com matches adamlucas12@Hotmail.com when in mysql
            # but not in perl. It also looks like artirix do some normalisation themselves.
            # the below parsing over 35 addresses takes ~0.0016s
            my ( $email_obj ) = Email::Address->parse( $email );
            unless ( $email_obj ) {
                $log->warn("Invalid/non-parseable email address: '$email'");
                next;
            }
            my $host = $email_obj->host;
            my $user = $email_obj->user;
            my $normalised_email = $user . '@' . lc( $host );

            do {
                $flagging_ref_by_email->{ $normalised_email }->{ $_ } += $flagging_ref->{ $email }->{ $_ } * 1;
            } for keys %{$flagging_ref->{$email}};
        }

        # map the applications back onto the result objects
        map {
            $_->attr('flagging_history', $flagging_ref_by_email->{ ( $_->email // '_WILL_NEVER_MATCH_' ) } // {});
        } @$result_arr;
    }
}

=head2 advert_shortlist_candidate

See L<App::Stream2::Plugin::Auth::Role::Provider>

=cut

sub advert_shortlist_candidate{
    my ($self, $advert, $candidate, $cv_hash , $user, $opts ) = @_;

    my $adcapi = $user->stream2->adcourier_api();

    unless( $advert ){
        $log->warn("Missing advert. Not doing anything to shortlist this candidate");
        return;
    }

    unless( $advert->{AdvertID} ){
        $log->warn("The given advert does not look like an adcourier advert. Not doing anything");
        return;
    }

    my $url = $adcapi->url('/adcourier/users_byid/'.$user->provider_id().'/adverts/'.$advert->{AdvertID}.'/shortlist');

    my $request = HTTP::Request::Common::POST( $url ,
                                               Content =>
                                               [ candidate_email => $candidate->email(),
                                                 candidate_name  => $candidate->name(),
                                                 source_channel  => $candidate->destination(),
                                                 $cv_hash ? %{$cv_hash} : ()
                                               ]
                                             );
    $adcapi->authorize_request($request);
    my $response = $adcapi->perform_request($request);
    my $hash = $adcapi->dejsonize_response($response);

    unless( $hash ){
        return "Could not find shortlisting resource $url";
    }

    unless( $response->is_success() ){
        return $hash->{message};
    }

    return undef;
}

=head2 user_siblings_ids

Adcourier Juicy implementation. See Superclass.

=cut

sub user_siblings_ids{
    my ($self, $user) = @_;
    my $user_id = $user->id() || confess("Please give a user saved in the DB");
    unless( $user->provider() =~ /^adcourier/ ){
        confess("Please give an adcourier user");
    }
    my $stream2 = $user->stream2();
    my @team_mates = @{ $stream2->user_api->siblings($user->provider_id() , { go_up => 1 } ) };

    $log->info("For adcourier user id = ".$user->provider_id()." got siblings ".join(', ', map{ $_->{id} } @team_mates  ));
    # Get the right IDs from the local DB.
    return $stream2->stream_schema->resultset('CvUser')
      ->search({ provider => $user->provider(),
                 group_identity => $user->group_identity(),
                 id => { '!=' => $user->id() },
                 provider_id => { -in => [ map{ $_->{id} } @team_mates ] }
               },
               { select => ['id'] }
              )->get_column('id')->all();
}

=head2 adc_oversees

get a list of users what this user oversees
returns array of user_refs

    IN:
        my $user_rs = $auth_provider->user_oversees( $user );

    OUT:
        my @users = $user_rs->all();

=cut

sub adc_oversees{
    my ( $self, $user, $include_zombies ) = @_;
    my $stream2 = $user->stream2();
    my @adc_oversees = @{ $stream2->user_api->oversees( $user->provider_id(), { active => 1 , exclude_zombies => !$include_zombies } ) };
    return @adc_oversees;
}

sub user_oversees {
    my ( $self, $user, $include_zombies ) = @_;

    my @adc_overseen = $self->adc_oversees( $user, $include_zombies );

    unless ( scalar ( @adc_overseen ) ){
        return;
    }

    # Enrich the overseen with the local user ids
    my $user_rs = $user->stream2->factory('SearchUser')->search(
        {
            group_identity  => $user->group_identity(),
            provider        => $user->provider(),
            provider_id     => { -in => [ map { $_->{id} } @adc_overseen ] }
        }
    );

    return $user_rs;
}

=head2 user_overseen_hierarchy

Builds a hierarchy from the given user.

See superclass for doc

Usage:

    my $hierarchy_ref = $this->user_overseen_hierarchy( $SearchUser );

Out:

    {
        label => 'Microsoft',
        path => '/microsoft',
        children => [
            {
                label => 'XBox',
                path => '/microsoft/xbox',
                children => [
                    {
                        label => 'Games',
                        path => '/microsoft/xbox/games',
                        children => [],
                    },
                    ...
                ]
            },
            ...
        ]
    }

=cut

sub user_overseen_hierarchy{
    my ($self, $user) = @_;

    my $stream2 = $user->stream2();

    my $tr = $stream2->translations();
    my $office_string = $tr->__('Office');
    my $team_string = $tr->__('Team');

    my @adc_overseen = $self->adc_oversees( $user );
    # Make sure to inject myself innit.
    push @adc_overseen , { id => $user->provider_id(),
                           %{ $stream2->user_api->get_user($user->provider_id()) }
                         };

    # Enrich the overseen with the local user ids
    my @local_users = $stream2->factory('SearchUser')->search({ group_identity => $user->group_identity(),
                                                                provider =>  $user->provider(),
                                                                provider_id => { -in => [ map { $_->{id} } @adc_overseen ] }
                                                              }, { order_by => [ 'contact_name', 'contact_email' , 'id' ] })->all();

    my %local_per_adcid = map{ $_->provider_id => $_ } @local_users;

    my @rich_overseen = ();
    foreach my $overseen ( @adc_overseen ){
        my $local = $local_per_adcid{$overseen->{id}};
        unless( $local ){ next; }
        $local->{_team} = $overseen->{team};
        $local->{_office} = $overseen->{office} // '';
        $local->{_consultant} = $overseen->{consultant};
        $local->{_company} = $overseen->{company};
        $local->{_path} = $self->_get_hierarchical_identifier_from_ref( $overseen ); # better than calling get_hierarchical on each searchuser
        push @rich_overseen , $local;
    }

    # Right, go through the rich overseen and generate the tree.
    my %offices = ();
    my $company;
    foreach my $overseen ( @rich_overseen ){
        my $office  = $overseen->{_office};
        my $team    = $overseen->{_team};
        $company  //= $overseen->{_company};

        my $user_hash =
          {
           id => $overseen->id(),
           label => $overseen->{_consultant}.' / '.( $overseen->contact_name() ||  '' ).' ('.( $overseen->contact_email() || $overseen->id() ).')',
           path => $overseen->{_path}
          };

        # Vivify the required stuff
        $offices{$office} //= {
            label       => $office,
            children    => [],
            teams       => {}, # A hash of all office teams
            path        => $self->_get_hierarchical_identifier_from_ref({ company => $company, office => $office })
        };

        # No team, go directly in the office
        unless( $team ){
            push @{ $offices{$office}->{children} }  , $user_hash;
            next;
        }

        $offices{$office}->{teams}->{$team} //= {
            label       => $team_string . ": " . $team,
            children    => [],
            path        => $self->_get_hierarchical_identifier_from_ref({ company => $company, office => $office, team => $team })
        };
        # Push this user in his right team. This will be in the right order
        push @{ $offices{$office}->{teams}->{$team}->{children} } , $user_hash;
    }


    # sort offices
    my @offices = @offices{ sort keys %offices };
    # In each office, sort the teams
    foreach my $office ( @offices ){
        my %teams = %{ delete $office->{teams} };
        $office->{label} = $office_string . ": " . $office->{label};
        $office->{children} = [ @{$office->{children}} ,  @teams{ sort keys %teams } ];
    }

    return {
        label       => $tr->__('Company: ').$user->group_identity(),
        children    => \@offices,
        path        => $self->_get_hierarchical_identifier_from_ref({ company => $company })
    };
}


=head2 user_import_candidate

See L<App::Stream2::Plugin::Auth::Role::Provider>

One VERY important thing:

$cv_hash should be  { cv_mimetype => .. , cv_filename => .. , $cv_content => BASE64 ENCODED BINARY }

Usage:

 $this->user_import_candidate($search2_user , { cv_mimetype => .. , cv_filename => ... , cv_content => .. },
                              $search2_candidate , { candidate_attributes => { forced_attribute1 => ... , forced_attribute2 => ..  }  } );

=cut

sub user_import_candidate{
    my ( $self, $user, $cv_hash, $candidate, $opts ) = @_;

    unless( $user && $cv_hash && $candidate ){ confess("Missing user, cv_hash or candidate"); };

    my @backends = $self->get_internal_database_backends( $user, $candidate );
    unless ( scalar( @backends ) ){
        confess("User ".$user->id()." does not have a subscription to any internal database");
    }

    return $self->import_candidate_to_backends( $user, $cv_hash, $candidate, $opts, \@backends );
}

=head2 get_internal_database_backends

Returns a list of "backend" names suitable to import the given
$candidate to (for the given $user).

The "backend" names are used in the tsimport API

Usage:

    my @backends = $this->get_internal_database_backends( $user, $candidate );

Out:

    ( 'Artirix', ... )

=cut

{
    my %backend_map = (
        talentsearch    => 'Artirix',
        cb_mysupply     => 'MySupply',
        candidatesearch => 'BBCSS'
    );
    sub get_internal_database_backends {
        my ( $self, $user, $candidate ) = @_;
        return
            map { $backend_map{$_} }
            grep { $candidate->destination ne $_ && $user->has_subscription($_) }
            keys %backend_map;
    }
}

=head2 user_import_candidate_to_backends

Import a candidate to an explicit list of internal databases

Usage:

    $this->import_candidate_to_backends(
        $user,
        {
            cv_content => $b64_cv,
            cv_filename => 'John-Smith.docx'
        },
        $candidate,
        {
            candidate_attributes => {
                applicant_email => 'johnsmith@example.com'
            }
        },
        [ 'Artirix', 'MySupply' ]
    )

=cut

sub import_candidate_to_backends {
    my ( $self, $user, $cv_hash, $candidate, $opts, $backends_ref ) = @_;

    my @backends = @$backends_ref;
    my $cv_content = $cv_hash->{cv_content} // confess("Missing cv_content in cv_hash");
    my $cv_filename = $cv_hash->{cv_filename} // confess("Missing cv_filename in cv_hash");

    my $client_id = scalar($user->group_identity());
    # talentsearch can have a different client_id than the company name.
    if ( List::Util::first { $_ eq 'Artirix' } @backends && ( my $ts_subscription = $user->has_subscription('talentsearch') ) ){
        if( my $subscription_client_id = $ts_subscription->{auth_tokens}->{talentsearch_client_id} ){
            $log->info("client_id is overriden by talentsearch subscription one = '$subscription_client_id'");
            $client_id = $subscription_client_id;
        }
    }

    my $locale = $user->tsimport_tagger_locale();
    # Do the import in the internal databases backends
    my %import_attributes = (
        company => $client_id,
        backends => \@backends,
        content => $cv_content,
        filename => $cv_filename,
        attributes => {
            %{ $candidate->to_talent_search_properties($user) },
            %{ $opts->{candidate_attributes} // {} },
            ( $locale ? ( locale => $locale ) : () ),
        }
    );

    $log->infof( "Will import in internal database(s): %s as client_id = %s", join(', ' , @backends ), $client_id );
    $log->infof( "CV Content is %s bytes", length($cv_content) );

    my $file_ref = $user->stream2->tsimport_api()->import_cv_content(%import_attributes);

    if ( my $file_id = $file_ref->{id} ){
        # store a row to be used to map the external candidate ID with that of the internal database
        foreach my $backend( @backends ) {
            # actually, the API and client will only every return 1 ID, but actually it doesn't matter...
            # as long as we have a mapping to the candidate in one of the "backends" who cares.. its a copy of the CV
            # as the TS import API has not for the capacity for storing multiple external IDs, for the time
            # being we are to work on the assumption that there will only ever be one "backend" per import.
            $user->stream2->factory('CandidateInternalMapping')->find_or_create({
                import_id => $file_id,
                group_identity => $user->group_identity,
                candidate_id => $candidate->id,
                backend => $backend
            });

            $log->infof( "Stored candidate tsimport mapping id: %s", $file_id );
        }
    }

    $log->infof( "Imported attributes: %s", Dumper($import_attributes{attributes}) );
    $log->infof( "Imported candidate %s:%s", $candidate->destination() ,$candidate->candidate_id() );

    return 1;
}

=head2 user_can_import_from

See L<App::Stream2::Plugin::Auth::Role::Provider>

=cut

sub user_can_import_from{
    my ($self, $user, $destination) = @_;

    # If the destination is ONE of the internal ones, no import button please
    # see story https://broadbean.atlassian.net/browse/SEAR-92
    if( grep{ $_ eq $destination } qw/talentsearch cb_mysupply candidatesearch adcresponses/ ){
        return;
    }

    # If the user has got no subscription to ANY internal backend, then
    # he cannot import ANYWAY.
    unless( $user->has_adcourier_search() ){ return };


    # So, the destination is not an internal one, the user has got internal destination (has_adcourier_search is true)
    # so we can save!
    return 1;
}

=head2 user_can_download_from

See L<App::Stream2::Plugin::Auth::Role::Provider>

A pure Adcourier user can always download a candidate (if they have a CV).

=cut

sub user_can_download_from{
    return 1;
}

=head2 get_shortlist_email_settings

See superclass.

=cut

sub get_shortlist_email_settings{
    my ($self, $user) = @_;
    $user // confess("Missing user");
    return $user->stream2()->stream_api->get_shortlist_email_settings($user->provider_id());
}

=head2 get_save_email_settings

See superclass.

=cut

sub get_save_email_settings{
    my ($self, $user) = @_;
    $user // confess("Missing user");
    return $user->stream2()->stream_api->get_save_email_settings($user->provider_id());
}


sub authenticate {
    my ( $self, $controller, $options_ref ) = @_;

    $options_ref //= {};

    my $code = $options_ref->{code} || $controller->param('code')
      or return;

    return $self->oauth_client->exchange_code( $code )->then(
        sub {
            my $user_ref = shift;

            my $office = $user_ref->{user}->{read_contact_details}->{office};
            if ($office && lc($office) eq 'zombie') {
                die "User is a zombie\n"
            }
            # state should just contain a criteria id to be loaded once the user is signed in
            my $state_ref = {};
            if( my $json_state = $controller->param('state') ){
                $state_ref = JSON::decode_json( $json_state );
            }
            if ( my $criteria_id = $state_ref->{criteria_id} ){
                #redirect_url gets deleted from session as soon as the page is being loaded
                $controller->session( 'redirect_url' => qq{/search/$criteria_id} );
            }

            ## Example $user_ref->{user}->{read_contact_details}:
            # {
            #     'contact_telephone' => '1234567894',
            #     'contact_email' => 'drop@broadbean.net',
            #     'contact_name' => 'Search Demo',
            #     'company' => 'andy'
            # };

            my $subscriptions = $user_ref->{user}->{read_stream_subscriptions} || {};

            # An overseer is a user who has the option to search as a user they oversee.
            # An admin is one who has access to the search preferences of those they oversee.
            my $is_overseer = 0;
            my $is_superadmin = 0;
            my $user_role = $user_ref->{user}->{user_role} || 'USER';
            if ( $user_role =~ m/\A(?:(?:SUPER)?ADMIN|TEAMLEADER)\z/ ){
                $is_overseer = 1;
            }
            if ( $user_role eq 'SUPERADMIN' ) {
                $is_superadmin = 1;
            }

            my $contact_details = $user_ref->{user}->{read_contact_details};
            my $user_settings   = delete( $user_ref->{user}->{read_contact_details}->{settings} );
            my $currencies = $user_ref->{user}->{read_currencies} || [ qw/GBP USD EUR/ ];

            # Can enable by passing manage_responses_view=1 when entering Search
            my $manage_responses_view = $state_ref->{manage_responses_view}; # && $user_settings->{responses}->{new_manage_responses_view};

            # If an overseer is logging in, if there is a criteria and
            # consultant_id log them in as that user, and perform the search,
            # if not send them to popuser to pick a user to log in as.
            if ( $is_overseer ) {
                my $redirect_url = '/search';

                if ( $state_ref->{consultant_id} && $state_ref->{criteria_id} ) {
                    $redirect_url =
                      sprintf "/login/as/%d?redirect_to=/search/%s",
                      $state_ref->{consultant_id},
                      $state_ref->{criteria_id};
                }

                $controller->session( 'redirect_url' => $redirect_url );
            }

            my $params_ref = {
                provider => {
                    user_id           => $user_ref->{user}->{user_id},
                    identity_provider => 'adcourier',
                    login_provider    => 'adcourier',
                    group_identity    => $user_ref->{user}->{read_contact_details}->{company},
                    group_nice_name   => $user_ref->{user}->{read_contact_details}->{company_nice_name},
                    navigation        => $self->_filter_tabs( $user_ref->{user}->{read_navigation_tabs} , { manage_responses_view => $manage_responses_view } ),
                    is_admin          => ( $user_role eq 'SUPERADMIN' ) || 0,
                    is_overseer       => $is_overseer,
                },
                contact_details => $user_ref->{user}->{read_contact_details},
                subscriptions   => $subscriptions,
                settings        => $user_settings,
                currencies      => $currencies,
                ## Note this is the vanilla shortlist feature
                ## it will work through this Provider's methods
                ## That is why it hasn't got any candidate URLs or anything like that.
                ## Just name and label.
                ripple_settings => {
                    'custom_lists' => [
                        {
                            name => 'adc_shortlist',
                            label => 'Shortlist',
                            allowPreview => 1,
                        },
                    ]
                },
                manage_responses_view => $manage_responses_view
            };

            # Autocomplete for adcourier users is fraught with issues, mostly because the antiquated user system
            # keep a table with a cache of user contact details
            my $cache_key = 'AdC_user_sync-'. $user_ref->{user}->{read_contact_details}->{company};
            $controller->app->stream2->stream2_cache->compute( $cache_key, '60 minutes', sub {
                # attempt to keep users in stream2 in sync with those in adcourier.
                $controller->queue_job(
                      class => 'Stream2::Action::SynchroniseAdCourierUserSiblings',
                      queue => 'SynchroniseAdCourierUsers',
                      parameters   => {
                        company         => $user_ref->{user}->{read_contact_details}->{company},
                        adc_id          => $user_ref->{user}->{user_id},
                        clean_missing   => $is_superadmin,
                      }
                );

                return 1;
            });

            return $params_ref;
        },
        sub {
            confess( shift );
        }
    );
}

sub _build_oauth_client {
    my $self = shift;
    return Stream2->instance->oauth_client('adcourier');
}

sub _filter_tabs {
    # Only store the visible and primary tabs in the session cookie with only the relevant information
    # I tried storing all of the data in the session cookie but I was getting a strange problem
    # where I was being sent to and fro from search to oauth, this was fixed by the filtering and 
    # minimising of the data as done below. I can only assume it was because the data was too large
    # to be stored in the session and the $controller->session( $user_ref ) in lib/App/Stream2/Plugin/Auth.pm
    # was failing and therefore the user was never logging in correctly.
    # something to keep in mind if we need to add more data to the user anyway!
    my ( $self, $tabs_arr , $options ) = @_;
    $options //={};

    my @out = ();

    my $active_tab = $options->{manage_responses_view} ? 'manageresponses': 'stream';

    foreach my $tab ( grep {
        $_->{show} && $_->{level} eq 'primary' && $_->{id} ne 'contactus'
    }
    sort {
        $a->{order} <=> $b->{order} or
        $a->{text} cmp $b->{text}
    } @{$tabs_arr} ){

        my $url = $tab->{href};


        # Only prepend if not already absolute.
        unless( $url =~ /^https?:\/\// ){
            $url = '//www.adcourier.com'.$url;
        }

        push @out , {
                     h => $url,
                     t => $tab->{text},
                     active => ( $tab->{id} eq $active_tab )
                    }
    }

    return \@out;
}

=head2 download_attachment

Given an adcourier based (in the sense of identity_provider) user and
an adcourier attachment name (something like 1460616486-2223-28-talentsearch_118524323.doc ),
downloads the binary content of it and returns it.

Or undef if no such thing exists.

Usage:

 if( my $attachment =  $this->download_attachment( $user , '134033-2-2-bladiblabla.doc' ) ){
    my $binary_content = $attachment->{content};
    my $filename       = $attachment->{filename};
 }

=cut

sub download_attachment {
    my ( $self, $user, $filename ) = @_;

    my $adcapi = $user->stream2->adcourier_api();
    my $response = $adcapi->request_json(
        GET => $adcapi->url( sprintf( '/%s/response_attachment', $user->group_identity ), { filename => $filename } )
    );

    unless ( $response ) {
        return;
    }

    # Base64 decode the binary.
    $response->{content} = MIME::Base64::decode_base64( $response->{content} );
    return $response;
}

=head2 get_hierarchical_identifier

For use with path type queries

Usage:

    my $user = Stream2::O::SearchUser->new({ username => 'consultant@team.office.company' });
    my $path = $this->get_hierarchical_identifier( $user );

Out:

    /company/office/team/consultant

=cut

sub get_hierarchical_identifier {
    my ( $self, $user ) = @_;
    my $username = ( $user->contact_details() // {} )->{username}
        or confess( "User ".$user->id()." does not have a username in its contact details." );
    my ( $consultant, $other ) = split( /\@/, $username );
    return join( '/', reverse( $consultant, split( /\./, $other ), '' ) );
}

# { company => 'xx', office => 'xxxx', team => 'xxx', user => 'x' } => /xx/xxxx/xxx/x
sub _get_hierarchical_identifier_from_ref {
    my ( $self, $user_ref ) = @_;
    return join( '/', '', grep { $_ } map { $user_ref->{$_} } qw/company office team consultant/ );
}

=head2 get_oversees_hierarchical_identifier

When reading from a hierarchically based system such as the Quota API, we need a way
of controller which part of the hierarchy a user can see, this method
returns a path indicating this level of hierarchy

Usage:

    $SearchUser->contact_details()->{username} = 'admin@office6.company3';
    my $path = $this->get_oversees_hierarchical_identifier( $SearchUser );

Out:

    /company3/office6

=cut

sub get_oversees_hierarchical_identifier {
    my ( $self, $user ) = @_;
    my $details = $user->contact_details();

    my $path_ref = {};
    if ( $details->{office} eq 'superadmin' ){
        $path_ref = { company => $details->{company} };
    }
    elsif ( $details->{consultant} eq 'admin' ) {
        $path_ref = { company => $details->{company}, office => $details->{office} };
    }
    elsif ( $details->{consultant} eq 'teamleader' ){
        $path_ref = { company => $details->{company}, office => $details->{office}, team => $details->{team} };
    }
    else {
        return $self->get_hierarchical_identifier( $user );
    }

    return $self->_get_hierarchical_identifier_from_ref( $path_ref );
}

__PACKAGE__->meta->make_immutable;

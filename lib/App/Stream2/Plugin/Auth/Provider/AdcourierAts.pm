package App::Stream2::Plugin::Auth::Provider::AdcourierAts;

use Moose;
extends qw/App::Stream2::Plugin::Auth::Provider::Adcourier/;

use Log::Any qw/$log/;
use DateTime;
use XML::LibXML;
use MIME::Base64;
use Data::Dumper;
use Promises;
use HTTP::Request::Common;
use String::Snip;

use Stream2;
use Stream::EngineException;
use SQL::Abstract;

sub show_header {
    my ( $self, $current_user ) = @_;
    return $current_user->ripple_settings()->{show_adcourier_navigation} // 0;
}

sub show_advert_preview { 0; }

sub _build_oauth_client{
    my ($self) = @_;
    return Stream2->instance->oauth_client('adcourier_ats');
}

# this gets registered as a helper.
# That is why it has a controller parameter.
## This login url has to be the standard adcourier one.
sub login_url {
    my ($self,$controller) = @_;
    return $controller->auth_provider({ provider => 'adcourier'} )->oauth_client->auth_url();
}

=head2 after_sign_in

Slurp the data from the adcourier_ats user if such a thing exists.

Note: after a little while (like 6 months after november 2015)

=cut

sub after_sign_in{
    my ($self , $user ) = @_;
    unless( $user->provider() eq 'adcourier' ){
        confess("This should never sign in a non adcourier user");
    }

    my $stream2 = $user->stream2();
    my $other_user = $stream2->factory('SearchUser')->search({ provider => 'adcourier_ats',
                                                               provider_id => $user->provider_id(),
                                                               group_identity => $user->group_identity()
                                                           })->first();
    unless( $other_user ){
        $log->info("No other adcourier_ats user for ".$user->id()." Doing nothing");
        return;
    }

    # We try to slurp data only every 120 minutes. This is
    # to avoid error flooding in case something goes wrong.
    my $cache_key = __PACKAGE__.'::after_sign_in.adopt_data'.$user->id();
    $stream2->stream2_cache->compute( $cache_key, '120 minutes',
                                      sub {
                                          $log->info("Found other adcourier_ats user for ".$user->id().". Adopting data from ".$other_user->id());
                                          # Bit of tech reporting. We really want to know if that goes pop.
                                          $stream2->report_tech([ $user->id() , $other_user->id() ] , sub{
                                                                    my ($s2, $user_id, $other_id ) = @_;
                                                                    my $user = $s2->factory('SearchUser')->find($user_id);
                                                                    my $other_user = $s2->factory('SearchUser')->find($other_id);
                                                                    my $res = $user->adopt_data_from($other_user);
                                                                    $other_user->delete();
                                                                    1;
                                                                });
                                          # Reload the user from the DB. Stuff could have changed.
                                          $user->discard_changes();
                                          return 1;
                                      });
    return 1;
}



sub find_action{
    my ($self, $current_user,  $action_name) = @_;
    my $schema = $current_user->stream2->stream_schema();
    unless( $action_name =~ /shortlist/ ){
        # standard behaviour.
        return $schema->resultset('CandidateActions')->find({ action => $action_name });
    }

    # For shortlisting, we need to do more checkin'
    # Get the ripple settings. Check the action_name maches one of the lists.

    ## Extract the list name from the action name
    my ($list_name) = $self->_extract_list_name( $action_name );

    my $ripple_settings = $current_user->ripple_settings();

    my ( $list_settings ) = grep { $_->{name} eq $list_name } @{$ripple_settings->{custom_lists} // []};
    unless( $list_settings ){
        confess("Action '$action_name' is invalid. No such '$list_name' setting exists in ripple settings");
    }

    # We are talking about the completely standard adcourier shortlist feature.
    # Use the super class method.
    if( $list_settings->{name} eq 'adc_shortlist' ){
        return $self->next::method($current_user, $action_name);
    }

    # All is fine. We can vivify this action.
    return $schema->resultset('CandidateActions')->find_or_create({ action => $action_name });
}

sub _extract_list_name {
    my $self = shift;
    my ( $action_name ) = @_;

    my ($list_name) = ( $action_name =~ /shortlist_(.+?)(?:_fail)?$/ );
    return $list_name;
}


=head2 get_adverts

Get a list of adverts ( structures like { label => .. , advert_id => .., preview_url => ... } )
for the given search user and options.

Usage:

  my $adverts = $this->get_adverts( $user,
                                    { list_name => 'cheese_sandwich',
                                      keywords => 'typed keywords for quick searching'
                                    });

Or if you want just ONE advert you know the ID for:

  my $adverts = $this->get_adverts( $user,
                                    { list_name => 'cheese_sandwich',
                                      advert_id => 1234
                                    });


=cut

sub get_adverts{
    my ($self,  $search_user , $opts) = @_;

    my $now = DateTime->now();

    my $list_name = $opts->{list_name} // confess("Missing list_name");

    my $ripple_settings = $search_user->ripple_settings();

    my ( $list_settings ) = grep { $_->{name} eq $list_name } @{$ripple_settings->{custom_lists} // []};
    unless( $list_settings ){
        confess("Missing list_settings for list named '$list_name' on user ID = ".$search_user->id());
    }

    # We are talking about the completely standard adcourier shortlist feature.
    # Use the super class method.
    if( $list_settings->{name} eq 'adc_shortlist' ){
        return $self->next::method($search_user, $opts);
    }


    my $options_url = $list_settings->{options_url};
    unless( $options_url  ){ confess("Missing options_url in list settings for list '$list_name'"); }

    $log->info("Will hit URL $options_url for adverts");
    # Build the request according to our funky DTD
    my $xdoc = XML::LibXML::Document->new('1.0', 'UTF-8');
    my $xreq = $xdoc->createElement('SearchCustomList');
    $xdoc->setDocumentElement($xreq);

    $xreq->appendChild($xdoc->createElement('Version'))->appendText('3.0');
    $xreq->appendChild($xdoc->createElement('Time'))->appendText($now->iso8601());
    my $xlist = $xreq->appendChild($xdoc->createElement('CustomList'));
    $xlist->setAttribute('name' , $list_name);

    # When we ask for adverts matching keywords
    if( my $keywords = $opts->{keywords} ){
        $xlist->appendChild($xdoc->createElement('Filter'))->appendText($keywords);
    }

    # When we ask for just ONE advert_id
    if( my $advert_id = $opts->{advert_id} ){
        $xlist->appendChild($xdoc->createElement('Value'))->appendText($advert_id);
    }
    $xreq->appendChild($search_user->ripple_xdoc_custom_fields($xdoc));

    # A bit of quality control
    # This would crash if we are naughty.
    $xdoc->validate( $search_user->stream2->dtd('ripple/custom-list-request.dtd') );

    # Dummy case. Nice to have in dev.
    if( $options_url =~ /_this_is_dummy_/ ){
        if( $list_name eq 'cheese_sandwiches' ){
            return [
                { label => 'Emmental' , advert_id => 1,  preview_url => 'http://www.cheese.com' },
                { label => 'Cheddar'  , advert_id => 2,  preview_url => 'http://www.cheese.com' },
                { label => 'Gruyere'  , advert_id => 3,  preview_url => 'http://www.cheese.com' },
                { label => 'Gouda'    , advert_id => 4,  preview_url => 'http://www.cheese.com' },
            ];
        }else{
            return [
                    { label => 'Chimay'          , advert_id => 1, preview_url => 'http://www.beer.com' },
                    { label => 'Bombardier'      , advert_id => 2, preview_url => 'http://www.beer.com' },
                    { label => 'Leffe'           , advert_id => 3, preview_url => 'http://www.beer.com' },
                    { label => 'Delirium tremens', advert_id => 3, preview_url => 'http://www.beer.com' },
                   ];
        }
    }

    # Posting this XML
    {
        my $post = HTTP::Request::Common::POST( $options_url ,
                                                %{ $search_user->ripple_header_custom_fields() },
                                                'Content-Type' => 'text/xml',
                                                Content => $xdoc->toString(1) );

        $log->trace("REQUEST " . String::Snip::snip($post->as_string()) ) if $log->is_trace();
        my $resp = $search_user->stream2->user_agent->request( $post );
        $log->trace("RESPONSE ".$resp->as_string()) if $log->is_trace();
        unless( $resp->is_success() ){
            $log->error("Posting to $options_url failed: ".$resp->status_line());
            return [];
        }

        # Analyze the response.
        my $xml_parser = $search_user->stream2()->xml_parser();
        my $xres =  eval{ $xml_parser->load_xml( string => $resp->content() ); };
        unless( $xres ){
            $log->error("Failed to parse ".$resp->content().' PARSING FAILURE: '.$@);
            return [];
        }

        # Validate the shit out of it
        eval{ $xres->validate($search_user->stream2()->dtd('ripple/custom-list-response.dtd') ); };
        if( my $err = $@ ){
            $log->error("Failed to validate XML ".$resp->content().' : '.$err);
            return [];
        }

        # Xresponse is all good.
        my @xoptions = $xres->findnodes('/SearchCustomListResponse/Options/Option');
        my @options = ();
        foreach my $xopt ( @xoptions ){
            push @options , {
                             label => scalar($xopt->textContent()),
                             advert_id => scalar($xopt->getAttribute('value')),
                             preview_url => scalar( $xopt->getAttribute('previewURL')),
                            };
        }
        return \@options;
    }

}

sub find_advert{
    my ($self, $search_user, $advert_id, $opts ) = @_;
    $opts //= {};
    my $list_name = $opts->{list_name};
    unless( $list_name ){ confess("Missing list_name in options"); }

    # We are talking about the completely standard adcourier advert.
    # Use the super class method.
    if( $list_name eq 'adc_shortlist' ){
        return $self->next::method($search_user, $advert_id,  $opts);
    }


    # Find the ripple settings and this listname settings.
    my $ripple_settings = $search_user->ripple_settings();

    my ( $list_settings ) = grep { $_->{name} eq $list_name } @{$ripple_settings->{custom_lists} // []};
    unless( $list_settings ){
        $log->warnf('Missing list_settings for list named %s', $list_name);
        return;
    }

    # If a list allows preview, it means
    # it implements querying just ONE advert
    if( $list_settings->{allowPreview} ){
        my ( $advert ) = @{ $self->get_adverts( $search_user , { list_name => $list_name, advert_id => $advert_id }) };
        unless( $advert ){ confess("Could not find advert with list_name = $list_name and advert_id = $advert_id. Not implemented?"); }
        return {
            list_name => $list_name,
            advert_id => $advert_id,
            stream2_jobref => $list_name.':'.$advert_id ,
            stream2_jobtitle => $list_name.':'.$advert_id,
            %$advert,
        };
    }

    ## HERE WE RETURN ONLY THE { list_name => ... , advert_id => ... }
    ## This is because the list doesn't specify
    ## that it implements preview URLs (allowPreview).
    ## In this case, there's no point asking for the implementation of returning just one
    ## advert.
    return { list_name => $list_name , advert_id => $advert_id , stream2_jobref => $list_name.':'.$advert_id , stream2_jobtitle => $list_name.':'.$advert_id };
}

=head2 advert_preview_url

Given an advert (for instance found by 'find_advert'),
returns the preview URL of it if such a thing exists.

=cut

sub advert_preview_url{
    my ($self, $advert, $search_user, $opts) = @_;

    # Does the advert have an aplitrack ID?
    if( ref( $advert ) && $advert->{AplitrakID} ){
        return $self->next::method($advert, $search_user, $opts);
    }

    return $advert->{preview_url} || undef;
}

=head2 advert_shortlist_candidate

Usage:

 $this->advert_shortlist_candidate($advert, $candidate ,$cv_hash , $search_user , $options);

Note that advert is not mandatory.

=cut

sub advert_shortlist_candidate{
    my ($self, $advert, $candidate , $cv_hash , $search_user , $opts) = @_;

    $opts //= {};

    ## Find where to send the candidate.
    my $ripple_settings = $search_user->ripple_settings();

    $log->debug("Got ripple_settings in $ripple_settings= ".Dumper($ripple_settings));

    my $new_candidate_url = $ripple_settings->{new_candidate_url};

    # shortlist candidate should return undef on success
    if ( my $error = $self->next::method($advert, $candidate ,$cv_hash , $search_user , $opts ) ){
        return $error;
    }

    # No new candidate URL. Use the default adcourier stuff.
    unless ( $new_candidate_url || scalar(@{ $ripple_settings->{webhooks} || [] }) ) {
        return undef;
    }

    my $cv_parsing_res = $opts->{cv_parsing_res};
    unless( $cv_parsing_res ){
        my $cv_b64 = $cv_hash->{cv_content} // confess("No cv_content in cv_hash");

        # We ALWAYS want to CV parse.
        $log->info("CV Has not been parsed before. Parsing it now");
        $cv_parsing_res = $search_user->parse_cv_with_locale(MIME::Base64::decode_base64($cv_b64));

        $log->infof('Parsing result for %s, user_id: %s, candidate_id: %s, locale: %s, postcode: %s',
            $search_user->group_identity,
            $search_user->id,
            $candidate->id,
            $cv_parsing_res->{locale},
            $cv_parsing_res->{post_code}
        ) if $cv_parsing_res;
    }

    if( $cv_parsing_res ){
        $candidate->enrich_with_cvparsing($cv_parsing_res);
        $log->trace("CV PARSER RES: ".Dumper($cv_parsing_res)) if $log->is_trace();
        $opts->{cv_parsing_res} = $cv_parsing_res;
    }

    ## Right, we have an advert as given by find_advert.
    ## We have a candidate, we have a CV, we have a Stream2::SearchUser.
    my $xdoc = $candidate->to_xml($advert, $cv_hash, $search_user, $opts);
    my $candidate_xml_str = $xdoc->toString(1);

    # WARNING!!!!
    # When calling candidate_xml_to_webhook_xml() with $xdoc (Candidate XML object generated with XML::LibXML)
    # the original object will be updated and will no longer validate against the candidate.dtd.
    # This is the reason the candidate XML is converted to a string and stored in $candidate_xml_str
    # If you need the candidate XML after this point, you can either use the stringified version or call $candidate->to_xml() again (highly inefficient)
    if ( scalar(@{ $ripple_settings->{webhooks} || [] }) ){
        $search_user->emit_webhook("candidate/shortlist", $candidate, $xdoc, $advert);
    }

    if ( $new_candidate_url && $new_candidate_url =~ /_this_is_dummy/ ){
        $log->info("This is a dummy URL. Not doing anything");
        return undef;
    }

    my $post = HTTP::Request::Common::POST(  $new_candidate_url,
                                             %{ $search_user->ripple_header_custom_fields() },
                                             'Content-Type' => 'text/xml',
                                             Content => $candidate_xml_str );

    {
        # We dont want the full CV Base64 in the log
        my $request_string = $post->as_string();
        $request_string =~ s|(<Doc.*?>)(.+?)(</Doc>)|"$1<!-- removed to reduce log size: ".length($2)." bytes -->$3"|esg;
        $log->trace("REQUEST " . String::Snip::snip($request_string) ) if $log->is_trace();
    }

    my $resp = $search_user->stream2->user_agent->request($post);
    $log->tracef( "RESPONSE " . $resp->as_string() ) if $log->is_trace();

    unless ( $resp->is_success() ) {
        $log->errorf( "Request to %s failed: %s", $new_candidate_url, $resp->status_line() );
        return { type => 'unmanaged', message => sprintf("Failed to send candidate to '%s': %s", $new_candidate_url, $resp->status_line() ) };
    }

    if ( $resp->content ) {
        # parse the response
        my $xml_parser = $search_user->stream2()->xml_parser();
        my $xres = eval { $xml_parser->load_xml( string => $resp->content() ); };

        if ( $@ ) {
            $log->error( "Failed to parse " . $resp->content() . ' PARSING FAILURE: ' . $@ );
            return { type => 'unmanaged', message => $resp->content };
        }

        if ( $xres->findvalue('/StreamCandidateResponse/Status') eq 'ERROR' ) {
            my ($error) = $xres->findvalue('/StreamCandidateResponse/Message');
            $log->error( "Request to $new_candidate_url failed: " . $error );
            return { type => 'managed', message => "Failed to send candidate to '$new_candidate_url': " . $error };
        }
    }
    return undef;
}


sub user_import_candidate {
    my ( $self, $user, $cv_hash, $candidate, $opts ) = @_;
    $opts //= {};
    my $stream2 = $user->stream2();

    $log->infof( "Entering user_import_candidate on %s", $self );
    if ( my @adcourier_backends = $self->get_internal_database_backends( $user, $candidate ) ) {
        #
        # Use the method from the Adcourier provider to import the candidate
        # in the compatible internal databases only.
        #
        $self->import_candidate_to_backends( $user, $cv_hash, $candidate, $opts, \@adcourier_backends );
    }

    my $ripple_settings = $user->ripple_settings();

    my $new_candidate_url = $ripple_settings->{new_candidate_url};
    if ( $new_candidate_url || scalar(@{ $ripple_settings->{webhooks} || [] }) ) {

        # In opts would be board_nice_name, which is needed by
        # the Candidate XML we would send to the board

        $log->infof( "Will send candidate %s to Ripple endpoint %s", $candidate->id(), $new_candidate_url );

        # Reuse the advert_shortlist_candidate, except this time, there is no advert.
        if ( my $failure = $self->advert_shortlist_candidate( undef, $candidate, $cv_hash, $user, $opts ) ) {
            $failure->{type} eq 'managed' ?
                die Stream::EngineException::ImportingError->new( { message => $stream2->__("Cannot save candidate: $failure->{message}") } )
                : die $failure->{message}; # unmanaged
        }

        $log->infof( "Successfully sent candidate %s to Ripple endpoint %s", $candidate->id(), $new_candidate_url );
    }

    return 1;
}

=head2 user_can_import_from

See L<App::Stream2::Plugin::Auth::Provider::Adcourier>

Decides whether to show the import button based on Ripple settings.

Will show the import button if an import URL is provided in the Ripple request,
unless the hide save button config options are true.

=cut

sub user_can_import_from{
    my ($self, $user , $destination ) = @_;

    my $ripple_settings = $user->ripple_settings();
    my $subscription = $user->has_subscription($destination);

    return if $ripple_settings->{hide_save} ||
        ( $ripple_settings->{hide_save_internal} && $subscription->{type} eq 'internal' );

    return $ripple_settings->{new_candidate_url} ? 1 : $self->next::method($user, $destination);
}

=head2 user_can_download_from

Users can always download a CV (if there is one). EXCEPT when
the destination (feed) is bullhorn.

=cut

sub user_can_download_from{
    my ($self, $user, $destination) = @_;
    return $destination ne 'bullhorn';
}


__PACKAGE__->meta->make_immutable;

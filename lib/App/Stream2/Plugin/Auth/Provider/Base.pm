package App::Stream2::Plugin::Auth::Provider::Base;

use Moose;

# All the doc is there
with 'App::Stream2::Plugin::Auth::Role::Provider';

has 'user_hierarchy_header' => ( is => 'ro', isa => 'ArrayRef[Str]' );

sub find_action{
    my ($self, $current_user , $action_name) = @_;
    return $current_user->stream2->stream_schema->resultset('CandidateActions')->find({ action => $action_name });
}

sub user_can_import_from{ confess("Not implemented"); }
sub user_can_download_from{ confess("Not implemented"); }
sub user_import_candidate{ confess("Not implemented"); }
sub advert_preview_url{ confess("Not implemented"); }
sub advert_shortlist_candidate{ confess("Not implemented"); }
sub authenticate{ confess("Not implemented"); }
sub find_advert{ confess("Not implemented"); }
sub get_adverts{ confess("Not implemented"); }
sub login_url{ confess("Not implemented"); }
sub application_history{ confess("Not implemented"); }
sub flagging_history{ confess("Not implemented"); }
sub user_oversees{ confess("Not implemented"); }
sub download_attachment{ confess("Not implemented"); }
sub get_hierarchical_identifier{ confess("Not implemented"); }
sub get_oversees_hierarchical_identifier{ confess("Not implemented"); }

=head2 after_sign_in

a hook called once an external user is resolved to a search user

provider::authenticate -> user_sign_in -> provider::after_sign_in

=cut

sub after_sign_in{ return undef; }

=head2 user_siblings_ids

Returns a list of L<Stream2::O::SearchUser> IDs of the given
L<Stream2::O::SearchUser> in the hierarchy that would be returned by 'user_overseen_hierarchy'

Note that this EXCLUDES this given user.

This default implementation simply returns all users of the same group_identity, but
different providers will implement that as they wish.

=cut

sub user_siblings_ids{
    my ($self, $user) = @_;

    my $user_id = $user->id() || confess("Please give a user saved in the DB (with an id)");

    my $schema = $user->stream2()->stream_schema();
    return $schema->resultset('CvUser')->search({provider => $user->provider(),
                                                 group_identity => $user->group_identity(),
                                                 id => { '!=' => $user->id() }
                                                },
                                                { select => [ 'id' ] } # Only interested in the id
                                               )->get_column('id')->all();
}

=head2 user_overseen_hierarchy

returns a tree of overseen users that looks like:

   {
        label: "Company1",
        children: [
            {
                label: "office1",
                children: [
                    {
                        label: "A real user",
                        id: 123
                    },
                    ...
                ]
            },
            ...
        ]
    };


=cut

sub user_overseen_hierarchy{
    my ($self, $user) = @_;

    my $stream2 = $user->stream2();

    my $all_users = $stream2->factory('SearchUser')->search({provider => $user->provider(),
                                                             group_identity => $user->group_identity()},
                                                            { order_by => [ 'contact_name' , 'contact_email' , 'id' ] });

    my @flat_children = ();
    while( my $overseen_user = $all_users->next() ){
        push @flat_children , { label => ( $overseen_user->contact_name() // '' ).' ('.( $overseen_user->contact_email() || $overseen_user->id() ).')',
                                id => $overseen_user->id() };
    }
    return {
            label => $user->group_identity(),
            children => \@flat_children
           };
}

=head2 autocomplete_colleagues

searches the search 2 user table for users of the same group_identity matching keywords against their name email

Usage:

    my @search_users = $auth_provider->autocomplete_colleagues( $user, $keywords );

=cut

sub autocomplete_colleagues {
    my ( $self, $user, $stem ) = @_;

    my @users = $user->stream2->factory('SearchUser')->autocomplete($user->group_identity(), $stem )
        ->search(undef, {
            rows => 10,
            order_by => [ 'contact_name', 'contact_email' ]
        })->all();

    return @users;
}


=head2 get_shortlist_email_settings

Gets the shortlisting email settings of a user

Usage:

 if( my $email_settings = $this->get_shortlist_email_settings($user) ){
    ...
 }

=cut

sub get_shortlist_email_settings{ return undef; }

=head2 get_save_email_settings

Gets the 'save' (aka Import) feature email settings of a user

Usage:

 if( my $email_settings = $this->get_save_email_settings($user) ){
    ...
 }

=cut

sub get_save_email_settings{ return undef; }

=head2 user_exists

Test that the given user PROVIDER ID exists remotely.

Usage:

   if( $this->user_exists($provider_id) ){
     ...
   }

=cut

sub user_exists{
    ...
}

__PACKAGE__->meta->make_immutable();

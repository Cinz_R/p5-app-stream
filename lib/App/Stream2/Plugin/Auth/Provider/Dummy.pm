package App::Stream2::Plugin::Auth::Provider::Dummy;

use Moose;
extends qw/App::Stream2::Plugin::Auth::Provider::Base/;

has '+user_hierarchy_header' => ( default => sub{ [ 'Provider', 'Group Identity', 'User' ] } );


=head1 NAME

App::Stream2::Plugin::Auth::Provider::Dummy - A Dummy provider to be used in test suites.

=cut

=head2 user_exists

See superclass.

=cut

sub user_exists{
    my ($self, $provider_id) = @_;
    return 1;
}

# User oversees here returns
# all the users from the same group. Including myself.
sub user_oversees{
    my ($self, $user) = @_;
    my $user_rs = $user->stream2->factory('SearchUser')->search(
        {
            'me.group_identity'  => $user->group_identity(),
            'me.provider'        => $user->provider(),
        },{ prefetch => undef }
    );

    return $user_rs;
}

=head2 get_hierarchical_identifier

For use with path type queries

Usage:

    my $user = Stream2::O::SearchUser->new({ username => 'consultant@team.office.company' });
    my $path = $this->get_hierarchical_identifier( $user );

Out:

    /dummy/group_identity/user_id

=cut

sub get_hierarchical_identifier{
    my ($self, $user) = @_;
    return '/dummy/'.$user->group_identity().'/'.$user->id();
}


__PACKAGE__->meta->make_immutable;

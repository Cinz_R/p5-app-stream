package App::Stream2::Plugin::Auth::Provider::Demo;

use Moose;
extends qw/App::Stream2::Plugin::Auth::Provider::Base/;
use Promises;


has '+user_hierarchy_header' => ( default => sub{ [ 'User ID' ] } );


has 'username' => (
  is       => 'ro',
  isa      => 'Str',
  required => 1,
  lazy     => 1,
  builder  => '_default_username',
);

has 'password' => (
  is       => 'ro',
  isa      => 'Str',
  required => 1,
  lazy     => 1,
  builder  => '_default_password',
);

sub login_url { 'login' }
sub logout_url { '/login?logged_out=1' }
sub retry_login { 1 }
sub _default_username { 'demo' }
sub _default_password { 'demo' }

sub get_adverts{
    my ($self, $search_user , $opts) = @_;
    return [];
}

sub find_advert{
    return undef;
}

sub advert_preview_url{
    return undef;
}

sub advert_shortlist_candidate{
    confess("No shortlisting in Demo providers");
}

sub authenticate {
    my ($self, $c, $opts) = @_;

    # to stay compliant with the async nature of authentication, we need to
    # return a promise.
    my $d = Promises::deferred;

    if ( my $demo_user = $self->_authenticate( $c, $opts ) ){
        $d->resolve( $demo_user );
    }
    else {
        $d->reject( "Usename and Password are Incorrect." )
    };

    return $d->promise;
}

sub _authenticate {
  my ($self, $controller, $options_ref) = @_;

  my $username = $options_ref->{username} || $controller->param('username')
    or return;
  my $password = $options_ref->{password} || $controller->param('password')
    or return;

  if ( $username eq $self->username && $password eq $self->password ){
      return $self->demo_user();
  }
  return;
}

sub demo_user {
    my $self = shift;

    my $user_ref = {
        user_id => 'andy@users.live.andy',
        contact => {
            contact_name => 'Andy Jones',
            contact_telephone => '012345678901',
            contact_email => 'andy@broadbean.com',
            company => 'andy',
        },
        subs => {
            talentsearch => {
                nice_name => "Talent Search",
                auth_tokens => {},
                type => "internal"
            },
            careerbuilder => {
                nice_name => 'CareerBuilder',
                auth_tokens => { careerbuilder_email => 'RDBwsTest@careerbuilder.com', careerbuilder_password => 'jePub7KE' },
                type => "external"
            }
        }
    };

    return {
        provider        => {
            user_id         => $user_ref->{user_id},
            login_provider  => 'demo',
            identity_provider => 'demo',
            group_identity  => $user_ref->{contact}->{company}
        },
        contact_details => $user_ref->{contact},
        subscriptions   => $user_ref->{subs},
            ripple_settings => {
                                'custom_lists' => [
                                                   { name => 'vanilla_shortlist',
                                                     label => 'Shortlist',
                                                   },
                                                  ]
                               }
    };
}

sub get_oversees_hierarchical_identifier{
    my ( $self, $user ) = @_;
    return 'Demo-'. $user->id;
}

sub user_can_download_from {
    my ($self, $current_user, $destination) = @_;
    return 1 if $destination eq 'dummy';
    return 0;
}

sub user_can_import_from {
    return 0;
}

__PACKAGE__->meta->make_immutable;

package App::Stream2::Plugin::Auth::Providers;

use Mojo::Base 'Mojo::EventEmitter';
use Mojo::Loader;
use Mojo::Util 'camelize';

has namespaces => sub { ['App::Stream2::Plugin::Auth::Provider'] };

use Log::Any qw/$log/;

sub load_provider {
  my ($self, $name) = @_;
  # Full module name
  $log->trace("Attempting to load '$name'");
  return $name if _load($name);

  # Try all namespaces
  my $class = $name =~ /^[a-z]/ ? camelize($name) : $name;
  foreach my $namespace (@{ $self->namespaces }) {
    my $module = $namespace . '::' . $class;
    $log->trace("Attempting to load '$module'");
    return $module if _load($module);
  }

  die qq{Could not find the "$name" provider};
}

sub register_provider {
  my ($self, $provider_name, @provider_args) = @_;
  $self->load_provider($provider_name)->new(@provider_args);
}

sub _load {
  my $module = shift;
  if ( my $e = Mojo::Loader::load_class($module)) {
    ref $e ? die $e : return undef;
  }
  return $module->can('does') && $module->does('App::Stream2::Plugin::Auth::Role::Provider') ? 1 : undef;
}

1;

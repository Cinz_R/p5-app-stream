package App::Stream2::Plugin::Auth::Role::Provider;

use Moose::Role;

requires 'authenticate';
requires 'login_url';

# Provider level settings
sub logout_url { 0; }

# on unsuccessful logins, show the login screen / forward the user again
sub retry_login { 1; }

# bb logo and login name / email - accepts the current user
sub show_header { 1; }

# Can add a link to an advert when messaging candidates, this is a very specific option
sub show_advert_preview { 0; }


=head2 application_history

    given a candidate object, return any associated application data

    [
        {
            application_time => '2014-08-09 16:55:22',
            job_title        => 'Web Developer',
            advert_link      => '/view-vacancy.cgi?script_action=view_responses;advert_id=144788#response_59935',
            rank_colour      => 'green', # indication of flagging in adc - optional
        }
    ]

=cut

requires 'application_history';

=head2 flagging_history

    given an array of candidates, fetch the flagging history.

    [
        {
            colour => 'red',
            count  => 15,
        },
        {
            colour => 'amber',
            count  => 1,
        },
        ...
    ]

=cut

requires 'flagging_history';

## Adverts stuff (Nipple API compatible)
requires 'get_adverts';

=head2 get_adverts

Gets a collection of adverts PURE PERL datastructures as an ArrayRef
this L<Stream2::SearchUser> user can access, possibly with some keywords filtering.

These Perl Hashes MUST have a key 'label' and a key 'advert_id'. The rest is up to you.

Its possible this returns nothing if this user doesnt exist in adcourier.

Usage:

 my @adverts = @{$this->get_adverts($current_user)};

 my @adverts = @{$this->get_adverts($current_user, { opt1 => .. , opt2 => ..})};

Options:

 limit, keywords, offset

=cut

requires 'find_advert';

=head2 find_advert

Finds one advert (a free formed hashref) from the adverts source by using the given options.
Returns undef if nothing is found.

The returned hashref will ALWAYS contain at least { stream2_jobref => ... } , giving stream2 a consistent
jobref accross different sources.

Usage:

 my $advert = $this->find_advert( $search_user, $advert_id , { %opts } );

=cut

requires 'advert_preview_url';

=head2 advert_preview_url

Returns a preview pure absolute URL string from the given Advert data structure,
for the given L<Stream2::SearchUser> or undef if no such a thing exists.

The $advert data structure should be something given back from $this->find_advert or by $this->get_adverts.

Usage:

 my $url = $this->advert_preview_url($advert, $search_user, { %opts });

=cut

requires 'advert_shortlist_candidate';

=head2 advert_shortlist_candidate

Shortlist the given candidate with the given CV hash against the given advert
In the User context, with some options.

Returns empty in case everything went fine, or an error message if something went wrong.

$advert should be something from either find_advert or get_adverts

$candidate is a L<Stream2::Results::Result> (a result is a candidate).

$cv_hash is { cv_mimetype => .. , cv_filename => .. , $cv_content => 'Base64 encoded binary' }

$user is the current L<Stream2::SearchUser> as usual.

$opts can be a hash of anything.

Usage:

  $this->advert_shortlist_candidate( $advert, $candidate , $cv_hash , $user , { %opts } );

=cut

=head2 user_import_candidate

A user imports a candidate, free of any advert in its database.

The $user, $cv_hash and $candidate arguments are the same as in L<advert_shortlist_candidate>

Usage:

  $this->user_import_candidate( $user , $cv_hash , $candidate );

=cut

requires 'user_import_candidate';

=head2 user_can_import_from

The given $user (a L<Stream2::SearchUser>) can import (save) candidates from the given $destination (as a string).
If the candidate has got a CV of course.

Usage:

 if( $this->user_can_import_from($user, $destination);

=cut

requires 'user_can_import_from';

=head2 user_can_download_from

The given $user (a L<Stream2::SearchUser>) can download candidates from the given $destination (as a string).
If the candidate has got a CV of course.

=cut

requires 'user_can_download_from';

1;

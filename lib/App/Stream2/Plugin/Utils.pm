package App::Stream2::Plugin::Utils;
use Mojo::Base 'Mojolicious::Plugin';

use Carp;
use Log::Any qw/$log/;
use Data::Validate::IP ();

=head1 NAME

App::Stream2::Plugin::Utils - Various utilities for our mojo App.

=cut

sub register {
    my ($self, $app, $config) = @_;

    $app->helper( 'remote_ip' => sub{
        my ($c) = @_;

        if ( my $real_ip =  $c->req->headers->header('X-Real-IP') ) {
            unless( $real_ip =~ m/unknown/i || ! Data::Validate::IP::is_public_ip($real_ip) ) {
                $log->infof("Remote IP is %s (from header: X-Real-IP)", $real_ip);
                return $real_ip;
            }
        }

        if ( my $forwarded_for = $c->req->headers->header('X-Forwarded-For') ) {
            my ($forwarded_for_ip) = grep { $_ } split('[, ]+', $forwarded_for);
            if ( $forwarded_for_ip ) {
                $log->infof("Remote IP is %s (from header: X-Forwarded-For)", $forwarded_for_ip);
                return $forwarded_for_ip;
            }
        }

        if ( my $forwarded = $c->req->headers->header('Forwarded') ) {
            my ($forwarded_ip) = $forwarded =~ /for=([\d\.]+)/;
            if ( $forwarded_ip ) {
                $log->infof("Remote IP is %s (from header: Forwarded)", $forwarded_ip);
                return $forwarded_ip;
            }
        }

        $log->info("Remote IP is " . $c->tx->remote_address);
        return $c->tx->remote_address;
  });
}

1;

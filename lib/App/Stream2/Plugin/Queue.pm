package App::Stream2::Plugin::Queue;
use Mojo::Base 'Mojolicious::Plugin';
use Promises qw[ deferred collect ];
use Mojo::IOLoop;

=head1 NAME

App::Stream2::Plugin::Queue - Queue primitives for App::Stream2

=cut

sub register {
  my ($self, $app, $config) = @_;
  $self->register_helpers( $app );
}

sub register_helpers {
  my ($self, $app) = @_;

  foreach my $name ( $self->queue_helpers() ) {
    $app->helper( $name => sub { $self->$name(@_) } );
  }
}

# plugin helpers
sub queue_helpers {
  return qw(do_job retrieve_job queue_job wait_jobs wait_job);
}

=head2 do_job

Helper. Enqueues and wait for the job.

Returns a L<Promises>

=cut

sub do_job {
    my $self = shift;
    my $controller = shift;

    my $job = $self->queue_job( $controller, @_ );
    return $self->wait_job( $controller, $job );
}

sub wait_job {
    my $self = shift;
    my $controller = shift;
    my $job = shift;

    my $d = deferred;

    ## Have a look there: http://mojolicio.us/perldoc/Mojolicious/Guides/Cookbook
    # I think we were missing Mojo::IOLoop->remove($id).
    # Hence the zillions of die "Job ABC1234 not found, has it expired after 900 seconds"
    # in the STDERR
    my $loop_id;
    $loop_id = Mojo::IOLoop->recurring( 0.3, sub {
                                            # Protect against any kind of refreshing failure.
                                            eval{
                                                $job->refresh;
                                            };
                                            if( my $err = $@ ){
                                                $controller->app()->log->error("Error in job polling: ".$err);
                                                $d->reject( $job );
                                            }
                                            if ( $job->is_finished ){
                                                ## Removing this loop before executing
                                                ## the reject or the resolve.
                                                ## This will prevent this loop to carry on being called
                                                ## when one of those things crash.
                                                Mojo::IOLoop->remove($loop_id);
                                                my $res = $job->status eq 'failed' ? $d->reject($job) : $d->resolve($job);
                                                return $res;
                                            }
                                        });
    return $d;
}

sub retrieve_job {
    my ($self, $controller, $job_id) = @_;
    return $controller->app->_queue->load_job( $job_id );
}

=head2 queue_job

Enqueues a straight L<Qurious> job built from the arguments and returns it.

=cut

sub queue_job {
    my $self = shift;
    my $controller = shift;

    my %args = @_;
    $args{parameters} //= {};
    # Make sure we ALWAYS have the stream2_base_url as a parameter.
    $args{parameters}->{stream2_base_url} =  $controller->url_for('/')->to_abs()->to_string();
    # Indicate to the action that we are being called interactively
    $args{parameters}->{stream2_interactive} = 1;

    my $j = $controller->app->_queue->create_job(@_);

    $j->enqueue();
    return $j;
}

sub wait_jobs {
    my $self = shift;
    my $controller = shift;
    my @wait_jobs = @_;

    my $d = deferred;
    Mojo::IOLoop->timer( 0, sub {

        my @promises = ();
        foreach my $job ( @wait_jobs ){
            my $local_job = $job;
            push @promises, $self->wait_job( $controller, $local_job );
        }

        collect ( 
            @promises 
        )->then( sub {
            my @return = ();
            foreach my $resolve ( @_ ){
                push @return, $resolve->[0];
            }
            $d->resolve(@return); 
        });
    });
    return $d;
}

1;

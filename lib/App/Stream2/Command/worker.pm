package App::Stream2::Command::worker;
use Mojo::Base 'Mojolicious::Command';

use Stream2::ForkManager;
use Stream2::DirWatcher;
use Stream2::Queue::SearchWorker;

use Getopt::Long qw(GetOptionsFromArray :config no_auto_abbrev no_ignore_case);

use Log::Any  qw/$log/;

use Log::Log4perl::MDC;

has description => "Start a Search2 worker\n";
has usage => "usage: $0 worker -q QueueOne -q QueueTwo\n";

sub run {
  my ($self, @args) = @_;

  my $lib_dir = $self->app->home->lib_dir();
  my $top_stream2 = $self->app->stream2();
  my $n_workers = $top_stream2->config()->{n_workers} || 3;
  my $arguments_array = $top_stream2->config()->{workers_arguments} ||
    [
     [ [ qw/Stream2ActionSearch Stream2ActionDownloadCV Stream2ActionDownloadProfile/ ] ],
     [ [ qw/Stream2ActionDownloadCV Stream2ActionDownloadProfile Stream2ActionSearch/ ] ],
     [ [ qw/Stream2ActionDownloadProfile Stream2ActionSearch Stream2ActionDownloadCV/ ] ],
    ];
  # ^^^^^^^^^^^^^^ This is just the default. The real thing should be in your config file.

  Log::Log4perl::MDC->put('sentry_tags', { subsystem => 'worker' });

  my $to_restart =
    sub{
        $log->info("Will use $n_workers workers");
        my $nested_stream2 = $top_stream2->clone();

        ## This is the main worker code
        my $main = sub{
            my ($fork_manager, $queues) = @_;
            $queues || confess("Missing queues argument");

            # This avoids sharing connections handles between processes.
            # Which would be very baaaaad
            my $stream2 = $nested_stream2->clone();

            my $worker;
            eval{
                $log->info("Building Stream2::Queue::SearchWorker");

                $worker = Stream2::Queue::SearchWorker->new(
                                                           debug           => 1,
                                                           log_to_stderr   => 1, # Need to sort that out.
                                                           stream2         => $stream2,
                                                           qurious         => $stream2->qurious(),
                                                           queues          => $queues,
                                                           # max_jobs => 1, # You can try this. It works.
                                                          );


                $log->info("Worker $$ built successfully");
            };
            if ( my $err = $@ ){
                ## The Qurious worker crashed for some reason.
                ## We will log that and refork as usual.
                $log->error("Exception in process $$,building qurious worker, will refork normally. Error was: $err");
                Stream2::ForkManager->refork();
            }


            local $SIG{TERM} = sub{
                $worker->shutdown_pending(1);
            };

            eval {
                $log->info("Starting to work on queues ".join(', ', @$queues));
                # Set the default language to english
                $stream2->translations()->set_language('en');
                $worker->work();
            };
            if( my $err = $@ ){
                ## The Qurious worker crashed for some reason.
                ## We will log that and refork as usual.
                $log->error("Exception in process $$, qurious worker $worker. Will refork normally. Error was: $err");
            }

            unless( $worker->shutdown_pending() ){
                # When work is finished, we should restart. This is because
                # the worker would only work for a fixed number of jobs.
                Stream2::ForkManager->refork();
            }else{
                $log->info("Shutdown of worker child [$$] was asked politely. Not restarting");
            }
        };

        my $periodic_should_finish = 0;
        my $periodic = sub{
            my $stream2 = $nested_stream2->clone();
            while( ! $periodic_should_finish ){
                eval{
                    $stream2->periodic->run({ finish_if => sub{ return $periodic_should_finish ; } });
                };
                if( my $err = $@ ){
                    # The periodic runner has gone very wrong.
                    $log->critical("Periodic worker is gone: $err. Will restart after 10 seconds");
                    sleep(10);
                }
            }
            $log->info("Periodic workers is now finished");
        };

        my $workers_master_pid = fork();
        unless( defined $workers_master_pid ){
            confess("Cannot fork. Lack of resource?");
        }

        unless($workers_master_pid){
            # The workers manager runs in its own child.
            ## This will fire some workers.
            my $workers_manager = Stream2::ForkManager->new({ main => $main,
                                                              start_n => $n_workers,
                                                              arguments_array => $arguments_array,
                                                            });
            local $SIG{TERM} = sub{
                $log->info("Caught SIGTERM in master process. Terminating all children");
                $workers_manager->kill_all(15);
                $workers_manager->wait_children();
                exit(0);
            };

            # Wait for the main worker children.
            $workers_manager->wait_children();
            exit(0);
        }

        # We can receive that from the dirwatcher
        # When that happens, terminate the periodic and the master workers process.
        local $SIG{TERM} = sub{
            $periodic_should_finish = 1;
            kill 15 , $workers_master_pid;
        };

        # Do the periodic stuff in this master process.
        &$periodic();

        # Blocking Wait for the workers master pid.
        waitpid($workers_master_pid, 0 );

        $log->info("Finished waiting for all managed subprocesses");
    };

  if( ( $ENV{MOJO_MODE} || '' ) eq 'development' ){
      my $dir_watcher = Stream2::DirWatcher->new({ restart => $to_restart,
                                                   dir => $lib_dir,
                                                   validate_path => sub{
                                                       my ($path) = @_;
                                                       # Avoid .git and .svn dirs.
                                                       if ( $path =~ m/\.(?:git|svn)/ ) {
                                                           return 0;
                                                       }
                                                       # Anything matching .pm$ is good.
                                                       return ( $path =~ m/\.pm$/ );
                                                   }
                                                 });
      $dir_watcher->watch();
  }else{
      $log->info("Will NOT watch for any change in directory");
      &{$to_restart}();
  }

}

1;

package App::Stream2::Command::cover;

use Mojo::Base 'Mojolicious::Command';
use Mojo::Home;
use Getopt::Long qw(GetOptionsFromArray :config no_auto_abbrev no_ignore_case);

has description => "Run unit tests with Devel::Cover.\n";
has usage       => <<EOF;
usage: $0 cover [OPTIONS] [TESTS]

These options are available:
  -v, --verbose   Print verbose debug information to STDERR.
  -d, --db        Directory in which coverage will be collected (default \$APP_ROOT/cover_db)
EOF

has verbose   => sub { 0 };

has ignore_extensions => sub { return ['.tt.ttc'] };
has ignore_files      => sub { return ['t/', 'blib/lib/'] };

has app_root => sub { return Mojo::Home->new->detect };

has cover_bin => sub { 'cover' };
has cover_db  => sub { shift->app_root->rel_dir('cover_db'); };

has _test_harness => sub {
  my ($self) = @_;
  require TAP::Harness;
  TAP::Harness->new({
    verbosity => $self->verbose || 0,
    color     => 1,
    lib       => [
      $self->app_root->lib_dir
    ],
    switches  => [
      '-MDevel::Cover='.$self->_devel_cover_opts,
    ],
  });
};

has test_files => sub {
  my ($self) = @_;
  my $test_dir = Mojo::Home->new($self->app_root->rel_dir('t'));
  say "Scanning '", $test_dir, "' for tests.";
  return [ map { $test_dir->rel_file($_) } grep { m/\.t$/ } @{$test_dir->list_files} ];
};

sub run {
  my ($self, @args) = @_;

  $self->_initialise(@args);
  $self->_run();
}

sub _initialise {
  my ($self, @args) = @_;

  GetOptionsFromArray( \@args,
    'v|verbose' => sub { $self->verbose(1); },
    'd|db=s'    => sub { $self->cover_db(@_); },
  );

  $self->test_files([@args]) if (@args);
}

sub _run {
  my ($self) = @_;

  $self->_clear_coverage();
  my $tests_passed = $self->_run_tests();
  $self->_build_coverage();

  return $tests_passed;
}

sub _clear_coverage { shift->_run_cover("-delete"); }
sub _build_coverage { shift->_run_cover(); }

sub _run_cover {
  my ($self, @args) = @_;
  system($self->cover_bin, @args, $self->cover_db) == 0;
}

sub _run_tests {
  my ($self) = @_;

  my $aggregator = $self->_test_harness->runtests(sort @{ $self->test_files });
  return !$aggregator->has_errors;
}

sub _devel_cover_opts {
  my $self = shift;
  return join ',', '-dir', $self->app_root->to_string, $self->_devel_cover_ignore;
}

sub _devel_cover_ignore {
  my ($self) = @_;

  my $ignore_extensions = join '|', map { quotemeta($_) } @{ $self->ignore_extensions };
  my $ignore_files      = join '|', map { quotemeta($_) } @{ $self->ignore_files };

  my @ignores;
  push @ignores, "($ignore_extensions)\$" if $ignore_extensions;
  push @ignores, "^($ignore_files)" if $ignore_files;

  return join ',', map { "+ignore,$_" } grep { $_ } @ignores;
}

1;

__END__

=head 1 NAME

App::Stream2::Command::cover - Run tests with coverage

=head1 SYNOPSIS

  use Mojolicious::Command::cover;

  my $cover = Mojolicious::Command::cover->new;
  $test->run(@ARGV);

=head1 DESCRIPTION

L<Mojolicious::Command::cover> runs application tests from the C<t>
directory under L<Devel::Cover>.

=head1 ATTRIBUTES

L<Mojolicious::Command::cover> inherits all attributes from
L<Mojolicious::Command> and implements the following new ones.

=head2 description

  my $description = $cover->description

Provides a short description to the command list.

=head2 usage

  my $usage = $cover->usage;

Provides usage information for this command to the help screen.

=head2 verbose

  my $verbose = $cover->verbose;
  $cover->verbose(1);

Print verbose debugging information to STDERR.

=head2 ignore_extensions

Defaults to C<.tt.ttc> files.

=head2 ignore_files

Defaults to C<t/> and C<blib/lib/>

=head2 app_root

  my $app_root = $cover->app_root;
  $cover->app_root( Mojo::Home->new("/path/to/your/app") );

=head2 cover_bin

Defaults to C<cover>.

=head2 cover_db

Defaults to C<$APP_ROOT/cover_db>.

=head2 test_files

Defaults to all tests under C<$APP_ROOT/t>.

=head1 METHODS

L<Mojolicious::Command::cover> inherits all methods from
L<Mojolicious::Command> and implements the following new ones.

=head2 run

  $cover->run(@ARGV);

Run this command.

=head1 SEE ALSO

L<Mojolicious>, L<Mojolicious::Command::test>, L<Devel::Cover>, L<http://mojolicio.us>

=cut

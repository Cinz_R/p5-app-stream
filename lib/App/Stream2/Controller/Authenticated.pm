package App::Stream2::Controller::Authenticated;

use strict;

use Mojo::Base 'App::Stream2::Controller';

sub requires_auth { 1; }

=head2 handle_unauthorised_user

See L<App::Stream2::Plugin::Auth> in our code.

=cut

sub handle_unauthorised_user { die "Subclass me!"; }

1;

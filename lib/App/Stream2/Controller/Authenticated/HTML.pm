package App::Stream2::Controller::Authenticated::HTML;

use strict;

use Mojo::Base 'App::Stream2::Controller::Authenticated';

use Log::Any qw/$log/;

=head2 handle_unauthorised_user

Redirects an unauthorised_user to the login URL given by
one of the Mojolicious/Plugin/Auth/Provider/*

Additionally, you can give a specific URL to redirect the user
to after he has logged in.

Returns a Mojolicious redirect response.

Usage (in a controller method):

  return $this->handle_unauthorised_user();

  return $this->handle_unauthorised_user($this->url_for('/search'));

=cut

sub handle_unauthorised_user {
    my ( $self , $url ) = @_;

    my $redirect_url = $url // $self->tx->req->url->to_string();

    # Clear any original user
    $self->session( original_user => undef );

    $log->info("Will redirect to '$redirect_url' after login");
    $self->session->{redirect_url} = $redirect_url;

    # The login URL in not authorized user should ONLY
    # attempt logging in adcourier users.
    my $login_url = $self->login_url({ provider => 'adcourier' });
    $log->info( "Redirection unauthorised user to " .$login_url );

    return $self->redirect_to( $login_url );
}

1;

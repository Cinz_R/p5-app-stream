package App::Stream2::Controller::Authenticated::JSON;

use strict;

use Mojo::Base 'App::Stream2::Controller::Authenticated';

sub handle_unauthorised_user {
    my $self = shift;
    $self->stash(status => 403);
    return $self->render(json => {error => 'Not authenticated'});
}

1;

package Test::Mojo::Stream2;

use Carp qw/confess/;
use Crypt::JWT;
use Mojo::Base 'Test::Mojo';
use Mojo::URL;

use Stream2;

use Mojolicious::Sessions::ThreeS;
use Mojolicious::Sessions::ThreeS::Storage::Memory;

use App::Stream2;
=head1 NAME

Test::Mojo::Stream2 - A Stream2 specific subclass of Test::Mojo. Uses a Real App::Stream2

=cut

sub new {
  my ($class, $params) = @_;

  ## For a little while, we are hit by 'activity time out' issues
  ## on jenkins small slaves. See http://mojolicious.org/perldoc/Mojolicious/Guides/FAQ#What-does-Inactivity-timeout-mean
  $ENV{MOJO_INACTIVITY_TIMEOUT} = 60;

  ## Note that anything that uses this test class should be run from
  ## the root of the mojo config, so the config file taken into account
  ## is t/app-stream2.conf
  $ENV{MOJO_HOME} = 't/';

  # default the stream2_class to Stream2, other options could be Test::Stream2,
  # NOTE: ONCE WE FULLY SWITCH OVER TO TEST::STREAM2, WE CAN CHANGE THIS LOGIC.
  $params->{stream2_class} //= 'Stream2';

  # only load Test::Stream2 when needed EG, in tests.
  require Test::Stream2 if $params->{stream2_class} eq 'Test::Stream2';
  my $self = $class->SUPER::new(App::Stream2->new($params));

  # Set the Mojolicous::Sessions::ThreeS to have a memory storage.

  $self->app()->sessions()->storage( Mojolicious::Sessions::ThreeS::Storage::Memory->new() );

  ## Deploy the schema. Will be SQLite according to the test t/app-stream2.conf
  if ( $params->{stream2_class} eq 'Stream2' ) {
      $self->app->stream2->stream_schema->deploy();
  }
  elsif ( $params->{stream2_class} eq 'Test::Stream2' ) {
      $self->app->stream2->deploy_test_db();
  }
  else {
      die 'I dont know how to deploy the database';
  }


  return $self;
}

sub login_ok {
  my $self = shift;

  $self->ua->max_redirects(1);
  $self->post_ok('/login', form => { provider => 'demo', username => $self->username, password => $self->password });

  $self;
}

sub username { 'test_username' }
sub password { 'test_password' }

# returns a hashref of session data that the STOK param in $url refers to
sub get_session_from_STOK {
    my ($self, $url)  = @_;
    my $encoded_token = Mojo::URL->new( $url )->query->param('STOK');
    confess "URL '$url' should contain an 'STOK' param but doesn't"
        unless $encoded_token;
    my $decoded_token = Crypt::JWT::decode_jwt(
                            token => $encoded_token,
                            key   => $self->app->secrets->[0],
                        );
    my ($session) = grep {
                        $_->{"mojox.sessions3s.id"} eq $decoded_token->{sid}
                    } @{ $self->app->sessions->storage->list_sessions };
    return $session;
}

1;

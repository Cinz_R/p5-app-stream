package Test::Stream2;

use Moose;
extends qw/Stream2/;

use Log::Any qw/$log/;
use Test::mysqld;
use Path::Class;

=head1 NAME

Test::Stream2 - A Stream2 class geared towards testing

=cut

has '+config_file' => ( default => 't/app-stream2.conf' );

has 'mysqld' => (
    is         => 'ro',
    isa        => 'Test::mysqld',
    lazy_build => 1,
    predicate  => 'has_mysqld',
);

has 'rebuild_test_db_files' => (
    is      => 'ro',
    isa     => 'Bool',
    default => 0
);

has 'mysqld_data_dir_from' => (
    is         => 'ro',
    isa        => 'Str',
    lazy_build => 1
);

sub _build_mysqld {
    my ($self) = @_;
    my $mysqld_bin =
      Test::mysqld::_find_program( 'mysqld', qw/bin libexec sbin/ );
    $log->info("Building Test::mysqld with '$mysqld_bin'");
    my $mysqld_data_dir       = $self->mysqld_data_dir_from;
    my $rebuild_test_db_files = $self->rebuild_test_db_files;
    return Test::mysqld->new(
        $rebuild_test_db_files ? () : ( copy_data_from => $mysqld_data_dir ),
        my_cnf => {
            'skip-networking' => '',
            socket            => "/tmp/test-$$-mysqld.sock",

            # 'SEE t/test_db.t FOR WHY THIS IS USEFUL'
            $rebuild_test_db_files ? ( datadir => $mysqld_data_dir ) : (),
        }
    );
}

sub _build_mysqld_data_dir_from {
    my ($self) = @_;
    my $shared_directory =
      Path::Class::dir( $self->shared_directory )->subdir('t/mysqld/var');
    die $shared_directory->mkpath unless -e $shared_directory;
    return $shared_directory->stringify;
}

sub BUILD {
    my ( $self, $args ) = @_;
    $log->info("Injecting test configuration in this test config");
    $self->config()->{stream_dsn}      = $self->mysqld->dsn();
    $self->config()->{stream_username} = undef;
    $self->config()->{stream_password} = '';
}

sub deploy_test_db {
    my ( $self, $args ) = @_;
    $self->developer()->run_sqitch( ['deploy'] )
      || die 'could not deploy the database';
    $self->developer()->update_quota_schema(); # externally managed schema
    return 1;
}

sub deploy_test_db_and_verify {
    my ( $self, $args ) = @_;
    $self->deploy_test_db();
    $self->developer()->run_sqitch( ['verify'] )
      || die 'could not verify the database';
    return 1;
}

__PACKAGE__->meta()->make_immutable();
1;

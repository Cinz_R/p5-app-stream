#!/usr/bin/perl

# $Id: Jobsite.pm 72013 2012-03-28 13:58:22Z chrisp $

package Stream::Agents::Jobsite;

use strict;

use HTTP::Response;
use HTTP::Request;
use XML::LibXML;
use MIME::Base64;

use Stream::Templates::Jobsite;

use base qw(Stream::Agents::XML);

sub update_response {
    my ($self, $response) = @_;

    if ( !$response->is_success() ) {
        # HTTP error - don't parse XML
        return $self->SUPER::update_response( $response );
    }

    my $xml = eval {
        $response->decoded_content();
    };

    if ( $@ ) {
        return $self->_throw_error( $response, 'Unable to decode response:' . $@ );
    }

    my $parser = XML::LibXML->new();

    my $doc = eval {
        $parser->parse_string( $xml );
    };

    if ( $@ ) {
        return $self->_throw_error( $response, 'Unable to parse XML:'. $@ );
    }

    my ($message, $message_hash) = eval {
        $self->_parse_jobsite_xml( $doc );
    };

    if ( $@ ) {
        return $self->_throw_error( $response, 'Error extracting the xml from Jobsite: '  . $@ );
    }

    my $local_message_hash = $self->_jobsite_hash_request( $message );

    if ( $message_hash ne $local_message_hash ) {
        return $self->_throw_error(
            $response,
            "Message Hash differ:\n\treceived msg: $message_hash\n\tcalculated msg: $local_message_hash\n",
        );
    }

    # Message hashes match
    my $phoney_response = $self->_build_success_message(
        $response,
        $message,
    );

    return $self->SUPER::update_response(
        $phoney_response,
    );
}

sub _parse_jobsite_xml {
    my $self = shift;
    my $doc = shift;

#    print "\tdoc is:\n";
#    print $doc->toString(1) . "\n\n";

    my $namespace = 'SOAP-ENV';

    my $doc_as_xml = $doc->toString();
    if ( $doc_as_xml =~ m/<soap:/ ) {
        $namespace = 'soap';
    }

    my $encoded_xml = $doc->findvalue('//'.$namespace.':Body/*/*[1]/text()');
    my $message_hash = $doc->findvalue('//'.$namespace.':Body/*/*[2]/text()');
    my $soap_type = $doc->findvalue('//'.$namespace.':Body/*/*[1]/@xsi:type');

#    print "\tmessage hash is: $message_hash\n";
#    print "\tbase64 xml: " . length( $encoded_xml ) . "\n";

    # lame but should work
    my $decoded_xml;
    if ( $soap_type =~ m/xsd:string/ || $encoded_xml =~ m/<xml/ ) {
#        print "\tNot decoding xml\n";
        $decoded_xml = $encoded_xml;
    }
    else {
        $decoded_xml = MIME::Base64::decode_base64( $encoded_xml );
#        print "\tDecoded xml: " . length( $decoded_xml ) . "\n";
    }

    return ( $decoded_xml, $message_hash );
}

sub _jobsite_hash_request {
    my $self = shift;
    my $message = shift;

    return Stream::Templates::Jobsite->jobsite_response_hash(
        $message,
    );
}

sub _build_success_message {
    my $self = shift;
    my $original_response = shift;
    my $new_xml_content = shift;

    my $fake_request = HTTP::Request->new('GET' => 'Decode Jobsite XML');
    my $response     = HTTP::Response->new(
        200 => 'OK', [], $new_xml_content,
    );

    $response->previous( $original_response );
    $response->request( $fake_request );

    return $response;
}

1;

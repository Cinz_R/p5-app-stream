#!/usr/bin/perl

# $Id: REST.pm 63066 2011-11-03 06:37:00Z andy $

package Stream::Agents::REST;

# 2016-06-10 - HTTPS in LWP backend uses IO::Socket::SSL
# by using NET::SSL, forces LWP to use NET:SSL which unless there is a really
# good reason is REALLY BAD thing to do.
# Make sure SSL (https) support is loaded.
# Crypt::SSLeay is a dependency so it should be supplied
# by it.
# use Net::SSL;

use strict;

use base qw(LWP::UserAgent);

# This is unused.
# use Net::HTTPS;

use Log::Any qw/$log/;

# LWP class that simplifies proxy handling
# and remembers the last content

use Scalar::Util;

sub new {
    my $class = shift;
    my %options = @_;

    my $engine = delete $options{engine};
    my $proxy  = delete $options{proxy};
    my $username = delete $options{basic_username};
    my $password = delete $options{basic_password};

    my $self = $class->SUPER::new(
                                  # setting verify_hostname to 0 replicates the historical
                                  # behaviour that Search 1/1.5 is used to, because they use an
                                  # old version of LWP::UserAgent (see https://metacpan.org/pod/LWP::UserAgent , section verify_hostname )
                                  ssl_opts => { verify_hostname => 0 },

                                  %options );

    $self->engine( $engine );
    $self->basic_username( $username );
    $self->basic_password( $password );
    $self->stream_proxy( $proxy );

    return $self;
}

sub engine {
    # gotcha - circular reference!
    return $_[0]->{_cvsearch_engine} if @_ == 1;

    $_[0]->{_cvsearch_engine} = $_[1];
    Scalar::Util::weaken( $_[0]->{_cvsearch_engine} );

    return $_[0]->engine();
}

sub stream_proxy {
    return $_[0]->{_stream_proxy} if @_ == 1;
    return $_[0]->{_stream_proxy} = $_[1];
}

sub basic_username {
    return $_[0]->{_basic_username} if @_ == 1;
    return $_[0]->{_basic_username} = $_[1];
}

sub basic_password {
    return $_[0]->{_basic_password} if @_ == 1;
    return $_[0]->{_basic_password} = $_[1];
}

sub agent_name {
    my $class = shift;

    if ( ref( $class ) ) {
        $class = ref($class);
    }

    $class =~ s/^.+:://g;

    return $class;
}

my $count = 0;
sub request {
    my $self = shift;
    my $request = shift;

    $request->header('ACJ' => ++$count);
    return $self->update_response( $self->SUPER::request( $request, @_ ) );
}

sub prepare_request {
    my ( $self, $request ) = @_;

    my $proxy = $self->stream_proxy();
    if ( $proxy ) {
        my $url = $request->uri();
        my $proxy_url = $proxy->proxy_url( $url );
        if ( grep { $_ eq $url->scheme() } ( 'http' , 'https' ) ) {
            $log->info("Setting proxy = $proxy_url for http AND https protocol");
            $self->proxy(
                ['http' , 'https' ], # Comment was there:  # do not add HTTPS here!!!! see stream/agents/01-https-proxy.t for more info
                $proxy_url,
            );
        }
    }

    return $self->SUPER::prepare_request( $request );
}

sub send_request {
    my ( $self, $request, $arg, $size ) = @_;

    if ( $self->basic_username() ) {
        $request->authorization_basic(
            $self->basic_username(),
            $self->basic_password(),
        );
    }


    ## Don't care about that. We trust the new HTTPS LWP backend (IO::Socket::SSL, instead of Net::SSL)
    ## with the proxy settings set in the 'prepare_request'
    ## See Net::HTTPS
    ##
    # my $proxy = $self->stream_proxy();
    # if ( $proxy ) {
    #     my $url = $request->uri();
    #     if ( $url->scheme() eq 'https' ) {
    #         $log->info("Net::HTTPS will use SSL Implementation '".$Net::HTTPS::SSL_SOCKET_CLASS."'");
    #         # configure Crypt::SSLeay to connect via our proxy
    #         # don't let LWP configure the proxy because it will connect over http instead of https
    #         my $proxy_url = $proxy->proxy_url( $url );
    #         $log->info("Setting proxy = $proxy_url for https HTTPS_PROXY env variable");
    #         local $ENV{HTTPS_PROXY} = $proxy_url;
    #         return $self->SUPER::send_request( $request, $arg, $size );
    #     }
    # }

    # let LWP handle everything as normal
    return $self->SUPER::send_request( $request, $arg, $size );
}

sub update_response {
    my ($self, $response) = @_;

    return $self->set_response( $response );
}

sub set_response {
    my $self = shift;
    my $response = shift;

    $self->{res} = $response;

    return $response;
}

sub response { return $_[0]->{res} };
sub res { return $_[0]->{res} };

# used by subclasses to throw errors back to the engine
sub _throw_error {
    my $self = shift;
    my $original_response = shift;
    my $error_msg = shift;

    my $fake_request = HTTP::Request->new('GET' => 'CvSearch Error');
    my $response     = HTTP::Response->new(
        500 => '(CV SEARCH ERROR)', [], $error_msg,
    );
    $response->previous( $original_response );
    $response->request( $fake_request );

    # don't call update_response in case you get an infinite loop
    return $self->set_response( $response );
}

sub progress {
    my $self = shift;
    my $status = shift;
    my $request_or_response = shift;

    my $engine = $self->engine();
    if ( $engine ) {
        $engine->do_callback('progress', $status);
    }
}

sub DESTROY {
    my $self = shift;
    delete $self->{_cvsearch_engine};
    return;
}

1;

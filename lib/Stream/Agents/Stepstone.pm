package Stream::Agents::Stepstone;

use base 'Stream::Agents::Mechanize';

use Log::Any qw/$log/;

sub send_request {
    my ( $self, $request, $arg, $size ) = @_;

    $request->push_header('Cookie', 'STEPSTONEV5LANG=en;');

    return $self->SUPER::send_request($request, $args, $size);
}

1;

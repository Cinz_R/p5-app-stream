package Stream::Agents::Mechanize;

use strict;

use base qw(WWW::Mechanize);

use Scalar::Util; # for weaken

use Log::Any qw/$log/;

sub new {
    my $class = shift;
    my %options = @_;

    my $engine = delete $options{engine};
    my $proxy  = delete $options{proxy};
    my $username = delete $options{basic_username};
    my $password = delete $options{basic_password};

    # WWW::Mechanize is a subclass of LWP::UserAgent.
    # setting verify_hostname to 0 replicates the historical
    # behaviour that Search 1/1.5 is used to, because they use an
    # old version of LWP::UserAgent (see https://metacpan.org/pod/LWP::UserAgent , section verify_hostname )
    #
    # This is dangerous, but some sites like 'reed' dont behave nicely regarding SSL
    # Try curl -v -X GET https://www.reed.co.uk/recruiter/
    # a few times and you will see what is going on with reed.
    my $self = $class->SUPER::new(onwarn => sub{ my @args = @_ ; $log->warn( @args ); },
                                  ssl_opts => { verify_hostname => 0 },
                                  %options );

    $self->engine( $engine );
    $self->basic_username( $username );
    $self->basic_password( $password );
    $log->debug("Setting proxy = $proxy");
    $self->stream_proxy( $proxy );

    return $self;
}

sub engine {
    # gotcha - circular reference!
    return $_[0]->{_cvsearch_engine} if @_ == 1;

    $_[0]->{_cvsearch_engine} = $_[1];
    Scalar::Util::weaken( $_[0]->{_cvsearch_engine} );

    return $_[0]->engine();
}

sub basic_username {
    return $_[0]->{_basic_username} if @_ == 1;
    return $_[0]->{_basic_username} = $_[1];
}

sub basic_password {
    return $_[0]->{_basic_password} if @_ == 1;
    return $_[0]->{_basic_password} = $_[1];
}

sub stream_proxy {
    return $_[0]->{_stream_proxy} if @_ == 1;
    return $_[0]->{_stream_proxy} = $_[1];
}

sub agent_name {
    my $class = shift;

    if ( ref( $class ) ) {
        $class = ref($class);
    }

    $class =~ s/^.+:://g;

    return $class;
}

sub prepare_request {
    my ( $self, $request ) = @_;

    my $proxy = $self->stream_proxy();
    if ( $proxy ) {
        my $url = $request->uri();
        if ( $url->scheme() eq 'http' ) {
            my $proxy_url = $proxy->proxy_url( $url );
            $self->proxy(
                ['http'], # do not add HTTPS here!!!! see stream/agents/01-https-proxy.t for more info
                $proxy_url,
            );
        }
    }

    return $self->SUPER::prepare_request( $request );
}

sub send_request {
    my ( $self, $request, $arg, $size ) = @_;

    if ( $self->basic_username() ) {
        $request->authorization_basic(
            $self->basic_username(),
            $self->basic_password(),
        );
    }

    my $proxy = $self->stream_proxy();
    if ( $proxy ) {
        my $url = $request->uri();
        if ( $url->scheme() eq 'https' ) {
            # configure Crypt::SSLeay to connect via our proxy
            # don't let LWP configure the proxy because it will connect over http instead of https
            $log->debug("Setting proxy HTTPS_PROXY to ".$proxy->proxy_url());
            local $ENV{HTTPS_PROXY} = $proxy->proxy_url();
            return $self->_postprocess_response(
                $self->SUPER::send_request( $request, $arg, $size ),
            );
        }
    }

    # let LWP handle everything as normal
    return $self->_postprocess_response(
        $self->SUPER::send_request( $request, $arg, $size ),
    );
}

sub _postprocess_response {
    my $self = shift;
    my $response = shift;

    # RegionJobs return an invalid content-disposition
    # like: Content-Disposition: filename=123456.doc
    # instead of: Content-Disposition: attachment; filename=123456.doc
    my $cd = $response->header('Content-Disposition');
    if ( $cd =~ s/^filename=/attachment; filename=/ ) {
        $response->header('Content-Disposition' => $cd );
    }

    return $response;
}

sub progress {
    my $self = shift;
    my $status = shift;
    my $request_or_response = shift;

    my $engine = $self->engine();
    if ( $engine ) {
        $engine->do_callback('progress', $status);
    }
}

sub DESTROY {
    my $self = shift;
    delete $self->{_cvsearch_engine};
    return;
}

1;

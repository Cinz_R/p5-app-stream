#!/usr/bin/perl

# $Id: Jobsite.pm 20174 2009-06-02 11:36:40Z andy $

package Stream::Agents::CareerBuilder;

use strict;
use HTML::Entities;

use base qw(Stream::Agents::XML);

sub agent_name {
    return 'CareerBuilder';
}

sub update_response {
    my ($self, $response) = @_;

    my $xml = $response->content;
    # get rid of the namespace that disrupts libxml findvalue
    $xml =~ s/ xmlns\="http:\/\/ws\.careerbuilder\.com\/resumes\/"//;
    decode_entities($xml);
    $response->content($xml);
    return $self->set_response( $response );
}
1;

package Stream::Agents::TotalJobs;

use strict;
use warnings;

use base qw(Stream::Agents::REST);

=head2 request

Overrides the LWP::UserAgent request to add the authentication header

=cut

sub request {
    my $self = shift;
    my $request = shift;

    $self->add_authentication_header($request);

    $self->SUPER::request($request,@_);
}

=head2 add_authentication_header

Adds the Authorization header required for totaljobs with
the access_token value

=cut

sub add_authentication_header {
    my ($self, $request) = @_;

    my $engine = $self->engine();

    if ( $engine->account_value('access_token') ) {
        my $header = 'bearer ' . $engine->account_value('access_token');

        $request->header(
            'Authorization' => $header,
        );
    }

    return $request;
}

1;

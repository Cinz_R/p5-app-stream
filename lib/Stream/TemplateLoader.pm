package Stream::TemplateLoader;
use warnings;
use strict;

use Carp;
use Class::Load;
use Log::Any qw/$log/;

# All methods are class methods (e.g. Stream::TemplateLoader->list_tokens() ):

sub list_tokens {
    my ($self, $destination) = @_;
    return $self->load_call_and_sort($destination, sub { shift->standard_tokens });
}

sub list_authtokens {
    my ($self, $destination) = @_;
    return $self->load_call_and_sort($destination, sub { shift->authtokens });
}

sub list_authtoken_names {
    my $self = shift;
    my $destination = shift;

    my $class = $self->load( $destination );

    # returns a hash ref contains { token_name => token_definition_ref }
    my $tokens_ref = $class->authtokens();

    return sort keys %$tokens_ref;
}

sub list_global_authtokens {
    my ($self, $destination) = @_;
    return $self->load_call_and_sort($destination, sub { shift->global_authtokens });
}

sub list_optional_tokens {
    my ($self, $destination) = @_;
    return $self->load_call_and_sort($destination, sub { shift->optional_tokens });
}

sub list_mandatory_tokens {
    my ($self, $destination) = @_;
    return $self->load_call_and_sort($destination, sub { shift->mandatory_tokens });
}

sub list_essential_tokens {
    my ($self, $destination) = @_;
    return $self->load_call_and_sort($destination, sub { shift->essential_tokens });
}

sub load_call_and_sort {
    my ($self, $destination, $cb) = @_;

    my $feed_class = $self->load( $destination );
    my $tokens_ref = $cb->($feed_class);
    return $self->sort_tokens( $tokens_ref );
}

sub sort_tokens {
    my $self = shift;
    my $tokens_ref = shift or return ();

    my @sorted_tokens = sort {
        $a->{SortMetric} <=> $b->{SortMetric}
        or   $a->{Label} cmp $b->{Label},
    } values %$tokens_ref;

    return (@sorted_tokens);
}

# Convenience method: loads the feed and returns a new feed object
sub new_request {
    my $self = shift;
    my $destination = shift;

    my $class = $self->load( $destination );

    return $class->new();
}

=head2 load

Loads a template class as either a sub package of Stream::Templates::
or as a full absolute class name.

All templates should be subclasses of L<Stream::Templates::Default>.

Keep in mind this just returns a Class name as a string.

=cut

sub load {
    my $self = shift;
    my $destination = shift;

    my @modules = ($destination, "Stream::Templates::$destination");
    foreach my $module ( @modules ) {
        if ( $module->can('VERSION') && $module->isa('Stream::Templates::Default') ) {
            return $module;
        }
    }

    foreach my $module ( @modules ) {
       if ( $self->_load($module) ) {
           return $module;
       }
    }

    Carp::confess("Could not load any of ".join(' OR ', @modules )." for destination '$destination'");
}

sub _load {
    my ($self, $module) = @_;

    # Trap any errors
    $log->debug("Attempting to load '$module'");
    eval { Class::Load::load_class($module); };
    if ( my $err =  $@ ) {
        if( $module =~ /^Stream::Templates/ ){
            # Its OK to confess here.
            Carp::confess("Could not load '$module':".$err);
        }
        $log->debug("'$module' not there");

        # Perl will not try to compile this file again unless we do this
        $module =~ s{::}{/}g;
        delete $INC{$module.'.pm'};
        return;
    }

    $log->debug("'$module' loaded");
    return $module;
}

1;

#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: Constants.pm 30466 2010-02-23 15:16:49Z andy $

package Stream::Constants;

use strict;

use Stream::Constants::Currencies  ':all';
use Stream::Constants::SalaryPer   ':all';
use Stream::Constants::SortMetrics ':all';
use Stream::Constants::Control     ':all';
use Stream::Constants::Distances   ':all';

use base qw(Exporter);

our @EXPORT_OK = (
    @Stream::Constants::Currencies::EXPORT_OK,
    @Stream::Constants::SalaryPer::EXPORT_OK,
    @Stream::Constants::SortMetrics::EXPORT_OK,
    @Stream::Constants::Control::EXPORT_OK,
    @Stream::Constants::Distances::EXPORT_OK,
);

our %EXPORT_TAGS = (
    'all'         => \@EXPORT_OK,
    'currencies'  => \@Stream::Constants::Currencies::EXPORT_OK,
    'salary_per'  => \@Stream::Constants::SalaryPer::EXPORT_OK,
    'sortmetrics' => \@Stream::Constants::SortMetrics::EXPORT_OK,
    'control'     => \@Stream::Constants::Control::EXPORT_OK,
    'distances'   => \@Stream::Constants::Distances::EXPORT_OK,
);

1;

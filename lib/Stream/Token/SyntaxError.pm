#!/usr/bin/perl

# $Id: SyntaxError.pm 19727 2009-05-21 11:27:49Z andy $

# package that deliberately does not compile to see how the test suite handles it
package Stream::Token::SyntaxError;

use strict;

use base qw(Stream::Token);

sub type { return 'syntax_error' }

deliberate error;

1;

# vim: expandtab shiftwidth=4

package Stream::Token::List;

use strict;

use base qw(Stream::Token);

use Scalar::Util qw(looks_like_number);

#  list/multilist/check/radio: options, min select, max select
sub initialise {
    my $self = shift;

    $self->{options} ||= [];

    my $options_ref = shift;

    if ( defined( my $options = delete $options_ref->{options} ) ) {
        $self->options( $options );
    }

    return $self->SUPER::initialise( $options_ref );
}

sub type { return 'list' }

sub is_valid {
    my $self = shift;
    my $value = shift || $self->value();

    no warnings "numeric";

    foreach my $option ( $self->option_values() ) {
        if ( $option eq $value && $option == $value ) {
            return 1;
        }
    }

    return 0;
}

sub default {
    my $self = shift;
    if ( @_ ) {
        return $self->SUPER::default(@_);
    }

    my $default = $self->SUPER::default();
    if ( defined( $default ) ) {
        return $default;
    }

    if ( $self->options() == 1 ) {
        my ($only_option) = $self->option_values();
        return $only_option;
    }

    return;
}

sub options {
    return @{ $_[0]->{options} } if @_ == 1;

    my $self = shift;
    my $options_ref = shift;

    if ( ref( $options_ref ) ) {
        $self->{options} = [
            parse_option_ref( $options_ref )
        ];
    }
    else {
        $self->{options} = [
            parse_option_string( $options_ref )
        ];
    }

    return if !defined wantarray;

    return @{ $self->{options} };
}

sub option_labels {
    return map { $_->[0] } @{ $_[0]->{options} };
}

sub option_values {
    return map { $_->[1] } @{ $_[0]->{options} };
}

sub parse_option_ref {
    return map {
        ref($_) ? $_ : parse_option($_)
    } @{ $_[0] };
}

sub parse_option_string {
    my $options_string = shift;

    $options_string =~ s/^\(//;
    $options_string =~ s/\)$//;

    return map { parse_option($_) } split /\|/, $options_string;
}

sub parse_option {
    my $separator = '=';
    if ( $_[0] =~ m/!!/ ) {
        $separator = '!!';
    }

    my @options = (split /$separator/, $_[0], 2);

    push @options, $_[0] unless @options == 2;

    return \@options;
}

sub first_less_than {
    my $self  = shift;
    my $value = shift;

    if ( !defined( $value ) ) {
        return;
    }

    my $last_option_ref;

    OPTION:
    foreach my $option_ref ( sort { $a->[0] <=> $b->[0] } $self->options() ) {
        if ( looks_like_number( $option_ref->[0] ) ) {
#            print STDERR "Checking $option_ref->[0] vs $value\n";
            if ( $option_ref->[0] > $value ) {
                last OPTION;
            }
            else {
                $last_option_ref = $option_ref;
            }
        }
#        else {
#            print STDERR "Skipping $option_ref->[0]\n";
#        }
    }

    if ( $last_option_ref ) {
#        print STDERR "Returning $last_option_ref->[1] (selected $last_option_ref->[0])\n";
        return $last_option_ref->[1];
    }

    return;
}

sub first_more_than {
    my $self  = shift;
    my $value = shift;

    if ( !defined( $value ) ) {
        return;
    }

    foreach my $option_ref ( sort { $a->[0] <=> $b->[0] } $self->options() ) {
        if ( looks_like_number( $option_ref->[0] ) ) {
#            print STDERR "Checking label $option_ref->[0] vs $value (id=$option_ref->[1])\n";
            if ( $option_ref->[0] >= $value ) {
#                print STDERR "Returning $option_ref->[1] (selected $option_ref->[0])\n";
                return $option_ref->[1];
            }
        }
        # handle the 'any greater than \d\d' options
        elsif ( $option_ref->[0] =~ m/\+$/ ) {
            my $option_val = $option_ref->[0];
            $option_val =~ s/\+$//;

            if ( $option_val <= $value ) { # is our value greather than the option
                return $option_ref->[1];
            }
        }
    }

    return;
}

sub first_between {
    my $self  = shift;
    my $value = shift;

    if ( !defined( $value ) || $value eq ''  ) {
        return $self->default();
    }

    my $any_option;

    OPTION:
    foreach my $option_ref ( sort { $a->[0] <=> $b->[0] } $self->options() ) {
        my $min_value = 0;
        my $max_value = 0;
        if ( looks_like_number( $option_ref->[0] ) ) {
            $max_value = $option_ref->[0];
        }
        elsif ( $option_ref->[0] =~ m/^\s*(\d+)\s*-\s*(\d+)\s*$/ ) {
                                # match   10000   -   20000
            $min_value = $1;
            $max_value = $2;
        }
        elsif ( $option_ref->[0] =~ m/^\s*(\d+)\s*\+\s*$/ ) {
                                 # match 10000     +
            $min_value = $1;

            if ( $value > $min_value ) {
                return $option_ref->[1];
            }
            next OPTION;
        }
        elsif ( $option_ref->[0] =~ m/^any(?: salary)$/i) {
            $any_option = $option_ref->[1];
            next OPTION;
        }
        else {
#            print STDERR "Skipping $option_ref->[0] / $option_ref->[1]\n";
            next OPTION;
        }

        if ( $value <= $max_value && $value >= $min_value ) {
            return $option_ref->[1];
        }
    }

    return $any_option;
}


1;
# vim: expandtab shiftwidth=4

package Stream::Token::Salary;

use strict;

use base qw(Stream::Token);

__PACKAGE__->mk_accessors( qw(currency minimum maximum per) );

#  text: value, size, minlength?, maxlength?
my %SALARY_PER_CONVERSION = (
    'ANNUM_TO_YEAR' => 1,
    'YEAR_TO_YEAR'  => 1,
    'MONTH_TO_YEAR' => 12,
    'WEEK_TO_YEAR'  => 48,
    'DAY_TO_YEAR'   => 240,
    'HOUR_TO_YEAR'  => 1920,
);

sub type { return 'salary'; }

sub initialise {
    my $self        = shift;
    my $options_ref = shift;

    if ( $options_ref ) {
        if ( exists( $options_ref->{currency} ) ) {
            $self->currency( delete $options_ref->{currency} );
        }
        if ( exists( $options_ref->{minimum} ) ) {
            $self->minimum( delete $options_ref->{minimum} );
        }
        if ( exists( $options_ref->{maximum} ) ) {
            $self->maximum( delete $options_ref->{maximum} );
        }
        if ( exists( $options_ref->{per} ) ) {
            $self->per( delete $options_ref->{per} );
        }
    }

    return $self->SUPER::initialise( $options_ref );
}

sub set {
    my $self  = shift;
    my $input = shift;

    my $name = $self->name;

    $self->currency( $input->param( "${name}_cur" ) );
    $self->minimum(  $input->param( "${name}_min" ) );
    $self->maximum(  $input->param( "${name}_max" ) );
    $self->per(      $input->param( "${name}_per" ) );

    return;
}

# $self->minimum_per( 'annum' )
sub minimum_per {
    my $self = shift;
    my $per  = uc(shift);

    return _value_per( $self->minimum(), $self->per(), $per );
}

sub maximum_per {
    my $self = shift;
    my $per  = uc(shift);

    return _value_per( $self->maximum(), $self->per(), $per );
}

sub _value_per {
    my $salary = shift;
    my $per    = uc(shift);
    my $new_per = uc(shift);

    $salary =~ s/,//g;

    if ( $per eq $new_per ) {
        return $salary;
    }

    # convert salary to per year
    my $salary_per_annum = _convert_to_annum( $salary, $per );

    # convert salary to per $new_per
    return _convert_per_annum_to( $salary_per_annum , $new_per );
}

=head2 convert_to_annum

Converts the given salary/per to its annum value. Pure function.

Usage:

   my $amount_per_annum = Stream::Token::Salary::convert_to_annum( $amount , $period );

=cut

sub convert_to_annum{
    goto &_convert_to_annum;
}

sub _convert_to_annum {
    my $salary = shift;
    my $per    = shift;

    my $conversion = $SALARY_PER_CONVERSION{ uc($per) . '_TO_YEAR' } || 1;

    return $salary * $conversion;
}

sub _convert_per_annum_to {
    my $salary  = shift;
    my $new_per = shift;

    my $conversion = $SALARY_PER_CONVERSION{ $new_per . '_TO_YEAR' } || 1;

    my $new_salary = $salary / $conversion;

    if ( $new_per eq 'DAY' ) {
        return sprintf("%.2f", $new_salary);
    }

    return sprintf("%d", $new_salary);
}

# $self->minimum_in( 'GBP' )
sub minimum_in {
    my $self = shift;
    my $convert_to = shift;

    return _value_in( $self->minimum(), $self->currency(), $convert_to );
}

# $self->maximum_in( 'GBP' )
sub maximum_in {
    my $self = shift;
    my $convert_to = shift;

    return _value_in( $self->maximum(), $self->currency(), $convert_to );
}

sub _value_in {
    my $salary      = shift;
    my $currency    = uc(shift);
    my $currency_to = uc(shift);

    if ( $currency eq $currency_to ) {
        return $salary;
    }

    # salary conversion time - leave for now
    die 'I am lazy and have not finished this bit yet';
}

1;

# vim: expandtab shiftwidth=4

#!/usr/bin/perl

# $Id: Text.pm 19727 2009-05-21 11:27:49Z andy $

package Stream::Token::Text;

use strict;

use base qw(Stream::Token);

__PACKAGE__->mk_accessors(qw(size minlength maxlength));

#  text: value, size, minlength?, maxlength?
sub type { return 'text'; }

1;

# vim: expandtab shiftwidth=4

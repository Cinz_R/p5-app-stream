#!/usr/bin/perl

# $Id: SalaryMultiValue.pm 35204 2010-06-01 08:51:37Z andy $

package Stream::Token::SalaryMultiValue;

use strict;

use Scalar::Util qw(looks_like_number);

use base qw(Stream::Token::MultiList);

my $DEBUG = 0;

sub select_salaries_between {
    my $self = shift;
    my $min  = shift;
    my $max  = shift;

    if ( $min && $max ) {
        print "Selecting salaries between $min - $max\n" if $DEBUG;
        return $self->salaries_between( $min, $max );
    }

    if ( $min ) {
        return $self->salaries_greater_than( $min );
    }

    if ( $max ) {
        return $self->salaries_less_than( $max );
    }

    #return;

    return $self->salary_any();
}

sub salary_any {
    my $self = shift;

    OPTION:
    foreach my $option_ref ( $self->options() ) {
        my $option_label = $option_ref->[0];
        my $option_value = $option_ref->[1];

        if ( $option_label =~ m/^any(?: salary)?$/i ) {
            return $option_value;
        }
    }

    return;
}

sub salaries_greater_than {
    my $self = shift;
    my $min  = shift;

    my @good_options;

    OPTION:
    foreach my $option_ref ( $self->options() ) {
        my $option_label = $option_ref->[0];
        my $option_value = $option_ref->[1];

        my ($option_min,$option_max) = $self->extract_salary_option( $option_label );

        print "# comparing ($option_min-$option_max) against ($min) [$option_value]\n" if $DEBUG;

        my $falls_in_band = 0;
        if ( defined( $option_min ) && defined( $option_max ) ) {
            # handles XXXX to YYYY (min must be bigger than XXXX for a match)
            if ( $option_min >= $min || $option_max >= $min ) {
                $falls_in_band = 1;
            }
        }
        elsif ( defined( $option_max ) ) {
            # handles <XXXX
            if ( $option_max >= $min ) {
                $falls_in_band = 1;
            }
        }
        elsif ( defined( $option_min ) ) {
            # handles YYYY+ (ie. more than YYYY)
            if ( $option_min >= $min ) {
                $falls_in_band = 1;
            }
        }

        # this option is a keeper
        if ( $falls_in_band ) {
            print "# matched ($option_min-$option_max) against ($min)\n" if $DEBUG;
            push @good_options, $option_value;
        }
    }

    return @good_options;
}

sub salaries_less_than {
    my $self = shift;
    my $max  = shift;

    my @good_options;

    OPTION:
    foreach my $option_ref ( $self->options() ) {
        my $option_label = $option_ref->[0];
        my $option_value = $option_ref->[1];

        my ($option_min,$option_max) = $self->extract_salary_option( $option_label );

        print "# comparing ($option_min-$option_max) against ($max) [$option_value]\n" if $DEBUG;

        my $falls_in_band = 0;
        if ( defined( $option_min ) && defined( $option_max ) ) {
            # handles XXXX to YYYY
            if ( $option_max <= $max || $option_min <= $max ) {
                $falls_in_band = 1;
            }
        }
        elsif ( defined( $option_min ) ) {
            # handles XXXX+
            if ( $option_min < $max ) {
                $falls_in_band = 1;
            }
        }
        elsif ( defined( $option_max ) ) {
            # handle <XXXX
            if ( $option_max < $max ) {
                $falls_in_band = 1;
            }
        }

        # this option is a keeper
        if ( $falls_in_band ) {
            print "# matched ($option_min-$option_max) against ($max)\n" if $DEBUG;
            push @good_options, $option_value;
        }
    }

    return @good_options;
}

sub salaries_between {
    my $self = shift;
    my $min  = shift;
    my $max  = shift;

    my @good_options;

    OPTION:
    foreach my $option_ref ( $self->options() ) {
        my $option_label = $option_ref->[0];
        my $option_value = $option_ref->[1];

        my ($option_min,$option_max) = $self->extract_salary_option( $option_label );

        if ( !$option_min && !$option_max ) {
            next OPTION;
        }

        print "# comparing ($option_min-$option_max) against ($min,$max) [$option_value]\n" if $DEBUG;

        my $falls_in_band = 0;
        if ( defined( $option_min ) && defined( $option_max ) ) {
            # option_label is a range (e.g. 10000-20000)
            # e.g. option_min is O(min), option_max is O(max)
            if ( $option_min >= $min && $option_min <= $max ) {
                # the lower part of this option falls between the salary range
                # MIN ...... $option_min ...... MAX
                $falls_in_band = 1;
            }
            elsif ( $option_max >= $min && $option_max <= $max ) {
                # the higher part of this option falls between the salary range
                # MIN ...... $option_max ...... MAX
                $falls_in_band = 1;
            }
            elsif ( $option_min <= $min && $option_max >= $max ) {
                # This option contains the supplied salary range
                # $option_min .... MIN .... MAX .... $option_max
                $falls_in_band = 1;
            }
        }
        elsif ( defined( $option_min ) ) {
            # either $min or $max must be greater than $option_min
            if ( $min >= $option_min || $max > $option_min ) {
                $falls_in_band = 1;
            }
        }
        elsif ( defined( $option_max ) ) {
            # either $min or $max must be less than $option_max
            if ( $max < $option_max ) {
                $falls_in_band = 1;
            }
        }

        if ( $falls_in_band ) {
            # this option is a keeper
            print "# matched ($option_min-$option_max) against ($min,$max)\n" if $DEBUG;
            push @good_options, $option_value;
        }
    }

    return @good_options;
}

sub extract_salary_option {
    my $self = shift;
    my $label = shift;

    if ( looks_like_number( $label ) ) {
        # single value
        return (undef,$label);
    }

    if ( $label =~ m/^\s*(\d+)\s*-\s*(\d+)\s*$/ ) {
               # match   10000   -   20000
        return ( $1, $2 );
    }

    if ( $label =~ m/^\s*(\d+)\s*\+\s*$/ ) {
               # match   1000     +
        return ( $1, undef );
    }

    if ( $label =~ m/^\s*<\s*(\d+)\s*$/ ) {
               # match   <   1000
        return ( undef, $1 );
    }

    return (undef, undef);
}

1;

# vim: expandtab shiftwidth=4

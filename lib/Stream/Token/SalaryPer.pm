package Stream::Token::SalaryPer;

use strict;

use base qw(Stream::Token::List);

use POSIX qw(ceil);

my %SALARY_PER_CONVERSION = (
    'YEAR_TO_YEAR'  => 1,
    'MONTH_TO_YEAR' => 12,
    'WEEK_TO_YEAR'  => 48,
    'DAY_TO_YEAR'   => 240,
    'HOUR_TO_YEAR'  => 1920,
);

sub type { return 'salary_per'; }

# inherited from Stream::Token::List
# sub is_valid {
#     my $self = shift;
# 
#     return 0;
# }

sub convert {
    my $self  = shift;
    my $salary = shift or return;

    my $current_per = uc($self->value());
    my $default_per = uc($self->default());

    $salary =~ s/,//g;
    $salary =~ s/k\s*$/000/;

    if ( $self->is_valid( $current_per ) ) {
        return $self->format_salary( $current_per, $salary );
    }

    if ( $current_per eq $default_per ) {
        return $self->format_salary( $current_per, $salary );
    }

    # convert to per annum first
    my $yearly_salary = $self->per_annum(
        $current_per, $salary,
    );

    # convert to default second
    if ( $default_per eq 'ANNUM' ) {
        return $self->round( $default_per, $yearly_salary );
    }

    my $converted_salary = $self->per_annum_to(
        $default_per => $yearly_salary,
    );

    return $self->round( $default_per => $converted_salary );
}

sub round {
    my $self   = shift;
    my $per    = uc(shift);
    my $salary = shift;

    if ( $per eq 'DAY' || $per eq 'HOUR'  ) {
        # round to 2 dp
        return $self->format_salary( $per => $salary );
    }

    if ( $per eq 'WEEK' ) {
        # round to nearest pound
        return sprintf("%d", $salary );
    }

    # round to nearest 5
    return sprintf("%d", POSIX::ceil($salary / 5)) * 5;
}

sub format_salary {
    my $self = shift;
    my $per = uc(shift);
    my $salary = shift;

    if ( $per eq 'HOUR' ) {
        # per hour is always 2dp
        return sprintf '%.2f', $salary;
    }

    if ( int( $salary ) == $salary ) {
        return $salary;
    }

    return sprintf '%.2f', $salary;
}

sub per_annum {
    my $self   = shift;
    my $per    = shift;
    my $salary = shift;

    my $conversion = $SALARY_PER_CONVERSION{ $per . '_TO_YEAR' } || 1;

    return $salary * $conversion;
}

sub per_annum_to {
    my $self    = shift;
    my $new_per = shift;
    my $salary  = shift;

    my $conversion = $SALARY_PER_CONVERSION{ $new_per . '_TO_YEAR' } || 1;

    my $new_salary = $salary / $conversion;

    return $new_salary;
}

1;

# vim: expandtab shiftwidth=4

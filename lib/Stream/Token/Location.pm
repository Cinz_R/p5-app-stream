#!/usr/bin/perl

# $Id: Location.pm 19727 2009-05-21 11:27:49Z andy $

package Stream::Token::Location;

use strict;

use base qw(Stream::Token);

__PACKAGE__->mk_accessors(qw(location_id freetext_location radius radius_unit));

#  text: value, size, minlength?, maxlength?
sub type { return 'location'; }

1;

# vim: expandtab shiftwidth=4

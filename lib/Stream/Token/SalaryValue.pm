#!/usr/bin/perl

# $Id: SalaryValue.pm 21828 2009-07-17 08:15:45Z andy $

package Stream::Token::SalaryValue;

use strict;

use base qw(Stream::Token::List);

1;

sub first_less_than {
    my $self  = shift;
    my $value = shift;

    my $option = $self->SUPER::first_less_than( $value );

    if ( defined($option) ) {
        return $option;
    }

    return $self->salary_any();
}

sub first_more_than {
    my $self  = shift;
    my $value = shift;

    my $option = $self->SUPER::first_more_than( $value );

    if ( defined($option) ) {
        return $option;
    }

    return $self->salary_any();
}


sub salary_any {
    my $self = shift;

    OPTION:
    foreach my $option_ref ( $self->options() ) {
        if ( $option_ref->[0] =~ m/Any|All/i ) {
            return $option_ref->[1];
        }
    }

    return;
}

# vim: expandtab shiftwidth=4

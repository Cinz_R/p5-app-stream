#!/usr/bin/perl

# $Id: SalaryValue.pm 19727 2009-05-21 11:27:49Z andy $

package Stream::Token::DistanceList;

use strict;

use base qw(Stream::Token::List);

my $KM_PER_MILE;
BEGIN {
    $KM_PER_MILE = 1.609344;
};

sub type { return 'distancelist'; }

sub unit {
    my $self = shift;
    if ( @_ ) {
        my $new_unit = lc(shift) || 'miles';
        if ( $new_unit !~ m/^(?:miles|km)/ ) {
            die "unsupported distance unit: $new_unit\n";
        }
        $self->{unit} = lc( $new_unit ) || 'miles';
    }

    return lc($self->{unit}) || 'miles';
}

sub first_greater_than {
    my $self = shift;
    my $value = shift; # should be in miles ( if valueunit not specified )
    my $valueunit = shift || 'miles'; # added to allow values to be specified in km

    if ( !defined( $value ) ) {
        return ($self->option_values())[0]; # return the lowest option
    }
    my $unit = $self->unit();
    if ( $unit eq 'km' && $valueunit ne 'km' ) {
        $value = $self->_miles_to_km( $value );
    }

    my $largest_option;
    foreach my $option ( $self->option_values() ) {
        if ( defined($option) && $option =~ m/(\d+(?:\.\d+)?)/ ) {
            my $option_miles = $1;
            if ( $option_miles >= $value ) {
                return $option;
            }
            $largest_option = $option;
        }
    }

    return $largest_option;
}

sub _miles_to_km {
    my $self = shift;
    my $miles = shift
        or return 0;

    return sprintf( '%.2f', ($miles * $KM_PER_MILE ) );
}

sub _km_to_miles {
    my $self = shift;
    my $km = shift
        or return 0;

    return sprintf( '%.2f', ($km / $KM_PER_MILE ) );
}

1;

# vim: expandtab shiftwidth=4

#!/usr/bin/perl

# $Id: Hidden.pm 19727 2009-05-21 11:27:49Z andy $

package Stream::Token::Hidden;

use strict;
use base qw(Stream::Token);

#  hidden: value
sub type { return 'hidden'; }

1;
# vim: expandtab shiftwidth=4

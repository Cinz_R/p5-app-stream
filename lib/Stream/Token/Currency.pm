package Stream::Token::Currency;

use strict;

use base qw(Stream::Token::List);

use Carp;
use Log::Any qw/$log/;

use Stream2;

sub type { return 'currency'; }

=head2 convert

Converts the given salary (as an integer) from
the current  value of this token (so a currency code like 'EUR')
to the 'default' value of this token (a currency code too, like 'GBP');

=cut

sub convert {
    my $self = shift;
    my $salary = shift or return;

    my $value = $self->value;
    my $default = $self->default;

    my $current_cur = uc(ref($value) ? $value->[0] : $value);
    my $default_cur = uc(ref($default) ? $default->[0] : $default);

    $salary =~ s/,//g;
    $salary =~ s/k\s*/000/g;

    if ( $self->is_valid( $current_cur ) ) {
        return $salary;
    }

    if ( $current_cur eq $default_cur ) {
        return $salary;
    }

    my $exchange_rate = $self->exchange_rate( $current_cur => $default_cur );

    if ( !$exchange_rate ) {
        confess "No exchange rate between $current_cur => $default_cur\n";
    }

    $salary = $salary * $exchange_rate;

    return $self->format_salary( $default_cur, $salary );
}

sub format_salary {
    my $self = shift;
    my $default_cur = shift;
    my $salary = shift;

    if ( $salary > 1000 ) {
        return sprintf("%d", $salary);
    }

    return sprintf("%.2f", $salary);
}

=head2 exchange_rate

Shortcut method to get the exchange rate from a currency to another.

Usage:

  $this->exchange_rate('GBP' , 'EUR' );

=cut

sub exchange_rate {
    my $self     = shift;
    my $orig_cur = shift;
    my $new_cur  = shift;

    unless( $orig_cur && $new_cur ){
        confess("Missing orig_cur or new_cur");
    }

    $log->info("Getting ExChange Rate from $orig_cur TO $new_cur");

    my $s2 = Stream2->instance();

    my $cache_key = __PACKAGE__.'_Exchange_'.$orig_cur.'_'.$new_cur;

    return $s2->stream2_cache->compute($cache_key,
                                       '60 minutes',
                                       sub{
                                           return $s2->currency_api->get_rate($orig_cur , $new_cur );
                                       });
}

1;

# vim: expandtab shiftwidth=4

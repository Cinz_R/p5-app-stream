#!/usr/bin/perl

# $Id: MultiList.pm 24153 2009-09-17 11:15:20Z andy $

package Stream::Token::MultiList;

use strict;

use base qw(Stream::Token::List);

use Scalar::Util qw(looks_like_number);

#  list/multilist/check/radio: options, min select, max select

__PACKAGE__->mk_accessors(qw(min_select max_select));

sub initialise {
    my $self = shift;

    $self->{values} = {};

    my $options_ref = shift;

    if ( $options_ref && defined( my $values = delete $options_ref->{values} )) {
        $self->values( $values );
    }

    return $self->SUPER::initialise( $options_ref );
}

sub type { return 'multilist' }

sub values {
    return keys %{ $_[0]->{values} } if @_ == 1;

    my $self = shift;

    if ( ref($_[0]) ) {
        $self->{values} = _array_to_hashref( $_[0] );
    }
    elsif ( @_ > 1 ) {
        $self->{values} = _array_to_hashref( @_ );
    }
    else {
        $self->{values} = _array_to_hashref( split /\|/, $_[0] );
    }

    return if !defined wantarray;

    return keys %{ $self->{values} };
}

sub selected {
    my $self = shift;

    my $values_ref = $self->{'values'};

    return @_ > 1 ? map { exists( $values_ref->{$_} ) } @_
              : exists( $values_ref->{$_[0]} );
}

sub _array_to_hashref {
    my @array = ref($_[0]) eq 'ARRAY' ? @{ $_[0] } : @_;
    return { map { $_ => 1 } @array };
}

sub numeric_options {
    my $self = shift;

    my @numeric_options = grep {
        looks_like_number( $_->[0] )
    } $self->options();

    return _scalar_or_array( @numeric_options );
}

sub sorted_numeric_options {
    my $self = shift;

    return _sort_options($self->numeric_options());
}

=head2 select_all_between( $min, $max )

Returns all numeric options that are between $min and $max

=cut

sub select_all_between {
    my $self = shift;
    my $min  = shift;
    my $max  = shift;

    my @matches;

    OPTION:
    foreach my $option_ref ( $self->sorted_numeric_options() ) {
        if ( $max && $option_ref->[0] > $max ) {
            last OPTION;
        }

        if ( $option_ref->[0] >= $min ) {
            push @matches, $option_ref->[1];
        }
    }

    return @matches;
}

# Gets ugly about now :)
#

=head2 select_banded_between( $min, $max )

Returns all options that surround $min and $max

e.g. options: 10,20,30,40

select_all_between(15,25) would return 20 because only 20 falls between 15 and 25.
select_banded_between(15,25) returns 10,20,30 because that is the smallest range
than includes 15 and 25

=cut

sub select_banded_between {
    my $self = shift;
    my $min  = shift;
    my $max  = shift;

    my @options = $self->sorted_numeric_options();

    return $self->banded_between(
        $min,
        $max,
        @options,
    );
}

sub banded_between {
    my $self = shift;
    my $min  = shift;
    my $max  = shift;

    my @options = @_;

    # find all options greater than min
    my @options_greater_than_min = $self->banded_options_greater_than(
        $min,
        @options,
    );

    if ( !$max ) {
        return _option_values( @options_greater_than_min );
    }

    # remove all options that are greater than max
    my @matches = $self->banded_options_less_than(
        $max,
        @options_greater_than_min,
    );

    return _option_values( @matches );
}

sub select_banded_greater_than {
    my $self = shift;
    my $min  = shift;

    return _option_values(
        $self->select_banded_options_greater_than( $min ),
    );
}

sub select_banded_options_greater_than {
    my $self = shift;
    my $min  = shift;

    my @options = $self->sorted_numeric_options();

    return $self->banded_options_greater_than(
        $min,
        @options,
    );
}

sub banded_options_greater_than {
    my $self = shift;
    my $min  = shift;
    my @options = _sort_options( @_ );

    my @matches;
    my $perfect_match = 0;
    my $first_band_below_min;
    foreach my $option_ref ( @options ) {
        my $option_label = $option_ref->[0];
        if ( $option_label == $min ) {
            $perfect_match = 1;
            push @matches, $option_ref;
        }
        elsif ( $option_label >= $min ) {
            push @matches, $option_ref;
        }
        else {
            $first_band_below_min = $option_ref;
        }
    }

    if ( !$perfect_match && $first_band_below_min ) {
        unshift @matches, $first_band_below_min;
    }

    return @matches;
}

sub select_banded_less_than {
    my $self = shift;
    my $max  = shift;

    return _option_values(
        $self->select_banded_options_less_than( $max ),
    );
}

sub select_banded_options_less_than {
    my $self = shift;
    my $max  = shift;

    my @options = $self->sorted_numeric_options();

    return $self->banded_options_less_than(
        $max,
        @options,
    );
}

sub banded_options_less_than {
    my $self = shift;
    my $max  = shift;
    my @options = _sort_options( @_ );

    my @matches;
    my $perfect_match = 0;
    my $first_band_above_max;

    OPTION:
    foreach my $option_ref ( @options ) {
        my $option_label = $option_ref->[0];
        if ( $option_label == $max ) {
            $perfect_match = 1;
            push @matches, $option_ref;
        }
        elsif ( $option_label <= $max ) {
            push @matches, $option_ref;
        }
        else {
            $first_band_above_max = $option_ref;
            last OPTION;
        }
    }

    if ( !$perfect_match && $first_band_above_max ) {
        push @matches, $first_band_above_max;
    }

    return @matches;
}


# helpers
sub _sort_options {
    return sort { $a->[0] <=> $b->[0] } @_;
}

sub _option_values {
    return _scalar_or_array( map { $_->[1] } @_ );
}

sub _scalar_or_array {
    my @values = @_;

    if ( @values == 1 ) {
        return shift @values;
    }

    return @values;
}

1;

# vim: expandtab shiftwidth=4

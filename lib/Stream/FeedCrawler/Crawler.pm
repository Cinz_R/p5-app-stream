package Stream::FeedCrawler::Crawler;

=head1 NAME

Stream::FeedCrawler::Crawler - A remote site verifyer and scraper.
Makes a series of requets to a remote site, scraping and verifying the
content. The resulting data is stored as JSON for use by feeds.

verify_remote should be extended to include all your requests and scraping.
Use $self->agent for the requests and where possible use validate_xpath_value() and validate_xpath_nodes()
to scrape and verify data. Data stored in the hashref $self->crawl_output can be stored when your
crawling is complete using $self->store_output. Previous entires will be overwritten.

The board accesses the data via


See Stream::Templates::CrawledFeed

=cut

use Moose;

use Stream::FeedCrawler::Types;
use Stream::FeedCrawler::Config;
use Log::Any qw/$log/;
use WWW::Mechanize;
use Path::Tiny;
use HTML::TreeBuilder::XPath;

has base_url => (
    is => 'ro',
    isa => 'URL',
    coerce => 1,
    required => 1,
);

has board => (
    is       => 'rw',
    isa      => 'Str',
    required => 1
);

has board_group => (
    is  => 'ro',
    isa => 'Str',
    default => ''
);

has agent => (
    is      => 'ro',
    isa     => 'WWW::Mechanize',
    lazy    => 1,
    default => sub {
        my $self = shift;
        my $agent = WWW::Mechanize->new();
        $self->record_agent($agent);
        return $agent;
    }
);

has config => (
    is => 'rw',
    isa => 'Stream::FeedCrawler::Config',
    lazy => 1,
    default => sub {
        my $self = shift;
        Stream::FeedCrawler::Config->new(
            stream2     => $self->stream2,
            board       => $self->board,
            board_group => $self->board_group,
            base_url    => $self->base_url
        );
    }
);

has xpc => (
    is => 'rw',
    isa => 'HTML::TreeBuilder::XPath',
    default => sub {
        HTML::TreeBuilder::XPath->new();
    }
);

has stream2 => (
    is => 'rw',
    isa => 'Stream2',
    required => 1
);
=head2 record_agent($agent)

Add event handlers to $agent that will log its activity.

=cut

sub record_agent {
    my $self = shift;
    my $agent = shift;

    $agent->add_handler(
        request_prepare => sub {
            my($request, $agent, $h) = @_;
            $log->infof('(REQUEST)  %s %s', $request->method, $request->uri->as_string);
            #$log->info($request->as_string);
        }
    );
    $agent->add_handler(
        response_done => sub {
            my($response, $agent, $h) = @_;
            $log->infof('(RESPONSE) %s %s', $response->request->method, $response->request->uri->as_string);
            #$log->info($response->headers->as_string);
        }

    );
    return $agent;
}

=head2 validate_xpath_nodes(%options)

Tests an XPath and returns the resulting node(s)

Options:
    label         => 'Description'
    contenxt_node => $xpath context
    path          => $xpath_string
Optional options:
    expected_count => int
    min_count      => int
    max_count      => int
    severity       => fatal, warn, optional

=cut

sub validate_xpath_nodes {
    my $self = shift;
    my %options = @_;
    my $label = $options{label}
      or die( 'No label given' );

    my ( $context_node, $path, $severity ) = map {
        $options{$_} or die sprintf('%s is requried for %s', $_, $label);
    } qw/context_node path severity/;

    my @results = $context_node->findnodes($path);
    my $node_count = scalar(@results);

    $log->infof('%d nodes found for %s.', $node_count, $options{label});

    if(exists $options{expected_count} && $node_count != $options{expected_count}) {
        $log->warnf('Should be exactly %d nodes.', $options{expected_count});
        die if $options{severity} eq 'fatal';

    } elsif (exists $options{min_count} && $node_count < $options{min_count}) {
        $log->warnf('Should be min %d nodes.', $options{min_count});
        die if $options{severity} eq 'fatal';

    } elsif (exists $options{max_count} && $node_count > $options{max_count}) {
        $log->warnf('Should be max %d nodes.', $options{max_count});
        die if $options{severity} eq 'fatal';
    }

    if(exists $options{expected_count} and $options{expected_count} == 1 || scalar(@results) == 1) {
        return $results[0];
    }
    return @results;
}

=head2 validate_xpath_value(%options)

Tests an XPath and returns the resulting value

Options:
    context_node => $xpath context,
    path          => $xpath_string,
Optional options:
    like     => regex test
    severity => fatal, warn, optional

=cut

sub validate_xpath_value {
    my $self = shift;
    my %options = @_;

    my $label = $options{label}
      or die( 'No label given' );

    my ( $context_node, $path, $severity ) = map {
        $options{$_} or die sprintf('%s is requried for %s', $_, $label);
    } qw/context_node path severity/;

    my $value = $context_node->findvalue($path);

    if(!$value) {
        $log->errorf('No value found for %s. XPath: %s', $label, $path);
        die if $severity eq 'fatal';
    }

    if($options{like} && $value !~ $options{like}) {
        $log->errorf('For %s, %s did not match %s', $label, $value, $options{like});
        die if $severity eq 'fatal';
    }

    return $value;
}

=head2 verify_remote()

Verifies content and behaviour of the remote site.

=cut

sub verify_remote {
    #Extend me.
    ...
}

__PACKAGE__->meta->make_immutable();

1;

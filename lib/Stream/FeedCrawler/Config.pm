package Stream::FeedCrawler::Config;

=head1 Stream::FeedCrawler::Config

Handles configuration information for FeedCrawler::Crawler and Stream::Templates modules. Values are retrieved using the get functions.

=cut

use Moose;

use Stream2;
use Log::Any qw/$log/;

use Path::Tiny;
use XML::LibXML;


has stream2 => (
    isa => 'Stream2',
    is => 'rw',
    lazy => 1,
    default => sub {
        $log->debug('Grabbing new S2 instance');
        return Stream2->instance();
    }
);

has base_url => (
    isa => 'URI',
    is => 'rw',
    lazy => 1,
    default => sub {
        my $self = shift;
        return URI->new($self->_get_value('path', 'base_url'));
    }
);

has board => (
    isa => 'Str',
    is => 'rw',
    required => 1,
);

has board_group => (
    isa => 'Str',
    is => 'rw',
);

has sources => (
    isa => 'HashRef[HashRef]',
    is => 'rw',
    lazy => 1,
    builder => '_build_sources'
);

# Temporary per-crawl data to be stored at the end.
has _current_stash => (
    isa => 'HashRef',
    is => 'rw',
    default => sub { {} }
);

has _data_dir => (
    isa => 'Path::Tiny',
    is => 'ro',
    lazy => 1,
    default => sub {
        my $self = shift;
        return Path::Tiny->new(
            $self->stream2->shared_directory
        )->child('feedcrawler');
    }
);

has output_file => (
    isa => 'Path::Tiny',
    is => 'rw',
    default => sub {
        my $self = shift;
        return $self->_data_dir->child('crawled')->child($self->board . '.json');
    },
    lazy => 1
);

has precedence => (
    isa => 'ArrayRef[Str]',
    is => 'ro',
    default => sub {
        [qw(
            current_stash
            board_crawled
            board_group_crawled
            board_base
            board_group_base
        )];
    }
);

sub _build_sources {
    my $self = shift;
    return {
        current_stash => $self->_current_stash,
        board_base          => $self->_get_config('base', $self->board),
        board_group_base    => $self->_get_config('base', $self->board_group),
        board_crawled       => $self->_get_config('crawled', $self->board),
        board_group_crawled => $self->_get_config('crawled', $self->board_group)
    };
}

sub _get_config {
    my ($self, $type, $board) = @_;
    $log->debugf('Loading config type: %s, board: %s', $type, $board);
    if(!$board) {
        return {};
    }
    my $file = $self->_data_dir->child($type)->child($board . '.json');
    return {} unless $file->is_file;
    my $config;
    eval {
        $config = $self->stream2->json->decode($file->slurp);
    };
    die sprintf ('Could not load config %s-%s, %s', $type, $board, $@) if $@;
    return $config || {};
}

=head2 get_tokens()

Returns a hash of tokens refs ready for insertion into a feed template's %standard_tokens.

=cut

sub get_tokens {
    my $self = shift;
    return map {
        %{$self->sources->{$_}->{token_ref} || {}};
    } @{$self->precedence};
}


=head2 get_url($key)

Returns a URI object specified by $key. The URI points to the stored path and
is absolute. Use $uri->path for a relative path.

=cut

sub get_url {
    my ($self, $key) = @_;
    my $path = $self->_get_value('path', $key);
    return URI->new_abs($path, $self->base_url);
}

=head2 get_xpath($key)

Returns the XPath string specified by $key.

=cut

sub get_xpath {
    my ($self, $key) = @_;
    return $self->_get_value('xpath', $key);
}


=head2 get_regex($key)

Returns a compiled Regexp as specified by $key.
=cut

sub get_regex {
    my ($self, $key) = @_;
    my $regex = $self->_get_value('regex', $key);
    return qr/$regex/;
}


=head2 get_token_ref($key)

Returns the token hash specified by $key.

=cut

sub get_token_ref {
    my ($self, $key) = @_;
    return $self->_get_value('token', $key);
}


=head2 get_value($key)

Returns value specified by $key.

=cut

sub get_value {
    my ($self, $key) = @_;
    return $self->_get_value('general', $key);
}


=head2 set_uri($key, $uri)

Sets a $uri that is absolute or relative to the stored base_url.

=cut

sub set_uri {
    my ($self, $key, $path) = @_;
    my $uri = URI->new_abs($path, $self->base_url);
    $self->_set_crawl_value('path', $key, $uri->as_string)
}


=head2 set_xpath($key, $xpath)

Validates and stores the XPath string in our stash.

=cut

sub set_xpath {
    my ($self, $key, $xpath) = @_;
    #Validate XPath
    eval {
        XML::LibXML::XPathExpression->new($xpath);
    };
    die $@ if $@;

    $self->_set_crawl_value('xpath', $key, $xpath);
}


=head2 set_regex($key, $regex)

Validates and stores the xpath in our stash.

=cut

sub set_regex {
    my ($self, $key, $regex) = @_;
    eval {
        qr/$regex/;
    };
    die $@ if $@;

    $self->_set_crawl_value('regex', $key, $regex);
}


=head2 set_token_ref($key, $token_ref)

Stores the xpath in our stash. $token_ref must be a facet-like ref.
E.g.
Not


=cut

# TODO: validate the token ref as well
sub set_token_ref {
    my ($self, $key, $token_ref) = @_;
    $self->_set_crawl_value('token_ref', $key, $token_ref);
}


=head2 set_value($key, $value)

Stores $value in our stash.

=cut

sub set_value {
    my ($self, $key, $value) = @_;
    $self->_set_crawl_value('general', $key, $value);
}

# Retrieve the value from our hash sources. They may contain duplicates.
sub _get_value {
    my ($self, $type, $key, $optional) = @_;

    $log->debugf('Getting value. Type: %s, Key: %s', $type, $key);

    foreach my $source (@{$self->precedence}) {
        return $self->sources->{$source}->{$type}->{$key} if defined $self->sources->{$source}->{$type}->{$key};
    }

    die sprintf ('Could not find value. Key: %s, Type: %s', $key, $type);
}

# Store a crawl value in our stash for JSON stringification later by store_crawl
sub _set_crawl_value {
    my ($self, $type, $key, $value) = @_;
    $log->debugf('Setting value. Type: %s, Key: %s, Value: %s', $type, $key, $value);
    $self->_current_stash->{$type}->{$key} = $value;
}

=head2 get_xpath($key)

Returns the XPath specified by $key.

=cut

sub store_crawl {
    my $self = shift;

    $self->output_file->spew($self->stream2->json->encode($self->_current_stash))
      or die 'Could not store crawl data: ' . $@;
    $log->infof('Current Stash stored as %s', $self->output_file->stringify);
}

__PACKAGE__->meta->make_immutable();

1;

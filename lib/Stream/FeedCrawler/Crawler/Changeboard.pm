package Stream::FeedCrawler::Crawler::Changeboard;
use Moose;

extends qw(Stream::FeedCrawler::Crawler);

use Log::Any qw/$log/;
use JSON;
use URI;

has '+board' => (
    default => 'changeboard',
);

has credentials => (
    isa => 'HashRef',
    is  => 'ro'
);

sub verify_remote {
    my $self = shift;

    my $agent = $self->agent;

    $log->infof('Verifying: %s', $self->board);

    #Verify login url
    $agent->get($self->base_url);
    my $login_page = $self->xpc->parse_content($agent->content);
    my $login_href = $self->validate_xpath_value(
        label        => 'Login href',
        context_node => $login_page,
        path         => './/a[@id="login-top-menu"]/@href',
        severity     => 'fatal',
    );
    $self->config->set_uri('login_form' => $login_href);

    # Test login request with redirect
    my %credentials = %{$self->credentials};

    my $login_url = $self->config->get_url('login_form');

    $agent->get($login_url);
    $agent->form_with_fields( qw(Email Password) );
    $agent->set_fields(
        Email    => $credentials{username},
        Password => $credentials{password},
    );
    $agent->submit();

    # We should have a link to switch to the company account
    $agent->follow_link(
        url_regex => $self->config->get_regex('account_switch_url')
    ) or die 'Could not find account list link';

    my $xpc = $self->xpc->parse_content($agent->content);
    # Check link to search form
    my $search_form_anchor = $self->validate_xpath_nodes(
        label          => 'Search Form Anchor',
        context_node   => $xpc,
        path           => '//a[text() = "Search CV database"]',
        expected_count => 1,
        severity       => 'fatal'
    );

    $self->config->set_uri('search_form' => $search_form_anchor->attr('href'));
    $agent->get( $self->config->get_url('search_form'));

    $self->verify_search_page($agent->content);

    $agent->form_with_fields(qw(SearchKeywords SectorId))
      or die;

    # Make a broad search for lots of results
    $agent->set_fields(SearchKeywords => 'a b c d');
    $agent->submit();

    $self->verify_search_results($agent->content);

    $self->config->store_crawl;

    print 'Changeboard Verification Complete';

    print "\n";
}

sub verify_search_page {
    my ($self, $page_content) = @_;

    my $xpc = $self->xpc->parse_content($page_content);
    my $search_form = $self->validate_xpath_nodes(
        label          => 'Search Form',
        path           => '//form[contains(@class, "new_broadcast_cv_profile")]',
        context_node   => $xpc,
        expected_count => 1,
        severity       => 'fatal'
    );
    $self->verify_search_form($search_form);
}

sub verify_search_form {
    my ($self, $search_form) = @_;
    my $agent = $self->agent;

    $self->config->set_uri('search_form_action' => $search_form->attr('action'));

    # Check search form fields.

    $self->validate_xpath_nodes(
        label          => 'Keywords input',
        context_node   => $search_form,
        path           => './/input[@name="SearchKeywords"]',
        expected_count => 1,
        severity       => 'fatal'
    );

    # Search panel is a subsection of the search form.
    my $search_panel = $self->validate_xpath_nodes(
        label          => 'Search Panel',
        context_node   => $search_form,
        path           => './div[@class="panel"][1]',
        expected_count => 1,
        severity       => 'fatal'
    );

    $self->verify_search_panel($search_panel);
}

sub verify_search_panel {
    my ($self, $search_panel) = @_;

    $log->info('Verifying search panel');

    $self->verify_sectors($search_panel);
    $self->verify_locations($search_panel);

    my @job_hours = $self->checkbox_from_name($search_panel, 'JobHoursIds');

    $self->config->set_token_ref(
        'hours' => {
            Label => 'Hours',
            Type => 'MultiList',
            Options => [
                map {
                    [
                        $_->{label},
                        $_->{value}
                    ];
                } @job_hours
            ]
        }
    );

    my @contracts = $self->checkbox_from_name($search_panel, 'JobContractIds');

    my %contract_label_map = (
        Permanent => 'permanent',
        Contract  => 'contract',
        Interim   => 'temporary'
    );

    my %contract_value_map = map {
        my $contract_type = $contract_label_map{$_->{label}}
          or die 'Unknown contract label ' . $_->{label};
        $contract_type => $_->{value};
    } @contracts;

    $self->config->set_value(contract_map => \%contract_value_map);

    my @cv_ages = $self->validate_xpath_nodes(
        label        => 'CV Ages',
        context_node => $search_panel,
        path         => './/select[@id="CVAge"]/input/value',
        severity     => 'fatal'
    );

    @cv_ages = grep{$_} map {$_->getValue} @cv_ages;

    $self->config->set_value(cv_ages => \@cv_ages);
}

# Given an XPath context and an  input/@name value,
# we return a hashref with the label value of the input.
sub checkbox_from_name {
    my ($self, $xpc, $input_name) = @_;
    return map {
        {
            value => $_->findvalue('./input/@value'),
            label => $_->findvalue('./label/text()')
        };
    } $xpc->findnodes('.//li[input[@name = "' . $input_name . '"]]');

}

# We have to get several AJAX requests
# to get the board's sectors
sub verify_sectors {
    my ($self, $search_form) = @_;

    $log->info('Verifying sectors.');

    my @parent_sector_nodes = $self->validate_xpath_nodes(
        label          => 'Parent Sectors',
        context_node   => $search_form,
        path           => './/select[@id="SectorId"]/option',
        expected_count => 4,
        severity       => 'warn',
    );

    # Output token hash
    my %sectors = (
        Type    => 'groupedMultiList',
        Label   => 'Sector',
        Options => []
    );

    my $agent = $self->agent->clone();
    $self->record_agent($agent);

    foreach my $parent_sector_node (@parent_sector_nodes) {
        my $parent_sector_id = $parent_sector_node->attr('value')
          or next; #skip "Please choose..."

        $agent->get( '/jobscms/getspecialisations/' . $parent_sector_id );

        my $child_sectors = $self->stream2->json->decode($agent->content);

        # Token ref-esque options.
        my @sector_options = map { [ $_->{Name}, $_->{Id} ] } @$child_sectors;

        push (
            @{$sectors{Options}},
            {
                Label   => $parent_sector_node->as_text(),
                Options => \@sector_options,
            }
        );
    }

    $self->config->set_token_ref('sectors' => \%sectors);
}

# We have to recursively make several AJAX requests
# to get the board's locations
sub verify_locations {
    my ($self, $search_form) = @_;

    my $agent = $self->agent->clone();
    $self->record_agent($agent);
    my $found_locations = 0;
    my @region_nodes = $search_form->findnodes('.//select[@id="RegionId"]/option');

    my $regions = [];
    foreach my $region_node (@region_nodes) {
        my $region_label = $region_node->as_text;
        my $region_id = $region_node->attr('value');
        next unless $region_id;

        # imitate ajax request for children
        $log->debugf('Finding areas under %s', $region_label);
        $agent->get( '/jobscms/GetJobLocationsForFrontendSearch/?id=' . $region_id);
        my $region = {
            label => $region_label,
            id    => $region_id
        };
        push @$regions, $region;
        my $areas = $self->stream2->json->decode($agent->content);
        foreach my $area (@$areas) {
            $log->debugf("\tFinding locations under %s", $area->{Name});
            $agent->get( '/jobscms/GetJobLocationsForFrontendSearch/?id=' . $area->{Id});
            my $locations = $self->stream2->json->decode($agent->content);
            $area->{children} = $locations;
            push @{$region->{children}}, $area;
        }
    }

    $self->config->set_value('unmapped_locations', $regions);
}

sub verify_search_results {
    my ($self, $search_results_page) = @_;

    my $page = $self->xpc->parse_content($search_results_page);

    # Check candidate nodes
    my @candidates = $self->validate_xpath_nodes(
        label        => 'Candidates list item',
        context_node => $page,
        path         => $self->config->get_xpath('candidate_result'),
        min_count    => 10, # Should be on a page with max results
        severity     => 'fatal',
    );

    # TODO: Check that we can find the number of results

    $self->verify_candidate( $candidates[0] );
}

sub verify_candidate {
    my ($self, $candidate) = @_;

    # Check the candidate result body
    $self->validate_xpath_nodes(
        label => 'Candidate location',
        context_node => $candidate,
        path => $self->config->get_xpath('candidate_location'),
        expected_count => 1,
        severity => 'fatal',
    );

    $self->validate_xpath_nodes(
        label          => 'Candidate summary',
        context_node   => $candidate,
        path           => $self->config->get_xpath('candidate_summary'),
        expected_count => 1,
        severity       => 'fatal',
    );

    # These are similarly structured fields
    my $additional_field_container = $self->validate_xpath_nodes(
        label          => 'Additional field container',
        context_node   => $candidate,
        path           => $self->config->get_xpath('candidate_additional_field_container'),
        expected_count => 1,
        severity       => 'fatal',
    );

    my @additional_field_labels = grep {
        $_ !~ m/Download/;
    } map {
        $_->getValue;
    } $self->validate_xpath_nodes(
        label        => 'Candidate additional field labels',
        context_node => $additional_field_container,
        path         => $self->config->get_xpath('candidate_additional_field_label'),
        severity     => 'warn',
    );

    my $additional_field_format = $self->config->get_value('candidate_additional_field_content_format');
    foreach my $additional_field_label (@additional_field_labels) {
        # Make sure the field's content's where we want it.
        my $field_content_xpath = sprintf(
            $additional_field_format,
            $additional_field_label
        );
        $self->validate_xpath_nodes(
            label          => 'Candidate additional field content',
            context_node   => $additional_field_container,
            path           => $field_content_xpath,
            expected_count => 1,
            severity       => 'fatal'
        );
    }

    my $cv_link = $self->validate_xpath_value(
        label        => 'Candidate CV link',
        context_node => $candidate,
        path         => $self->config->get_xpath('candidate_cv_link'),
        severity     => 'fatal',
    );

    $self->config->set_value( candidate_additional_field_labels => \@additional_field_labels);

    if($cv_link =~ $self->config->get_regex('candidate_cv_link_id')) {
        $log->debug('Cound candidate ID');
    } else {
        die('Could not find candidate ID');
    }

    my $candidate_id = $1;

    my $agent = $self->agent->clone;
    $self->record_agent($agent);
    $agent->get($cv_link);

    my $purchase_page = $self->xpc->parse_content($agent->content);

    my $purchase_cv_regex = $self->config->get_regex('candidate_cv_purchase_link');
    my $purchase_anchor = $self->validate_xpath_value(
        label        => 'CV Purchase Anchor',
        context_node => $purchase_page,
        path         => $self->config->get_xpath('cv_purchase_href'),
        like         => qr/\A$purchase_cv_regex\z/,
        severity     => 'fatal'
    );

}

__PACKAGE__->meta->make_immutable();

1;

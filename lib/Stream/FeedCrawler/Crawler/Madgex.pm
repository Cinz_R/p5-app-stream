package Stream::FeedCrawler::Crawler::Madgex;

use Moose;
extends qw(Stream::FeedCrawler::Crawler);

use Stream::FeedCrawler::Types;

use File::Slurp;
use HTML::TreeBuilder::XPath;
use List::Util;
use URI;

use Log::Any qw/$log/;

has credentials => (
    is => 'rw',
    isa => 'HashRef',
    lazy => 1,
    default => sub { {} }
);

has '+board_group' => (
    default => 'madgex'
);

has candidate_id_regex => (
    is => 'ro',
    default => sub {qr/[A-z0-9]{8}-(?:[A-z0-9]{4}-){3}[A-z0-9]{12}/},
);


sub login  {
    my $self = shift;
    my $agent = $self->agent;

    $agent->get($self->config->get_url('login_form'));

    $agent->form_with_fields(keys %{$self->credentials});
    $agent->set_fields( %{$self->credentials});
    $agent->submit();

    my $xpc = $self->xpc->parse_content($agent->response->decoded_content);

    foreach my $error (@{$self->config->get_value('login_errors')}) {
        if($agent->content =~ $error->{regex}) {
            die($error->{message});
        }
    }
}

sub scrape_login_form {
    my $self = shift;
    my $agent = $self->agent;

    # == Login form check ==
    $agent->get($self->config->get_url('login_form'));
    my $xpc = $self->xpc->parse_content($agent->response->decoded_content);

    my $login_form = $self->validate_xpath_nodes(
        label          => 'Login Form',
        path           => '//form[@action="/login/"]',
        context_node   => $xpc,
        expected_count => 1,
        severity       => 'fatal'
    );

    $self->validate_xpath_nodes(
        label          => 'Login username input',
        path           => './/label[@for="inpUsername"]',
        context_node   => $login_form,
        expected_count => 1,
        severity       => 'fatal'
    );

    $self->validate_xpath_nodes(
        label          => 'Login password input',
        path           => './/label[@for="inpPassword"]',
        context_node   => $login_form,
        expected_count => 1,
        severity       => 'fatal'
    );
}

sub scrape_search_form {
    my $self = shift;
    my $agent = $self->agent();

    $agent->get($self->config->get_url('search_form'));
    if($agent->success) {
        $log->info('Navigated to search form');
    }

    my $xpc = $self->xpc->parse_content( $agent->response->decoded_content );


    #Required fields
    #   keywords
    #   updated within
    #   current location
    #   location radius

    my $search_form = $self->validate_xpath_nodes(
        label => 'Search form',
        context_node => $xpc,
        #Ends with new-search
        path => '//form/@action[substring(., string-length(.) - 10) = "#new-search"]/..', #Ends with "#new-search".
        expected_count => 1,
        severity => 'fatal'
    );

    $search_form->attr('action') =~ m/(.+)#new-search/;
    $self->config->set_uri('search_form' => $1);

    #Check for special fields
    my $keyword_input = $self->validate_xpath_nodes(
        label          => 'Keyword input',
        context_node   => $search_form,
        path           => './/input[@id="keywords"]',
        expected_count => 1,
        severity => 'fatal',
    );

    my $updated_within_input = $self->validate_xpath_nodes(
        label => 'Updated within input',
        context_node => $search_form,
        path => './/select[@id="MinLastUpdated"]',
        expected_count => 1,
        severity => 'fatal',
    );

    #Location inputs for current and preferred location are standardised.
    my $location_input_id = $self->validate_xpath_value(
        label => 'Location input id',
        context_node => $search_form,
        path => './/label[contains(text(), "Current")]/following-sibling::div/input/@data-prefix',
        like => qr/^\d+\|$/,
        severity => 'warn',
    );

    if($location_input_id) {
        $location_input_id =~ s/\|$//;
        $self->config->set_value(location_input_id => $location_input_id);

        #Locate matching radius ID
        my $radius_input_id = $self->validate_xpath_value(
            label => 'Radius input',
            context_node => $search_form,
            path => sprintf('.//select[@id="RadialLocation_%s"]/option/@value', $location_input_id),
            expected_count => 1,
            severity => 'fatal' #Since the board is using a location
        );
    } else {
        $self->config->set_value(location_input_id => '');
    }

    my @multi_lists = $search_form->findnodes('.//div[@class="js-count-checked"]');
    $log->infof('Found %s multi lists', scalar(@multi_lists));

    for(my $i = scalar(@multi_lists) -1; $i > -1; $i--){
        my $multi_list = $multi_lists[$i];

        $log->info('Scraping mutli-list');

        $multi_list = $self->scrape_multi_list($multi_list);

        $self->config->set_token_ref(
            $multi_list->{key} => {
                Label  => $multi_list->{label},
                Values => $multi_list->{options},
                Type   => $multi_list->{type}
            }
        );
    }

    #Scrape candidate list results
    my $candidate_list = $self->validate_xpath_nodes (
        label => 'Candidate list container',
        path => '//ul[@class="lister cf block"]',
        context_node => $xpc,
        expected_count => 1,
        severity => 'fatal',
    );

    my @candidates = $self->validate_xpath_nodes(
        label => 'Candidates (in <ul>)',
        path => './li[contains(@class, "card")]',
        context_node => $candidate_list,
        expected_count => 10, #Per page. blank criteria search should have enough results to fill a page
        severity => 'optional',
    );

    if(!@candidates) {
        $log->info('Trying alternative candiate XPath');
        @candidates = $self->validate_xpath_nodes(
            label => 'Candidates alternative (out of <ul>)',
            context_node => $xpc,
            path => './/li',
            expected_count => 10,
            severity => 'fatal'
        );
    }

    my $last_profile_path = '';
    my $profile_path = '';
    foreach my $candidate (@candidates) {
        #Want href, but check for anchor. A change in anchor results is worth attention.
        my $profile_anchor = $self->validate_xpath_nodes(
            label => 'Candidate profile url',
            context_node => $candidate,
            path => './/a',
            expected_count => 1,
            severity => 'fatal',
            details => 'Relying on a single anchor in the per-candidate container to link to profile.'
        );

        my $profile_href = $profile_anchor->attr('href');
        die('No profile url.') unless $profile_href;
        my $id_regex = $self->candidate_id_regex;
        $profile_href =~ m/(\/[\w-]+\/)$id_regex\/.+/;
        $profile_path = $1;
        $log->infof('Profile path: [%s]', $profile_path);

        die(sprintf('No profile url in expeted format, "%s"', $profile_href)) unless $profile_path;
        die(sprintf('Differing profile paths found "%s" and "%s"', $profile_path && $last_profile_path))
          if $last_profile_path && ($last_profile_path ne $profile_path);
    }
    $log->infof('Determined profile_path "%s"', $profile_path);
    $self->config->set_uri(profile_path => $profile_path);

    #@candidates should be set.
    #Verify using the first candidate.
    my $candidate = $candidates[0];

    $log->debug('Navigating to first candidate page');
    $agent->get($candidate->findvalue('.//a/@href'));
    $self->scrape_candidate_profile($agent->response->decoded_content);
}

#Scrape multi list containing element
sub scrape_multi_list {
    my $self = shift;
    my $list_element = shift;

    my $title = $self->validate_xpath_nodes(
        label          => 'Multi list title',
        path           => './h4[@class="category-header"]',
        context_node   => $list_element,
        expected_count => 1,
        severity       => 'fatal'
    );

    my $input_value = $self->validate_xpath_value(
        label => 'Multi list input element',
        path => './/input/@value',
        context_node => $list_element,
        severity => 'fatal'
    );

    $list_element->findvalue('.//input/@value') =~ m/^(\d+)\|\d/;
    my $key = $1;

    my @option_elements = $list_element->findnodes('./fieldset/div/div[@class="checkbox"]');
    my @flat_options;
    my @nested_options;

    foreach my $option_element (@option_elements) {
        my $option = $self->scrape_multi_list_option($option_element);
        if(my @child_option_elements = $option_element->findnodes('./div/div[@class="checkbox"]')) {
            my @child_options = map { $self->scrape_multi_list_option($_) } @child_option_elements;
            push(
                @nested_options,
                {
                    Label => $option->[0],
                    Options => \@child_options
                }
            );
        } else {
            push @flat_options, $option;
        }
    }

    # If we're at all nested, flat options go into a special group with label ''
    my @output_options = ();
    my $type = 'MultiList';
    if(@nested_options) {
        $log->infof('Found a nested list: %s', $title->as_text);
        @output_options = @nested_options;
        $type = 'grouped' . $type;

        if(@flat_options) {
            $log->warn('Nested list with flat options');
            push(
                @output_options,
                {
                    Label => '',
                    Options => \@flat_options
                }
            );
        } else{
            $log->warn('Nested list, no flat options');
        }
    } else {
        $log->debug('Flat list');
        @output_options = @flat_options;
    }


    return {
        label   => $title->as_text,
        key     => $key,
        options => \@output_options,
        type => $type
    };
}

sub scrape_multi_list_option {
    my $self = shift;
    my $option_element = shift;
    my %output = ();


    my $input = $option_element->findnodes('./input')->[0];
    if($input) {
        $output{value} = $input->attr('value');
    } else {
        $log->error('<input/> not found');
        $log->info($option_element->as_HTML);
        return;
    }

    my $label = $option_element->findnodes('./label')->[0];
    if($label) {
        $output{label} = $label->as_text;
    } else {
        $log->error('<label/> not found');
        return;
    }

    return [$output{label}, $output{value}];
 }

sub scrape_candidate_profile {
    my $self = shift;
    my $decoded_content = shift;

    if(!$decoded_content) {
        die('Give me decoded_content');
    }
    my $xpc = $self->xpc->parse_content($decoded_content);

    my @paragraph_titles = $self->validate_xpath_nodes(
        label => 'Profile Paragraph Titles',
        context_node => $xpc,
        path => './/h3[following-sibling::p]/text()',
        severity => 'optional',
        min_count => 0
    );

    my $candidate_id = $self->validate_xpath_value(
        label => 'Candidate ID',
        context_node => $xpc,
        path => '//h1/@id',
        severity => 'fatal',
        like => sprintf('^%s$',$self->candidate_id_regex)
    );

    my @action_buttons = $self->validate_xpath_nodes(
        label => 'Profile Action Buttons.',
        context_node => $xpc,
        path => './/li[@class="cv-actions__item"]/a',
        min_count => 2,
        max_count => 4,
        severity => 'fatal'
    );

    #Either we get the download or unlock button
    #Use the one we get to determine the other
    my $action_button_count = scalar(@action_buttons);
    my $cv_download_prefix;
    if($action_button_count == 4) {
        my $download_button = $action_buttons[0];
        my $email_button = $action_buttons[1];
        if($download_button->as_text !~ m/download/i) {
            die(sprintf('Expect first of two action buttons to contain \'download\', instead got "%s"', $download_button->as_text));
        }
        if($email_button->as_text !~ m/email/i) {
            die(sprintf('Expect second of two action buttons to contain \'email\', instead got "%s"', $email_button->as_text));
        }

        my $download_link = URI->new_abs($download_button->attr('href'), $self->base_url);
        my @download_link_segments = grep {$_} $download_link->path_segments;
        #shoudl be ['', download_path, id]

        unless (scalar(@download_link_segments) == 2 and $download_link_segments[1] eq $candidate_id) {
            $log->errorf('Bad Profile href format, %s - %s', scalar(@download_link_segments), $download_link->as_string);
            die();
        }

        $download_link_segments[0] =~ m/^(\w+)-download/;
        $cv_download_prefix = $1;
    } elsif($action_button_count == 2) {
        my $unlock_button = $action_buttons[0];
        if($unlock_button->as_text !~ m/unlock/i) {
            die(sprintf('Expect single action button to contain \'Unlock\', instead got "%s"', $unlock_button->as_text));
        }

        my @unlock_href_segments =  grep {$_} URI->new_abs($unlock_button->attr('href'), $self->base_url)->path_segments;
        $unlock_href_segments[0] =~ '^(\w+)-unlock';
        $cv_download_prefix = $1;
    } else {
        die(sprintf('Should be 2 or 4 action buttons. Found %s', $action_button_count));
    }

    $self->config->set_uri(cv_download => $cv_download_prefix . '-download');
    $self->config->set_uri(cv_unlock => $cv_download_prefix . '-unlock');
}

sub scrape_homepage {
    my $self = shift;
    my $agent = $self->agent;

    $log->info('Veryfing homepage');

    $agent->get( $self->base_url );

    my $xpc = $self->xpc->parse_content( $agent->response->decoded_content );

    #Login button & area
    my $login_anchor;
    my $login_button = $self->validate_xpath_nodes(
        label => 'Login button',
        context_node => $xpc,
        path => '//a[@href="/login/"]',
        expected_count => 2,
        severity => 'warn'
    );

    #Look for alternatives
    if($login_button) {
        $log->info('found login button');
        $self->config->set_uri(login_form_path => '/login');

        #correct text
        my $button_text = $login_button->as_text;

        if(! List::Util::any {$button_text eq $_} @{$self->config->get_value('login_button_text')} ) {
            $log->warnf('Unknown login button text "%s"', $button_text );
        }
    }

    my $search_form_anchor = $self->validate_xpath_nodes(
        label         => 'Search form anchor',
        context_node  => $xpc,
        path          => '//a[@href="/resume-search/" or @href="/cv-search/"]',
        expected_count => 1,
        severity      => 'fatal'
    );
    $self->config->set_uri(search_form => $search_form_anchor->attr('href'));

    #Verify login link

    #Verify search form link
    return 1;
}

sub verify_remote {
    my $self = shift;
    $log->info("Verifying: " . $self->board);
    $self->config->set_uri(base_url => $self->base_url);
    $self->scrape_homepage();
    $self->scrape_login_form();

    $self->login();

    $self->scrape_search_form();

    $self->config->store_crawl();

    print "\n\n";
}

1;

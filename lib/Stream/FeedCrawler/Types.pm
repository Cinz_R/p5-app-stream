package Stream::FeedCrawler::Types;

use Moose::Util::TypeConstraints;

use URI;

subtype 'URL',
    as 'URI';

coerce 'URL',
    from 'Str',
    via { return URI->new($_); };

no Moose::Util::TypeConstraints;

1;

package Stream::TP2::Utils;
use strict;
use warnings;

##
## This package is doomed as most of it relies on adcourier data
## and Bean::DB.
##

use Encode;

my $MAX_SALARY = 10_000_000;

## This is GONE.
##
# sub client_id {
#     my $company = shift;

#     if ( $company eq 'alexmannjobs' ) {
#         return 'amscont';
#     }

#     if ( $company eq 'amscont' ) {
#         return 'ams_live';
#     }

#     if ( my $associate = get_account_associations($company,'share') ){
#         return $associate if $associate eq 'demo_data';
#     }

#     return $company;
# }

## This is NOT used in Stream2
##
# sub client_id_to_company {
#     my $client_id = shift;

#     if ( $client_id eq 'ams_live' ) {
#         return 'amscont';
#     }

#     return $client_id;
# }

sub has_tp2 {
    my $username = shift;

    require Bean::Company::Permissions;
    return Bean::Company::Permissions->has_talentpod_two( $username );

}

sub jobs_applied_for {
    my $company = shift;
    my $email = shift;
    my $limit = @_ ? shift : '10';

    if ( $limit && $limit =~ m/^\d+$/ ) {
        $limit = ' LIMIT ' . $limit;
    }
    else {
        $limit = '';
    }

    require Bean::DB;
    require Bean::AdvertRow;

    my $response_sth = Bean::DB->prepare(
        'SELECT id, consultant, rank, time
           FROM '.$company.'_responses
          WHERE from_email = ?
       ORDER BY time DESC' . $limit
    ) or die Bean::DB->errstr();

    $response_sth->execute( $email )
        or die $response_sth->errstr();

    my @jobs;
    while( my ($aplitrakid, $consultant, $rank, $time_applied) = $response_sth->fetchrow_array() ) {
        my $advert = Bean::AdvertRow->new_from_aplitrakid(
            $aplitrakid,
            $consultant,
            $company,
        );

        my %job = (
            'rank'            => $rank,
            'jobtype'         => $advert->jobtype,
            'jobref'          => $advert->jobref,
            'jobtitle'        => $advert->jobtitle,
            'contactname'     => $advert->contactname,
            'salary_cur'      => $advert->currency,
            'salary_from'     => $advert->salary_from,
            'salary_to'       => $advert->salary_to,
            'salary_per'      => $advert->salary_per,
            'salary_benefits' => $advert->salary_benefits,
            'skills'          => $advert->skills,
            'location_1'      => $advert->location_1,
            'location_2'      => $advert->location_2,
            'industry'        => $advert->industry,
            'startdate'       => $advert->startdate,
            'duration'        => $advert->duration,
            'location_id'     => $advert->location_id,
            'time'            => $time_applied,
        );

        push @jobs, \%job;
    }

    return \@jobs;
}

# use Stream::TP2::ImportantTags instead
sub important_tags_label {
    my $company = shift;

    require Stream::TP2::ImportantTags;
    return Stream::TP2::ImportantTags->label($company);
}

# use Stream::TP2::ImportantTags instead
#sub important_tags {
#    my $username = shift;
#
#    return if !Stream::TP2::Utils::has_tp2($username);
#
#    require Stream::Tag::AutoCompleter;
#    my $suggestions = Stream::Tag::AutoCompleter->Suggest($username,'', { from => 0, size => '*', important => 'only'});
#    if ($suggestions && $suggestions->{'total'}){
#        my $results_ref = $suggestions->{'results'};
#        if ($results_ref && ref($results_ref) eq 'ARRAY'){
#            my @lowercase = map { { 
#                                    count => $_->{'count'},
#                                    tag   => lc($_->{'tag'}),
#                                    id    => $_->{'id'},
#                                    important => $_->{'important'},
#                                } } @$results_ref;
#            return wantarray ? @lowercase : \@lowercase;
#        }
#    }
#    return;
#}

sub get_applicant_by_id {
    my $company = shift;
    my $id = shift;

    require Stream::TP2::Search;
    my $json_ref = Stream::TP2::Search::submit_search($company, { id => $id }, { fields_to_return => '*' });
    my $docs = $json_ref->{'response'}->{'docs'};

    if (ref($docs) eq 'ARRAY' && @$docs[0]){
        my $candidate = $docs->[0];
        if ($candidate->{availability_date} && $candidate->{availability_date} =~/^\d+$/) {
            require DateTime;
            $candidate->{availability_date_dt} = DateTime->from_epoch(epoch => $candidate->{availability_date});
        }

        if ( $candidate->{applicant_tags} ) {
            require Stream::TP2::ImportantTags;
            my %all_important_tags = Stream::TP2::ImportantTags->unique_for($company);

            my @important_tags;
            my @normal_tags;
            foreach my $tag ( @{ $candidate->{applicant_tags} } ) {
                if ( exists($all_important_tags{ lc($tag) }) ) {
                    push @important_tags, $tag;
                }
                else {
                    push @normal_tags, $tag;
                }
            }

            $candidate->{applicant_tags} = \@normal_tags;
            $candidate->{important_tags} = \@important_tags;
        }

        return $candidate;
    }
}

sub get_cv_by_id {
    my $company = shift;
    my $id = shift;

    my $applicant_ref = get_applicant_by_id( $company, $id )
        or return;

    if ( $applicant_ref->{client_id} eq 'demo_data' ){
        require Path::Class;
        return Path::Class::file( '/data0/www/adcourier.broadbean/site/Stream/TP2/FakeCV/sample-cv.doc' )->slurp();
    }

    my $attachment_url = $applicant_ref->{attachment_url}
        or return;

    require Stream::TP2::DownloadCV;
    return Stream::TP2::DownloadCV::download_url( $attachment_url );
}

###
### Note: Those are GONE and replaces by users api calls in consuming code.
###

# sub get_account_associations {

#     my $company = shift;

#     # TODO
#     # account associations will need to be passed through from AdC,
#     # for TP2 it should be part of the auth tokens for this board
#     return undef;

#     my $type = shift;
#     my @associates = ();

#     require Bean::DB;
#     my $sth = Bean::DB->prepare('SELECT associates FROM tp2_associations WHERE company = ? AND type = ?') or die Bean::DB->errstr();
#     $sth->execute( $company, $type ) or die $sth->errstr();

#     my $dbh_out = $sth->fetchrow_array();
#     if ( wantarray ) {
# 	    return $dbh_out ? split ( /,/ , $dbh_out ) : ();
#     }

#     return $dbh_out;

# }

# sub set_account_associations {

#     my $company = shift;
#     my $last_updated = shift;
#     my $type = shift;
#     my $associates = join(',',@_);

#     require Bean::DB;
#     my $sth = Bean::DB->prepare('UPDATE tp2_associations SET associates=?,last_updated=? WHERE type = ? AND company=? LIMIT 1') or die Bean::DB->errstr();
#     $sth->execute( $associates, $last_updated, $type, $company ) or die $sth->errstr();

#     unless ( $sth->rows() > 0 ) {
#         my $sth = Bean::DB->prepare('INSERT INTO tp2_associations VALUES(?,?,?,?)') or die Bean::DB->errstr();
#         $sth->execute( $company, $associates, $last_updated, $type ) or die $sth->errstr();
#     }
#     return 1;

# }

sub response_to_tp2 {

    my $options_ref = shift;

    require Stream::Resque;
    Stream::Resque->add({
        task    =>  'distribute_response',
        queue   =>  'UploadTP2',
        class   =>  'Stream::Resque::UploadTP2',
        args    =>  $options_ref
    });

}

# returns a GBP annual & daily salary for comparison purposes
sub normalise_salary {
    my ($salary_cur, $salary_from, $salary_to, $salary_per) = @_;
    my ($from_annum, $to_annum) = normalise_annual_salary( $salary_cur, $salary_from, $salary_to, $salary_per );
    my ($from_day, $to_day) = normalise_daily_salary( $salary_cur, $salary_from, $salary_to, $salary_per );
    my ($from_week, $to_week) = normalise_weekly_salary( $salary_cur, $salary_from, $salary_to, $salary_per );

    return ($from_annum, $to_annum, $from_day, $to_day, $from_week, $to_week);
}

sub normalise_annual_salary {
    my ($salary_cur, $salary_from, $salary_to, $salary_per) = @_;

    return _normalise_salary( 'annum', $salary_cur, $salary_from, $salary_to, $salary_per );
}

sub normalise_daily_salary {
    my ($salary_cur, $salary_from, $salary_to, $salary_per) = @_;

    return _normalise_salary( 'day', $salary_cur, $salary_from, $salary_to, $salary_per );
}

sub normalise_weekly_salary {
    my ($salary_cur, $salary_from, $salary_to, $salary_per) = @_;

    return _normalise_salary( 'week', $salary_cur, $salary_from, $salary_to, $salary_per );
}

=head2 _normalise_salary

Ensure salary elements conform to the expected format.
Converts between different salary periods ( week, month, annum, etc )
Also ensures the salary is not negative or greater than $MAX_SALARY

=cut

sub _normalise_salary {
    my $new_salary_per = shift;
    my ($salary_cur, $salary_from, $salary_to, $salary_per) = @_;

    if ( !$salary_from && !$salary_to ) {
        return;
    }

    # some safe defaults just in case
    $salary_cur ||= 'GBP';
    $salary_per ||= 'annum';

    ($salary_from, $salary_to) = normalise_currency( $salary_cur, $salary_from ,$salary_to );

    require Stream::Token;
    my $convert = Stream::Token->new({
        type => q{salaryper},
        options => $new_salary_per,
        value   => $salary_per,
    });

    my $normalised_salary_from = $salary_from ? $convert->convert( $salary_from ) : 0;
    my $normalised_salary_to   = $salary_to   ? $convert->convert( $salary_to )   : $MAX_SALARY;

    if ( $normalised_salary_from > $MAX_SALARY ) {
        $normalised_salary_from = $MAX_SALARY;
    }

    if ( $normalised_salary_to > $MAX_SALARY ) {
        $normalised_salary_to = $MAX_SALARY;
    }

    return ($normalised_salary_from, $normalised_salary_to);
}

=head2 normalis_currency

convert symbol to ISO currency
Also checks if currency is valid by attempting a conversion

=cut

sub normalise_currency {
    my ($salary_cur, $salary_from, $salary_to) = @_;

    my $pound = decode('utf-8', '£');
    my $euro  = decode('utf-8','€');

    if ( $salary_cur =~ m/$pound/ ) {
        $salary_cur = 'GBP';
    }
    elsif ( $salary_cur =~ m/$euro/ ) {
        $salary_cur = 'EUR';
    }
    elsif ( $salary_cur eq '$' ) {
        $salary_cur = 'USD';
    }
    elsif ( $salary_cur eq 'CAN' ) {
        $salary_cur = 'CAD'; # we screwed up somewhere
    }
    elsif ( length($salary_cur) < 3 ) {
        $salary_cur = 'GBP';
    }

    require Stream::Token;
    my $currency = Stream::Token->new({
        type    => q{currency},
        options => q{GBP},
        value   => $salary_cur,
    });

    if ( !$currency->is_valid() ) {
        if ( !$currency->exchange_rate( $salary_cur => 'GBP' ) ) {
            warn "Do not recognise currency '$salary_cur' so assuming GBP\n";
            $currency->value( 'GBP' );
        }

        $salary_from = $currency->convert( $salary_from );
        $salary_to   = $currency->convert( $salary_to );
    }

    return ($salary_from, $salary_to);
}

1;

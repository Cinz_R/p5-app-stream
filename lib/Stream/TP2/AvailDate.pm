#
#===============================================================================
#
#         FILE:  AvailDate.pm
#
#      COMPANY:  Broadbean
#      CREATED:  05/01/12 10:39:03
#     REVISION:  $Id: AvailDate.pm 72853 2012-04-13 09:56:02Z darren $ 
#===============================================================================
package Stream::TP2::AvailDate;
use strict;
use warnings;

## ----------------------------------------------------------------
##  Object Constructor
## ----------------------------------------------------------------
sub new {
    my $package = shift;
    my $self = bless {}, $package;
    my $str = shift;

    die "Missing Format." if ! $str;
    if (is_oop($str)) {
        $self = $str;
    }
    elsif ($str =~/^\d+$/) {
        $self->{epoch} = $str;
    }
    elsif (isIso8601String($str)) { 
        $self->{epoch} = datetime(new => getHashFromIso8601($str))->epoch;
    }
    elsif (my $days = getDateRange($str)) {
        $self = $days->[0]->niche =~/Day/ ? $days->[0] : $days->[0]->clone(days => 1)
    }
    else {
        die "Unknown Format ($str).";
    }
    return $self;
}

sub clone {
    my $self = shift;
    my $unit = shift;
    my $days = shift;
    my $package = ref $self;
    $package->new($self->datetime->add($unit => $days)->epoch);
}

## ----------------------------------------------------------------
##  Object Methods
## ----------------------------------------------------------------
sub epoch {
    my $self = shift;
    return $self->{epoch};
}

sub get2days {
    my $self  = shift;
    my @range  = &getDateRange;
    my $package = ref $self;
    for (my $i = 0; $i <= $#range; $i += 2) {
        my ($label, $between) = @range[$i, $i+1];
        if ($self->epoch >= $between->[0]->epoch && $self->epoch < $between->[1]->epoch) {
            #$between->[0]->set_datetime(qw(hour 0 minute 0 second 0));
            #$between->[1]->set_to_last_minute;
            return @$between;
        }
    }
}

sub dmy {
    my $self    = shift;
    my $epoch   = $self->epoch;
    my $format  = shift || "%02d %s %04d" ;
    my @element = $#_ != -1 ? @_ : qw(day month_name year);
    my $date = datetime(from_epoch => (epoch => $self->epoch));
    return sprintf $format => map {$date->$_} @element; 
}

sub ymd {
    my $self = shift;
    $self->dmy("%04d-%02d-%02d", qw(year month day));
}

sub niche {
    my $self   = shift;
    my $epoch  = $self->epoch;
    my @range  = &getDateRange;
    for (my $i = 0; $i <= $#range; $i += 2) {
        my ($label, $between) = @range[$i, $i+1];
        if ($epoch > $between->[0]->epoch && $epoch <= $between->[1]->epoch) {
            return $label;
        }
    }
}

sub pair_label {
    my $self = shift;
    my %pair = getDateRange("pair-labels");
    my $niche = $self->niche;
    return $pair{$niche};
}

## ----------------------------------------------------------------
##  Static / Object Methods
## ----------------------------------------------------------------
sub datetime {
    require DateTime;
    my $dt;
    if (is_oop(@_)) {
        my $self = shift;
        $dt = DateTime->from_epoch(epoch => $self->epoch);
    }
    else {
        my $constructor = shift;
        $dt = DateTime->$constructor(@_);
    }
    $dt->set_time_zone("Europe/London");
}

sub getDateStringIso8601 {
    if (is_oop(@_)) {
        my $self = shift;
        return $self->datetime->iso8601 . 'Z';
    }
    else {
        my $label = shift;
        my $days = getDateRange($label);
        return $days->[0]->datetime->iso8601 . 'Z' if $days;
    }
}

## ----------------------------------------------------------------
##  Static Methods
## ----------------------------------------------------------------
sub is_oop {
    my $test = shift;
    return defined($test) && UNIVERSAL::isa($test, __PACKAGE__);
}

sub getHashFromIso8601 {
    my $str = shift;
    chop $str;
    my ($date, $time) = split /T/, $str;
    my (%date, %time);
    @date{qw(year month day)} = split /-/, $date;
    @time{qw(hour minute second)} = split /\:/, $time;
    return (%date, %time);
}

sub getDateRange {
    my $find = shift;
    my $now  = __PACKAGE__->new(time);
    my @range = (
          "1 Day"     => [$now->clone(days   =>  0), $now->clone(days   =>  1)]
        , "2 Days"    => [$now->clone(days   =>  1), $now->clone(days   =>  2)]
        , "3 Days"    => [$now->clone(days   =>  2), $now->clone(days   =>  3)]
        , "4 Days"    => [$now->clone(days   =>  3), $now->clone(days   =>  4)]
        , "5 Days"    => [$now->clone(days   =>  4), $now->clone(days   =>  5)]
        , "6 Days"    => [$now->clone(days   =>  5), $now->clone(days   =>  6)]
        , "1 Week"    => [$now->clone(days   =>  6), $now->clone(days   =>  7)] 
        , "2 Weeks"   => [$now->clone(days   =>  7), $now->clone(days   => 14)] 
        , "3 Weeks"   => [$now->clone(days   => 14), $now->clone(days   => 21)] 
        , "1 Month"   => [$now->clone(days   => 21), $now->clone(months =>  1)] 
        , "2 Months"  => [$now->clone(months =>  1), $now->clone(months =>  2)]
        , "3 Months"  => [$now->clone(months =>  2), $now->clone(months =>  3)]
        , "4 Months"  => [$now->clone(months =>  3), $now->clone(months =>  4)]
        , "5 Months"  => [$now->clone(months =>  4), $now->clone(months =>  5)]
        , "6 Months"  => [$now->clone(months =>  5), $now->clone(months =>  6)]
        , "7 Months"  => [$now->clone(months =>  6), $now->clone(months =>  7)]
        , "8 Months"  => [$now->clone(months =>  7), $now->clone(months =>  8)]
        , "9 Months"  => [$now->clone(months =>  8), $now->clone(months =>  9)]
        , "10 Months" => [$now->clone(months =>  9), $now->clone(months => 10)]
        , "11 Months" => [$now->clone(months => 10), $now->clone(months => 11)]
        , "1 Year"    => [$now->clone(months => 11), $now->clone(months => 18)]
        , "2 Years"   => [$now->clone(months => 18), $now->clone(years  =>  2)]
    );
    if ($find) {
        if ($find =~/^\d+\s/) {
            if ($find =~/\~/) {
                my %pair = reverse getDateRange("pair-labels");
                $find =~ s/1\s\~\s(\d+)\s(\w+)s/1 $2 ~ $1 $2s/; #Horrible hack to allow us to send 1 ~ 2 Weeks for example
                $find = $pair{$find};
            }
            my %range = @range;
            return $range{$find};
        }
        elsif ($find eq "labels") {
            my @labels = grep {/\s/} @range;
            return wantarray ? @labels : \@labels;
        }
        elsif ($find eq "pair-labels") {
            my @labels;
            for (my $i = 0; $i <= $#range; $i+=2) {
                if ($range[$i+2]) {
                    my @from = split /\s/, $range[$i];
                    my @till = split /\s/, $range[$i+2];
                    my $from = $from[1] eq $till[1] ? $from[0] : $range[$i];
                    push @labels, $range[$i+2], "$from ~ $range[$i+2]";
                }
                else {
                    push @labels, $range[$i], "After $range[$i]";
                }
            }
            return @labels;
        }
    }
    return wantarray ? @range : {@range};
}

sub isValidNiche {
    return (grep {$_[0] eq $_} getDateRange("labels")) ? 1 : 0;
}

sub isValidRangeLabel {
    my %pair = reverse getDateRange("pair-labels");
    return exists $pair{$_} ? 1 : 0;
}

sub isIso8601String {
    $_[0] =~/^\d{4}\-\d{2}\-\d{2}T\d{2}\:\d{2}\:\d{2}Z?$/ ? 1 : 0;
}

## Convert  $tokens_ref->{facet_counts}{facet_fields}{applicant_availability_date}
## To       @stats = (1 Day => 5 People, 1 Month => 10 People, etc...) in array format
##          %stats = (...      ...          ...       ...         ...) in hash format
sub getDateStats {
    my $list = shift;
    my %dates = @$list;
    my %map;
    foreach my $date(keys %dates) {
        my $avd = __PACKAGE__->new($date);
        next if $avd->epoch < time;
        my $label = $avd->niche;
        $map{$label} ||= [];
        push @{$map{$label}}, $date;
    }
    my @range = grep {/\s/} &getDateRange;
    my @new_list;
    my %pair = getDateRange("pair-labels");
    foreach my $label(@range) {
        my $sub   = $map{$label};
        next if ! $sub;
        my $total = 0;
        map {$total += $dates{$_}} @$sub;
        next if ! $total;
        push @new_list, $pair{$label}, $total;
    }
    return @$list = @new_list;
}

sub getDateStatsES {
    my $ranges_ref = shift || [];
    my @counts;

    RANGE:
    foreach my $range (sort { $a->{'from'} <=> $b->{'from'} } @$ranges_ref){
        next RANGE if !$range->{'count'}; #Don't include ranges with no results

        #epochs are in milliseconds
        my $from_epoch = $range->{'from'} / 1000;
        my $to_epoch = $range->{'to'} / 1000;

        my $label;
        
        if ($from_epoch && $to_epoch){
            $label = get_range_label($from_epoch, $to_epoch);
        } elsif (!$from_epoch && $to_epoch) {
            $label = 'Past';
        }

        push @counts, $label, $range->{'count'};
    }

    return @$ranges_ref = @counts;
}   

sub get_range_label {
    my $from_epoch = shift;
    my $to_epoch = shift;

    my @ranges = (
        #Label => time in seconds difference
        { '1 Day' => '86400' },
        { '2 Days' => '172800' },
        { '3 Days' => '259200' },
        { '4 Days' => '345600' },
        { '5 Days' => '432000' },
        { '6 Days' => '518400' },
        { '1 Week' => '604800' },
        { '2 Weeks' => '1209600' },
        { '3 Weeks' => '1814400' },
        { '1 Month' => '2419200' },
        { '2 Months' => '4838400' },
        { '3 Months' => '7257600' },
        { '4 Months' => '9676800' },
        { '5 Months' => '12096000' },
        { '6 Months' => '14515200' },
        { '7 Months' => '16934400' },
        { '8 Months' => '19353600' },
        { '9 Months' => '21772800' },
        { '10 Months' => '24192000' },
        { '11 Months' => '26611200' },
        { '1 Year' => '29030400' },
        { '2 Years' => '58060800' },
    );

    my $from_label = _label_from_range($from_epoch, @ranges);
    my $to_label = _label_from_range($to_epoch, @ranges);

    $from_label =~ s/\s?(?:day|week|month|year)s?//i;

    return "$from_label ~ $to_label";
}

sub _label_from_range {
    my $epoch = shift;
    my @ranges = @_;

    my $epoch_diff = $epoch - time;

    my $range_label;
    foreach my $range (@ranges){
        my ($label) = keys %$range;
        my $seconds_diff = $range->{$label};

        if ($epoch_diff > $seconds_diff){
            $range_label = $label;
        } else {
            last;
        }
    }

    return $range_label;
}

1;

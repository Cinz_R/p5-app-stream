package Stream::TP2::Callbacks;

use strict;
use warnings;

use Stream::SharedUtils;
use Bean::DB;
use Bean::Statsd;

=head1 Stream::TP2::Callbacks

=head2 SYNOPSIS

  use Stream::TP2::Callbacks;

  # create a callback for a TP2 update
  my $callbacks = Stream::TP2::Callbacks->new({ document => $doc });
  $callbacks->create();
  my $success_callback = $callbacks->on_success(); # url that Artirix will post to if request is successful
  my $error_callback   = $callbacks->on_error();   # url that Artirix will post to if request fails

  # to complete a callback
  my $callbacks = Stream::TP2::Callbacks->new({ id => $callback_id });
  $callbacks->success();    # if Artirix says it was successful
  $callbacks->error($msg);  # or if Artirix says it failed

=cut

use base 'Class::Accessor::Fast';

__PACKAGE__->mk_accessors( qw/id command solr_id applicant_email applicant_mobile employer_org_name client_id context/ );

# DB accessors
__PACKAGE__->mk_accessors( qw/was_success msg response_datetime/ );

sub was_error {
    return !$_[0]->was_success() && $_[0]->is_complete() ? 1 : 0;
}

sub is_complete {
    return $_[0]->{response_datetime} ? 1 : 0;
}

sub new_from_doc {
    my $class = shift;
    my $doc_ref = shift;

    my %options = (
        command => $doc_ref->{command} || 'missing!',
    );

    my $record_ref = $doc_ref->{ref} || {};
    if ( exists( $record_ref->{id} ) ) {
        $options{solr_id} = $record_ref->{id};
    }
    foreach my $attribute ( qw/applicant_email applicant_mobile employer_org_name client_id/ ) {
        if ( exists( $record_ref->{$attribute} ) ) {
            $options{$attribute} = $record_ref->{$attribute};
        }
    }

    if ($doc_ref->{'context'}){
        $options{'context'} = $doc_ref->{'context'};
    }

    return $class->new( \%options );
}

#
#---- METHODS ----#
#
sub create {
    my $self = shift;

    my ($consultant,$team,$office,$company) = Stream::SharedUtils::current_lutoc();

    my $new_row_ref = {
        id          => undef, #  autoincrement :)

        # context for the request
        consultant  => $consultant,
        team        => $team,
        office      => $office,
        company     => $company,
        devuser     => Stream::SharedUtils::current_devuser() || '',
        remote_addr => Stream::SharedUtils::remote_addr()     || '',
        context         => $self->context()         || '',

        # what was sent
        request_datetime  => \'NOW()',
        command           => $self->command()           || '',
        solr_id           => $self->solr_id()           || '',
        applicant_email   => $self->applicant_email()   || '',
        applicant_mobile  => $self->applicant_mobile()  || '',
        employer_org_name => $self->employer_org_name() || '',
        client_id         => $self->client_id()         || '',
    };

    my $id = $self->insert( $new_row_ref );

    if ( $id ) {
        $self->id( $id );
        Bean::Statsd->increment("tp2.callbacks.new");
        return $self;
    }
    
    return undef;
}

# completion methods
sub success {
    Bean::Statsd->increment("tp2.callbacks.success");
    return $_[0]->complete(1, $_[1]);
}

sub error {
    Bean::Statsd->increment("tp2.callbacks.error");
    return $_[0]->complete(0, $_[1]);
}

sub complete {
    my $self = shift;
    my $success = shift;
    my $msg     = shift;

    my %update = (
        success           => $success,
        msg               => $msg,
        response_datetime => \'NOW()', 
    );

    if ($self->solr_id()){
        $update{'solr_id'} = $self->solr_id();
    }

    my $ok = $self->update(\%update);

    # don't worry if this fails
    eval { $self->record_lag(); };

    return $ok;
}

sub after_success {
    my $self = shift;
    $self->fetch();

    if ($self->context()){
        require JSON;
        my $context_ref = JSON::from_json($self->context());
        $self->_act_on_context($context_ref);
    }
}

sub _act_on_context {
    my $self = shift;
    my $context_ref = shift || {};

    if ($context_ref->{'stream_id'} && $self->solr_id()){
        require Stream::Results::Candidate;
        my $candidate = eval { Stream::Results::Candidate->from_file($context_ref->{'stream_id'}) };
        if ($candidate && !$@){
            $self->_add_mapping($candidate);
            $self->_copy_tags_to_artirix($candidate);
        }
    }
}

sub _copy_tags_to_artirix { #Copy the candidates tags from the board to artirix
    my $self = shift;
    my $candidate = shift;

    require Stream::Tagging;
    my $tagging = Stream::Tagging->new({
            username => $candidate->username,
            board => $candidate->destination,
            candidate_id => $candidate->candidate_id,
    });

    my @tags = $tagging->list_tags_by_board_id();

    foreach my $tag (@tags){
        #"Copy" the record as an artirix candidate
        $tag->id(undef);
        $tag->board('artirix');
        $tag->candidate_id($self->solr_id);
        $tag->store(); #will upsert

        #Update artirix so it has ALL the tags associated with that candidate
        require Stream::Tagging::Plugins::Artirix;
        my $artirix = Stream::Tagging::Plugins::Artirix->new({
                username => $candidate->username,
                board => $tag->board,
                candidate_id => $tag->candidate_id,
                tag_name => $tag->tag_name,
        });

        $artirix->add_tag();
    }
}

sub _add_mapping { #Create an entry in the mapping table so we can go between artirix & stream
    my $self = shift;
    my $candidate = shift;

    require Stream::TP2::Map;
    my $map = Stream::TP2::Map->new({ 
            solr_id => $self->solr_id(),
            candidate_id => $candidate->candidate_id,
            board => $candidate->destination,
            company => $candidate->company,
            });

    return $map->store();
}

sub on_success {
    if ( my $id = $_[0]->id() ) {

        my $host = 'www';
        if ($_[0]->_host() eq 'cheerleader.gs'){
            $host = 'cheerleader.gs';
        }

        return sprintf 'http://%s.adcourier.com/pp/_tp2/%s/success', $host, $id;
    }
    die "Callback has not been created yet - we are missing an id, call create() first";
}

sub on_error {
    if ( my $id = $_[0]->id() ) {

        my $host = 'www';
        if ($_[0]->_host() eq 'cheerleader.gs'){
            $host = 'cheerleader.gs';
        }

        return sprintf 'http://%s.adcourier.com/pp/_tp2/%s/error', $host, $id;
    }
    die "Callback has not been created yet - we are missing an id, call create() first";
}

sub _host {
    require Stream::SharedUtils;
    return Stream::SharedUtils::hostname();
}

sub url_to_id {
    my $class = shift;
    my $url = shift;

    if ( $url =~ m{_tp2/([^/]+)/} ) {
        return $1;
    }

    return;
}

sub all {
    my $self = shift;
    return {
        success => $self->on_success(),
        failure => $self->on_error(),
    };
}

#
#---- DB manipulation, private ----#
# 
sub insert {
    my $self = shift;
    my $new_row_ref = shift or die "Missing new_row_ref - we can't insert nothing";

    my ($sth, @binds) = Bean::DB->insert( 'tp2_callbacks', $new_row_ref );
    $sth->execute( @binds )
        or die $sth->errstr();

    my ($id) = Bean::DB::last_insert_id( $sth, 'tp2_callbacks', 'id' );

    return $id;
}

sub fetch {
    my $self = shift;
    my $id = $self->id() or die "Missing id() - we can't fetch unless you tell us what id to fetch";

    my @cols_to_fetch = qw/
        consultant
        team
        office
        company
        devuser
        remote_addr

        request_datetime
        command
        solr_id
        applicant_email
        applicant_mobile
        employer_org_name
        client_id
        context

        msg
        response_datetime
    /;
    my ($sth, @binds) = Bean::DB->select(
        'tp2_callbacks',
        [ @cols_to_fetch, 'success' ],
        { id => $id },
    );

    $sth->execute( @binds )
        or die $sth->errstr();

    my @values = $sth->fetchrow_array();
    if ( !@values || !$sth->rows() ) {
        return;
    }

    # the success column corresponds to the was_success accessor
    my $success = pop @values;
    $self->was_success( $success );

    # the rest of the columns have an accessor with the same name
    @$self{@cols_to_fetch} = @values;

    return $self;
}

sub update {
    my $self = shift;
    my $update_ref = shift or die "Missing update_ref - we can't update if you don't tell us what to update";
    my $where_ref = {
        id => $self->id(),
    };

    my ($sth, @binds) = Bean::DB->update( 'tp2_callbacks', $update_ref, $where_ref );
    $sth->execute( @binds )
        or die $sth->errstr();

    return $sth->rows();
}

sub record_lag {
    my $self = shift;

    my $lag = $self->get_lag();

    if ( $lag >= 0 ) { 
        return Bean::Statsd->average("tp2.callbacks.lag" => $lag * 1000);
    }

    return;
}

sub get_lag {
    my $self = shift;
    my $id = shift;

    my ($sth,@binds) = Bean::DB->select(
       'tp2_callbacks',
       [ 'IF(response_datetime,TIMESTAMPDIFF(SECOND,request_datetime,response_datetime),NULL)' ], {
           id => $self->id(),
        },
    );

    $sth->execute(@binds)
        or die $sth->errstr();

    my ($lag) = $sth->fetchrow_array();

    return $lag;
}

1;

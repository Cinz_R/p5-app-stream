package Stream::TP2::DownloadCV;
use strict;
use warnings;

use URI;
use Sys::Hostname;

sub download_url {
    my $attachment_url = shift;

    my $uri = URI->new( $attachment_url )
        or die "can't download '$attachment_url': the url is invalid";

    if ( $uri->scheme() eq 's3' ) {
        ## This is now irrelevant. Because we are in the cloud.
        ## Remove next time you see this.
        # # bg3.gs and bg4.gs cannot access the outside world
        # # no idea why it has started happening
        # # no other server is affected
        # # grab the TP2 CV via the proxy on blade11.gs
        # # while there are problems.
        # # Remove this when resolved - 08/02/2013
        # local %ENV = %ENV;
        # if ( hostname() eq 'bg4.gs.broadbean.net' ) {
        #     $ENV{HTTP_PROXY} = "http://46.254.116.43:8888";
        # }
        return _download_from_s3( $uri );
    }

    return _download_http( $uri );
}

sub _download_from_s3 {
    my $uri = shift;

    my $host = $uri->authority();
    my $key  = $uri->path();

    $key =~ s{^/}{}g;

    if ( $host eq 'attachments.broadbean.com' ) {
        require Bean::FileStore::ArtirixCV;
        return Bean::FileStore::ArtirixCV->get_file_data($key, $host);
    }

    require Bean::FileStore::S3;
    return Bean::FileStore::S3->get_file_data($key, $host);
}

sub _download_http {
    my $url = shift;
    my $options_ref = shift || {};

    require LWP::UserAgent;
    my $lwp = LWP::UserAgent->new();
    my $resp = $lwp->get( $url );
    if ( $resp->is_success() ) {
        return $resp->decoded_content();
    }
    return;
}

1;

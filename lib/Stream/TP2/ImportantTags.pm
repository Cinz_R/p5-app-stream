package Stream::TP2::ImportantTags;
use strict;
use warnings;

use Stream2;

use Stream::TP2::Utils;
use Stream::Tag::AutoCompleter;

use List::MoreUtils qw(uniq);

# returns a list of tags, only goes to the DB if the username is different to the previous call
{
    my $cached_important_tags_ref;
    my $cached_username;
    my $cached_time;
    sub cached_list {
        my $class = shift;
        my $username = shift;

        my $refresh_cache = 0;
        if ( !$cached_important_tags_ref
            || !$cached_username 
            || $username ne $cached_username ) {
            $refresh_cache = 1;
        }
        elsif ( time - $cached_time > 5 ) {
            $refresh_cache = 1; # cache is over 5 seconds old so refresh it
        }

        if ( $refresh_cache ) {
            $cached_username = $username;
            $cached_important_tags_ref = $class->list( $username ) || [];
            $cached_time = time;
        }

        return $cached_important_tags_ref ? @$cached_important_tags_ref : ();
    }
};

# returns a hash containing the unique important tags ( lowercase tag => tag hash )
sub unique_for {
    my $class    = shift;
    my $username = shift;
    my @tags = $class->cached_list( $username );

    my %unique = map {
        lc($_->{tag}) => $_
    } @tags;

    return %unique;
}

# returns an array containing just the important tags names
sub list_names {
    my $class = shift;
    my $username = shift;

    my @important_tags = $class->list( $username );
    my @tag_names = map { $_->{'tag'} } @important_tags;

    return @tag_names;
}

# returns an array containing the important tags
sub list {
    my $class = shift;
    my $username = shift;

    return if !Stream::TP2::Utils::has_tp2($username);

    my $suggestions = Stream::Tag::AutoCompleter->Suggest(
        $username, '', {
            from => 0,
            size => '*',
            important => 'only'
        },
    ) or return;

    if ( !$suggestions->{total} ) {
        return;
    }

    my $results_ref = $suggestions->{'results'}
        or return;

    return unless ref($results_ref) eq 'ARRAY';

    my @lowercase = map { { 
        count => $_->{'count'},
        tag   => lc($_->{'tag'}),
        id    => $_->{'id'},
        important => $_->{'important'},
    } } @$results_ref;

    return wantarray ? @lowercase : \@lowercase;
}

sub label {
    my $class = shift;
    my $company = shift;

    return Stream2->instance()->user_api()->company_has_talentpools($company) ?
      'Talent Pools' : 'Hotlists';
}

=head2 separate_tags_and_talentpools

Class method.

Given a company and a  list of tags (as strings), returns two ArrayRefs:

One containing the 'pure' tags. One containing the 'important' (aka talentpool, hotlist, ...)
tags.

# pass the company & a list of tags and talentpools
# and it returns two array refs:
# the first contains the tags, the second contains the talentpools

=cut

sub separate_tags_and_talentpools {
    my $class = shift;
    my $company = shift;
    my @all_tags = @_;


    # Use the DB to separate Tags and TalentPool tags (AKA hotlist)

    return (\@tags, \@talentpools);
}

1;

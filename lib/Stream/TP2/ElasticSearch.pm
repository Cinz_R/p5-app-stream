package Stream::TP2::ElasticSearch;

use strict;
use warnings;

use Data::Dumper;

use base qw/Stream::TP2::Search/;

use Stream::TP2;
use Stream::TP2::Constants;

use Stream::TP2::Utils;
use Stream::SharedUtils;
use Time::HiRes;
use Stream::TP2::AvailabilityDate;
use DateTime;

use Carp;

use JSON qw//;

use Log::Any qw/$log/;

#DEBUG
#   - 1 to turn on explain
#   - 2 to turn on score display
#   - 3 to include header
#   - 4 to die with request/response
my $DEBUG = 0; 

sub using_es {
    return 1; # everyone is now using elasticsearch
}

=head2 submit_search

Performs a search and returns a Solr style response pure hash.

This is a PURE package function

Options MUST contain a location_api instance of L<Bean::Juice::APIClient::Location>
Options MUST contain a user_api instance of L<Bean::Juice::APIClient::User>

If the param contains a key 'id', redirects to this::get_by_id

Usage:

   Stream2::TP2::ElasticSearch::submit_search($company, \%params , \%options );

=cut

sub submit_search {
    my $company = shift;
    my $params = shift || {};
    my $options_ref = shift || {};

    my $location_api = $options_ref->{location_api} || confess("Missing location_api in options");
    my $user_api = $options_ref->{user_api} || confess("Missing user_api in options");

    # Asking for a specific candidate ID. Redirecting to get_by_id
    if ($params->{'id'}){
        return get_by_id($params->{'id'}, $company, $options_ref);
    }

    # Not asking for a specific candidate.

    my $keywords = $params->{'keywords'} || '';
    my $start = $params->{'start'} || 0;
    my $rows = $params->{'rows'} || 10;

    #This is where the magic happens. or not.

    ## Elastic search main query
    my $query_ref = { from => $start, size => $rows };

    # Query components
    my (@query, @filters, @query_filters, $facet_filters_ref);

    if ( $options_ref->{explain} ) {
        $query_ref->{explain} = $options_ref->{explain};
    }

    my $client_id = _tp2_client_id_from_company( $user_api , $company ) || die "No Client ID";
    if ($client_id && $client_id ne '*'){
        my @client_ids = ($client_id);

        # We only care for the first one.
        if ( my ( $account_associate ) = $user_api->company_associations($company,'share')){
            push @client_ids, _tp2_client_id_from_company($user_api, $account_associate);
        }

        my @ors;
        $log->debug("Client ids are '".join(',', @client_ids)."'");

        foreach my $id (@client_ids){ push @ors, { term => { 'client_id' => $id } }; }
        push @query_filters, { 'or' => \@ors };
    }

    if ($params->{'order_by'}) {
        $query_ref->{sort} = {'_score' => {'order' => 'desc'}} if( $params->{'order_by'} eq "Relevance" );
        $query_ref->{sort} = {'applicant_application_time' => {'order' => 'desc'}} if( $params->{'order_by'} eq "Last Updated" );
    }

    if ($keywords){
        push @query, _query_string( _all => $keywords );
        $query_ref->{'highlight'} = { 'fields' => { 
                                                    'cv_text' => {},
                                                    'employer_org_position_title' => {},
                                                    'employer_org_position_description' => {} 
                                                   },
                                        fragment_size => 300,
                                        number_of_fragments => 3,
                                    };
    } else {
        $query_ref->{'explain'} = 'true' if $DEBUG; 
    }

    if ( exists($options_ref->{es_sort}) && $options_ref->{es_sort} ) {
        $query_ref->{sort} = $options_ref->{es_sort};
    }

  # you can select multiple options for these fields. All these fields are terms (ie. no analyzing/no stemming)
    my @multilist_fields = qw/advert_job_type advert_industry applicant_recruitment_status/;
    FIELD:
    foreach my $field ( @multilist_fields ) {
        next if ( !exists($params->{$field}) );

        my (@selected_opts) = ref($params->{$field}) eq 'ARRAY' ? @{ $params->{$field} } : ($params->{$field});

        my @ors;

        foreach my $selected_opt ( @selected_opts ) {
            if ( $selected_opt ) {
                if ( $field eq 'applicant_recruitment_status' ) {
                    if ( $selected_opt eq 'Available' ) {
                        push @ors, { missing => { field => $field } };
                    }
                }
                push @ors, { term => { $field => $selected_opt } };
            }
        }

        if ( @ors ) {
            push @filters, { 'or' => \@ors };
            $facet_filters_ref->{$field} = { 'or' => \@ors };
        }
    }
    
    my @tags = Stream::TP2::Search::normalise_tags($params->{'applicant_tags'});
    if (scalar(@tags)){

        # We need to separate tags from hot tags (aka talentpool, aka 'important' tags).
        my ($talentpools_ref, $tags_ref) = Stream2->instance->factory('Tag')->split_hotlist_tags( $company,
                                                                                        \@tags );

        my @tag_ands = _tags_to_filter(@$tags_ref);
        my @tp_ands = _tags_to_filter(@$talentpools_ref);

        my @ands;
        push @ands, { 'bool' => { 'should' => \@tag_ands } } if scalar @tag_ands;
        push @ands, { 'bool' => { 'should' => \@tp_ands } } if scalar @tp_ands;

        if (scalar (@ands)){
            push @query, { 'bool' => { 'must' => \@ands } };
        }
    }

    #location with radius
    if (my $location_id = $params->{'location_id'}) {

        my $location = $location_api->find({ id => $location_id});

        if ( $location ) {
            my ($latitude, $longitude) = ($location->latitude(), $location->longitude());
            my $location_within = $params->{'location_within'} || 10; # 10 km not miles
            # TODO: This default should be changed back to 'km' once we are using user locale
            my $location_distance = $params->{'distance_unit'} || 'km';
            my $distance = $location_within . $location_distance;

            my $geo_filter_ref = {
                "applicant_latlon" => { 'lat' => $latitude, 'lon' => $longitude },
                distance => $distance,
            };

            push @filters, { 'geo_distance' => $geo_filter_ref };
            $facet_filters_ref->{'geo_distance'} = { 'geo_distance' => $geo_filter_ref };
        }
    }

    #job title
    FIELD:
    foreach my $query_field ( qw/advert_job_title employer_org_position_title employer_org_name/ ) {
        if (!$params->{$query_field}){
            next FIELD;
        }
        my $query_ref = _query_string(
            $query_field => $params->{$query_field}
        );

        push @query, $query_ref;

        # these text search type fields don't need to be part of the facet filter
        # it displeases the capricious temperemant of the elastic search gods
#        $facet_filters_ref->{$query_field} = $query_ref;
    }

    my $salary_from = $params->{'salary_from'} || $params->{'advert_salary_from'};
    my $salary_to = $params->{'salary_to'} || $params->{'advert_salary_to'};

    if ($salary_from || $salary_to){
        my ($annual_from, $annual_to, $daily_from, $daily_to, $weekly_from, $weekly_to) = Stream::TP2::Utils::normalise_salary(
            $params->{'salary_cur'},
            $salary_from || 0,
            $salary_to,
            $params->{'salary_per'}
        );
        $salary_from = $daily_from;
        $salary_to   = $daily_to;
    }

    #salary_from
    if ($salary_from){
        push @filters, { 'range' => { 'advert_daily_salary_from' => { from => $salary_from } } };
        $facet_filters_ref->{'advert_daily_salary_from'} = { 'range' => { 'advert_daily_salary_from' => { from => $salary_from } } };
    }

    #salary_to
    if ($salary_to){
        push @filters, { 'range' => { 'advert_daily_salary_to' => { to => $salary_to } } };
        $facet_filters_ref->{'advert_daily_salary_to'} = { 'range' => { 'advert_daily_salary_to' => { to => $salary_to } } };
    }


    if ( my $raw_ranges = $params->{applicant_availability_date} ) {
        my @ranges = ref($raw_ranges) ? @$raw_ranges : $raw_ranges;

        my @date_range_filters = Stream::TP2::AvailabilityDate::range_to_es(
            ref($raw_ranges) ? @$raw_ranges : $raw_ranges
        );

        my @date_filters = map {
            { range => { applicant_availability_date => $_ } }
        } @date_range_filters;

        if ( @date_filters ) {
            my $filter = @date_filters == 1 ? $date_filters[0] : { or => \@date_filters };
            push @filters, $filter;
            $facet_filters_ref->{'applicant_availability_date'} = $filter;
        }
    }

    #id_from
    if ($params->{'id_from'}){
        push @filters, { 'range' => { 'id' => { from => $params->{'id_from'} } } };
        $facet_filters_ref->{'id'} = { 'range' => { 'id' => { from => $params->{'id_from'} } } };
    }

    #application_time_from
    my $application_time_from = Stream::TP2::Search::_application_time_from($params->{'cv_updated_within'}) || $params->{applicant_application_time};
    if ($application_time_from) {
        push @filters, { 'numeric_range' => { 'applicant_application_time' => { from => $application_time_from } } };
        $facet_filters_ref->{'applicant_application_time'} = { 'numeric_range' => { 'applicant_application_time' => { from => $application_time_from } } };
    }

    if ( !@query ) {
        push @query, { 'match_all' => {} };
    }

    if (@query_filters){
        $query_ref->{'query'}->{'filtered'}->{'query'} = _must(@query);
        $query_ref->{'query'}->{'filtered'}->{filter}->{'and'} = \@query_filters;
    }
    else {
        $query_ref->{'query'} = _must(@query);
    }

    if (@filters){
        $query_ref->{'filter'}->{'and'} = \@filters;
    }

    if ($options_ref->{'include_facets'}){
        my %facets = build_facets($facet_filters_ref);

        if (%facets){
            $query_ref->{'facets'} = \%facets;
        }
    }
    elsif ( exists($params->{facets}) ) {
        $query_ref->{facets} = $params->{facets};
    }

    my $tp2 = Stream::TP2->new();

    my $search_url = _es_search_url();

    my $response = $tp2->json_get($search_url, $query_ref);

    if ( $DEBUG > 3 || $response->message ne 'OK' || $params->{dump} ){

        if ($options_ref->{'no_decode'}){
            return $response->{'api_response'};
        }

        die _pretty_dump([
            { params   => $params },
            { options  => $options_ref },
            { request  => $query_ref },
            { response => JSON::from_json($response->decoded_content)},
        ]);

        die $tp2->last_raw_response->request()->as_string() . "\n\n" . $response->as_string();
    }

    if ( $options_ref->{explain} && exists($INC{'Test/More.pm'}) ) {
        Test::More::diag(_pretty_dump($query_ref) );
    }

    return $response->{'api_response'} if $options_ref->{'no_decode'};

    my $es_ref = JSON::from_json($response->decoded_content);

    if ( $DEBUG > 2 || $options_ref->{explain} ) {
        $es_ref->{'header'} = $query_ref;
    }

    return es_to_solr($es_ref);
}

# HELPERS TO BUILD PARTS OF THE QUERY
sub _query_string {
    my $field = shift;
    my $query = shift;

    return {
        query_string => {
            default_field => $field,
            query         => _uc_boolean_operators($query),
        },
    };
}

sub _must {
    my @queries = @_;

    if ( @queries == 1 ) {
        return \@queries;
    }

    return {
        bool => {
            must => \@queries,
        }
    }
}

# turns out that boolean operators must be uppercase in a query_string query
sub _uc_boolean_operators {
    my $query = shift;

    $query =~ s/\band\b/AND/ig;
    $query =~ s/\bor\b/OR/ig;
    $query =~ s/\bnot\b/NOT/ig;

    return $query;
}

sub _pretty_dump {
    my $data = shift;
    return JSON::to_json($data, { pretty => 1 });
}

sub _es_search_url { Stream::TP2::Constants::elastic_search_url(); }

sub es_to_solr { #Hack the response from ES so it matches SOLR.
    my $es_ref = shift || {};
    my $solr_ref = {};

    my $facets_ref = $es_ref->{'facets'};

    $solr_ref->{'header'} = $es_ref->{'header'};

    if ($facets_ref){
        $solr_ref->{'facet_counts'} = { facet_ranges => 0 };

        my %facets = _es_facets();
        foreach my $facet (keys %facets){
            my $facet_ref = $facets_ref->{$facet};

            my @facet;
            if ($facet_ref->{'ranges'}){ #handle ranges differently.
                my $ranges_ref = $facet_ref->{'ranges'};

                foreach my $range_ref ( @$ranges_ref ) {
                    my $from = $range_ref->{from} || '';
                    my $to   = $range_ref->{to}   || '';
                    my $count = $range_ref->{count};
                    push @facet, $from . ".." . $to, $count;
                }
            } else {
                my $terms_ref = $facet_ref->{'terms'};
                foreach my $term ( @$terms_ref ){
                    push @facet, $term->{'term'}, $term->{'count'};
                }
            }

            $solr_ref->{'facet_counts'}->{'facet_fields'}->{$facet} = \@facet;
        }
    }

    $solr_ref->{'response'}->{'maxScore'} = eval { $es_ref->{'hits'}->{'max_score'} } || 0;

    my $hits_ref = $es_ref->{'hits'}->{'hits'}; my @docs;

    my %highlighting;
    if (ref($hits_ref) eq 'ARRAY'){
        foreach my $hit (@$hits_ref){
            my $source = $hit->{'_source'};

            if ($DEBUG > 1){
                my $score = $hit->{'_score'};
                $source->{'applicant_name'} = $score . ' - ' . $source->{'applicant_name'};
            }

            my $highlight_ref = $hit->{'highlight'};

            if ($highlight_ref){
                $highlighting{ $source->{'id'} } = $highlight_ref;
            }

            # populate the applicant_salary/advert_salary fields because
            # one day later and I still can't do it in tempo.js
            if ( $source->{applicant_salary_from} || $source->{applicant_salary_to} ) {
                $source->{applicant_salary} = sprintf("%s %s - %s per %s",
                    $source->{applicant_salary_currency} || 'GBP',
                    $source->{applicant_salary_from}     || 0,
                    $source->{applicant_salary_to}       || 0,
                    $source->{applicant_salary_per}      || 'week',
                );
            }
            elsif ( $source->{advert_salary_from} || $source->{advert_salary_to} ) {
                $source->{advert_salary} = sprintf("%s %s - %s per %s",
                    $source->{advert_salary_currency} || 'GBP',
                    $source->{advert_salary_from}     || 0,
                    $source->{advert_salary_to}       || 0,
                    $source->{advert_salary_per}      || 'week',
                );
            }

            push @docs, $source;
        }
    }

    $solr_ref->{'highlighting'} = \%highlighting;

    my $total = eval { $es_ref->{'hits'}->{'total'} } || scalar(@docs);
    $solr_ref->{'response'}->{'numFound'} = $total;
    $solr_ref->{'response'}->{'docs'} = \@docs;

    return $solr_ref;
}

sub build_facets {
    my $facet_filters_ref = shift || {};

    my %facets = _es_facets();

    foreach my $facet (keys %facets){

        #apply each filter except the one that affects that facet
        my %copy = %$facet_filters_ref;
        delete $copy{$facet}; #We don't want to filter on the facet itself

        if (scalar( keys %copy) ){
            my @filters;
            foreach my $filter (keys %copy){
                push @filters, $copy{$filter};
            }
            $facets{$facet}->{'facet_filter'}->{'and'} = \@filters;
        }
    }

    return %facets;
}

sub _es_facets {
    my @facet_ranges = _es_availability_date_facets();

    return (
        "applicant_tags" => {
            terms => { field => "applicant_tags", size => 2000 }, # All is consistent up to two thousand tags.
        },
        "advert_industry" => {
            terms => { field => "advert_industry", size => 30 },
        },
        "advert_job_type" => {
            terms => { field => "advert_job_type", size => 3 },
        },
        "applicant_availability_date" => {
            range => { field => "applicant_availability_date", ranges => [ \@facet_ranges ] },
        }
    );
}

sub _es_availability_date_facets {
    return Stream::TP2::AvailabilityDate::range_to_es(
        Stream::TP2::AvailabilityDate::availability_dates(),
    );
}

=head2 get_by_id

PURE function (not package/class method). Returns a single application (applicant document) as a solr
style response from artirix from a given ID and company (name).

Usage:

  Stream::TP2::ElasticsSearch::get_by_id($candidate_id , $company);

If the applicant is NOT found, will return an empty hashref. Returns something like that
in case of success:

  { response: {
                docs: [
                           { all sort of candidate attributes }
                      ]
               }

  }

=cut

sub get_by_id {
    my $id = shift || die "ID is mandatory";
    my $company = shift || die "Company is mandatory";

    $log->info("Getting ONE candidate (id '$id' ) for company '$company'");

    #http://www.elasticsearch.org/guide/reference/api/get.html
    my $tp2 = Stream::TP2->new();
    my $response = $tp2->json_get('https://es.bb.artirix.com/items/applicant/' . $id);

    my $es_ref = JSON::from_json($response->decoded_content);

    if ( $es_ref->{'exists'} ){
        my $solr_ref = {};
        my $applicant_ref = $es_ref->{'_source'};

        if ( !company_can_see_applicant($company, $applicant_ref->{client_id}) ) {
            $log->warn( "$id is not owned by $company" );
            return {};
        }

        $solr_ref->{'response'}->{'docs'} = [ $applicant_ref ];

        return $solr_ref;
    }

    confess("Invalid response:". $tp2->last_raw_response->request()->as_string() . "\n\n" . $response->as_string() );
}

# Checks if a company can see a client id
sub company_can_see_applicant {
    my $user_company = shift;
    my $applicant_client_id = shift;

    # This is a pure function. Need to access the user api
    # through the Stream2 instance.
    my $user_api = Stream2->instance()->user_api();

    # wildcard can see all companies
    # used in scripts when we know the candidate id but not the client id
    if ( $user_company eq '*' ) {
        return 1;
    }

    # compare the client_id to the company that the user is part of
    my $client_id = _tp2_client_id_from_company( $user_api,  $user_company )
        || die "No Client ID";

    if ( $client_id eq $applicant_client_id ) {
        return 1;
    }

    # if the client can see other accounts check that too
    if ( my ( $account_associate ) = $user_api->company_associations($user_company,'share') ) {
        if ( _tp2_client_id_from_company( $user_api , $account_associate) eq $applicant_client_id ) {
            return 1;
        }
    }

    # otherwise they don't have access
    return 0;
}

# Fiddle with the COMPANY ID with some hardcoded data
# and rule.
# THIS IS A PURE FUNCTION
sub _tp2_client_id_from_company{
    my ($user_api , $company_id) = @_;

    if ( $company_id eq 'alexmannjobs' ) {
        return 'amscont';
    }

    if ( $company_id eq 'amscont' ) {
        return 'ams_live';
    }

    if ( my ( $associate ) = $user_api->company_associations($company_id,'share') ){
        return $associate if $associate eq 'demo_data';
    }
    return $company_id;
}

sub _tags_to_filter {
    my @tags = @_;
    my @ands;
    foreach my $tag (@tags){
        ## Note that the tag is lc'ed. This is because artirix indexes the tags in a lower case way, but does not lower
        ## case the query itself.
        push @ands, { 'constant_score' => { 'filter' => { 'term' => { 'applicant_tags' => lc($tag) } }, boost => '2' } };
    }
    return wantarray ? @ands : \@ands;
}

1;

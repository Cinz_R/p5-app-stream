package Stream::TP2::Search;
use strict;
use warnings;

use Stream::TP2;
use Stream::TP2::ElasticSearch;
use Stream::TP2::Utils;
use Stream::TP2::Constants;
use List::MoreUtils qw(uniq);
use Time::HiRes;

our $VERBOSE = 0;

sub submit_search {
    my $company = shift;
    my $params = shift || {};
    my $options_ref = shift || {};

    return Stream::TP2::ElasticSearch::submit_search($company, $params, $options_ref);
}


# Circular dependency. This 'normalise_tags' is called from
# Stream::TP2::ElasticSearch ..

=head2 normalise_tags

PURE function. Just returns its arguments (any suite of values or array refs) split by ','.

Useful in case the GUI sends something like tag eq  a,b,c instead of  tag = [ qw/a b c/ ].

Usage:

  my @norm_tags = Stream::TP2::Search::normalise_tags('a', 'b' , 'c,d');
  # Norm tags is now (qw/a b c d/)

=cut

sub normalise_tags {
    my @params = map { ref($_) eq 'ARRAY' ? @$_ : $_ } @_;

    my @all_tags;

    PARAM:
    foreach my $param ( @params ) {
        if ( !$param ) {
            next PARAM;
        }
        $param =~ s/\,$//;
        push @all_tags, split(/\s*,\s*/, $param);
    }

    return uniq( grep { $_ } @all_tags );
}

sub get_facets {
    my $company = shift;

    require Stream::TP2::ImportantTags;

    my %facets = (
        _priority         => [qw(applicant_tags advert_job_type advert_industry applicant_availability_date)],
        advert_industry   => 'Industry',
        advert_job_type   => 'Job Type',
        applicant_tags    => Stream::TP2::ImportantTags->label($company),
        applicant_availability_date => "Availability Date",
    );

    return %facets;
}

sub get_facets_fqhandlers {
    my $facet = shift;
    my %handlers = (
        default => sub {facet_default_fqhandler($facet => @_)},
        applicant_availability_date => \&facet_avail_date_fqhandler
    );
    my $key = exists($handlers{$facet}) ? $facet : "default";
    return $handlers{$key};
}

sub facet_default_fqhandler {
    my $facet = shift;
    my (@selected) = @_;

    my $filter = join " OR ", map { qq{$facet:"$_"} } @selected;

    my @params = (
        fq => "{!tag=${facet}_filter}" . $filter,
    );

    if ( $facet eq 'applicant_tags' ) {
        push @params, bq => $filter;
    }

    return @params;
}

sub facet_avail_date_fqhandler {
    my @ranges = @_;
    my @fq;
    foreach my $range(@ranges) {
        ##push @fq, fq => sprintf "%s:[%s TO %s]", "{!tag=applicant_availability_date_filter}applicant_availability_date", map { /^(\*|NOW|PASS)$/ ? $_ : qq{"$_"} } @$range;
        push @fq, fq => sprintf "%s:[%s TO %s]", applicant_availability_date => map { /^(\*|NOW|PASS)$/ ? $_ : qq{"$_"} } @$range;
    }
    return @fq;
}

sub facet_avail_date_fqhandler_for_stream {
    return fq => sprintf "%s:[%s TO %s]", applicant_availability_date => map { /^(\*|NOW|PASS)$/ ? $_ : qq{"$_"} } @_;
}

sub _debug_search {
    if ( !$VERBOSE ) {
        return;
    }

    my $response = shift;

    open(TMP, '>> /tmp/artirix_log');
    print TMP scalar(localtime()) . "\n";

    if ( UNIVERSAL::can($response, 'request') ) {
        print TMP $response->request->as_string();
        if ( $VERBOSE > 1 ) {
            print TMP $response->as_string();
        }
    }
    else {
        print TMP $response . "\n";
    }
    print TMP "=================\n\n";
    close TMP;
}

sub _artirix_url {
    return Stream::TP2::Constants::default_search_url();
}

sub _fields_to_return {
    return qw/client_id applicant_name employer_org_name applicant_application_time employer_org_position_end_date id employer_org_position_title applicant_address applicant_tags cv_text applicant_email applicant_telephone employer_org_position_start_date advert_job_title advert_id advert_consultant applicant_rank advert_industry advert_job_type applicant_latitude applicant_longitude attachment_url salary_from salary_to applicant_salary_from applicant_salary_to applicant_salary_per applicant_salary_currency applicant_availability_date score/;
}

sub _application_time_from {
    my $cv_updated_within = shift;

    my $days_to_minus = 0;
    if ( !$cv_updated_within ||  $cv_updated_within eq 'ALL' ){
        return;
    } elsif ( $cv_updated_within eq 'TODAY' ){
        $days_to_minus = 1;
    } elsif ( $cv_updated_within eq 'YESTERDAY' ) {
        $days_to_minus = 2;
    }
    elsif ($cv_updated_within eq '3D'){
        $days_to_minus = 3;
    }
    elsif ($cv_updated_within eq '1W'){
        $days_to_minus = 7;
    }
    elsif ($cv_updated_within eq '2W'){
        $days_to_minus = 14;
    }
    elsif ($cv_updated_within eq '1M'){
        $days_to_minus = 30;
    }
    elsif ($cv_updated_within eq '2M'){
        $days_to_minus = 60;
    }
    elsif ($cv_updated_within eq '3M'){
        $days_to_minus = 90;
    }
    elsif ($cv_updated_within eq '6M'){
        $days_to_minus = 180;
    }
    elsif($cv_updated_within eq '1Y'){
        $days_to_minus = 365;
    }
    elsif($cv_updated_within eq '2Y'){
        $days_to_minus = 730;
    }
    elsif($cv_updated_within eq '3Y'){
        $days_to_minus = 1095;
    }

    require DateTime;
    my ($date) = DateTime->now;
    $date->subtract( days => $days_to_minus );

    # truncate to the nearest hour to increase the chances that
    # we hit the ES filter cache
    $date->truncate( to => 'hour' );

    return $date->iso8601() . 'Z';
}

1;

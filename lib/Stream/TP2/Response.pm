package Stream::TP2::Response;

use strict;
use warnings;

use JSON;

sub new {
    my $class = shift;
    my $api_response = shift;

    return bless { api_response => $api_response }, $class;
}

sub success {
    my $self = shift;
    my $resp = $self->{api_response};

    if ( !$resp->is_success() ) {
        return 0;
    }

    my $content = $resp->decoded_content();

    my $return_ref = JSON::from_json( $content );
    return $return_ref->{success};
}

sub as_string {
    return $_[0]->{api_response}->as_string();
}

sub message {
    return $_[0]->{api_response}->message();
}

sub decoded_content {
    return $_[0]->{'api_response'}->decoded_content();
}

# returns a list containing the callback ids so you can monitor them if you wish
sub callbacks {
    my $self = shift;
    my $resp = $self->{api_response};
    my $req  = $resp->request();

    my $last_json = $req->content();
    my $json_ref = eval { JSON::from_json($last_json); };
    if ( $@ ) {
        warn "Unable to parse json: $@\n";
        warn "Content was: $last_json\n";
        return;
    }

    if ( !$json_ref || ref($json_ref) ne 'ARRAY' ) {
        return;
    }

    require Stream::TP2::Callbacks;

    my @callbacks;
    DOC:
    foreach my $doc_ref ( @$json_ref ) {
        if ( !exists($doc_ref->{document}) ) {
            next DOC;
        }

        my $root_ref = $doc_ref->{document};
        if ( !exists($root_ref->{callbacks}) ) {
            next DOC;
        }
        my $callbacks_ref = $root_ref->{callbacks};
        foreach my $callback_url ( values %$callbacks_ref ) {
            my $id = Stream::TP2::Callbacks->url_to_id($callback_url);
            if ( $id ) {
                push @callbacks, $id;
                next DOC;
            }
        }
    }

    return @callbacks;
}

1;

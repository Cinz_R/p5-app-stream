package Stream::TP2::AvailabilityDate;

use strict;
use warnings;

use DateTime;
use DateTime::Format::ISO8601;
use Memoize;

memoize('_availability_dates');

sub availability_dates {
    # TODO: localise so today is the start of their day not ours
    return _availability_dates(DateTime->today());
}

# Memoized for the day
sub _availability_dates {
    my $today_start = shift;
    my $today_end   = $today_start->clone()->add( days => 1 );

    my @facets;
    push @facets, [ Past  => _to_range('' => $today_start) ];
    push @facets, [ Today => _to_range($today_start, $today_end) ];

    my @ranges = (
        # Label          => today + clone arguments to get range end
        ['Tomorrow'      => { days   => 1 }],
        ['2 days'        => { days   => 2 }],
        ['3 days'        => { days   => 3 }],
        ['4 days'        => { days   => 4 }],
        ['5 ~ 7 days'    => { days   => 7 }],
        ['1 ~ 2 weeks'   => { days   => 14 }],
        ['2 ~ 3 weeks'   => { days   => 21 }],
        ['3 ~ 4 weeks'   => { months => 1 }],
        ['1 ~ 2 months'  => { months => 2 }],
        ['2 ~ 3 months'  => { months => 3 }],
        ['3 ~ 4 months'  => { months => 4 }],
        ['4 ~ 5 months'  => { months => 5 }],
        ['5 ~ 6 months'  => { months => 6 }],
        ['6 ~ 12 months' => { years  => 1 }],
        ['1 ~ 2 years'   => { years  => 2 }],
    );

    my $last_range_end = $today_end;
    foreach my $range_ref ( @ranges ) {
        my $label = $range_ref->[0];
        my $range_end_delta_ref = $range_ref->[1];

        # NB. adding 1 month + 1 day is different to adding 1 day + 1 month
        # when it is the last day of the month
        my $range_end = $today_start->clone()->add(%$range_end_delta_ref)->add( days => 1 );

        push @facets, [ $label => _to_range($last_range_end => $range_end) ];

        $last_range_end = $range_end;
    }

    push @facets, [ '2 years +' => _to_range($last_range_end => '') ];

    return @facets;
}

sub _to_range {
    my $start = shift || '';
    my $end   = shift || '';

    # prefer epochs to iso8601 because it makes it easy to look up
    # the facet label later
    if ( $start && UNIVERSAL::can($start,'epoch') ) {
        $start = $start->epoch() * 1000; # milliseconds since UTC
    }

    if ( $end && UNIVERSAL::can($end,'epoch') ) {
        $end = $end->epoch() * 1000;
    }

    return join('..', $start, $end);
}

sub range_to_es {
    my @filters;

    RANGE:
    foreach my $range ( @_ ) {
        # allow facets [ Label => range ] to be passed in directly
        # for convenience
        $range = ref($range) ? $range->[1] : $range;

        my ($start, $end) = split /\.\./, $range;

        if ( !$start && !$end ) {
            next RANGE;
        }

        # FYI, range facets only supports from & to, they do not support
        # options from the range filter like lt, gte, include_lower etc
        # also note that the range facet includes the 'from' value
        # but excludes the 'to' value unlike the range query
        # good news for me because that is what I want!
        # source: http://elasticsearch-users.115913.n3.nabble.com/Lower-and-upper-bound-of-range-facet-is-not-consistent-with-RangeQuery-td3119848.html
        my %filter = (
            # specify include_lower & include_upper in case we are doing a
            # range filter. this ensures the range filter behaves the same
            # as a range facet. The range facet currently ignores these options
            include_lower => 1,
            include_upper => 0,
        );
        if ( $start ) {
            $filter{from} = $start;
        }
        if ( $end ) {
            $filter{to} = $end;
        }

        push @filters, \%filter;
    }

    return @filters;
}

sub range_to_label {
    my $option = shift;

    foreach my $facet_ref ( availability_dates() ) {
        my $label = $facet_ref->[0];
        my $range = $facet_ref->[1];

        if ( $range eq $option ) {
            return $label;
        }
    }

    return $option;
}

sub display_date {
    my $date = shift
        or return;
    my $username = shift; # optional, will help us localise later

    my $dt = DateTime::Format::ISO8601->parse_datetime($date);

    # TODO: later update to Bean::Locale::L(....)
    return $dt->strftime('%d %b %Y');
}

1;

package Stream::TP2::Constants;

use strict;
use warnings;

use base qw(Exporter);

our @EXPORT = ();
our @EXPORT_OK = qw(default_search_url default_api_url auth_username auth_password notice_period_labels);

sub default_search_url {
    return 'https://bbsearch.artirix.com/solr/applicants/edismax/';
}

sub elastic_search_url {
    return 'https://es.bb.artirix.com/items/applicant/_search';
}

sub default_api_url {
    return 'https://bbapi.artirix.com/api/add_document';
}

sub default_api_b_url {
    return 'https://bbapi.artirix.com/api_b/add_document';
}

sub auth_username { 'broadbean' }
sub auth_password { 'wor2lahT'  }

sub notice_period_labels {
    my %labels;
    $labels{1} = "Immediate";
    $labels{2} = "1 week";
    $labels{3} = "2 weeks";
    $labels{4} = "4 weeks";
    $labels{5} = "6 weeks";
    $labels{6} = "8 weeks";
    $labels{7} = "16 weeks";
    $labels{8} = "3 months+";
    return \%labels;
}

sub advert_per_field {
    my $per  = shift;
    my $range = shift; ## Either "from" or "to"
    if ($per eq "year" || $per eq "annum" || $per eq "annual" || $per eq "yearly") {
        return "advert_annual_salary_$range";
    }
    elsif ($per eq "week" || $per eq "weekly") {
        return "advert_weekly_salary_$range";
    }
    elsif ($per eq "day" || $per eq "daily") {
        return "advert_daily_salary_$range";
    }
    else {
        die "Unknown Per";
    }
}

1;

package Stream::Engine::Composition;

=head1 NAME

Stream::Engine::Composition

=head1 description

A L<Stream::Engine> based on Composition

=cut


use base 'Stream::Engine';
use Carp;

use WWW::Mechanize;
use Stream2::Feeds::Scraper;
use Stream2::Feeds::FacetList;
use Stream2::Feeds::Facet::Text;

=head2 _stream2

Returns the Stream2 instance

=cut

sub _stream2 {
    my ($self) = @_;
    return $self->user_object->stream2;
}

=head1 Entrypoints

=head2 $feed->search($options)

Set $feed->results first.

=cut

sub search {
    my ($self, $options) = @_;
    $options ||= {};

    my $results = $self->results;
    $options->{results} = $results;

    $self->_search($options);
}

=head2 download_profile($candidate);

=cut

sub download_profile {
    my ($self, @args) = @_;
    $self->_download_profile(@args);
}

=head2 download_cv($candidate);

=cut

sub download_cv {
    my ($self, $candidate) = @_;
    $self->_download_cv($candidate);
}

=head1 Internal API methods

=head2 _search($request)

    my $results = $feed->_search($request);

=cut

sub _search {
    ...
}

=head2 _download_profile($candidate)

    my $candidate = $feed->_download_profile($candidate);

=cut

sub _download_profile {
    ...
}

=head2 _download_cv($candidate)

    my $candidate = $feed->_download_profile($candidate);

=cut

sub _download_cv {
    ...
}

=head2 mech

Our logged L<WWW::Mechanize> instance.

=cut

sub mech {
    my ($self) = @_;
    return $self->{mech} //= $self->_new_mech;
}

sub _new_mech {
    my ($self) = @_;
    my $mech = WWW::Mechanize->new(autocheck => 0);
    $mech->add_handler(
        response_done => sub {
            my($response, $ua, $h) = @_;
            if(!$response->is_success) {
                my $error = $self->lwp_error($response, $ua);
                confess($error) if $error;
            }
            return;
        }
    );
    $self->log_lwp_ua($mech);
    return $mech;
}

# Overwrite to handle unsuccessful lwp requests
sub lwp_error {
    my ($self, $response, $ua) = @_;
    return;
}
=head2 scraper

A L<Stream2::Feeds::Scraper> instance.

=cut

sub scraper {
    my ($self) = @_;
    return $self->{scraper} //= $self->_new_scraper;
}

sub _new_scraper {
    my ($self) = @_;
    my $scraper = new Stream2::Feeds::Scraper;
    return $scraper;
}

sub search_begin_search {
    my ($self, @args) = @_;
    return $self->search(@args);
}


=head2 facets

A L<Stream2::Feeds::FacetList> of the feed instance's facets

    my $facet_list = $feed->facets;

    my $experience_facet = $facet_list->Get('experience');

=cut

sub facets {
    my ($self) = @_;
    return $self->{facets} //= $self->_initial_facets;
}

sub _initial_facets {
    return Stream2::Feeds::FacetList->new();
}

sub stream2 {
    my ($self) = @_;
    return $self->user_object->stream2;
}
=head2 standard_tokens

Legacy feed compatibility mimicking L<Stream::Templates::Default>

    my %tokens = $feed->standard_tokens

=cut

sub standard_tokens {
    my ($self) = @_;
    return $self->facets->to_standard_tokens();
}

=head2 tokens value


Legacy feed compatibility mimicking L<Stream::Templates::Default>

    my %tokens = (experience => 3, education => 7);
    while(my ($key, $value)) = each %tokens) {
        $feed->token_value($key => $value);
    }

=cut

sub token_value {
    my ($self, $key, $value) = @_;
    my $prefix = $self->destination . '_';
    $key =~ s/^\Q$prefix\E//g;
    my $facet = $self->facets->get($key);
    if(!$facet) {
        return;
    }
    if(scalar(@_) == 2) {
        return $facet->value;
    }
    $facet->set_token($value);
}

1;

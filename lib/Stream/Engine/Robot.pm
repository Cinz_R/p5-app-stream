#!/usr/bin/perl

# $Id: Robot.pm 92383 2013-01-31 11:47:10Z robert.marlton $

package Stream::Engine::Robot;

use strict;

use base qw(Stream::Engine);

use Stream::Agents::Mechanize;
use URI;

our $VERSION = qq(0.01);

sub VERSION {
    return $VERSION;
}

sub AGENT_CLASS {
    return 'Stream::Agents::Mechanize';
}

################## LOGIN OPTIONS #########################
sub login_success {}
sub login_failed {}
sub login_url {} # used to be called login_page
sub login_form_name {}
sub login_fields {}
sub login_fill_out_form {}
sub login_post {}
sub login_form_with_fields {}
sub http_post {
    my $self = shift;
    my $url  = shift;
    my $post_params_ref = shift;

    $self->log_debug( 'doing a http post to %s', $url );

    if ( my $ref_type = ref($post_params_ref) ) {
        my @unprocessed_params = $ref_type eq 'HASH' ? %$post_params_ref : @$post_params_ref;

        my @processed_params = $self->field_value_pairs( @unprocessed_params );
        return $self->post( $url, \@processed_params );
    }

    return $self->post( $url, $post_params_ref );
}

sub login {
    my $self = shift;

    $self->log_mark('logging in');

    if ( $self->logged_in() ) {
        $self->log_debug('We are logged in already');
        return;
    }

    my $login_page = $self->login_url();

    if ( !defined( $login_page ) ) {
        $self->log_info( 'no login page' );
        $self->{logged_in} = 1;
        return 1;
    }

    $self->log_info( 'logging in' );

    my $login_post_ref = $self->login_post();

    if ( $login_post_ref ) {
        $self->http_post( $login_page, $login_post_ref );
    }
    else {
        $self->log_debug( 'fetching %s', $login_page );

        $self->get( $login_page );

        if ( my $form = $self->login_form_name() ) {
            $self->log_debug( 'found login form called %s', $form );
            $self->form_name( $form );
        }

        if ( my (@login_form_fields) = $self->login_form_with_fields() ){
            if (scalar(@login_form_fields)){
                $self->log_debug( 'Trying to find form based on fields' );
                $self->form_with_fields(@login_form_fields); 
            }
        }

        if ( my (%login_fields) = $self->login_fields() ) {
            $self->fill_form( %login_fields );
        }

        $self->login_fill_out_form();

        $self->log_info('Submitting login page');
        $self->submit();
    }

    $self->check_for_login_error();

    $self->log_info( 'login successful' );

    $self->{logged_in} = 1;

    return 1;
}

# takes a hash like: {
#        field       => 'value',
#        multiselect => ['value1','value2','value3','%token1%']
# }
# and fills in the form
sub fill_form {
    my $self = shift;
    my %fields = @_;

    FIELD:
    while ( my ( $field, $values_ref ) = each %fields ) {
        my @values = ref( $values_ref ) ? @$values_ref : $values_ref;

        $self->log_debug( 'filling in field: %s', $field );

        my @processed_values;
        foreach my $value ( @values ) {
            if ( $value =~ m/^%(.+)%$/ ) {
                my $token = $1;

                $self->log_debug( 'value [%s] will be populated from token [%s]', $value, $token );

                my @token_values = $self->token_value( $token );

                if ( @token_values && defined($token_values[0]) ) {
                    $self->log_debug( 'token [%s] = [%s]',  $token, (join '|', @token_values) );
                    push @processed_values, @token_values;
                }
                else {
                    $self->log_debug( 'token [%s] is not defined', $token );
                }
            }
            else {
                push @processed_values, $value;
            }
        }

        if ( !defined($processed_values[0]) ) {
            $self->log_debug( 'NOT setting %s (reason is no value: %s)', $field, \@values );
            next FIELD;
        }


        $self->log_debug( 'setting %s => %s', $field, \@processed_values );
        $self->field( $field, \@processed_values );
    }
}

sub field_value {
    my $self = shift;
    my $field = shift;
    my $number = shift;

    my $mech = $self->agent();

    return $mech->value( $field, $number );
}

# similar to mech's field EXCEPT we will create the field if we need to
sub field {
    my $self = shift;
    my $field = shift;
    my $values_ref = shift;
    my $number = shift;

    if ( $self->set_field( $field, $values_ref, $number ) ) {
        return;
    }

    return $self->create_field( $field, $values_ref );
}

sub set_field {
    my $self = shift;
    my $field = shift;
    my $values_ref = shift;
    my $number = shift;

    my $mech = $self->agent();

    # attempt to set the field (this doesn't work for submit buttons though)
    eval {
        local $Carp::CarpLevel = 1;
        $mech->field( $field, $values_ref, $number );
    };

    if ( $@ ) {
        my $error = $@;
        $self->log_warn('Field %s does not exist in the form so we will create it (error was %s)', $field, $error );

        return 0;
    }

    # handle setting the value of submit buttons
    my $form = $mech->current_form();
    if ( !$form ) {
        return 0;
    }

    my $input = $form->find_input( $field, undef, $number );
    if ( !$input ) {
        return 0;
    }

    if ( $input->type() eq 'submit' ) {
        if ( $values_ref ) {
            # if there is a value, click the button
            $input->{clicked} = [1,1];
        }
        else {
            # otherwise make sure the button is not clicked
            delete $input->{clicked};
        }
    }

    # handle iffy behaviour with checkboxes sending the value when they shouldn't
    if ( $input->type() eq 'checkbox' ) {
        my $got_value = $values_ref;
        if ( ref( $values_ref ) eq 'ARRAY' ) {
            if ( scalar(@$values_ref) == 1 && !$values_ref->[0] ) {
                $got_value = 0;
            }
        }
        if ( $got_value ) {
            # if there is a value, HTML::Form & mech should handle it
        }
        else {
            $self->log_debug('Unticking checkbox because it is empty [%s]', $field );
            # otherwise make sure the checkbox is not selected
            $input->value( undef );
        }
    }

    return 1;
}

sub select_to_multi_select {
    my $self = shift;
    my $field = shift;
    my $number = shift;

    $self->log_debug('converting %s to a multi-select', $field);

    my $input = $self->find_input( $field, $number );

    if ( !$input ) {
        $self->log_warn('cannot convert %s to a multi-select: unable to field in form', $field);
        return;
    }

    if ( $input->type() ne 'option' ) {
        $self->log_warn(
            'cannot convert %s to a multi-select: field is a %s not an select input', $field, $input->type(),
        );
        return;
    }

    if ( $input->{multiple} ) {
        $self->log_warn(
            '%s is already a multi-select', $field,
        );

        return 1;
    }

    # gets really ugly here
    # HTML::Form is great for reading forms. it isn't suitable for manipulating them
    my $form = $self->current_form();
    _replace_select_with_multi_select( $form, $input );

    return 1;
}

# html::form stores a select as a single object with lots of options
# it stores a multi-select as many objects, each representing a single option
sub _replace_select_with_multi_select {
    my $form = shift;
    my $select = shift;

    my @inputs;

    INPUT:
    foreach my $input ( $form->inputs() ) {
        if ( $input->type() eq $select->type()
            && $input->name() eq $select->name()
            && $input->dump() eq $select->dump() ) {
            my @multi_select = _select_to_multi_select( $select )
                or next INPUT;

            push @inputs, @multi_select;
        }
        else {
            push @inputs, $input;
        }
    }

    $form->{inputs} = \@inputs;

    return $form;
}

sub _select_to_multi_select {
    my $select = shift;

    my %new_select = (
        %$select,
        type => 'option',
        multiple => 1,
    );

    delete $new_select{idx};
    delete $new_select{current};

    my $menu_ref = delete $new_select{menu};
    my $current_val = delete $new_select{current};

    my @options;
    foreach my $option_ref ( @$menu_ref ) {
        my $value = $option_ref->{value};
        my $name  = $option_ref->{name};
        my $new_option = HTML::Form::ListInput->new(
            %new_select,
            value => $value,
            value_name => $name,
            option_selected => $current_val eq $value,
        );
        $new_option->fixup();
        push @options, $new_option;
    }

    return @options;
}

sub create_field {
    my $self = shift;
    my $field = shift;
    my $values_ref = shift;

    $self->log_debug('Creating field %s', $field );

    if ( !ref( $values_ref ) ) {
        $values_ref = [ $values_ref ];
    }

    my $form = $self->current_form();
    foreach my $value ( @$values_ref ) {
        $form->push_input(
            'text', {
                name  => $field,
                value => $value,
            },
        );
    }

    return;
}

################## SEARCH #########################

sub search_form_name {}
sub search_fill_out_form {}
sub search_fields {}
sub search_success {}
sub search_button {}
sub search_failed {}
sub search_find_form {}
sub search_url {}
sub search_navigate_to_page {}
sub search_fixup {}

# sends the search information to the board
# normal route is:
#  1) we have logged into the board
#  2) the page contains a html form
#  3) we fill in the html form and hit submit
#
# but the code branches
sub search_submit {
    my $self = shift;

    # if there are multiple forms on the page, you can specify which form you want
    if ( my $form = $self->search_form_name() ) {
        if ( $self->form_name( $form ) ) {
            $self->log_debug( 'found search form called [%s]', $form );
        }
        else {
            $self->log_warn( 'Could not find a form called [%s]', $form );
        }
    }

    # or you can supply your own routine to find the form
    $self->search_find_form();

    if ( my (%search_fields) = $self->search_fields() ) {
        # if you specify a search_request_method (ie. the HTTP verb we should use)
        # then stream will assume you know what you are doing and will ignore the current form
        # and submit the search_fields() to the search_url()
        my $request_type = $self->can('search_request_method') && $self->search_request_method();

        $self->log_debug("Request method is [%s]", $request_type);
        if ( !$self->current_form() || $request_type ) {
            # no form to submit so attempt a http post instead
            my $search_uri = URI->new($self->search_url());
            
            $self->log_debug("Either there is no search form or we have a request type so we will just http $request_type the information to [%s]", $search_uri);

            if(uc($request_type) eq 'GET'){
                foreach my $key ( keys %search_fields ){
                    $search_fields{ $key } = $self->fill_in_tokens( $search_fields{ $key } );
                }
                $search_uri->query_form( \%search_fields );
                return $self->get( $search_uri );
            }

            return $self->http_post(
                $search_uri,
                \%search_fields,
            );
        }
        # fill in the form (this will fill in tokens)

        $self->log_debug('No request method and we have a form so we will fill it in');
        $self->fill_form( %search_fields );
    }

    # or you can supply your own routine to fill in the form
    $self->search_fill_out_form();

    # submit the form by clicking a button
    if ( my ($submit_button) = $self->search_button() ) {
        $self->log_debug( 'clicking the search button [%s]', $submit_button );
        $self->click( $submit_button );
    }
    else {
        # or just submitting the form as it is
        $self->log_debug( 'making the search request (no submit button found)' );
        $self->submit();
    }
}

sub search_begin_search {
    my $self = shift;

    $self->log_mark('Beginning search');

    $self->current_page( 1 );

    $self->log_debug( 'Clicking through to CV search page if needed' );

    # find the search page (may not be neccessary)
    $self->search_navigate_to_page();

    # send our search to the channel
    $self->search_submit();

    # call a hook which lets us extract things like sessions from the search response
    $self->search_fixup();

    # check that our search was okay
    my $search_resp = $self->response();

    my $decoded_content = $search_resp->decoded_content();

    my $search_success = $self->search_success();
    if ( defined( $search_success ) ) {
        $self->log_debug('Checking for success message [%s]', $search_success);
        if ( $decoded_content !~ m/$search_success/ ) {
            $self->log_debug('Did not find the success message');
            $self->throw_error('Unable to search');
        }
        else {
            $self->log_debug('Found a success message');
        }
    }

    my $search_failed = $self->search_failed();
    if ( defined( $search_failed ) ) {
        $self->log_debug('Checking for failure message [%s]', $search_failed);
        if ( $decoded_content =~ m/$search_failed/ ) {
            $self->log_debug('Found failure message - this search failed');
            $self->throw_error('Unable to search');
        }
        else {
            $self->log_debug('Did not find the failure message');
        }
    }

    # store the content so we can scrape it
    $self->recent_content( $decoded_content );

    # scrape the results page for candidates
    $self->log_mark('Scraping the first page');
    $self->scrape_page();

    # identify the total number of results
    $self->log_debug('Scraping for the number of results');
    $self->scrape_number_of_results();

    # identify the total number of pages
    $self->log_debug('Scraping paginator');
    $self->scrape_paginator();

    # identify the link to the next page (this could be a form!)
    # TODO: add if/when needed
    $self->log_debug('Determining next page');
    $self->search_determine_next_page();

    return 1;
}

sub search_specific_page_search {
    my $self = shift;
    $self->log_debug( 'Clicking through to CV search page if needed' );

     # find the search page (may not be neccessary)
    $self->search_navigate_to_page();

    # send our search to the channel
    $self->search_submit();

    # call a hook which lets us extract things like sessions from the search response
    $self->search_fixup();

    # check that our search was okay
    my $search_resp = $self->response();
    my $decoded_content = $search_resp->decoded_content();

    my $search_success = $self->search_success();
    if ( defined( $search_success ) ) {
        $self->log_debug('Checking for success message [%s]', $search_success);
        if ( $decoded_content !~ m/$search_success/ ) {
            if (!$self->{tried_login} && defined($self->stored_cookie_time())) {
                $self->force_login();
                return $self->search_begin_search();
            }
            $self->log_debug('Did not find the success message');
            $self->throw_error('Unable to search');
        }
        else {
            $self->log_debug('Found a success message');
        }
    }

    my $search_failed = $self->search_failed();
    if ( defined( $search_failed ) ) {
        $self->log_debug('Checking for failure message [%s]', $search_failed);
        if ( $decoded_content =~ m/$search_failed/ ) {
            if (!$self->{tried_login} && defined($self->stored_cookie_time())) {
                $self->force_login();
                return $self->search_begin_search();
            }
            $self->log_debug('Found failure message - this search failed');
            $self->throw_error('Unable to search');
        }
        else {
            $self->log_debug('Did not find the failure message');
        }
    }

    $self->recent_content( $decoded_content );
}

sub search_specific_page {
    my $self = shift;
    my $specific_page = shift
        or $self->throw_error('No page given for specific page search');

    $self->log_mark('Beginning search at page %s', $specific_page);

    $self->current_page( $specific_page );

    $self->search_specific_page_search();

    #now we have completed the search, lets get to the page we need
    $self->search_navigate_to_specific_page( $specific_page );

    if ( $self->current_page != $specific_page ) {
        $self->throw_error('Unable to navigate to specified page');
    }

    my $search_resp = $self->response();
    my $decoded_content = $search_resp->decoded_content();

    # store the content so we can scrape it
    $self->recent_content( $decoded_content );

    # scrape the results page for candidates
    $self->log_mark('Scraping the first page');
    $self->scrape_page();

    # identify the total number of results
    $self->log_debug('Scraping for the number of results');
    $self->scrape_number_of_results();

    # identify the total number of pages
    $self->log_debug('Scraping paginator');
    $self->scrape_paginator();

    # identify the link to the next page (this could be a form!)
    # TODO: add if/when needed
    $self->log_debug('Determining next page');
    $self->search_determine_next_page();

    return 1;
}


=head2 search_determine_specific_page

Should return a URL pointing to the given page, or undef if no
such thing can be found on the page.

Override that per feed.

Usage:

  if( my $specific_page_url = $this->search_determine_specific_page(4) ){
    ...
  }

=cut

sub search_determine_specific_page{
    # As of 2014-09-04, this was never implemented.
    return undef;
}

sub search_navigate_to_specific_page {
    my $self = shift;
    my $specific_page = shift;

    $self->log_info( 'Navigating to specific page: %s', $specific_page );
    if ( my $specific_page_url = $self->search_determine_specific_page( $specific_page ) ) {
        # got a link
        $self->log_debug('specific page link is: %s', $specific_page_url);
        my $resp = $self->get( $specific_page_url );
        $self->recent_content( $resp->decoded_content );
        $self->current_page( $specific_page );
    }
    else {
        while ( $self->current_page < $specific_page ){
            $self->search_determine_next_page();

            if ( my $next_page = $self->next_page_link() ) {
                # got a link
                $self->log_debug('next page link is: %s', $next_page);
                my $resp = $self->get( $next_page );
                $self->recent_content( $resp->decoded_content );
            }
            elsif ( my $next_page_button = $self->search_next_page_button() ) {
                # click a button to go to the next page

                # if there are multiple forms on the page, you can specify which form you want
                if ( my $form = $self->search_form_name() ) {
                    if ( $self->form_name( $form ) ) {
                        $self->log_debug( 'found search form called [%s]', $form );
                    }
                    else {
                        $self->log_warn( 'Could not find a form called [%s]', $form );
                    }
                }

                if ( !$self->find_input( $next_page_button ) ) {
                    # make sure there is a button to click
                    $self->log_debug('There is no button on this page called [%s] so we cannot view the next page', $next_page_button);
                    return 0;
                }

                $self->log_debug('next page button is: %s', $next_page_button);
                $self->click( $next_page_button );
            }
            elsif ( $self->search_navigate_to_next_page() ) {
                # assume it does it for us
                $self->log_debug('navigated to the next page');
            }
            else {
                $self->log_debug('unable to find or there is no next page');
                return 0;
            }
            $self->{current_page}++;
        }
    }
}

sub content {
    my $self = shift;

    my $response = $self->response() or return undef;
    return $response->decoded_content();
}

sub next_page_link {
    return delete $_[0]->{next_page} if @_ == 1;

    return $_[0]->{next_page} = ref($_[1]) ? $_[1]->url() : $_[1];
}

sub search_next_page {
    my $self = shift;

    $self->{current_page}++;

    $self->log_mark('Searching page %s', $self->current_page() );

    if ( my $next_page = $self->next_page_link() ) {
        # got a link
        $self->log_debug('next page link is: %s', $next_page);
        $self->get( $next_page );
    }
    elsif ( my $next_page_button = $self->search_next_page_button() ) {
        # click a button to go to the next page

        # if there are multiple forms on the page, you can specify which form you want
        if ( my $form = $self->search_form_name() ) {
            if ( $self->form_name( $form ) ) {
                $self->log_debug( 'found search form called [%s]', $form );
            }
            else {
                $self->log_warn( 'Could not find a form called [%s]', $form );
            }
        }

        if ( !$self->find_input( $next_page_button ) ) {
            # make sure there is a button to click
            $self->log_debug('There is no button on this page called [%s] so we cannot view the next page', $next_page_button);
            return 0;
        }

        $self->log_debug('next page button is: %s', $next_page_button);
        $self->click( $next_page_button );
    }
    elsif ( $self->search_navigate_to_next_page() ) {
        # assume it does it for us
        $self->log_debug('navigated to the next page');
    }
    else {
        $self->log_debug('unable to find or there is no next page');
        return 0;
    }

    my $resp = $self->response();

    $self->recent_content( $resp->decoded_content() );
    # parse new candidates

    $self->log_mark('Scraping page %s', $self->current_page() );
    $self->scrape_page();

    # identify the link to the next page (this could be a form!)
    $self->log_debug('determining next page');
    $self->search_determine_next_page();

    return 1;
}

sub search_navigate_to_next_page {}
sub search_next_page_button      {}

sub search_determine_next_page {
    my $self = shift;

    my (@find_link_options) = $self->search_find_next_page_link();

    if ( !@find_link_options ) {
        $self->log_debug('NB. no options to automatically find the next page');
        return;
    }

    my $mech = $self->agent();
    my $link = $mech->find_link(
        @find_link_options,
    );

    if ( $link ) {
        $self->log_debug('Next page link found: [%s]', $link->url() );
        return $self->next_page_link( $link );
    }

    $self->log_debug('Unable to find the next page link');

    return;
}

sub search_find_next_page_link {
# returns options that are used to find the next page link
# see WWW::Mechanize's find_link method for all valid options
# e.g. return ( text_regex => qr/^Next/, class_regex => qr/\bnext_page\b/ );
}


################## DOWNLOAD CV ####################
sub download_base_url {}

sub do_download_cv {
    my $self = shift;
    my $candidate = shift;

    $self->login() if $self->download_cv_requires_login();

    my $resp = $self->download_cv_make_request( $candidate );

    $self->check_for_success_or_failure(
        $self->download_cv_success() || undef,
        $self->download_cv_failed()  || undef,
    );

    if ( $resp->is_success() && $resp->decoded_content() ) {
        return $resp;
    }

    return $self->throw_error('Downloading CVs not available');
}

sub download_cv_make_request {
    my $self = shift;
    my $candidate = shift;

    my $download_link = $self->download_cv_link_to_cv($candidate);
    if ( !$download_link ) {
        $self->throw_error('Downloading CVs not available');
    }

    return $self->get($download_link);
}

sub download_cv_link_to_cv {
    my $self = shift;
    my $candidate = shift;

#    if ( my $cv_link = $candidate->cv_link() ) {
#        # cv_link() returns a link to stream if we have downloaded the CV already
#        # otherwise returns nothing
#        # so we shouldn't be using it here
#        return $cv_link;
#    }

    if ( my $cv_link = $candidate->attr('cv_link') ) {
        $self->log_debug("Downloading CV using candidate's CV link: [%s]", $cv_link);
        return $cv_link;
    }

    my $candidate_id = $candidate->candidate_id();
    if ( $candidate_id && defined(my $download_url = $self->download_base_url() ) ) {
        $self->log_debug("Downloading CV using download_base_url: [%s]", $download_url);
        return sprintf $download_url, $candidate_id;
    }

    return;
}

sub profile_url {
    my $self = shift;
    my $candidate = shift;
    my $candidate_id = $candidate->candidate_id();
    if ( $candidate_id && defined(my $download_url = $self->profile_base_url() ) ) {
        return sprintf $download_url, $candidate_id;
    } else {
        return "";
    }
}

################# DOWNLOAD CANDIDATE_PROFILE ######
sub download_profile_submit {

    my $self = shift;
    my $candidate = shift;

    my $download_link = $self->profile_url($candidate);

    if ( !$download_link ) {
        $self->throw_error('Downloading profile not available');
    }

    my $resp = $self->get($download_link);
    $self->recent_content( $resp->decoded_content() );

    if ( !$resp->is_success() ) {
        return $self->throw_error( "Unable to download <%s>: <%s>", $download_link, $resp->status_line() )
    }

    return $resp;
}



################## LOGOUT #########################
sub logout_url {}

sub logout {
    my $self = shift;

    $self->log_mark('logging out');

    if ( !$self->logged_in() ) {
        $self->log_debug('nothing to do - not logged in!');
        return;
    }

    my $logout_url = $self->logout_url();
    if ( $logout_url ) {
        $self->log_info('Following logout link [%s]', $logout_url);

        eval {
            $self->get( $logout_url );
        };

        if ( $@ ) {
            $self->log_warn('We could not logout: [%s]', $@ );
        }

        delete $self->{logged_in};
        return 1;
    }

    delete $self->{logged_in};

    return undef;
}

# equivalent to find_link method from WWW::Mechanize
sub find_inputs {
    my $self = shift;
    my %params = ( n => 1, @_ );

    my $form = $self->current_form()
        or return;

    my @inputs = $form->inputs()
        or return;

    my $wantall = wantarray;

    my $nmatches = 0;
    my @matches;
    foreach my $input (@inputs) {
        if ( _match_any_input_params( $input, \%params ) ) {
            if ( $wantall ) {
                push @matches, $input;
            }
            else {
                ++$nmatches;
                return $input if $nmatches >= $params{n};
            }
        }
    } # foreach @inputs

    if ( $wantall ) {
        return @matches;
    }

    return shift @matches;
}

### PRIVATE ###
# used by find_inputs to check for matches
# The logic is such that ALL param criteria that are given must match
# based on WWW/Mechanize.pm find_links method
sub _match_any_input_params {
    my $input = shift;
    my $p_ref = shift;

    # no conditions, anything matches
    return 1 unless keys %$p_ref;

    return if defined $p_ref->{name}          && !($input->name() eq $p_ref->{name});
    return if defined $p_ref->{name_regex}    && !($input->name() =~ $p_ref->{name_regex});

    return if defined $p_ref->{id}            && !($input->id() eq $p_ref->{id});
    return if defined $p_ref->{id_regex}      && !($input->id() =~ $p_ref->{id_regex});

    return if defined $p_ref->{type}          && !($input->type() eq $p_ref->{type});
    return if defined $p_ref->{type_regex}    && !($input->type() =~ $p_ref->{type_regex});

    return if defined $p_ref->{value}         && !($input->value() eq $p_ref->{type});
    return if defined $p_ref->{value_regex}   && !($input->value() =~ $p_ref->{type_regex});

    return if defined $p_ref->{class}         && !($input->class() eq $p_ref->{class});
    return if defined $p_ref->{class_regex}   && !($input->class() =~ $p_ref->{class_regex});

    # Success: everything that was defined passed.
    return 1;
}

sub find_input {
    my $self = shift;
    my $field = shift;
    my $number = shift;

    my $form = $self->current_form()
        or return undef;

    my $input = $form->find_input( $field, $number );

    return $input;
}

=head2 install_www_methods

Monkey patch this to delegate some methods to the underlying WWW::Mechanize agent.

=cut

sub install_www_methods {
    # We aren't subclassing WWW::Mechanize
    # but some of the methods it has are useful

    no strict 'refs'; # messing with the symbol table

    # install basic methods
    foreach my $mech_method ( qw(set_fields form_with_fields forms form_name current_form uri response) ) {
        *{__PACKAGE__.'::'.$mech_method} = sub {
            my $self = shift;
            my $mech = $self->agent();

            local $Carp::CarpLevel = 1;
            return $mech->$mech_method( @_ );
        };
    }

    # install methods that fetch/submit forms (so we can automatically log them)
    foreach my $mech_method ( qw(get submit follow_link request click post ) ) {
        *{__PACKAGE__.'::'.$mech_method} = sub {
            my $self = shift;
            my $mech = $self->agent();

            my $resp = eval {
                $mech->$mech_method( @_ )
            };

            ## Loop through all the intermediary redirects, end
            ## with this last response.

            if ( !$resp ) {
                $self->log_warn('Response was empty: %s', $@ );
                $self->log_debug('Arguments were: %s->%s(%s)', $mech_method, \@_);
            }
            elsif ( !$resp->is_success ) {
                $self->throw_error('Response was unsuccessful: ' . $resp->status_line);
            }

            $self->recent_content(undef);

            return $resp;
        };
    }
}

sub agent {
    return $_[0]->{agent} ||= $_[0]->new_agent();
}

sub new_agent {
    my $class = shift;
    my $agent_class = $class->AGENT_CLASS();

    $class->log_debug('Using Mechanize agent - '.$agent_class);

    my $username = $class->basic_username();
    my $password = $class->basic_password();
    $class->log_debug('Basic username [%s] & password [%s]', $username, $password);

    my $agent = $agent_class->new(
        agent       => $class->USER_AGENT,
        engine      => $class,
        stack_depth => 0, # No history (causes circular references?)
        proxy       => $class->proxy(),
        basic_username => $username,
        basic_password => $password,
    );
    my $timeout = $class->AGENT_TIMEOUT();
    if ( $timeout && $timeout > 0 ) {
        $agent->timeout( $timeout );
    }

    my $max_size = $class->AGENT_MAX_SIZE();
    if ( $max_size && $max_size > 0 ) {
        $agent->max_size( $max_size );
    }

    $class->log_lwp_ua($agent);

    return $agent;
}

BEGIN {
    install_www_methods();
};

1;

# vim: expandtab shiftwidth=4

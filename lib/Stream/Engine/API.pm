package Stream::Engine::API;

use strict;
use warnings;

use base qw(Stream::Engine);

=head2 default_url (STUB/MANDATORY)

The url to the webservice. C<login_url>, C<search_url> will all
fallback to this url if they aren't specified.

=cut

sub default_url          {} # stub
sub login_url            { return $_[0]->default_url(); }
sub search_url           { return $_[0]->default_url(); }
sub download_cv_url      { return $_[0]->default_url(); }
sub download_profile_url { return $_[0]->default_url(); }

sub default_content_type          {} # stub
sub login_content_type            { return $_[0]->default_content_type(); }
sub search_content_type           { return $_[0]->default_content_type(); }
sub download_cv_content_type      { return $_[0]->default_content_type(); }
sub download_profile_content_type { return $_[0]->default_content_type(); }

# callbacks
# =========
sub login_callback {} # called after we have successfully logged in

=head2 login_session_is_valid

Stub to check if a value stored in the users cache (or wherever) is
available or not. Should return a boolean value, with the check
performed in this method.

=cut

sub login_session_is_valid {}


# Header customisations
# =====================
# stubs to make it easier to add SOAP headers etc.

# XXX_get
# -------
# Add stage headers + global headers to the GET request
sub login_get {
    my $self = shift;
    my $url  = shift;
    my @headers = $self->add_login_headers(@_);

    return $self->get( $url, @headers );
}

sub search_get {
    my $self = shift;
    my $url  = shift;
    my @headers = $self->add_search_headers(@_);

    return $self->get( $url, @headers );
}

sub download_cv_get {
    my $self = shift;
    my $url  = shift;
    my @headers = $self->add_download_cv_headers(@_);

    return $self->get( $url, @headers );
}

sub download_profile_get {
    my $self = shift;
    my $url  = shift;
    my @headers = $self->add_download_profile_headers(@_);

    return $self->get( $url, @headers );
}

# XXX_post
# --------
# Add stage headers + global headers to the POST request
sub login_post {
    my $self = shift;
    my $url  = shift;
    my @headers = $self->add_login_headers(@_);

    return $self->post( $url, @headers );
}

sub search_post {
    my $self = shift;
    my $url  = shift;
    my @headers = $self->add_search_headers(@_);

    return $self->post( $url, @headers );
}

sub download_cv_post {
    my $self = shift;
    my $url  = shift;
    my @headers = $self->add_download_cv_headers(@_);

    return $self->post( $url, @headers );
}

sub download_profile_post {
    my $self = shift;
    my $url  = shift;
    my @headers = $self->add_download_profile_headers(@_);
    return $self->post( $url, @headers );
}


# Login header customisations
# ---------------------------
# Only add these headers to the login requests
sub add_login_headers {
    my $self = shift;
    my @existing_headers = @_;

    my @extra_headers = $self->login_headers();
    if ( @extra_headers ) {
        $self->log_debug('finding login http headers');
        push @existing_headers, @extra_headers;
        $self->log_debug('added [%d] login http headers', scalar(@extra_headers) );
    }

    return @existing_headers;
}

sub add_search_headers {
    my $self = shift;
    my @existing_headers = @_;

    my @extra_headers = $self->search_headers();
    if ( @extra_headers ) {
        $self->log_debug('finding search http headers');
        push @existing_headers, @extra_headers;
        $self->log_debug('added [%d] search http headers', scalar(@extra_headers) );
    }

    return @existing_headers;
}

sub add_download_cv_headers {
    my $self = shift;
    my @existing_headers = @_;

    my @extra_headers = $self->download_cv_headers();
    if ( @extra_headers ) {
        $self->log_debug('finding download_cv http headers');
        push @existing_headers, @extra_headers;
        $self->log_debug('added [%d] download_cv http headers', scalar(@extra_headers) );
    }

    return @existing_headers;
}

sub add_download_profile_headers {
    my $self = shift;
    my @existing_headers = @_;

    my @extra_headers = $self->download_profile_headers();
    if ( @extra_headers ) {
        $self->log_debug('finding download_profile http headers');
        push @existing_headers, @extra_headers;
        $self->log_debug('added [%d] download_profile http headers', scalar(@extra_headers) );
    }

    return @existing_headers;
}

# return headers that are specific to a phase
# and replace any token names with their value
sub default_headers {
    my $self = shift;
    return $self->_process_headers( $self->default_set_headers() );
}

sub login_headers {
    my $self = shift;
    return $self->_process_headers( $self->login_set_headers() );
}

sub search_headers {
    my $self = shift;
    return $self->_process_headers( $self->search_set_headers() );
}

sub download_cv_headers {
    my $self = shift;
    return $self->_process_headers( $self->download_cv_set_headers() );
}

sub download_profile_headers {
    my $self = shift;
    return $self->_process_headers( $self->download_profile_set_headers() );
}


# *_set_headers
# ------------------
# return a hash containing ( Header => Value )
# you can return token names in the value
#
sub default_set_headers          {} # adds headers to all phases
sub login_set_headers            {} # adds headers to the login phase
sub search_set_headers           {} # etc....
sub download_cv_set_headers      {}
sub download_profile_set_headers {}

# _process_headers
# ----------------
sub _process_headers {
    my $self = shift;

    my @extra_headers;
    while ( scalar(@_) ) {
        my $header = shift;
        my $unprocessed_value = shift;

        $self->log_debug('seen unprocessed header [%s] => [%s]', $header, $unprocessed_value);
        my $processed_value = $self->fill_in_tokens( $unprocessed_value );

        $self->log_debug('adding header [%s] => [%s]', $header, $processed_value);
        push @extra_headers, $header => $processed_value;
    }

    return @extra_headers;
}


# Agent methods
# -------------
sub agent {
    if ( !$_[0]->{agent} ) {
        $_[0]->{agent} = $_[0]->new_agent();
    }
    return $_[0]->{agent};
}

sub new_agent {
    my $class = shift;
    my $agent_class = $class->AGENT_CLASS();

    $class->log_debug('Using agent "%s"', $agent_class);

    my $username = $class->basic_username();
    my $password = $class->basic_password();
    $class->log_debug('Basic username [%s] & password [%s]', $username, $password);

    my $mech = $agent_class->new(
        agent          => $class->USER_AGENT,
        engine         => $class,
        proxy          => $class->proxy(),
        basic_username => $username,
        basic_password => $password,
    );

    # follow redirects even during a HTTP post
    push(@{ $mech->requests_redirectable }, 'POST'); # Make sure this is on

    my $timeout = $class->AGENT_TIMEOUT();
    if ( $timeout && $timeout > 0 ) {
        $mech->timeout( $timeout );
    }

    my $max_size = $class->AGENT_MAX_SIZE();
    if ( $max_size && $max_size > 0 ) {
        $mech->max_size( $max_size );
    }

    $class->log_lwp_ua($mech);

    return $mech;
}

# methods to handle the response from boards
# ------------------------------------------
sub content {
    my $self = shift;

    my $response = $self->response() or return undef;
    return $response->decoded_content();
}

sub guess_content_type_from_filename {
    my $self = shift;
    my $filename = shift;

    if ( $filename =~ m/\.html?$/i ) {
        return 'text/html; charset=UTF-8';
    }

    # try and detect from the filename or assume plaintext
    require Stream::DetectFileType;
    my $mime_type = Stream::DetectFileType->guess_mime_type_from_filename( $filename );
    return $mime_type || 'text/plain; charset=UTF-8';
}

sub build_cv_filename {
    my $self      = shift;
    my $candidate = shift;
    my $raw_text  = shift;

    my $filename = $self->destination().'_'.$candidate->candidate_id().'.';

    require Stream::DetectFileType;
    my $extension = Stream::DetectFileType->guess( $raw_text ) || 'txt';
    return $filename . $extension;
}

1;

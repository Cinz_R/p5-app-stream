#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: REST.pm 85915 2012-10-23 16:37:39Z chrisp $

package Stream::Engine::API::REST;

=head1 NAME

Stream::Engine::API::REST - REST CvSearch robot and scraper

=head1 AUTHOR

Darren - 18/07/2011

=cut

use strict;

use Stream::Agents::REST;

use base qw(Stream::Engine::API);

our $VERSION = qq(0.01);
use MIME::Base64;
use JSON;

=head1 OVERVIEW

Specialised REST robot expecting a JSON response.

=cut

=head1 Class Methods

=head2 VERSION (GENERIC)

Current version of this module

=cut

sub VERSION {
    return $VERSION;
}

=head2 AGENT_CLASS

Agent to use (default is Stream::Agents::REST)

=cut

sub AGENT_CLASS {
    return 'Stream::Agents::REST';
}

## Some defaults

=head2 default_content_type (GENERIC/MANDATORY)

Fallback content type used if not defined.

Default is application/x-www-form-urlencoded

=cut

sub default_content_type { return 'application/x-www-form-urlencoded'; }

## Login ##

=head1 Workflow Methods

=head2 login (GENERIC/MANDATORY)

=over

=item * login_url (GENERIC)

URL to submit login params (leave empty if there is no login).
Default is to fallback to C<default_url>

=item * login_content_type (GENERIC)

Request content type. Default is to fallback to C<default_content_type>

=item * login_fields (GENERIC/MANDATORY)

Returns the params to post to login. Leave empty if there is no login.

=item * login_post (GENERIC/MANDATORY)

Sends the params to the webservice. Default is a HTTP post request.

=item * login_success (STUB/OPTIONAL)

Regex that will match if the login is successful

=item * login_failed (STUB/OPTIONAL)

Regex that will match if the login fails

=item * login_callback (STUB/OPTIONAL)

Custom callback to allow scraping of session ids if needed.
Only called if the login is successful.

=back

=cut

sub login {
    my $self = shift;

    if ( $self->logged_in() ) {
        return;
    }

    my $login_url = $self->login_url;

    if ( !defined( $login_url ) ) {
        $self->log_info( 'no login page' );
        $self->{logged_in} = 1;
        return 1;
    }

    if ( !$self->can('login_fields') ) {
        $self->log_info( 'no login params - assuming no login needed' );
        $self->{logged_in} = 1;
        return 1;
    }

    if ( $self->login_session_is_valid() ) {
        $self->log_info('session value used to login is valid, marking as logged in');
        $self->{logged_in} = 1;
        return 1;
    }

    $self->log_debug('submitting to %s', $login_url );

    my @query = $self->field_value_pairs($self->login_fields());

    $self->login_post(
        $login_url,
        Content_Type => $self->login_content_type(),
        Content      => $self->to_querystring(@query),
    );

    $self->check_for_login_error();

    $self->login_callback();

    $self->{logged_in} = 1;

    $self->log_debug('logged in');
}

=head2 search_begin_search

Starts the cv search process by submitting the form and scraping the first page.
It also identifies the number of results in the search as well as the next page
links if available.

=over

=item * search_submit()

=item * search_success()

=item * search_failed

=item * scrape_page()

=item * scrape_number_of_results()

=item * scrape_paginator()

=back

=cut



sub response_to_JSON {
    my $self = shift;
    my %defaults = ('utf8' => 1);
    my %fields = (%defaults, @_);
    return JSON::from_json( $self->recent_content() , \%fields); 
}

=head2 scrape_facets

This is an optional hook which can be used to return facets with the results.

Example implementation:

    sub scrape_facets {
        my $facet_item = {
            SortMetric      => $SM_INDUSTRY,
            Label           => 'Industry Facet',
            Type            => 'multilist',
            Id              => 'myboardname_industry',
            Name            => 'industry',
            # In case of multilist. tags or groupmultilist:
            SelectedValues  => { 'fsh1' => 1, 'acc3' => 1 },
            # In case of other types:
            Value => 'fsh1',
            Options         => [ [ 'label', 'id', $count ], [ 'Fishing', 'fsh1', 10 ], [ 'Accountancy', 'acc3', 15 ], [ 'Software', 'sft2', 1 ] ]
        };

        push @{$self->results->facets}, $facet_item;
    }

=cut

sub scrape_facets {};

=head2 scrape_page

Scrapes the facets and the actual candidate results from the 'page' (even though
this is about a REST API).

Returns the $self->results() object.

After a call to this, $self->results() is NOT GUARANTEED to be fully populated. Usually, that is when its
used with the almighty L<Stream2::Results::Store>, the candidates added go directly to redis, and
are NOT stored in memory (in the ->results()->results() array ref).

Note: This overrides L<Stream::Engine> 'scrape_page' which despite being
in a super class seems to specialise in scraping XML stuff.

=cut

sub scrape_page {
    my $self = shift;
    my $logger = $self->logger();

    $self->log_info('REST scraper: template is %s', ref($self) );
    $self->log_info('Page: %d', $self->current_page() );

    my @candidate_results = $self->scrape_candidate_array();

    $self->scrape_facets();

    my $results = $self->results();

    if ( !scalar(@candidate_results)) {
        $self->log_warn('Candidates array is empty');
        return $results;
    }

    $self->results_per_scrape( scalar( @candidate_results ) );

    foreach my $candidate (@candidate_results){
       $self->scrape_candidate($candidate);
    }

    $self->log_debug('finished scraping candidate results');

    return $results;

}

sub scrape_candidate {
    my $self = shift;
    my $result = shift;

    my %attributes = $self->scrape_candidate_details($result);

    if ( !$self->scrape_found_attributes( %attributes ) ) {
        $self->log_warn('No candidate information found');
        return;
    }

    my $candidate = $self->new_candidate();

    $candidate->attributes(
        %attributes,
    );

    $self->candidate_messaging_uri ( $candidate );
    $self->scrape_fixup_candidate( $candidate );
    $self->scrape_candidate_add_headline( $candidate );
    $self->log_debug('Adding candidate: %s - id: %s', $candidate->name, $candidate->candidate_id() );
    $self->add_candidate( $candidate );
    return $candidate;
}

sub search_begin_search {
    my $self = shift;

    $self->log_mark('Beginning search');

    $self->current_page( 1 );

    my $response = $self->search_submit();

    my $decoded_content = $response->decoded_content();
    $self->recent_content($decoded_content);

    my $search_success = $self->search_success();
    if ( defined( $search_success ) ) {
        $self->log_debug('Checking for success message [%s]', $search_success);
        if ( $decoded_content !~ m/$search_success/ ) {
            $self->throw_error('Unable to search - no success message');
        }
    }

    my $search_failed = $self->search_failed();
    if ( defined( $search_failed ) ) {
        $self->log_debug('Checking for failure message [%s]', $search_failed);
        if ( $decoded_content =~ m/$search_failed/ ) {
            $self->throw_error('Unable to search - found failure message: ' . $search_failed);
        }
    }

    $self->log_debug('Assuming success');

    # scrape response for candidates
    $self->log_mark('Scraping the first page');
    $self->scrape_page();

    # identify the total number of results
    $self->log_debug('Scraping number of results');
    $self->scrape_number_of_results();

    # identify the total number of pages
    $self->log_debug('Scraping paginator');
    $self->scrape_paginator();

    $self->log_debug('search_begin done');

    return 1;
}


=head2 search_next_page

Searches and scrapes the second page onwards

=over

=item * search_submit()

=item * scrape_page()

=back

=cut

sub search_next_page {
    my $self = shift;

    $self->log_mark('Searching the page after %s', $self->current_page);

    if ( !($self->max_pages() || $self->total_results()) ) {
        $self->log_debug('Unable to search next page, not sure how many pages there are still to search');
        return 0;
    }

    $self->{current_page}++;

    $self->log_debug('Searching page %s', $self->current_page);

    my $response = $self->search_submit();

    if ( !$response ) {
        $self->log_debug('Stopping search early because we have no response');
        return 0;
    }

    $self->log_debug('Assuming success');
    $self->log_mark('Scraping page %d of %d for candidates', $self->current_page, $self->max_pages() );
    $self->scrape_page();

    return 1;
}

sub search_specific_page {
    my $self = shift;
    my $page = shift
        or $self->throw_error('No page given for specific page search');

    $self->log_mark('Beginning search at page %s', $page);

    $self->current_page( $page );

    my $response = $self->search_submit();

    my $decoded_content = $response->decoded_content();
    $self->recent_content($decoded_content);

    my $search_success = $self->search_success();
    if ( defined( $search_success ) ) {
        $self->log_debug('Checking for success message [%s]', $search_success);
        if ( $decoded_content !~ m/$search_success/ ) {
            $self->throw_error('Unable to search - no success message');
        }
    }

    my $search_failed = $self->search_failed();
    if ( defined( $search_failed ) ) {
        $self->log_debug('Checking for failure message [%s]', $search_failed);
        if ( $decoded_content =~ m/$search_failed/ ) {
            $self->throw_error('Unable to search - found failure message: ' . $search_failed);
        }
    }

    $self->log_debug('Assuming success');

    # scrape response for candidates
    $self->log_mark('Scraping the first page');
    $self->scrape_page();

    # identify the total number of results
    $self->log_debug('Scraping number of results');
    $self->scrape_number_of_results();

    # identify the total number of pages
    $self->log_debug('Scraping paginator');
    $self->scrape_paginator();

    $self->log_debug('search_begin done');

    return 1;
}

=head2 search_submit (GENERIC)

Builds the search request submits it to the search engine

=over

=item * search_fields (MANDATORY)

Builds the search params

=item * search_post (GENERIC)

Submits the params to the search engine

=item * search_url (GENERIC)

Web address to submit the params to (fallsback to C<default_url>)

=item * search_content_type

Content type of http request (fallsback to C<default_content_type>)

=back

=cut

sub search_submit {
    my $self = shift;

    my @query = $self->field_value_pairs($self->search_fields());

    return $self->search_post(
        $self->search_url(),
        Content_Type => $self->search_content_type(),
        Content => $self->to_querystring(@query),
    );
}

sub search_success {}
sub search_failed  {}

# Stub - override me!!
sub search_fields {
}

=head2 download_profile (GENERIC)

Downloads the candidate profile. 

This should NEVER charge the consultant.

=cut

sub download_profile_submit {
    my $self = shift;
    my $candidate = shift;

    if ( !$self->can('download_profile_fields') ) {
        return $self->throw_profile_not_supported('No "download_profile_fields" method to build profile request');
    }

    my @query = $self->field_value_pairs($self->download_profile_fields());

    if ( !@query ) {
        return $self->throw_error('No params to send to the board');;
    }

    return $self->download_profile_post(
        $self->download_profile_url(),
        Content_Type => $self->download_profile_content_type(),
        Content => $self->to_querystring(@query),
    );
}

=head2 download_cv (GENERIC)

=over

=item * download_cv_submit

Returns a HTTP::Response containing the CV

=item * download_cv_fields (MANDATORY)

Builds the params to request the cv

=item * download_cv_post (GENERIC)

Submits the params to the search engine

=item * download_cv_url (GENERIC)

Web address to submit the params to (fallsback to C<default_url>)

=item * download_cv_content_type (GENERIC)

Content type of http request (fallsback to C<default_content_type>)

=item * scrape_download_cv (GENERIC)

Uses XPath to scrape the CV

=back

=cut

sub do_download_cv {
    my $self = shift;
    my $candidate = shift;

    $self->login() if $self->download_cv_requires_login();

    my $response = $self->download_cv_submit( $candidate );

    $self->log_debug('Checking for errors');

    $self->check_for_success_or_failure(
        $self->download_cv_success() || undef,
        $self->download_cv_failed()  || undef,
    );

    return $self->scrape_download_cv( $candidate );
}

sub download_cv_submit {
    my $self = shift;
    my $candidate = shift;

    $self->log_debug('Building download cv params');

    my @query = $self->field_value_pairs($self->download_cv_fields($candidate));

    return $self->download_cv_post(
        $self->download_cv_url(),
        Content_Type => $self->download_cv_content_type(),
        Content => $self->to_querystring(@query),
    );
}

sub scrape_download_cv {
    my $self = shift;
    my $candidate = shift;

    $self->log_info('Scraping download cv' );

    $self->scrape_generic_download_cv( $candidate );

    # try to get the word CV
    my $word_doc = $candidate->attr( 'resume_word_base64' );

    my $raw_text;
    if ( $word_doc ) {
        $self->log_debug('Decoding base64 word doc');
        $raw_text = MIME::Base64::decode_base64( $word_doc );
        $self->log_debug('Decoded base64 word doc, deleting resume_word_base64 attribute');

        $candidate->delete_attr( 'resume_word_base64' );
    }

    # fallback to the text CV
    if ( !$raw_text ) {
        $self->log_debug('Falling back to the raw text version');
        $raw_text = $candidate->attr( 'resume_text' );
    }

    if ( !$raw_text ) {
        $self->log_debug('We could not find a CV for this candidate in their response');
        return $self->throw_error('NO CV CONTENT FOUND');
    }

    # get the resume filename or guess one
    my $filename = $candidate->attr( 'resume_filename' );
    $self->log_debug('resume_filename is: %s', $filename);
    if ( !$filename ) {
        $self->log_debug('no resume_filename so building one');
        $filename = $self->build_cv_filename( $candidate, $raw_text );
        $self->log_debug('built a resume filename: %s', $filename);
    }

    # get the resume content type or guess it
    my $content_type = $candidate->attr( 'resume_content_type' );
    $self->log_debug('resume_content_type is: %s', $content_type);
    if ( !$content_type ) {
        $self->log_debug('no resume_content_type so guessing');
        $content_type = $self->guess_content_type_from_filename( $filename );
        $self->log_debug('guessed resume_content_type: %s', $content_type);
    }

    # provided by Stream::Engine::API::REST
    return _MockResponse->new({
        decoded_content => $raw_text,
        filename        => $filename,
        content_type    => $content_type,
        headers         => _MockHTTP::Headers->new()
    });
}

sub scrape_generic_download_cv {
    my $self = shift;
    my $candidate = shift;

    $self->log_info('Generic download cv scraper: template is %s', ref($self) );

    if ( $self->can('before_scrape_download_cv' ) ) {
        $self->log_debug('Calling hook "before_scrape_download_cv"');
        $self->before_scrape_download_cv( $candidate );
    }

    my $json = $self->response_to_JSON();
    my %attributes = $self->scrape_download_cv_details($json);

    if ( !$self->scrape_found_attributes( %attributes ) ) {
        $self->log_warn('No candidate information found');
        return;
    }

    $self->log_debug( 'Found some attributes for this candidate' );

    $candidate->attributes( %attributes );

    $self->log_debug('Finished scraping CV' );

    return $candidate;
}

=item * scrape_download_cv_details (GENERIC)

reference containing xpaths

=back

=cut

sub new_agent {
    my $class = shift;
    my $agent_class = $class->AGENT_CLASS();

    $class->log_debug('Using REST agent - '.$agent_class);

    my $username = $class->basic_username();
    my $password = $class->basic_password();
    $class->log_debug('Basic username [%s] & password [%s]', $username, $password);

    my $mech = $agent_class->new(
        agent  => $class->USER_AGENT,
        engine => $class,
        proxy      => $class->proxy(),
        basic_username => $username,
        basic_password => $password,
    );

    push(@{ $mech->requests_redirectable }, 'POST'); # Make sure this is on

    my $timeout = $class->AGENT_TIMEOUT();
    if ( $timeout && $timeout > 0 ) {
        $mech->timeout( $timeout );
    }

    $class->log_lwp_ua($mech);

    return $mech;
}

sub install_lwp_methods {
    # We aren't subclassing LWP::UserAgent
    # (mainly because I don't trust multiple inheritance in perl)
    # but some of the methods it has are useful

    no strict 'refs'; # messing with the symbol table

    # install basic methods
    foreach my $lwp_method ( qw(uri response) ) {
        *{__PACKAGE__.'::'.$lwp_method} = sub {
            my $self = shift;
            my $lwp  = $self->agent();

            local $Carp::CarpLevel = 1;
            return $lwp->$lwp_method( @_ );
        };
    }

    # install methods that make http requests (so we can log them)
    foreach my $lwp_method ( qw(get request post) ) {
        *{__PACKAGE__.'::'.$lwp_method} = sub {
            my $self = shift;
            my $lwp  = $self->agent();
            my $url  = shift;
            my @headers = @_;

            my @extra_headers = $self->default_headers();
            if ( @extra_headers ) {
                $self->log_debug('Adding [%d] http headers', scalar(@extra_headers) );
                push @headers, @extra_headers;
            }

            my $resp = $lwp->$lwp_method( $url, @headers );

            if ( !$resp->is_success ) {
                $self->throw_error( 'Response was unsuccessful: ' . $resp->status_line );
            }

            $self->recent_content($resp->decoded_content());

            return $resp;
        };
    }
}

BEGIN {
    install_lwp_methods();
};

# provide a response like object that we can return from download_cv
package _MockResponse;

use strict;

use base qw(Class::Accessor::Fast);

__PACKAGE__->mk_accessors( qw(headers filename decoded_content content_type) );

sub header {
    my $self = shift;
    my $header = shift;
    my $value = shift;

    if ( $value ) {
        $self->{headers}->{$header} = $value;
        return 1;
    }

    if ( $header =~ m/content[-_]type/i ) {
        return $self->content_type();
    }

    return $self->{headers}->{$header};
}

1;

# a http headers like object - required if you want to return _MockResponse directly
package _MockHTTP::Headers;

use strict;

sub new {
    return bless {}, $_[0];
}

sub scan {
    my ($self, $coderef) = @_;
    my $headers = ['X-Candidate-Email']; # only one we use so far
    foreach my $key (@$headers) {
        my $val = $self->{$key};
        $coderef->($key, $val);
    }
}

1;

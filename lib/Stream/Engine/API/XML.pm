#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: XML.pm 85786 2012-10-22 14:46:22Z andy $

package Stream::Engine::API::XML;

=head1 NAME

Stream::Engines::XML - XML CvSearch robot and scraper

=head1 AUTHOR

ACJ - 10/09/2008

=cut

use strict;

use Stream::Agents::XML;

use base qw(Stream::Engine::API);

our $VERSION = qq(0.01);
use XML::LibXML;
use XML::LibXSLT;
use List::Util qw(first);
use List::MoreUtils qw(uniq);
use Log::Any qw/$log/;
use File::Spec;
use Bean::XML;
use MIME::Base64;

use Stream2;

=head1 OVERVIEW

Specialised XML robot. For a SOAP service, see
C<Stream::Engine::API::XML::SOAP> instead.

The XML robot is in C<Stream::Agents::XML> (just
LWP::UserAgent that remembers the last request/response)

Scraping is done using XPath (in particular C<XML::LibXML::XPathContext>
to allow namespace support)

=cut

=head1 Class Methods

=head2 VERSION (GENERIC)

Current version of this module

=cut

sub VERSION {
    return $VERSION;
}


=head2 AGENT_CLASS (XML only)

Agent to use (default is Stream::Agents::XML)

=cut

sub AGENT_CLASS {
    return 'Stream::Agents::XML';
}

=head2 XSLT_DIRECTORY (XML only)

Where to look for XSLT documents

=cut

sub XSLT_DIRECTORY {
    return Stream2->instance->shared_directory().'/feeds/xslt';
}

##
## Some defaults
##

=head2 default_content_type (GENERIC/MANDATORY)

Fallback content type used if not defined.

Default is text/xml

=cut

sub default_content_type { return 'text/xml'; }

## Login ##

=head1 Workflow Methods

=head2 login (GENERIC/MANDATORY)

=over

=item * login_url (GENERIC)

URL to submit xml (leave empty if there is no login).
Default is to fallback to C<default_url>

=item * login_content_type (GENERIC)

Request content type. Default is to fallback to C<default_content_type>

=item * login_build_xml (GENERIC/MANDATORY)

Returns the xml to post to login. Leave empty if there is no login.

=item * login_post (GENERIC/MANDATORY)

Sends the xml to the webservice. Default is a HTTP post request.

=item * login_success (STUB/OPTIONAL)

Regex that will match if the login is successful

=item * login_failed (STUB/OPTIONAL)

Regex that will match if the login fails

=item * login_callback (STUB/OPTIONAL)

Custom callback to allow scraping of session ids if needed.
Only called if the login is successful.

=back

=cut

sub login {
    my $self = shift;

    if ( $self->logged_in() ) {
        return;
    }

    my $login_url = $self->login_url;

    if ( !defined( $login_url ) ) {
        $self->log_info( 'no login page - assuming no logged_in = true (no login needed)' );
        $self->{logged_in} = 1;
        return 1;
    }

    if ( !$self->can('login_build_xml') ) {
        $log->info( 'no login xml - assuming no login needed' );
        $self->{logged_in} = 1;
        return 1;
    }

    $log->debugf('submitting to %s', $login_url );

    my $xml = $self->login_build_xml();

    # just in case login_build_xml() returns a Bean::XML object instead of raw content
    if ( ref( $xml ) && UNIVERSAL::can( $xml, 'asString' ) ) {
        $xml = $xml->asString();
    }

    $self->login_post(
        $login_url,
        Content_Type => $self->login_content_type(),
        Content      => $xml,
    );

    $self->check_for_login_error();

    $self->login_callback();

    $self->{logged_in} = 1;

    $log->debug('logged in');
}

=head2 search_begin_search

Starts the cv search process by submitting the form and scraping the first page.
It also identifies the number of results in the search as well as the next page
links if available.

=over

=item * search_submit()

=item * search_success()

=item * search_failed

=item * scrape_page()

=item * scrape_number_of_results()

=item * scrape_paginator()

=back

=cut

sub search_begin_search {
    my $self = shift;

    $self->log_mark('Beginning search');

    $self->log_debug('building xml');

    $self->current_page( 1 );

    my $response = $self->search_submit();

    my $decoded_content = $response->decoded_content();

    my $search_success = $self->search_success();
    if ( defined( $search_success ) ) {
        $self->log_debug('Checking for success message [%s]', $search_success);
        if ( $decoded_content !~ m/$search_success/ ) {
            $self->throw_error('Unable to search - no success message');
        }
    }

    my $search_failed = $self->search_failed();
    if ( defined( $search_failed ) ) {
        $self->log_debug('Checking for failure message [%s]', $search_failed);
        if ( $decoded_content =~ m/$search_failed/ ) {
            $self->throw_error('Unable to search - found failure message: ' . $search_failed);
        }
    }

    $self->log_debug('Assuming success');

#    $self->recent_content( $decoded_content );

    # scrape xml for candidates
    $self->log_mark('Scraping the first page');
    $self->scrape_page();

    # identify the total number of results
    $self->log_debug('Scraping number of results');
    $self->scrape_number_of_results();

    # identify the total number of pages
    $self->log_debug('Scraping paginator');
    $self->scrape_paginator();

    $self->log_debug('search_begin done');

    return 1;
}

sub search_specific_page {
    my $self = shift;
    my $page = shift
        or $self->throw_error('No page given for specific page search');

    $self->log_mark('Beginning search at page %s', $page);
    $self->log_debug('building xml');

    $self->current_page( $page );

    my $response = $self->search_submit();

    my $decoded_content = $response->decoded_content();

    my $search_success = $self->search_success();
    if ( defined( $search_success ) ) {
        $self->log_debug('Checking for success message [%s]', $search_success);
        if ( $decoded_content !~ m/$search_success/ ) {
            $self->throw_error('Unable to search - no success message');
        }
    }

    my $search_failed = $self->search_failed();
    if ( defined( $search_failed ) ) {
        $self->log_debug('Checking for failure message [%s]', $search_failed);
        if ( $decoded_content =~ m/$search_failed/ ) {
            $self->throw_error('Unable to search - found failure message: ' . $search_failed);
        }
    }

    $self->log_debug('Assuming success');

#    $self->recent_content( $decoded_content );

    # scrape xml for candidates
    $self->log_mark('Scraping the first page');
    $self->scrape_page();

    # identify the total number of results
    $self->log_debug('Scraping number of results');
    $self->scrape_number_of_results();

    # identify the total number of pages
    $self->log_debug('Scraping paginator');
    $self->scrape_paginator();

    $self->log_debug('search_begin done');

    return 1;

}

=head2 search_next_page

Searches and scrapes the second page onwards

=over

=item * search_submit()

=item * scrape_page()

=back

=cut

sub search_next_page {
    my $self = shift;

    $self->log_mark('Searching the page after %s', $self->current_page);

    if ( !($self->max_pages() || $self->total_results()) ) {
        $self->log_debug('Unable to search next page, not sure how many pages there are still to search');
        return 0;
    }

    $self->{current_page}++;

    $self->log_debug('Searching page %s', $self->current_page);

    my $response = $self->search_submit();

    if ( !$response ) {
        $self->log_debug('Stopping search early because we have no response');
        return 0;
    }

    my $decoded_content = $response->decoded_content();

    $self->log_debug('Assuming success');
#    $self->recent_content( $decoded_content );

    # scrape xml for candidates
    $self->log_mark('Scraping page %d of %d for candidates', $self->current_page, $self->max_pages() );
    $self->scrape_page();

    return 1;
}

=head2 search_submit (GENERIC)

Builds the search xml and submits it to the search engine

=over

=item * search_build_xml (MANDATORY)

Builds the search xml

=item * search_post (GENERIC)

Submits the xml to the search engine

=item * search_url (GENERIC)

Web address to submit the xml to (fallsback to C<default_url>)

=item * search_content_type

Content type of http request (fallsback to C<default_content_type>)

=back

=cut

sub search_submit {
    my $self = shift;

    my $xml = $self->xml_to_string($self->search_build_xml());

    $self->log_trace("POSTING $xml");

    my $response =  $self->search_post(
                                       $self->search_url(),
                                       Content_Type => $self->search_content_type(),
                                       Content => $xml,
                                      );
    return $response;
}

sub search_success {}
sub search_failed  {}

# Stub - override me!!
sub search_build_xml {
}

=head2 download_profile (GENERIC)

Downloads the candidate profile. 

This should NEVER charge the consultant.

=cut

sub download_profile_submit {
    my $self = shift;
    my $candidate = shift;
    if ( !$self->can('download_profile_xml') ) {
        return $self->throw_profile_not_supported('No "download_profile_xml" method to build xml request');
    }
    my $xml = $self->xml_to_string( $self->download_profile_xml( $candidate ) );
    if ( !$xml ) {
        return $self->throw_error('No xml to send to the board');;
    }
    return $self->download_profile_post(
        $self->download_profile_url(),
        Content_Type => $self->download_profile_content_type(),
        Content      => $xml,
    );
}

=head2 download_cv (GENERIC)

=over

=item * download_cv_submit

Returns a HTTP::Response containing the CV

=item * download_cv_xml (MANDATORY)

Builds the xml to request the cv

=item * download_cv_post (GENERIC)

Submits the xml to the search engine

=item * download_cv_url (GENERIC)

Web address to submit the xml to (fallsback to C<default_url>)

=item * download_cv_content_type (GENERIC)

Content type of http request (fallsback to C<default_content_type>)

=item * scrape_download_cv (GENERIC)

Uses XPath to scrape the CV

=back

=cut

sub do_download_cv {
    my $self = shift;
    my $candidate = shift;

    my $response = $self->download_cv_submit( $candidate );

    $self->log_debug('Checking for errors');

    $self->check_for_success_or_failure(
        $self->download_cv_success() || undef,
        $self->download_cv_failed()  || undef,
    );

    # scrape xml for candidates
    return $self->scrape_download_cv( $candidate );
}

sub download_cv_submit {
    my $self = shift;
    my $candidate = shift;

    $self->log_debug('Building download cv xml');
    my $xml = $self->xml_to_string( $self->download_cv_xml( $candidate ) );

    $self->log_debug('Sending XML to cv database');
    return $self->download_cv_post(
        $self->download_cv_url(),
        Content_Type => $self->download_cv_content_type(),
        Content      => $xml,
    );
}

sub scrape_download_cv {
    my $self = shift;
    my $candidate = shift;

    $self->log_info('Scraping download cv xml' );

    $self->scrape_generic_download_cv( $candidate );

    return $self->extract_cv_from_candidate( $candidate );
}

=head2 scrape_generic_download_cv

Scrapes xml using xpath to extract the CV. Moved to Stream::Engine

=over

=item * scrape_download_cv_xpath (MANDATORY)

Xpath to identify candidate node

=item * scrape_download_cv_details (GENERIC)

reference containing xpaths

=back

=cut

# don't need to customise me!
sub xml_to_string {
    my $self = shift;
    my $xml  = shift;

    if ( ref($xml) ) {
        if ( UNIVERSAL::can( $xml, 'asString' ) ) {
            return $xml->asString();
        }
        elsif ( UNIVERSAL::can( $xml, 'as_string' ) ) {
            return $xml->as_string();
        }
    }

    return $xml;
}

# handy if you have a single namespace.
# returns the URI of the default namespace
sub default_namespace {}

# handy if you have multiple namespaces
# returns a hash containing ( prefix => uri )
sub namespaces {}


sub registerNs {
    my $self = shift;
    my $scraper = $self->scraper();

    if ( $scraper ) {
        $self->log_debug('Registering namespaces');
        while ( @_ ) {
            my $prefix = shift;
            my $namespace = shift;
            $self->log_debug('Registering namespace: %s => %s', $prefix, $namespace);
            $scraper->registerNs( $prefix => $namespace );
        }
    }
    else {
        $self->log_warn('No scraper to register namespaces against');
    }

    return;
}

sub libxml_doc {
    my $self = shift;
    my $scraper = $self->scraper()
        or return;
    if ( $scraper->type() eq 'xml' ) {
        return $scraper->doc();
    }
    return;
}

sub apply_xslt {
    my $self = shift;
    my $xslt_file = shift;

    my $xslt = $self->load_xslt( $xslt_file );

    # transform expects a XML::LibXML object not a XML::LibXML::XPathContext object
    my $xc = $self->libxml_doc();
    my $context_node = $xc->getContextNode();
    my $owner_document = $context_node->ownerDocument();

    # this may die if the xslt has syntax errors
    my $transformed_doc = $xslt->transform( $owner_document );

    return $xslt->output_as_chars( $transformed_doc );
}

sub load_xslt {
    my $self = shift;
    my $xslt_file = shift;
    
    my $abs_xslt_file = File::Spec->catfile( $self->XSLT_DIRECTORY(), $xslt_file );

    my $style_doc = eval {
        XML::LibXML->load_xml(
            location => $abs_xslt_file,
            no_cdata => 1,
        ),
    };

    if ( $@ ) {
        $self->log_error('Unable to read XSLT stylesheet: [%s]', $@);
        die $@;
    }

    my $xslt = XML::LibXSLT->new();
    my $stylesheet = $xslt->parse_stylesheet( $style_doc );

    return $stylesheet;
}

sub install_lwp_methods {
    # We aren't subclassing LWP::UserAgent
    # (mainly because I don't trust multiple inheritance in perl)
    # but some of the methods it has are useful

    no strict 'refs'; # messing with the symbol table

    # install basic methods
    foreach my $lwp_method ( qw(uri response) ) {
        *{__PACKAGE__.'::'.$lwp_method} = sub {
            my $self = shift;
            my $lwp  = $self->agent();

            local $Carp::CarpLevel = 1;
            return $lwp->$lwp_method( @_ );
        };
    }

    # install methods that make http requests (so we can log them)
    foreach my $lwp_method ( qw(get request post) ) {
        *{__PACKAGE__.'::'.$lwp_method} = sub {
            my $self = shift;
            my $lwp  = $self->agent();
            my $url  = shift;
            my @headers = @_;

            my @extra_headers = $self->default_headers();
        
            if ( @extra_headers ) {
                $self->log_debug('Adding [%d] http headers', scalar(@extra_headers) );
                push @headers, @extra_headers;
            }
            my $resp = $lwp->$lwp_method( $url, @headers );

            if ( !$resp->is_success ) {
                $self->throw_error( 'Response was unsuccessful: ' . $resp->status_line );
            }

            $self->recent_content($resp->decoded_content());

            return $resp;
        };
    }
}

BEGIN {
    install_lwp_methods();
};

1;

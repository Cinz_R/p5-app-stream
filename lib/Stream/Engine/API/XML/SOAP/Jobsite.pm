#!/usr/bin/perl

# $Id: Jobsite.pm 79087 2012-07-16 15:47:46Z andy $

package Stream::Engine::API::XML::SOAP::Jobsite;

use strict;

use Stream::Agents::Jobsite;

use base qw(Stream::Engine::API::XML::SOAP);

our $VERSION = qq(0.01);

use Bean::XML;
use Digest::MD5 qw( &md5_hex );
use HTTP::Request;
use MIME::Base64;

sub VERSION {
    return $VERSION;
}

sub AGENT_CLASS {
    return 'Stream::Agents::Jobsite';
}

sub soap_xml {
    my $self = shift;
    my $options_ref = shift;

    my ($body_callback_ref);
    if ( ref( $options_ref ) eq 'HASH' ) {
        $body_callback_ref = delete $options_ref->{body};
    }
    else {
        ($body_callback_ref,$options_ref) = ($options_ref, {});
    }
    
    # make it easier to compare the xml this generates with the SOAP::Lite client
    my $body_xml = Bean::XML->new( ENCODING => 'ISO-8859-1', DATA_MODE => 1, DATA_INDENT => 2 );
    $body_xml->xmlDecl( 'ISO-8859-1' );
    $body_callback_ref->( $self, $body_xml );

    $self->{message} = $body_xml->asString();

    return $self->SUPER::soap_xml({
        %$options_ref,
        body => \&build_jobsite_body,
    });
}

sub build_jobsite_body {
    my $self = shift;
    my $xml  = shift;

    my $message_hash = $self->jobsite_request_hash(
        $self->{message},
    );

    my $action = 'search';
    if ( $self->{message} =~ m/cvType/ ) {
        $action = 'view';
    }

    # Log the original payload XML before we stick we base64 it
    $self->log_original_payload( $self->{message} );

    $xml->startTag( $action,
        xmlns => 'Jobsite_cv_search_webservice',
    );
    {
        $xml->startTag('c-gensym3', 'xsi:type' => 'xsd:string');
            $xml->characters( $message_hash );
        $xml->endTag();

        $xml->startTag('c-gensym5', 'xsi:type' => 'xsd:base64Binary');
            $xml->characters( MIME::Base64::encode_base64( $self->{message} ) );
        $xml->endTag();
    }
    $xml->endTag();
}

sub log_original_payload {
    my $self = shift;
    my $payload = shift;

    my $fake_request = HTTP::Request->new(
        'STREAM', 'PayloadXML'
    );

    $fake_request->content( $payload );

    $self->log_request( $fake_request );

    return;
}

sub jobsite_request_hash {
    my $self = shift;
    my $message = shift;

    return md5_hex( $self->REQUEST_KEY . $message );
}

sub jobsite_response_hash {
    my $self = shift;
    my $message = shift;

    return md5_hex( $self->RESPONSE_KEY . $message );
}

1;

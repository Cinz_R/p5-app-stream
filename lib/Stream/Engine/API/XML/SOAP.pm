#!/usr/bin/perl

# $Id: SOAP.pm 79071 2012-07-16 13:45:51Z andy $

package Stream::Engine::API::XML::SOAP;

use strict;

use base qw(Stream::Engine::API::XML);

our $VERSION = qq(0.01);
use Bean::XML;

sub VERSION {
    return $VERSION;
}

sub soap_action {}
sub login_soap_action { return $_[0]->soap_action; }

sub default_encoding {
    return 'UTF-8';
}

sub soap_xml {
    my $self = shift;
    my $body_callback_ref = shift;

    my ($header_callback_ref, $xml);
    if ( ref( $body_callback_ref ) eq 'HASH' ) {
        my $options_ref = $body_callback_ref;
        $body_callback_ref = delete $options_ref->{body};
        $xml = delete $options_ref->{xml};
        $header_callback_ref = delete $options_ref->{header};
    }

    $xml ||= Bean::XML->new( ENCODING => $self->default_encoding() );

    $xml->startTag('soap:Envelope',
        'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
        'xmlns:xsd' => 'http://www.w3.org/2001/XMLSchema',
        'xmlns:soap' => 'http://schemas.xmlsoap.org/soap/envelope/',
    );

    if ( $header_callback_ref ) {
        $xml->startTag('soap:Header');
            $self->call_as_method($header_callback_ref, $xml);
        $xml->endTag('soap:Header');
    }

    if ( $body_callback_ref ) {
        $xml->startTag('soap:Body');
            $self->call_as_method($body_callback_ref, $xml);
        $xml->endTag('soap:Body');
    }

    $xml->endTag('soap:Envelope');
    $xml->end();

    return $xml->asString();
}

sub call_as_method {
    my ($self,$callback,@args) = @_;

    if ( ref($callback) ) {
        return $callback->($self, @args);
    }

    return $self->$callback(@args);
}

sub login_post {
    my $self = shift;
    my $url  = shift;
    my @options = @_;

    my $soap_action = $self->login_soap_action();
    if ( defined( $soap_action ) ) {
        $self->log_debug('SOAPAction is: %s', $soap_action);
        push @options, 'SOAPAction' => qq("$soap_action");
    }
    else {
        $self->log_debug('No SOAPAction');
    }

    $self->log_debug('Submitting post to %s (%d options)', $url, scalar(@options));

    return $self->post(
        $url,
        @options,
    );
}

sub logout {}

sub search_soap_action { return $_[0]->soap_action; }
sub search_post {
    my $self = shift;
    my $url  = shift;
    my @options = @_;



    my $soap_action = $self->search_soap_action();
    if ( defined( $soap_action ) ) {
        $self->log_debug('SOAPAction is: %s', $soap_action);
        push @options, 'SOAPAction' => qq("$soap_action");
    }
    else {
        $self->log_debug('No SOAPAction');
    }

    $self->log_debug('Submitting post to %s (%d options)', $url, scalar(@options));

    return $self->post(
        $url,
        @options,
    );
}

sub download_cv_soap_action { return $_[0]->soap_action; }
sub download_cv_post {
    my $self = shift;
    my $url  = shift;
    my @options = @_;

    my $soap_action = $self->download_cv_soap_action();
    if ( defined( $soap_action ) ) {
        $self->log_debug('SOAPAction is: %s', $soap_action);
        push @options, 'SOAPAction' => qq("$soap_action");
    }
    else {
        $self->log_debug('No SOAPAction');
    }

    $self->log_debug('Submitting post to %s (%d options)', $url, scalar(@options));

    return $self->post(
        $url,
        @options,
    );
}

sub download_profile_soap_action { return $_[0]->soap_action; }
sub download_profile_post {
    my $self = shift;
    my $url  = shift;
    my @options = @_;

    my $soap_action = $self->download_profile_soap_action();
    if ( defined( $soap_action ) ) {
        $self->log_debug('SOAPAction is: %s', $soap_action);
        push @options, 'SOAPAction' => qq("$soap_action");
    }
    else {
        $self->log_debug('No SOAPAction');
    }

    $self->log_debug('Submitting post to %s (%d options)', $url, scalar(@options));

    return $self->post(
        $url,
        @options,
    );
}

sub engine_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    if ( !defined( $content ) ) {
        $content = $self->content();
        return unless $content;
    }

    if ( $content =~ m{<faultstring>([^<]+)</faultstring>}i ) {
        my $faultstring = $1;

        if ( $faultstring =~ m/Timeout expired/ ) {
            return $self->throw_timeout( $faultstring );
        }

        return $faultstring;
    }

    return;
}

1;

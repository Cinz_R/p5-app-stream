#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: XML.pm 25910 2009-10-23 16:00:34Z andy $

package Stream::Engine::API::XML::FVP;

=head1 NAME

Stream::Engine::API::XML::FVP - XML CvSearch robot and scraper

=cut

use strict;

use base qw(Stream::Engine::API::XML);

our $VERSION = qq(0.01);

=head1 OVERVIEW

Use when we should send HTTP field-value pairs and they return XML.

This is geared to xml=<XML> style requests rather than REST like requests.

Also see L<Stream::Engine::API::XML>

=head1 CLASS METHODS

=head2 VERSION (GENERIC)

Current version of this module

=cut

sub VERSION {
    return $VERSION;
}

=head1 DEFAULT PROPERTIES

=head2 default_content_type (GENERIC/MANDATORY)

Fallback content type used if not defined.

Default is application/x-www-form-urlencoded

=cut

sub default_content_type { return 'application/x-www-form-urlencoded'; }

# New methods for this subclass

=head2 default_http_method (GENERIC)

HTTP method to use. GET and POST are supported. POST is the default.

=cut

sub default_http_method           { return 'POST'; }
sub search_http_method            { return $_[0]->default_http_method(); }
sub download_cv_http_method       { return $_[0]->default_http_method(); }
sub download_profile_http_method  { return $_[0]->default_http_method(); }

=head2 default_xml_param (GENERIC)

Field name to send the xml in. Default is C<xml> (ie. xml=<xml....>)

=cut

sub default_xml_param             { return 'xml'; }
sub search_xml_param              { return $_[0]->default_xml_param(); }
sub download_cv_xml_param         { return $_[0]->default_xml_param(); }
sub download_profile_xml_param    { return $_[0]->default_xml_param(); }


=head1 LOGIN METHODS

Not done yet, todo todo todo. I haven't needed them yet. Login methods in Stream::Engine::API::XML
need refactoring first so we can subclass them effectively

=head1 SEARCH METHODS

=head2 search_submit (GENERIC)

Builds the search xml and submits it to the search engine

=over

=item * search_content (GENERIC)

Builds the field-value pairs request. Should return an hash or array.

The default returns an array containing:

 ( $self->search_xml_param() => $self->search_build_xml() )

=item * search_url (GENERIC)

Web address to submit the xml to (fallsback to C<default_url>)

=item * search_content_type

Content type of http request (fallsback to C<default_content_type>)

=item * search_http_method

HTTP method to use. Either GET or POST supported (fallsback to C<default_http_method>)

=back

=cut

sub search_submit {
    my $self = shift;

    my @search_content = $self->search_content();

    my $http_method = uc($self->search_http_method());
    if ( $http_method eq 'GET') {
        my $search_uri = URI->new($self->search_url());
        $search_uri->query_form( \@search_content );
        return $self->search_get( $search_uri );
    }

    my @request = (
        $self->search_url(),
        Content_Type => $self->search_content_type(),
        Content      => \@search_content,
    );

    # Assume HTTP post
    return $self->search_post( @request );
}

sub search_content {
    my $self = shift;

    my $xml_param = $self->search_xml_param();

    my $xml = $self->xml_to_string( $self->search_build_xml() );

    if ( !$xml ) {
        return $self->throw_error('No xml to send to the board');;
    }

    return ( $xml_param => $xml );
}

=head1 DOWNLOAD PROFILE METHODS

=head2 download_profile_submit (GENERIC)

Builds the profile xml and submits it to the search engine

=over

=item * download_profile_content (GENERIC)

Builds the field-value pairs request. Should return an hash or array.

The default returns an array containing:

 ( $self->download_profile_xml_param() => $self->download_profile_xml() )

=item * download_profile_url (GENERIC)

Web address to submit the xml to (fallsback to C<default_url>)

=item * download_profile_content_type

Content type of http request (fallsback to C<default_content_type>)

=item * download_profile_http_method

HTTP method to use. Either GET or POST supported (fallsback to C<default_http_method>)

=back

=cut

sub download_profile_submit {
    my $self = shift;
    my $candidate = shift;

    my @profile_content = $self->download_profile_content($candidate);

    my @request = (
        $self->download_profile_url(),
        Content_Type => $self->download_profile_content_type(),
        Content      => \@profile_content,
    );

    my $http_method = uc($self->download_profile_http_method());
    if ( $http_method eq 'GET') {
        return $self->download_profile_get( @request );
    }

    # Assume HTTP post
    return $self->download_profile_post( @request );
}

sub download_profile_content {
    my $self = shift;
    my $candidate = shift;

    if ( !$self->can('download_profile_xml') ) {
        return $self->throw_profile_not_supported('No "download_profile_xml" method to build xml request');
    }
 
    my $xml_param = $self->download_profile_xml_param();

    my $xml = $self->xml_to_string( $self->download_profile_xml( $candidate ) );

    if ( !$xml ) {
        return $self->throw_error('No xml to send to the board');
    }

    return ( $xml_param => $xml );
}

=head1 DOWNLOAD CV METHODS

=head2 download_cv_submit (GENERIC)

Builds the download cv xml and submits it to the search engine

=over

=item * download_cv_content (GENERIC)

Builds the field-value pairs request. Should return an hash or array.

The default returns an array containing:

 ( $self->download_cv_xml_param() => $self->download_cv_xml() )

=item * download_cv_url (GENERIC)

Web address to submit the xml to (fallsback to C<default_url>)

=item * download_cv_content_type

Content type of http request (fallsback to C<default_content_type>)

=item * download_cv_http_method

HTTP method to use. Either GET or POST supported (fallsback to C<default_http_method>)

=back

=cut

sub download_cv_submit {
    my $self = shift;
    my $candidate = shift;

    my @cv_content = $self->download_cv_content($candidate);

    my @request = (
        $self->download_cv_url(),
        Content_Type => $self->download_cv_content_type(),
        Content      => \@cv_content,
    );

    my $http_method = uc($self->download_cv_http_method());
    if ( $http_method eq 'GET') {
        return $self->download_cv_get( @request );
    }

    # Assume HTTP post
    return $self->download_cv_post( @request );
}

sub download_cv_content {
    my $self = shift;
    my $candidate = shift;

    my $xml_param = $self->download_cv_xml_param();

    my $xml = $self->xml_to_string( $self->download_cv_xml( $candidate ) );

    if ( !$xml ) {
        return $self->throw_error('No xml to send to the board');
    }

    return ( $xml_param => $xml );
}

1;

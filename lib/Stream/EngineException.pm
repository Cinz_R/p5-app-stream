package Stream::EngineException;
use Moose;
use Carp;

use Stream2;

use String::CamelCase qw//;

## Useful in tests
## Dangerous in real life.
# use overload '""' => \&to_string;

=head1 NAME

Stream::EngineException - An exception thrown by a candidate source feed

=head1 ATTRIBUTES

=head2 message

An as human readable as possible message. To be displayed.

=head2 stack_trace

AKA guru meditation.

=head2 human_message

A slightly enhanced human readable message.

=head2 error_string

An error class descriptive string

=cut

has 'message' => ( is => 'ro' , isa => 'Maybe[Str]', required => 1 );
has 'stack_trace' => ( is => 'ro' , isa => 'Str' , required => 1 );
has 'error_properties' => ( is => 'ro', isa => 'HashRef', default => sub { {} });

around BUILDARGS => sub{
    my ($orig, $class, $args) = @_;
    $args->{stack_trace} //= Carp::longmess($args->{message} // 'No specific message');
    return $class->$orig($args);
};

sub shortclass{
    my ($self) = @_;
    my $short_class = ref($self);
    $short_class =~ s/^Stream::EngineException:://;
    return $short_class;
}

sub error_string{
    return Stream2->instance->translations->__p("Engine Error", "General Exception");
}

sub human_message{
    my ($self) = @_;
    return join(' ' , $self->error_string().': '.$self->message() );
}

sub to_string{
    my ($self) = @_;
    return $self->message()."\n\n".$self->stack_trace();
}

=head2 error_calendar_id

The string to go in the error calendar. You can get the full list of strings
using the shell:

  s2 [app-stream2.jerome.conf]=>
  Dumper($s2->aws_elastic_search->search( index => 'threads', body => { facets => { error_type => { "terms" => { "field" => "error_type" , size => 50  }}}} ));

=cut

sub error_calendar_id{
    my ($self) = @_;
    return uc( join(' ' , String::CamelCase::wordsplit($self->shortclass())) );
}

__PACKAGE__->meta->make_immutable();

## All subclasses are defined here, as they only differ by their name and error_string
package Stream::EngineException::LoginError;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Login Error"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::Timeout;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Time out"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::Critical;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Critical Error"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::Internal;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Internal Error"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::InactiveAccount;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Inactive Account"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::NoCVSearch;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "No CV Search available"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::LackOfCredit;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Not enough credits available"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::AccountInUse;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Account already in use"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::TooManyResults;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Too many results"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::InvalidBooleanSearch;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Invalid Boolean Search"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::KeywordsTooLong;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Keywords too long"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::Unavailable;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Service Unavailable"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::InvalidEmailAddress;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Invalid Email Address"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::CVUnavailable;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "CV Unavailable"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::NotSupported;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Not Supported"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::ProfileNotSupported;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Profile Not Supported"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::CVNotReady;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "CV not ready"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::NoCV;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "No CV"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::WatchdogNotSupported;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Watchdog not supported"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::QuotaLimitExceeded;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Quota limit exceeded"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::ShortlistingError;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Shortlisting error"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::ForwardingError;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Forwarding error"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::MessagingError;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Messaging error"); }
__PACKAGE__->meta->make_immutable();

package Stream::EngineException::ImportingError;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Importing error"); }
__PACKAGE__->meta->make_immutable();
1;

package Stream::EngineException::TaggingError;
use Moose; extends qw/Stream::EngineException/;
sub error_string{ Stream2->instance->translations->__p("Engine Error" , "Tagging error"); }
__PACKAGE__->meta->make_immutable();
1;

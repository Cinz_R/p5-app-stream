package Stream::Engine;

=head1 NAME

Stream::Engine - Base class for CvSearch engines

=head1 AUTHOR

ACJ - 10/09/2008

=cut

use base qw(Stream::Templates::Default);

use strict;

use Carp;

use Encode;

use List::Util qw(first);
use Stream::SharedUtils;
use Stream::Scraper::Guess;

use Stream::EngineException;

use Stream2::Results;

use Log::Any qw/$log/;

## BASE ATTRIBUTES ##

=head1 OVERVIEW

Each subclass of this should be used in a transient manner.
Do NOT keep instances of Stream::Engine (Stream::Templates:*) around.

Generally the cv search templates are harder to follow than posting
templates because they have more work to do.

A typical cv search robot has to:

=over

=item * login

=item * navigate to cv search page

=item * fill in and submit cv search form

=item * extract candidate information from cv search results

=item * log out

=back

Separate routines handle downloading CVs and downloading candidate
profiles.

Each step in the process can vary - e.g. some logins involve http auth,
others sites require cookies to authenticate.

=head2 Web Roboting

Either based on C<LWP::UserAgent> or C<WWW::Mechanize>. Both will
store cookies automatically.

=head2 Scraping/Xpath

The CV Search uses a lot of Xpath to scrape both XML and HTML.

Here are a couple of introductions to xpath:

L<http://www.zvon.org/xxl/XPathTutorial/General/examples.html>
L<http://en.wikipedia.org/wiki/XPath>
L<http://www.w3schools.com/XPath/default.asp>

and here is a page that can test out xpaths to make sure they work.

L<http://v4.adcourier.com/test_scripts/test_xpath.cgi>

If an xpath is not matching, check to see if it has any namespaces
declared in the xml (ie. xmlns=""...)

Then search the code/docs for lots of rants about the default namespace.

=cut

our $VERSION = 0.01;

=head1 Class Methods

=head2 VERSION (GENERIC)

Returns the current version of this module

=cut

sub VERSION {
    return $VERSION;
}

=head2 USER_AGENT (GENERIC)

The default agent value that is sent in the C<User-Agent> header
in http requests

=cut

sub USER_AGENT {
    my $class = shift || __PACKAGE__;
    return 'Bean-CvSearch/'. $class->VERSION;
}

=head2 AGENT_CLASS

Agent class to use (Screenscrapers use Stream::Agents::Mechanize, XML default to Stream::Agents::XML)

=cut

sub AGENT_CLASS {
    die "Subclass me!";
}

sub AGENT_TIMEOUT {
    return 60; # timeout each request after 60 seconds
}

# defend against very large responses
# some nutjob has a 100mb CV on CV-Library.
# stream used 4gb memory to scrape/decode and save a 100mb CV from CV-Library
# (log-candidate-201845636-1343203889-20605)
sub AGENT_MAX_SIZE {
    return 20971520; # 20mb
}

# Overwrite to add error handling to the Juice APIs.
# Clone them as not to affect the rest of the application.
sub new {
    my ($self, $tokens_ref, $attributes) = @_;

    my $handler = sub {
        my ($response, $ua, $h) = @_;
        if($response->decoded_content  =~ m/AdCourier is currently unavailable/) {
            $self->throw_unavailable(
                'This search is temporarily unavailable. Please try again shortly.'
            );
        }
        return;
    };

    my $ua;
    foreach my $key (qw(location_api user_api docments_api)) {
        my $api = $attributes->{$key} or next;
        # Use a clone of the first UA.
        $ua //= $api->ua->clone;
        $ua->add_handler(response_done => $handler);
        $attributes->{$key} = $api->clone({ua => $ua});
    }

    return $self->SUPER::new($tokens_ref, $attributes);
}

sub log_lwp_ua {
    my ($self, $agent) = @_;

    $agent->add_handler(
        response_done => sub {
            my ($response, $agent, $h) = @_;
            my $request = $response->request;
            $self->log_debug(join("\n",
                "===== REQUEST =====",
                $request->as_string,
                "\n\n===== RESPONSE =====\n",
                $self->response_as_string($response)
            ));
            return;
        }
    );
}

sub PROXY_CLASS {
    return;
}

sub proxy {
    my $class = shift;

    my $proxy_class = $class->PROXY_CLASS()
        or return '';

    if ( $proxy_class ) {
        my $ok = eval "use $proxy_class; 1;";
        if ( !$ok ) {
            $log->error("Cannot load proxy class $proxy_class");
            return '';
        }

        if ( UNIVERSAL::can($proxy_class,'new') ) {
            return $proxy_class->new();
        }
    }

    return '';
}

sub basic_username {} # username used in basic http auth
sub basic_password {} # password for basic http auth

=head2 Mutators

=head2 logger (GENERIC)

Gets/sets the current log object (see L<Bean::Log>)

=cut

# Install logging methods
BEGIN {
    my %log_methods = (
        log_trace       => 'trace',
        log_debug       => 'debug',
        log_info        => 'info',
        log_warn        => 'warn',
        log_error       => 'error',
        log_mark        => 'info',
        log_request     => 'debug',
        log_response    => 'debug',
        log_transaction => 'debug',
        log_sql         => 'trace',
        log_id          => 'trace',
    );

    while (my ($method, $log_method) = each %log_methods ) {
        # is_.. methods from Log::Any
        my $is_method = 'is_'.$log_method;
        no strict 'refs';
        *{__PACKAGE__.'::'.$method} = sub {
            my $self = shift;
            my $logger = $self->logger() or return undef;
            unless( $logger->$is_method() ){
                return undef;
            }

            my @message_bits = map {
                ref( $_ ) eq 'ARRAY' ? join (', ', @$_ ) : $_;
            } map {
                defined($_) ? $_ : 'undef'
            } @_; # Avoid undef warnings.

            # This is good.
            my $message = shift( @message_bits );
            if( scalar( @message_bits ) ){
                $message = sprintf( $message , @message_bits );
            }
            $logger->$log_method( $message );
        };
    }
};

=head2 logger

Returns an instance of Log::Any for this class.

This is deprecated as we prefer using 'use Log::Any qw/$log/;'
everywhere possible.

Usage:

  my $log = $this->logger();

=cut

sub logger {
    my ($self) = @_;
    # Return a logger with the most bottom category (like Stream::Templates::blabla)
    # Beware of class issues.
    return Log::Any->get_logger( category =>  ref($self) || $self );
}

# IP address of person who originally ran the search
# May be different to the IP of the person running the search
# if they are using more results/refresh results/refresh search or watchdogs
sub creator_ip {
    return $_[0]->{creator_ip} if @_ == 1;
    return $_[0]->{creator_ip} = $_[1];
}

=head2 results (GENERIC)

Gets/sets the current results bucket object (for instance, in Stream2, that's a  L<Stream2::Results>).

=cut

sub results {
    if ( @_ == 1 ) {
        return $_[0]->{results} ||= $_[0]->default_results();
    }

    return $_[0]->{results} = $_[1];
}

sub default_results {
    my $self = shift;

    my $default_result_class = $self->default_result_class();
    $self->log_debug('Default results class is [%s]', $default_result_class);

    return $default_result_class->new({
        destination          => $self->destination(),
        logger               => $self->logger(),
        cv_fed_search_id     => $self->cv_fed_search_id(),
        cv_run_id            => $self->cv_run_id(),
        company              => $self->company(),
        office               => $self->office(),
        team                 => $self->team(),
        consultant           => $self->consultant(),
    });
}

sub default_result_class {
    my $self = shift;
    if ( exists( $ENV{STREAM_RESULTS_CLASS} ) ) {
        my $env_class = $ENV{STREAM_RESULTS_CLASS};

        if ( $env_class ) {
            return $env_class;
        }
    }

    $self->log_debug('Search type is [%s]', $self->type() );
    if ( (  $self->type()  // '' ) eq 'refresh' ) {
        confess("No such thing as Stream::Results::WatchdogResult anymore. Think about it and come back");
        # $self->log_debug('Using Watchdog results class because we are refreshing the results');
        # require Stream::Results::WatchdogResults;
        # return 'Stream::Results::WatchdogResults';
    }
    return 'Stream2::Results';
}

=head2 criteria

Sets/gets a Stream2::Criteria object in the search feed.

Usage:
    # Set
    $feed->criteria($criteria_object);

    # Get
    my $criteria = $feed->criteria();
    $criteria->id();

Note: If access to the criteria is required in actions other than
'search', you may find caching a dear friend.

=cut

sub criteria {
    my ($self, $criteria) = @_;
    if ( $criteria ) {
        $self->{criteria} = $criteria;
    }
    return $self->{criteria};
}

# Changing to a getter/setter so the watchdog runner can set it easily
# This appears to be important in the cvlibrary feed
sub is_watchdog {
    my $self = shift;
    if ( scalar( @_ ) ) {
        $self->{_is_watchdog} = shift;
    }

    unless ( defined $self->{_is_watchdog} ){
        if ( $self->type() eq 'refresh' ) {
            if ( $ENV{ADC_DEVUSER} eq 'stream_watchdog' ) {
                return 1;
            }
        }
        if ( $0 =~ m/^cv_watchdog/ ) {
            return 1;
        }
    }

    return $self->{_is_watchdog};
}

=head2 cv_fed_search_id (GENERIC)

Gets/sets the current cv_fed_search_id
(see L<Stream::Core>)

=cut

sub cv_fed_search_id {
    if ( @_ == 1 ) {
        if ( $_[0]->{cv_fed_search_id} ) {
            return $_[0]->{cv_fed_search_id};
        }

        if ( $_[0]->{parent} ) {
            return $_[0]->{parent}->cv_fed_search_id();
        }

        return undef;
    }

    $_[0]->{cv_fed_search_id} = $_[1];
}

=head2 cv_run_id (GENERIC)

Gets/sets the current cv_run_id
(see L<Stream::Core::Run>)

=cut

sub cv_run_id {
    if ( @_ == 1 ) {
        if ( $_[0]->{cv_run_id} ) {
            return $_[0]->{cv_run_id};
        }

        if ( $_[0]->{parent} ) {
            return $_[0]->{parent}->cv_run_id();
        }

        return undef;
    }

    $_[0]->{cv_run_id} = $_[1];
}

=head2 type (GENERIC)

Gets/sets the type of search to run. Can be either more_results or refresh_results
(see L<Stream::Core>)

=cut

sub type {
    if ( @_ == 1 ) {
        if ( $_[0]->{type} ) {
            return $_[0]->{type};
        }

        if ( $_[0]->{parent} ) {
            return $_[0]->{parent}->type();
        }

        return undef;
    }

    $_[0]->{type} = $_[1];
}

=head2 current_candidate (GENERIC)

Gets/sets the candidate that we are downloaded the CV/profile for so it can
be accessed in exception handling

=cut

sub current_candidate {
    return $_[0]->{current_candidate} if @_ == 1;
    return $_[0]->{current_candidate} = $_[1];
}

=head2 total_results (GENERIC)

Returns the total number of candidates found in the search

=cut

sub total_results {
    return $_[0]->{total_results} if @_ == 1;
    $_[0]->{total_results} = $_[1];

    if ( $_[0]->{results} ) {
        $_[0]->{results}->total_results( $_[1] );
    }
}

=head2 max_pages (GENERIC)

Returns the total number of pages in the current search

=cut

sub max_pages {
    my $self = shift;
    if ( my $pages = shift ){
        $self->{max_pages} = $pages;
        $self->results->max_pages( $pages );
    }
    return $self->{max_pages};
}

=head2 total_pages

Because some boards use total_pages and others use max_pages

=cut

sub total_pages {
    shift->max_pages( @_ );
}

=head2 current_page (GENERIC)

Returns the current page number that we are scraping

=cut

sub current_page {
    return $_[0]->{current_page} if @_ == 1;

    return $_[0]->{current_page} = $_[1];
}

=head2 results_offset (GENERIC)

Returns the result offset for the current page.

E.g. There are 20 candidates per page.
The offset for the first page will be 0.
The offset for the second page is 20, third page will be 40 etc

=cut

sub results_offset {
    my $self = shift;
    my $current_page = $self->current_page();
    my $candidates_per_page = $self->results_per_page();

    $self->log_mark('Calculating offset with page %s', $current_page);

    my $offset = ($current_page - 1) * $candidates_per_page;

    if ( $offset < 0 ) {
        $self->log_info('Returning 0 for offset, we might be on the first page');
        return 0;
    }

    $self->log_info('Returning %s for offset', $offset);
    return $offset;
}

=head2 candidates_seen (GENERIC)

Returns the number of candidates we have scrape so far in this search

=cut

sub candidates_seen {
    my $self = shift;
    return $self->{results} ? scalar( $self->results()->n_results() ) : 0;
}

=head2 candidates_found (GENERIC)

Returns the number of new candidates we have scraped so far that we
have not found in previous searches

=cut

sub candidates_found {
    my $self = shift;
    return $self->{results} ? scalar( $self->results()->new_results() ) : 0;
}

=head2 reset_number_of_candidates_found (GENERIC)

Resets the count of new candidates found back to zero. Handy for rerunning search

=cut

sub reset_number_of_candidates_found {
    my $self = shift;
    if ( $self->{results} ) {
        $self->{results}->new_results(0);
        return 1;
    }
    return 0;
}

sub recent_content {
    return $_[0]->{recent_content} if @_ == 1;

    my $self = shift;
    my $new_content = shift;

    $self->before_content_updated($new_content);
    $self->reset_scraper();

    # remove Byte order markers (e.g. monster)
    if ( $new_content =~ s/^\xEF\xBB\xBF// ) {
        $self->log_debug('Removed UTF8 byte order marker from response');
    }

    $self->{recent_content} = $new_content;

    if ( $self->{recent_content} ) {
        $self->after_content_updated();
    }

    return $new_content;
}

sub before_content_updated {}
sub after_content_updated  {}

=head2 content (GENERIC/STUB)

??????? TODO

=cut

sub content {
    return undef;
}

=head2 add_cookie (GENERIC/STUB)

Adds a new cookie. Should work with all LWP::UserAgent based modules

=cut

sub add_cookie {
    my $self = shift;
    my $cookie_name = shift;
    my $cookie_value = shift;
    my $url          = shift;

    if ( !$url ) {
        $url = $self->uri();
    }

    my $uri = URI->new( $url );

    my $port   = $uri->port();
    my $domain = $uri->host();
    my $secure = $uri->scheme() eq 'https' ? 1 : 0;

    my $version = 0;
    my $path = '/';
    my $path_spec;
    my $maxage = undef;
    my $discard;

    $self->log_debug('Adding cookie: [%s] => [%s]', $cookie_name, $cookie_value );

    my $cookie_jar = $self->_cookie_jar();
    $cookie_jar->set_cookie(
        $version,
        $cookie_name,
        $cookie_value,
        $path,
        $domain,
        $port,
        $path_spec,
        $secure,
        $maxage,
        $discard,
    );

    return 1;
}

=head2 _cookie_jar (GENERIC)

Returns the cookie jar object. private

=cut

sub _cookie_jar {
    my $self = shift;
    my $agent = $self->agent();

    return $agent->cookie_jar();
}

=head2 has_authtokens

returns 1 if the feed uses authtokens

=cut

sub has_authtokens {
    my $self = shift;

    my ($authtokens_ref) = $self->authtokens();

    # only fetch the authtokens if they haven't been supplied
    $self->log_debug('checking if we have been supplied with authtokens');
    $self->log_debug('we are expecting %d authtokens', scalar(keys %$authtokens_ref) );

    AUTHTOKEN:
    foreach my $token ( keys %$authtokens_ref ) {
        my (@values) = $self->token_value( $token );

        if ( @values ) {
            # authtokens have been supplied so don't fetch them
            $self->log_debug('we have been supplied with authtoken "%s" => (%s)', $token, join(',',@values) );
            return 1;
        }

        $self->log_debug('we have not been supplied with value for authtoken "%s"', $token );
    }

    $self->log_debug('we have not been supplied with any authtokens' );

    return 0;
}

# overwritable methods

#
# The cvsearch "engine"
#
=head2 destination (GENERIC)

Returns effectively the template name

=cut

sub destination {
    my $self = shift;

    my $class = ref($self) || $self;

    $class =~ s/^.*:://;

    return $class;
}


## Login ##

=head1 Workflow Methods

Methods that control how we cvsearch the site

=head2 login (STUB/MANDATORY)

Stub to login to sites. Should be overwritten in your subclass

Should die if the login fails

=cut

sub login {
    my $self = shift;
    $log->info('No login method implemented');
}  # stub

=head2 logged_in

Returns true if a user is already logged in.

=cut

sub logged_in {
    return $_[0]->{logged_in};
}

=head3 check_for_login_error (GENERIC)

Checks last response to see if we are logged in.
Customisabled with C<login_success> and C<login_failed>

Throws an 'Unable to login' error if it fails
Returns nothing if successful or if there is no success or
failure messages defined

=cut

sub check_for_login_error {
    my $self = shift;

    my $resp = $self->response();

    my $decoded_content;
    my $login_success = $self->login_success();
    if ( defined( $login_success ) ) {
        $decoded_content ||= $resp->decoded_content();
        $self->log_debug('Checking for success message [%s]', $login_success);
        if ( $decoded_content !~ m/$login_success/ ) {
            $self->log_debug('Success message NOT found');
            $self->throw_login_error( $self->user_object->stream2->__('Unable to login') );
        }

        $self->log_debug('Success message found');
    }

    my $login_failed = $self->login_failed();
    if ( defined( $login_failed ) ) {
        $decoded_content ||= $resp->decoded_content();
        $self->log_debug('Checking for failure message [%s]', $login_failed);
        if ( $decoded_content =~ m/$login_failed/ ) {
            $self->log_debug('Failure message found');
            $self->throw_login_error( $self->user_object->stream2->__('Unable to login') );
        }

        $self->log_debug('No failure message found');
    }

    return;
}

=over 4

=item * login_success (STUB/OPTIONAL)

If defined, C<check_for_login_error> will fail the search
unless the content matches this regex

e.g C<return qr/Logged in as/;>

=cut

sub login_success {}

=item * login_failed (STUB/OPTIONAL)

If defined, C<check_for_login_error> will fail the search
if the content matches this regex

e.g C<return qr/Please enter a username/;>

=back

=cut

sub login_failed {}



## Logout ##

=head2 logout (STUB/MANDATORY)

Called after the CV search is finished. Should be overwritten
in your subclass

=cut

sub logout{}


sub candidates_required { return 10; } # default to the number of candidates of their first search page

=head3 field_value_pairs

Returns a hash containing a query_form suitable to pass into URI->query_form()

Substitutes in tokens and removes an field without a value

=cut
sub field_value_pairs {
    my $self = shift;

    my @field_value_pairs;

    FIELD:
    while ( scalar(@_) ) {
        my $field = shift @_;
        my $value = shift @_;

        next FIELD unless $field;

        my (@values) = $self->fill_in_tokens( $value );

        # skip empty fields
        if ( !@values || (scalar(@values)==1 && $values[0] eq '') ) {
            next FIELD;
        }

        foreach my $filled_value ( @values ) {
            $filled_value =~ s/BBTECH_RESERVED_NULL//g;
            push @field_value_pairs, $field, $filled_value;
        }
    }

    return @field_value_pairs;
}

sub to_querystring {
    my $self = shift;

    my @field_value_pairs;
    if ( @_ == 1 ) {
        my $ref  = shift;

        if ( !ref( $ref ) ) {
            return $ref;
        }

        (@field_value_pairs) = ref( $ref ) eq 'HASH' ? %$ref : @$ref;
    }
    else {
        (@field_value_pairs) = (@_);
    }

    my @pairs;
    while ( @field_value_pairs ) {
        my $field = shift @field_value_pairs;
        my $value = shift @field_value_pairs;

        require URI::Escape;
        my $escaped_value = URI::Escape::uri_escape( $value );

        push @pairs, sprintf '%s=%s', $field, $escaped_value;
    }

    my $URI_SEPARATOR = '&';
    return join($URI_SEPARATOR, @pairs);
}


=head2 search (GENERIC/MANDATORY)

Generic loop that will cv search until an error occurs or
we find enough candidates

=cut

sub register_callback{
    my $self = shift;
    my $callback = shift;
    $self->{'callback'} = $callback; # this will be called after each candidate is scraped
}

sub do_callback {
    my $self = shift;
    my @args = (@_);

    if ( $self->{callback} ) {
        my $callback_ref = $self->{callback};
        eval {
            $callback_ref->( @args );
        };
        if ( $@ ) {
            $self->log_warn('Problem calling callback: [%s]', $@);
        }
    }

    return 1;
}

sub callback_ref {
    my $self = shift;
    if ( ref( $self ) ) {
        # object method
        return sub { $self->do_callback( @_ ) };
    }

    # class method, no callbacks
    return;
}

sub page_ratio_modifier { #overwrite me?
    my $self = shift;
    my $options_ref = shift;

    my $modifier = 1;

    my $candidates_required = $self->candidates_required();

    my $local_page_desired = $options_ref->{page}
        or return;

    my $board_page = $local_page_desired;

    my $results_per_scrape = $self->results_per_scrape() #overwrite me?
        || $self->results_per_scrape($options_ref->{results_per_scrape});

    if ( $results_per_scrape && $candidates_required > $results_per_scrape ) { #we know we had to scrape more than one board page
        my $ratio = ( $candidates_required / $results_per_scrape );
        $modifier = $ratio == int $ratio ? $ratio : int ( $ratio + 1 );
        $board_page = ( ( $local_page_desired - 1 ) * $modifier ) + 1;
    }

    my $max_pages = $self->max_pages;
    my $total_results = $self->total_results;
    if ( !$max_pages && $results_per_scrape && $total_results ){
        my $max_pages_est = $total_results / $results_per_scrape;
        $max_pages = ( int($max_pages_est) == $max_pages_est ) ? $max_pages_est : int($max_pages_est + 1);
    }


    if ( $max_pages && $board_page > $max_pages ) {
        $self->throw_error("required board page $board_page is beyond our estimated page limit of $max_pages for the board");
    }

    #returns the starting point for the search
    return ( ( $local_page_desired - 1 ) * $modifier ) + 1;
}

=head2 search

Main entry point.

Performs a search on the remote source. Return whatever $self->results() is.
(in Stream2, that would be a L<Stream2::Results>).

=item The methods that you have to implement to have this working:

search_specific_page

search_begin_search

results_per_page

=item The methods that you can implement to have this working:

login

Usage:

  my $results = $this->search();

Options include:

 'callback': A sub that will be called after each candidate is found.

=cut

sub search {
    my $self = shift;
    my $options_ref = shift || {};

    if ( $options_ref->{'callback'} ) {
        $self->register_callback( $options_ref->{'callback'} );
    }

    $log->debug( 'Checking if we are already logged in' );

    $self->login();

    $self->reset_number_of_candidates_found();

    if ( defined( $options_ref->{'page'} ) && $options_ref->{'page'} > 1 && $self->supports_paging() ) {
        my $specific_page = $self->page_ratio_modifier( $options_ref );
        $self->search_specific_page( $specific_page );
    }
    else { #start from the beginning
        $self->search_begin_search();
    }

    my $failsafe = 0; #instead of relying on the current_page
    PAGE:
    while ( 1 ) {
        $failsafe++;

        if ( !$self->candidates_seen() ) {
            $log->info('Stopping multi-page search');
            last PAGE;
        }

        if ( $self->candidates_found() >= $self->candidates_required() ) {
            $log->infof('Stopping search: looking for %d candidates, found %d candidates',
                $self->candidates_required(), $self->candidates_found(),
            );
            last PAGE;
        }

        $log->debugf(
            'Found %d candidates, we need to find %d candidates. Lets look for more',
            $self->candidates_found(), $self->candidates_required(),
        );

        my $total_results = $self->total_results();
        if ( defined( $total_results ) && $self->candidates_seen >= $total_results ) {
            $log->infof('Stopping search: we have fetched %d out of %d results for this search',
                $self->candidates_seen(), $self->total_results(),
            );
            last PAGE;
        }

        $log->debugf(
            'Found %d out of %d candidates. %d of these candidates are new. There are more candidates in this search',
            $self->candidates_seen(), $self->total_results(), $self->candidates_found(),
        );

        my $total_pages = $self->max_pages();
        if ( defined( $total_pages) && $self->current_page() >= $total_pages ) {
            $log->infof('Stopping search: we have fetched %d pages out of %d for this search',
                $self->current_page(), $self->max_pages(),
            );

            last PAGE;
        }

        $log->infof('Searching page %d of %d',
            $self->current_page(), $self->max_pages(),
        );

        if ( $failsafe > 5 ) { #commented out on live server
            $self->throw_error("Pager probably isn't working like it should! We seem to be in a loop");
        }

        my $candidates_seen = $self->candidates_seen(); #  number of candidates found so far
        if ( !$self->search_next_page() ) {
            $log->infof(
                'Stopping search! Either reached the last page or something went wrong searching page %d of %d',
                $self->current_page(), $self->max_pages(),
            );
            last PAGE;
        }

        my $results_per_scrape = $self->results_per_scrape();
        my $results_last_search = $self->results_last_search();
        $log->debugf("RESULTS_PER_SCRAPE: %s",$results_per_scrape);
        $log->debugf("RESULTS LAST: %s", $results_last_search);
        if ( $results_per_scrape && $results_last_search < $results_per_scrape ){ #we found less than we should have, last page?
            $self->log_warn(
                'Stopping search! We did not find the expected amound of candidates on the last search, is it the last page?'
            );
            last PAGE;
        }

        if ( $candidates_seen == $self->candidates_seen() ) { # have we found any more candidates after scraping another page
            $log->warn(
                'Stopping search! We did not find any candidates on the last page - is scraping broken?',
            );
            last PAGE;
        }

    }

    $self->logout();

    return $self->results();
}

=head3 search_begin_search (STUB/MANDATORY)

Stub that should send the cv search request. It should
parse the first page of results and identify the total number of
candidates found in the search and the number of pages in the search.

It should set some stuff like:

$self->max_pages();

=cut

sub search_begin_search { ... }

=head3 search_next_page (STUB/MANDATORY)

Stub that should submit the next page of results and parse the
search

=cut

sub search_next_page { die 'stub: overwrite me!'; }

=head2 download_cv

Returns a HTTP response that we can save. Overwrite do_download_cv instead

=cut

sub download_cv {
    my $self = shift;
    my $candidate = shift;

    $self->before_downloading_cv($candidate);

    my $resp = $self->do_download_cv($candidate);

    return $self->after_downloading_cv($candidate, $resp);
}

=head3 update_candidate

If the board allows the editing of candidates, such as TP2

=cut

sub update_candidate { ... }

=head3 before_downloading_cv

Called before downloading any CV

=cut

sub before_downloading_cv {
    my $self = shift;
    my $candidate = shift;
}

=head3 do_download_cv (OPTIONAL/STUB)

Downloads the CV from the board and returns a HTTP::Response object.

Note that this HTTP::Response object WILL NOT be returned to the interface directly.

See method 'download_cv' for more details about what's going on.

=cut

sub do_download_cv {
    my $self = shift;
    my $candidate = shift;

    return;
}

=head3 after_downloading_cv

Called after downloading any CV

It returns a fresh HTTP::Response object
that doesn't contain the chain of requests
and it doesn't contain any code ref handlers that LWP may add

This is important as any code refs will prevent the response
from being serialised back across gearman

=cut

sub after_downloading_cv {
    my $self = shift;
    my $candidate = shift;
    my $resp = shift or return;


    my $raw_text = $resp->decoded_content();
    my $filename = $resp->filename();
    my $content_type = $resp->content_type();

    my $http_response =  $self->to_response_attachment({
                                                        decoded_content => $raw_text,
                                                        filename        => $filename,
                                                        content_type    => $content_type,
                                                       });

    # Also transfer the extra headers that might have been set.
    $resp->headers()->scan(sub{
                               my ($name, $value) = @_;
                               unless( $name =~ /^X/ ){ return; }
                               $http_response->header($name ,$value);
                          });

    $self->logout();

    return $http_response;
}

=head3 download_cv_requires_login

Should we login to the site before downloading the CV search

Default is YES

=cut

sub download_cv_requires_login { 1; }

=head3 download_cv_success (OPTIONAL)

The response is only counted as successful if it contains this regex

=cut

sub download_cv_success {}

=head3 download_cv_failed (OPTIONAL)

Any response that contains this response is treated as a failure

=cut

sub download_cv_failed  {}

=head2 scrape_generic_download_cv

Scrapes xml using xpath to extract the CV. Used by Stream::Engines::XML but generic enough
that Stream::Engines::ScreenScraper feeds could use it.

=over

=item * scrape_download_cv_xpath (MANDATORY)

Xpath to identify candidate node

=item * scrape_download_cv_details (GENERIC)

reference containing xpaths

=back

=cut

sub scrape_download_cv_xpath {}
sub scrape_download_cv_details {}

sub scrape_generic_download_cv {
    my $self = shift;
    my $candidate = shift;

    $self->log_info('Generic download cv scraper: template is %s', ref($self) );

    if ( $self->can('before_scrape_download_cv' ) ) {
        $self->log_debug('Calling hook "before_scrape_download_cv"');
        $self->before_scrape_download_cv( $candidate );
    }

    my $candidate_node_xpath = $self->scrape_download_cv_xpath();
    if ( !$candidate_node_xpath ) {
        $self->log_warn('$self->scrape_download_cv_xpath() is empty - no generic scraper defined');
        return undef;
    }

    my (@candidate_xpath) = $self->scrape_download_cv_details();
    if ( !@candidate_xpath ) {
        $self->log_warn('$self->scrape_candidate_details() is empty - NOT SCRAPING');
        return undef;
    }

    $self->log_debug('Result node xpath is: %s', $candidate_node_xpath );

    my $scrape_ok = 0;

    NODE:
    foreach my $candidate_node ( $self->findnodes( $candidate_node_xpath ) ) {
        $log->debug('Scraping a candidate node');

        my %attributes = $self->scrape_generic_node( $candidate_node, \@candidate_xpath );
        if ( !$self->scrape_found_attributes( %attributes ) ) {
            $log->warn('No candidate information found in this node');
            next NODE;
        }

        $log->debug( 'Found some attributes for this candidate' );

        $candidate->attributes(
            %attributes,
        );

        $scrape_ok = 1;
    }

    if ( $scrape_ok ) {
        if ( $self->can('after_scrape_download_cv_success' ) ) {
            $log->debug('Calling hook "after_scrape_download_cv_success"');
            $self->after_scrape_download_cv_success( $candidate );
        }
    }

    $log->debug('Finished scraping CV' );

    return $candidate;
}

=head2 to_response_attachment

Creates a fresh HTTP::Response from the given options.

Mainly used by the 'download_cv' method.

Options include:

 - filename - The name of the attached file, typically a CV. (MUST)

 - decoded_content - The binary content of the file. (MUST)

 - content_type - The MIME Type of the file (MUST)

=cut

sub to_response_attachment {
    my $self = shift;
    my $options_ref = shift || {};

    $self->log_debug("Creating a fake response with a content type [%s]", $options_ref->{content_type});
    $self->log_debug("and content-disposition [%s]", $options_ref->{filename});

    require HTTP::Response;
    my $resp = HTTP::Response->new(
        200,
        'OK',
    );

    my $escaped_filename = $options_ref->{filename};
    $escaped_filename =~ s/"//g; # remove double quotes just in case as many browsers don't handle them

    $resp->header('Content-Disposition' => qq(attachment;filename="$escaped_filename") );
    $resp->header('Content-Type'        => $options_ref->{content_type} );

    my $content = $options_ref->{decoded_content};
    my $content_copy = $content;
    # check if out content is octets or characters
    my $fail_silently = 1;
    if ( utf8::downgrade( $content_copy, $fail_silently ) ) {
        # we have perl bytes
        $self->log_debug("Content is already in perl bytes");
        $resp->content( $content );
    }
    else {
        # we have perl characters, HTTP::Message only supports bytes
        $self->log_debug('Content is perl characters, we need to convert to bytes');

        my $content_charset = $self->charset_from_headers( $resp );
        my $new_charset     = $content_charset || 'UTF-8';

        $self->log_debug('Original charset is [%s], new charset is [%s]', $content_charset, $new_charset);

        my $bytes = Encode::encode( $new_charset, $content );
        $resp->content( $bytes );

        if ( ( $content_charset // 'UNDEF' ) ne $new_charset ) {
            $options_ref->{content_type} .= '; charset='. $new_charset;
            $self->log_debug('Setting the content_type to [%s] on the phony HTTP::Response object', $options_ref->{content_type});
            $resp->header('Content-Type' => $options_ref->{content_type});
        }
    }

    $self->log_debug('The filename on our CV response is: %s',$resp->filename() );
    $self->log_debug('The content type for our CV response is: %s', $resp->header('Content_Type') );

    return $resp;
}

sub charset_from_headers {
    my $self = shift;
    my $h = shift;

    if ( $h->can( 'content_type_charset' ) ) {
        # version 6.00 of HTTP::Headers has this method
        return $h->content_type_charset();
    }

    # older versions of HTTP::Headers do not so we roll our own
    require HTTP::Headers::Util;
    my ( @v ) = HTTP::Headers::Util::split_header_words( $h->header('Content-Type') );
    my ($ct, undef, %ct_param) = @{$v[0]};
    my $charset = $ct_param{charset};
    $charset = uc($charset);
    $charset =~ s/^\s+//;  $charset =~ s/\s+\z//;
    return $charset;
}

sub extract_cv_from_candidate {
    my $self = shift;
    my $candidate = shift;

    # try to get the word CV
    my $word_doc = $candidate->attr( 'resume_word_base64' );

    my $raw_text;
    if ( $word_doc ) {
        $self->log_debug('Decoding base64 word doc');
        $raw_text = MIME::Base64::decode_base64( $word_doc );
        $self->log_debug('Decoded base64 word doc, deleting resume_word_base64 attribute');

        $candidate->delete_attr( 'resume_word_base64' );
    }

    # fallback to the text CV
    if ( !$raw_text ) {
        $self->log_debug('Falling back to the raw text version');
        $raw_text = $candidate->attr( 'resume_text' ); # This resume text is in fact binary.
    }

    if ( !$raw_text ) {
        $self->log_debug('We could not find a CV for this candidate in their response');
        return $self->throw_error('NO CV CONTENT FOUND');
    }

    # get the resume filename or guess one
    my $filename = $candidate->attr( 'resume_filename' );
    $self->log_debug('resume_filename is: %s', $filename);
    if ( !$filename ) {
        $self->log_debug('no resume_filename so building one');
        $filename = $self->build_cv_filename( $candidate, $raw_text );
        $self->log_debug('built a resume filename: %s', $filename);
    }

    # get the resume content type or guess it
    my $content_type = $candidate->attr( 'resume_content_type' );
    $self->log_debug('resume_content_type is: %s', $content_type);
    if ( !$content_type ) {
        $self->log_debug('no resume_content_type so guessing');
        $content_type = $self->guess_content_type_from_filename( $filename );
        $self->log_debug('guessed resume_content_type: %s', $content_type);
    }

    # provided by Stream::Engines::XML
    my $http_response = $self->to_response_attachment({
                                                       decoded_content => $raw_text,
                                                       filename        => $filename,
                                                       content_type    => $content_type,
                                                      });
    if( my $email = $candidate->email() ){
        $log->info("Got candidate email after download scraping. Will set X-Candidate-Email header with $email");
        $http_response->header( 'X-Candidate-Email' ,  $candidate->email() )
    }
    return $http_response;
}


=head2 download_profile (STUB/OPTIONAL)

Stub to download more detailed information about the candidate.

Note: You CAN implement that yourself in a feed. This method is the entry point
of the framework.

This will enrich the given candidate (a L<Stream2::Results::Result) in stream2
using its 'attributes' method.

This is used in the profile preview feature.

Not all boards support this.

Returns the $candidate object.

usage:

   if( $this->download_profile($candidate) ){
      .. this is a success. candidate is richer ..
   }

=cut

sub download_profile {
    my $self = shift;
    my $candidate = shift;

    $self->login() if $self->download_profile_requires_login();

    $self->download_profile_submit( $candidate );

    $self->log_debug('Checking for errors');

    $self->check_for_success_or_failure(
        $self->download_profile_success() || undef,
        $self->download_profile_failed()  || undef,
    );

    # scrape xml for candidates
    $candidate = $self->scrape_generic_download_profile( $candidate );

    $self->logout();

    return $candidate;
}

sub download_profile_requires_login {
    return 1;
}

=head2 download_profile_submit

Implement that to enrich the given candidate. Will be called by the engine as

 $this->download_profile_submit($candidate);

In stream2, the candidate is a L<Stream2::Results::Result>

=cut

sub download_profile_submit {
    my $self = shift;
    return $self->throw_profile_not_supported('No "download_profile_submit" method to make profile request');
}


=head2 download_profile_success

=cut

sub download_profile_success {}
sub download_profile_failed  {}

sub scrape_download_profile_xpath {}
sub scrape_download_profile_details {}
sub scrape_generic_download_profile {
    my $self = shift;
    my $candidate = shift;

    $self->log_info('Scraping profile');
    my $candidate_node_xpath = $self->scrape_download_profile_xpath();
    if ( !$candidate_node_xpath ) {
        $self->log_warn('$self->scrape_download_profile_xpath() is empty - no generic scraper defined');
        return undef;
    }

    my (@candidate_xpath) = $self->scrape_download_profile_details();
    if ( !@candidate_xpath ) {
        $self->log_warn('$self->scrape_download_profile_details() is empty - we are not scraping');
        return undef;
    }

    $self->log_debug('Result node xpath is: %s', $candidate_node_xpath );

    my $scrape_ok = 0;

    RESULT:
    foreach my $result_node ( $self->findnodes( $candidate_node_xpath ) ) {
        $self->log_debug('Scraping a result node');

        my %attributes = $self->scrape_generic_node( $result_node, \@candidate_xpath );

        if ( !$self->scrape_found_attributes( %attributes ) ) {
            $self->log_warn('No candidate information found in this node');
            next RESULT;
        }

        $self->log_debug( 'Found some attributes for this candidate' );

        $candidate->attributes(
            %attributes,
        );

        $self->scrape_fixup_candidate_profile ( $candidate );

        $scrape_ok = 1;
    }

    if ( $scrape_ok ) {
        if ( $self->can('after_download_profile_scrape_success' ) ) {
            $self->after_download_profile_scrape_success( $candidate );
        }
        else {
            $self->log_debug('No after_download_profile_scrape_success method available');
        }

        $candidate->profile_downloaded( time );
    }

    $self->log_debug('Finished scraping profile');

    return $candidate;
}

# give the feed a chance to update the candidate profile and sanitise data
sub scrape_fixup_candidate_profile { }


### scraping "engine"

=head1 Scraping Engine

Methods that extract information from cv search pages

=head2 scrape_number_of_results (GENERIC/OPTIONAL)

Scrapes the total number of candidates this search has found
(not per page)

e.g. For "Displaying 20 out of 124 candidates", this method should
return 124.

=cut

sub scrape_number_of_results {
    my $self = shift;

    my $xpath = $self->search_number_of_results_xpath();
    my $regex = $self->search_number_of_results_regex();

    $self->log_debug( 'Scraping to find number of results, xpath is [%s] and regex is [%s]', $xpath, $regex );

    my $number_of_results = $self->scrape_generic_xpath_then_regex( $xpath, $regex );

    # remove any commas and . (. is thousand seperator in europe)
    # Also remove whitespace as careers24 has a space as a thousand seperator
    $number_of_results =~ s/\.|,|\s//g;

    if ( defined( $number_of_results ) ) {
        $self->total_results( $number_of_results );

        $self->log_debug('In total, this search would find %d candidates', $self->total_results() );
    }
    else {
        $self->log_warn('Unable to find the total number of results');
    }

    return;
}

=over 4

=item * search_number_of_results_regex (GENERIC/STUB)

Default is C<qr/\b([\d,.\s]+)\b/>

=cut

sub search_number_of_results_regex {
    return $_[0]->search_number_of_results_xpath() ? qr/\b([\d,.\s]+)\b/ : undef;
}

=item * search_number_of_results_xpath (STUB)

TODO TODO TODO TODO

=back

=cut

sub search_number_of_results_xpath { return undef; }

=head2 scrape_paginator (GENERIC/OPTIONAL)

Scrapes the total number of pages in this search.

e.g. For "Page 1 of 20", this method should return 20.

=cut

sub scrape_paginator {
    my $self = shift;

    my $xpath = $self->search_number_of_pages_xpath();
    my $regex = $self->search_number_of_pages_regex();

    $self->log_debug( 'Scraping to find number of pages, xpath is [%s] and regex is [%s]', $xpath, $regex );

    my $number_of_pages;
    if ($xpath || $regex){
       $number_of_pages = $self->scrape_generic_xpath_then_regex( $xpath, $regex );
    } else {
        my $total_results = $self->total_results();

        $self->log_debug("Total results=".$total_results);

        my $results_per_page = $self->results_per_page() || $self->candidates_required();

        $self->log_debug("Result per page=".$results_per_page);

        if ( $total_results <= $results_per_page ) {
            $number_of_pages = 1;
        } else {
            require POSIX;
            my $total_pages = eval { POSIX::ceil( $total_results / $results_per_page ) };
            $total_pages = 1 if $@;
            $number_of_pages = $total_pages;
        }
    }

    if ( defined( $number_of_pages ) ) {
        $self->max_pages( $number_of_pages );

        $self->log_debug('In total, this search has %d pages', $self->max_pages() );
    }
    else {
        $self->log_warn('Unable to find the number of pages for this search');
    }

    return;
}

=over 4

=item * search_number_of_pages_regex (GENERIC/STUB)

Default is C<qr/\b(\d+)\b/>

=cut

sub search_number_of_pages_regex {
    return $_[0]->search_number_of_pages_xpath() ? qr/\b(\d+)\b/ : undef;
}

=item * search_number_of_results_xpath (STUB)

TODO TODO TODO TODO

=back

=cut

sub search_number_of_pages_xpath { return undef; }

=head2 scrape_page (GENERIC/STUB)

=cut

sub scrape_page {
    my $self = shift;

    return $self->scrape_generic_search_results();
}

=head2 scrape_generic_search_results (GENERIC)

Scrapes cv search results for candidates.

=cut

sub before_scraping_results {}

sub scrape_generic_search_results {
    # scrape the xml for candidates
    my $self = shift;

    $self->log_info('Generic scraper: template is %s', ref($self) );

    $self->before_scraping_results();

    $self->log_info('Page: %d', $self->current_page() );

    my $candidate_node_xpath = $self->scrape_candidate_xpath();

    my $results = $self->results();
    if ( !$candidate_node_xpath ) {
        $self->log_warn('$self->scrape_candidate_xpath() is empty - no generic scraper defined');
        return $results;
    }

    $self->log_debug('Candidate result XPath: %s', $candidate_node_xpath);
    my $results_found = $self->foreach_node(
        $candidate_node_xpath,
        'scrape_generic_result_node',
    );

    $self->results_last_search( $results_found );
    $self->results_per_scrape( $results_found );

    $self->log_debug('finished scraping [%d] results with foreach_node', $results_found);

    return $results;
}

=head2 results_last_search (GENERIC)

useful to find out if the amount of candidates found last search
are less than we expected, last page?

=cut

sub results_last_search {
    $_[0]->{results_last_search} = $_[1] if @_ == 2;
    return $_[0]->{results_last_search};
}

sub supports_paging { #subclass me, throw an error if not
    return 1; #feed supports searching a specific page of results
}

sub results_per_scrape {
    my $self = shift;
    my $count = shift;
    my $results = $self->results();

    # only set it once.
    unless ( $results->results_per_scrape ){
        $results->results_per_scrape($count);
    }

    return $results->results_per_scrape();
}

# max number of results to scrape - stop scraping if we hit this number
# default is unlimited
sub scrape_results_to_scrape {}

sub scrape_generic_result_node {
    my $self = shift;
    my $result_node = shift;

    # start by checking if we need to scrape this node or have we already found enough results
    # this check is primarily for jobsite who return 300 results but we only store 100 of them
    # for performance
    my $results_to_scrape = $self->scrape_results_to_scrape();
    if ( defined( $results_to_scrape ) && $results_to_scrape > 0 ) {
        my $results_seen = $self->candidates_found(); # this is new candidates found

        if ( $results_seen >= $results_to_scrape ) {
            $self->log_warn('We are looking to scrape [%s] results', $results_to_scrape);
            $self->log_warn('We have scraped [%s] new results', $results_seen);
            $self->log_warn('Not scraping this candidate because we have already scraped enough');
            return;
        }
    }

    my (@candidate_xpaths) = $self->scrape_candidate_details();
    if ( !@candidate_xpaths ) {
        $self->log_warn('$self->scrape_candidate_details() is empty - NOT SCRAPING');
        return;
    }

    my %attributes = $self->scrape_generic_node( $result_node, \@candidate_xpaths );

    if ( !$self->scrape_found_attributes( %attributes ) ) {
        $self->log_warn('No candidate information found in this node');
        return;
    }

    my $candidate = $self->new_candidate();
    $candidate->attributes(
        %attributes,
    );

    # give candidate a messaging_uri for Messaging feature
    $self->candidate_messaging_uri( $candidate );

    # give the feed a chance to update the candidate and sanitise data
    $self->scrape_fixup_candidate( $candidate );

    # add the "headline"
    $self->scrape_candidate_add_headline( $candidate );

    $self->log_debug('Adding candidate: %s - id: %s', $candidate->name, $candidate->candidate_id() );

    $self->add_candidate( $candidate );

    return $candidate;
}

sub candidate_messaging_uri {
    my $self = shift;
    my $candidate = shift;

    my %message = $self->candidate_messaging_uri_options( $candidate );

    my $protocol = $self->candidate_messaging_uri_protocol();
    my $uri = URI->new( "${protocol}://" );
    $uri->query_form( %message );
    $candidate->attr( 'messaging_uri' => $uri->as_string() );

    return $candidate;
}

sub candidate_messaging_uri_options {
    my $self = shift;
    my $candidate = shift;

    return (
        to => $candidate->attr('email'),
    );
}

sub candidate_messaging_uri_protocol {
    return 'email';
}

sub new_candidate {
    my $self = shift;
    my $results = $self->results();
    my $candidate = $results->new_candidate();

    $candidate->destination($self->destination());

    return $candidate;
}

=head2 add_candidate

Shortcut to $self->results()->add_candidate($candidate)

=cut

sub add_candidate { shift->results()->add_candidate(shift); }

=over

=item * scrape_candidate_xpath (STUB)

XPath that identifies the node that we should start scraping for
candidate data.

e.g. <candidates>
        <candidate id="1">...</candidate>
        <candidate id="2">...</candidate>
        <candidate id="3">...</candidate>
    </candidates>

The xpath would be: //candidates/candidate

=cut

sub scrape_candidate_xpath {}

=item * scrape_candidate_details (STUB/OPTIONAL)

XPath to scrape candidate details from nodes found by
C<scrape_candidate_xpath>. See C<scrape_generic_node>.

=cut

sub scrape_candidate_details {}

=item * scrape_fixup_candidate (STUB/OPTIONAL)

Hook called after each candidate is scraped, but before the
candidate is stored. Provides an opportunity to clean up data
if needed (e.g. standardise date formats etc)

=back

=cut

sub scrape_fixup_candidate {}

=item * scrape_candidate_add_headline (GENERIC)

Copies the headline from other candidate attributes trying each one
until it finds a value

=cut

sub scrape_candidate_add_headline {
    my $self = shift;
    my $candidate = shift;

    if ( $candidate->attr('headline') ) {
        # headline already set, nothing to do
        return;
    }

    my (@headline_attrs) = $self->scrape_candidate_headline();
    foreach my $headline_attr ( @headline_attrs ) {
        my $value = $candidate->attr( $headline_attr );
        if ( $value ) {
            $self->log_debug('populating headline from <%s> attribute', $headline_attr);
            $candidate->attr('headline' => $value);
            return 1;
        }
    }

    $self->log_debug('unable to populate headline');

    return;
}

=item * scrape_candidate_headline (STUB/OPTIONAL)

Returns a list of attributes that scrape_candidate_add_headline()
will check when it attempts to populate the headline

=back

=cut

sub scrape_candidate_headline {
    return ();
}

=head1 Error Handling

=head2 check_response_for_success_or_failure (GENERIC)

Checks a HTTP response for a success regex or failure regex.

Throws an exception if:

=over 4

=item * the response does not contain the success regex

=item * the response contains the failure regex

=back

=cut

sub check_for_success_or_failure {
    my $self = shift;
    my $success_regex = shift;
    my $failure_regex = shift;

    my $decoded_content = $self->recent_content();
    if ( defined($success_regex) ) {
        $self->log_debug('Checking for success message [%s]', $success_regex);
        if ( $decoded_content !~ m/$success_regex/ ) {
            $self->log_debug('Did not find the success message');
            $self->throw_error('Response was not successful');

        }
        else {
            $self->log_debug('Found a success message');
        }
    }

    if ( defined($failure_regex) ) {
        $self->log_debug('Checking for failure message [%s]', $failure_regex);
        if ( $decoded_content =~ m/$failure_regex/ ) {
            $self->log_debug('Found failure message - this search failed');
            $self->throw_error('Failure response found');
        }
        else {
            $self->log_debug('Did not find the failure message');
        }
    }

    return 1;
}

sub report_error {
    my $self = shift;
    my $mask = shift;
    my $msg_error = @_ ? sprintf( $mask, @_ ) : $mask;

    # only send me the email once per search (not once per candidate!)
    my $errors_reported_ref = $self->{errors_reported} ||= {};
    if ( exists( $errors_reported_ref->{$mask} ) ) {
        return;
    }

    require Carp;
    my $msg_contents = $msg_error . "\n\nStack Trace\n" . Carp::longmess( $msg_error );

    require MIME::Lite;
    my $msg = MIME::Lite->new(
        To => 'andy@broadbean.net',
        Subject => 'Stream Error: ' . $msg_error,
        Data    => $msg_contents,
    );

    $msg->send();

    return 1;
}

=head2 throw_error (GENERIC)

Throws an error (either a subclass of Stream::EngineException) or a plain confession string.

If possible, it will use the Feed's 'feed_identify_error', or the 'engine_identify_error'
, or even the http_error to throw specific subclasses of Stream::EngineException.

=cut

sub throw_error {
    my ( $self, $message , $content) = @_;

    $content //= $self->content();

    # Protect against recursive error throwing.
    if ( $self->{throwing_error} ) {
        # we are already being called to throw an error
        $log->warn('preventing infinite loop, someone called throw_error() from throw_error()');
        confess("Recursive throw_error detected");
    }

    $self->{throwing_error}++;

    if( $self->can('feed_identify_error') ){
        # Give a chance to feed identify_error
        # This would die with a proper subclass of Stream::EngineException or return a generic error string.
        if( my $error_string = $self->feed_identify_error($message,$content) ){
            # We passed that, this is a generic exception.
            die Stream::EngineException->new({ message => $error_string });
        }
    }
    $log->debug("'$self' has not managed any error through 'feed_identify_error'");
    # No dying yet here.

    if( $self->can('engine_identify_error') ){
        # This would die if engine_identify_error throws a specific error
        if( my $error_string = $self->engine_identify_error($message,$content) ){
            die Stream::EngineException->new({ message => $error_string });
        }
    }
    $log->debug("'$self' has not managed any error through 'engine_identify_error'");

    # Give a changce to http error
    if( $message =~ m/Response was unsuccessful: / ){
        # This would identify HTTP errors.
        if( my $error = $self->http_error($message, $content) ){
            # http_error didnt die by itself.
            die Stream::EngineException->new({ message => $message });
        }
    }
    $log->debug("'$self' has not handled this error specifically");
    confess($self->user_object->stream2->__x("Unmanaged error in {destination}", destination => $self->destination()) . ': ' . $message );
}

sub has_logger {
    my $self = shift;

    if ( $self->{logger} ) {
        return 1;
    }

    return 0;
}

=head2 throw_*

Each of those helpers throw a specific subclass of L<Stream::EngineException>, allowing a human targetted
message to be given.

Usage:

  $this->throw_login_error("Sorry wrong credentials blabla");

Or

  $this->throw_lack_of_credit("You don't have any credits left to do some stuff");

Or with some additional display properties:

  $this->throw_login_error("You don't have any credits left to do some stuff", { src => 'http://somewhere.to.login' });

=cut

sub throw_login_error {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::LoginError->new({ message => $message, error_properties => $properties });
}

sub throw_timeout {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::Timeout->new({ message => $message, error_properties => $properties });
}

sub throw_critical {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::Critical->new({ message => $message, error_properties => $properties });
}

sub throw_known_internal {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::Internal->new({ message => $message, error_properties => $properties });
}

sub throw_inactive_account {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::InactiveAccount->new({ message => $message, error_properties => $properties });
}

sub throw_no_cvsearch {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::NoCVSearch->new({ message => $message, error_properties => $properties });
}

sub throw_lack_of_credit {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::LackOfCredit->new({ message => $message, error_properties => $properties });
}

sub throw_account_in_use {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::AccountInUse->new({ message => $message, error_properties => $properties });
}

sub throw_too_many_results {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::TooManyResults->new({ message => $message, error_properties => $properties });
}

sub throw_invalid_boolean_search {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::InvalidBooleanSearch->new({ message => $message, error_properties => $properties });
}

sub throw_keywords_too_long {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::KeywordsTooLong->new({ message => $message, error_properties => $properties });
}

sub throw_unavailable {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::Unavailable->new({ message => $message, error_properties => $properties });
}

sub throw_invalid_email_address {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::InvalidEmailAddress->new({ message => $message, error_properties => $properties });
}

sub throw_cv_unavailable {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::CVUnavailable->new({ message => $message, error_properties => $properties });
}

sub throw_profile_not_supported {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::ProfileNotSupported->new({ message => $message, error_properties => $properties });
}

sub throw_cv_not_ready {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::CVNotReady->new({ message => $message, error_properties => $properties });
}

sub throw_no_cv {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::NoCV->new({ message => $message, error_properties => $properties });
}

sub throw_watchdog_not_supported {
    my ($self, $message, $properties) = @_;
    $properties ||= {};
    die Stream::EngineException::WatchdogNotSupported->new({ message => $message, error_properties => $properties });
}


=head2 http_error

Identifies generic http transport errors and throws an appropriate exception,
or return a more general error string (or undef if no error was found).

=cut

sub http_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    $message =~ s/Response was unsuccessful: //;

    if ( $message =~ m/^\s*404\s*/ ) {
        return $self->throw_critical('404 - Page Not Found.');
    }

    if ( $message =~ m/^\s*500\s*/ ) {
        if ( $message =~ m/read timeout|Can\'t connect|Server closed connection|read failed|Connection timed out|Timeout expired|connect: timeout|Connect failed|Connection refused/ ) {
            return $self->throw_timeout($message);
        }
        if ( $message =~ m/Bad hostname/ ) {
            return $self->throw_critical('DNS Error: unable to lookup host');
        }
    }

    if ( $content =~ m/504 Gateway Time\-?out/ ) {
        return $self->throw_timeout($message);
    }

    return;
}

=head1 Utility Methods

=head2 scraper

Returns a Stream::Scraper:: object

=cut

{
    # This is a horrible hack to give a logger
    # to the instance of Stream::Scraper that it would be happy with.
    # Stream::Scraper is a dependency of this
    # project, but Stream::Scraper's logging
    # mechanism relies on the fact we give it an instance of
    # Bean::Log::Stream, which lives in this project.
    #
    # This shamelessly emulates Bean::Log::Stream (or any of its
    #  numerous ancestry) so our log:Any/Log4perl  LogStash format works just fine
    #  and our STDERR doenst get spammed with Log::Any/Log4perl warnings.
    #
    # Circular dependencies are bad. Cross project/remote circular dependencies are evil.
    #
    package Stream2::Log::Any::ScraperAdapter;
    sub new{
        my ($class) = @_;
        # This will log things under category Stream2::Log::Any::ScraperAdapter
        my $logany = Log::Any->get_logger();
        return bless { 'logany' => $logany }, $class;
    };
    sub _call_logany{
        my ($self, $level, @args ) = @_;
        my $message = sprintf(shift @args, @args);
        $self->{logany}->$level($message);
    }
    sub debug{ shift->_call_logany('debug', @_ ); }
    sub info{ shift->_call_logany('info',@_ );}
    sub warn{ shift->_call_logany('warn', @_ );}
    sub error{ shift->_call_logany('error', @_ );}
    sub mark{ shift->_call_logany('info', @_ ); }
    1;
}


sub scraper {
    my $self = shift;

    if ( !$self->{scraper} ) {
        $self->log_debug('Initialising Scraper');

        my @content_type = $self->response()->content_type();

        my %options = (
            content => $self->recent_content(),
            content_type => $content_type[0],
            # NOTE: Look up a few line for an explanation of this thing
            logger  => Stream2::Log::Any::ScraperAdapter->new(),
        );

        if ( $self->can('default_namespace') ) {
            $options{'default_namespace'} = $self->default_namespace();
        }
        if ( $self->can('namespaces') ) {
            my @namespaces = $self->namespaces();
            $options{namespaces} = \@namespaces;
        }

        my $scraper = eval { Stream::Scraper::Guess->new(\%options); };
        if ( $@ ) {
            $self->log_error('Unexpected response from the board, we failed to initialise the scraper: %s', $@);
            $self->throw_error('INVALID XML RETURNED FROM BOARD');
        }

        if ( !$scraper ) {
            $self->log_warn('Failed to initialise scraper');
            return;
        }

        $self->{scraper} = $scraper;

        $self->log_debug('Scraper [%s] initialised', $scraper->type() );
    }

    return $self->{scraper};

}

sub reset_scraper {
    my $self = shift;
    delete $self->{scraper};
}

=head2 response_as_string

    like $resp->as_string but will decode gzipped content for nice logging

=cut

sub response_as_string {
    my ( $self, $resp ) = @_;

    my $status_line = $resp->status_line;
    my $proto = $resp->protocol();
    $status_line = "$proto $status_line" if $proto;

    # we don't want pages of binary nonsense in logs do we?
    # ... especially if its a 15 page PDF CV
    my $content = $resp->decoded_content();
    if ( $resp->content_type !~ m/(?:text|xml|json|html)/i ){
        $content = substr( $content, 0, 200, ) . "\n\n... Binary Content ...\n\n";
    }

    return join( "\n", $status_line, $resp->headers()->as_string(), $content );
}


=head2 scrape_generic_xpath_then_regex

Uses xpath to narrow down the content and a regex to match the specific
content.

Both our scrapers (XML & HTML) understand xpath so this is in the
default template.

Ideally this should be moved into an XpathScraper module that XML &
ScreenScraper inherit.

=cut

sub scrape_generic_xpath_then_regex {
    my $self = shift;
    my $xpath = shift;
    my $regex = shift;

    return unless $xpath || $regex;

    $self->log_debug('Scraping content with xpath query [%s] and regex [%s]', $xpath, $regex );

    my $xpath_content = $xpath ? $self->scrape_generic_with_xpath( $xpath ) : $self->recent_content();

    return $xpath_content unless defined($regex);

    if ( $xpath_content =~ m/$regex/ ) {
        my $regex_found = $1;
        $self->log_debug( 'Regex %s found [%s]', $regex, $regex_found );
        return $regex_found;
    }

    $self->log_debug( 'Regex %s did not match', $regex );

    return undef;
}

=head2 foreach_node

Example of usage:

  $self->foreach_node(
      $XPATH,
      sub {
          my $self = shift;
          my $node = shift;

          ... do something with each node ...
      },
  );

Calls the subroutine for each node that matches $XPATH

=cut

sub foreach_node {
    my $self = shift;
    my $list_nodes_xpath = shift;
    my $method = shift;

    if ( !$list_nodes_xpath ) {
        return;
    }

    my $scraper = $self->scraper()
        or return;


    my $callback_ref = ref( $method ) ? $method : $self->can( $method );
    if ( !$callback_ref ) {
        die "foreach_node: unsupported or missing method: '$method'";
    }

    # bind the callback to $self
    my $real_callback_ref = sub { $callback_ref->( $self, @_ ); };
    return $scraper->foreach_node( $list_nodes_xpath, $real_callback_ref );
}

sub scrape_found_attributes {
    my $self = shift;
    my %attributes = @_;
    my $found_attributes = first {
        ($_ && (
            !ref($_)) ||
            (ref($_) eq 'ARRAY' && scalar(@$_))
        )
    } values %attributes;

    return $found_attributes;
}


=head2 scrape_generic_node

Lots of docs required!!!! TODO TODO TODO

Explain me!!!!

=cut

sub scrape_generic_node {
    my $self = shift;
    my $node = shift;
    my $xpaths_aref = shift;

    my $scraper = $self->scraper()
        or return;

    return $scraper->scrape_generic_node( $node, $xpaths_aref );
}

=head2 scrape_generic_with_xpath (DEPRECATED)

Use C<findvalue($xpath)> instead

=cut

sub scrape_generic_with_xpath {
    my $self = shift;
    my $xpath = shift or return undef;

    my ($value) = $self->findvalue( $xpath );

    if ( defined( $value ) ) {
        $self->log_debug('%s found content: [%s]', $xpath, $value);
        return $value;
    }

    $self->log_debug('%s did not match content', $xpath);

    return undef;
}

=head2 findnodes (MANDATORY)

Agnostic layer to xpath regardless of the underlying module
that perform the actual xpath search

expects: $self->findnodes( 'XPATH', [$node_to_search] );
returns: @array_of_matching_nodes

=cut

sub findnodes {
    my $self = shift;
    my $scraper = $self->scraper()
        or return ();

    return $scraper->findnodes( @_ );
}

=head2 findvalue (MANDATORY)

Agnostic layer to xpath regardless of the underlying module
that perform the actual xpath search

expects: $self->findvalue( 'XPATH', [$node_to_search] );
returns: @array_of_matching_values

=cut

sub findvalue {
    my $self = shift;
    my $scraper = $self->scraper()
        or return ();

    return $scraper->findvalue( @_ );
}

sub hasnodes {
    my $self = shift;

    my (@nodes) = $self->findnodes(@_);

    return scalar(@nodes);
}

=head1 Session handling

Stream2 uses the Stream2::StreamUser to persist key values pairs
against the current user.

Values will automatically expire
the value from the store after $expire_after seconds.

The key is associated with the owner of the search and their IP address
(CV Library insisted on locking it to their IP address).

See CV Library template for an example of it's use.

=cut

=head2 session_life_default

default life of any key-value stored against session or user_object's

=cut

sub session_life_default {
    return; # stub
}

=head2 session_value

mutator. gets or sets values from Stream::Session

=cut

sub session_value {
    my $self = shift;
    my $token = shift;

    if ( @_ ) {
        return $self->set_session_value( $token, @_ );
    }

    return $self->get_session_value( $token );
}

=head2 get_session_value

getter - Retrieves the board specific token if present and fallsback to the global token

Gets tokens stored against their session
See get_account_value to get tokens stored against the adcourier account instead

=cut

sub get_session_value {
    my $self = shift;
    my $token = shift;

    confess("Implement me if needed. Would require server side and worker code to have access to session");

    $self->log_debug('Getting session token [%s] from session [%s]', $token, Stream::SharedUtils::current_session() );

    # Check the board token and the global token
    foreach my $token_name ( $self->_potential_token_names( $token ) ) {
        # need to set the session against the person who owns the search
        # which may be different to the person who is logged in
        $self->log_debug('Getting session token [%s]', $token_name);
        my $value = Stream::Session->session_get( $token_name );

        if ( $value ) {
            $self->log_debug('returning session token [%s] => [%s]', $token_name, $value );
            return $value;
        }
    }

    return;
}

=head2 set_session_value

setter - Always saves the value as a board specific token. Stored against their Stream session

=cut

sub set_session_value {
    my $self = shift;
    my $token = shift;
    my $value = shift;
    my $expire_after_secs = shift || $self->session_life_default();

    confess("Implement me if needed. Would require server side and worker code to have access to the user session");

    # make sure the token is board specific (ie. cvlibrary_session_id vs session_id)
    if ( !$self->is_board_specific( $token ) ) {
        $token = $self->make_board_specific( $token );
    }

    $self->log_debug(
        'Session: storing token [%s] => value [%s] to expire after [%s] seconds',
        $token,
        $value,
        $expire_after_secs,
    );

    # need to set the session against the person who owns the search
    # which may be different to the person who is logged in
    return Stream::Session->session_set(
        $token => $value, $expire_after_secs
    );
}

=head2 account_value

mutator. gets or sets values from Stream::Session

=cut

sub account_value {
    my ( $self , $token ) = ( shift  , shift );
    return @_ ? $self->set_account_value( $token, @_ ) : $self->get_account_value( $token );
}

=head2 get_account_value

getter - Retrieves the board specific token stored against the user account if present and fallsback to the global token

=cut

sub get_account_value {
    my $self = shift;
    my $token = shift;

    my $user = $self->user_object();

    $log->debugf('Getting account token [%s] for user (id= [%d])', $token , $user->id() );

    # Check the board token and the global token
    foreach my $token_name ( $self->_potential_token_names( $token ) ) {
        # need to set the session against the person who owns the search
        # which may be different to the person who is logged in
        $log->debugf('Getting account token [%s]', $token_name);
        if ( my $value =  $self->user_object->get_cached_value($token_name) ) {
            $log->debugf('returning account token [%s] => [%s]', $token_name, $value );
            return $value;
        }
    }

    return;
}

=head2 set_acount_value

setter - Always saves the value as a board specific token

Usage:

  $this->set_account_value('token_name' , 'value' , $expiry_or_undef );

=cut

sub set_account_value {
    my $self = shift;
    my $token = shift;
    my $value = shift;
    my $expire_after_secs = shift || $self->session_life_default();

    # make sure the token is board specific (ie. cvlibrary_session_id vs session_id)
    if ( !$self->is_board_specific( $token ) ) {
        $token = $self->make_board_specific( $token );
    }

    $log->debugf(
        'Account Session: storing token [%s] => value [%s] to expire after [%s] seconds',
        $token,
        $value,
        $expire_after_secs,
    );

    # need to set the session against the person who owns the search
    # which may be different to the person who is logged in
    $self->user_object->set_cached_value($token, $value, $expire_after_secs);
}

=head2 rendered_attachments

An arrayref of rendered attachments. See the Ember attachments model

=cut

sub rendered_attachments {
    my ($self, $candidate) = @_;
    return [];
}

=head2 download_attachment

A file download HTTP::Response of the attachment specified by $attachment_key

=cut

sub download_attachment {
    my ($self, $candidate, $attachment_key) = @_;
    ...
}

=head2 download_attachments

An arrayref of download responses of he given candidat's attachments

Usage:

    my $attachment_downloads = $feed->download_attachments($candidate);
    my $filename = $attachment_downloads->[0]->filename

=cut

sub download_attachments {
    my ($self, $candidate) = @_;
    return [];
}

sub DESTROY {
    my $self = shift;

    # don't mess with the global $@ otherwise any evals in logout() will wipe the original error message
    local $@;

    # destroy any possibility of a circular reference
    delete $self->{logger};
    delete $self->{agent};
    delete $self->{callback};

    $self->reset_scraper(); # make sure our scraper is freed
}

1;

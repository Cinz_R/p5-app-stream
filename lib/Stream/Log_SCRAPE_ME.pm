#!/usr/bin/perl

# Global logger for stream workers
package Stream::Log;

use strict;

use Bean::Log::Stderr;

use Log::Any qw/$log/;

my $_global_log;

sub logger {
    return $log; # From Log::Any
    ## return $_global_log ||= Bean::Log::Stderr->new();
}

sub mark {
    my $self = shift;
    return $self->logger()->mark( @_ );
}

sub debug {
    my $self = shift;
    return $self->logger()->debug( @_ );
}

sub info {
    my $self = shift;
    return $self->logger()->info( @_ );
}

sub warn {
    my $self = shift;
    return $self->logger()->warn( @_ );
}

sub error {
    my $self = shift;
    return $self->logger()->error( @_ );
}

sub request {
    my $self = shift;
    return $self->logger()->request( @_ );
}

sub response {
    my $self = shift;
    return $self->logger()->response( @_ );
}

sub debug_sql {
    my $self = shift;
    return $self->logger()->debug_sql( @_ );
}

1;

package Stream::TP2;

use strict;
use warnings;

use JSON qw(); # import nothing

use Stream::TP2::Response;
use Stream::TP2::Constants;

use Log::Any qw/$log/;

use HTTP::Request::Common;

use base 'Class::Accessor::Fast';

__PACKAGE__->mk_accessors(
    qw/default_params last_response last_raw_response/,
);

#
# Accessors
#
sub agent {
    return $_[0]->{agent} ||= $_[0]->default_agent() if @_ == 1;
    return $_[0]->{agent}   = $_[1];
}

sub search_url {
    return $_[0]->{search_url} ||= $_[0]->default_search_url() if @_ == 1;
    return $_[0]->{search_url}   = $_[1];
}

sub api_url {
    return $_[0]->{api_url} ||= $_[0]->default_api_url() if @_ == 1;
    return $_[0]->{api_url}   = $_[1];
}

sub api_b_url {
    return $_[0]->{api_b_url} ||= $_[0]->default_api_b_url() if @_ == 1;
    return $_[0]->{api_b_url}   = $_[1];
}

#
# Default values
#
sub default_agent {
    require LWP::UserAgent;
    return LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 });
}

sub default_search_url {
    return Stream::TP2::Constants::default_search_url();
}

sub default_api_url {
    return Stream::TP2::Constants::default_api_url();
}

sub default_api_b_url {
    return Stream::TP2::Constants::default_api_b_url();
}

#
# Methods
#
sub add {
    my $self = shift;
    my @docs = @_;

    my @json;

    my $low_priority = 0;
    foreach my $doc ( @docs ) {
        my $doc_ref = $doc->to_json();
        $doc_ref->{command} = 'new';
        push @json, $self->_to_doc( $doc_ref );

        if ( $doc->low_priority () ) {
            $low_priority++;
        }
    }

    return $self->api_post( \@json, { low_priority => $low_priority });
}

sub update {
    my $self = shift;
    my @docs = @_;

    my @json;
    my $low_priority = 0;
    foreach my $doc ( @docs ) {
        my $doc_ref = $doc->to_json();
        $doc_ref->{command} = 'update';
        push @json, $self->_to_doc( $doc_ref );

        if ( $doc->low_priority () ) {
            $low_priority++;
        }
    }

    return $self->api_post( \@json, { low_priority => $low_priority });
}

sub delete {
    my $self = shift;
    my @docs = (@_);

    my @json;
    my $low_priority = 0;
    foreach my $doc ( @docs ) {
        my $doc_ref = $doc->to_json();
        $doc_ref->{command} = 'delete';
        push @json, $self->_to_doc( $doc_ref );

        if ( $doc->low_priority () ) {
            $low_priority++;
        }
    }

    return $self->api_post( \@json, { low_priority => $low_priority });
}

sub delete_client {
    my $self = shift;
    my $client_id = shift or return;

    # prepare the document
    my $doc_ref = {
        command => 'delete_client_id',
        ref     => {
            client_id => $client_id,
        },
    };

    # add the callbacks + boilerplate
    my @json = $self->_to_doc( $doc_ref );

    # send it to artirix
    return $self->api_post( \@json );
}

# utility methods for preparing record
sub _to_doc {
    my $self = shift;
    my %document = %{ shift(@_) };

    if ( !$document{callbacks} ) {
        require Stream::TP2::Callbacks;
        my $callbacks = Stream::TP2::Callbacks->new_from_doc( \%document );
        $callbacks->create();
        $document{callbacks} = $callbacks->all();
    }

    return {
        collection => 'broadbean',
        uri        => '',
        document   => \%document,
    };
}

sub search {
    my $self   = shift;
    my $query  = shift;

}

#
# under the hood
#

sub api_post_doc {
    my $self = shift;
    my $doc  = shift;

    my @json = (
        $self->_to_doc( $doc ),
    );

    return $self->api_post( \@json );
}

sub api_post {
    my $self = shift;
    my $json = shift;
    my $options_ref = shift || {};

    my $api_url = $options_ref->{low_priority} ? $self->api_b_url() : $self->api_url();

    return $self->json_post(
        $api_url,
        $json,
    );
}

sub search_post {
    my $self = shift;
    my $query_form = shift;

    return $self->form_post(
        $self->search_url(),
        $query_form,
    );
}

sub form_post {
    my $self = shift;
    my $url  = shift;
    my $query_form = shift;

    my $req = HTTP::Request::Common::POST( $url => (
            Content => $query_form,
        ),
    );

    return $self->request( $req );
}

sub json_get {
    my $self = shift;
    my $url  = shift;
    my $json = shift;

    if ( ref( $json ) ) {
        $json = $self->to_json( $json );
    }

    $log->debug("Querying GET ".$url." with ".( $json // 'NO DATA' ));
    my $req = HTTP::Request::Common::GET( $url => (
            Content      => $json || '',
            Content_type => 'application/json',
        ),
    );

    my $raw_resp = $self->request( $req );

    my $resp = Stream::TP2::Response->new( $raw_resp );
    return $self->last_response( $resp );
}

sub json_post {
    my $self = shift;
    my $url  = shift;
    my $json = shift;

    if ( ref( $json ) ) {
        $json = $self->to_json( $json );
    }
    $log->info( "POSTING ".( $json // 'NO DATA' )." to $url" );
    my $req = HTTP::Request::Common::POST( $url => (
            Content      => $json,
            Content_type => 'application/json',
        ),
    );

    my $raw_resp = $self->request( $req );

    my $resp = Stream::TP2::Response->new( $raw_resp );
    return $self->last_response( $resp );
}

sub request {
    my $self = shift;
    my $req  = shift;

    $req->authorization_basic(
        Stream::TP2::Constants::auth_username(),
        Stream::TP2::Constants::auth_password(),
    );

    my $agent = $self->agent();
    my $raw_resp = $agent->request( $req );
    $self->last_raw_response( $raw_resp );
    return $raw_resp;
}

#
# Utility methods
#

sub to_json {
    my $self = shift;
    my $json = shift;

    return JSON::to_json( $json, {utf8 => 1} );
}

1;

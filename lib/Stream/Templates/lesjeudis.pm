package Stream::Templates::lesjeudis;

use strict;
use utf8;

use base qw(Stream::Templates::careerbuilder);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
use Stream::Constants::SortMetrics ':all';

BEGIN {
    __PACKAGE__->inherit_tokens();

    delete $standard_tokens{military_experience};
    delete $standard_tokens{relocation_filter};
    delete $standard_tokens{school};
    delete $standard_tokens{work_status};
    delete $standard_tokens{company};
    delete $standard_tokens{management_experience};
    delete $standard_tokens{maximum_travel_requirements};
    delete $standard_tokens{minimum_travel_requirements};
    delete $standard_tokens{maximum_commute};
    delete $standard_tokens{minimum_employees_managed};
    delete $standard_tokens{security_clearance};
    delete $standard_tokens{exclude_ivr_resumes};
    delete $standard_tokens{minimum_degree};
    delete $standard_tokens{category};
    delete $standard_tokens{currently_employed};
    delete $posting_only_tokens{military_experience_csv};

    $authtokens{lesjeudis_email} = {
        Label => 'Email Address',
        Type  => 'Text',
        Size  => 25,
        SortMetric => 1,
    };

    $authtokens{lesjeudis_password} = {
        Label => 'Password',
        Type  => 'Text',
        Size  => 25,
        SortMetric => 2,
    };

    $standard_tokens{jobtype} = {
        Label => 'Additional Job Types',
        Type  => 'MultiList',
        Values => '(Contrat de professionalisation=ETSP|Contrat d’apprentissage=ETSA|Stage=ETIN|Indépendant=ETFL|VIE=ETIV|Bénévolat=ETCW|Saisonnier=ETSE|Franchises=ETFR)',
        SortMetric=> 100,
    };

    $derived_tokens{salary_cur} = {
        Label        => 'Currency',
        Type         => 'Currency',
        Options      => 'EUR',
        Helper       => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric   => $SM_SALARY,
    };

    $standard_tokens{minimum_degree} = {
        Label => 'Minimum Degree',
        Type  => 'List',
        Values => '(Non précisé=CENS|Aucun(e)=CE3|BEP, CAP=CEF01|Niveau Bac=CEF02|Bac Professionnel=CEF05|Bac général=CE31|DUT, BTS, Bac + 2=CE32|Licence, Bac+3=CEU3YRS|Maîtrise, IEP, IUP, Bac + 4=CE321|Master, DESS, DEA=CEU5YRS|Ecole d’ingenieur=CEF04|Doctorat, 3eme cycle=CEUCDR)',
        SortMetric=> 55,
    };

};

sub careerbuilder_niche_sites { return qw(LJ); }

sub careerbuilder_jobtype_helper {
    my ($self) = @_;

    my @jobtypes = ();

    if ( my $jobtype = $self->token_value('default_jobtype') ) {
        push(@jobtypes, 'ETPE') if $jobtype eq 'permanent';
        push(@jobtypes, 'ETTF') if $jobtype eq 'temporary';
        push(@jobtypes, 'ETCT') if $jobtype eq 'contract';
    }

    # custom jobtypes, can be multiple
    if ( $self->token_value('jobtype') ) {
        my @custom_jobtypes = $self->token_value('jobtype');
        push @jobtypes, @custom_jobtypes;
    }
    return join ('|', @jobtypes);
}

1;

package Stream::Templates::Jobsite;

=head1 NAME

Stream::Templates::Jobsite - Jobsite Search Agent

=head1 AUTHOR

ACJ - 19/02/2009

=head1 CONTACT

Ian Young <ian.young@jobsite.co.uk>
Software Engineer

May be better trying Steve O'Connor <steve.oconnor@jobsite.co.uk> who is
Head of Software Development when Ian is busy.

=head1 TESTING INFORMATION

Agency ID: 18462

Email: broadbean@iyoung.dev.jobsite.co.uk

=head1 DOCUMENTATION

Web Service URL
L<http://www.jobsite.co.uk/cgi-bin/cv_search_webservice.cgi>

Documentation
L<http://trac/adcourier/wiki/MultipleCvSearch>

=head1 OVERVIEW

All posts are XML over HTTP (it is a SOAP interface)

Each XML request and response has a message hash with the XML.

Stream::Engine::Jobsite will add the SOAP wrapper
and message hash automatically

Stream::Agent::Jobsite will verify the message
and decode the SOAP request automatcially


The CV is base64 encoded.

Jobsite allow up to 1000 characters in the keywords

=head1 EXTRA INFORMATION

IP Restricted

Request message hash: jh:hn&*Hxg516@B,Ikg$OjTy60k*

Response message hash: kVft5£d9,^5fp~30sSxiUZalq!wt

£ MUST BE A Windows-CP1252 £ SIGN NOT A UTF-8

=head2 LOCATIONS

Tips from Steve O'Conner @ Jobsite

%preferredCountries% - don't use unless looking for locations ourside of the UK

%city% / cityWithin - best field to search on. city may be a postcode
(only the outcode is used)

Don't combine city + counties - you will get less results than a search for just the city.

%preferredCounties% on their own will get results but not as many as
sending the city / cityWithin

While Jobsite support "freetext" locations, Jobsite resolve the location to an internal code.
If a location is ambigious/wrong or doesn't exist in jobsite's db then who knows
what will happen (Jobsite don't) so best to send a postcode

The logic is:

If they select a non-UK location, we send %preferredCountries%.

If they select a city, we will send a postcode (we will send the city name only if we can't
work out a postcode).

If they select a county, we will try to send a postcode. We will send %preferredCounties% if
we can't find a postcode.

If they select anything else, we will try to send %preferredCounties%. If they select a
location that is too vague, we will silently ignore the location.

=head2 KNOWN BUGS

relocation_yes/relocation_maybe do not work

sending either relocation_yes or relocation_maybe return the same results

Sending any value in relocation_yes or relocation_maybe make it return the same results
It returns the same results if you send Y or N.

=head1 ERROR MESSAGES

=over

=item

<error>The supplied XML is invalid.</error>

May mean missing agency id and email address

=back

=head1 MAJOR CHANGES

=over 4

=item * 27/04/10 - ACJ - Updated the list of other sites that Jobsite can search. Corrected This Is site code.

=item * 20/04/10 - ACJ - Updated the list of other sites that Jobsite can search

=back

=head1 BUG FIXES

=over

=item

Need to send both reqdSalaryLow & reqdSalaryHigh together
if either has a value

=item

If salary_from was lower than the lowest salary band we sent the salary_from value
instead of ANY

=item

The jobsite webservice defaults are different to Jobsite's website.

Defaulting stemmed terms=YES, EU permit=YES, Maybe Relocate=NO, Willing to Relocate=NO

=item

Searching ALL Jobsite sites was not working. If we do not send a site, Jobsite default
to only searching Jobsite CVs (they did not search all jobsite sites).

=item 07/04/2010

Sorting order not working because of typo's in the token name. The token was called
sort_by. We were sending %sort_score%, the token was really called %sort_by%

=item 27/04/2010

The site code list from Jobsite has This Is down as 'T%'. It should be 'NE' instead.

=back

=cut

use strict;

use utf8;

use Stream::Constants::SortMetrics ':all';
use MIME::Base64;

use base qw(Stream::Engine::API::XML::SOAP::Jobsite);

sub REQUEST_KEY {
    return 'jh:hn&*Hxg516@B,Ikg$OjTy60k*';
}

sub RESPONSE_KEY {
    # Must be a windows cp1252 string
    # chr(163) = £
    return 'kVft5'.chr(163).'d9,^5fp~30sSxiUZalq!wt';
}

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################

BEGIN {

=head1 TOKENS

=head2 STANDARD TOKENS

=head3 Roles (%roles%)

Freetext, up to 500 characters.

eg. software engineer

=cut

    $standard_tokens{'roles'} = {
        Label      => 'Preferred Jobtitle',
        Type       => 'Text',
        Size       => 60,
        MaxLength  => 500,
        Example    => 'eg. software engineer',
        SortMetric => $SM_KEYWORDS,
    };

=head3 Main Skills (%main_skills%)

List of skills. Freetext, up to 500 characters.

=cut

    $standard_tokens{'main_skills'} = {
        Label      => 'Main Skills',
        Type       => 'Text',
        Size       => 60,
        MaxLength  => 500,
        SortMetric => $SM_KEYWORDS,
    };

=head3 searchString (%keywords%)

Up to 1000 characters, freetext field

=cut

=head3 Search Name (%search_name%)

Freetext, up to 128 characters.

eg. Andy Jones

=cut

    $standard_tokens{'search_name'} = {
        Label      => "Candidate's Name",
        Type       => 'Text',
        Size       => 60,
        MaxLength  => 128,
        SortMetric => $SM_KEYWORDS,
    };

=head3 thesaurus (%thesaurus%)

Either Yes or No

=cut

    $standard_tokens{'thesaurus'} = {
        Label      => 'Search for stemmed terms?',
        Type       => 'List(Yes=Y|No=N)',
        Default    => 'Y',
        SortMetric => $SM_SEARCH,
    };

=head3 Job Type (%jobtypes%)

Multiple selections supported. Can be any combination of the following:

=over 4

=item Any (E)

=item Permanent (P)

=item Contract/Temp (C)

=item Temporary/Fixed Term (T)

=item Part-Time (H)

=item Contract/Interim (X)

=back

=cut

    $standard_tokens{'jobtypes'} = {
        Label      => 'Job Type',
        Type       => 'MultiList(Any=E|Permanent=P|Contract/Temp=C|Temporary/Fixed Term=T|Part-Time=H|Contract/Interim=X)',
        Deprecated => "JobType",
        SortMetric => $SM_JOBTYPE,
    };


=head3 EU Permit (%eu_permit%)

Has a permit to work in the EU. Either Yes or No

=cut

    $standard_tokens{'eu_permit'} = {
        Label      => 'EU Permit',
        Type       => 'List(Yes=Y|No=N)',
        Default    => 'Y',
        SortMetric => $SM_ELIGIBILITY,
    };

=head3 Driving Licence (%driving_licence%)

Has a driving licence. Either Yes or No

=cut

    $standard_tokens{'driving_licence'} = {
        Label      => 'Driving Licence',
        Type       => 'List(Either=|Yes=Y|No=N)',
        Default    => '',
        SortMetric => $SM_ELIGIBILITY,
    };

=head3 Employment Market (%emp_mkt_cds%)

Any combination of:

=over 4

=item All sectors

=item Accounting

=item Automotive

=item Aviation

=item Banking & Finance

=item Construction

=item Consultancy

=item Customer Service

=item Defence

=item Distribution

=item Education

=item Electronics

=item Engineering

=item Graduate

=item Health

=item Human Resources

=item Information Technology

=item Insurance

=item Legal

=item Logistics & Transport

=item Management & Executive

=item Manufacturing

=item Marketing

=item Media

=item Military

=item Production & Operations

=item Public Sector

=item Purchasing

=item Recruitment

=item Retail

=item Sales

=item Scientific

=item Secretarial & Administration

=item Telecommunications

=item Training

=item Travel & Hospitality

=back

=cut

    $standard_tokens{'emp_mkt_cds'} = {
        Label => 'Industry',
        Type  => 'MultiList(All sectors=ALL|Accounting=AC|Automotive=AU|Aviation=AV|Banking & Finance=FI|Construction=CN|Consultancy=CY|Customer Service=CU|Defence=DE|Distribution=DI|Education=ED|Electronics=EL|Engineering=EN|Graduate=GR|Health=OT|Human Resources=HR|Information Technology=IT|Insurance=IN|Legal=LW|Logistics & Transport=LO|Management & Executive=ME|Manufacturing=MA|Marketing=MK|Media=MD|Military=MI|Production & Operations=PO|Public Sector=PS|Purchasing=PU|Recruitment=RC|Retail=RT|Sales=SM|Scientific=SC|Secretarial & Administration=SE|Telecommunications=TE|Training=TR|Travel & Hospitality=TV)',
        SortMetric => $SM_INDUSTRY,
        Default => "ALL",
    };

=head3 Education Codes (%education_codes%)

Any combination of:

=over 4

=item No Formal Qualifications

=item GCSE or equivalent

=item GNVQ or equivalent

=item A Level or equivalent

=item Diploma

=item Pass Bachelors

=item 3rd Class Bachelors

=item Lower 2nd Class Bachelors

=item Upper 2nd Class Bachelors

=item 1st Class Bachelors

=item Masters Degree

=item Doctorate

=back

=cut

    $standard_tokens{'education_codes'} = {
        Label      => 'Education',
        Type       => 'MultiList(No Formal Qualifications=0|GCSE or equivalent=10|GNVQ or equivalent=15|A Level or equivalent=20|Diploma=30|Pass Bachelors=40|3rd Class Bachelors=50|Lower 2nd Class Bachelors=60|Upper 2nd Class Bachelors=70|1st Class Bachelors=80|Masters Degree=90|Doctorate=100)',
        SortMetric => $SM_EDUCATION,
    };

    $derived_tokens{'salary_cur'} = {
        Type         => 'Currency',
        Options      => 'GBP',
        Default      => 'GBP',
        Helper       => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric   => $SM_SALARY,
    };

    $derived_tokens{'salary_per'} = {
        Type         => 'SalaryPer',
        Options      => 'annum|hour',
        Default      => 'annum',
        Helper       => 'SALARY_PER(salary_per|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric   => $SM_SALARY,
    };

=head3 Salary Low (%salary_low%)

Either send Annual or Hourly values.

=cut

    $derived_tokens{'salary_from'} = {
        Type     => 'SalaryValue',
        GBP_hour => '(Any Salary=ANY|4=SALHA4|5=SALHA5|8=SALHA8|10=SALHA10|15=SALHA15|18=SALHA18|20=SALHA20|26=SALHA26|31=SALHA31|36=SALHA36|40=SALHA40|45=SALHA45|50=SALHA50|60=SALHA60|70=SALHA70|75=SALHA75|81=SALHA81|90=SALHA90|100=SALHA100|110=SALHA110|120=SALHA120|130=SALHA130|140=SALHA140)',
        GBP_annum => '(Any Salary=ANY|8000=SALAA8|10000=SALAA10|12000=SALAA12|15000=SALAA15|17000=SALAA17|20000=SALAA20|25000=SALAA25|30000=SALAA30|35000=SALAA35|40000=SALAA40|45000=SALAA45|50000=SALAA50|60000=SALAA60|70000=SALAA70|80000=SALAA80|90000=SALAA90|100000=SALAA100|110000=SALAA110|120000=SALAA120|130000=SALAA130|150000=SALAA150|170000=SALAA170|180000=SALAA180|190000=SALAA190)',
        Helper    => 'SALARY_BAND_BELOW(salary_from|%salary_cur%|%salary_per%|%salary_from%)',
        HelperMetric => 2,
        SortMetric => $SM_SALARY,
    };

=head3 Salary High (%salary_high%)

Either send Annual or Hourly values.

=cut

    $derived_tokens{'salary_to'} = {
        Type     => 'SalaryValue',
        GBP_hour => '(Any Salary=ANY|6=SALHB6|7=SALHB7|9=SALHB9|11=SALHB11|12=SALHB12|14=SALHB14|17=SALHB17|19=SALHB19|21=SALHB21|23=SALHB23|25=SALHB25|28=SALHB28|30=SALHB30|35=SALHB35|39=SALHB39|44=SALHB44|49=SALHB49|59=SALHB59|64=SALHB64|69=SALHB69|74=SALHB74|79=SALHB79|84=SALHB84|89=SALHB89|94=SALHB94|99=SALHB99|104=SALHB104|109=SALHB109|114=SALHB114|119=SALHB119|124=SALHB124|129=SALHB129|134=SALHB134|139=SALHB139|144=SALHB144|150=SALHB150)',
        GBP_annum => '(Any Salary=ANY|9999=SALAB9|11999=SALAB11|13999=SALAB13|14999=SALAB14|16999=SALAB16|18999=SALAB18|19999=SALAB19|21999=SALAB21|24999=SALAB24|27999=SALAB27|29999=SALAB29|32999=SALAB32|34999=SALAB34|39999=SALAB39|44999=SALAB44|49999=SALAB49|54999=SALAB54|59999=SALAB59|64999=SALAB64|69999=SALAB69|74999=SALAB74|79999=SALAB79|84999=SALAB84|89999=SALAB89|94999=SALAB94|99999=SALAB99|109999=SALAB109|119999=SALAB119|129999=SALAB129|139999=SALAB139|149999=SALAB149|159999=SALAB159|169999=SALAB169|179999=SALAB179|189999=SALAB189|199999=SALAB199|200999=SALAB200)',
        Helper       => 'SALARY_BAND_ABOVE(salary_to|%salary_cur%|%salary_per%|%salary_to%)',
        HelperMetric => 2,
        SortMetric => $SM_SALARY,
    };

=head3 Include Any to Any Salaries (%include_any_to_any%)

=cut

    $derived_tokens{'include_any_to_any'} = {
        Label  => 'Include Any to Any Salaries',
        Type   => 'List(Yes=Y|No=N)',
        Helper => 'jobsite_salary_unspecified(%salary_unknown%)',
        SortMetric => $SM_SALARY,
    };

=head3 Notice Periods (%notice_periods%)

Can be any combination

=cut

    $standard_tokens{'notice_periods'} = {
        Label => 'Notice Periods',
        Type  => 'MultiList(Immediate=IM|1 week=1W|2 weeks=2W|3 weeks=3W|4 weeks=4W|6 weeks=6W|8 weeks=8W|16 weeks=16W|3 months +=3M)',
    };

=head3 Languages (%languages%)

Freetext, up to 500 characters

=cut

    $standard_tokens{'languages'} = {
        Label => 'Languages',
        Type  => 'Text',
        Size      => 60,
        MaxLength => 500,
        SortMetric => $SM_LANGUAGE,
    };

=head3 Preferred Countries (%preferred_countries%)

Freetext, up to 500 characters

=cut

    $derived_tokens{'preferred_countries'} = {
        Label => 'Preferred Countries',
        Type  => 'Text',
        Size      => 60,
        MaxLength => 500,
        SortMetric => $SM_LOCATION,
    };

=head3 Preferred Counties (%preferred_counties%)

Freetext, up to 500 characters

=cut

    $standard_tokens{'preferred_counties'} = {
        Label      => 'Preferred Location',
        Example    => 'e.g. London, SO12  (Seperate locations by a comma)',
        Type       => 'Text',
        Size       => 60,
        MaxLength  => 500,
        SortMetric => $SM_LOCATION,
    };
    
    $derived_tokens{partial_postcode} = {
        Type   => 'Text',
        Helper => 'LOCATION_PARTIAL_POSTCODE(%location_id%|%location_id_postcode%)',
        HelperMetric => 1, # Must run before 'jobsite_location_mappings' helper
    };


=head3 City Within (%city_within%)

Used in conjunction with the %city% token

=cut

    $derived_tokens{'city_within'} = {
        Label => 'City Within',
        Type  => 'List(The centre of...=E|1 mile of the centre of ...=1|3 miles of the centre of ...=3|5 miles of the centre of ...=5|10 miles of the centre of ...=10|20 miles of the centre of ...=20|30 miles of the centre of ...=30|50 miles of the centre of ...=50)',
        Helper => 'jobsite_location_mappings(%location_id%|%location_within_miles%)',
        SortMetric => $SM_LOCATION,
        HelperMetric => 2,
    };

=head3 City (%city%)

Freetext, up to 4000 characters, used in conjuction with the %city% token

=cut

    $derived_tokens{'city'} = {
        Label => 'City',
        Type  => 'Text',
        Size  => 60,
        MaxLength => 4000,
        SortMetric => $SM_LOCATION,
    };

=head3 Relocation Yes (%relocation_yes%)

Either yes or no (sending No doesn't seem to work)

=cut

    $standard_tokens{'relocation_yes'} = {
        Label      => 'Relocation Yes',
        Type       => 'List(Yes=Y|No=N)',
        Default    => 'N',
        SortMetric => $SM_LOCATION,
    };

=head3 Relocation Maybe (%relocation_maybe%)

Either yes or no

=cut

    $standard_tokens{'relocation_maybe'} = {
        Label      => 'Relocation Maybe',
        Type       => 'List(Yes=Y|No=N)',
        Default    => 'N',
        SortMetric => $SM_LOCATION,
    };

=head3 Main CV Exclude (%main_cv_exclude%)

Freetext, up to 500 characters

=cut

    $standard_tokens{'main_cv_exclude'} = {
        Label => 'Exclude words',
        Type  => 'Text',
        Size  => 60,
        MaxLength => 500,
        SortMetric => $SM_KEYWORDS,
    };

=head3 Location Exclude (%location_exclude%)

Freetext, up to 500 characters

=cut

    $standard_tokens{'location_exclude'} = {
        Label => 'Exclude locations',
        Type  => 'Text',
        Size  => 60,
        MaxLength => 500,
        SortMetric => $SM_LOCATION,
    };

=head3 Days Between (%days_between%)

Don't show any CVs viewed since this many days ago

Integer only

=cut

    $standard_tokens{'days_between'} = {
        Label => "Don't show me any CVs viewed since this many days ago",
        Type  => 'Text',
        Size  => 3,
        MaxLength => 5,
        Example => 'e.g. 1',
    };

=head3 Sort By (%sort_by%)

Can either sort by most relevant first or most recent first

Default is most relevant first

=cut

    $standard_tokens{'sort_by'} = {
        Label => 'Sort Results By',
        Type  => 'List(Relevance=Y|Last Updated=N)',
        Default => 'Y',
        SortMetric => $SM_SORT,
    };

=head3 Loaded Since (%loaded_since%)

Find candidates who have been added to Jobsite within ....

This is optional in the webservice, but I'm not sure what it defaults to when you don't select anything (it doesn't mean it searches everything)

For consistency we will send 1Y if we can't determine a sensible loaded_since.

=cut

    $derived_tokens{'loaded_since'} = {
        Label      => 'Find candidates added within',
        Type       => 'List(24 hours=0D|2 days=1D|7 days=7D|1 month=1M|3 months=3M|6 months=6M|1 year=1Y)',
        Default    => '1Y',
        Helper     => 'jobsite_loaded_since(%cv_updated_within%)',
        SortMetric => $SM_DATE,
    };

=head3 Search Site (%cvsearch_site%)

Can be any combination of:

NB. If we leave it blank, Jobsite only searches within Jobsite CVs

The jobsite_cvsearch_sites helper will return all sites if there are none selected
because that is the default on the Jobsite website.

=cut

    $standard_tokens{'cvsearch_site'} = {
        Label => 'Sites',
        #Type  => 'MultiList(ALL=|Jobsite=UK|ScotRecruit=SR|Senior Sales Jobs=OS|SecRecruit=SX|Engineer Board=EE|Purely IT=IB|LegalProspects=LE|This is Sites=NE|Careers In Logistics=CL|Recruit Construction Jobs=UC|Marketing JobBoard=MK|OnlineInsuranceJobs=OI|LondonJobs=LJ|inHR=HR|Customer Support=CS|E-Med Careers=EC|OfficeRecruit=OR|EuroMoneyJobs=EM)',
        # This one works for everyone but is missing some sites
        #Type  => 'MultiList(ALL=|Jobsite=UK|ScotRecruit=SR|Senior Sales Jobs=OS|SecRecruit=SX|Engineer Board=EE|Purely IT=IB|LegalProspects=LE|Careers In Logistics=CL|Recruit Construction Jobs=UC|Marketing JobBoard=MK|OnlineInsuranceJobs=OI|LondonJobs=LJ|inHR=HR|Customer Support=CS)',

        # This is the exhaustive list from Jobsite provided by Steve O on 16th Oct 2009
        # "UK","www.jobsite.co.uk","AV,T%,SR,OS,SX,EE,IB,AP,LJ,UC,LE,CL,MK,HR,OI,CS,CO,DB,ER,RJ,PS,BJ,JX,YJ"
        # NB. Do not send a site code of T% for This Is. It will always fail. Send a site code of 'NE' instead
        Type   => 'MultiList(ALL=|BCSrecruit=BC|Banking Jobs UK=BJ|CareersInLogistics=CL|DefenceJobs=4D|ERTOnline=ER|Engineer Board=EE|ICS JobsBoard=CS|InCatering=R4|InHR=HR|InRetail=R1|InRetail Ireland=R2|JobRapido=JR|Jobs My Way=YJ|Jobsection=JX|Jobsite Middle East=DB|Jobsite=UK|Just4Aviation=4A|Just4Graduates=4G|JustConstruction=4C|JustEngineers=4E|JustRail=4R|JustUtilities=4U|Legal Prospects=LE|Localpeople Jobs=LP|London Jobs=LJ|London Metro Jobs=LM|MailOnline=DM|MarketingJobBoard=MK|MyAccountancyJobs=AP|OfficeRecruit=OR|OnlineAviationJobs=AV|OnlineInsuranceJobs=OI|Publicsectorjobs=PS|PurelyIT=IB|RecJobs=RJ|RecruitConstruction=UC|RenewablesCareers=RN|Retail Careers=R3|ScotRecruit=SR|SecRecruit=SX|Senior Sales Jobs=OS|Telecomsjobsource.co.uk=CO|This is Jobs network=NE)',
        Default => '',
    };

    $posting_only_tokens{cvsearch_sites} = {
        Helper => 'jobsite_cvsearch_sites(%cvsearch_site%)',
    };

    $posting_only_tokens{default_jobtypes_id} = {
        ## Any=E|Permanent=P|Contract/Temp=C|Temporary/Fixed Term=T|Part-Time=H|Contract/Interim=X
        Helper => "HASH_MAP_MULTIPLE(%default_jobtype%|permanent=P|contract=C|contract=X|temporary=T)",
        HelperMetric => 1,
    };

    $posting_only_tokens{jobtypes_id} = {
        Helper => 'jobsite_jobtypes(default_jobtypes_id|jobtypes)',
        HelperMetric => 2,
    };

};


############### HELPERS ##########################

## Test plan (jobsite_jobtypes): tests/stream/feeds/gojobsite/03-global-jobtype.t
sub jobsite_jobtypes {
    my $self      = shift;
    my $default_mapped_token = shift;
    my $original_mapped_token = shift;

    my (@default_jobtypes) = $self->token_value( $default_mapped_token );
    if (@default_jobtypes) {
        return @default_jobtypes;
    }

    return $self->token_value( $original_mapped_token );
#    my @args      = @_;
#    my @mapping   = grep { $_ &&                     /\=/ } @args; ## MAPPER
#    my @jobtype   = grep { $_ && length($_) >  1 && !/\=/ } @args; ## NEW FEED
#    my @job_types = grep { $_ && length($_) == 1          } @args; ## OLD FEED 
#    return ($#jobtype == -1) ? ($#job_types == -1 ) ? ( q{} ) : @job_types 
#                             : (map  { $self->HASH_MAP_MULTIPLE($_ => @mapping) } @jobtype)
#                             ;
}

sub jobsite_salary_unspecified {
    my $self = shift;
    my $include_unspecified_salary = shift;

    return $include_unspecified_salary ? 'Y' : 'N';
}

sub jobsite_loaded_since {
    my $self = shift;
    my $cv_updated_within = shift or return '1Y';

    if ( $cv_updated_within eq 'ALL' ) {
        return '1Y';
    }

    if ( $cv_updated_within eq 'TODAY' ) {
        return '0D';
    }

    if ( $cv_updated_within eq 'YESTERDAY' ) {
        return '1D';
    }

    # Ask Liza why.
    if ( $cv_updated_within eq '3D' ) {
        return '2D';
    }

    my $n_days = $self->CV_UPDATED_WITHIN_TO_DAYS( $cv_updated_within );

    if ( $cv_updated_within eq '1W' || $n_days <= 7 ) {
        return '7D';
    }

    if ( $cv_updated_within eq '1M' || $n_days <= 30 ) {
        return '1M';
    }

    if ( $cv_updated_within eq '3M' || $n_days <= 90 ) {
        return '3M';
    }

    if ( $cv_updated_within eq '6M' || $n_days <= 180 ) {
        return '6M';
    }

    return '1Y';
}

# Location helpers
sub jobsite_location_mappings {
    my $self = shift;
    my $location_id = shift or return;
    my $location_within = shift;

    # Replace with Stream2->instance()->location_api if that goes bang
    my $location = $self->location_api->find({
        id => $location_id,
    });

    if ( ! $location ) {
        $self->log_warn( 'Unable to fetch location [%s] from DB', $location_id );
        return;
    }

    my $country = $location->country()
        or return;

    # only set the preferred countries if they have selected a non-UK country
    if ( $location->is_country() || !$country->in_uk() ) {
        $self->log_debug( 'Setting <preferred_countries> to [%s]', $country->name() );
        $self->token_value(
            preferred_countries => $country->name(),
        );
    }

    if ( $location->is_country() || $location->is_continent() ) {
        $self->log_debug( 'A countries was selected so searching the whole country' );
        return;
    }

    # set counties if we are looking at a region (we can't determine the postcode for a region)
    my $county = $location->county();

    if ( !$county ) {
        $self->log_debug( 'A region was selected so searching counties' );
        $self->jobsite_search_counties( $location_id, $location_within );
        return;
    }

    # jobsite only support up to 50 miles away
    # if they have selected greater than 50 miles away then we will fall back to the counties
    if ( $location_within > 50 ) {
        $self->log_debug( 'Searching for counties because they have selected a huge %%location_within%% (%s)', $location_within );
        $self->jobsite_search_counties( $location_id, $location_within );
        return;
    }

    # in the UK try to send the postcode (postcode searching is more reliable)
    my $postcode = $self->token_value('partial_postcode');

    if ( $postcode ) {
        $self->log_debug( 'A city or county was selected and we have found a postcode: <%s>', $postcode );
        $self->token_value(
            city => $postcode,
        );

        return $self->jobsite_city_within( $location_within );
    }

    $self->log_debug( 'A city or county was selected but we could not find the postcode' );

    # no postcode found - probably abroad
    if ( $location->is_place() ) {
        $self->log_debug( 'A city was selected so lets send the city: <%s>', $location->name() );
        $self->token_value(
            city => $location->name(),
        );

        return $self->jobsite_city_within( $location_within );
    }

    # fallback to sending the county

    # preferredCounties is jobsites main location search (not city!)
    $self->log_debug( 'An area or county was selected so lets send the counties' );
    $self->jobsite_search_counties( $location_id, $location_within );
    return;
}

sub jobsite_search_counties {
    my $self = shift;
    my $location_id = shift;
    my $location_within = shift;

    my $location_api = $self->location_api;
    my $location = $location_api->find({
        id => $location_id,
    });

    if ( !$location ) {
        $self->log_warn( 'Unable to fetch location [%s] from DB', $location_id );
        return;
    }

    my @nearby_county_ids = $location_api->get_stream_location_mappings(
        'stream_counties',
        $location_within,
        $location_id
    );

    my @nearby_counties;

    COUNTY:
    foreach my $county_id ( @nearby_county_ids ) {
        $self->log_debug('Fetching county id [%s]', $county_id);
        my $county = $location_api->find({
            id => $county_id,
        }) or next COUNTY;

        push @nearby_counties, $county->name();
    }

    my $existing_counties = $self->token_value( 'preferred_counties' );
    if ( $existing_counties ) {
        # Jenrick CPI want to enter the freetext "preferred" location
        # add their preferred location as the first county so it never gets chopped
        unshift @nearby_counties, $existing_counties;
    }

    my $preferred_counties = join( ', ', @nearby_counties );

    # preferredCounties:
    # must be 500 character(s) or less.
    if ( length($preferred_counties) > 500 ) {
        $self->log_warn('The location criteria is too vague and matches too many locations! Ignoring the location' );
        return;
    }

    $self->log_debug( 'Setting <mapped_preferred_counties> to [%s]', $preferred_counties );
    $self->token_value(
         mapped_preferred_counties => $preferred_counties,
    );

    return;
}

sub jobsite_city_within {
    my $self = shift;
    my $location_within = shift or return 'E';

    my $city_within = $self->build_token( 'city_within' );

    foreach my $option ( sort { $a <=> $b } $city_within->option_values() ) {
        if ( $option >= $location_within ) {
            return $option;
        }
    }

    return;
}

sub jobsite_cvsearch_sites {
    my $self = shift;

    my @cvsearch_sites = grep { $_ } @_;

    if ( @cvsearch_sites ) {
        return @cvsearch_sites;
    }

    return $self->jobsite_select_all_cvsearch_sites();
}

sub jobsite_select_all_cvsearch_sites {
    my $self = shift;

    if ( $self->company() eq 'officeangels' ) {
        # officeangels do not have access to all the jobsite niche boards
        # hardcode to UK for now
        return 'UK';
    }

    if ( $self->company() eq 'huntress' ) {
        # huntress do not have access to all the niche boards
        # hardcode to UK for now
        return 'UK';
    }

    if ( $self->company() eq 'goodmanmasson' ) {
        # goodmanmasson do not have access to all the niche boards
        # hardcode to UK for now
        return 'UK';
    }

    my $list = $self->build_token('cvsearch_site');
    return grep { $_ } $list->option_values();
}

sub token_yes_or_empty {
    my $self = shift;
    my $token = shift;
    my $value = $self->token_value_or_default( $token );

    if ( !$value || lc($value) eq 'n' ) {
        return;
    }

    return $value;
}

#### HELPERS OVER ####


sub default_url {
    return 'http://www.jobsite.co.uk/cgi-bin/cv_search_webservice.cgi';
}

############### LOGIN ############################
############### SEARCH PAGE #######################
sub search_failed {
    return 'systemErrors|dataErrors';
}

sub search_soap_action { return 'Jobsite_cv_search_webservice#search'; }

sub search_build_xml {
    my $self = shift;
    
    $self->jobsite_catch_missing_credentials();

    return $self->soap_xml({
        body => \&search_build_soap_body_xml,
    });
}

sub jobsite_catch_missing_credentials {
    my $self = shift;

    my $agency_id = $self->token_value('agency_id');
    my $email     = $self->token_value('email');

    if ( !$agency_id && !$email ) {
        return $self->throw_login_error('Missing Agency ID and Email address - fill in your agency id and email address and try again');
    }
    if ( !$agency_id ) {
        return $self->throw_login_error('Missing Agency ID. Fill in your agency ID and try again');
    }
    if ( !$email ) {
        return $self->throw_login_error('Missing Email address. Fill in your email address and try again');
    }

    return;
}

# Overwrite in individual feeds as Jobsite add support for pay per view to each feed
# Also add a profile template for board, as this will enable the profile option in stream
sub requires_cv_unlock {
    return 0;
}

sub search_build_soap_body_xml {
    my $self = shift;
    my $xml = shift;

    $xml->startTag('cvSearchRequest');
    {
        $xml->startTag('recruiter');
        {
            $xml->startTag('agencyId');
                $xml->characters( $self->token_value('agency_id') );
            $xml->endTag();

            $xml->startTag('email');
                $xml->characters( $self->token_value('email') );
            $xml->endTag();
        }
        $xml->endTag();

        $xml->startTag('params');

        if ( my $roles = $self->token_value('roles') ) {
            $xml->startTag('roles');
                $xml->characters( $roles );
            $xml->endTag();
        }

        if ( my $main_skills = $self->token_value('main_skills') ) {
            $xml->startTag('mainSkills');
                $xml->characters( $main_skills );
            $xml->endTag();
        }

        if ( my $keywords = $self->token_value('keywords') ) {
            if ( $keywords =~ m/\*/ ){
                $keywords =~ s/\*//g; # * breaks search - Jobsite will not fix it #54128
                $self->results()->add_notice('We removed all wildchars ( * ) from your query, because Jobsite doesnt support them');
            }
            $xml->startTag('searchString');
                $xml->characters( $keywords );
            $xml->endTag();
        }

        if ( my $search_name = $self->token_value('search_name') ) {
             $xml->startTag('searchName');
                 $xml->characters( $search_name );
             $xml->endTag();
        }

        if ( my $thesaurus = $self->token_value_or_default('thesaurus') ) {
             $xml->startTag('thesaurus');
                 $xml->characters( $thesaurus );
             $xml->endTag();
        }

        foreach my $jobtype ( $self->token_value('jobtypes_id') ) {
            $xml->startTag( 'jobtype' );
                $xml->characters( $jobtype );
            $xml->endTag();
        }

        if( !$self->token_value('jobtypes_id') ){
            $xml->startTag( 'jobtype' );
                $xml->characters( 'E' );
            $xml->endTag();
        }

        if ( my $eu_permit = $self->token_value_or_default('eu_permit') ) {
             $xml->startTag('euPermit');
                 $xml->characters( $eu_permit );
             $xml->endTag();
        }

        if ( my $driving_licence = $self->token_value('driving_licence') ) {
             $xml->startTag('drivingLicence');
                 $xml->characters( $driving_licence );
             $xml->endTag();
        }

        foreach my $emp_mkt_cd ( $self->token_value('emp_mkt_cds') ) {
            if (length $emp_mkt_cd) {
                $xml->startTag('empMktCd');
                    $xml->characters( $emp_mkt_cd );
                $xml->endTag();
            }
        }

        foreach my $education_code ( $self->token_value( 'education_codes' ) ) {
            if (length $education_code) {
                $xml->startTag('educationCode');
                    $xml->characters( $education_code );
                $xml->endTag();
            }
        }

        my $salary_low = $self->token_value( 'salary_from' );
        my $salary_high = $self->token_value( 'salary_to' );
        if ( $salary_low || $salary_high ) {
             # must send salary low & high together
             # if one is specified we must send the other
             $xml->startTag( 'reqdSalaryLow' );
                 $xml->characters( $salary_low || 'ANY');
             $xml->endTag();

             $xml->startTag( 'reqdSalaryHigh' );
                 $xml->characters( $salary_high || 'ANY' );
             $xml->endTag();


             $xml->startTag( 'annualHourlyReqdSalaryLow' );
                 if($self->token_value('salary_per') eq "annum"){
                     $xml->characters( "annual");
                 }else{
                     $xml->characters( "hourly");
                 }
             $xml->endTag();

        }

        if ( my $include_any_to_any = $self->token_value('include_any_to_any') ) {
             $xml->startTag( 'includeAnyToAnySalaries' );
                 $xml->characters( $include_any_to_any );
             $xml->endTag();
        }

        foreach my $notice_period ( $self->token_value( 'notice_periods' ) ) {
            if (length $notice_period) {
                $xml->startTag( 'noticePeriod' );
                    $xml->characters( $notice_period );
                $xml->endTag();
            }
        }

        if ( my $languages = $self->token_value('languages') ) {
             $xml->startTag( 'languages' );
                 $xml->characters( $languages );
             $xml->endTag();
        }

        if ( my $preferred_countries = $self->token_value('preferred_countries') ) {
             $xml->startTag( 'preferredCountries' );
                 $xml->characters( $preferred_countries );
             $xml->endTag();
        }

        if ( my $preferred_counties = $self->token_value( 'mapped_preferred_counties' ) || $self->token_value( 'preferred_counties' ) ) {
             $xml->startTag( 'preferredCounties' );
                 $xml->characters( $preferred_counties );
             $xml->endTag();
        }

        if ( my $city_within = $self->token_value( 'city_within' ) ) {
             $xml->startTag( 'cityWithin' );
                 $xml->characters( $city_within );
             $xml->endTag();
        }

        if ( my $city = $self->token_value( 'city' ) ) {
             $xml->startTag( 'city' );
                 $xml->characters( $city );
             $xml->endTag();
        }

        if ( my $relocation_yes = $self->token_yes_or_empty( 'relocation_yes' ) ) {
             $xml->startTag( 'relocationYes' );
                 $xml->characters( $relocation_yes );
             $xml->endTag();
        }

        if ( my $relocation_maybe = $self->token_yes_or_empty( 'relocation_maybe' ) ) {
             $xml->startTag( 'relocationMaybe' );
                 $xml->characters( $relocation_maybe );
             $xml->endTag();
        }

        if ( my $main_cv_exclude = $self->token_value( 'main_cv_exclude' ) ) {
             $xml->startTag( 'mainCvExclude' );
                 $xml->characters( $main_cv_exclude );
             $xml->endTag();
        }

        if ( my $location_exclude = $self->token_value( 'location_exclude' ) ) {
             $xml->startTag( 'locationExclude' );
                 $xml->characters( $location_exclude );
             $xml->endTag();
        }

        if ( my $days_between = $self->token_value( 'days_between' ) ) {
             $xml->startTag( 'daysBetween' );
                 $xml->characters( $days_between );
             $xml->endTag();
        }

        if ( my $sort_score = $self->token_value( 'sort_by' ) ) {
             $xml->startTag( 'sortScore' );
                 $xml->characters( $sort_score );
             $xml->endTag();
        }

        if ( my $loaded_since = $self->token_value( 'loaded_since' ) ) {
             $xml->startTag( 'loadedSince' );
                 $xml->characters( $loaded_since );
             $xml->endTag();
        }

        foreach my $cvsearch_site ( $self->token_value('cvsearch_sites') ) {
            $xml->startTag( 'cvsearchSite' );
                $xml->characters( $cvsearch_site );
            $xml->endTag();
        }

        $xml->startTag('offset');
        $xml->characters(($self->current_page() - 1) * $self->results_per_page() + 1);
        $xml->endTag;

        $xml->startTag('total');
        $xml->characters($self->results_per_page());
        $xml->endTag;

        $xml->endTag();
    }

    $xml->endTag();
}

sub results_per_page {
    return 30;
}

# only scrape 100 out of the 300 results from jobsite
# because no one ever looks at the results from 101 -> 300
# and scraping them slows down the search
sub scrape_candidate_xpath { return '//cv'; }
sub scrape_candidate_details {
    return (
        candidate_id     => './applicantId/text()',
        firstname        => './firstname/text()',
        surname          => './surname/text()',
        skills           => './mainSkills/text()',
        roles            => './roles/text()',
        jobtype_ids      => './jobtype/text()',
        work_permit      => './workPermit/text()',
        salary           => './salary/text()',
        location_text    => './locns/text()',
        city             => './city/text()',
        dob              => './dob/text()',
        email            => './email/text()',
        relocation       => './relocation/text()',
        raw_last_updated => './loadedDate/text()',
        work_required    => './workReqd/text()',
        telephone        => './tel/text()',
        mobile_telephone => './mobTel/text()',
        start_date       => './startDt/text()',
        notice_period    => './noticePeriod/text()',
        languages        => './languages/text()',
        online_days      => './onlineDays/text()',
        watchdog_days    => './watchdogDays/text()',
        snippet          => './cvSummary/text()',
        order            => './order/text()',
        unlocked         => './isUnlocked/text()',
    );
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    # fix jobtype
    my @jobtype_ids = split /,/, $candidate->attr('jobtype_ids');

    my @jobtypes;
    foreach my $jobtype_id ( @jobtype_ids ) {
        push @jobtypes, $self->lookup_jobtype( $jobtype_id );
    }

    my $jobtype = join ', ', @jobtypes;
    $candidate->attr('jobtype' => $jobtype );

    # date format is 18 Oct 2009 - 22:24
    my $last_updated = $candidate->attr('raw_last_updated');
    if ( $last_updated ) {
        $candidate->cv_last_updated_format( '%d %b %Y - %T', $last_updated );
    }

# Disable the profile for now
#    $candidate->has_profile( 1 );
#    $candidate->profile_downloaded( time );

    if ( $self->requires_cv_unlock ) {
        $candidate->has_profile( 1 );
    }
    return $candidate;
}

sub lookup_jobtype {
    shift;
    my $code = shift;

    my %code_to_type = (
        P => 'Permanent',
        X => 'Contract',
        T => 'Temporary/Fixed Term',
        H => 'Part Time',
    );

    return $code_to_type{ $code };
}

sub search_number_of_results_xpath { return '//cvSearchResponse/totalResults'; }


######### DOWNLOAD PROFILE / TEXT CV ##########
sub download_profile_soap_action { return 'Jobsite_cv_search_webservice#view'; }

sub download_profile_xml {
    my $self = shift;
    my $candidate = shift;

    my $xml = $self->soap_xml({
        body => sub {
            my $self = shift;
            my $xml  = shift;

            $xml->startTag('cvSearchRequest');
            {
                $xml->startTag('recruiter');
                {
                    $xml->startTag('agencyId');
                        $xml->characters( $self->token_value('agency_id') );
                    $xml->endTag();

                    $xml->startTag('email');
                        $xml->characters( $self->token_value('email') );
                    $xml->endTag();
                }
                $xml->endTag();

                $xml->startTag('params');
                {
                    $xml->startTag('cvType');
                        $xml->characters('text');
                    $xml->endTag();

                    $xml->startTag('applicantId');
                        $xml->characters( $candidate->candidate_id() );
                    $xml->endTag();
                }
                $xml->endTag();
            }
            $xml->endTag();
        },
    });

    return $xml;
}

sub scrape_download_profile_xpath   { return '//cv'; }

sub scrape_download_profile_details {
    my $self = shift;
    return (
        raw_textcv => './textCv',
    );
}

sub scrape_fixup_candidate_profile {
    my $self = shift;
    my $candidate = shift;

    my $textcv = MIME::Base64::decode_base64( $candidate->attr('raw_textcv') );
    $candidate->attr( text_cv => $textcv );

    $self->log_debug( 'Decoded text cv is  [%s]', $textcv );


}


#################### DOWNLOAD CV ##############
sub download_cv_soap_action { return 'Jobsite_cv_search_webservice#view'; }

sub download_cv_xml {
    my $self = shift;
    my $candidate = shift;

    $self->login();

    my $xml = $self->soap_xml({
        body => sub {
            my $self = shift;
            my $xml  = shift;

            $xml->startTag('cvSearchRequest');
            {
                $xml->startTag('recruiter');
                {
                    $xml->startTag('agencyId');
                        $xml->characters( $self->token_value('agency_id') );
                    $xml->endTag();

                    $xml->startTag('email');
                        $xml->characters( $self->token_value('email') );
                    $xml->endTag();
                }
                $xml->endTag();

                $xml->startTag('params');
                {
                    $xml->startTag('cvType');
                        $xml->characters('word');
                    $xml->endTag();

                    $xml->startTag('applicantId');
                        $xml->characters( $candidate->candidate_id() );
                    $xml->endTag();
                    if( $self->requires_cv_unlock() ){
                        $xml->startTag('unlockIfRequired');
                            $xml->characters( 'Y' );
                        $xml->endTag();
                    }
                }
                $xml->endTag();
            }
            $xml->endTag();
        },
    });

    return $xml;
}

sub scrape_download_cv_xpath   { return '//cv'; }
sub scrape_download_cv_details {
    my $self = shift;

    return (
        resume_word_base64 => './wordCv',
        content_type       => 'application/msword',
    );
}

sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    if ( $content =~ m/(No suitable nodes are available to serve your request)/ ) {
        return $self->throw_timeout( $1 );
    }

    # e.g. http://blade7.adcourier.com/manage/view-log.cgi?key=4097
    if ( $content =~ m{(The Agency Id/Recruiter Email supplied do not have sufficient rights)} ) {
        return $self->throw_no_cvsearch( $1 );
    }

    if ( $content =~ /(You have no licence assigned to spend CV Views)/ ) {
        return $self->throw_lack_of_credit( $1 );
    }

    # e.g. http://blade7.adcourier.com/manage/view-log.cgi?key=27161
    if ( $content =~ m{(You do not have access to this service)} ) {
        return $self->throw_no_cvsearch( $1 );
    }

    if ( $content =~ m{>searchString</fieldName>} && $content =~ m{must be (\d+) character} ) {
        return $self->throw_keywords_too_long( 'searchString must be '.$1.' characters or less' );
    }

    if ( $content =~ m/(one or more of the selected sites are invalid)/ ) {
        # use ~/backend/scripts/stream/jobsite_homesite.pl to work out which site is causing the error
        return $self->throw_known_internal( $1 );
    }

    if ( $content =~ m/(The CV chosen is still being processed)/ ){
        return $self->throw_cv_not_ready( $1 );
    }

    if ( $content =~ m/We are performing some essential maintenance/i ){
        # https://search.adcourier.com/searchrecords/LqtBPZIISiWxTqh97EEXMg
        return $self->throw_unavailable(q{Jobsite is down for maintanence});
    }


    if ( $content =~ m{<error>(.+?)</error>} ) {
        return $1;
    }

    if ( $message =~ m/\s*404 Not Found\s*/ ) {
        return $self->throw_unavailable(q{404: -seen Jobsite's site apology page});
    }

    if ( $content =~ /the site is experiencing delays at this time/ ) {
        # http://blade7.adcourier.com/manage/view-log.cgi?key=233697&destination=
        return $self->throw_unavailable(q{We apologise that the site is experiencing delays at this time. Please try again in a short while.});
    }

    return undef;
}


1;
# vim: expandtab tabstop=4 encoding=utf-8

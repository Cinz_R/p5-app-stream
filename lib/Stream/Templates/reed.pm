#!/usr/bin/env perl
#===============================================================================
#
#         FILE:  reed.pm
#
#  DESCRIPTION:
#         DOCS:  http://www.reed.co.uk/developers/recruiter
#        FILES:  ---
#         BUGS:  ---
#        NOTES:  Test account: aliz@broadbean.com / broadbean1
#      CONTACT:  recruiter.apis@reedonline.co.uk
#      COMPANY:  Broadbean
#       AUTHOR:  Elizaveta
#      CREATED:  22/08/14 10:00
#===============================================================================
package Stream::Templates::reed;
use base qw{Stream::Engine::API::REST};
use Digest::SHA qw(sha1 sha1_hex sha1_base64 hmac_sha1_base64);
use MIME::Base64;
use POSIX qw(strftime);
use Digest::MD5 qw(md5_hex);
use Stream::Constants::SortMetrics qw(:all);
use URI::Escape qw/uri_unescape uri_escape/;
use Unicode::Normalize;
use JSON;
use URI;
use HTTP::Request;
use Encode qw(decode);
use Stream2::O::File;
use utf8;
use strict;
use warnings;

use vars qw{%authtokens %standard_tokens %derived_tokens %posting_only_tokens};

$authtokens{reed_username}= {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{reed_posting_key} = {
    Label      => 'Posting Key',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 3,
};

$derived_tokens{'salary_per'} = {
    Type         => 'SalaryPer',
    Options      => 'annum|hour',
    Default      => 'annum',
    Helper       => 'SALARY_PER',
    HelperMetric => 1,
};

$posting_only_tokens{'reed_keywords'} = {
    Helper       => 'reed_keywords'
};

sub base_url{
    return "https://www.reed.co.uk/recruiter/api/1.0";
}

sub search_url{
    my $self = shift;
    return $self->base_url() . "/cvsearch";
}

sub reed_max_keyword_length { 500; }

sub reed_keywords {
    my ( $self ) = @_;
    my $keywords = $self->token_value('keywords') // '';
    if ( length( $keywords ) > $self->reed_max_keyword_length() ){
        my $max_length = $self->reed_max_keyword_length();
        return $self->throw_keywords_too_long(sprintf("%s characters maximum", $max_length), { limit => $max_length });
    }
    return $keywords;
}

sub filter_keywords {
    my ($self, $keywords) = @_;

    $keywords =~ s/[^-A-z() 0-9,#"*+.']//g;
    $keywords =~ s/[\[\]\\]//g; #not sure why these characters won't strip in above regex, doing separately
    return $keywords;
}

sub search_query{
    my $self = shift;
    my %fields;

    #location is postcode
    if ($self->token_value('location_id_postcode')) {
        $fields{'location'} = $self->token_value('location_id_postcode');
    }
    else { #location is id
        my $location_id = $self->token_value('location_id');
        if ($location_id) {
            my $location = $self->location_api()->find( { id => $location_id } );
            if (! $location ) {
                $self->log_warn('Unable to fetch location [%s] from |DB', $location_id);
            }

            $fields{'location'} = $location->name();

            my $reed_custom_loc = $self->location_api()->find($location_id)->get_exact_mapping('reed_custom_locs');
            $self->log_debug("got a custom location mapping for Reed: $reed_custom_loc") if $reed_custom_loc;

            if ( $location->type() !~ m/country|region/ && $location->county()->name()) {
                $fields{'location'} .= ', ' . $location->county()->name();
            }

            #Reed may not like non-ascii characters, converting to the characters alphabet equivalent
            $fields{'location'} = $self->reed_normalize_string($fields{'location'});

            $fields{'location'} = $reed_custom_loc if ($reed_custom_loc);

        }
    }

    $fields{'radius'} = int $self->token_value('location_within_miles');

    #job type
    {
        my $job_type = $self->token_value('default_jobtype') || '';
        if    ( $job_type eq "permanent" ) { $fields{'permanent'} = 'true'; }
        elsif ( $job_type eq "temporary" ) { $fields{'temporary'} = 'true'; }
        elsif ( $job_type eq "contract" )  { $fields{'contract'}  = 'true'; }
    }

    $fields{$self->token_value('hours')} = 'true' if ($self->token_value('hours'));

    #salary
    if ($self->token_value('salary_to')){ $fields{'salaryTo'} = $self->token_value('salary_to'); }
    if ($self->token_value('salary_from')){ $fields{'salaryFrom'} = $self->token_value('salary_from'); }
    if ($self->token_value('salary_per') eq "annum"){ $fields{'salaryType'} = '5'; }
    if ($self->token_value('salary_per') eq "hour"){ $fields{'salaryType'} = '1'; }

    #keywords
    my $original_keywords = $self->token_value('keywords') // '';
    $fields{'keywords'} = $self->filter_keywords($original_keywords);

    if ( $fields{'keywords'} ne $original_keywords ) {
        $self->results()->add_notice('We actually searched for: ' . uri_unescape($fields{'keywords'}));
    }

    #other fields

    $fields{'titleOnly'} = $self->token_value('title_only') || 'false';

    my @parent_sectors = ();
    my @sectors = ();
    foreach my $sector ($self->token_value('multi_sectors')){
        if ($sector =~ m/YOLO-(\d+)/) {
            push @parent_sectors, $1;
        }
        else {
            $sector =~ s/^(.*?)://g;
            push @sectors, $sector;
        }
    }

    $fields{'sectors'} = \@sectors  if (@sectors && $self->token_value('multi_sectors'));
    $fields{'parentSectors'} = \@parent_sectors if (@parent_sectors && $self->token_value('multi_sectors'));

    $fields{'activityType'} = $self->token_value('activity') || '1';
    #$fields{'activityTimeFrame'} = $self->token_value('activity_time')  if ($self->token_value('activity_time'));

    my %cv_updated = (
        'TODAY' => '1',
        'YESTERDAY' => '2',
        '3D' => '3',
        '1W' => '7',
        '2W' => '14',
        '1M' => '30',
        '2M' => '60',
        '3M' => '90',
        '6M' => '180',
        '1Y' => '365',
        '2Y' => '730',
        '3Y' => '1095',
        'ALL' => '9999'
    );
    $fields{'activityTimeFrame'} = $cv_updated{$self->token_value('cv_updated_within')};

    $fields{'includeIneligible'} = $self->token_value('include_ineligible')  if ($self->token_value('include_ineligible'));
    $fields{'hasDrivingLicence'} = $self->token_value('has_driving_licence')  if ($self->token_value('has_driving_licence'));
    $fields{'isCarOwner'} = $self->token_value('is_car_owner')  if ($self->token_value('is_car_owner'));
    $fields{'languages'} = [$self->token_value('languages')]  if ($self->token_value('languages'));
    $fields{'languageFluency'} = $self->token_value_or_default('languages_fluency');
    $fields{'minimumQualification'} = $self->token_value('minimum_qualification')  if ($self->token_value('minimum_qualification'));
    if( $self->token_value('institutions') ){
        my @institutions_arr = split(/,/,$self->token_value('institutions'));
        $fields{'institutions'} = [@institutions_arr];
    }
    $fields{'degreeSubjectKeywords'} = $self->token_value('degree_subject_keywords')  if ($self->token_value('degree_subject_keywords'));
    $fields{'finishedOnStart'} = $self->token_value('finished_on_start')  if ($self->token_value('finished_on_start'));
    $fields{'finishedOnEnd'} = $self->token_value('finished_on_end')  if ($self->token_value('finished_on_end'));
    $fields{'degreeGrade'} = $self->token_value('degree_grade')  if ($self->token_value('degree_grade'));
    $fields{'sortBy'} = $self->token_value_or_default('sort');
    $fields{'pageSize'} = 25;
    $fields{'page'} = $self->current_page();

    return %fields;
}

sub api_key {
    return "9ade48b6-932f-49a2-babc-22292d5c9732"; # API Token
}
sub api_client_id {
    return '147'; # API Client ID
}

sub reed_normalize_string {
    my ($self, $string) = @_;
    $string = NFKD($string);
    $string =~ s/\p{Non_Spacing_Mark}//g; # search accented characters within a word
    return $string;
}

sub reed_build_request {
    my ( $self, %options) = @_;

    my $request = HTTP::Request->new(
        'GET',
        $options{url}
    );
    $options{query} //= {};

    my $key = $self->api_key();
    my $posting_key = $self->token_value('reed_posting_key');
    my $username = $self->token_value('reed_username');
    my $client_id = $self->api_client_id();
    my $browser = LWP::UserAgent->new;
    my $user_agent = $browser->agent;
    my $host = "www.reed.co.uk";

    $request->uri->query_form(
        username => $username,
        postingKey => $posting_key,
        %{$options{query}},
    );

    use DateTime;
    my $dt = DateTime->now();
    $dt->set_time_zone("Europe/London");
    my $now_string = $dt->strftime('%Y-%m-%dT%H:%M:%S');
    my $signature_string = join(
        '',
        'GET',
        $user_agent,
        $request->uri->as_string,
        $host,
        $now_string
    );
    my $dotnet_guid = guid_to_dotnet_guid($key);
    my $digest = hmac_sha1_base64($signature_string, $dotnet_guid);

    $request->headers->header(
        'Content_type'      => 'application/json',
        'Method'            => 'GET',
        'X-ApiClientId'     => $client_id,
        'X-TimeStamp'       => $now_string,
        'X-ApiSignature'    => $digest.'=',
        'User-Agent'        => $user_agent,
    );

    return (
        $request->uri->as_string,
        {$request->headers->flatten}
    );
}

sub search_submit {
    my $self = shift;

    my ($url, $headers) = $self->reed_build_request(
        url => $self->search_url(),
        query => {$self->search_query},
    );
    my $response = $self->get( $url, %{ $headers });
    return $response;
}

sub guid_to_dotnet_guid {

    my @guid = split //,$_[0];
    @guid = map(ord, @guid);
    my @new_guid_order = (6,7,4,5,2,3,0,1,11,12,9,10,16,17,14,15,19,20,21,22,24,25,26,27,28,29,30,31,32,33,34,35);
    my @reordered_guid = @guid[@new_guid_order];
    my $new_guid = join("", map(chr, @reordered_guid));
    my @array = ( $new_guid =~ m/../g );
    my @final_array;

    foreach(@array){
        push(@final_array, hex($_));
    };

    $new_guid = join("", map(chr, @final_array));
    return $new_guid;
}

sub search_success{
    return qr/"totalResults":/;
}

sub results_per_page{
    return 25;
}

sub scrape_number_of_results {
    my $self = shift;
    my $json = JSON::from_json($self->content());

    my $results = $json->{'totalResults'};
    $self->total_results($results);
}

sub scrape_paginator {
    my $self = shift;

    my $json = JSON::from_json($self->content());
    my $no_of_results = $json->{'totalResults'};
    my $page_size = $json->{'pageSize'};

    my $no_of_pages = $no_of_results/$page_size;
    my $dec_part = $no_of_pages-int($no_of_pages);
    if( $dec_part > 0 ){
        $no_of_pages = int($no_of_pages) + 1;
    }
    $self->log_debug("No of pages = " . $no_of_pages);
    $self->total_pages($no_of_pages);
}

sub scrape_candidate_array {
    my $self = shift;
    my $json = JSON::from_json($self->content());
    return @{$json->{'candidates'}};
}

sub scrape_candidate_details {
    my ($self, $json) = @_;
    return %{$json};
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    $candidate->attr(can_enhance => 1);
    $candidate->attr( 'candidate_id' => $candidate->attr('candidateId') );

    if ( $candidate->attr('forename') || $candidate->attr('surname') ) {
        $candidate->attr( 'name' => join(' ', ($candidate->attr('forename'), $candidate->attr('surname'))) );
    }
    else {
        my @cand_title = split(/\/|,/, $candidate->attr('mostRecentJobTitle'));
        $candidate->attr( 'name' => $cand_title[0] );
    }

    if( $candidate->attrs('permanentWork') ){
        $candidate->attrs('permanent_work' => "Permanent");
    }
    if( $candidate->attrs('tempWork') ){
        $candidate->attrs('temp_work' => "Temporary");
    }
    if( $candidate->attrs('contractWork')){
        $candidate->attrs('contract_work' => "Contract");
    }

    $candidate->attrs('date_recently_viewed' => $candidate->attrs('dateRecentlyViewed'));
    $candidate->attrs('preferred_work_locations' => $candidate->attrs('preferredWorkLocations'));

    # employer_org_* attrs used for artirix/talentsearch, best not remove

    # These two things are the same in the context of search
    $candidate->attrs('most_recent_job_title' => $candidate->attrs('mostRecentJobTitle'));
    $candidate->attrs('employer_org_position_title' => $candidate->attrs('mostRecentJobTitle'));

    # These two things are the same in the context of search
    $candidate->attrs('most_recent_employer' => $candidate->attrs('mostRecentEmployer'));
    $candidate->attrs('employer_org_name' => $candidate->attrs('mostRecentEmployer'));

    $candidate->attrs('minimum_salary' => $candidate->attrs('minimumSalary'));
    $candidate->attrs('minimum_temp_rate' => $candidate->attrs('minimumTempRate'));
    $candidate->attrs('created_on' =>  $candidate->attrs('createdOn'));

    my $last_login = $candidate->attrs('lastCandidateLogin');
    if( $last_login =~ m/([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):.*/ ){
        $last_login = $3."-".$2."-".$1." at ".$4.":".$5;
    }
    $candidate->attrs('last_candidate_login' => $last_login );

    my $create_on = $candidate->attrs('createdOn');
    if( $create_on =~ m/([0-9]{4})-([0-9]{2})-([0-9]{2})T([0-9]{2}):([0-9]{2}):.*/ ){
        $create_on = $3."-".$2."-".$1;
    }
    $candidate->attrs('created_on' => $create_on );

    foreach my $field ($candidate->attrs('workHistory')) {
        if( $field->{'dateFrom'} && $field->{'dateFrom'} =~ m/([0-9]{4})-([0-9]{2})-([0-9]{2})T.*/ ){
            $field->{'dateFrom'} = $3."-".$2."-".$1;
        }
        if( $field->{'dateTo'} && $field->{'dateTo'} =~ m/([0-9]{4})-([0-9]{2})-([0-9]{2})T.*/ ){
            $field->{'dateTo'} = $3."-".$2."-".$1;
        }
    }

    # These are used during the profile
    # request to get CV snippets.
    $candidate->attr(
        'search_keywords',
        $self->filter_keywords($self->get_token_value('keywords'))
    );
    $candidate->has_cv(1);
    $candidate->has_chargeable_profile(1);
}

sub eligibility_map {
    return (
        hasDrivingLicence => 'Has driving licence',
        hasCar            => 'Has car',
        isEligible        => 'Eligible to work in the UK',
        noticePeriod      => 'Notice Period',
    );
}

# Takes an arrayref of reed qualifications
# (simlar qualifications should be adjacent in the arrayref)
# Returns them grouped in array refs in a list
# Groups them by datetime, institution and type
sub group_qualifications {
    my ($self, $qualifications) = @_;
    my @groups = ([]);
    
    my @group_by = qw(
        finishedOn
        startedOn
        institutionName
        qualificationType
    );

    my $previous;
    foreach my $current( @$qualifications ) {
        if($previous) {
            # Whether we should separate into a new group
            my $separate = grep {
                $current->{$_} ne $previous->{$_};
            } @group_by;

            # Create group/arrayref;
            unshift(@groups, []) if $separate;
        }

        # Add qualification to latest group
        push $groups[0], $current;
        $previous = $current;
    }

    return \@groups;
}

sub download_profile {
    my ($self, $candidate) = @_;

    my $profile = $self->get_candidate_profile($candidate);

    my @keys = keys %$profile;
    foreach my $key (@keys) {
        $candidate->attr($key => $profile->{$key});
    }

    my @jobtypes;
    push(@jobtypes, 'permanent') if $profile->{permanentWork};
    push(@jobtypes, 'temporary') if $profile->{tempWork};
    push(@jobtypes, 'contract') if $profile->{contractWork};
    push(@jobtypes, 'full time') if $profile->{fullTimeWork};
    push(@jobtypes, 'part time') if $profile->{partTimeWork};

    $candidate->attr(jobtypes => \@jobtypes);

    $candidate->attr(
        'address',
        [grep {$_} (
            $profile->{address},
            $profile->{town},
            $profile->{postcode},
            $profile->{country},
        )]
    );

    $candidate->attr(
        'qualificationGroups',
        $self->group_qualifications( $profile->{'education'} )
    );

    my $cv_response;
    my $cv_html;
    eval {
        $cv_response = $self->do_download_cv($candidate);
        $cv_html = $self->{documents_api}->htmlify(
            $cv_response->decoded_content
        );
    };

    $candidate->attr(cv_html => $cv_html);

    return $candidate;
}

sub do_download_cv {
    my ($self, $candidate) = @_;

    my ($url, $headers) = $self->reed_build_request(
        url => $self->base_url() . '/cvsearch/cv/' . $candidate->candidate_id
    );

    my $response = $self->get( $url, %{ $headers } );
    return $response;
}

sub get_candidate_profile {
    my ($self, $candidate) = @_;
    my ($url, $headers) = $self->reed_build_request(
        url => $self->base_url . '/cvsearch/candidate/' . $candidate->candidate_id
    );
    my $response = $self->get($url, %$headers);
    my $profile = JSON::decode_json($response->decoded_content);

    foreach my $qualification (@{$profile->{education}}) {
        $qualification->{startedOn} =~ s/\A(\d{4}).+/$1/;
        $qualification->{finishedOn} =~ s/\A(\d{4}).+/$1/;
    }

    foreach my $employment (@{$profile->{workHistory}}) {
        $employment->{dateFrom} =~  s/\A(\d{4})-(\d{2})-(\d{2}).+/$3-$2-$1/;
        $employment->{dateTo} =~  s/\A(\d{4})-(\d{2})-(\d{2}).+/$3-$2-$1/;
    }

    return $profile;
}

# Returns a JSON hash of a given candidate's preview.
sub get_candidate_preview {
    my ($self, $candidate) = @_;
    my $agent = $self->agent->clone();
    my ($url, $headers) = $self->reed_build_request(
        url => $self->base_url . '/cvsearch/candidate/' . $candidate->candidate_id . '/preview',
        query => {
            keywords => $candidate->attr('search_keywords') // ''
        }
    );

    $agent->get($url, %$headers);

    my $preview = JSON::decode_json($agent->response->decoded_content);

    $preview->{eligibility}->{isEligible} = $preview->{eligibility}->{isEligible} ?
      'Eligible' : 'Unconfirmed';

    foreach my $qualification (@{$preview->{qualifications}}) {
        $qualification->{startedOn} =~ s/\A(\d{4}).+/$1/;
        $qualification->{finishedOn} =~ s/\A(\d{4}).+/$1/;
    }

    return $preview;
}

sub result_enhancement_details {
    my ($self, $candidate) = @_;
    my $preview = $self->get_candidate_preview($candidate);

    return {
        search_keywords => $candidate->{search_keywords},
        cv_snippets     => $preview->{cvSnippets},
        eligibility     => $preview->{eligibility},
        locations       => $preview->{locations},
        qualifications  => $preview->{qualifications},
        skills          => $preview->{skills},
        sectors         => $preview->{sectorsAppliedFor},
    };
}

sub feed_identify_error {
    my ($self, $message, $content) = @_;
    $content //= $self->recent_content;

    # if an error is in the html content, catch it before decoding the json
    # to avoid it die'ing
    if ($content =~ m/(The website has experienced a temporary fault)/) {
        $self->throw_unavailable("$1. Please try this action again later");
    }
    elsif ($content =~ m/Connection timed out|Can't connect to www\.reed\.co\.uk/) {
        $self->throw_timeout("The operation has timed out on the board. Please try again later");
    }

    my $json = JSON::decode_json($content);

    if($json->{message} =~ m/You don't have a vaild cv access subscription/) {
        $self->throw_lack_of_credit($json->{message});
    }
    elsif($json->{message} =~ m/(You have reached your daily download limit)/) {
        $self->throw_cv_unavailable($1);
    }
    elsif($json->{message} =~ m/You have exceeded your per-hour request limit for this service/) {
        $self->throw_unavailable('Hourly search limit exceeded');
    }
    elsif($json->{message} =~ m/The location provided is not recognized/){
        $self->throw_known_internal('The board does not support this location');
    }
    elsif($json->{message} =~ m/ResponseMessage(.*?)Keywords contain/){
        $self->throw_invalid_boolean_search('Unmatched quote, bracket or missed space? - The keywords used are invalid. Please check your input and try again.');
    }
    elsif($json->{message} =~ m/The candidate you requested is not cv searchable/){
        $self->throw_cv_unavailable('Candidate CV is no longer available');
    }
    elsif($json->{message} =~ m/(Invalid|Inactive) r?ecruiter/) {
        $self->throw_login_error("$1 Recruiter. Your account may be suspended or archived, please contact Reed to resolve this issue.");
    }
    elsif($json->{message} =~ m/You don't have access to CV Search/) {
        $self->throw_login_error('You don\'t have access to Reed CV Search. You need a valid Reed subscription or CV Search credits');
    }
    elsif($json->{modelState} && $json->{modelState}->{postingKey}->[0] =~ m/is not valid for PostingKey/) {
        $self->throw_login_error("Posting Key is not valid");
    }
    elsif($json->{exceptionMessage} && $json->{exceptionMessage} =~ m/The wait operation timed out/) {
        $self->throw_timeout("The operation has timed out on the board. Please try again later");
    }
    elsif($json->{exceptionMessage} && $json->{exceptionMessage} =~ m/The multi-part identifier/) {
        $self->throw_keywords_too_long('500 character limit for keywords on board', { limit => 500 });
    }

}

###############################################

$standard_tokens{title_only} = {
    Type        => 'List',
    Label       => 'Search only in job titles',
    Values      => 'No=false|Yes=true',
    SortMetric  => 1,
};

$standard_tokens{activity} = {
    Type        => 'List',
    Label       => 'Activity type',
    Values      => 'Signed in=1|Registered=2',
    Default     => '1',
    SortMetric  => 2,
};
=cut
$standard_tokens{activity_time} = {
    Type        => 'List',
    Label       => 'Activity time frame',
    Values      => '1 day=1|2 days=2|1 week=7|2 weeks=14|1 month=30|3 month=90|6 month=180|1 year=365',
    Default     => '30',
    SortMetric  => 3,
};
=cut
my @sectors = (
    q{Accountancy=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-2|Accounts Admin=4747:174|Accounts Assistant=1921|Accounts Manager=1756|Analyst=1559|Assistant Accountant=4|Auditor=1556|Bookkeeper=1757|Credit Controller=9|External Auditor (Non-Qual.)=1558|Finance Manager=1918|Graduate training and internship schemes=175|Insolvency=864|Internal Auditor (Non-Qual.)=1557|Legal Cashier=808|Management Accountant (Part-Qual.)=1560|Other Accountancy=15|Part-Qualified Acc.=22|Payroll=23|Practice (Non-Qual.)=176|Project Accountant=1758|Public Sector=1919|Purchase Ledger Clerk=1561|Qualified by Experience=28|Sales Ledger Clerk=1562|Treasury=1920},
    q{Accountancy (Qualified)=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-100|Accountancy Practice=1667|Analyst=841|Banking Accounting=1668|Corporate Finance=1759|Cost Accounting=843|Executive Level=1760|External Auditor=1669|Finance Director=844|Finance Manager=846|Financial Accountant=860|Financial Controller=845|Financial Services Accounting=1670|Forensic & Fraud=847|Group Accountant=1922|IAS/SOX/IFRS=1671|Interim Manager=848|Internal Auditor=1672|Management Accountant=850|Other Accountancy Qualified=859|Project Accountant=853|Public Sector Finance=1673|Qualified by Experience=854|Risk & Compliance=1675|Systems Accountant=856|Tax Accountant=857|Treasury=1676|VAT Specialist=1965},
    q{Admin, Secretarial & PA=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-3|Administrator=177|Bilingual/Multilingual=1958|Data Entry=178|DTP/Graphics=1960|Electoral Services=2031|Executive Assistant=1959|Executive PA=1761|HR Administrator=457|Legal Secretary=334|Medical Secretary=180|Office Assistant=868|Office Manager=181|Other Admin & Secretarial=38|PA=14|Post Room Operative=1762|Project Support=1924|Receptionist=166|Sales Administrator=1923|Secretary=32|Team Secretary=1563|Typist=869},
    q{Apprenticeships=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-1964|Agriculture & Horticulture=2070|Animal Care=2071|Arts & Media=2072|Business & Administration=2073|Construction & Property=2074|Education & Training=2075|Engineering=2076|Health & Public Services=2077|IT & Telecoms=2078|Legal=2079|Leisure & Tourism=2080|Manufacturing=2081|Other Apprenticeships=2085|Publishing=2082|Retail & Commercial Enterprise=2083|Social Care=2084},
    q{Banking=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-5|Agribusiness=1740|Analyst=497|Asset Finance Sales=1741|Back Office=291|Business Banking Sales=1764|Cashier=1966|Client Services=1765|Compliance=345|Corp Actions/Divs=292|Corporate Business Sales=1742|Credit/Risk Management=301|Derivatives=293|Director/Senior Management=1743|Equity=294|Fixed Income=295|Front Office/Trading=499|Fund Administration=296|Fund Management=297|FX/MM=298|High Net Worth=1744|Loans Administration=1766|Offshore Banking=1750|Other Banking=42|Private Banking=498|Project Management=300|Reconciliations=1767|Retail Banking=161|Securities Lending/Collateral=1768|Settlements=302|Static Data=1769|Trade Finance=1770|Trade Support=303|Trainee=304|Treasury/Payments=305},
    q{Charity & Voluntary=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-500|Administration/Secretarial=1771|Advice & Counselling=1712|Advocacy=1967|Campaigning=1713|Charity Shop=1968|Community Support=1772|Corporate Partnerships=1969|Events=1773|Finance=1714|Fundraising=1715|International Development=1716|Marketing & PR=1717|Office Management=1774|Other Charity & Voluntary=1721|Policy & Research=1718|Project Management=1719|Senior Management=1720},
    q{Construction & Property=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-146|Air Conditioning=1970|Architecture=803|Asbestos Surveyor=1975|Bid Manager=1775|Building Services Maintenance=1687|Building Surveying=1777|Business Development=1776|Buyer=1778|CAD Technician=1686|Chartered Surveyor=1974|Commercial Manager=802|Contracts Manager=1779|D&B Co-ordinator=1780|Demolition=1972|Development Manager=1781|Estimator=1685|Facilities Management=1689|Foreman=876|General Practice Surveying=329|Health & Safety Manager/SHEQ Advisor=1690|Highways & Transportation=1782|Interior Design & Space Planning=1691|Land Management/Acquisition=1783|Landscape Architect=1973|Logistics Coordinator=1927|Materials Controller=1784|Monitoring & Evaluation Co-ordinator=1692|Operations Manager=1926|Other Construction & Property=331|Planner=325|Planning Manager=1785|Project Manager=805|Quantity Surveyor/PQS=328|Rail=1925|Reinstatement Manager=1787|Site Engineer=1693|Site Management=1688|Technical Co-ordinator=1788|Town Planning=1695|Trades & Labour=883|Waste & Recycling=2033|Water & Environmental Consultancy=1789},
    q{Education=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-68|Adult Education=993|Advanced Skills Teacher=1976|Assessor/Verifier=463|Basic Skills=1792|Bursar/Business Manager=1793|Education Administration=1794|Education Senior Management=1935|Educational Psychology=1977|E-learning=1978|ESOL / TEFL=1795|Further Education=153|Head Teacher/Deputy Head Teacher=1796|Higher Education=309|Invigilator=1934|Key Skills=1797|Key Stage 1=148|Key Stage 2=149|Key Stage 3 & 4=150|Key Stage 5=1798|Learning Support Assistant=311|Lecturer=1799|Libraries=984|Nursery=885|Other Education=160|Outdoor Education=1979|Preparatory Education=1980|Primary School=157|Secondary School=152|Special Needs=310|Supply Teacher=886|Teaching Assistant=1981},
    q{Energy=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-1961|Alternative Energy=2048|Climate Change=2055|Drilling=2049|Energy Advisor=2053|Exploration=2050|Management=2051|Meter Reader=2052|Nuclear=1573|Oil/Gas=1574|Other Energy=2086|Power Supply/Generation=1816|Resource Trading=2054|Utilities=1696},
    q{Engineering=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-12|Aeronautical / Aerospace=198|Applications=1800|ARM=1801|Automotive=318|Aviation=1802|Avionics=1982|Chemical/Process=199|Civil=200|CNC Programming=1803|Composite=1804|Design=1983|Draughts=1805|Electrical=201|Electronic=316|Environmental=1806|Field=202|Fluid Systems=1984|Geotechnical=348|Health & Safety=1807|Human Factors=1808|Industrial=1809|Instrumentation=1810|Lift/Escalator Engineer=1811|Maintenance=203|Manufacturing=204|Marine=205|Materials=206|Mathematical Modelling=1812|Mechanical=207|Metallurgy=1813|Micro Electronics=1985|Military/Defence=1814|Nanotechnology=1986|Operations Management=1815|Other Engineering=495|Plastics=1987|Project/Programme Manager=209|Quality Control=1817|Research & Development=1818|Semiconductors=1819|Shipbuilding=1988|Structural=210|Systems=211|Technical Author=1820|Technical Management=1821|Technical Sales=1822|Technical Support=1823|Technical Training=1824|Thermal Engineer=1989|Welding/Plating/Pipefitting=1825},
    q{Estate Agency=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-1962|Auctioneer=2059|Branch Manager=2056|Estate Agent=801|Lettings Negotiator=2058|Other Estate Agency=2087|Property Manager=2057|Property Sales=1786|Valuer/Lister=2060},
    q{Financial Services=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-117|Actuarial=220|Bancassurance=1677|Business Development Manager=1826|Collections=1992|Compliance=224|Consultancy=1678|Employee Benefits=1679|General Management=1681|Group Risk Advisor=1827|Hedge Funds=1991|Independent Financial Advisor=937|Independent Financial Advisor - Trainee=1834|Insolvency Advisor=1828|Investments=226|Life=1682|Loans - Other=1830|Loans Advisor=1829|Loans Underwriter=1831|Mergers & Acquisitions=1990|Mortgage Administrator=1832|Mortgage Advisor=934|Mortgage Underwriter=935|Mortgages - Other=936|Other Financial Services=314|Paraplanning=887|Pensions=230|Retail Financial Advisor=938|Sales & Business Development=1683|Sales Support=233|Stockbroker=1833},
    q{FMCG=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-1722|Buying=1723|Engineering=1724|Manufacturing=1726|Marketing=1727|National Accounts=1729|Operations=1730|Other FMCG=1735|Production=1731|Sales=1733|Supply Chain=1734},
    q{General Insurance=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-16|Account Executive=888|Account Handler=889|Actuarial=347|Broker=221|Broker Technician=1835|Claims=222|Commercial=223|Compliance & Regulatory=1576|Consultancy=1577|Credit Risk=1578|General Management=1580|Healthcare=1582|Lloyds=1585|Loss Adjusting=228|Other General Insurance=315|Personal Lines=231|Re-Insurance=1586|Risk Management=232|Sales & Business Development=1587|Sales Support=1588|Underwriting=227|Wordings Technician=1836},
    q{Graduate Training & Internships=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-169|Graduate Training Schemes=1736|Internships & Work Experience=1737|Research=1837},
    q{Health & Medicine=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-36|Alternative Medicine=1993|Associate Specialist=1608|Audiology=1609|Biomedical Science=1610|Carer=263|Chemotherapy=1994|Chiropody & Podiatry=481|Clinical Assistant/Fellow=1611|Clinical Psychology=482|Community Practice Nurse=1838|Community Psychiatric Nurse=1839|Consultant=1612|Dentistry=484|Dietetics=485|Emergency Medical Technician=1995|General Healthcare Assistant=1614|General Practice=1613|Haematology=1996|Health Education/Promotion=486|Healthcare Trainer=1997|Home Management=1998|Hospital Porter=820|House Officer & Senior House Officer=1615|Hygienist=1999|Mental Healthcare Assistant=1840|Midwifery=488|Mortuary Technician=1616|Nuclear Medicine Technician=1841|Nurse - Grade D=1617|Nurse - Grade E=1618|Nurse - Grade F=1619|Nurse - Grade G=1620|Nurse - Grade H=1621|Obstetrician=2000|Occupational Therapy=265|ODAs/ODPs/Theatre Nurses=1622|Oncology Nurse=2006|Optometry=266|Orthopaedic Technician/Plaster Technician=1623|Other Health & Medicine=37|Paediatric Nurse=2005|Personal Assistant/Personal Aid=1842|Pharmaceutical=1843|Pharmacology=2001|Pharmacy Technician=267|Phlebotomy=491|Physiotherapy=268|Practice Nurse=1844|Public Health=2002|Radiography & Sonography=493|Registered Mental Health Nurse=1845|Registered Nurse Band 5=1846|Registered Nurse Band 6=1847|Registered Nurse Band 7=1848|Resident Medical Officer=1625|School Nurse=1849|Sexual health=2003|Specialist Registrar=1624|Speech & Language Therapy=269|Staff Grade=1626|Substance Misuse Nurse=1850|Toxicology=2004|Trust Grade=1627},
    q{Hospitality & Catering=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-6|Assistant Manager=890|Bar Management=182|Barista=897|Caretaker=821|Chef de Partie=893|Chef de Rang=2007|Chef Manager=896|Commis-Chef=894|Concierge=2008|Conference & Banqueting=1564|Contract Catering Management=1565|Duty Manager=1936|Events Management=461|Head/Executive Chef=891|Hotel Management=185|Housekeeper=186|Kitchen Staff=187|Night Management=2009|Other Hospitality & Catering=167|Pastry Chef=895|Porter=2010|Receptionist=317|Restaurant Deputy Manager=1851|Restaurant Manager=188|Sous-Chef=892|Waiting & Bar Staff=189},
    q{Human Resources=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-24|Change Management=1600|Compensation & Benefits=1601|Compliance & Quality=2012|Employee Relations=1940|Head of HR=1602|Health & Safety=1938|HR Administrator=462|HR Advisor=212|HR Analyst=1937|HR Assistant=1603|HR Business Partner=1604|HR Consultant=213|HR Director=214|HR Manager=215|HR Officer=1910|Learning & Development=1939|Organisational Development=2011|Other Human Resources=219|Recruitment & Resourcing=453},
    q{IT & Telecoms=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-52|1st Line Support/Helpdesk=307|2nd Line Support=1628|3rd Line Support=1629|Analyst Prog/Dev=56|Business Analyst=121|Cabling Engineer=1630|Consultant=163|CRM=1631|Database Developer=306|Datacoms Engineer=136|DBA=53|Desktop Support=55|ERP=1632|Field Service Engineer=1633|Functional Consultant=1634|GIS=1852|Hardware Engineer=1853|Installation/Rollout Engineer=1635|IT Sales=332|IT Sales – Post Sales Support=1855|IT Sales – Pre Sales Support=1854|IT Trainer=54|IT/Systems Manager=64|Middleware & EAI=900|Mobile Engineer=1636|Network Administration=1637|Network Analyst=899|Network Manager=1638|Network Security=165|Operations Analyst=1858|Other IT & Telecoms=65|Programme Manager=1639|Project Manager=119|Project Office Support=1640|Quality Assurance=122|Service Delivery=1856|Service Desk=1857|Software Developer=58|Software Engineer=138|Software Testing=60|Systems Admin=1642|Systems Testing=1643|Systems/Network Admin=59|Systems/Support Analyst=1644|Technical Architect=1859|Technical Author=123|Telecoms Consultant=1645|Telecoms Engineer=137|Test Manager=1646|Tester/Test Analyst=1860|UAT=1647|Web Design=901|Web Developer=902},
    q{Legal=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-101|Barrister=1861|Barrister Clerk=1862|Billing Co-ordinator=1863|Company Secretary=386|Conveyancing=903|General Legal Support=1864|Graduate training and internship schemes=387|In House=343|Legal Cashier=388|Legal Executive=235|Legal Secretary=335|Other Legal=240|Paralegal=236|Partner=389|Partnership Secretary=390|Patent/Trademark=1865|Practice Manager=1866|Private Practice=342|Probate=1941|Solicitor/Lawyer=238|Team Manager=1867},
    q{Leisure & Tourism=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-92|Accommodation Manager=2013|Airline=904|Beauty & Spa=905|Business Travel=906|Childrens Activity=2014|Cruise=2015|Health & Fitness=907|Museums & Galleries=985|Other Leisure & Tourism=908|Personal Training=2016|Reservations=909|Sport Development=2017|Sport Management=2018|Sports Coaching=910|Travel Agent & Consultant=911},
    q{Manufacturing=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-168|Automation=2019|Computer Numerical Control (CNC)=2020|Factory/Floor Manager=1868|Forecast Analyst=2021|Maintenance=1869|Operations Manager=1870|Other Manager=466|Other Manufacturing=996|Production Manager=1871|Production Planning Manager=1872|Purchasing=467|Quality Control=468|Semi-skilled Operator=469|Skilled Operator=470|Supervisor=471|Textiles=912|Waste Management=1873|Workshop Manager=1874},
    q{Marketing & PR=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-18|Account Manager=1875|Advertising/Sponsorship=1589|Brand Management=1590|Campaign Manager=1944|Digital Marketing=1942|Direct Marketing Executive=1591|Direct Marketing Manager=1592|Events Assistant=1945|Events Management=382|Internal Communications=1594|Market Research - Analyst=1876|Market Research - Executive=244|Market Research - Manager=1877|Marketing Analytics=1943|Marketing Assistant=245|Marketing Director=246|Marketing Executive=994|Marketing Manager=247|Online Marketing Executive=1595|Online Marketing Manager=1596|Other Marketing & PR=173|PR Executive=1597|PR Manager=1598|Product Management=1599},
    q{Media, Digital & Creative=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-71|Account Director=1649|Account Executive=1650|Account Manager=1651|Animation=1878|Arts & Entertainment=940|Artworker=1652|CAD Design=1653|Copywriter=1654|Creative Director=1655|Desk Top Publishing=1656|Editorial=1657|Events Design=1879|Fashion Design=942|Games=1658|Graphic Design=944|iGaming=2022|Interior Design=945|Journalism=1659|Media Sales=1660|Music Industry=1880|New Media=946|Other Media & Creative=947|Photography=1881|Planner/Buyer=1661|Print Room=1662|Product Design=1882|Project Manager=1663|Publishing=949|Radio=950|Researcher=1664|Search - SEO & PPC=1946|Television=951|Traffic Manager=1665|Web Design=1666},
    q{Motoring & Automotive=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-1700|Aftersales=1701|Body Repair &  Fit=1702|Driver=1947|Finance &  Leasing=1704|Fleet Operations=1705|Management=2023|Motorsport=1763|Other Motoring &  Automotive=1711|Parts Advisor=1706|Quality &  Testing=1707|Research, Design &  Development=1703|Service Receptionist=1708|Valeting=2024|Vehicle Movement=1948|Vehicle Sales=1709|Vehicle Technician=1710},
    q{Purchasing=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-27|Assistant Buyer=1606|Buyer=913|Contract Manager=914|Head of Procurement=1607|Other Purchasing=915|Procurement=989|Procurement Manager=916|Purchasing Manager=917|Purchasing Support=918|Senior Buyer=919|Stock Control=920},
    q{Recruitment Consultancy=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-338|Account Manager=427|BD Executive/Manager=428|Branch/Recruitment Manager=429|Director=430|Divisional/Regional Manager=431|Internal Recruiter=1949|On-site Consultant=433|Other Recruitment Consultancy=439|Recruitment Consultant=434|Researcher/Resourcer=435|Search & Selection Consultant=437|Senior Recruitment Consultant=436|Team Leader Other=438|Team Leader Permanents=1884|Team Leader Temporaries=1885|Trainee Consultant=432},
    q{Retail=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-90|Area Manager=250|Assistant Manager=352|Buying=254|Fashion=1886|Floor Manager=252|In-Store Promotions=1887|Merchandiser=253|Other Retail=257|Sales Assistant=251|Store Manager=255|Store Staff=256|Team Leader=473},
    q{Sales=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-30|Account Manager=258|Business Development=922|Direct Sales=1950|Field Sales=921|Healthcare=1889|Home Working=346|IT Sales=333|Media Sales=337|Medical Sales=381|Other Sales=31|Pharmaceutical=1888|Sales Director=259|Sales Executive=260|Sales Manager=261|Sales Manager - National=1952|Sales Manager - Regional=1951|Telesales=933|Telesales - Inbound Only=932},
    q{Scientific=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-89|Analyst=1892|Analytical Chemistry=970|Bioanalysis=971|Bioinformatics=363|Biotechnology=270|Chemical Engineering=973|Chemistry=272|Clinical Data Management=972|Clinical Research/Trials=1890|Earth Science=2034|Ecology=2035|Environmental Science=356|Food Science=977|Forensic Science=357|Formulation=975|Geoscience=1891|Inorganic Chemistry=976|Laboratory Technician=273|Life Science=979|Materials Science=978|Medical Devices=965|Medical Information/Writing=368|Microbiology=369|Molecular Biology=2036|Organic Chemistry=366|Other Scientific=276|Patent Specialists=1953|Personal Care=1954|Physics=274|Polymer Chemistry=966|Process Chemistry=374|Quality Assurance=375|Quality Control=360|Regulatory Affairs=376|Researcher=275|Sales=361|SAS Programming=967|Statistics=968|Validation=969},
    q{Security & Safety=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-1963|CCTV Operator=2061|Counter Terrorist Cleared=1698|Environmental=981|Event Safety Steward=2062|Fire Safety=2063|Health & Safety Officer=2064|Health and Safety=465|Law Enforcement=983|Other Security & Safety=2088|Parking Attendant=2065|Probation/Prison Service=827|Security=831|Security Consultant=2067|Security Contracts Manager=2066|Security Guard=2068|Traffic Warden=2069},
    q{Social Care =BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-34|Advice Worker=474|Approved Social Worker=1893|Benefits Assessor=2030|Care Assistant=312|Care Manager=277|Case Worker=475|Child Care/Nanny=320|Elderly Care=2037|Family Support=2038|Home Carer=1955|Homelessness=2039|Housing=982|Learning Mentors=1894|Other Social Care=39|Project Worker=279|Qualified Social Worker=280|Registered Manager=1895|Residential Child Care=1896|Residential Support=281|Social Care Assessment=282|Support Worker=283|Youth Worker=477},
    q{Strategy & Consultancy=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-1755|Bids & Tenders=2040|Business Advisor=2041|Business Analyst=1897|Change Management=1898|Corporate Business Sales=2042|Economist=1899|Financial Analyst=1900|Management Consultant=1901|Mergers & Acquisitions=2043|Other Strategy & Consultancy=1907|Outsourcing/Offshoring=2044|Policy=987|Programme Management=1903|Project Management=1902|Statistician=1904|Strategy Consultant=1905|Technical Project Manager=1906},
    q{Training=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-1909|Design Author=1956|Director=1957|Disability Inclusion=2045|Driving Instructor=2046|Finance Trainer=1912|Freelance Trainer=1913|Learning & Development=1605|Other Training=218|Product Trainer=1914|Sales Trainer=1915|Training Assistant=1916|Training Manager=496},
    q{Transport & Logistics=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-11|Airline=286|Courier=1908|Depot Manager=2047|Driving=287|Export Clerk=1566|Import Clerk=1567|Import Co-Ordinator=1568|Loader/Shifter=927|Logistics=288|Other Transport & Logistics=290|Picker/Packer=928|Postal Worker=826|Rail=289|Shipping=107|Stock Control=1569|Supply Chain=925|Team Leader=924|Transport Planner=923|Warehouse Manager=1570|Warehouse Operative=926|Warehouse Supervisor=931},
    q{Other=BBTECH_RESERVED_OPTGROUP|* All Sectors=YOLO-21|Agriculture=809|Antiques=2027|Archaeology=2028|Archivist/Curator=810|Cleaner=811|Clergy=812|Conservation/Environment=813|Fishing=2026|Forestry=2025|Gardener=817|Heritage=2029|Home Working=819|Pest Control=824|Quality Assurance=830|Translator/Interpreter=834|Undertaker=835|Uniformed Services=836|Veterinary Services/Animal Care=837}
);

    my $sector_list = join('|', @sectors);

$standard_tokens{multi_sectors} = {
    Label       => 'Job Sectors',
    Type        => 'MultiList',
    Options     => $sector_list,
    SortMetric  => 5,
};

$standard_tokens{hours} = {
    Type        => 'List',
    Label       => 'Working hours',
    Values      => 'All=|Full-time=fullTime|Part-time=partTime',
    SortMetric  => 4,
};

$standard_tokens{minimum_qualification} = {
    Type        => 'List',
    Label       => 'Minimum qualification',
    Values      => '|University degree=1|Masters degree=2|PhD=4|A Level=8|GCSE=16|Other=32',
    SortMetric  => 6,
};

$standard_tokens{degree_subject_keywords} = {
    Type        => 'Text',
    Label       => 'Degree subject keywords',
    SortMetric  => 7,
};

$standard_tokens{institutions} = {
    Type        => 'Text',
    Label       => 'Institutions (e.g. university of london,imperial college)',
    SortMetric  => 8,
};

$standard_tokens{finished_on_start} = {
    Type        => 'Text',
    Label       => 'Minimum graduating year (e.g 2009)',
    SortMetric  => 9,
};

$standard_tokens{finished_on_end} = {
    Type        => 'Text',
    Label       => 'Maximum  graduating year (e.g 2010)',
    SortMetric  => 10,
};

$standard_tokens{degree_grade} = {
    Type        => 'List',
    Label       => 'Degree grade',
    Values      => '|University Degree=BBTECH_RESERVED_OPTGROUP|First class=1|2:1=2|2:2=3|Third class=4|Pass=5|Masters Degree=BBTECH_RESERVED_OPTGROUP|Distinction=6|Merit=7|Pass=5|PHD=BBTECH_RESERVED_OPTGROUP|Distinction=6|Merit=7|Pass=5|GCSE=BBTECH_RESERVED_OPTGROUP|A*=9|A=10|B=11|C=12|D=13|E=14|F=16|G=17|Other=BBTECH_RESERVED_OPTGROUP|Other=18',
    SortMetric  => 11,
};

$standard_tokens{languages} = {
    Type        => 'Multilist',
    Label       => 'Languages',
    Values      => 'Arabic=2|Cantonese=7|French=17|German=18|Greek=19|Italian=30|Japanese=31|Mandarin=37|Polish=45|Portuguese=46|Spanish=55|Welsh=75|Afrikaans=78|Albanian=85|Amharic=1|Arabic=2|Armenian=3|Azerbaijani=86|Basque=87|Belarusian=88|Bengali=4|Bosnian=100|Bulgarian=79|Burmese=5|Cambodian=6|Cantonese=7|Catalan=89|Cebuano=8|Cham=9|Chamorro=10|Chechen=90|Croatian=99|Czech=11|Danish=12|Dutch=13|Estonian=91|Farsi=14|Fijian=15|Finnish=16|Flemish=74|French=17|Galician=97|Georgian=92|German=18|Greek=19|Gujarati=20|Hakka=21|Hebrew=22|Hindi=23|Hmong=24|Hungarian=25|Ibo=26|Icelandic=93|Ilocano=27|Ilongo=28|Indonesian=29|Irish Gaelic=77|Italian=30|Japanese=31|Kazakh=82|Korean=32|Kurdish=94|Kyrgyz=83|Laotian=33|Latvian=34|Lithuanian=95|Macedonian=35|Malaysian=36|Mandarin=37|Marathi=38|Marshallese=39|Mien=40|Norwegian=41|Oromo=42|Pashto=43|Persian=44|Polish=45|Portuguese=46|Punjabi=47|Quechua=48|Romanian=49|Russian=50|Samoan=51|Serbian=101|Sign Language=98|Sinhala=76|Slovak=53|Slovenian=96|Somali=54|Spanish=55|Sudanese=56|Swahili=57|Swedish=58|Tagalog=59|Taiwanese=60|Tajik=84|Tamil=61|Thai=62|Tibetan=63|Tigrigna=64|Toishanese=65|Tongan=66|Trukese=67|Turkish=68|Turkmen=81|Ukranian=69|Urdu=70|Uzbek=80|Vietnamese=71|Visayan=72|Welsh=75|Yoruba=73',
    SortMetric  => 12,
};

$standard_tokens{languages_fluency} = {
    Type        => 'List',
    Label       => 'Language fluency',
    Values      => 'Fluent=1|Intermediate=2',
    Default     => '2',
    SortMetric  => 13,
};

$standard_tokens{include_ineligible} = {
    Type        => 'List',
    Label       => 'Include ineligible candidates',
    Values      => 'No=false|Yes=true',
    SortMetric  => 14,
};

$standard_tokens{has_driving_licence} = {
    Type        => 'List',
    Label       => 'Candidates with driving licence',
    Values      => 'No=false|Yes=true',
    SortMetric  => 15,
};

$standard_tokens{is_car_owner} = {
    Type        => 'List',
    Label       => 'Car owners',
    Values      => 'No=false|Yes=true',
    SortMetric  => 16,
};

$standard_tokens{sort} = {
    Type        => 'List',
    Options     => 'Relevancy=relevancyDesc|Registration date=registrationDesc|Last login date=activityDesc|Salary high to low=salaryDesc|Salary low to high=salaryAsc',
    Default     => 'relevancyDesc',
    SortMetric  => $SM_SORT,
};

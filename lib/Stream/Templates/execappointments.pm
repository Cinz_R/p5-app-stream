package Stream::Templates::execappointments;
use base qw(Stream::Templates::CrawledMadgex);

use strict;
use warnings;

__PACKAGE__->load_tokens_from_config();

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

use Stream::Constants::SortMetrics qw(:all);
use Log::Any qw/$log/;

$authtokens{execappointments_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{execappointments_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

$derived_tokens{salary_cur}->{Options} = 'GBP';
$derived_tokens{salary_per}->{Options} = 'annum';


sub fixup_tokens {
    my $self = shift;
    my %tokens = @_;

    # Remove "Preferred Job Type" token
    # We're mapping it to default
    delete $tokens{1427323}
      or die('Could not find "Preferred Job Type" token');

    return %tokens;
}


sub mapped_jobtype {
    my $self = shift;
    my $jobtype = $self->get_token_value('default_jobtype');
    my $output;

    if(!$jobtype) {
        $output = '';
    } elsif($jobtype eq 'permanent') {
        $output = '1427323|1525128';
    } else {
        $output = '1427323|1525129';
    }

    $log->infof('Mapped jobtype %s to %s', $jobtype, $output);
    return $output;
}

sub fixup_search_fields {
    my $self = shift;
    my %fields = @_;

    my $jobtype = $self->mapped_jobtype;
    push @{$fields{q}}, $jobtype if $jobtype;

    return %fields;
}

1;

package Stream::Templates::oilandgasjobsearch;
use base qw(Stream::Templates::OilAndGasPlatform);

use strict;
use warnings;

__PACKAGE__->inherit_tokens();

sub base_url {
    return 'https://www.oilandgasjobsearch.com';
}

1;

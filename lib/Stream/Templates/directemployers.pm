package Stream::Templates::directemployers;
use base qw(Stream::Engine::Robot);

use strict;
use warnings;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
use Stream::Constants::SortMetrics qw(:all);

use List::Util;
use DateTime;
use HTML::Scrubber;
use HTML::TreeBuilder::XPath;

$authtokens{directemployers_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{directemployers_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

# takes a $days argument
# return date in the mm/dd/yy format offset by $days days.
sub _map_availability {
    my $self = shift;
    my $days = shift;

    return '' if $days eq 'any';

    return DateTime->now()
      ->add(days => $days)
      ->strftime('%m/%d/%Y');
}

sub base_url {
    return 'https://us.jobs';
}

sub login_url {
    my $self = shift;
    return $self->base_url . '/secure/emplogin.asp';
}

sub login_form_with_fields {
    return qw(username password UserID);
}

sub login_fields {
    my $self = shift;
    return (
        username => $self->token_value('username'),
        password => $self->token_value('password'),
    );
}

sub login_success {
    return qr/DirectEmployers Association Member Desktop/;
}

sub logout_url {
    my $self = shift;
    return $self->base_url . '/emplogout.asp';
}

sub search_url {
    my $self = shift;
    return $self->base_url . '/empsearchresults.asp';
}

sub search_navigate_to_page {
    my $self = shift;
    $self->get($self->base_url . '/empsearchresumes.asp');
}
sub _get_radius_id {
    my $self = shift;
    my $radius_miles = int shift;

    # miles => id
    my %boundaries = (
        0   => '0',
        25  => '1',
        50  => '2',
        100 => '3',
        300 => '4',
    );

    # Return largest that's still under the given
    my $boundary = List::Util::max( grep {$_ <= $radius_miles} keys %boundaries );
    return $boundaries{$boundary} || '0';
}

sub search_request_method {
    return 'GET';
}

sub search_fields {
    my $self = shift;

    my $state_id   = '';
    my $radius_id  = '';
    my $city       = '';
    my $country_id = '';

    if( my $input_location_id = $self->token_value('location_id') ) {
        my $location = $self->location_api->find($input_location_id);
        my $location_id = $location->get_mapping('directemployers');

        if(!$location_id) {
            $self->results->add_notice('Location ' . $location->name . ' not supported');
        } else {

            # For locations like Guam that the board treats as a US state, but our system doesn't
            my $in_usa = $location_id =~ s/~~state//;
            # US states are handled differently
            if($in_usa || $location->in_usa) {
                $self->log_debug('Mapping US location');
                $state_id = $location_id;
                $radius_id = $self->_get_radius_id($self->token_value('location_within_miles'));
                $city = $location->name;
                $self->log_debug('state_id: [%s], city: [%s], radius_id: [%s]', $state_id, $city, $radius_id);
            } else {
                $country_id = $location_id;
                $self->log_debug('countr_id: [%s]', $country_id);
            }
        }
    }

    my $last_updated = $self->CV_UPDATED_WITHIN_TO_DAYS( $self->token_value('cv_updated_within') );

    my $availability = $self->_map_availability( $self->token_value_or_default('availability'));

    $self->log_debug('Mapped date [%s]', $availability);

    my $availability_days = $self->token_value_or_default('availability');

    my %fields = (
        k             => $self->token_value('keywords') || '',
        ci            => $city,
        st            => $state_id,
        radius        => $radius_id,
        cid           => $country_id,
        ed            => $self->token_value_or_default('education'),
        wa            => $self->token_value_or_default('work_auth'),
        vet           => $self->token_value_or_default('veterans_only'),
        da            => $availability,
        re            => $self->token_value_or_default('willing_to_relocate'),
        rn            => $self->token_value_or_default('candidate_name'),
        tm            => $last_updated,
        up            => 'yes',
        intl          => 'yes',
        Search        => 'SEARCH RESUMES',
        license       => $self->token_value_or_default('licence'),
        certification => $self->token_value_or_default('certification'),
        page          => $self->current_page(),
    );

    $fields{ba} = 'be' if $availability; #Set availability type to before date.

    if(my $work_hours = $self->token_value_or_default('work_hours')) {
        $fields{$work_hours} = 'yes';
    }

    if($self->token_value('default_jobtype') eq 'contract') {
        $fields{ct} = 'yes';
    }

    return %fields;
}

sub search_number_of_pages_xpath {
    return '//td[contains(text()[1], "Jump to page")]/span[last()]/text()';
}

sub search_number_of_results_xpath {
    return '//td[@class="t12 ttnj" and contains(text(), "Results")]/span[last()]/text()';
}

sub scrape_candidate_xpath {
   return '//table[@cellspacing = "0"]';
}

sub scrape_candidate_details {
    return (
        headline       => './/a/text()',
        profile_url    => './/a/@href',
        location       => './/tr[1]/td[@class="t11 l"]/text()[1]',
        work_types     => './/tr[1]/td[@class="t11 l"]/text()[2]',
        availability   => './/tr[1]/td[@class="t11 l"]/text()[3]',
        joined_details => './/tr[2]/td[@class="t11 l"]/text()',
    );
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;
    $candidate->has_cv(0);
    $candidate->has_profile(1);

    $candidate->attr('profile_url') =~ m/resumeid=(\d+)/;
    $candidate->attr('candidate_id' => $1);

    my $joined_details = $candidate->attr('joined_details');
    my ($education, $work_auth) = split('\s*\/\s*', $joined_details);
    $candidate->attr('education' => $education);
    $candidate->attr('work_auth' => $work_auth);
    $self->log_debug('parsed education: [%s], work_auth: [%s]', $education, $work_auth);

    my %work_types_map = (
        FT => 'Full Time',
        PT => 'Part Time',
        Contract => 'Contract',
    );

    my $work_types = $candidate->attr('work_types');
    $work_types =~ s/\s//g;
    my @work_types = split('\/', $work_types);
    @work_types = map { $work_types_map{$_} } @work_types;
    $candidate->attr('work_types' => \@work_types);
    $self->log_debug('split work_types into [' . scalar(@work_types) . '] items');
}


sub profile_url {
    my $self = shift;
    my $candidate = shift;
    return $self->base_url . '/' . $candidate->attr('profile_url');
}


sub download_profile_success {
    return qr/Printable Resume/;
}

sub scrape_download_profile_xpath {
    return '//div[@id="contentm"]';
}

sub scrape_download_profile_details {
    return (
        quick_facts        => ['.//div[@id="qfbox"]/div[@class="l"]/text()'],
    );
}

sub scrape_fixup_candidate_profile {
    my $self = shift;
    my $candidate = shift;

    # Have to scrape the CV ourselves because
    # our generic scraper steals our <br /> elements
    $self->log_info('Scraping CV');
    my $xpc = HTML::TreeBuilder::XPath->new();
    $xpc->parse( $self->response->decoded_content );
    my $raw_cv = $xpc->findnodes_as_string('//div[@class="fl0 l t12"]');
    $self->log_debug( $raw_cv ? 'CV found' : 'CV not found');

    my $scrubber = HTML::Scrubber->new(
        allow => [qw[div p ul ol li br b u i strong em]]
    );
    my $clean_cv = $scrubber->scrub($raw_cv);
    $candidate->attr('html_cv' => $clean_cv);

    my $fact_count = scalar( @{$candidate->attr('quick_facts')} );
    $self->log_debug("[$fact_count] quick facts found");

}

$standard_tokens{education} = {
    Label  => 'Minimum education level',
    Type   => 'List',
    Values => [
        [''                        => '0'],
        ['Less than High School'   => '1'],
        ['High School Diploma/GED' => '2'],
        ['Some College'            => '3'],
        ['Vocational Degree'       => '4'],
        ['Associate\'s Degree'     => '5'],
        ['Bachelor\'s Degree'      => '6'],
        ['Master\'s Degree'        => '7'],
        ['Doctoral Degree'         => '8'],
        ['GED'                     => '9'],
        ['High School Diploma'     => '10'],
    ],
    Default => '0',
};

$standard_tokens{certification} = {
    Label   => 'Certifications',
    Type    => 'Text',
    Default => '',
};

$standard_tokens{licence} = {
    Label   => 'Licence',
    Type    => 'Text',
    Default => '',
};

$standard_tokens{work_auth} = {
    Label   => 'Authorized to work in US',
    Type    => 'List',
    Values  => [
        [Yes => 'yes'],
        [No  => 'no'],
    ],
    Default => 'yes',
};

$standard_tokens{veterans_only} = {
    Label  => 'Veterans only',
    Type   => 'List',
    Values => [
        [Yes => 'yes'],
        [No  => ''],
    ],
    Default => '',
};

$standard_tokens{willing_to_relocate} = {
    Label  => 'Willing to relocate',
    Type   => 'List',
    Values => [
        [Yes => '1'],
        [No  => ''],
    ],
    Default => '',
};

$standard_tokens{work_hours} = {
    Label  => 'Work Hours',
    Type   => 'List',
    Values => [
        ['All'       => ''],
        ['Full Time' => 'ft'],
        ['Part Time' => 'pt'],
    ],
    Default => '',
};

$standard_tokens{candidate_name} = {
    Label => 'Candidate Name',
    Type  => 'Text',
    Default => '',
};

$standard_tokens{availability} = {
    Label   => 'Availability',
    Type    => 'List',
    Values  => [
        ['Immediate' => 0],
        ['1 week'    => 7],
        ['2 weeks'   => 14],
        ['1 month'   => 30],
        ['2 months'  => 60],
        ['3 months'  => 90],
        ['6 months'  => 180],
        ['Any'       => 'any'],
    ],
    Default => 'any',
};

1;

package Stream::Templates::twitter;

use base Stream::Engine;

use Log::Any qw/$log/;
use Net::Twitter;
use Date::Parse;
use DateTime;
use strict;
use warnings;
use Data::Dump;
use JSON::WebToken;
# Returns a HashRef of Twitter authentication values fetched from Juice.

=head

Relies very heavily on Net::Twitter.
You'll notice errors are stringified becuase Net::Twitter throws
Net::Twitter::Error objects which which Search doesn't like.

=cut

use vars qw(%standard_tokens);

$standard_tokens{search_type} = {
    Label => 'Search Type',
    Type => 'List',
    # These values are bitflags
    Options => [
        ['All'        => __PACKAGE__->TWEETS | __PACKAGE__->PROFILES],
        ['By tweets'  => __PACKAGE__->TWEETS],
        ['By profile' => __PACKAGE__->PROFILES],
    ],
    Default => 3,
    SortMetric => 1,
};

$standard_tokens{result_type} = {
    Label => 'Result Type',
    Type => 'List',
    Options => [
        ['Mixed'   => 'mixed'],
        ['Recent'  => 'recent'],
        ['Popular' => 'popular'],
    ],
    Default => 'mixed',
    SortMetric => 2,
};

$standard_tokens{filters} = {
    Label => 'Filters',
    Type => 'MultiList',
    Options => [
        ['Safe'         => 'safe'],
        ['Media'        => 'media'],
        ['Native video' => 'native_video'],
        ['Periscope'    => 'periscope'],
        ['Vine'         => 'vine'],
        ['Images'       => 'images'],
        ['Twimg'        => 'twimg'],
        ['Links'        => 'links'],
    ],
    Default => 'safe',
    SortMetric => 3,
};

$standard_tokens{attitude} = {
    Label => 'Attitude',
    Type => 'MultiList',
    Options => [
        ['Positive' => ':)'],
        ['Negative' => ':('],
        ['Question' => '?']
    ]
};

$standard_tokens{url} = {
    Label => 'URL',
    Type => 'Text',
};

$standard_tokens{to} = {
    Label => 'To',
    Type => 'Text',
};

$standard_tokens{language} = {
    Type => 'MultiList',
    Label => 'Language',
    Default => '',
    SortMetric => 10,
    Options => [
        ['Amharic'         => 'am'],
        ['Arabic'          => 'ar'],
        ['Armenian'        => 'hy'],
        ['Bengali'         => 'bn'],
        ['Bulgarian'       => 'bg'],
        ['Burmese'         => 'my'],
        ['Central Kurdish' => 'ckb'],
        ['Chinese'         => 'zh'],
        ['Danish'          => 'da'],
        ['Divehi'          => 'dv'],
        ['Dutch'           => 'nl'],
        ['English'         => 'en'],
        ['Estonian'        => 'et'],
        ['Finnish'         => 'fi'],
        ['French'          => 'fr'],
        ['Georgian'        => 'ka'],
        ['German'          => 'de'],
        ['Greek'           => 'el'],
        ['Gujarati'        => 'gu'],
        ['Haitian Creole'  => 'ht'],
        ['Hebrew'          => 'he'],
        ['Hindi'           => 'hi'],
        ['Hungarian'       => 'hu'],
        ['Icelandic'       => 'is'],
        ['Indonesian'      => 'id'],
        ['Italian'         => 'it'],
        ['Japanese'        => 'ja'],
        ['Kannada'         => 'kn'],
        ['Khmer'           => 'km'],
        ['Korean'          => 'ko'],
        ['Lao'             => 'lo'],
        ['Latvian'         => 'lv'],
        ['Lithuanian'      => 'lt'],
        ['Malayalam'       => 'ml'],
        ['Marathi'         => 'mr'],
        ['Nepali'          => 'ne'],
        ['Norwegian'       => 'no'],
        ['Oriya'           => 'or'],
        ['Pashto'          => 'ps'],
        ['Persian'         => 'fa'],
        ['Polish'          => 'pl'],
        ['Portuguese'      => 'pt'],
        ['Punjabi'         => 'pa'],
        ['Romanian'        => 'ro'],
        ['Russian'         => 'ru'],
        ['Serbian'         => 'sr'],
        ['Sindhi'          => 'sd'],
        ['Sinhala'         => 'si'],
        ['Slovenian'       => 'sl'],
        ['Spanish'         => 'es'],
        ['Swedish'         => 'sv'],
        ['Tagalog'         => 'tl'],
        ['Tamil'           => 'ta'],
        ['Telugu'          => 'te'],
        ['Thai'            => 'th'],
        ['Tibetan'         => 'bo'],
        ['Turkish'         => 'tr'],
        ['Urdu'            => 'ur'],
        ['Uyghur'          => 'ug'],
        ['Vietnamese'      => 'vi'],
    ],
};

# Bitflag
sub TWEETS {
    return 1;
}

# Bitflag
sub PROFILES {
    return 2;
}

sub creds {
    my ($self)  = @_;
    my $subscription = $self->user_object->stream2->user_api->oauth_token(
        $self->user_object->provider_id,
        'twitter'
    );

    if(!%$subscription) {
        $self->_throw_oauth_failure('Twitter permissions not set.');
    }

    my %credentials = (
        access_token        => $subscription->{access_token},
        access_token_secret => $subscription->{access_token_secret},
        consumer_key        => $subscription->{twitter}->{consumer_key},
        consumer_secret     => $subscription->{twitter}->{consumer_secret},
    );

    foreach (keys %credentials) {
        $self->_throw_oauth_failure('Missing permission value.')
          unless defined $credentials{$_}
    }

    return \%credentials;
}

sub _throw_oauth_failure {
    my ($self, $message) = @_;

    my $criteria_id = $self->criteria()->id();
    my $callback_url = URI->new_abs(
        'external/twitter/oauth/callback',
        $self->stream_action->stream2_base_url
    );
    my $jwt = JSON::WebToken->encode(
        {
            iss  => 'stream',
            exp  => time() + 120,
            page => 1,
            criteria_id => $criteria_id,
            provider_id => $self->user_object()->provider_id()
        },
        'stream'
    );

    $callback_url->query_form( jwt => $jwt );

    my $oauth_url = URI->new('https://www.adcourier.com/oauth.cgi');

    $oauth_url->query_form(
        type    => 'twitter',
        action  => 'request_tokens',
        company => $self->company(),
        office  => $self->office(),
        redirect_me  => 1,
        callback_url => $callback_url->as_string(),
    );

    $self->throw_login_error(
        $message,
        {
            src => $oauth_url->as_string(),
            label => 'Click here to set up permissions.',
        }
    );
}

# A Net::Twitter instance to handle Twitter's OAuth layer
sub twitter_client {
    my ($self) = @_;
    return $self->{twitter_client} //= $self->_new_twitter_client;
}

# Create our Net::Twitter instance
sub _new_twitter_client {
    my ($self) = @_;

    $log->debugf(
        'Authenticating with creds:%s',
        Data::Dump::pp($self->creds)
    );

    my $client = Net::Twitter->new(
        traits   => [qw/OAuth API::RESTv1_1/],
        %{$self->creds},
    );

    $self->log_lwp_ua($client->ua);

    return $client;
}

# Convert location to twitter lat/long/radius string
sub _map_location {
    my ($self, $location_id, $radius_miles) = @_;
    $log->infof(
        'Mapping location ID "%s" and radius (miles) "%s"',
        $location_id,
        $radius_miles,
    );
    if(!$location_id) {
        $log->debug('Location ID missing');
        return '';
    }

    my $location = $self->location_api->find($location_id);
    my $geocode;

    if($radius_miles) {
        $geocode = sprintf(
            '%s,%s,%smi',
            $location->latitude,
            $location->longitude,
            $radius_miles,
        );
    } else {
        $geocode = sprintf(
            '%s,%s',
            $location->latitude,
            $location->longitude
        );
    }
    $log->infof(
        'Mapped location ID %d to geocode "%s"',
        $location_id,
        $geocode
    );
    return $geocode;
}

sub _map_cv_updated {
    my ($self, $updated_since) = @_;
    $log->debugf('Mapping update date "%s"', $updated_since);

    my $datetime = $self->cv_updated_within_to_datetime($updated_since);
    unless( $datetime ) {
        $log->info('Mapped date to undefined');
        return;
    }

    my $mapped_value = $datetime->strftime('%Y-%m-%d');

    $log->infof(
        'Mapped update date from "%s" to "%s"',
        $updated_since,
        $mapped_value
    );
    return $mapped_value;
}

# Search tweets and use them to find useres
# Allows more extensive options
sub search_users_by_tweets {
    my ($self) = @_;

    my @query_parts;

    my $keywords = $self->token_value('keywords');
    push @query_parts, $keywords if $keywords;

    my $geocode = $self->_map_location(
        $self->token_value('location_id') || '',
        $self->token_value('location_within_miles') || ''
    );
    $log->infof('Generated geocode "%s"', $geocode);

    my @filters = map {
        'filter:' . $_;
    } $self->token_value('filters');
    push @query_parts, join(' ', @filters) if @filters;

    my $lang_code = $self->token_value('language');
    push @query_parts, 'lang:' . $lang_code if $lang_code;

    my $url = $self->token_value('url');
    push @query_parts, 'url:"' . $url . '"' if $url;

    # At least one of the previous query operators is required
    # in order to run a search
    if(!@query_parts && !$geocode) {
        $log->info('Empty query and geocode, skipping searching tweets');
        $self->results->add_notice("Nothing to search tweets with");
        return ();
    }

    my $since = $self->_map_cv_updated(
        $self->token_value('cv_updated_within')
    );

    push @query_parts, $self->token_values('attitude');

    push @query_parts, 'since:' . $since if $since;

    my $query = join(' ', @query_parts);

    $log->debugf(
        "Query parts: %s\nQuery string: \"%s\"",
        join(', ', map {"'$_'"} @query_parts),
        $query
    );

    my $result_type = $self->token_value_or_default('result_type');
    my $response = eval {
        $self->twitter_client->search(
            {
                q       => $query,
                count   => 100,
                result_type => $result_type,
                ($geocode ? (geocode => $geocode) : ()),
            }
        );
    };

    $self->feed_identify_error( "$@" ) if $@;

    my @users;
    foreach my $tweet (@{$response->{statuses}}) {
        my $user = delete $tweet->{user};
        $user->{status} = $tweet;
        push @users, $user;
    }

    # Return ID key hash
    return map {$_->{id} => $_} (@users);
}

# Search for profiles which is limited to using keywords
sub search_users_direct {
    my ($self) = @_;

    my @users;

    my $keywords = $self->token_value('keywords');
    if(!$keywords) {
        $log->info('Empty keywords. Skipping searching by tweets');
        $self->results->add_notice('No keywords to search for profiles');
        return ();
    }

    foreach my $page (0..4) {
        my $response = eval {
            $self->twitter_client->users_search({
                q     => $self->token_value('keywords') || '',
                page  => $page,
                count => 20
            });
        } or last;

        push @users, @$response;
    }

    $self->feed_identify_error( "$@" ) if $@;

    delete $_->{status}->{retweeted_status} for @users;

    # Return ID key hash
    return map {$_->{id} => $_} (@users);
}

sub search_specific_page  {
    my ($self, $page) = @_;

    # Load up the client
    # Gives it a change to check for working creds
    $self->twitter_client;

    my %direct_users = $self->search_users_direct();
    my %tweet_users = $self->search_users_by_tweets();
    my %all_users = (%direct_users, %tweet_users);

    my %selected_users;
    # Check value against bitflags
    if($self->token_value_or_default('search_type') & $self->TWEETS) {
        $log->info('Adding tweet-found users to results');
        %selected_users = (%selected_users, %tweet_users);
    };
    if($self->token_value_or_default('search_type') & $self->PROFILES) {
        $log->info('Adding direct-found users to results');
        %selected_users = (%selected_users, %direct_users);
    }

    # User IDs to filter unique values
    my @users = values %selected_users;

    foreach my $user (@users) {
        $user->{url_html} = $self->parse_twitter_entities(
            $user->{url},
            $user->{entities}->{url}
        );
        $user->{description_html} = $self->parse_twitter_entities(
            $user->{description},
            $user->{entities}->{description}
        );

        my $tweet = $user->{status}->{retweeted_status} || $user->{status};
        $tweet->{html_text} = $self->parse_twitter_entities(
            $tweet->{text},
            $tweet->{entities}
        );
        # Get image sizes by modifying the standard image URL.
        $user->{profile_image_url_mini_https}     = $user->{profile_image_url_https};
        $user->{profile_image_url_normal_https}   = $user->{profile_image_url_https};
        $user->{profile_image_url_bigger_https}   = $user->{profile_image_url_https};
        $user->{profile_image_url_original_https} = $user->{profile_image_url_https};
        $user->{profile_image_url_200_https}      = $user->{profile_image_url_https};

        $user->{profile_image_url_mini_https}     =~ s/_normal\./_mini./;
        $user->{profile_image_url_bigger_https}   =~ s/_normal\./_bigger./;
        $user->{profile_image_url_original_https} =~ s/_normal\.//;
        $user->{profile_image_url_200_https}      =~ s/_normal\./_200x200\./;

        my $candidate = $self->user_to_candidate($user);
        $self->add_candidate($candidate);
    }

    my %type_token = %{$standard_tokens{search_type}};

    $type_token{Options}->[0]->[2] = scalar keys %all_users;
    $type_token{Options}->[1]->[2] = scalar keys %tweet_users;
    $type_token{Options}->[2]->[2] = scalar keys %direct_users;

    $self->results->facets([{
        Id => $self->destination . '_search_type',
        Name => 'search_type',
        %type_token,
        Type => 'list'
    }]);
}

# Returns a sanitized HTML string with HTML elements
# correponding to the given twitter entities
sub parse_twitter_entities {
    my ($self, $text, $entities) = @_;
    my @ordered_entities;

    my $html = $text;

    while( my ($type => $entities) = each %$entities) {
        foreach my $entity (@$entities) {
            $entity = \%{$entity};
            $entity->{type} = $type;
            push @ordered_entities, $entity;
        }
    }

    @ordered_entities = sort {
        $b->{indices}->[1] <=> $a->{indices}->[1];
    } @ordered_entities;

    my $previous_index = length($html);
    foreach my $entity (@ordered_entities) {
        my ($start, $end) = @{$entity->{indices}};
        my $length = $end - $start;

        my $trailing_text = substr($html, $end, $previous_index - $end);
        my $trailing_html = HTML::Entities::encode_entities($trailing_text);
        substr($html, $end, $previous_index - $end, $trailing_html);

        my $type = $entity->{type};
        my $content;
        if($type eq 'user_mentions') {

            $content = sprintf(
                '<a target="_blank" href="https://twitter.com/%s">@%s</a>',
                HTML::Entities::encode_entities($entity->{screen_name}),
                HTML::Entities::encode_entities($entity->{screen_name}),
            );
        } elsif($type eq 'hashtags') {
            $content = sprintf(
                '<a target="_blank" href="https://twitter.com/hashtag/%s">#%s</a>',
                HTML::Entities::encode_entities($entity->{text}),
                $entity->{text}
            );
        } elsif($type eq 'urls') {
            $content = sprintf(
                '<a target="_blank" href="%s">%s</a>',
                HTML::Entities::encode_entities($entity->{expanded_url}),
                $entity->{display_url},
            );
        } elsif($type eq 'media') {
            $content = sprintf(
                '<a target="_blank" href="%s">%s</a>',
                $entity->{url},
                $entity->{display_url}
            );
        }
        #Type dependent
        if( defined($content) ) {
            substr($html, $start, $end - $start, $content);
            $previous_index = $start - 1;
        }
    }
    return $html;
}

# returns a Stream Candidate object from a Twitter user hash.
sub user_to_candidate {
    my ($self, $user) = @_;
    my $candidate = $self->new_candidate;

    $candidate->attributes(
        headline => $user->{name},
        twitter_user => $user,
        profile_link => 'https://www.twitter.com/' . $user->{screen_name},
    );
    $candidate->candidate_id($user->{id_str});
    $candidate->has_profile(1);

    my $date = DateTime->from_epoch(
        epoch => Date::Parse::str2time($user->{created_at})
    );

    $user->{created_at_formatted} = $self->_format_date(
        $user->{created_at}
    );

    for ($user->{status}, $user->{status}->{retweeted_status}) {
        next unless $_ && $_->{created_at};
        $_->{created_at_formatted} = $self->_format_date(
            $_->{created_at}
        );
    }

    my @formatted_number_fields = qw(
        statuses_count
        friends_count
        followers_count
        favourites_count
    );

    for my $field (@formatted_number_fields) {
        $user->{$field . '_formatted'} = $self->_format_number($user->{$field});
    }

    $candidate->has_cv(0);
    return $candidate;
}

sub search_begin_search {
    my ($self) = @_;
    $self->search_specific_page(1);
}

# Twitter-like number formatting
sub _format_number {
    my ($self, $number) = @_;

    if($number >= 1_000_000) {
        return sprintf(
            '%.1fM', $number / 1_000_000
        );
    }
    if($number >= 10_000) {
        return sprintf(
            '%.1fK', $number / 1000
        );
    }

    if($number >= 1000) {
        return sprintf(
            '%d,%d',
            substr($number, 0, 1),
            substr($number, 1)
        );
    }
    return $number;
}

sub _format_date {
    my ($self, $date) = @_;

    my $datetime = DateTime->from_epoch(
        epoch => Date::Parse::str2time($date)
    );
    return sprintf(
        $datetime->strftime('%l:%M%p - %e %%s %Y'),
        $datetime->month_abbr
    );
}

sub feed_identify_error {
    my ($self, $message, $content) = @_;
    if($message =~ /^Invalid or expired token/) {
        $self->_throw_oauth_failure('Invalid permission tokens.');
    }
    if($message =~ /^Query parameters are missing/) {
        $self->throw_no_cvsearch('Keywords are required');
    }
    if($message =~ /Rate limit exceeded/) {
        $self->throw_unavailable(
            'Twitter search rate limit exceeded. Please try again shortly'
        );
    }
    warn sprintf("\n\n%s\n====\n%s\n", $message, $content);
    return;
}

1;

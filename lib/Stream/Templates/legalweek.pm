package Stream::Templates::legalweek;
use base qw(Stream::Templates::CrawledMadgex);

use strict;
use warnings;

__PACKAGE__->load_tokens_from_config();

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

use Stream::Constants::SortMetrics qw(:all);

=test auth tokens


http://legalweekjobs-rs.madgexjb.com/
Username: Urwa@broadbean.com
Password: alex12345

=cut

$authtokens{legalweek_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{legalweek_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

1;

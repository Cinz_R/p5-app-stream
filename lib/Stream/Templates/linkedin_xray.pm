#!/usr/bin/perl

package Stream::Templates::linkedin_xray;
use strict;
use utf8;

use base qw(Stream::Templates::google_cse);

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

$standard_tokens{current_job_title} = {
    Label => 'Current Job Title',
    Type  => 'Text',
    SortMetric => $SM_KEYWORDS + 2,
};


$standard_tokens{company_name} = {
    Label => 'Company Name',
    Type  => 'Text',
    SortMetric => $SM_KEYWORDS + 3,
};
$derived_tokens{and_terms} = {
    Type => 'Text',
    Helper => 'google_split_keywords(%keywords%|and_terms|or_terms|not_terms|exact_terms)',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{or_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{not_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{exact_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

# Offload to a different quota. See Google Developer Console.
sub api_key {
    return 'AIzaSyBKF5RUfgr0n5AULD6Eo9Lf75QfExI_X8I';
}

sub build_google_search_query {
    my $self = shift;

    my $base_url = 'linkedin.com';
    my ($location_id, $country_mapping, $li_mapping) = ($self->token_value('location_id'), '');

    # Location mapping for linkedin_xray:-
    # - First we identify the sub domain we should be searching on.  We have a mapping for over 200 countries.
    # - Then we lookup a mapping for what location to send.  If we do not find anything in the mapping for any country
    # other than the UK and the US we send no location at all.
    # - For the UK, we leave out the country (United Kingdom), and if we have no mapping we default to just sending either the county or the location_name.  
    # - This is because we are already searching the UK version of the site.

    if ($location_id){
        my @mapping = $self->LOCATION_MAPPING('linkedin', $location_id);
        $country_mapping = $1 if $mapping[0] =~ /^(\w\w)~/;
        my $subdomain = $country_mapping;
        if ($subdomain eq 'gb'){ $subdomain = 'uk'; }
        if ($subdomain eq 'us'){ $subdomain = 'www';}
        $base_url = $subdomain . '.' . $base_url;

        @mapping = $self->LOCATION_MAPPING('linkedin_xray', $location_id);
        $li_mapping = $mapping[0];

        if (!$li_mapping){
            my $location = $self->location_api()->find({
                id => $location_id,
            });

            if ($location){
                my $county = $location->county();
                my $county_name = $county ? $county->name() : undef;
                if ($country_mapping eq 'us'){
                    if ($location->is_county()){ 
                        $li_mapping = "$county_name Area";
                    } elsif ($location->is_place()){
                        my $place_name = $location->name();
                        $li_mapping = "$place_name, $county_name";
                    }
                } elsif ($country_mapping eq 'gb'){
                    $li_mapping = "$county_name";
                }
            }
        }
        elsif ( $li_mapping eq '*, United Kingdom' ){ #45080
            # If it's a UK location, don't just send a * if we haven't manually added a mapping for it anymore
            my $location = $self->location_api->find({ id => $location_id });
            if ( $location  ){
                my $location_name = $location->name();
                $li_mapping = qq{$location_name};
            }
        } else {
            $li_mapping =~ s/, United Kingdom$//;  #52858 - If United Kingdom comes from the mapping file, remove it.  
        }
    }
 
    my $keywords = "site:" . $base_url . '/in';

    my @tokens = split /\s+/, $self->token_value('company_name') . " " . $self->token_value('current_job_title');

    foreach my $token (@tokens) {
        $keywords .= " AND more:pagemap:person-role:" . $token . " ";
    }
    
    if ($li_mapping){
        $keywords .= " more:pagemap:person-location:$li_mapping ";
        # This works by querying the page map for the linked in site.
        # Example XML for page map is http://www.google.com/cse?cx=003231768667224905379:yagfk7izsje&client=google-csbe&output=xml_no_dtd&q=https://uk.linkedin.com/pub/jamie-caplin/59/321/403
    }


    if ($self->token_value('keywords')){
        if ($self->token_value('and_terms')){
            $keywords .= ' AND (' . $self->token_value('and_terms') . ')';
        }
        if ($self->token_value('exact_terms')){
            $keywords .= ' AND (' . $self->token_value('exact_terms') .')';
        }
        if (my $or_terms = $self->token_value('or_terms')){
            $or_terms =~ s/^\s+OR//;
            $keywords .= ' AND (' . $or_terms .')';
        }
        if ($self->token_value('not_terms')){
            $keywords .= ' AND ' . $self->token_value('not_terms');
        }
    }

    return $keywords;
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    my $title = $candidate->attr('headline');
    $title =~ s/\| LinkedIn//;
    $title =~ s/ \- [^-]+$//;
    $title =~ s/^\s+//;
    $title =~ s/\s+$//;

    my $name = $candidate->attr('name') || $title;

    my $description = $candidate->attr('snippet');
    $candidate->attr('snippet', $self->linkedin_clean_snippet($description, $name));

    return $self->SUPER::scrape_fixup_candidate($candidate);
}

sub linkedin_clean_snippet {
    my ($self, $snippet, $name) = @_;

    $snippet =~ s/View .+?professional profile on LinkedIn\.//;
    $snippet =~ s/LinkedIn is the world's largest business network, helping professionals like \Q$name\E//;
    $snippet =~ s/\Q$name\E.+?Overview//;
    $snippet =~ s/^discover inside//;
    $snippet =~ s/^discover inside connections//;
    $snippet =~ s/^Join LinkedIn and access .+?\.//;
    $snippet =~ s/It\'s free\!//;
    $snippet =~ s/As a LinkedIn member, you'll join 225 million other professionals who are sharing//;
    $snippet =~ s/^\.//;

    return $snippet;
}

1;

package Stream::Templates::BBCSS;

=head1 NAME

Stream::Templates::BBCSS - Base class for templates running on the candidate search platform

=head1 AUTHOR

Patrick - 11/02/2016

=head1 CONTACT

Monica ( PM ) - Monica.Skidmore@careerbuilder.com
Bob ( Lead Dev ) - Bob.Fritsch@careerbuilder.com

=head1 TESTING INFORMATION

=head1 DOCUMENTATION

http://candidatesearchuseast.cb1cl.com/CandidateSearchAPI/swagger/ui/index.html

=head1 OVERVIEW

=head1 EXTRA INFORMATION

=head1 MAJOR CHANGES

none yet

=head1 BUG FIXES

none yet

=cut

use strict;

use base qw(Stream::Engine::API::REST);

use Carp;

use Bean::CareerBuilder::CandidateSearch;
use Log::Any qw/$log/;
use Date::Parse qw//;
use Data::Dumper;
use HTML::Scrubber;

use String::Snip;

use MIME::Base64;

sub search_submit {
    my ( $self ) = @_;
    # the second argument ( subroutine ) tells the agent to return the response without any post-processing ( i.e. json decoding )

    my $search_ref = $self->bbcss_build_search_json();
    my $response = $self->bbcss_client()->search( $search_ref , sub { pop; } );

    unless ( $response->is_success ){
        return $self->determine_error( $response );
    }

    return $response;
}


=head2 adcourier_api_client

Local caching aware adcourier_api_client

=cut

sub adcourier_api_client{
    my ($self) = @_;
    unless( $self->{_adcourier_api_client} ){
        $self->{_adcourier_api_client} = $self->user_object->stream2->adcourier_api();
    }
    return $self->{_adcourier_api_client};
}


sub bbcss_client {
    my ( $self ) = @_;

    return $self->{_bbcss_client} //= $self->user_object->stream2->build_bbcss_api({ process_response_func => sub{ pop; } });
}

=head2 bbcss_build_search_json

Responsible for compiling the actual search request content

=cut

sub bbcss_build_search_json {
    my ( $self ) = @_;
    my $s2 = $self->user_object()->stream2();

    # standard fields
    my %field_map = (
        keywords        => 'keyword',
        location_within => 'radius',
    );
    my %fields = (
        (
            map {
                my $value = $self->bbcss_token_value( $_ );
                $value ? ( $field_map{$_} => $value ) : ();
            } keys %field_map
        ),
        customer_key => $self->bbcss_customer()->{careerbuilder_customer_key},
    );

    # location
    if ( my $location_id = $self->token_value('location_id') ){
        my $location = $self->location_api()->find( { id => $location_id } );
        $fields{location} = $location->fullpath();
    }

    # Any emails addresses in the keywords?
    my @field_filters;
    if( my $keyword = $fields{keyword} ){
        if( my @addresses = Email::Address->parse( $keyword ) ){
            my @email_addresses = map{ $_->address() } @addresses;
            push @field_filters , {
                field_name => 'email',
                operation => 'OR',
                values => [ map{ +{ value => $_ } }  @email_addresses ]
            };
            # Remove email addresses from keywords.
            foreach my $address ( @email_addresses ){
                $keyword =~ s/$address/ /g;
            }
            # And replace keywords
            $fields{keyword} = $keyword;
        }
    }

    if( @field_filters ){
        $fields{filter_aggregates} //= {
            operation => 'AND',
            filters => []
        };
        push @{ $fields{filter_aggregates}->{filters} } , @field_filters;
    }

    $fields{start_page} = $self->current_page();
    $fields{results_per_page} = $self->results_per_page();

    return \%fields;
}

sub download_profile{
    my ($self, $candidate) = @_;

    if( my $cv_url = $candidate->attr('cv_url') ){
        my $cv_response = $self->download_cv( $candidate );
        my $content_type = $cv_response->headers()->content_type();
        my $content = $cv_response->content();

        $candidate->set_attr('cv_mimetype', $content_type );

        if ( $content_type =~ /html$/ ){ # parse-normalize doesn't accept HTML
            my $scrubber = HTML::Scrubber->new();
            $content = $scrubber->scrub( $content );
        }

        # application/pdf files will be rendered with PDF viewer Iframe from the DownloadProfile action
        # So we only need to render non PDF files as HTML.
        if( $content_type ne 'application/pdf'){
            my $html;

            my $res = eval { 
                $self->user_object()->stream2()->parse_normalize_api->parse_normalize({
                    document => MIME::Base64::encode_base64( $content, '' ),
                    'desired_enrichments' => [ 'none' ]
                });
            };

            if ( my $error = $@ ) {
                $self->log_warn( "Got error from CB parse service - Trying BB: %s", Dumper($error) );
                $html = $self->bbcss_use_bb_text_extract( $content );
            }
            elsif ( ! $res->{data}->{resume_html} ){
                confess("Failed to extract HTML from CV for candidate ".$candidate->attr('candidate_id').": ".Dumper($res));
            }
            else {
                $html = $res->{data}->{resume_html};
            }

            # HTML is a perl string.
            if( my $html_fragment = $self->generate_html_fragment( $html ) ){
                $candidate->set_attr('cv_html' , $html_fragment );
            }
        }
    }
    else {
        $candidate->set_attr('cv_html', "<b>No CV found. Cannot render profile.</b>");
    }
    return $candidate;
}

sub bbcss_use_bb_text_extract {
    my ( $self, $content ) = @_;
    my $res = $self->user_object->stream2()->text_extractor()->extract(
        string => $content,
        format => 'html'
    );
    unless( $res->{status} eq 'OK' ){
        confess("Failed to extract HTML from CV for candidate ".Dumper($res));
    }
    return $res->{data};
}

=head2 bbcss_token_value

gets value of field and returns arrayref in event of array

=cut

sub bbcss_token_value {
    my ( $self, $field ) = @_;
    my @values = $self->token_value( $field );

    if ( scalar( @values ) > 1 ){
        return \@values;
    }

    return $values[0];
}

sub search_success { 'total_matching_records'; }
sub search_failed { '400 Bad Request'; }
sub results_per_page {
    my $self = shift;

    #  return a specified number of results per page, if it exists
    return $self->token_value('results_per_page') ?
        $self->token_value('results_per_page') + 0  : 20;
}

sub scrape_candidate_array {
    my ( $self ) = @_;

    #$self->scrape_facets();

    my $result_ref = $self->response_to_JSON();

    if ( exists ( $result_ref->{data}->{results}->{documents} ) ){
        return @{ $result_ref->{data}->{results}->{documents} };
    }

    return;
}

sub bbcss_naics_lookup {
    my ( $self, @codes ) = @_;

    my @naics_rows = $self->user_object->stream2->stream_schema->resultset('NaicsCode')->search({
        naics_code => { -in => \@codes }
    })->all();

    return map { $_->naics_code => $_->title } @naics_rows;
}

sub bbcss_onet_lookup {
    my ( $self, @codes ) = @_;

    my @onet_rows = $self->user_object->stream2->stream_schema->resultset('OnetCode')->search({
        onetsoc_code => { -in => \@codes }
    })->all();

    my %map = map { $_->onetsoc_code => $_->title } @onet_rows;
    $map{'99-9999.99'} = 'other';

    return %map;
}

sub scrape_candidate_details {
    my ( $self, $result_ref ) = @_;

    if ( my $custom_field_list = $result_ref->{custom_field_list} ){
        # do something clever with the custom field list
    }

    return (
        candidate_id                => $result_ref->{document_id},

        first_name                  => $result_ref->{first_name},
        last_name                   => $result_ref->{last_name},
        email                       => $result_ref->{email},
        phone                       => $result_ref->{phone},

        address                     => $result_ref->{address1},
        county                      => $result_ref->{admin_area_1},
        city                        => $result_ref->{city},
        country                     => $result_ref->{country},
        postcode                    => $result_ref->{postal_code},
        latitude                    => $result_ref->{latitude},
        longitude                   => $result_ref->{longitude},

        cv_text                     => $result_ref->{resume_description},
        documents                   => $result_ref->{document_files},

        currently_employed          => $result_ref->{currently_employed},
        total_years_experience      => $result_ref->{total_years_experience},
        activity_history            => $result_ref->{activity_history},
        skill_list                  => $result_ref->{skill_list},
        company_experience_list     => $result_ref->{company_experience_list},
        education_list              => $result_ref->{education_list},
        custom_field_list           => $result_ref->{custom_field_list},

        has_done_delete             => $result_ref->{has_done_delete},

        tags                        => $result_ref->{broadbean_tags},
    );
}

sub scrape_fixup_candidate {
    my ( $self, $candidate ) = @_;

    if ( scalar( @{ $candidate->attr('company_experience_list') // [] } ) ){
        # normalise a few fields / dates
        foreach my $experience ( @{ $candidate->attr('company_experience_list') } ){
            $experience->{start_date_epoch} = Date::Parse::str2time( $experience->{start_date} );
            $experience->{end_date_epoch} = Date::Parse::str2time( $experience->{end_date} );

            # Inject this years of services in this candidate
            # if it is not there already. This is to accomodate
            # old alpha clients who didnt have this field 'broadbean_employer_years'
            unless( defined $candidate->attr('broadbean_employer_years') ){
                my $years_of_service = $self->_calculate_time_diff_in_years(
                    $experience->{start_date_epoch},$experience->{end_date_epoch}
                );
                $candidate->attr('broadbean_employer_years' => $years_of_service );
            }
            # Inject the company name as broadbean_employer_org_name
            # if it is not there already. This is to accomodate old alpha
            # stage clients who did not have the field 'broadbean_employer_org_name' yet.
            unless( defined $candidate->attr('broadbean_employer_org_name' ) ){
                $candidate->attr('broadbean_employer_org_name' => $experience->{company_name} );
            }

            # Inject the company job_title as broadbean_employer_org_job_title
            # if it is not there already. This is to accomodate old alpha
            # stage clients who did not have the fied 'broadbean_employer_org_job_title' yet.
            unless( defined $candidate->attr('broadbean_employer_org_job_title')  ){
                $candidate->attr('broadbean_employer_org_job_title' => $experience->{job_title} );
            }

            if ( $experience->{carotene_v2} && $experience->{carotene_v2} ne 'Unclassified' ){
                $experience->{job_title} = $experience->{carotene_v2};
            }
            $experience->{description} =~ s/\n/ /g;

            if ( ! $candidate->attr('current_position') && $experience->{is_current_position} ){
                $candidate->attr('current_position' => $experience);
            }
        }
        if ( ! $candidate->attr('current_position') ){
            $candidate->attr( current_position => $candidate->attr('company_experience_list')->[0] );
        }
    }

    # convert the Iso dates to epochs for display / localisation
    foreach my $education ( @{ $candidate->attr('education_list') // [] } ){
        # don't want to display "None" as the subject
        if ( $education->{degree_name} eq 'None' ){
            delete $education->{degree_name};
        }

        # graduation date to epoch
        if( ( my $graduation_date = Date::Parse::str2time( $education->{graduation_date} ) ) > 0 ){
            $education->{graduation_date_epoch} = $graduation_date;
        }
    }

    if ( defined( $candidate->attr('documents') ) && scalar( @{$candidate->attr('documents')} ) ){
        my $cv_ref = $candidate->attr('documents')->[0];
        $candidate->has_cv(1);
        $candidate->attr('cv_url' => $cv_ref->{document_file_url});
    }
    else {
        $candidate->has_cv(0);
    }

    if ( defined( $candidate->attr('skill_list') ) && scalar( @{$candidate->attr('skill_list')} ) ){
        $candidate->attr('skills' => join( ", ", @{$candidate->attr('skill_list')} ));
    }

    $candidate->attr('has_profile' => 1);
    $candidate->attr('editable' => 1);
}

sub scrape_candidate_add_headline {
    my ( $self, $candidate ) = @_;

    if ( my $jobtitle = $candidate->attr('current_job_title') ){
        $candidate->attr('headline' => $jobtitle);
    }
}

sub scrape_number_of_results {
    my ( $self ) = @_;
    my $results_ref = $self->response_to_JSON();
    my $total_results = $results_ref->{data}->{total_matching_records} || 0;
    $self->total_results( $total_results );
}

=head2 after_content_updated

    called each time $self->recent_content is set

=cut

sub after_content_updated {
    my ( $self ) = @_;
    $self->{recent_json} = eval {
        JSON::from_json( $self->recent_content(), { utf8 => 1 } );
    };
    if ( my $err = $@ ) {
        $log->infof('Error in feed BBCSS  decoding json response: %s', $err );
        $log->debug("^^^^ This happens because of all the automagic in feeds when one uses ->get");
        $self->{recent_json} = undef;
    }
}

sub response_to_JSON { shift->{recent_json} || {} }

sub bbcss_flavour {}

sub bbcss_customer {
    my ( $self ) = @_;

    my $flavour = $self->bbcss_flavour();
    unless ( $self->{customer} ){
        $self->{customer} = $self->bbcss_client()->get_customer({
            broadbean_name => $self->company(),
            ( $flavour ? ( flavour => $flavour ) : () )
        }) or $self->throw_login_error("Customer ".$self->company()." does not yet exist");
    }

    return $self->{customer};
}

sub download_cv {
    my ( $self, $candidate ) = @_;

    if ( ! $candidate->attr('cv_url') ){
        $self->throw_error( sprintf( "Candidate: %s does not have a cv_url", $candidate->id ) );
    }
    $log->info("Candidate CV URL: ".$candidate->attr('cv_url'));
    my $location = $self->bbcss_to_s3_url( $candidate->attr('cv_url') );
    return $self->agent()->get( $location );
}

=head2 bbcss_to_s3_url

Exchanges a BBCSS CV URL for an limited-life authenticated S3 url

=cut

sub bbcss_to_s3_url {
    my ( $self, $url ) = @_;
    return unless( $url );

    my $response = $self->bbcss_client()->ua->request(
        $self->bbcss_client->authenticate( HTTP::Request->new( GET => $url ) )
    );
    return $response->header('location');
}

=head2 update_candidate

Usage:

 $this->update_candidate( $candidate , { field => 'blabla' } );

=cut

sub update_candidate {
    my ( $self, $candidate, $fields , $opts  ) = @_;
    $opts //= {};

    $log->info("Updating candidate ".$candidate->id()." with fields =".Dumper($fields));

    my $request_ref = {
        document_id => $candidate->candidate_id,
        update_fields => [ map { { name => $_, value => $fields->{$_} } } keys %$fields ],
        customer_key => $self->bbcss_customer()->{careerbuilder_customer_key},
        $opts->{action} ? ( action => $opts->{action} ) : (),
    };

    my $response = $self->bbcss_client->update_document( $request_ref );
    unless ( $response->is_success() ){
        $self->log_error( 'Update failed: RESPONSE: %s, REQUEST: %s', $response->as_string, $response->request->as_string );
        return $self->throw_error( 'Update candidate failed' );
    }

    return 1;
}

sub determine_error {
    my ( $self, $response ) = @_;

    # extract JSON body
    my $decoded_content = $response->decoded_content();
    $self->recent_content($decoded_content);
    my $json_ref = $self->response_to_JSON();

    # extract error from that JSON body
    my $error;
    if ( my $error_content = $json_ref->{error} ){
        my $error_response = eval { HTTP::Response->parse( $error_content ) };
        if ( !$@ && $error_response ){
            $error = $self->_extract_error( $error_response );
        }
    }

    unless ( $error ) {
        return $self->throw_error( 'Request failed' );
    }

    return $self->throw_error( $error );
}

sub _extract_error {
    my ( $self, $response ) = @_;

    my $json_ref = eval {
        JSON::from_json( $response->decoded_content, { utf8 => 1 } );
    };

    if ( defined( my $errors = $json_ref->{errors} ) ){
        return $errors->[0]->{message};
    }

    return;
}

=head2 tags_field_name

Returns the name of the BBCSS custom field holding the tags.

Implement that in subclasses!

=cut

sub tags_field_name{
    confess("Implement me please.");
}

=head2 tag_candidate

Updates the tags in talent search with the newly created tag.

=cut

sub tag_candidate{
    my ($self, $candidate, $tag) = @_;

    # Update the candidate in talent search.
    $log->infof("Tagging candidate '%s' with tag '%s' through BBCSS API",
        $candidate->id(),
        $tag->tag_name
    );

    my $resp = $self->bbcss_client->add_multilist({
        customer_key => $self->bbcss_customer()->{careerbuilder_customer_key},
        document_id => $candidate->candidate_id,
        field_name => $self->tags_field_name(),
        field_value => $tag->tag_name,
    });
    my $decoded_content = $resp->decoded_content();
    $self->recent_content($decoded_content);

    # return the final value of the tag as added to the candidate
    $log->infof('Tagged candidate %s with "%s" successfully', $candidate->id(), $tag->tag_name);
    return $self->response_to_JSON()->{field_value};
}

=head2 untag_candidate

Removes the tag in talent search.

=cut

sub untag_candidate{
    my ($self, $candidate, $tag) = @_;

    unless ( $tag ) {
        $log->infof('No tag specified, will not untag for "%s"', $candidate->id())
        and return;
    }

    $log->infof("Untagging candidate '%s' with tag '%s' through BBCSS API",
        $candidate->id(),
        $tag->tag_name
    );

    my $resp = $self->bbcss_client->del_multilist({
        customer_key => $self->bbcss_customer()->{careerbuilder_customer_key},
        document_id => $candidate->candidate_id,
        field_name => $self->tags_field_name(),
        field_value => $tag->tag_name,
    });
    my $decoded_content = $resp->decoded_content();
    $self->recent_content($decoded_content);

    $log->infof('Untagged candidate %s from "%s" successfully', $candidate->id(), $tag->tag_name);
    return $tag;
}

=head2 untag_candidate_bulk

Untag every single candidates tagged with this tag.

=cut

sub untag_candidate_bulk{
    my ($self, $tag_name) = @_;

    $tag_name // confess("Missing tag_name to remove");

    unless( $self->user_object() ){
        confess("Missing user object on self");
    }


    my $page = 1 ;
    my $n_on_page = 0;

    my $stream2 = $self->user_object()->stream2();

    do{

        $log->info("Looking up candidates with tag '$tag_name' on page '$page'");

        # Build a new instance of talentsearch to look up all the tagged candidates.
        my $results_collector = Stream2::Results->new({destination => 'candidatesearch'});
        $results_collector->current_page($page);
        my $ts_feed = $stream2->build_template('candidatesearch',
                                               undef,
                                               {company => $self->user_object->group_identity(),
                                                user_object => $self->user_object()
                                               }
                                              );
        $ts_feed->results($results_collector);

        $ts_feed->token_value( $self->tags_field_name() , $tag_name );
        $ts_feed->run_helpers();
        $ts_feed->search({ page => $page });

        $n_on_page = $results_collector->n_results();

        $log->info("Found $n_on_page candidates on page '$page'. Will untag them all");

        foreach my $candidate ( @{ $results_collector->results() } ){
            $self->untag_candidate( $candidate, $tag_name );
        }

        $page += 1;
    } while( $n_on_page > 0 );

    return;
}


sub bbcss_board_nice_name {
    my ( $self, $board ) = @_;
    if ( my $board_obj = $self->user_object->stream2->factory('Board')->fetch({ id => $board }) ) {
        return $board_obj->nice_name;
    }
    return $board;
}


sub _calculate_time_diff_in_years {
    my ($self,$start_time_epoch,$end_time_epoch) = @_;

    return unless $start_time_epoch && $end_time_epoch;

    my $dt_start_date = DateTime->from_epoch( epoch => $start_time_epoch );
    my $dt_end_date = DateTime->from_epoch( epoch => $end_time_epoch );
    my $dt_diff = $dt_end_date->subtract_datetime($dt_start_date);
    return '< 1' if 0 == $dt_diff->years;
    return $dt_diff->years;
}
1;

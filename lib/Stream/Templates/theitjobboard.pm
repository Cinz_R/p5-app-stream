package Stream::Templates::theitjobboard;
use strict;
use utf8;
use Encode;

use base qw(Stream::Engine::API::XML);

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

$authtokens{'theitjobboard_username'} = {
        Label => 'Username',
        Type  => 'Text',
        Size  => 20,
        SortMetric => 1,
};

$authtokens{'theitjobboard_password'} = {
        Label => 'Password',
        Type  => 'Text',
        Size  => 10,
        SortMetric => 2,
};

$derived_tokens{'edited_within'} = {
        Label      => 'Edited Within',
        Type       => 'List',
        Options    => '(0|1|2|3|4|5|7|14|30|60|90|120|365|INT1|INT2|INT3|INT4|INT5|INT6|INT7)',
        Helper     => 'theitjobboard_edited_within(%cv_updated_within%)',
        Default    => 'All',
        SortMetric => $SM_DATE,
};

$derived_tokens{salary_cur} ={
    Type    => 'Currency',
    Label   => 'Salary Currency',
    Options => 'GBP|EUR',
    Default => 'GBP',
    Helper  => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
    HelperMetric => 2,
    SortMetric  => $SM_SALARY,
    };

$standard_tokens{'jobtype'} = {
        Label      => 'Job Type',
        Type       => 'List(All=0|Contract=1|Permanent=2|Internship=7)',
        SortMetric => $SM_JOBTYPE,
        Deprecated => 'JobType',
};

$posting_only_tokens{job_type} = {
        #List(All=0|Contract=1|Permanent=2|Internship=7)
        Helper  => 'theitjobboard_jobtype_helpder(%default_jobtype%|%theitjobboard_jobtype%|permanent=2|contract=1|temporary=1)',
        Default => '0'
};

$standard_tokens{'education'} = {
        Label => 'Education',
        Type => 'List(Unknown=0|All=-1|GCSE=1|A Level / AS Level / BTEC / NVQ=2|HND=3|BA / BSc / BEng=4|MA / MSc / MEng=5|PhD=6)',
        SortMetric => $SM_EDUCATION,
        Default => '0'
};

$standard_tokens{'experience'} = {
        Label => 'Experience',
        Type  => 'List(All=0|1=1|2=2|3=3|4=4|5+=5)',
        SortMetric => $SM_EXPERIENCE,
        Default => '0',
};

$standard_tokens{'relocation'} = {
        Label => 'Relocation',
        Type  => 'List(Any=|Yes=Y)',
        SortMetric => $SM_LOCATION,
};

$standard_tokens{'eu_elibiable'} = {
        Label => 'EU Eligability',
        Type  => 'List(Any=|Yes=Y)',
        SortMetric => $SM_LOCATION
};


$standard_tokens{'availability'} = {
        Label => 'Availability',
        Type  => 'List(Dont Care=-1|Now=0|Next Week=7|2 Weeks=14|1 Month=30|3 Months=90|6 Months=180)',
        SortMetric => $SM_JOBTYPE,
        Default => '-1'
};

$derived_tokens{'radius'} = {
        Label      => 'Search X miles around the postcode',
        Type       => 'DistanceList',
        Options    => '(0|1|5|10|20|30|40|50|100|200)',
        Unit       => 'miles',
        Helper     => 'LOCATION_WITHIN(radius|%location_within_miles%)',
        SortMetric => $SM_LOCATION,
};

$posting_only_tokens{'populate_location'} = {
        Helper => 'theitjobboard_location(%location_id%)',
};

##  theitjobboard_jobtype_helpder test: tests/stream/feeds/global-jobtype/13-theitjobboard.t
sub theitjobboard_jobtype_helpder {
    my $self = shift;
    my $new  = shift;
    my $old  = shift;
    return defined($new) && $new ne q{} ? $self->HASH_MAP($new => @_) : $old;
}

sub base_url {
    return 'http://recruiters.eu.dice.com/api/broadbean/index.php';
}

sub search_url {
    return $_[0]->base_url() . '?Mode=Search';
}

sub search_failed {
    return qr/<Error>/;
}

sub search_build_xml{
    my $self = shift;

    my $salary_unknown  = ($self->token_value('salary_unknown')) ? 'Yes' : 'No';
    my $doc = Bean::XML->new( ENCODING => 'utf8' );
    $doc->startTag('CV_Search_Query');
        $doc->startTag('Account');
            $doc->startTag('Username');
                $doc->characters( $self->token_value('username')  );
            $doc->endTag('Username');
            $doc->startTag('Password');
                $doc->characters( $self->token_value('password') );
            $doc->endTag('Password');
        $doc->endTag('Account');
        $doc->startTag('Search_Query');
            $doc->startTag('Page');
                $doc->characters($self->current_page());
            $doc->endTag('Page');
            $doc->startTag('SearchTerms');
                $doc->characters($self->token_value('keywords'));
            $doc->endTag('SearchTerms');
            $doc->startTag('Location');
                $doc->characters( $self->token_value('location_text'));
            $doc->endTag('Location');
            $doc->startTag('CountryIsoCode');
                $doc->characters($self->token_value('country_iso'));
            $doc->endTag('CountryIsoCode');
            $doc->startTag('Postcode');
                $doc->characters($self->token_value('postcode'));
            $doc->endTag('Postcode');
            $doc->startTag('RadiusDistance');
                if ($self->token_value('postcode') && $self->token_value('country_iso')){
                    $doc->characters( $self->token_value('radius') );
                }
            $doc->endTag('RadiusDistance');
            $doc->startTag('RadiusIsInKilometres');
                $doc->characters('0');
            $doc->endTag('RadiusIsInKilometres');
            $doc->startTag('JobType');
                $doc->characters($self->token_value_or_default('job_type'));
            $doc->endTag('JobType');
            $doc->startTag('Availability');
                $doc->characters( $self->token_value_or_default('availability') );
            $doc->endTag('Availability');
            $doc->startTag('EducationLevel');
                $doc->characters($self->token_value('education'));
            $doc->endTag('EducationLevel');
            $doc->startTag('YearsExperience');
                $doc->characters($self->token_value('experience'));
            $doc->endTag('YearsExperience');
            $doc->startTag('DateAdded');
                $doc->characters( $self->token_value('edited_within') );
            $doc->endTag('DateAdded');
            $doc->startTag('WillingToRelocate');
                $doc->characters($self->token_value('relocation'));
            $doc->endTag('WillingToRelocate');
            $doc->startTag('EUEligable');
                $doc->characters($self->token_value('eu_elibiable'));
            $doc->endTag('EUEligable');
           
        if($self->token_value('salary_from') || $self->token_value('salary_to')){
        
            $doc->startTag('SalaryFrequency');
                $doc->characters(ucfirst($self->token_value('salary_per')));
            $doc->endTag('SalaryFrequency');
            $doc->startTag('Minsalary');
                $doc->characters($self->token_value('salary_from'));
            $doc->endTag('Minsalary');
            $doc->startTag('Maxsalary');
                $doc->characters($self->token_value('salary_to'));
            $doc->endTag('Maxsalary');
            $doc->startTag('SalaryCurrency');
                $doc->characters($self->token_value('salary_cur'));
            $doc->endTag('SalaryCurrency');
            $doc->startTag('Include_unspecified_salaries');
                $doc->characters($salary_unknown);
            $doc->endTag('Include_unspecified_salaries');
       
       }


        $doc->endTag('Search_Query');
    $doc->endTag('CV_Search_Query');
    $doc->end();
    return $doc->asString();
}

# bit weird: if there are 2 pages of results and we ask for the 3rd page, the IT Job Board returns the
# the 2 page of results instead of an empty page
sub search_number_of_results_xpath { return '//Matches_Total'; }

sub scrape_candidate_xpath { return '//Result'; }

sub scrape_candidate_details {
    return (
        candidate_id        => './CandidateId/text()',
        first_name       => './FirstName/text()',
        last_name       => './LastName/text()',
        job_title       => './CurrentJobTitle/text()',
        country     => './Country/text()',
        town        =>  './Town/text()',
        postcode    =>  './PostCode/text()',
        date_added => './PostedDate/text()',
        availibility => './Availibility/text()',
        eu_eligable => './EuEligable/text()',
        snippet => './Synopsis/text()',
        home_telephone => './HomeTelephone/text()',
        mobile_telephone => './MobileTelephone/text()',
        salary  =>  './Salary',
        jobtype =>  './JobType',
        will_relocate   =>  './Will_Relocate',
    );
}

sub scrape_fixup_candidate{
    my $self = shift;
    my $candidate = shift;
    
    my $raw_date_added = $candidate->attr('date_added');
    my $raw_availibility = $candidate->attr('availibility');   

    if ( $raw_date_added ) {
        my $date_added = $candidate->pattern_to_epoch( '%d/%m/%Y' => $raw_date_added ) || $raw_date_added;
        $candidate->attr('date_added_epoch' => $date_added );
    }

    if ($raw_availibility){
        my $availibility = $candidate->pattern_to_epoch( '%d/%m/%Y %H:%M:%S' => $raw_availibility ) || $raw_availibility;
        $candidate->attr('availibility_epoch' => $availibility );
    }
#    foreach my $field (qw/town/) {
#        $candidate->attrs($field => decode('utf-8', $candidate->attrs($field)));
#    }
    $candidate->has_profile(1);
}


sub download_profile_url{
    return $_[0]->base_url() . '?Mode=ViewCandidate';
}

sub download_profile_xml{
    my $self = shift;
    my $candidate = shift;

    return $self->theitjobboard_download_profile_xml($candidate->candidate_id());
    
    }

sub scrape_download_profile_xpath{
    return "//CV_Candidate_Query";    
    
    }
sub scrape_download_profile_details{
    return ( cv_preview => '//CV_Preview');
    
    }

sub theitjobboard_download_profile_xml{
    my $self = shift;
    my $candidate_id = shift;
    
    my $doc = Bean::XML->new( ENCODING => 'utf8' );
    $doc->startTag('CV_Search_Query');
        $doc->startTag('Account');
            $doc->startTag('Username');
                $doc->characters($self->token_value('theitjobboard_username'));
            $doc->endTag('Username');
            $doc->startTag('Password');
                $doc->characters($self->token_value('theitjobboard_password'));
            $doc->endTag('Password');
        $doc->endTag('Account');
        $doc->startTag('Candidate_Query');
            $doc->startTag('CandidateId');
                $doc->characters($candidate_id);
            $doc->endTag('CandidateId');
        $doc->endTag('Candidate_Query');
    $doc->endTag('CV_Search_Query');
    $doc->end();
    return $doc->asString();
   
    
    }

sub download_cv_url{
    return $_[0]->base_url() . '?Mode=DownloadCV';
}

sub download_cv_xml {
    my $self = shift;
    my $candidate = shift;

    return $self->theitjobboard_download_cv_xml(
        $candidate->candidate_id()
    );
}

sub theitjobboard_download_cv_xml {
    my $self = shift;
    my $candidate_id = shift;
    my $doc = Bean::XML->new( ENCODING => 'utf8' );
    $doc->startTag('CV_Search_Query');
        $doc->startTag('Account');
            $doc->startTag('Username');
                $doc->characters($self->token_value('theitjobboard_username'));
            $doc->endTag('Username');
            $doc->startTag('Password');
                $doc->characters($self->token_value('theitjobboard_password'));
            $doc->endTag('Password');
        $doc->endTag('Account');
        $doc->startTag('Candidate_CV_Query');
            $doc->startTag('CandidateId');
                $doc->characters($candidate_id);
            $doc->endTag('CandidateId');
        $doc->endTag('Candidate_CV_Query');
    $doc->endTag('CV_Search_Query');
    $doc->end();
    return $doc->asString();
}

sub scrape_download_cv_xpath   { return '//Candidate_CV_Response'; }

sub scrape_download_cv_details {
    my $self = shift;

    return (
        resume_word_base64 => './/File',
        resume_filename    => './/File/@filename',
    );
}

sub theitjobboard_edited_within {
    my $self = shift;
    my $cv_updated_within = shift;

    if ( !$cv_updated_within ){
        return;
    } elsif ( $cv_updated_within eq 'ALL' ) {
        return 0;
    } elsif ( $cv_updated_within eq 'TODAY' ) {
        return 1;
    } elsif ( $cv_updated_within eq 'YESTERDAY' ) {
        return 2;
    }
    elsif ($cv_updated_within eq '3D'){
        return 7;     
    }
    elsif ($cv_updated_within eq '1W'){
        return 7;
    }
    elsif ($cv_updated_within eq '2W'){
        return 14;
    }
    elsif ($cv_updated_within eq '1M'){
        return 30;
    }
    elsif ($cv_updated_within eq '2M'){
        return 60;
    }
    elsif ($cv_updated_within eq '3M'){
        return 90;
    }
    elsif($cv_updated_within eq '1Y'){
        return 365;
    }
    elsif($cv_updated_within eq '2Y'){
        return 730;
    }
    elsif($cv_updated_within eq '3Y'){
        return 1095;
    }

    return 180;
}

sub theitjobboard_location {
    my $self = shift;
    my $location_id = shift or return;

    my $location = $self->location_api()->find({
        id => $location_id,
    });

    if ( !$location ) {
        return;
    }

    my $iso_code = $location->iso_country();
    my $text_val = $location->name();

    my $user_pcode = $self->token_value('location_id_postcode');

    my $holland_id = 38524; # location id of holland

    my $postcode;
    if ( $location->in_uk() ) {
        # returns a UK postcode only
        $postcode = $self->LOCATION_FULL_POSTCODE($location_id, $user_pcode);
    }
    elsif ( $location->has_parent($holland_id, $location_id) ) {
        # send dutch postal codes too as the It Job Board supports them
        $postcode = $location->zipcode();
    }

    my %supported_isos = (
        'AT' => 'AUT',
        'BE' => 'BEL',
        'FR' => 'FRA',
        'DE'  => 'DEU',
        'NL'  => 'NLD',
        'CH'  => 'CHE',
        'GB'  => 'GBR'
    );

    if ( $postcode ) {
        $self->token_value( 'postcode' => $postcode );
    } else {
        $self->token_value( 'location_text' => $text_val );
    }

    if (exists $supported_isos{$iso_code}){
        $self->token_value( 'country_iso' => $supported_isos{$iso_code} );
    }
}

sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    if ( !defined( $content ) ) {
        $content = $self->content();

        return unless $content;
    }

    if ( $content =~ m{<Error>(.+)</Error>} ) {
        my $error_msg = $1;

        if ( $error_msg =~ m{ERROR.NO_DOWNLOAD_QUOTA} ) {
            return $self->throw_lack_of_credit( $error_msg );
        }
        if ( $error_msg =~ m{Invalid username or password} ) {
            return $self->throw_login_error( $error_msg );
        }

        return $error_msg;
    }

    return undef;
}



package Stream::Templates::bullhorn;

use strict;
use warnings;
use base qw(Stream::Engine::API::REST);

use utf8; # some of the locations has utf8 encoded accents in them
use POSIX qw(ceil strftime);

use Stream::Constants::SortMetrics qw(:all);
use Stream::Keywords;

use JSON;
use Data::Dumper;
use DateTime;

use Mojo::UserAgent;
use Mojo::IOLoop;

use Lucy;
use Lucy::Plan::Schema;

my @BULLHORN_TOKENS = ();

# Bullhorn return every facet known to man, we must remove
# most of them as they are not desired - see SEAR-915
my @FIELDS_TO_SKIP = (
"Username",
"Travel Method",
"Time Zone Offset EST",
"Tax State",
"State Filing Status",
"State Exemption",
"State Additional Witholdings Amount",
"Secondary Owners",
"Person Subtype",
"Paper Work on File",
"Num Owners",
"Num Categories",
"Migrate GUID",
"Master User ID",
"Local Tax Code",
"Local Filing Status",
"Local Exemptions",
"Local Additional Withholdings Amount",
"Is Onboarding Ready To Start",
"Is Locked Out",
"Is Day Light Savings",
"I9 On File",
"Federal Filing Status",
"Federal Exemptions",
"Federal Additional Withholdings Amount",
"Date Next Call",
"Date I9 Expiration",
"Contractor Name",
"General Contractor Comments",
"Contractor Rehire",
"Contractor Name",
"Contractor",
"CV Updated"
);

## We will use a Lucy query parser to
## expand the keywords to all the bullhorn fields.
my $LUCY_PARSER;

{
    my $schema = Lucy::Plan::Schema->new();

    my $easyanal
      = Lucy::Analysis::RegexTokenizer->new( pattern => '\S+' );

    my $str_type = Lucy::Plan::FullTextType->new( analyzer => $easyanal );

    my @fields = qw/id name companyName type occupation description fileAttachments.description/;
    foreach my $field ( @fields ){
        $schema->spec_field( name => $field , type => $str_type );
    }


    $LUCY_PARSER = Lucy::Search::QueryParser->new( schema => $schema,
                                                   default_boolop  => 'OR',
                                                   fields => \@fields
                                                 );
    ## Do not parse bla:thing
    $LUCY_PARSER->set_heed_colons(0);
}

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

## Broadbean sandbox account:
## User: lhopkins
## Pass: Broadbean1
## bullhornstaffing.com

$posting_only_tokens{updated_after} = {
    Label      => 'Updated Within',
    Helper     => 'CV_UPDATED_WITHIN_TO_ISO8601(%cv_updated_within%)',
};

$derived_tokens{and_terms} = {
    Type => 'Text',
    Helper => 'SPLIT_KEYWORDS(%keywords%|and_terms|or_terms|not_terms|exact_terms)',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{or_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{not_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{exact_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{'salary_per'} = {
    Type  => 'SalaryPer',
    Options => 'hour|day|annum',
    Helper => 'SALARY_PER(salary_per|salary_from|salary_to)',
    Default => 'annum',
    HelperMetric => 3,
};

$derived_tokens{'salary_cur'} = {
    Type         => 'Currency',
    Options      => 'USD',
    Helper       => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
    HelperMetric => 2,
};

$posting_only_tokens{'populate_bullhorn_location'} = {
            Helper => 'bullhorn_location(%location_id%)',
};

sub login_content_type {
    return 'application/x-www-form-urlencoded; charset=UTF-8';
}

sub session_life_default {
    return 86400 * 365; # 1 year.
}

sub login {
    my $self = shift;

    if ( $self->logged_in() ) {
        return;
    }

    $self->log_debug('Attempting to get a Bullhorn session');

    my $stream2 = $self->user_object()->stream2();

    my $session = $stream2->user_api->bullhorn_session($self->user_object->provider_id());
    unless( $session ){
        $self->throw_login_error('No Bullhorn session could be built');
    }

    my $session_key = $session->{BhRestToken}
        || $self->throw_login_error('No Session Found');
    my $rest_url    = $session->{restUrl}
        || $self->throw_login_error('No Session Found');

    $self->{rest_url} = $rest_url;
    $self->bullhorn_session($session_key);

    $self->{logged_in} = 1;
    $self->log_debug('logged in');
}

sub bullhorn_session {
    return $_[0]->account_value( 'session' ) if @_ == 1;
    return $_[0]->account_value( 'session' => $_[1] );
}

sub base_url {
    my $self = shift;
    return $self->{rest_url};
}

sub bullhorn_private_label_url {
    my $self = shift;
    return $self->{rest_url} . 'settings/privateLabelId';
}

sub results_per_page{
    return 30;
}

sub bullhornStaffingHost{
    my $self = shift;
    return @_ ? $self->{bullhornStaffingHost} = shift : $self->{bullhornStaffingHost};
}

sub bullhorn_private_label_id {
    my $self = shift;

    unless ( $self->account_value('private_label_id') ) {
        my $response = $self->get( $self->bullhorn_private_label_url(), $self->set_auth_headers());
        my $json = JSON::decode_json($response->decoded_content);
        $self->set_account_value('private_label_id' => $json->{privateLabelId}->{id}, 86400);
    }

    $self->log_info('Returned a private label value of [%s]', $self->account_value('private_label_id'));

    return $self->account_value('private_label_id');
}

sub search_submit {
    my $self = shift;

    my @query = $self->field_value_pairs($self->search_fields());

    my $query_string = $self->to_querystring(@query);

    ## Determine the bullHornStaffingHost
    ## This will be the host to be used to build a link in the fixup_candidate.

    $self->log_info("Getting the bullhostStaffingHost");
    my $json_resp = $self->get( $self->{rest_url} . 'settings/bullhornStaffingHost', $self->set_auth_headers() );
    $json_resp = JSON::from_json( $json_resp->decoded_content() );
    $self->bullhornStaffingHost($json_resp->{'bullhornStaffingHost'});
    $self->log_info( "Bullhorn staffing host is: " . $self->bullhornStaffingHost() );

    return $self->get($self->search_url(), $self->set_auth_headers());
}

sub set_auth_headers {
    my $self = shift;
    return ("BhRestToken" => $self->bullhorn_session());
}

sub search_url {
    my $self = shift;

    use URI::Escape;

    return $self->{rest_url} .
    "search/Candidate?query=" . uri_escape_utf8($self->search_query())
                              . "&fields=*&showTotalMatched=true&showLabels=true&entityId=-1&useV2=true"
                              . "&sort=" . $self->token_value_or_default('sort') . "&count=30&start=" . ($self->current_page() - 1 ) * 30;
}

=head2 bullhorn_expand_query_string

Expands the given query string (meaning concatenate to it) with
an expansion of the given keywords).

Usage:

  my $query_string = $this->bullhorn_expand_query_string($query_string , 'kitten manager');

=cut

sub bullhorn_expand_query_string{
    my ($self, $query_string , $bh_keywords) = @_;

    # Never any ',' please
    $bh_keywords =~ s/,/ /g;

    # Trim leading and trailing spaces
    $bh_keywords =~ s/^\s+//;
    $bh_keywords =~ s/\s+$//;


    unless( $bh_keywords ){
        # Do nothing if no keywords are given
        return $query_string;
    }

    my $lucy_query = eval {
        $LUCY_PARSER->parse($bh_keywords);
    };
    if($@) {
        $self->throw_invalid_boolean_search('We could not process the query. Please check quotes.');
    }

    my $expanded_keywords = $lucy_query->to_string()."\n";

    # No new lines please
    $expanded_keywords =~ s/\n/ /;

    # Change [NOMATCH] with something that never match. This is
    # output by Lucy in case of syntax horror.
    $expanded_keywords =~ s/\[NOMATCH\]/ id:e59a8214adfa11e4a5b9c7d4c8ae23a0 /;

    return $query_string . ' AND ( '.$expanded_keywords.' ) ';
}

sub search_query {

    my $self = shift;

    my $privateLabelId = $self->bullhorn_private_label_id() || '';

    $self->bullhorn_custom_facets($self->base_url() . "meta/Candidate?fields=*&meta=full&privateLabelId=$privateLabelId");

    my $query_string = "isDeleted:0 ";

    #Keywords
    my $bh_keywords = $self->token_value('keywords');

    $bh_keywords =~ s/&/ /g;#remove ampersands as thier API does not like it, even when escpaed!!!!!!!!!

    if($bh_keywords){
        $query_string = $self->bullhorn_expand_query_string($query_string, $bh_keywords);
    }

    $query_string =~ s/isDeleted:0\s+(AND|OR|NOT)\s+\(\s+(AND|OR|NOT)/isDeleted:0 (/; #cleanup

    #CV Updated

    my %cv_updated = ('TODAY' => '1', 'YESTERDAY' => '2', '3D' => '3', '1W' => '7', '2W' => '14', '1M' => '30', '2M' => '60', '3M' => '91', '6M' => '183', '1Y' => '365', '2Y' => '730', '3Y' => '1095', 'ALL' => '');

    if ($self->token_value('cv_updated_within') ne 'ALL') {

        my $from = $self->token_value('updated_after'); $from =~ s/-//g; $from = substr($from,0,8);
        $from ||= '*';
        my $to = strftime("%Y%m%d", localtime(time));

        $query_string .= " AND dateLastModified:" . "[$from TO $to]";

    }

    #Custom fields

    my @fields = @BULLHORN_TOKENS;
    foreach my $fieldname (@fields) {

        my $value = $self->token_value($fieldname);
        if ($value) {

            if($fieldname eq 'experience') {

                $query_string .= " AND $fieldname:{$value TO 999999999}";

            }
            elsif($fieldname eq 'travelLimit') {

                $query_string .= " AND $fieldname:[$value TO 999999999]";

            }
            elsif($fieldname =~ m/date/i) {
                $query_string .= " AND $fieldname:$value";
            }
            else{
                my $multi_vals = [$self->token_value($fieldname)];
                $fieldname =~ s/_/\./; # having a hash key with '.' doesn't work for me
                $query_string .= " AND $fieldname:(";
                foreach my $val (@$multi_vals) {
                    $query_string .= "+" . lc($val) . ' ';
                }
                chop($query_string);
                $query_string .= ")";
            }
        }
    }

    if ($self->token_value('default_jobtype')){

        my $jobtype = $self->token_value('default_jobtype');

        $jobtype = "permanent" if ($jobtype eq 'permanent');

        $jobtype = "contract" if ($jobtype eq 'contract');

        $jobtype = "contract to hire" if ($jobtype eq 'temporary');

        $query_string .= " AND employmentPreference:(+$jobtype)";

    }

    #Location
    my $location_string = $self->token_value('populate_bullhorn_location');

    $query_string .= $location_string if $location_string;

    #Salary
    #left out for now as no results returned from API with salary set
#    if ($self->token_value('salary_from') && $self->token_value('salary_to')) {
#        my @salaries = ($self->token_value('salary_from'), $self->token_value('salary_to'));
#        my @fields = qw/salaryLow salary/; #annum
#        @fields = qw/hourlyRateLow hourlyRate/ if $self->token_value('salary_per') eq 'hour';
#        @fields = qw/dayRateLow dayRate/ if $self->token_value('salary_per') eq 'day';
#
#        for (my $i = 0; $i <= $#fields; $i++) {
#            $query_string .= " AND $fields[$i]:{$salaries[$i] TO 999999999}";
#        }
#    }

   $query_string .= " AND NOT status:Archive";

   $self->log_debug("Query string is " . $query_string);

   return $query_string;

}

sub fake_numeric_range {
# Bullhorn supports lucene searching which is lexographical not numerical ranges.
# Can fake a numeric range by doing several lexographic ranges.
# This isn't currently used as I couldn't get it to work on bullhorns site.
    my $self = shift;
    my $from = int(shift);
    my $to = int(shift);
    my $field = shift;

    my $query_string = '';
    if (length($from) == length($to)) {
        $query_string .= " AND $field:[$from TO $to]";
    } else {
        $query_string .= " AND ($field:[$from TO " . ("9" x length($from)) . "]";
        # Lower bound, ie if from is 55, then search between 55 and 99.

        foreach my $length ((length($from) + 1) .. (length($to) - 1)) {
            $query_string .= " OR $field:[1" . ("0" x ($length - 1)) . " TO " . ("9" x $length) . "]";
        }
        # Ranges in the middle ie. 100-999, 1000-9999, 10000-99999

        $query_string .= " OR $field:[1" . "0" x (length($to) - 1) . " TO " . $to . "])";
        # Upper bound, ie if from is 453524, then search between 100000 and 453524.
    }
    return $query_string;
}

sub search_success {
    return qr{\"total\"\s*\:\s*\d+};
}

sub scrape_paginator {
    my $self = shift;
    my $json = $self->response_to_JSON();
    my $no_of_results = $json->{'total'};
    $self->log_info("Total number of result is $no_of_results");
    $self->total_results($no_of_results);
    my $no_of_pages = ceil ($no_of_results/$self->results_per_page());
    $self->max_pages($no_of_pages);
}

sub scrape_candidate_array {
    my $self = shift;
    my $json = $self->response_to_JSON();

    return @{$json->{'data'}};
}

sub scrape_candidate_details {
    my $self = shift;
    my $json = shift;
    $json->{'cand_id'} = $json->{'id'}; #change id attribute as it messes with our system

    delete ($json->{'id'});

    while (my ($key, $value) = each %$json) {
        if ($key =~ /Date|^date/ && ( defined($value) && $value > 0 )) {
            $json->{$key} = strftime("%e %h %Y", localtime($value/1000));
        }
        if(defined($value) && $value eq "NULL"){
            $json->{$key} = ''; #set null values to blank
        }
        if(($key =~ /willRelocate/) && ($value eq 'true' || $value eq 'false' ) ){ 
            if ($value eq 'false'){ $json->{$key} = 'No'; }
            else{ $json->{$key} = 'Yes'; }

         }
    }

    return %$json;
}

sub bullhorn_location {
    my $self = shift;
    my $location_id = shift or return;

    my $location = $self->location_api()->find({ id => $location_id });

    my $location_string = ' AND ';
    my $distance;

    if ( my $within_kms = $self->token_value('location_within_km') ) {
        $distance = $within_kms * 0.621371;
    } else {
        $distance = $self->token_value('location_within_miles');
    }

    if ( $distance ) {
        if ( my ($min_lat, $max_lat, $min_long, $max_long) = $self->bullhorn_extend_bounding_box($location, $distance) ) {
            $location_string .= "(address.latitude:[$min_lat TO $max_lat] AND address.longitude:[\\$max_long TO \\$min_long])";
        }
        else {
            $self->log_warn('Received no bounding coordinates for location_id %s', $location_id);
            return;
        }
    }
    else {
        my $zip = $location->zipcode();
        my $city = $location->is_place() ? $location->name() : '';
        my $state_iso = $location->get_mapping({ board => 'ISO_STATES' });
        my $state = $location->county()->name();
        my $country = $location->country()->name();
        $country = 'United States' if $country eq 'USA';

        if ( $city && $zip ) {
            $location_string .= "address:((((address.city:\"$city\" AND (address.state:\"$state_iso\" OR address.state:\"$state\")) OR address.zip:\"$zip\")))";
        }
        elsif ( $city ) {
            $location_string .= "address:(((address.city:\"$city\" AND (address.state:\"$state_iso\" OR address.state:\"$state\"))))";
        }
        elsif( !$city && $state ) {
            $location_string .= "address:(((address.state:\"$state_iso\" OR address.state:\"$state\") AND address.country.name:\"$country\"))";
        }
    }

    return $location_string;
}

sub bullhorn_extend_bounding_box {
    my ($self, $location, $miles) = @_;

    my $s2 = $self->user_object()->stream2;
    my $geocoder = $s2->geocoder;

    my $location_text;
    if ( $location->is_place() ) {
        $location_text = sprintf('%s, %s, %s', $location->name(), $location->county()->name(), $location->iso_country());
    }
    elsif ( $location->is_county() ) {
        $location_text = sprintf('%s, %s', $location->name(), $location->iso_country());
    }
    elsif ( $location->is_country() ) {
        $location_text = $location->iso_country();
    }

    $self->log_debug('asking google for location data with text: %s', $location_text);

    my $coordinates = eval { $geocoder->geocode(location => $location_text); };
    if ( $@ ) {
        $self->log_warn('Error retrieving location coordinates from google: %s', $@);
        $self->results()->add_notice(
            'Unable to fetch location data temporarily, location will be excluded from this query. Please try again later'
        );
        return;
    }

    $self->log_info(
        'Will calculate bounds using these lat/long coordinates from google: lat=[%s] long=[%s]',
        $coordinates->{geometry}->{location}->{lat},
        $coordinates->{geometry}->{location}->{lng},
    );

    my $bounds = $coordinates->{geometry}->{bounds};

    # each degree is 69 miles apart, but we want to determine our own miles using this distance
    my $distance_between_degrees = 1 / 69;

    return (
        $bounds->{southwest}->{lat} - ($miles * $distance_between_degrees), # min
        $bounds->{northeast}->{lat} + ($miles * $distance_between_degrees), # max

        # negatives - do the opposite
        $bounds->{northeast}->{lng} + ($miles * $distance_between_degrees), # min
        $bounds->{southwest}->{lng} - ($miles * $distance_between_degrees), # max
    );
}

#sub bullhorn_convert_currency {
#    my $self = shift;
#    my $selected_currency = shift;
#
#    my (@locale_currencies) = $self->user_api()->currency_order($self->user_object->adcourier_user_id());
#    my $bullhorn_currency = (shift @locale_currencies) || 'GBP';
#
#    my $currency = Stream::Token->new({
#       Type => 'Currency',
#       Options => $bullhorn_currency,
#        Value   => $selected_currency,
#    });
#
#    # convert and set the salary to and from
#    foreach my $salary_token ( qw/salary_from salary_to/ ) {
#        my $current_salary = $self->token_value( $salary_token );
#        if ( $current_salary ) {
#           $self->token_value(
#               $salary_token => $currency->convert( $current_salary ),
#           );
#        }
#    }
#
#    return $bullhorn_currency;
#}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    $candidate->attr('block_bulk_message' => 1 ); #SEAR-781

    my $candidateheadline = '';
    my $company_name;
#the companyName value keeps switching between array and string!
        $self->log_debug("company name data type is: " . ref($candidate->attr('companyName')));


    my $company = $candidate->attr('companyName'); 
    $company_name = (ref($candidate->attr('companyName')) eq 'ARRAY') ? $company->[0] : $company;


    if($candidate->attr('occupation')){
        $candidateheadline = $candidate->attr('occupation');
        if($candidate->attr('companyName')){
            $candidateheadline .= " at " . $company_name;
        }
    }
    elsif($candidate->attr('companyName')){
        $candidateheadline = $company_name;
    }
   #some are arrays and some are strings for some reason
    if ( ref($candidate->attr('skillSet')) eq "ARRAY" ){
        $self->log_debug('skillSet is an array, changing to string');
        $candidate->attr( 'skillSet' =>  join(',',$candidate->attr('skillSet')) );
    }



    if( my $description = $candidate->attr('description') ){
        $description =~ s/<b>/<strong>/g;
        $description =~ s/<\/b>/<\/strong>/g;

        $candidate->attr( "description" => $description );
    }

    ## This will replace the email as a sub headline, because we have
    ## a name
    $candidate->attr('headline' => $candidateheadline );

    $candidate->attr('newheadline' => $candidateheadline);
    $candidate->attr('candidate_id' => $candidate->attr('cand_id'));
    $candidate->attr('user' => $candidate->attr('username'));
    $candidate->attr('score' => $candidate->attr('_score'));

    # Note the bullhornStaffingHost comes from before the search scraping.
    $candidate->attr("profile_link" => "https://" .$self->bullhornStaffingHost(). "/BullhornStaffing/OpenWindow.cfm?Entity=Candidate&id=" . $candidate->candidate_id() . "&view=Overview" );

    #$resp = $s

    $candidate->has_profile(1);
    # A bullhorn candidate always have a cv
    # even if its a fake one.
    $candidate->has_cv(1);
    # $candidate->has_cv(0);
    return $candidate;
}

sub download_profile {
    return 1;
}

sub download_cv{
    my ($self, $candidate) = @_;

    my $response = HTTP::Response->new(200);
    $response->message("Placeholder document");
    $response->header('Content-Type' => 'text/html; charset=UTF-8');
    $response->header('Content-Disposition' => 'attachment; filename=Placeholder_'.$candidate->candidate_id().'.html');

    ## Build a nice piece of text
    ## burning glass can understand.
    my $html = '<html><body>Placeholder</body></html>';
    $response->content(Encode::encode_utf8($html));
    return $response;
}

sub download_cv_url {
}

sub download_cv_fields {
    return (
    );
}

sub download_cv_set_headers {
    return ('Accept' => 'application/json');
}

sub download_cv_success {
    return 'fileName';
}

sub scrape_download_cv_details {
    my $self = shift;
    my $json = shift;

#    my $resume_type = $json->{'cvFile'}->{'transform'} eq 'base64' ? 'resume_word_base64' : 'resume_text';

 #   return (
 #       $resume_type => $json->{'cvFile'}->{'content'},
 #       filename => $json->{'cvFile'}->{'fileName'},
 #   );
}

sub scrape_candidate_headline{
    return qw/newheadline/;
}

=head2 bullhorn_custom_facets

    Clients have their own custom fields on the board, this will call
    the appropriate URI to get these and build facets.

=cut
sub bullhorn_custom_facets {
    my $self = shift;
    my $meta_url = shift;

    $self->get($meta_url, $self->set_auth_headers());
    my $json = $self->response_to_JSON();

    unless ( $self->account_value('client_facets') ) {
        $self->log_info('this search request builds the facets, might take long to fetch the values');
        if ( $json->{'fields'} ) {
            $self->_bullhorn_build_facets(@{$json->{'fields'}});
        }
    }
    $self->results()->facets($self->account_value('client_facets'));
}

sub _bullhorn_build_facets {
    my $self = shift;
    my @fields = @_;
    my @facets = ();
    my @token_names = ();
    my $requests = {};
    my @async_option_requests = ();
    my $sort_metric = 5;

    FIELD:
    foreach my $field ( @fields ) {
        next if ( !$field ||
                    ( defined $field->{'dataType'} && $field->{'dataType'} =~ m/Address/ ) ||
                    $field->{'name'} =~ m/\A(?:id|salary|salaryLow|salary|countryID|zip|hourlyRate|hourlyRateLow|dayRate|dayRateLow|password|dateLastModified|employmentPreference|linkedPerson|referredByPerson|clientCorporationBlackList|clientCorporationWhiteList|categories|isDeleted|gender|federalAddtionalWitholdingsAmount|stateAddtionalWitholdingsAmount|localAddtionalWitholdingsAmount)\z/ ||
                    $field->{'hideFromSearch'} || # bullhorn doesn't know what this field is
                    $field->{'confidential'} || # confidential fields are returned to us with the taxonomy but we are to ignore it...
                    $field->{'readOnly'} # fields which are marked as hidden on bullhorn
                );

        # additional fields to remove based on label (because that's how they told us)
        foreach my $field_to_skip ( @FIELDS_TO_SKIP ) {
            next FIELD if $field->{label} =~ m/$field_to_skip/i;
        }

        $sort_metric++;
        my $facet_item = {
            Name    =>  $field->{'name'},
            Label   =>  $field->{'label'},
            SortMetric => $sort_metric,
            ( $field->{maxLength} ? ( Size => $field->{maxLength} ) : () )
        };

        # Validation
        if ( $field->{'name'} =~ m/\A(?:workPhone)\z/ ) {
            $facet_item->{'Validation'}  = 'ForceNumeric';
        }

        if ( defined $field->{'dataType'} ) {
            # Boolean fields
            if ( $field->{'dataType'} eq 'Boolean' ) {
                $facet_item->{'Type'} = 'list';
                $facet_item->{'Options'} =  [ ['Not Specified',''], ['True','true'], ['False','false'] ];
            }

            # Text List Fields
            elsif ( $field->{'dataType'} eq 'String' && $field->{'options'} ) {
                my @options = @{$field->{'options'}};
                $facet_item->{'Type'} = 'list';
                $facet_item->{'Options'} = [ map {
                    [ $_->{label}, $_->{value} ]
                } ( { label => 'Not Specified', value => '' }, @{$field->{options}} ) ] || [];
            }

            # Plain Text
            elsif ( $field->{'dataType'} =~ m/\A(?:Integer|BigDecimal|Double|String)\z/ ) {
                $facet_item->{'Type'} = 'text';
            }

            # Time Stamp
            elsif ( $field->{'dataType'} eq 'Timestamp' ) {
                my @timestamps = ();
                my $now = DateTime->now();
                foreach my $day (qw/1 7 14 21 30 60 90 180 270 365/) {
                    my $ts = {
                        label   =>  $day . ' days',
                        value   =>  $now->clone->subtract( days => $day )->strftime('%Y%m%d'),
                    };
                    push(@timestamps, $ts);
                }
                $self->log_debug('timestamps' . Dumper(@timestamps));
                $facet_item->{'Type'} = 'list';
                $facet_item->{'Options'} = [ map {
                    [ $_->{label}, '[' . $_->{value} . ' TO ' . $now->strftime('%Y%m%d') . '235959' . ']' ]
                } ( { label => 'Not Specified', value => '' }, @timestamps ) ] || [];
            }
        }
            #------------
            # Fail Castle >
            #------------
            #
        ##########
        # OH NO! #
        #        # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
        # Large lists of enumerated types which require further fetching #
        # on our part to retrieve the available options                  #
        # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # ##
        elsif ( !$field->{'dataType'} && $field->{'type'} =~ m/\A(?:TO_ONE|TO_MANY)\z/ ) {
            # Only make requests for entities that we know return values
            next if ( $field->{'optionsType'} !~ m/\A(?:Appointment|BusinessSector|Category|CorporateUser|Placement|Skill|Speciality)\z/ );

            $facet_item->{'Name'} = $field->{'name'} . '_' . $field->{'associatedEntity'}->{'fields'}->[0]->{'name'};
            $facet_item->{'Type'} = 'multilist';

            foreach my $start ( qw/0 300 600 900 1200/ ) {
                push @async_option_requests, {
                    url => sprintf( $field->{'optionsUrl'} . '?start=%s&count=300&BhRestToken=%s', $start, $self->bullhorn_session() ),
                    cb => sub {
                        my $data_ref = JSON::decode_json( shift );
                        my $options = $data_ref->{data}
                            or return;

                        $facet_item->{Options} //= [];
                        push @{$facet_item->{Options}}, grep { $_->[0] } map {
                            $self->log_info( $_->{label} );
                            [ $_->{label}, $_->{value} ]
                        } ( { label => 'Not Specified', value => '' }, @{$options} );
                    }
                }
            }
        }

        if ( $facet_item ) {
            push(@token_names, $facet_item->{'Name'});
            push(@facets, $facet_item);
        }

    }

    # Asynchronous url getting to save time on fetching and parsing,
    # to please bullhorn clients who have 100+ facets
    my $mua = Mojo::UserAgent->new;
    Mojo::IOLoop->delay(
        sub {
            my ( $delay ) = @_;
            foreach my $async_request ( @async_option_requests ){
                $mua->get( $async_request->{url} => $delay->begin );
            }
        },
        sub {
            my $delay = shift;
            foreach my $async_request ( @async_option_requests ){
                my $response = shift;
                eval {
                    $async_request->{cb}->( $response->res->body );
                };
                if ( $@ ) {
                    $self->log_error( $@ );
                }
            }}
    )->wait;

    $self->log_debug('facets are' . Dumper(@facets) . 'tokens are' . Dumper(@token_names));
    $self->_bullhorn_token_names(@token_names); # to be able to dynamically build custom fields in search_query
    $self->set_account_value('client_facets', \@facets, 86400); # store facets for a day
}

sub _bullhorn_token_names {
    my $self = shift;
    my @tokens = @_;
    @BULLHORN_TOKENS = @tokens;
}

sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    if ( !defined( $content ) ) {
        $content = $self->content();

        return unless $content;
    }
}

$standard_tokens{sort} = {

  Type          =>  'List',

  Label         =>  'Sort results by',

  Values        =>  '(Name=name|Current Company=companyName|Job Title=occupation|Status=status|Source=source|Last Note=dateLastComment|Date Added=-dateAdded|CV Updated=dateLastModified|Occupation=occupation|Postcode/Zipcode=zip)',

  Default       =>  '-dateAdded',

  SortMetric    =>  1,

};

#simon return array instead of joining them
sub SPLIT_KEYWORDS_ARRAY {
    my $self = shift;
    my $keywords = shift;
    my $and_token = shift;
    my $or_token  = shift;
    my $not_token = shift;
    my $exact_token = shift;

    $self->log_debug('Splitting keywords: [%s]', $keywords);

    my $q = Stream::Keywords->new( $keywords );

    if ( !$q->process_query() ) {
        $self->log_debug('Unable to split up the keywords');
        $self->throw_invalid_boolean_search('We could not process the query');
    }

    $self->log_debug( $q->diag() );

    my (@and_words)   = $q->and_words();
    $self->log_debug('AND WORDS: [%s]', \@and_words );
    $self->token_value( $and_token   => \@and_words );

    my (@or_words)    = $q->or_words();
    $self->log_debug('OR WORDS: [%s]', \@or_words );
    $self->token_value( $or_token    => \@or_words );

    my (@not_words)   = $q->not_words();
    $self->log_debug('NOT WORDS: [%s]', \@not_words );
    $self->token_value( $not_token   => \@not_words );

    my (@exact_words) = $q->exact_words();
    $self->log_debug('PHRASES: [%s]', \@exact_words );
    $self->token_value( $exact_token => \@exact_words );

    return;
}

1;

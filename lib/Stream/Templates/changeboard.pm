package Stream::Templates::changeboard;

use strict;
use warnings;

use base qw(Stream::Templates::CrawledFeed);

use Stream::Constants::SortMetrics qw(:all);

__PACKAGE__->load_tokens_from_config();

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

use URI;

%authtokens = (
    changeboard_username => {
        Label      => 'Username',
        Type       => 'Text',
        Mandatory  => 1,
        SortMetric => 1,
    },
    changeboard_password => {
        Label      => 'Password',
        Type       => 'Text',
        Mandatory  => 1,
        SortMetric => 2,
    }
);

$derived_tokens{salary} = {
    Label        => 'Salary',
    Type         => 'SalaryMultiValue',
    GBP_annum    => '(0-14999=1|15000-24999=22|25000-34999=23|35000-49999=24|50000-74999=25|75000-99999=26|100000+=27)',
    GBP_day      => '(0-80=28|80-120=29|120-160=30|160-200=31|200-240=32|240-280=33|280-320=34|320-360=35|360-400=36|400-480=37|480-560=38|560+=39)',
    GBP_hour     => '(0-10=28|10-15=29|15-20=30|20-25=31|25-30=32|30-35=33|35-40=34|40-45=35|45-50=36|50-60=37|60-70=38|70+=39|)',
    Helper       => 'SALARY_BANDS_BETWEEN(salary|%salary_from%|%salary_to%|%salary_cur%|%salary_per%)',
    HelperMetric => 3,
};

sub base_url {
    my $self = shift;
    return $self->config->base_url;
}

# Logs in and switches from the board-side user account to
# the company/organisation which has search access.
sub login_url {
    my $self = shift;

    my $login_url = $self->config->get_url('login_form');
    return $login_url;
}

sub login {
    my ($self) = @_;
    my %login_fields = $self->login_fields;

    $self->get($self->login_url);

    $self->form_with_fields(keys %login_fields);
    $self->fill_form(%login_fields);

    $self->submit;

    $self->follow_link(
        url_regex => $self->config->get_regex('account_switch_url')
    ) or die('No account switch link');

    $self->check_for_login_error();
}

sub login_fields {
    my $self = shift;
    return (
        Email    => $self->get_token_value('username'),
        Password => $self->get_token_value('password')
    );
}

sub login_form_with_fields {
    my $self = shift;
    my %credentials = $self->login_fields;
    return keys %credentials;
}

sub check_for_login_error {
    my ($self) = @_;

    # We should have been redirected to "organisation overview"
    my $path = $self->response->request->url->path;
    my $overview_path = $self->config->get_url('organisation_overview')->path;

    unless($path eq $overview_path) {
        $self->throw_login_error(
            $self->user_object->stream2->__('Unable to login')
        );
    }
}

sub search_submit {
    my $self = shift;
    my $search_url = $self->config->get_url('search_form_action');
    $search_url->query_form(
        $self->search_fields()
    );
    $self->get($search_url);
}

sub search_fields {
    my $self = shift;

    my @salary_ids = $self->get_token_value('salary');
    $self->log_debug('Salary IDs: %s', join(', ', @salary_ids));

    my %fields = (
        page              => $self->current_page // '',
        SearchKeywords    => $self->get_token_value('keywords') // '',
        SalaryBandIds     => [$self->get_token_value('salary')],

        #Crawled tokens
        SpecialisationIDs => [$self->get_token_value('sectors')],
        JobHoursIds       => [$self->get_token_value('hours')],
    );

    if ( my $location_id = $self->token_value('location_id') ){
        $fields{LocationIds} = $self->BEST_LOCATION_MAPPING(
            'stream_changeboard_new',
            $location_id,
            $self->token_value('location_within')
        );
    }

    if(my $cv_age = $self->get_token_value('cv_updated_within')) {
        $cv_age = $self->CV_UPDATED_WITHIN_TO_DAYS($cv_age);
        $fields{CVAge} = $cv_age;
    }

    if(my $jobtype = $self->get_token_value('default_jobtype')) {
        my $contract_map = $self->config->get_value('contract_map');
        $fields{JobContractIds} = $contract_map->{$jobtype};
    }

    return %fields;
}

sub scrape_candidate_xpath {
    my $self = shift;
    return $self->config->get_xpath('candidate_result');
}

sub scrape_candidate_details {
    my $self = shift;

    my %additional_fields;
    foreach my $additional_field_label ( @{$self->config->get_value('candidate_additional_field_labels')} ) {
        $additional_fields{$additional_field_label} = sprintf(
            $self->config->get_value('candidate_additional_field_content_format'),
            $additional_field_label
        );
    }

    return (
        name     => $self->config->get_xpath('candidate_name'),
        location => $self->config->get_xpath('candidate_location'),
        summary  => $self->config->get_xpath('candidate_summary'),
        cv_link  => $self->config->get_xpath('candidate_cv_link'),
        additional_fields => [
            $self->config->get_xpath('candidate_additional_field_container') => \%additional_fields
        ]
    );
}

sub scrape_fixup_candidate {
    my ($self, $candidate) = @_;

    my $id_regex = $self->config->get_regex('candidate_cv_link_id');
    $candidate->attr('cv_link') =~ $id_regex;
    $self->log_debug('Found Candidate ID: %s', $1);
    $candidate->candidate_id($1);
    $candidate->has_cv(1);

    return $candidate;
}

sub search_number_of_pages_xpath {
    return '//ul[@class="pagination__pages"]/li[last()]/a/@href';
}

sub search_number_of_pages_regex {
    return 'page=(\d+)';
}

sub before_downloading_cv {
    my $self = shift;
    $self->login();
}

sub do_download_cv {
    my ($self, $candidate) = @_;
    my $response = $self->get(
        sprintf(
            $self->config->get_value('cv_download_format'),
            $candidate->candidate_id
        )
    );

    my $file_download_test = qr/\A\/_assets/;
    # Already purchased CVs will have redirected use to withing
    # this path for the download
    if($response->request->url->path =~ $file_download_test) {
        return $response;
    }

    $self->recent_content($response->decoded_content);
    my $purchase_href = $self->findvalue( $self->config->get_xpath('cv_purchase_href') );
    $self->log_debug('Purchase href: %s', $purchase_href);
    $response = $self->get( $purchase_href );

    # Confirms we got a CV download.
    if($response->request->url->path =~ $file_download_test) {
        return $response;
    }
    $self->throw_cv_unavailable('Could not download CV');

}

# This will give the value they show on the site
# but that value is off. Board issue ): .
sub search_number_of_results_regex {
    my $self = shift;
    return $self->config->get_regex('number_of_results');
}

1;
package Stream::Templates::jobscentral;

use strict;
use warnings;

use base qw(Stream::Templates::careerbuilder);
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

BEGIN {
    __PACKAGE__->inherit_tokens();

    # Deleting some custom fields which are US specific / not available on board

    delete $authtokens{careerbuilder_email};
    delete $authtokens{careerbuilder_password};

    delete $standard_tokens{work_status};
    delete $standard_tokens{minimum_travel_requirements};
    delete $standard_tokens{currently_employed};
    delete $standard_tokens{company};
    delete $standard_tokens{category};
    delete $standard_tokens{minimum_degree};
    delete $standard_tokens{military_experience};
    delete $standard_tokens{security_clearance};
    delete $standard_tokens{relocation_filter};
    delete $standard_tokens{work_status};

    delete $posting_only_tokens{military_experience_csv};
    delete $posting_only_tokens{categories};

    $authtokens{jobscentral_email} = {
        Type        =>  'Text',
        Label       =>  'Email address',
        SortMetric  =>  1,
        Mandatory   =>  1,
    };

    $authtokens{jobscentral_password} = {
        Type        =>  'Text',
        Label       =>  'Password',
        SortMetric  =>  1,
        Mandatory   =>  1,
    };

    $derived_tokens{salary_cur} = {
        Type         => 'Currency',
        Options      => 'SGD',
        Default      => 'SGD',
        Helper       => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
        HelperMetric => 1,
    };

    $derived_tokens{salary_per} = {
        Type         => 'SalaryPer',
        Options      => 'annum|hour',
        Default      => 'annum',
        Helper       => 'SALARY_PER(salary_per|salary_from|salary_to)',
        HelperMetric => 2,
    };
};

# ---------------------------
# Test credentials
# username: aliz@broadbean.com
# password: Career2015
# ---------------------------

sub careerbuilder_default_country_code {
    my $self = shift;
    if ( $self->token_value('location_id') ) {
        return 'SG';
    }
    else { return ''; }
}

1;

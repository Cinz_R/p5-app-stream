#!/usr/bin/env perl

# $Id: Mock.pm 19466 2009-05-15 16:21:45Z andy $

package Stream::Templates::Tests::ThrowErrorDuringSearch;

use strict;

use base qw(Stream::Engine::Robot);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################

############### HELPERS ##########################


############### LOGIN ############################
sub search_navigate_to_page {
    my $self = shift;

    return $self->throw_error('BBTECH_ERROR:LoginFailed(Missing username)');
}

1;

# vim: expandtab shiftwidth=4

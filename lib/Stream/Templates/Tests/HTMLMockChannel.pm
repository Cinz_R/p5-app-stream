#!/usr/bin/env perl

# $Id: Mock.pm 19466 2009-05-15 16:21:45Z andy $

package Stream::Templates::Tests::HTMLMockChannel;

use strict;

use Stream::Constants::SortMetrics ':all';

use base qw(Stream::Engine::Robot);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################
BEGIN {
    $standard_tokens{state} = {
        Label => 'State',
        Type  => 'Text',
        Size  => 30,
        SortMetric => $SM_LOCATION,
    };

    $standard_tokens{max_results} = {
        Label   => 'Number of Results',
        Type    => 'List',
        Values  => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
    };
};

############### HELPERS ##########################

############### SEARCH ############################

sub search_form_name {
    my $self = shift;
    $self->log_debug('search_form_name was called which is good');
    return 'next_page';
}

sub search_fields {
    return (
        state => '%state%',
    );
}

sub search_success {
    return qr/Found (\d+) results/;
}

sub scrape_candidate_xpath {
    return '//tr[contains(@class,"result")]';
}

sub scrape_candidate_details {
    return (
        row_id    => './@id',
        firstname => './td[2]',
        lastname  => './td[3]',
    );
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    my $row_id = $candidate->attr('row_id');
    $row_id =~ s/^result_//;

    $self->log_debug('Setting candidate id to [%s]', $row_id);

    $candidate->candidate_id( $row_id );

    return $candidate;
}

1;

# vim: expandtab shiftwidth=4

#!/usr/bin/env perl

# $Id: Mock.pm 19466 2009-05-15 16:21:45Z andy $

package Stream::Templates::Tests::HTMLNextLink;

use strict;

use base qw(Stream::Templates::Tests::HTMLMockChannel);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################
BEGIN {
    __PACKAGE__->inherit_tokens();
};

############### HELPERS ##########################

############### SEARCH ############################

sub candidates_required {
    return 11;
}

sub search_url {
    return 'http://cheerleader2.gs.adcourier.com/cvsearch/mock-channel/html-next-link.cgi';
}

# We could work out the number of pages - the point of this test is we don't have to
#sub search_number_of_pages_xpath {
#    return '';
#}
#
#sub search_number_of_results_regex {
#
#}

sub search_find_next_page_link {
    return (
        class_regex => qr/\bnext_page\b/,
    );
}

1;

# vim: expandtab shiftwidth=4

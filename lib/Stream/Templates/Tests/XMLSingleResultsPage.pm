#!/usr/bin/env perl

# $Id: Mock.pm 19466 2009-05-15 16:21:45Z andy $

package Stream::Templates::Tests::XMLSingleResultsPage;

use strict;

use base qw(Stream::Templates::Tests::XMLMockChannel);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################
BEGIN {
    __PACKAGE__->inherit_tokens();
};

############### HELPERS ##########################

############### SEARCH ############################

sub candidates_required {
    return 11;
}

sub search_url {
    return 'http://cheerleader2.gs.adcourier.com/cvsearch/mock-channel/single-page-of-results.cgi';
}

1;

# vim: expandtab shiftwidth=4

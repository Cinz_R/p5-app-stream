#!/usr/bin/env perl

# $Id: Mock.pm 19466 2009-05-15 16:21:45Z andy $

package Stream::Templates::Tests::XMLMockChannel;

use strict;

use Stream::Constants::SortMetrics ':all';

use base qw(Stream::Engine::API::XML);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################
BEGIN {
    $standard_tokens{state} = {
        Label => 'State',
        Type  => 'Text',
        Size  => 30,
        SortMetric => $SM_LOCATION,
    };

    $standard_tokens{max_results} = {
        Label   => 'Number of Results',
        Type    => 'List',
        Values  => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
    };
};

############### HELPERS ##########################

############### SEARCH ############################

sub search_submit {
    my $self = shift;

    my $search_uri = $self->mock_search_uri();
    return $self->search_get(
        $search_uri,
    );
}

sub mock_search_uri {
    my $self = shift;

    my %query_form = (
        state => $self->token_value('state') || '',
        page  => $self->current_page()       || 1,
    );

    my $search_uri = URI->new( $self->search_url() );
    $search_uri->query_form( \%query_form );

    return $search_uri;
}

sub search_success {
    return qr/<results/;
}

sub scrape_candidate_xpath {
    return '//results/result';
}

sub scrape_candidate_details {
    return (
        candidate_id => './id',
        firstname    => './first_name',
        lastname     => './last_name',
        phone        => './phone',
        snippet      => './snippet',
    );
}

1;

# vim: expandtab shiftwidth=4

package Stream::Templates::Tests::XMLNumberOfPages;

use strict;

use base qw(Stream::Templates::Tests::XMLMockChannel);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################
BEGIN {
    __PACKAGE__->inherit_tokens();
};

############### HELPERS ##########################

############### SEARCH ############################

sub candidates_required {
    return 11;
}

sub search_url {
    return 'http://cheerleader2.gs.adcourier.com/cvsearch/mock-channel/xml-number-of-pages.cgi';
}

# We could work out the number of pages - the point of this test is we don't have to
sub search_number_of_pages_xpath {
    return '//max_pages';
}

1;

# vim: expandtab shiftwidth=4

#!/usr/bin/env perl

# $Id: Mock.pm 19466 2009-05-15 16:21:45Z andy $

package Stream::Templates::Tests::MaxResults;

use strict;

use base qw(Stream::Engine::Robot);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################
BEGIN {
    $standard_tokens{max_results} = {
        Label   => 'Number of Results',
        Type    => 'List',
        Values  => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
    };
};


############### HELPERS ##########################


############### LOGIN ############################

1;

# vim: expandtab shiftwidth=4

#!/usr/bin/perl

# $Id: gojobsite.pm 96362 2013-03-28 10:52:11Z neil $

package Stream::Templates::gojobsite;

=head1 NAME

Stream::Templates::gojobsite - Jobsite Search Agent

=head1 AUTHOR

ACJ - 23/06/2009

=head1 CONTACT

Ian Young <ian.young@jobsite.co.uk>
Software Engineer

=head1 TESTING INFORMATION

None

=head1 DOCUMENTATION

Web Service URL
L<http://www.gojobsite.com/cgi-bin/cv_search_webservice.cgi>

Documentation
L<http://trac/adcourier/wiki/MultipleCvSearch>

=head1 OVERVIEW

See Stream::Templates::gojobsite

=head1 MAJOR CHANGES

none yet

=head1 BUG FIXES

=over

=item

Need to send both reqdSalaryLow & reqdSalaryHigh together
if either has a value

=back

=cut

use strict;

use utf8;

use Stream::Constants::SortMetrics ':all';
use base qw(Stream::Templates::Jobsite);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################

BEGIN {
    __PACKAGE__->inherit_tokens();
};

=head1 TOKENS

=head2 STANDARD TOKENS

Inherited from Jobsite

=head3 Search Site (%cvsearch_site%)

Can be any combination of:

=cut


%authtokens = (
    gojobsite_agency_id => {
        Label => 'Agency ID',
        Type  => 'Text',
        Size  => 10,
        SortMetric => 1,
        Mandatory  => 1,
        Validation => 'ForceNumeric',
    },
    gojobsite_email => {
        Label => 'Email Address',
        Type  => 'Text',
        Size  => 40,
        SortMetric => 2,
        Mandatory  => 1,
        Validation => 'Email',
    },
);

sub requires_cv_unlock {
    return 1;
}

sub AGENT_TIMEOUT { 90 }

1;
# vim: expandtab tabstop=4 encoding=utf-8


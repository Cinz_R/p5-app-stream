package Stream::Templates::Default;
use warnings;
use strict;

use Carp;
use Clone 'clone';
use Scalar::Util qw(blessed);
use Stream2;
use Stream::EngineException; # Also contains all Exception subclasses

use Stream::Constants::SortMetrics ':all';
use Stream::Templates::Default::Helpers;
use Stream::Token;


use Log::Any qw/$log/;

use Data::Dumper;

my $MAX_CHECKBOX_OPTIONS = 4;
my $DEFAULT_SORTMETRIC   = 255;

our $VERSION = qw(0.01);

=head1 NAME

Stream::Templates::Default - The base class of all templates and intermediary helper classes.

=head2 new

Returns a new instance of this based on a tokens hash,
and possibly other attributes.

Usage:

   my $template_class = ... whatever ...

   my $feed = $template_class->new(); # Empty instance.
   my $feed = $template_class->new(\%tokens) ; # With Tokens.
   my $feed = $template_class->new(undef, \%attributes); # With other attributes.
   my $feed = $template_class->new(\%tokens, \%attributes); # With tokens AND other attributes.

=cut

sub new {
    my $class = shift;
    my $tokens_ref = shift;
    my $other_attributes = shift;
    $other_attributes //= {};

    my $self = bless { tokens => {},
                       %{$other_attributes} # Shallow copy
                     }, $class;

    $self->tokens( $tokens_ref ) if ( $tokens_ref );

    return $self;
}

sub parent {
    return $_[0]->{parent} if @_ == 1;
    return $_[0]->{parent} = $_[1];
}

sub company {
    return $_[0]->{company} if @_ == 1;
    return $_[0]->{company} = $_[1];
}

sub office {
    return $_[0]->{office} if @_ == 1;
    return $_[0]->{office} = $_[1];
}

sub team {
    return $_[0]->{team} if @_ == 1;
    return $_[0]->{team} = $_[1];
}

sub consultant {
    return $_[0]->{consultant} if @_ == 1;
    return $_[0]->{consultant} = $_[1];
}

=head2 location_api

An instance of L<Bean::Juice::APIClient::Location>

Not mandatory for now, but some templates might die.

=cut

sub location_api{
    my $self = shift;
    @_ ? $self->{'location_api'} = shift : $self->{location_api} ;
}

=head2 user_api

An instance of L<Bean::Juice::APIClient::User>

Not mandatory for now, but some templates might die.

=cut

sub user_api{
    my $self = shift;
    @_ ? $self->{'user_api'} = shift : $self->{user_api} ;
}

=head2 documents_api

An instance of L<Bean::Juice::APIClient::Documents>

Some templates might die if it's not there.

=cut

sub documents_api{
    my ($self) = @_;
    @_ ? $self->{'documents_api'} = shift : $self->{documents_api} ;
}

=head2 user_object

Returns the Stream::SearchUser that have been injected in this template as the
user_object attribute.

Will DIE if no such thing exists.

Usage:

  my $user_object = $this->user_object();

=cut

sub user_object{
    my ($self) = @_;
    return $self->{user_object} // confess("No user_object attribute in $self");
}

=head2 original_user

Returns the Stream2::O::SearchUser (if there is such a thing) that is the
original user (Understand the Overseer of the user_object).

Usage:

  if(   my $original_user = $this->original_user() ){
    ...
  }

=cut

sub original_user{
    my ($self) = @_;
    return $self->{original_user};
}

=head2 feed_config

Returns a hash ref of config data from conf/feeds.conf based on the feed name given.

Usage:

    my $config = $this->feed_config('dododoo');
    my $client_id = $config->{client_id};

=cut

sub feed_config{
    my ($self, $board) = @_;

    my $s2 = $self->user_object->stream2();
    return $s2->config()->{feeds}->{$board} // undef;
}

=head2 stream_action

Returns the Stream::Action being run

Usage:

    my $action = $this->stream_action;
    if($action) {
        ...
    }

=cut

sub stream_action {
    my ($self) = @_;
    return $self->{stream_action};
}

=head2 quota_guard

Runs the given code under a quota spending/refunding mechanism for the given type of quota.

Any crash in the sub will trigger some quota refund.

Usage:

   $this->quota_guard($type , sub{ ... } );

The type of quota should be an arrayref contianing at least one of: cv, profile, search, profile-or-cv.

=cut

sub quota_guard{
    my ($self, $types, $code ) = @_;

    $code //= 'UNDEFINED code';

    unless( grep { /^cv|profile|search|profile-or-cv$/ } @{ $types } ){
        confess("Invalid type provided. Please use cv, profile, search or profile-or-cv");
    }
    unless( $code && ref($code) eq 'CODE' ){
        confess("Invalid code '$code'. Please give a coderef");
    }

    my $user = $self->user_object();

    unless( $user->provider() =~ /^adcourier/ ){
        # Not an adcourier user, this quota_guard doesnt do nothing.
        return &$code();
    }

    my $s2 = Stream2->instance();

    my $stream_api = $s2->stream_api();

    # Try to spend a quota.
    my $destination = $self->destination();
    my @new_quota_txn_objects;

    foreach my $type ( @{ $types } ){
        $log->info("Attempting to spend a quota for '$type' on '$destination' for user ".$user->id());

        my $new_quota_txn_object;
        $log->tracef('Quota library: ACTION:deduct - PATH:%s - TYPE:%s - BOARD:%s', $user->hierarchical_path(), $type, $destination);
        $s2->report_tech(
            [
                {   path => $user->hierarchical_path(),
                    board => $destination,
                    type => sprintf( "search-%s", $type ),
                },
                {   actioned_by => $user->id,
                    remote_addr => $ENV{REMOTE_ADDR} // 'ADDR_UNKNOWN',
                }
            ],
            sub {
                eval{
                    push @new_quota_txn_objects, shift->quota_object->deduct( @_ );
                };
                if ( $@ ){
                    return undef if $@ =~ /NO_QUOTA_REMAINING/; #no crashes (and so emails) from no quota
                    die $@;
                }
            }
        );
    }

    # No quota left to perform this action.
    $self->_throw_quota_exceeded_error('QUOTA') unless @new_quota_txn_objects;

    my $ret = eval{
        &$code();
    };
    if( my $err = $@ ){
        foreach my $new_quota_txn_object ( @new_quota_txn_objects ){
            # Refund quota and re-throw the original error
            $log->infof('Quota library: ACTION:refund - PATH:%s - TYPE:%s - BOARD:%s - TXN:%s', $user->hierarchical_path(), $types, $destination, $new_quota_txn_object->id);
            my $new_refund_txn_ref = $s2->report_tech(
                [ $new_quota_txn_object , $user->id ],
                sub {
                    shift; # Skip stream2
                    shift->refund(shift);
                }
            );
        }
        die $err;
    }

    return $ret;
}

# Throws an adaptive quota exceeded message, dependent on the admins name, phone
# and email being present.

sub _throw_quota_exceeded_error {
    my ( $self, $prefix_log ) = @_;

    my $s2                   = Stream2->instance();
    my $user_contact_details = $self->user_object()->contact_details;
    my $admin_name           = $user_contact_details->{admin_name} // undef;
    my $admin_email          = $user_contact_details->{admin_email} // undef;
    my $admin_phone          = $user_contact_details->{admin_phone} // undef;

    $log->infof(
        "%sFailed to spend a quota on '%s' for user %s.",
        ( $prefix_log ? "$prefix_log: " : '' ),
        $self->destination(),
        $self->user_object()->id()
    );

    # THIS IS EVIL and maybe too clever... N=name E=email P=phone.
    # N E P
    # 1 1 1  Please contact your account administrator, N at E or on P.
    # 1 1 0  Please contact your account administrator, N at E.
    # 1 0 1  Please contact your account administrator, N on P.
    # 1 0 0  Please contact your account administrator, N.
    # 0 1 1  Please contact your account administrator, at E or on P.
    # 0 1 0  Please contact your account administrator, at E.
    # 0 0 1  Please contact your account administrator, on E.
    # 0 0 0  Please contact your account administrator.

    die Stream::EngineException::QuotaLimitExceeded->new({ message =>
        $s2->__("Please contact your account administrator") .
        (($admin_name || $admin_email || $admin_phone ) ? ',' : '' ).
        ($admin_name  ? sprintf(" %s",$admin_name) : '') .
        ($admin_email ? $s2->__x(" at {admin_email}",admin_email => $admin_email): '') .
        ($admin_email && $admin_phone  ? $s2->__(' or') : '') .
        ($admin_phone ? $s2->__x(" on {admin_phone}",admin_phone => $admin_phone): '') .
        '.'
    });
    return;
}

=head2 mark_candidate_contacted

Marks the given candidate as being contacted now. This is only
if the channel supports this kind of feature. This default does
nothing.

=cut

sub mark_candidate_contacted{
    my ($self, $candidate) = @_;
}


=head2 add_advert_identifier

Adds the advert_identifier against the given candidate
in this channel.

Usage:

 $this->add_candidate_advert_identifier($candidate, $identifier);

Returns the added identifier (so true) on success. Dies on error.

=cut

sub add_candidate_advert_identifier{
    my ($self, $candidate , $identifier) = @_;
    return $identifier;
}

=head2 tag_candidate

Stub method to send the newly created candidate tag to
the board.

Usage:

  my $tag = $self->tag_candidate($candidate, $new_tag);

=cut

sub tag_candidate{
    my ($self, $candidate, $tag) = @_;
    $log->debug('No specific tag_candidate implementation provided');
    return $tag;
}

=head2 untag_candidate

Removes the given tag from the given candidate.

This is a no-op if the given tag is not there against this candidate.

Usage:

  my $tag = $this->untag_candidate($some_results_result , $tag);

=cut

sub untag_candidate{
    my ($self, $candidate, $tag) = @_;
    $log->debug('No specific untag_candidate implementation provided');
    return $tag;
}

=head2 result_enhancement_details

Method that adds more information to the result snippet of the candidate.
Should return a hash reference of data that you want to attribute to
the candidate.

=cut

sub result_enhancement_details{ return {}; }

# object methods

=head2 tokens

Accessor for this object's tokens property.

=cut

sub tokens {
    my $self = shift;

    if ( @_ ) {
        $self->{tokens} = shift;
    }

    return $self->{tokens};
}

sub _cleanup_hashref {
    my $hashref = shift || return;

    foreach ( values %$hashref ) {
        if ( ref( $_ ) ) {
            if ( @$_ == 1 ) {
                $_ = shift @$_;
            }
        }
    }

    return $hashref;
}

=head2 token

WARNING

Wanna get the value of a token? DO - NOT - USE - THAT

Use token_value instead.

This thing is not really compatible with the fancy automatic yesbutno token
name prefixing magic.

=cut

sub token {
    my $self = shift;
    my $token = shift;

    if ( !$self->{token_cache} ) {
        $self->load_token_cache();
    }

    if ( exists( $self->{token_cache}->{ $token } ) ) {
        return $self->{token_cache}->{ $token };
    }

    return undef;
}

=head2 build_token

Builds a token object (not a token value) of class L<Stream::Token>

=cut

sub build_token {
    my $self = shift;
    my $token = shift;

    my $token_ref = ref( $token ) ? $token : $self->token( $token );

    if ( !$token_ref ) {
        return undef;
    }

    return $self->_build_token( $token_ref );
}

sub _build_token {
    my $self = shift;
    my $token_ref = shift;

    my %token = %$token_ref;
    if ( !exists( $token{value} ) ) {
        my (@values) = $self->token_value( $token{Name} );
        $token{ value } = @values  == 1 ? shift @values : \@values;
    }

    return Stream::Token->new( \%token );
}

sub token_value_or_default {
    my $self = shift;
    my $token_name = shift;

    my (@values) = $self->get_token_value( $token_name );

    if ( scalar(@values) ) {
        if ( @values == 1 ) {
            return shift @values;
        }
        return @values;
    }

    return $self->token_default( $token_name );
}

sub token_default {
    my $self = shift;
    my $token_name = shift;
    my $token = $self->build_token( $token_name );

    if ( !$token ) {
        $log->warn(sprintf('Unable to build token [%s] so could not find a default for it', $token_name));
        return;
    }

    return $token->default();
}

=head2 set_token_value

Sets the given token with the given value(s).

If the value is single, set the token to be a scalar. Otherwise,
set the token to be an ArrayRef of all values.

If no value is given, deletes the token from this.

Usage:

 $thos->set_token_value( 'scalar_token' => 1 );
 $this->set_token_value( 'multivalue_token', 1 , 2, 3 );
 $this->set_token_value( 'empty_token' );


=cut

sub set_token_value {
    my $self = shift;
    my $token = shift;

    my $tokens_ref = $self->{tokens};
    if ( @_ == 1 ) {
        ($tokens_ref->{$token}) = @_;
    }
    elsif ( @_ ) {
        $tokens_ref->{$token} = [ @_ ];
    }
    else {
        delete $tokens_ref->{ $token };
    }

    return;
}

=head2 token_value

Dispatch to set_token_value or get_token_value

=cut

sub token_value {
    my $self = shift;
    my $token = shift;

    if ( @_ ) {
        $self->set_token_value( $token, @_ );
    }

    return $self->get_token_value( $token );
}

=head2 get_token_value

Usage:

 my $single_value = $this->get_token_value('token_name');

 my @values = $this->get_token_value('token_name');

=cut

sub get_token_value {
    my $self  = shift;
    my $token = shift;

    my $tokens_ref = $self->tokens();

    my @token_list = $self->_potential_token_names( $token );

    TOKEN:
    foreach my $token_name ( @token_list ) {
        if ( !exists( $tokens_ref->{$token_name} ) ) {
            next TOKEN;
        }

        my $value = $tokens_ref->{$token_name};
        if ( ( ref( $value ) // '' ) eq 'ARRAY' ) {
            return wantarray ? @$value : $value->[0];
        }

        return $value;
    }

    $log->debug(sprintf('%s is empty and has not been created', $token ));

    return;
}


#
# Returns the board specific name of the given token plus the original name
# or just the original name if its already board specific
#
#
sub _potential_token_names {
    my $self = shift;
    my $token = shift;
    my $prefix = $self->destination();

    if ( $self->is_board_specific( $token ) ) {
        return $token;
    }

    return ($self->make_board_specific( $token ), $token );
}

=head2 is_board_specific

Is the given token name board_specific. In other words,
does it starts with $self->destination() ?

=cut

sub is_board_specific {
    my $self = shift;
    my $token = shift;
    my $prefix = $self->destination();

    if ( $token =~ m/^\Q${prefix}\E_/ ) {
        return 1;
    }

    return 0;
}

=head2 make_board_specific

Prefix the given token name with $this->destination()

=cut

sub make_board_specific {
    my $self = shift;
    my $token = shift;
    return $self->destination().'_'.$token;
}

=head2 destination

Name of this lowermost feed package.

=cut

sub destination {
    return _final_class(blessed($_[0]) || $_[0]);
}

# in: Stream::Templates::computerweekly
# out: computerweekly
sub _final_class {
    my $class = shift;
    $class =~ s/^.*:://;
    return $class;
}

sub parent_class {
    my $parent_class = $_[0]->determine_isa()
        or return;

    $parent_class =~ s/^Stream::Templates?:://;

    return $parent_class;
}

=head2 determine_isa

Returns the bottom most template class name from this or undef
if this is of an intermediary "Engine" helper class.

Usage:

    if( my $template_class = $this->determine_isa() ){
       ...
    }

=cut

sub determine_isa {
    my $class = shift;

    no strict 'refs';
    my $top_class = ${ "${class}::ISA" }[ $#{ "${class}::ISA" } ];

    # only return the template that we inherit from, don't return the engine etc
    if ( ( $top_class // '' ) =~ m/^Stream::Template/ ) {
        return $top_class;
    }

    return;
}

sub token_values {
    my $self = shift;

    if ( !@_ ) {
        return ();
    }

    if ( @_ ==  1 ) {
        return $self->get_token_value( @_ );
    }

    my $tokens_ref = $self->tokens();

    if ( !$tokens_ref ) {
        return ();
    }

    my @values;
    TOKEN:
    foreach my $token ( @_ ) {
        foreach my $token_name ( $self->_potential_token_names( $token ) ) {
            if ( exists( $tokens_ref->{ $token_name } ) ) {
                push @values, $tokens_ref->{ $token_name };
                next TOKEN;
            }
        }
    }

    return map {
        # dereference any arrays
        ref( $_ ) ? @$_ : $_,
    }
    @values;
}

sub load_token_cache {
    my $self = shift;

    # only load the token cache once
    return 1 if $self->{token_cache};

    my %token_cache;

    TOKEN_CLASS:
    foreach my $tokens_ref ( $self->standard_tokens(), $self->derived_tokens(), $self->posting_only_tokens() ) {
        if ( !$tokens_ref ) {
            next TOKEN_CLASS;
        }
        foreach my $token_ref ( values %$tokens_ref ) {
            $token_cache{ $token_ref->{Name} } = $token_ref;
        }
    }

    $self->{token_cache} = \%token_cache;

    return 1;
}

=head2 invalid_token_values

Validate the given token values against this board's standard tokens.

Returns a struct like:

 { token_name => [ 'bad value1' , 'bad value2' ],
   token_name2 => [ 'bad value3' , ... ]
 }

If there's any bad value.

Returns undef if nothing is wrong

=cut

sub invalid_token_values{
    my ($self, $candidate_tokens) = @_;

    $candidate_tokens //= {};

    # check standard tokens
    my $standard_tokens = $self->standard_tokens();

    my $errors = undef;

    foreach my $standard_token ( keys %$standard_tokens ){
        my $i = 0;

        ## $log->info("TOKEN $standard_token is :".Dumper($standard_tokens->{$standard_token}) );

        my @hierarchy_options_values = @{ $standard_tokens->{$standard_token}->{Options} // [] } ;

        my @good_options_values = ();

        ## Flatten the potential hierarchy
        foreach my $hierarchy ( @hierarchy_options_values ){
            if( ref($hierarchy) eq 'ARRAY' ){
                #$log->info("Valid value: ".Dumper($hierarchy));
                push @good_options_values , $hierarchy;
            }
            if( ref($hierarchy) eq 'HASH' ){
                my @valid_values = @{ $hierarchy->{Options} // [] };
                # $log->info("Valid value_s_ from hierarchy :".Dumper(\@valid_values));
                push @good_options_values, @valid_values ;
            }
        }

        ## Consider only the values, not the labels.
        @good_options_values = map { $_->[1] } @good_options_values;


        # No options, no validation.
        unless( @good_options_values ){ next; }

        my $look_for = $self->make_board_specific($standard_token);

        $log->trace("Looking for token '$look_for' in candidate hash");

        my $values = $candidate_tokens->{$look_for};
        # No such thing found. Skipping.
        unless( $values ){ next; }

        # Check each value belongs to the good options
        foreach my $candidate_value ( @$values ){
            my $hit = 0;
            foreach my $compare_with ( @good_options_values ){
                if( $compare_with eq $candidate_value ){
                    $hit = 1;
                    last;
                }
            }
            unless( $hit ){
                $errors //= {};
                $errors->{$look_for} //= [];
                push @{$errors->{$look_for}} , $candidate_value;
            }
        }
    }

    return $errors;
}

=head2 standard_tokens

Returns an inflated/decoded -  in short usable - data structure
describing the standard tokens of this feed.

Usage:

  my $tokens = $this->standard_tokens();


Note that the token names are not prefixed with the name of this feed.

Returns something like that:

  {
         'minimum_qualification' => {
                                       'Options' => [
                                                      [ '', '' ],
                                                      [ 'University degree', '1'],
                                                      [ 'Masters degree', '2' ],
                                                      ...
                                                    ],
                                       'Type' => 'list',
                                       'Id' => 'reed_minimum_qualification',
                                       'Board' => 'reed',
                                       'Name' => 'minimum_qualification',
                                       'Label' => 'Minimum qualification',
                                       'SortMetric' => 6
                                     },
          'include_ineligible' => { ... }
  }

=cut

sub standard_tokens {
    my $class = ref($_[0]) ? ref($_[0]) : shift;

    # fetch feed specific fields
    my %feed_tokens = $class->_peek_at_hash( 'standard_tokens' );

    # merge the default fields with the feed specific fields
    return $class->_standardise_tokens( \%feed_tokens );
}

# returns all the standard tokens that are not mandatory
sub optional_tokens {
    my $class = ref($_[0]) ? ref($_[0]) : shift;

    # get a list of all the standard tokens
    my $tokens_ref = $class->standard_tokens();

    # remove the mandatory tokens
    while ( my ($key,$token_ref) = each %$tokens_ref ) {
        my $remove_token = 0;
        if ( $token_ref->{Mandatory} || $token_ref->{Essential}) {
            $remove_token = 1;
        }
        if ( exists( $token_ref->{Deprecated} ) && lc( $token_ref->{Deprecated}) eq 'jobtype' ) {
            $remove_token = 1;
        }

        if ( $remove_token ) {
            delete $tokens_ref->{$key};
        }
    }

    return $tokens_ref;
}

sub mandatory_tokens {
    my $class = ref($_[0]) ? ref($_[0]) : shift;

    # get a list of all the standard tokens
    my $tokens_ref = $class->standard_tokens();

    # remove all optional tokens
    while ( my ($key,$token_ref) = each %$tokens_ref ) {
        if ( !$token_ref->{Mandatory} ) {
            delete $tokens_ref->{$key};
        }
    }

    return $tokens_ref;
}

sub essential_tokens {
    my $class = ref($_[0]) ? ref($_[0]) : shift;

    # get a list of all the standard tokens
    my $tokens_ref = $class->standard_tokens();

    # remove all optional tokens
    while ( my ($key,$token_ref) = each %$tokens_ref ) {
        if ( !$token_ref->{Essential} ) {
            delete $tokens_ref->{$key};
        }
    }

    return $tokens_ref;
}

sub derived_tokens {
    my $class = ref($_[0]) ? ref($_[0]) : shift;

    # fetch feed specific fields
    my %feed_tokens = $class->_peek_at_hash( 'derived_tokens' );

    # merge the default fields with the feed specific fields
    return $class->_standardise_tokens( \%feed_tokens );
}

sub posting_only_tokens {
    my $class = ref($_[0]) ? ref($_[0]) : shift;

    # fetch feed specific fields
    my %feed_tokens = $class->_peek_at_hash( 'posting_only_tokens' );

    # merge the default fields with the feed specific fields
    return $class->_standardise_tokens( \%feed_tokens );
}

sub authtokens {
    my $class = ref($_[0]) ? ref($_[0]) : shift;
    return $class->_get_and_standardise_authtokens('authtokens');
}

sub global_authtokens {
    my $class = ref($_[0]) ? ref($_[0]) : shift;
    return $class->_get_and_standardise_authtokens('global_authtokens');
}

sub _get_and_standardise_authtokens {
    my ($class, $authtoken_type) = @_;

    my %authtokens = $class->_peek_at_hash( $authtoken_type );

    foreach my $token_ref ( values %authtokens ) {
        # all authtokens default to Mandatory
        if ( !exists( $token_ref->{Mandatory} ) ) {
            $token_ref->{Mandatory} = 1;
        }
    }

    return $class->_standardise_tokens( \%authtokens );
}

sub run_helpers {
    $_[0]->process_derived_tokens();
    $_[0]->process_posting_only_tokens();
}

sub process_derived_tokens {
    my ( $self ) = @_;

    # because sometimes it's called salary_unknown and sometimes it isn't
    $self->token_value( 'salary_unknown', $self->token_value( 'include_unspecified_salaries' ) );

    return $self->process_tokens( $self->derived_tokens() );
}

sub process_posting_only_tokens {
    return $_[0]->process_tokens( $_[0]->posting_only_tokens() );
}

sub process_tokens {
    my $self = shift;

    $self->create_default_tokens();

    my $tokens_ref = shift || return;

    TOKEN:
    foreach my $token_ref ( sort { ( $a->{HelperMetric} // 0 ) <=>  ( $b->{HelperMetric} // 0 ) } values %$tokens_ref ) {
        # check to see if we should run a helper
        if ( !$token_ref->{Helper} ) {
            $log->debug( sprintf('%s> no helper - moving on',
                                 $token_ref->{Name},
                                ));

            next TOKEN;
        }

        # interpret the helper string
        # e.g. Helper => 'helper(%token_one%|%token_two%|param3)'
        my ( $helper, $param_string ) = split /\(/, $token_ref->{Helper};

        if ( !$helper ) {
            $log->debug(sprintf('%s> no helper found in <%s> - moving on',
                    $token_ref->{Name}, $token_ref->{Helper}
            ));

            next TOKEN;
        }

        unless( defined $param_string ){
            $log->warn("Missing param_string (undefined) for helper $helper");
            $param_string = '';
        }

        $param_string =~ s/\)\s*$//;
        my @unprocessed_params = split /\|/, $param_string;

        # substitute tokens in the parameters
        my @processed_params;
        foreach my $param ( @unprocessed_params ) {
            my (@params) = $self->fill_in_tokens( $param );
            push @processed_params, @params;
        }

        # run helper and store result as we go
        my $token_name = $token_ref->{Name};
        $log->info(sprintf('%s> running helper %s (%s)',
                $token_name, $helper, join('|', @processed_params))
        );

        eval {
            $self->token_value(
                $token_name,
                $self->$helper( @processed_params )
            );
        };

        if ( my $orig_error = $@ ) {
            my $err = sprintf( 'error running helper <%s>: %s', $helper, $orig_error );
            $log->error( $err );
            if ( ref($orig_error) && $orig_error->isa( 'Stream::EngineException' ) ){
                die $orig_error;
            }
            die $err;
        }

        my @values = $self->token_value( $token_name );

        $log->debug(sprintf(
            'info', '%s> helper %s returned %s',
                $token_name, $helper, join(',', grep{ defined($_) } @values )),
        );

    }

    return;
}

# makes sure all our default tokens are present
# and creates them
sub create_default_tokens {
    my $self = shift;

    $self->create_location_within_tokens();

    return 1;
}

=head2 create_location_within_tokens

Creates tokens 'location_within_miles' and
'location_within_km' tokens in case its possible
given the tokens already present.

Returns true if such tokens were generated, false otherwise.


Usage:

 if( $this->create_location_within_tokens() ){
     # Guaranteed to have a value:
     $self->token_value( 'location_within_km' );
     # and/or
     $self->token_value( 'location_within_miles' );
 }

=cut
sub create_location_within_tokens {
    my $self = shift;

    my $distance = $self->token_value('location_within');

    # clean the location within token (perhaps we should do this in Stream::Core?)
    if ( $distance && $distance =~ s/\s+// ) {
        $self->log_debug('Removed extra whitespace from location_within token');
        $self->token_value('location_within' => $distance );
    }

    if ( !$distance ) {
        # no distance so nothing to do
        $self->log_debug('%%location_within%% (%s) is empty so not creating _miles and _km tokens', $distance);
        return 0;
    }

    my $distance_unit = $self->token_value('distance_unit') || '';

    if ( $distance_unit eq 'km' ) {
        $self->token_value( 'location_within_km' => $distance );

        my $distance_in_miles = int( Stream2->instance()->location_api->convert_km_to_miles( $distance ) + 0.5);
        $self->log_debug('(%s)km is (%s) miles', $distance, $distance_in_miles );
        $self->token_value( 'location_within_miles' => $distance_in_miles );
    }
    else {
        $self->token_value( 'location_within_miles' => $distance );

        my $distance_in_km = int( Stream2->instance()->location_api->convert_miles_to_km( $distance ) + 0.5);
        $self->log_debug('(%s)miles is (%s)km', $distance, $distance_in_km );
        $self->token_value( 'location_within_km'    => $distance_in_km );
    }

    return 1;
}

sub _standardise_tokens {
    my $class = shift;
    my $tokens_ref = shift;

    # make sure all fields have a name & label
    while ( my ($key, $token_ref)  = each %$tokens_ref ) {
        $token_ref->{Name}  ||= $key; # name is the cgi param used

        $class->_standardise_token($token_ref);
    }

    return $tokens_ref;
}

sub _standardise_token {
    my ($class, $token_ref) = @_;

    $token_ref->{Label} ||= $token_ref->{Name}; # label is displayed to the consultant
    $token_ref->{Board} = $class->destination();
    $token_ref->{Id} = $token_ref->{Board}.'_'.$token_ref->{Name};

    $class->_standardise_token_type($token_ref);
    $class->_standardise_token_validation($token_ref);

    $token_ref->{SortMetric} = $DEFAULT_SORTMETRIC unless defined( $token_ref->{SortMetric} );

    if ( $token_ref->{Name} eq 'max_results' ) {
        $class->_standardise_max_result_token($token_ref);
    }

    return $token_ref;
}

sub _standardise_token_type {
    my ($class, $token_ref) = @_;

    return unless $token_ref->{Type};

    if ( $token_ref->{Type} =~ s/^((?:multi)?list)\((.+)\)$/$1/i ) {
        # extract list options from Type(Option 1=Value 1|Option 2=Value 2....)
        $token_ref->{Options} = $2;
    }

    $token_ref->{Type} = lc($token_ref->{Type});

    if ( $token_ref->{Type} =~ m/list$/ || $token_ref->{Type} eq 'sort' ) {
        my $options_ref = delete $token_ref->{Options}
            || delete $token_ref->{Values};

        if ( $options_ref && (  ! ref($options_ref) )  && ( $options_ref =~ /BBTECH_RESERVED_OPTGROUP/ ) ){
            # This shit is a complex grouped multi list defined as one line of text
            # masquerading as a multi list
            $token_ref->{Options} = $class->_parse_grouped_list( $options_ref );
            $token_ref->{Type} = 'grouped' . $token_ref->{Type};
            return $token_ref;
        }

        $token_ref->{Options} = $class->build_list( $options_ref );
    }

    return $token_ref;
}

sub _parse_grouped_list {
    my ( $class, $rawlist ) = @_;
    my $group_ref = [];
    my @rawoptions = split( /\|/, $rawlist );

    # we are working with a flat string which describes 2 levels of array, list + sublist
    foreach my $raw ( @rawoptions ){
        next if !$raw;
        my $opt = [ split "=", $raw ];
        # if the value is OPTGROUP then it is the title of the sublist
        if ( $opt->[1] eq 'BBTECH_RESERVED_OPTGROUP' ){
            push @$group_ref, { Label => $opt->[0], Options => [] };
            next;
        }
        if ( ! scalar( @$group_ref ) ){
            # Some grouped list items don't need to belong to a group apparently so they will be added to a special group
            # which has no label and is always visible - hooray
            push @$group_ref,  { Label => '', Options => [] };
        }
        push $group_ref->[-1]->{Options}, $opt;
    }

    return $group_ref;
}

sub _standardise_token_validation {
    my ($class, $token_ref) = @_;

    return unless $token_ref->{Validation};

    my $validation = $token_ref->{Validation};
    if ( $validation =~ m/select(\d*)-(\d+)/i ) {
        $token_ref->{Validation} = 'Select';
        $token_ref->{Minimum} = $1;
        $token_ref->{Maximum} = $2;
    }
    elsif ( $validation =~ m/numeric/i ) {
        $token_ref->{Validation} = 'Numeric';
    }

    return $token_ref;
}

sub _standardise_max_result_token {
    my ($class, $token_ref)= @_;

    # ensure max_results has a default
    # otherwise loading the "detailed search" lightbox will default the search to the first option

    if ( !defined($token_ref->{Default}) ) {
        my $token = Stream::Token->new( $token_ref );
        $token_ref->{Default} = $class->results_per_page_from_token( $token );
    }

    return $token_ref;
}

sub build_list {
    my ($class, $options_ref) = @_;

    if ( ref($options_ref) eq 'ARRAY' ) {
        # already an array - lets just standardise the array
        return standardise_list($options_ref);
    }

    if ( ref($options_ref) eq 'HASH' ) {
        # { option1 => value1, .... optionN => valueN }
        return build_list_from_hash( $options_ref );
    }

    # (option1=value1|.....optionN=valueN)
    return build_list_from_scalar( $options_ref );
}

sub standardise_list {
    my $options_ref = shift;

    return [
        map { ref($_) ? $_ : [ $_ => $_ ]; }
            @$options_ref,
    ];
}

sub build_list_from_scalar {
    my $options = shift or return [];

    $options =~ s/^\(//;
    $options =~ s/\)$//;

    my $separator = '=';
    if ( $options =~ m/!!/ ) {
        $separator = '!!';
    }

    my @options = map {
        [ m/$separator/ ? split( /$separator/, $_, 2) : ($_ => $_) ],
    } split /\|/, $options;

    return \@options;
}

sub build_list_from_hash {
    my $options_ref = shift;

    my @sorted_options;
    foreach my $option ( sort keys %$options_ref ) {
        push @sorted_options, [ $option => $options_ref->{ $option } ];
    }

    return \@sorted_options;
}

sub inherit_tokens {
    my $class = shift;
    my $from_class = shift || $class->determine_isa() || return;

    foreach my $token_type ( qw(standard_tokens derived_tokens posting_only_tokens) ) {
        no strict 'refs';
        %{ "${class}::${token_type}" } = _peek_at_hash( $from_class, $token_type );
    }

    return $from_class;
}

sub _peek_at_hash {
    my $class = shift;
    my $varname = shift;

    # check the symbol table for $class to see if a variable has been declared
    # see `perldoc perlmod` for further details

    no strict 'refs';
    if ( !exists(${"${class}::"}{$varname}) ) {
        return ();
    }

    # check that the symbol has a hash declaration
    if ( !defined( *{ "${class}::${varname}" }{HASH} ) ) {
        return ();
    }

    return %{ clone( \%{ "${class}::${varname}" } ) };
}

sub fill_in_tokens {
    my $self = shift;
    my $value = shift;

    return '' unless $value;
    return $value unless $value =~ m/%/;

    # 2 special cases to handle
    # 1. %special_token%%normal_token%
    #   should return a scalar value (ie.'special_valuenormal_value')
    # 2. %normal_token_array% should return ['value1','value2','value3']

    my $copy_of_value = $value;
    if ( $copy_of_value =~ s/%[^%]+%//g && !$copy_of_value ) {
        # only contains tokens
        my $seen_array = 0;
        my @all_values;
        while ( $value && $value =~ s/^%([^%]+)%// ) {
            my $token = $1;
            my @values = $self->token_value( $token );

            if ( @values > 1 ) {
                $seen_array = 1;
            }

            push @all_values, @values;
        }

        # we want %array% to return the array
        # but %token_1%%token_2% should return a string
        if ( $seen_array && wantarray ) {
            return @all_values;
        }

        # Protect against undef values
        return join('', grep{ $_ } @all_values);
    }

    # contains a mixture of text and tokens

    # substitute in tokens - messy but works for now
    # arrays get treated as a string
    $value =~ s/%([^%]+)%/$self->token_value($1)/eg;

    return $value;
}

sub log {
    my $self  = shift;
    my $priority = shift;
    my (@msgs) = (@_);

    # Message is always in sprintf style.
    @msgs = map{ defined($_) ? $_ : 'undef' } @msgs;
    my $message = sprintf(shift @msgs , @msgs );

    # Use lower case priority as log method
    # This need to be compatible with Log::Any
    my $log_method = lc($priority);

    if ( ref( $self ) && UNIVERSAL::can( $self, 'logger' ) ) {
        # provided by down the inheritance path by Stream::Engine, as an instance of Log::Any
        # Can always be a printf style message
        $self->logger()->$log_method( $message );

        return;
    }

    my $class = ref($self) || $self;
    Log::Any->get_logger( category => $class  )->$log_method($message);
}

=head2 generate_html_fragment

From a given HTML source STRING that can be a full html document,
or just a body, or just random html, parse it in a lenient way
and return a correct html string fragment to be inserted wherever
you see fit.

Usage:

  if( my $fragment = $this->generate_html_fragment( '<html><body><p>Blabla</p><p>Foo</p></body></html>' ) ){
     # fragment is <p>Blabla</p><p>Foo</p>
     ...
  }

=cut

sub generate_html_fragment{
    my ($self, $html) = @_;

    # Despite the 'string' parameter name, load_html expects bytes.
    my $xdoc = $self->{html_parser}->load_html( string => Encode::encode('UTF-8', $html),
                                                encoding => 'UTF-8'
                                            );
    $self->cleanup_xdoc_anchors($xdoc);

    my ( $xbody ) = $xdoc->findnodes('/html/body');
    unless( $xbody ){
        return undef;
    }
    # This generates a string
    return join('', map { $_->toString() } $xbody->childNodes() );
}

=head2 cleanup_xdoc_anchors

Removes <a> tags and their attributes from CV preview and replaces them with <span>

Usage:

    my $no_a_tags_doc = $self->cleanup_xdoc_anchors( XML::LibXML::Document );

=cut

sub cleanup_xdoc_anchors {
    my $self = shift;
    my $xdoc = shift;

    foreach my $a_node( $xdoc->getElementsByTagName('a') ) {
        $a_node->removeAttribute('name');
        $a_node->removeAttribute('class');
        $a_node->removeAttribute('style');
        $a_node->removeAttribute('href');

        $a_node->setName('span');
    }

    return $xdoc;
}

sub build_salary {
    my $self = shift;
    my $salary_cur  = shift;
    my $salary_from = shift;
    my $salary_to   = shift;
    my $salary_per  = shift;

    if ( defined($salary_from) && defined($salary_to) ) {
        return sprintf('%s%s - %s per %s', $salary_cur, $salary_from, $salary_to, $salary_per);
    }

    if ( defined($salary_to) ) {
        return sprintf('%s%s per %s', $salary_cur, $salary_to, $salary_per);
    }

    if ( defined($salary_from) ) {
        return sprintf('%s%s+ per %s', $salary_cur, $salary_from, $salary_per);
    }

    return '';
}

# this is a stub - it does not work as a class method
# may need to subclass this
sub results_per_page {
    my $self = shift;

    if ( !ref( $self ) ) {
        # being called as a class method
        return '';
    }

    # called as an instance method

    # see if they have selected how many results they want
    my $max_results = $self->token_value('max_results');

    if ( defined( $max_results ) && $max_results > 0 ) {
        return $max_results;
    }

    # see if the feed has a max_results token
    my $max_results_token = $self->build_token( 'max_results' );

    if ( $max_results_token ) {
        return $self->results_per_page_from_token( $max_results_token );
    }

    return '';
}

sub results_per_page_from_token {
    my $self = shift;
    my $max_results_token = shift or return;

    # see if the feed has a default
    if ( $max_results_token->default() ) {
        return $max_results_token->default();
    }

    # fallback to the last value under 50
    # or the lowest value over 50
    my @option_values = $max_results_token->option_values();

    my $last_n_results;
    foreach my $n_results ( $max_results_token->option_values() ) {
        if ( $n_results > 50 ) {
            return $last_n_results || $n_results;
        }

        $last_n_results = $n_results;
    }

    return $last_n_results;
}

1;

# vim: expandtab shiftwidth=4

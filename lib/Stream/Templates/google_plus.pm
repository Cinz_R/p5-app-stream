#!/usr/bin/perl
#This is now a copy of google_plus_xray
package Stream::Templates::google_plus;
use strict;
use utf8;

use base qw(Stream::Templates::google_cse);

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

$derived_tokens{and_terms} = {
    Type => 'Text',
    Helper => 'google_split_keywords(%keywords%|and_terms|or_terms|not_terms|exact_terms)',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{or_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{not_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{exact_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

sub google_plus_country_location{
    my $self = shift;
    my $country = shift;
    my $location_id = shift;
    my $location = shift;
    
    #location->name depends on the locale that the user is in, i.e netherlands for dutch users will return nederlands 
    #try and get as many locations as possible if they have choosen a company
    my $keywords = ' AND (';
    
    $keywords .= '("Lives in * ';
    $keywords .= $country ||  $location->name;
    $keywords .= '" OR "Lives in ';
    $keywords .= $country || $location->name;
    $keywords .= '")';
    
        
#   in french, dutch, belgium    
    
    my %translate_country_name = (
                Netherlands =>  'Nederland',  
                Belgium     =>  'België',
            );

        if( my $country_name = $translate_country_name{ $location->name } ){ 
        $self->log_debug("country name is :" . $country_name );            
                $keywords .= ' OR ("Lives in * ';
                $keywords .= $country_name;
                $keywords .= '" OR "Lives in ';
                $keywords .= $country_name;
                $keywords .= '")';
            
        }
    
     $keywords .= ")";
    
    return $keywords;
    
    }

sub build_google_search_query {
    my $self = shift;

    my $base_url = 'plus.google.com';

    my $keywords = "site:(" . $base_url .") AND -inurl:about AND -inurl:posts AND -inurl:communities AND (\"in his circles\" OR \"in her circles\")"; 

    if ($self->token_value('keywords')){
        if ($self->token_value('and_terms')){
            $keywords .= ' AND (' . $self->token_value('and_terms') . ')';
        }
        if ($self->token_value('exact_terms')){
            $keywords .= ' AND (' . $self->token_value('exact_terms') .')';
        }
        if (my $or_terms = $self->token_value('or_terms')){
            $or_terms =~ s/^\s+OR//;
            $keywords .= ' AND (' . $or_terms .')';
        }
        if ($self->token_value('not_terms')){
            $keywords .= ' AND ' . $self->token_value('not_terms');
        }
    }

    my $location_id = $self->token_value('location_id');

    if ($location_id){
        my $location = $self->location_api()->find({
            id => $location_id,
        });
        if ($location){
            my $country;

            if( $location->is_country  ){

                $country = "United Kingdom" if $location->in_uk;     
                $keywords .= $self->google_plus_country_location( $country, $location_id, $location );

            }
            else{
                
                $keywords .= ' AND ("Lives in ';
                $keywords .= $self->google_loc_check( $location->name );
            
                if (!$location->is_country){
                    if ($location->in_usa){
                        if ($location->is_county){
                            #Meh. hope for the best
                        } else {
                            my $state = $location->get_mapping({ board => 'ISO_STATES' });
                            $keywords .= ', ' . $state;
                        }
                    } else {
                         
                        $country = $location->in_uk ? 'United Kingdom' : $location->country->name;
                        $keywords .= ', ' . $country;
                        
                            if( !$location->in_uk ){
                              my %lang_codes = ( Belgium => 'NL', France => 'FR', Netherlands  => 'NL' );  
                              #try and search in the countries langauge as well, i.e netherlands is nederlands in dutch
                              if( my $lang_code  = $lang_codes{ $country } ){
                                    my $location_translation = '" OR "Lives in ';
                                    my $location2 = $self->location_api()->find({
                                        id => $location_id,
                                        # TODO: lang => $lang_code
                                    });
                                    if($location2){
                                        my $location2_name = $location2->name;
                                        my $location2_country = $location2->country->name;
                                        $location_translation .= $location2_name . ', ' . $location2_country;
                                        $self->log_debug( "Location  translation for " . $location2_country . " is : " . $location_translation );
                                            
                                        if( $location2_name ne $location->name || 
                                            $location2_country ne $country ){
                                                $keywords .= $location_translation;  
                                        }
                                    }
                                 }
                                
                              }

                    }
                  
            }
            $keywords .= '")';
        }
      }
    }
    return $keywords;


}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;
    $candidate->attr('google_plus_profile_url' => $candidate->attr('profile_link'));

    my $google_plus_id = $1 if $candidate->attr('profile_link') =~ /(\d+)$/;
    if ($google_plus_id){
        $candidate->attr('google_plus_id', $google_plus_id);
        $candidate->attr('profile_url', $candidate->attr('profile_link')); #link inside preview
        $candidate->attr('profile_link', ''); #force our preview to load
        $candidate->has_profile(1); 
    }

    return $self->SUPER::scrape_fixup_candidate($candidate);
}

sub google_add_cache_link { 0; }

sub google_plus_xray_api_key{
    return "AIzaSyAb1-7pykchkJFtDo9l4yQmXzDhYZITXrc";
}

sub google_plus_xray_api_base_url{
    return "https://www.googleapis.com/plus/v1/people";
}
sub download_profile_url{
    my $self = shift;
    my $candidate = shift;    
    return $self->google_plus_xray_api_base_url() . '/' . $candidate->attr('google_plus_id')  . '?key=' . $self->google_plus_xray_api_key();
}

sub download_profile{
    my $self = shift;
    my $candidate = shift;

    my $resp = $self->get( $self->download_profile_url( $candidate ) );

    return $self->scrape_generic_download_profile( $candidate );
}

sub scrape_generic_download_profile{
    my $self = shift;
    my $candidate = shift;
    my $json = $self->response_to_JSON();

    delete $json->{id};
    delete $json->{destination};
    delete $json->{name}; #this is messing with the results list
    $candidate->attributes( %{ $json } );
    $self->scrape_fixup_candidate_profile( $candidate );

    $candidate->profile_downloaded( time );

    return $candidate;

}

sub scrape_fixup_candidate_profile{
    my $self = shift;
    my $candidate = shift;
    my @school;
    my @work;
   
   ### text inside <b> tags appear invisible in the preview for some reason, so need to change to strong
    my @story = qw /tagline aboutMe braggingRights/;

    foreach my $story_fields (@story ){
        
        if( my $story_value = $candidate->attr( $story_fields) ){
                $story_value =~ s/<b>/<strong>/g;
                $story_value =~ s/<\/b>/<\/strong>/g;
                $candidate->attr( $story_fields => $story_value );
            }
        }
    ##
    ##
    if ( $candidate->attr('placesLived') ){

        my $currently_lives;
        my $previously_lived;

        my $i = 0;
        foreach my $places(@{$candidate->attr('placesLived')}){
            
            if ( $places->{'primary'} eq 'true'){
                    $currently_lives = 1;
                    $candidate->attr('currentLocation' => $places->{'value'});
                    last;
                }
            if ( $i == 0 ){
                $previously_lived = $places->{'value'};
                }
            $i++;
            }

            if( !$currently_lives && $previously_lived ){
                $candidate->attr( 'previouslyLived' => $previously_lived );    
            
            }
       }
    
    if( $candidate->attr('organizations') ){
    my $ii =0;
    my $currently_works = 0;
    my $previously_worked;
    my $currently_attends = 0;
    my $previously_attended;
    
    foreach my $org( @{$candidate->attr( 'organizations' )} ){
            push (@work,$org) if( $org->{type} eq 'work' );
            push (@school,$org) if( $org->{type} eq 'school' );
            
            if($org->{type} eq 'work'){

                if( $org->{primary} eq 'true'){
                 $candidate->attr( 'currentlyWorks' =>   $org->{name} );
                 $currently_works = 1;
                }
               
                if( $ii == 0 ){
                    $previously_worked = $org->{name}; 
                    }
               }

             if($org->{type} eq 'school'){
                  
                  if( $org->{primary} eq 'true' ){
                   $candidate->attr( 'currentlyAttends' => $org->{name} ); 
                   $currently_attends = 1; 
                  }
                 
                 if( $ii == 0 ){
                    $previously_attended = $org->{name}; 
                    }

                 }   
            $ii++;
        }
        
        if( !$currently_attends && $previously_attended ){
                $candidate->attr('previouslyAttended' =>   $previously_attended );
            }

        if( !$currently_works && $previously_worked ){
                $candidate->attr('previouslyWorked' =>   $previously_worked );
            }

    }


    if ( my $birthday = $candidate->attr('birthday') ){
       $self->log_debug( 'Birthday is: ' . $birthday );
        my ($year,$month,$day) = split('-',$birthday,3);
        my %months = (
                '01'   =>  'Jan',
                '02'   =>  'Feb',
                '03'   =>  'Mar',
                '04'   =>  'Apr',
                '05'   =>  'May',
                '06'   =>  'June',
                '07'   =>  'July',
                '08'   =>  'Aug',
                '09'   =>  'Sept',
                '10'   =>  'Nov',
                '11'   =>  'Oct',
                '12'   =>  'Dec',

            );
        if($year eq '0000' ){
           $candidate->attr('birthday' =>   $day .' ' . $months{$month} ); 
            } 
     
     }

    if( my $rel_status = $candidate->attr('relationshipStatus') ){
       my %relationship_status = (
                        single            =>  'Single',
                        in_a_relationship =>  'In a Relationship',
                        engaged           =>  'Engaged',
                        married           =>  'Married',
                        its_complicated   =>  'It\'s complicated',
                        open_relationship =>  'Open relationship',
                        widowed           =>  'Widowed',
                        in_domestic_partnership =>  'Domestic partnership',
                        in_civil_union          =>  'In civil union',          
       
                         );

        
        $candidate->attr('relationshipStatus' => $relationship_status{$rel_status} );
        
        }
    $candidate->attr('work' => \@work) if @work;
    $candidate->attr('school' => \@school) if @school;
    
    $self->log_debug('running fixup candidate profile');

    return;
}

sub scrape_download_profile_success{
    return qr{kind};
}

sub cse_engine_id {
    return '003231768667224905379:tulwst-of8i';
}

return 1;

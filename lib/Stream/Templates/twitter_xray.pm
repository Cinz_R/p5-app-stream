#!/usr/bin/perl

package Stream::Templates::twitter_xray;
use strict;
use utf8;

use base qw(Stream::Templates::google_cse);

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

$derived_tokens{and_terms} = {
    Type => 'Text',
    Helper => 'google_split_keywords(%keywords%|and_terms|or_terms|not_terms|exact_terms)',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{or_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{not_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{exact_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

sub build_google_search_query {
    my $self = shift;

    my $base_url = 'twitter.com';

    my $keywords = "site:" . $base_url . " AND intitle:\"on twitter\" AND -inurl:\"support.twitter\" AND -inurl:\"blog.twitter\" AND -\"#jobs\" AND -\"#job\" AND -\"#hiring\" AND -\"Job in\"";
    
   #job tags in different languages 
    my %lang_filters = ( 
        NL  => " AND -\"#Vacatures\" AND -\"#banen\" AND -\"#huren\" AND -\"#werk\"",
        FR  => " AND -\"#emploi\" AND -\"#emplois\" AND -\"#embauche\"", 
        BE  => " AND -\"#Vacatures\" AND -\"#banen\" AND -\"#huren\" AND -\"#werk\"",           
             );
        

    if ($self->token_value('keywords')){
        if ($self->token_value('and_terms')){
            $keywords .= ' AND (' . $self->token_value('and_terms') . ')';
        }
        if ($self->token_value('exact_terms')){
            $keywords .= ' AND (' . $self->token_value('exact_terms') .')';
        }
        if (my $or_terms = $self->token_value('or_terms')){
            $or_terms =~ s/^\s+OR//;
            $keywords .= ' AND (' . $or_terms .')';
        }
        if ($self->token_value('not_terms')){
            $keywords .= ' AND ' . $self->token_value('not_terms');
        }
    }

    my $location_id = $self->token_value('location_id');

    if ($location_id){
        my $location = $self->location_api()->find({
            id => $location_id,
        });
        if ($location){
            my $country_code = $location->get_mapping( { board => "ISO_COUNTRIES"} );
            my $excluded_keywords = $lang_filters{ $country_code  };
            
            $keywords .= $excluded_keywords if $excluded_keywords;
            
            $keywords .= ' AND ("' . $self->google_loc_check( $location->name ) . '"';
            #search in the country's langauge
            if( $location->is_country ){
               my %countries = (
                            Netherlands =>  'Nederland',
                            Belgium     =>  'België',
                        );
               if( my $country_translation = $countries{ $location->name } ){
                        $keywords .= ' OR "' . $country_translation  .  '"';            
                   } 

               }
             else{
                my %lang_codes = ( Netherlands => 'NL', Belgium  => 'NL', France  => 'FR' );      
                 if( my $lang_code = $lang_codes{ $location->country->name  } ){

                        # TODO: implement languages/translations!
                        my $location2 = $self->location_api()->find({
                            id => $location_id,
                            # lang => $lang_code,
                        });

                        if( $location2 ){
                           my $location_translation = $location2->name; 
                              if( $location_translation ne $location->name ){
                                  $keywords .= ' OR "' . $location_translation . '"';
                                  } 
                          }

                    } 
              } 

           $keywords .= ')';     
        }
    }

    return $keywords;
}

sub scrape_fixup_candidate{
    my $self = shift;
    my $candidate = shift;

    my $name = $candidate->attr('headline');
    $name =~ s/on Twitter//;
    $candidate->attr('name' => $name);

    my $twitter_handle = $1 if $name =~ /(\(.+\))/;
    $twitter_handle =~ s/\(|\)//g;
    $candidate->attr('twitter_handle' => $twitter_handle);

    my $headline = $candidate->attr('headline');
    $name =~ s/\(|\)//g;
    $headline =~ s/$name\s\((.+?)\)/$1/;
    $candidate->attr('headline' => $headline);
    return $self->SUPER::scrape_fixup_candidate($candidate);
}

sub google_add_cache_link { 0 }

return 1;

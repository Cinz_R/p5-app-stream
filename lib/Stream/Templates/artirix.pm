package Stream::Templates::artirix;

use strict;
use base qw(Stream::Engine::API::REST);
use utf8;

use Stream2;
use Stream::TP2::ElasticSearch;
use Stream::TP2::Search;
use Stream::TP2::DownloadCV;

use Search::Elasticsearch;
use Stream::TP2::Constants;

use Log::Any qw/$log/;

use Data::Dumper;
use Carp;

use Stream::Constants::SortMetrics qw(:all);
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

# Define all facets here and leave off Options for multilists as they
# are populated in &scrape_candidate_array from response
%standard_tokens = (
    applicant_tags => {
        Id => 'talentsearch_tags',
        Type => 'tag',
        Name => 'tags',
        Label => 'Tags/Hotlists',
        Size => 60,
        Essential => 1,
        SortMetric => 100,
    },
    advert_industry => {
        Id => 'talentsearch_advert_industry',
        Type => 'multilist',
        Label => 'Industry',
        Name => 'advert_industry',
        SortMetric => 200,
    },
    advert_job_type => {
        Id => 'talentsearch_job_type',
        Type => 'multilist',
        Label => 'Job Type',
        Name => 'advert_job_type',
        SortMetric => 300,
    },
    applicant_availability_date => {
        Id => 'talentsearch_applicant_availability_date',
        Type => 'multilist',
        Label => 'Availability Date',
        Name => 'applicant_availability_date',
        Values => [
            'In the past',
            &get_availability_dates
        ],
        SortMetric => 400,
    },
    employer_org_position_title => {
        Label => 'Current Job Title',
        Type  => 'Text',
        Size       => 60,
        SortMetric => 600,
        Essential  => 1,
    },
    employer_org_name => {
        Label      => 'Current Employer',
        Type       => 'Text',
        Size       => 60,
        SortMetric => 500,
        Essential  => 1,
    },
    applicant_recruitment_status => {
        Label       => 'Recruitment Status',
        Type        => 'List',
        Values      => '(Any=|Available|In Progress|On Assignment)',
        SortMetric  => 700,
    },
    order_by => {
        Type    => 'List',
        Values  => [
            'Relevance',
            'Last Updated',
        ],
	Default => 'Relevance',
       	SortMetric  => $SM_SORT,
    },
);

%derived_tokens = (
    'salary_to' => {
        Type => 'SalaryText',
        SortMetric => $SM_SALARY,
    },
    'salary_from' => {
        Type => 'SalaryText',
        SortMetric => $SM_SALARY,
    },
    'salary_per' => {
        Type    => 'SalaryPer',
        Options => 'week',
        Helper  => 'SALARY_PER(salary_per|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric => $SM_SALARY,
    },
    'salary_cur'   => {
        Type    => 'Currency',
        Options => 'GBP',
        Helper  => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric => $SM_SALARY,
    },
);

%posting_only_tokens = (
    advert_job_type => {
        Helper => "jobtype_helper(%default_jobtype%|%job_type%|permanent=Permanent|contract=Contract|temporary=Temporary)",
    },
    advert_salary_from => {
        Helper => "ROUND(%salary_from%)",
    },
    advert_salary_to => {
        Helper => "ROUND(%salary_to%)",
    }
);


=head2 get_elastic_search

Returns a L<Search::ElasticSearch> client suitable for querying talent search.
Note that this is not used by anything for now.

Usage:

 my $es = $this->get_elastic_search();

=cut

sub get_elastic_search{
    my ($self) = @_;
    return Search::Elasticsearch->new(client => '0_90::Direct',
                                      nodes => [ 'https://broadbean:wor2lahT@es.bb.artirix.com/' ] );
}

=head2 loop_through_all

Loop through all the documents of this company's (aka client) documents.

Emits one hit (a Elastic search hash) at a time for the 'on_hit' given method.

max_docs defaults to 100 Millions, which should be enough for most uses.

Usage:

   my $report = $this->loop_through_all({ max_docs => 123456,
                             on_hit => sub{
                                   my ($hit) = @_;
                             }
                           });

$report contains { docs_processed => < a number > }

=cut

sub loop_through_all{
    my ($self, $opts ) = @_;

    $opts //= {};
    $opts->{on_hit} //= sub{};

    my $client_id = $self->talentsearch_client_id();

    my $e = $self->get_elastic_search();

    my $max_docs = $opts->{max_docs} // 100_000_000;

    $log->info("client_id = '$client_id'");

    my $scroll = $e->scroll_helper(
                                   scroll => '5m',
                                   search_type => 'scan', # Go fast, do not sort. See https://metacpan.org/pod/Search::Elasticsearch::Scroll#DEEP-SCROLLING
                                   size => 10, # 10 from EACH shard (if the index is sharded)
                                   index => 'items1',
                                   type => 'applicant',
                                   body => {
                                            filter => { term => { client_id  => $client_id  } },
                                            query => { match_all => {} }
                                           }
                                  );
    $log->info("TOTAL HITS: ".$scroll->total(). " going through them");

    my $docs_processed = 0;

    while( $docs_processed < $max_docs && ( my $hit = $scroll->next() ) ){
        $docs_processed++;

        &{ $opts->{on_hit} }( $hit );

        unless( $docs_processed % 100 ){
            $log->info("Processed $docs_processed documents");
        }
    }

    # Finish that whatever happens.
    eval {
        $scroll->finish();
    };
    $log->info("Done looping through all artirix documents");

    return { docs_processed => $docs_processed };
}

sub get_availability_dates {
    require Stream::TP2::AvailDate;
    return  Stream::TP2::AvailDate::getDateRange("labels");
}

sub artirix_start_record {
    my $self = shift;
    my $logger = $self->logger();

    my $current_page = $self->current_page() || 1;
    my $records_per_page = $self->results_per_page();
    my $start = 0 + (($current_page - 1) * $records_per_page);

    $logger->debug('Current Page: %s - Records Per Page: %s - Start: %s', $current_page, $records_per_page, $start );

    return $start;
}

sub results_per_page {
    my ($self) = @_;
    #  return a specified number of results per page, if it exists
    return $self->token_value('results_per_page') ?
        $self->token_value('results_per_page') + 0 : 50;
}

=head2 talentsearch_client_id

Returns the client ID to be used for talent search.

This should come from an auth_token called 'talentsearch_client_id', otherwise
it will fall back to the usual company (or group_identity).


=cut

sub talentsearch_client_id{
    my ($self) = @_;

    ## WARNING. This will get the token 'talentsearch_client_id' if you
    ## use a subclass of that (Like talentsearch.pm).
    my $client_id = $self->token_value('client_id');

    if( $client_id ){
        $log->info("Will use client_id = '$client_id' from Auth Tokens");
        return $client_id;
    }

    $log->warn("Falling back to company (or user objects' group identity)");

    $client_id =  $self->company() || $self->user_object->group_identity();

    if( $client_id ){
        $log->warn("Falling back to client_id = '$client_id' from company (or user objects' group identity)");
        return $client_id;
    }
    confess("Could not find any client_id");
}

#Search
sub search_submit {
    my $self = shift;

    my $params_ref = {
        'start' => $self->artirix_start_record(),
        'rows' => $self->results_per_page(),
    };

    if ($self->token_value('tags')){
        $self->token_value('applicant_tags' => $self->token_value('tags'));
    }

    my @extra_tokens = qw/cv_updated_within location_id keywords applicant_tags location_within distance_unit advert_job_type advert_salary_from advert_salary_to advert_industry employer_org_position_title applicant_tags employer_org_name salary_per applicant_availability_date applicant_recruitment_status order_by/;

    TOKEN:
    foreach my $token (@extra_tokens){
        my @options = $self->token_value($token);
        if ( !@options ) {
            next TOKEN;
        }

        if ( scalar(@options) > 1 ) {
            $params_ref->{$token} = \@options;
        }
        else {
            $params_ref->{$token} = shift @options;
        }
    }

    my $options_ref = {
        'no_decode' => 1,
        'include_facets' => 1, #This will be needed *someday*. This is now!
        'location_api' => $self->location_api(),
        'user_api' => $self->user_api(),
    };

    my $response = Stream::TP2::Search::submit_search( $self->talentsearch_client_id() , $params_ref, $options_ref );

    $self->log_transaction( $response->as_string() );

    # make the response available for scraping
    # normally the agent does this but we aren't using an agent
    my $decoded_content = $response->decoded_content();
    $self->recent_content($decoded_content);

    return $response;
}

sub search_number_of_results_regex {
    my $self = shift;

    require Stream::TP2::ElasticSearch;
    if (Stream::TP2::ElasticSearch::using_es( $self->talentsearch_client_id() ) ){
        #hits":{"total":13,"max_score
        return qr/hits":{"total":(\d+),/;
    }

    return qr/numFound":(\d+),/;
}

sub scrape_candidate_array {
    my $self = shift;

    # This will use the recent_content that has been
    # previously set by the search_submit method.
    # THis is from Stream::Engine::API::REST;
    # The recent_content is a STRING.
    my $json = $self->response_to_JSON();

    # Go through standard_tokens and assign Options for them
    # if defined in response
    my @facets;
    while ( my ( $k, $facet ) = each %standard_tokens ) {
        
        next if $facet->{Type} =~ m/text/i;

        # Term type facets ( enumerated types ) are easy peasy, we are just converting them from ES facet output
        # Into something the engine/front end will understand
        $facet->{Options} = [];
        if ( defined( $json->{facets}->{$k}->{terms} ) ){
            map {
                # coerce the facet output into the same format as options are output normally by a feed [ label, value ]
                my $option_arr = [ $_->{term}, $_->{term}, $_->{count} ];
                push @{$facet->{Options}}, $option_arr;
            } @{$json->{facets}->{$k}->{terms} // []};
            push @facets, $facet;
        }

        # Range facets! We have an elaborate setup for these, get the output from ES and convert it to an appropriate label
        # Then convert it back!
        elsif ( defined( $json->{facets}->{$k}->{ranges} ) ) {
            map {
                if ( $_->{count} ){
                    my $value = join('..', $_->{from_str}, $_->{to_str});
                    my $label = Stream::TP2::AvailabilityDate::range_to_label( $value );
                    my $option_arr = [ $label, $value, $_->{count}  ];
                    push @{$facet->{Options}}, $option_arr;
                }
            } @{$json->{facets}->{$k}->{ranges} // []};
            push @facets, $facet;
        }
    }
    $self->results()->facets( \@facets );

    if (Stream::TP2::ElasticSearch::using_es( $self->talentsearch_client_id() ) ){
        $json = Stream::TP2::ElasticSearch::es_to_solr( $json );
    }


    $self->{'response_highlighting'} = $json->{'highlighting'};
    return @{ $json->{'response'}->{'docs'} };
}

sub scrape_candidate_details {
    my $self = shift;
    my $json = shift;

    # NOOOOO!
    # $log->info("Applicant tags from JSON response is ".Dumper($json->{applicant_tags}));

    $json->{'applicant_name'} =~ s/<br>//gi;


    return (
        #Advert
        advert_id => $json->{'advert_id'},
        advert_salary_per => $json->{'advert_salary_per'},
        advert_salary_currency => $json->{'advert_salary_currency'},
        advert_county => $json->{'advert_county'},
        advert_industry => $json->{'advert_industry'},
        advert_language => $json->{'advert_language'},
        advert_country => $json->{'advert_country'},
        advert_postcode => $json->{'advert_postcode'},
        advert_job_reference => $json->{'advert_job_reference'},
        advert_user_name => $json->{'advert_user_name'},

        advert_salary_from_pw => $json->{'advert_salary_from_pw'},
        advert_office => $json->{'advert_office'},
        advert_job_title => $json->{'advert_job_title'},
        advert_salary_to_pw => $json->{'advert_salary_to_pw'},
        advert_job_type => $json->{'advert_job_type'},
        advert_team => $json->{'advert_team'},
        advert_city => $json->{'advert_city'},

        #Candidate
        candidate_id => $json->{'id'},
        name => $json->{'applicant_name'},
        email => $json->{'applicant_email'},
        channel_id => $json->{'applicant_channel_id'}, # To be displayed as "Source 2: ... " in the interface.
        original_channel_id => $json->{original_applicant_channel_id}, # To be displayed as 'Source 1' in the interface.
        rank => $json->{'applicant_rank'},
        uri => $json->{'uri'},

        cv_text => $json->{'cv_text'},
        cv_url => $json->{'attachment_url'},

        address => $json->{'applicant_address'},
        extracted_postcode => $json->{'applicant_extracted_postcode'},

        longitude => $json->{'applicant_longitude'},
        latitude => $json->{'applicant_latitude'},
        latlon => $json->{'applicant_latlon'},

        telephone => $json->{'applicant_telephone'},
        telephone_norm => $json->{'applicant_telephone_norm'},
        mobile => $json->{'applicant_mobile'},
        mobile_norm => $json->{'applicant_mobile_norm'},

        application_time => $json->{'applicant_application_time'},
        applicant_availability_date => $json->{'applicant_availability_date'},
        applicant_recruitment_status => $json->{'applicant_recruitment_status'},
        applicant_notice_period => $json->{'applicant_notice_period'},

        salary_from => $json->{'applicant_salary_from'},
        salary_to => $json->{'applicant_salary_to'},
        salary_currency => $json->{'applicant_salary_currency'},
        salary_per => $json->{'applicant_salary_per'},

        tags => $json->{'applicant_tags'} // [],

        #Employer 
        employer_org_position_description => $json->{'employer_org_position_description'},
        employer_org_position_end_date => $json->{'employer_org_position_end_date'},
        employer_org_position_start_date => $json->{'employer_org_position_start_date'},
        employer_org_position_title      => $json->{'employer_org_position_title'},
        employer_org_name => $json->{'employer_org_name'},
    );
}

##
## This was implemented in talensearch.pm
## Forget about that for now.
##

# sub scrape_fixup_candidate {
#  ...
# }

# Our own implementation of download profile
sub download_profile{
    my ($self, $candidate) = @_;
    if( my $cv_url = $candidate->attr('cv_url') ){

        my $content = Stream::TP2::DownloadCV::download_url( $candidate->{'cv_url'} );

        my $res = $self->user_object->stream2()->text_extractor()->extract( string => $content,
                                                                            format => 'html' );
        unless( $res->{status} eq 'OK' ){
            confess("Failed to extract HTML from CV for candidate ".$candidate->attr('candidate_id').": ".Dumper($res));
        }
        my $html = $res->{data};

        # HTML is a perl string.

        # Parse it as encoded UTF8 with the html parser.
        # extract the body's children and output them as
        # a join of html strings.

        # load_html loads bytes. Encoding in UTF-8 is always good.
        my $xdoc = $self->{html_parser}->load_html( string => Encode::encode('UTF-8', $html),
                                                    encoding => 'UTF-8'
                                                  );
        $self->cleanup_xdoc_anchors($xdoc);
        
        my ( $xbody ) = $xdoc->findnodes('/html/body');
        unless( $xbody ){
            $candidate->set_attr('cv_html', "No html found");
        }

        $candidate->set_attr('cv_html' , join('', map { $_->toString() } $xbody->childNodes() ));
    }
    return $candidate;
}

#Download CV
sub download_cv_requires_login { 0 }

sub do_download_cv {
    my $self = shift;
    my $candidate = shift;

    # if the candidate does not have a cv_url, we can fetch it resonably quickly from TS
    # this may happen if they are using their internal database as a cache, where we have an ID mapping but the cv_url
    # is a mystery
    if ( ! $candidate->{'cv_url'} && $candidate->candidate_id ){
        $log->info( "Have an ID but no cv_url, fetch candidate from the board" );
        my $solr_ref = Stream::TP2::ElasticSearch::get_by_id( $candidate->candidate_id, $self->talentsearch_client_id() );
        my $cv_url = $solr_ref->{response}->{docs}->[0]->{attachment_url};
        $log->info( sprintf( "got cv_url: %s", $cv_url ) );
        $candidate->{cv_url} = $cv_url;
    }

    # a CV url is required but not found
    if ( ! $candidate->{'cv_url'} ){
        $self->throw_error( sprintf( "Candidate: %s does not have a cv_url", $candidate->id ) );
    }

    my $content = Stream::TP2::DownloadCV::download_url( $candidate->{'cv_url'} );

    $candidate->attr('resume_text' => $content );
    return $self->extract_cv_from_candidate( $candidate );
}

#Helpers
sub jobtype_helper {
    my $self = shift;
    my $new  = shift;
    my $old  = shift;
    return defined($new) && $new ne q{} ? $self->HASH_MAP($new => @_) : $old;
}

=head2 tag_candidate

Update the tags in talent search with new tag.

=cut

sub tag_candidate{
    my ($self, $candidate, $tag) = @_;

    $log->infof("Tagging candidate '%s' with tag '%s' through Talent search tsimport API",
        $candidate->id(),
        $tag->tag_name
    );

    my $ts_client = $self->user_object->stream2->tsimport_api();
    my $company = $self->talentsearch_client_id();
    my $resp = $ts_client->make_request(
        path    => '/company/'.$company.'/candidate/Artirix/'.$candidate->candidate_id.'/tags',
        method  => 'POST',
        arguments => { tag_name => $tag->tag_name }
    );

    $log->infof('Tagged candidate %s with "%s" successfully', $candidate->id(), $tag->tag_name);
    return $tag;
}

=head2 untag_candidate

Remove the tag from the tags in talent search.

=cut

sub untag_candidate{
    my ($self, $candidate, $tag) = @_;

    unless ( $tag ) {
        $log->infof('No tag given when untagging candidate "%s"', $candidate->id())
        and return;
    }

    $log->infof("Untagging candidate '%s' with tag '%s' through Talent search tsimport API",
        $candidate->id(),
        $tag->tag_name
    );

    my $ts_client = $self->user_object->stream2->tsimport_api();

    my $company = $self->talentsearch_client_id();
    my $resp = $ts_client->make_request(
        path    => '/company/'.$company.'/candidate/Artirix/'.$candidate->candidate_id.'/tags/tag?tag_name='.URI::Escape::uri_escape_utf8($tag->tag_name),
        method  => 'DELETE',
        arguments => {}
    );

    $log->infof('Untagged candidate %s from "%s" successfully', $candidate->id(), $tag->tag_name);
    return $tag;
}

=head2 untag_candidate_bulk

Untag every single candidates tagged with this tag.

=cut

sub untag_candidate_bulk{
    my ($self, $tag_name) = @_;

    $tag_name // confess("Missing tag_name to remove");

    unless( $self->user_object() ){
        confess("Missing user object on self");
    }


    my $page = 1 ;
    my $n_on_page = 0;

    my $stream2 = $self->user_object()->stream2();

    do{

        $log->info("Looking up candidates with tag '$tag_name' on page '$page'");

        # Build a new instance of talentsearch to look up all the tagged candidates.
        my $results_collector = Stream2::Results->new({destination => 'talentsearch'});
        $results_collector->current_page($page);
        my $ts_feed = $stream2->build_template('talentsearch',
                                               undef,
                                               {company => $self->user_object->group_identity(),
                                                user_object => $self->user_object()
                                               }
                                              );
        $ts_feed->results($results_collector);

        $ts_feed->token_value('applicant_tags' , $tag_name );
        $ts_feed->run_helpers();
        $ts_feed->search({ page => $page });

        $n_on_page = $results_collector->n_results();

        $log->info("Found $n_on_page candidates on page '$page'. Will untag them all");

        foreach my $candidate ( @{ $results_collector->results() } ){
            $self->untag_candidate( $candidate, $tag_name );
        }

        $page += 1;
    } while( $n_on_page > 0 );

    return;
}


=head2 update_candidate

Updates the given talent search candidate on behalf of this feeds' company or user's company with the
given properties.

Unles the option 'keep_keys'  is given, any key not starting with employer_ , advert_ or applicant_ will
be turned into its applicant_< > flavour.

Usage

   $this->update_candidate($candidate , { foo => 'bar' } );
   # Is the same as:
   $this->update_candidate($candidate , { applicant_foo => 'bar' });
   # Bot not the same as:
   $this->update_candidate($candidate , { foo => 'bar' } , { keep_keys => 1});

=cut

sub update_candidate {
    my ( $self, $candidate, $fields , $opts  ) = @_;

    $opts //= {};

    unless( $opts->{keep_keys} ){
        foreach my $key ( keys %$fields ) {
            # TODO why do we do this mapping in the scrape_candidate?
            # it's probably for consistency actually
            if ( $key !~ m/\A(?:employer|advert|applicant)_/ ){
                $fields->{"applicant_$key"} = delete $fields->{$key};
            }
        }
    }

    my $solr_id = $candidate->candidate_id;
    my $ts_client = Stream2->instance->tsimport_api();

    my $company = $self->talentsearch_client_id();

    my $path = sprintf(q{/company/%s/candidate/%s/update}, $company, $solr_id);
    $log->info("Will update candidate at $path, with Data: ".Dumper($fields));

    my $resp = $ts_client->make_request(
        path    => $path,
        method  => 'POST',
        arguments => $fields
    );

    return ( $resp );
}

=head2 delete_candidate

Delete the given candiate (as a Stream2::Results::Result ) from its talentsearch
using the magic Adcourier API.

The URL to use in the adcourier API is /search/talentsearch/candidate/delete/<candidate ID>

See an example of that at https://bitbucket.org/broadbean/candidate-delete/src/a625ac19620a3a9ae1eb23e734e02625b9921f57/lib/CandidateDelete/Source/TS.pm?at=master

Returns 1 in case of success. Dies miserably in case of failure.

=cut

sub delete_candidate{
    my ($self, $candidate) = @_;

    my $stream2 = Stream2->instance();

    my $solr_id = $candidate->candidate_id() || confess("No candidate_id in candidate");

    # Client ID.
    my $client_id = $self->talentsearch_client_id();

    # Check the given ID really belongs to the client.
    my $params_ref = {
                      'id' => $solr_id,
                     };

    $log->info("Attempting round trip with candidate id = $solr_id into artirix");

    my $ref = Stream::TP2::Search::submit_search( $client_id , $params_ref, { location_api => $stream2->location_api(),
                                                                              user_api => $stream2->user_api(),
                                                                            } );
    unless( keys %$ref ){
        confess("Candidate ID = $solr_id does not belong to company '$client_id'");
    }

    my $todelete_id = $ref->{'response'}->{'docs'}->[0]->{id}; # This was returned by artirix.

    unless( $todelete_id ){
        confess("No artirix ID found in ".Dumper($ref));
    }

    $log->info("Will delete candidate $todelete_id");
    my $adc_api = $stream2->adcourier_api();
    my $response = $adc_api->request( GET => $adc_api->url("/search/talentsearch/candidate/delete/$todelete_id") );
    unless( $response->is_success() ){
        confess("Response was not successfull: ".$response->as_string());
    }
    return 1;
}

1;

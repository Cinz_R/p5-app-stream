#!/usr/bin/env perl

# $Id: careerbuilder.pm 167098 2016-05-09 09:58:41Z aliz $

package Stream::Templates::careerbuilder;

=head1 NAME

Stream::Templates::careerbuilder - Careerbuilder

=head1 AUTHOR

ACJ - 29/09/2008

=head1 CONTACT

Support address:
WebServicesSupport@CareerBuilder.com

Emil.Perera@careerbuilder.com
Custom Solutions - Team Lead @ CareerBuilder

=head1 TESTING INFORMATION

Test User: RDBwsTest@careerbuilder.com
Password: cbwst3st

Or known to be working as of 30th August 2016:

username: aliz@broadbean.com
password: Career2016

=head1 DOCUMENTATION

Use the webservice to get the docs.
Web Service URL
L<http://ws.careerbuilder.com/resumes/resumes.asmx>

Documentation
L<http://ws.careerbuilder.com/schemas>

=head1 OVERVIEW

Searches both CareerBuilder.com and their country sites but predominantly
USA. I suspect we have to send US salary and they return US salary as there are no
currency options. I haven't confirmed this with CareerBuilder.

All posts are XML over HTTP (it is a SOAP interface)

It is session based so we login to start a session and then perform
our search. We don't current cache sessions

CareerBuilder supply an XML profile instead of a CV. They will
provide the CV if it is available.

Locations: China and Greek counties are not mapped

CareerBuilder xml consists of a SOAP request that contains an XML packet.

C<Stream::Agents::CareerBuilder> unpacks the response packet so you don't have
to decode two lots of XML.

=head1 EXTRA INFORMATION

You can test your request against that of the web service here: http://cbrdb-ws.herokuapp.com/

The docs are now part of the webservice.

The resume webservice has 3 methods:

=over 4

=item * BeginSessionV2

=item * V2_AdvancedResumeSearch

=item * V2_GetResume

=back

Each method takes an input "packet" and returns an output "packet".

The format of the input packet is available through the *_ValidFields
methods.

The format of the response package is available through the *_SampleResponse
methods.

These methods can be called in a browser from the webservice.

=head1 MAJOR CHANGES

=over 4

=item * 28/07/10 - ACJ - CareerBuilder are returning a datetime in LastUpdate instead of a date

=item * 25/02/10 - ACJ - Support location_within_km and zipcodes for USA/France/Netherlands

=item * 29/09/09 - ACJ - Careerbuilder supports all countries

Removed domain acl:

andy,i4recruitment,pashleyrecruitment,esandarecruitment,eclypserecruitment,citr

=item * 16/07/09 - ACJ - Updated to V2 methods and uses the AdvancedResumeSearch

=back

=head1 BUG FIXES

=over 4

=item * 25/02/10 - ACJ - Fixed sending salary_from in salary_max field

=back

=cut

use strict;

use base qw(Stream::Engine::API::XML::FVP);

use utf8; # some of the lists have utf8 characters

use Carp;

use Stream::Constants::SortMetrics ':all';
use Stream::Agents::CareerBuilder;
use DateTime;

use Bean::OAuth::CareerBuilder;
use Bean::XML;
use Log::Any qw/$log/;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################

BEGIN {
    $standard_tokens{sort} = {
        Label       =>  'Order By',
        Type        =>  'List',
        Values      =>  'Relevancy=-RELV|Recent Pay=-RECENTYEARLYPAY',
        Default     =>  '-RELV',
        SortMetric  =>  $SM_SORT,
    };

    $standard_tokens{apply_last_activity} = {
        Label       =>  'Resume Freshness',
        Type        =>  'List',
        Values      =>  'Last Activity=True|Last Modified=False',
        Default     =>  'True',
        SortMetric  =>  1,
    };

    $standard_tokens{jobtitle} = {
        Label       => 'Job Title',
        Type        => 'Text',
        Size        =>  60,
        SortMetric  =>  5,
    };

    $standard_tokens{work_status} = {
        Label       => 'Work Status',
        Type        => 'MultiList',
        Values      => '(Any=|US Citizen=CTCT|Have H1 Visa=CTEM|Greencard Holder=CTGR|Need H1 Visa Sponsor=CTNO|TN Permit Holder=EATN|Employment Authorization Document=EAEA|Not Specified=CTNS)',
        SortMetric  =>  10,
    };

    $standard_tokens{min_years_exp} = {
        Label       =>  'Minimum Years of Experience',
        Type        =>  'Text',
        Default     =>  '0',
        Validation  =>  'ForceNumeric',
        SortMetric  =>  15,
    };

    $standard_tokens{max_years_exp} = {
        Label       =>  'Maximum Years of Experience',
        Type        =>  'Text',
        Default     =>  '0',
        Validation  =>  'ForceNumeric',
        SortMetric  =>  20,
    };

    $standard_tokens{minimum_travel_requirements} = {
        Label       =>  'Minimum Travel Requirements',
        Type        =>  'List',
        Values      =>  '(Any=|Negligible=DT3|Up to 25%=DT32|Up to 50%=DT321|Road Warrior=DT3210)',
        SortMetric  =>  25,
    };

    $standard_tokens{currently_employed} = {
        Label       =>  'Currently Employed',
        Type        =>  'List',
        Options     =>  'Any=|Yes=Yes|No=No',
        SortMetric  =>  30,
    };

    $standard_tokens{management_experience} = {
        Label       =>  'Management Experience',
        Type        =>  'List',
        Options     =>  'Any=|Yes=Yes|No=No',
        SortMetric  =>  35,
    };

    $standard_tokens{minimum_employees_managed} = {
        Label       =>  'Minimum employees managed',
        Type        =>  'Text',
        Default     =>  '0',
        Size        =>  10,
        Validation  =>  'ForceNumeric',
        SortMetric  =>  40,
    };

    $standard_tokens{company} = {
        Label       =>  'Company Name',
        Type        =>  'Text',
        Size        =>  60,
        SortMetric  =>  45,
    };

    $standard_tokens{category} = {
        Label       =>  'Job Category',
        Type        =>  'MultiList',
        Values      =>  '(Accounting=JN001|Admin - Clerical=JN002|Automotive=JN054|Banking=JN038|Biotech=JN053|Business Development=JN019|Business Opportunity=JN059|Construction=JN043|Consultant=JN020|Customer Service=JN003|Design=JN021|Distribution - Shipping=JN027|Education=JN031|Engineering=JN004|Entry Level=JN022|Executive=JN018|Facilities=JN017|Finance=JN005|Franchise=JN060|General Business=JN006|General Labor=JN051|Government=JN046|Government - Federal=JN070|Grocery=JN055|Health Care=JN023|Hospitality - Hotel=JN040|Human Resources=JN007|Information Technology=JN008|Installation - Maint - Repair=JN056|Insurance=JN034|Inventory=JN015|Legal=JN030|Legal Admin=JN041|Management=JN037|Manufacturing=JN029|Marketing=JN009|Media - Journalism - Newspaper=JN047|Nonprofit - Social Services=JN058|Nurse=JN050|Other=JN010|Pharmaceutical=JN049|Professional Services=JN024|Purchasing - Procurement=JN016|QA - Quality Control=JN025|Real Estate=JN057|Research=JN026|Restaurant - Food Service=JN035|Retail=JN033|Sales=JN011|Science=JN012|Skilled Labor - Trades=JN013|Strategy - Planning=JN028|Supply Chain=JN014|Telecommunications=JN048|Training=JN032|Transportation=JN044|Veterinary Services=JN069|Warehouse=JN045)',
        Validation  =>  'Select0-5',
        SortMetric  =>  50,
    };

    $standard_tokens{minimum_degree} = {
        Label       =>  'Minimum Degree',
        Type        =>  'List',
        Values      =>  '(Any=|None=CE3|High School=CE31|Vocational=CE30|Associate Degree=CE32|Bachelors Degree=CE321|Masters Degree=CE3210|Doctorate=CE3211)',
        SortMetric  =>  55,
    };

    $standard_tokens{languages} = {
        Label       =>  'Languages Spoken',
        Type        =>  'MultiList',
        Values      =>  '(English=LAEN|Catalán=LACA|Chinese-Mandarin=LAZH|Czech=LACS|Danish=LADA|Dutch=LANL|Estonian=LAET|Euskera=LAEU|Finnish=LAFI|French=LAFR|German=LADE|Greek=LAEL|Hebrew=LAHE|Hungarian=LAHU|Icelandic=LAIS|Italian=LAIT|Japanese=LAJA|Korean=LAKO|Latvian=LALV|Lithuanian=LALT|Norwegian=LANO|Polish=LAPL|Spanish=LAES|Portuguese=LAPT|Romanian=LARO|Russian=LARU|Swedish=LASV|Arabic=LAAR|Bengali=LABN|Chinese-Cantonese=LAHK|Chinese-Taiwanese=LATW|Hindi=LAHI|Urdu=LAUR|Armenian=LAHY|Assamese=LAAS|Farsi=LAFO|Gujarati=LAGU|Kannada=LAKN|Kashmiri=LAKS|Malayalam=LAML|Oriya=LAOR|Pashto=LAPS|Punjabi=LAPA|Sanskrit=LASA|Sindhi=LASD|Tamil=LATA|Telugu=LATE|Turkish=LATR|Uzbek=LAUZ|Indonesian=LAFJ|Vasco=LAVA|Bulgarian=LABU|Croatian=LACR|Macedonian=LAMC|Serbian=LASB|Albanian=LASQ|Cambodian=LACM|Chinese-Chinois=LACH|Ukranian=LAKR|Vietnamese=LAVT|Tagalog=LATL|Thai=LATH|Malay=LAMY|Others=LAOT)',
        Validation  =>  'Select0-3',
        SortMetric  =>  60,
    };

    $standard_tokens{military_experience} = {
        Label       =>  'Military Experience',
        Type        =>  'MultiList',
        Values      =>  '(Any=|Active Duty=M1|Reservist-Drilling=M2|National Guard-Drilling=M3|Inactive Reserve=M4|Inactive National Guard=M5|Retired Military=M6|Veteran=M7)',
        SortMetric  =>  65,
    };

    $standard_tokens{security_clearance} = {
        Label       =>  'Security Clearance',
        Type        =>  'List',
        Values      =>  '(Any=|Yes|No)',
        SortMetric  =>  70,
    };

    $standard_tokens{searchtype} = {
        Label       =>  'Search Pattern',
        Type        =>  'List',
        Values      =>  '(Match All Keywords=ALL|Match Any Keyword=ANY|Match Exact Keywords=EXACT|Match Keywords Using Boolean Logic=BOO)',
        Default     =>  'BOO', # careerbuilder default is ALL, but have advised us to default to BOO, so BOO!
        SortMetric  =>  75,
    };

    $standard_tokens{max_results} = {
        Label       =>  'Number of Results',
        Type        =>  'List',
        Values      =>  '(10 results per page=10|20 results per page=20|25 results per page=25|30 results per page=30|40 results per page=40|50 results per page=50)',
        Default     =>  25,
        SortMetric  =>  80,
    };

#    $standard_tokens{school} = {
#        Label => 'School',
#        Type  => 'Text',
#        Size  => 60,
#        SortMetric => $SM_EDUCATION,
#    };
#
    $standard_tokens{relocation_filter} = {
        Label => 'Relocation filter',
        Type  => 'List',
        Values => '(None=|Relocate State=RS|Relocate City=RC|Relocate Nation=RN|Relocate All=RA)',
        Default => '',
        SortMetric => $SM_LOCATION,
    };
#
#    $standard_tokens{minimum_experience} = {
#        Label => 'Minimum Experience',
#        Type  => 'List',
#        Values => '(Any=|Not specified=RENS|None=RENONE|College=RE32100|Last than 1 year=RE3210|At least 1 year=RE321|At least 3 years=RE32|More than 5 years=RE3)',
#        SortMetric => $SM_EXPERIENCE,
#    };
#
#    $standard_tokens{maximum_commute} = {
#        Label => 'Maximum Commute',
#        Type  => 'Text',
#        Size  => 10,
#        Validation => 'ForceNumeric',
#    };
#
#    $standard_tokens{lemmatize} = {
#        Label => 'Lemmatize - Fuzzy Keyword Searching',
#        Type  => 'List',
#        Values => '(Yes=true|No=false)',
#        Default => 'true',
#    };
#
#    $standard_tokens{exclude_ivr_resumes} = {
#        Label => 'Exclude IVR Resumes',
#        Type  => 'List',
#        Values => '(Any=|Yes|No)',
#        Default => '',
#        SortMetric => $SM_SEARCH,
#    };

    $derived_tokens{city} = {
        Label => 'City',
        Type  => 'Text',
        SortMetric => $SM_LOCATION,
    };

    $derived_tokens{zipcode} = {
        Label => 'US Zipcode/Postal Code',
        Type  => 'Text',
        SortMetric => $SM_LOCATION,
    };

    $derived_tokens{radiusmiles} = {
        Label =>  'Search X miles around the zipcode',
        Type  => 'DistanceList',
        Options => '1 mile=1|2 miles=2|3 miles=3|4 miles=4|5 miles=5|10 miles=10|15 miles=15|20 miles=20|25 miles=25|30 miles=30|40 miles=40|50 miles=50|75 miles=75|100 miles=100|125 miles=125|150 miles=150',
        Unit   => 'miles',
        Helper => 'careerbuilder_location_search(%location_id%|%location_within_miles%)',
        SortMetric => $SM_LOCATION,
    };

    $derived_tokens{state} = {
        Label => 'State/County/Province',
        Type  => 'Text',
        SortMetric => $SM_LOCATION,
    };

    $derived_tokens{country} = {
        Label => 'Country',
        Type  => 'Text',
        Example => 'e.g. GB',
        SortMetric => $SM_LOCATION,
    };

    $derived_tokens{salary_cur} = {
        Label        => 'Currency',
        Type         => 'Currency',
        Options      => 'USD',
        Helper       => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric   => $SM_SALARY,
    };

    $derived_tokens{salary_per} = {
        Label   => 'Salary Per',
        Type    => 'SalaryPer',
        Options => 'annum|hour',
        Default => 'annum',
        Helper  => 'SALARY_PER(salary_per|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric => $SM_SALARY,
    };

    $posting_only_tokens{jobtypes} = {
        Helper => 'careerbuilder_jobtype_helper(%default_jobtype%)',
    };

    $posting_only_tokens{compensation_type} = {
        Helper => 'HASH_MAP(%salary_per%|annum=SALR|hour=HOUR)',
    };

    $posting_only_tokens{exclude_resumes_with_no_salary} = {
        Helper => 'careerbuilder_resumes_with_no_salary(%salary_unknown%|%include_unspecified_salaries%)',
    };

    $posting_only_tokens{languages_spoken} = {
        Helper => 'MULTILIST_COMMA(%languages%)',
    };

    $posting_only_tokens{military_experience_csv} = {
        Helper => 'MULTILIST_COMMA(%military_experience%)',
    };

    $posting_only_tokens{work_status_csv} = {
        Helper => 'MULTILIST_COMMA(%work_status%)',
    };

    $posting_only_tokens{categories} = {
        Helper    => 'MULTILIST_COMMA(%category%)',
    };

    $posting_only_tokens{freshnessdays} = { #Their docs are wrong, we just need to send through a number of days
        Helper => 'careerbuilder_cv_updated_days(%cv_updated_within%)', 
    };

    $authtokens{careerbuilder_email} = {
        Label => 'Email Address',
        Type  => 'Text',
        Size  => 25,
        SortMetric => 1,
    };

    $authtokens{careerbuilder_password} = {
        Label => 'Password',
        Type  => 'Text',
        Size  => 25,
        SortMetric => 2,
    };
};
############### HELPERS ##########################

sub careerbuilder_cv_updated_days {
    my ($self, $value) = @_;
    # Use 9999 for 'ALL'
    my $freshness = $self->CV_UPDATED_WITHIN_TO_DAYS($value) // 9999;
    # CB wants 1 for 'Today' instead of zero and so forth
    return $freshness + 1;
}

sub careerbuilder_default_country_code {
    return 'US';
}

sub careerbuilder_location_search {
    my $self = shift;
    my $location_id = shift or return;
    my $location_within = shift;

    # careerbuilder support 3 locations
    # city="city 1,city 2,city3"
    # state="state 1,state 2,state 3"
    #
    # etc.
    #
    # we only support one

    # lets see what they have chosen
    if ($location_id) {
        my $location = $self->location_api()->find($location_id);
        if (! $location ) {
            $self->log_warn('Unable to fetch location [%s] from |DB', $location_id);
        }

    # always send the country
    my $country = $location->iso_country();

    my $county = $location->get_mapping('stream_careerbuilder_counties');
    my $city = $location->name();

    if ($country eq 'GB'){ #19852
        $country = 'UK';
    }

    if ( $country ) {
        $self->log_debug('location_search> sending country [%s]', $country );
        $self->token_value( 'country' => $country );
    }
    else {
        $self->log_debug('location_search> could not find which country %s is in', $location->name() );
        return;
    }

    # try to send the zipcode
    if ($country eq 'UK') {
        #special case - we want to use new location helpers for UK postcodes
        my $user_pcode = $self->token_value('location_id_postcode');
        my $postcode = $self->LOCATION_FULL_POSTCODE($location_id, $user_pcode);
        if ($postcode) {
            $self->token_value( 'zipcode' => $postcode );
            return $self->careerbuilder_location_within( $location_within );
        }
    }

    if ($country eq 'CA') {
	# zipcodes are not that great for CA locations right now - do a state/city search until its more mature
        $self->token_value( 'state' => $county );
        $self->log_debug('location_search> this is a Canadian search - setting state [%s]', $county);
        if ( $location->type =~ m/place/ ) {
            $self->token_value( 'city' => $city );
            $self->log_debug('location_search> this is a Canadian search - setting city [%s]', $city);
        }
        return $self->careerbuilder_location_within( $location_within );
    }

    my $zipcode = $location->zipcode();
    if ( $zipcode ) {
        # send radiusmiles and zipcode
        $self->log_debug('location_search> sending zipcode [%s]', $zipcode );
        $self->token_value( 'zipcode' => $zipcode );
        return $self->careerbuilder_location_within( $location_within );
    }

    if ( $county ) {
        $self->log_debug('location_search> sending county [%s]', $county );
        $self->token_value( 'state' => $county );
    }

    # state + city work together (you can't just send the state)
    if ( !$location->is_place() ) {
        $self->log_debug('location_search> not supported for this location');
        return;
    }

    $self->log_debug('location_search> sending location city [%s]', $city );
    $self->token_value( 'city' => $city );

    return $self->careerbuilder_location_within( $location_within );
    }
}

sub careerbuilder_location_within {
    my $self = shift;
    my $location_within = shift;

    my $distance = $self->build_token('radiusmiles');

    my $closest_option = $distance->first_greater_than( $location_within );

    return $closest_option;
}

sub careerbuilder_jobtype_helper {
    my ($self, @jobtypes) = @_;
    my %jobtype_maps = ( 'permanent' => 'ETFE', 'contract' => 'ETCT', 'temporary' => 'ETTS' );

    @jobtypes = grep { $_ ne '' } @jobtypes;
    for (my $i = 0; $i <= $#jobtypes; $i++) {
        foreach ( keys %jobtype_maps ) {
            $jobtypes[$i] = $jobtype_maps{$jobtypes[$i]} if ($jobtypes[$i] eq $_ );
        }
    }
    
    return join('|', @jobtypes);
}

sub careerbuilder_resumes_with_no_salary {
    my $self = shift;
    my $salary_unknown = shift || 0;
    my $V2_include_salary = shift || 0;

    if ( $salary_unknown || $V2_include_salary ) {
        # No - do not exclude resumes with no salary
        return 'No';
    }
    
    # Yes - exclude resumes with no salary
    return 'Yes';
}

######### END OF HELPERS #########

sub AGENT_CLASS {
    return 'Stream::Agents::CareerBuilder';
}

sub config {
    my $self = shift;
    # hardcode 'careerbuider' so child feeds use it too, as
    # opposed to $self->destination
    return $self->feed_config('careerbuilder');
}

sub base_url {
    my $self = shift;
    return $self->config->{base_url};
}

sub login_url {
    my $self = shift;
    return $self->base_url() . '/resumes/resumes.asmx/BeginSessionV2';
}

sub search_url {
    my $self = shift;
    return $self->new_search_url if $self->_use_cb_oauth_flow;
    return $self->base_url() . '/resumes/resumes.asmx/V2_AdvancedResumeSearch';
}

sub download_profile_url {
    my $self = shift;
    return $self->new_profile_url if $self->_use_cb_oauth_flow;
    return $self->base_url() . '/resumes/resumes.asmx/V2_GetResume';
}

sub download_cv_url {
    my $self = shift;
    return $self->download_profile_url();
}

sub new_base_url {
    my $self = shift;
    return $self->config->{new_url} . '/corporate/rdb';
}

sub new_search_url { return $_[0]->new_base_url() . '/V2_AdvancedResumeSearch'; }
sub new_profile_url { return $_[0]->new_base_url() . '/V2_GetResume'; }

############### LOGIN ############################
sub login_failed {
    return 'CBError';
}

sub login_build_xml {
    my $self = shift;
    return $self->careerbuilder_beginsession_packet();
}

sub login_content_type {
    return 'application/x-www-form-urlencoded';
}

sub login_xml_param {
    return "Packet";
}

sub careerbuilder_beginsession_packet {
    my $self = shift;
    my $xml  = Bean::XML->new();

    $xml->startTag('Packet');
        $xml->startTag('Email');
            $xml->characters( $self->token_value('email') );
        $xml->endTag('Email');
        $xml->startTag('Password');
            $xml->characters( $self->token_value('password') );
        $xml->endTag('Password');
    $xml->endTag('Packet');

    return $xml->asString();
}

=head2 login

Overriding

=cut

sub login{
    my ($self, @rest) = @_;

    if($self->_use_cb_oauth_flow) {
        # we skip the login phase
        return;
    }
    my $cached_session_key = 'careerbuilder_sessiontoken';
    if( my $session_value =  $self->get_account_value( $cached_session_key ) ){
        $log->info("Got session token from cache. Setting it against this and skipping all login bit");
        $self->session( $session_value );
        return;
    }
    $log->info("No $cached_session_key cache found. Loggin in as usual");
    my $ret = $self->login_submit();
    unless( $self->session() ){
        confess("Session $cached_session_key is supposed to be set by the whole login magic.");
    }
    # Remember the session token for 2 hours - 5 minutes
    $self->set_account_value( $cached_session_key , $self->session() , 6900 );

    # Return what the real login method has returned.
    return $ret;
}

sub login_submit {
    my $self = shift;

    if ( $self->logged_in() ) {
        return;
    }

    my $login_url = $self->login_url;

    if ( !defined( $login_url ) ) {
        $self->log_info( 'no login page - assuming no logged_in = true (no login needed)' );
        $self->{logged_in} = 1;
        return 1;
    }

    if ( !$self->can('login_build_xml') ) {
        $log->info( 'no login xml - assuming no login needed' );
        $self->{logged_in} = 1;
        return 1;
    }

    my @login_content = $self->login_content();

    my $http_method = uc('POST');

    my @request = (
        $self->login_url(),
        Content_Type => $self->login_content_type(),
        Content      => \@login_content,
    );

    # Assume HTTP post
    $self->login_post( @request );

    $self->check_for_login_error();

    $self->login_callback();

    $self->{logged_in} = 1;

    $log->debug('logged in');
}

sub login_content {
    my $self = shift;

    my $xml_param = $self->login_xml_param();

    my $xml = $self->xml_to_string( $self->login_build_xml() );

    if ( !$xml ) {
        return $self->throw_error('No xml to send to the board');;
    }

    return ( $xml_param => $xml );
}

sub login_callback {
    my $self = shift;

    return 1 if $self->_use_cb_oauth_flow;

    $log->debug('extracting careerbuilder session token');

    my $xpath_session = '//SessionToken[1]/text()';
    $log->debug('xpath is: %s', $xpath_session );

    my ($session_token) = $self->findvalue($xpath_session);

    $log->info('careerbuilder session token is: %s', $session_token);

    if ( !$session_token || $session_token eq 'Invalid' ) {
        die "Unable to login: no session token";
    }

    # Inject the session token in myself for the rest of the game.
    $self->session( $session_token );

    return 1;
}

sub session {
    my $self = shift;

    unshift @_, 'careerbuilder_sessiontoken';

    return $self->token_value(@_);
}

sub default_set_headers {
    my $self = shift;

    return $self->_auth_header() if $self->_use_cb_oauth_flow;
    return $self->SUPER::default_set_headers(@_);
}

############### SEARCH PAGE #######################
sub search_failed {
    return 'CBError';
}

sub search_build_xml {
    my $self = shift;

    return $self->careerbuilder_advancedresume_search_packet();
}

sub search_xml_param {
    return "Packet";
}

sub careerbuilder_advancedresume_search_packet {
    my $self = shift;
    my $xml  = Bean::XML->new();

    $xml->startTag('Packet');
        if(!$self->_use_cb_oauth_flow) {
            $xml->startTag('SessionToken');
                $xml->characters( $self->session() );
            $xml->endTag('SessionToken');
        }

        $xml->startTag('Keywords');
            $xml->characters( $self->token_value('keywords') );
        $xml->endTag('Keywords');

        $xml->startTag('SearchPattern');
            $xml->characters( $self->token_value_or_default('searchtype') );
        $xml->endTag('SearchPattern');

        $xml->startTag('JobCategories');
            $xml->characters( $self->token_value('categories') );
        $xml->endTag('JobCategories');

        $xml->startTag('City');
            $xml->characters( $self->token_value('city') );
        $xml->endTag('City');

        $xml->startTag('ZipCode');
            # 5 digit US zipcode OR non-us postal code
            $xml->characters( $self->token_value('zipcode') );
        $xml->endTag('ZipCode');

        $xml->startTag('State');
            # 2-3 letter state/province/region code
            $xml->characters( $self->token_value('state') );
        $xml->endTag('State');

        $xml->startTag('Country');
            # 2 letter country code
            $xml->characters( $self->token_value('country') || $self->careerbuilder_default_country_code() );
        $xml->endTag('Country');

        $xml->startTag('SearchRadiusInMiles');
            $xml->characters( $self->token_value('radiusmiles') );
        $xml->endTag('SearchRadiusInMiles');

        $xml->startTag('RelocationFilter');
            $xml->characters( $self->token_value('relocation_filter') );
        $xml->endTag('RelocationFilter');

        $xml->startTag('FreshnessInDays');
            $xml->characters( $self->token_value('freshnessdays') );
        $xml->endTag('FreshnessInDays');

        $xml->startTag('EmploymentType');
            $xml->characters( $self->token_value('jobtypes') || '' );
        $xml->endTag('EmploymentType');

#        $xml->startTag('MinimumExperience');
#           $xml->characters( $self->token_value('minimum_experience') || '' );
#        $xml->endTag('MinimumExperience');

        $xml->startTag('MinimumTravelRequirement');
           $xml->characters( $self->token_value('minimum_travel_requirements') );
        $xml->endTag('MinimumTravelRequirement');

        $xml->startTag('MinimumDegree');
            $xml->characters( $self->token_value('minimum_degree') );
        $xml->endTag('MinimumDegree');
        
        $xml->startTag('CBMinimumExperience', 'DataType'=>'Integer', 'Restriction'=>'99');
            $xml->characters( $self->token_value_or_default('min_years_exp') );
        $xml->endTag('CBMinimumExperience');
    
        $xml->startTag('CBMaximumExperience', 'DataType'=>'Integer', 'Restriction'=>'99');
            $xml->characters( $self->token_value_or_default('max_years_exp') );
        $xml->endTag('CBMaximumExperience');

        $xml->dataElement(
            'CompensationType' => $self->token_value('compensation_type'),
        );
        $xml->dataElement(
            'MinimumSalary' => $self->token_value('salary_from'),
        );
        $xml->dataElement(
            'MaximumSalary' => $self->token_value('salary_to'),
        );
        $xml->dataElement(
            'ExcludeResumesWithNoSalary' => $self->token_value('exclude_resumes_with_no_salary'),
        );

        $xml->dataElement(
            'LanguagesSpoken' => $self->token_value('languages_spoken'),
        );

        $xml->dataElement(
            'CurrentlyEmployed' => $self->token_value('currently_employed'),
        );

        $xml->dataElement(
            'ManagementExperience' => $self->token_value('management_experience'),
        );

        $xml->dataElement(
            'MinimumEmployeesManaged' => $self->token_value_or_default('minimum_employees_managed'),
        );

#        $xml->dataElement(
#            'MaximumCommute' => $self->token_value('maximum_commute'),
#        );

        $xml->dataElement(
            'SecurityClearance' => $self->token_value('security_clearance'),
        );

        $xml->dataElement(
            'WorkStatus' => $self->token_value('work_status_csv'),
        );

#        $xml->dataElement(
#            'ExcludeIVRResumes' => $self->token_value('exclude_ivr_resumes'),
#        );

        $xml->dataElement(
            'OrderBy' => $self->token_value_or_default('sort'),
        );

        $xml->startTag('PageNumber');
            $xml->characters( $self->current_page() );
        $xml->endTag('PageNumber');

        $xml->startTag('RowsPerPage');
            $xml->characters( $self->results_per_page() );
        $xml->endTag('RowsPerPage');

#        $xml->emptyTag('CustAcctCode');

#        $xml->emptyTag('CustomXML');

        $xml->dataElement(
            'MilitaryExperience' => $self->token_value('military_experience_csv'),
        );

#        $xml->dataElement('Lemmatize' => $self->token_value('lemmatize'));

        $xml->dataElement(
            'JobTitle' => $self->token_value('jobtitle'),
        );

        $xml->dataElement(
            'Company' => $self->token_value('company'),
        );

#        $xml->dataElement(
#            'School' => $self->token_value('school'),
#        );
        $xml->dataElement(
            'ApplyLastActivity' => $self->token_value_or_default('apply_last_activity'),
        );

        $xml->dataElement(
            'RemoveDuplicates' => 'True',
        );

        if ( my $ofccp_context = $self->token_value('ofccp_context') ) {
            $xml->dataElement(
                DataStoreLabel => $ofccp_context
            );
        }

        my (@niche_sites) = $self->careerbuilder_niche_sites();
        if ( @niche_sites ) {
            $xml->dataElement(
                'NicheInclusion' => join(',', @niche_sites),
            );
        }

    $xml->endTag('Packet');

    return $xml->asString();
}

sub careerbuilder_niche_sites { return (); }

sub scrape_candidate_xpath { return '//ResumeResultItem_V3'; }
sub scrape_candidate_details {
    return (
        email             => './ContactEmail/text()',
        name              => './ContactName/text()',
        location_text     => './HomeLocation/text()',
        raw_last_updated  => './LastUpdate/text()',
        raw_last_activity => './LastActivity/text()',
        candidate_viewed  => './ActionType/text()',
        resume_title      => './ResumeTitle/text()',
        employer          => './RecentEmployer/text()',
        jobtitle          => './RecentJobTitle/text()',
        salary            => './RecentPay/text()',
        candidate_id      => './ResumeID/text()',
        snippet           => './Teaser/text()',
        user_id           => './UserDID/text()',
        contact_email_md5 => './ContactEmailMD5/text()',
        highest_degree    => './HighestDegree/text()',
    );
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    my $email = $candidate->attr('contact_email');

    if ( $email !~ m/\@careerbuilder/ ) {
        $candidate->attr('email' => $email);
    }

    my $snippet = $candidate->attr('snippet');
    if ( $snippet ) {
        # careerbuilder highlight keywords with <b> tags
        # convert <b> tags to <em> tags
        $snippet =~ s{<b>}{<em>}g;
        $snippet =~ s{</b>}{</em>}g;
        
        # remove the trailing newline from the snippet
        $snippet =~ s{<br>}{}g;

        # save the new snippet
        $candidate->attr('snippet' => $snippet );
    }

    my $raw_last_updated = $candidate->attr('raw_last_updated');
    if ( $raw_last_updated ) {
        # before 27th July 2011 CareerBuilder returned a date
        # after 27th July 2011 CareerBuilder returned an awkward datetime
        # supporting both formats in case CareerBuilder change it without telling us again
        if ( $raw_last_updated =~ m/^\d{4}/ ) {
            # original date format returned by careerbuilder
            # the raw date comes back in a YYYY/MM/DD format
            $candidate->cv_last_updated_from_date( $raw_last_updated );
        }
        else {
            # current date format returned by careerbuilder
            # eg. 6/20/2011 8:35:57 PM
            $candidate->cv_last_updated_format( '%m/%d/%Y %I:%M:%S %p', $raw_last_updated );
        }
    }

    $candidate->has_chargeable_profile(1);

    return $candidate;
}

sub search_number_of_results_xpath { return '//Hits[1]/text()'; }

sub search_number_of_pages_xpath { return '//MaxPage[1]/text()'; }

#################### DOWNLOAD CV ##############

sub download_profile_xml {
    my $self = shift;
    my $candidate = shift;
    return $self->careerbuilder_getresume_packet($candidate);
}

sub download_cv_xml {
    my $self = shift;
    my $candidate = shift;
    $self->login();

    return $self->careerbuilder_getresume_packet($candidate);
}

sub download_cv_xml_param {
    return "Packet";
}

sub download_profile_xml_param {
    return "Packet";
}

sub careerbuilder_getresume_packet {
    my $self = shift;
    my $candidate = shift;

    my $xml = Bean::XML->new();
    $xml->startTag('Packet');
        if(!$self->_use_cb_oauth_flow) {
            $xml->startTag('SessionToken');
                $xml->characters($self->session());
            $xml->endTag('SessionToken');
        }
        $xml->startTag('ResumeID');
            $xml->characters($candidate->candidate_id());
        $xml->endTag('ResumeID');
        $xml->emptyTag('CustAcctCode');
        $xml->dataElement(
            'GetWordDocIfAvailable' => 'True',
        );
    $xml->endTag('Packet');

    return $xml->asString();
}

sub download_profile {
    my $self = shift;
    my $candidate = shift;
    if ($candidate->profile_downloaded()) {
        return;
    } else {
        return $self->SUPER::download_profile($candidate, @_);
    }
}

sub do_download_cv {
    my $self = shift;
    my $candidate = shift;

    if ($candidate->profile_downloaded()) {
        return $self->extract_cv_from_candidate($candidate);
    } else {
        return $self->SUPER::do_download_cv($candidate, @_);
    }
}

sub scrape_generic_download_cv {
    my ($self, $candidate) = @_;
    $self->SUPER::scrape_generic_download_cv($candidate);
    if( ! $candidate->attr('resume_text') && ! $candidate->attr('resume_word_base64') ) {
        $self->throw_cv_unavailable('No CV available');
    }

    return $candidate;
}

sub scrape_download_cv_xpath { return '//Packet'; }
sub scrape_download_profile_xpath { return '//Packet'; }
sub scrape_download_profile_details { return scrape_download_cv_details(); }

sub scrape_download_cv_details {
    return (
# already have these from the search results
#         candidate_id    => './ADC:ResumeID/text()',
#         name            => './ADC:ContactName/text()',
#         location_text   => './ADC:HomeLocation/text()',
#         resume_title    => './ADC:ResumeTitle/text()',
#         salary          => './ADC:RecentPay/text()',
#         cv_last_updated => './ADC:LastUpdate/text()',
        email               => './ContactEmail/text()',
        phone               => './ContactPhone/text()',

        location_city       => './HomeLocation/City/text()',
        location_state      => './HomeLocation/State/text()',
        location_country    => './HomeLocation/Country/text()',
        location_zipcode    => './HomeLocation/ZipCode/text()',
        location_workstatus => './HomeLocation/WorkStatus/text()',

        relocations => [
            './Relocations/ExtLocation' => {
                city       => './City/text()',
                state      => './State/text()',
                zipcode    => './ZipCode/text()',
                country    => './Country/text()',
                workstatus => './WorkStatus/text()',
            },
        ],

        max_commute_miles    => './MaxCommuteMiles/text()',
        travel_preference    => './TravelPreference/text()',
        currently_employed   => './CurrentlyEmployed/text()',

        recent_salary_amount => './MostRecentPay/Amount/text()',
        recent_salary_per    => './MostRecentPay/Per/text()',

        desired_salary_amount => './DesiredPay/Amount/text()',
        desired_salary_per    => './DesiredPay/Per/text()',

        desired_job_types     => [
            './DesiredJobTypes/string/text()',
        ],

        most_recent_title     => './MostRecentTitle/text()',
        experience_months     => './ExperienceMonths/text()',

        management_experience => './Management/ManagedOthers/text()',
        management_number_managed => './Management/NumberManaged/text()',

        jobs_last_three_years  => './JobsLastThreeYears/text()',
        last_job_tenure_months => './LastJobTenureMonths/text()',
        security_clearance     => './SecurityClearance/text()',
        felony_convictions     => './FelonyConvictions/text()',

        highest_degree            => './HighestDegree/text()',
        certifications            => './Certifications/text()',
        motivation_to_change_jobs => './MotivationToChangeJobs/text()',
        employment_type           => './EmploymentType/text()',

        languages                 => [
            './Languages/string/text()',
        ],

        desired_shift_preferences => [
            './DesiredShiftPreferences/string/text()',
        ],

        interests => [
            './Interests/ExtInterest' => {
                interest => './Interest/text()',
                experience_months => './ExperienceMonths/text()',
                jobtitles => [
                    './JobTitles/string/text()',
                ],
            },
        ],

        resume_text        => './ResumeText',
        resume_word_base64 => './OriginalWordDoc/Base64Data/text()',
        resume_filename    => './OriginalWordDoc/FileName/text()',

        military_experience => './MilitaryExperience/text()',

        workhistory => [
            './WorkHistory/ExtCompany' => {
                company  => './CompanyName/text()',
                jobtitle => './JobTitle/text()',
                tenure   => './Tenure/text()',
            },
        ],

        educationhistory => [
            './EducationHistory/ExtSchool' => {
                school          => './SchoolName/text()',
                major           => './Major/text()',
                degree          => './Degree/text()',
                graduation_date => './GraduationDate/text()',
            },
        ],
    );
}

sub after_scrape_download_cv_success {
    my $self = shift;
    my $candidate = shift;

    $candidate->has_profile( 1 );
    $candidate->profile_downloaded(time);

    if ( !$candidate->email() ) {
        my $email = $candidate->attr('contact_email');
        if ( $email !~ m/\@careerbuilder/ ) {
            $candidate->attr('email' => $email);
        }
    }

    return $candidate;
}

sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    my $error_code;
    my $error_type;

    if ( $content =~ m{<CBError>\s*<Code>(.+?)</Code>\s*<Text>(.+?)</Text>} ) {
        $error_code = $1;
        $error_type = $2;
    }
    elsif( $content =~ m{<Error>(.+?)</Error>} ) {
        $error_type = $1;
    }

    if ( $error_type =~ m/could not be validated/ ) {
        return $self->throw_login_error( $error_type );
    }

    if ( $error_type =~ m/Not associated with an account that has RDB Web Service access/ ) {
        return $self->throw_no_cvsearch( $error_type );
    }

    if ( $content =~ m/You do not have permission to view this directory or page using the credentials that you supplied/ ) {
        return $self->throw_login_error('Login credentials are incorrect');
    }

    if ( $error_type ) {
        return $error_type;
    }
    
    return undef;
}

################ SSO #########################

sub _use_cb_oauth_flow {
    my $self = shift;
    return $self->token_value('cb_user_code') ? 1 : 0;
}

sub _auth_header {
    my $self = shift;
    return (Authorization => 'Bearer ' . $self->_oauth_bearer_token);
}

# Returns a fresh oauth token
sub _oauth_bearer_token {
    my $self = shift;

    $self->{auto_token} //= $self->_new_autotoken;

    return $self->{auto_token}->token;
}

sub _new_autotoken {
    my $self = shift;

    my $config = $self->config;

    my %oauth_param = (
        client_id      => $config->{oauth}->{client_id},
        client_secret  => $config->{oauth}->{client_secret},
        service_region => $config->{oauth}->{service_region},
    );
    if( $self->token_value('cb_user_code') ) {
        $oauth_param{auth_code} = $self->token_value('cb_user_code');
        $oauth_param{redirect_uri} = $config->{oauth}->{redirect_uri};
    }
    my $auth_provider = Bean::OAuth::CareerBuilder->new(%oauth_param);

    my $auto_token = $auth_provider->get_auto_token(
        $self->user_object->stream2->stream2_cache()
    );
    return $auto_token;
}

1;
# vim: expandtab tabstop=4

package Stream::Templates::Stadvacaturebank;

use strict;
use warnings;

use Stream2::Feeds::FacetList;
use Stream2::Feeds::Facet::List;
use Stream2::Feeds::Facet::Text;
use Stream2::Feeds::Facet::Checkbox;

use Stream2::Translations qw(t9n t9np);

use base 'Stream::Engine::Composition';

use URI;
use Log::Any qw/$log/;

use Stream::Constants::SortMetrics ();

# Extend and use to specify the site in use.
# The config is the only change between sites
sub stadvacaturebank_site_key {
    ...
}

# Get our specific site's config
sub feed_config {
    my ($self) = @_;
    return $self->stream2->config->{feeds}->{stadvacaturebank}->{
        $self->stadvacaturebank_site_key
    };
}

# The board base url
sub base_url {
    my ($self) = @_;
    return URI->new( $self->feed_config->{base_url} );
}

# URL to query with search_fields() as GET params
sub search_url {
    my ($self) = @_;
    return URI->new( $self->feed_config->{search_url} );
}

# Hash of GET params for the search query
sub search_fields {
    my ($self) = @_;

    my %fields;

    $fields{what} = $self->facets->get('keywords')->value;

    my %keys = $self->_param_keys;
    while( my ($facet_key, $param_key) = each %keys) {
        my %params = $self->_list_param(
            $param_key,
            $self->facets->get($facet_key)->selected_values
        );
        %fields = (%fields, %params);
    }

    $fields{driverlicense} = $self->facets->get('driving_licence')->value;

    my $updated = $self->facets->get('cv_updated_within')->mapped_value(
        'ALL'       => '',
        'TODAY'     => 'd1',
        'YESTERDAY' => 'd2',
        '3D'        => 'd3',
        '1W'        => 'w1',
        '2W'        => 'w2',
        '1M'        => 'm1',
        '2M'        => 'm2',
        '3M'        => 'm3',
        '6M'        => 'm6',
        '1Y'        => 'm6',
        '2Y'        => 'm6',
        '3Y'        => 'm6'
    );
    $fields{age} = $updated;

    my $sort_value = ($self->facets->get('sort')->selected_values)[0];
    if($sort_value) {
        ($fields{sorton}, $fields{sorttype}) = split(',', $sort_value);
    }

    return %fields;
}

# Returns a hash to add to GET params.
# Turns $key with multiple @values into multiple GET params
# Each with a single value
sub _list_param {
    my ($self, $key, @values) = @_;
    my $index = 0;
    return map {
        my ($option_key, $value) = split(',', $_);
        $key . '[' . $option_key . ']' => $value;
    } @values;
}

# A hash of internal and external keys for dynamic parameters
sub _param_keys {
    return (
        # internal_key => external_board_key
        experience => 'exper',
        jobtype    => 'flexs',
        education  => 'levels',
        provinces  => 'provinces',
    );
}

sub _search {
    my ($self, $options) = @_;

    my $results = $options->{results};

    my %fields = $self->search_fields;
    $fields{pnr} = $options->{page} if $options->{page};

    my $url = $self->search_url;
    $url->query_form(%fields);
    $self->mech->get($url);

    my $doc = XML::LibXML->load_html(
        string => $self->mech->content,
        recover => 2
    );

    my $scraped_nodes = $self->scraper->scrape_node(
        $doc,
        $self->result_scrape_instructions,
    )->[0];

    my $scrape_results = $self->scraper->finalize($scraped_nodes);

    $results->max_pages($scrape_results->{max_pages});

    $scrape_results->{total_results} =~ m/^([\d+.]+)/;
    my $total_results = $1;
    $total_results =~ s/\.//g;
    $log->info('Total candidates: ' . $total_results);
    $results->total_results($total_results);

    foreach my $attributes (@{$scrape_results->{candidates}}) {

        my $candidate = $self->new_candidate;

        $candidate->attrs(%$attributes);

        my $profile_url = URI->new($candidate->attr('profile_url'));
        my $id = (grep {$_} $profile_url->path_segments)[-1];
        $candidate->has_profile(1);
        $candidate->has_cv(1);

        $candidate->candidate_id( $id );

        $results->add_candidate($candidate);
    }

    # Scrape facet counts
    my %keys = $self->_param_keys;
    while( my($token_key, $input_name) = each %keys) {

        my $facet = $self->facets->get($token_key);
        my @options = @{$facet->options};
        for my $option (@options) {
            my ($option_key, $value) = (split ',', $option->value);
            my $input_id = substr($input_name, 0, 1) . $value;

            my $count = $doc->find('string(id("' . $input_id . '")/following-sibling::label/span/text())');
            if(!$count) {
                next;
            }

            $count = $count->value;
            $count =~ s/\(|\)//g;

            $option->count($count);
        }

    }


    my $tokens = $self->facets->to_standard_tokens;
    $results->facets([ values $tokens ]);
    return $results;
}

sub result_scrape_instructions {
    my ($self) = (@_);
    return {
        candidates => {
            _path             => 'id("rightCntr")/div[@class="latestBox"]/ul/li/div',
            headline          => 'normalize-space(./h3[1]/a/text())',
            bio               => 'normalize-space(./p/text())',
            last_updated      => 'normalize-space(./div[@class="metainfo" and contains(./em/text(), "Laatste update CV")]/b/text())',
            location          => 'normalize-space(./div[@class="metainfo" and contains(./em/text(), "Plaats")]/b/text())',
            education         => 'normalize-space(./div[@class="metainfo" and contains(./em/text(), "Opleiding")]/b/text())',
            desired_job_title => 'normalize-space(./div[@class="metainfo" and contains(./em/text(), "Gewenste functie")]/b/text())',
            seniority         => 'normalize-space(./div[@class="metainfo" and contains(./em/text(), "Werkniveau")]/b/text())',
            experience        => 'normalize-space(./div[@class="metainfo" and contains(./em/text(), "Werkervaring")]/b/text())',
            profile_url       => 'string(./h3[1]/a/@href)',
            # job_types is instead a list of 'b' elements
            job_types         => './div[@class="metainfo" and contains(./em/text(), "Dienstverband")]/b/text()',
        },
        max_pages => 'string(.//div[@class="paging"]/center/span[@class="numbers"]/span[position()=last()])',
        total_results => 'normalize-space(id("rightCntr")/div[@class="latestBox"]/span[@class="title"]/text())',
    };
}

sub _login_url {
    my ($self) = @_;
    my $url = $self->base_url;
    $url->path_segments(
        $url->path_segments,
        'inloggen',
        ''
    );
    $url->query_form(cl => 1);
    return $url;
}

sub _login_params {
    my ($self) = @_;
    return {
        username => $self->facets->value_of('email'),
        password => $self->facets->value_of('password'),
        #email => 'alexr@broadbean.com',
        #password => 'mble9693',
        autologinvac => '1',
        loginvac => 'Inloggen',
    };
}

sub _login {
    my ($self) = @_;
    $log->info('Logging in');
    my $agent = $self->mech;
    my $previous_max_redirect = $agent->max_redirect;
    $agent->max_redirect(0); #Skip unnecessary redirect request.
    my $response = $self->mech->post(
        $self->_login_url,
        $self->_login_params
    );
    $agent->max_redirect($previous_max_redirect);
    return $self->validate_login($response);
}

sub _logout {
    my ($self) = @_;
    my $url = $self->base_url;
    $url->path_segments('index.php');
    $url->query_form(
        page => 'logout',
        target => 1
    );
    $self->mech->get($url);
}
# Validate our login using the response from our login request
# Verifies a login via the redirect header
# Dies if it doesn't not verify
sub validate_login {
    my ($self, $response) = @_;
    $log->info('Validating login');

    # This header is given for a valid login
    my $given_redirect = URI->new($response->headers->header('Location'));

    my $expected_redirect = $self->base_url;
    $expected_redirect->path_segments('login', 'beheer', '');

    $log->debugf(
        "Comparing given login redirect with expected. Respectively:\n%s\n%s",
        $given_redirect->as_string,
        $expected_redirect->as_string,
    );

    if($given_redirect->eq($expected_redirect)) {
        $log->info('Login successful');
        return 1;
    }

    if($response->decoded_content =~ m/Logingegevens zijn niet correct/
    || $response->decoded_content =~ m/is verplicht/) {
        $self->throw_login_error('Subscription details are not correct');
    };

    $self->throw_login_error('Could not log in');
}

sub _download_profile {
    my ($self, $candidate) = @_;
    $log->info('Donwloading profile');

    my $mech = $self->mech;
    $mech->get($candidate->attr('profile_url'));

    my $scrape_results = $self->scraper->scrape_html(
        $self->mech->content,
        $self->profile_scrape_instructions,
        ['PROFILE']
    );

    my $attributes = $self->scraper->finalize($scrape_results->[0]);

    $candidate->attrs(%$attributes);

    return $candidate;
}

sub profile_scrape_instructions {
    return {
        _path => 'id("left")',

        last_updated      => 'normalize-space(./div[@class="detailsBox"]/ul/li/span[contains(text(), "Laatste update CV")]/following-sibling::text())',
        location          => 'normalize-space(./div[@class="detailsBox"]/ul/li/span[contains(text(), "Plaats")]/following-sibling::text())',
        seniority         => 'normalize-space(./div[@class="detailsBox"]/ul/li/span[contains(text(), "Werkniveau")]/following-sibling::text())',
        job_types         => 'normalize-space(./div[@class="detailsBox"]/ul/li/span[contains(text(), "Dienstverband")]/following-sibling::text())',
        experience        => 'normalize-space(./div[@class="detailsBox"]/ul/li/span[contains(text(), "Werkervaring")]/following-sibling::text())',

        education           => 'normalize-space(./div[@class="contentBox"][1]/h2[contains(text(), "Opleiding")]/following-sibling::p)',
        desired_jobtitle    => 'normalize-space(./div[@class="contentBox"][1]/h2[contains(text(), "Gewenste functie")]/following-sibling::p)',
        personal_motivation => 'normalize-space(./div[@class="contentBox"][1]/h2[contains(text(), "Persoonlijke motivatie")]/following-sibling::text())',
        cv_url => 'string(.//a[@title="Download dit CV"]/@href)',
    };
}

sub _download_cv {
    my ($self, $candidate) = @_;
    
    $self->_login;

    if(!$candidate->attr('cv_url')) {
        $self->_download_profile($candidate);
    }

    # CV URL isn't available unless there's a valid subscription
    if(!$candidate->attr('cv_url')) {
        $self->throw_lack_of_credit('Board CV subscription expired');
    }
    return $self->mech->get($candidate->attr('cv_url'));
}

sub _initial_facets {
    my ($self) = @_;
    my $list = Stream2::Feeds::FacetList->new();

    $list->add_facets(
        sort => Stream2::Feeds::Facet::List->new(
            label => 'Sort',
            sortmetric => $Stream::Constants::SortMetrics::SM_SORT,
            options => [
                [t9n('Employment : Asc')  => '1,ASC' ],
                [t9n('Employment : Desc') => '1,DESC'],
                [t9n('Province : Asc')    => '2,ASC' ],
                [t9n('Province : Desc')   => '2,DESC'],
                [t9n('Seniority : Asc')   => '3,ASC' ],
                [t9n('Seniority : Desc')  => '3,DESC'],
                [t9n('Job Types : Asc')   => '4,ASC' ],
                [t9n('Job Types : Desc')  => '4,DESC'],
                [t9n('Experience : Asc')  => '5,ASC' ],
                [t9n('Experience : Desc') => '5,DESC'],
            ]
        ),
        provinces => Stream2::Feeds::Facet::List->new(
            label => 'Province',
            max_select => -1,
            options => [
                [t9n('Buitenland')    => '0,13'],
                [t9n('Drenthe')       => '1,3' ],
                [t9n('Flevoland')     => '2,4' ],
                [t9n('Friesland')     => '3,2' ],
                [t9n('Gelderland')    => '4,8' ],
                [t9n('Groningen')     => '5,1' ],
                [t9n('Limburg')       => '6,11'],
                [t9n('Noord-Brabant') => '7,10'],
                [t9n('Noord-Holland') => '8,6' ],
                [t9n('Overijssel')    => '9,5' ],
                [t9n('Utrecht')       => '10,7' ],
                [t9n('Zeeland')       => '11,12'],
                [t9n('Zuid-Holland')  => '12,9' ],
            ]
        ),
        education => Stream2::Feeds::Facet::List->new(
            label => 'Education',
            max_select => -1,
            options => [
                [t9n('LBO / VMBO')   => '0,1'],
                [t9n('MBO')          => '1,2'],
                [t9n('HBO')          => '2,3'],
                [t9n('Universitair') => '3,4'],
            ],   
        ),
        jobtype => Stream2::Feeds::Facet::List->new(
            label => 'Job Type',
            max_select => -1,
            options => [
                [t9n('Vast contract')          => '0,1'],
                [t9n('Detachering/Interim')    => '1,2'],
                [t9n('Tijdelijk contract')     => '2,3'],
                [t9n('Freelance')              => '3,4'],
                [t9n('Thuiswerk')              => '4,5'],
                [t9n('Stage')                  => '5,6'],
                [t9n('Vrijwilliger')           => '6,7'],
                [t9n('Leer-werk overeenkomst') => '7,8'],
                [t9n('Bijbaan')                => '8,9'],
            ]
        ),
        experience => Stream2::Feeds::Facet::List->new(
            label => 'Experience',
            max_select => -1,
            options => [
                [t9n('Starter')                 => '0,1'],
                [t9n('1 - 3 Years experience')  => '1,2'],
                [t9n('3 - 5 Years experience')  => '2,3'],
                [t9n('5 - 10 Years experience') => '3,4'],
                [t9n('10+ Years experience')    => '4,5'],
            ],
        ),

        password => Stream2::Feeds::Facet::Text->new(
            credential => 1,
        ),

        email => Stream2::Feeds::Facet::Text->new(
            credential => 1,
        ),
        driving_licence => Stream2::Feeds::Facet::Checkbox->new(
            label => 'Driving licence'
        ),
    );
    return $list;
}



1;

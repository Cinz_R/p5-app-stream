package Stream::Templates::leisurejobs;
use base qw(Stream::Templates::CrawledMadgex);

use strict;
use warnings;

__PACKAGE__->load_tokens_from_config();

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

use Stream::Constants::SortMetrics qw(:all);

$authtokens{username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};


sub fixup_tokens {
    my $self = shift;
    my %tokens = @_;

    #Remove Preferred salary as a token - We're mapping it instead.
    delete $tokens{18}
      or die('Could not find \'Preferred salary\'');

    return %tokens;
}

sub salary_mappings {
    return {
        unspecified => [
            '18|513124', #Unspecified
        ],
        GBP => {
            ranges => { 
               # id => range
                '18|683' => [0       =>  10_000],
                '18|684' => [10_000  =>  15_000],
                '18|685' => [15_000  =>  20_000],
                '18|686' => [20_000  =>  25_000],
                '18|687' => [25_000  =>  30_000],
                '18|688' => [30_000  =>  40_000],
                '18|689' => [40_000  =>  50_000],
                '18|690' => [50_000  =>  60_000],
                '18|691' => [60_000  =>  70_000],
                '18|692' => [70_000  => 100_000],
                '18|693' => [100_000 =>   "inf"]
            }
        }
    };
}


sub fixup_search_fields {
    my $self = shift;
    my %fields = @_;

    my @salary_ranges = $self->_salary_ids;
    push @{$fields{q}}, @salary_ranges;
    return %fields;
}

1;

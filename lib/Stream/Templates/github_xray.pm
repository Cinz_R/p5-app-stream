#!/usr/bin/perl

package Stream::Templates::github_xray;
use strict;
use utf8;

use base qw(Stream::Templates::google_cse);

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

$derived_tokens{and_terms} = {
    Type => 'Text',
    Helper => 'google_split_keywords(%keywords%|and_terms|or_terms|not_terms|exact_terms)',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{or_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{not_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{exact_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

sub build_google_search_query {
    my $self = shift;

    my $base_url = 'github.com';

    my $keywords = "site:" . $base_url ." AND \"Joined on\" AND \"Public Activity\"  AND -inurl:jobs"; 

    if ($self->token_value('keywords')){
        if ($self->token_value('and_terms')){
            $keywords .= ' AND (' . $self->token_value('and_terms') . ')';
        }
        if ($self->token_value('exact_terms')){
            $keywords .= ' AND (' . $self->token_value('exact_terms') .')';
        }
        if (my $or_terms = $self->token_value('or_terms')){
            $or_terms =~ s/^\s+OR//;
            $keywords .= ' AND (' . $or_terms .')';
        }
        if ($self->token_value('not_terms')){
            $keywords .= ' AND ' . $self->token_value('not_terms');
        }
    }

    my $location_id = $self->token_value('location_id');

    if ($location_id){
        my $location = $self->location_api()->find({
            id => $location_id,
        });
        if ($location){
            $keywords .= ' AND "' . $self->google_loc_check( $location->name );
            if (!$location->is_country){
                my $country = $location->in_uk ? 'UK' : $location->in_usa ? 'US' : $location->country->name;
                $keywords .= ', ' . $country;
            }
            $keywords .= '"';

        }
    }


    return $keywords;
}

sub google_add_cache_link { 0; }

sub cse_engine_id {
    return '003231768667224905379:eb7hw3cps2q';
}
return 1;

package Stream::Templates::Monster;

=head1 NAME

Stream::Templates::Monster - Monster Cv Search version 1.7

=head1 AUTHOR

ACJ - 11/09/2008

=head1 CONTACT

gillian.hasley@monster.co.uk (Product Manager, +44 (0)20 7345 6141)

(backup is Lakis Floudas <Chris.Floudas@monster.de>)

=head1 TESTING INFORMATION

# to test you need, set your host files to

# https://63.112.170.8:8443/bgwBroker

 company xcode = xbroadbeanukx
 username      = xbroadbeanukx01
 password      = jellybeans
 CAT           = 1:cQBc5FFAQa05p3paoOgU9Fg8b04X2FsOby7MBsMM_z765B_0kxVJTCxhriNVVrAz3c4mYKmqaHaX0p8gFG1dyteRejVzzul4E4njYzH8kP8-055og

=head1 DOCUMENTATION

Available here: http://trac/adcourier/wiki/MultipleCvSearch

Development and test guide.doc - how to connect to the monster test site
                               - XML spec. for retrieving a CV
External resume Search Service Specification.doc - resume search documentation

ResumeSearchExampleResults.xml - example Monster response

The docs are over 30 pages long :(

Monster's help for the CV search is available online:
L<http://monster-uken.custhelp.com/cgi-bin/monster_uken.cfg/php/enduser/std_alp.php?p_li=cF91c2VyaWQ9dGVtcF91aWRieXAwMDI3ZmIzNi0zMjQ1LTQ1ODAtOWU3MS1kYTI3ZDIwZDVjYTUzMTBAJnBfcGFzc3dkPW1vbnN0ZXImcF9uYW1lLmZpcnN0PSZwX25hbWUubGFzdD0mcF9lbWFpbC5hZGRyPVRlbXBfVUlEQnlQMDAyN2ZiMzYtMzI0NS00NTgwLTllNzEtZGEyN2QyMGQ1Y2E1MzEwQCZwX2NjZl81NT0yNTMmcF9jY2ZfNTQ9MSZwX2NjZl81Mz0x&p_cv=1.257&p_cats=257&p_pv1=1.253&p_maxrows=25&p_partner=200&p_m_id=184,494,394,367,370,344,348,382,379,385,496,497,228,388,26,391,455,495>

=head2 Online examples

Soap envelope:
http://schemas.monster.com/Current/XSI/SOAPEnvelopeSamples/SampleSOAPEnvelopeWithCATAuthentication.xml

Soap body:
http://schemas.monster.com/Current/XSI/QuerySamples/RetrieveResume.xml

=head1 OVERVIEW

Search is a field-value pairs get request. Response is XML (UTF8)
Downloading a search is a XML post to Monster BGW. Response is XML.
Only a text CV is available - no word CVs are available.

Searching uses a CAT (company access ticket) instead of an xcode etc.

=head1 EXTRA INFORMATION

Monster BGW only supports Windows-1252 (no UTF8 support).
Resume Search request & response is UTF8 though

IP Restricted (only blade7 has access)

Debugging tip: if you perform the search on monster directly and then hit "Edit search",
the query string of the page shows all the parameters they used to query their webservice

=head2 DOWNLOADING CVS

Candidates can either upload a CV or use Monster's CV builder to create one.

Candidates who use the CV builder do not have a TextResume so we build a CV
using XLST instead. This is a similar process to providing a quick CV from BG xml.

See ticket 23 for the gory details.

If a candidate uploads a CV, Monster can provide it in Text or HTML format.

=head2 CAT CODES AND CREDITS

You can have RESUME_SEARCH and RESUME_VIEW credit. If they can search but they can't view CVs
then Monster have not given the CAT code any RESUME_VIEW credit. It is usually easier to ask
Monster to create a new CAT code than ask them to add RESUME_VIEW credit.

=head2 BOOLEAN SEARCHING

Pinched from Monster help:

Using keywords is an effective way to search for words, phrases and acronyms contained in a job seeker's CV (e.g., UNIX, programmer, SAP, sales, COBOL, human resources). How you create your search string will influence your search results. Below are a few ways you can define your keyword search terms using Boolean operators for a more effective CV search.

Use AND between terms to search for CVs that include all surrounding terms. For example, UNIX AND programmer will return only CVs that include both terms. If you don't separate your terms, (e.g., UNIX programmer), AND is used as the default.

Use OR between terms to search for CVs that contain either word surrounding it. For example, UNIX OR programmer will return CVs that have either term. Using OR can be useful when there is more than one word that describes a desired search term.

Use "" (quotation marks) around multiple terms to search for CVs that include the term included in quotes. For example, "UNIX programmer" will return CVs that include that specific term, rather than separate occurrences of the two words.

More information: L<http://help.network.monster.co.uk/help/index.asp?app=hiring&ma=/resumesearch/resumesearch.aspx&userflow=MHX&domain=hiring.monster.co.uk&qs=>

=head1 MAJOR CHANGES

=over 4

=item * 07/07/10 - ACJ - Only send the major part of the dutch postcodes. Monster don't support the full postcode

=item * 15/06/10 - ACJ - Restrict the search to 18 months because Monster only keep CVs for 18 months

=item * 13/05/10 - ACJ - Removed %moc%, %edumjr%, %mgrex%, %yrsexpid%, %projld% and %industry% tokens because they aren't available on Monster direct

=item * 10/05/10 - ACJ - Send missing "Most Recent *" tokens

=item * 26/04/10 - ACJ - Build CV from XML when Monster do not provide one

=item * 16/03/10 - ACJ - Relevancy sort order not working because we weren't sending the sort parameter

=item * 16/03/10 - ACJ - Improved HTML CV detection. Not all HTML CVs include the <html tag

=item * 11/03/10 - ACJ - Grab the HTML CV by sending <StoreRenderedTextResume>0</StoreRenderedTextResume>

=item * 25/01/10 - ACJ - Monster changed XML from <Resume SID="..."> to <Resume Value="...">

=item * 13/10/09 - ACJ - Mappings send residential location not target work location

=item * 09/10/09 - ACJ - Use tnsalmin/tnsalmax instead of tsalmin/tsalmax

=item * 04/08/09 - ACJ - Send UK postcode if available

=item * 09/07/09 - ACJ - Monster released BGW v5, fixed CV download

=item * 19/06/09 - ACJ - use %cv_updated_within% token

=back

=head1 BUG FIXES

=head2 GB country code not support

Monster do not support the GB country code. We must send UK instead

=head2 Do not send empty options in the querystring

Sending rlid=<empty> caused most to return no results instead of searching any location

=head2 HTML CV full of UTF8 garbage

Monster HTML is UTF8 but they don't include a <meta> tag so the charset is lost when the CV
is saved

Bean::FileStore::S3 will attempt to insert a meta tag into all html to get around this

=cut

use strict;

use utf8; # some of the locations has utf8 encoded accents in them

use base qw(Stream::Engine::API::XML);

use Stream::Constants::SortMetrics qw(:all);

use POSIX ();
use DateTime::Format::Strptime;
use Bean::HTMLStrip; # check if we have a HTML CV or a Text one

use Encode;
use Log::Any qw/$log/;

################ TOKENS #########################
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

my ($MONSTER_LOCATIONS, $MONSTER_CATEGORIES);
BEGIN {

    $MONSTER_CATEGORIES = 'Academic Research=11708|Account Management - Non-Commissioned=11710|Account Management, Commissioned=11709|Accounts Payable/Receivable=11711|Actuarial Analysis=11712|Administrative Support=11713|Advertising Writing - Creative=11714|Aeronautic/Avionic Engineering=11715|Agricultural/Greenhouse Work=11716|Aircraft/Ship Inspection=11717|Architecture/Interior Design=11719|Assembly/Assembly Line=11721|Audio/Video Broadcast & Postproduction=11723|Audit=11724|Bank Teller=11726|Barrister=11727|Bio-Engineering=11728|Biological/Chemical Research=11729|Bookkeeping/General Ledger=11730|Brand Marketing=11732|Building/Construction Inspection=11733|Business Analysis/Research=11734|Business Development/New Accounts=11735|Business Unit Management=11736|CAD/Drafting=11737|Call Centre=11738|Car, Van and Bus Driving=11739|Career Fair=11741|Carpentry/Framing=11743|Chemical Engineering=11746|Civil & Structural Engineering=11747|Claims Processing=14865|Claims Review and Adjusting=14866|Classroom Teaching=11748|Clinical Research=11749|Collections=11750|Compensation/Benefits Policy=11751|Computer Animation & Multimedia=11752|Computer/Electronics/Telecomm Install / Maintain/Repair=11753|Computer/Network Security=11754|Concrete and Masonry=11755|Continuing/Adult =11756|Contracts Administration=11757|Copy Writing/Editing=11758|Corporate Accounting=14867|Corporate Development and Training=11759|Corporate Finance=11760|Cost Estimating=11763|Council/Revenue/Benefits=11764|Creative Direction/Lead=11765|Credit Review/Analysis=11766|Criminal Law=11767|Customer Training=11768|Customs/Immigration=11769|Data Entry/Order Processing=11770|Data Warehousing=11771|Database Development/Administration=11772|Dental Practitioner=11773|Desktop Service and Support=11774|Direct Marketing (CRM)=11776|Diversity Management/EEO/Compliance=11777|Documentation/Technical Writing=11778|Early Childhood Care & Development=11779|Editing & Proofreading=11781|Electrical/Electronics Engineering=11782|Electrician=11783|EMT/Paramedic=11785|Energy/Nuclear Engineering=11786|Enterprise Software Implementation & Consulting=11787|Environmental and Geological Engineering=11788|Environmental Protection=11789|Environmental/Geological Testing & Analysis=11790|Equipment Install/Maintain/Repair=11791|Equipment Operations=11792|Equipment/Forklift/Crane Operation=11793|Event Planning/Coordination=11794|Events/Promotional Marketing=11795|Executive Recruiting=11796|Executive Support=11797|Facilities/HVAC Maintenance=11798|Family Law=11799|Fashion & Accessories Design=11800|Field Sales=11801|Filing/Records Management=11802|Financial Analysis/Research/Reporting=11803|Financial Control=14868|Financial Planning/Advising=11804|Firefighting and Rescue=11805|Fishing and Fishing Vessel Operation=11806|Fitness & Sports Training/Instruction=11807|Flight Attendant=11808|Flooring/Tiling/Painting/Wallpapering=11809|Food & Beverage Serving=11810|Food Preparation/Cooking=11811|Franchise-Business Ownership=11814|Fraud Investigation=11815|Front Desk/Reception=11816|Fund Accounting=11817|Fundraising=11818|General/Other: Accounting/Finance=11893|General/Other: Administrative/Clerical=11894|General/Other: Business/Strategic Management=11895|General/Other: Construction/Skilled Trades=11896|General/Other: Creative/Design=11897|General/Other: Customer Support/Client Care=11898|General/Other: Editorial/Writing=11899|General/Other: Engineering=11900|General/Other: Food Services=11901|General/Other: Human Resources=11902|General/Other: Installation/Maintenance/Repair=11903|General/Other: IT/Software Development=11904|General/Other: Legal=11905|General/Other: Logistics/Transportation=11906|General/Other: Marketing/Product=11907|General/Other: Medical/Health=11908|General/Other: Production/Operations=11909|General/Other: Project/Program Management=11910|General/Other: Quality Assurance/Safety=11911|General/Other: R&D/Science=11912|General/Other: Sales/Business Development=11913|General/Other: Security/Protective Services=11914|General/Other: Training/Instruction=11915|Geriatric/Home Care=11820|Graphic Arts/Illustration=11821|Guest Services/Concierge=11822|Hair Cutting/Styling=11824|Hazardous Materials Handling=11827|Head Teacher=11828|Heavy Equipment Operation=11829|Hospital/Clinic Administration=11830|Host/Hostess=11831|Housing and Community Development=11833|HVAC Design and Installation=11835|Import/Export Administration=11836|Industrial Design=11837|Industrial/Manufacturing Engineering=11838|InsuranceAgent/Broker=11839|International Sales=11840|Inventory Planning and Management=11842|Investment Management=11843|Investor and Public/Media Relations=11844|Ironwork/Metal Fabrication=11845|ISO Certification=11846|IT Consulting=11847|IT Project Management=11848|Janitorial & Cleaning =11849|Journalism=11850|Junior/High School=11851|Laboratory/Pathology Services=11853|Labour & Employment Law=11852|Landscape Maintenance=11854|Layout, Prepress, Printing, & Binding Operations=11855|Lecturing=11856|Library Services=11857|Locksmith=11858|Machining-CNC=11859|Managerial Consulting=11860|Market Research=11862|Marketing Communications=11863|Marketing Production/Traffic=11864|Materials/Physical Research =11865|Mathematical/Statistical Research=11866|Mechanical Engineering=11867|Media and Advertising Sales=11868|Media Planning and Buying=11869|Medical Practitioner=11870|Medical Therapy/Rehab Services=11871|Medical/Dental Assistant=11872|Mental Health=11873|Merchandise Planning and Buying=11874|Mergers and Acquisitions=11875|Messenger/Courier=11876|Metal Fabrication and Welding =11877|Midwifery=11878|Military=11879|Moldmaking/Casting=11880|Mortgage Advising=14872|Naval Architecture/Marine Engineering=11881|Network and Server Administration=11882|New Product R&D=11883|Nursing=11885|Nutrition and Diet=11886|Occupational Health and Safety=11887|Office Management=11888|Oil Rig & Pipeline Maintenance and Repair=11889|Operations/Plant Management=11890|Optical=11891|Other=11892|Paralegal & Legal Secretary=11916|Patent/IP Law=11917|Payroll and Benefits Administration=11918|Performer/Artist=11919|Pharmaceutical Research=11921|Pharmacy =11922|Photography and Videography=11923|Piloting: Air and Marine=11924|Plastering=11963|Plumbing/Pipefitting=11925|Police-Law Enforcement=11926|Policy Underwriting=11927|Postal Worker=11928|President/Top Executive=11929|Prison Officer=11762|Product Management=11931|Production Quality Assurance=11932|Production/Operations Planning=11933|Program Management=11934|Project Management=11935|Property Management=14874|Public Health Administration=11936|Purchasing Goods and Services=11937|Real Estate Agent/Broker=11939|Real Estate Law=11941|Real Estate Leasing/Acquisition=14875|Reception/Switchboard=11942|Recruiting/Sourcing =11943|Recruitment Consulting=14876|Reservations/Ticketing=11945|Restaurant Management=11947|Retail Customer Service=11948|Retail Department Management=11949|Retail/Counter Sales and Cashier=11950|RF/Wireless Engineering=11951|Risk Management/Compliance=11952|Roofing=11953|Sales Support/Assistance=11954|Sales Support/Assistance=11955|Scriptwriting and Review=11957|Secretary/Executive Assistant=11958|Securities and Commodities Brokerage=11960|Security Guard=11961|Sewing and Tailoring=11962|Shipping and Receiving/Warehousing=11964|Site Superintendent=11965|Six Sigma/Black Belt/TQM=11966|Social Service=11967|Software Quality Assurance=11968|Software/System Architecture=11969|Software/Web Development=11970|Solicitor=11722|Special Education=11972|Sports Medical Trainer=11973|Store Security/Loss Prevention=11974|Store/Branch Management=11975|Strategy/Planning=11976|Supplier Management/Vendor Management=11977|Surveying=11978|Systems Analysis - IT=11979|Systems/Process Engineering=11980|Tax Accounting=11981|Tax Assessment and Collections=11982|Tax Law=11983|Teaching Support=11984|Technical Customer Service=11985|Technical Presales Support & Technical Sales=11986|Telecommunications Administration/Technician=11987|Telemarketing=11988|Telesales=11989|Town/City Planning=11990|Train or Rail Operator=11991|Translation/Interpretation=11992|Travel Agent/Ticket Sales=11993|Truck Driving=11994|University =11995|Usability/Information Architecture=11996|Vehicle Dispatch, Routing and Scheduling=11998|Vehicle Inspection=11999|Vehicle Repair and Maintenance=12000|Veterinary/Animal Care =12001|Visual/Display Merchandising=12002|Waste Pick-up and Removal=12003|Web Mastering=12004|Web/UI/UX Design=12005|Wholesale/Reselling Sales=12006|Wine Steward (Sommelier)=12007|Work at Home (Commission Only)=12008';
};

BEGIN {

=head1 TOKENS

=head2 STANDARD TOKENS

=head3 Recent Job Title (%qrjt%)

Available on Monster directly as Most Recent Job Title.

The qrjt argument allows a recruiter to search for seekers based on
the most recent job title. Several titles may be used in the search
by submitting a comma-delimited list.

For example:

 qrjt=programmer,developer

=cut

    $standard_tokens{qrjt} = {
        Label   => 'Most Recent Job Title',
        Type    => 'Text',
        Example => 'programmer,developer',
        SortMetric => $SM_KEYWORDS,
        Size       => 40,
    };

=head3 Target Job Title (%qtjt%)

Disabled because not available on Monster directly.

The qtjt argument allows a recruiter to search for seekers based on
the target job title. Several titles may be used in the search by
submitting a comma-delimited list.

For example:

 qtjt=programmer,developer

=cut

    $standard_tokens{qtjt} = {
        Label => 'Target Job Title',
        Type  => 'Text',
        Example => 'programmer,developer',
        SortMetric => $SM_KEYWORDS + 4,
        Size       => 40,
    };

=head3 Recent job title or Target job title (%qajt%)

Disabled because not available on Monster directly

The qajt argument allows a recruiter to search for seekers based on
the target job title or the most recent jobtitle. Several titles
may be used in the search by submitting a comma-delimited list.

For example:

 qajt=programmer,developer

=cut

#    $standard_tokens{qajt} = {
#        Label => 'Recent job title or Target job title',
#        Type  => 'Text',
#        Example => 'programmer,developer',
#        SortMetric => $SM_KEYWORDS,
#        Size       => 40,
#    };

=head3 Recent Company Name (%qrcn%)

Available on Monster directly as "Most Recent Employer"

The qrcn allows a recruiter to search for seekers based on the most
recent company that they were employed with. Several company names
may be used in the search by submitting a comma-delimited list.

For example:

 qrcn=company1,company2

=cut

    $standard_tokens{qrcn} = {
        Label => 'Most Recent Employer',
        Type  => 'Text',
        Example => 'company1,company2',
        SortMetric => $SM_KEYWORDS,
        Size       => 40,
    };

=head3 Recent job description (%qrjd%)

Available on Monster as "Most Recent Job Description"

The qrjd allows a recruiter to search for seekers based on the most
recent job description.  Several job descriptions may be used in the
search by submitting a comma-delimited list.

For example:

 qrjd=computer+programmer,project+manager

=cut

    $standard_tokens{qrjd} = {
        Label => 'Most Recent Job Description',
        Type  => 'Text',
        Example => 'computer programmer,project manager',
        SortMetric => $SM_KEYWORDS,
        Size       => 40,
    };

=head3 Recent School Name (%qsn%)

Available on Monster as "Schools Attended"

The qsn allows a recruiter to search for seekers that have attended
a specific school. Several school names may be used in the search
by submitting a comma-delimited list. For example:

 qsn=Harvard,Yale

=cut

    $standard_tokens{qsn} = {
        Label => 'Schools Attended',
        Type  => 'Text',
        Example => 'Harvard,Yale',
        SortMetric => $SM_EDUCATION,
        Size       => 40,
    };

=head3 Monster Sort By (%sort%)

Only available on the Monster search results. The default sort order on
Monster directly is C<rank> not C<mdate>.

The C<sort> parameter specifies the desired presentation order of
the search results.

The default sort order is C<mdate>. If C<rank> is specified, an
additional Relevance element is output in each Resume node. This
element is a floating-point value between 0 and 1, and indicates
how well the resume matches the keyword query.

TIP: Relevance values cannot be meaningfully compared between
successive queries, or between relevance values calculated from
other sources.

=over

=item Rank

Keyword relevance

=item Mdate

Modification / activation date of the resume

=back

=cut

    $standard_tokens{sort} = {
        Label => 'Sort by',
        Type  => 'List',
        Options => 'Relevance!!Rank|Last updated!!Mdate',
        Default => 'Rank',
        SortMetric => $SM_SORT,
    };

=head3 Monster Results per page (%max_results%)

Number of results per page - default is 20 candidates

=cut

    $standard_tokens{max_results} = {
        Label => 'Number of Results',
        Type  => 'List',
        Options => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
        SortMetric => $SM_SEARCH,
    };

=head3 Monster Job Type (%monstermxl_jobtype%)

Combination of Job Status and Job Types on the monster site.

Per Diem corresponds to Per Day.
Intern corresponds to Placement Student on Monster directly

Supports Permanent, Temp/Contract, Full Time, Part Time, Intern,
Per Diem and Seasonal

=cut

    $standard_tokens{'jobtype'} = {
        Label      => 'Job Type',
        Type       => 'MultiList',
        Options    => 'Full Time|Part Time|Placement Student=Intern|Per Day=Per Diem|Seasonal',
        SortMetric => $SM_JOBTYPE,
    };

    $derived_tokens{mapped_jobtype} = {
        Label      => 'Job Type',
        Type       => 'MultiList',
        Helper     => 'HASH_MAP_MULTIPLE(%default_jobtype%|permanent=Permanent|contract=Temp/Contract|temporary=Temp/Contract)',
        Deprecated => 'JobType',
        SortMetric => $SM_JOBTYPE,
    };

    $posting_only_tokens{'jobtypes'} = {
        Helper => 'COMBINE_UNIQ_TOKENS(%jobtype%|%mapped_jobtype%)',
    };

=head3 Monster Education Level (%edulv%)

Education level is available on Monster directly but I don't think they match up.

Options searching Monster directly:

=over 4

=item * Some Secondary School Coursework (id = 12)

edulv=12+1+2+3+9+4+5+6+7+8

=item * Secondary School or Equivalent (id=1)

edulv=1+2+3+9+4+5+6+7+8

=item * 'A' Level/Higher or Equivalent (id=2)

edulv=2+3+9+4+5+6+7+8

=item * Vocational (id=3)

edulv=3+9+4+5+6+7+8

=item * Some College Coursework Completed (id=9)

edulv=9+4+5+6+7+8

=item * HND/HNC or equivalent (Associate Degree) (id=4)

edulv=4+5+6+7+8

=item * Bachelor's Degree (id=5)

edulv=5+6+7+8

=item * Master's Degree (id=6)

edulv=6+7+8

=item * Doctorate (id=7)

edulv=7+8

=item * Professional (id=8)

edulv=8

=back

Vocational - High School and Vocation - Degree aren't available on Monster directly.
A new option, "Some Secondary School Coursework completed" is only available on Monster directly.
On Monster directly, you can select one option not multiple.

The C<edulv> parameter is a multi-valued parameter that restricts the
search to resumes that contain one or more (if delimited with <space>)
of the supplied list of education levels. Possible values include:

 High School or equivalent         => 1
 Certification                     => 2
 Vocational                        => 3
 Associate Degree                  => 4
 Bachelors Degree                  => 5
 Masters Degree                    => 6
 Doctorate                         => 7
 Professional                      => 8
 Some College Coursework Completed => 9
 Vocational – High School          => 10
 Vocational – Degree               => 11

=cut

    $standard_tokens{education} = {
        Label   => 'Education',
        Type    => 'List',
        Options => q{Any=|Some Secondary School Coursework=12 1 2 3 9 4 5 6 7 8|Secondary School or Equivalent=1 2 3 9 4 5 6 7 8|'A' Level/Higher or Equivalent=2 3 9 4 5 6 7 8|Vocational=3 9 4 5 6 7 8|Some College Coursework Completed=9 4 5 6 7 8|HND/HNC or equivalent=4 5 6 7 8|Bachelor's Degree=5 6 7 8|Masters Degree=6 7 8|Doctorate=7 8|Professional=8},
        SortMetric => $SM_EDUCATION,
    };

=head3 Monster Residence Location (%rlid%)

Corresponds to the "Search within selected locations" on Monster directly.

The C<rlid> parameter is a multi-valued parameter that restricts
the search to resumes with owners who reside in one of the
locations specified. Allowable values are given in Appendix A.

=cut

    $derived_tokens{rlid} = {
        Label => 'Residence Location',
        Type  => 'MultiList',
        # Should be a location mapping
        Options => $MONSTER_LOCATIONS,
        Helper  => 'monsterxml_location_mapping(%location_id%|%location_within_miles%)',
        SortMetric => $SM_LOCATION,
    };

=head3 Residence Postal Code Radius (%rpcr%)

Corresponds to "Search within Postcode Radius" on Monster directly.

Is automagically set by the monsterxml_location_mapping() helper if the location
has a zipcode/postcode

The C<rpcr> parameter is a compound, multi-valued parameter that restricts
the search to resumes with owners who reside within a certain radius of a
specified zip code. The parameter should be specified as a zip code / radius
(in miles) pair, separated by a hyphen, eg. C<rpcr=90210-20>. Multiple values
may be specified by delimiting the pairs with a space

eg. C<rpcr=90210-20+01754-10+46202-100>.

We can send UK postcodes by appending a &co=UK to the query

=cut

=head3 Target Work Location (%twlid%)

Corresponds to "Include job seekers willing to work" checkbox on Monster directly.
You cannot choose different Residential and Target Work locations on Monster directly.

The C<twlid> parameter restricts the search to resumes with owners who have
expressed a desire to work in one or more or all of the locations specified.
Possible values are given in Appendix A.

=cut

    $standard_tokens{twlid} = {
        Label => 'Target Work Location',
        Type  => 'MultiList',
        Options => $MONSTER_LOCATIONS,
        SortMetric => $SM_LOCATION,
    };

=head3 Target Work Location Boolean

Not available on Monster directly. Default on site is AND if the
"Include job seekers willing to work in any of the selected locations" is ticked

The C<twlb> parameter specifies how to interpret queries that contain both a
Residence Location parameter (C<rpcr> or C<rlid>) and a Target Work Location
parameter (C<twlid>).

Monster default is OR

=cut

    $standard_tokens{twlb} = {
        Label   => 'Target Work Location Boolean',
        Type    => 'List',
        Options => q{(Residence Location query is OR'd to Target Work Location=0|Residence Location query is AND'd to Target Work Location=1)},
        SortMetric => $SM_LOCATION,
    };

=head3 Relocate (%relo%)

Not available on Monster directly.

The C<relo> parameter has the following possible values and meanings:

  Value   Meaning
  0       Show resumes where user is not willing to relocate for employment
  1       Show resumes where user is willing to relocate for employment

=cut

    $standard_tokens{relo} = {
        Label   => 'Willing to relocate',
        Type    => 'List',
        Options => '(Any=|Show resumes where user is not willing to relocate for employment=0|Show resumes where user is willing to relocate for employment=1)',
        SortMetric => $SM_LOCATION,
    };

=head3 Target Work Site (%twsite%)

Not available on Monster directly.

The C<twsite> parameter has the following possible values and meanings:

  Value     Meaning
  0         Show resumes where user is willing to work on-site
  1         Show resumes where user is willing to work off-site
  2         Show resumes where user has indicated no work-site
            preference or has not specified a preference

=cut

#    $standard_tokens{twsite} = {
#        Label   => 'Target Work Site',
#        Type    => 'List',
#        Options => '(Any=|Show resumes where user is willing to work on-site=0|Show resumes where user is willing to work off-site=1|Show resumes where user has indicated no work-site=2)',
#        SortMetric => $SM_LOCATION,
#    };

=head3 Work Authorisation (%wa%)

Partially available on Monster directly through tick box
"Only show job seekers authorised to work for any employer in any of the selected locations"

NOT SUPPORTED BY US!

The C<wa> parameter is a compound, multi-valued parameter that restricts
the search to resumes containing one or more of the supplied list of
work authorization statuses. This is a compound parameter with two
components separated by a hyphen: C<CountryID> (from Appendix B) and
C<WorkStatusID> (below). The possible values for WorkStatusID are
as follows:

  WorkStatus ID   Meaning
  1               Authorized to work for any employer
  2               Authorized to work for current employer only
  3               Require sponsorship

For example, the query C<wa=164-1+160-1> will query for resumes
with owners who are authorized to work in the US or the UK for
any employer.

=cut
    $standard_tokens{wa} = {
        Label => 'Work Authorisation',
        Type  => 'List',
        Options => '(Any=|Authorized to work for any employer=1)',
    };

    $derived_tokens{wa_id} = {
        Label => 'Work Authorisation',
        Type  => 'List',
        Helper  => 'monster_wa_location(stream_monster_wa|%location_id%)',
        SortMetric => $SM_LOCATION,
        #Options => '(Authorized to work for any employer=1|Authorized to work for current employer only=2|Require sponsorship=3)',
    };

=head3 Monster Occupation Code (%moc%)

Disabled as not available on Monster directly. See %tcc% token instead.

The C<moc> parameter is a multi-valued parameter that restricts the
search to resumes that have been classified into one or more of the
supplied list of Monster Occupation Codes.

=cut

#    $standard_tokens{moc} = {
#        Label => 'Monster Occupations',
#        Type  => 'MultiList',
#        Options => $MONSTER_CATEGORIES,
#        SortMetric => $SM_INDUSTRY,
#    };

=head3 Target Salary Minimum (%salary_min%)

The C<salmin> parameter restricts the search to resumes with desired minimum salary greater than or equal to the supplied value.

=cut

    $derived_tokens{salary_from} = {
        Type  => 'SalaryText',
        SortMetric => $SM_SALARY,
    };

=head3 Target Salary Maximum (%salary_max%)

The C<salmax> parameter restricts the search to resumes with desired minimum salary less than or equal to the supplied value.

=cut

    $derived_tokens{salary_to} = {
        Type  => 'SalaryText',
        SortMetric => $SM_SALARY,
    };

=head3 Target Salary Currency (%salary_cur%)

The C<tsalcur> parameter restricts the search to resumes with desired salary in the currency supplied. The possible values are given in Appendix C.

=cut

    # convert the currency to one that monster supports
    $derived_tokens{salary_cur} = {
        Type  => 'Currency',
        Options => '(ARS|AUD|BEF|BRL|CAD|CNY|CZK|DKK|EUR|FJD|FIM|FRF|DEM|GRD|HKD|HUF|INR|IDR|IEP|ILS|ITL|JPY|LUF|MYR|MXN|NLG|NZD|NOK|PLN|RUB|SGD|ZAR|KRW|ESP|SEK|CHF|TWD4|GBP|USD)',
        Default => 'GBP', # United Kingdom Pound
        Helper => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric => $SM_SALARY,
    };

    # convert the iso currency code to a monster currency id
    $posting_only_tokens{salary_cur_id} = {
        Helper => 'HASH_MAP(%salary_cur%|ARS=3|AUD=4|BEF=5|BRL=6|CAD=7|CNY=9|CZK=10|DKK=38|EUR=2|FJD=13|FIM=39|FRF=14|DEM=11|GRD=16|HKD=17|HUF=18|INR=22|IDR=19|IEP=20|ILS=21|ITL=23|JPY=24|LUF=36|MYR=27|MXN=26|NLG=28|NZD=29|NOK=37|PLN=30|RUB=31|SGD=33|ZAR=35|KRW=25|ESP=12|SEK=32|CHF=8|TWD=34|GBP=15|USD=1)',
    };

=head3 Target Salary Type (%salary_per%)

The C<tsaltyp> parameter restricts the search to resumes with desired
salary of one or more of the supplied types. The possible values are
as follows:

  Value         Meaning
  1             Per year
  2             Per hour
  3             Per week
  4             Per month

=cut

    $derived_tokens{salary_per} = {
        Type  => 'SalaryPer',
        Options => '(annum|hour|week|month)',
        Helper => 'SALARY_PER(salary_per|salary_from|salary_to)',
        Default => 'annum',
        HelperMetric => 1,
        SortMetric => $SM_SALARY,
    };

    # convert the annum/week etc into monster id
    $posting_only_tokens{salary_per_id} = {
        Helper => 'HASH_MAP(%salary_per%|annum=1|hour=2|week=3|month=4)',
    };


=head3 Education Major (%edumjr)

Disabled because not available on Monster directly

The C<edumjr> parameter allows a user to restrict a query based on a
seekers education major.  One or more education majors may be
specified and the resume must match one of the requested values.

=cut

#    $standard_tokens{edumjr} = {
#        Label => 'Education Major',
#        Type  => 'Text',
#        SortMetric => $SM_EDUCATION,
#    };

=head3 Managerial Experience (%mgrex%)

Disabled as not available on Monster directly

The C<mgrex> argument restricts the query based on managerial experience.

  Value   Meaning
  0       No – Does not have managerial experience.
  1       Yes – Has managerial experience

=cut

#    $standard_tokens{mgrex} = {
#        Label => 'Managerial Experience',
#        Type  => 'List',
#        Options => '(Any=|No – Does not have managerial experience=0|Yes – Has managerial experience=1)',
#        SortMetric => $SM_EXPERIENCE,
#    };

=head3 Monster Resume Language (%rslang%)

Not available on Monster directly. See %lskill% instead

The C<rslang> parameter restricts the search to only resumes that are
composed in a certain language.  Language is specified by the seeker
when the resume is entered/uploaded to the site.  One or more resume
languages may be specified and the resume must match one of the
requested values.

=cut

    $standard_tokens{rslang} = {
        Label => 'Resume Language',
        Type  => 'MultiList',
        Options => '(Aboriginal Dialects=1|Afrikaans=2|American Sign Language=3|Ancient Greek=4|Arabic=5|Assamese=6|Australian Sign Language=7|Bahasa Indonesia=8|Basque=9|Bengali=10|Brazilian Portugese=11|Breton=12|British Sign Language=13|Bulgarian=14|Burmese=15|Catalan=16|Chinese=17|Chinese – Cantonese=18|Chinese – Mandarin=19|Chinese – Taiwanese=20|Corsican=21|Croatian=22|Czech=23|Danish=24|Dutch=25|Egyptian=26|English=27|Estonian=28|Farsi=29|Finnish=30|Flemmish=31|French=32|Frisian=33|Gaelic=34|German=35|Greek=36|Greenlandic=37|Gujarati=38|Hebrew=39|Hindi=40|Hindustan=41|Hmong=42|Hungarian=43|Icelandic=44|Irish=45|Irish Gaelic=46|Italian=47|Japanese=48|Kannada=49|Kashmiri=50|Korean=51|Kurdish=52|Latin=53|Latvian=54|Lithuanian=55|Malay=56|Malayalam=57|Manipuri=58|Manx Gaelic=59|Maori=60|Marathi=61|Nepali=62|Norwegian=63|Oriya=64|Other=65|Polish=66|Portuguese=67|Prakrit=68|Punjabi=69|Romanian=70|Russian=71|Sanskrit=72|Scots=73|Scots Gaelic=74|Serbian=75|Sicilian=76|Slovak=77|Slovene=78|Spanish=79|Swahili=80|Swedish=81|Tagalog=82|Tamil=83|Telugu=84|Thai=85|Tibetan=86|Turkish=87|Ukrainian=88|Urdu=89|Vietnamese=90|Welsh=91|Yiddish=92|Ainu=93|Akkadian=94|Alurian=95|Arkian=96|Assyrian=97|Asturian=98|Aymara=99|Basque Language-Euskara=100|Berber=101|Buhi=102|Cherokee=103|Chichewa=104|Church Slavonic=105|Cornish=106|Dakota=107|Degaspregos=108|Dilhok=109|Dongxiang=110|Esperanto=111|Eurolang=112|Faroese=113|Friulian=114|Galician=115|Georgian=116|Guarani=117|Haponish=118|Hausa=119|Hawaiian=120|Hawaiian Pidgin English=121|Ido=122|Ingush=123|Jameld=124|Kankonian=125|Khmer=126|Kiswahili=127|Konkani=128|Ladin=129|Ladino=130|Lakhota=131|Loglan=132|Low Saxon=133|Malat=134|Mongolian=135|Neelan=136|Novial=137|Occitan=138|Ojibwe=139|Pashto=140|Pidgin=141|Quechua=142|Rhaeto -Romance=143|Romany=144|Shiyeyi=145|Sindhi=146|Sinhalese=147|Swabian=148|Tengwar=149|Tok Pisin=150|Uzbek=151|Vogu=152|Xhamagas=153|Yoruba=154|Albanian=155|Luxembourgish=156|Sami=157|Xhosa=158|Zulu=159|Bosnian=160|Pidgin Signed English=161|Signing Exact English=162|Tactile Sign Language=163|Armenian=164|Macedonian=165)',
        SortMetric => $SM_LANGUAGE,
    };

=head3 Language Skills (%langskills%)

This is NOT USED. We do not send this to Monster even though we let consultants
pick one in Stream. Disable this option

=cut

#    $standard_tokens{langskills} = {
#        Label => 'Language Skills',
#        Type => 'MultiList',
#        Options => $standard_tokens{rslang}->{Options},
#        SortMetric => $SM_LANGUAGE,
#    };

=head3 Language ID and Skill Level (NOT IMPLEMENTED)

Available on Monster directly. Would require a custom widget to support in Stream.

The C<lskill> parameter is an optional, multi-value argument that restricts
a search to resumes that specify one or more languages and associated
proficiencies. The syntax is:

C<lskill=<lid>-<profid>,<lid>-<profid>>

where C<lid> = language Id and C<profid> = proficiency Id

The allowable languages are in Appendix E. The allowable proficiency Ids
are:

  Proficiency Id        Meaning
  0                     Unknown.
  1                     Basic – Familiar
  2                     Conversational - Limited
  3                     Conversational
  4                     Conversational – Advanced
  5                     Fluent - Wide Knowledge
  6                     Fluent - Full  Knowledge

Example: the following specifies  a seeker with Fluent (full knowledge)
proficiency of the Chinese language and Basic (familiar) proficiency
of the French language.

C<lskill=17-6,32-1>

=cut

=head3 # Years Experience (%yrsexpid%)

Disabled because not available on Monster directly.

The C<yrsexpid> parameter is an optional, multi-value parameter that
restricts the search to resumes with the specified years of experience.

The valid values for this parameter are:

  Value         Meaning
  1             Less than 1 Year
  2             1+ to 2 Years
  3             2+ to 5 Years
  4             5+ to 7 Years
  5             7+ to 10 Years
  6             10+ to 15 Years
  7             more than 15 Years

Example: the following specifies 5-10 years of experience:

C<yrsexpid=4,5>

=cut

#    $standard_tokens{yrsexpid} = {
#        Label   => 'Years of Experience',
#        Type    => 'MultiList',
#        Options => '(Less than 1 Year=1|1+ to 2 Years=2|2+ to 5 Years=3|5+ to 7 Years=4|7+ to 10 Years=5|10+ to 15 Years=6|more than 15 Years=7)',
#        SortMetric => $SM_EXPERIENCE,
#    };

=head3 Project Leadership (%projld%)

Disabled because not available on Monster directly

The projld argument restricts search to resumes that indicate project leadership experience.  The following are the valid values for this parameter:

  Value     Meaning
  0         No – Does not have project leadership experience.
  1         Yes – Has project leadership experience

=cut

#    $standard_tokens{projld} = {
#        Label   => 'Project Leadership',
#        Type    => 'List',
#        Options => '(Any=|No – Does not have project leadership experience=0|Yes – Has project leadership experience=1)',
#        SortMetric => $SM_EXPERIENCE,
#    };

=head3 Target Work Industry (%twindus%)

Corresponds to "Company Industries" on Monster directly.

The C<twindus> parameter restricts search results to resumes that indicate
a desire for employment in a specific industry. One to N activity levels
scores may be passed on the query string. These values will be numbers (0-N)
and should be passed in a comma or space delimited format.

For example: http://rs.monster.com/query.ashx?q=test&twindus=1,487.

Contact your monster representative for a list of Monster industries.

=cut

    $standard_tokens{twindus} = {
        Label   => 'Company Industries',
        Type    => 'MultiList',
        Options => '(Accounting and Auditing Services=28|Advertising and PR Services=29|Aerospace and Defense=6|Agriculture/Forestry/Fishing=1|Architectural and Design Services=30|Automotive and Parts Mfg=7|Automotive Sales and Repair Services=75|Banking=23|Biotechnology/Pharmaceuticals=8|Broadcasting, Music, and Film=21|Business Services - Other=76|Chemicals/Petro-Chemicals=9|Clothing and Textile Manufacturing=14|Computer Hardware=32|Computer Software=33|Computer/IT Services=77|Construction - Industrial Facilities and Infrastructure=4|Construction - Residential & Commercial/Office=78|Education=38|Electronics, Components, and Semiconductor Mfg=11|Energy and Utilities=3|Engineering Services=79|Entertainment Venues and Theaters=80|Financial Services=81|Food and Beverage Production=82|Government and Military=50|Healthcare Services=39|Hotels and Lodging=44|Insurance=24|Internet Services=20|Legal Services=34|Management Consulting Services=31|Manufacturing - Other=12|Marine Mfg & Services=83|Medical Devices and Supplies=84|Metals and Minerals=2|Nonprofit Charitable Organizations=47|Other/Not Classified=85|Performing and Fine Arts=42|Personal and Household Services=48|Personal Care and Cosmetics=10|Printing and Publishing=13|Real Estate/Property Management=26|Rental Services=27|Restaurant/Food Services=45|Retail=17|Security and Surveillance=74|Sports and Physical Recreation=43|Staffing/Employment Agencies=46|Telecommunications Services=22|Transport and Storage - Materials=19|Travel, Transportation and Tourism=18|Waste Management=37|Wholesale Trade/Import-Export=15)',
        SortMetric => $SM_INDUSTRY,
    };

=head3 Industry

Disabled because this is not available on Monster directly. See %twindus% instead.

The C<indus> argument allows the searching of resumes that have experience
in a particular industry. One or more industries may be specified and
the resume must match one of the requested values.

=cut

#    $standard_tokens{industry} = {
#        Label => 'Industry',
#        Type  => 'MultiList',
#        Options => $standard_tokens{twindus}->{Options},
#        SortMetric => $SM_INDUSTRY,
#    };

=head3 sai (Activity Index) [Licensed] * ## NOT IMPLEMENTED ##

The C<sai> parameter restricts search results to resumes that contain a
particular activity index score. One to N activity levels scores may be
passed on the query string. These values will be numbers (0-N) and should
be passed in a comma or space delimited format.

=cut

=head3 tcc (target job category, target occupation)

This is available on Monster directly as "Company Categories". Monster also
have a ttcp field that combines the category with the occupation.

The C<tcc> parameter restricts search results to resumes that contain
a particular job category or occupation. One to N values may be passed
on the query string. These values will be numbers (0-N) and should be
passed in a comma or space delimited format. For example:

http://rs.monster.com/query.ashx?q=test&tcc=1,487.

Contact your monster representative for a list of job categories and
occupations.

=cut

    $standard_tokens{tcc} = {
        Label => 'Company Categories',
        Type  => 'MultiList',
        Options => $MONSTER_CATEGORIES,
        SortMetric => $SM_INDUSTRY + 1, # appear below target work industry
    };


=head3 Monster Career Level (%clv%)

This is available on Monster directly. The options have slightly different
labels on Monster directly

=over 4

=item * None of these

clv=16

=item * Student (Higher education/Graduate)

clv=10

=item * Entry Level

clv=11

=item * Experienced (Non-Manager)

clv=12

=item * Manager (Manager/Supervisor of Staff)

clv=13

=item * Executive (Director, Department Head)

clv=14

=item * Senior Executive (Chairman, MD, CEO)

clv=15

=back

The C<clv> parameter restricts search results to resumes for seekers
of a particular career level. One to N values may be passed on the
query string. These values will be numbers (0-N) and should be passed
in a comma or space delimited format. For example:

http://rs.monster.com/query.ashx?q=test&clv=1,487

Contact your Monster representative for a list of career levels.

=cut

    $standard_tokens{clv} = {
        Label => 'Career Level',
        Type  => 'MultiList',
        Options => '(Student (High School)=16|Student (undergraduate/graduate)=10|Entry Level=11|Experienced (Non-Manager)=12|Manager (Manager/Supervisor of Staff)=13|Executive (SVP, VP, Department Head, etc)=14|Senior Executive (President, CEO, etc)=15)',
        SortMetric => $SM_EXPERIENCE,
    };

=head3 Global Security/Military Options ## NOT IMPLEMENTED ##

Requires a separate license

The extra options are:

=over 4

=item *

Global Security Clearance

=item *

Global Military Status

=back

=cut

};

BEGIN {

=head2 POSTING ONLY TOKENS

=head3 Target Job Type - Permanent (%tjtp%)

The C<tjtp> parameter, with a value of 1, restricts the search to
resumes with owners who have expressed an interest in working in a
I<Permanent> position.

=cut

    $posting_only_tokens{tjtp} = {
        Helper => 'monsterxml_jobtype(Permanent|%jobtypes%)',
        HelperMetric => 3,
    };

=head3 Target Job Type Temp/Contract (%tjttc%)

The C<tjttc> parameter, with a value of 1, restricts the search to
resumes with owners who have expressed an interest in working in a
I<Temporary/Contract> position.

=cut

    $posting_only_tokens{tjttc} = {
        Helper => 'monsterxml_jobtype(Temp/Contract|%jobtypes%)',
        HelperMetric => 3,
    };

=head3 Target Job Type Full Time (%tjtft%)

The C<tjttc> parameter, with a value of 1, restricts the search to resumes
with owners who have expressed an interest in working in a I<Full Time>
position.

=cut

    $posting_only_tokens{tjtft} = {
        Helper => 'monsterxml_jobtype(Full Time|%jobtypes%)',
        HelperMetric => 3,
    };

=head3 Target Job Type Part Time (%tjtpt%)

The C<tjtpt> parameter, with a value of 1, restricts the search to resumes
with owners who have expressed an interest in working in a I<Part Time> position.

=cut

    $posting_only_tokens{tjtpt} = {
        Helper => 'monsterxml_jobtype(Part Time|%jobtypes%)',
        HelperMetric => 3,
    };

=head3 Target Job Type Intern (%tjti%)

The C<tjti> parameter, with a value of 1, restricts the search to resumes
with owners who have expressed an interest in working in a I<Intern> position.

=cut

    $posting_only_tokens{tjti} = {
        Helper => 'monsterxml_jobtype(Intern|%jobtypes%)',
        HelperMetric => 3,
    };

=head3 Target Job Type Per Diem (%tjtpd%)

The C<tjtpd> parameter, with a value of 1, restricts the search to resumes
with owners who have expressed an interest in working in a I<Per Diem>
position.

=cut

    $posting_only_tokens{tjtpd} = {
        Helper => 'monsterxml_jobtype(Per Diem|%jobtypes%)',
        HelperMetric => 3,
    };

=head3 Target Job Type Seasonal (%tjts%)

The C<tjts> parameter, with a value of 1, restricts the search to resumes
with owners who have expressed an interest in working in a I<Seasonal>
position.

=cut

    $posting_only_tokens{tjts} = {
        Helper => 'monsterxml_jobtype(Seasonal|%jobtypes%)',
        HelperMetric => 3,
    };

=head3 Modified date maximum age (%mdatemaxage%)

Available on Monster directly. Monster site seems to say that searching all 
is equivalent to mdatemaxage=788400.

The C<mdatemaxage> parameter restricts the search to resumes that have
been modified or reactivated within the specified number of minutes.

TIP: Use 1440 for one day, 10080 for one week, 525600 for one year etc.

=cut

    $posting_only_tokens{mdatemaxage} = {
        Helper => 'monster_cv_updated_within(%cv_updated_within%)',
        HelperMetric => 3,
    };

=head3 Target salary not included (%tsni%)

Available on Monster directly.

The C<tsni> parameter is a flag that indicates whether resumes that do not
specify a target salary should be included in the search results
(0: Do not include resumes, 1: Include resumes).

C<tsni> only works if you are using C<tnsalmin>/C<tnsalmax>. It does not work if you
use C<tsalmin> and C<tsalmax>

We now use the tnsalmin/tnsalmax parameters because otherwise I get lots of
tickets saying Stream searches give different results to the monster site

The monster site converts all salaries into a normalised annual salary.

The conversion rate they use is:

convert per hour to per year: * 2080
convert per week to per year: * 52
convert per month to per year: * 12
convert per fortnight to per year: * 26
convert per day to per year: * 260

I worked that out by doing lots of searches on the site directly

=cut

    $posting_only_tokens{tsni} = {
        Helper => 'monster_target_salary_not_included(%salary_from%|%salary_to%|%salary_unknown%)',
    };

    $posting_only_tokens{tnsalmin} = {
        Helper => 'monster_target_salary(%salary_from%|%salary_per%)',
    };

    $posting_only_tokens{tnsalmax} = {
        Helper => 'monster_target_salary(%salary_to%|%salary_per%)',
    };
};

BEGIN {

=head2 AUTHTOKENS

=head3 Monster CAT (%cat)

Your Company Access Ticket (CAT) is case-sensitive and should be supplied in
its entirety. It is valid only under the terms and conditions of your contract
with Monster. Restrictions may include, but are not limited to, IP address,
date range and access to specific search functionality.

TIP: If you supply an invalid CAT to the Search Service, you will receive one
of the CAT errors described in the Errors section of this document.

=cut

# Create one authtoken in your subclass
#    $authtokens{monster_cat} = {
#        Label => 'Company Access Ticket',
#        Type  => 'Text',
#        Size  => 60,
#        Mandatory  => 1,
#    };
};

############### HELPERS ##########################

sub PROXY_CLASS {
    return 'Stream::Proxies::Monster';
}

sub AGENT_TIMEOUT {
    return 30;
}

sub monster_cv_updated_within {
    my $self = shift;
    my $cv_updated_within = shift;

    #Directly on Monster site updated 3 days ago = 96h, so matching against our option     
    $cv_updated_within = "4D" if $cv_updated_within =~ m/3D/;

    my $last_updated_mins = $self->CV_UPDATED_WITHIN_TO_MINS( $cv_updated_within );

    # monster keep CVs for 18 months. candidates older than that do not have CVs in Monster
    # through Monster direct, you can only search the last 18 months worth of candidates
    # through RSX, you can search all candidates, but when you go to download the CV, it fails
    # but does not give you an error message
    # #40304 simonr 14/10/2013 updating the max mins from 788400 to 1576800 as directly on the site there is an ALL CV's option which has a value of 1576800 
    
    my $max_last_updated_mins = 1576800; # 3 years
    if ( !$last_updated_mins ) {
        $self->log_debug('Searching the last 36 months worth of candidates');
        return $max_last_updated_mins;
    }

    if ( $last_updated_mins > $max_last_updated_mins ) {
        $self->log_debug('Searching the last 36 months worth of candidates');
        return $max_last_updated_mins;
    }

    return $last_updated_mins;
}

sub monsterxml_jobtype {
    my $self = shift;
    my $needle = shift;

    my (@matches) = $self->SEARCH_ARRAY( $needle, @_ );

    if ( @matches ) {
        return 1;
    }

    return 0;
}

sub monster_wa_location {
    my $self = shift;
    my ($mapping, $location_id) = @_;

    my $wa = $self->token_value('wa');

    return '' if !$wa;

    my ($monster_location) = $self->LOCATION_MAPPING($mapping, $location_id);
   
    return $monster_location."-".$wa if ( $monster_location );
    return '';
}   

sub monsterxml_location_mapping {
    my $self = shift;
    my $location_id = shift or return;
    my $location_within = shift;

    my $location = $self->location_api->find( $location_id );

    unless ( $location ) {
        $self->log_warn( 'Unable to fetch location [%s] from DB', $location_id );
        return;
    }

    my ($mapped_postcode) = $location->get_exact_mapping( 'stream_monsterxml_postcode' );
    
    my $country = $location->country()
        or return $self->LOCATION_MAPPING('monster', $location_id, $location_within);

    my $zipcode = $mapped_postcode || $location->zipcode();
    if ( $zipcode ) {
        my $country_code = $location->iso_country();
        if ( $country_code eq 'GB' ) {
            $country_code = 'UK';
        }

        $self->log_debug('Country code is [%s]', $country_code);
        $self->token_value('country_code' => $country_code);

        $self->log_debug('Sending a zipcode/postcode: [%s]', $zipcode);
        if ( $country_code eq 'NL' ) {
            # Dutch postcodes have a major area and a minor area
            # monster only want the major area. So for a postal code of 1012RJ, we just send 1012
            # ironically Monster want the full UK postcode, they don't accept just the major part
            $zipcode =~ s/[A-Z]{2}$//;
            $self->log_debug('This is a dutch postcode so only sending the major part of the postal code: [%s]', $zipcode);
        } elsif ( $country_code eq 'UK') {
            #we want to use the real postcode if one was provided during the search
            my $pcode_provided = $self->token_value('location_id_postcode');
            if ($pcode_provided) {
                #$self->log_debug('A real postcode was provided for this search: [%s]', );
                $zipcode = $self->LOCATION_FULL_POSTCODE($location_id, $pcode_provided);
            }
        } elsif( $country_code eq 'CA') {
            my $mapping = $location->get_mapping('monster_ca_provinces')
              or $self->throw_known_internal('Location not supported.');
            return $mapping;
        }

        my $monster_zipcode = $zipcode .= '-' . (int($location_within+0.5) || '1' );
        $monster_zipcode =~ s/\s+//g;
        $self->log_debug('Setting rpcr to [%s]', $monster_zipcode);
        $self->token_value('rpcr'         => $monster_zipcode);

        return;
    }

    # fallback to normal monsterxml location mappings
    $self->log_debug('No postcode found so normal monster mappings');
    return $self->LOCATION_MAPPING('monster', $location_id, $location_within);
}

sub monster_target_salary {
    my $self = shift;
    my $salary_value = shift;
    my $salary_per   = lc(shift);

    if ( !$salary_value ) {
        return $salary_value;
    }

    my %conversion = (
        hour  => 2080,
        day   => 260,
        week  => 52,
        month => 12,
#        fortnight => 26,
    );

    if ( exists( $conversion{ $salary_per } ) ) {
        my $conversion_rate = $conversion{ $salary_per };
        return $salary_value * $conversion_rate;
    }

    return $salary_value;
}

sub monster_target_salary_not_included {
    my $self = shift;
    my $salary_from = shift;
    my $salary_to   = shift;
    my $salary_unknown = shift;

    if ( $salary_from || $salary_to ) {
        return $salary_unknown || 0;
    }

    return;
}

sub search_url {
    return 'http://rsx.monster.com/query.ashx';
}

sub monsterxml_catch_missing_credentials {
    my $self = shift;

    my $cat     = $self->token_value('cat');

    if ( !$cat ) {
        return $self->throw_login_error('Missing Monster CAT code - ask Monster for your CAT code and try again');
    }

    return;
}

sub search_submit {
    my $self = shift;

    $self->monsterxml_catch_missing_credentials();

    my $search_uri = $self->monster_search_uri();

    return $self->search_get(
        $search_uri,
#        Content_Type => $self->search_content_type(),
#        Content      => '',
    );
}

sub monster_search_uri {
    my $self = shift;

    my %query_form = (
        # Mandatory
        'q'           => $self->token_value('keywords') || '',
        ver           => '1.7',
        page          => $self->current_page(),
        pagesize      => $self->results_per_page(),
        cat           => $self->token_value( 'cat' ) || '',
    );

    my %optional_tokens = (
        # token     => CGI param to send to Monster (if blank it uses the token name instead)
        qrjt        => '', # Recent job title (comma delimited)
        qtjt        => '', # Target job title
#        qajt        => '', # Recent or job title
        qrcn        => '', # Recent company name
        qrjd        => '', # Recent job description
        qsn         => '', # Recent school name

        #sort        => '', # sort by parameter (default is Mdate, can also be Rank)
        mdatemaxage => '', # max age of cv in minutes
        tjtp        => '', # target job type permanent
        tjttc       => '', # target job type temp/contract
        tjtft       => '', # target job type full time
        tjtpt       => '', # target job type part time
        tjti        => '', # target job type intern
        tjtpd       => '', # target job type per diem
        tjts        => '', # target job type seasonal

        twlb        => '', # target work location boolean
        relo        => '', # willing to relocate
#        twsite      => '', # willing to work on/off site
#        mgrex       => '', # managerial experience
#        projld      => '', # project leadership
        country_code => 'co', # country code
    );
    while ( my ($optional_token, $cgi_param) = each %optional_tokens ) {
        $cgi_param ||= $optional_token;
        my $token_val = $self->token_value( $optional_token );
        if ( defined( $token_val ) ) {
            # if the token is empty, do not add it to the search parameters
            # remembering that 0 is a valid option
            if ( $token_val ne '' ) {
                $query_form{ $cgi_param } = $token_val;
            }
        }
    }

    my %optional_form = (
        # education level (space delimited)
        edulv         => $self->join_tokens( ' ' => 'education' ) || '',
        # residence location (space delimited?)
        rlid          => $self->join_tokens( ' ' => 'rlid' ) || '',
        # residence postal code radius
        rpcr          => $self->join_tokens( ' ' => 'rpcr' ) || '',
        # target work location
        twlid         => $self->join_tokens( ' ' => 'twlid' ) || '',
        # work authorisation
        # NOW SUPPORTED (%country_id%-%workstatus_id%)
        wa            => $self->join_tokens(' ' => 'wa_id'),        # occupation codes
#        moc           => $self->join_tokens( ' ' => 'moc' ) || '',
        # education major
#        edumjr        => $self->join_tokens( ' ' => 'edumjr' ) || '',
        # language of resume
        rslang        => $self->join_tokens( ' ' => 'rslang' ) || '',
        # language skills
#        lang          => $self->join_tokens( ' ' => 'lang' ) || '',
        # language ID and skill
        # NOT SUPPORTED (%language_id%-%proficiency_id%)
        # NB. comma separated
        #lskill        => $self->join_tokens( ',' => 'lskill' ),
        # years experience (comma separated)
#        yrsexpid      => $self->join_tokens( ',' => 'yrsexpid' ) || '',
        # target industry
        twindus       => $self->join_tokens( ',' => 'twindus' ) || '',
        # industry
#        indus         => $self->join_tokens( ',' => 'indus' ) || '',
        # target job category or occupation
        tcc           => $self->join_tokens( ' ' => 'tcc' ) || '',

        # career level
        clv           => $self->join_tokens( ',' => 'clv' ) || '',
    );
    
    # only send optional fields if they have a value
    while ( my ($field, $value) = each %optional_form ) {
        if ( $value ) {
            $query_form{ $field } = $value;
        }
    }

    my $salary_min = $self->token_value( 'salary_from' );
    my $salary_max = $self->token_value( 'salary_to' );

    if ( $salary_min || $salary_max ) {
        # only send salary cur and salary per if they have entered a salary
        $query_form{tnsalmin} = $self->token_value( 'tnsalmin' ) || 0;
        $query_form{tnsalmax} = $self->token_value( 'tnsalmax' );
        $query_form{tsni}    = $self->token_value( 'tsni' );
        $query_form{tsalcur} = $self->token_value( 'salary_cur_id' );
        $query_form{tsaltyp} = $self->token_value( 'salary_per_id' );
    }

    $query_form{sort} = $self->token_value_or_default('sort') . ",distance,mdate" if $self->token_value_or_default('sort') eq 'Rank';
    $query_form{sort} = $self->token_value_or_default('sort') . ",distance,rank" if $self->token_value_or_default('sort') eq 'Mdate';
    


    my $search_uri = URI->new( $self->search_url() );
    $search_uri->query_form( \%query_form );

    return $search_uri;
}
# we perform a get
#'http://rsx.monster.com/query.ashx?q=%s&ver=1.7&cat='

# qrjt = Recent job title (comma delimited)
# qtjt = Target job title
# qajt = Recent or job title
# qrcn = Recent company name
# qrjd = Recent job description
# qsn  = Recent school name
# sort = 'Rate', 'Mdate' (default)
# page = 1....
# pagesize = 20
# maxfound = 1000
## folderid # optional (OFCCP compliance)
# lcid = Locale for boolean search (1033=US english)
# mdatemaxage = in minutes! (1440=1 day,10080=1 week)
## rv= search within certain resumes
# tjtp=1  # only search permanent positions
# tjttc=1 # only search temp/contract
# tjtft=1 # only search full time

# sub default_url {
# }

sub search_failed {
    return qr/Error Number="/;
}

sub scrape_candidate_xpath { return '/Monster/Resumes/Resume'; }
sub scrape_candidate_details {
    return (
        # SID and Value are the same, but Monster keep switching between the two
        # without telling us so we handle both
#        candidate_id       => './@Value', # e.g. u84tgt63iwut2z7b
        candidate_id       => './@SID', # e.g. u84tgt63iwut2z7b
        candidate_value    => './@Value',

        relevance          => './Relevance/text()',
        distance           => './Distance/text()', # not sure if this is still used
        raw_date_created   => './DateCreated/@Date',
        raw_date_modified  => './DateModified/@Date',
        snippet            => './ResumeTitle/text()',

        confidential       => './PersonalData/Confidential/text()',
        email              => './PersonalData/EmailAddress/text()',
        firstname          => './PersonalData/Name[1]/First/text()',
        lastname           => './PersonalData/Name[1]/Last/text()',
        country_id         => './PersonalData/Address[1]/Country/@LID',
        state_id           => './PersonalData/Address[1]/State/@LID',
        city               => './PersonalData/Address[1]/City/text()',
        postal_code        => './PersonalData/Address[1]/PostalCode/text()',
        location_text      => './PersonalData/Address[1]/Location/text()',

        military_service_flag => './PersonalData/MilitaryExperience/ServiceFlag/text()',
        military_involvement  => './PersonalData/MilitaryExperience/MilitaryInvolvement/text()',
        objective          =>   './Objective/text()', 
        target             => [
            './Target' => {
                job_title  => './JobTitle/text()',
                relocation => './Relocation/text()',
                salary_cur => './Salary[1]/Currency/text()',
                salary_min => './Salary[1]/Min/text()',
                salary_max => './Salary[1]/Max/text()',
                salary_per => './Salary[1]/Type/text()',
                job_types  => [
                    './JobTypes/JobType/text()',
                ],
            },
        ],

        education          => [
            './Educations/Education' => {
                id    => './Level/@ID',
                level => './Level/text()',
            },
        ],

        experience         => [
            './Experiences/Experience' => {
                company_name => './Company/Name/text()',
                job_title    => './Job/Title/text()',
                description  => './Job/Description/text()',
            },
        ],

        work_authorisation => [
            './WorkAuths/WorkAuth' => {
                country => './Country/text()',
                auth_type => './AuthType/text()',
            },
        ],

        boards             => [
            './Boards/Board' => {
                id   => './@ID',
                name => './text()',
            },
        ],
    );
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    # format is YYYY-MM-DDTHH:MM:SS
    my $dt_parser = DateTime::Format::Strptime->new(
        pattern => '%FT%T',
    );

    my $raw_date_created  = $candidate->attr('raw_date_created');
    my $raw_date_modified = $candidate->attr('raw_date_modified');

    my $date_created = $dt_parser->parse_datetime( $raw_date_created );
    my $date_modified = $dt_parser->parse_datetime( $raw_date_modified );

    # some candidates do not have a date created
    if ( !$date_created && $date_modified ) {
        $date_created = $date_modified;
    }

    $candidate->attrs(
        date_created       => $date_created  ? $date_created->epoch() : undef,
        cv_last_updated    => $date_modified ? $date_modified->epoch() : undef,
    );

    my $experiences_aref = $candidate->attr('experience');
    my $headline;
    if ( $experiences_aref ) {
        $headline = $experiences_aref->[0]->{job_title};
    }

    my $targets_aref = $candidate->attr('target');
    if ( $targets_aref ) {
        my $salary_cur = $targets_aref->[0]->{salary_cur};
        my $salary_min = $targets_aref->[0]->{salary_min};
        my $salary_max = $targets_aref->[0]->{salary_max};
        my $salary_per = $targets_aref->[0]->{salary_per};

        if ( $salary_min || $salary_max ) {
            $candidate->attr('salary' => $self->build_salary( $salary_cur, $salary_min, $salary_max, $salary_per ) );
        }

        if ( !$headline ) {
            $headline = $targets_aref->[0]->{job_title};
        }
    }

    if ( $headline ) {
        $candidate->attr('headline' => $headline);
    }

    $self->log_debug($candidate->attr('name'));
    if (!$candidate->attr('firstname') && !$candidate->attr('lastname')) {
        $candidate->attr('name' => 'Anonymous candidate');
    }

    $self->monsterxml_standardise_work_authorisation( $candidate );

    if ( !$candidate->candidate_id() ) {
        $self->report_error('Monster have changed the XML again');
        my $candidate_id = $candidate->attr('candidate_value');
        if ( $candidate_id ) {
            $candidate->candidate_id( $candidate_id );
        }
    }

    $candidate->has_chargeable_profile(1);

    return $candidate;
}

sub monsterxml_standardise_work_authorisation {
    my $self = shift;
    my $candidate = shift;

    my $work_authorisation_aref = $candidate->attr('work_authorisation');

    if ( !$work_authorisation_aref ) {
        return;
    }

    my (@authorised, @not_authorised);
    foreach my $auth_ref ( @$work_authorisation_aref ) {
        if ( ref( $auth_ref ) ) {
            if ( $auth_ref->{auth_type} eq 'Authorized' ) {
                push @authorised, $auth_ref->{country};
            }
            elsif ( $auth_ref->{auth_type} eq 'Not Authorized' ) {
                push @not_authorised, $auth_ref->{country};
            }
        }
    }

    $candidate->attr('authorised_to_work' => \@authorised );
    $candidate->attr('not_authorised_to_work' => \@not_authorised );

    return 1;
}

sub search_number_of_results_xpath {
    return '/Monster/Resumes/@Found';
}

sub scrape_paginator {
    my $self = shift;

    my $logger = $self->logger();

    my $total_results = $self->total_results();
    my $results_per_page = $self->results_per_page();

    $logger->debug('Found %d candidates, fetched %d of them', $total_results, $results_per_page );

    if ( $total_results <= $results_per_page ) {
        $logger->debug('There is only 1 page of results');
        $self->total_pages( 1 );
    }
    else {
        my $total_pages = POSIX::ceil( $total_results / $results_per_page );
        $logger->debug('There are %d pages', $total_pages);
        $self->total_pages( $total_pages );
    }
}

sub download_cv_url {
    # Monster Test URL
#    return 'https://63.112.170.8:8443/bgwBroker';

    # good test url
#    return 'https://208.71.198.74:8443/bgwBroker';
    return 'https://gateway.monster.com:8443/bgwBroker';
}

sub download_profile_url {
    return 'https://gateway.monster.com:8443/bgwBroker';
}

sub download_cv_failed  { return qr/<ReturnCode returnCodeType="failure">/; }
sub download_cv_success { return qr/<ReturnCode returnCodeType="success">|<QueriesResponse /; }
sub download_profile_failed { return download_cv_failed (@_); }
sub download_profile_success { return download_cv_success (@_); }



sub download_profile_xml {
    return download_candidate_xml(@_);
}

sub download_cv_xml {
    return download_candidate_xml(@_);
}

# example xml:
# http://schemas.monster.com/Current/XSI/SOAPEnvelopeSamples/SampleSOAPEnvelopeWithCATAuthentication.xml
# http://schemas.monster.com/Current/XSI/QuerySamples/RetrieveResume.xml
# schema:
# http://schemas.monster.com/Current/XSD/QueryDocumentation.html
sub download_candidate_xml {
    my $self = shift;
    my $candidate = shift;

    my $xml = Bean::XML->new( ENCODING => 'Windows-1252' );

    my $message_id = join('-', time(), $self->company(), $candidate->candidate_id() );

    $xml->startTag('SOAP-ENV:Envelope', 'xmlns:SOAP-ENV' => 'http://schemas.xmlsoap.org/soap/envelope/');
        $xml->startTag('SOAP-ENV:Header');
            $xml->startTag('mh:MonsterHeader', 'xmlns:mh' => 'http://schemas.monster.com/MonsterHeader');
                $xml->startTag('mh:MessageData');
                    $xml->startTag('mh:MessageId');
                        $xml->characters( $message_id );
                    $xml->endTag('mh:MessageId');
                    $xml->startTag('mh:Timestamp');
                        $xml->characters('2003-07-23T10:48:43Z');
                    $xml->endTag('mh:Timestamp');
                $xml->endTag('mh:MessageData');
            $xml->endTag('mh:MonsterHeader');

            $xml->startTag('cat:CompanyAuthHeader', 'xmlns:cat' => 'http://webservices.monster.com/MonsterPortal');
                $xml->startTag('cat:CompanyAccessTicket');
                    $xml->characters( $self->token_value( 'cat' ) );
                $xml->endTag('cat:CompanyAccessTicket');
            $xml->endTag('cat:CompanyAuthHeader');
        $xml->endTag('SOAP-ENV:Header');
        $xml->startTag('SOAP-ENV:Body');
# body
            $xml->startTag('Query', 'xmlns' => 'http://schemas.monster.com/Monster', 'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance' );
                $xml->startTag('Target');
                    $xml->characters('JobSeekers');
                $xml->endTag('Target');

                $xml->startTag('ReturnRestriction');
                    # we want the resume text not the parsed xml from monster
                    $xml->startTag('ResumeRestriction');
                        $xml->startTag('StoreRenderedTextResume');
                            $xml->characters(0); # send 1 to get the text CV. send 0 to get the HTML CV according to Gillian
                        $xml->endTag('StoreRenderedTextResume');
                    $xml->endTag('ReturnRestriction');
                $xml->endTag('ResumeRestriction');

                $xml->startTag('SelectBy');
                    $xml->startTag('Criteria');
                        $xml->characters('TextResumeId');
                    $xml->endTag('Criteria');
                    $xml->startTag('Value');
                        $xml->characters( $candidate->candidate_id() );
                    $xml->endTag('Value');
                    $xml->startTag('Criteria');
                        $xml->characters('ChannelAlias');
                    $xml->endTag('Criteria');
                    $xml->startTag('Value');
                        $xml->characters( 'mons' );
                    $xml->endTag('Value');
                $xml->endTag('SelectBy');
            $xml->endTag('Query');
        $xml->endTag('SOAP-ENV:Body');
    $xml->endTag('SOAP-ENV:Envelope');
    $xml->end();

    return $xml;
}

=head2 download_cv

Complete override of the L<Stream::Template> version of this method.

=cut

sub download_cv {
    my $self = shift;
    my $candidate = shift;

    my $response = $self->download_cv_submit( $candidate );

    $self->log_debug('Checking for errors');

    $self->check_for_success_or_failure(
        $self->download_cv_success() || undef,
        $self->download_cv_failed()  || undef,
    );

    # scrape xml for candidates
    $self->scrape_generic_download_profile($candidate);
    return $self->scrape_download_cv( $candidate ); 
}

sub download_profile {
    my $self = shift;
    my $candidate = shift;

    $self->login() if $self->download_profile_requires_login();
    $self->download_profile_submit( $candidate );
    $self->log_debug('Checking for errors');
   
    $self->check_for_success_or_failure(
        $self->download_profile_success() || undef,
        $self->download_profile_failed()  || undef,
    );
    
    $self->scrape_download_cv ($candidate);
    # Scrape for downloaded CV.  
   
    # scrape xml for candidates
    return $self->scrape_generic_download_profile( $candidate ); 
}

sub scrape_download_profile_xpath {
    my $self = shift;
    $self->registerNs( 'ADC' => 'http://schemas.monster.com/Monster');
    return '/SOAP-ENV:Envelope/SOAP-ENV:Body/ADC:QueriesResponse/ADC:JobSeekers/ADC:JobSeeker';
}

sub scrape_download_profile_details {
    my $self = shift;
    my $candidate = shift;
    return (
    email                     => './ADC:PersonalData/ADC:Contact/ADC:E-mail/text()',
    disabled                  => './ADC:PersonalData/ADC:DemographicDetail/ADC:Disability/@status',
    victimofterrorism         => './ADC:PersonalData/ADC:DemographicDetail/ADC:VictimOfTerrorism/@status',
    exconvict                 => './ADC:PersonalData/ADC:DemographicDetail/ADC:ExConvict/@status',
    workatweekend             => './ADC:Resumes/ADC:Resume/ADC:WorkPreferences/ADC:Weekend/text()',
    workovertime              => './ADC:Resumes/ADC:Resume/ADC:WorkPreferences/ADC:Overtime/text()',
    availabletostart          => './ADC:Profile/ADC:Availability/ADC:AvailableTimeStart/text()',
    highest_education_degree  => './ADC:Profile/ADC:HighestEducationDegree/text()',
    target_job_title          => './ADC:Profile/ADC:TargetJobTitle/ADC:Title/text()',
    career_status             => './ADC:Profile/ADC:CareerStatus/text()',
    willing_to_travel_id      => './ADC:Resumes/ADC:Resume/ADC:WillingToTravel/@id',


    contact_preference        => './ADC:Profile/ADC:ContactPreference/text()',

    geographic_prefs          => [
        './ADC:Resumes/ADC:Resume/ADC:DetailedSearchExtras/ADC:GeographicPrefs/ADC:GeographicPref' => {
            'city'          => './ADC:City',
            'state'         => './ADC:State',
            'country_code'  => './ADC:CountryCode',
            'continent'     => './ADC:Continent',
        }
    ],

    employment_references => [
        './ADC:Resumes/ADC:Resume/ADC:EmploymentReferences/ADC:Reference' => {
            'position_title' => './ADC:PositionTitle',
            'contact_name'   => './ADC:Contact/ADC:Name',
            'company_name'   => './ADC:Contact/ADC:CompanyName',
            'email'          => './ADC:Contact/ADC:E-mail',
            'phones'         => [
                './ADC:Contact/ADC:Phones/ADC:Phone' => {
                    phonetype => './@phoneType',
                    number => './text()',
                }
            ],
        }
    ],


    experience         => [
        './ADC:Resumes/ADC:Resume/ADC:EmploymentHistory/ADC:Position' => {
                company_name => './ADC:EmployerName/text()',
                job_title    => './ADC:PositionTitle/text()',
                description  => './ADC:ExperienceDescription/text()',
                fromyear    => './ADC:DateRange/ADC:StartDate/ADC:Year/text()',
                frommonth   =>'./ADC:DateRange/ADC:StartDate/ADC:Month/text()',
                toyear      =>'./ADC:DateRange/ADC:EndDate/ADC:Year/text()',
                tomonth     =>'./ADC:DateRange/ADC:EndDate/ADC:Month/text()',
            },
        ],
    
    education_history => [
        './ADC:Resumes/ADC:Resume/ADC:EducationalHistory/ADC:SchoolOrInstitution' => {
            school_name => './ADC:SchoolName/text()',
            toyear => './ADC:DateRange/ADC:EndDate/ADC:Year/text()',
            tomonth => './ADC:DateRange/ADC:EndDate/ADC:Month/text()',
            summary => './ADC:EducationSummary/text()',
            degree  => './ADC:EducationDegree/text()',
            fields_of_study => [
                './ADC:EducationFieldsOfStudy/ADC:EducationFieldOfStudy' => {
                    'field_of_study' => './ADC:FieldOfStudyName',
                },
            ],
        },
    ],

    skills => [
        './ADC:Skills/ADC:Skill' => {
            skillname => './ADC:SkillName/text()',
            skillused => './ADC:SkillUsed/text()',
        }
    ],

    affiliations => [
        './ADC:Affiliations/ADC:Affiliation' => {
            name => './ADC:AssociationName/text()',
            role => './ADC:Role/text()',
            from => './ADC:DateRange/ADC:StartDate/ADC:Date/text()',
        }
    ],

    compensation_type => './ADC:DesiredCompensation/CompensationType/text()',
    compensation_currency => './ADC:DesiredCompensation/Currency/text()',
    compensation_rate => './ADC:DesiredCompensation/Rate/text()',
    compensation_ratemin => './ADC:DesiredCompensation/RateMin/text()',
    compensation_ratemax => './ADC:DesiredCompensation/RateMax/text()',
    
    ideal_job_description => './ADC:Resumes/ADC:Resume/ADC:DesiredEmployer/ADC:IdealJobDescription',


    employer_categories => [
        './ADC:Resumes/ADC:Resume/ADC:DesiredEmployer/ADC:TargetEmployerCategories/ADC:TargetEmployerCategory' => {
            'category' => './text()',
        }
    ],

    employer_occupations => [
        './ADC:Resumes/ADC:Resume/ADC:DesiredEmployer/ADC:TargetEmployerOccupations/ADC:TargetEmployerOccupation' => {
            'occupation' => './ADC:Name/text()',
        }
    ],

    job_industries => [
        './ADC:Resumes/ADC:Resume/ADC:TargetJobIndustries/ADC:Industry' => {
            'industry' => './ADC:IndustryName/text()'
        }
    ],

    job_titles => [
         './ADC:Resumes/ADC:Resume/ADC:TargetJobTitles/ADC:TargetJobTitle' => { 
            'targetjobtype' => './@type',
            'title' => './ADC:Title/text()',
        }
    ],

    languages => [
        './ADC:Profile/ADC:Languages/ADC:Language' => {
            'languagename' => './ADC:LanguageName/text()',
            'languageproficiency' => './ADC:LanguageProficiency/text()',
        }
    ],

    phones => [
        './ADC:PersonalData/ADC:Contact/ADC:Phones/ADC:Phone' => {
            phonetype => './@phoneType',
            priority => './@priority',
            number => './text()',
        }
    ],

    html_cv => './/ADC:TextResume/text()',
    
    );
}

sub scrape_fixup_candidate_profile {
    my $self = shift;
    my $candidate = shift;
    my $willing_to_travel_id = $candidate->attr("willing_to_travel_id");

    my %willing_to_travel_lookup = {
        1 => 'No travel required',
        2 => 'Up to 25% travel',
        3 => 'Up to 50% travel',
        4 => 'Up to 75% travel',
        5 => 'Up to 100% travel',
    };

    $candidate->attr("willing_to_travel" => $willing_to_travel_lookup{$willing_to_travel_id});
}


sub scrape_download_cv {
    my $self = shift;
    my $candidate = shift;

    $log->info('Scraping Monster XML');


    $self->registerNs( 'ADC' => 'http://schemas.monster.com/Monster');

    if ( $self->findvalue('//ReturnCode[@returnCodeType="failure"]') ) {
        # safety check: we should never get here
        # because the download_cv_failed() should pick this up
        $log->error('Monster returned an error downloading this CV.');

        die "Unable to download CV: Monster returned a failure";
    }

    if ( !$candidate->email() ) {
        $log->info("Candidate has no email. Attempting to update its email address");
        $self->monsterxml_update_profile( $candidate );
    }

    #Firstly, try and get it from the ResumeDocument node
    my ($resume_content, $resume_filename, $resume_content_type) = $self->monsterxml_cv_from_resume();


    # Calculate the HTTP::Response attachment
    my $http_response;

    if ($resume_content && $resume_filename && $resume_content_type){
        require MIME::Base64;
        my $resume_content_decoded = MIME::Base64::decode_base64( $resume_content );

        $http_response = $self->to_response_attachment({
                                                        decoded_content => $resume_content_decoded,
                                                        filename        => $resume_filename,
                                                        content_type    => $resume_content_type,
                                                       });
    }else{

        #Secondly, try and get from the text resume
        my $raw_text = $self->monsterxml_cv_from_monster();
        if ( !$raw_text ) { #Thirdly, try to build it ourselves.
            $raw_text = $self->monsterxml_build_cv_from_profile();
        }

        $http_response =  $self->monsterxml_save_content( $candidate, $raw_text );
    }

    if( my $email = $candidate->email() ){
        $log->info("Setting response header with X-Candidate-Email = ".$candidate->email());
        $http_response->header( 'X-Candidate-Email' , $candidate->email() );
    }
    return $http_response;

}

sub monsterxml_update_profile {
    my $self = shift;
    my $candidate = shift;

    $self->log_debug('This candidate does not have an email address so trying to scrape it');
    my $email = $self->findvalue('//ADC:PersonalData[1]/ADC:Contact/ADC:E-mail');
    if ( $email ) {
        $self->log_debug('Found candidate email [%s]', $email);
        $candidate->email( $email );
    }

    return $candidate;
}

sub monsterxml_cv_from_resume {
    my $self = shift;
    my $content = $self->findvalue('//ADC:ResumeDocument[1]/ADC:File');
    my $filename = $self->findvalue('//ADC:ResumeDocument[1]/ADC:FileName');
    my $content_type = $self->findvalue('//ADC:ResumeDocument[1]/ADC:FileContentType');

    if ($content && $filename && $content_type){
        return ($content, $filename, $content_type);
    }
}

sub monsterxml_cv_from_monster {
    my $self = shift;
    return $self->findvalue('//ADC:Resume[1]/ADC:TextResume/text()');
}

sub monsterxml_build_cv_from_profile {
    my $self = shift;

    # inherited from Stream/Engines/XML.pm
    return $self->apply_xslt( 'monstercv.xsl' );
}

sub monsterxml_save_content {
    my $self = shift;
    my $candidate = shift;
    my $raw_text = shift;

    my $candidate_id = $candidate->candidate_id();

    my $content_type;
    my $filename = 'monster_'. $candidate_id . '.';
    if ( Bean::HTMLStrip->looks_like_html( $raw_text ) ) {
        $filename .= 'html';
        $content_type = 'text/html; charset=UTF-8';
    }
    else {
        $filename .= 'txt';
        $content_type = 'text/plain; charset=UTF-8';

        # convert unix new lines to windows new lines (otherwise the text cv looks rubbish in windows)
        $raw_text = _convert_to_windows_new_lines( $raw_text );
    }
    
    my $response_attachment = $self->to_response_attachment({
        decoded_content => $raw_text,
        filename        => $filename,
        content_type    => $content_type,
    });

    return $response_attachment;
}

sub _convert_to_windows_new_lines {
    my $raw_text = shift;

    my $CRLF = "\015\012";
    my $CR   = "\015";
    my $LF   = "\012";
    if ( $raw_text =~ m/$CRLF/ ) {
        # nothing to do
        return $raw_text;
    }

    if ( $raw_text =~ m/$LF/ ) {
        $raw_text =~ s/$LF/$CRLF/g;
        return $raw_text;
    }

    if ( $raw_text =~ m/$CR/ ) {
        $raw_text =~ s/$CR/$CRLF/g;
        return $raw_text;
    }

    return $raw_text;
}

sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    if ( !defined( $content ) ) {
        $content = $self->content();

        return unless $content;
    }

    # monster is ip restricted
    if ( $content =~ m/HTTP Error 403.6/ || $message =~ m/HTTP Error 403.6/ ) {
        return $self->throw_known_internal('IP Restricted');
    }
    
    if ( $content =~ m/(An unexpected error occurred, Please resubmit the request and contact Monster customer service if the issue persists)/ ) {
	return $self->throw_unavailable($1);
    }
       
    if ( $content =~ m/Cannot find lat\/lon for geoid for radius search/ ) {
        return $self->throw_known_internal('Monster did not recognise the postcode selected');
    }

    if ( $content =~ m/Missing CAT/ ) {
        return $self->throw_login_error('Missing CAT');
    }

    if ( $content =~ m/Invalid length for a Base\-64 char array/ ) {
        return $self->throw_login_error('Your Monster CAT is incorrect, it looks too short');
    }

    if ( $content =~ m/Inactive CAT/ ) {
        return $self->throw_inactive_account('Inactive CAT');
    }

    if ( $content =~ m/Resume search license not found/ ) {
        return $self->throw_login_error('No Monster resume search license');
    }

    if ( $content =~ m/Client not authenticated after processing available headers/ ) {
        return $self->throw_login_error('Client not authenticated after processing available headers. Contact Monster and ask them to set the permissions on the cat code correctly');
    }

    if ( $content =~ m/(Invalid or broken AuthToken|CAT processing error|Search license not found|CAT authentication failure|Company Access Ticket is invalid)/ ) {
        return $self->throw_login_error( $1 );
    }

    if ( $content =~ m{(No active RESUME_VIEW license was found for .*)</Message>} ) {
        return $self->throw_no_cvsearch( $1 );
    }

    if ( $content =~ m/(No ResumeView license for \S+)/ ) {
        return $self->throw_login_error( $1 );
    }

    if ( $content =~ m{(the page you are trying to reach is temporarily unavailable)} ) {
        # http://www.adcourier.com/manage/view-log.cgi?key=755344&destination=
        return $self->throw_unavailable( $1 );
    }

    if ( $content =~ m{(TextResumeID \[.*\] is invalid)} ) {
        # candidate no longer exists on monster. See #49455 for more information.
        return $self->throw_cv_unavailable( $1 );
    }

    if ( $content =~ m{(JobSeeker didnt not have any active resumes)} ) {
        # candidate does not have a CV
        return $self->throw_cv_unavailable( $1 );
    }

    if ( $content =~ m{(Error Number="0xA0000999")} ) {
         #clients account has expired
          return $self->throw_login_error("Your Monster CAT has expired, please contact Monster to solve this issue.");
     }
    
    # Other error messages, not sure what this guy means
    # Error in search. Status: -2
    # e.g. http://blade7.adcourier.com/manage/view-log.cgi?key=270515&destination=

    if ( $content =~ m{<Message>(.+?)</Message></Error>} ) {
        return $1;
    }

    return undef;
}

1;

__END__

=head2 ERROR MESSAGES

=head3 IP Restrictions

The monster interface is IP Restricted. Currently it can only be accessed
by deweb3.

=head3 Interface errors

To view the details of this error the query must be re-issued with the
debug parameter and the debug password. For example:

http://rsx.monster.com/query.ashx?q=test&rev=1.7cat=[CAT]&debug=[password]

The debug password can be obtained from Monster technical support.

The following lists the detailed error messages that may be encountered
during a resume search.

B<  Value>       B<Message>                 B<Meaning>
  0xA0000999  Processing error        Search error
                                      Contact Monster - often means that the
                                      CAT has not been setup correctly
  0xA0000000  Unknown error           A fatal unknown error
  0xA0001000  Search error            General search error
  0xA0001001  Search parse error      Query parsing error
  0xA0001002  Search timeout          Search timeout
  0xA0001003  Search index error      Fatal index error
  0xA0001004  Unable to log search    A fatal error occurred while logging
                                      the search string.
  0xA0002000  Invalid input parameter Input parameters are invalid or out
                                      of range
  0xA0003000  Invalid CAT             Company Access Ticket is invalid
  0xA0003001  Missing CAT             No CAT was specified
  0xA0003002  Invalid CAT version     CAT is not a recognized version
  0xA0003003  Invalid CAT IP address  CAT IP mismatches client IP
  0xA0003004  Invalid CAT user        CAT user is not valid
  0xA0003005  Inactive CAT            CAT has expired, or is not yet active
  0xA0003006  Invalid username        CAT contains an invalid username
  0xA0003007  Invalid password        CAT contains an invalid password
  0xA0003008  Invalid email address   CAT contains an invalid email address
  0xA0003009  License not found       Search license was not found
  0xA0003999  Error                   General error
  0xA0004000  Job not found           Job not found
  0xA0005000  Required parameter not found
                                      Required parameter is missing

=over

=item Processing error

This is a general error but usually means that the boolean query is invalid.

e.g. and "test" and "developer" should be "test" and "developer"
.net should be ".net"

Monster are very strict on Boolean search syntax. They will even complain if there is trailing
whitespace after the query (we strip that out to avoid it)

=item Failed to process NVC by "TargetJobAttributesInternal"

Probably our bug - it means that the jobtype parameters aren't
correct (e.g. we sent Permanent when we should have sent 1)

=item Failed to parse "rpcr" parameter

Consultant's fault - usually they have entered something ridiculous
in the Location within box. They should be entering a number in miles
(e.g. Within 10 miles) and they sent something stupid like Bristol
(within Bristol miles!)

=back

=head3 Downloading CVs

If you the only error message you get is B<ReturnCode returnCodeType="failure">1</ReturnCode>
contact Monster because your license has not been setup correctly

=cut

# vim: expandtab shiftwidth=4

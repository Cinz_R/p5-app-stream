#!/usr/bin/perl

package Stream::Templates::bitbucket_xray;
use strict;
use utf8;

use base qw(Stream::Templates::google_cse);

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

$derived_tokens{and_terms} = {
    Type => 'Text',
    Helper => 'google_split_keywords(%keywords%|and_terms|or_terms|not_terms|exact_terms)',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{or_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{not_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{exact_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

sub build_google_search_query {
    my $self = shift;

    my $base_url = 'bitbucket.org';

    my $keywords = "(site:" . $base_url .") AND \"Member since\""; 

    if ($self->token_value('keywords')){
        if ($self->token_value('and_terms')){
            $keywords .= ' AND (' . $self->token_value('and_terms') . ')';
        }
        if ($self->token_value('exact_terms')){
            $keywords .= ' AND (' . $self->token_value('exact_terms') .')';
        }
        if (my $or_terms = $self->token_value('or_terms')){
            $or_terms =~ s/^\s+OR//;
            $keywords .= ' AND (' . $or_terms .')';
        }
        if ($self->token_value('not_terms')){
            $keywords .= ' AND ' . $self->token_value('not_terms');
        }
    }

    my $location_id = $self->token_value('location_id');

    if ($location_id){
        my $location = $self->location_api()->find({
            id => $location_id,
        });
        if ($location){
            $keywords .= ' AND "' . $self->google_loc_check( $location->name ) .' * * Member since"';
        }
    }

    return $keywords;
}

sub scrape_fixup_candidate{
    my $self = shift;
    my $candidate = shift;

    my $snippet = $candidate->attr('snippet');
    if ( $snippet =~ m/^(.+?)\sis a developer/ ){
        $candidate->attr('name' => $1);
    }
    elsif ( my $headline = $candidate->attr('headline') ){
        my $name = $1 if $headline =~ m/^(\w+)/;
        $candidate->attr('name', $name);
    }

    return $self->SUPER::scrape_fixup_candidate($candidate);
}

sub cse_engine_id {
    return '003231768667224905379:hvrd5ueus3s';
}

sub google_add_cache_link { 0; }


return 1;

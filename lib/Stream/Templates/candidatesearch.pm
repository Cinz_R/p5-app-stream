package Stream::Templates::candidatesearch;

=head1 NAME

Stream::Templates::candidatesearch - Template for our/CB's candidate search service

=head1 AUTHOR

Patrick - 21/07/2015

=head1 CONTACT

Monica ( PM ) - Monica.Skidmore@careerbuilder.com
Bob ( Lead Dev ) - Bob.Fritsch@careerbuilder.com

=head1 TESTING INFORMATION

=head1 DOCUMENTATION

L<http://candidate-search-useast.cb-search.com/CandidateSearchAPI/swagger/ui/index.html#!/Search/Search_Post>

=head1 OVERVIEW

=head1 EXTRA INFORMATION

=head1 MAJOR CHANGES

none yet

=head1 BUG FIXES

none yet

=cut

use strict;

use base qw(Stream::Templates::BBCSS);

use Bean::CareerBuilder::CandidateSearch;
use Date::Parse qw//;
use Email::Address;
use Locale::Country qw//;
use Log::Any qw/$log/;
use Data::Dumper;

use Stream2;
use Stream::Token::Salary;
use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %custom_tokens);

=head2 candidatesearch_required_fields

A list of properties we would like CS to return per candidate

=cut

sub candidatesearch_required_fields {
    qw/
        first_name
        last_name
        email
        city
        postal_code
        latitude
        longitude
        country
        phone
        currently_employed
        total_years_experience
        activity_history
        address_1
        admin_area_1
        document_files
        company_experience_list
        skill_list
        education_list
        document_id

        broadbean_desired_salary_min_yearusd
        broadbean_desired_salary_max_yearusd

        broadbean_applicant_availability_time

        broadbean_desired_salary_minimum
        broadbean_desired_salary_range_top
        broadbean_desired_salary_pay_period
        broadbean_notice_period
    /;
}

%standard_tokens = (
    currently_employed => {
        Id => 'candidatesearch_currently_employed',
        Name => 'currently_employed',
        Type => 'list',
        Label => 'Currently Employed',
        SortMetric => 80,
    },
    ## See https://broadbean.atlassian.net/browse/SEAR-1059
    # degree_name => {
    #     Id => 'candidatesearch_degree_name',
    #     Name => 'degree_name',
    #     Type => 'multilist',
    #     Label => 'Degree Name',
    #     SortMetric => 200,
    # },
    country => {
        Id => 'candidatesearch_country',
        Name => 'country',
        Type => 'multilist',
        Label => 'Country',
        SortMetric => 210,
    },
    skill_list => {
        Id => 'candidatesearch_skill_list',
        Name => 'skill_list',
        Type => 'multilist',
        Label => 'Skills',
        SortMetric => 50,
    },
    ## See https://broadbean.atlassian.net/browse/SEAR-1059
    # onet_17 => {
    #     Id => 'candidatesearch_onet_17',
    #     Name => 'onet_17',
    #     Type => 'multilist',
    #     Label => 'Occupation',
    #     SortMetric => 40,
    # },
    ## See https://broadbean.atlassian.net/browse/SEAR-1059
    # naics_code => {
    #     Id => 'candidatesearch_naics_code',
    #     Name => 'naics_code',
    #     Type => 'multilist',
    #     Label => 'Industry Experience',
    #     SortMetric => 30,
    # },
);

%custom_tokens = (
    # "custom fields"
    broadbean_tags => {
        Id => 'candidatesearch_broadbean_tags',
        Name => 'broadbean_tags',
        Type => 'tag',
        Label => 'Tags',
        SortMetric => 0
    },
    broadbean_advert_industry => {
        Id => 'candidatesearch_broadbean_advert_industry',
        Name => 'broadbean_advert_industry',
        Type => 'multilist',
        Label => 'Advert Industry',
        SortMetric => 20
    },
    broadbean_advert_job_type => {
        Id => 'candidatesearch_broadbean_advert_job_type',
        Name => 'broadbean_advert_job_type',
        Type => 'multilist',
        Label => 'Advert Job Type',
        SortMetric => 60,
    },
    broadbean_applicant_availability_time => {
        Id => 'candidatesearch_broadbean_applicant_availability_time',
        Name => 'broadbean_applicant_availability_time',
        Type => 'list',
        WithTotal => 0,
        Label => 'Availability Time',
        SortMetric => 70,
    },
    broadbean_employer_org_name => {
        Id => 'candidatesearch_broadbean_employer_org_name',
        Name => 'broadbean_employer_org_name',
        Type => 'text',
        Label => 'Current Employer',
        SortMetric => 90,
    },
    broadbean_initial_source => {
        Id => 'candidatesearch_broadbean_initial_source',
        Name => 'broadbean_initial_source',
        Type => 'multilist',
        Label => 'Original Source',
        SortMetric => 160,
    },
    broadbean_final_source => {
        Id => 'candidatesearch_broadbean_final_source',
        Name => 'broadbean_final_source',
        Type => 'multilist',
        Label => 'Latest Source',
        SortMetric => 170,
    },
    broadbean_desired_salary_currency => {
        Id => 'candidatesearch_broadbean_desired_salary_currency',
        Name => 'broadbean_desired_salary_currency',
        Type => 'list',
        Label => 'Desired Salary Currency',
        SortMetric => 129,
    },
    broadbean_job_requisition_ids => {
        Id => 'candidatesearch_broadbean_job_requisition_ids',
        Name => 'broadbean_job_requisition_ids',
        Type => 'multilist',
        Label => 'Job Requisition',
        SortMetric => 180,
    },
    broadbean_employer_org_job_title => {
        Id => 'candidatesearch_broadbean_job_title',
        Name => 'broadbean_employer_org_job_title',
        Type => 'text',
        Label => 'Job Title',
        SortMetric => 100,
    },
    broadbean_start_date => {
        Id => 'candidatesearch_broadbean_start_date',
        Name => 'broadbean_start_date',
        Type => 'date',
        Label => 'Start Date',
        SortMetric => 110,
    },
    broadbean_end_date => {
        Id => 'candidatesearch_broadbean_end_date',
        Name => 'broadbean_end_date',
        Type => 'date',
        Label => 'End Date',
        SortMetric => 120,
    },
    broadbean_recruitment_status => {
        Id => 'candidatesearch_broadbean_recruitment_status',
        Name => 'broadbean_recruitment_status',
        Type => 'multilist',
        Label => 'Recruitment Status',
        SortMetric => 85
    },
    broadbean_most_recent_activity_date => {
        Id => 'candidatesearch_broadbean_most_recent_activity_date',
        Name => 'broadbean_most_recent_activity_date',
        Type => 'list',
        Label => 'Most Recent Activity',
        SortMetric => 190,
    }
);

# check Stream2::Results::Result::set_canonical_values for the full list
# of canonical names
my %canonical_attr_mappings = (
    name      => 'name',
    address   => 'address',
    city      => 'city',
    country   => 'country',
    postal_code  => 'postcode',
    telephone => 'phone',
    cv_text   => 'cv_text',
    last_activity  => 'broadbean_most_recent_activity_date',
    education => 'education_list',
    employer  => 'broadbean_employer_org_name',
    job_title => 'broadbean_employer_org_job_title',
);

sub tags_field_name{ 'broadbean_tags'; }

=head2 candidatesearch_custom_facet_fields

Returns the same as candidatesearch_custom_fields, except only
for the fields that can be faceted.

=cut

sub candidatesearch_custom_facet_fields{
    my ($self) = @_;
    my %fields = %{ $self->candidatesearch_custom_fields() };
    foreach my $nofacetfield ( qw/broadbean_start_date broadbean_end_date
                                  broadbean_job_requisition_ids broadbean_most_recent_activity_date/ ){
        delete $fields{$nofacetfield};
    }
    return \%fields;
}

=head2 candidatesearch_custom_fields

Fetches list of custom fields from BBCSS and builds a field definition for each one

=cut

sub candidatesearch_custom_fields {
    my ( $self ) = @_;
    my $customer = $self->bbcss_customer();

    unless ( $self->{_candidatesearch_custom_fields} ){
        # we have some standard custom fields defined above, use that definition if available
        $self->{_candidatesearch_custom_fields} = { map {

            defined( $custom_tokens{$_->{field_name}} ) ?
                ( $_->{field_name} => $custom_tokens{$_->{field_name}} ) :
                ();

                # this is the definition used if the facet has not been previously defined
                # for now we will just hide any facets which aren't pre-defined
#                {
#                    Id      => 'bbcss_' . $_->{field_name},
#                    Name    => $_->{field_name},
#                    Type    => $_->{is_facetable} ? 'multilist' : 'text',
#                    Label   => $_->{field_name}
#                }
        } @{$customer->{config}->{custom_fields} // []} };
    }

    return $self->{_candidatesearch_custom_fields};
}


sub search_submit {
    my ( $self ) = @_;
    # the second argument ( subroutine ) tells the agent to return the response without any post-processing ( i.e. json decoding )

    my $search_ref = $self->candidatesearch_build_search_json();

    # we only want to facet on list type fields
    $search_ref->{facet} = [
        ( map { { name => $_->{Name} } } grep { $_->{Type} =~ m/(?:tag|list)\z/i } values %standard_tokens ),
        ( map { { name => $_->{Name} } } grep { $_->{Type} =~ m/(?:tag|list)\z/i && $_->{Name} !~ /availability_time$/ } values %{$self->candidatesearch_custom_facet_fields()} ),
    ];

    # We also want the facets on availability date stuff.
    # my $the_past = DateTime->new( year => 1977 , month => 10, day => 20 );
    push @{$search_ref->{facet}}, {
        name => 'broadbean_applicant_availability_time',
        ranges => [
            map{ +{
                name => $_->[0],
                lower => { value => $_->[1]->iso8601().'.000Z' },
                upper => { value => $_->[2]->iso8601().'.000Z' }
            }
             } @{$self->_availability_time_ranges()}
         ],
    };
    my $response = $self->bbcss_client()->search( $search_ref , sub { pop; } );

    unless ( $response->is_success ){
        return $self->determine_error( $response );
    }

    return $response;
}


sub _availability_time_ranges{
    my ($self) = @_;
    my $now = DateTime->now();
    # Something long enough in the past to capture candidates
    # that have an availability date in the past as well.
    my $the_past = DateTime->new( year => 1977 , month => 10, day => 20 );
    my $today = DateTime->now()->truncate( to => 'day' );
    return [
        [ 'Past' , $the_past, $today ],
        [ "1 Day" , $today, $now->clone->add( days => 1)->truncate( to => 'day' ) ],
        [ "2 Days", $today, $now->clone->add( days => 2)->truncate( to => 'day' ) ],
        [ "3 Days", $today, $now->clone->add( days => 3)->truncate( to => 'day' ) ],
        [ "4 Days", $today, $now->clone->add( days => 4)->truncate( to => 'day' ) ],
        [ "5 Days", $today, $now->clone->add( days => 5)->truncate( to => 'day' ) ],
        [ "6 Days", $today, $now->clone->add( days => 6)->truncate( to => 'day' ) ],
        [ "1 Week", $today, $now->clone->add( days => 7)->truncate( to => 'day' ) ],
        [ "2 Weeks", $today, $now->clone->add( days => 14)->truncate( to => 'day' ) ],
        [ "3 Weeks", $today, $now->clone->add( days => 21)->truncate( to => 'day' ) ],
        [ "1 Month", $today, $now->clone->add(months => 1)->truncate( to => 'day' )],
        [ "2 Months", $today, $now->clone->add(months => 2)->truncate( to => 'day' )],
        [ "3 Months", $today, $now->clone->add(months => 3)->truncate( to => 'day' )],
        [ "4 Months", $today, $now->clone->add(months => 4)->truncate( to => 'day' )],
        [ "5 Months", $today, $now->clone->add(months => 5)->truncate( to => 'day' )],
        [ "6 Months", $today, $now->clone->add(months => 6)->truncate( to => 'day' )],
        [ "7 Months", $today, $now->clone->add(months => 7)->truncate( to => 'day' )],
        [ "8 Months", $today, $now->clone->add(months => 8)->truncate( to => 'day' )],
        [ "9 Months", $today, $now->clone->add(months => 9)->truncate( to => 'day' )],
        [ "10 Months", $today, $now->clone->add(months => 10)->truncate( to => 'day' )],
        [ "11 Months", $today, $now->clone->add(months => 11)->truncate( to => 'day' )],
        [ "1 Year",  $today, $now->clone->add( years => 1 )->truncate( to => 'month' )],
        [ "2 Years",  $today, $now->clone->add( years => 2 )->truncate( to => 'month' )],
    ];
}



=head2 adcourier_api_client

Local caching aware adcourier_api_client

=cut

sub adcourier_api_client{
    my ($self) = @_;
    unless( $self->{_adcourier_api_client} ){
        $self->{_adcourier_api_client} = $self->user_object->stream2->adcourier_api();
    }
    return $self->{_adcourier_api_client};
}

=head2 candidatesearch_build_search_json

Responsible for compiling the actual search request content

=cut

sub candidatesearch_build_search_json {
    my ( $self ) = @_;
    my $s2 = $self->user_object()->stream2();

    my %fields = (
        %{$self->bbcss_build_search_json},
        fields_to_return => [ $self->candidatesearch_required_fields(), keys(%{$self->candidatesearch_custom_fields()}) ]
    );

    # Now build an array of field filter for
    # the filter_aggregate of the query (see http://candidate-search-useast.cb-search.com/CandidateSearchAPI/swagger/ui/index.html#!/Search/Search_Post )
    my @field_filters = ();

    # Salaries
    my $salary_currency = $self->token_value('salary_cur');
    my $salary_per      = $self->token_value('salary_per');
    my $salary_from = $self->token_value('salary_from');
    my $salary_to   = $self->token_value('salary_to');
    my $USD_rate;
    if( length( $salary_from ) ){
        $USD_rate //= $s2->currency_api()->get_rate( $salary_currency , 'USD' );

        my $from_USDpa = $USD_rate * Stream::Token::Salary::convert_to_annum( $salary_from , uc($salary_per) );
        # Filtering with a minimum salary means we want the people who want to be paid at a maximum
        # exceeding this minimum.
        # We EXCLUDE people with a maximum lower than the min we want to pay.
        # We INCLUDE people with a maximum higher than the min we want to pay.
        # The default value is very big, so that will include people who didnt lower their
        # max salary
        push @field_filters,
            {
                field_name => 'broadbean_desired_salary_max_yearusd',
                operation => 'AND',
                values => [{ value => '['.int($from_USDpa).' TO *]' }]
            };
    }
    if( length( $salary_to ) ){
        $USD_rate //= $s2->currency_api()->get_rate( $salary_currency , 'USD' );

        my $to_USDpa = $USD_rate * Stream::Token::Salary::convert_to_annum( $salary_to   , uc($salary_per) );
        # Filtering with a maximum salary means including only the people who want to be paid
        # a compatible minimum.
        # We EXCLUDE people with a minimum higher than the max we offer.
        # We INCLUDE people with a minimum lower  than the max we offer.
        # The default value is zero, so we will include people who did not specify
        # any minimum salary.
        push @field_filters,
            {
                field_name => 'broadbean_desired_salary_min_yearusd',
                operation => 'AND',
                values => [{ value => '[* TO '.int($to_USDpa).']' }]
            };
    }

    my $include_unspecified_salaries = $self->token_value('include_unspecified_salaries');
    unless( $include_unspecified_salaries ){
        # Filter IN the one who HAVE specified their preferences.
        push @field_filters,
            {
                field_name => 'broadbean_desired_salary_specified',
                operation => 'AND',
                values => [{ value => 'true' }]
            };
    }

    if( my $date_range = $self->candidatesearch_cv_updated_range_string() ){
        push @field_filters , {
            field_name => 'broadbean_cv_updated',
            operation => 'AND',
            values => [{
                value => '['.$date_range.']'
            }]
        };
    }

    # Manage the logic between the top level default_jobtype
    # and the local candidatesearch_broadbean_advert_job_type
    my $jobtype = $self->token_value('default_jobtype');
    my $broadbean_advert_job_type = $self->token_value('candidatesearch_broadbean_advert_job_type');

    if( $broadbean_advert_job_type && $jobtype && ( $broadbean_advert_job_type ne $jobtype )){
        $self->results()->add_notice( $self->user_object->stream2()->__p('feed.candidatesearch', 'Your chosen Advert Type has overridden the global Job Type') );
    }
    if( $jobtype && ! $broadbean_advert_job_type ){
        $self->token_value('candidatesearch_broadbean_advert_job_type',  $jobtype );
    }

    # broadbean_applicant_availability_time filter.
    if( my $availability_time_filter = $self->get_token_value('candidatesearch_broadbean_applicant_availability_time') ){
        my %range_index = map{ $_->[0] => [ $_->[1] , $_->[2] ] } @{$self->_availability_time_ranges()};
        my $range = $range_index{$availability_time_filter} // confess("Unsupported availability_time filter value $availability_time_filter");
        push @field_filters , {
            field_name => 'broadbean_applicant_availability_time',
            operation => 'AND',
            values => [{
                value => '['.$range->[0]->iso8601().'.000Z TO '.$range->[1]->iso8601().'.000Z]'
            }]
        };
    }



    my $tokens_ref = $self->tokens();
    foreach my $field ( keys %$tokens_ref ){
        # all fields starting with candidatesearch_ which haven't been accounted for yet
        # should be 'filter' fields

        next if ( exists( $fields{$field} ) );
        next if ( index( $field, 'candidatesearch_' ) != 0 );

        # Never allow broadbean_applicant_availability_time to be an automated filter.
        next if ( $field eq 'candidatesearch_broadbean_applicant_availability_time' );

        my @values = $self->token_value( $field );

        # Only push some filters when there are some values
        if( scalar( @values ) ){
            push @field_filters , {
                field_name =>  substr( $field, length('candidatesearch_') ),
                operation => 'OR',
                values => [ map{ +{ value => $_ } } @values ],
            };
        }
    }

    if( @field_filters ){
        $fields{filter_aggregates} //= {
            operation => 'AND',
            filters => [],
        };
        push @{ $fields{filter_aggregates}->{filters} },  @field_filters;
    }

    return \%fields;
}

=head2 candidatesearch_cv_updated_range_string

Usage:

 if( my $string_range = $this->candidatesearch_cv_updated_range_string() ){
    ...
 }

=cut

sub candidatesearch_cv_updated_range_string {
    my ( $self ) = @_;
    my $cv_updated_within = $self->token_value( 'cv_updated_within' );

    if ( ! $cv_updated_within || ( $cv_updated_within eq 'ALL' ) ){
        return;
    }
    my $today = DateTime->now()->add( days => 1 );
    my $today_str = $today->iso8601.'.000Z';

    my $from_dt = {
        'TODAY'         => sub{ $today->subtract( days => 1 )->truncate( to => 'day' ) },
        'YESTERDAY'     => sub{ $today->subtract( days => 2 )->truncate( to => 'day' ) },
        '3D'            => sub{ $today->subtract( days => 3 )->truncate( to => 'day' ) },
        '1W'            => sub{ $today->subtract( weeks => 1 )->truncate( to => 'day' ) },
        '2W'            => sub{ $today->subtract( weeks => 2 )->truncate( to => 'day' ) },
        '1M'            => sub{ $today->subtract( months => 1 )->truncate( to => 'day' ) },
        '2M'            => sub{ $today->subtract( months => 2 )->truncate( to => 'day' ) },
        '3M'            => sub{ $today->subtract( months => 3 )->truncate( to => 'day' ) },
        '6M'            => sub{ $today->subtract( months => 6 )->truncate( to => 'day' ) },
        '1Y'            => sub{ $today->subtract( years => 1 )->truncate( to => 'month' ) },
        '2Y'            => sub{ $today->subtract( years => 2 )->truncate( to => 'month' ) },
        '3Y'            => sub{ $today->subtract( years => 3 )->truncate( to => 'month' ) },
    };
    return &{$from_dt->{$cv_updated_within}}()->iso8601().'.000Z'.' TO '.$today_str;
}

=head2 scrape_facets

Returned in the results are facets and also a summary of what we originally
searched for which is handy to prepopulate the SelectedValues field

=cut

sub scrape_facets {
    my ( $self ) = @_;
    my $result_ref = $self->response_to_JSON();

    my $facets_ref = { map { $_->{name} => $_ } ( @{$result_ref->{data}->{facets}},
                                                  @{$result_ref->{data}->{queryFacets}} )
                   };
    my @facets = ();

    # build available options
    my $facet_schema_ref = { %standard_tokens, %{$self->candidatesearch_custom_facet_fields()} };

    while ( my ( $name, $attr_ref ) = each %{$facet_schema_ref} ){

        if ( $name eq 'onet_17' ){
            my %onet_map = $self->bbcss_onet_lookup( map { $_->{name} } @{$facets_ref->{$name}->{values}} );
            $attr_ref->{Options} = [
                map {
                    $onet_map{$_->{name}} ?
                        [ $onet_map{$_->{name}}, $_->{name}, $_->{count} ] :
                        ()
                } @{$facets_ref->{$name}->{values}}
            ];
        }
        elsif ( $name eq 'naics_code' ){
            my %naics_map = $self->bbcss_naics_lookup( map { $_->{name} } @{$facets_ref->{$name}->{values}} );
            $attr_ref->{Options} = [
                map {
                    $naics_map{$_->{name}} ?
                        [ $naics_map{$_->{name}}, $_->{name}, $_->{count} ] :
                        ()
                } @{$facets_ref->{$name}->{values}}
            ];
        }
        elsif ( $name eq 'country' ){
            $attr_ref->{Options} = [
                map {
                    [ Locale::Country::code2country($_->{name}), $_->{name}, $_->{count} ]
                } @{$facets_ref->{$name}->{values}}
            ];
        }
        elsif ( $name =~ m/\Abroadbean_(?:initial|final)_source\z/ ){
            $attr_ref->{Options} = [
                defined( $facets_ref->{$name} ) ?
                    ( map {
                        [ $self->bbcss_board_nice_name($_->{name}), $_->{name}, $_->{count} ]
                    } ( @{$facets_ref->{$name}->{values}} ) )
                : ()
            ];
        }
        elsif( $name eq 'broadbean_advert_industry' ){
            $attr_ref->{Options} = [
                defined( $facets_ref->{$name} ) ?
                    ( map {
                        [ ucfirst($_->{name}), $_->{name}, $_->{count} ]
                    } ( @{$facets_ref->{$name}->{values}} ) )
                : ()
            ];
        }elsif( $name eq 'broadbean_applicant_availability_time' ){
            $attr_ref->{Options} = [
                defined( $facets_ref->{$name} ) ?
                    ( grep { $_->[2] } map {
                        [ $_->{name}, $_->{name}, $_->{count} ]
                    } ( @{$facets_ref->{$name}->{values}} ) )
                : ()
            ];
        }
        else {
            $attr_ref->{Options} = [
                defined( $facets_ref->{$name} ) ?
                    ( map {
                        [ $_->{name}, $_->{name}, $_->{count} ]
                    } ( @{$facets_ref->{$name}->{values}} ) )
                : ()
            ];
        }

        # Transfer values for all the facets.
        if ($attr_ref->{'Type'} eq 'multilist' || $attr_ref->{'Type'} eq 'tag' || $attr_ref->{'Type'} eq 'groupedmultilist') {
            # Transfer multiple values
            my @values = $self->get_token_value( $name );
            $attr_ref->{'SelectedValues'} = { map { $_ => 1 } @values };
        }else{
            # Transfer single values
            $attr_ref->{Value} = $self->get_token_value( $name );
        }

        if ( $attr_ref->{Type} eq 'list' && scalar( @{$attr_ref->{Options}} ) ){
            if( $attr_ref->{WithTotal} // 1 ){
                my $total = 0;
                $total += $_->[-1] for ( @{$attr_ref->{Options}} );
                unshift( $attr_ref->{Options}, [ 'Any', '', $total ] );
            } else {
                unshift( $attr_ref->{Options}, [ 'Any', '' ] );
            }
        }
    }

    # append selected values according to candidatesearch
#    $facet_schema_ref->{$_->{name}}->{SelectedValues}->{$_->{value}} = 1 for @{$result_ref->{forensics}->{input}->{filters}};

    $self->results()->facets( [ values %$facet_schema_ref ] );
}

sub scrape_candidate_details {
    my ( $self, $result_ref ) = @_;

    return (
        $self->SUPER::scrape_candidate_details( $result_ref ),

        broadbean_desired_salary_minimum => $result_ref->{broadbean_desired_salary_minimum},
        broadbean_desired_salary_range_top => $result_ref->{broadbean_desired_salary_range_top},
        broadbean_desired_salary_pay_period => $result_ref->{broadbean_desired_salary_pay_period},
        broadbean_notice_period     => $result_ref->{broadbean_notice_period},
        broadbean_applicant_availability_time => $result_ref->{broadbean_applicant_availability_time},

        ( map { $_ => $result_ref->{$_} } keys ( %custom_tokens ) )
    );
}

sub scrape_fixup_candidate {
    my ( $self, $candidate ) = @_;

    $self->SUPER::scrape_fixup_candidate( $candidate );

    if ( my $start_date = $candidate->attr('broadbean_start_date') ){
        $candidate->attr(broadbean_start_date_epoch => Date::Parse::str2time( $start_date ));
    }
    if ( my $end_date = $candidate->{broadbean_end_date} ){
        $candidate->attr(broadbean_end_date_epoch => Date::Parse::str2time( $end_date ));
    }

    if ( my $initial = $candidate->attr('broadbean_initial_source') ){
        $candidate->attr( broadbean_initial_source => $self->bbcss_board_nice_name( $initial ) );
    }
    if ( my $final = $candidate->attr('broadbean_final_source') ){
        $candidate->attr( broadbean_final_source => $self->bbcss_board_nice_name( $final ) );
    }

    {
        # ArrayIFY the tags property
        my $tag_str = $candidate->attr('tags') // '';
        my @tags = grep { $_ } split( /\|/, $tag_str );
        $candidate->attr( tags => \@tags );
    }

    # set the canonical values as the final step.
    $candidate->set_canonical_values(%canonical_attr_mappings);

}


sub bbcss_customer {
    my ( $self ) = @_;

    unless ( $self->{customer} ){
        # Find the customer_key from the adcapi
        my $customer_key = $self->adcourier_api_client()->candidatesearch_customer_key( $self->company() );
        unless( $customer_key ){
            $self->throw_login_error("Customer ".$self->company()." does not have a Candidate Search customer_key");
        }
        $self->{customer} = $self->bbcss_client()->get_customer({ customer_key => $customer_key })
            or $self->throw_login_error("Customer ".$self->company()." with customer_key='$customer_key' does not yet exist");
    }

    return $self->{customer};
}


1;

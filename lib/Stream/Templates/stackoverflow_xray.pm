#!/usr/bin/perl

package Stream::Templates::stackoverflow_xray;
use strict;
use utf8;

use base qw(Stream::Templates::google_cse);

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

$derived_tokens{and_terms} = {
    Type => 'Text',
    Helper => 'google_split_keywords(%keywords%|and_terms|or_terms|not_terms|exact_terms)',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{or_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{not_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{exact_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

sub build_google_search_query {
    my $self = shift;

    my $base_url = 'stackoverflow.com/users/';

    my $keywords = "site:" . $base_url ." "; 

    if ($self->token_value('keywords')){
        if ($self->token_value('and_terms')){
            $keywords .= ' AND (' . $self->token_value('and_terms') . ')';
        }
        if ($self->token_value('exact_terms')){
            $keywords .= ' AND (' . $self->token_value('exact_terms') .')';
        }
        if (my $or_terms = $self->token_value('or_terms')){
            $or_terms =~ s/^\s+OR//;
            $keywords .= ' AND (' . $or_terms .')';
        }
        if ($self->token_value('not_terms')){
            $keywords .= ' AND ' . $self->token_value('not_terms');
        }
    }

    my $location_id = $self->token_value('location_id');

    if ($location_id){

        my $location = $self->location_api()->find({
            id => $location_id,
        });

        if ($location){

            my $location_name = ( $location->is_country && $location->in_uk ) ? "United Kingdom" : $self->google_loc_check( $location->name );
            
            $keywords .= ' AND ("' . $location_name;
            
            if( $location->is_country && !$location->in_uk ){
                my %countries_translation = ( 
                            Netherlands =>  'Nederland',
                            Belgium     =>  'België',
                        );
                    if( my $country_translation =  $countries_translation{ $location->name } ){
                            $keywords .= '" OR "' . $country_translation;
                        }
                }
            if(!$location->is_country){
                if ($location->in_usa){
                    if ($location->is_county){
                        #Meh. hope for the best
                    } else {
                        my $state = $location->get_mapping({ board => 'ISO_STATES' });
                        $keywords .= ', ' . $state;
                    }
                } else {
                    my $country = ( $location->in_uk ) ? 'United Kingdom' : $location->country->name;
                    $keywords .= ', ' . $country;
                    if( !$location->in_uk ){
                        my %lang_codes = ( Netherlands =>  'NL', Belgium  =>  'NL', France      =>  'FR' );

                        if( my $lang_code = $lang_codes{ $country  } ){
                                
                                # TODO: implement languages/translations!
                                my $location2 = $self->location_api()->find({
                                    id => $location_id,
                                    # lang => $lang_code,
                                });

                                if( $location2 ){
                                    my $location2_name = $location2->name;
                                    my $location2_country = $location2->country->name;

                                    if( $location2_name ne $location->name || $location2_country ne $country ){
                                            $keywords .= '" OR "' . $location2_name . ', ' . $location2_country;
                                        }
                                    
                                    }
                            }
                      } 
                }
            }
            $keywords .= '")';

        }
    }

    return $keywords;
}


sub scrape_fixup_candidate{
    my $self = shift;
    my $candidate = shift;

    my $name = $candidate->attr('name');
    $name =~ s/^\w+\s//;

    my $headline = $candidate->attr('headline');
    $headline =~ s/^\w+\s//;

    $candidate->attr('name' => $name);
    $candidate->attr('headline' => $headline);
    return $self->SUPER::scrape_fixup_candidate($candidate);    
}

sub google_add_cache_link { 0; }

sub cse_engine_id {
    return '003231768667224905379:yvhgr-13eew';
}

return 1;

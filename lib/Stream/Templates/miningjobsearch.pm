package Stream::Templates::miningjobsearch;
use base qw(Stream::Templates::OilAndGasPlatform);

use strict;
use warnings;

__PACKAGE__->inherit_tokens();

sub base_url {
    return 'https://www.miningjobsearch.com';
}

1;

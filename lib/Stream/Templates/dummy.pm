package Stream::Templates::dummy;
use strict;
use base qw(Stream::Engine);

use HTTP::Response;
use JSON;
use Stream2::O::File;
use Lingua::EN::Numbers;

use vars qw{%standard_tokens};


$standard_tokens{title_only} = {
    Type        => 'List',
    Label       => 'Search only in job titles',
    Values      => 'No=false|Yes=true',
    SortMetric  => 1,
};

$standard_tokens{activity} = {
    Type        => 'List',
    Label       => 'Activity type',
    Values      => 'Signed in=1|Registered=2',
    SortMetric  => 2,
};

$standard_tokens{activity_time} = {
    Type        => 'List',
    Label       => 'Activity time frame',
    Values      => '1 day=1|2 days=2|1 week=7|2 weeks=14|1 month=30|3 month=90|6 month=180|1 year=365',
    Default     => '30',
    SortMetric  => 3,
};

$standard_tokens{place} = {
    Type        => 'MultiList',
    Label       => 'Some place',
    Values      => 'FOO=BBTECH_RESERVED_OPTGROUP|Carlow=2|Dublin South=98|BAR=BBTECH_RESERVED_OPTGROUP|Clare=41|Cork=42|BAZ=BBTECH_RESERVED_OPTGROUP|Paris=1|Lille=2',
    SortMetric  => 5,
};


=head1 NAME

Stream::Templates::dummy - Dummy feed for development purposes.

=cut

=head2 search_begin_search

Minimal dummy implementation of search_begin_search (See L<Stream::Engine>)

=cut

sub search_begin_search{
    my ($self) = @_;
    $self->total_results(1000);

    my $i = 1;
    for my $candidate_id ( map { 'dummy_'.$_ } ( 1..10 ) ){
        my %attributes = (
            destination => 'dummy',
            candidate_id => $candidate_id,
            name => 'Henry the '.ucfirst(Lingua::EN::Numbers::num2en_ordinal($i++)),
            email => ( $i == 9 ? undef : 'john_'.$candidate_id.'@example.com' ),
            location_text => 'Bratislava',
            snippet => 'Pie eater, cat lover, beer drinker, cake muncher',
            experience => '20 years Beer drinking',
            salary => 'Lot of money',
            date_published => 'Right now',
            has_cv => 1,
            has_profile => 1
        );
        my $candidate = $self->new_candidate();
        $candidate->attributes(
            %attributes,
        );
        $self->add_candidate( $candidate );
    }
}

=head2 download_cv

Returns an L<HTTP::Response> containing the CV from the given candidate
(in the case of Stream2, this is a L<Stream2::Results::Result>).

=cut

sub download_cv{
    my ($self, $candidate) = @_;

    if ( $candidate->candidate_id eq 'dummy_1' ){
        die "Can't Download CV (This is the first dummy candidate";
    }


    # Just echo the candidate as JSON text..
    my $response = HTTP::Response->new(200);
    $response->message("All is cool");
    $response->header('Content-Type' => 'text/html; charset=UTF-8');
    $response->header('Content-Disposition' => 'attachment; filename=CV_dummy_'.$candidate->candidate_id().'.html');
    $response->header('X-Candidate-Email' => 'bloupbloup@example.com' );

    ## Build a nice piece of text
    ## burning glass can understand.
    my $html = '<html><body>';
    my $ref = $candidate->to_ref();
    foreach my $key ( sort keys %$ref ){
        $html .= '<p>'.$key.':'.$ref->{$key}."</p>\n";
    }
    $html .= '</body></html>';

    $response->content(Encode::encode_utf8($html));
    return $response;
}

sub download_profile {
    my ( $self, $candidate ) = @_;

    if ( $candidate->candidate_id eq 'dummy_1' ){
        die "Can't get profile";
    }

    $candidate->attributes(
        ipsum => q{<p>Your bones don't break, mine do. That's clear. Your cells react to bacteria and viruses differently than mine. You don't get sick, I do. That's also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.</p>

        <p>You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man.</p>

        <p>The path of the righteous man is beset on all sides by the iniquities of the selfish and the tyranny of evil men. Blessed is he who, in the name of charity and good will, shepherds the weak through the valley of darkness, for he is truly his brother's keeper and the finder of lost children. And I will strike down upon thee with great vengeance and furious anger those who would attempt to poison and destroy My brothers. And you will know My name is the Lord when I lay My vengeance upon thee.</p>}
    );

    return $candidate;
}

sub update_candidate {
    my ( $self, $candidate, $fields ) = @_;
    my $updated = $fields;
    return $updated;
}

sub _candidate_attachment {
    my ($self, $key) = @_;
    return Stream2::O::File->new(
        {
            mime_type      => 'text/plain',
            binary_content => 'BLABLABLA',
            name           => $key.'_someubercoolfile.txt'
        }
    );
}
sub rendered_attachments {
    my ($self, $candidate) = @_;
    # generate some file to parse
    my @rendered = map {
        my $count = $_;
        my $file = $self->_candidate_attachment($count);

        {
            key           => $count,
            filename      => $file->name,
            rendered_html => $file->html_render,
        };

    } ( 0 .. 2 );
    return \@rendered;
}

sub download_attachment {
    my ($self, $candidate, $key) = @_;
    my $file = $self->_candidate_attachment($key)->to_download_response;
}

sub download_attachments {
    my ($self, $candidate) = @_;
    my @attachments = map {
        $self->_candidate_attachment($_)->to_download_response;
    } (0..2);
    return \@attachments;
}
sub result_enhancement_details {
    my ($self, $candidate) = @_;
    return {
        location => 'North Carolina',
        favourite_team => 'Panthers',
    };
}

sub tag_candidate {
    my ($self, $candidate, $tag) = @_;

    $candidate->add_tag($tag->tag_name);
    return $tag;
}

sub untag_candidate {
    my ($self, $candidate, $tag) = @_;

    if ( $tag ) {
        $tag->delete();
        $candidate->remove_tag($tag->tag_name);
    }
    return $tag;
}

1;

__END__

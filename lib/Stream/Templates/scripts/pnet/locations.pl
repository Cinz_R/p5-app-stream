#!/usr/bin/perl

use Stream2;
use Data::Dumper;
use XML::LibXML;

my $config_file = 't/app-stream2.conf';

my $stream2 = Stream2->new({ config_file => $config_file });
$stream2->stream_schema->deploy();

my $user_factory = $stream2->factory('SearchUser');
my $user = $user_factory->new_result({
    provider => 'adcourier',
    provider_id => 157656, # simonr@test.simonr.simonr
    group_identity => 'acme',
});
$user->save();

my $template = $stream2->build_template('pnet', undef , { location_api => $stream2->location_api(), user_object => $user } );

$template->token_value(pnet_username => 'chantelle1');
$template->token_value(pnet_password => 'zebra1-1');

eval { $template->login(); };

die "aww, cant even login: $@" if $@;

my @alphabets = (a..b);
my $MAPPINGS = {};

foreach my $alphabet ( @alphabets ) {
    my $response = eval { &request_locations($alphabet); };
    if ( $@ ) {
        warn "Died while requesting locations, printing what we have so far to file";
        &write_to_file();
    }
    &store_external_mappings($response);
    warn "sleeping for 10";
    sleep 10;
}

sub request_locations {
    my $alphabet = shift;
    return $template->post('https://www.pnet.co.za/5/index.cfm?event=recruiterSpace.cvsearchv2.CvAutosuggest.getAutosuggest', {
        keyword => $alphabet,
        offset => "0",
        limit => 100,
        criteria => 'country',
    });
}

sub store_external_mappings {
    my $response = shift;

    if ( $response->is_success() ) {
        my $dom = XML::LibXML->load_xml( string => '<xml>'.$response->decoded_content().'</xml>' ); # no envelope? bish please...
        my $xml = XML::LibXML::XPathContext->new($dom);
        my @locations = $xml->findnodes('//xml/li');
        foreach my $location ( @locations ) {
            $MAPPINGS->{$location->findvalue('.//span[@class="suggestionName"]/text()')} = $location->findvalue('.//@data-id');
        }
    }
    else {
        die "DAMMIT: " . $response->message();
    }
}

warn "We have these mappings: " . Dumper($MAPPINGS);

sub write_to_file {
    open(my $fh, '>', 'locations.txt') or die "BOO $@";
    while ( my ($key, $val) = each %{$MAPPINGS} ) {
        print $fh "$key,$value\n";
    }
    close $fh;
}

eval { $template->logout(); };

warn "We seem to be done, check locations.txt"

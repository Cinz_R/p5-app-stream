#!/usr/bin/env perl

use LWP::UserAgent;
use JSON;

my $ua = LWP::UserAgent->new;

my $response = $ua->get('https://recruiter.caterer.com/CandidateSearchApi/Dictionary/DesiredLocations');

my $json = JSON::from_json($response->decoded_content);

my @locs = ();

foreach my $loc ( @$json ) {
	push @locs, $loc->{Name} . '=BBTECH_RESERVED_OPTGROUP';
	my @this_opts = ();
	push @this_opts, '* '.$loc->{Name} . '=' . $loc->{Name};
	foreach ( @{$loc->{ChildLocations}} ) {
		push @this_opts, $_ . '=' . $_;
	}
	push @locs, @this_opts;
}

use Data::Dumper;
print Dumper(join("|", @locs));

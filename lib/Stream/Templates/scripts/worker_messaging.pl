#!/usr/bin/env perl 

use Gearman::Worker;
use Storable qw( thaw );
use List::Util qw( sum );
use Module::Load;
use URI::Split;
use Data::Dumper;
use CGI;
use strict;
use warnings;
use lib '/data0/www/adcourier.broadbean/site/Stream/Templates/scripts'; 
#use mailto;

my $worker = Gearman::Worker->new;
$worker->job_servers('127.0.0.1');
$worker->register_function(work => \&work ); 
$worker->register_function(message => \&message ); 
$worker->work while 1;


sub work {
my $args = thaw($_[0]->arg);

my $module = ${$args}{module};
my $message = ${$args}{message};
my $recipient = ${$args}{recipient};
my $sender = ${$args}{sender};
my $options = ${$args}{options};

return eval("$module"."::work(\$sender,\$recipient,\$message,\$options)");

}

sub message {

    my $args = CGI->new($_[0]->arg);
   # die Dumper $_[0]->arg;

    my ($module,$recipient) = split(":",$args->param('to')); 
    logit($module);
    load $module;
    #die $args;
  return eval ("$module"."::message(\$_[0]->arg)");
    
}

sub logit
{
    my $s = shift;
    my ($logsec,$logmin,$loghour,$logmday,$logmon,$logyear,$logwday,$logyday,$logisdst)=localtime(time);
    my $logtimestamp = sprintf("%4d-%02d-%02d %02d:%02d:%02d",$logyear+1900,$logmon+1,$logmday,$loghour,$logmin,$logsec);
    $logmon++;
    my $logfile="logfile.log";
    my $fh;
    open($fh, '>>', "$logfile") or die "$logfile: $!";
    print $fh "$logtimestamp $s\n";
    close($fh);
}


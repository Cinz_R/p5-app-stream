package Stream::Templates::sologig;

use strict;

use base qw(Stream::Templates::careerbuilder);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

BEGIN {
    __PACKAGE__->inherit_tokens();

    $authtokens{sologig_email} = {
        Label => 'Email Address',
        Type  => 'Text',
        Size  => 25,
        SortMetric => 1,
    };

    $authtokens{sologig_password} = {
        Label => 'Password',
        Type  => 'Text',
        Size  => 25,
        SortMetric => 2,
    };
};

sub careerbuilder_niche_sites { return qw(SG); }

1;

package Stream::Templates::hsjjobs;
use base qw(Stream::Templates::CrawledMadgex);

use strict;
use warnings;


__PACKAGE__->load_tokens_from_config();

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

use Stream::Constants::SortMetrics qw(:all);

$authtokens{hsjjobs_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{hsjjobs_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};


sub fixup_tokens {
    my $self = shift;
    my %tokens = @_;

    # Remove job type - we're mapping it instead
    delete $tokens{2576004}
      or die('Could not find "Preferred Job Type');
    #Remove current salary as a token - We're mapping it instead.
    delete $tokens{2576001}
      or die('Could not find "Current salary"');

    return %tokens;
}

sub salary_mappings {
    return {
        GBP => {
            ranges => { 
               # id => range
                '2576001|1531099' => [0       =>  10_000],
                '2576001|1531100' => [10_000  =>  20_000],
                '2576001|1531101' => [20_000  =>  30_000],
                '2576001|1531102' => [30_000  =>  40_000],
                '2576001|1531103' => [40_000  =>  50_000],
                '2576001|1531104' => [50_000  =>  60_000],
                '2576001|1531105' => [60_000  =>  70_000],
                '2576001|1531106' => [70_000  =>  80_000],
                '2576001|1531107' => [80_000  =>  90_000],
                '2576001|1539145' => [90_000  => 100_000],
                '2576001|1539146' => [100_000 =>   "inf"],
            }
        }
    };
}


sub fixup_search_fields {
    my $self = shift;
    my %fields = @_;
    my $query_fields = $fields{q};

    my $jobtype = $self->token_value('default_jobtype');
    if($jobtype eq 'permanent') {
        push @$query_fields, '2576004|2046148';
    } elsif($jobtype) {
        # contract/temporary
        push @$query_fields, '2576004|2046149';
    }

    my @salary_ranges = $self->_salary_ids;
    push @$query_fields, @salary_ranges;
    return %fields;
}


1;

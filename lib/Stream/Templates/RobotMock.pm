package Stream::Templates::RobotMock;

use strict;

use Bean::Log::DevNull;

use base qw(Stream::Engine::Robot);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
################ TOKENS #########################
BEGIN {
    $standard_tokens{location} = {
        Label   => 'Location',
        Type    => 'Text',
        Default => 'Canary Wharf',
        Size    => '60',
    };

    $standard_tokens{mandatory_token} = {
        Label     => 'Mandatory Token',
        Type      => 'Text',
        Default   => '',
        Mandatory => 1,
        Size      => 60,
    };

    $derived_tokens{salary_per} = {
        Type    => 'SalaryPer',
        Options => 'annum|hour|week',
    };

    $derived_tokens{salary_from} = {
        Type    => 'SalaryValue',
        Helper  => 'SALARY_CHANGE_FREQUENCY(salary_from|annum|hour|1)',
        HelperMetric => 1,
    };

    $posting_only_tokens{location_copy} = {
        Helper  => 'COPY(%location%)',
    };

    $posting_only_tokens{test_1} = {
        Helper  => 'add(1|1)',
        HelperMetric => 1,
    };

    $posting_only_tokens{test_2} = {
        Helper => 'add(%test_1%|1)',
        HelperMetric => 2,
    };

    $posting_only_tokens{test_3} = {
        Helper => 'add(%test_1%|%test_2%)',
        HelperMetric => 3,
    };

    $posting_only_tokens{test_4} = {
        Helper => 'add(%test_1%%test_2%)',
        HelperMetric => 4,
    };

    $posting_only_tokens{array} = {
        Helper => 'build_array(3)',
        HelperMetric => 1,
    };

    $posting_only_tokens{array_test_1} = {
        Helper => 'MULTILIST_COMMA(%array%)',
        HelperMetric => 255,
    };

    $authtokens{token_1} = {
        Label => 'Authtoken 1',
        SortMetric => 1,
        Type  => 'Text',
    };
};


############### HELPERS ##########################
sub build_array {
    my $self = shift;
    my $array_size = shift;

    my (@array) = ('XXX') x $array_size;

    return (@array);
}

sub add {
    my $self = shift;

    my $total = 0;
    foreach ( @_ ) {
        $total += $_;
    }

    return $total;
}

1;

# vim: expandtab shiftwidth=4

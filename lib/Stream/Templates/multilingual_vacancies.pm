package Stream::Templates::multilingual_vacancies;
use base qw(Stream::Templates::CrawledMadgex);

use strict;
use warnings;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

__PACKAGE__->load_tokens_from_config();

use Stream::Constants::SortMetrics qw(:all);

=test auth tokens

https://recruiters.multilingualvacancies.com/
Username => 'AlexR@broadbean.com',
Password => 'alex2016'

=cut

$authtokens{multilingual_vacancies_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{multilingual_vacancies_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

$derived_tokens{salary_cur}->{Options} = 'GBP';
$derived_tokens{salary_per}->{Options} = 'annum';

1;

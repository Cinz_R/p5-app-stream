package Stream::Templates::constructionjobsearch;
use base qw(Stream::Templates::OilAndGasPlatform);

use strict;
use warnings;

__PACKAGE__->inherit_tokens();

sub base_url {
    return 'https://www.construction-jobsearch.com/';
}

1;

package Stream::Templates::dice;

use base qw{Stream::Engine::API::XML};

use strict;
use warnings;

use MIME::Base64 qw();
use Stream::Constants::SortMetrics qw(:all);
use URI;
use JSON qw();
use DateTime::Format::ISO8601;
use utf8; # For '£' symbols
use vars qw{%authtokens %standard_tokens %derived_tokens %posting_only_tokens};

$authtokens{dice_client_id}= {
    Label      => 'Client ID',
    Type       => 'Text',
    Size       => 10,
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{dice_email}= {
    Label      => 'Username',
    Type       => 'Text',
    Size       => 10,
    Mandatory  => 1,
    SortMetric => 1,
};
$authtokens{dice_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Size       => 10,
    Mandatory  => 1,
    SortMetric => 2,
};
$authtokens{dice_client_secret} = {
    Label      => 'Client Secret',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 3,
};

$derived_tokens{'salary_per'} = {
    Type         => 'SalaryPer',
    Options      => 'annum|hour',
    Default      => 'annum',
    Helper       => 'SALARY_PER',
    HelperMetric => 1,
};

$derived_tokens{'salary_cur'} = {
    Type         => 'Currency',
    Options      => 'USD',
    Default      => 'USD',
    Helper       => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
    HelperMetric => 1,
    SortMetric   => $SM_SALARY,
};

=head2 dice_oauth

Returns an Auhtorization header.
Uses a cached access token or acquires a new one.

=cut

sub dice_oauth {
    my $self = shift;

    my %credentials = (
        username      => $self->token_value('dice_email'),
        password      => $self->token_value('dice_password'),
        client_id     => $self->token_value('dice_client_id'),
        client_secret => $self->token_value('dice_client_secret'),
    );

    # Cache access token against subscription values.
    my $cache_key = 'dice_authentication' . join('-', values %credentials);
    my $cached_auth_header = $self->get_account_value($cache_key);
    if($cached_auth_header) {
        $self->log_info('Using cached access token');
        return $cached_auth_header;
    }

    $self->log_info('Acquiring new access_token');

    my $auth_string = MIME::Base64::encode_base64(
        $self->token_value('dice_client_id') . ":" . $self->token_value('dice_client_secret')
    );

    my $response = $self->post(
        'https://secure.dice.com/oauth/token',
        {
            grant_type => 'password',
            username => $credentials{username},
            password => $credentials{password},
        },
        Authorization => "Basic " . $auth_string,
    );
    my $json = JSON::decode_json($response->content());

    my $auth_header = $json->{'token_type'} . " " . $json->{'access_token'};
    $self->log_debug("Access Token: " . $auth_header);

    if( $json->{'token_type'} ne 'bearer'){
        die('Could not retrieve bearer access token');
    }

    $self->set_account_value(
        $cache_key,
        $auth_header,
        ($json->{expires_in} - 120) # Set a 2 minutes margin
    );

    return $auth_header;
}

sub base_url{
    return URI->new('https://api.dice.com/integratedProfiles');
}

sub search_url{
    my $self = shift;
    my $url = $self->base_url;
    $url->path_segments(
        $url->path_segments,
        'search'
    );
    return $url;
}

sub search_fields{
    my ($self) = (@_);
    my %fields;

    $fields{q} = $self->token_value('keywords');

    $fields{yearsExp} = $self->map_experience(
        $self->token_value('min_experience'),
        $self->token_value('max_experience'),
    );

    my $salary_per = $self->token_value('salary_per');
    my $salary = $self->map_salary(
        $self->token_value('salary_from'),
        $self->token_value('salary_to'),
    );
    if($salary_per eq 'annum') {
        $fields{salary} = $salary if $salary;
    } elsif($salary_per eq 'hour') {
        $fields{hourly} = $salary if $salary;
    }

    $fields{page} = $self->current_page;
    $fields{count} = $self->results_per_page;

    #location
    my $location_id = $self->token_value('location_id');
    if ($location_id) {
        my $location = $self->location_api()->find($location_id);
        if (! $location ) {
            $self->log_warn('Unable to fetch location [%s] from |DB', $location_id);
        }
        $fields{location} = $location->zipcode();
        $fields{distance} = $self->token_value('location_within_miles');
        $fields{country} = $location->iso_country;
    }

    $fields{daysAgo} = $self->CV_UPDATED_WITHIN_TO_DAYS(
        $self->token_value('cv_updated_within')
    );

    $fields{desiredPosition} = $self->token_value('desired_position')
        if $self->token_value('desired_position');
    $fields{employerName} = $self->token_value('employer_name')
        if $self->token_value('employer_name');

    my %facets = %{$self->dynamic_facets};
    while( my ($board_key, $token_key) = each %facets) {
        $self->log_debug('Getting values from dynamic facet ' . $token_key);
        my @values = $self->token_value_or_default($token_key);
        next unless @values;

        $fields{ $board_key } = join('::', @values);
    }

    $fields{searchType} = $self->token_value_or_default('search_type') || '0';

    $fields{webProfiles} = join('::', $self->token_value('web_profiles'));
    $fields{likelyToSwitch} = $self->token_value_or_default(
        'likely_to_switch'
    );

    $fields{facet} = join(',', keys %{$self->dynamic_facets});

    foreach my $key (keys %fields) {
        delete $fields{$key} unless defined $fields{$key};
    }
    return %fields;
}

sub search_submit{
    my ($self) = @_;

    my $access_token = $self->dice_oauth();

    my $url = $self->search_url;
    my %fields = $self->search_fields;

    if(!$fields{country}) {
        $self->throw_unavailable('You must select a location');
    }

    if($fields{searchType} == 2 && !$fields{jobTitle} && !$fields{q} && !$fields{skill}) {
        $self->throw_unavailable('Open Web Searches require one of: Job Title, Skills, Keywords');
    }
    $url->query_form(%fields);

    my $response = $self->get( $url,
        'Authorization' => $access_token,
    );

    my @facets = $self->_build_facets($response);

    $self->results->facets(\@facets);
    return $response;
}

# Build facets with dynamic facet counts
sub _build_facets {
    my ($self, $response) = @_;
    my $response_data = JSON::decode_json($response->decoded_content);

    return grep {$_} map {
        $self->_build_facet($_);
    } @{ $response_data->{facets} };
}

# Builds a facet to update a token.
# Existing (determined by value) list options are updated with a count
# New list options are added using the value as the label
sub _build_facet {
    my ($self, $board_facet) = @_;


    my $board_key = $board_facet->{primaryAlias};
    my $token_key = $self->dynamic_facets->{$board_key};

    # Only applies to existing tokens
    my $token = $standard_tokens{ $token_key };
    if(!$token) {
        $self->log_debug('Token ' . $token_key . 'not found');
        return;
    }

    my @options = @{$token->{Options}};
    foreach my $board_option (@{$board_facet->{values}}) {

        # Find corresponding token option by value
        my ($token_option) = grep {
            $_->[1] eq $board_option->{value};
        } @options;

        if ($token_option) {
            # Update corresponding token option
            # Facet counts from dice are not accurate
            # Omit.
            #$token_option->[2] = $board_option->{count};
        } else {
            # No corresponding token option
            # Create a new one.
            push @options, [
                $board_option->{value},
                $board_option->{value},
                # See above
                #$board_option->{count},
            ]
        }
    }

    my $facet = {
        Label   => $token->{Label},
        Id      => $token_key,
        Name    => $token_key,
        Type    => $token->{Type},
        SortMetric => $token->{SortMetric},
        Options => [@options],
    };
    return $facet;
}

sub search_success{
    return qr/"total"/;
}

sub results_per_page{
    return 10;
}

sub scrape_number_of_results{
    my ($self) = @_;

    my $json = JSON::decode_json($self->content());
    my $results = $json->{'total'};
    $self->total_results($results);
    $self->log_debug("Total number of results: " .  $self->total_results());
}

sub scrape_paginator {
    my ($self) = @_;

    my $json = JSON::decode_json($self->content());
    my $no_of_results = $json->{'total'};
    my $page_size = $json->{'count'};
    $self->log_debug("Page size: " . $page_size);

    my $no_of_pages = $json->{'pageCount'};
    $self->log_debug("
        No of pages: " . $no_of_pages);
    $self->total_pages($no_of_pages);
}

sub scrape_generic_search_results{
    my ($self) = @_;

    my $json = JSON::decode_json($self->content());
    foreach my $candidate_details (@{$json->{documents}}){

        my $candidate = $self->new_candidate();
        my $cleaned_details = $self->_remove_empty_fields($candidate_details);
        $self->scrape_integrated_profile($candidate, $cleaned_details);

        my @id_parts = grep { $_ } (
            $cleaned_details->{openWebId},
            $cleaned_details->{id}
        );
        $candidate->candidate_id( join('-', @id_parts) );
        $self->add_candidate($candidate);
    }
}


# Recursively removes dead ends
# from Dice's JSON structure
sub _remove_empty_fields {
    my ($self, $value) = @_;

    if(ref $value eq 'HASH') {
        my %hash = %$value;
        my @valid_keys;
        foreach my $key (keys %hash) {
            my $child = $self->_remove_empty_fields( $hash{$key} );
            $hash{$key} = $child or delete $hash{$key};
        }
        return keys %hash ? \%hash : undef;
    } elsif(ref $value eq 'ARRAY') {
        my @values = grep {$_} map {
            $self->_remove_empty_fields($_);
        } @$value;
        return @values ? \@values : undef;
    }

    # Dice sends the string "null" sometimes
    return $value && $value ne 'null' ? $value : undef;
}

# Make Dice wages look nices
sub format_dice_salary {
    my ($self, $currency, $from, $to) = @_;

    # Format salary ranges.
    if($currency eq 'USD') {
        $currency = '$';
    } elsif($currency eq 'GBP') {
        $currency = '£';
    } else {
        $currency .= ' ';
    }

    my $formatted = join ' - ', map {
        my $wage = $_;
        $wage =~ s/000\z/,000/;
        $currency . $wage;
    } grep { $_ && $_ > 0} ( $from, $to );
    return $formatted;
}

# Adds the dice data to the candidate as attribute 'profile'
# after cleaning it up a bit
sub scrape_integrated_profile {
    my ($self, $candidate, $profile) = @_;

    # Clean up the profile and give it to the candidate
    # as an attribute
    my $id;
    my $type;

    $profile->{talentMatchId} = $profile->{id};


    my $name = $profile->{contact}->{formattedName};
    $candidate->name($name);
    $profile->{initials} = $self->name_initials($name);

    $candidate->email($profile->{contact}->{email});
    $candidate->contact_telephone(
        $profile->{contact}->{unformattedPhoneNumber}
    );
    
    $candidate->attr(headline => $profile->{desiredPosition});


    if($profile->{desiredEmployment}) {
        $profile->{desiredEmployment}->{job_types} = $self->map_dice_job_type(
            $profile->{desiredEmployment}->{type}
        );

        $profile->{desiredEmployment}->{hourly} = $self->format_dice_salary(
            $profile->{currency},
            $profile->{desiredEmployment}->{minimumHourlyPayRate},
            $profile->{desiredEmployment}->{hourlyPayRate},
        );

        $profile->{desiredEmployment}->{annual} = $self->format_dice_salary(
            $profile->{currency},
            $profile->{desiredEmployment}->{minimumAnnualSalary},
            $profile->{desiredEmployment}->{annualSalary},
        );
    }

    foreach my $job ( @{ $profile->{employmentHistoryList} } ) {
        my $start_date = $self->format_dice_datetime($job->{startYear});
        my $end_date = $self->format_dice_datetime($job->{endYear});

        my $date_range = join ' - ', grep {$_} ($start_date, $end_date);
        $job->{dateRange} = $date_range;
    }

    foreach my $education (@{ $profile->{educationList}}) {
        my $location = $self->format_dice_location($education->{location});
        $education->{location} = $location if $location;
    }

    my ($recent_employment) = grep {
        $_->{mostRecentEmployer}
    } @{$profile->{employmentHistoryList}};
    $profile->{mostRecentEmployment} = $recent_employment;

    $candidate->attr(json => JSON->new->pretty->encode($profile));

    my $location = $self->format_dice_location(
        $profile->{contact}->{location}
    );
    $profile->{location} = $location;


    # Format some dates.
    $profile->{dateAvailable} = $self->format_dice_datetime(
        $profile->{dateAvailable}
    );
    $profile->{lastModified} = $self->format_dice_datetime(
        $profile->{lastModified}
    );
    $profile->{datePosted} = $self->format_dice_datetime(
        $profile->{datePosted}
    );

    if(!$profile->{id}) {
        $candidate->has_cv(0);
        $candidate->can_forward(0);
    }
    $candidate->has_profile(1);
    $candidate->attr(profile => $profile);
}

sub download_profile_submit{
    my ($self, $candidate) = @_;

    my $access_token = $self->dice_oauth();

    my $id;
    my $type;

    # Dice offers several profile types
    # See docs
    my $profile = $candidate->attr('profile');
    if($profile->{talentMatchId} && $profile->{openWebId}) {
        $id = $profile->{talentMatchId};
        $type = 0; # Both

    } elsif($profile->{talentMatchId}) {
        $id = $profile->{talentMatchId};
        $type = 1; # Resume/CV profile

    } elsif($profile->{openWebId}) {
        $id = $profile->{openWebId};
        $type = 2; # Openweb
    }


    my $url = $self->base_url;
    $url->path_segments( $url->path_segments, $id );
    $url->query_form( $url->query_form, type => $type );

    my $response = $self->get( $url,
        'Authorization' => $access_token,
    );

    return $response;
}

# Make Dice's ISO8601 dates look nices
sub format_dice_datetime {
    my ($self, $date_string, $include_time) = @_;
    return '' unless $date_string;
    my $datetime = eval {
        DateTime::Format::ISO8601->parse_datetime($date_string)
    };
    return $date_string if $@;

    my $format = $include_time ? '%H:%M %d %b %Y' : '%d %b %Y';
    return $datetime->strftime($format);
}

# Stringify Dice's location structures
sub format_dice_location {
    my ($self, $location) = @_;
    return join ', ', grep { $_ } (
        $location->{formattedLocation},
        $location->{country},
        $location->{postalCode},
    );
}

# Return two (at most) initials from a name
# These are displayed where a candidate's missing avatar image would
# otherwise be
sub name_initials {
    my ($self, $name) = @_;
    my @initials = map{ uc substr($_, 0, 1) } split('\s', $name);
    @initials = grep {$_} @initials[0, -1];
    return \@initials;
}

sub map_dice_job_type {
    my ($self, $job_type) = @_;
    return '' unless $job_type;
    my %map = (
        'FULLTIME'      => 'Full-Time',
        'PARTTIME'      => 'Part-Time',
        'CON_HIRE'      => 'Contract to Hire',
        'CON_HIRE_CORP' => 'Contract to Hire-Corp-to-Corp',
        'CON_HIRE_IND'  => 'Contract to Hire-Independent',
        'CON_HIRE_W2'   => 'Contract to Hire-W2',
        'CON_CORP'      => 'Contract-Corp-to-Corp',
        'CON_IND'       => 'Contract-Independent',
        'CON_W2'        => 'Contract-W2',
        'CON'           => 'Contract',
    );

    my @types = map {
        $map{ $_ } // $_;
    } split(',', $job_type);
    return \@types;
}

# Expects salaries in USD and the
# salary_per to be checked externally
# See search_fields()
sub map_salary {
    my ($self, $from, $to) = @_;
    return unless $from || $to;
    unless( defined $to && length $to ) {
        # practically infinity
        $to = 100_000_000;
    }
    {
        no warnings;
        $from = int($from);
        $to = int($to);
    }
    if($from > $to) {
        my $temp = $from;
        $from = $to;
        $to = $temp;
    }

    my $salary = sprintf('[%d to %d]', $from, $to);
    $self->log_info('Mapped salary to %s', $salary);
    return $salary;
}

# Takes a min and max and returns
# a string like "[4 to 8]"
sub map_experience {
    my ($self, $min, $max) = @_;
    {
        no warnings;
        $min = int($min);
        if($min && !defined($max)) {
            # practically infinity
            $max = 100;
        }
        $max = int($max);
    }

    if($min > $max) {
        my $temp = $min;
        $min = $max;
        $max = $temp;
    }

    return '' unless $min || $max;
    return sprintf(
        '[%d to %d]',
        $min,
        $max
    );
}

sub scrape_generic_download_profile {
    my ($self, $candidate) = @_;

    my $json = JSON::decode_json($self->content());
    my $profile = $json->{integratedProfile};

    my $cleaned_details = $self->_remove_empty_fields($profile);

    $self->scrape_integrated_profile($candidate, $cleaned_details);

    $candidate->profile_downloaded( time );
    return $candidate;
}

# For these facets (only), further information can be requested
# from the board including list options and counts.
sub dynamic_facets {
    return {
        # board_key => token_key
        skills   => 'skills',
        title    => 'job_title',
        degree   => 'degree',
        auth     => 'work_auth',
        relocate => 'relocate',
        comp     => 'company',
        email    => 'has_email',
        jobType  => 'job_type',
        phone    => 'has_phone',
    };
}

# CV data is given during profile download,
# (re)request the profile and return it as an attachment
sub do_download_cv {
    my ($self, $candidate) = @_;
    $candidate = $self->download_profile($candidate);
    my $profile = $candidate->attr('profile');

    unless( $profile->{resume}->{resumeData} ){
        return $self->throw_cv_unavailable(
            'Candidate does not have a resume document associated with them'
        );
    }
    $self->log_debug('Candidate has a CV');

    my $filename = $profile->{resume}->{fileName};
    $filename =~ s/\s/-/g;
    return $self->to_response_attachment({
    filename => $filename,
        content_type => 'application/msword',
        decoded_content => MIME::Base64::decode_base64($profile->{resume}->{resumeData})
    });
}

sub feed_identify_error{
    my ($self, $message, $content) = @_;

    if($content =~ m/API access not allowed/) {
        $self->throw_lack_of_credit("Lack of profile view credits");
    }
    elsif($content =~ m/Bad credentials|No client with requested id/) {
        $self->throw_login_error("The login credentials entered are incorrect");
    }
    elsif($content =~ m/(Customer account is locked)/) {
        $self->throw_inactive_account($1);
    }
}

$standard_tokens{search_type} = {
    Label => 'Search Type',
    Type => 'List',
    SortMetric => 1,
    StartShown => 1,
    Options => [
        ['Both'           => '0'],
        ['Resume/CV only' => '1'],
        ['Open Web'       => '2'],
    ],
    Default => '0',
    DisplayStyle => 'major_filter',
};

$standard_tokens{sort_by} = {
    Label => 'Sort By',
    Type => 'Sort',
    Options => [
        ['Recency DESC'   => 'announceDate+desc'],
        ['Recency ASC'    => 'announceDate+asc'],
        ['Relevance DESC' => 'relevancy+desc'],
        ['Relevance ASC'  => 'relevancy+asc'],
    ]
};

$standard_tokens{likely_to_switch} = {
    Label => 'Likely to Switch',
    Type => 'list',
    SortMetric => 2,
    Options => [
        ['' => ''],
        ['Yes' => 'true'],
    ],
    Default => '',
};

$standard_tokens{has_email} = {
    Label => 'Provides Email Address',
    Type => 'list',
    SortMetric => 3,
    Options => [
        ['' => ''],
        ['Yes' => 'true'],
    ],
    Default => '',
};

$standard_tokens{company} = {
    Label => 'Company',
    Type => 'list',
    SortMetric => 4,
    Options => [
        ['Any' => ''],
    ],
    Default => '',
};

$standard_tokens{skills} = {
    Label => 'Skills',
    Type => 'multilist',
    SortMetric => 5,
    Options => [],
};

$standard_tokens{job_title} = {
    Label => 'Job Title',
    Type => 'multilist',
    SortMetric => 6,
    Options => [],
};

$standard_tokens{resume_search_heading} = {
    Label => 'Resume Only Filters',
    Type => 'heading',
    SortMetric => 100,
};

$standard_tokens{has_phone} = {
    Label => 'Provides Phone Number',
    Type => 'list',
    SortMetric => 101,
    Options => [
        [''    => ''],
        ['Yes' => 'true'],
        ['No'  => 'false'],
    ],
    Default => '',
};

$standard_tokens{job_type} = {
    Label => 'Job Type',
    Type => 'multilist',
    SortMetric => 102,
    Options => [
        ['Full-Time'                     => 'FULLTIME'],
        ['Part-Time'                     => 'PARTTIME'],
        ['Contract to Hire'              => 'CON_HIRE'],
        ['Contract to Hire-Corp-to-Corp' => 'CON_HIRE_CORP'],
        ['Contract to Hire-Independent'  => 'CON_HIRE_IND'],
        ['Contract to Hire-W2'           => 'CON_HIRE_W2'],
        ['Contract-Corp-to-Corp'         => 'CON_CORP'],
        ['Contract-Independent'          => 'CON_IND'],
        ['Contract-W2'                   => 'CON_W2'],
        ['Contract'                      => 'CON'],
    ],
};

$standard_tokens{degree} = {
    Label => 'Degree',
    Type => 'multilist',
    SortMetric => 103,
    Options => [
        ['High School'       => 'HS'],
        ['Military Service'  => 'MIL'],
        ['Vocational School' => 'VOC'],
        ['Associate'         => 'ASC'],
        ['MBA'               => 'MBA'],
        ['Pre-Bachelors'     => 'PBA'],
        ['Bachelors'         => 'BA'],
        ['Masters'           => 'MST'],
        ['Post-Masters'      => 'PMS'],
        ['Pre-Doctorate'     => 'PDO'],
        ['Doctorate'         => 'DOC'],
    ],
};

$standard_tokens{relocate} = {
    Label => 'Willing to Relocate',
    Type => 'list',
    SortMetric => 104,
    Options => [
        ['' => ''],
        ['Yes' => 'true'],
        ['No' => 'false'],
    ],
    Default => '',
};

$standard_tokens{work_auth} = {
    Label => 'Work Authorization',
    Type => 'multilist',
    SortMetric => 105,
    Options => [
        ['US Citizen'        => 'USC'],
        ['EU Eligible'       => 'EUE'],
        ['Green Card Holder' => 'GCH'],
        ['Emp Auth Doc'      => 'EAD'],
        ['TN Permit Holder'  => 'TNPH'],
        ['Canadian Citizen'  => 'CAN'],
        ['Have H1 Visa'      => 'H1'],
        ['Need H1 Visa'      => 'H1R'],
    ],
};

$standard_tokens{min_experience} = {
    Label => 'Minimum Years Experience',
    Type => 'list',
    SortMetric => 106,
    Options => [
        # Stringified so 0 is truthy and not omitted.
        map{ ["$_" => "$_"] } (0..50),
    ]
};

$standard_tokens{max_experience} = {
    Label => 'Maximum Years Experience',
    Type => 'list',
    SortMetric => 107,
    Options => [
        map{ ["$_" => "$_"] } (0..50),
    ]
};

$standard_tokens{open_web_heading} = {
    Label => 'Open Web Filters',
    Type => 'heading',
    SortMetric => 200,
};

$standard_tokens{web_profiles} = {
    Label => 'Web Profiles',
    Type => 'multilist',
    SortMetric => 201,
    Options => [
        ['Github'        => 'Github'],
        ['StackOverflow' => 'StackOverflow'],
        ['Dribbble'      => 'Dribbble'],
        ['Twitter'       => 'Twitter'],
        ['Facebook'      => 'Facebook'],
        ['LinkedIn'      => 'LinkedIn'],
        ['Meetup'        => 'Meetup'],
    ],
};


1;

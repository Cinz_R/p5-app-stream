package Stream::Templates::rengineering;

use strict;
use warnings;

use base qw(Stream::Engine::API::REST);

use JSON;
use URI::Template;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

my %CATEGORIES = (
    5283 => 'Aerospace',
    5284 => 'Automotive',
    5285 => 'Building Services',
    5286 => 'Civil and Structural',
    5287 => 'Construction',
    5288 => 'Defence',
    5289 => 'Environmental',
    5290 => 'Highways & Transport',
    5291 => 'Marine',
    5298 => 'Manufacturing',
    5292 => 'Medical & Pharmaceutical',
    5299 => 'Mining',
    5293 => 'Nuclear',
    5294 => 'Oil & Gas',
    5295 => 'Power & Energy',
    5296 => 'Rail',
    5297 => 'Water',
    5300 => 'Utilities',
);

$authtokens{rengineering_email} = {
    Label      => 'Email address',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{rengineering_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

$standard_tokens{categories} = {
    Label   => 'Categories',
    Type    => 'MultiList',
    Values  => &rengineering_categories(),
    SortMetric  => 5,
};

$standard_tokens{willing_to_relocate} = {
    Label   => 'Willing to Relocate',
    Type    => 'List',
    Values  => 'Any=|Yes=5270|No=5271',
    SortMetric  => 10,
};

sub default_url {
    return 'http://www.rengineeringjobs.com/main/api/broadbean';
}

sub search_url {
    return $_[0]->default_url . '/search';
}

sub download_profile_url {
    return $_[0]->default_url . '/profile';
}

sub download_cv_url {
    my $url = $_[0]->default_url . '/attached_file';
    return URI::Template->new( $url . '?email={email}&password={password}&profileId={candidate_id}' );
}

# overwritten - results begin from page 0
sub current_page {
    my ($self, $page) = @_;

    unless ( $page ) {
        return $self->{current_page};
    }

    $self->{current_page} = $page - 1;
    $self->log_info('Actual page searched is: %d', $self->{current_page});
    return $self->{current_page};
}

sub search_fields {
    my $self = shift;

    my %fields = (
        email => $self->token_value('email'),
        password => $self->token_value('password'),
        date_posted => $self->rengineering_cv_updated($self->token_value('cv_updated_within')),
        page => $self->current_page(),
    );
    $fields{keyword} = $self->token_value('keywords');
    $fields{category} = join(",", $self->token_value('categories'));
    $fields{job_type} = $self->rengineering_jobtype($self->token_value('default_jobtype'));
    $fields{willing_to_relocate} = $self->token_value('willing_to_relocate');

    my ($country, $city) = $self->rengineering_location($self->token_value('location_id'));
    $fields{country} = $country;
    $fields{city} = $city;

    return %fields;
}

sub search_submit {
    my $self = shift;
    my $uri = URI->new($self->search_url());
    $uri->query_form($self->search_fields());
    return $self->get($uri);
}

sub search_success {
    return qr/totalResults/;
}

sub scrape_number_of_results {
    my $self = shift;
    $self->total_results( $self->response_to_JSON()->{totalResults} );
}

sub scrape_candidate_array {
    my $self = shift;
    return @{ $self->response_to_JSON()->{candidates} };
}

# Some properties are returned as IDs - don't display them until
# labels are returned
sub scrape_candidate_details {
    my ($self, $json) = @_;
    return (
        candidate_id            => $json->{id},
        key_skills              => $json->{'Key Skills'},
        postcode                => $json->{'Post Code'},
        phone                   => $json->{'Phone Number'},
        employer                => $json->{'Employer Name'},
        city                    => $json->{'City/Town'},
        name                    => $json->{'CV Name'},
#        preferred_loc           => $json->{'Preferred Location'},
#        desired_salary          => $json->{'Desired Salary'},
        email                   => $json->{'Email'},
#        county                  => $json->{'County/State'},
        prev_jobtitle           => $json->{'Current or Last Job Title'},
        postdate_epoch          => $json->{'Post Date'},
#        preferred_role          => $json->{'Preferred Role Type'},
#        country_of_residence    => $json->{'Country of Residence'},
        industries              => $json->{'Industry'},
        date_last_employed      => $json->{'Dates for previous employment'},
        resume_filename         => $json->{'Upload Your CV'}->{filename},
        resume_content_type     => $json->{'Upload Your CV'}->{content_type},
    );
}

sub scrape_fixup_candidate {
    my ($self, $candidate) = @_;

    $candidate->has_profile(1);
    $candidate->has_cv(1);

    my @category_labels;
    foreach ( $candidate->attr('industries') ) {
        push @category_labels, $CATEGORIES{$_};
    }
    $candidate->attr( 'industries' => \@category_labels );

}

sub download_profile {
    my ($self, $candidate) = @_;
    my ($cv_response, $html);

    my $cv_url = $self->download_cv_url()->process(
            email => $self->token_value('email'),
            password => $self->token_value('password'),
            candidate_id => $candidate->candidate_id
        );

    $cv_response = $self->get( $cv_url );

    if ( $cv_response->is_success() ) {
        $html = eval { $self->{documents_api}->htmlify($cv_response->content); };
        if ( $@ ) {
            $candidate->attr('cv_html' => 'CV downloading is not available at this time. Please try later');
        }
        else {
            my $xdoc = $self->{html_parser}->load_html( string => Encode::encode('UTF-8', $html), encoding => 'UTF-8' );
            my ($xbody) = $xdoc->findnodes('/html/body');
            $candidate->set_attr('cv_html', join('', map { $_->toString() } $xbody->childNodes()));
        }
    }

    $candidate->attr('cv_html' => $self->rengineering_fixup_candidate_html_cv($candidate));

    return $candidate;
}

sub download_cv_submit {
    my ($self, $candidate) = @_;
    my $url = $self->download_cv_url()->process(
        email => $self->token_value('email'),
        password => $self->token_value('password'),
        candidate_id => $candidate->candidate_id,
    );
    return $self->get($url);
}

sub scrape_download_cv {
    my ($self, $candidate) = @_;
    my $response = $self->response();
    return $self->to_response_attachment({
        content_type => $candidate->attr('resume_content_type'),
        decoded_content => $response->decoded_content,
        filename => $candidate->attr('resume_filename'),
    });
}

sub feed_identify_error {
    my ($self, $message, $content) = @_;
    my $json = $self->response_to_JSON();

    my $error_msg = $json->{error};

    if ( $error_msg =~ m/(Wrong login or password)/ ) {
        return $self->throw_login_error($1);
    }
    elsif ( $error_msg =~ m/(Not allowed to search\/open resumes)/ ) {
        return $self->throw_inactive_account($1);
    }

    return 1;
}

=head2 rengineering_fixup_candidate_html_cv

    Strips some HTML retrieved from converting the CV doc to HTML
    via the tika service so that the page doesn't look all that horrible

=cut
sub rengineering_fixup_candidate_html_cv {
    my ($self, $candidate) = @_;
    return unless my $html = $candidate->attr('cv_html');

    $html =~ s/<img.+?>//g;
    $html =~ s/class="body"//g;
    $html =~ s/<(\/)?h[1-2]>/<$1h4>/g;
    $candidate->attr('cv_html' => $html);
}

=head2 rengineering_location

    Returns a list containing a country ID and a city name or nothing if no location_id.
    Country ID is taken from the boards loc mapping and city is just the location name

=cut
sub rengineering_location {
    my ($self, $location_id) = @_;

    unless ( $location_id ) {
        $self->log_info('No location_id provided, returning an owl');
        return ('','');
    }

    my ($country, $city);

    $country = $self->BEST_LOCATION_MAPPING('rengineering', $location_id, $self->token_value('location_within'));
    $country =~ m/\d+?~~(\d+)/ if $country;
    $country = $1 || '';

    my $location = $self->location_api()->find({ id => $location_id });

    if ( $location && $location->type !~ m/country|continent/i ) {
        $city = $location->name;
    }

    return ($country, $city);
}

=head2 rengineering_cv_updated

    Maps the board IDs for date_posted field to the selected cv_updated_within

    Mappings are:
    Any Period=36 / 12 Months or less=29 / 6 Months or less=30 / 3 Months or less=31 / 1 Month or less=37 / Last 2 week=38 / Last Week=39 / Yesterday=40
=cut
sub rengineering_cv_updated {
    my ($self, $cv_updated_within) = @_;
    my %mapping = (
        'TODAY' => 40,
        'YESTERDAY' => 40,
        '3D' => 39,
        '1W' => 39,
        '2W' => 38,
        '1M' => 37,
        '2M' => 31,
        '3M' => 31,
        '6M' => 30,
        '1Y' => 29,
        '2Y' => 36,
        '3Y' => 36,
        'ALL' => 36,
    );
    return $mapping{$cv_updated_within};
}

=head2 rengineering_categories

    Takes the global categories hash and combines it
    into a string which is used by standard_token{categories}.

    This allows reuse of %CATEGORIES for the candidates industry lookup and
    when performing a search

=cut
sub rengineering_categories {
    my ($self) = @_;
    my @categories;
    while ( my ($id, $label) = each %CATEGORIES ) {
        push @categories, "$label=$id";
    }
    return join("|", sort @categories);
}

sub rengineering_jobtype {
    my ($self, $jobtype) = @_;
    return 50 if $jobtype eq 'Permanent';
    return 2001 if $jobtype eq 'Temporary';
}

1;

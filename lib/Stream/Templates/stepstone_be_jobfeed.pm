package Stream::Templates::stepstone_be_jobfeed;

use strict;
use warnings;

use base qw(Stream::Engine::Robot);

use Stream::Constants::SortMetrics qw(:all);

use Stream::Agents::Stepstone;
use JSON ();
use URI;
use List::MoreUtils qw/uniq/;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

my %FACET_TOKENS = (
    token_names => [],
    parameters => {
        language    => { level => 1 },
        function    => { minDuration => 0, maxDuration => 7, fact_pref => 'FACT' },
        sector      => { minDuration => 0, maxDuration => 7, fact_pref => 'FACT' },
    },
    ids => {
        worktype    => 'widget_worktype',
        statute     => 'widget_statute',
        diploma     => 'widget_diploma',
    },
    types => {
        function    => 'experience_duration',
        sector      => 'experience_duration',
        statute     => 'widget',
        worktype    => 'widget',
        diploma     => 'Widget',
    },
    options => {
        function    => { forceLatestJob => 'false' },
    },
    values => {
        worktype    => '0',
        statute     => '0',
        sector      => [0, 6],
        function    => [0, 6],
        diploma     => '5',
    },
);

$authtokens{stepstone_be_jobfeed_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{stepstone_be_jobfeed_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

$derived_tokens{salary_cur} = {
    Type => 'Currency',
    Options => 'EUR',
    Default => 'EUR',
    Helper  => 'SALARY_CURRENCY',
    HelperMetric => 1,
};

$derived_tokens{salary_per} = {
    Type         => 'SalaryPer',
    Options      => 'month',
    Default      => 'month',
    Helper       => 'SALARY_PER(salary_per|salary_from|salary_to)',
    HelperMetric => 1,
};

$derived_tokens{salary_from} = {
    Type            => 'SalaryValue',
    EUR_month       => '(1000=379~380~381~382|2000=383~384~385|3000=386~387|5000=390|7000=392|9000+=394)',
    Helper          => 'SALARY_BAND_BELOW(salary_from|%salary_cur%|%salary_per%|%salary_from%)',
    HelperMetric    => 2
};

$derived_tokens{salary_to} = {
    Type            => 'SalaryValue',
    EUR_month       => '(1000=379~380~381~382|2000=383~384~385|3000=386~387|5000=390|7000=392|9000+=394)',
    Helper          => 'SALARY_BAND_ABOVE(salary_to|%salary_cur%|%salary_per%|%salary_to%)',
    HelperMetric    => 2
};

sub AGENT_CLASS { 'Stream::Agents::Stepstone' }

sub base_url { return 'https://www.stepstone.be'; }

sub login_url { return $_[0]->base_url() . "/5/index.cfm?event=rsLogin.doLogin"; }

sub logout_url { return $_[0]->base_url() . "/5/index.cfm?event=recruiterspace.dologout"; }

sub search_request_method { 'POST' }

sub results_per_page { 10 }

sub search_url { return $_[0]->base_url() . '/?event=directsearch:searchui.search.doCvSearchAjax'; }

sub search_success { return qr{RESULTLIST}; }

sub download_profile_success { return qr{Profile activity}; }

sub scrape_candidate_headline { 'last_updated' }

sub login_success { return qr/logout/i; }

sub login_post {
    return {
        'loginUserLogin' => $_[0]->token_value('username'),
        'loginUserPassword' => $_[0]->token_value('password'),
    };
}

sub search_fields {
    my $self = shift;

    my %fields = (
        resultListOffset    => $self->results_offset(),
        resultListLimit     => $self->results_per_page(),
        cvsearchpreview     => 'cvsearchpreview',
        showFacetFilter     => 'false',
        responseFormat      => 'json'
    );

    if ( $self->token_value('location_id') ) {
        $self->results()->add_notice('Location is not supported by the board');
    }

    my @criteria = ();

    if ( my $keywords = $self->token_value('keywords') ) {
        push @criteria, {
            id          => $keywords,
            type        => 'keyword',
            value       => '0',
            clause      => 'MUST',
            codeType    => 'keyword',
            description => $keywords,
            fatherId    => '0',
            parameters  => {}
        };
    }

    my $dt = $self->cv_updated_within_to_datetime($self->token_value('cv_updated_within'));
    if ( $dt ) {
        push @criteria, {
            id          => $dt->strftime('%Y%m%d'),
            type        => 'lastupdated',
            value       => '0',
            clause      => 'MUST',
            codeType    => 'lastupdated',
            description => 'lastupdated',
            fatherId    => '0',
            parameters  => {},
            queryType   => 'StringRange',
        };
    }

    if ( my $last_activity = $self->token_value('last_activity') ) {
        push @criteria, {
            id          => $last_activity,
            type        => 'lastactivity',
            value       => '0',
            clause      => 'MUST',
            codeType    => 'lastactivity',
            description => 'lastactivity',
            fatherId    => '0',
            parameters  => {},
            queryType   => 'StringRange',
        };
    }

    my @salary_ids = ();
    foreach my $salary ( qw/salary_from salary_to/ ) {
        if ( my $token = $self->token_value($salary) ) {
            my @ids = split(/~/, $token);
            push @salary_ids, @ids;
        }
    }

    if ( scalar(@salary_ids) > 0 ) {
        @salary_ids = uniq(map { 'widget_revenue_' . $_ } @salary_ids);
        push @criteria, {
            id          => \@salary_ids,
            type        => 'widget',
            value       => [0,0],
            clause      => 'MUST',
            codeType    => 'revenue',
            description => 'Expected monthly gross salary',
            fatherId    => '0',
            parameters  => {},
            queryType   => 'StringListQuery',
        };
    }

    foreach my $token ( @{$FACET_TOKENS{token_names}} ) {
        my @values = $self->token_value($token);

        if ( scalar(@values) > 0 ) {
            my $parameters = $FACET_TOKENS{parameters}->{$token} // {};
            my $options = $FACET_TOKENS{options}->{$token} // '';

            foreach my $value ( @values ) {
                my ($label, $id) = split('_', $value);
                my $id_prefix = $FACET_TOKENS{ids}->{$token} // $token;

                push @criteria, {
                    id          => $id_prefix . '_' . $id,
                    type        => $FACET_TOKENS{types}->{$token} // $token,
                    value       => $FACET_TOKENS{values}->{$token} // '1',
                    clause      => 'MUST',
                    codeType    => $token,
                    description => $label,
                    fatherId    => '0',
                    parameters  => $parameters,
                    ( $options ? (options => $options) : () ),
                };
            }
        }
    }

    $fields{criteria} = JSON::encode_json(\@criteria);

    return %fields;
}

sub scrape_number_of_results {
    my $self = shift;

    my $response = $self->response();
    my $json = eval { JSON::from_json($response->decoded_content()); };
    if ( $@ ) {
        $self->log_warn("Cant decode json for finding number of results: $@");
        return;
    }

    if ( my $total = $json->{TOTALRESULTSCOUNT} ) {
        $self->log_info("Found [$total] number of results in total");
        $self->total_results($total);
    }
    else {
        $self->log_warn("Did not find any total number of results");
    }
    return;
}

sub search_navigate_to_next_page {
    my ($self) = @_;
    my %fields = $self->search_fields();
    $fields{resultListOffset} = $self->results_offset();
    return $self->http_post($self->search_url, \%fields);
}

sub before_scraping_results {
    my ($self) = @_;
    my $response = $self->response();
    my $json = eval { JSON::from_json($response->decoded_content()); };
    if ( $@ ) {
        $self->log_warn("Unable to decode json before scraping results: $@");
        return;
    }

    $self->stepstone_scrape_facets($json->{FACETDATA});

    if ( my $content_from_json = $json->{RESULTLIST} ) {
        $self->log_info("Found content from JSON in RESULTLIST key, setting new recent_content");
        $self->recent_content($content_from_json);
    }
}

sub stepstone_scrape_facets {
    my ($self, $data) = @_;
    return unless $data;

    my @facets = ();
    my $sort_metric = 10;
    my @token_names = ();

    while ( my ($key, $options) = each %$data ) {
        push @token_names, $key;

        my $label = $key;
        $label =~ s/_/ /g;

        my $facet = {
            Name    => $key,
            ID      => $self->destination() . '_' . $key,
            Type    => 'multilist',
            Label   => ucfirst $label,
            SortMetric => $sort_metric++,
        };

        my @values = $self->token_values($facet->{Name});
        my %selected_values = map { $_ => 1 } @values;
        $facet->{SelectedValues} = \%selected_values;

        map {
            push @{$facet->{Options}}, [
                $_->{DESCRIPTION},
                $_->{DESCRIPTION} . '_' . $_->{RELATIVEID},
                $_->{RELATIVEFREQUENCY}
            ];
        } @{$options};

        push @facets, $facet;
    }

    $FACET_TOKENS{token_names} = \@token_names; # so we can do a lookup in search_fields

    $self->results()->facets(\@facets);
}

sub scrape_candidate_xpath {
    return '//div[@class="resultlist__result"]';
}

sub scrape_candidate_details {
    return (
        candidate_id        => './/@data-id',
        first_name          => './/span[@class="miniprofile__firstname" or @class="miniprofile__anonymousname"]/text()',
        last_name           => './/span[@class="miniprofile__lastname" or @class="miniprofile__anonymoustext"]/text()',
        last_activity       => './/span[@class="formatted-date"][1]/text()',
        last_updated        => './/span[@class="formatted-date"][2]/text()',
        location            => ['.//div[contains(@class,"miniprofile__section-personaldetails")]/div/div/span/span/span/span[@class="miniprofile__detailmain"]/span/text()'],
        age                 => './/div[@class="miniprofile__sectiondetails miniprofile__sectiondetails-age"]/div/span/span[@class="miniprofile__detailmain"]/text()',
        last_exp_title      => './/div[contains(@class, "miniprofile__section-workexperience")]/div/div/span/span[@class="miniprofile__detailmain"]/text()',
        last_exp_years      => './/div[contains(@class, "miniprofile__section-workexperience")]/div/div/span/span[@class="miniprofile__detailyearsexperience"]/span[@class="miniprofile__detailmain"]/text()',
        last_exp_from       => './/div[contains(@class, "miniprofile__detaildatesexperience")]/div/div/span/span[@class="miniprofile__detaildatesexperience"]/span[@class="miniprofile__detailextra"]/span/text()',
        last_exp_to         => './/div[contains(@class, "miniprofile__detaildatesexperience")]/div/div/span/span[@class="miniprofile__detaildatesexperience"]/span[@class="miniprofile__detailextra"]/text()',
        skills              => ['.//div[contains(@class, "miniprofile__section-skills")]/div[@class="miniprofile__sectiondetails"]/div/span/text()'],
        cv_endpoint         => './/div[contains(@class, "miniprofile__section-attachments")]/div/div/a[1]/@href',
        education           => ['.//div[contains(@class, "miniprofile__section-education")]/div/div/span[@class="miniprofile__detailtext"]/span/text()'],
        preferred_location  => './/div[contains(@class, "miniprofile__sectiondetails-preferredlocation")]/div/span/text()',
        preferred_position  => './/div[contains(@class, "miniprofile__sectiondetails-availability")]/div/span/text()',
        cant_download_cv    => './/li[contains(@class, "miniprofile__action-viewmainattachment")]/@title',
    );
}

sub scrape_fixup_candidate {
    my ($self, $candidate) = @_;

    $candidate->has_profile(1);
    $candidate->has_cv(0) if $candidate->attr('cant_download_cv'); # if board says they don't have cv, mark as no cv available at all

    if ( my $cv_endpoint = $candidate->attr('cv_endpoint') ) {
        $candidate->attr(cv_link => $self->base_url() . $cv_endpoint );
    }

    if ( $candidate->attr('last_exp_from') && $candidate->attr('last_exp_to') ) {
        $candidate->attr(last_exp_duration => $candidate->attr('last_exp_from') . $candidate->attr('last_exp_to') );
    }

    foreach my $attr ( qw/location skills education/ ) {
        if ( $candidate->attr($attr) && ref($candidate->attr($attr)) eq 'ARRAY' ) {
            $candidate->attr($attr => join(', ', @{$candidate->attr($attr)}) );
        }
    }
}

sub profile_url {
    my ($self, $candidate) = @_;
    my $uri = URI->new($self->base_url(). '/5/index.cfm');
    $uri->query_form(
        event           => 'recruiterspace.cvsearchv2.cvview.tabs',
        cvid            => $candidate->candidate_id(),
        tabid           => 'view',
        full            => 1,
        log             => 1,
        source          => 'resultList',
        PnetListingID   => 1
    );
    return $uri->as_string;
}

sub download_profile_submit {
    my ($self, $candidate) = @_;

    my $response = $self->get($self->profile_url($candidate));

    if ( !$response->is_success() ) {
        $self->feed_identify_error($response->message, $response->decoded_content);
    }

    my $json = eval { JSON::from_json($response->decoded_content); };
    if ( $@ ) {
        $self->log_warn('Unable to decode json for downloading profile. Error was: %s', $@);
        $self->throw_error('Unable to download profile');
    }

    $self->recent_content($json->{CVDATA}); #NB: this is not cv data, it's the profile html

    return $response;
}

sub scrape_download_profile_xpath {
    return '//div[contains(@class,"fullprofileContainer")]';
}

sub scrape_download_profile_details {
    return (
        address         => ['.//ul[@class="address"]/li/text()'],
        phone_numbers   => ['.//ul[@class="phone"]/li/text()'],
        email           => './/ul[@class="email"]/li[1]/text()',
        cv_attid        => './/a[contains(@class, "attachment__document-primary")]/@href',
    );
}

sub scrape_fixup_candidate_profile {
    my ($self, $candidate) = @_;

    my @address_parts = $candidate->attr('address');
    if ( scalar(@address_parts) > 0 ) {
        $candidate->attr(address => join(', ', @address_parts) );
    }

    my @phone_numbers = $candidate->attr('phone_numbers');
    if ( scalar(@phone_numbers) > 0 ) {
        $candidate->attr(phone_numbers => join(', ', @phone_numbers) );
    }

    if ( my $cv_attid = $candidate->attr('cv_attid') ) {
        my ($attid) = $cv_attid =~ m/\('(\d+)'?/;
        my $uri = URI->new($self->base_url() . '/5/index.cfm');
        $uri->query_form(
            event   => 'recruiterspace.cvsearchv2.cvAttachment.downloadFile',
            attid   => $attid,
            cvid    => $candidate->candidate_id(),
        );
        $candidate->attr(cv_link => $uri->as_string );
    }
}

sub after_download_profile_scrape_success {
    my ($self, $candidate) = @_;

    my $scraper = $self->scraper();

    my @profile = $scraper->findnodes('.//div[@class="innerProfileContent"]');
    if ( scalar(@profile) > 0 ) {
        $candidate->attr(profile_content => $profile[0]->as_HTML);
    }
}

sub before_downloading_cv {
    my ($self, $candidate) = @_;

    if ( !$candidate->attr('cv_link') ) {
        $self->download_profile($candidate);
    }
}

sub check_for_login_error {
    my $self = shift;

    my $response = $self->response();

    if ( $response->decoded_content =~ m/multiple login not allowed/i ) {
        $self->throw_login_error('Multiple account usage is not allowed by the board');
    }
    $self->SUPER::check_for_login_error();
}

sub feed_identify_error {
    my ( $self, $message, $content ) = @_;
}

1;

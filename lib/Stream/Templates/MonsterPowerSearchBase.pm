#!/usr/bin/perl

# vim: expandtab:tabstop=4

package Stream::Templates::MonsterPowerSearchBase;

# DOCS: http://partner.monster.com/power-resume-search-query

use strict;
use utf8;

use base qw(Stream::Engine::API::XML);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

use Stream::Constants::SortMetrics qw(:all);
use DateTime::Format::Strptime;
use Bean::HTMLStrip;

    # Skipped since not available on Power Search directly
    #$standard_tokens{clvid} = {
    #    Label => 'Career Level',
    #    Type  => 'MultiList',
    #    Options => '(Other=9|Assistant=15|Coordinator=14|Clerk=13|Specialist=12|Representative=11|Analyst=10|Lead=8|Manager=7|Head=6|Director=5|Vice President=4|General Manager=3|Executive Level=2|President=1)',
    #    SortMetric => $SM_EXPERIENCE,
    #};

    $standard_tokens{'cname'} = {
        Label      => "Candidate Name",
        Type       => 'Text',
        Size       => 60,
        MaxLength  => 128,
        SortMetric => 40,
    };

    $standard_tokens{'jt'} = {
        Label      => 'Job title',
        Type       => 'Text',
        Essential  => 1,
        Size       => 60,
        MaxLength  => 500,
        Example    => '(e.g., Sales, Accountant, Engineer)',
        SortMetric => $SM_KEYWORDS,
    };

    $standard_tokens{'comp'} = {
        Label      => 'Company name',
        Type       => 'Text',
        Size       => 60,
        MaxLength  => 500,
        SortMetric => 30,
        Example    => 'This field accepts multiple values, separated by a comma',
    };

   $derived_tokens{'and_terms'} = {
        Type       => 'Text',
        Label      => 'Skills',
        Helper     => 'monster_split_keywords(%keywords%|and_terms|or_terms|not_terms|exact_terms)',
        SortMetric => $SM_KEYWORDS,
    };

    $derived_tokens{or_terms} = {
        Type => 'Text',
        SortMetric => $SM_KEYWORDS,
    };

    $derived_tokens{exact_terms} = {
        Type => 'Text',
        SortMetric => $SM_KEYWORDS,
    };

    $standard_tokens{'sch'} = {
        Label      => 'University / College',
        Type       => 'Text',
        Size       => 60,
        MaxLength  => 500,
        SortMetric => 25,
        Example    => 'This field accepts multiple values, separated by a comma',
    };

    $posting_only_tokens{'mdatemaxage'} = {
        Label      => 'Modified date maximum age',
        Helper     => 'monsterpowersearch_mdatemaxage(%cv_updated_within%)',
        HelperMetric => 3,
    };
    #mdateminage skipped, not availbale on power search directly

   $standard_tokens{minedulvid} = {
        Label => 'Minimum Education Level',
        Type  => 'List',
        Options => '(Any=|Some High School Coursework=12|High School or equivalent=1|Certification=2|Vocational=3|Some College Coursework Completed=9|Associate Degree=4|Bachelors Degree=5|Masters Degree=6|Doctorate=7|Professional=8)',
        SortMetric => 20,
    };

    #skipped, rv (ResumeValue), rb(Resume Board), not available on Power Search directly

   $standard_tokens{tjtid} = {
        Label      => 'Job Type',
        Type       => 'MultiList',
        Options    => '(Permanent=1|Contract=2|Intern=3|Temp=4|Seasonal=5|FullTime=6|PartTime=7|Statutaire=8|PerDiem=9)',
        SortMetric => 50,
        Example    => 'Ctrl-click to select multiple job types',
    };

   $standard_tokens{relo} = {
        Label => 'Willing to Relocate',
        Type  => 'List',
        Options => '(Any=|No=false|Yes=true)',
        SortMetric => 110,
    };

    $standard_tokens{wa_choice} = {
        Label => 'Work Authorisation',
        Type  => 'List',
        Options => '(Any=|Authorized to work for any employer=1)',
    };

    $derived_tokens{wa} = {
        Label => 'Work Authorisation',
        Type  => 'List',
        Helper  => 'monsterpowersearch_wa_location(stream_monsterpowersearch_wa|%location_id%)',
        SortMetric => $SM_LOCATION,
        #Options => '(Authorized to work for any employer=1|Authorized to work for current employer only=2|Require sponsorship=3)',
    };

    $derived_tokens{salary_cur} = {
        Type  => 'Currency',
        Options => '(AED|ARS|AUD|BEF|BOB|BRL|CAD|CHF|CLP|CNY|COP|CZK|DEM|DKK|EGP|ESP|EUR|FIM|FJD|FRF|GBP|GRD|GTQ|HKD|HUF|IDR|IEP|ILS|INR|IQD|ITL|JPY|KRW|LUF|MXN|MYR|NLG|NOK|NZD|PEN|PGK|PHP|PKR|PLN|PYG|ROL|RUR|SAR|SEK|SGD|SKK|THB|TND|TRY|TTD|TWD|USD|UYU|VEB|ZAR)',
        Default => 'GBP',
        Helper => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric => $SM_SALARY,
    };

    $derived_tokens{salary_per} = {
        Type  => 'SalaryPer',
        Options => '(annum)',
        Helper => 'SALARY_PER(salary_per|salary_from|salary_to)',
        Default => 'annum',
        HelperMetric => 2,
        SortMetric => $SM_SALARY,
    };

    $posting_only_tokens{tsni} = {
        Helper => 'monster_target_salary_not_included(%salary_from%|%salary_to%|%salary_unknown%)',
        HelperMetric => 10,
    };

    $standard_tokens{'edumjr'} = {
        Label      => 'Degree',
        Type       => 'Text',
        Size       => 60,
        MaxLength  => 500,
        SortMetric => 22,
    };

    $standard_tokens{'ten'} = {
       Label      => 'Job Duration',
       Type       => 'List',
       Options    => '(Any=|1 year=1|2 years=2|5 years=5|10 years=10)',
       SortMetric => $SM_EXPERIENCE,
    };

    $standard_tokens{'wtt'} = {
        Label => 'Willing to Travel',
        Type  => 'List',
        Options => '(Any=|No travel required=1|Up to 25% travel=2|Up to 50% travel=3|Up to 75% travel=4|Up to 100% travel=5)',
        SortMetric => 100,
    };

    #Either use yex or ten
    #yex dosent work, seems to map against Job Duration on site
    #needs to be yex for UK / EU searches, as this only affects the ranking of results
    #use yexf for USA as this affects the results by filtering on search criteria
    $standard_tokens{'yex'} = {
        Label      => 'Years of Experience',
        Type       => 'Text',
        Essential  => '1',
        Size       => 60,
        MaxLength  => 6,
        SortMetric => 1, # They want this option to display first in search options
        Example    => '(e.g., 4, 2-3, 5+, <8)',
    };

    # Skipped since not available on Power Search directly
    #$standard_tokens{'gsc'} = {
    #    Label => 'Global Security Clearance',
    #    Type  => 'List',
    #    Options => '(None=11|Active Confidential=12|Active Secret=13|Active Top Secret=14|Inactive Confidential=15|Inactive Secret=16|Inactive Top Secret=17)',
    #    SortMetric => $SM_ELIGIBILITY,
    #};

    $posting_only_tokens{loc} = { #also sets co value
        Helper => 'monster_location(%location_id%|%location_within_miles%)',
        HelperMetric => 10,
    };

sub monster_location{
    my $self = shift;
    my $location_id = shift;
    my $location_within = shift;

    my $location = $self->location_api()->find({
        id => $location_id,
    });

    return unless $location;

    #set co
    my $country_code = $location->iso_country();
    if ( $country_code eq 'GB' ) {
        $country_code = 'UK';
    }

    $self->log_debug('Country code is [%s]', $country_code);
    $self->token_value('co' => $country_code) if $country_code;

    my $return = $location->name();

    # if location is a place in the USA, append the state ISO to remove ambiguity
    # eg. search "New Hope, MN" and not "New Hope, PA"
    if ( $location->is_place() && $location->in_usa() ) {
        my $state_iso = $self->BEST_LOCATION_MAPPING('ISO_STATES', $location_id, $location_within);
        $return = sprintf("%s, %s", $location->name(), $state_iso);
    }
    $return .= "-${location_within}" if( $location_within && $return );

    return $return;
}

sub monster_target_salary_not_included {
    my $self = shift;
    my $salary_from = shift;
    my $salary_to   = shift;
    my $salary_unknown = shift;

    if ( $salary_from || $salary_to ) {
        return $salary_unknown || 0;
    }

    return;
}

sub monsterpowersearch_wa_location {
    my $self = shift;
    my ($mapping, $location_id) = @_;

    my $wa = $self->token_value('wa_choice');

    return '' if !$wa;

    my ($monster_location) = $self->LOCATION_MAPPING($mapping, $location_id);

    return $monster_location."-".$wa if ( $monster_location );

    return '';
}

sub monsterpowersearch_mdatemaxage {
    my $self = shift;
    my $interval = shift;

    my %day_maps = (
        'TODAY' => 1,
        'YESTERDAY' => 2,
        '3D' => 3,
        '1W' => 7,
        '2W' => 14,
        '1M' => 30,
        '2M' => 60,
        '3M' => 90,
        '6M' => 180,
        '1Y' => 365,
        '2Y' => 730,
        'ALL' => 1095, # 'All resumes' option in monster sets minutes to 3 years
    );

    my $n_days = $day_maps{$interval};

    ## NOTE - need to find out if this feed only returns 18 months of CV's like the Monster search feed does

    # calculate without considering months with 31 days, as monster does
    return ( $n_days * 24 ) * 60;
}

sub search_url {
    return 'http://prsx.monster.com/query.ashx'; # Live
    #return 'http://208.71.198.110/query.ashx'; # Test
}

sub search_submit {
    my $self = shift;

    $self->monster_catch_missing_credentials();

    my $search_uri = $self->monster_search_uri();
    return $self->search_get(
        $search_uri,
    );
}

sub monster_search_uri {
    my $self = shift;

    my %query_form = (
        # Mandatory
        ver           => '2.0',
        cat           => $self->token_value( 'cat' ) || '',

        # Optional
        page          => $self->current_page(),
        pagesize      => 40,
    );

    # Build Skills string
    my ( @and_terms , @or_terms , @exact_terms );

    require URI::Escape;
    my @all_terms = grep { $_ } ( $self->token_value('and_terms') ,  $self->token_value('or_terms') , $self->token_value('exact_terms') );
    if ( scalar ( @all_terms ) <= 1 ) { # single terms should be nice to have
        @and_terms = map { URI::Escape::uri_escape( $_ ) ." nth" } @all_terms;
        $self->log_debug('Found single keyword, making this nth');
    }else {
        @and_terms = map { URI::Escape::uri_escape( $_ ) ." req" } $self->token_value('and_terms'); #required
        @or_terms = map { URI::Escape::uri_escape( $_ ) ." nth" } $self->token_value('or_terms'); #nice to have
        @exact_terms = map { URI::Escape::uri_escape( $_ ) ." req" } $self->token_value('exact_terms'); #required
    }
    my $skills =  join ( ',', @and_terms, @or_terms, @exact_terms );

    #usa wants the skills/keywords in separate fields #42909
    my @individual_skills_fields = qw/skills1 skills2 skills3/;
    foreach my $individual_skill( @individual_skills_fields){
        if( my $skills_keywords =  $self->token_value( $individual_skill )){
            $skills .= "," if $skills; 
            $skills .= URI::Escape::uri_escape( $skills_keywords ) . " " . $self->token_value( $individual_skill . "_nice_req" );
        }

    }

    my %optional_fields = (
        #q             => '%keywords%', # using skills instead
        #clvid
        cname         => $self->token_value( 'cname' ) || '',
        jt            => $self->token_value( 'jt' ) || '',
        comp          => $self->token_value( 'comp' ) || '',
        sk            => $skills || '',
        sch           => [ split( ',', $self->token_value( 'sch' ) ) ] || '',
        mdatemaxage   => $self->token_value( 'mdatemaxage' ) || '',
        #mdateminage
        minedulvid    => $self->token_value( 'minedulvid' ) || '',
        #rv
        #rb
        tjtid         => [ $self->token_value( 'tjtid' ) ] || '',
        relo          => $self->token_value( 'relo' ) || '',
        wa            => $self->token_value( 'wa' ) || '',
        #tsalcur
        #tnsalmin
        #tnsalmax
        #tsni
        edumjr        => $self->token_value( 'edumjr' ) || '',
        ten           => $self->token_value( 'ten' ) || '',
        wtt           => $self->token_value( 'wtt' ) || '',
        yex           => $self->token_value( 'yex' ) || '',
        yexf          => $self->token_value( 'yexf' ) || '',
        #gsc
        co            => $self->token_value( 'co' ) || '',
        loc           => $self->token_value( 'loc' ) || '',
        #lstat
        #veteran
    );

    my $salary_min = $self->token_value( 'salary_from' );
    my $salary_max = $self->token_value( 'salary_to' );
    if ( $salary_min || $salary_max ) {
        # only send salary cur if they have entered a salary
        #$query_form{tnsalmin} = $self->token_value( 'salary_from' ) || 0; #do not send, as not available directly
        $query_form{tnsalmax} = $self->token_value( 'salary_to' ) || $self->token_value( 'salary_from' );
        $query_form{tsni}    = $self->token_value( 'tsni' );
        $query_form{tsalcur} = $self->token_value( 'salary_cur' );
    }

    while ( my ($field, $value) = each %optional_fields ) {
        if ( $value ) {
            if ($field ne 'sk') { #skills is encoded separately
                if ( ref($value) eq 'ARRAY' ){ # we dont want to double escape the commas for list values / array refs in optional_fields
                    $value = join ( ',' , map { URI::Escape::uri_escape($_) } @$value );
                    next if !$value;
                }
                else{
                    $value = URI::Escape::uri_escape( $value );
                }
            }
            $query_form{ $field } = $value;
        }
    }

    my $search_uri = URI->new( $self->search_url() );
    $search_uri->query_form( \%query_form );

    return $search_uri;
}

sub monster_split_keywords {
    my $self = shift;
    my $keywords = shift;
    my $and_token = shift;
    my $or_token  = shift;
    my $not_token = shift;
    my $exact_token = shift;

    $self->log_debug('Splitting keywords: [%s]', $keywords);

    my $q = Stream::Keywords->new( $keywords );

    if ( !$q->process_query() ) {
        $self->log_debug('Unable to split up the keywords');
        $self->throw_invalid_boolean_search('We could not process the query');
    }

    $self->log_debug( $q->diag() );

    my ($and_words)   = join(' ', $q->and_words() );
    $self->log_debug('AND WORDS: [%s]', $and_words );
    $self->token_value( $and_token   => $q->and_words );

    my ($or_words)    = join(' ', $q->or_words() );
    $self->log_debug('OR WORDS: [%s]', $or_words );
    $self->token_value( $or_token    => $q->or_words );

    my ($not_words)   = join(' ', $q->not_words() );
    $self->log_debug('NOT WORDS: [%s]', $not_words );
    $self->token_value( $not_token   => $q->not_words );

    my ($exact_words) = join(' ', $q->exact_words() );
    $self->log_debug('PHRASES: [%s]', $exact_words );
    $self->token_value( $exact_token => $q->exact_words );

    return;
}

sub scrape_candidate_xpath { return '/Monster/Resumes/Resume'; }
sub scrape_candidate_details {
    return (
        # SID and Value are the same, but Monster keep switching between the two
        # without telling us so we handle both
#        candidate_id       => './@Value', # e.g. u84tgt63iwut2z7b
        candidate_id       => './@SID', # e.g. u84tgt63iwut2z7b
        candidate_value    => './@Value',

        relevance          => './Relevance/text()',
        distance           => './Distance/text()', # not sure if this is still used
        raw_date_created   => './DateCreated/@Date',
        raw_date_modified  => './DateModified/@Date',
        total_experience   => './TotalYearsExperience/text()',
        snippet            => './ResumeTitle/text()',

        confidential       => './PersonalData/Confidential/text()',
        email              => './PersonalData/EmailAddress/text()',
        firstname          => './PersonalData/Name[1]/First/text()',
        lastname           => './PersonalData/Name[1]/Last/text()',
        country_id         => './PersonalData/Address[1]/Country/@LID',
        state_id           => './PersonalData/Address[1]/State/@LID',
        city               => './PersonalData/Address[1]/City/text()',
        postal_code        => './PersonalData/Address[1]/PostalCode/text()',
        location_text      => './PersonalData/Address[1]/Location/text()',

        military_service_flag => './PersonalData/MilitaryExperience/ServiceFlag/text()',
        military_involvement  => './PersonalData/MilitaryExperience/MilitaryInvolvement/text()',

        target             => [
            './Target' => {
                job_title  => './JobTitle/text()',
                relocation => './Relocation/text()',
                salary_cur => './Salary[1]/Currency/text()',
                salary_min => './Salary[1]/Min/text()',
                salary_max => './Salary[1]/Max/text()',
                salary_per => './Salary[1]/Type/text()',
                job_types  => [
                    './JobTypes/JobType/text()',
                ],
            },
        ],

        education          => [
            './Educations/Education' => {
                id    => './Level/@ID',
                level => './Level/text()',
            },
        ],

        experience         => [
            './Experiences/Experience' => {
                company_name => './Company/Name/text()',
                job_title    => './Job/Title/text()',
                description  => './Job/Description/text()',
                fromyear    => './DateFrom/@Year',
                frommonth   => './DateFrom/@Month',
                toyear      => './DateTo/@Year',
                tomonth     => './DateTo/@Month',
                years       => './Years/text()',
            },
        ],

        work_authorisation => [
            './WorkAuths/WorkAuth' => {
                country => './Country/text()',
                auth_type => './AuthType/@ID',
            },
        ],

        skill_list => [
            './Skills/Skill' => {
                matches => './Matches/Label',
                name => './Name',
                lastused => './LastUsed',
                yrsused => './YrsUsed',
            }
        ],

        boards             => [
            './Boards/Board' => {
                id   => './@ID',
                name => './text()',
            },
        ],
    );
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    # format is YYYY-MM-DDTHH:MM:SS
    my $dt_parser = DateTime::Format::Strptime->new(
        pattern => '%FT%T',
    );

    my $raw_date_created  = $candidate->attr('raw_date_created');
    my $raw_date_modified = $candidate->attr('raw_date_modified');

    my $date_created = $dt_parser->parse_datetime( $raw_date_created );
    my $date_modified = $dt_parser->parse_datetime( $raw_date_modified );

    # some candidates do not have a date created
    if ( !$date_created && $date_modified ) {
        $date_created = $date_modified;
    }

    # Requested by board
    if ( !$candidate->attr('firstname') && !$candidate->attr('lastname') ) {
        $candidate->attr( 'name' => 'CONFIDENTIAL');
    }

    $candidate->attributes(
        date_created       => $date_created  ? $date_created->epoch() : undef,
        cv_last_updated    => $date_modified ? $date_modified->epoch() : undef,
    );

    my $relevance = $candidate->attr('relevance');
    $relevance = sprintf("%.1f", ( $relevance * 10 ) );
    $candidate->attr( 'relevance' => $relevance ); # Display relevance out of 10, and to 1 decimal place as on site direct

    my $experiences_aref = $candidate->attr('experience');
    my $headline;
    if ( $experiences_aref ) {
        $headline = $experiences_aref->[0]->{job_title};
    }

    my $targets_aref = $candidate->attr('target');
    if ( $targets_aref ) {
        my $salary_cur = $targets_aref->[0]->{salary_cur};
        my $salary_min = $targets_aref->[0]->{salary_min};
        my $salary_max = $targets_aref->[0]->{salary_max};
        my $salary_per = $targets_aref->[0]->{salary_per};

        if ( $salary_min || $salary_max ) {
            $candidate->attr('salary' => $self->build_salary( $salary_cur, $salary_min, $salary_max, $salary_per ) );
        }

        if ( !$headline ) {
            $headline = $targets_aref->[0]->{job_title};
        }
    }
    if ( $headline ) {
        $candidate->attr('headline' => $headline);
    }


    my @skills = sort { $b->{yrsused} <=> $a->{yrsused} } @{$candidate->attr('skill_list')};

    if (@skills) {
        @skills = @skills [0..9];
        $_->{'lastused'} = $candidate->pattern_to_epoch( '%FT%T' , $_->{'lastused'} ) foreach @skills;
        $candidate->attr( 'skills' => @skills);
    }
    $self->monsterpowersearch_standardise_work_authorisation( $candidate );

    if ( !$candidate->candidate_id() ) {
        $self->report_error('Monster have changed the XML again');
        my $candidate_id = $candidate->attr('candidate_value');
        if ( $candidate_id ) {
            $candidate->candidate_id( $candidate_id );
        }
    }

    $candidate->profile_chargeable(1);

    return $candidate;
}

sub monsterpowersearch_standardise_work_authorisation {
    my $self = shift;
    my $candidate = shift;

    my $work_authorisation_aref = $candidate->attr('work_authorisation');

    if ( !$work_authorisation_aref ) {
        return;
    }

    my (@authorised, @not_authorised);
    foreach my $auth_ref ( @$work_authorisation_aref ) {
        if ( ref( $auth_ref ) ) {
            if ( $auth_ref->{auth_type} eq '1' ) {
                push @authorised, $auth_ref->{country};
            }
            elsif ( $auth_ref->{auth_type} eq 'Not Authorized' ) {
                push @not_authorised, $auth_ref->{country};
            }
        }
    }

    $candidate->attr('authorised_to_work' => \@authorised );
    $candidate->attr('not_authorised_to_work' => \@not_authorised );

    return 1;
}


sub download_profile_url {
    return 'https://gateway.monster.com:8443/bgwBroker'; #Live
}



sub download_cv_url {
    return 'https://gateway.monster.com:8443/bgwBroker'; #Live
    #return 'https://208.71.198.74:8443/bgwBroker'; #Test
}

sub download_cv_failed       { return qr/<ReturnCode returnCodeType="failure">/; }
sub download_cv_success      { return qr/<ReturnCode returnCodeType="success">|<QueriesResponse /; }
sub download_profile_failed  { return qr/<ReturnCode returnCodeType="failure">/; }
sub download_profile_success { return qr/<ReturnCode returnCodeType="success">|<QueriesResponse /; }

sub download_profile_xml {
    return download_candidate_xml(@_);
}

sub download_cv_xml {
    return download_candidate_xml(@_);
}

sub download_candidate_xml {
    my $self = shift;
    my $candidate = shift;

    my $xml = Bean::XML->new( ENCODING => 'Windows-1252' );

    my $message_id = $candidate->attr( 'message_id' );
    unless ( $message_id )  {
        $message_id = join('-', time(), $self->company(), $candidate->candidate_id() );
        $candidate->attr( 'message_id' => $message_id );
    }

    $xml->startTag('SOAP-ENV:Envelope', 'xmlns:SOAP-ENV' => 'http://schemas.xmlsoap.org/soap/envelope/');
        $xml->startTag('SOAP-ENV:Header');
            $xml->startTag('mh:MonsterHeader', 'xmlns:mh' => 'http://schemas.monster.com/MonsterHeader');
                $xml->startTag('mh:MessageData');
                    $xml->startTag('mh:MessageId');
                        $xml->characters( $message_id );
                    $xml->endTag('mh:MessageId');
                    $xml->startTag('mh:Timestamp');
                        $xml->characters('2003-07-23T10:48:43Z');
                    $xml->endTag('mh:Timestamp');
                $xml->endTag('mh:MessageData');
            $xml->endTag('mh:MonsterHeader');

            $xml->startTag('cat:CompanyAuthHeader', 'xmlns:cat' => 'http://webservices.monster.com/MonsterPortal');
                $xml->startTag('cat:CompanyAccessTicket');
                    $xml->characters( $self->token_value( 'cat' ) );
                $xml->endTag('cat:CompanyAccessTicket');
            $xml->endTag('cat:CompanyAuthHeader');
        $xml->endTag('SOAP-ENV:Header');
        $xml->startTag('SOAP-ENV:Body');
# body
            $xml->startTag('Query', 'xmlns' => 'http://schemas.monster.com/Monster', 'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance' );
                $xml->startTag('Target');
                    $xml->characters('JobSeekers');
                $xml->endTag('Target');

                $xml->startTag('ReturnRestriction');
                    # we want the resume text not the parsed xml from monster
                    $xml->startTag('ResumeRestriction');
                        $xml->startTag('StoreRenderedTextResume');
                            $xml->characters(0); # send 1 to get the text CV. send 0 to get the HTML CV according to Gillian
                        $xml->endTag('StoreRenderedTextResume');
                    $xml->endTag('ReturnRestriction');
                $xml->endTag('ResumeRestriction');
                $xml->startTag('SelectBy');
                    $xml->startTag('Criteria');
                        $xml->characters('TextResumeId');
                    $xml->endTag('Criteria');
                    $xml->startTag('Value');
                        $xml->characters( $candidate->candidate_id() );
                    $xml->endTag('Value');
                    $xml->startTag('Criteria');
                        $xml->characters('ChannelAlias');
                    $xml->endTag('Criteria');
                    $xml->startTag('Value');
                        $xml->characters( 'mons' );
                    $xml->endTag('Value');
                $xml->endTag('SelectBy');
            $xml->endTag('Query');
        $xml->endTag('SOAP-ENV:Body');
    $xml->endTag('SOAP-ENV:Envelope');
    $xml->end();
    return $xml;
}

sub do_download_cv {
    my $self = shift;
    my $candidate = shift;

    my $response = $self->download_cv_submit( $candidate );

    $self->log_debug('Checking for errors');

    $self->check_for_success_or_failure(
        $self->download_cv_success() || undef,
        $self->download_cv_failed()  || undef,
    );

    # scrape xml for candidates
    $self->scrape_generic_download_profile($candidate);
    return $self->scrape_download_cv( $candidate );
}

sub download_profile {
    my $self = shift;
    my $candidate = shift;
    $self->login() if $self->download_profile_requires_login();
    $self->download_profile_submit( $candidate );
    $self->log_debug('Checking for errors');

    $self->check_for_success_or_failure(
        $self->download_profile_success() || undef,
        $self->download_profile_failed()  || undef,
    );
    $self->scrape_download_cv ($candidate);
    # Scrape for downloaded CV.

    # scrape xml for candidates
    return $self->scrape_generic_download_profile( $candidate );
}

sub scrape_download_cv {
    my $self = shift;
    my $candidate = shift;

    $self->log_info('Scraping Monster XML');

    $self->registerNs( 'ADC' => 'http://schemas.monster.com/Monster');

    if ( $self->findvalue('//ReturnCode[@returnCodeType="failure"]') ) {
        # safety check: we should never get here
        # because the download_cv_failed() should pick this up
        $self->log_error('Monster returned an error downloading this CV.');
        die "Unable to download CV: Monster returned a failure";
    }

    if ( !$candidate->email() ) {
        $self->monsterpowersearch_update_profile( $candidate );
    }

    #Firstly, try and get it from the ResumeDocument node
    my ($resume_content, $resume_filename, $resume_content_type) = $self->monsterpowersearch_cv_from_resume();

    if ($resume_content && $resume_filename && $resume_content_type){
        require MIME::Base64;
        my $resume_content_decoded = MIME::Base64::decode_base64( $resume_content );
        my $response_attachment = $self->to_response_attachment({
            decoded_content => $resume_content_decoded,
            filename        => $resume_filename,
            content_type    => $resume_content_type,
        });

        return $response_attachment;
    }

    #Secondly, try and get from the text resume
    my $raw_text = $self->monsterpowersearch_cv_from_monster();
    if ( !$raw_text ) { #Thirdly, try to build it ourselves.
        $raw_text = $self->monsterpowersearch_build_cv_from_profile();
    }

    return $self->monsterpowersearch_save_content( $candidate, $raw_text );
}



sub monsterpowersearch_update_profile {
    my $self = shift;
    my $candidate = shift;

    $self->log_debug('This candidate does not have an email address so trying to scrape it');
    my $email = $self->findvalue('//ADC:PersonalData[1]/ADC:Contact/ADC:E-mail');
    if ( $email ) {
        $self->log_debug('Found candidate email [%s]', $email);
        $candidate->email( $email );
    }

    return $candidate;
}


sub scrape_download_profile_xpath {
    my $self = shift;
    $self->registerNs( 'ADC' => 'http://schemas.monster.com/Monster');
    return '/SOAP-ENV:Envelope/SOAP-ENV:Body/ADC:QueriesResponse/ADC:JobSeekers/ADC:JobSeeker'; 
}

sub scrape_download_profile_details {
    my $self = shift;
    my $candidate = shift;
    return (
    email                     => './ADC:PersonalData/ADC:Contact/ADC:E-mail/text()',
    disabled                  => './ADC:PersonalData/ADC:DemographicDetail/ADC:Disability/@status',
    victimofterrorism         => './ADC:PersonalData/ADC:DemographicDetail/ADC:VictimOfTerrorism/@status',
    exconvict                 => './ADC:PersonalData/ADC:DemographicDetail/ADC:ExConvict/@status',
    workatweekend             => './ADC:Resumes/ADC:Resume/ADC:WorkPreferences/ADC:Weekend/text()',
    workovertime              => './ADC:Resumes/ADC:Resume/ADC:WorkPreferences/ADC:Overtime/text()',
    availabletostart          => './ADC:Profile/ADC:Availability/ADC:AvailableTimeStart/text()',
    highest_education_degree  => './ADC:Profile/ADC:HighestEducationDegree/text()',
    target_job_title          => './ADC:Profile/ADC:TargetJobTitle/ADC:Title/text()',
    career_status             => './ADC:Profile/ADC:CareerStatus/text()',
    career_level              => './ADC:Profile/ADC:CareerLevel',
    willing_to_travel_id      => './ADC:Resumes/ADC:Resume/ADC:WillingtoTravel/@monsterId',
    willing_to_relocate       => './ADC:Resumes/ADC:Resumes/ADC:DetailedSearchExtras/ADC:WillingToRelocate',
    contact_preference        => './ADC:Profile/ADC:ContactPreference/text()',

    geographic_prefs          => [
        './ADC:Resumes/ADC:Resume/ADC:DetailedSearchExtras/ADC:GeographicPrefs/ADC:GeographicPref' => {
            'city'          => './ADC:City',
            'state'         => './ADC:State',
            'country_code'  => './ADC:CountryCode',
            'continent'     => './ADC:Continent',
        }
    ],

    employment_references => [
        './ADC:Resumes/ADC:Resume/ADC:EmploymentReferences/ADC:Reference' => {
            'position_title' => './ADC:PositionTitle',
            'contact_name'   => './ADC:Contact/ADC:Name',
            'company_name'   => './ADC:Contact/ADC:CompanyName',
            'email'          => './ADC:Contact/ADC:E-mail',
            'phones'         => [
                './ADC:Contact/ADC:Phones/ADC:Phone' => {
                    phonetype => './@phoneType',
                    number => './text()',
                }
            ],
        }
    ],

    experience         => [
        './ADC:Resumes/ADC:Resume/ADC:EmploymentHistory/ADC:Position' => {
                company_name => './ADC:EmployerName/text()',
                job_title    => './ADC:PositionTitle/text()',
                description  => './ADC:ExperienceDescription/text()',
                fromyear    => './ADC:DateRange/ADC:StartDate/ADC:Year/text()',
                frommonth   =>'./ADC:DateRange/ADC:StartDate/ADC:Month/text()',
                toyear      =>'./ADC:DateRange/ADC:EndDate/ADC:Year/text()',
                tomonth     =>'./ADC:DateRange/ADC:EndDate/ADC:Month/text()',
            },
        ],

    education_history => [
        './ADC:Resumes/ADC:Resume/ADC:EducationalHistory/ADC:SchoolOrInstitution' => {
            school_name => './ADC:SchoolName/text()',
            toyear => './ADC:DateRange/ADC:EndDate/ADC:Year/text()',
            tomonth => './ADC:DateRange/ADC:EndDate/ADC:Month/text()',
            summary => './ADC:EducationSummary/text()',
            degree  => './ADC:EducationDegree/text()',
            fields_of_study => [
                './ADC:EducationFieldsOfStudy/ADC:EducationFieldOfStudy' => {
                    'field_of_study' => './ADC:FieldOfStudyName',
                },
            ],
        },
    ],

    skills => [
        './ADC:Skills/ADC:Skill' => {
            skillname => './ADC:SkillName/text()',
            skillused => './ADC:SkillUsed/text()',
        }
    ],

    affiliations => [
        './ADC:Affiliations/ADC:Affiliation' => {
            name => './ADC:AssociationName/text()',
            role => './ADC:Role/text()',
            from => './ADC:DateRange/ADC:StartDate/ADC:Date/text()',
        }
    ],

    compensation_type => './ADC:DesiredCompensation/CompensationType/text()',
    compensation_currency => './ADC:DesiredCompensation/Currency/text()',
    compensation_rate => './ADC:DesiredCompensation/Rate/text()',
    compensation_ratemin => './ADC:DesiredCompensation/RateMin/text()',
    compensation_ratemax => './ADC:DesiredCompensation/RateMax/text()',

    ideal_job_description => './ADC:Resumes/ADC:Resume/ADC:DesiredEmployer/ADC:IdealJobDescription',


    employer_categories => [
        './ADC:Resumes/ADC:Resume/ADC:DesiredEmployer/ADC:TargetEmployerCategories' => {
            'category' => './ADC:TargetEmployerCategory',
        }
    ],

    employer_occupations => [
        './ADC:Resumes/ADC:Resume/ADC:DesiredEmployer/ADC:TargetEmployerOccupations' => {
            'occupation' => './ADC:TargetEmployerOccupation',
        }
    ],

    job_industries => [
        './ADC:Resumes/ADC:Resume/ADC:TargetJobIndustries' => {
            'industry' => './ADC:Industry/ADC:IndustryName'
        }
    ],

    job_titles => [
         './ADC:Resumes/ADC:Resume/ADC:TargetJobTitles/ADC:TargetJobTitle' => {
            'targetjobtype' => './@type',
            'title' => './ADC:Title/text()',
        }
    ],

    languages => [
        './ADC:Profile/ADC:Languages' => {
            'languagename' => './ADC:LanguageName/text()',
            'languageproficiency' => './ADC:LanguageProficiency/text()',
        }
    ],

    phones => [
        './ADC:PersonalData/ADC:Contact/ADC:Phones' => {
            phonetype => './ADC:Phone/@phoneType',
            priority => './ADC:Phone/@priority',
            number => './ADC:Phone/text()',
        }
    ],

    html_cv =>  './/ADC:TextResume/text()',
    );
}

sub scrape_fixup_candidate_profile {
    my $self = shift;
    my $candidate = shift;
    my $willing_to_travel_id = $candidate->attr("willing_to_travel_id");

    my %willing_to_travel_lookup = {
        1 => 'No travel required',
        2 => 'Up to 25% travel',
        3 => 'Up to 50% travel',
        4 => 'Up to 75% travel',
        5 => 'Up to 100% travel',
    };

    $candidate->attr("willing_to_travel" => $willing_to_travel_lookup{$willing_to_travel_id});
#    my $cv_preview = $candidate->attr('text_resumes');
}

sub monsterpowersearch_cv_from_resume {
    my $self = shift;
    my $content = $self->findvalue('//ADC:ResumeDocument[1]/ADC:File');
    my $filename = $self->findvalue('//ADC:ResumeDocument[1]/ADC:FileName');
    my $content_type = $self->findvalue('//ADC:ResumeDocument[1]/ADC:FileContentType');

    if ($content && $filename && $content_type){
        return ($content, $filename, $content_type);
    }
}

sub monsterpowersearch_cv_from_monster {
    my $self = shift;
    return $self->findvalue('//ADC:Resume[1]/ADC:TextResume/text()');
}

sub monsterpowersearch_build_cv_from_profile {
    my $self = shift;

    # inherited from Stream/Engines/XML.pm
    return $self->apply_xslt( 'monstercv.xsl' );
}

sub _convert_to_windows_new_lines {
    my $raw_text = shift;

    my $CRLF = "\015\012";
    my $CR   = "\015";
    my $LF   = "\012";
    if ( $raw_text =~ m/$CRLF/ ) {
        # nothing to do
        return $raw_text;
    }

    if ( $raw_text =~ m/$LF/ ) {
        $raw_text =~ s/$LF/$CRLF/g;
        return $raw_text;
    }

    if ( $raw_text =~ m/$CR/ ) {
        $raw_text =~ s/$CR/$CRLF/g;
        return $raw_text;
    }

    return $raw_text;
}

sub monsterpowersearch_save_content {
    my $self = shift;
    my $candidate = shift;
    my $raw_text = shift;

    my $candidate_id = $candidate->candidate_id();

    my $content_type;
    my $filename = 'monster_'. $candidate_id . '.';
    if ( Bean::HTMLStrip->looks_like_html( $raw_text ) ) {
        $filename .= 'html';
        $content_type = 'text/html; charset=UTF-8';
    }
    else {
        $filename .= 'txt';
        $content_type = 'text/plain; charset=UTF-8';

        # convert unix new lines to windows new lines (otherwise the text cv looks rubbish in windows)
        $raw_text = _convert_to_windows_new_lines( $raw_text );
    }

    my $response_attachment = $self->to_response_attachment({
        decoded_content => $raw_text,
        filename        => $filename,
        content_type    => $content_type,
    });

    return $response_attachment;
}

sub search_number_of_results_xpath {
        return '/Monster/Resumes/@Found';
}

sub scrape_paginator {
    my $self = shift;

    my $logger = $self->logger();

    my $total_results = $self->total_results();
    my $results_per_page = $self->findvalue('/Monster/Resumes/@Returned');

    $logger->debug('Found %d candidates, fetched %d of them', $total_results, $results_per_page );

    if ( $total_results <= $results_per_page ) {
        $logger->debug('There is only 1 page of results');
        $self->max_pages( 1 );
    }
    else {
        my $total_pages = POSIX::ceil( $total_results / $results_per_page );
        $logger->debug('There are %d pages', $total_pages);
        $self->max_pages( $total_pages );
    }
}

sub monster_catch_missing_credentials {
    my $self = shift;
    my $cat     = $self->token_value('cat');

    if ( !$cat ) {
        return $self->throw_login_error('Missing Monster CAT code - ask Monster for your CAT code and try again');
    }
    return;
}

sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    if ( !defined( $content ) ) {
        $content = $self->content();

        return unless $content;
    }

    # monster is ip restricted
    if ( $content =~ m/HTTP Error 403.6/ || $message =~ m/HTTP Error 403.6/ ) {
        return $self->throw_known_internal('IP Restricted');
    }

    if ( $content =~ m/Cannot find lat\/lon for geoid for radius search/ ) {
        return $self->throw_known_internal('Monster did not recognise the postcode selected');
    }

    if ( $content =~ m/Missing CAT/ ) {
        return $self->throw_login_error('Missing CAT');
    }

    if ( $content =~ m/Invalid length for a Base\-64 char array/ ) {
        return $self->throw_login_error('Your Monster CAT is incorrect, it looks too short');
    }

    if ( $content =~ m/Inactive CAT/ ) {
        return $self->throw_inactive_account('Inactive CAT');
    }

    if ( $content =~ m/Resume search license not found/ ) {
        return $self->throw_login_error('No Monster resume search license');
    }

    if ( $content =~ m/Client not authenticated after processing available headers/ ) {
        return $self->throw_login_error('Client not authenticated after processing available headers. Contact Monster and ask them to set the permissions on the cat code correctly');
    }

    if ( $content =~ m/(Invalid or broken AuthToken|CAT processing error|Search license not found|CAT authentication failure|Company Access Ticket is invalid)/ ) {
        return $self->throw_login_error( $1 );
    }

    if ( $content =~ m{(No active RESUME_VIEW license was found for .*)</Message>} ) {
        return $self->throw_no_cvsearch( $1 );
    }

    if ( $content =~ m/(No ResumeView license for \S+)/ ) {
        return $self->throw_login_error( $1 );
    }
    if ( $content =~ m{(the page you are trying to reach is temporarily unavailable)} ) {
        # http://www.adcourier.com/manage/view-log.cgi?key=755344&destination=
        return $self->throw_unavailable( $1 );
    }

    if ( $content =~ m{(TextResumeID \[.*\] is invalid)} ) {
        # candidate no longer exists on monster
        return $self->throw_cv_unavailable( $1 );
    }

    if ( $content =~ m{(JobSeeker didnt not have any active resumes)} ) {
        # candidate does not have a CV #38801 simon updating error message
        return $self->throw_no_cv('JobSeeker does not have any active resumes or you have tried to view a JobSeeker that is outside your account license');
    }

    # Other error messages, not sure what this guy means
    # Error in search. Status: -2
    # e.g. http://blade7.adcourier.com/manage/view-log.cgi?key=270515&destination=

    if ( $content =~ m{<Message>(.+?)</Message></Error>} ) {
        return $1;
    }

    if ( $content =~ m{Processing error} ) {
        return $self->throw_critical(qq{Processing error. Possible causes: invalid boolean keywords, postcode not supported by Monster, invalid location radius, invalid Monster CAT code});
    }

    return undef;
}

1;

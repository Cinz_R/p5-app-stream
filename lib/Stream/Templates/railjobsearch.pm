package Stream::Templates::railjobsearch;
use base qw(Stream::Templates::OilAndGasPlatform);

use strict;
use warnings;

__PACKAGE__->inherit_tokens();

sub base_url {
    return 'https://www.railjobsearch.com';
}

1;

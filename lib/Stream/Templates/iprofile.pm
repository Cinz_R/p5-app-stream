#!/usr/bin/perl

# vim:expandtab:tabstop=4

# $Id: jobsie.pm 30489 2010-02-24 08:27:34Z andy $

package Stream::Templates::iprofile;

=head1 NAME

Stream::Templates::iprofile - iProfile search API

=head1 CONTACT

Best contact: michael.jefferys@iprofileuk.com

Tom (Thomas.Newman@iprofile.org) - Test Team Lead

(New contact will be Graham when he starts)

=head1 TESTING INFORMATION

Username: andy
Password: jones

Security Key: a03afe74-2163-474d-8c93-f98e307f17c9

New credentials from Tom:

Username: 1018363.support@iprofile.org
Password: Password#1

(ExternalUserId should contain the membership key)

=head1 DOCUMENTATION

=head1 OVERVIEW

Bog standard SOAP webservice using .NET.

We are using mock APIs rather than real thing. The search xml is actually static (they
validate our search query and then return the same xml file each time).

Only candidates 5746822, 5746825, 5746836,5746836, 5746859, 5746860, 5746862, 5746864,
5746892, 5746897, 5746902, 5746903, 5746907, 5746909, 5746912 are valid in the mock service.

There is a test harness available at http://iprofile.org:8087. Select the "Mock Service"
from the Service Type dropdown.

** UPDATE ** - the test harness is at: http://mockpubliccandidategateway.iprofile.org/TestHarness/default.aspx

HR-XML version 2.4 works.

=head1 CHANGES

=over 4

=item * 21/03/2011 - ACJ - New authtoken for recruiter's email

=item * 03/03/2010 - ACJ - Started feed

=head1 BUG FIXES

none yet

=cut

use strict;
use boolean;
use base qw(Stream::Engine::API::XML::SOAP);

use Stream::Constants::SortMetrics ':all';

use DateTime;
use DateTime::Format::Strptime;
use MIME::Base64;
use Carp;


################ TOKENS #########################
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens %global_authtokens);

BEGIN {
    $standard_tokens{jobtitle} = {
        Label   => 'Job Title',
        Type    => 'Text',
        Size    => 60,
        SortMetric => $SM_KEYWORDS,
    };

    $standard_tokens{employer} = {
        Label      => 'Employer',
        Type       => 'Text',
        Size       => 60,
        SortMetric => $SM_KEYWORDS,
    };

    # this searches the time range within which the person has had the job title or the employer on their CV
    $standard_tokens{recency_count} = {
        Label      => ' - Search Within',
        Type       => 'List',
        Options    => '(Search within last 1 job=withinlast1job|Search within last 2 jobs=withinlast2jobs|Search within last 3 jobs=withinlast3jobs)',
        Default    => 'withinlast1job',
        SortMetric => $SM_KEYWORDS + 1,
    };

    $derived_tokens{work_history_essential} = {
        Type     => 'List',
        Options  => 'True=true|False=false',
        Helper   => 'iprofile_filter_is_essential(%exact_matches_only%)',
    };

    $derived_tokens{cv_text_essential} = {
        Type     => 'List',
        Options  => 'True=true|False=false',
        Helper   => 'iprofile_filter_is_essential(%exact_matches_only%)',
    };

    # this token will be removed when the job type token goes live
    $standard_tokens{employment_type} = {
        Label      => 'Job Type',
        Type       => 'List',
        Options    => 'None=None|All=All|Permanent=Permanent|Contract/Temp=ContractTemp',
        Default    => 'None', # don't filter by this
        Deprecated => 'JobType',
        SortMetric => $SM_JOBTYPE,
    };

    $posting_only_tokens{jobtype_id} = {
        # All=All|Permanent=Permanent|Contract/Temp=ContractTemp|None=None
        Helper     => 'HASH_MAP(%default_jobtype%|permanent=Permanent|contract=ContractTemp|temporary=ContractTemp)',
        Default    => 'None',
        HelperMetric => 10,
    };

    $posting_only_tokens{employment_type_id} = {
        Helper     => 'iprofile_jobtype(%default_jobtype%|%jobtype_id%|%employment_type%)',
        HelperMetric => 20,
    };

    $derived_tokens{availability} = {
        Label      => 'Availabililty',
        Type       => 'List',
        Options    => 'Any=0|1 day=1|2 days=2|7 days=7|30 days=30|90 days=90|180 days=180|365 days=365',
        Default    => 0,
        SortMetric => $SM_DATE,
        Helper     => 'iprofile_availability(%cv_updated_within%)',
    };

    $derived_tokens{'salary_per'} = {
        Type         => 'SalaryPer',
        Options      => 'annum|day',
        Helper       => 'SALARY_PER(salary_per|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric   => $SM_SALARY,
        Default      => 'annum',
    };

    $posting_only_tokens{annual_salary_lower} = {
        Helper       => 'iprofile_annual_salary(%salary_per%|%salary_from%)',
    };

    $posting_only_tokens{annual_salary} = {
        Helper       => 'iprofile_annual_salary(%salary_per%|%salary_to%)',
    };

    $posting_only_tokens{daily_rate_lower} = {
        Helper       => 'iprofile_daily_salary(%salary_per%|%salary_from%)',
    };

    $posting_only_tokens{daily_rate} = {
        Helper       => 'iprofile_daily_salary(%salary_per%|%salary_to%)',
    };

    $derived_tokens{filter_essential} = {
        Type     => 'List',
        Options  => 'True=true|False=false',
        Helper   => 'iprofile_filter_is_essential(%exact_matches_only%)',
    };

    $standard_tokens{candidate_name} = {
        Label    => ' - Candidate Name',
        Type     => 'Text',
        Size     => 60,
    };

    # location tokens
    $posting_only_tokens{latitude} = {
        Helper => 'iprofile_latitude(%location_id%)',
    };

    $posting_only_tokens{longitude} = {
        Helper => 'iprofile_longitude(%location_id%)',
    };

    $posting_only_tokens{country_iso_code} = {
        Helper => 'LOCATION_MAPPING(ISO_COUNTRIES|%location_id%|%location_within%)',
    };

    $standard_tokens{'max_results'} = {
        Label => ' - Number of Results',
        Type  => 'List',
        Values => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
        Default => 50,
        SortMetric => $SM_SEARCH,
    };

    $standard_tokens{exact_matches_only} = {
        Label => ' - Exact Matches Only',
        Type  => 'List',
        Values => '(Yes=yes|No=no)',
        Default => 'yes',
        SortMetric => $SM_SEARCH,
    };

    # Authtokens
    $authtokens{'iprofile_username'} = {
        Label      => 'Username', # Usually the Agency email address
        Type       => 'Text',
        Size       => 20,
        SortMetric => 1,
    };

    $authtokens{'iprofile_password'} = {
        Label      => 'Password', # Usually the Agency password
        Type       => 'Text',
        Size       => 20,
        SortMetric => 2,
    };

    $authtokens{'iprofile_email'} = {
        Label      => 'Recruiter Email', # Must be the Recruiter's email address
        Type       => 'Text',
        Size       => 20,
        SortMetric => 3,
    };

# They are abandoning the membership key
#    $global_authtokens{'iprofile_membership_key'} = {
#        Label      => 'Membership Key',
#        Type       => 'Text',
#        Size       => 60,
#        Mandatory  => 0, # Not required but may be in the future
#        SortMetric => 1,
#    };

    $global_authtokens{'iprofile_security_key'} = {
        Label      => 'Security Key', # Security key gives us access
        Type       => 'Text',
        Size       => 60,
        SortMetric => 2,
    };
};

sub iprofile_availability {
    my $self = shift;
    my $cv_updated_within = shift;

    my $n_days = $self->CV_UPDATED_WITHIN_TO_DAYS( $cv_updated_within );

    if ( $cv_updated_within eq 'TODAY' ){
        return 1;#becuase thats as close as we are going to get otherwise we default 0
    }

    if ( !$n_days ) {
        return 0; # anytime
    }

    if ( $n_days <= 1 || $cv_updated_within eq '1D' ) {
        return 1;
    }

    if ( $n_days <= 2 || $cv_updated_within eq '2D' ) {
        return 2;
    }

    if ( $n_days <= 7 || $cv_updated_within eq '1W' ) {
        return 7;
    }

    if ( $n_days <= 30 || $cv_updated_within eq '1M' ) {
        return 30;
    }

    if ( $n_days <= 90 || $cv_updated_within eq '3M' ) {
        return 90;
    }

    if ( $n_days <= 180 || $cv_updated_within eq '6M' ) {
        return 180;
    }

    if ( $n_days <= 365 || $cv_updated_within eq '1Y' ) {
        return 365;
    }

    # fallback to any time
    return 0;
}

sub iprofile_latitude {
    my $self = shift;
    my $location_id = shift
        or return;

    my $location = $self->location_api()->find({
        id => $location_id,
    });

    if ( !$location ) {
        $self->log_warn('Unable to fetch latitude for location [%s]', $location_id );
        return;
    }

    if ( $location->is_place() || $location->is_area() ) {
        $self->log_debug('This location is an area or place so should have a lat/long');
        return $location->latitude();
    }

    $self->log_info('This location is a country or continent so too big to send a lat/long');
    return;
}

sub iprofile_longitude {
    my $self = shift;
    my $location_id = shift
        or return;

    my $location = $self->location_api()->find({
        id => $location_id,
    });
    if ( !$location ) {
        $self->log_warn('Unable to fetch longitude for location [%s]', $location_id );
        return;
    }

    if ( $location->is_place() || $location->is_area() ) {
        $self->log_debug('This location is an area or place so should have a lat/long');
        return $location->longitude();
    }

    $self->log_info('This location is a country or continent so too big to send a lat/long');
    return;
}

sub iprofile_annual_salary {
    my $self = shift;
    my $salary_per = shift;
    my $salary_rate = shift;

    if ( $salary_per eq 'annum' ) {
        return $salary_rate;
    }

    return;
}

sub iprofile_daily_salary {
    my $self = shift;
    my $salary_per = shift;
    my $salary_rate = shift;

    if ( $salary_per eq 'day' ) {
        return $salary_rate;
    }

    return;
}

sub iprofile_jobtype {
    my $self = shift;
    my $job_type = shift;
    my $job_type_id = shift;
    my $employment_type = shift;

    if ( $job_type ) {
        return $job_type_id;
    }

    return $employment_type;
}

sub iprofile_filter_is_essential {
    my $self = shift;
    my $exact_matches_only = shift;

    if ( !$exact_matches_only ) {
        $exact_matches_only = $self->token_default('exact_matches_only');
    }

    if ( $exact_matches_only eq 'yes' ) {
        return 'true';
    }
    return 'false';
}

############### HELPERS ##########################

sub search_url {
    return 'http://searchapi.iprofileuk.com/IProfileInternalSearchApi.asmx';
}
############### LOGIN ############################
############### SEARCH PAGE #######################

sub iprofile_search_gateway_header {
    my $self = shift;
    my $xml  = shift;

    $xml->startTag('ServiceAuthHeader',
        xmlns => 'http://www.iprofile.org/internal/',
    );
        $xml->dataElement('Username'       => $self->token_value('username') );
        $xml->dataElement('Password'       => $self->token_value('password') );
        $xml->dataElement('RecruiterEmail' => $self->token_value('email') );
    $xml->endTag('ServiceAuthHeader');

    return $xml;
}

sub iprofile_candidate_gateway_header {
    my $self = shift;
    my $xml  = shift;

    $xml->startTag('ServiceAuthHeader',
        xmlns => 'http://www.iprofile.org/candidategateway/',
    );
        $xml->dataElement('Username'       => $self->token_value('username') );
        $xml->dataElement('Password'       => $self->token_value('password') );
        $xml->dataElement('RecruiterEmail' => $self->token_value('email') );
    $xml->endTag('ServiceAuthHeader');

    return $xml;
}

sub search_soap_action {
    return 'http://www.iprofile.org/internal/Fetch';
}

sub search_failed {
    return 'TransactionStatus>false<';
}

sub search_build_xml {
    my $self = shift;

    return $self->soap_xml({
        header => \&iprofile_search_gateway_header,
        body   => \&search_build_soap_body_xml,
    });
}

sub search_build_soap_body_xml {
    my $self = shift;
    my $xml = shift;

    $self->{_iprofile_node_index} = 0;

    $xml->startTag('Fetch',
        xmlns => 'http://www.iprofile.org/internal/',
    );
        $xml->startTag('searchQuery');
            my $work_history_filters = 0;
            if ( my $jobtitle = $self->token_value('jobtitle') ) {
                $xml->dataElement('WHJobTitle' => $jobtitle );
                $work_history_filters++;
            }

            if ( my $employer = $self->token_value('employer') ) {
                $xml->dataElement('WHEmployer' => $employer);
                $work_history_filters++;
            }

            if ( my $recency_count = $self->token_value('recency_count') ) {
                $xml->dataElement('WHRecencyCount' => $recency_count);
                $work_history_filters++;
            }

            if ( $work_history_filters ) {
                $xml->dataElement('WHEssential' => $self->token_value_or_default('work_history_essential') );
            }

            $xml->startTag('CVText');
                $xml->characters( $self->token_value('keywords') );
            $xml->endTag('CVText');

            if ( my $cv_text_essential = $self->token_value('cv_text_essential') ) {
                $xml->dataElement('CVTextEssential' => $cv_text_essential);
            }

            my $country_code = $self->token_value('country_iso_code');
            my $lat = $self->token_value('latitude');
            my $long = $self->token_value('longitude');
            if ( $lat || $long || $country_code ) {
                $xml->dataElement('LocationPostCodeEssential' => 'true')
            }

            my $filters_used = 0;
            if ( my $employment_type = $self->token_value('employment_type_id') ) {
                $xml->dataElement('FilterEmploymentType' => $employment_type);
                $filters_used++;
            }

            if ( my $availability = $self->token_value('availability') ) {
                $xml->dataElement('FilterAvailability' => $availability || 0 );
                $filters_used++;
            }

            my $annual_salary_lower  = $self->token_value('annual_salary_lower');
            my $annual_salary_higher = $self->token_value('annual_salary');

            my $daily_salary_lower  = $self->token_value('daily_salary_lower');
            my $daily_salary_higher = $self->token_value('daily_salary');
            if ( $annual_salary_lower || $annual_salary_higher || $daily_salary_lower || $daily_salary_higher ) {
                $filters_used++;
                # removed due to 28052 to prevent search failures
                # - known bug with this XML node not working, not due to be fixed by iprofile until end Jan 2012
                #$xml->startTag('FilterCurrency');
                #    $xml->characters( $self->token_value('salary_cur') );
                #$xml->endTag('FilterCurrency');

                if ( $annual_salary_lower ) {
                    $xml->dataElement('FilterAnnualSalaryLower' => $annual_salary_lower);
                }
                if ( $annual_salary_higher ) {
                    $xml->dataElement('FilterAnnualSalary' => $annual_salary_higher);
                }

                if ( $daily_salary_lower ) {
                    $xml->dataElement('FilterDailyRateLower' => $daily_salary_lower);
                }
                if ( $daily_salary_higher ) {
                    $xml->dataElement('FilterDailyRate' => $daily_salary_higher);
                }
            }

            if ( $filters_used ) {
                $xml->dataElement('FilterEssential' => $self->token_value_or_default('filter_essential') );
            }

            $xml->startTag('PageIndex');
                $xml->characters( $self->iprofile_current_page() );
            $xml->endTag('PageIndex');

            $xml->startTag('PageSize');
                $xml->characters( $self->iprofile_results_per_page() );
            $xml->endTag('PageSize');

            if ( $lat || $long ) {
                my $location_within = $self->token_value('location_within_km') || 0;
                $xml->dataElement('LocationRadial' => $location_within );
            }

            if ( my $candidate_name = $self->token_value('candidate_name') ) {
                $xml->dataElement('FullName' => $candidate_name );
            }

            if ( $lat || $long ) {
                $xml->dataElement('Longitude' => $long || 0);
                $xml->dataElement('Latitude'  => $lat  || 0);
            }

            if ( $country_code ) {
                $xml->dataElement('CountryIsoCode' => $country_code);
            }

#            $xml->dataElement('NoOfSearchResultsItemsRequested' => $self->results_per_page() );
            $xml->dataElement('NoOfSearchResultsItemsRequested' => '' ); #$self->results_per_page() );

=cut

    # TODO: understand what this is doing and see if it is necessary to have in an API

            if ($self->token_value("tags")) {
                require Stream::Integrations::IProfile;
                if (Stream::Integrations::IProfile->credentials($self->username)) {
                    my @tags = $self->list_tags;
                    if ($#tags != -1) {
                        $xml->startTag("Tags");
                        foreach my $tag(@tags) {
                            $xml->startTag("Tag");
                            foreach my $key(keys %$tag) {
                                $xml->dataElement($key => $tag->{$key});
                            }
                            $xml->endTag("Tag");
                        }
                        $xml->endTag("Tags");
                    }
                }

                $xml->dataElement('TagEssential' => 'true');
            }
=cut

        $xml->endTag('searchQuery');
    $xml->endTag('Fetch');

    return $xml;
}

=head2 iprofile paging

    iprofile always returns 500 results regardless of what you ask it so we need to impose our own
    paging to save the front end

=cut

sub iprofile_results_per_page { return 500; }

sub iprofile_current_page {
    my $self = shift;

    my $iprofile_results_per_page = $self->iprofile_results_per_page();
    my $desired_results_per_page = $self->results_per_page();
    my $desired_page = $self->current_page or confess( "current page is required" );

    return int( ( ( $desired_page - 1 ) * $desired_results_per_page ) / $iprofile_results_per_page ) + 1;
}

# The plan is to start parsing from a given offset and limit the parsing to the results_per_page
sub iprofile_node_start {
    my $self = shift;

    my $iprofile_results_per_page = $self->iprofile_results_per_page();
    my $desired_results_per_page = $self->results_per_page();
    my $desired_page = $self->current_page - 1;

    my $result_offset = $desired_page * $desired_results_per_page;

    return $result_offset % $iprofile_results_per_page;
}

# is the current _iprofile_node_index within the range we need?
sub iprofile_result_in_range {
    my $self = shift;
    my $index = shift || $self->{_iprofile_node_index};

    if ( $index < $self->iprofile_node_start ){
        return 0;
    }
    elsif ( $index >= ( $self->iprofile_node_start + $self->results_per_page ) ){
        return 0;
    }

    return 1;
}

# gets called by the scraper for each result in the XML document
sub scrape_generic_result_node {
    my $self = shift;

    my $in_range = $self->iprofile_result_in_range( $self->{_iprofile_node_index} );

    $self->{_iprofile_node_index}++;

    if ( $in_range ) {
        return $self->SUPER::scrape_generic_result_node(@_);
    }

    return 1;
}

sub list_tags {
    my $self     = shift;
    my @keywords = $self->token_value("tags") ? (map { s/^\s*//; s/\s*$//; $_ } split /,/, $self->token_value("tags")) : ( );
    return map { {TagText => $_, Id => 0, CandidateId => 0, IsAgencyTag => "true"} } @keywords;
}

sub namespaces {
    return (
        # prefix => full uri
        Int      => 'http://www.iprofile.org/internal/',
        CGW      => 'http://www.iprofile.org/candidategateway/',
        CGWInt   => 'http://www.iprofile.org/internal', # the candidate gateway internal uri doesn't have the final slash
    );
}


sub scrape_candidate_xpath { return '//Int:SearchResultItem'; }
sub scrape_candidate_details {
    return (
        candidate_id            => './Int:ProfileId/text()',
        firstname               => './Int:FirstName/text()',
        lastname                => './Int:LastName/text()',
        email                   => './Int:EmailAddress/text()',
        telephone               => './Int:PhoneNumber/text()',
        employment_type         => './Int:EmploymentType/text()',
        preferred_rate_amount   => './Int:PreferredRate',
        preferred_rate_cur      => './Int:PreferredRateCUR',
        preferred_salary_amount => './Int:PreferredSalary',
        preferred_salary_cur    => './Int:PreferredSalaryCUR',
        location_text           => './Int:Location',
        town                    => './Int:Town',
        job_title               => './Int:JobTitle',
        employer                => './Int:Employer',
        raw_availability        => './Int:Available',
        score                   => './Int:Score',
        hits                    => './Int:Hits',
        profile_link            => './Int:FullProfileLink',
        match_engine_order      => './Int:MatchEngineOrder',
        total_result_from_elise => './Int:TotalResultFromElise',
        raw_cv_updated_dates      => './Int:CvUpdatedDates',
        raw_iprofile_updated_date => './Int:IProfileUpdatedDate',
    );
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    $self->log_debug('Fixing dates and salary for candidate');

    my $raw_availability = $candidate->attr('raw_availability');
    if ( $raw_availability ) {
        # dd/mm/yyyy
        my $availability_epoch = $candidate->pattern_to_epoch( '%d/%m/%Y' => $raw_availability );
        $self->log_debug('This candidate is available on [%s] (epoch = [%s])', $raw_availability, $availability_epoch );
        $candidate->attr('availability' => $availability_epoch);
    }

    my $raw_last_updated = $candidate->attr('raw_cv_updated_dates');
    if ( $raw_last_updated ) {
        # dd/mm/yyyy
        $candidate->cv_last_updated_from_date( $raw_last_updated );
    }

    # fix up the salary
    my $preferred_salary_cur = $candidate->attr('preferred_salary_cur');
    my $preferred_salary_amount = $candidate->attr('preferred_salary_amount');
    $preferred_salary_amount =~ s/000\.00$/k/;
    if ( $preferred_salary_amount ) {
        my $preferred_salary = $preferred_salary_cur . $preferred_salary_amount;
        $candidate->attr('preferred_salary' => $preferred_salary);
    }

    # fix up the rate
    my $preferred_rate_cur = $candidate->attr('preferred_rate_cur');
    my $preferred_rate_amount = $candidate->attr('preferred_rate_amount');
    $preferred_rate_amount =~ s/000\.00$/k/;

    if ( $preferred_rate_amount ) {
        my $preferred_rate = $preferred_rate_cur . $preferred_rate_amount;
        $candidate->attr('preferred_rate' => $preferred_rate);
    }

    my $employment_type = $candidate->attr('employment_type');
    if ( $employment_type =~ s/\bUndefined\b//g ) {
        $self->log_debug('Removed "Undefined" from employment type');
        $candidate->attr('employment_type' => $employment_type);
    }

    # iprofile return some empty values as '&nbsp;', lets replace that
    foreach my $attr ( qw/job_title employer/ ) {
        my $value = $candidate->attr($attr);
        if ( $value =~ s/&nbsp;/ /g ) {
            $value =~ s/^\s+//; # trim leading whitespace
            $value =~ s/\s+$//; # trim trailing whitespace
            $self->log_debug('Removed "&nbsp;" from %s', $value);
            $candidate->attr($attr => $value);
        }
    }

    # the profile link stops working 5 minutes after the search
    # because iProfile don't want consultants emailing the link around
    # The requirements say the link expires 2 hours after the search but ignore that
    # I've confirmed it with Jason @ iProfile
    # iProfile have increased the timeout to 2 months - 02/08/2010
    #$candidate->profile_link_expires( time + (2*30*24*60*60) );

    # now 24 hours - Confirmed with Michael @ iProfile - 24/02/2011
    $candidate->profile_link_expires( time + (24*60*60) );

    $self->log_debug('Dates and salary for candidate have been fixed');

    return $candidate;
}

sub search_number_of_results_xpath { return '//Int:TotalSearchResultItemsFound'; }

#################### DOWNLOAD CV ##############
sub download_cv_url {
    # live gateway address
    #return q{http://publicgateway.sb.iprofile.org/CandidateGateway.asmx};
    return q{http://publicgateway.iprofileuk.com/CandidateGateway.asmx};
}
sub download_cv_soap_action { return q{http://www.iprofile.org/candidategateway/GetCandidateCVById}; }
sub download_cv_xml {
    my $self = shift;
    my $candidate = shift;

    # this is the format mask for the security key
    my $dummy_security_key = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx';

    return $self->iprofile_download_cv_xml(
        $self->token_value('security_key') || '',
        $candidate->candidate_id()         || '',
    );
}

# separate subroutine so we can hardcode the candidate id for the demo
sub iprofile_download_cv_xml {
    my $self = shift;
    my $security_key = shift;
    my $candidate_id = shift;

    return $self->soap_xml({
        header => \&iprofile_candidate_gateway_header,
        body   => sub {
            my $self = shift;
            my $xml  = shift;

            $xml->startTag('GetCandidateCVById',
                xmlns => q{http://www.iprofile.org/candidategateway/},
            );
                $xml->startTag('request');
                    $xml->dataElement( 'SecurityKey' => $security_key );
                    $xml->dataElement( 'IProfileID'  => $candidate_id );
                    $xml->dataElement( 'CVType'      => 1, ); # 1=Original; other types not defined
                $xml->endTag('request');
            $xml->endTag('GetCandidateCVById');

            return $xml;
        },
    });
}


#sub download_cv_failed {
#    return qr//;
#}

sub scrape_download_cv_xpath   { return '//CGW:CV'; }
sub scrape_download_cv_details {
    return (
        resume_word_base64 => './/CGWInt:Bytes',
        resume_file_ext    => './/CGWInt:FileExtension',
        revision_date      => './/CGWInt:RevisionDate',
    );
}

sub after_scrape_download_cv_success {
    my $self = shift;
    my $candidate = shift;

    # work out a cv filename/content type from the resume_file_ext
    # resume_file_ext have the format "*.doc" etc
    my $extension = $candidate->attr('resume_file_ext');
    my $filename = 'iprofile_' . $candidate->candidate_id() . '.' . $extension;

    # tidy up the filename
    $filename =~ s/\*//g;
    $filename =~ s/\.+/./g;

    $self->log_debug('Built filename [%s] (extension was [%s])', $filename, $extension);
    $candidate->attr('resume_filename' => $filename);

    return $candidate;
}

#sub download_profile_url {
#    return q{http://publicgateway.iprofile.org/CandidateGateway.asmx};
#}
#
#sub download_profile_soap_action {
#    return q{http://www.iprofile.org/candidategateway/GetFormattedCVById};
#}
#
#sub download_profile_xml {
#    my $self = shift;
#    my $candidate = shift;
#
#    return $self->iprofile_download_profile_xml(
#        $self->token_value('security_key') || '',
#        $candidate->candidate_id()         || '',
#    );
#}
#
#sub iprofile_download_profile_xml {
#    my $self = shift;
#    my $security_key = shift;
#    my $candidate_id = shift;
#
#    my $xml = $self->soap_xml({
#        header => \&iprofile_candidate_gateway_header,
#        body   => sub {
#            my $self = shift;
#            my $xml  = shift;
#
#            $xml->startTag('GetFormattedCVById',
#                xmlns => q{http://www.iprofile.org/candidategateway/},
#            );
#                $xml->startTag('request');
#                    $xml->dataElement( 'SecurityKey' => $security_key );
#                    $xml->dataElement( 'IProfileID'  => $candidate_id );
#                $xml->endTag('request');
#            $xml->endTag('GetCandidateCVById');
#
#            return $xml;
#        },
#    });
#
#    return $xml;
#}
#
#sub scrape_download_profile_xpath   { return '//CGW:ByteCV'; }
#sub scrape_download_profile_details {
#    return (
#        profile_as_base64 => './text()',
#    );
#}
#
#sub after_download_profile_scrape_success {
#    my $self = shift;
#    my $candidate = shift;
#
#    my $profile_as_base64 = $candidate->attr( 'profile_as_base64' );
#    if ( $profile_as_base64 ) {
#        $self->log_debug('We have got a base64 encoded iprofile');
#        my $iprofile = MIME::Base64::decode_base64( $profile_as_base64 );
#        $candidate->attr( iprofile => $iprofile );
#
#        $candidate->delete_attr('profile_as_base64');
#        $self->log_debug('Decoded base64 iProfile');
#    }
#    else {
#        $self->log_warn('We do not have a base64 iProfile');
#    }
#
#    return $candidate;
#}

sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    if ( !defined( $content ) ) {
        $content = $self->content();

        return unless $content;
    }

    if ( $content =~ m{(Could not find any recognizable digits)} ) {
        return $self->throw_login_error($1);
    }

    if ( $content =~ m{(Guid should contain 32 digits with 4 dashes)} ) {
        return $self->throw_login_error($1);
    }

    if ( $content =~ m{(Authentication Failed)} ) {
        return $self->throw_login_error($1);
    }

    if ( $content =~ m{(Candidate not available for this key)} ) {
        return $self->throw_cv_unavailable($1);
    }

    if ( $content =~ m{Cannot parse} ) {
        return $self->throw_invalid_boolean_search('Keywords contain invalid boolean query. Please amend and try again');
    }

    return undef;
}

1;
# vim: expandtab shiftwidth=4

package Stream::Templates::jobing;

=head1 Stream::Templates::jobing - Feed for Jobing / Recruitment.com 


=head1 OVERVIEW

Mixes HTML and JSON requests.
We don't have direct access to the board's search API,
so we make some HTML requests to authenticate and acquire keys.

=cut

use base qw(Stream::Engine);

use warnings;
use strict;

use XML::LibXML;
use URI;
use URI::QueryParam;
use URI::Escape;
use DateTime;
use DateTime::Format::ISO8601;
use HTML::Entities;

use List::Util;

use Log::Any qw/$log/;
use WWW::Mechanize;

use HTML::Scrubber;

use Stream::Constants::SortMetrics;;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens %global_authtokens);

%authtokens = (
    jobing_username => {
        Label      => 'Username',
        Type       => 'Text',
        Mandatory  => 1,
        SortMetric => 1
    },
    jobing_password => {
        Label      => 'Password',
        Type       => 'Text',
        Mandatory  => 1,
        SortMetric => 2
    }
);

%standard_tokens = (
    sort_by => {
        Label => 'Sort by',
        Type => 'Sort',
        Options => [
            ['Sort: Best match'            => ',asc'],
            ['Time: recent activity first' => 'last_updated_date,desc'],
            ['Salary: lowest first'        => 'max_salary,asc'],
            ['Salary: highest first'       => 'max_salary,desc'],
            ['Career Level: lowest first'  => 'career_level_id,asc'],
            ['Career Level: highest first' => 'career_level_id,desc'],
            ['Education: lowest first'     => 'education_level_id,asc'],
            ['Education: highest first'    => 'education_level_id,desc'],
        ],
        Default => ',asc'
    },
    min_education_level => {
        Label => 'Minimum Education Level',
        Type => 'List',
        Options => [
            ['All levels'             => ''],
            ['High School/equivalent' => 'High School/equivalent'],
            ['Certificate'            => 'Certificate'],
            ['Vocational'             => 'Vocational'],
            ['Some College Courses'   => 'Some College Courses'],
            ['Associate\'s Degree'    => 'Associate-s Degree'],
            ['Bachelor\'s Degree'     => 'Bachelor-s Degree'],
            ['Master\'s Degree'       => 'Master-s Degree'],
            ['Doctorate Degree'       => 'Doctorate Degree'],
        ]
    },
    min_career_level => {
        Label => 'Minimum Career Level',
        Type => 'List',
        Options => [
            ['All levels' => ''],
            ['Student' => 'Student'],
            ['Internship' => 'Internship'],
            ['Entry Level (less than 2 yrs)' => 'Entry Level (less than 2 yrs)'],
            ['Mid Career (2+ yrs)' => 'Mid Career (2+ yrs)'],
            ['Senior/Team Lead (5+ yrs)' => 'Senior/Team Lead (5+ yrs)'],
            ['Management (Director of Staff)' => 'Management (Director of Staff)'],
            ['Executive (SVP, EVP, VP)' => 'Executive (SVP, EVP, VP)'],
        ]
    },
    category => {
        Label => 'Sector',
        Type => 'List',
        Options => [
            ['All categories' => ''],
            ['Accounting/Auditing' => 'Accounting/Auditing'],
            ['Administrative/Clerical' => 'Administrative/Clerical'],
            ['Advertising/Marketing/PR' => 'Advertising/Marketing/PR'],
            ['Art/Creative/Design' => 'Art/Creative/Design'],
            ['Automotive/Motor Vehicle/Parts' => 'Automotive/Motor Vehicle/Parts'],
            ['Aviation/Aerospace' => 'Aviation/Aerospace'],
            ['Banking/Credit Unions' => 'Banking/Credit Unions'],
            ['Call Center/Telemarketing' => 'Call Center/Telemarketing'],
            ['Childcare/Daycare' => 'Childcare/Daycare'],
            ['Collections' => 'Collections" selected="selected'],
            ['Construction/Trades' => 'Construction/Trades'],
            ['Consulting Services' => 'Consulting Services'],
            ['Customer Service' => 'Customer Service'],
            ['Drivers' => 'Drivers'],
            ['Education - Faculty' => 'Education - Faculty'],
            ['Education - Professional' => 'Education - Professional'],
            ['Education - Support Staff' => 'Education - Support Staff'],
            ['Education - Training' => 'Education - Training'],
            ['Engineering/Architecture' => 'Engineering/Architecture'],
            ['Entertainment/Gaming/Casino' => 'Entertainment/Gaming/Casino'],
            ['Financial Services - Investments/Securities' => 'Financial Services - Investments/Securities'],
            ['Financial Services - Mortgage' => 'Financial Services - Mortgage'],
            ['General Labor' => 'General Labor'],
            ['Government' => 'Government'],
            ['Green' => 'Green'],
            ['Grocery/Convenience Stores' => 'Grocery/Convenience Stores'],
            ['Healthcare - Admin/Office/Records/Finance' => 'Healthcare - Admin/Office/Records/Finance'],
            ['Healthcare - Assisted Living/Home Health' => 'Healthcare - Assisted Living/Home Health'],
            ['Healthcare - Behavioral/Mental Health' => 'Healthcare - Behavioral/Mental Health'],
            ['Healthcare - Dental' => 'Healthcare - Dental'],
            ['Healthcare - Dietary/Nutrition' => 'Healthcare - Dietary/Nutrition'],
            ['Healthcare - Lab/Hematology/Pathology' => 'Healthcare - Lab/Hematology/Pathology'],
            ['Healthcare - LPNs &amp; LVNs' => 'Healthcare - LPNs &amp; LVNs'],
            ['Healthcare - Medical &amp; Dental Practitioners' => 'Healthcare - Medical &amp; Dental Practitioners'],
            ['Healthcare - Optical' => 'Healthcare - Optical'],
            ['Healthcare - Paramedics/EMT\'s' => 'Healthcare - Paramedics/EMT\'s'],
            ['Healthcare - Pharmacy' => 'Healthcare - Pharmacy'],
            ['Healthcare - Radiology/Imaging' => 'Healthcare - Radiology/Imaging'],
            ['Healthcare - RNs &amp; Nurse Management' => 'Healthcare - RNs &amp; Nurse Management'],
            ['Healthcare - Support Services' => 'Healthcare - Support Services'],
            ['Healthcare - Therapy/Rehab Services' => 'Healthcare - Therapy/Rehab Services'],
            ['Hospitality/Resort/Hotel' => 'Hospitality/Resort/Hotel'],
            ['Human Resources - Comp &amp; Benefits' => 'Human Resources - Comp &amp; Benefits'],
            ['Human Resources - Employee Relations' => 'Human Resources - Employee Relations'],
            ['Human Resources - Generalists' => 'Human Resources - Generalists'],
            ['Human Resources - Management' => 'Human Resources - Management'],
            ['Human Resources - Recruitment/Staffing' => 'Human Resources - Recruitment/Staffing'],
            ['Human Resources - Risk &amp; Safety' => 'Human Resources - Risk &amp; Safety'],
            ['Human Resources - Training &amp; Development' => 'Human Resources - Training &amp; Development'],
            ['Insurance' => 'Insurance'],
            ['IT - Computer Services &amp; Support' => 'IT - Computer Services &amp; Support'],
            ['IT - Hardware/Networking' => 'IT - Hardware/Networking'],
            ['IT - Internet &amp; Ecommerce' => 'IT - Internet &amp; Ecommerce'],
            ['IT - Sales' => 'IT - Sales'],
            ['IT - Software/Development' => 'IT - Software/Development'],
            ['Legal' => 'Legal'],
            ['Management - Entry Level' => 'Management - Entry Level'],
            ['Management - Executive/Senior (C-Level, VP)' => 'Management - Executive/Senior (C-Level, VP)'],
            ['Management - Mid-Level (Manager, Director)' => 'Management - Mid-Level (Manager, Director)'],
            ['Management - Project / Program' => 'Management - Project / Program'],
            ['Manufacturing/Production' => 'Manufacturing/Production'],
            ['Marine' => 'Marine'],
            ['Media/Publishing' => 'Media/Publishing'],
            ['Military/Defense' => 'Military/Defense'],
            ['Mining' => 'Mining'],
            ['Non-Profit/Social Services' => 'Non-Profit/Social Services'],
            ['Oil/Gas/Energy' => 'Oil/Gas/Energy'],
            ['Other/General' => 'Other/General'],
            ['Personal Care/Spa/Beauty' => 'Personal Care/Spa/Beauty'],
            ['Police/Fire/Emergency Personnel' => 'Police/Fire/Emergency Personnel'],
            ['Purchasing/Procurement' => 'Purchasing/Procurement'],
            ['Real Estate/Property Mgmt' => 'Real Estate/Property Mgmt'],
            ['Restaurant/Food Service' => 'Restaurant/Food Service'],
            ['Retail' => 'Retail'],
            ['Sales' => 'Sales'],
            ['Science/Biotech/Research' => 'Science/Biotech/Research'],
            ['Security/Protection Services' => 'Security/Protection Services'],
            ['Skilled Trades - General/Assembly' => 'Skilled Trades - General/Assembly'],
            ['Skilled Trades - Machinist/Technician' => 'Skilled Trades - Machinist/Technician'],
            ['Skilled Trades - Mechanical/Electrical' => 'Skilled Trades - Mechanical/Electrical'],
            ['Skilled Trades - Welders/Fabricators' => 'Skilled Trades - Welders/Fabricators'],
            ['Sports/Recreation/Fitness' => 'Sports/Recreation/Fitness'],
            ['Summer Jobs' => 'Summer Jobs'],
            ['Telecommunications' => 'Telecommunications'],
            ['Tourism/Travel/Airline' => 'Tourism/Travel/Airline'],
            ['Transportation/Supply Chain/Logistics' => 'Transportation/Supply Chain/Logistics'],
            ['Veterinary Services' => 'Veterinary Services'],
            ['Warehouse/Maintenance' => 'Warehouse/Maintenance'],
        ]
    }
);

sub site_url {
    return URI->new('https://crm.recruiting.com');
}

sub api_url {
    return URI->new('https://api.recruiting.com/4.2/json');
}

sub session_life_default {
    return 30 * 60; #30 minutes
}

sub per_page {
    return 25;
}

sub search_url {
    my $self = shift;
    my $url = $self->api_url;
    $url->path_segments(
        $url->path_segments,
        'resumes',
        'search'
    );
    return $url;
}

# returns the api credentials
# makes a login reques unless they're set.
sub api_credentials {
    my $self = shift;

    my $params = $self->{api_credentials} //= $self->get_account_value('api_credentials');
    if($params) {
        $log->info('Already logged in');
        return %$params;
    } else {
        $self->_login();
    }
    return %{$self->{api_credentials}};
}

# Returns the company_unique_name. Used here and there for requests
# Makes a login request unless it's set.
sub company_unique_name {
    my $self = shift;

    my $company_unique_name = $self->{company_unique_name} //= $self->get_account_value('company_unique_name');
    return $company_unique_name if $company_unique_name;

    $self->_login();

    return $self->{company_unique_name};
}

# Logs into the site and sets session and user authentication values.
sub _login {
    my $self = shift;

    $log->info('Logging in');

    my $agent = $self->new_agent;

    my $login_url = $self->site_url;
    $login_url->query_form(
        email    => $self->get_token_value('username'),
        password => $self->get_token_value('password'),
        submit   => 'Login'
    );

    $agent->get($login_url);
    if($agent->response->request->uri !~ 'dashboard.asp') {
        $self->throw_login_error('Incorrect email and password');
    }

    my $login_dom = XML::LibXML->load_html(
        string => $agent->content,
        recover => 2
    );

    my $company_unique_name = $login_dom->findvalue('/html/body/@data-company-unique-name');

    $self->set_account_value(
        'company_unique_name',
        $company_unique_name
    );

    my $login_cookies = $agent->cookie_jar
      ->{'COOKIES'}
      ->{'.recruiting.com'}
      ->{'/'};

    $self->set_account_value(
        'login_cookies',
        $agent->cookie_jar->{'COOKIES'}
    );

    my %credentials = (
        timestamp    => $login_cookies->{ApiTimestamp}->[1],
        key          => $login_cookies->{ApiKey}->[1],
        token        => $login_cookies->{ApiToken}->[1],
        user_key     => $login_cookies->{UserKey}->[1],
        recruiter_id => $login_cookies->{RecruiterID1}->[1],
        # Board encodes this. I don't know why.
        user_auth_id => URI::Escape::uri_unescape(
            $login_cookies->{UserGuid}->[1]
        )
    );


    $self->set_account_value(
        api_credentials => \%credentials
    );
    $self->{api_credentials} = \%credentials;

    return 1;
}

# Returns the login cookies.
# Makes a login request unless they're set.
sub login_cookies {
    my $self = shift;
    my $cookies = $self->get_account_value('login_cookies');

    return $cookies if %$cookies;

    $log->info('Loggin in for login cookies');
    $self->_login();

    return $self->get_account_value('login_cookies');
}

# Returns the search areas. The IDs of the locations the user's account is allowed to 
# search in. Will filter results when sent.
# Makes a login request unelss they're set.
sub search_areas {
    my $self = shift;
    my $search_areas = $self->{search_areas} //= $self->get_account_value('search_areas');
    return @$search_areas if $search_areas;

    $log->info('Requesting search areas');

    my $url = $self->api_url;
    $url->path_segments(
        $url->path_segments,
        'companies',
        $self->company_unique_name,
        'searchareas'
    );
    $url->query_form($self->api_credentials);
    my $response = $self->new_agent->get($url);

    $search_areas = $self->user_object->stream2->json->decode(
        $response->decoded_content
    );
    my @allowed_areas = map { $_->{id}; } @{$search_areas->{search_area}};

    $self->{search_areas} = \@allowed_areas;
    $self->set_account_value(search_areas => \@allowed_areas);
    return @allowed_areas;
}

# Map to a US state (otherwise return a notice.
# Check through the board's API whether this user has access to this state.
sub map_location {
    my($self, $location_id) = @_;
    my %output = ();

    $log->info('Mapping location ID %d', $location_id);

    my $location = $self->location_api->find($location_id);
    my $state = $location->get_mapping('state_name');

    if(!$state) {
        return (
            notice => 'Only US states are supported'
        );
    }

    my $url = $self->api_url;
    $url->path_segments(
        $url->path_segments,
        'locations',
        'search',
        $state
    );
    $url->query_form(
        $self->api_credentials,
    );

    my $response = $self->new_agent->get($url);
    my $board_locations = $self->user_object->stream2->json->decode(
        $response->decoded_content
    );
    my $board_state = List::Util::first {
        $_->{location_description} eq 'area' && $_->{name} eq $state;
    } @$board_locations;

    if(!$board_state) {
        $output{notice} = 'Board does not support locaton';
    } elsif ( List::Util::any { $_ == $board_state->{location_type_id} } $self->search_areas ) {

        if($location->type ne 'state') {
            # We're not a state. Give notice that the search location is the parent state.
            $output{notice} = sprintf(
                'Searching using state: %s',
                HTML::Entities::encode_entities($board_state->{name})
            );
        }

        $output{label} = $board_state->{name};
        $output{id}    = $board_state->{id};
    } else {
        $output{notice} = 'Your board subscription does not cover this location';
    }

    return %output;
}

sub search_fields{
    my $self = shift;

    my $cv_age = $self->CV_UPDATED_WITHIN_TO_DAYS(
        $self->get_token_value('cv_updated_within')
    );

    my ($sort_field, $sort_order) = split ',', $self->get_token_value('sort_by') // '';

    my %fields = (
        $self->api_credentials,
        searchtype   => 'sourcing',
        displaylevel => 'E',
        compun => $self->get_account_value('company_unique_name') // '',
        q => $self->get_token_value('keywords') // '',
        d => $cv_age,
        ps => $self->per_page,
        p => $self->current_page(),
        grp => 'y',
        'jobtypenames[]' => $self->get_token_value('category') // '',
        minimum_education_level => $self->get_token_value('min_education_level') // '',
        minimum_career_level => $self->get_token_value('min_career_level') // '',
        sortfield => $sort_field,
        sortorder => $sort_order,

        miles => '25',
        'loc[]' => [$self->search_areas],
        sct => ['ks', 'mfl'], # Gets both results and facets.
        miles => $self->get_token_value('location_within_miles'),
        location => '',
    );

    my $location_id = $self->get_token_value('location_id');
    
    if($location_id) {
        my %location = $self->map_location($location_id);
        $self->results->add_notice($location{notice}) if $location{notice};
        $fields{location} = $location{id} if $location{id};
    }

    return %fields;
}

# Returns a new WWW::Mechanize object with a handler for logging set
# and a shared cookie jar.
sub new_agent {
    my $self = shift;
    $self->{agent} //= WWW::Mechanize->new();

    # Shares cookie jar.
    my $agent = $self->{agent}->clone();

    $self->log_lwp_ua($agent);

    return $agent;
}

sub search_begin_search {
    my $self = shift;
    return $self->search_specific_page(1);
}

sub search_specific_page {
    my ($self, $page) = @_;

    $self->current_page($page);
    $log->infof('Searching page %d', $page);
    
    my %fields = $self->search_fields();
    my $search_url = $self->search_url;
    $search_url->query_form(
        %fields,
        '_' => time
    );
    
    my $search_results = $self->user_object->stream2->json->decode(
        $self->new_agent->get($search_url)->decoded_content
    );

    $self->total_results($search_results->{numFound});
    $self->max_pages(int($self->total_results / $self->per_page + 0.5));

    my @candidates = $self->scrape_candidates($search_results->{resumes});
    $log->infof('Found %d candidates', scalar @candidates);
    foreach my $candidate (@candidates) {
        $self->add_candidate($candidate);
    }
}

# Takes parsed JSON candidate list
# returns a list of scraped Search candidates from that lis.
sub scrape_candidates {
    my ($self, $candidates_container) = @_;
    $log->infof('Scraping %d candidates', scalar @$candidates_container);
    return map {
        $self->scrape_candidate($_);
    } @$candidates_container;
}

sub scrape_candidate {
    my ($self, $candidate_source) = @_;
    my $candidate = $self->new_candidate();

    my @keys = qw(
        career_level
        city_stateabbr
        create_date
        education_level
        guid
        hourly_rate
        job_seeker_id
        last_updated_date
        photo_url_50x50
        recruiter_last_viewed_date
        resume_headline
        salary_per_year
        snippet
    );

    foreach my $key (@keys) {
        $candidate->attr($key => $candidate_source->{$key});
    }

    my $name = join (
        ' ',
        grep {$_} (
            $candidate_source->{first_name},
            $candidate_source->{last_name}
        )
    );

    $candidate->attr(name => $name);
    $candidate->attr(headline => $candidate_source->{resume_headline});
    
    # Give the dates a similar format to the board
    # e/g/ 'X units since'
    my $now = DateTime->now();
    my $last_seen = eval {
        DateTime::Format::ISO8601->parse_datetime(
            $candidate->delete_attr('recruiter_last_viewed_date')
        );
    };

    # Unviewed candidates has a last viewed time of year AD 0001
    # Leave their last_viewed empty
    if($last_seen && $last_seen->year > 1) {
        $candidate->attr(
            last_viewed => $self->duration_nice_name($now - $last_seen)
        );
    }

    my $last_updated = eval {
        DateTime::Format::ISO8601->parse_datetime(
            $candidate->delete_attr('last_updated_date')
        );
    };

    $candidate->attr(
        last_updated => $last_updated ?
          $self->duration_nice_name($now - $last_updated) : ''
    );

    $candidate->candidate_id($candidate_source->{guid});
    $candidate->has_profile(1);
    $candidate->has_cv(1);

    return $candidate;
}

# Returns the diration with beautiful nice name.
sub duration_nice_name {
    my ($self, $duration) = @_;
    my @units = qw(
        year month day
        hour minute second
    );
    my $value;
    foreach my $unit (@units) {
        $value = $duration->in_units($unit . 's')
          or next;
        return sprintf(
            '%s %s%s ago.',
            $value,
            $unit,
            $value == 1 ? '' : 's'
        );
    }
}

# Points to a JSON file with candidate profile details.
sub profile_url {
    my ($self, $candidate) = @_;
    my $url = $self->api_url;
    $url->path_segments(
        $url->path_segments,
        'companies',
        $self->get_account_value('company_unique_name'),
        'candidates',
        $candidate->attr('job_seeker_id'),
        'profile'
    );
    $url->query_form(
        $self->api_credentials
    );
    return $url;
}

# Sanitise CV HTML.
sub scrub_html_cv {
    my ($self, $html) = @_;
    my $scrubber = HTML::Scrubber->new(
        allow => [
            qw( h1 h2 div span br strong b u i em p )
        ]
    );
    return $scrubber->scrub($html);
}

# Points to a JSON file with details on the candidate's CV
# information on the board.
sub resume_preview_url {
    my ($self, $candidate) = @_;
    my $url = $self->api_url;
    $url->path_segments(
        $url->path_segments,
        'candidates',
        'resumes'        
    );

    $url->query_form(
        $self->api_credentials,
        uniquename => $self->get_account_value('company_unique_name'),
        jobseekerid => $candidate->attr('job_seeker_id'),
        resume_id => $candidate->attr('resume_id'),
        '_' => time,
    );
    return $url;
}

sub scrape_resume_preview {
    my ($self, $candidate, $resume) = @_;

    my @keys = qw(
        resume_signature
        resume_conversion
        resume_id
    );

    foreach my $key (@keys) {
        my $value = $resume->{$key}
          or next;

        $log->debugf('Assigning candidate attribute %s => %s', $key, $value);
        $candidate->attr($key => $value);
    }

    return;
}

# Returns sanitized HTML for a candidate CV preview.
# Returns undef if no preview is available.
sub candidate_cv_preview_html {
    my ($self, $candidate) = @_;

    my $resumes_url = $self->resume_preview_url($candidate);
    $resumes_url->query_param('_' => time);    
    
    my $resumes = $self->user_object->stream2->json->decode(
        $self->new_agent->get($resumes_url)->decoded_content
    );

    # Use the first resume hash with content
    my $resume = List::Util::first {
        $_->{resume_conversion}->{resume_html} ||
        @{$_->{resume_conversion}->{resume_image}->{image_path}}
    } @$resumes;
    # Othewise defualt to first
    $resume ||= $resumes->[0];

    $self->scrape_resume_preview($candidate, $resume);

    my $resume_conversion = $candidate->attr('resume_conversion');

    my $cv_html = $resume_conversion->{resume_html};
    my @cv_images = @{$resume_conversion->{resume_image}->{image_path} || [] };
    my $cv_preview_html;

    if($cv_html) {
        $log->info('Candidate has a CV preview HTML');
        $cv_preview_html = $self->scrub_html_cv($cv_html);
    } elsif(@cv_images) {
        $log->info('Candidate has a CV preview images');
        $cv_preview_html = join '<br />', map {
            my $src = HTML::Entities::encode_entities($_);
            "<img src=\"$src\" />";
        } @cv_images;
    } else {
        $log->info('No resume preview found');
    }

    return $cv_preview_html;
}

sub build_candidate_profile {
    my ($self, $candidate) = @_;

    my $agent = $self->new_agent;
    my $profile_url = $self->profile_url($candidate);
    $profile_url->query_param( '_' => time );
    $agent->get($profile_url);

    my $profile = $self->user_object->stream2->json->decode(
        $agent->content
    );

    $self->scrape_profile($candidate, $profile);
}

sub download_profile {
    my ($self, $candidate) = @_;

    $self->build_candidate_profile($candidate);
    
    my $cv_preview_html = eval {
        $self->candidate_cv_preview_html($candidate);
    };
    $log->errorf('Could not generate HTML CV Preview: %s', $@) if $@;

    $cv_preview_html ||= 'No CV preview available.';
    $candidate->attr(cv_preview_html => $cv_preview_html);
    #That's all we need
    return $candidate;
}

sub scrape_profile {
    my ($self, $candidate, $profile) = @_;

    my @keys = qw(
        birth_date
        career_level
        education_level
        full_name
        hourly_rate
        job_categories
        job_statuses
        location
        phones
        relocation
        salary
        salary_comment
        signature
        socials
        status
        resume_id
    );

    foreach my $key (@keys) {
        my $profile_value = $profile->{candidate}->{$key}
          or next;

        $log->debugf('Assigning candidate attribute %s => %s', $key, $profile_value);
        $candidate->attr($key => $profile_value);
    }
    $candidate->attr('email' => $profile->{email});

    return;
}

sub do_download_cv {
    my ($self, $candidate) = @_;

    # Resume signature is scraped from the profile.
    $candidate = $self->download_profile($candidate);

    my $viewer_url = $self->site_url();
    $viewer_url->path('resume_view.asp');
    $viewer_url->query_form(
        resumeId  => $candidate->attr('resume_id'),
        signature => $candidate->attr('resume_signature')
    );

    my $agent = $self->new_agent;
    
    $agent->cookie_jar->{COOKIES} = $self->login_cookies();

    my $preview_response = $agent->get($viewer_url);

    my $doc = XML::LibXML->load_html(
        string  => $preview_response->decoded_content,
        recover => 2
    );

    my $cv_download_link = $doc->findvalue('//object/@data')
      or $self->throw_no_cv('No CV download available');
    $log->debugf('CV download link scraped: %s', $cv_download_link);

    
    # Get PDF download link
    my $file_response = $self->new_agent->get(
        $cv_download_link
    );
    return $file_response;
}

sub feed_identify_error {
    my ($self, $message, $content) = @_;    

    return unless $content //= $self->content();

    if($message =~ m/Not enough storage is available to complete this operation/) {
        return $self->throw_throw_unavailable('The board is currently unavailable');
    }
    return;
}

1;
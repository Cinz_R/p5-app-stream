
package Stream::Templates::OilAndGasPlatform;

=head1 OilAndGasPlatform

Base module for feeds on the Oil&Gas platform.

=cut

use base qw(Stream::Engine::Robot);

use strict;
use warnings;
use utf8;

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);
use Stream::Proxies::UK;

$authtokens{username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

# Board wants us coming through specified IPs for analytics
sub PROXY_CLASS {
    return 'Stream::Proxies::UK';
}

sub base_url {
    confess('base_url() required but not implemented');
}

# The board wants client_ip with every request for analytics
# Override to add injection of client_ip
sub new_agent {
    my ($self, @args) = @_;
    my $agent = $self->SUPER::new_agent(@args);
    $agent->add_handler(request_prepare => sub {
        my ($request, $ua, $h) = @_;

        $self->log_debug($request->as_string);

        $self->log_info('Adding client_ip URI param');
        my $url = URI->new($request->uri);
        $url->query_form(
            $url->query_form,
            client_ip => $self->_client_ip,
        );
        $request->uri($url);

        return $request;
    });

    return $agent;
}

# Login with an immediate POST request
# Confirm success via returned headers
sub login {
    my ($self) = @_;

    $self->log_info('Logging in');

    my $agent = $self->agent();

    # Skip redirects
    my $old_redirect_count = $agent->max_redirect();
    $agent->max_redirect(0);
    my $response = $agent->post(
        $self->base_url . '/Account/LogOn',
        Content => {
            UserName => $self->token_value('username') || '',
            Password => $self->token_value('password') || '',
        }
    );
    $agent->max_redirect($old_redirect_count);

    # Validate login with location header
    if($response->headers->header('Location') || '' eq '/Client/AccountHome') {
        $self->log_info('Found redirect to "/Client/AccountHome". Login successful');
        return;
    }

    $self->log_error('Redirect header not find. Login failed');
    # Login failed. The board should return a failure message in the body
    my $content = $response->decoded_content;
    if($content =~ m/Login details incorrect/) {
        $self->throw_login_error('Login details incorrect');
    }
    $self->throw_login_error('Unable to login');
}

# Logout with a HEAD request to the logout URL
# Confirm success via returned headers
sub logout {
    my ($self) = @_;
    my $agent = $self->agent;
    $self->log_info('Logging out, skipping redirects');

    # Skip unnecessary redirect
    $agent->max_redirect(0);

    # use HEAD to avoid unnecessary response body
    my $response = $agent->head($self->base_url . '/Account/LogOff');

    if($response->headers->header('Location') eq '/') {
        $self->log_info('Found redirect to "/". Logout successful');
    } else {
        $self->log_warn('Did not find redirect to "/". Logout failed');
    }
}

sub search_find_form {
    my $self = shift;
    $self->current_form($self->form_with_fields( 'CVSearchTerm' ));
}

sub search_url {
    my $self = shift;
    return $self->base_url . '/CVSearch/Advanced';
}

sub _client_ip {
    my ($self) = @_;
    return $self->stream_action->remote_ip || 'unavailable';
}

sub search_query {
    my $self = shift;

    my %fields;

    my $stream2 = $self->user_object->stream2;

    my %cv_updated = (
        'TODAY' => '1d',
        'YESTERDAY' => '1d',
        '3D' => '7d',
        '1W' => '7d',
        '2W' => '1m',
        '1M' => '1m',
        '2M' => '3m',
        '3M' => '3m',
        '6M' => '6m',
        '1Y' => '1y',
        '2Y' => '2y',
        '3Y' => '3y',
        'ALL' => 'all'
    );

    $fields{'SearchPeriod'} = $cv_updated{$self->token_value('cv_updated_within')};
    $fields{'Direct'} = "False";

    # Country of residence
    my $country_id = $self->token_value('country_id') || '';
    if( $country_id =~ m/~/){
        my @loc = split('~',$country_id);
        $fields{'cor-PreferredCountry'} = $loc[0];
        $fields{'RegionId'}= "0";
        my @loc_reg = split('_', $loc[1]);
        $fields{'cor-PreferredRegion'} = \@loc_reg;
    } elsif( $country_id =~ m/^(?:0|10001|10002|10003|10004|10005|10006)$/  ){
        my %locs = oilandgas_locations($country_id);
        $fields{'cor-PreferredArea'} = $locs{area};
        $fields{'cor-PreferredCountry'} = $locs{countries};
        $fields{'cor-PreferredRegion'} = $locs{regions};

        if( $country_id eq '0'){
            $fields{'PreferredArea_All'} = 'on';
        }
    } elsif( $country_id ne "") {
        $fields{'cor-PreferredCountry'} = $country_id;
    }

    # Nationality
    my $nationality = $self->token_value('nationality') || '';
    if( $nationality =~ m/^(?:0|10001|10002|10003|10004|10005|10006)$/  ){
        my %locs = oilandgas_locations($nationality);
        $fields{'nat-PreferredArea'} = $locs{area};
        $fields{'nat-PreferredCountry'} = $locs{countries};

        if( $nationality eq '0'){
            $fields{'PreferredArea_All'} = 'on';
        }
    } elsif( $nationality ne "") {
        $fields{'nat-PreferredCountry'} = $nationality;
    }


    my $job_type = $self->token_value('default_jobtype');
    if ($job_type eq "permanent") {
        $fields{'JobType'} = "Permanent";
    } elsif ($job_type eq "contract") {
        $fields{'JobType'} = "Contract";
    } else {
        $fields{'JobType'} = "Either";
    }
    $fields{'CVSearchTerm'}= $self->token_value('keywords');

    $fields{'Nationality'}= $nationality if $nationality;

    if($self->token_value('location_id')) {
        my $message = $stream2->__('This board does not support default locations. Use the "Country of Residence" filter instead.');
        $self->results->add_notice($message);
    }
    $fields{'PreferredLocation'} = '0';


    $fields{'JobTitleSearchTerm'}= $self->token_value('job_title') || '';

    my $exp1="0";
    my $exp2="Any";
    $exp1 = $self->token_value('exp_min') if( $self->token_value('exp_min') );
    $exp2 = $self->token_value('exp_max') if( $self->token_value('exp_max') );
    if($exp2 eq "41" ){
        $exp2 = "Any";
    }
    $fields{'ExperienceDescription'} = $exp1 . " - " . $exp2 . " years";
    $fields{'MinYearsExperience'} = $self->token_value('exp_min') || '0';
    $fields{'MaxYearsExperience'} = $self->token_value('exp_max') || '41';

    if( $self->token_value('work_in_eu') || '' eq "on" ){
        $fields{'nationality-filter-euro-checkbox'} = "on";
        delete $fields{'nat-PreferredCountry'};
        my @eu_array = (100,106,113,117,120,129,135,140,145,146,149,161,174,176,178,181,183,184,193,203,205,207,209,211,219,229,232,237,241,247,249,259,263,271,272,273,274,276,39,41,44,47,49,51,52,53,54,56,57,58,59,61,62,64,77,83,88,90,99);
        $fields{'nat-PreferredCountry'} = \@eu_array;
        delete $fields{'nat-PreferredArea'};
    }

    # Add a warning that salary is not supported if the user entered salary values
    if($self->token_value('salary_from') || $self->token_value('salary_to')) {
        my $message = $stream2->__('This board does not support searching by salary.');
        $self->results->add_notice($message);
    }

    $fields{'Submit'} = "SEARCH";

    my $search_uri = URI->new();
    $search_uri->query_form(\%fields);
    $self->log_debug("Query is: " . $search_uri->query());

    return $search_uri->query();
}

# Searching is split into 2 requests
# The first creates a "search" on the board
# The second retrieves results from the "search" on the board
sub search_submit {
    my $self = shift;

    my $agent = $self->agent;

    my $url = URI->new($self->search_url);
    my $page = $self->current_page;
    $url->query_form(
        ajax => 'False',
    );

    # This creates a search that is cached on the board
    my $old_redirect_count = $agent->max_redirect();

    # Skip the board's automatic redirect to page 1
    $agent->max_redirect(0);
    $agent->post( $url,
        Content => $self->search_query(),
        Content_type => 'application/x-www-form-urlencoded'
    );
    $agent->max_redirect($old_redirect_count);

    # Query that cached search to a specific page
    $url->query_form(
        sortorder => 'distance',
        sortdir => 'asc',
        page => $self->current_page,
        ajax => 'true',
        _ => time(),
    );
    my $response = $agent->get($url);
    $self->content($response->decoded_content);
    return $response;
}

sub search_success {
    return qr{Candidates found|No results found|Candidate found};
}

sub search_number_of_results_xpath {
    '//div[@id="results-info"]'
}

sub search_number_of_results_regex {
    return qr{^(\d+) Candi};
}

sub profile_url {
    my $self = shift;
    my $candidate = shift;
    return $candidate->{'profile_url'};
}

sub scrape_candidate_xpath {
    return '//tr[contains(@class, "candidate")]'
}

sub scrape_candidate_details {
    return (
        'cv_link' => './/img[@alt="Download this CV"]/parent::a/@href',
        'modified_date' => './td[1]/a[@class="candidate-details"]/text()',
        'name'  => './td[2]//a/text()',
        'profile_url'  => './td[2]//a/@href',
        'job_title' => './td[3]',
        'country_resid' => './td[4]',
        'nationality' => './td[5]',
        'relevance' => './td[6]/a/text()',
        'profile' => './following-sibling::tr[1]/td',
        'employment_history' => './following-sibling::tr[3]/td',
        'experience' => './following-sibling::tr[2]/td',
        'qualifications' => './following-sibling::tr[4]/td',
    );
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;
    my $cv_link = $candidate->attr('cv_link');
    $cv_link =~ /(\d+)$/;
    $candidate->attr('candidate_id' => $1);

    $candidate->{'qualifications'} =~ s/^Qualifications: //g;
    $candidate->{'employment_history'} =~ s/^Employment History://g;
    $candidate->{'profile'} =~ s/^Profile: //g;

    $candidate->has_profile(1);
    $candidate->has_cv(1);
    return $candidate;
}

sub scrape_candidate_headline {
    return 'job_title';
}

sub download_profile_success {
    return qr{Candidate Details For};
}

sub scrape_download_profile_xpath {
    return '//table[@class="grid"]'
}

sub scrape_download_profile_details {
    return (
        'email' => '//a[contains(@href, "/content/recruiters/cvdatabase/contact_cand.asp")',
        'locations' => [
            './/div[@class="candidate-preferred-location"]//ul/li'
        ],
        'data' => [
            './/td[@class="lbl"]' => {
                fieldname => '.',
                fieldvalue => './following-sibling::td',
            }
    ]);
}

sub scrape_fixup_candidate_profile {
    my $self = shift;
    my $candidate = shift;
    $self->process_field_value_pairs($candidate);
}

sub process_field_value_pairs {
    my $self = shift;
    my $candidate = shift;
    my %field_mapping =(
        'Title' => 'title',
        'Full Name' => 'full_name',
        'Email' => 'email',
        'Address1' => 'address1',
        'Address2' => 'address2',
        'Town' => 'town',
        'Postcode' => 'postcode',
        'Country' => 'country',
        'Region' => 'region',
        'Telephone' => 'telephone',
        'Medical Certificate Expiry Date' => 'medical_certificate_expiry_date',
        'Survival Certificate Expiry Date' => 'survival_certificate_expiry_date',
        'Quick Description' => 'quick_description',
        'Date Created' => 'date_created',
        'Date Updated' => 'date_updated',
        'Last Visited' => 'last_visited',
        'Date Of Birth' => 'date_of_birth',
        'Current Job Title' => 'current_job_title',
        'Minimum Salary Required ' => 'minimum_salary_required',
        'Minimum Hourly Rate Required' => 'minimum_hourly_rate_required',
        'Preferred Job Locations' => 'preferred_job_locations',
    );

    my @data = $candidate->attr("data");
    foreach my $record (@data) {
        my $new_fieldname = $field_mapping{$record->{'fieldname'}};
        if ($new_fieldname =~ /^s+$/) {
            # Ignore fields with mappings to whitespace.
        } elsif ($new_fieldname) {
            $candidate->attr($new_fieldname => $record->{'fieldvalue'});
        } else {
            $self->log_warn("Fieldname: " . $record->{'fieldname'} . " not found");
        }
    }
}

sub feed_identify_error {
    my ($self, $message, $content) = @_;

    # We end up on the accounts page if a search failed because of no subscription
    # It includes a table with 'INACTIVE' as the value for 'CV/Resume Search'
    if($content =~ m/INACTIVE/) {
        $self->throw_login_error(
            'You don\'t have a vaild cv access subscription'
        );
    }
    return;
}

sub oilandgas_locations {
    my $loc_id = shift;

    my @countries_1 = (103,107,109,118,126,127,132,134,152,157,158,159,160,163,165,167,169,172,173,179,180,187,194,2,202,204,206,209,212,214,216,218,222,231,232,236,239,240,242,244,252,253,256,262,264,266,280,3,4,5,6,7,78,8,85,87);
    my @regions_1 = (214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251);
    my @area = $loc_id;

    my @countries_2 = (101,124,128,130,138,142,144,153,154,156,162,164,168,175,177,178,185,190,191,193,197,198,199,200,201,208,210,217,219,22,220,221,223,224,229,23,230,234,24,248,25,250,251,254,255,257,26,263,265,27,28,29,30,31,32,33,34,35,36);
    my @regions_2 = (141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,278,279,66,67,68,69,70,71);

    my @countries_3 = (100,106,113,115,116,117,120,125,129,135,136,143,146,181,192,195,203,205,213,215,233,243,246,261,270,278,37,38,39,40,41,42,43,44,45,47,49,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,77,83,88,90,92,98,99);
    my @regions_3 = (100,101,102,103,104,105,106,108,109,110,111,112,113,114,115,116,117,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,54,55,56,57,58,59,60,61,62,63,64,65,86,87,88,89,90,91,92,93,94,95,96,97,98,99);

    my @countries_4 = (10,11,12,13,14,15,16,17,18,19,20,279,80,86,89,95);
    my @regions_4 = (128,129,130,131,132,133,134,135,136,137,138,139,140,72,73,74,75,76,77,78,79,80,81,82,83,84,85);

    my @countries_5 = (183,237,67,68,69);
    my @regions_5 = (1,10,11,118,119,12,120,121,122,123,124,125,126,127,13,14,15,16,17,18,19,2,20,21,22,23,24,25,26,27,28,280,281,282,29,3,30,31,32,33,34,35,36,37,38,39,4,41,42,43,44,45,46,47,49,5,50,51,52,53,6,7,8,9);

    my @countries_6 = (102,121,122,131,133,137,140,141,145,147,148,149,150,151,155,161,166,170,171,174,176,182,184,186,188,189,196,207,211,225,227,228,235,238,241,245,247,249,258,259,260,271,272,273,274,276,281,70,71,72,73,74,76,79,91,93);
    my @regions_6 = (40,48);

    my @countries_all = (@countries_1,@countries_2,@countries_3,@countries_4,@countries_5,@countries_6);
    my @regions_all = (@regions_1,@regions_2,@regions_3,@regions_4,@regions_5,@regions_6);
    my @area_all = (10001,10002,10003,10004,10005,10006);

    return ("area" => \@area, "countries" => \@countries_1, "regions" => \@regions_1) if( $loc_id eq '10001');
    return ("area" => \@area, "countries" => \@countries_2, "regions" => \@regions_2) if( $loc_id eq '10002');
    return ("area" => \@area, "countries" => \@countries_3, "regions" => \@regions_3) if( $loc_id eq '10003');
    return ("area" => \@area, "countries" => \@countries_4, "regions" => \@regions_4) if( $loc_id eq '10004');
    return ("area" => \@area, "countries" => \@countries_5, "regions" => \@regions_5) if( $loc_id eq '10005');
    return ("area" => \@area, "countries" => \@countries_6, "regions" => \@regions_6) if( $loc_id eq '10006');
    return ("area" => \@area_all, "countries" => \@countries_all, "regions" => \@regions_all) if( $loc_id eq '0');

}

sub results_per_page {
    return 20;
}


$standard_tokens{job_title} = {
    Type        => 'Text',
    Label       => 'Search in Current Job Title ',
    SortMetric  => 1,
};

$standard_tokens{exp_min} = {
    Type        => 'List',
    Label       => 'Years of Experience (Min)',
    Values      => '0=0|1=1|2=2|3=3|4=4|5=5|6=6|7=7|8=8|9=9|10=10|11=11|12=12|13=13|14=14|15=15|16=16|17=17|18=18|19=19|20=20|21=21|22=22|23=23|24=24|25=25|26=26|27=27|28=28|29=29|30=30|31=31|32=32|33=33|34=34|35=35|36=36|37=37|38=38|39=39|40=40|41=41',
    Default     => '0',
    SortMetric  => 2,
};

$standard_tokens{exp_max} = {
    Type        => 'List',
    Label       => 'Years of Experience (Max)',
    Values      => 'Any=41|40=40|39=39|38=38|37=37|36=36|35=35|34=34|33=33|32=32|31=31|30=30|29=29|28=28|27=27|26=26|25=25|24=24|23=23|22=22|21=21|20=20|19=19|18=18|17=17|16=16|15=15|14=14|13=13|12=12|11=11|10=10|9=9|8=8|7=7|6=6|5=5|4=4|3=3|2=2|1=1|0=0',
    Default     => '41',
    SortMetric  => 3,
};


$standard_tokens{country_id} = {
    Type =>   'List',
    Label =>  'Country of Residence',
    Default => '0',
    Values => 'Not specified=|All=0|Africa=10001|Asia & Pacific=10002|Europe & CIS=10003|Middle East=10004|North America=10005|South America=10006|Afghanistan=142|Åland Islands=146|Albania=37|Algeria=8|American Samoa=144|Andorra=143|Angola=2|Anguilla=145|Antarctica=147|Antigua and Barbuda=141|Argentina=70|Armenia=38|Aruba=140|Australia=22~278_279_66_67_68_69_70_71|Austria=39|Azerbaijan=40|Bahamas, The=150|Bahrain=10|Bangladesh=23|Barbados=148|Belarus=115|Belgium=41|Belize=151|Benin=152|Bermuda=149|Bhutan=154|Bolivia=131|Bonaire, Sint Eustatius and Saba=274|Bosnia=43|Botswana=126|Bouvet Island=155|Brazil=71|British Indian Ocean Territory=193|British Virgin Islands=259|Brunei Darussalam=156|Bulgaria=44|Burkina Faso=256|Burma=128|Burundi=157|Cambodia=124|Cameroon=3|Canada=68~118_119_120_121_122_123_124_125_126_127_280_281_282|Cape Verde=167|Cayman Islands=161|Central African Republic=165|Chad=158|Chile=72|China=24|Christmas Island=200|Cocos (Keeling) Islands=162|Colombia=73|Comoros=163|Congo, Democratic Republic of the=160|Congo, Republic of the=159|Cook Islands=168|Costa Rica=93|Cote d\'Ivoire=194|Croatia=45|Cuba=166|Curaçao=271|Cyprus=106|Czech Republic=113|Denmark=47|Djibouti=169|Dominica=170|Dominican Republic=171|East Timor=254|Ecuador=74|Egypt=11|El Salvador=137|Equatorial Guinea=109|Eritrea=172|Estonia=135|Ethiopia=173|Falkland Islands (Islas Malvinas)=176|Faroe Islands=125|Fiji=175|Finland=129|France=49~157_158_159_160_161_162_163_164_165_166_167_168_169_170_171_172_173_174_175_176_177_178|French Guiana=174|French Polynesia=178|French Southern and Antarctic Lands=179|Gabon=107|Gambia, The=180|Georgia=98|Germany=51~283_284_285_286_287_288_289_290_291_292_293_294_295_296_377_378|Ghana=87|Gibraltar=181|Greece=77|Greenland=183|Grenada=182|Guadeloupe=184|Guam=185|Guatemala=186|Guernsey=278|Guinea=187|Guinea Bissau=231|Guyana=188|Haiti=189|Heard Island and McDonald Islands=191|Holy See (Vatican City)=261|Honduras=91|Hong Kong=190|Hungary=53|Iceland=117|India=25~179_180_181_182_183_184_185_186_187_188_189_190_191_192_193_194_195_196_197_198_199_200_201_202_203_204_205_206_207_208_209_210_211_212_213|Indonesia=26|Iran=12|Iraq=13|Ireland=83~373_374_375_376|Isle of Man=192|Israel=14|Italy=54~297_298_299_300_301_302_303_304_305_306_307_308_309_310_311_312_313_314_315_316|Jamaica=196|Japan=27|Jersey=195|Jordan=15|Kazakhstan=55|Kenya=127|Kiribati=199|Korea=28|Korea, North=198|Kosovo=270|Kuwait=16|Kyrgyzstan=197|Laos=201|Latvia=100|Lebanon=89|Lesotho=204|Liberia=202|Libya=17|Liechtenstein=203|Lithuania=90|Luxembourg=205|Macau=208|Macedonia=116|Madagascar=206|Malawi=212|Malaysia=29~141_142_143_144_145_146_147_148_149_150_151_152_153_154_155_156|Maldives=217|Mali=214|Malta=88|Marshall Islands=234|Martinique=207|Mauritania=216|Mauritius=134|Mayotte=209|Mexico=79|Micronesia, Federated States of=177|Moldova=42|Monaco=215|Mongolia=210|Montenegro=213|Montserrat=211|Morocco=4|Mozambique=218|Namibia=262|Nauru=224|Nepal=138|Netherlands=52~105_106_108_109_110_111_112_113_114_115_116_117|New Caledonia=219|New Zealand=30|Nicaragua=227|Niger=222|Nigeria=5~214_215_216_217_218_219_220_221_222_223_224_225_226_227_228_229_230_231_232_233_234_235_236_237_238_239_240_241_242_243_244_245_246_247_248_249_250_251|Niue=220|Norfolk Island=221|Northern Mariana Islands=164|Norway=56~100_101_102_103_104_86_87_88_89_90_91_92_93_94_95_96_97_98_99|Oman=18|Pakistan=31~269_270_271_272_273_274_275_276|Palau=230|Palestinian territories=279|Panama=133|Papua New Guinea=101|Paraguay=228|Peru=121|Philippines=32~252_253_254_255_256_257_258_259_260_261_262_263_264_265_266_267_268|Pitcairn Islands=229|Poland=57~317_318_319_320_321_322_323_324_325_326_327_328_329_330_331_332|Portugal=58|Puerto Rico=235~40|Qatar=19~79_80_81_82_83_84_85|Réunion=232|Romania=59|Russia=60|Rwanda=236|Saint Barthélemy=276|Saint Helena=241|Saint Kitts and Nevis=238|Saint Lucia=245|Saint Martin=273|Saint Pierre and Miquelon=237|Saint Vincent and the Grenadines=258|Samoa=265|San Marino=243|Sao Tome and Principe=253|Saudi Arabia=20~128_129_130_131_132_133_134_135_136_137_138_139_140|Senegal=240|Serbia=233|Seychelles=239|Sierra Leone=242|Singapore=33|Sint Maarten=272|Slovakia=120|Slovenia=99|Solomon Islands=153|Somalia=244|South Africa=6|South Georgia and the Islands=247|South Sudan=280|Spain=61~333_334_335_336_337_338_339_340_341_342_343_344_345_346_347_348_349_350_351|Sri Lanka=34|Sudan=103|Suriname=225|Svalbard=246|Swaziland=266|Sweden=62~352_353_354_355_356_357_358_359_360_361_362_363_364_365_366_367_368_369_370_371_372|Switzerland=92|Syria=95|Taiwan=130|Tajikistan=248|Tanzania=118|Thailand=35|Togo=252|Tokelau=250|Tonga=251|Trinidad and Tobago=102|Tunisia=78|Turkey=63|Turkmenistan=136|Turks and Caicos Islands=249|Tuvalu=255|UAE=80~72_73_74_75_76_77_78|Uganda=132|UK=64~54_55_56_57_58_59_60_61_62_63_64_65|Ukraine=65|United States Minor Outlying Islands=281|Uruguay=122|USA=67~1_10_11_12_13_14_15_16_17_18_19_2_20_21_22_23_24_25_26_27_28_29_3_30_31_32_33_34_35_36_37_38_39_4_41_42_43_44_45_46_47_49_5_50_51_52_53_6_7_8_9|Uzbekistan=257|Vanuatu=223|Venezuela=76|Vietnam=36|Virgin Islands (US)=260~48|Wallis and Futuna=263|Western Sahara=264|Yemen=86|Zambia=85|Zimbabwe=7',
    SortMetric => 10,
};

$standard_tokens{nationality} = {
    Type =>   'List',
    Label =>  'Nationality',
    Default => '0',
    Values => 'Not specified=|All=0|Africa=10001|Asia & Pacific=10002|Europe & CIS=10003|Middle East=10004|North America=10005|South America=10006|Afghan=142|Albanian=37|Algerian=8|American=67|American Islander=281|American Northern Mariana Islander=164|American Samoan=144|Andorran=143|Angolan=2|Anguillan=145|Argentinean=70|Armenian=38|Australian=22|Australian (Christmas Islander)=200|Australian (Cocossian)=162|Australian (Heard Island and McDonald Islands)=191|Australian (Norfolk Islander)=221|Austrian=39|Azerbaijani=40|Bahamian=150|Bahraini=10|Bangladeshi=23|Barbadian=148|Barbudan=141|Belarusian=115|Belgian=41|Belizean=151|Beninese=152|Bermudian=149|Bhutanese=154|Bolivian=131|Bonaire, Sint Eustatius and Saba Islander=274|Brazilian=71|British=64|British (Guernsey)=278|British (Jersey)=195|British Gibraltarian=181|British Indian Ocean Territory Islanders=193|British Manx=192|British South Georgia Islanders=247|British Turks and Caicos Islander=249|British Virgin Islander=259|Bruneian=156|Bulgarian=44|Burkinabe=256|Burmese=128|Burundian=157|Cambodian=124|Cameroonian=3|Canadian=68|Cape Verdian=167|Caymanian=161|Central African=165|Chadian=158|Chilean=72|Chinese=24|Chinese (Macau)=208|Chinese Hong Konger=190|Colombian=73|Comoran=163|Congolese (DRC)=159|Congolese (Republic)=160|Costa Rican=93|Croatian=45|Cuban=166|Curaçaoan=271|Cypriot=106|Czech=113|Danish=47|Djibouti=169|Dominican=170|Dominican (Dominica)=171|Dutch=52|Dutch Arubain=140|East Timorese=254|Ecuadorean=74|Egyptian=11|Emirati=80|Equatorial Guinean=109|Eritrean=172|Estonian=135|Ethiopian=173|Falkland Islander=176|Faroese=125|Fijian=175|Filipino=32|Finnish=129|Finnish Ålandish=146|French=49|French (Saint Pierre and Miquelon)=237|French Guianese=174|French Polynesian=178|French Southern and Antarctic Islanders=179|Gabonese=107|Gambian=180|Georgian=98|German=51|Ghanaian=87|Greek=77|Greenlandic=183|Grenadian=182|Guadeloupian=184|Guamanian=185|Guatemalan=186|Guinea Bissauan=231|Guinean=187|Guyanese=188|Haitian=189|Herzegovinian=43|Honduran=91|Hungarian=53|Icelander=117|I-Kiribati=199|Indian=25|Indonesian=26|Iranian=12|Iraqi=13|Irish=83|Israeli=14|Italian=54|Ivorian=194|Jamaican=196|Japanese=27|Jordanian=15|Kazakhstani=55|Kenyan=127|Kittian and Nevisian=238|Kosovan=270|Kuwaiti=16|Kyrgyzstani=197|Laotian=201|Latvian=100|Lebanese=89|Liberian=202|Libyan=17|Liechtensteiner=203|Lithuanian=90|Luxembourger=205|Macedonian=116|Mahorais=209|Malagasy=206|Malawian=212|Malaysian=29|Maldivan=217|Malian=214|Maltese=88|Marshallese=234|Martiniquais=207|Mauritanian=216|Mauritian=134|Mexican=79|Micronesian=177|Moldovan=42|Monegasque=215|Mongolian=210|Montenegrin=213|Montserratian=211|Moroccan=4|Mosotho=204|Motswana=126|Mozambican=218|Namibian=262|Nauruan=224|Nepalese=138|New Caledonian=219|New Zealander=30|New Zealander Cook Islander=168|Nicaraguan=227|Nigerian=5|Nigerien=222|Niuean=220|Ni-Vanuatu=223|None (Antarctica)=147|None (Holy See)=261|North Korean=198|Norwegian=56|Norwegian (Svalbard)=246|Norwegian Bouvet Islander=155|Omani=18|Pakistani=31|Palauan=230|Palestinian=279|Panamanian=133|Papua New Guinean=101|Paraguayan=228|Peruvian=121|Pitcairn Islander=229|Polish=57|Portuguese=58|Puerto Rican=235|Qatari=19|Reunionese=232|Romanian=59|Russian=60|Rwandan=236|Sahrawian=264|Saint Barthélemy=276|Saint Helenian=241|Saint Lucian=245|Saint Martiner=273|Saint Vincentian=258|Salvadoran=137|Sammarinese=243|Samoan=265|Sao Tomean=253|Saudi Arabian=20|Senegalese=240|Serbian=233|Seychellois=239|Sierra Leonean=242|Singaporean=33|Sint Maatener=272|Slovak=120|Slovene=99|Solomon Islander=153|Somali=244|South African=6|South Korean=28|South Sudanese=280|Spanish=61|Sri Lankan=34|Sudanese=103|Surinamer=225|Swazi=266|Swedish=62|Swiss=92|Syrian=95|Tadzhik=248|Taiwanese=130|Tanzanian=118|Thai=35|Togolese=252|Tokelauan=250|Tongan=251|Trinidadian=102|Tunisian=78|Turkish=63|Turkmen=136|Tuvaluan=255|Ugandan=132|Ukrainian=65|Uruguayan=122|Uzbekistani=257|Venezuelan=76|Vietnamese=36|Virgin Islander=260|Wallis and Futuna Islander=263|Yemeni=86|Zambian=85|Zimbabwean=7',
    SortMetric => 11,
};

$standard_tokens{work_in_eu} = {
    Type        => 'List',
    Label       => 'Right to work in European Union',
    Values      => 'No=|Yes=on',
    SortMetric  => 12,
};

1;

package Stream::Templates::longlist;
use strict;
use base qw(Stream::Engine);

use HTTP::Response;
use JSON;

use Carp;

use vars qw{%standard_tokens};

use Log::Any qw/$log/;

=head1 NAME

Stream::Templates::longlist - Expose longlist stored candidates to the interface

=cut

# 50 results per page for this feed.
sub results_per_page{ 50 ; }

=head2 search_begin_search

This will die violently if:

 - there is no user in this feed.

 - there is no long list ID as a token on this feed

 - Or the list deosnt belong to the user.

=cut

sub search_begin_search{
    my ($self) = @_;
    my $longlist_id = $self->token_value('longlist_id');

    my $longlist = $self->user_object()->longlists()->find($longlist_id);
    unless( $longlist ){
        warn("Cannot find long list for longlist_id = ".( $longlist_id // 'UNDEF' )." defaulting to the first one" );
        $longlist = $self->user_object()->longlists()->first() // confess("User hasnt got longlist");
    }

    my $page = $self->token_value('requested_page') || 1;
    $log->info("Will query page = $page");
    my $candidates = $longlist->candidates->search({} , { order_by => { '-desc' => 'insert_datetime' },
                                                          page => $page,
                                                          rows => $self->results_per_page() });

    $self->total_results($candidates->pager()->total_entries());

    while( my $longlist_candidate = $candidates->next() ){
        my $candidate = $self->new_candidate();
        $candidate->attributes(
                               %{ $longlist_candidate->candidate_content() },
                               viewed_destination => 'longlist',
                               stream2_longlist_id => $longlist_id,
                               stream2_longlist_candidate_id => $longlist_candidate->candidate_id(),
                              );
        $self->add_candidate( $candidate );
    }
}

sub search_specific_page{
    my ($self, $page) = @_;
    # Set an extra token and call
    # search_begin_search as usual.
    $self->token_value( 'requested_page' , $page);
    return $self->search_begin_search();
}


=head2 download_cv

Returns an L<HTTP::Response> containing the CV from the given candidate
(in the case of Stream2, this is a L<Stream2::Results::Result>).

=cut

sub download_cv{
    my ($self, $candidate) = @_;
    confess("This template cannot download a candidate by itself. Use the candidate destination");
}

sub download_profile {
    my ( $self, $candidate ) = @_;
    confess("This template cannot download a profile by itself. Use the candidate destination");
}

1;

__END__

package Stream::Templates::cb_mysupply;

=head1 NAME

Stream::Templates::cb_my_supply - Career Builder My Supply

=head1 CONTACT

Candidate Data Processing CandidateDataProcessing@careerbuilder.com

=head1 Credentials

    Account DID: AKD3FF709ZM3TNTHVZV
    Deprecated developer ID: WDHS2MY663RSPVY9MPQ0
    Manage Credentials: https://apimanagement.cbplatform.link/#/oauth/lookup

=head1 OVERVIEW

    Authenticates using Bean::OAuth::CareerBuilder. Our client ID specially whitelisted enabling us to authenticate users with usermail and AccountDID instead of OAuth.

=cut

use strict;
use warnings;
use utf8;

use base qw(Stream::Engine::API::XML::FVP);

use Stream::Constants::SortMetrics qw(:all);
use POSIX;

use Data::Dumper;
use DateTime;
use DateTime::Format::ISO8601;

use Log::Any qw/$log/;

use MIME::Base64;

use Stream2::O::File;

use URI;
use URI::Template;
use URI::Encode;
use XML::LibXML;
use Stream2::Translations qw/t9np/;
use List::Util;
use List::MoreUtils;
use Bean::OAuth::CareerBuilder;

sub _auth_header {
    my ($self) = @_;
    my %auth_config;
    my $cache;
    my $autotoken_key = 'autotoken';
    if($self->_use_cb_oauth_flow) {
        $log->info('Using OneAIM authentication');
        %auth_config = (
            %{ $self->config->{oauth}->{oneiam} },
            auth_code => $self->token_value('cb_user_code'),
        );
        $cache = $self->user_object->stream2->users_cache;
        $autotoken_key = 'oneiam_autotoken';
    } else {
        $log->info('Using client-flow authentication');
        %auth_config = %{$self->config->{oauth}->{default}};
        $cache = $self->user_object->stream2->stream2_cache;
    }

    $self->{$autotoken_key} //= Bean::OAuth::CareerBuilder->new(%auth_config)
        ->get_auto_token($cache);

    my $access_token = $self->{$autotoken_key}->token;
    return (Authorization => 'Bearer ' . $access_token);
}

################ TOKENS #########################
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

# FOR mysupply, we are only going to show filters with counts, lets do
# some amazing dynamic facet creation
# simple, below we have our desired sort order and label for the facets we know about
# if the facet is returned from my supply and has 'Multivalue' at the end of the name
# then its a multilist, otherwise its a list -> EASY PEASY

our %CB_TOKEN_ORDER = (
    JobReqIDs                => 10,
    Tags                     => 20,
    YearsExperienceRange     => 30,
    EducationLevel           => 40,
    SkillsV4                 => 50,
    CaroteneTitlesV3         => 60,  # Job titles
    Vendors                  => 80,
    TopSchoolMultivalue      => 90,
    TopMajorMultivalue       => 120,
    NaicsCode                => 130, # Industry
    CompanyName              => 140,
    CompanySizeRange         => 150,
    DocumentType             => 160,
    ContactDate              => 170,
    ApplicationJobSource     => 180,
    ApplicationJobTitle      => 190,
    Country                  => 200,
    State                    => 210,
    MSA                      => 220,
    Subsource                => 230,
    CandidateStatus          => 240,
);

our %CB_TOKEN_LABELS = (
    Tags                     => t9np('feed.cb_mysupply', 'Tags/Hotlists'),
    CaroteneTitlesV3         => t9np('feed.cb_mysupply', 'Job Titles'),
    SkillsV4                 => t9np('feed.cb_mysupply', 'Skills'),
    CompanyName              => t9np('feed.cb_mysupply', 'Company Name'),
    NaicsCode                => t9np('feed.cb_mysupply', 'Industry (NAICS)'),
    CompanySizeRange         => t9np('feed.cb_mysupply', 'Company Size'),
    EducationLevel           => t9np('feed.cb_mysupply', 'Education Level'),
    YearsExperienceRange     => t9np('feed.cb_mysupply', 'Experience'),
    TopSchoolMultivalue      => t9np('feed.cb_mysupply', 'School'),
    TopMajorMultivalue       => t9np('feed.cb_mysupply', 'Major'),
    Country                  => t9np('feed.cb_mysupply', 'Country'),
    State                    => t9np('feed.cb_mysupply', 'State'),
    MSA                      => t9np('feed.cb_mysupply', 'MSA'),
    Vendors                  => t9np('feed.cb_mysupply', 'System Source'),
    DocumentType             => t9np('feed.cb_mysupply', 'Document Type'),
    ContactDate              => t9np('feed.cb_mysupply', 'Last Contacted'),
    JobReqIDs                => t9np('feed.cb_mysupply', 'Shortlisted Candidates'),
    ApplicationJobTitle      => t9np('feed.cb_mysupply', 'Application Job Title'),
    ApplicationJobSource     => t9np('feed.cb_mysupply', 'Application Job Source'),
    Subsource                => t9np('feed.cb_mysupply', 'Subsource'),
    CandidateStatus          => t9np('feed.cb_mysupply', 'Candidate Status'),
);

our %CB_TOKEN_TYPES = ( Tags => 'tag',
                        ContactDate => 'list',
                        JobReqIDs => 'list'
                      );


%authtokens = (
    cb_mysupply_account_did => {
        Label => 'Account DID',
        Type  => 'Text',
        Size  => 10,
        Mandatory  => 1,
    },
);

$standard_tokens{sort_by} = {
    Label   => 'Sort By',
    Type    => 'Sort',
    Default => '',
    Values  => 'Relevancy=|Distance=latlong|Degree Level=maxeducation|Candidate Name=contactname|Recent Job Title=jobtitle|Company Name=normalizedcompanyname|Email=email|Data Source=vendorname',
    SortMetric  => $SM_SORT
};


$standard_tokens{sort_direction} = {
    Label   => 'Sort Direction',
    Type    => 'List',
    Values  => 'Ascending=asc|Descending=desc',
    SortMetric  => 195 # should be after the Application Job Title
};

# check Stream2::Results::Result::set_canonical_values for the full list
# of canonical names
my %canonical_attr_mappings = (
    name      => 'name',
    city      => 'city',
    state     => 'state',
    postal_code  => 'postcode',
    telephone => 'telephone',
    education_level => 'education_level',
    last_activity   => 'raw_last_login',
    employer  => 'employer',
    job_title => 'job_title',
);

sub _use_cb_oauth_flow {
    my $self = shift;
    return $self->token_value('cb_user_code') ? 1 : 0;
}

sub config {
    my $self = shift;
    return $self->feed_config($self->destination);
}

sub base_url {
    my $self = shift;
    return URI->new($self->config->{base_url});
}


sub search_url {
    my $self = shift;
    my $url = $self->base_url;
    $url->path_segments($url->path_segments, 'search');
    return $url;
}

sub search_fields {
    my $self = shift;

    my $user = $self->user_object();
    my $s2 = $user->stream2();
    my %fields = (
        'Keywords' => $self->token_value('keywords') || '',
        'Rows' => $self->results_per_page(),
        'PageNumber' => $self->current_page(),
        'AccountDID' => $self->token_value('account_did') || '',
        'UseActiveSupply' => 'FALSE',
        'MostRecentActivity' => $self->most_recent_activity_mappings(),
        'TagsReturnedFacetCount' => 500
    );

    if( $self->_use_cb_oauth_flow ) {
        # we do not want these keys
        delete $fields{AccountDID};
    }

    # Find custom fields via their prefix.
    my @token_names = keys %{$self->tokens()};
    my @custom_fields;
    foreach my $token_name (@token_names) {
        # Tokens automatically get our destination prepended.
        my $prefix = $self->destination . '_' .  $self->_custom_field_prefix();
        if($token_name =~ m/\A\Q$prefix\E(.+)\z/) {
            $self->log_debug('Adding custom field %s to search fields.', $token_name);
            #CustomField keys correspond to their node names.
            push @custom_fields, map {$1 . ':' . $_} $self->get_token_value($token_name);
        }
    }


    # Find custom fields via their prefix.
    my @attributes;
    foreach my $token_name (@token_names) {
        # Tokens automatically get our destination prepended.
        my $prefix = $self->destination . '_' .  $self->_attribute_field_prefix();
        if($token_name =~ m/\A\Q$prefix\E(.+)\z/) {
            $self->log_debug('Adding custom field %s to search fields.', $token_name);
            #CustomField keys correspond to their node names.
            push @attributes, map {$1 . ':' . $_} $self->get_token_value($token_name);
        }
    }

    $fields{Attributes} = \@attributes;
    $fields{CustomFields} = \@custom_fields;

    $fields{CB_semantic_query} = $self->token_value('CB_semantic_query') // $self->token_value('keywords');

    if( my $value = $self->token_value('ofccp_context') ){
        $fields{'OFCCPSearchTitle'} = $value;
    }

    $fields{useremail} = $user->contact_email;

    # The job_requisition_id parameter.
    my $job_requisition_id = scalar( $self->token_value('cb_mysupply_JobReqIDs') ) || scalar(  $self->token_value('job_requisition_id' ) );
    if( $job_requisition_id  ){

        # Set the right token to be picked up later.
        $self->token_value('cb_mysupply_JobReqIDs' => $job_requisition_id );

        $fields{JobReqIDs} = $job_requisition_id;
        my ( $list_name , $advert_id ) = split(':' , $job_requisition_id);
        # Definitely login_provider here, cause cheese-sandwiches (aka adverts) are
        # ripple dependant
        my $advert = $self->user_object()->login_provider()->find_advert($self->user_object(),
                                                                         $advert_id,
                                                                         { list_name => $list_name });
        if( $advert ){
            $log->trace("This is about advert ".Dumper($advert));
            $self->results()->add_notice({ level => 'info' , message => $s2->__px('feed.cb_mysupply', 'Candidates for "{jobtitle}"' , jobtitle =>  $advert->{stream2_jobtitle}) });
        }else{
            $self->results()->add_notice( $s2->__px('feed.cb_mysupply', 'No advert could be found with {job_requisition_id}. Do you have access to this?', job_requisition_id => $job_requisition_id) );
        }
    }

    my $location_id = $self->token_value('location_id');
    if ( $location_id ) {
        my $location = $self->location_api()->find( { id => $location_id } );

        my @location_parts = ($location->name);

        push(@location_parts, $location->county->name)
          if $location->county && $location->county->name ne $location->name;

        push(@location_parts, $location->country->name)
          if $location->country
          && $location->country->name ne $location_parts[-1];

        $fields{Location} = join(', ', @location_parts);

        if( $self->create_location_within_tokens() ){
            my $miles = int( $self->token_value('location_within_miles') );
            if ( $miles > 500 ) {
                $fields{'Radius'} = 500;
                $self->results()->add_notice( $s2->__p('feed.cb_mysupply', 'mySupply supports radiuses of 10 to 500 miles. We automatically capped your search to 500 miles.') );
            }
            elsif ( $miles < 10 ) {
                $fields{'Radius'} = 10;
                $self->results()->add_notice( $s2->__p('feed.cb_mysupply', 'mySupply supports radiuses of 10 to 500 miles. We automatically raised your search to 10 miles.') );
            }
            else { $fields{'Radius'} = $miles; }
        }
    }
    # Dynamic including of CB_TOKENS, by the fact that they are in the CB_TOKEN_ORDER.
    map {
        $fields{ $_ } = join("|", $self->token_value( $_ ) );
    } keys %CB_TOKEN_ORDER;

    $fields{'sortField'} = $self->token_value('sort_by');
    $fields{'sortDirection'} = $self->token_value('sort_direction');

    while (my ($key, $value) = each %fields) {
        if (( $value // '' ) eq '') {
            delete $fields{$key};
        }
    }

    return %fields;
}

# maps the cv_updated_within token to tthe correct vaules used in the
# MostRecentActivity search field

sub most_recent_activity_mappings {
    my ( $self ) = @_;
    my $cv_updated_within = $self->token_value( 'cv_updated_within' );

    if ( ! $cv_updated_within || ( $cv_updated_within eq 'ALL' ) ){
        return 'all time';
    }

    # 1, 2 and 3 months havs a slightly different format.
    return '30 days' if $cv_updated_within eq '1M';
    return '60 days' if $cv_updated_within eq '2M';
    return '90 days' if $cv_updated_within eq '3M';

    my $from_dt = $self->cv_updated_within_to_datetime($cv_updated_within);

    return $from_dt->truncate( to => 'day' )->strftime('%Y-%m-%d');
}


sub search_submit {
    my $self = shift;
    my %fields = $self->search_fields();

    my $keyword_length = length URI::Encode->new->encode($fields{Keywords});
    $keyword_length ||= 0;
    if( $keyword_length > 1155) {
        $self->throw_keywords_too_long('Keywords character limit reached.');
    }

    my $search_url = $self->search_url;
    $search_url->query_form( $self->search_fields );
    my $response = $self->get($search_url, $self->_auth_header);
    $self->recent_content($response->decoded_content);
    return $response;
}

# Add and update user-specific facets supplied by the board
sub before_scraping_results {
    my $self = shift;

    my %account_facet_groups = %{$self->get_account_value('account_facets') || {}};
    if(%account_facet_groups) {
        $self->log_info('Restoring account facets from cache');
    } else {
        $self->log_info('Fetching and caching all account facets');

        # Make a request with 0 filters to get all facets.
        # The facets returned are a summary of those applicable to the candidates
        # found by our search. A search with 0 filters should return all candidates
        # and therefore all possible facets.
        my $facet_url = $self->search_url;
        my %facet_params = (
            AccountDID      => $self->token_value('account_did') || '',
            Rows            => 0, # We don't need candidates in this request
            UseActiveSupply => 'FALSE',
            useremail       => $self->user_object->contact_email,
        );
        if($self->_use_cb_oauth_flow) {
            delete $facet_params{AccountDID};
        }

        $facet_url->query_form(%facet_params);
        my $xml = $self->agent->get($facet_url, $self->_auth_header)->decoded_content;
        my $doc = XML::LibXML->load_xml(string => $xml);
        %account_facet_groups = $self->_mysupply_parse_facets( $doc->findnodes('/Response/Data/SuggestedFilters'));
        # Cache for an hour
        $self->set_account_value('account_facets', \%account_facet_groups, 3600);
    }
    # Flatten the hash of arrayRefs into array of facets.
    my @account_facets = map { @$_ } values %account_facet_groups;

    # Get facets for current search
    my %search_facet_groups = $self->_mysupply_parse_facets( $self->findnodes( '/Response/Data/SuggestedFilters' ) );
    # Flatten the hash of arrayRefs into array of facets.
    my @search_facets = map { @$_ } values %search_facet_groups;

    foreach my $search_facet (@search_facets) {

        # Clear options with 0 results
        $search_facet->{Options} = [grep {defined $_->[2] && int $_->[2]} @{$search_facet->{Options}}];

        my @selected_values = $self->get_token_value($search_facet->{Id});

        foreach my $selected_value (@selected_values) {
            # If the corresponding option for this value is not present,
            # fetch it from account facets and add it with a count of 0

            # Skip if the option is present
            next if List::Util::any {$_->[1] eq $selected_value} @{$search_facet->{Options}};

            # Find the option from account facets
            my $account_facet = List::Util::first {$_->{Id} eq $search_facet->{Id}} @account_facets;
            my $option = List::Util::first { $_->[1] eq $selected_value } @{$account_facet->{Options}};

            #Set the count to 0
            $option->[2] = "0";

            push @{$search_facet->{Options}}, $option;
        }
    }

    my $has_custom_fields = List::Util::any {
        @{$_->{Options}}
    } ( @{$search_facet_groups{custom_fields}}, @{$search_facet_groups{attributes}} );

    my $has_campaign_attributes = List::Util::any {
        @{$_->{Options}}
    } @{$search_facet_groups{campaign_attributes}};

    #Separate custom_fields and campaign_attributes
    # with a heading when they are present.
    my @headings = ();
    if($has_custom_fields) {
        push(
            @headings,
            {
                Name       => 'custom_fields_label',
                Type       => 'heading',
                Label      => 'Custom Questions',
                SortMetric => 400 # One above custom fields
            }
        );
    }
    if($has_campaign_attributes) {
        push(
            @headings,
            {
                Name       => 'campaign_attribute_label',
                Type       => 'heading',
                Label      => 'Campaign Details',
                SortMetric => 500 # One above campaign attributes
            }
        );
    }

    $self->results->facets(
        [@search_facets, @headings]
    );
}

sub _mysupply_parse_facets {
    my ( $self, $facet_parent_node ) = @_;

    return () unless $facet_parent_node;
    # Inject this facet array in the results,
    # regardless of what happens later.

    # Extra facets from SuggestedFilters

    my @suggested_filter_nodes = $facet_parent_node->findnodes('./*');
    # User-specified facets
    my @custom_field_nodes = $facet_parent_node->findnodes('./CustomFields/*');

    my @campaign_attribute_nodes;
    my @attribute_nodes;

    my %campaign_label_map = $self->campaign_attribute_label_map;
    foreach my $node ($facet_parent_node->findnodes('./Attributes/*')){
        my $label = $node->getAttribute('label');

        if(my $new_label = $campaign_label_map{$label}){
            $node->setAttribute(label => $new_label);
            push @campaign_attribute_nodes, $node;
        } else {
            push @attribute_nodes, $node;
        }
    }

    my @suggested_filters_facets = grep {$_} map {
        $self->_build_suggested_filter_facet($_);
    } @suggested_filter_nodes;

    my @custom_fields_facets = grep {$_} map {
        $self->_build_custom_field_facet($_, $self->_custom_field_prefix);
    } @custom_field_nodes;

    my @campaign_attribute_facets = grep {$_} map {
        $self->_build_custom_field_facet($_, $self->_attribute_field_prefix);
    } @campaign_attribute_nodes;

    my @attribute_facets = grep {$_} map {
        $self->_build_custom_field_facet($_, $self->_attribute_field_prefix);
    } @attribute_nodes;



    $self->log_info(
        'Found %d suggested filters, %d custom fields, %d attributes and %d campaign_attributes',
        scalar @suggested_filters_facets,
        scalar @custom_fields_facets,
        scalar @attribute_facets,
        scalar @campaign_attribute_facets
    );

    # Nasty customisation for Hilton (SEAR-1731).
    # Put the "Talent Network" facet first and "Member Joined Language" second
    # if they're both present
    $self->_order_facets(
        \@custom_fields_facets,
        [
            'Talent Network',
            'Member Joined Language'
        ]
    );

    # Incremental sortmetrics
    # Exclusive to the initial value which is
    # used by the heading.
    my $sort_metric = 401;
    for (@custom_fields_facets, @attribute_facets) {
        $_->{SortMetric} = $sort_metric;
        $sort_metric++ if $sort_metric < 499;
    }
    $sort_metric = 501;
    for (@campaign_attribute_facets) {
        $_->{SortMetric} = $sort_metric;
        $sort_metric++ if $sort_metric < 599;
    }

    return (
        suggested_filters   => \@suggested_filters_facets,
        custom_fields       => \@custom_fields_facets,
        attributes          => \@attribute_facets,
        campaign_attributes => \@campaign_attribute_facets
    );
}

# takes a facet list array ref and an arrayref with the facet labels in the desired order
# If all the labels are present in the facet list, order the list
# the items to order are put at the top of the list in the given order.
# Changes the facet list by reference
sub _order_facets {
    my ($self, $facet_list, $order) = @_;

    # Check that all facets to order are present
    my $all_present = List::Util::all {
        my $label = $_;
        List::Util::any {
            $_->{Label} eq $label;
        } @$facet_list;
    } @$order;

    return unless $all_present;

    # splice item from list then put back into list_name
    foreach my $label (reverse @$order) {

        my $facet_index = List::MoreUtils::first_index {
            $_->{Label} eq $label;
        } @$facet_list;

        my $facet = splice(
            @$facet_list,
            $facet_index,
            1
        );
        #put it back in the list
        unshift @$facet_list, $facet;
    }
}

sub _build_suggested_filter_facet {
    my ($self, $node) = @_;
    return if $node->nodeName eq 'CustomFields' || $node->nodeName eq 'Attributes';
    my $facet_name = $node->nodeName()
      or return;

    my $user = $self->user_object();
    my $stream2 = $user->stream2();

    # skip the old skills and carotene facets until they are deprecated
    return if ( $facet_name =~ m/\A(?:skills|normalizedjobtitle|onet(?:code|17)|MostRecentActivity)\z/i );

    my $facet_item = {
        # The sort order can be controlled by the HASH at the top of the feed
        SortMetric  => $CB_TOKEN_ORDER{$facet_name} || '300',
        # Lets have a nicer label for the facets we know about
        Label       => ( $CB_TOKEN_LABELS{$facet_name} ? $stream2->translations()->__p('feed.cb_mysupply', $CB_TOKEN_LABELS{$facet_name} ) : $facet_name ),
        # multivalue literally means nothing to us, they are all multilists
        Type        => $CB_TOKEN_TYPES{$facet_name} || 'multilist',
        Id          => 'cb_mysupply_' . $facet_name,
        Name        => $facet_name,
    };

    my (@values) = $self->token_value($facet_name);
    if (grep{ $_ eq $facet_item->{'Type'} } (  'multilist', 'tag' ) ) {
        my %selected_values = map { $_ => 1 } @values;
        $facet_item->{'SelectedValues'} = \%selected_values;
    }
    else {
        if ( scalar(@values) ) {
            $facet_item->{'Value'} = $values[0];
        }
        $facet_item->{'Options'} = [ ['--Please Select--',''] ];
    }

    foreach my $option_node ($node->childNodes) {
        # for each option [ 'label', 'name', 'count' ]
        if ( my $name = $option_node->findvalue('./@Name') ){
            push ( @{$facet_item->{Options}},[
                    $name,
                    $option_node->findvalue('./@Code'),
                    $option_node->textContent,
            ]);
        }
    }

    if( $facet_item->{Id} eq 'cb_mysupply_JobReqIDs' ){
        # Translate all options names and scrape the ones we do not have access to
        $facet_item->{Options} = [ grep{ $_->[0] ne '__PLEASE_SCRAPE_ME_OFF_' } map{ [ $self->_job_req_id_to_ref($_->[0]) , $_->[1], $_->[2] ] } @{$facet_item->{Options}} ];
    }
    elsif ( $facet_item->{Id} =~ m/\Acb_mysupply_(skillsv4|carotenetitlesv3|onet17)\z/i ) {
        $facet_item->{Options} = $self->_build_mysupply_taxonomy_options($facet_item->{Options}, $1);
    }

    # Facet option counts must be numeric for proper sorting
    foreach my $option (@{$facet_item->{Options}}) {
        $option->[2] = int $option->[2] if defined($option->[2]);
    }

    return $facet_item;
}

# give us a way of identifying custom fields.
sub _custom_field_prefix {
    return 'custom_field_';
}

sub _attribute_field_prefix {
    return 'attribute_';
}

sub _build_custom_field_facet {
    my ($self, $node, $prefix) = @_;
    my @options = $node->getElementsByTagName('Name');

    my $name = $node->nodeName;

    @options = map { [$_->getAttribute('Name'), $_->getAttribute('Code'), $_->textContent]} @options;

    return {
        Name       => $prefix . $name,
        Id         => $self->destination . $name,
        Label      => $node->getAttribute('label'),
        Type       => 'multilist',
        Options    => \@options,
    };
}
sub campaign_attribute_label_map {
    return (
        CAMPAIGN_DELIVERED => 'Campaign Tag',
        CAMPAIGN_INTEREST  => 'Campaign Activity'
    );
}

sub _job_req_id_to_ref{
    my ($self, $job_requisition_id) = @_;
    unless( $job_requisition_id =~ /^\w+:\S+/ ){
        $log->warn("'$job_requisition_id' doesnt look like a job_requisition_id. Returning it as-is");
        return $job_requisition_id;
    }

    my ( $list_name , $advert_id ) = split(':' , $job_requisition_id);

    if ( $list_name eq 'csp_job_shortlist' ) {
        my ($jobref) = $advert_id =~ m/(\w+)\/?/;
        return $jobref if $jobref;
    }

    # Definitely login_provider here, as finding cheese sandwiches, aka adverts
    # is ripple setting/login provider dependant.
    my $advert = $self->user_object()->login_provider()->find_advert($self->user_object(),
                                                                     $advert_id,
                                                                     { list_name => $list_name });
    if( $advert ){
        return $advert->{stream2_jobref};
    }

    $log->info("NO Advert found for $job_requisition_id. No access to that?");
    return '__PLEASE_SCRAPE_ME_OFF_';
}

=head2 _build_mysupply_taxonomy_options

Some facet options come with only the ids and don't include the labels.
The labels are available through careerbuilders api which we store in
a taxonomy table in Search.

Will fetch the matching ids for the type given from the taxonomy table and return
an arrayref of options for the facet.

Taxonomy ids will be uppercased for the db query due to careerbuilders api
returning them in uppercase (which is where we fetched the taxonomies from).

=cut

sub _build_mysupply_taxonomy_options {
    my($self, $facet_options, $type) = @_;

    $type =~ s/titles//i;

    $self->log_info("Fetching labels for facet '$type'");

    my @taxonomy_ids = map { uc $_->[1] } @$facet_options;

    my $s2 = $self->user_object()->stream2();
    my $resultset = $s2->factory('Taxonomy')->search({
            field_type => lc $type,
            taxonomy_id => { -in => \@taxonomy_ids },
        }
    );

    my %label_map = map { $_->taxonomy_id => $_->label } $resultset->all();

    foreach my $option ( @$facet_options ) {
        my $label = $label_map{uc $option->[1]};
        unless ( $label ) {
            $self->log_warn("cannot find label stored in taxonomies for %s with id %s", $type, $option->[1]);
            next;
        }
        $option->[0] = $label;
    }

    return $facet_options;
}

sub search_number_of_results_xpath {
    my $self = shift;
    return '//Results/TotalRecords';
}

sub results_per_page{
    my $self = shift;

    #  return a specified number of results per page, if it exists
    return $self->token_value('results_per_page') + 0 if $self->token_value('results_per_page');

    # SEAR-1039 - company wants to bulk message without paging
    if ( $self->company() eq 'trugreen' ) {
        return 150;
    }
    return 20;
}

sub search_success { return qr/<Results>|Not enough data matched your search/; }

sub scrape_candidate_xpath { return '/Response/Data/Results/Applications/Application'; }
sub scrape_candidate_details {
    return (
        candidate_id            => './DID/text()',
        name                    => './Name/text()',
        email                   => './Email/text()',
        city                    => './City/text()',
        postcode                => './Zip/text()',
        state                   => './State/text()',
        raw_last_login          => './MostRecentActivity/text()',
        telephone               => './Phone/text()',
        vendor_key              => './VendorKey/text()',
        vendor_name             => './VendorName/text()',
        score                   => './Score/text()',
        source                  => './Source/text()',
        education_level         => './EducationLevel/text()',
        job_title               => './JobTitle/text()',
        application_jobtitle    => './ApplicationJobTitle/text()',
        application_source      => './ApplicationJobSource/text()',
        education_level         => './EducationLevel/text()',
        employer                => './Employer/text()',
        tags_string             => './Tags/text()',
        last_contact_date       => './ContactDate/text()',
        subsource               => './Subsource/text()',
        candidate_status        => './CandidateStatus/text()',
        attributes => [
            './Attributes/*' => {
                label => './Label/text()',
                value => './Value/text()'
            }
        ],
        custom_fields => [
            './CustomFields/*' => {
                label => './Label/text()',
                value => './Value/text()'
            }
        ]
    );
}

{
    # Not rebuilding that each time.
    my $iso8601 = DateTime::Format::ISO8601->new();


    sub scrape_fixup_candidate {
        my $self = shift;
        my $candidate = shift;

        # A tags_string has to be split on | (pipe)
        if( my $tag_string = $candidate->get_attr('tags_string') ){
            my @tags = grep{ $_ } split(/\|/, $tag_string);
            foreach my $tag ( @tags ){
                $candidate->add_tag($tag);
            }
        }

        if( my $last_contact_date = $candidate->get_attr('last_contact_date') ){
            # The central space should not be there.
            $last_contact_date =~ s/(\d)\s(\d)/$1T$2/;
            $candidate->set_attr('last_contact_date_epoch' , $iso8601->parse_datetime($last_contact_date)->epoch());
        }

        if ( my $ofccp = $self->token_value('ofccp_context') ){
            $candidate->set_attr('ofccp_context' => $ofccp);
        }

        $candidate->has_profile(1);
        $candidate->has_cv(1);

        my %custom_fields = $self->_group_custom_fields(
            $candidate->attr('custom_fields') || []
        );

        my %attributes = $self->_group_custom_fields(
            $candidate->attr('attributes') || []
        );

        my %label_map = $self->campaign_attribute_label_map;
        my %campaign_attributes;
        while( my ($old_label, $new_label) = each %label_map) {
            if($attributes{$old_label}) {
                $campaign_attributes{$new_label} = delete $attributes{$old_label};
            }
        }

        %custom_fields = (%custom_fields, %attributes);
        $candidate->attr(custom_fields => \%custom_fields);
        $candidate->attr(campaign_attributes => \%campaign_attributes);

        # The board's live sends us 2017-02-23T14:26:40.000Z
        # The boad's staging sends up 2017-02-23
        # Turn the former into the latter.
        my $raw_last_login = $candidate->attr('raw_last_login');
        $raw_last_login =~ s/T.+$//;
        $candidate->attr(raw_last_login => $raw_last_login);

        # set the canonical values as the final step.
        $candidate->set_canonical_values(%canonical_attr_mappings);

        return $candidate;
    }
}

# Set max pages to total_results / results_per page
# with a max of 100
sub scrape_paginator {
    my $self = shift;
    my $results = $self->total_results();
    my $total_pages = $results / $self->results_per_page();
    $total_pages = ceil($total_pages);
    $total_pages = 100 if $total_pages > 100;

    $self->max_pages($total_pages);
    return;
}

sub download_profile_url {
    my ($self, $candidate) = @_;

    my $url = $self->base_url;
    $url->path_segments(
        $url->path_segments,
        'candidate',
        $candidate->attr('candidate_id'),
        'resume'
    );
    return $url;
}

sub download_profile_submit {
    my ($self, $candidate) = @_;
    my $url = $self->download_profile_url($candidate);
    my %fields = (
        inlinebase64 => 'true',
        detail       => 'true',
        useremail    => $self->user_object->contact_email,
        ( $candidate->attr('ofccp_context') ? ( OFCCPSearchTitle => $candidate->attr('ofccp_context') ) : () ),
        AccountDID => $self->token_value('account_did') || '',
    );
    delete $fields{AccountDID} if $self->_use_cb_oauth_flow;

    $url->query_form( %fields );

    $log->info("download_profile_url $url");

    return $self->get($url, $self->_auth_header);
}

# One of the many many calls downloading a profile makes..
sub scrape_fixup_candidate_profile {
    my($self, $candidate) = @_;

    if( ( $candidate->attr('has_image') || '' )  eq 'True' ){
        # The image counts as the other attachment.
        # This tells the frontend it can attempt fetching the other attachments.
        $candidate->has_other_attachments(1);
    }

    return $candidate;
}

sub download_profile_success {
    return qr/Resume/; # This is a regex.
}

sub scrape_download_profile_xpath {
    return '/Response/Data/Results/Applications/Application';
}

sub scrape_download_profile_details {
    return (
        profile => './Resume/text()',
        has_image => './IsImageFile/text()',
        cv_base64 => './ResumeBase64/text()',
    );
}

# Takes an ArryaRef of fields
# Returns a hash of arrayrefs of these field.
# grouped by label
sub _group_custom_fields {
    my ($self, $fields) = @_;

    my %groups;
    foreach my $field (@$fields) {
        my $label = $field->{label};
        $groups{$label} //= [];
        my $value = $field->{value};
        push @{$groups{$label}}, $value;
    }
    return %groups;
}


=head2 download_cv_submit

    Returns HTTP::Response
    Used in Stream::Engine::API::XML::do_download_cv() to grab the response from this routines request to download CV.
    We will initially request the base64 file, if response indicates this isn't available, we will
    perform another request for the text file.

=cut

sub download_cv {
    my ($self, $candidate) = @_;

    my $download_url = $self->download_profile_url($candidate);;
    my %fields = (
        inlinebase64 => 'true',
        useremail    => $self->user_object->contact_email,
        ( $candidate->attr('ofccp_context') ? ( OFCCPSearchTitle => $candidate->attr('ofccp_context') ) : () ),
        AccountDID => $self->token_value('account_did') || '',
    );
    delete $fields{AccountDID} if $self->_use_cb_oauth_flow;

    $download_url->query_form(%fields);
    my $response = $self->get($download_url, $self->_auth_header);

    my $dom = XML::LibXML->load_xml( string => $response->content );
    my $is_image = $dom->findvalue('string(//IsImageFile)');
    my $encoded_cv = $dom->findvalue('string(//ResumeBase64)');
    my $text_cv = $dom->findvalue('string(//Resume)');


    if( $encoded_cv && $is_image ne 'True' ) {
        $log->info('Downloading CV document');

        my $filename = $dom->findvalue('string(//FileName)');
        my $decoded_cv = MIME::Base64::decode_base64($encoded_cv);

        my $stream2 = $self->user_object->stream2;
        my $type = $stream2->mime_types()->mimeTypeOf( $filename );

        return HTTP::Response->new(
            200,
            'OK',
            [
                'Content-Disposition' => 'attachment; filename="' . quotemeta($filename) . '";',
                'Content-Type' => $type->type,
            ],
            $decoded_cv
        );
    } elsif ($text_cv) {
        $log->info('Returning plain text CV');

        my $filename = $self->destination . '_' . $candidate->candidate_id . '.txt';
        return HTTP::Response->new(
            200,
            'OK',
            [
                'content-type' => 'text/plain',
                'Content-Disposition' => 'attachment; filename=' . $filename,
            ],
            $text_cv
        );
    }

    $self->throw_no_cv('Candidate has no CV available');
}

=head2 _candidate_attachment

Returns the candidate's attacment as a Stream2::O::File

Usage:
    
    my $attachment = $self->_candidate_attachment($candidate);

=cut

sub _candidate_attachment {
    my ($self, $candidate) = @_;

    # Make sure to get the full profile.
    # That's the only way to get the 'has_image' property,
    # cause it's not in the straight candidate coming from the
    # results.
    $candidate = $self->download_profile( $candidate );
    my $has_image = $candidate->attr('has_image');
    unless( $has_image eq 'True' ){
        return 0;
    }

    # Candidate has an image.
    # The cv_base64 looks like:
    # 'cv_base64' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/7QA4UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAAA4QklNBCUAAAAAABDUHYzZjwCyBOmACZjs+EJ+/8AAEQgCVwMgAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABA
    my $cv_base64 = $candidate->attr('cv_base64') // '';
    my ($mime_type , $base64) = ( $cv_base64 =~ m/data:([^;]+);base64,(\S+)/ );
    unless( $mime_type && $base64 ){
        $log->warn("has_image was true but no image can be found in cv_base64");
        return 0;
    }

    my $type = $self->user_object->stream2()->mime_types()->type( $mime_type );
    unless( $type ){
        $log->warn("Found no registered mime type for $mime_type. Not generating any file");
        return 0;
    }

    # Pick the first extension.
    my ( $extension )  = $type->extensions();

    # Use candidate_id-name.extension as filename
    my $filename = $candidate->attr('candidate_id').'-'.$candidate->attr('name').'.'.$extension;

    return Stream2::O::File->new({
        name => $filename ,
        mime_type => $type->type() ,
        binary_content => MIME::Base64::decode_base64( $base64 )
    });
}

# See Stream::Templates::Engine
sub rendered_attachments{
    my ($self, $candidate) = @_;

    my $file = $self->_candidate_attachment($candidate)
      or return [];

    return [
        {
            rendered_html => $file->html_render,
            key           => 'attachment',
            filename      => $file->name,
        }
    ];
}

# See Stream::Templates::Engine
sub download_attachment {
    my ($self, $candidate, $key) = @_;
    return unless $key eq 'attachment';

    my $file = $self->_candidate_attachment($candidate)
      or return;

    return $file->to_download_response;
}

sub download_attachments {
    my ($self, $candidate) = @_;
    return [ $self->download_attachment($candidate, 'attachment')];
}

sub feed_identify_error {
    my ($self, $message, $content) = @_;
    $self->throw_known_internal('Please narrow your search with some Keywords, a Location or other filters') if ( $content =~ m/No filter specified/ );
    if($content =~ m/The keyword that you entered was invalid/){
        $self->throw_keywords_too_long('Keywords character limit is 1000 characters');
    }
    elsif($content =~ m/Invalid location/) {
        $self->throw_known_internal('Location not supported by board');
    }
    elsif($content =~ m/Invalid Account DID/) {
        $self->throw_login_error('Your mySupply subscription key is not valid');
    }
    elsif($content =~ m/Missing Account\s?DID/) {
        $self->throw_login_error('Restricted Access: You are not set up to access mySupply');
    }
    elsif($content =~ m/Badly Formatted Input: AccountDID is required/) {
        $self->throw_login_error('Restricted Access: You are not set up to access mySupply');
    }
    elsif($content =~ m/At this time your access has been blocked, Please contact/) {
        $self->throw_known_internal('Service temporarily unavailable. Please retry.');
    }
    elsif($content =~ m/no data received for default limit of \d+ seconds/) {
        $self->throw_timeout('Our request to the board timed out. Please retry.');
    }
}


=head2 get_company

Returns the MySupply company.

=cut

sub get_company{
    my ($self) = @_;
    return $self->company() || $self->user_object->group_identity();
}


=head2 mark_candidate_contacted

Wraps around the super methid (from L<Stream::Templates::Default>) to
tell MySupply (Though the TSImport API) that this candidate has been contacted.

=cut

sub mark_candidate_contacted{
    my ($self, $candidate) = @_;

    my $stuff = sub{
        $self->SUPER::mark_candidate_contacted($candidate);
        my $ts_client = $self->user_object()->stream2()->tsimport_api();
        my $company = $self->get_company();

        $ts_client->make_request(
                                 path => '/company/'.$company.'/candidate/MySupply/'.$candidate->candidate_id().'/last_contacted',
                                 method => 'POST',
                                 arguments => {
                                               date_time => DateTime->now()->iso8601().'Z'
                                              }
                                );

    };
    return $self->user_object->stream2->stream_schema->txn_do($stuff);
}

=head2 add_candidate_advert_identifier

Wraps around the super method to do some MySupply specific stuff.

=cut

sub add_candidate_advert_identifier{
    my ($self,$candidate, $advert_identifier) = @_;
    unless( $advert_identifier ){
        confess("Missing identifier");
    }
    my $stuff = sub{
        # Do the super thing first.
        $self->SUPER::add_candidate_advert_identifier($candidate, $advert_identifier);

        my $ts_client = $self->user_object->stream2->tsimport_api();

        $log->info("Adding identifier $advert_identifier to candidate ".$candidate->id()." using the TSImport API");

        my $company = $self->get_company();
        $ts_client->make_request(
            path    => '/company/'.$company.'/candidate/MySupply/'.$candidate->candidate_id.'/job_requisition_ids',
            method  => 'POST',
            arguments => { jobreqid => $advert_identifier }
          );
        return $advert_identifier;
    };
    return $self->user_object->stream2->stream_schema->txn_do($stuff);
}

=head2 tag_candidate

Updates the tags in MySupply through the TSImport API

=cut

sub tag_candidate{
    my ($self, $candidate, $tag) = @_;

    $log->infof("Tagging candidate '%s' with tag '%s' through MySupply search tsimport API",
        $candidate->id(),
        $tag->tag_name
    );

    my $ts_client = $self->user_object->stream2->tsimport_api();
    my $company = $self->get_company();
    my $resp = $ts_client->make_request(
        path    => '/company/'.$company.'/candidate/MySupply/'.$candidate->candidate_id.'/tags',
        method  => 'POST',
        arguments => { tag_name => $tag->tag_name }
    );

    $log->infof('Tagged candidate %s with "%s" successfully', $candidate->id(), $tag->tag_name);
    return $tag;
}

=head2 untag_candidate

Wraps around the super method (from Stream::Templates::Default) to also
update the tags in the talent search itself.

=cut

sub untag_candidate{
    my ($self, $candidate, $tag) = @_;

    unless ( $tag ) {
        $log->infof('No tag specified, will not untag for "%s"', $candidate->id())
        and return;
    }

    $log->infof("Untagging candidate '%s' with tag '%s' through MySupply search tsimport API",
        $candidate->id(),
        $tag->tag_name
    );

    my $ts_client = $self->user_object->stream2->tsimport_api();

    my $company = $self->get_company();
    my $resp = $ts_client->make_request(
        path    => '/company/'.$company.'/candidate/MySupply/'.$candidate->candidate_id.'/tags/tag?tag_name='.URI::Escape::uri_escape_utf8($tag->tag_name),
        method  => 'DELETE',
        arguments => {}
    );

    $log->infof('Untagged candidate %s from "%s" successfully', $candidate->id(), $tag->tag_name);
    return $tag;
}

1;

package Stream::Templates::Default::Helpers;

=head1 NAME

Stream::Templates::Default::Helpers - Implements stuff in Stream::Templates::Default

=head1 SYNOPSIS

  use Stream::Templates::Default::Helpers;

Implements extra stuff in Stream::Templates::Default via package switching.

Note that T::Defaults already uses T::Default::Helpers. So any attempt to use this module in
any other templates would emit a lot of warnings about methods being redefined.

=cut

1;

package Stream::Templates::Default;
use warnings;
use strict;

# Avoid warnings issue described in SYNOPSIS.
no warnings 'redefine';

use DateTime;
use Stream2;

use Stream::Keywords;
use List::MoreUtils qw(uniq);
use Log::Any qw/$log/;

# global helpers are in CAPS

sub COPY {
    my $self = shift;

    return (@_);
}

sub COMBINE_UNIQ_TOKENS {
    my $self = shift;

    my @values = uniq( grep { $_ } @_ );
    return @values;
}

sub TOKEN_DEFAULTS {
    my $self = shift;
    my $token_name = shift;

    my $token = $self->build_token( $token_name );
    return map { ref($_) eq 'ARRAY' ? @$_ : $_ } $token->default();
}

sub SPLIT_KEYWORDS {
    my $self = shift;
    my $keywords = shift;
    my $and_token = shift;
    my $or_token  = shift;
    my $not_token = shift;
    my $exact_token = shift;

    $log->debugf('Splitting keywords: [%s]', $keywords);

    my $q = Stream::Keywords->new( $keywords );

    if ( !$q->process_query() ) {
        $log->debug('Unable to split up the keywords');
        $self->throw_invalid_boolean_search('We could not process the query');
    }

    $log->debug( $q->diag() );

    my ($and_words)   = join(' ', $q->and_words() );
    $log->debugf('AND WORDS: [%s]', $and_words );
    $self->token_value( $and_token   => $and_words );

    my ($or_words)    = join(' ', $q->or_words() );
    $log->debugf('OR WORDS: [%s]', $or_words );
    $self->token_value( $or_token    => $or_words );

    my ($not_words)   = join(' ', $q->not_words() );
    $log->debugf('NOT WORDS: [%s]', $not_words );
    $self->token_value( $not_token   => $not_words );

    my ($exact_words) = join(' ', $q->exact_words() );
    $log->debugf('PHRASES: [%s]', $exact_words );
    $self->token_value( $exact_token => $exact_words );

    return;
}

# Salary helpers
sub SALARY_CURRENCY {
    my $self       = shift;
    my $token_cur  = shift || 'salary_cur';

    my @tokens_to_convert = @_;
    unless( @tokens_to_convert ){ @tokens_to_convert = qw/salary_from salary_to/; }

    my $currency = $self->build_token( $token_cur );

    $log->debugf( 'SALARY_CURRENCY> token %s, consultant selected %s', $token_cur, $currency->value() );

    if ( $currency->is_valid() ) {
        $log->debugf( 'SALARY_CURRENCY> %s currency is supported by this board', $currency->value() );
        return $currency->value();
    }

    $log->debugf( 'SALARY_CURRENCY> %s currency is NOT supported by this board, falling back to %s', $currency->value(), $currency->default() );

    foreach my $salary_token ( @tokens_to_convert ) {
        my $salary = $self->token_value( $salary_token );

        if ( $salary ) {
            # convert salary
            $log->debugf( "SALARY_CURRENCY> Converting '%s' %s %s to %s", $salary_token, $salary, $currency->value(), $currency->default() );
            my $converted_value = $currency->convert( $salary );
            $log->debugf( 'New value in %s for token %s is %s', $currency->default(), $salary_token, $converted_value );
            $self->token_value(
                $salary_token => $converted_value
            );
        }
    }

    return $currency->default();
}

sub SALARY_PER {
    my $self       = shift;
    my $token_per  = shift || 'salary_per';
    my $token_from = shift || 'salary_from';
    my $token_to   = shift || 'salary_to';

    my $salary_per = $self->build_token( $token_per );
    if ( $salary_per->is_valid() ) {
        return $salary_per->value();
    }

    $log->debugf( 'SALARY_PER> per %s is NOT supported by this board, falling back to per %s', $salary_per->value(), $salary_per->default() );

    foreach my $salary_token ( $token_from, $token_to ) {
        my $salary = $self->token_value( $salary_token );

        if ( $salary ) {
            # convert salary
            $log->debugf( 'SALARY_PER> Converting %s to per %s', $salary_token, $salary_per->default() );
            $self->token_value(
                $salary_token => $salary_per->convert( $salary ),
            );
        }
    }

    return $salary_per->default();
}

sub SALARY_BAND_BETWEEN {
    my $self       = shift;
    my $token_from = shift; # token to find options from (e.g. salary_to - NOT %salary_to%)
    my $currency   = shift; # currency                   (e.g. %salary_cur%)
    my $per        = shift; # salary per                 (e.g. %salary_per%)
    my $value      = shift; # salary_to/salary_from      (e.g. %salary_to%)

    my $salary_from = $self->SALARY_BUILD_TOKEN( $token_from, $currency, $per );

    $log->debugf( 'SALARY_BAND_BETWEEN> Returning the salary band that contains %s', $value );

    return $salary_from->first_between( $value );
}

sub SALARY_BAND_BELOW {
    my $self       = shift;
    my $token_from = shift;
    my $currency   = shift;
    my $per        = shift;
    my $value      = shift;

    if ( !defined( $value ) || $value eq '' ) {
        return undef;
    }

    my $salary_from = $self->SALARY_BUILD_TOKEN( $token_from, $currency, $per );

    $log->debugf('SALARY_BAND_BELOW> Returning the first option less than %s', $value );

    return $salary_from->first_less_than( $value );
}

sub SALARY_BAND_ABOVE {
    my $self       = shift;
    my $token_from = shift;
    my $currency   = shift;
    my $per        = shift;
    my $value      = shift;

    if ( !defined( $value ) || $value eq '' ) {
        return undef;
    }

    my $salary_from = $self->SALARY_BUILD_TOKEN( $token_from, $currency, $per );

    $log->debugf('SALARY_BAND_ABOVE> Returning the first option greater than %s', $value );

    return $salary_from->first_more_than( $value );
}

sub SALARY_BETWEEN {
    my $self = shift;
    my $token_name = shift;
    my $min  = shift;
    my $max  = shift;

    if ( !$min && !$max ) {
        return;
    }

    my $token = $self->build_token( $token_name );

    return $token->select_banded_between( $min, $max );
}

sub SALARY_BANDS_BETWEEN {
    my $self = shift;
    my $token_name = shift;
    my $min  = shift;
    my $max  = shift;

    # either specify both currency and per or neither of them
    my $currency = shift;
    my $per      = shift;

    if ( !$min && !$max ) {
        return;
    }

    my $token = $self->SALARY_BUILD_TOKEN( $token_name, $currency, $per );

    return $token->select_salaries_between( $min, $max );
}

sub SALARY_BUILD_TOKEN {
    my $self       = shift;
    my $token_from = shift;
    my $currency   = shift;
    my $per        = shift;

    my $token_ref = $self->token( $token_from );

    if ( !$token_ref ) {
        $log->errorf('SALARY_BUILD_TOKEN> Unable to find token [%s] - check that it is defined and has no typos', $token_from );
        die 'INTERNAL - unable to run SALARY_BUILD_TOKEN because we could not find the token';
    }
    my %token = %$token_ref;

    if ( exists( $token{ $currency.'_'.$per } ) ) {
        $log->debugf('SALARY_BUILD_TOKEN> Using custom options for currency: %s, Per: %s', $currency, $per );

        $token{Options} = $token{ $currency.'_'.$per };
    }
    else {
        $log->warnf('SALARY_BUILD_TOKEN> Not using custom options for currency:%s, per:%s',$currency, $per);
    }

    return $self->build_token( \%token );
}

# Will change the value of $token_name to the desired frequency
# e.g. salary_from token = 50000
# $self->SALARY_CHANGE_FREQUENCY('salary_from', 'annum', 'hour', 1);
# salary_from token will now be 24
sub SALARY_CHANGE_FREQUENCY {
    my $self = shift;
    my $token_name = shift;
    my $per = shift;
    my $target_per = shift;
    my $round = shift;

    return $self->token_value($token_name) if $per eq $target_per;

    my $value = $self->token_value($token_name);

    $log->infof("SALARY_CHANGE_FREQUENCY> Attempting to change frequency of [%s] per [%s] to [%s]", $value, $per, $target_per);

    my $matrix = {
        annum => {'month' => 1/12, 'week' => 1/48, 'day' => 1/240, 'hour' => 1/2080},
        month => {'annum' => 12,   'week' => 1/4,  'day' => 1/30,  'hour' => 1/160},
        week  => {'annum' => 48,   'month' => 4,   'day' => 1/5,   'hour' => 1/40},
        day   => {'annum' => 240,  'month' => 30,  'week' => 5,    'hour' => 1/8},
        hour  => {'annum' => 2080, 'month' => 160, 'week' => 40,   'day' => 8}
    };

    $self->token_value( $token_name => $value * $matrix->{$per}->{$target_per} );

    $self->token_value( $token_name => $self->ROUND($self->token_value($token_name)) ) if $round;

    $log->infof("SALARY_CHANGE_FREQUENCY> Salary adjusted to [%s] per [%s] for token [%s]", $self->token_value($token_name), $target_per, $token_name);

    return $self->token_value($token_name);
}

# Will return undef if the given value is
# an empty string.
sub ROUND {
    my $self = shift;
    my $unrounded = shift;
    unless( length($unrounded) ){
        return undef;
    }
    return sprintf("%.0f", $unrounded);
}

### end of salary helpers ###

### date helpers ###
sub CV_UPDATED_WITHIN_TO_EPOCH {
    my $self = shift;
    my $interval = shift or return;

    my $datetime = $self->cv_updated_within_to_datetime( $interval )
        or return;

    return $datetime->epoch();
}

# YYYY-MM-DDTHH:MM:SS
sub CV_UPDATED_WITHIN_TO_ISO8601 {
    my $self = shift;
    my $interval = shift or return;

    my $datetime = $self->cv_updated_within_to_datetime( $interval )
        or return;

    return $datetime->iso8601();
}

sub CV_UPDATED_WITHIN_TO_MONTHS {
    my $self = shift;
    my $interval = shift or return;

    if ( $interval =~ s/(\d+)M$// ) { # return number if a month time period is selected
        return $1;
    }

    my $datetime = $self->cv_updated_within_to_datetime( $interval )
        or return;

    my $today = DateTime->today();
    my $duration = $today->delta_md( $datetime );
    my $delta_months = $duration->delta_months();
    my $delta_days   = $duration->delta_days();

    if ( $delta_days > 0  || $delta_months == 0 ) {
        return $delta_months+1;
    }

    return $delta_months;
}

sub CV_UPDATED_WITHIN_TO_DAYS {
    my $self = shift;
    my $interval = shift or return;

    my $datetime = $self->cv_updated_within_to_datetime( $interval )
        or return;

    my $today = DateTime->today();
    return $today->delta_days( $datetime )->delta_days();
}

sub CV_UPDATED_WITHIN_TO_MINS { #  monster want in mins!!
    my $self = shift;
    my $interval = shift or return;

    my $n_days;
    if ($interval eq 'TODAY'){
        $n_days = 1;
    } elsif ($interval eq 'YESTERDAY'){
        $n_days = 2;
    } else {
        $n_days = $self->CV_UPDATED_WITHIN_TO_DAYS( $interval );
    }

    if ($n_days < 7){
        my $hours = 24 * $n_days;
        return 60 * $hours;
    }

    my $datetime = $self->cv_updated_within_to_datetime( $interval )
        or return;

    my $today = DateTime->now();

    my $duration = $today->delta_ms( $datetime );
    return $duration->delta_minutes();
}

sub cv_updated_within_to_datetime {
    my $self = shift;
    my $interval = uc(shift) or return;

    if ( $interval eq 'ALL' ) {
        return;
    }

    my $today = DateTime->today();
    if ( $interval eq 'TODAY' ) {
        return $today;
    }

    if ( $interval eq 'YESTERDAY' ) {
        my $yesterday = $today->clone()->subtract( days => 1 );
        return $yesterday;
    }

    if ( $interval =~ s/D$// ) {
        my $date = $today->clone()->subtract( days => $interval );
        return $date;
    }

    if ( $interval =~ s/W$// ) {
        my $date = $today->clone()->subtract( weeks => $interval );
        return $date;
    }

    if ( $interval =~ s/M$// ) {
        my $date = $today->clone()->subtract( months => $interval );
        return $date;
    }

    if ( $interval =~ s/Y$// ) {
        my $date = $today->clone()->subtract( years => $interval );
        return $date;
    }

    return;
}

### end of date helpers ###

### location helpers ###

sub LOCATION_FREETEXT {
    my $self = shift;
    my $location_id = shift or return;

    # Replace by Stream2->instance()->location_api if that goes bang
    my $location = $self->location_api->find({ id => $location_id });

    if ( !$location  ) {
        $log->warnf( "unable to find location [%s] in the database", $location_id );
        return;
    }

    return $location->specific_path;
}

sub BEST_LOCATION_MAPPING {
    my $self = shift;
    my $board = shift;
    my $location_id = shift or return;
    my $location_within = shift || 0;

    my @mappings = $self->LOCATION_MAPPING( $board, $location_id, $location_within );

    if ( @mappings ) {
        return shift @mappings;
    }

    return;
}

sub LOCATION_MAPPING {
    my $self  = shift;
    my $board = shift;
    my $location_id = shift or return;
    my $location_within = shift || 0;

    # Replace by Stream2->instance()->location_api if that goes bang
    my $location_api = $self->location_api();
    my $location = $location_api->find({
        id => $location_id,
    });

    if ( ! $location  ) {
        $log->warnf( "unable to find location [%s] in the database", $location_id );
        return;
    }

    return $location_api->get_stream_location_mappings(
        $board,
        $location_within,
        $location_id
    );
}

sub LOCATION_UK_POSTCODE {
    my $self = shift;
    my $location_id = shift;

    if ( !$location_id ) {
        $log->debug('No location selected');
        return;
    }

    # Replace by Stream2->instance()->location_api if that goes bang
    my $location = $self->location_api->find({
        id => $location_id,
    });

    if ( ! $location ) {
        $log->warnf( "unable to find location [%s] in the database", $location_id );
        return;
    }

    if ( !$location->in_uk() ) {
        $log->debug( 'location is not in the uk' );
        return;
    }

    return $location->postcode();
}

sub LOCATION_WITHIN {
    my $self = shift;
    my $token = shift;
    my $location_within = shift;

    my $distance = $self->build_token( $token );

    if ( !defined( $location_within ) ) {
        $location_within = $self->token_value( $token );
    }

    my $closest_option = $distance->first_greater_than( $location_within );

    return $closest_option;
}

# returns $postcode if it is entered otherwise returns $location->postcode()
sub LOCATION_PARTIAL_POSTCODE {
    my ($self, $location_id, $postcode) = @_;

    if ($postcode) {
        $log->debugf( 'Postcode provided, building postcode object' );

        return Stream2->instance->location_api->get_major_area_from_postcode($postcode);
    } elsif ($location_id) {
        $log->debugf( 'Postcode not provided, building location from location_id' );

        # Replace by Stream2->instance()->location_api if that goes bang
        my $location = $self->location_api->find({
            id => $location_id,
        });

        return $location->major_postcode();
    }

    return undef;
}

# same as LOCATION_PARTIAL_POSTCODE but returns a full UK postcode if they have only entered a partial postcode
sub LOCATION_FULL_POSTCODE {
    my ($self, $location_id, $postcode) = @_;

    if ($postcode) {
        $log->debugf( 'Postcode provided, building postcode object' );
        return Stream2->instance()->location_api()->get_full_postcode_from_postcode($postcode);
    } elsif ($location_id) {
        $log->debugf( 'Postcode not provided, building location from location_id' );

        # Replace by Stream2->instance()->location_api if that goes bang
        my $location = $self->location_api->find({
            id => $location_id,
        });

        return $location->postcode();
    }

    return undef;
}

### end of location helpers ###


# Helpers
sub IF_ELSE {
    my $self = shift;
    my $test = shift;
    my $true = shift;
    my $false = shift;

    if ( $test ) {
        return $true;
    }

    return $false;
}

sub UPPERCASE {
    my $self = shift;
    return join ' ', map { uc($_) } @_;
}

sub HASH_MAP {
    my $self = shift;
    my $value = shift;

    foreach ( @_ ) {
        return $_ if $_ eq $value;

        my ($name, $itsvalue) = split /=/, $_, 2;

        if ( $name eq $value ) {
            return $itsvalue || '';
        }
    }

    return undef;
}

sub HASH_MAP_MULTIPLE {
    my $self = shift;
    my $value = shift;
    my @mappings = @_;

    my @mapped;
    foreach ( @mappings ) {
        if ($_ eq $value){
            push @mapped, $_;
            next;
        }

        my ($name, $itsvalue) = split /=/, $_, 2;

        if ( $name eq $value && $itsvalue) {
            push @mapped, $itsvalue;
            next;
        }
    }

    return @mapped;
}

sub BOOLEAN_QUERY {
    my $self = shift;

    my $search_anywords     = $self->token_value('search_anywords') || '';
    my $search_exactphrase  = $self->token_value('search_exactphrase') || '';
    my $search_allwords     = $self->token_value('search_allwords') || '';
    my $search_withoutwords = $self->token_value('search_withoutwords') || '';

    my @search_builder;

    if ( $search_allwords ) {
        my $query = _build_query( ' AND ', $search_allwords );

        push @search_builder, $query if $query;
    }

    if ( $search_exactphrase ) {
        $search_exactphrase =~ s/\A\s+|\s+\z//g;
        $search_exactphrase =~ s/\A"(.*)"\z/$1/g;
        $search_exactphrase =~ s/\A\s+|\s+\z//g;

        if ( $search_exactphrase ) {
            push @search_builder, qq("$search_exactphrase");
        }
    }

    if ( $search_anywords ) {
        my $query = _build_query( ' OR ', $search_anywords );

        push @search_builder, $query if $query;
    }

    if ( $search_withoutwords ) {
        my $query = _build_query( ' NOT ', $search_withoutwords );

        push @search_builder, 'NOT ' . $query
            if $query;
    }

    my $search_query = join ') AND (', @search_builder;

    if ( $search_query =~ m/\) AND \(/ ) {
        $search_query = "($search_query)";
    }

    return $search_query;
}

sub MAP_HASHREF {
    my $self = shift;
    my $hash_ref = shift;
    my $mappings_ref = shift;

    my %new_hash;
    while ( my ($key, $value) = each %$hash_ref ) {
        if ( exists($mappings_ref->{$key}) ) {
            $key = $mappings_ref->{$key};
        }

        $new_hash{ $key } = $value;
    }

    return %new_hash;
}

sub _build_query {
    my $operator = shift;
    my $keywords = shift;

    # trim leading & trailing whitespace
    $keywords =~ s/\A\s+|\s+\z//g;

    return join $operator, grep { $_ } split /\s+/, $keywords;
}

sub OPTION_SELECTED {
    my $self = shift;
    my $value_to_look_for = shift;

    foreach my $selected_option ( @_ ) {
        if ( $value_to_look_for eq $selected_option ) {
            return $selected_option;
        }
    }

    return;
}

sub MULTILIST_COMMA {
    my $self = shift;
    my @values = grep { $_ } @_;

    return join(',', @values);
}

sub join_tokens {
    my $self = shift;
    my $separator = shift;

    return join( $separator, grep { $_ } $self->token_values( @_ ) );
}

sub SEARCH_ARRAY {
    return '' unless @_ > 2;
    my $self = shift;
    my $needle = shift;

    return grep {
        $_ eq $needle
    } @_;
}

sub reverse_mapping {
    my $self = shift;
    my $value = shift or return;
    my $list = shift or return $value;

    if ( ref( $list ) eq 'HASH' ) {
        if ( exists( $list->{Options} ) ) {
            $list = $list->{Options};
        }
    }

    my $list_ref = $self->build_list( $list );
    return $self->_reverse_mapping( $list_ref, $value );
}

sub _reverse_mapping {
    my $self = shift;
    my $list_ref = shift;

    my @labels;

    VALUE:
    foreach my $value ( @_ ) {
        foreach my $item_ref ( @$list_ref ) {
            if ( $item_ref->[1] eq $value ) {
                push @labels, $item_ref->[0];
                next VALUE;
            }
        }

        # fallback to the id value
        push @labels, $value;
    }

    return wantarray ? @labels : shift @labels;
}

sub reverse_map {
    my $self = shift;
    my $candidate = shift || return;
    my $from_attr = shift || return;
    my $to_attr   = shift || return;
    my $token_ref = shift || return;

    my (@current_values) = $candidate->attr($from_attr);

    if ( !@current_values ) {
        return;
    }

    if ( ref( $token_ref ) eq 'HASH' ) {
        if ( exists( $token_ref->{Options} ) ) {
            $token_ref = $token_ref->{Options};
        }
    }

    # cache the list so we don't have to rebuild it per value
    my $list_ref = $self->build_list( $token_ref );

    my (@labels) = $self->_reverse_mapping( $list_ref, @current_values );
    if ( @labels ) {
        $candidate->attr( $to_attr => @labels );
        return wantarray ? @labels : shift @labels;
    }

    return;
}

1;

# vim: expandtab shiftwidth=4

package Stream::Templates::washingtonpost;
use base qw(Stream::Templates::CrawledMadgex);

use strict;
use warnings;

__PACKAGE__->load_tokens_from_config();

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

use Stream::Constants::SortMetrics qw(:all);

=test auth tokens

https://employers.washingtonpost.com/
Broadbean@baesystems.com
baesystems1 

=cut

$authtokens{washingtonpost_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{washingtonpost_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

$derived_tokens{salary_cur}->{Options} = 'GBP';
$derived_tokens{salary_per}->{Options} = 'annum';


sub fixup_tokens {
    my $self = shift;
    my %tokens = @_;

    #Remove Preferred salary as a token - We're mapping it instead.
    delete $tokens{21}
      or die('Could not find \'Preferred salary\'');

    return %tokens;
}

sub salary_mappings {
    return {
        unspecified => [
            '21|241', #Negotiable
            '21|242', #Unpaid
        ],
        GBP => {
            ranges => { 
               # id => range
                '21|232' => [0       =>  20_000],
                '21|233' => [20_000  =>  30_000],
                '21|234' => [30_000  =>  40_000],
                '21|235' => [40_000  =>  60_000],
                '21|236' => [60_000  =>  80_000],
                '21|237' => [80_000  => 100_000],
                '21|238' => [100_000 => 120_000],
                '21|239' => [120_000 => 140_000],
                '21|240' => [140_000 =>   "inf"]
            }
        }
    };
}


sub fixup_search_fields {
    my $self = shift;
    my %fields = @_;

    my @salary_ranges = $self->_salary_ids;
    push @{$fields{q}}, @salary_ranges;
    return %fields;
}

1;

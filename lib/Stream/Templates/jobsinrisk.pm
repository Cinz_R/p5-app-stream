package Stream::Templates::jobsinrisk;
use base qw(Stream::Templates::CrawledMadgex);

use strict;
use warnings;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

__PACKAGE__->load_tokens_from_config();

use Stream::Constants::SortMetrics qw(:all);

$authtokens{jobsinrisk_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{jobsinrisk_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

1;

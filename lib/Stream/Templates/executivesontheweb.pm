#!/usr/bin/perl

# vim: expandtab:tabstop=4

# $Id: monsterxml.pm 25468 2009-10-15 13:01:21Z andy $

package Stream::Templates::executivesontheweb;

=head1 NAME

Stream::Templates::executivesontheweb - Execs on the web feed

=head1 CONTACT

Andrius Stanaitis <andriuss@executivesontheweb.com>

01543 411454

=head1 TESTING INFORMATION

The client credentials are:

 Username: broadbean
 Password: VFXZY

=head1 DOCUMENTATION

Available in trac

L<http://trac/adcourier/attachment/wiki/MultipleCvSearch/Broadbean%20Stream%20API%20-%20ExecutivesOnTheWeb.doc>

Values are the same as on: http://www.executivesontheweb.com/uk/recruiters/cv_srch.cfm

=head1 OVERVIEW

You don't need to be registered with EOTW to search them. You must
register to download the CV.

HTTP Post of XML. They return XML. They support a CV preview.

They do not support boolean searching but you can choose to search by
ANY word, ALL words or treat as a phrase.

When you do a location search on their site, if you select UK then EOTW
automatically tick all sub-locations. This is just for illustrational
purposes so the user can be sure that everything within the UK is included.
For our purposes, it is enough for us to send through only the id for the UK.

=head2 Implementation details

Info from Andrius:

"You can use any form field available in our CV Search form within your CV search request. I designed the request script in a way so it converts all xml tags into the form variables, and then it runs the same CV search engine which is used in CV search pages. So if you would include a tag <andworklocation>1</andworklocation> into your XML document, it would act in the same way as the tick box "andworklocation" in CV search form."

=head1 MAJOR CHANGES

None yet

=head1 BUG FIXES

None yet

=cut

use strict;

use utf8;

use base qw(Stream::Engine::API::XML);

use Stream::Constants::SortMetrics qw(:all);
use POSIX;
use HTML::Entities;

################ TOKENS #########################
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

BEGIN {
=head1 TOKENS

=head2 STANDARD TOKENS

=cut

    $standard_tokens{title} = {
        Label      => 'Job Title',
        Type       => 'Text',
        SortMetric => undef,
    };

    $standard_tokens{functions} = {
        Label      => 'Job Disciplines',
        Type       => 'MultiList',
        Options    => '(Accounting / Finance / CFO=10|Banking=15|Business Analyst=12|Chairman / Non Executive=1|Change Management=11|Commercial / Administration=30|Communications / Media / PR=26|Customer Service / CRM=13|Engineering / Design=14|HR / Training / Teaching=16|IT Director / CTO=9|IT Technical=35|Legal / Company Secretarial=18|Managing Director / CEO=2|Manufacturing / Production=19|Marketing / Advertising=20|Operations / COO=21|Partner=3|Procurement / Buyer / Purchasing=22|Product / Service Development=23|Project / Programme Management=24|Property / Facilities / Health & Safety=25|Research=31|Sales / Business Development=8|Strategy / Planning=27|Supply Chain / Service Delivery=33|Telecommunications, Non IT=17|Transport / Logistics=32|Treasury / Risk / Compliance / Fraud=28)',
        SortMetric => $SM_INDUSTRY,
    };

    $standard_tokens{sectors} = {
        Label      => 'Sectors',
        Type       => 'MultiList',
        Options    => '(Accountancy/Taxation=2|Aerospace=47|Architecture/Construction/Property=20|Automotive=46|Banking/Finance=1|Charity (non-profit)=3|Consultancy=45|Customer Service=21|Defence=42|Distribution/Transport/Logistics=15|eBusiness/eCommerce=8|Education/Training=22|Electronics/Electrical=4|Engineering=23|Financial Services=48|Healthcare/Pharmaceutical=6|Insurance=7|IT=31|Legal=9|Leisure/Entertainment=36|Manufacturing=19|Marketing/PR=24|News/Media/Design/Advertising=11|Printing & Packaging=27|Public Sector/Government=5|Recruitment/HR=13|Retail/FMCG=14|Telecomms/Comms=18|Tourism/Travel=10|Utilities/Energy/Nuclear=16)',
        SortMetric => $SM_INDUSTRY,
    };

    $standard_tokens{work_types} = {
        Label      => 'Work Types',
        Type       => 'MultiList',
        Options    => '(Permanent=1|Interim=2|Non-Executive=3|Contractor=4|Self Employed / Business Opportunities=6)',
        Deprecated => 'JobType',
        SortMetric => $SM_JOBTYPE,
    };

    $posting_only_tokens{'job_type'} = {
        Helper     => 'executivesontheweb_jobtype(%default_jobtype%|%work_types%|permanent=1|contract=4|temporary=4)',
    };

    $derived_tokens{home_location} = {
        Label      => 'Location',
        Type       => 'MultiList',
        Helper     => 'LOCATION_MAPPING(stream_executivesontheweb|%location_id%|%location_within_miles%)',
        SortMetric => $SM_LOCATION,
    };

    $standard_tokens{andworklocation} = {
        Label      => 'Include Desired Work Location',
        Type       => 'List',
        Options    => '(Both=|Yes=1|No=1)',
        Default    => '',
        SortMetric => $SM_LOCATION,
    };

    $derived_tokens{salary_per} = {
        Type         => 'SalaryPer',
        Options      => 'annum',
        Helper       => 'SALARY_PER',
        HelperMetric => 1,
        SortMetric   => $SM_SALARY,
    };

    $derived_tokens{salary_cur} = {
        Type         => 'Currency',
        Options      => 'GBP',
        Helper       => 'SALARY_CURRENCY',
        HelperMetric => 1,
        SortMetric   => $SM_SALARY,
    };

    $derived_tokens{salary_from} = {
        Type      => 'SalaryText',
        SortMetric => $SM_SALARY,
    };

    $derived_tokens{salary_to} = {
        Type      => 'SalaryText',
        SortMetric => $SM_SALARY,
    };

    $derived_tokens{updated_within_months} = {
        Label      => 'Execs on the Web Last Updated in months',
        Type       => 'Text',
        Helper     => 'CV_UPDATED_WITHIN_TO_MONTHS(%cv_updated_within%)',
        SortMetric => $SM_DATE,
    };

    $standard_tokens{'max_results'} = {
        Label => 'Number of Results',
        Type  => 'List',
        Values => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
        Default => 50,
        SortMetric => $SM_SEARCH,
    };
};

BEGIN {

=head2 AUTHTOKENS

=cut
    $authtokens{executivesontheweb_username} = {
        Label      => 'Username',
        Type       => 'Text',
        Mandatory  => 0,
        SortMetric => 1,
    };

    $authtokens{executivesontheweb_password} = {
        Label      => 'Password',
        Type       => 'Text',
        Mandatory  => 0,
        SortMetric => 2,
    };
};

############### HELPERS ##########################

sub default_url {
    return 'http://www.executivesontheweb.com/xmljobfeed/broadbean_stream.cfm';
}

sub token_value_csv {
    my $self = shift;
    my $token = shift;

    my (@values) = $self->token_value( $token );

    return join(',', @values);
}

sub search_build_xml {
    my $self = shift;

    my $xml = Bean::XML->new();
    $xml->startTag('CV_Search_Query');
        $xml->startTag('Account');
            $xml->dataElement('Username' => $self->token_value('username') || '' );
            $xml->dataElement('Password' => $self->token_value('password') || '' );
        $xml->endTag('Account');
        $xml->startTag('Search_Query');
            $xml->dataElement('title'            => $self->token_value('title') || '' );
            $xml->dataElement('functions'        => $self->token_value_csv('functions') || '' );
            $xml->dataElement('sectors'          => $self->token_value_csv('sectors') || '' );
            $xml->dataElement('work_types'       => $self->token_value_csv('job_type') || '' );
            $xml->dataElement('home_location'    => $self->token_value_csv('home_location') || '' );
            $xml->dataElement('salaries'         => $self->token_value('salary_from') );
            $xml->dataElement('max_salary'       => $self->token_value('salary_to') );
            $xml->dataElement('months'           => $self->token_value('updated_within_months') || '' );
            # gotcha: Don't use brackets, quotes, operators, or too general words like "Manager". 
            $xml->dataElement('keywords'         => $self->token_value('keywords') || '' );
            $xml->dataElement('andworklocation'  => $self->token_value('andworklocation') || '' );
            $xml->dataElement('Limit_Results_to' => $self->results_per_page() );
            $xml->dataElement('Results_offset'   => $self->results_offset() );
        $xml->endTag('Search_Query');
    $xml->endTag('CV_Search_Query');

    return $xml;
}

sub search_number_of_results_xpath {
    return '//Current_Search/Record_Count';
}

sub scrape_paginator {
    my $self = shift;

    my $total_results = $self->total_results();
    my $results_per_page = $self->results_per_page();

    if ( $total_results <= $results_per_page ) {
        $self->log_debug('There is only 1 page of results');
        $self->max_pages( 1 );
    }
    else {
        my $total_pages = POSIX::ceil( $total_results / $results_per_page );
        $self->log_debug('There are %d pages', $total_pages);
        $self->max_pages( $total_pages );
    }
}

sub search_success { return qr/<Results>/; }

sub scrape_candidate_xpath { return '//Results/Result'; }
sub scrape_candidate_details {
    return (
        candidate_id        => './Candidate_Id',
        job_title           => './CV_Title',
        job_discipline      => './Job_Discipline',
        jobtype             => './Work_Types',
        salary              => './Salary_Required',
        location_text       => './Home_Location',
        snippet             => './Summary',
        more_info_link      => './More_Info',
        raw_cv_updated_date => './CV_Update_Date',
        downloaded          => './Downloaded',
    );
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    my $raw_cv_updated_date = $candidate->attr('raw_cv_updated_date');
    if ( $raw_cv_updated_date ) {
        # 2009-12-09
        $candidate->cv_last_updated_from_date( $raw_cv_updated_date );
    }

    $self->remove_html_entities( $candidate, qw/job_title job_discipline jobtype salary location_text snippet/ );

    $candidate->has_profile( 1 );

    if ( $candidate->attr('downloaded') ) {
        $candidate->attr('got_cv' => 1);
    }

    $candidate->attr('headline',$candidate->attr('job_title'));

    return $candidate;
}

sub download_cv_xml {
    my $self = shift;
    my $candidate = shift;

    my $xml = Bean::XML->new();
    $xml->startTag('CV_Search_Query');
        $xml->startTag('Account');
            $xml->dataElement('Username' => $self->token_value('username') || '' );
            $xml->dataElement('Password' => $self->token_value('password') || '' );
        $xml->endTag('Account');
        $xml->startTag('Candidate_CV_Query');
            $xml->dataElement('Candidate_Id' => $candidate->candidate_id() || '' );
        $xml->endTag('Candidate_CV_Query');
    $xml->endTag('CV_Search_Query');

    return $xml;
}

sub download_cv_success {
    return qr/Candidate_CV_Response/;
}

sub scrape_download_cv_xpath { return '//CV_Search_Query'; }

sub scrape_download_cv_details {
    return (
        authorised          => './Account_Status/Authorised',
        credits_available   => './Account_Status/Credits_Available',
        status_message      => './Account_Status/Status_Message',

        first_name          => './Candidate_CV_Response/First_Name',
        last_name           => './Candidate_CV_Response/List_Name',
        email               => './Candidate_CV_Response/Email',
        mobile_telephone    => './Candidate_CV_Response/Mobile_Phone',
        photo               => './Candidate_CV_Response/Photo',
        sectors             => './Candidate_CV_Response/Sectors',

        resume_word_base64  => './Candidate_CV_Response/File/text()',
        resume_filename     => './Candidate_CV_Response/File/@filename',
        resume_content_type => './Candidate_CV_Response/File/@content-type',
    );
}

sub download_profile_success {
    return qr/Candidate_ID>/i;
}

#sub download_profile_failed {
#    return qr/Can not get CV at this time/i;
#}

sub download_profile_xml {
    my $self = shift;
    my $candidate = shift;

    my $xml = Bean::XML->new();
    $xml->startTag('CV_Search_Query');
        $xml->startTag('Account');
            $xml->dataElement('Username' => $self->token_value('username') || '' );
            $xml->dataElement('Password' => $self->token_value('password') || '' );
        $xml->endTag('Account');
        $xml->startTag('Candidate_Preview_Query');
            $xml->dataElement('Candidate_Id' => $candidate->candidate_id() || '' );
        $xml->endTag('Candidate_Preview_Query');
    $xml->endTag('CV_Search_Query');

    return $xml;
}

sub scrape_download_profile_xpath {
    return '//CV_Search_Query';
}

sub scrape_download_profile_details {
    return (
        authorised        => './Account_Status/Authorised',
        credits_available => './Account_Status/Credits_Available',
        status_message    => './Account_Status/Status_Message',

        photo             => './Candidate_Preview_Response/Photo',
        sectors           => './Candidate_CV_Response/Sectors',

        cv_preview        => './Candidate_Preview_Response/CV_Preview/text()',
    );
}

sub after_download_profile_scrape_success {
    my $self = shift;
    my $candidate = shift;

    $self->remove_html_entities( $candidate, qw/sectors cv_preview/ );

    return $candidate;
}

sub executivesontheweb_jobtype {
    my $self = shift;
    printf ">>>>>>>>>>> %s\n", join ", ", map { qq{"$_"} } @_;
    my @args = @_;
    my @old  = grep { defined($_) &&        /^[0-9]{1}$/          } @args;
    my @new  = grep { defined($_) && $_ && !/^[0-9]{1}$/ && !/\=/ } @args;
    my @map  = grep { defined($_) &&                         /\=/ } @args;
    my @return = $#new == -1 ? $#old == -1 ? ( q{} ) : @old
                             : map { $self->HASH_MAP_MULTIPLE($_ => @map) } @new
                             ;
    printf ">>>>>>>>>>> %s\n", join ", ", map { qq{"$_"} } @return;
    return @return;
}


sub remove_html_entities {
    my $self = shift;
    my $candidate = shift;

    my @attributes = @_;
    if ( !@attributes ) {
        return;
    }

    # remove HTML entities
    ATTR:
    foreach my $attr ( @attributes ) {
        my $html_value = $candidate->attr( $attr );

        if ( !$html_value ) {
            next ATTR;
        }

        my $value = decode_entities( $html_value );

        $candidate->attr( $attr => $value );
    }

    return $candidate;
}

sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    if ( !defined( $content ) ) {
        $content = $self->content();

        return unless $content;
    }

    if ( $content =~ m{you have no CV download credits} ) {
        return $self->throw_lack_of_credit( $content );
    }

    # displayed when they are using no credentials
    if ( $content =~ m{User unauthorised} ) {
        return $self->throw_login_error( $content );
    }

    if ( $content =~ m{(The request has exceeded the allowable time limit)} ) {
        return $self->throw_timeout( $1 );
    }

    # Exec on the web return some nice error messages
    #  so lets present them internally
    return $content;

#    if ( $content =~ m{(No such user)} ) {
#        return $self->throw_login_error( $1 );
#    }
#
#    if ( $content =~ m{<Results>(.+?)</Results>} ) {
#        return $1;
#    }

    return $message;
}

1;

__END__

# vim: expandtab shiftwidth=4

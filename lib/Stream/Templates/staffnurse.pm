package Stream::Templates::staffnurse;

use strict;

use base qw(Stream::Templates::careerbuilder);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

BEGIN {
    __PACKAGE__->inherit_tokens();

    $authtokens{staffnurse_email} = {
        Label => 'Email Address',
        Type  => 'Text',
        Size  => 25,
        SortMetric => 1,
    };

    $authtokens{staffnurse_password} = {
        Label => 'Password',
        Type  => 'Text',
        Size  => 25,
        SortMetric => 2,
    };
};

sub careerbuilder_niche_sites { return qw(SF); }

1;

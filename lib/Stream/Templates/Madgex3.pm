package Stream::Templates::Madgex3;

#===============================================================================
#
#   FILE:  Madgex3.pm
#   NOTES: All feeds inheriting from this base should inhert tokens: BEGIN { __PACKAGE__->inhert_tokens(); };
#   DESCRIPTION:  Search integration for Madgex3.
#
#   CREATED:  2015-06-04
#   REVISION:  $Id: Madgex3.pm 141227 2015-03-30 16:32:00Z aliz $
#===============================================================================

use strict;
use warnings;

use base qw(Stream::Engine::Robot);

use Stream::Constants::SortMetrics qw(:all);
use List::MoreUtils qw/uniq/;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

#####  *****  ANY NEW MADGEX FEED NEEDS TO BE ADDED TO THE LIST OF FEEDS AT ~/svn_working_directory/tt/stream/subscriptions/view.tpl ****** #####

$authtokens{madgex3_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{madgex3_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

$derived_tokens{salary_cur}  = {
    Type => 'Currency',
    Options => 'GBP',
    Helper  => 'SALARY_CURRENCY',
    HelperMetric => 1,
};

$derived_tokens{salary_per} = {
    Type         => 'SalaryPer',
    Options      => 'annum',
    Helper       => 'SALARY_PER(salary_per|salary_from|salary_to)',
    HelperMetric => 1,
};

$standard_tokens{sort} = {
    Type    => 'List',
    Values  => 'Relevancy=Relevancy|Last updated=DateLastModified',
    Default => 'Relevancy',
    SortMetric => $SM_SORT,
};

$standard_tokens{status} = {
    Label   => 'Status',
    Type    => 'List',
    Values  => 'All=|Not downloaded=NotYetDownloaded|Already downloaded=AlreadyDownloaded',
};

sub session_life_default { 120; }

sub login_url { return $_[0]->base_url() . "/login"; }

sub logout_url { return $_[0]->base_url() . "/logout"; }

sub search_request_method { 'GET' }

sub search_url {
    my $self = shift;
    return $self->base_url . sprintf('/cv-search/%s', $self->current_page);
}

sub search_success { return qr{CV alerts|Resumes found|CVs found|Your search returned no results}; }

sub profile_base_url { return $_[0]->base_url . '/cv-details/%s'; }

sub download_profile_success { return qr{Full details}; }

sub scrape_candidate_headline { 'last_updated' }

sub login_form_with_fields { 'Username' }

sub login_success { return qr/Sign out/i; }

sub login_fields {
    return (
        'Username' => $_[0]->token_value('email') || $_[0]->token_value('username'),
        'Password' => $_[0]->token_value('password'),
    );
}

sub check_for_login_error {
    my $self = shift;

    my $response = $self->response();
    my $login_success = $self->login_success();

    # check if consultant has unlimited credits, so we can show the cv in profile
    # without worrying about spending their credits
    if ( $response->decoded_content =~ m/$login_success/ ) {
        $self->log_info('checking for unlimited credits');
        my $credit_response = $self->get($self->base_url() . '/credit-balance/');
        $self->recent_content($credit_response->decoded_content);
        my $credits = $self->findvalue('//td[contains(text(), "Candidate View") or contains(text(), "Search the CV database") or contains(text(), "CV Search") or contains(text(), "Candidate CV View") or contains(text(), "CV Download") or contains(text(), "CV database package")]/following-sibling::td/text()');
        if ( defined $credits && $credits =~ m/Unlimited/ ) {
            $self->log_info('user has unlimited credits');
            $self->set_account_value('unlimited_credits', 1);
        }
    }

    $self->SUPER::check_for_login_error();
}

sub search_fields {
    my $self = shift;

    my %fields = (
        MinLastUpdated => $self->madgex_cv_updated($self->token_value('cv_updated_within')),
    );

    $fields{Keywords} = $self->token_value('keywords');
    $fields{q} = [ $self->madgex_custom_fields() ];
    $fields{SortOrder} = $self->token_value_or_default('sort');
    $fields{DownloadedStatus} = $self->token_value('status');

    if ( my $madgex_location_id = $self->madgex_location_id ) {
        my ($id, $miles) = $self->madgex_location( $self->token_value('location_id'), $self->token_value('location_within_miles') );

        $fields{LocationIds} = $madgex_location_id . "|$id" if $id;
        $fields{RadialLocation} = $madgex_location_id . "|$miles" if $miles;
    }

    return %fields;
}

sub search_number_of_results_xpath {
    return '//h2';
}

sub search_number_of_results_regex {
    return qr{CVs found:? ([\d,]+)};
}

sub scrape_candidate_xpath {
    return '//li[contains(@class,"card") and contains(@class,"highlight-lighter") and contains(@class,"cf") and contains(@class,"block")]';
}

sub scrape_candidate_details {
    return (
        candidate_id        => './/@id',
        name                => './/h2[@class="lister__header"]/a/text()',
        last_updated        => './/p[@class="small"]/span/strong/text()',
        last_login          => './/p[@class="small"]/strong/text()',
        summary             => './/p[@class="small"]/following-sibling::p/text()',
        sectors             => './/dt[@class="grid-item one-whole"][contains(text(),"Sector")]/following-sibling::dd/text()',
        disciplines         => './/dt[@class="grid-item one-whole"][contains(text(),"Disciplines")]/following-sibling::dd/text()',
        current_jobtitle    => './/dt[@class="grid-item one-whole"][contains(text(),"job title") or contains(text(), "Job Title")]/following-sibling::dd/text()',
        duration_in_role    => './/dt[@class="grid-item one-whole"][contains(text(),"Duration in current role")]/following-sibling::dd/text()',
        current_salary      => './/dt[@class="grid-item one-whole"][contains(text(),"Current salary")]/following-sibling::dd/text()',
        qualifications      => './/dt[@class="grid-item one-whole"][contains(text(),"Qualifications")]/following-sibling::dd/text()',
        availability        => './/dt[@class="grid-item one-whole"][contains(text(),"Availability")]/following-sibling::dd/text()',
        preferred_location  => './/dt[@class="grid-item one-whole"][contains(text(),"Preferred location")]/following-sibling::dd/text()',
        eligible_to_work    => './/dt[@class="grid-item one-whole"][contains(text(),"Eligible to work in the UK")]/following-sibling::dd/text()',
        position_type       => './/dt[@class="grid-item one-whole"][contains(text(),"Position Type")]/following-sibling::dd/text()',
        preferred_salary    => './/dt[@class="grid-item one-whole"][contains(text(),"Preferred salary")]/following-sibling::dd/text()',
        preferred_jobtitle  => './/dt[@class="grid-item one-whole"][contains(text(),"Preferred job title")]/following-sibling::dd/text()',
        preferred_contract  => './/dt[@class="grid-item one-whole"][contains(text(),"Preferred Contract Type")]/following-sibling::dd/text()',
        preferred_hours     => './/dt[@class="grid-item one-whole"][contains(text(),"Preferred Hours")]/following-sibling::dd/text()',
        jobrole             => './/dt[@class="grid-item one-whole"][contains(text(),"Job Role") or contains(text(), "job role")]/following-sibling::dd/text()',
        location            => './/dt[@class="grid-item one-whole"][contains(text(),"Location") or contains(text(), "location")]/following-sibling::dd/text()',
        job_categories      => './/dt[@class="grid-item one-whole"][contains(text(),"Job Categories") or contains(text(), "location")]/following-sibling::dd/text()',
        clearance_level     => './/dt[@class="grid-item one-whole"][contains(text(),"Clearance level")]/following-sibling::dd/text()',
    );
}

sub scrape_fixup_candidate {
    my ($self, $candidate) = @_;

    $candidate->has_profile(1);
    $candidate->has_cv(1);

    return $candidate;
}

sub scrape_download_profile_xpath {
    return '//html';
}

sub scrape_download_profile_details {
    return (
        description         => './/h3[contains(text(),"Personal summary") or contains(text(), "Your CV")]/following-sibling::p/text()',
        objectives          => './/h3[contains(text(),"Personal objectives")]/following-sibling::p/text()',
        email               => './/dt[contains(text(),"Email Address")]/following-sibling::dd/text()',
        disciplines         => './/dt[contains(text(),"Disciplines")]/following-sibling::dd/text()',
        qualifications      => './/dt[contains(text(),"Qualifications")]/following-sibling::dd/text()',
        other_qualifications=> './/dt[contains(text(),"Other Qualifications")]/following-sibling::dd/text()',
        function            => './/dt[contains(text(),"Job functions")]/following-sibling::dd/text()',
        position_sought     => './/dt[contains(text(),"Position Sought")]/following-sibling::dd/text()',
        driving_licence     => './/dt[contains(text(),"driving licence")]/following-sibling::dd/text()',
        meet_requirements   => './/dt[contains(text(),"Meet Requirements")]/following-sibling::dd/text()',
        current_location    => './/dt[contains(text(),"Current location") or contains(text(), "Location")]/following-sibling::dd/text()',
        year_qualified      => './/dt[contains(text(),"Year Qualified")]/following-sibling::dd/text()',
        jobrole             => './/dt[contains(text(),"Preferred job role") or contains(text(), "Job Role")]/following-sibling::dd/text()',
        availability        => './/dt[contains(text(),"Availability")]/following-sibling::dd/text()',
        jobtype             => './/dt[contains(text(),"Job Type")]/following-sibling::dd/text()',
        eligible_to_work    => './/dt[contains(text(),"Eligible to work in")]/following-sibling::dd/text()',
        spoken_languages    => './/dt[contains(text(),"Spoken Languages")]/following-sibling::dd/text()',
        sectors             => './/dt[contains(text(),"Sector")]/following-sibling::dd/text()',
        position_type       => './/dt[contains(text(),"Position Type") or contains(text(), "Contract Type")]/following-sibling::dd/text()',
        preferred_location  => './/dt[contains(text(),"Preferred location")]/following-sibling::dd/text()',
        duration_in_role    => './/dt[contains(text(),"Duration in current role") or contains(text(), "Time in role")]/following-sibling::dd/text()',
        current_salary      => './/dt[contains(text(),"Current salary") or contains(text(), "CurrentSalary") or contains(text(), "Current Salary")]/following-sibling::dd/text()',
        preferred_contract  => './/dt[contains(text(),"Preferred Contract Type")]/following-sibling::dd/text()',
        work_experience     => './/dt[contains(text(),"Years of work experience")]/following-sibling::dd/text()',
        current_jobtitle    => './/dt[contains(text(),"Your current role") or contains(text(), "Current Job Title") or contains(text(), "Current job title")]/following-sibling::dd/text()',
        preferred_jobtitle  => './/dt[contains(text(),"PreferredJobTitle")]/following-sibling::dd/text()',
        preferred_hours     => './/dt[contains(text(),"Preferred Hours")]/following-sibling::dd/text()',
        profile_location    => './/dt[contains(text(),"ProfileLocation")]/following-sibling::dd/text()',
        willing_to_relocate => './/dt[contains(text(),"Willing to relocate") or contains(text(), "Relocate")]/following-sibling::dd/text()',
        preferred_salary    => './/dt[contains(text(),"Preferred salary") or contains(text(), "Salary Band")]/following-sibling::dd/text()',
        personal_details    => './/dt[contains(text(),"Personal Details")]/following-sibling::dd/text()',
        postcode            => './/dt[contains(text(),"Post Code")]/following-sibling::dd/text()',
        phone_number        => './/dt[contains(text(),"Phone Number")]/following-sibling::dd/text()',
        boolean             => './/dt[contains(text(),"Boolean")]/following-sibling::dd/text()',
        cips_qualification  => './/dt[contains(text(),"CIPS Qualification")]/following-sibling::dd/text()',
        job_categories      => './/dt[contains(text(),"Job Categories")]/following-sibling::dd/text()',
        action_btn          => '//li[@class="cv-actions__item"][1]/a/text()',
        clearance_level     => './/dt[contains(text(), "Clearance level")]/following-sibling::dd/text()',
    );
}

sub scrape_fixup_candidate_profile {
    my ($self, $candidate) = @_;

    if ( $candidate->attr('action_btn') =~ /Download/ ) {
        $self->log_info("candidate [%s] has a CV and client can download, setting cv_link", $candidate->attr('candidate_id'));
        $candidate->attr( 'cv_link' => $self->base_url() . '/cv-download/' . $candidate->attr('candidate_id') );
    }
    else {
        $self->log_debug('the candidate wasn\'t downloaded before, we won\'t set the cv_link so it can be unlocked via download_cv');
        $candidate->attr('cv_link' => undef);
    }

    return $candidate;
}

sub scrape_generic_download_profile {
    my ($self, $candidate) = @_;

    $candidate = $self->SUPER::scrape_generic_download_profile($candidate);

    return $candidate if !$candidate->attr('cv_link');

    if ( !$self->account_value('unlimited_credits') ) {
        if ( $candidate->attr('action_btn') && $candidate->attr('action_btn') !~ m/Download/ ) {
            return $candidate;
        }
    }

    return $candidate if $self->madgex_cv_preview_not_available();

    my $cv_response = $self->get( $candidate->attr('cv_link') );

    if ( $cv_response->is_success() ) {
        my $html = eval { $self->{documents_api}->htmlify($cv_response->content); };
        if ( $@ ) {
            $candidate->attr('cv_html' => 'CV downloading is not available for this candidate.');
        }
        else {
            my $xdoc = $self->{html_parser}->load_html( string => Encode::encode('UTF-8', $html), encoding => 'UTF-8' );
            my ($xbody) = $xdoc->findnodes('/html/body');
            $candidate->set_attr('cv_html', join('', map { $_->toString() } $xbody->childNodes()));
        }
    }

    $self->madgex_fixup_candidate_html_cv($candidate);

    return $candidate;
}

sub madgex_fixup_candidate_html_cv {
    my ($self, $candidate) = @_;
    return unless my $html = $candidate->attr('cv_html');

    $html =~ s/<img.+?>//g;
    $html =~ s/class="body"//g;
    $html =~ s/<(\/)?h[1-2]>/<$1h4>/g;
    $candidate->attr('cv_html' => $html);
}

=head2 madgex_cv_preview_not_available

Determines whether or not to download and display the cv in the profile.
Default is to display it (as long as client has unlimited credits).

=cut

sub madgex_cv_preview_not_available { return 0; }

=head2 madgex_unlock_cv_submit

Sometimes clients have limited credits. They can unlock the candidate by clicking on the download CV button.
Since we display the CV in the profile, if the candidate is locked, this will attempt to unlock their CV
and redirect back to the profile page so we can start scraping for profile details.

We don't want to fail showing the profile if we can't download the CV. If the client doesn't have credits,
we will simply not download the CV and display whatever information we can via the profile page.

=cut

sub madgex_unlock_cv_submit {
    my ($self, $candidate) = @_;

    $self->login();

    my $url = $self->base_url() . '/cv-unlock';
    my $unlocked_profile = $self->post($url, Content => { UserId => $candidate->candidate_id, ReturnUrl => $self->profile_url($candidate) } );

    if ( $unlocked_profile->decoded_content =~ m/Buy credits/ ) {
        return 0;
    }

    $self->log_info('CV was unlocked');
    $self->recent_content( $unlocked_profile->decoded_content );

    $self->logout();

    return 1;
}

=head2 before_downloading_cv

Checks to see if a user with limited credits can unlock a CV by looking at a
button value ('action_btn'). If so, we attempt to unlock the CV and spend
a credit and set the cv_link.

=cut

sub before_downloading_cv {
    my ($self, $candidate) = @_;

    $candidate = $self->download_profile($candidate);

    unless ( $candidate->attr('cv_link') ) {
        if ( $candidate->attr('action_btn') && $candidate->attr('action_btn') =~ /Unlock/ ) {
            my $unlocked_profile = $self->madgex_unlock_cv_submit($candidate);
            if ( $unlocked_profile ) {
                $candidate->attr( 'cv_link' => $self->base_url() . '/cv-download/' . $candidate->attr('candidate_id') );
                }
            else {
                $candidate->attr('cv_link' => undef);
            }
        }
    }

    return;
}

# --------------------
#
#   Madgex helpers
#
# --------------------

=head2 madgex_location_id

Used in search_fields() to send location/miles_within

=cut

sub madgex_location_id { 3000014 }

=head2 madgex_jobtype_mapping

Returns hash to be used in madgex_custom_fields to map to default_jobtype

=cut

sub madgex_jobtype_mapping {
    return (
        jobtype_id  => 3000023,
        permanent   => 513023,
        contract    => 513025,
        temporary   => 513024,
    );
}

=head2 madgex_miles_boundaries

Distance mappings for the board found on boards search form
Used in madgex_location() below

=cut

sub madgex_miles_boundaries { qw/0 5 10 15 20 50/ }

=head2 madgex_location_mapping

The name of the database table which stores the location id for the board.

=cut

sub madgex_location_mapping {
    return 'madgex_locs_v4';
}

=head2 madgex_location

Uses global Madgex location mapping given location_id/miles_within
Returns location mapping ID and mapped miles from madgex_mile_boundaries()

=cut

sub madgex_location {
    my ($self, $location_id, $miles) = @_;
    return unless defined $location_id;

    my $id;
    $miles ||= 0;
    my @boundaries = $self->madgex_miles_boundaries();

    for ( @boundaries ) {
        if ( $miles <= $_ ) {
            $miles = $_;
            last;
        }
    }

    $miles = $boundaries[-1] if $miles >= $boundaries[-1];

    $id = $self->BEST_LOCATION_MAPPING( $self->madgex_location_mapping() , $location_id, $miles );

    return ($id, $miles);
}

=head2 madgex_field_mappings

Used to map custom fields to IDs of the board.
Your %standard_tokens keys should have the same name
as you specify here for use in $self->madgex_custom_fields()

=cut

sub madgex_field_mappings {
    return (
        salary_from     => 3000015,
        salary_to       => 3000015,
        sectors         => 3000010,
        qualifications  => 3000011,
        eligibility     => 3000021,
        current_role    => 3000013,
        availability    => 3000019,
        languages       => 3000022,
    );
}

=head2 madgex_custom_fields

Used to supply array of multiple QuestionTerms key in search_fields()
Filters through token names and IDs from madgex_field_mappings()

=cut

sub madgex_custom_fields {
    my $self = shift;
    my %field_mappings = $self->madgex_field_mappings();
    my @fields = ();

    while ( my ($field, $id) = each %field_mappings ) {
        my @values = $self->token_value($field) if $self->token_value($field);
        foreach ( @values ) {
            push @fields, $id . "|" . $_;
        }
    }

    if ( defined($self->token_value('default_jobtype')) && defined($self->madgex_jobtype_mapping()) ) {
        my $jobtype = $self->token_value('default_jobtype');
        my %jobtype_mappings = $self->madgex_jobtype_mapping();
        push @fields, $jobtype_mappings{jobtype_id} . "|" . $jobtype_mappings{contract} if $jobtype eq 'contract';
        push @fields, $jobtype_mappings{jobtype_id} . "|" . $jobtype_mappings{permanent} if $jobtype eq 'permanent';
        push @fields, $jobtype_mappings{jobtype_id} . "|" . $jobtype_mappings{temporary} if $jobtype eq 'temporary';
    }

    return uniq(@fields);
}

=head2 madgex_cv_updated

Maps the boards cv updated within values to ours and returns the selected interval value

=cut

sub madgex_cv_updated {
    my ($self, $interval) = @_;
    my %cv_updated = ('TODAY' => 'Day', 'YESTERDAY' => 'Day', '3D' => 'Day', '1W' => 'Week', '2W' => 'TwoWeeks', '1M' => 'Month', '2M' => 'TwoMonths', '3M' => 'ThreeMonths', '6M' => 'ThreeMonths', '1Y' => 'Year', '2Y' => 'Year', '3Y' => 'Year', 'ALL' => '');
    return $cv_updated{$interval};
}

sub feed_identify_error {
    my ( $self, $message, $content ) = @_;

    if ( $content =~ m/Buy credits/ ) {
        $self->throw_lack_of_credit('You do not have enough credits to complete this action.');
    }
    elsif ( $content =~ m/password has expired/ ) {
        $self->throw_login_error('Your password has expired. Please update your credentials on the board before searching.');
    }
    else {
        return;
    }
}

1;


package Stream::Templates::indeed;
use warnings;

use strict;
use utf8;
use base qw(Stream::Engine::API::REST);
use POSIX qw(ceil);
use JSON ();

use Stream::Constants::SortMetrics qw(:all);
################ TOKENS #########################
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);


BEGIN {
    $standard_tokens{last_job} = {
        Label      => 'Title of last job',
        Type       => 'Text',
        SortMetric => 4,
    };

    $standard_tokens{any_job} = {
        Label      => 'Title of any job',
        Type       => 'Text',
        SortMetric => 5,
    };
    $standard_tokens{company_last_job} = {
        Label      => 'Company of latest job',
        Type       => 'Text',
        SortMetric => 6,
    };

    $standard_tokens{company_any_job} = {
        Label      => 'Company of any job',
        Type       => 'Text',
        SortMetric => 7,
    };

    $standard_tokens{experience} = {
        Label       => 'Years of work experience',
        Type        => 'List',
        Values => 'Any amount|Less than 1 year!!1-11|1-2 years!!12-24|3-5 years!!25-60|6-10 years!!61-120|More than 10 years!!121',
        Default    => '',
        SortMetric => 8,
    };

    $standard_tokens{school} = {
        Label      => 'School Name',
        Type       => 'Text',
        SortMetric => 9,
    };

    $standard_tokens{education} = {
    Label       => 'Degree',
    Type        => 'List',
    Values =>   'Any degree|Diploma!!di|Associates!!as|Bachelors!!ba|Masters!!ms|Doctorate!!doc',
    Default    => '',
    SortMetric => 10
    };

    $standard_tokens{study_field} = {
        Label      => 'Field of study',
        Type       => 'Text',
        SortMetric => 11,
    };
};

sub PROXY_CLASS {
    return 'Stream::Proxies::Indeed';
}

sub results_per_page {
    return 30;
}


sub search_url {
    return 'http://api.indeed.com/resumes';
}

sub search_begin_search {
    my $self = shift;

    # If keywords AND location are empty, fake a successful search (return 1
    # without actually searching). Otherwise allow parent's default search.
    unless ( $self->token_value('keywords') ||
             $self->token_value('location') ) {
            my $s2 = $self->user_object()->stream2();
            $self->results()->add_notice( { level => 'info', message => $s2->__("Either 'keywords' or 'location' must be specified. You can't leave both empty.") } );
            return 1;
    }
    return $self->SUPER::search_begin_search( @_ );
}

sub search_submit {
    my $self = shift;
    my %query = $self->field_value_pairs($self->search_fields());
    my $search_uri = URI->new($self->search_url());
    $search_uri->query_form( %query );
    my $response = $self->search_get($search_uri);

    my $count = JSON::decode_json($response->decoded_content)->{'meta'}->{'paging'}->{'total'};

    my $text_location = $query{l};
    if(!$count && $text_location) {
        my $postcode = $self->indeed_location(
            $self->token_value('location_id'),
            $self->token_value('location_within_miles'),
            1
        );
        if ( $postcode ){
            $self->log_info('No results using location text. Attempting again with postcode');
            $query{l} = $postcode;
            $search_uri->query_form( %query );

            $response = $self->search_get($search_uri);
            $count = JSON::decode_json($response->decoded_content)->{'meta'}->{'paging'}->{'total'};
            if($count) {
                $self->results->add_notice(
                    sprintf(
                        'Initial location "%s" not recognised by Indeed.
                        Searching by postcode "%s"',
                        $text_location,
                        $postcode
                    )
                );
            }
        }
    }

    return $response;
}

sub search_request_method {
    return 'GET';
}

sub search_success{
    return qr/^\{"meta":\{"status":200/;
}

sub search_fields {
    my $self = shift;

    my $search_string = $self->token_value('keywords');
    $search_string .= " title:(" . $self->token_value('last_job') . ")" if($self->token_value('last_job')) ;
    $search_string .= " anytitle:(" . $self->token_value('any_job') . ")" if($self->token_value('any_job'));
    $search_string .= " company:(" . $self->token_value('company_last_job') . ")" if($self->token_value('company_last_job'));
    $search_string .= " anycompany:(" . $self->token_value('company_any_job') . ")" if($self->token_value('company_any_job'));
    $search_string .= " school:(" . $self->token_value('school') . ")" if($self->token_value('school'));
    $search_string .= " fieldofstudy:(" . $self->token_value('study_field') . ")" if($self->token_value('study_field'));

    my $rb = "dt:" . $self->token_value('education') if ($self->token_value('education'));
    $rb .= ",yoe:" . $self->token_value('experience') if($self->token_value('experience'));

    my $location = $self->indeed_location($self->token_value('location_id'), $self->token_value('location_within_miles'));

    my $updated = $self->map_cv_updated($self->token_value('cv_updated_within'));
    my %fields = (
        'q'  => $search_string,
        'l'  => $location,
        'radius' => scalar( $self->token_value("location_within_miles") ),
        'rb' => $rb,
        'start' => ($self->results_per_page() * ($self->current_page() - 1)),
        'limit' => $self->results_per_page(),
        'co' => $self->token_value('country_code') // '',
        'v' => 1,
        'client_id' => '024126951b5ed6de58ae730b6bd7619af67090e2d62ecfb1bc37fa20f35eccaf',
        'lmd' => $updated,
    );

    return %fields;
}

sub map_cv_updated {
    my ($self, $updated) = @_;
    my %cv_updated = (
        'TODAY' => 'day',
        'YESTERDAY' => 'day',
        '3D' => 'week',
        '1W' => 'week',
        '2W' => 'month',
        '1M' => 'month',
    );
    return $cv_updated{$updated} // 'all';
}

sub scrape_paginator {
    my $self = shift;
    my $json = $self->response_to_JSON();
    my $no_of_results = $json->{'meta'}->{'paging'}->{'total'};
    $self->total_results($no_of_results);
    my $no_of_pages = ceil ($no_of_results/$self->results_per_page());
    $self->log_debug("No of pages = " . $no_of_pages);
    $self->max_pages($no_of_pages);
}



#sub search_determine_next_page{
#
#my $self = shift;
#
#    my $href = $self->findvalue("//div[\@id='pagination']/a[\@class='next']/\@href");
#    if($href){
#
#        $self->log_debug('The next page is [%s]', $self->search_url().$href);
#        $self->next_page_link( $self->search_url().$href );
#    }
#    else {
#        $self->log_warn('Could not find the link for the next page');
#    }
#
#    return;

#}



#sub search_number_of_results_xpath{
#return '//div[@id="result_count"]';
#}

#sub scrape_candidate_xpath {
#    return "//ol[\@id='results']/li";
#}
sub scrape_candidate_array {
    my $self = shift;
    my $json = $self->response_to_JSON(); ##takes response from json..
    return @{ $json->{'data'}->{'resumes'} } ;
}


sub scrape_candidate_details {
    my $self = shift;
    my $json = shift;

    my %fields = (
        location => $json->{'city'},
        contact     => $json->{'url'},
        candidate_id => $json->{'resumeKey'},
        firstName => $json->{'firstName'},
        lastName => $json->{'lastName'},
        date_created => $json->{'dateCreated'}->{'displayDate'},
        date_modified => $json->{'dateModified'}->{'displayDate'},
        headline => $json->{'headline'},
        skills => $json->{'skills'},
        additional_info => $json->{'additionalInfo'},
        experience => $json->{'workExperiences'},
        education => $json->{'educations'},
        awards => $json->{'awards'},
        certifications => $json->{'certifications'},
        patents => $json->{'patents'},
        publications => $json->{'publications'},
        military_background => $json->{'militaryBackground'},
        military_experiences => $json->{'militaryExperiences'},
    );
}

sub scrape_fixup_candidate {
    my $self      = shift;
    my $candidate = shift;

    my $link = $self->base_url() . "/r/" . $candidate->attr('candidate_id') . "/pdf";
    $candidate->attr('cv_link' => $link);
    $candidate->attr('contact' => $self->base_url()  . $candidate->attr('contact'));
    $candidate->attr('name' => $candidate->attr('firstName') . " " . $candidate->attr('lastName'));

    if (!$candidate->attr('headline')) {
        my $experience = $candidate->attr('experience');
        $candidate->attr('headline' => $experience->[0]->{'title'} . " - " . $experience->[0]->{'company'});
    }

    $candidate->has_profile(1);
    $candidate->has_cv(1);

    $candidate->attr('block_message' => 1 );
    return $candidate;
}

sub do_download_cv {
    my $self = shift;
    my $candidate = shift;

    $self->log_info("In do_download_cv. GETting ".$candidate->attr('cv_link'));

    ## This will use a proxy because of the PROXY_CLASS
    my $resp = $self->get($candidate->attr('cv_link'));
    if ( $resp->is_success() && $resp->decoded_content() ) {
        return $resp;
    }

    return $self->throw_error('Downloading CVs not available');
}

sub base_url {
    return "http://api.indeed.com";
}
sub download_profile {
    return 1;
}

sub download_cv_success {
    return;
}

sub download_cv_fields {
    return;
}

sub indeed_location {
    my ($self, $location_id, $location_within, $use_postcode) = @_;
    return unless $location_id;

    my $location = $self->location_api()->find({ id => $location_id });

    if ( !$location ) {
        $self->log_debug('unable to find location [%s] in our location database', $location_id );
        return;
    }

    if($use_postcode) {
        my $postcode = $location->postcode;
        $self->log_info('Using postcode %s', $postcode);
        return $postcode;
    }

    $self->token_value(country_code => $location->iso_country());
    my $location_text = $location->is_country() ? "" : $location->name();

    # return your custom values after setting country_code
    return 'Wellington Somerset' if $location_id == 40512;
    return 'Ipswich QLD' if $location_text eq 'Ipswich City';

    ## Ticket 37901 - Add state to the US place name
    if ( $location->in_usa() ) {
        my $us_state = $self->BEST_LOCATION_MAPPING("ISO_STATES", $location_id);
        if ( $us_state && $location->is_place() ) {
            $location_text .= ", " . $us_state;
        }
    }
    elsif ( $location->in_uk() ) {
        if ( $location->is_place() && $location->parent->type() !~ m/country|region/ ) {
            $location_text .= ", " . $location->parent->name();
        }
    }

    return $location_text;
}

1;

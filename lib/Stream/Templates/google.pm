#!/usr/bin/perl

package Stream::Templates::google;
use strict;
use utf8;

use base qw(Stream::Engine::API::REST);

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

use JSON;
use LWP::UserAgent;
use URI::Escape;

sub google_split_keywords {
    my $self = shift;
    my $keywords = shift;
    my $and_token = shift;
    my $or_token  = shift;
    my $not_token = shift;
    my $exact_token = shift;

    $self->log_debug('Splitting keywords: [%s]', $keywords);

    my $q = Stream::Keywords->new( $keywords );

    if ( !$q->process_query() ) {
        $self->log_debug('Unable to split up the keywords');
        $self->throw_invalid_boolean_search('We could not process the query');
    }

    $self->log_debug( $q->diag() );

    #Google really wants words to be quoted, Google fails more frequently if they are not.
    $self->token_value( $and_token => join ' ', map { qq/"$_"/ } $q->and_words() );
    $self->token_value( $exact_token => join ' ', map { qq/"$_"/ } $q->exact_words() );
    $self->token_value( $or_token => join ' ', map { qq/ OR "$_"/ } $q->or_words() );
    $self->token_value( $not_token => join ' ', map { qq/-"$_"/ } $q->not_words() );

    return;
}

sub search_url {
    return 'https://www.googleapis.com/customsearch/v1';
}

sub results_per_page { 10; }

sub search_submit {
    my $self = shift;

    my $s2 = $self->user_object()->stream2();

    if( $self->token_value('location_within') ne '30' ){
        $self->results()->add_notice({ level => 'info',
                                       message => $s2->__("Note that this channel does not support Location Radius Filtering.")
                                   });
    }

    my $query = $self->build_google_search_query();

    my @search = (
        'q' => $query,
        'start' => ($self->current_page - 1) * $self->results_per_page(),
        'num' => $self->results_per_page(),
        'key' => 'AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU',
        'cx' => '003231768667224905379:yagfk7izsje',
    );

    my @query_form = $self->field_value_pairs(@search);
    my $search_uri = URI->new( $self->search_url() );
    $search_uri->query_form( @query_form );
    $self->get($search_uri);
}

sub scrape_paginator {
    my $self = shift;
    my $json = $self->response_to_JSON();
    $self->total_results($json->{'searchInformation'}->{'totalResults'});
}

sub scrape_candidate_array {
    my $self = shift;
    my $json = $self->response_to_JSON();
    if ($json->{'items'}){
        return @{ $json->{'items'} };
    }
}

#Tries it's best to find something every time, but it's messy now - overwrite this routine rather than add to it.
sub scrape_candidate_details {
    my $self = shift;
    my $json = shift;
    my %fields = (
        candidate_id => eval { $json->{'formattedUrl'} } || '',
        name => eval{ $json->{'pagemap'}->{'person'}[0]->{'name'} } 
             || eval{ $json->{'pagemap'}->{'hcard'}[0]->{'fn'} } 
             || eval{ $json->{'pagemap'}->{'webpage'}[0]->{'name'} } 
             || eval{ $json->{'pagemap'}->{'metatags'}[0]->{'og:title'} } 
             || '',
        location => eval { $json->{'pagemap'}->{'person'}[0]->{'location'} } || '',
        headline => eval { $json->{'pagemap'}->{'hcard'}[0]->{'title'} } 
                 || eval { $json->{'pagemap'}->{'webpage'}[0]->{'name'} } 
                 || eval { $json->{'title'} } 
                 || '',
        picture_url => eval { $json->{'pagemap'}->{'thumbnail'}[0]->{'src'} } 
                    || eval { $json->{'pagemap'}->{'person'}[0]->{'image'} } 
                    || eval { $json->{'pagemap'}->{'cse_thumbnail'}[0]->{'src'} } 
                    || eval { $json->{'pagemap'}->{'cse_image'}[0]->{'src'} }
                    || eval { $json->{'pagemap'}->{'hcard'}[0]->{'photo'} }
                    || '',
        snippet => eval { $json->{'snippet'} } 
                || eval { $json->{'pagemap'}->{'webpage'}[0]->{'description'} } 
                || '',
        profile_link => eval { $json->{'link'} } || '',
        has_cv => 0,
    );

    return %fields;
}

sub scrape_fixup_candidate{
    my $self = shift;
    my $candidate = shift;

    if (my $profile_link = $candidate->attr('profile_link')){
        #Use href.li to hide us as the referrer
        $candidate->attr('profile_link' => 'http://href.li/?' . $profile_link);
        $candidate->attr('original_profile_link' => $profile_link);

        if ($self->google_add_cache_link()){
            my $cache_link = $profile_link;
            $cache_link =~ s/http:\/\///;
            $candidate->attr('cache_profile_link', 'http://webcache.googleusercontent.com/search?strip=1&q=cache:' . $cache_link);
        }
    }

    $candidate->can_forward( 0 );
}

sub google_add_cache_link { 1; }


sub google_loc_check{
    my $self = shift;
    my $location_text = shift;
    my %resolve_location = (
        "City of London"  =>  "London",
    );

    if( my $correct_location = $resolve_location{ $location_text } ){
        $location_text = $correct_location;
    }

    return $location_text;
}


return 1;

#!/usr/bin/perl

package Stream::Templates::xing_xray;
use strict;
use utf8;

use base qw(Stream::Templates::google_cse);

use Stream::Constants::SortMetrics qw(:all);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

$derived_tokens{and_terms} = {
    Type => 'Text',
    Helper => 'google_split_keywords(%keywords%|and_terms|or_terms|not_terms|exact_terms)',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{or_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{not_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

$derived_tokens{exact_terms} = {
    Type => 'Text',
    SortMetric => $SM_KEYWORDS,
};

sub build_google_search_query {
    my $self = shift;

    my $base_url = 'xing.com/profile/';

    my $keywords = "site:" . $base_url; 

    if ($self->token_value('keywords')){
        if ($self->token_value('and_terms')){
            $keywords .= ' AND (' . $self->token_value('and_terms') . ')';
        }
        if ($self->token_value('exact_terms')){
            $keywords .= ' AND (' . $self->token_value('exact_terms') .')';
        }
        if (my $or_terms = $self->token_value('or_terms')){
            $or_terms =~ s/^\s+OR//;
            $keywords .= ' AND (' . $or_terms .')';
        }
        if ($self->token_value('not_terms')){
            $keywords .= ' AND ' . $self->token_value('not_terms');
        }
    }

    my $location_id = $self->token_value('location_id');

    if ($location_id){
        my $location = $self->location_api()->find({
            id => $location_id,
        });
        if ($location){

            $keywords .= ' AND ("work * * '; 
            my $location_name = $location->name();
                
            if( $location->in_uk() && $location->is_country() ){
                $location_name = "United Kingdom";
            }
            $keywords .= $location_name . '"';

            if( !$location->in_uk ){
                my %lang_codes =( Netherlands => 'NL', Belgium  => 'NL',  France => 'FR' );

                if( my $lang_code = $lang_codes{ $location->country->name }  ){
                    # TODO: implement language in API
                    my $location2 = $self->location_api()->find({
                        id => $location_id,
                        # lang => $lang_code
                    });

                    if( $location2 ){
                        $keywords .= ' OR "work * *' . $location2->name . '"' if( $location2->name ne $location_name );
                    }
                }
            } 

            $keywords .= ')'; 
        }
    }

    return $keywords;
}

sub google_add_cache_link { 0; }

return 1;

package Stream::Templates::CrawledMadgex;

use strict;
use warnings;

use base qw(Stream::Templates::CrawledFeed);

use Stream::Constants::SortMetrics qw(:all);

use Encode;
use URI;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

$derived_tokens{salary_cur} = {
    Type    => 'Currency',
    Helper  => 'SALARY_CURRENCY',
    Options => 'GBP'
};

$derived_tokens{salary_per} = {
    Type    => 'SalaryPer',
    Helper  => 'SALARY_PER',
    Options => 'annum'
};

$standard_tokens{sort_order} = {
    Type       => 'List',
    Label      => 'Sort By',
    Values     => [
        ['Newest First' => 'DateLastModified'],
        ['Relevancy'    => 'Relevancy']
    ],
    Default    => 'DateLastModified',
    SortMetric => $SM_SORT,
};

$standard_tokens{download_status} = {
    Type => 'List',
    Label => 'Download Status',
    Values => [
        ['All'            => ''],
        ['Downloaded'     => 'AlreadyDownloaded'],
        ['Not Downloaded' => 'NotYetDownloaded']
    ],
    Default => '',
};

sub board_group {
    return 'madgex';
}

sub login_success {
    return qr/Sign out/i;
}

#Redirect must be relative to base_url
sub login_url {
    my $self = shift;
    my $redirect = shift || '';
    $redirect = $redirect->path if ref $redirect && $redirect->isa('URI');
    $redirect = '~' . $redirect if $redirect;

    my $login_url = $self->config->get_url('login_form');

    $login_url->query_form( PipelinedPage => $redirect) if $redirect;

    return $login_url;
}

sub check_for_login_error {
    my ($self, $response) = @_;

    my $content = $response->decoded_content;
    if($content !~ $self->login_success) {
        $self->throw_login_error( $self->user_object->stream2->__('Unable to login') );
    }
    
    # We can get signed in but still not be able to continue
    # with expired passwords.
    my $login_errors = $self->config->get_value('login_errors');
    foreach my $error (@$login_errors) {
        if($content =~ $error->{regex}) {
            $self->throw_login_error($error->{message});
        }
    }
}

sub login_fields {
    return (
        #Authtokens may vary
        'Username' => $_[0]->token_value('email') || $_[0]->token_value('username'),
        'Password' => $_[0]->token_value('password'),
    );
}

# Avoid default usage. See _login.
sub login {};

# Login and redirect with GET params
# usage: $self->_login( redirect => $url, query => $query )
# options:
#   redirect - Str. We will  be redirected to this page.
#   query - HashRef. Will be our GET query for the redirected page.
#   success_on_redirect - Truthy/falsy. If true, we will determine login sucess
#   by us being redirected, otherwise by the default check_for_login_error().
# returns the response (After redirects).
sub _login {
    my ($self, %options) = @_;
    $options{redirect} ||= '';
    $options{query} ||= {};
    $self->log_debug('Loggin in with redirect to "%s" and query with keys "%s"', $options{redirect}, keys %{$options{query}});
    $self->get($self->login_url($options{redirect}));
    my %credentials = $self->login_fields;

    #Generate query string
    my $uri = URI->new();
    $uri->query_form(%{$options{query}});
    my $query_string = $uri->query;

    $self->form_with_fields( keys %credentials );
    $self->set_fields(
        %credentials,
        PipelinedQueryString => $query_string,
    );



    my $redirect_success;
    if($options{success_on_redirect}) {
        #If we're redirected to where we asked to be, we've been logged in.
        # Temporarily add a handler to check the first redirect
        $self->agent->set_my_handler(
            response_done => sub {
                my ($response, $agent, $handler) = @_;

                my $actual_redirect = URI->new_abs(
                    $response->headers->header('Location'),
                    $options{redirect}
                );
                my $desired_redirect = URI->new($options{redirect});
                $desired_redirect->query( $query_string );

                $self->log_debug(
                    'Validating login redirect. Desired: %s, Received: %s',
                    $desired_redirect,
                    $actual_redirect
                );

                #Attempt to normalise URLS
                $desired_redirect->path_segments( grep {$_} $desired_redirect->path_segments);
                $actual_redirect->path_segments( grep {$_} $actual_redirect->path_segments);

                $redirect_success = $actual_redirect->eq($desired_redirect);

                #Response done. Remove our handler.
                $agent->set_my_handler(
                    response_done => undef,
                    %$handler
                );

                return undef;
            }
        );
    }

    my $resp = $self->submit();

    $self->recent_content($resp->decoded_content);

    if( $options{success_on_redirect} ){
        if(!$redirect_success) {
            $self->log_debug('Failed login redirect');
            $self->throw_login_error('Unable to login.');
        } else {
            $self->log_debug('Validated login with redirect');
        }
    } else {
        $self->check_for_login_error($resp)
    }


    return $resp;
}

sub search_submit {
    my $self = shift;
    my $search_form_path = $self->search_url->path;

    my %search_fields = $self->search_fields;
    my $response = $self->_login(
        redirect => $search_form_path,
        query    => \%search_fields
    );
    return;
}

sub search_url {
    my $self = shift;
    my $url = $self->config->get_url('search_form');
    $url->path_segments($url->path_segments, $self->current_page);
    return $url;
}

sub search_success {
    #0 results message | results element class
    return qr/Your search returned no results|lister cf block/;
}

sub download_profile_success {
    return qr/Full details/;
}

sub scrape_candidate_headline {
    return 'last_updated' ;
}


sub search_fields {
    my $self = shift;

    my %fields;
    $self->log_info('Using %s custom fields', scalar keys %fields);

    #Crawled fields go straight into the 'q' field.
    my @query_fields;
    my %query_tokens = $self->config->get_tokens();

    while( my($query_token_key, $query_token) = each %query_tokens) {
        if($query_token->{Type} eq 'MultiList') {
            push @query_fields, ($self->get_token_value($query_token_key));
        }
    }

    $fields{MinLastUpdated} = $self->madgex_cv_updated($self->token_value('cv_updated_within'));

    $fields{Keywords} = $self->token_value('keywords');
    $fields{q} = \@query_fields;
    $fields{SortOrder} = $self->token_value_or_default('sort_order');
    $fields{DownloadedStatus} = $self->token_value('download_status');

    if ( my $madgex_location_id = $self->madgex_location_id ) {
        my ($id, $miles) = $self->madgex_location( $self->token_value('location_id'), $self->token_value('location_within_miles') );

        $fields{LocationIds} = $madgex_location_id . "|$id" if $id;
        $fields{RadialLocation} = $madgex_location_id . "|$miles" if $miles;
    }

    if($self->can('fixup_search_fields')) {
        %fields = $self->fixup_search_fields(%fields);
    }
    return %fields;
}

sub search_number_of_results_xpath {
    return '//h2/strong/text()';
}

sub scrape_candidate_xpath {
    return '//li[contains(@class,"card") and contains(@class,"highlight-lighter") and contains(@class,"cf") and contains(@class,"block")]';
}

sub scrape_candidate_details {
    return (
        candidate_id   => './/@id',
        name           => './/h2[@class="lister__header"]/a/text()',
        last_updated   => './/p[@class="small"]/span/strong/text()',
        last_login     => './/p[@class="small"]/strong/text()',
        summary        => './/p[@class="small"]/following-sibling::p/text()',
        #Paragraphs that vary across Madgex boards
        summary_fields => [
            './/dt/..' => {
                label => './dt/text()',
                value => './dd/text()'
            }
        ],
    );
}

sub scrape_fixup_candidate {
    my ($self, $candidate) = @_;

    $candidate->has_profile(1);
    $candidate->has_cv(1);

    # [Name withheld] means the CV needs to be unlocked/purchased
    my $cv_unlocked = $candidate->attr('name') !~ m/\[Name withheld\]/;
    $candidate->attr('cv_unlocked' => $cv_unlocked);
    return $candidate;
}

sub profile_path {
    my $self = shift;
    my $candidate = shift;

    my $profile_path = $self->config->get_url('profile_path');
    $profile_path->path_segments($profile_path->path_segments, $candidate->attr('candidate_id'));

    $self->log_debug('Generated profile url %s', $profile_path);
    return $profile_path->path;
}

sub download_profile {
    my $self = shift;
    my $candidate = shift;

    my $profile_response = $self->_login( redirect => $self->profile_path($candidate) );

    $self->check_for_success_or_failure(
        $self->download_profile_success() || undef,
        $self->download_profile_failed()  || undef,
    );

    $candidate = $self->scrape_generic_download_profile( $candidate );
    return $candidate unless $candidate->attr('cv_unlocked');

    my $cv_response;
    eval {
        $cv_response = $self->download_cv(
            $candidate,
            {
                logged_in => 1,
                #no_unlocking => 1
            }
        );
    };
    if($@) {
        $candidate->attr('cv_html' => 'CV downloading is not available for this candidate.');
        return $candidate;
    }

    if($cv_response && $cv_response->is_success) {
        $self->log_info('Parsing to HTML and appending CV doc to profile');

        my $cv_html = eval { $self->{documents_api}->htmlify($cv_response->content); };

        if ( $@ ) {
            $candidate->attr('cv_html' => 'CV downloading is not available for this candidate.');
        } else {

            my $document_body = $self->{html_parser}->load_html(
                string => Encode::encode('UTF-8', $cv_html),
                encoding => 'UTF-8'
            )->findnodes('/html/body')->[0];

            $cv_html = join( '', map { $_->toString() } $document_body->childNodes() );
            $cv_html = $self->fixup_candidate_cv_html($cv_html);
            $self->log_info('Candidate CV html is %d characters', length($cv_html));
            $candidate->attr('cv_html' => $cv_html);
        }
    }

    return $candidate;
}

sub fixup_candidate_cv_html {
    my ($self, $html) = @_;

    $html =~ s/<img.+?>//g;
    $html =~ s/class="body"//g;
    #A bit funky to avoid an undefined value for $1
    $html =~ s/(<\/?)h[1-2]>/$1h4>/g;

    return $html;
}

sub scrape_download_profile_xpath {
    return '/html/body';
}

sub scrape_download_profile_details {
    #summary_fields and paragraphs vary between Madgex boards
    return (
        name        => './/div[@id="cv-detail"]//h1/text()',
        last_update => './/div[@id="cv-detail"]//h1/following-sibling::p/span/strong/text()',
        last_login  => './/div[@id="cv-detail"]//h1/following-sibling::p/strong/text()',
        summary_fields => [
            './/dt/..' => {
                label => './dt/text()',
                value => './dd/text()'
            }
        ],

        details => ['.//dt[contains(@class, "palm-one-whole")]'],
        paragraphs => [
            './/h3' => {
                title => '.',
                value => './following-sibling::p'
            }
        ],
        action_btn_text => '//li[@class="cv-actions__item"][1]/a/text()',
        action_btn_href => '//li[@class="cv-actions__item"][1]/a/@href',
    );
}


# 1. Unlock CV if it's locked
# 2. Download CV
sub download_cv {
    my ($self, $candidate, $options) = @_;

    my $download_response;

    my $download_path = $self->config->get_url('cv_download');
    $download_path->path_segments(
        $download_path->path_segments,
        $candidate->candidate_id
    );

    if($candidate->attr('cv_unlocked')) {
        #Can go straight to download
        if($options->{logged_in}) {
            $download_response = $self->get($download_path);
        } else {
            $download_response = $self->_login(
                redirect => $download_path,
                success_on_redirect => 1
            );
        }
    } else {
        #Used as a safeguard for automatic in-profile downloads.
        return if $options->{no_unlocking};

        $self->log_info('Unlocking CV');
        my $unlock_path = $self->config->get_url('cv_unlock');

        #Login will redirect to unlock form
        my $unlock_form_response = $self->_login(
            redirect => $unlock_path,
            query    => {
                UserID    => $candidate->candidate_id,
                ReturnUrl => $download_path
            },
            success_on_redirect => 1
        );

        if($unlock_form_response->decoded_content =~ m/Buy credits/) {
            $self->throw_lack_of_credit('You do not have enough credits to complete this action');
        }
        # We're on the unlock confirmation page
        # that we've set up to redirect us to the download.
        $download_response = $self->submit();
        $candidate->attr('cv_unlocked', 1);
    }

    # Something's gone wrong if we didn't get a file.
    if ($download_response->headers->header('content_disposition') !~ m/attachment/) {
        my $content = $download_response->decoded_content;
        my $error = 'File not downloaded';
        $error = 'Board daily download limit reached' if $content =~ m/You have reached your daily download limit/;
        $self->throw_cv_unavailable($error);
    }

    return $self->after_downloading_cv($candidate, $download_response);
}

sub madgex_location_id {
    my $self = shift;
    return $self->config->get_value('location_input_id');
}

=head2 madgex_miles_boundaries

Distance mappings for the board found on boards search form
Used in madgex_location() below

=cut

sub madgex_miles_boundaries {
    return qw/0 5 10 15 20 50/;
}

=head2 madgex_location_mapping

The name of the database table which stores the location id for the board.

=cut

sub madgex_location_mapping {
    return 'madgex_locs_v4';
}

=head2 madgex_location

Uses global Madgex location mapping given location_id/miles_within
Returns location mapping ID and mapped miles from madgex_mile_boundaries()

=cut

sub madgex_location {
    my ($self, $location_id, $miles) = @_;
    return unless defined $location_id;

    $miles ||= 0;

    my @boundaries = $self->madgex_miles_boundaries();

    for ( @boundaries ) {
        if ( $miles <= $_ ) {
            $miles = $_;
            last;
        }
    }

    $miles = $boundaries[-1] if $miles >= $boundaries[-1];

    my $id = $self->BEST_LOCATION_MAPPING( $self->madgex_location_mapping() , $location_id, $miles );

    return ($id, $miles);
}

=head2 madgex_cv_updated

Maps the boards cv updated within values to ours and returns the selected interval value

=cut

sub madgex_cv_updated {
    my ($self, $interval) = @_;
    my %cv_updated = (
        'TODAY' => 'Day',
        'YESTERDAY' => 'Day',
        '3D' => 'Day',
        '1W' => 'Week',
        '2W' => 'TwoWeeks',
        '1M' => 'Month',
        '2M' => 'TwoMonths',
        '3M' => 'ThreeMonths',
        '6M' => 'ThreeMonths',
        '1Y' => 'Year',
        '2Y' => 'Year',
        '3Y' => 'Year',
        'ALL' => ''
    );
    return $cv_updated{$interval};
}

=pod

# Uses values from salary_cur and salary_per
# to select values from salary_ranges()
# salary ranges should return a hashref with the following structure
{
    unspecified => [
        values to send when this currency and unspecified is selected
    ]
    $currency => {
        unspecified => [
            values to send when this currency and unspecified is selected
        ],
        ranges => {
            $key => [$min, $max]
        }
    },
    $currency2 => {...}
}

=cut

sub _salary_ids {
    my $self = shift;

    return unless my $salary_mappings = $self->salary_mappings;

    my $salary_cur = $self->token_value('salary_cur');
    my %ranges = %{$salary_mappings->{$salary_cur}->{ranges}};
    my @matches = ();
    my $matched;

    no warnings 'numeric';

    my $from = int $self->token_value('salary_from');
    my $to   = int $self->token_value('salary_to');

    if($from > $to) {
        my $temp = $from;
        $from = $to;
        $to = $temp;
    }

    $self->log_debug('Finding ranges between %s and %s', $from, $to);

    while( my ($key => $range) = each %ranges) {
        $matched = 0;
        my $min = $range->[0];
        my $max = $range->[1];
        if($from) {
            $matched = 1 if $from >= $min && $from < $max;
        }
        if($to) {
            $matched = 1 if $to >= $min && $to < $max;
        }

        if($to && $from) {
            $matched = 1 if grep {$_ >= $from && $_ < $to} ($min, $max);
        }

        #We're using a band if either its min or its max is between from and to.
        push @matches, $key if $matched;
    }

    #Add unspecified only if we're using others.
    if( @matches && $self->token_value('include_unspecified_salaries') ) {
        # Use both general unspecified and currency specific unspecified values
        push @matches, @{$salary_mappings->{unspecified}}  if $salary_mappings->{unspecified};
        push @matches, @{$salary_mappings->{$salary_cur}->{unspecified}}  if $salary_mappings->{$salary_cur}->{unspecified};
    }

    $self->log_debug( 'Mapped salary IDs: %s', join(', ', @matches) );
    return  @matches;
}

sub feed_identify_error {
    my ( $self, $message, $content ) = @_;

    if ( $content =~ m/Buy credits/ ) {
        $self->throw_lack_of_credit('You do not have enough credits to complete this action.');
    }
    elsif ( $content =~ m/password has expired/ ) {
        $self->throw_login_error('Your password has expired. Please update your credentials on the board before searching.');
    }
    else {
        return;
    }
}

1;
package Stream::Templates::jobstoday;
use base qw(Stream::Templates::CrawledMadgex);

use strict;
use warnings;


__PACKAGE__->load_tokens_from_config();

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

use Stream::Constants::SortMetrics qw(:all);

$authtokens{jobstoday_username} = {
    Label      => 'Username',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 1,
};

$authtokens{jobstoday_password} = {
    Label      => 'Password',
    Type       => 'Text',
    Mandatory  => 1,
    SortMetric => 2,
};

$derived_tokens{salary_cur}->{Options} = 'GBP';
$derived_tokens{salary_per}->{Options} = 'annum';

sub fixup_tokens {
    my $self = shift;
    my %tokens = @_;

    # Skip preferred salary token as it's being mapped to the default salary input.
    delete $tokens{36}
      or die('Could not find "Preferred Salary" token');

    return %tokens;
}

sub salary_mappings {
    return {
        GBP => {
            ranges => { 
               # id => range
               '36|103' => [0       =>  10_000],
               '36|104' => [10_000  =>  15_000],
               '36|105' => [15_000  =>  20_000],
               '36|106' => [20_000  =>  25_000],
               '36|107' => [25_000  =>  30_000],
               '36|108' => [30_000  =>  40_000],
               '36|109' => [40_000  =>  50_000],
               '36|110' => [50_000  =>  60_000],
               '36|111' => [60_000  =>  70_000],
               '36|112' => [70_000  => 100_000],
               '36|113' => [100_000 =>   "inf"]
            }
        }
    };
}


sub fixup_search_fields {
    my $self = shift;
    my %fields = @_;

    my @salary_ranges = $self->_salary_ids;
    push @{$fields{q}}, @salary_ranges;

    return %fields;
}


1;

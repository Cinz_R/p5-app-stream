#!/usr/bin/env perl

# vim:expandtab:tabstop=4

# $Id: talentspa.pm 118179 2014-03-14 09:21:18Z gareth $

package Stream::Templates::talentspa;

=head1 NAME

Stream::Templates::talentspa - iProfile search API

=head1 CONTACT

Tom (Thomas.Newman@iprofile.org) - Test Team Lead

(New contact will be Graham when he starts)

=head1 TESTING INFORMATION

New credentials from Tom:

Security Key: 'cba2bc18-59ac-4448-9b54-dcd5002d89ab

Username: patrick@broadbean.com
Password: Tal3ntSpa

=head1 DOCUMENTATION

=head1 OVERVIEW

This is a copy of the iprofile feed, TalentSpa is a product of Iprofile. The main 
difference, as far as we are concerned, is the lack of CV download. The preview / profile 
is still available and acts in the place of a CV.

=cut

use strict;
use base qw(Stream::Templates::iprofile);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens %global_authtokens);

BEGIN {
        __PACKAGE__->inherit_tokens();
};

# Authtokens
$authtokens{'talentspa_username'} = {
    Label      => 'Username', # Usually the Agency email address
    Type       => 'Text',
    Size       => 20,
    SortMetric => 1,
};

$authtokens{'talentspa_password'} = {
    Label      => 'Password', # Usually the Agency password
    Type       => 'Text',
    Size       => 20,
    SortMetric => 2,
};

$authtokens{'talentspa_email'} = {
    Label      => 'Recruiter Email', # Must be the Recruiter's email address
    Type       => 'Text',
    Size       => 20,
    SortMetric => 3,
};

$global_authtokens{'talentspa_security_key'} = {
    Label      => 'Security Key', # Security key gives us access
    Type       => 'Text',
    Size       => 60,
    SortMetric => 2,
};

#################### DOWNLOAD CV ##############
sub download_cv_soap_action { return q{http://www.iprofile.org/candidategateway/GetFormattedCVById}; }

sub download_cv_xml {
    my $self = shift;
    my $candidate = shift;

    # this is the format mask for the security key
    my $dummy_security_key = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx';

    return $self->talentspa_download_cv_xml(
        $self->token_value('security_key') || '',
        $candidate->candidate_id()         || '',
    );
}

# separate subroutine so we can hardcode the candidate id for the demo
sub talentspa_download_cv_xml {
    my $self = shift;
    my $security_key = shift;
    my $candidate_id = shift;

    return $self->soap_xml({
        header => "iprofile_candidate_gateway_header",
        body   => sub {
            my $self = shift;
            my $xml  = shift;

            $xml->startTag('GetFormattedCVById',
                xmlns => q{http://www.iprofile.org/candidategateway/},
            );
                $xml->startTag('request');
                    $xml->dataElement( 'SecurityKey' => $security_key );
                    $xml->dataElement( 'IProfileID'  => $candidate_id );
                    $xml->dataElement( 'CVType'      => 1, ); # 1=Original; other types not defined
                $xml->endTag('request');
            $xml->endTag('GetFormattedCVById');

            return $xml;
        },
    });
}

sub scrape_download_cv_xpath   { return '//CGW:ByteCV'; }
sub scrape_download_cv_details {
    return (
        resume_word_base64 => './text()'
    );
}

sub after_scrape_download_cv_success {
    my $self = shift;
    my $candidate = shift;

    # work out a cv filename/content type from the resume_file_ext
    # resume_file_ext have the format "*.doc" etc
    my $extension = "doc";
    my $filename = 'taletnspa_' . $candidate->candidate_id() . '.' . $extension;

    # tidy up the filename
    $filename =~ s/\*//g;
    $filename =~ s/\.+/./g;

    $self->log_debug('Built filename [%s] (extension was [%s])', $filename, $extension);
    $candidate->attr('resume_filename' => $filename);

    return $candidate;
}

1;
# vim: expandtab shiftwidth=4

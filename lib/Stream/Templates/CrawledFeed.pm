package Stream::Templates::CrawledFeed;

use base qw(Stream::Engine::Robot);

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

=head1 NAME

Stream::Templates::CrawledFeed - A feed that uses configuration information scraped from a remote site.
Set the crawler in $self->new_crawler.
See Stream::FeedCrawler::Crawler

=cut

use strict;
use warnings;

use Stream::FeedCrawler::Config;

=head2 load_crawler_tokens

Get dynamic board data from crawling & potentially
other sources.

use the hook fixup_tokens(%tokens) to alter tokens before loading. The token hash returned by this hook is used.
=cut

sub load_tokens_from_config {
    #We're not an object yet, just a class.
    my $class = shift;
    $class->inherit_tokens();

    my %tokens = $class->config->get_tokens;
    $class->log_debug('Using %s crawled tokens.', scalar keys %tokens);
    if($class->can('fixup_tokens')) {
        $class->log_info('Running fixup_tokens hook');
        %tokens = $class->fixup_tokens(%tokens);
    }

    no strict 'refs';
    #Add to existing tokens
    %{ $class . '::standard_tokens' } = (%{ $class . '::standard_tokens' }, %tokens);
}

=head2 config

A cached config object. Used to retrieve data
of each board from when it was last scraped.

=cut

sub config {
    my $self = shift;
    return $self->{config} ||= $self->new_config if ref $self;
    return $self->new_config;
}

=head2 new_crawler

Returns a new instance of our config

=cut

sub new_config {
    my $self = shift;
    $self->log_info('Loading config');

    return Stream::FeedCrawler::Config->new(
        board => $self->destination(),
        $self->can('board_group') ? (board_group =>  $self->board_group) : (),
        ref $self ? (stream2 => $self->user_object->stream2) : ()
    );
}
1;

#!usr/bin/perl
use warnings;

package Stream::Templates::viadeorecruiter;
$Stream::Templates::viadeorecruiter::VERSION = '0.006';
use strict;
use utf8;
use base qw(Stream::Engine::API::REST);

use Stream::Constants::SortMetrics qw(:all);
use Encode;
use Locale::Language;
################ TOKENS #########################
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

=test auth tokens

=cut

BEGIN {

=head2 AUTHTOKENS

=cut

    $standard_tokens{company} = {
        Label      => 'Company Name',
        Type       => 'Text',
        Example    => '',
        SortMetric => $SM_KEYWORDS - 4,
        Size       => 40,
    };

    $standard_tokens{name} = {
        Label      => 'Candidate Name',
        Type       => 'Text',
        Example    => '',
        SortMetric => $SM_KEYWORDS - 5,
        Size       => 40,
    };

    $standard_tokens{school} = {
        Label      => 'School Name',
        Type       => 'Text',
        Example    => '',
        SortMetric => $SM_KEYWORDS,
        Size       => 40,
    };

    $standard_tokens{position} = {
        Label      => 'Job Title',
        Type       => 'Text',
        Example    => '',
        SortMetric => $SM_KEYWORDS - 2,
        Size       => 40,
    };

#### Options: none, all, any, strict
    $standard_tokens{company_option} = {
        Label      => 'Match all company words?',
        Type       => 'List',
        Example    => '',
        Default    => 'any',
        SortMetric => $SM_KEYWORDS - 3,
        Values     => '(At least one word=any|All words=all|Exact phrase=strict)',
    };

    $standard_tokens{position_option} = {
        Label      => 'Match all position words?',
        Type       => 'List',
        Example    => '',
        Default    => 'any',
        SortMetric => $SM_KEYWORDS - 1,
        Values     => '(At least one word=any|All words=all|Exact phrase=strict)',
    };

    $standard_tokens{language} = {
        Label        => 'Resume Language',
        Type         => 'List',
        Values      => '(|French=fr|English=en|German=de|Spanish=es|Italian=it|Portuguese=pt|Russian=ru)',
       #Helper       => 'Bean::Locale::lang()',
        SortMetric   => $SM_LOCATION,
    };

    $standard_tokens{keyword_option} = {
        Label      => 'Match all keywords?',
        Type       => 'List',
        Example    => '',
        Default    => 'any',
        SortMetric => $SM_KEYWORDS - 6,
        Values     => '(At least one word=any|All words=all|Exact phrase=strict)',
    };

    $standard_tokens{industry} = { # {{{ 
        Label => 'Industry',
        Type  => 'List',
        Values => join('|', '(|Aeronautic - Navy - Space - Armament=okipalfjvtlqhnhbpkupjbbqOkimtixeDocDarazzhkepfeliAAA',
'---Aeronautic construction=okipalfjvtlqhnhbpkupjbbqOkimtixeDocDarazzhkepfeliAAA,okipalfjvtlqhnhbpkupjbbqOncwtkAyEpDVgqEichIouDttDdpA',
'---Armament=okipalfjvtlqhnhbpkupjbbqOkimtixeDocDarazzhkepfeliAAA,okipalfjvtlqhnhbpkupjbbqOlvAhyfvdDeVyuemdqEvgprbbEek',
'---Equipment and aeronautic systems=okipalfjvtlqhnhbpkupjbbqOkimtixeDocDarazzhkepfeliAAA,okipalfjvtlqhnhbpkupjbbqOlznyeemjbEuDvlIozebbhhAcEgA',
'---Naval construction=okipalfjvtlqhnhbpkupjbbqOkimtixeDocDarazzhkepfeliAAA,okipalfjvtlqhnhbpkupjbbqOnyxjnhjjaVOIcjufkxkssVfwbxA',
'---Spatial industry=okipalfjvtlqhnhbpkupjbbqOkimtixeDocDarazzhkepfeliAAA,okipalfjvtlqhnhbpkupjbbqOnhyjjaoEEtclmlzbmIqybkmjnoA',
'Agribusiness and agriculture=okipalfjvtlqhnhbpkupjbbqOnvmflnoxnrbgwsDhbtmIAAqvtxk',
'---Agriculture=okipalfjvtlqhnhbpkupjbbqOnvmflnoxnrbgwsDhbtmIAAqvtxk,okipalfjvtlqhnhbpkupjbbqOmVmIEtuDpeepyfmdqcOmVhDhjzA',
'---Fishing=okipalfjvtlqhnhbpkupjbbqOnvmflnoxnrbgwsDhbtmIAAqvtxk,okipalfjvtlqhnhbpkupjbbqOnqnIoroxfsssEeykInxbuchdIfk',
'---Fresh and perishable products=okipalfjvtlqhnhbpkupjbbqOnvmflnoxnrbgwsDhbtmIAAqvtxk,okipalfjvtlqhnhbpkupjbbqOktzEgubqqcvishlwVtoAzmkshgk',
'---Frozen food=okipalfjvtlqhnhbpkupjbbqOnvmflnoxnrbgwsDhbtmIAAqvtxk,okipalfjvtlqhnhbpkupjbbqOnhtstlEOdnofiuhtbiOuajvtrak',
'---Intermediary produce=okipalfjvtlqhnhbpkupjbbqOnvmflnoxnrbgwsDhbtmIAAqvtxk,okipalfjvtlqhnhbpkupjbbqOnEzzubApwtDcIibmqAesdgjhIxA',
'---Wines - Spirits=okipalfjvtlqhnhbpkupjbbqOnvmflnoxnrbgwsDhbtmIAAqvtxk,okipalfjvtlqhnhbpkupjbbqOkAjxOhEdvejjVnjhcouDhmkmmhk',
'Art and Culture=okipalfjvtlqhnhbpkupjbbqOlsVqbfIcqnrjAkjnDaubufosczk',
'---Architecture=okipalfjvtlqhnhbpkupjbbqOlsVqbfIcqnrjAkjnDaubufosczk,okipalfjvtlqhnhbpkupjbbqOlVvxIeflEumbszIEvVvkjxpllvk',
'---Cinema=okipalfjvtlqhnhbpkupjbbqOlsVqbfIcqnrjAkjnDaubufosczk,okipalfjvtlqhnhbpkupjbbqOmcsrstcVgmqrOzcgpnaeVAAhxIk',
'---Design=okipalfjvtlqhnhbpkupjbbqOlsVqbfIcqnrjAkjnDaubufosczk,okipalfjvtlqhnhbpkupjbbqOnzpsEDilwksgnEwgyrvAytoIkgk',
'---Literature=okipalfjvtlqhnhbpkupjbbqOlsVqbfIcqnrjAkjnDaubufosczk,okipalfjvtlqhnhbpkupjbbqOnkraxkVOijtyEtefbVeIzcrOjpA',
'---Music=okipalfjvtlqhnhbpkupjbbqOlsVqbfIcqnrjAkjnDaubufosczk,okipalfjvtlqhnhbpkupjbbqOnmjnscOeoDnlIqgolmqgbwacdkk',
'---Painting=okipalfjvtlqhnhbpkupjbbqOlsVqbfIcqnrjAkjnDaubufosczk,okipalfjvtlqhnhbpkupjbbqOkefhvdqxIcqckqalecswewrxtlA',
'---Performing Arts=okipalfjvtlqhnhbpkupjbbqOlsVqbfIcqnrjAkjnDaubufosczk,okipalfjvtlqhnhbpkupjbbqOkdDnEcEVrceEmpmOOetdhdczrkA',
'---Sculpture=okipalfjvtlqhnhbpkupjbbqOlsVqbfIcqnrjAkjnDaubufosczk,okipalfjvtlqhnhbpkupjbbqOmxODgquxIpztojbztVboAawtabA',
'Business services=okipalfjvtlqhnhbpkupjbbqOmimndbygrDpqaEvpOOxrepjjggk',
'Charities, NGOs and Associations=okipalfjvtlqhnhbpkupjbbqOmkukeotsIVmcmjijkcwqqjmyElA',
'---NGO=okipalfjvtlqhnhbpkupjbbqOmkukeotsIVmcmjijkcwqqjmyElA,okipalfjvtlqhnhbpkupjbbqOnatqpfdmsOkjlfcfhqDVeebIsak',
'---Not for profit associations=okipalfjvtlqhnhbpkupjbbqOmkukeotsIVmcmjijkcwqqjmyElA,okipalfjvtlqhnhbpkupjbbqOmiuxagfVeinxvIpljfmrknVjsaA',
'Communication and Media=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek',
'---Advertising=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOkbmdVbqriIyarnAziEktppAwAwA',
'---Audiovisual and Radio=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOntrcqOncrjfVzgefrEwcebsgEjA',
'---Film / Documentary=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOkbmpIOblxnzIIVDcewdxioIAsyA',
'---Internet=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOlVqgOatqjhchdsnkIutdwdhtwzk',
'---Music=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOnimkjttngqzpAtqVnywmxjmnEAk',
'---Posters and billboards=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOkbmtupmgosyylnynIyjjfgnfoAA',
'---Press and editorial=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOliqfwVAIyrOoVyjDgkjEtVacwOA',
'---Printing - publication=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOleptuyjInvcdAkjvvaiowxEpamk',
'---Radio=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOkhOktqOsgprggwmcOEwIsOxcOEA',
'---Trade shows and exhibitions=okipalfjvtlqhnhbpkupjbbqOmrsyoEgxayEybhVxAmbbpjzIsek,okipalfjvtlqhnhbpkupjbbqOlrDebvyznAuryahrmmDfwxxlyjk',
'Construction=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA',
'---Alarm Fitter=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOnavyrptAbklcDbgadpyOgVAIEvk',
'---Architecture=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOnetwDDsdancufcdEivurOhgdoek',
'---Bricklayer=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOllafssAlVdVjisxmhVVAoDOEdsk',
'---Carpenter=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOnwlAizuhysbiDnggnbAqAlVdygA',
'---Carpenter=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOkeuyIbytDqOoOiAowkAlAbOElsA',
'---Chimney Sweep=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOmEwDVrrozmEkOsheAwwgfgqzzaA',
'---Construction=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOlEyAIlgzhthsbdwwmafmvrEuAfk',
'---Electrician=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOkDDqAcjmfDvxqgAunobobgvhtlk',
'---Heating Engineer=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOmqAouamymwckkAyIagecxcIIOvA',
'---Insulation fitter=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOnogOlgtvOqafVzxvApjrdnfvsIA',
'---Locksmith=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOnzxpVuDjsvuybivzhtaDobiObmA',
'---Metallurgist=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOlcmhplbhevbtheqzIvIjfsIbxpk',
'---Painter / Decorator=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOnkdgIcqkzylobDsbwssAjzuereA',
'---Plasterboarder=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOnvqIrhElkqOVIswutcrymciuEqA',
'---Plasterer=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOlaDbifnfiuDxcfqDzanoaiitblk',
'---Plumber=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOmhtDdDyAhkfDbppIkOpDyDfwmIk',
'---Roofer=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOkVIvzeertteahybsEbppzrksaqA',
'---Surveyor =okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOnnktainIgbzsafIArEAvchjemAk',
'---Swimming Pool construction=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOkbmrxwIcsImqwxxopexrrIcibOk',
'---Tiling=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOmEAafybdkgtEawOogiAwAcmkjwA',
'---Welder=okipalfjvtlqhnhbpkupjbbqOlfpghIrAtklxfIhbisbjabmwAeA,okipalfjvtlqhnhbpkupjbbqOkhkkoumpAgxVfwfqfwjcqwzzdlA',
'Consulting and Services=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk',
'---Car rental and leasing - Miscellaneous=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOkIhbhwAuxmmVyvvnAAqAazhuutA',
'---Computer services=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOmptryDwmaufVxxcDodDquIgaffA',
'---Engineering - Projects management=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOnieEVsqsqbfxjqIsywEulwVDwcA',
'---Human resources and recruitment=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOmrzoAEootvykqbjEiVztVjugqAk',
'---Industrial Maintenance=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOmyuDagunseshOiugdnAzormOAOA',
'---Industrial Waste Disposal - Safety=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOkgsfwVwrcopippwuhAmzIzVOonk',
'---Legal=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOloprmfywaismdjExvsEDkfAubdA',
'---Leisure - Culture - Cinema=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOlkAcEEdadzngmtEIAkcqnlzsoIA',
'---Market Research=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOluDpVExonlApfzcrxvqfoiApDfk',
'---Marketing and Advertisement=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOlccIivDwEIvikApafDdnjyoqgAk',
'---Organization and Strategy=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOkqEValmgrxAgoqIAngfVngOabxk',
'---Public Relations and communication=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOmozEoaqDwqtlmjyoejIefAxdVDk',
'---Real Estate=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOkDIDdOmdExnvawEtDAapjenmkDk',
'---Temporary employment=okipalfjvtlqhnhbpkupjbbqOllDIImgDvDOhvuxwywgmtqhrenk,okipalfjvtlqhnhbpkupjbbqOnpIuvuwkdwOppaAhVdflsbfskfA',
'Consumer Goods=okipalfjvtlqhnhbpkupjbbqOkfiychIoiIVlqrhkptfAxqlddzk',
'---Furniture - Home Appliances=okipalfjvtlqhnhbpkupjbbqOkfiychIoiIVlqrhkptfAxqlddzk,okipalfjvtlqhnhbpkupjbbqOmwoDlpOzxrpjEVVkmqpejbtrmbA',
'---Leisure - Sports=okipalfjvtlqhnhbpkupjbbqOkfiychIoiIVlqrhkptfAxqlddzk,okipalfjvtlqhnhbpkupjbbqOlmuknOscpjakbgrxIgArplfEjtA',
'---Luxury Goods and Cosmetics=okipalfjvtlqhnhbpkupjbbqOkfiychIoiIVlqrhkptfAxqlddzk,okipalfjvtlqhnhbpkupjbbqOndoxjcrntDhwvvmrInbmOVmIryk',
'---Miscellanous manufactured products=okipalfjvtlqhnhbpkupjbbqOkfiychIoiIVlqrhkptfAxqlddzk,okipalfjvtlqhnhbpkupjbbqOmnoriaemyoiidhvbeuEAimtvigA',
'---Textile - Clothing - Accessories=okipalfjvtlqhnhbpkupjbbqOkfiychIoiIVlqrhkptfAxqlddzk,okipalfjvtlqhnhbpkupjbbqOljvthOgypwArEOIbaEdwzhfhDdA',
'Distribution=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk',
'---Cars - Motorbikes=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOkluDDitjcbhxsgcnDbbIiOowxuk',
'---Clothing - Luxury products - Fashion - Sport=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOmhlmocrAIswgDAwshoAnuvlhIDk',
'---Computers - Consumer electronics=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOmAoIrmylpltuiDkzzcndbclcyyA',
'---Discount Stores=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOmdswuzgpcVAbDpazzgwEadkcwmk',
'---DIY - Gardening=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOloeawjxeImqgoexuyItdnIADuEk',
'---e-commerce and distance selling=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOknxsbchbmcEAxwvlkqwdbstiAtA',
'---Grocery and food businesses=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOlIarqwewxmkVcreowaOmseOcawk',
'---Import - Export=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOnavOlDbesqzIlngmpwwdqIbnfzA',
'---Local shops=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOkuruoiihOpzvmzjOyjhApgngshk',
'---Purchase=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOljexceaxhrnbzcjylqaxmqogseA',
'---Records - Books=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOmzyhdzIpjlzddIfvtbpjVVelmgA',
'---Retail=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOkDnDnhEeOypvkfgzIkufDAledEA',
'---Supermarkets=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOmAnzdprpsbEuOmfscAiVlyimhgk',
'---Trade - Industrial Wholesale Trade=okipalfjvtlqhnhbpkupjbbqOnxqozhwaxotrhmdvieOOAznEnAk,okipalfjvtlqhnhbpkupjbbqOncuhItAEvvilarariawthAAItIA',
'Education ',
' Training=okipalfjvtlqhnhbpkupjbbqOnatzfreltAmtqgrxcmOjcldIksk',
'---Créche=okipalfjvtlqhnhbpkupjbbqOnatzfreltAmtqgrxcmOjcldIksk,okipalfjvtlqhnhbpkupjbbqOkomguqIVktAbDpaOaAjcezffjtA',
'---Further/Technical College=okipalfjvtlqhnhbpkupjbbqOnatzfreltAmtqgrxcmOjcldIksk,okipalfjvtlqhnhbpkupjbbqOmDabVErVkdAIpdAEtAdngkntjhk',
'---Kindergarten=okipalfjvtlqhnhbpkupjbbqOnatzfreltAmtqgrxcmOjcldIksk,okipalfjvtlqhnhbpkupjbbqOkvVuIAdvdnhiziVzvAqpAqmlcrA',
'---Primary School=okipalfjvtlqhnhbpkupjbbqOnatzfreltAmtqgrxcmOjcldIksk,okipalfjvtlqhnhbpkupjbbqOlfqDxkkjVflwjtvVIkIEOiwhAjA',
'---Research and Development=okipalfjvtlqhnhbpkupjbbqOnatzfreltAmtqgrxcmOjcldIksk,okipalfjvtlqhnhbpkupjbbqOlDdOnppoODsgeikjOEfsDDveVxk',
'---Secondary School=okipalfjvtlqhnhbpkupjbbqOnatzfreltAmtqgrxcmOjcldIksk,okipalfjvtlqhnhbpkupjbbqOkApOcydIwmztqtdtqyEqOhqAcgk',
'---Universities=okipalfjvtlqhnhbpkupjbbqOnatzfreltAmtqgrxcmOjcldIksk,okipalfjvtlqhnhbpkupjbbqOndjDnpDhVeeiixorrbwmDEwOflA',
'---Vocational Training=okipalfjvtlqhnhbpkupjbbqOnatzfreltAmtqgrxcmOjcldIksk,okipalfjvtlqhnhbpkupjbbqOkVDeVDiVesErIaoiDbudxwqOqIk',
'Finance=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA',
'---Asset Management=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOlzemcfOwsEdlIhyrbDtpezlikEk',
'---Banking=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOnExEdgAomtnvysIicftkdbzrwok',
'---Business Management=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOktoAehogubyclVkpAgzlscpusqA',
'---Certified Public Accounting=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOnbzrcnEjkfmbnEVrgvdOrboclzk',
'---Financial information and communication=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOnIxumOAtDtyqcOpaIImouwevAEA',
'---Financial Services=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOmjklnnzEbkuypvdqeyjjcrxadwk',
'---Insurance=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOnkcqybdezxmcxiAaOthjilnOznk',
'---Real Estate=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOljmccEdlgAwxOIgEIOkIfxusauA',
'---Stock Markets - Brokers=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOmdqmmcocidvODIhVhkOxixqzkAA',
'---Venture Capital, LBOs and Private Equity=okipalfjvtlqhnhbpkupjbbqOnOwiftuptpwmuasfpmzyqxkEOvA,okipalfjvtlqhnhbpkupjbbqOmsnDEtrsbinxjlokbnDmkOErmok',
'Food=okipalfjvtlqhnhbpkupjbbqOkOqkyphnkjapucwAmnkEVflzomk',
'---Bakery=okipalfjvtlqhnhbpkupjbbqOkOqkyphnkjapucwAmnkEVflzomk,okipalfjvtlqhnhbpkupjbbqOlxryDxtmlOforkkauquqniylkeA',
'---Butcher=okipalfjvtlqhnhbpkupjbbqOkOqkyphnkjapucwAmnkEVflzomk,okipalfjvtlqhnhbpkupjbbqOnygewhnaAwzqVVtkOIsjslezeck',
'---Cheese Shop=okipalfjvtlqhnhbpkupjbbqOkOqkyphnkjapucwAmnkEVflzomk,okipalfjvtlqhnhbpkupjbbqOmEfslifzwprfhObibAEVAxdasvk',
'---Fishmonger=okipalfjvtlqhnhbpkupjbbqOkOqkyphnkjapucwAmnkEVflzomk,okipalfjvtlqhnhbpkupjbbqOkaVxiIawgyrthDOlpVOAqDIIpmk',
'---Greengrocer=okipalfjvtlqhnhbpkupjbbqOkOqkyphnkjapucwAmnkEVflzomk,okipalfjvtlqhnhbpkupjbbqOltAprjgxIVuOafecenyjDammlOk',
'---Supermarket=okipalfjvtlqhnhbpkupjbbqOkOqkyphnkjapucwAmnkEVflzomk,okipalfjvtlqhnhbpkupjbbqOnxcwvqaopbkhditDadgonsebueA',
'---Takeaway / Deli=okipalfjvtlqhnhbpkupjbbqOkOqkyphnkjapucwAmnkEVflzomk,okipalfjvtlqhnhbpkupjbbqOnAcsgVEEneyDIgaylqjgfbOwfzA',
'Health=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk',
'---Cardiology=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOmswIouEtibsnwOAmyfEunbmgIlA',
'---Clinical=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOmwyveyvoEVuhEoiVtsEhajriedk',
'---Dentistry=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOnyVIdEtmpihIbjxzpAwqxjDmrgk',
'---Dermatology=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOmpzcvetzVDlsmkeoxvmqfwwVrxA',
'---Doctor=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOneVIoyDbVhckqiydiApkEkyiAbk',
'---ENT (ear, nose and throat) Specialist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOnkAkEqmlfhbIAEvjVAnywpneusA',
'---Gynechologist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOnuOrvvEpmmhmiAlijgdDAcocuAA',
'---Homeopathist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOmvhhsDeuazxyvdnIVqmOpErhmek',
'---Midwife=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOnEvmeIlmhznfiVilfiejAObtyeA',
'---Nursery=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOmAqwzlkqAkohfbjsveowVEdpziA',
'---Opthamologist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOnsuoEVdpozfupDjcfpvmiEEODvA',
'---Optician=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOmkyDoeVDxzhhxjDDdgpOgnOdiiA',
'---Osteopath=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOnwDpfvIiwtxuEucEbnnderVuezk',
'---Paediatrician=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOlxpgErzEqbvlpdarcwshVlgkIdA',
'---Pharmacist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOlaEysiihigrVtifckoIbqgzrzmk',
'---Physiotherapist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOmnAcaknkVOwyxrpyeiilwmpyOsA',
'---Podiatrist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOnbzyzDvoykgvVcuxpmwijqokguA',
'---Psychiatrist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOlfjEumfqnloqnqyVkswAvftrjzA',
'---Psychologist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOllvgmlxxczlbpirEDttbzxbuudk',
'---Radiographer=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOltjqnileDEmiDxvwObdwVyjzolk',
'---Speech therapist=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOnoggEnEuoyEjmrjucVozuyVsreA',
'---Vet=okipalfjvtlqhnhbpkupjbbqOlVammImfDxfsrxvdnwxzfcunjjk,okipalfjvtlqhnhbpkupjbbqOkbtrlOgxEDAVudsghuifiqIegIk',
'Health & Pharma=okipalfjvtlqhnhbpkupjbbqOndbiiEepzdOAgoyyuukVfsorrrk',
'---Biotech=okipalfjvtlqhnhbpkupjbbqOndbiiEepzdOAgoyyuukVfsorrrk,okipalfjvtlqhnhbpkupjbbqOmgpItEqEjpAzbznIpyhsbjEVgqA',
'---Health Care and pharmaceutical=okipalfjvtlqhnhbpkupjbbqOndbiiEepzdOAgoyyuukVfsorrrk,okipalfjvtlqhnhbpkupjbbqOllciwoyqIExwyroimfeIlsouzek',
'---Medical Equipment=okipalfjvtlqhnhbpkupjbbqOndbiiEepzdOAgoyyuukVfsorrrk,okipalfjvtlqhnhbpkupjbbqOkvrtlwmthOvfzmjDAVykbyropVA',
'---Pharmaceutical=okipalfjvtlqhnhbpkupjbbqOndbiiEepzdOAgoyyuukVfsorrrk,okipalfjvtlqhnhbpkupjbbqOkEwyzhjtuOdvoVozlgxarhadluA',
'---Veterinary and animal healthcare=okipalfjvtlqhnhbpkupjbbqOndbiiEepzdOAgoyyuukVfsorrrk,okipalfjvtlqhnhbpkupjbbqOlbeOhnxAexEfsxAaaflkqpkvuhk',
'High Tech=okipalfjvtlqhnhbpkupjbbqOmyqulohpIEbpgtattlcwqwkotIA',
'---Computer Equipment & Peripherals=okipalfjvtlqhnhbpkupjbbqOmyqulohpIEbpgtattlcwqwkotIA,okipalfjvtlqhnhbpkupjbbqOnIsvzesyIqtrvOawwrIkbsoaDAk',
'---Electronics and microelectronics=okipalfjvtlqhnhbpkupjbbqOmyqulohpIEbpgtattlcwqwkotIA,okipalfjvtlqhnhbpkupjbbqOktjhaDthdjraDiaojIvjEvhzzak',
'---Multimedia - Internet=okipalfjvtlqhnhbpkupjbbqOmyqulohpIEbpgtattlcwqwkotIA,okipalfjvtlqhnhbpkupjbbqOnIghbAcxagvcqnVzIEvzxpAfhqk',
'---Software publishers=okipalfjvtlqhnhbpkupjbbqOmyqulohpIEbpgtattlcwqwkotIA,okipalfjvtlqhnhbpkupjbbqOkcchywwIvtisuhzaVhmbnkDDkVA',
'---Telecom - Internet Products and Services=okipalfjvtlqhnhbpkupjbbqOmyqulohpIEbpgtattlcwqwkotIA,okipalfjvtlqhnhbpkupjbbqOlsisutfgIefewuAwujcwtvtxjcA',
'---Telecom Operators=okipalfjvtlqhnhbpkupjbbqOmyqulohpIEbpgtattlcwqwkotIA,okipalfjvtlqhnhbpkupjbbqOkEamAsgkyleciAgqcrccOhEcyDk',
'Home / Domestic Services=okipalfjvtlqhnhbpkupjbbqOmOogwpcppOjpEhktEzgDkEhEslk',
'---Beautician=okipalfjvtlqhnhbpkupjbbqOmOogwpcppOjpEhktEzgDkEhEslk,okipalfjvtlqhnhbpkupjbbqOlyDjfcdggDylfewhnzDejqVAAgk',
'---Childcare=okipalfjvtlqhnhbpkupjbbqOmOogwpcppOjpEhktEzgDkEhEslk,okipalfjvtlqhnhbpkupjbbqOnmgOciudbvcrItwblAhosdaqeyA',
'---Cleaning',
' Ironing=okipalfjvtlqhnhbpkupjbbqOmOogwpcppOjpEhktEzgDkEhEslk,okipalfjvtlqhnhbpkupjbbqOlgOIsbVegesgzfujyrVncdocEtk',
'---Domestic Help=okipalfjvtlqhnhbpkupjbbqOmOogwpcppOjpEhktEzgDkEhEslk,okipalfjvtlqhnhbpkupjbbqOnzeDEqfyjsujhmphzfbxOAcbDek',
'---Elderly Care=okipalfjvtlqhnhbpkupjbbqOmOogwpcppOjpEhktEzgDkEhEslk,okipalfjvtlqhnhbpkupjbbqOmOIqjhvVfqAdlnAccOndpuAnczk',
'---Hairdressing=okipalfjvtlqhnhbpkupjbbqOmOogwpcppOjpEhktEzgDkEhEslk,okipalfjvtlqhnhbpkupjbbqOnhgdnEVviAgxEvVVphbiAqorfck',
'---Tutoring=okipalfjvtlqhnhbpkupjbbqOmOogwpcppOjpEhktEzgDkEhEslk,okipalfjvtlqhnhbpkupjbbqOkaxlVdEVlmerIjuekimxclDDtvA',
'Industry=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk',
'---Automobile=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOmvbzefovDvorncoimdArurfOcuk',
'---Aviation=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOljlzvgzEiiajDhixcrkkccEwqak',
'---Chemical industries=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOnpEEajlIODqvAVojVlqEkDozciA',
'---Cleaning products - Detergents=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlOwjfDnmVstsmkzvgrazwOAwtzk',
'---Cosmetic - Perfume - Hygene products=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOnslapmqysdIpnlrqkadrIjkueyk',
'---Electrical and electronic industries=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlDAIsAjczdnvkichyecjlnsgtjA',
'---Electrical Household Appliances=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOnEpbdcytovIxbcbpsptpsyokVbk',
'---Extraction - Mines=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOktbflvjIpmOtwimfcVorAjnneck',
'---Furniture - Wood products=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkaxolgquurIEelmawnmfsggqVbk',
'---Glass - Ceramics=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOndzckumaVhEpfacuswavxgbvdmA',
'---Household goods=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkrgctekgeVgzzfdoupOywzhafzA',
'---Industrial instruments=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkutzayegqOhipuOyAkckrmxjisA',
'---IT equipment=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkkIoEcOwoxkyggcnczokvqsicqA',
'---Leisure products - Toys - Games=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkllyfEmsagyEIpaoccdkpklEnbA',
'---Lighting and electrical fittings=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkleeEDbtdwmOnqevIVdmDmhxork',
'---Mechanical industries=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOmcVajcIDihOODxbcjDwyfyjblEk',
'---Medical equipment=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlODxOOsiAAyhdcqiohxiIuaowtA',
'---Metal=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlytxnfthvkhmkfumtDjkVidfqdk',
'---Music - Cinema - Video - Radio - Photo - TV=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlrqlontdagchotnIndavhkdVoOk',
'---Nuclear energy=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOmhsOefazsEksgrydirOayVmuytk',
'---Optical equipments=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlclhitotaDAbyfViqyItfshhrwA',
'---Packaging=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOnrhtpiphkxjtIdAeycVVcVvlAgk',
'---Painting - Ink=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkIVwhjjnbdDDnwVqglwvkrAjuIA',
'---Paper=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOmhhiwukDVxvhutjykzjpsitgbfA',
'---Petroleum or Petrochemical and related industries=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkaeVDjisumvdxDuanjgjxczAcak',
'---Petroleum production and distribution=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkwmnEerbjtOwggjjOhVbsOblobk',
'---Plastics=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlnAIVOeyjczcIthkAIIezitqkzk',
'---Pollution and  Waste Management=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlVyzxehvyjhhnuOjIjwsayyAcvk',
'---Publishing - Printing=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlbsjnEIDygglybfnbopdyxccDrk',
'---Railways=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlguovbubiaxvlDasbrErrhnxlkk',
'---Robotics=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlkrdityxayEVccyuknOOyEsebIk',
'---Table art - Decoration=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOmbxglaxiDahvycowddqrbDvtssA',
'---Textile - Clothing - Shoeware=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOmklkmwdOdDpOexnrcpyiAxlDlyA',
'---Textiles=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOkOfrjnailEvfEDvnuDcwsuOncrA',
'---Timepieces - Jewellery=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOmpgAzpcIhnhimfokDoOIsAivrdA',
'---Tobacco=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOlDyVjmzsVIrqcigovvAuOoreOvk',
'---Water - Electricity - Gas=okipalfjvtlqhnhbpkupjbbqOkgkjzyfowwjDvazgcOxmlAIysgk,okipalfjvtlqhnhbpkupjbbqOngjAIaviIklsdIkddmcxAlbEoVA',
'Leisure ',
' Tourism=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA',
'---Bar ',
' Café=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOnjEwVcsratjAzdawkVhAEebninA',
'---Bowling Alley=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOlsspokEgArtIfzaqrqyjiDwqymA',
'---Campsite=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOmxrxvrqcgcOpypIVsiklgcdEihk',
'---Cinema=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOkIVqpeoxwaIsbDoyetpdepdjapk',
'---Disco ',
' Club ',
' Nightclub=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOnVbocVAtOjgifbgvlivxibjvewk',
'---Equestrian Centre=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOneqhmokzlppbOlcEIujwrdOxmAk',
'---Hotel=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOkyhnwzyEbwdDVDyzhpqgsmvxDnk',
'---Leisure Centre=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOmVItriIDtklxAbjbzDtwAzqOglk',
'---Newsagent=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOnEcelgOjlrlVxnIehgndzkOehVk',
'---Restaurant=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOnAzvDzffjobulIOssOviugcecpk',
'---Swimming Pool=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOmbclADpukwqmawcnDViaiisObek',
'---Travel Agency=okipalfjvtlqhnhbpkupjbbqOmEssnvbgplnsEvbfbhEwbagreDA,okipalfjvtlqhnhbpkupjbbqOmiqridlAeqwrIiadkxutcygmVuA',
'Local Services=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA',
'---Bailiff=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOlqwfvuaOmiwfuyylgceiykptthk',
'---Beautician=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOnDucnIpEiDbvehVocaImwapEutk',
'---Car Rental=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOmkkzEggkvgxDovAzeApczbewtOk',
'---Chartered Accountant=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOnegowzrIgbfqucpfsvowDbIVnOk',
'---Hairdressing=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOkEmauDherytyjvrxdrshnOqyfgA',
'---Insurance ',
' Broker=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOmAEaAhDryhpglfizncDqminOigA',
'---Landscape Gardner=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOmVOebawyebVvpOuvgbpvDlngsxk',
'---Photographer=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOlrqDplDEdebsipsfvcmgczcsewk',
'---Real Estate=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOmAzsnAdupdvwfvmOywxrcsoooqA',
'---Retirement/Nursing Home=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOnskqOmlgkenjmsVeVVoigygArtA',
'---Security=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOldIvrjmeEznEivzVmhovbvEoyyk',
'---Solicitor / Lawyer=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOmnVpolurqAoOdfDmcsdprnzyOlk',
'---Solicitor / Notary=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOndhpjjsVdlbqxpwpEygtoqrluzk',
'---Spa ',
' Massage=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOkvhuErlkjwqbluiVaisOhhuoEvA',
'---Sports ',
' Gym=okipalfjvtlqhnhbpkupjbbqOlDxOuEwditlsaqjVcyuEntuqtvA,okipalfjvtlqhnhbpkupjbbqOlwhOjynowepdrnrdhItzVazhIbk',
'Public Services=okipalfjvtlqhnhbpkupjbbqOmEykycsAfrqdzozffqjOwwgfpDk',
'---Day-care centre=okipalfjvtlqhnhbpkupjbbqOmEykycsAfrqdzozffqjOwwgfpDk,okipalfjvtlqhnhbpkupjbbqOmqraOulEyxqhawxgEVIespzAgaA',
'---Library=okipalfjvtlqhnhbpkupjbbqOmEykycsAfrqdzozffqjOwwgfpDk,okipalfjvtlqhnhbpkupjbbqOlxApsifIDAhyiswzpgxDmqIjljk',
'---Occupational Health=okipalfjvtlqhnhbpkupjbbqOmEykycsAfrqdzozffqjOwwgfpDk,okipalfjvtlqhnhbpkupjbbqOkbDqeAmzDIVjnEdtpdywlItjuwA',
'---Police=okipalfjvtlqhnhbpkupjbbqOmEykycsAfrqdzozffqjOwwgfpDk,okipalfjvtlqhnhbpkupjbbqOndIIxqkebhDjOhaVclnvkbkVbuk',
'---Railway Stations=okipalfjvtlqhnhbpkupjbbqOmEykycsAfrqdzozffqjOwwgfpDk,okipalfjvtlqhnhbpkupjbbqOmDlmqhivjhjagunwfohOgmhfDjk',
'---Social Security=okipalfjvtlqhnhbpkupjbbqOmEykycsAfrqdzozffqjOwwgfpDk,okipalfjvtlqhnhbpkupjbbqOlxjOckddqlkoxemdjqIDjlpahgk',
'---Tourist Office=okipalfjvtlqhnhbpkupjbbqOmEykycsAfrqdzozffqjOwwgfpDk,okipalfjvtlqhnhbpkupjbbqOntyttuvOmnwbrefpEnDjDshgzdA',
'---Town Hall=okipalfjvtlqhnhbpkupjbbqOmEykycsAfrqdzozffqjOwwgfpDk,okipalfjvtlqhnhbpkupjbbqOnnvdkpaaOayqgObxsxerOwEusxA',
'Public Services - Administration=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk',
'---Administration and Public Services=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOkDDsaiogOrlOnkzqbahtIAbreqA',
'---Cultural Affairs=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOmIlrkbcheqOnfvqdEAszeliaDik',
'---Economic Development - Consular Agencies=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOkpbpugaeDsspwdbodmlwztidldk',
'---Economy - Finance=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOmbauxeadhzchEEscoivjiExiueA',
'---Environment - Agriculture=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOnhAvhfbAkbEsOmiVwIkcrwkzetk',
'---Foreign Affairs - International Affairs=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOkAssptlEggeVEEjdrlOOhbnOVnA',
'---Health Services - Social Issues=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOnkeIeVzbadkkitqvIachnvEaItA',
'---Industry - Energy=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOnswgdObcfoDsDaEwnyzzAAdwiVA',
'---IT - Telecommunications=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOkdcnEsjoaIlrszIVmosAyoDkqik',
'---Law - Justice=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOnwzuwyOiasxDVbgdrAwhmVqudhA',
'---Political organisations=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOnmczOftmoihkeEEOeIruhkzekjk',
'---Regional and local government=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOkojjwanfEDItVDaDriynhmbgpwk',
'---Renovation and restauration=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOmiDhoAjuzamcsIDVbupulfEpoaA',
'---Sport - Leisure - Tourism=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOkmpseblfkEzonEpadVktEudacnA',
'---Teaching  - Research=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOmuIyEextuspeaebycrdzirwAIOk',
'---Technical Services=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOnhEfzppVdhcqhuancvsvmtooxAk',
'---Urban planning - housing=okipalfjvtlqhnhbpkupjbbqOmaAIzruhmuIDiznulAOVnueirqk,okipalfjvtlqhnhbpkupjbbqOmjvnezljuhfpboOkoclucodAxEA',
'Real Estate, Civil Engineering=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA',
'---Architecture and Projects=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOmmAODmywogvqgcpfOkEtmOVozEA',
'---Building Trade: large projects=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOkiimvwnibdakzzmkOlapudgcOck',
'---Building Trade: residential homes=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOnpsaIAywbhluathcoluidoVprVk',
'---Construction materials=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOmAcjjfaAVcsgeiIqsgdzhVnjrgk',
'---Construction materials=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOmspAeshoiprmveAEscteOutvhck',
'---Craftsmanship=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOlaemgxxjjnvmmImiDweuAlOwEgk',
'---Management of Rental and Leased Properties=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOkEwmpdizjdDahueOcEIergtcVVA',
'---Real Estate=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOlOOniogcjOeqsepgskkcvcrxmhA',
'---Real Estate - Selling=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOmgrwghkuwxpuwvAEqVuyAuyiplA',
'---Real Estate Business=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOkpkwIogezgsAvuaxfjgDuzfdjtA',
'---Real Estate Companies - Selling=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOmfAVjrkkajrqsElbhldtmyuhzqA',
'---Real Estate Construction=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOmpjDIkiayyikEkIxwujwAmldgeA',
'---Renovations=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOklDustVzyibfkycAOAzArkaIlIA',
'---Road Construction=okipalfjvtlqhnhbpkupjbbqOmkhdoemrhDAlwakfckblvqiDduA,okipalfjvtlqhnhbpkupjbbqOlzhoygklachnrljgwirsOpwygIA',
'Retail=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk',
'---Antiques ',
' Second-hand=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOntnjpsqpfOzVfpuykbxOnlVjdbA',
'---Auto Parts ',
' Motorcycle Parts=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOknVivmOnoDazOmrspDrimkxjpwk',
'---Books ',
' Stationery=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOkdmlefkaibejgtOabvcscyVAyck',
'---Carpentry ',
' Cabinet Making ',
' Woodwork=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOnkblsqOpDeeEijlDgjcctegznvk',
'---Clothing=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOnhfhegjdDgruptEwoVgnrbmnlOA',
'---Cobbler=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOnolcnpeDeftopVpfwtzqturudnk',
'---DIY=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOljjIsxzsugDuObOeDnwxoayezDk',
'---Driving School=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOmqADvlDgxuutvzbouOnEzhjdmoA',
'---Florist=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOkyzbxgVrwmOhocezAoukjrzwOqA',
'---Funeral Parlour=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOlvxvDrhAlqAsogoImIqwgEcadEA',
'---Furniture=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOkldAvkconbhVszcggrOVgsfykik',
'---Garage=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOlpsnOvAsveEEcpidAmEunfDztaA',
'---Garden Centre / Nursery=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOmlptAbarznhshztVinDrfqgdzsA',
'---Hardware=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOkvjDrziutryodaenwnrbtqkjAfk',
'---Interior decoration=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOlsrykOlhpztugfAuxEnsdgEIwlk',
'---Ironmonger=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOkagkEtscVebEdfdzhEciOVxsAhA',
'---IT ',
' Computing=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOkItamumOqhaxDDyexqgEbajowgk',
'---Jeweller=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOlmgnDotcOEhbguDrxOmdjvzgthk',
'---Laundry=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOkowklcyIExVyccsqADIrVilzbvk',
'---Laundry and Ironing=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOkyacOpvxsljrcodOEugnDwniofk',
'---Mechanic ',
' MOT ',
' Inspection ',
' Service=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOlebjcwErmEfevnjAkmEmwaAOmrk',
'---Music shop=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOkmmEhIxspmdenpusppwaajOazyA',
'---Outlet Store=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOldlwhhjwpjecIOoVknAgckIxtsk',
'---Perfume=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOloeVcfhnvlmscIyOlogAApVhhdk',
'---Pet Parlour=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOnecrnkcqpabqoadfqcimizpfuuA',
'---Pet Shop=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOndylsphOcqkwoeeObjzxvizOOAk',
'---Printer=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOmpzdtlfpjkrqIfitcsywaquyiAA',
'---Shoe Shop=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOmDeAzEIVOnsOhflypbsrDeoIkuA',
'---Sports shop=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOnnDbymzjwOivhaicODydskVxsvA',
'---Telecoms=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOmsEDemVxOjfOtmgVbaoVOaEnqwk',
'---Wedding / Bridalwear shop=okipalfjvtlqhnhbpkupjbbqOkrfalkcxnwqkEavphataxdsyodk,okipalfjvtlqhnhbpkupjbbqOntArbrxtakzErpIsszoovIaOEnk',
'Transport=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA',
'---Car rental and leasing=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA,okipalfjvtlqhnhbpkupjbbqOkIlnupsgknDAsabpIyxbrbkOilA',
'---Logistics=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA,okipalfjvtlqhnhbpkupjbbqOmEuvzysVpxopDIxVsmODgaDIgtk',
'---Logistics and route planning=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA,okipalfjvtlqhnhbpkupjbbqOlDhpfxfupzhezeAIlVzzjAzlmhk',
'---Railroad transportation=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA,okipalfjvtlqhnhbpkupjbbqOkIaDxzvmllqwDxufjivVDaaIcdA',
'---Road haulage=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA,okipalfjvtlqhnhbpkupjbbqOlykcdirkAtgwnIhbaxzktjhlkyA',
'---Shipping=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA,okipalfjvtlqhnhbpkupjbbqOmpddeawaehkbDEvDjqokpvmdank',
'---Transport services=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA,okipalfjvtlqhnhbpkupjbbqOnupVweIsAvVVgccaajjuAgothwA',
'---Transportation by air=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA,okipalfjvtlqhnhbpkupjbbqOmgmyuzxgOvntEcDkuwVjgEfhfmk',
'---Warehousing=okipalfjvtlqhnhbpkupjbbqOmifjinOjvhpnbudzkepfrjcthOA,okipalfjvtlqhnhbpkupjbbqOldzVxwprvdozntibspiDzdqriyk',
'Transportation=okipalfjvtlqhnhbpkupjbbqOlVIlxfoljnyhtjpOlqIAOAaOrmA',
'---Ambulances=okipalfjvtlqhnhbpkupjbbqOlVIlxfoljnyhtjpOlqIAOAaOrmA,okipalfjvtlqhnhbpkupjbbqOkjselyenyfxwwttblpimfncrpdk',
'---Bus/Coach Transport=okipalfjvtlqhnhbpkupjbbqOlVIlxfoljnyhtjpOlqIAOAaOrmA,okipalfjvtlqhnhbpkupjbbqOlhudlVvEdcjjDfdzAmbgxqjhucA',
'---Removals=okipalfjvtlqhnhbpkupjbbqOlVIlxfoljnyhtjpOlqIAOAaOrmA,okipalfjvtlqhnhbpkupjbbqOnxnjaqcEynxVldrmOhldOmjItVA',
'---Taxi=okipalfjvtlqhnhbpkupjbbqOlVIlxfoljnyhtjpOlqIAOAaOrmA,okipalfjvtlqhnhbpkupjbbqOmEspclgvOrzizrAOsmgzydjmDsk)'),
        Default    => '',
        SortMetric => $SM_INDUSTRY,
    }; # }}}

    $derived_tokens{county} = {
        Label        => 'Preferred Locations',
        Type         => 'Text',
        Helper       => 'viadeo_location(%location_id%|%location_within_miles%)',
        SortMetric   => $SM_LOCATION,
        HelperMetric => 1
    };
    $posting_only_tokens{access_token} =
      { Helper => 'viadeo_retrieve_access_token()', };


}

sub search_fields {

    my $self = shift;

    my $indus_drop = $self->token_value('industry');

    my ( $indus, $subindus ) = split( /,/, $indus_drop );

    # Invalid to send empty keywords, but a keyword option
    my @keyword_option;
    if ($self->token_value('keywords')) {
        @keyword_option = ('keyword_option' => $self->token_value('keyword_option') || 'any');
    }

    return (
        'access_token'    => $self->token_value('access_token')    || '',
        'position_option' => $self->token_value('position_option') || '',
        @keyword_option,
        'company_option'  => $self->token_value('company_option')  || '',
        'max_experience'  => $self->token_value('max_experience')  || '',
        'min_experience'  => $self->token_value('min_experience')  || '',
        'distance'        => $self->token_value('distance')        || '',
        'user_detail'     => $self->token_value('user_detail')     || '',
        'page'            => $self->token_value('page')            || '',
        'limit'           => $self->token_value('limit')           || '30',# standard number of results stream wants
        'country'         => $self->token_value('country')         || '',
        'county'          => $self->token_value('county')          || '',
        'city'            => $self->token_value('city')            || '',
        'subindustry'     => $subindus,
        'industry'        => $indus,
#        'industry'        => $self->token_value('industry')        || '',
        'school'          => $self->token_value('school')          || '',
        'name'            => $self->token_value('name')            || '',
        'position'        => $self->token_value('position')        || '',
        'company'         => $self->token_value('company')         || '',
        'keyword'         => $self->token_value('keywords')        || '',
        'user_detail'     => 'full',
        'page'            => $self->current_page(),
        #'visit'           => 'talentbank',
       'connections'     => 'education|career|contact_cards',
        'language'        => $self->token_value('language') || '',
    );

}

############# ACCESS_TOKEN ##########

sub viadeo_retrieve_access_token {
    my $self = shift;

    my $results = $self->user_api->oauth_token( $self->user_object->provider_id, 'viadeo' );
    if ( my $token = $results->{access_token} ) {
        $self->log_debug("ACCESS_TOKEN is $token");
        return $token;
    }
    return $self->throw_login_error('Please reassociate AdCourier with your Viadeo account');
}

############# LOGIN #################
sub default_url {
    return 'https://api.viadeo.com/talentbank/users';
}

############# SEARCH ################
sub search_submit {
    my $self = shift;

    my %parameters = $self->search_fields();

    #removing blank parameters

    while ( my ( $key, $value ) = each %parameters ) {

        if ( $value eq '' or $value eq '0' ) {

            delete $parameters{$key};

        }
    }
    my $search = URI->new( $self->search_url() );
    $search->query_form(%parameters);
    $self->get($search);
}
############# SCRAPE ################

#sub scrape_candidate_xpath { return '//data/*[link]'; }

sub scrape_candidate_array {
    my $self = shift;
    my $json = $self->response_to_JSON();
    return @{ $json->{'data'}} ;
}

sub scrape_candidate_details {
    my $self = shift;
    my $json = shift;
 
    my %data = (
        candidate_id    => $json->{'id'},
        first_name      => $json->{'first_name'},
        name            => $json->{'name'},
        link            => $json->{'link'},
        updated_time    => $json->{'updated_time'},
        num_connections => $json->{'contact_count'},
        gender          => $json->{'gender'},
        nickname        => $json->{'nickname'},
        last_name       => $json->{'last_name'},
        picture_medium  => $json->{'picture_medium'},
        picture_large   => $json->{'picture_large'},
        has_picture     => $json->{'has_picture'},
        headline        => $json->{'headline'},
        snippet         => $json->{'introduction'},
        is_premium      => $json->{'is_premium'},
        twitter_account => $json->{'twitter_account'},
        premium_since   => $json->{'premium_since'},
        interests       => $json->{'interests'},
        location        => $json->{'location'}{'area'},
        city            => $json->{'location'}{city},
        zipcode         => $json->{'zipcode'},
        country         => $json->{'location'}{'country'},
        area            => $json->{'area'},
        language        => Locale::Language::code2language($json->{'language'}),
        distance        => $json->{'distance'},
    );

    foreach my $connection (@{$json->{connections}}) {
        if ($connection->{'key'} eq 'career') {
            foreach my $experience (@{$connection->{value}{data}}) {
                push (@{$data{experience}}, { 
                    company_name => $experience->{company_name},
                    position    => $experience->{position},
                    begin   => $experience->{begin},
                    end     => $experience->{end},
                    description => [split("\n", $experience->{description})],
                });
            }
        }
        elsif ($connection->{'key'} eq 'education') {
            foreach my $education (@{$connection->{value}{data}}) {
                push (@{$data{education}}, {
                    begin => $education->{begin},
                    end => $education->{end},
                    degree => $education->{degree},
                    school => $education->{school}->{name},
                    school_location => $education->{school}->{location}->{city},
                    field_of_expertise => $education->{school}->{field_of_expertise},
                });
            }
        }
    }

    return %data;
}

sub viadeo_location {
# NOTE:  When last tested, only french places and countries worked as searches (ie english cities like London were just ignored).  
# Not dealt with, as viadeo is a french site and better to support any expansion into other places automatically.  
    my $self = shift;
    my $location_id = shift or return;
    my $location_within = shift;
    my $location = $self->location_api->find( $location_id );

    if ( !$location ) {
        $self->log_debug('unable to find location [%s] in our location database', $location_id );
        return;
    }

   if ($location->is_country()){

        my @loc = $self->LOCATION_MAPPING('ISO_COUNTRIES', $location_id, $location_within);
            $self->log_debug('Adding country: [%s]', $loc[0] );
            
            $self->token_value('country' => lc($loc[0]));

        return ;
   }

    else {
        my @loc = $self->LOCATION_MAPPING('stream_viadeo', $location_id, $location_within);
            $self->log_debug('Adding locations: [%s]', $loc[0] );
            
            $self->token_value('county' => $loc[0]);

        return ;
    }
}


sub search_success {
    return qr/next/;
}

sub search_number_of_results_regex {
    return qr/count":(\d+),/;
}

sub scrape_fixup_candidate {
    my $self      = shift;
    my $candidate = shift;

    my $id = $candidate->{candidate_id};


    $candidate->attr ( 'snippet' => split("\n", $candidate->attr('snippet')));

    $candidate->has_profile(1);
	$candidate->has_cv(0);
}

# Need to do 2 requests to complete the candidate info. one for groups and one
# for keywords
sub download_profile {
    my ($self, $candidate) = @_;

    my $user_url = 'https://api.viadeo.com/' . $candidate->attr('candidate_id');
    my ($keywords, $groups) = ({}, {});
    eval {
        my $keywords_resp = $self->get($user_url . '/keywords?access_token=' . $self->viadeo_retrieve_access_token())
                                 ->decoded_content;
        my $groups_resp   = $self->get($user_url . '/groups?access_token=' . $self->viadeo_retrieve_access_token())
                                 ->decoded_content;
        $keywords = JSON::decode_json($keywords_resp);
        $groups   = JSON::decode_json($groups_resp);
    };
    if ($@) {
        $self->log_warn("Error retrieving keywords and/or groups: $@");
    }

    if ($keywords->{data}) {
        $candidate->attr(keywords => [ grep { $_ } @{$keywords->{data}} ]);
    }
    if ($groups->{data}) {
        $candidate->attr(groups =>
            [ grep { $_ } map {; $_->{name} } @{$groups->{data}} ]);
    }

    return 1;
}


sub candidate_messaging_uri_options {
    my $self = shift;
    my $candidate = shift;

    return (
        to => $candidate->attr('candidate_id'),
        access_token => '[viadeo_access_token]',
    );
}

sub candidate_messaging_uri_protocol {
    return 'viadeo';
}

sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    if ( !defined( $content ) ) {
        $content = $self->content();
        return unless $content;
    }
    if ( $content =~ m/Access Violation/i || $content =~ m/Access denied/i ) {
        # Our access token is invalid (most likely scenario)
        return $self->throw_login_error('Please reassociate AdCourier with your Viadeo account');
    }
    if($content =~ m/Interdit/i){
    return $self->throw_login_error('You need to have a Viadeo recruiter account');
    }
    if( $content =~ m/<error>/i){
        return $self->throw_error('An error occured');
    }

}

1;

# Industry list is folded using markers
# vim: set foldmethod=marker foldlevel=0:

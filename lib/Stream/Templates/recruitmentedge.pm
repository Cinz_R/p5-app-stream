package Stream::Templates::recruitmentedge;

use strict;
use warnings;

use base qw<Stream::Engine::API::REST>;

use JSON;
use URI::Template;
use List::MoreUtils qw/uniq/;
use Text::Autoformat qw/autoformat/;
use Stream2::O::File;

use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

# Some facets returned are not named as they are on the board, this is to
# make our labels consistent with their site.
my $EDGE_FACETS = {
    Skill => { Label => 'Skills', SortMetric => 10 },
    Industry => { Label => 'Industry', SortMetric => 20 },
    CaroteneCodeName => { Label => 'Job Title', SortMetric => 30 },
    Source => { Label => 'Source', SortMetric => 40 },
    City => { Label => 'City', SortMetric => 50 },
    State => { Label => 'State', SortMetric => 60 },
    Country => { Label => 'Country', SortMetric => 70 },
    WorkStatus => { Label => 'Work Status', SortMetric => 80 },
    AcademicDiscipline => { Label => 'Academic Discipline', SortMetric => 90 },
    DegreeLevel => { Label => 'Academic Degree', SortMetric => 100 },
    CurrentEmployer => { Label => 'Current Employer', SortMetric => 110 },
    PreviousEmployer => { Label => 'Previous Employer', SortMetric => 120 },
    HasRDBSource => { Label => 'RDB Source', SortMetric => 130 }, # don't know what this is
    HasResume => { Label => 'Resume Results', SortMetric => 140 },
    HasSocialSource => { Label => 'Social Results', SortMetric => 150 },
    ContactInfo => { Label => 'Contact Info', SortMetric => 160 },
};

my $OPTION_LABELS = {
    WorkStatus => {
        ctct => 'US Citizen',
        ctem => 'H1 Visa',
        ctgr => 'Green Card Holder',
        ctno => 'Need H1 Visa Sponsor',
        eatn => 'TN Permit Holder',
        eaea => 'Employment Authorization Document',
        ctay => 'Can work for any employer',
        ctns => 'Not Specified'
    }
};

$authtokens{refresh_token} = {
    Label   => 'Refresh Token',
    Type    => 'Text',
    Mandatory   => 1,
    SortMetric  => 1,
};

$standard_tokens{min_years_exp} = {
    Label       => 'Minimum Years Experience',
    Type        => 'Text',
    SortMetric  => 1,
};

$standard_tokens{max_years_exp} = {
    Label       => 'Maximum Years Experience',
    Type        => 'Text',
    SortMetric  => 2,
};

$standard_tokens{names} = {
    Label   => 'Names',
    Type    => 'Text',
    SortMetric => 3,
};

$standard_tokens{exclude_terms} = {
    Label   => 'Exclude Skills',
    Type    => 'Text',
    SortMetric => 4,
};

$standard_tokens{exclude_viewed} = {
    Label   => 'Exclude Viewed by Me',
    Type    => 'List',
    Options => '=|1 day=1|2 days=2|3 days=3|4 days=4|7 days=7|14 days=14|21 days=21|30 days=30|60 days=60',
    SortMetric  => 5,
};

sub recruitmentedge_developer_key { return 'WDHV2JY6B8PCBWQ0XPJX'; }
sub default_http_method { return 'GET'; }
sub results_per_page { return 20; }

sub base_url {
    my $self = shift;
    my $config = $self->feed_config($self->destination());
    my $base_url = $config->{base_url};
    return $base_url . '/consumer/edge';
}
sub search_url { return $_[0]->base_url() . '/search'; }
sub profile_url { return $_[0]->base_url() . '/profiles'; }

=head2 AGENT_CLASS

Handles authentication and setting the authorization header.
See this module for anything authentication related

=cut

sub config {
    my $self = shift;
    return $self->feed_config($self->destination);
}

sub _auth_header {
    my ($self) = @_;
    my $access_token = $self->_use_cb_oauth_flow
      ? $self->_cb_auth_access_token
      : $self->_offline_access_token;

    return (Authorization => 'Bearer ' . $access_token);
}

sub _offline_access_token {
    my ($self) = @_;
    my $config = $self->config->{oauth}->{offline};

    my $cache = $self->user_object->stream2->stream2_cache;
    my $cache_key = 'rec_edge_token:' . join(
        '-',
        $self->token_value('refresh_token'),
        $config->{client_id},
        $config->{client_secret},
    );

    my $cached_token = $cache->get( $cache_key );
    return $cached_token if $cached_token;

    my $response = $self->post(
        $config->{service_url},
        {
            grant_type => 'refresh_token',
            client_id => $config->{client_id},
            client_secret => $config->{client_secret},
            refresh_token => $self->token_value('refresh_token') // '',
        }
    );
    my $auth_data = JSON::decode_json($response->decoded_content);
    $cache->set(
        $cache_key,
        $auth_data->{access_token},
        {
            expires_in => $auth_data->{expires_in},
            expires_variance => 0.1,
        }
    );
    return $auth_data->{access_token};
}

sub _cb_auth_access_token {
    my ($self) = @_;
    my %auth_config = (
        %{ $self->config->{oauth}->{oneiam} },
        auth_code => $self->token_value('cb_user_code'),
    );
    my $cache = $self->user_object->stream2->users_cache;
    $self->{autotoken} //= Bean::OAuth::CareerBuilder->new(%auth_config)
        ->get_auto_token($cache);

    return $self->{autotoken}->token;
}
=head2 search_fields

Returns a hash of the query params to send to the board.
Takes the token values of the standard_tokens, facets
and default tokens and combines them into a text
query string that's sent in 'Filter'.

=cut

sub search_fields {
    my $self = shift;
    my %fields = (
        $self->_use_cb_oauth_flow?():( DeveloperKey => $self->recruitmentedge_developer_key() ),
        Page => $self->current_page() ? $self->current_page() : $self->current_page(1),
        ResultsPerPage => $self->results_per_page(),
    );

    $fields{IncludeFacets} = 'true';
    $fields{IncludeStats} = 'true';
    $fields{Query} = $self->token_value('keywords');
    $fields{Names} = $self->token_value('names');
    $fields{ExcludeTerms} = $self->token_value('exclude_terms');
    $fields{ExcludeViewedByMe} = $self->token_value('exclude_viewed');
    $fields{AuditLabelName} = $self->token_value('ofccp_context');

    if(my $postcode = $self->token_value('location_id_postcode')) {
        $fields{Locations} = $postcode;
        $fields{LocationRadius} = $self->token_value('location_within_miles');
    } elsif ( my $location_id = $self->token_value('location_id') ) {

        my ($location_text, $miles, $notice) = $self->recruitmentedge_location(
            $self->token_value('location_id'),
            $self->token_value('location_within_miles')
        );
        $self->results->add_notice($notice) if $notice;

        if ($location_text) {
            $fields{Locations} = $location_text;
            $fields{LocationRadius} = $miles;
        }
    }

    my @filters = ();
    foreach my $field ( keys %{$EDGE_FACETS} ) {
        my @values = $self->token_value($field);
        my @field_filter = map { "$field:\"$_\"" } @values;
        if ( scalar(@field_filter) > 0 ) {
            $field_filter[0] = "(" . $field_filter[0];
            $field_filter[-1] = $field_filter[-1] . ")";
            push @filters, join(' OR ', @field_filter);
        }
    }

    if ( $self->token_value('cv_updated_within') ne 'ALL' ) {
        my %cv_updated = ('TODAY' => '[Now-30Days to Now]', 'YESTERDAY' => '[Now-30Days to Now]', '3D' => '[Now-30Days to Now]', '1W' => '[Now-30Days to Now]', '2W' => '[Now-30Days to Now]', '1M' => '[Now-30Days to Now]', '2M' => '[Now-30Days to Now]', '3M' => '[Now-90Days to Now]', '6M' => '[Now-180Days to Now]', '1Y' => '[Now-1Year to Now]', '2Y' => '[Now-10Years to Now]', '3Y' => '[Now-10Years to Now]', 'ALL' => '');
        push @filters, "(ResumeModified:" . uc $cv_updated{$self->token_value('cv_updated_within')} . ")";
    }

    my $salary_from = $self->token_value('salary_from');
    my $salary_to = $self->token_value('salary_to');
    if ( $salary_from || $salary_to ) {
        my $frequency = $self->token_value('salary_per') eq 'hour' ? 'RecentWageHourly' : 'RecentWageYearly';
        my $currency = $self->token_value('salary_cur');
        my $salary_filter = "$frequency:";
        $salary_filter .= sprintf("[%d %s TO *]", $salary_from, $currency) if ( $salary_from && !$salary_to );
        $salary_filter .= sprintf("[* TO %d %s]", $salary_to, $currency) if ( $salary_to && !$salary_from );
        $salary_filter .= sprintf("[%d %s TO %d %s]", $salary_from, $currency, $salary_to, $currency) if ( $salary_from && $salary_to);
        push @filters, $salary_filter;
    }

    my $min_exp = $self->token_value('min_years_exp');
    my $max_exp = $self->token_value('max_years_exp');
    if ( $min_exp || $max_exp ) {
        my $experience_filter = "YearsOfExperience:";
        $experience_filter .= sprintf("[%d TO *]", $min_exp) if ( $min_exp && !$max_exp );
        $experience_filter .= sprintf("[* TO %d]", $max_exp) if ( $max_exp && !$min_exp );
        $experience_filter .= sprintf("[%d TO %d]", $min_exp, $max_exp) if ( $min_exp && $max_exp );
        push @filters, $experience_filter;
    }

    $fields{Filter} = join(' ', @filters);

    return %fields;
}

=head2 recruitmentedge_location

Returns a list (
    $location_text, # String to send to board
    $miles, # Mile radius to send to board
    $notice # Optional notice to display to the user.
)

=cut

sub recruitmentedge_location {
    my ($self, $location_id, $miles) = @_;

    unless ( $location_id ) {
        $self->log_info('No location ID, will not send location');
        return;
    }

    my $location = $self->location_api()->find({ id => $location_id });

    if ($location->in_usa) {
        if($location->type eq 'state') {
            $self->log_info('Mapping state');
            my $text = $location->name . ', US';
            # 0 miles forces state-wide search.
            # Otherwise radius works from the center
            return ($text, 0, 'Ignoring radius for state search');
        }
        $self->log_info('Mapping US non-state location');
        my $state = $location->get_mapping('ISO_STATES');
        my $text = join( ', ', $location->name, $state, 'US' );
        return ($text, $miles);
    }

    if($location->is_country) {
        $self->log_info('Mapping country');
        return ($location->name, 0);
    }

    $self->log_info('Mapping non-US location');
    my @crumbs = ($location->name);
    if($location->county) {
        my $county = $location->county->name;
        push(@crumbs, $county) if $county ne $location->name;
    }
    push(@crumbs, $location->country->name);

    return ( join(', ', @crumbs), $miles );
}

=head2 search_submit

Sends the search request to the board and returns the
response. This is where we get the facets from the
board and build them into the search results.

=cut

sub search_submit {
    my $self = shift;
    my $uri = URI->new($self->search_url());
    $uri->query_form($self->search_fields());

    my $response = $self->get($uri, $self->_auth_header);

    my $facets = $self->response_to_JSON()->{data}->{Facets};
    $self->_build_facets($facets);

    return $response;
}

=head2 search_success

Regex that searches the response and sees if we got a successful search.

=cut

sub search_success {
    return qr{"status":"Success"}i;
}

=head2 scrape_number_of_results

Takes the response from search_submit and finds the JSON key that notifies
us of how many candidates were returned in total for this search.

=cut

sub scrape_number_of_results {
    my $self = shift;
    my $json = $self->response_to_JSON();
    return $self->total_results($json->{data}->{TotalResults});
}

=head2 scrape_candidate_array

Looks for the JSON array given from the board which
has the candidates information for later looping

=cut

sub scrape_candidate_array {
    my $self = shift;
    my $json = $self->response_to_JSON();
    return @{$json->{data}->{Results}};
}

=head2 scrape_candidate_details

Sets the candidate attributes from the search results.
Note some attributes are only storing the first elements
of the array as they are only looking for the most recent
information.

=cut

sub scrape_candidate_details {
    my ($self, $json) = @_;

    my %data = (
        candidate_id        =>  $json->{EdgeID},
        rank                =>  $json->{EdgeScore},
        jobtitle            =>  $json->{JobTitle},
        city                =>  $json->{Location}->{City},
        state_iso           =>  $json->{Location}->{StateCode},
        county              =>  $json->{Location}->{County},
        name                =>  $json->{Name},
        photos              =>  $json->{Photos},
        keywords            =>  $json->{Keywords},
        years_exp           =>  $json->{YearsOfExperience},
        last_viewed         =>  $json->{Attributes}->{LastAccessed},
        recent_education    =>  $json->{Educations}->[0],
        recent_position     =>  $json->{Employments}->[0],
        social_sources      =>  $json->{Sources},
        views               =>  $json->{Views},
    );

    return %data;
}

=head2 scrape_candidate_headline

Sets the sub-heading of each candidate from the jobtitle attribute
set in scrape_candidate_details. This matches the boards layout,
hence not using last_updated as you would!

=cut

sub scrape_candidate_headline {
    return qw/jobtitle/;
}

=head2 scrape_fixup_candidate

Sets additional attributes for the candidate based off of existing
attributes (for a cleaner layout). Gravatar images never seem to
work, so they are not considered when setting the candidates
photo. Bulk message turned off based on request in SEAR-878

Note that this is called for each result from a search, not from
a profile point of view.

=cut

sub scrape_fixup_candidate {
    my ($self, $candidate) = @_;

    $candidate->has_profile(1);
    $candidate->has_cv(1);

    $candidate->attr(block_bulk_message => 1 );
    $candidate->attr(has_free_cv => 1 );

    if ( $candidate->attr('photos') && scalar(@{$candidate->attr('photos')}) > 0 ) {
        my @photos = map {
            $_ !~ m/gravatar/ ? $_ : undef;
        } @{$candidate->attr('photos')};
        @photos = grep { defined } @photos;
        $candidate->attr(photo => $photos[0]);
    }

    $self->_fixup_candidate_date_fields($candidate, 'last_viewed');

    my @location_parts = grep { defined } ( $candidate->attr('county'), $candidate->attr('city'), $candidate->attr('state_iso') );
    $candidate->attr( location => join(', ', @location_parts) );

    if ( my $education = $candidate->attr('recent_education') ) {
        my @recent_education = grep { defined } ( $education->{Title}, $education->{DegreeLevel} );
        $education->{Title} = join(', ', @recent_education) if ( scalar(@recent_education) > 0 );
        $candidate->attr( recent_education =>  $education );
    }

    if ( $candidate->attr('social_sources') && scalar(@{$candidate->attr('social_sources')}) > 0 ) {
        my @social_sources = map {
            $_ =~ s/ID|Name//;
            $_ =~ s/ResumeDB/Careerbuilder/;
            $_ =~ s/Edge.+/Edge/;
            $_;
        } @{$candidate->attr('social_sources')};
        @social_sources = grep { defined } @social_sources;
        $candidate->attr( social_sources => join(', ', uniq(@social_sources)) );
    }

    my $recent_position = $candidate->attr('recent_position');

    my @headline_parts = grep {$_} (
        $recent_position->{Position} || $candidate->attr('jobtitle'),
        $recent_position->{Employer},
    );

    $candidate->attr( headline => join(' - ', @headline_parts) );
    return $candidate;
}

=head2 download_profile_submit

Submits a request to the boards single candidate end-point.
Returns the HTTP::Response given by that

=cut

sub download_profile_submit {
    my ($self, $candidate) = @_;

    my $uri = URI->new($self->profile_url());
    $uri->query_form(
        EdgeID => $candidate->attr('candidate_id'),
        $self->_use_cb_oauth_flow?():( DeveloperKey => $self->recruitmentedge_developer_key() ),
        LoadCrowdsourcing => 'true',
    );
    return $self->get($uri, $self->_auth_header);
}

=head2 download_cv_url

Returns a URL with the auth parameters set to successfully download
the candidates PDF CV.

=cut

sub download_cv_url {
    my ($self, $candidate) = @_;
    my $uri = URI::Template->new($self->profile_url() . '/{edgeID}/pdf?DeveloperKey={dev_key}');
    $uri = $uri->process(
        edgeID => $candidate->candidate_id,
        $self->_use_cb_oauth_flow?():( dev_key => $self->recruitmentedge_developer_key ),
    );
    return $uri->as_string;
}

=head2 download_cv_submit

Takes the download_cv_url output and makes a request
to that URL, returning the HTTP::Response

=cut

sub download_cv_submit {
    my ($self, $candidate) = @_;

    return $self->get($self->download_cv_url($candidate), $self->_auth_header);
}

=head2 scrape_download_cv

Creates a fake HTTP::Response with the real response data so
we can download the CV as a file. CVs are always in PDF.

=cut

sub scrape_download_cv {
    my ($self, $candidate) = @_;
    my $response = $self->response();
    my $cv_download_response = $self->to_response_attachment({
        content_type => 'application/pdf',
        decoded_content => $response->decoded_content,
        filename => $candidate->attr('name') . '.pdf',
    });

    if ( $candidate->email ) {
        $cv_download_response->header('X-Candidate-Email' => $candidate->email);
    }

    return $cv_download_response;
}

=head2 scrape_generic_download_profile

Sets candidate attributes from the profile download response

=cut

sub scrape_generic_download_profile {
    my ($self, $candidate) = @_;

    my $candidate_profile = $self->response_to_JSON()->{data};
    $candidate->attrs(
        about => $candidate_profile->{About},
        phones => $candidate_profile->{Phones},
        websites => $candidate_profile->{Websites},
        current_locations => $candidate_profile->{Locations}->{CurrentLocations},
        desired_locations => $candidate_profile->{Locations}->{DesiredLocations},
        education => $candidate_profile->{Educations},
        employment => $candidate_profile->{Employments},
        spoken_languages => $candidate_profile->{SpokenLanguages},
        industries => $candidate_profile->{Industries},
        awards => $candidate_profile->{Awards},
        certificates => $candidate_profile->{Certifications},
        publications => $candidate_profile->{Publications},
        memberships => $candidate_profile->{Memberships},
        social_networks => $candidate_profile->{Profiles},
        scored_keywords => $candidate_profile->{Keywords},
    );

    # the linkedin properties may or may not be present, so we test for them,
    # and only set linkedin_properties to a hash-ref with valid values.
    my @linkedin_properties = (
        { name => 'LinkedIn_Connections',       key_name => 'connections'  },
        { name => 'LinkedIn_Current_Position',  key_name => 'current_position' },
        { name => 'LinkedIn_Groups',            key_name => 'groups' },
        { name => 'LinkedIn_Interests',         key_name => 'interests' },
        { name => 'LinkedIn_ProjectsCount',     key_name => 'projects' },
        { name => 'LinkedIn_Recommendations',   key_name => 'recommendations' },
    );

    my $candidate_linkedin_properties = undef;
    for my $linkedin_property (@linkedin_properties) {
        my $linkedin_property_value = $candidate_profile->{Custom_Properties}->{$linkedin_property->{name}}->{Value};
        if ( $linkedin_property_value ) {
                $candidate_linkedin_properties->{$linkedin_property->{key_name}} = $linkedin_property_value
        }
    }

    $candidate->attr( 'linkedin_properties' => $candidate_linkedin_properties);

    my $source_dates = $candidate_profile->{Timestamps}->{Timestamps}->{ResumeLastModified}->{SourceDates};
    my ($last_modified_key) = keys($source_dates);
    $candidate->attr( last_modified => $source_dates->{$last_modified_key} );

    my @emails = ();
    if ( my $email_network = $candidate_profile->{IDs}->{Email} ) {
        foreach my $identifier ( @{$email_network->{Identifiers}} ) {
            push @emails, $identifier;
        }
    }

    if ( scalar @emails > 0 ) {
        $candidate->attr('email' => $self->_valid_email(\@emails) );
        $candidate->attr('emails' => \@emails );
    }

    $self->scrape_fixup_candidate_profile( $candidate );

    $candidate->profile_downloaded( time );

    return $candidate;
}


=head2 download_cb_cv_url

Returns a URL with the auth parameters set to successfully download
the candidates PDF CV.

=cut

sub download_cb_cv_url {
    my ( $self, $candidate, $cb_cv_id ) = @_;
    my $uri = URI::Template->new( $self->profile_url()
          . '/{EdgeID}/resumes/RDB/{cb_cv_id}/pdf?DeveloperKey={dev_key}' );
    $uri = $uri->process(
        edgeID   => $candidate->candidate_id,
        dev_key  => $self->recruitmentedge_developer_key,
        cb_cv_id => $cb_cv_id,
    );
    return $uri->as_string;
}

=head2 download_cb_cv_submit

Takes the download_cb_cv_url output and makes a request
to that URL, returning the HTTP::Response

=cut

sub download_cb_cv_submit {
    my ( $self, $candidate, $cb_cv_id ) = @_;

    return $self->get( $self->download_cv_url( $candidate, $cb_cv_id ), $self->_auth_header );
}

=head2 before_downloading_cv

Hook to fixup candidate before downloading their cv. Used here to fetch
the profile before downloading cv to ensure we get the candidates
email address for shortlisting.

=cut

sub before_downloading_cv {
    my ($self, $candidate) = @_;
    $self->download_profile($candidate);
}

=head2 scrape_fixup_candidate_profile

Hook which takes the data of the candidate up to now and
allows for modifications/cleaning.

=cut

sub scrape_fixup_candidate_profile {
    my ($self, $candidate) = @_;

    $self->_fixup_candidate_date_fields($candidate, 'last_modified');

    if ( $candidate->attr('spoken_languages') ) {
        my @languages = ();
        foreach my $language ( @{$candidate->attr('spoken_languages')} ) {
            push(@languages, $language->{FreeSpokenLanguage}) if $language->{FreeSpokenLanguage};
        }
        $candidate->attr( spoken_languages => \@languages );
    }

    $self->_set_candidate_social_networks($candidate);
    $self->_set_candidate_top_skills($candidate);

    return $candidate;
}

# Dates come in the format: 2015-09-30T12:09:10+00:00,
# only taking the Y-M-D for now...
#
# Some date fields come in date parts (day => 20, month => 01, etc),
# this goes through the known date fields and creates a date string
# from them.

sub _fixup_candidate_date_fields {
    my ($self, $candidate, $field) = @_;

    if ( my $date = $candidate->attr($field) ) {
        $date =~ m/(.+)T/;
        $candidate->attr($field => $1);
    }

    if ( $candidate->attr('employment') ) {
        foreach my $employment ( @{$candidate->attr('employment')} ) {
            my $from = $employment->{DateFrom};
            my $to = $employment->{DateTo};
            $employment->{DateFrom} = join('-', grep { defined } ($from->{UtcYear}, $from->{UtcMonth}, $from->{UtcDay}));
            $employment->{DateTo} = join('-', grep { defined } ($to->{UtcYear}, $to->{UtcMonth}, $to->{UtcDay}));
        }
    }

    if ( $candidate->attr('education') ) {
        foreach my $education ( @{$candidate->attr('education')} ) {
            my $graduation_date = $education->{DateGraduated};
            $education->{DateGraduated} = join('-', grep { defined } ($graduation_date->{UtcYear}, $graduation_date->{UtcMonth}, $graduation_date->{UtcDay}));
        }
    }

    if ( $candidate->attr('publications') ) {
        foreach my $publication ( @{$candidate->attr('publications')} ) {
            my $publication_date = $publication->{PublishedDate};
            $publication->{PublishedDate} = join('-', grep { defined } ($publication_date->{UtcYear}, $publication_date->{UtcMonth}, $publication_date->{UtcDay}));
        }
    }
}

# Social networks are places where you can find the candidates profile,
#
# e.g. LinkedIn/Edge/Facebook/etc.
# 'ResumeDBDID' and Edge are the Careerbuilder profile pages, so always taking them.
#
# Some other channel names come in the following form:
# - LinkedInName
# - LinkedInID
#
# Which represent URLs based on either name or ID (eg linkedin.com/aliz or linkedin.com/123)
# We are only keeping the values with name to avoid duplicates
#
# ResumeDBWordDocDID is the key in which the Careerbuilder CV can be found, if it's available,
# we will show a separate download button in the profile to allow downloading of either the
# Recruitment Edge CV or Careerbuilders.
#
# Note that downloading the CB CV is not the CV downloaded within Search, it's just
# a profile attribute.

sub _set_candidate_social_networks {
    my ($self, $candidate) = @_;

    my @networks = ();
    foreach my $available_network ( %{$candidate->attr('social_networks')} ) {
        next if $available_network =~ m/onetapi/i;

        if ( $available_network eq 'ResumeDBWordDocDID' ) {
            if ( my $cb_cv = $candidate->attr('social_networks')->{$available_network}->[0]->{URL} ) {
                $candidate->attr( cb_cv => $cb_cv );
                # This will tell the frontend it can go and look for
                # other attachments for this candidate.
                $candidate->has_other_attachments( 1 );
                $candidate->attr( has_other_attachments_name => 'Careerbuilder Profile' );
            }
            next;
        }

        my $name = $available_network;
        $name = 'Gravatar' if $name =~ m/gravatar/i;
        $name = 'LinkedIn' if $name =~ m/linkedin/i;
        $name = 'Facebook' if $name =~ m/facebook/i;
        $name = 'Github'   if $name =~ m/github/i;
        $name = 'Angel'    if $name =~ m/angel/i;
        $name = 'StackExchange'   if $name =~ m/stackexchange/i;
        $name = 'Careerbuilder'   if $name eq 'ResumeDBDID';

        my $network = {
            Name => $name,
            URL => $candidate->attr('social_networks')->{$available_network}->[0]->{URL}
        };

        my %network_icons = (
            linkedin            => 'icon-linkedin-rect',
            facebook            => 'icon-facebook-rect',
            googleplus          => 'icon-gplus-squared',
            pinterest           => 'icon-pinterest-squared',
            twitter             => 'icon-twitter-squared',
            stackexchange       => 'icon-stackexchange',
            github              => 'icon-github-squared',
            angel               => 'icon-angellist',
            xing                => 'icon-xing-squared',
            viadeo              => 'icon-viadeo',
            quora               => 'icon-quora',
            youtube             => 'icon-youtube-squared',
            tumblr              => 'icon-tumblr-squared',
            meetup              => 'icon-meetup',
            careerbuilder       => 'icon-careerbuilder-logo',
            edge                => 'icon-careerbuilder-logo',
        );
        $network->{IconClass} = $network_icons{lc($network->{Name})} // 'icon-photo';

        push @networks, $network unless ( grep { $_->{Name} eq $name } @networks );
    }


    $candidate->attr('social_networks' => \@networks);
}


# Sets the candidates top 3 skills based off of the candidates Keyword attribute
# stored in scored_keywords. This is different from the Keyword attribute of
# the candidate from the search results because these come ranked
#
# The keywords come in as a hashref now :/, so cannot depend on order. Instead we
# use the 'Score' key in the hash and sort by that to create our array of top
# skills.

sub _set_candidate_top_skills {
    my ($self, $candidate) = @_;

    unless ( $candidate->attr('scored_keywords') ) {
        $candidate->attr('top_skills' => []);
        return;
    }

    my %scored_keywords = %{$candidate->attr('scored_keywords')};

    my @skills = sort { ($scored_keywords{$a}->{Score} || 1)  <=> ($scored_keywords{$b}->{Score} || 1) } (keys %scored_keywords);

    my @top_three = ();

    foreach my $skill ( splice(@skills, 0, 3) ) {
        $skill = $scored_keywords{$skill};

        my ($reason) = grep { defined } map {
            $skill->{Reasons}->{$_} ? $skill->{Reasons}->{$_}->[0]->{Reason} : undef
        } @{$skill->{Sources}};

        push @top_three, {
            name => $skill->{Keyword},
            reason => $reason,
        };
    }

    $candidate->attr('top_skills' => \@top_three);
}

# Emails have a validation attribute which, well, checks to see if
# the email on the candidate is valid!
#
# For Search, we will always use the first valid email address
# in the array. The rest are just for display

sub _valid_email {
    my ($self, $emails) = @_;

    return unless $emails;

    my $valid_email;
    foreach my $email ( @$emails ) {
        if ( $email->{Metadata}->{Validity} eq 'Valid' ) {
            $valid_email = $email->{ID};
            last;
        }
    }

    return $valid_email;
}

=head2 _build_facets

Builds facets!

=cut

sub _build_facets {
    my ($self, $board_facets) = @_;

    my @facets = ();

    unless ( $board_facets ) {
        return $self->results->facets(\@facets);
    }

    foreach my $facet ( @{$board_facets} ) {

        next if ( !$facet->{Name} || $facet->{Name} eq 'ResumeModified' );

        my $facet_item = {
            Name => $facet->{Name},
            ID => sprintf('%s_%s', $self->destination, lc $facet->{Name}),
            Type => 'multilist',
            Label => $EDGE_FACETS->{$facet->{Name}}->{Label} || $facet->{Name},
            SortMetric => $EDGE_FACETS->{$facet->{Name}}->{SortMetric} || 200,
        };

        my @values = $self->token_values($facet->{Name});
        my %selected_values = map { $_ => 1 } @values;
        $facet_item->{SelectedValues} = \%selected_values;

        map {
            push @{$facet_item->{Options}}, [
                $OPTION_LABELS->{$facet->{Name}} ? $OPTION_LABELS->{$facet->{Name}}->{$_->{Name}} : autoformat($_->{Name}, { case => 'highlight' }),
                $_->{Name},
                $_->{Count}
            ];
        } @{$facet->{FacetItem}};

        push @facets, $facet_item;
    }

    $self->results->facets(\@facets);
}

=head2 feed_identify_eror

Catches errors!

=cut

sub feed_identify_error {
    my ($self, $message, $content) = @_;

    if ( $content =~ m/\QValue cannot be null.\r\nParameter name: Query\E/ ) {
        $self->throw_known_internal("Keywords are mandatory");
    }

    my $json = JSON::from_json($content);

    if ( $json->{Status} && $json->{Status} eq 'ServiceError' ) {
        $self->throw_unavailable('There is a problem with the board. Please try again later');
    }
    elsif ( my $err_msg = $json->{error_description} ) {
        if ( $err_msg eq 'Refresh token not found.' ) {
            $self->throw_login_error('Refresh token is not authorized');
        }
        elsif ( $err_msg =~ m/(Refresh Token not provided)/ ) {
            $self->throw_login_error($1);
        }
    }
    elsif ( my $error_array = $json->{errors} ) {
        if ( $error_array->[0]->{message} =~ m/\QValue cannot be null.\r\nParameter name: AuditLabelName\E/ ) {
            $self->throw_known_internal('OFCCP Context field is required');
        }
        elsif ( $error_array->[0]->{message} =~ m/^AuditLabelName \(DataStore Label\) is required$/ ) {
            $self->throw_known_internal('OFCCP Context field is required');
        }
        elsif ( $error_array->[0]->{message} =~ m/(No Account with Recruitment Edge Access is available)/ ) {
            $self->throw_inactive_account($1);
        }
    }
}

# Use the CV as an attachment
sub rendered_attachments {
    my ($self, $candidate) = @_;

    # PLEASE FIX ME
    # When a search gets a candidate, the results are put into redis.
    # however when we download the profile we do not enrich the the redis result.
    # this means we have to re-redownload the profile over and over again.
    # This will need to be fixed.

    my $response = $self->download_cv($candidate);
    my $filename = 'CB-Resume-' . $candidate->id() . '.pdf';

    my $file = Stream2::O::File->new({
        name           => $filename,
        mime_type      => 'application/pdf',
        binary_content => $response->content,
    });

    return [
        {
            key           => 'cb_cv',
            filename      => $filename,
            rendered_html => $file->html_render,
        }
    ];
}

# Return the CV download as the attachment
sub download_attachment {
    my ($self, $candidate, $attachment_key) = @_;

    return unless $attachment_key eq 'cb_cv';

    return $self->download_cv($candidate);
}

sub download_attachments {
    my ($self, $candidate) = @_;
    return [ $self->download_attachment($candidate, 'cb_cv') ];
}

sub _use_cb_oauth_flow {
    my $self = shift;
    return $self->token_value('cb_user_code') ? 1 : 0;
}

1;

package Stream::Templates::cvlibrary;

use utf8;

=head1 NAME

Stream::Templates::cvlibrary - CV Library Cv Search

=head1 AUTHOR

ACJ - 07/09/2009

=head1 CONTACT

Developer is Brian Waken [mailto:b.wakem@cv-library.co.uk] (01252 810 995)

=head1 TESTING INFORMATION

username: bbstream
password: wisoxi685

=head1 DOCUMENTATION

See trac

=head1 OVERVIEW

Normal webservice but has unique sessions to prevent account sharing.

28/02/2011 - Patrick
CVLibrary have given us permission to store the session id per ADC user instead of ADC session... changing timeout to 660 minutes (CV Library expire all sessions at the end of the day)

They return a session id with the first request we make. All future
requests to CV Library must include that session id.

The session id is tied to the user's email address and remote IP address.
If there is already a session running then, CV Library will return an 
error ("User andy@broadbean.net already in session").

2017-06-01 SEAR-2582
The stored session is also now entangled with a BrowserTag, which prevents
account sharing via adc user sharing, this BrowserTag  is vivified upon search login.
Previously we were storing the session id under the cache key 'session_id', now it is
$browser_tag.'session_id'.

If you login directly to cvlibrary.co.uk, Stream will not be able to search
using those credentials until that session has ended.

After every Stream search, we include a 1x1 image hidden on the results page. This
tells CV library what the session Id was and means that you can login directly to
cv-library.co.uk immediately after searching CV library in Stream.

The image is hidden in panels/search_failures.tpl.

Template for this image: http://www.cv-library.co.uk/cgi-bin/umid.gif?sessionID=[% cvlibrary_session %]

Hitting this page will just mean "its OK to connect with the same credentials as this session from some other place.",
it DOES NOT MEAN "invalidate this session so we can login again using the same credentials from the same machine".

Sessions end 40 minutes after the last action.

Stream automatically expires the session id after 40 minutes. The session is stored
against the adcourier login session (CV Library insisted we did it this way)

Stream sets a session cookie when you first access Stream. When you logout or close your browser,
this cookie is cleared.

The session id is sent with all gearman tasks. The CV Library Session ID is stored against the
Stream login session using set_account_value and account_value.

This means:

=over 4

=item a)

If you login, search cv library, close your browser you will not be able to search CV Library
or download CVs etc.

=item b)

The Stream API cannot search CV Library because there is no session

=item c)

You cannot run CV watchdogs on CV Library because there is no session

=item d)

The session id is stored against the person running the search. If admin's run a search, the consultant
will not be able to search CV Library for 40 minutes

=item e)

If we run a search in somebodies account, they will not be able to search CV Library for 40 minutes

=back

If you get an "ACCOUNT ALREADY IN USE" error, wait 40 minutes and try again.

This is also the reason I have not added CV Library to the demo account.

=head1 Generic Emails and Forwarding CVs

CV Library will only let us forward CVs to email addresses that are registered with them. We are not allowed
to forward CVs from them to a generic email address at all. CV Library's viewpoint is each email address that
you forward CVs to requires a user license. This means we cannot forward CVs from CV Library to IProfile/Daxtra/
Bond etc etc etc.

=head1 Search Advice

Pinched from L<http://www.cv-library.co.uk/login/help_keywords_jobtitle.html>

Enter keywords or phrases to match the body of the CV.

Example: Sales Manager
Matches CVs with both sales and manager in.

Example: "Sales Manager"
Matches CVs with the phrase Sales Manager in.

Boolean Matches

Example Sales AND (Manager OR Managed)
Matches CVs with Sales in where the CV also contains either Manager or Managed.

Commas have no meaning C++, Java is the same as C++ JAVA or C++ AND Java

Example ((HTML AND (PHP OR ASP)) AND NOT frontpage)
Deep nested ANDs ORs and NOTs can be used.

Other tips

To match a word that commonlly appears in other words (i.e. sales appears in telesales) quote the word to match just sales - "Sales"

All searches are case insensitive - SALES is the same as sales

Leave the keyword box blank to match all CVs that match your other selected criteria

More help on boolean searching (it has venn diagrams :)

L<http://www.cv-library.co.uk/searchtips>

(watchdog tips: L<http://www.cv-library.co.uk/watchdogtips>)

=head1 MAJOR CHANGES

=over 4

=item * 08/03/11 - ACJ - Adding watchdog support

=item * 16/11/10 - ACJ - Bringing back email authtoken but it is mandatory now

=item * 24/03/10 - ACJ - Removed registered email authtoken at CV Library's request

=item * 22/03/10 - ACJ - New optional authtoken for the CV Library registered email address

=item * 18/03/10 - ACJ - CV Library return candidate's contact details when we download the CV

=item * 22/09/09 - ACJ - CV Library Session ID is stored against the Stream Session not the AdCourier account

=back

=head1 BUG FIXES

=over 4

=item * 21/09/09 - ACJ - Requires DateTime::Format::Strptime version 1.0800 or newer

=back

=cut

use strict;

use base qw(Stream::Engine::API::XML);

use Stream::Constants::SortMetrics ':all';

use POSIX;
use Stream::SharedUtils;

## use Bean::XMLDB::UserLib::GetInfoForUser;
use DateTime::Format::Strptime;

################ TOKENS #########################
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens);

BEGIN {
    $standard_tokens{'searchjobtitleonly'} = {
        Label => 'Search Jobtitle Only',
        Type  => 'List',
        Options  => '(Search jobtitle and CV=0|Only search the jobtitle=1)',
        Default  => 0,
    };

    $standard_tokens{'relocate'} = {
        Label => 'Relocation',
        Type  => 'List',
        Options => '(Do not include candidates willing to relocate=0|Include candidate willing to relocate=1)',
        SortMetric => $SM_LANGUAGE,
    };

    $derived_tokens{time} = {
        Label => 'CV Last Updated Within (days)',
        Type  => 'Text',
        Helper => 'cvlibrary_cv_updated(%cv_updated_within%)',
        SortMetric => $SM_DATE,
    };

    ### Salary fun ###

    $derived_tokens{'salary_cur'} = {
        Type    => 'Currency',
        Options => 'GBP',
        Helper  => 'SALARY_CURRENCY(salary_cur|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric => $SM_SALARY,
    };

    $derived_tokens{'salary'} = {
        Label => 'Salary',
        Type  => 'SalaryMultiValue',
        Options => '(1-10000=1|10001-15000=2|15001-20000=3|20001-25000=4|25001-30000=5|30001-40000=6|40001-60000=7|60001-80000=8|80001-100000=9|100001-120000=10|120001+=11)',
        Helper  => 'SALARY_BANDS_BETWEEN(salary|%salary_from%|%salary_to%)',
        HelperMetric => 2,
        SortMetric => $SM_SALARY,
    };


    $standard_tokens{jobtype} = {
        Label      => 'Job Type',
        Type       => 'List',
        Options    => '(Any=|Permanent=perm|Temporary=temp|Contract=cont|Part Time=part)',
        Default    => '',
        Deprecated => 'JobType',
        SortMetric => $SM_JOBTYPE,
    };

    $derived_tokens{'salary_per'} = {
        Type  => 'SalaryPer',
        Options => 'annum',
        Helper  => 'SALARY_PER(salary_per|salary_from|salary_to)',
        HelperMetric => 1,
        SortMetric => $SM_SALARY,
    };

    $standard_tokens{'industry'} = {
        Label => 'Industry',
        Type => 'MultiList',
        Options => '(Accountancy|Administration|Advertising|Aerospace/Aviation/Defence|Agriculture|Architecture|Art/Design/Graphics|Automotive|Banking|Biotechnology|Catering & Food Services|Charity/Voluntary/Non Profit|Construction|Consultancy|Counseling|Customer service|Dental|Driving|Education|Electrical|Electronic|Emergency|Engineering|Entertainment|Fashion|Financial|Graduate|Health & Safety|Health medical|Home worker|Hotel/Hospitality|Human-resources & Employment|Import export|Industrial|Insurance|IT/Internet/Technical|Journalism|Legal|Leisure|Management & Executive|Manufacturing|Marketing & PR|Mechanical|Media/Creative/Digital|Nursing|Oil & Gas/Energy|Other|Packaging|Pharmaceutical|Photography|Printing|Property|Public sector|Publishing|Purchasing|Recruitment|Retail|Sales|Scientific|Secretarial & PA|Security|Social services|Teaching|Telecomms|Textiles|Trades - Skilled & Unskilled|Transport & Logistics|Travel & Tourism|Utilities & Installation|Warehouse|Waste management)',
        SortMetric => $SM_INDUSTRY,
    };

    $standard_tokens{'languages'} = {
        Label => 'Languages',
        Type  => 'MultiList',
        Options => '(Afrikaans=1|Albanian=2|Arabic=3|Basque=4|Bulgarian=5|Byelorussian=6|Catalan=7|Croatian=8|Czech=9|Danish=10|Dutch=11|Esperanto=13|Estonian=14|Faroese=15|Finnish=16|French=17|Galician=18|German=19|Greek=20|Hebrew=21|Hungarian=22|Icelandic=23|Irish=24|Italian=25|Japanese=26|Korean=27|Lapp=28|Latvian=29|Lithuanian=30|Mandarin=31|Macedonian=32|Maltese=33|Norwegian=34|Polish=35|Portuguese=36|Romanian=37|Russian=38|Serbian=39|Slovak=41|Slovenian=42|Spanish=43|Swedish=44|Turkish=45|Ukrainian=46|Welsh=47)',
        SortMetric => $SM_LANGUAGE,
    };

    $standard_tokens{'expand_search'} = {
        Label => 'Expand my phrases',
        Type  => 'List',
        Options => '(Yes=1|No=0)',
        SortMetric => $SM_LANGUAGE,
    };


    $standard_tokens{'minmatch'} = {
        Label => 'Only return CVs that match this much (%)',
        Type  => 'List',
        Options => '(All=0|25% match=25|50% match=50|75% match)',
        Default => 0,
        SortMetric => $SM_SEARCH,
    };

    $derived_tokens{'geo'} = {
        Label        => 'Location (town or postcode)',
        Type         => 'Text',
        Helper       => 'cvlibrary_geolocation_search(%location_id%|%location_within%)',
        HelperMetric => 1,
        SortMetric   => $SM_LOCATION,
    };

    $derived_tokens{'distance'} = {
        Label       => 'Distance from town or postcode (miles)',
        Type        => 'Text',
        Helper      => 'cvlibrary_location_within(%geo%|%location_within%)',
        HelperMetric => 2,
        SortMetric   => $SM_LOCATION,
    };

    # Only populated if geo is empty
    $derived_tokens{areas} = {
        Label   => 'Areas (is ignored if town/postcode is used)',
        Type    => 'MultiList',
        Options => '(Anywhere=A0|England=A1|London=A11|Greater London=140|Middlesex=122|South East=A12|Berkshire=102|East Sussex=145|Hampshire=114|Kent=118|Surrey=133|West Sussex=155|South West=A13|Avon=141|Channel Islands=142|Cornwall=106|Devon=109|Dorset=110|Gloucestershire=113|Isles of Scilly=149|Somerset=130|Wiltshire=137|East Anglia=A14|Cambridgeshire=104|Essex=112|Norfolk=123|Suffolk=132|North East=A15|Cleveland=143|Durham=111|Humberside=147|North Yorkshire=151|Northumberland=125|South Yorkshire=152|Tyne and Wear=153|North West=A16|Cumberland=107|Cumbria=144|Greater Manchester=146|Isle of Man=148|Lancashire=119|Merseyside=150|West Yorkshire=156|Westmorland=136|West Central=A17|Cheshire=105|Derbyshire=108|Gloucestershire=113|Herefordshire=115|Oxfordshire=127|Shropshire=129|Staffordshire=131|Warwickshire=135|West Midlands=154|Worcestershire=138|East Central=A18|Bedfordshire=101|Buckinghamshire=103|Cambridgeshire=104|Hertfordshire=116|Huntingdonshire=117|Leicestershire=120|Lincolnshire=121|Northamptonshire=124|Nottinghamshire=126|Oxfordshire=127|Rutland=128|Scotland=A3|South=A31|Ayrshire=304|Berwickshire=306|Borders=335|Buteshire=307|Dumfries and Galloway=337|Dumfriesshire=311|Kirkcudbrightshire=318|Lanarkshire=319|Peeblesshire=324|Roxburghshire=328|Selkirkshire=329|Wigtownshire=334|Central=A32|Argyllshire=303|Central=336|Clackmannanshire=310|Dunbartonshire=312|East Lothian=313|Edinburgh=339|Fife=314|Glasgow=340|Kinross-shire=317|Lothian=343|Midlothian=320|Perth and Kinross=347|Perthshire=325|Renfrewshire=326|Stirlingshire=331|Strathclyde=344|West Lothian=333|East=A33|Aberdeenshire=301|Angus=302|Banffshire=305|Dundee=338|Grampian=341|Kincardineshire=316|Morayshire=321|Nairnshire=322|Tayside=345|North=A34|Caithness=309|Cromartyshire=308|Highland=342|Inverness-shire=315|Orkney=323|Ross-shire=327|Shetland=330|Sutherland=332|Western Isles=346|Wales=A2|North Wales=A21|Aberconwy and Colwyn=228|Anglesey=201|Caernarfonshire=203|Cardiganshire=205|Clwyd=214|Conwy=222|Denbighshire=206|Flintshire=207|Gwynedd=217|Merioneth=209|Montgomeryshire=211|Wrexham=223|South Wales=A22|Blaenau Gwent=224|Brecknockshire=202|Bridgend=230|Caerphilly=225|Cardiff=231|Carmarthenshire=204|Cardiganshire=205|Dyfed=215|Glamorgan=208|Gwent=216|Merthyr Tydfil=232|Mid Glamorgan=218|Monmouthshire=210|Neath and Port Talbot=235|Newport=226|Pembrokeshire=212|Powys=219|Radnorshire=213|Rhondda Cynon Taff=233|South Glamorgan=220|Swansea=236|Torfaen=227|Vale of Glamorgan=234|West Glamorgan=221|Ireland=A4|Carlow=401|Cavan=402|Clare=403|Cork=404|Derry=405|Donegal=406|Dublin=407|Galway=408|Kerry=409|Kildare=410|Kilkenny=411|Leitrim=412|Limerick=413|Longford=414|Louth=415|Laois=416|Mayo=417|Meath=418|Offaly=419|Roscommon=420|Sligo=421|Tipperary=422|Waterford=423|Westmeath=424|Wexford=425|Wicklow=426|Northern Ireland=A5|Antrim=501|Armagh=502|Belfast=507|Down=503|Fermanagh=504|Londonderry=506|Tyrone=505)',
        Helper       => 'cvlibrary_location_mapping(%geo%|%location_id%|%location_within%)',
        HelperMetric => 5,
        SortMetric   => $SM_LOCATION,
    };

    $standard_tokens{max_results} = {
        Label => 'Number of Results',
        Type  => 'List',
        Options => '(10 results max=10|20 results max=20|30 results max=30|40 results max=40|50 results max=50)',
        SortMetric => $SM_SEARCH,
    };

    $posting_only_tokens{use_geo} = {
        Helper => 'cvlibrary_use_geo(%geo%)',
    };


    #'part' has been dropped to support the global token
    $posting_only_tokens{'job_type'} = {
            Helper => 'cvlibrary_jobtype_helper(%default_jobtype%|%cvlibrary_jobtype%|permanent=perm|contract=cont|temporary=temp)'
    };

    $standard_tokens{'order'} = {
        Label => 'Order',
        Type  => 'List',
        Options => '(SmartMatch (relevance)=smartmatch|Date=date|Distance=distance)',
        Default => 'smartmatch',
        SortMetric => $SM_SORT,
    };
};

# AUTHTOKENS
BEGIN {
    $authtokens{'cvlibrary_username'} = {
        Label => 'Username',
        Type  => 'Text',
        Size  => 15,
        SortMetric => 1,
        Unique     => 1,
    };

    $authtokens{'cvlibrary_password'} = {
        Label => 'Password',
        Type  => 'Text',
        Size  => 15,
        SortMetric => 2,

    };

#    # CV library have a list of registered emails that are allowed to access each account
#    # This optional authtoken is handy when they have registered a different email address
#    # to their AdC account. If it is empty then we send the AdC email address instead
# Removed 24th March 2010 at CV Library's insistence
# If the client has a different registered email with CV Library they must update
# their AdCourier account so to match addresses
#   Now it is mandatory and unique - agreed by AlexC with Brian - 18 Nov 2010
    $authtokens{'cvlibrary_registered_email'} = {
        Label => 'Work Email',
        Type  => 'Text',
        Size  => 20,
        SortMetric => 3,
        Mandatory  => 1,
        Unique     => 1,
    };
};

############### HELPERS ##########################
## cvlibrary_jobtype_helper test: tests/stream/feeds/global-jobtype/09-cvlibrary.t
sub cvlibrary_jobtype_helper {
    my $self = shift;
    my $new  = shift;
    my $old  = shift;
    return $new ? $self->HASH_MAP($new => @_) : $old;
}

sub cvlibrary_cv_updated {
    my $self = shift;
    my $interval = shift;

    my $date_map = {
        'TODAY' => '1',
        'YESTERDAY' => '1',
        '3D' => '3',
        '1W' => '7',
        '2W' => '14',
        '1M' => '30',
        '2M' => '60',
        '3M' => '90',
        '6M' => '180',
        '1Y' => '365',
        '2Y' => '730',
        '3Y' => '1095',
        'ALL' => '-1',
    };

    if ( !defined( $date_map->{$interval} ) ) {
        return $self->cvlibrary_cv_updated_default();
    }

    return $date_map->{$interval};

}

sub cvlibrary_cv_updated_default {
    return -1; # -1 means all
}

sub cvlibrary_location_mapping {
    my $self = shift;
    my $geo = shift;
    my $location_id = shift;
    my $location_within = shift;

    if ( $self->cvlibrary_use_geo($geo) ) {
        return;
    }

    return $self->LOCATION_MAPPING('stream_cvlibrary', $location_id, $location_within);
}

sub cvlibrary_geolocation_search {
    my $self = shift;
    my $location_id = shift or return;
    my $location_within = shift;

    if ( !$location_within ) {
        # CV Library only support geo searching if there is a location_within
        # fallback to location mapping instead
        return;
    }

    my $location = $self->{location_api}->find($location_id);
    if ( ! $location ) {
        $self->log_debug('Location [%s] not found in database', $location_id);
        return;
    }

    #we want to be able to use the postcode entered by the user (if provided)
    my $user_pcode = $self->token_value('location_id_postcode');
    my $postcode = $self->LOCATION_FULL_POSTCODE($location_id, $user_pcode);
    if ( $postcode ) {
        $self->log_debug('Found postcode [%s]', $postcode);
        $self->cvlibrary_location_within( $location_within );
        return $postcode;
    }

    my $country = $location->country();
    my $country_id = $country ? $country->id() : undef;

    # CV library only support mainland UK only (so not ROI and Northern Ireland)
    if ( $country_id == 39493 || $country_id == 40643 || $country_id == 41110 ) {
        if ( $location->is_place() ) {
            $self->log_debug('UK town so we can radial search');
            $self->cvlibrary_location_within( $location_within );
            return $location->name();
        }

        $self->log_debug('UK county or region so searching by mapping instead');
    }
    else {
        $self->log_warn('Non-UK mainland location chosen. Using mappings instead');
    }

    return;
}

sub cvlibrary_location_within {
    my $self = shift;
    my $geo  = shift;
    my $location_within = shift;

    if ( $self->cvlibrary_use_geo($geo) ) {
        return $location_within;
    }

    return;
}

sub cvlibrary_use_geo {
    my $self = shift;
    my $geo  = shift;

    if ( $geo ) {
        return 1;
    }

    return 0;
}

sub default_url {
    return 'http://www.cv-library.co.uk/cgi-bin/bbstream.cgi';
#    return 'http://tempest.adcourier.com/dump_stuff.cgi';
}

# all session tokens we create last for 40 minutes after the last action
# this is also used for the seassioid/browserTag entanglement
sub session_life_default {
    return 39600; # 660 minutes in seconds
}

# after every request, get the session id from the xml and store it
sub after_content_updated {
    my $self = shift;

    if ( $self->is_watchdog() ) {
        $self->log_debug('This is a watchdog so ignoring the session as it expires immediately');
        return;
    }

    my $session_id = $self->findvalue('//SessionID[1]/text()');

    if ( $session_id ) {
        if ( my $browser_tag = $self->token_value('browser_tag') ) {
            $self->log_debug( 'Storing session id [%s] against browser ID: [%s]',
                $session_id, $browser_tag );
            $self->account_value( $browser_tag . 'session_id', $session_id );
        }
        else {
            # this should never happen, as there was a browser tag to kick
            # this request off
            $self->throw_error('UserHasLogedNoBrowserTag');
        }
    }
    else {
        $self->log_debug('No session id found');
    }
    return;
}

# build the account header xml that is common to both searching and downloading the cv
sub cvlibrary_xml {
    my $self = shift;
    my $callback_ref = shift;

    # the $session_id is '' for watchdogs, everything else should have a session_id
    my $session_id = '';
    unless ( $self->is_watchdog() ) {
        if ( my $browser_tag = $self->token_value('browser_tag') ) {
            $session_id = $self->account_value( $browser_tag . 'session_id' )
              || $self->account_value('session_id');    # For backward compat.
        }
        else {
            $self->throw_error('UserHasLogedNoBrowserTag');
        }

    }

    my $xml = Bean::XML->new( ENCODING => 'UTF-8' );
    $xml->startTag('CV_Search_Query');
        $xml->startTag('Account');
            $xml->dataElement( 'Username'  => $self->token_value('username') );
            $xml->dataElement( 'Password'  => $self->token_value('password') );
            $xml->dataElement( 'SessionID' => $session_id );
            $xml->dataElement( 'User'      => $self->token_value('registered_email') );
            $xml->dataElement( 'IP'        => $self->cvlibrary_ip() );
        $xml->endTag('Account');

        $callback_ref->( $self, $xml );

    $xml->endTag('CV_Search_Query');
    $xml->end();

    return $xml->asString();
}

# return the user's email address (currently comes from the adcourier account details)
# this returns the email of the search owner, not the person running the search
# No longer used as it is now an authtoken
sub cvlibrary_contact_email {
    confess("Nowhere to be found in the codebase. Fixme if you find me");
    my $self = shift;

    my $username = sprintf '%s@%s.%s.%s', $self->consultant(), $self->team(), $self->office(), $self->company();

    my $user_details_ref = eval {
        ## Make this work if we need it..
        $self->user_api->get_user({ username => $username });
        ## GetInfoForUser::xmldb_getinfo_user( $username );
    };

    if ( $@ ) {
        $self->log_warn('Unable to find username: %s', $username);
        return;
    }

    if ( $user_details_ref->{feedbackemail} =~ m/\@/ ) {
        my $email = $user_details_ref->{feedbackemail};
        $self->log_debug('Using feedback email [%s]', $email );
        return $email;
    }

    return $user_details_ref->{email};
}

# return the user's ip address
sub cvlibrary_ip {
    my $self = shift;

    if ( $self->is_watchdog() ) {
        $self->log_debug("We are a watchdog so sending the creator's IP address from the search");
        return $self->creator_ip();
    }

    return Stream::SharedUtils::remote_addr();
}

############### LOGIN ############################
############### SEARCH PAGE #######################
sub search_failed {
    return '<Error>';
}

sub search_build_xml {
    my $self = shift;

    $self->log_debug("Adding CV Library Bulk messaging notice");
    $self->results()->add_notice('Bulk messaging is only available directly on CV-Library');

    $self->log_debug('worker: [%s], pid: [%s]', $0, $$);

    return $self->cvlibrary_xml(
        \&search_build_body_xml,
    );
}

sub search_build_test_body_xml {
    my $self = shift;
    my $xml  = shift;

    $xml->startTag('Search_Query');
        $xml->dataElement('q' => 'sales');
        $xml->dataElement('searchjobtitleonly' => 0 );
        $xml->dataElement('use_geo' => 1 );
        $xml->dataElement('geo' => 'GU51' );
        $xml->dataElement('distance' => 20 );
        $xml->dataElement('distance_unit' => 'miles' );
        $xml->dataElement('relocate' => 0 );
        $xml->dataElement('time' => 7 );
        $xml->dataElement('salary' => 1 );
        $xml->dataElement('salary' => 2 );
        $xml->dataElement('salary' => 3 );
        $xml->dataElement('area' => '' );
        $xml->dataElement('industry' => '' );
        $xml->dataElement('language' => '' );
        #$xml->dataElement('temp-perm' => '' );
        $xml->dataElement('minmatch' => 0 );
        $xml->dataElement('order' => 'smartmatch');
        $xml->dataElement('perpage' => 10);
        $xml->dataElement('offset' => 0);
    $xml->endTag('Search_Query');

    $xml;
}

# return an array or an empty 1 element array so we will always create the xml element
# even if it is empty
sub token_force_array {
    my $self = shift;
    my $token = shift;

    my @values = $self->token_value($token);

    if ( @values ) {
        return (@values);
    }

    return ('');
}

sub search_build_body_xml {
    my $self = shift;
    my $xml = shift;

    $xml->startTag('Search_Query');
        $xml->dataElement('q'                  => $self->token_value('keywords') );
        $xml->dataElement('searchjobtitleonly' => $self->token_value('searchjobtitleonly') || '0' );
        $xml->dataElement('use_geo'            => $self->token_value('use_geo') );
        $xml->dataElement('geo'                => $self->token_value('geo') );
        $xml->dataElement('distance'           => $self->token_value('distance') );
        $xml->dataElement('distance_unit'      => 'miles' );
        $xml->dataElement('relocate'           => $self->token_value('relocate') || '0' );
        $xml->dataElement('time'               => $self->token_value('time') );

        foreach my $salary_id ( $self->token_force_array('salary') ) {
            $xml->dataElement('salary'   => $salary_id );
        }
        foreach my $area_id ( $self->token_force_array('areas') ) {
            $xml->dataElement('area'     => $area_id );
        }
        foreach my $industry ( $self->token_force_array('industry') ) {
            $xml->dataElement('industry' => $industry );
        }
        foreach my $language_id ( $self->token_force_array('languages') ) {
            $xml->dataElement('language' => $language_id );
        }
        $xml->dataElement('temp-perm' => $self->token_value('job_type') );
        $xml->dataElement('minmatch'  => $self->token_value('minmatch') || 0 );
        $xml->dataElement('order'     => $self->token_value('order')    || 'smartmatch' );
        $xml->dataElement('perpage'   => $self->results_per_page() );
        $xml->dataElement('offset'    => $self->results_offset() );
        $xml->dataElement('stem_phrases' => $self->token_value('expand_search') );

        if ( $self->is_watchdog() ) {
            $xml->dataElement('watchdog' => 1);
        }

    $xml->endTag('Search_Query');

    return $xml;
}

sub scrape_candidate_xpath { return '//Result'; }
sub scrape_candidate_details {
    return (
        candidate_id      => './Record_Id/text()',
        relevance         => './Relevance/text()',
        raw_last_modified => './Last_Modified/text()',
        availability      => './Available/text()',
        name              => './Name/text()',
        town              => './Town/text()',
        county            => './County/text()',
        current_job_title => './CurrentJobTitle/text()',
        desired_job_title => './DesiredJobTitle/text()',
        salary            => './ExpectedSalary/text()',
        skills            => './Skills/text()',
        travel            => './Travel/text()',
        relocate          => './Relocate/text()',
        jobtype           => ['./TempPerm/text()'],
        distance          => './Distance/text()',
    );
}

sub scrape_candidate_headline {
    return qw/current_job_title desired_job_title/;
}

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    $candidate->attr('block_bulk_message' => 1 );

    my @locations;
    foreach my $attr ( qw/town county/ ) {
        my $loc = $candidate->attr( $attr );
        if ( $loc ) {
            push @locations, $loc;
        }
    }

    $candidate->attr(
        location_text => join(', ', @locations),
    );

    $candidate->attr( snippet => $candidate->attr('skills') );

    my $last_modified = $candidate->attr('raw_last_modified');

    if ( $last_modified ) {
        my $strp = DateTime::Format::Strptime->new(
            pattern => '%d/%m/%Y (%T)',
        );

        $self->log_debug('Parsing date [%s]', $last_modified);

        my $dt = $strp->parse_datetime( $last_modified );

        if ( $dt ) {
            $candidate->attr( cv_last_updated => $dt->epoch() );
        }
        else {
            $self->log_warn('Unable to parse date [%s]: [%s]', $last_modified, $strp->errmsg );
        }
    }
    else {
        $self->log_warn('Candidate [%s] has no raw_last_modified attr', $candidate->id());
    }

    # CV Library return 'Account Expired' instead of the candidate name if your account
    # has expired. We will blank the name if that happens
    my $name = $candidate->name();
    if ( $name =~ m/Account Expired/i ) {
        $self->log_debug('Blanking name of candidate because their CV library account has expired');
        $candidate->name('');
    }

    $candidate->has_profile( 1 );

    return $candidate;
}

sub search_number_of_results_xpath { return '//Matches_Total/text()'; }
sub scrape_paginator {
    my $self = shift;
    my $total_results = $self->total_results();
    my $results_per_page = $self->results_per_page();

    if ( $total_results <= $results_per_page ) {
        $self->log_debug('There is only 1 page of results');
        $self->max_pages( 1 );
    }
    else {
        my $total_pages = POSIX::ceil( $total_results / $results_per_page );
        $self->log_debug('There are %d pages', $total_pages );
        $self->max_pages( $total_pages );
    }

    return;
}

#################### DOWNLOAD PROFILE #########

sub download_profile_xml {
    my $self = shift;
    my $candidate = shift;

    my $session_id = '';
    if ( my $browser_tag = $self->token_value('browser_tag') ) {
            $session_id = $self->account_value( $browser_tag . 'session_id' )
                 || $self->account_value('session_id');    # For backward compat.
    }
    else {
        $self->throw_error('UserHasLogedNoBrowserTag');
    }

    my $xml = Bean::XML->new();
    $xml->startTag('CV_Search_Query');
        $xml->startTag('Account');
            $xml->dataElement('Username' => $self->token_value('username') || '' );
            $xml->dataElement('Password' => $self->token_value('password') || '' );
            $xml->dataElement('SessionID' => $session_id );
            $xml->dataElement('User' => $self->token_value('registered_email') || '');
            $xml->dataElement('IP' => $self->cvlibrary_ip() || '');
        $xml->endTag('Account');
        $xml->startTag('Candidate_Webpage_Query');
            $xml->dataElement('Record_Id' => $candidate->candidate_id() || '' );
        $xml->endTag('Candidate_Webpage_Query');
    $xml->endTag('CV_Search_Query');

    return $xml;
}

sub scrape_download_profile_xpath {
    return '//CV_Search_Query';
}

sub scrape_download_profile_details {
    return (
        profile_html        => './Candidate_Webpage_Response',
    );
}

sub download_profile_success {
    return qr/Candidate_Webpage_Response>/i;
}

sub after_download_profile_scrape_success {
    
    my $self = shift;
    my $candidate = shift;

    #all we have is raw double escaped html

    my $raw_html = $candidate->attr('profile_html');
    
    use HTML::Entities;
    $raw_html = decode_entities($raw_html);

    $raw_html =~ s/<HTML>\n*<HEAD>\n*<TITLE>CV-library\.co\.uk - Reference# \d+<\/TITLE>\n*<HEAD>\n*<BODY>\n*//i;
    $raw_html =~ s/<br>\n*<\/BODY>\n*<\/html>//i;
    $raw_html =~ s/\salign="[^"]*"//gi; #32947, causing display errors in IE 7 / 8

    $candidate->attr('profile_html' => $raw_html);

    return $candidate;
}


#################### DOWNLOAD CV ##############
sub download_cv_xml {
    my $self = shift;
    my $candidate = shift;

    return $self->cvlibrary_xml(
        sub {
            my $self = shift;
            my $xml = shift;

            $xml->startTag('Candidate_CV_Query');
                $xml->startTag('Record_Id');
                    $xml->characters( $candidate->candidate_id() );
                $xml->endTag('Record_Id');
            $xml->endTag('Candidate_CV_Query');

            $xml;
        },
    );
}

sub download_cv_failed {
    return qr/<Error>/;
}

sub scrape_download_cv_xpath { return '//Candidate_CV_Response'; }
sub scrape_download_cv_details {
    return (
# already have these from the search results
        resume_word_base64 => './File/text()',
        resume_filename    => './File/@filename',
        telephone          => './Telephone',
        mobile_telephone   => './Mobile',
        email              => './Email',
    );
}

# Full list of errors from Brian Wakem: 15/09/2009
#<Error>Invalid XML received: $error</Error>
#<Error>User not supplied in xml</Error>
#<Error>Invalid IP '$ip' supplied in xml</Error>
#<Error>Request did not contain Search_Query or Candidate_CV_Query</Error>
#<Error>umid belongs to a different user</Error>
#<Error>No user associated with session</Error>
#<Error>Unknown session error umid:$cookie_umid account:$account time: $time</Error>
#<Error>Unknown search error</Error>
#<Error>Account does not have CV database access</Error>
#<Error>Download limit reached</Error>
#<Error>$ip is not allowed to connect to this service</Error>
#<Error>Account expired</Error>
#<Error>Invalid username/password</Error>
#<Error>$ip cannot use this account</Error>
#<Error>Too many users logged into this account. Call 01252 810095 to increase your user allowance</Error>
#<Error>User $email already in session</Error>
#<Error>You do not have permission to access this service. Please call 01252 810995 for further information</Error>
#<Error>Email $email is invalid</Error>
#<Error>CV does not exist</Error>
#<Error>Permission denied, CV is not in allowed areas for this account</Error>
#<Error>You do not have permission to view this CV</Error>
#<Error>Could not locate $location. This facility only works for UK mainland addresses</Error>
#<Error>Search returned more than $maxrows results. Please be more specific</Error>
#<Error>Invalid query. Unmatched quote or bracket?</Error>
#<Error>There is more than one $location, please use a postcode instead</Error>
#<Error>Please specify distance from $location</Error>
#<Error>Cannot rank matches without keyword(s)</Error>
sub feed_identify_error {
    my $self = shift;
    my $message = shift;
    my $content = shift;

    # this means a user has logged in from another browser, this error is generated
    # before any request is sent, so there is no content
    if ( $message =~ 'UserHasLogedNoBrowserTag' ) {
        return $self->throw_login_error( 'You cannot search CV Library from multiple browsers/devices concurrently.' );
    }

    if ( !defined( $content ) ) {
        $content = $self->content();
        return unless $content;
    }

    if ( $content =~ m{(Invalid username/password)} ) {
        return $self->throw_login_error($1);
    }

    if ( $content =~ m{<Error>(.+)</Error>} ) {
        my $error_msg = $1;

        if( $error_msg =~ m{User not supplied} ){
            return $self->throw_login_error("Missing CV Library account settings ($error_msg)");
        }

        if ( $error_msg =~ m{Search returned more than 10,000 results} ) {
            return $self->throw_too_many_results( $error_msg );
        }

        if ( $error_msg =~ m{User .+ already in session} ) {
            return $self->throw_account_in_use( $error_msg );
        }

        if ( $error_msg =~ m{Please specify distance from } ) {
            return $self->throw_known_internal( $error_msg );
        }

        if ( $error_msg =~ m{This facility only works for UK mainland addresses} ) {
            return $self->throw_known_internal( $error_msg );
        }

        # eg. we sent an invalid postcode
        if ( $error_msg =~ m{Unable to find location } ) {
            return $self->throw_known_internal( $error_msg );
        }

        if ( $error_msg =~ m{You do not have permission to access this service} ) {
            return $self->throw_login_error( $error_msg );
        }

        if ( $error_msg =~ m{\d+ cannot use this account} ) {
            return $self->throw_login_error( $error_msg );
        }

        if ( $error_msg =~ m{please use a postcode instead} ) {
            return $self->throw_known_internal( $error_msg );
        }

        if ( $error_msg =~ m{Account does not have CV database access} ) {
            return $self->throw_no_cvsearch( $error_msg );
        }

        if ( $error_msg =~ m{Account expired} ) {
            return $self->throw_inactive_account( $error_msg );
        }

        if ( $error_msg =~ m{Too many users logged into this account} ) {
            return $self->throw_account_in_use( $error_msg );
        }

        if ( $error_msg =~ m{Download limit reached} ) {
            return $self->throw_lack_of_credit( $error_msg );
        }

        if ( $error_msg =~ m{Invalid query|Unmatched quotes} ) {
            return $self->throw_invalid_boolean_search( $error_msg );
        }

        if ( $error_msg =~ m{cannot mix AND/OR in requests}i ) {
            return $self->throw_invalid_boolean_search( $error_msg );
        }

        if ( $error_msg =~ m{Email .*? is invalid} ) {
            return $self->throw_invalid_email_address( $error_msg );
        }

        return $error_msg;
    }

    return undef;
}

1;
# vim: expandtab shiftwidth=4

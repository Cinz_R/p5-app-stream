package Stream::Templates::RestMock;

use strict;
use warnings;

use base qw|Stream::Engine::API::REST|;

sub download_cv_url {
    return 'https://httpbin.org/post';
}

sub download_cv_fields {
    my ($self, $candidate) = @_;

    return ();
}

sub scrape_download_cv {
    my ($self, $candidate) = @_;
    my $response = $self->SUPER::scrape_download_cv($candidate);
    $response->header('X-Candidate-Email' => $candidate->email);
    return $response;
}

1;

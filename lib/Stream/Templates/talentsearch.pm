package Stream::Templates::talentsearch;

=head1 NAME

Stream::Templates::talentsearch - rebranded version of iProfile search for AMS phase 0

=head1 CONTACT

Graham Ruffell - graham.ruffell@iprofile.org

=head1 TESTING INFORMATION

None

=head1 DOCUMENTATION

=head1 OVERVIEW

Standard SOAP webservice using .NET.

=head1 CHANGES

none yet

=head1 BUG FIXES

none yet

=cut

use strict;

use base qw(Stream::Templates::artirix);

use Log::Any qw/$log/;

use Search::Tools::UTF8;

################ TOKENS #########################
use vars qw(%standard_tokens %derived_tokens %posting_only_tokens %authtokens %global_authtokens);

BEGIN {
    __PACKAGE__->inherit_tokens();
};


my @POTENTIALLY_BROKEN_STRINGS = qw/name address/;

# check Stream2::Results::Result::set_canonical_values for the full list
# of canonical names
my %canonical_attr_mappings = (
    name      => 'name',
    address   => 'address',
    postal_code  => 'extracted_postcode',
    telephone => 'telephone',
    mobile    => 'mobile',
    cv_text   => 'cv_text',
    employer  => 'employer_org_name',
    job_title => 'employer_org_position_title',
    job_description  => 'employer_org_position_description',
);

sub scrape_fixup_candidate {
    my $self = shift;
    my $candidate = shift;

    # this candidate can be edited
    $candidate->attr( editable => 1 );

    foreach my $potentially_broken_string ( @POTENTIALLY_BROKEN_STRINGS ){
        if( my $string = $candidate->attr($potentially_broken_string) ){
            # It is possible that the string was incorrectly stored
            # as UTF-8 raw bytes. Because somewhere in the whole process
            # inside adcourier/ talentsearch importing, stuff get Snafu and good luck to know where it is.
            #
            # The trick is to decode as utf8, ONLY when the string of bytes
            # looks like dodgy double encoded UTF8. Strings that are already correct will not be affected by that.
            # strings that are double encoded will now be correct most of the times.
            unless( Search::Tools::UTF8::is_sane_utf8( $string ) ){
                $log->warn("Candidate $potentially_broken_string '$string' looks like it was insanely UTF-8 double encoded. Decoding it once to try to fix things.");
                $string = Encode::decode('utf8', $string );
                $candidate->attr( $potentially_broken_string => $string );
            }
        }
    }

    if ($candidate->attr('cv_url')){
        $candidate->has_cv(1);
        # If theres is a CV, we can also build a
        # preview profile. has_profile says that.
        $candidate->has_profile(1);
    }

    my $raw_application_time = $candidate->attr('application_time');
    my $raw_employer_org_position_start_date = $candidate->attr('employer_org_position_start_date');
    my $raw_employer_org_position_end_date = $candidate->attr('employer_org_position_end_date');
    my $raw_applicant_availability_date = $candidate->attr('applicant_availability_date');

    if ( $raw_application_time ){
        my $application_time = $candidate->pattern_to_epoch( '%Y-%m-%d' => $raw_application_time)
        || $raw_application_time;

        $candidate->attr('application_time_epoch' => $application_time );
    }

    if ( $raw_employer_org_position_start_date ) {
        my $employer_org_position_start_date = $candidate->pattern_to_epoch( '%Y-%m-%d' => $raw_employer_org_position_start_date )
        || $raw_employer_org_position_start_date;

        $candidate->attr('employer_org_position_start_date_epoch' => $employer_org_position_start_date );
    }

    if ( $raw_employer_org_position_end_date ) {
        my $employer_org_position_end_date = $candidate->pattern_to_epoch( '%Y-%m-%d' => $raw_employer_org_position_end_date)
        || $raw_employer_org_position_end_date;

        $candidate->attr('employer_org_position_end_date_epoch' => $employer_org_position_end_date );
    }

    if ( $raw_applicant_availability_date ){
       my $applicant_availability_date = $candidate->pattern_to_epoch('%Y-%m-%d' => $raw_applicant_availability_date) || $raw_applicant_availability_date;
       $candidate->attr('applicant_availability_date_epoch' => $applicant_availability_date );
    }

    my $highlighting = $self->{'response_highlighting'};

    if ($highlighting && $highlighting->{$candidate->candidate_id}){
        $candidate->attributes(
                highlight_cv => $highlighting->{$candidate->candidate_id}->{'cv_text'},
                highlight_title => $highlighting->{$candidate->candidate_id}->{'employer_org_position_title'},
                highlight_description => $highlighting->{$candidate->candidate_id}->{'employer_org_position_description'},
         );
    }

    my $applicant_rec_status = $candidate->attr('applicant_recruitment_status');

    if(!defined $applicant_rec_status) {
        $candidate->attr('applicant_recruitment_status' => 'Available');
    }

    # set the canonical values as the final step.
    $candidate->set_canonical_values(%canonical_attr_mappings);
}


1;

package Stream::Templates::jobbird;

use strict;
use warnings;

use Stream2::Feeds::Facet::Text;

use base 'Stream::Engine::Composition';

use URI;
use Log::Any qw/$log/;
use POSIX ();

use Stream::Constants::SortMetrics ();

# The board does not return a reliable page size. They return
# the count of candidates on the current page. This can bug us out on the last page
# where we get fewer candidates than the pagesize. 
# Therefore use a constant size
sub page_size {
    return 25;
}

# Hash of GET params for the search query
sub search_fields {
    my ($self, $options) = @_;

    my %fields;

    $fields{Page} = $options->{results}->current_page;

    # The board doesn't handle dynamic page sizes robustly.
    # Their returned value is actually the number of results on that page
    # Messes up in the last page where there can be fewer results
    $fields{PageSize} = $self->page_size;

    $fields{Keyword} = $self->facets->value_of('keywords');

    my $location_id = $self->facets->value_of('location_id');
    my $radius = $self->facets->value_of('location_within_km');
    if($location_id) {
        my $location = $self->location_api->find($location_id);
        if(!$location) {
            return $self->throw_internal(
                'Could not find location with ID ' . $location_id
            );
        }
        $fields{Latitude} = $location->latitude;
        $fields{Longitude} = $location->longitude;
        $fields{Distance} = $radius if $radius;
    }

    # Broken on board's end. (2013 gets fewer results than 2016)
    # Waiting for them to fix/clarify
    #my $datetime = $self->facets->get('cv_updated_within')->datetime;
    #if($datetime) {
    #    $fields{ModifiedDateMax} = $datetime->subtract(days => 1)->datetime;
    #}

    $fields{MatchTitleWords} = $self->facets->value_of('job_title');
    return %fields;
}

# Add as a header to API requets
sub _auth_header {
    return 'Ocp-Apim-Subscription-Key' => 'bfd301352f384554a14fc77e0d6f7049';
}

sub base_url {
    return URI->new('https://api.jobbird.com');
}

sub _search {
    my ($self, $options) = @_;

    my %fields = $self->search_fields($options);

    my $url = $self->base_url;
    $url->path_segments( $url->path_segments, 'cv', '');
    $url->query_form(%fields);
    my $json = $self->mech->get( $url, $self->_auth_header )->decoded_content;

    my $data = JSON::decode_json($json);

    my $results = $options->{results};
    my $hits = $data->{Hits};
    $log->info('Found ' . scalar(@$hits) . ' candidates to scrape');
    foreach my $profile (@$hits) {
        my $candidate = $self->scrape_candidate($profile);
        $results->add_candidate($candidate);
    }

    $results->total_results($data->{Total});
    my $page_count = 0;
    $page_count = POSIX::ceil($data->{Total} / $self->page_size)
        if $data->{Total};

    # Max of 400 pages
    $page_count = 400 if $page_count > 400;
    $results->max_pages($page_count);

    return;
}

sub scrape_candidate {
    my ($self, $profile) = @_;

    my $candidate = $self->new_candidate;
    my $taxonomies = $self->taxonomies;

    $candidate->attr(profile => $profile);
    $candidate->attr(headline => $profile->{CvTitle});

    my $email = $profile->{EmailAddress};
    $candidate->email($email) if $email;

    $candidate->attr(telephone => $profile->{TelephoneNumber})
        if $profile->{TelephoneNumber};
    $candidate->candidate_id( $profile->{Id});

    my $country = $self->taxonomize('country', uc($profile->{Country}));
    my $location = join ', ', grep{ $_ } ( $profile->{City}, $country );
    $candidate->location_text($location) if $location;

    my @name_parts = grep {$_} $profile->{FirstName}, $profile->{SurName};
    my $name = join(' ', @name_parts);
    $candidate->name($name) if $name;
    $candidate->has_profile(1);
    $candidate->has_cv( $profile->{CvUrl} ? 1 : 0 );
    $candidate->attr(has_free_cv => 1);

    # Geolocation
    $candidate->attr(latitude => $profile->{Latitude})
        if $profile->{Latitude};
    $candidate->attr(longitude => $profile->{Longitude})
        if $profile->{Longitude};

    # Add positionCodes
    my $position_keys = $profile->{Position};
    if($position_keys) {
        $log->debug('Taxonomizing Position');
        $profile->{PositionCodes} = [
            map {
                $self->taxonomize('position', $_);
            } @$position_keys
        ];
    }

    my $salary_keys = $profile->{SalaryRange};
    if($salary_keys) {
        $log->debug('Taxonomizing SalaryRange');
        $profile->{SalaryRangeCodes} = [
            map {
                $self->taxonomize('salary', $_);
            } @$salary_keys
        ];
    }
    $candidate->attr(json => JSON->new->pretty->encode($profile));

    my $modified_date = $profile->{ModifiedDate};
    $modified_date =~ s/T.+$//;
    $candidate->attr(last_modified => $modified_date);

    return $candidate;
}

# Returns the label for key $key in taxonomy group $group
# or '' if not found
sub taxonomize {
    my ($self, $group, $key) = @_;
    my $value = $self->taxonomies->{$group}->{$key};
    if(!defined($value)) {
        $log->warn('Failed to taxonomise ' . $group . '=> ' . $key);
        return '';
    }
    return $value;
}

# Returns a hahref of taxonomies
sub taxonomies {
    my ($self) = @_;

    my $taxonomies = $self->{_taxonomies};
    return $taxonomies if $taxonomies;

    my $cache_key = 'jobbird_taxonomies';
    my $cache = $self->stream2->stream2_cache;
    $taxonomies = $cache->get($cache_key);
    if($taxonomies) {
        $log->info('Retrieved taxonomies from cache');
        $self->{_taxonomies} = $taxonomies;
        return $taxonomies;
    }
    $log->info('Fetching taxonomies');
    $taxonomies = {
        salary   => $self->get_taxonomy_group('Salary'),
        position => $self->get_taxonomy_group('Position'),
        country  => $self->get_taxonomy_group('Country'),
    };

    # Cache for a day
    $cache->set($cache_key, $taxonomies, 24 * 60 * 60);
    
    $self->{_taxonomies} = $taxonomies;
    return $taxonomies;
}

# Returns a hashref of taxonomy key => label
sub get_taxonomy_group {
    my ($self, $key) = @_;
    $log->info('Fetching taxonomy for ' . $key);
    my $url = $self->base_url;
    $url->path_segments(
        $url->path_segments,
        'MetaData',
        'Get' . $key . 'Options'
    );
    $self->mech->get(
        $url, $self->_auth_header
    );

    my $data = JSON::decode_json($self->mech->content);
    my $output = {};
    foreach my $taxonomy (@$data) {
        $output->{$taxonomy->{Key}} = $taxonomy->{Value};
    }
    return $output;
}

sub _download_cv {
    my ($self, $candidate) = @_;
    my $url = $candidate->attr('profile')->{CvUrl};
    my ($filename) = $url =~ /([^\/]+)$/;
    my $type = $self->_stream2->mime_types()->mimeTypeOf( $filename )->type;

    my $response = $self->mech->get($url);
    $response->headers->header(
        'Content-Disposition' => 'attachment; filename="' . quotemeta($filename) . '";',
        'Content-Type' => $type,
    );
    return $response;
}

# No additional profile information.
sub _download_profile {
    my ($self, $candidate) = @_;
    $log->info('All candidate information is provided from the search results');
    return $candidate;
}

sub _initial_facets {
    my ($self) = @_;
    my $facets= $self->SUPER::_initial_facets();

    $facets->add_facets(
        job_title => Stream2::Feeds::Facet::Text->new(
            label => 'Job Title'
        ),
    );

    return $facets;
}

1;

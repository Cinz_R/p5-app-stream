package Stream::Templates::adcresponses;

=head1 NAME

Stream::Templates::adcresponses - Template for AdCourier response page powered by BBCSS

=head1 AUTHOR

Patrick - 21/07/2015

=head1 CONTACT

Monica ( PM ) - Monica.Skidmore@careerbuilder.com
Bob ( Lead Dev ) - Bob.Fritsch@careerbuilder.com

=head1 TESTING INFORMATION

=head1 DOCUMENTATION

Original candidatesearch documentation:

L<http://candidate-search-useast.cb-search.com/CandidateSearchAPI/swagger/ui/index.html>

Specific ResponsesHolder customer (and their fields):

L<https://bitbucket.org/broadbean/p5-app-bbcss/src/master/lib/BBCSS/Role/Customer/ResponsesHolder.pm?at=master&fileviewer=file-view-default>

=head1 OVERVIEW

=head1 EXTRA INFORMATION

=head1 MAJOR CHANGES

none yet

=head1 BUG FIXES

none yet

=cut

use strict;

use base qw(Stream::Templates::BBCSS);

use Stream::Constants::SortMetrics ':all';
use Log::Any qw/$log/;
use Date::Parse qw//;
use Locale::Country qw//;
use Data::Dump ();
use HTML::Entities;
use List::Util ();
use MIME::Base64;
use Stream2::Translations qw/t9np/;
use Stream::Token::Salary;

use vars qw(%standard_tokens %custom_tokens);


use Stream2::O::File;

=head2 adcresponses_required_fields

A list of properties we would like CS to return per candidate

=cut

sub adcresponses_required_fields {
    qw/
        first_name
        last_name
        email
        city
        postal_code
        latitude
        longitude
        country
        phone
        currently_employed
        total_years_experience
        activity_history
        address_1
        admin_area_1
        document_files
        company_experience_list
        skill_list
        education_list
        document_id
        flagging_history

        broadbean_group_identity
        broadbean_adcresponse_id
        broadbean_adcadvert_id
        broadbean_adcboard_id
        broadbean_adcuser_id
        broadbean_adcidentity_path
        broadbean_adcresponse_date
        broadbean_adcresponse_subject
        broadbean_adcresponse_rank
        broadbean_adcresponse_rank_reason
        broadbean_adcresponse_attachement_cv
        broadbean_adcresponse_attachments
        broadbean_adcresponse_email_body
        broadbean_adcresponse_name
        broadbean_adcresponse_tags
        broadbean_adcresponse_progressed
        broadbean_adcresponse_forwarded
        broadbean_adcresponse_source_id
        broadbean_adcresponse_shortlisted
        broadbean_adcresponse_updated
        broadbean_adcresponse_tagged_time
        broadbean_adcresponse_gender
        broadbean_adcresponse_is_read
        broadbean_adcadvert_is_requisition_ad
        broadbean_adcresponse_general_submission_sector
        broadbean_adcresponse_general_submission_location
        broadbean_adcresponse_general_submission_jobtype
        broadbean_group_adcresponse_id
    /;
}

%standard_tokens = (
    country => {
        Id => 'adcresponses_country',
        Name => 'country',
        Type => 'multilist',
        Label => 'Country',
        SortMetric => 80,
    },
    skill_list => {
        Id => 'adcresponses_skill_list',
        Name => 'skill_list',
        Type => 'multilist',
        Label => 'Skills',
        SortMetric => 90,
    },
);

%custom_tokens = (
    # "custom fields"
    broadbean_adcadvert_id => {
        Id => 'adcresponses_broadbean_adcadvert_id',
        Name => 'broadbean_adcadvert_id',
        Type => 'advert',
        Label => t9np('feed.adcresponses', 'Adverts'),
        SortMetric => 0
    },
    broadbean_adcresponse_rank => {
        Id => 'adcresponses_broadbean_adcresponse_rank',
        Name => 'broadbean_adcresponse_rank',
        Type => 'multilist',
        Label => t9np('feed.adcresponses', 'Flags'),
        SortMetric => 5
    },
    broadbean_adcresponse_tags => {
        Id => 'adcresponses_broadbean_adcresponse_tags',
        Name => 'broadbean_adcresponse_tags',
        Type => 'tag',
        Label => t9np('feed.adcresponses', 'Tags'),
        SortMetric => 10
    },
    broadbean_adcresponse_usertags => {
        Id => 'adcresponses_broadbean_usertags',
        Name => 'broadbean_adcresponse_usertags',
        Type => 'multilist',
        Label => t9np('feed.adcresponses', 'My Folders'),
        SortMetric => 20        # clashes with $SM_LOCATION
    },
    broadbean_adcboard_id => {
        Id => 'adcresponses_broadbean_adcboard_id',
        Name => 'broadbean_adcboard_id',
        Type => 'multilist',
        Label => t9np('feed.adcresponses', 'Source'),
        SortMetric => 60
    },
    broadbean_adcresponse_is_read => {
        Id => 'adcresponses_broadbean_adcresponse_is_read',
        Name => 'broadbean_adcresponse_is_read',
        Type => 'multilist',
        Label => t9np('feed.adcresponses', 'Viewed'),
        SortMetric => 40
    },
    broadbean_employer_years => {
        Id => 'adcresponses_broadbean_employer_years',
        Name => 'broadbean_employer_years',
        Type => 'multilist',
        Label => t9np('feed.adcresponses', 'Years at current employer'),
        SortMetric => 210
    },
    broadbean_employer_org_job_title => {
        Id => 'adcresponses_broadbean_employer_org_job_title',
        Name => 'broadbean_employer_org_job_title',
        Type => 'text',
        Label => t9np('feed.adcresponses', 'Current job title'),
        SortMetric => 180
    },
    broadbean_employer_org_name => {
        Id => 'adcresponses_broadbean_employer_org_name',
        Name => 'broadbean_employer_org_name',
        Type => 'text',
        Label => t9np('feed.adcresponses', 'Current employer'),
        SortMetric => 200
    },
    broadbean_adcresponse_source_id => {
        Id => 'adcresponses_broadbean_adcresponse_source_id',
        Name => 'broadbean_adcresponse_source_id',
        Type => 'multilist',
        Label => t9np('feed.adcresponses', 'CV Origin'),
        SortMetric => 55
    },
    broadbean_adcresponse_general_submission_sector => {
        Id => 'adcresponses_broadbean_adcresponse_general_submission_sector',
        Name => 'broadbean_adcresponse_general_submission_sector',
        Type => 'multilist',
        Label => t9np('feed.adcresponses', 'General Submissions Sector'),
        SortMetric => 56
    },
    broadbean_adcresponse_general_submission_location => {
        Id => 'adcresponses_broadbean_adcresponse_general_submission_location',
        Name => 'broadbean_adcresponse_general_submission_location',
        Type => 'multilist',
        Label => t9np('feed.adcresponses', 'General Submissions Location'),
        SortMetric => 57
    },
    broadbean_adcresponse_general_submission_jobtype => {
        Id => 'adcresponses_broadbean_adcresponse_general_submission_jobtype',
        Name => 'broadbean_adcresponse_general_submission_jobtype',
        Type => 'multilist',
        Label => t9np('feed.adcresponses', 'General Submissions Job Type'),
        SortMetric => 58
    },

);

# check Stream2::Results::Result::set_canonical_values for the full list
# of canonical names
my %canonical_attr_mappings = (
    name      => 'name',
    address   => 'address',
    city      => 'city',
    country   => 'country',
    education => 'education_list',
    postal_code  => 'postcode',
    telephone => 'phone',
    cv_text   => 'cv_text',
);

sub tags_field_name{ 'broadbean_adcresponse_tags'; }

sub bbcss_flavour { 'ResponsesHolder' }

=head2 adcresponses_custom_fields

Fetches list of custom fields from BBCSS and builds a field definition for each one

=cut

sub adcresponses_custom_fields {
    my ( $self ) = @_;
    my $customer = $self->bbcss_customer();

    unless ( $self->{_adcresponses_custom_fields} ){
        # we have some standard custom fields defined above, use that definition if available
        # if no definition in %custom_tokens is given, the field is not taken into account.
        $self->{_adcresponses_custom_fields} = { map {

            defined( $custom_tokens{$_->{field_name}} ) ?
                ( $_->{field_name} => $custom_tokens{$_->{field_name}} ) :
                ();

        } @{$customer->{config}->{custom_fields} // []} };
    }

    return $self->{_adcresponses_custom_fields};
}

=head2 scrape_page

Complete re-implementation that will resolve the advert IDs
in the facets and in the results.

=cut

sub scrape_page {
    my ($self) = @_;

    $log->infof( 'Page: %d', $self->current_page() );

    my @candidate_results = $self->scrape_candidate_array();

    $self->scrape_facets();

    # At this point, $self->results()->facets() is populated
    # and the raw candidates are in @candidate_results.
    {
        # Resolve the wonderful broadbean_adcresponse_usertags
        my $usertags_facet =  List::Util::first { $_->{Name} eq 'broadbean_adcresponse_usertags' } @{ $self->results->facets() };

        # Filter usertags. We want only the ones for this installation ID.
        my $stream2 = $self->user_object()->stream2();
        my $installation_id = $stream2->installation_id();

        # We only want the options from this installation ID (Case insensitive, cuz you know..)
        $usertags_facet->{Options} = [ grep{ $_->[1] =~ /^$installation_id@/i } @{$usertags_facet->{Options} // []} ];

        # Extract tag Ids
        my @tag_ids = map{ $_->[1] =~ m/@(\d+)/ } @{$usertags_facet->{Options}};
        my $tags_by_installation_id = { map{ lc( $installation_id ).'@'.$_->id() => $_ }
                                            $stream2->factory('Usertag')->search({ id => { -in => \@tag_ids } })->all() };

        foreach my $tag_option ( @{$usertags_facet->{Options}} ){
            # Turn the display into actual tag names.
            if( my $tag_object = $tags_by_installation_id->{$tag_option->[1]} ){
                $tag_option->[0] = $tag_object->tag_name();
            } # Else this is a locally unknown tag, which should not happen.
        }
    }

    {
        # Resolve the advert IDs
        my $adverts = $self->get_adverts();
        my $advert_id_facet = List::Util::first { $_->{Name} eq 'broadbean_adcadvert_id' } @{ $self->results->facets() };
        if ( $advert_id_facet ) {
            $advert_id_facet->{Options} = $self->_advert_facet_options($advert_id_facet, $adverts);
        }

        my $resolved_advert_ids = $self->resolve_adcadvert_ids( $adverts );
        foreach my $candidate (@candidate_results) {
            $candidate->{broadbean_adcadvert_title} ||=
              $resolved_advert_ids->{ ($candidate->{broadbean_adcadvert_id} // '') }
              || 'No Advert found';
        }
    }

    $self->results_per_scrape( scalar(@candidate_results) );

    foreach my $candidate (@candidate_results) {
        $self->scrape_candidate($candidate);
    }

    $log->info('finished scraping candidate results');

    return $self->results();
}

sub _advert_facet_options {
    my ($self, $advert_id_facet, $adverts) = @_;

    my $options = $advert_id_facet->{Options};

    my $sorted_options = [];
    foreach my $advert ( @$adverts ) {
        my $option;

        # take the info from the facet and add the jobtitle,
        # otherwise build a new facet option from the retrieved adverts
        if ( my $facet_option = List::Util::first { $_->[0] eq $advert->{advert_id} } @$options ) {
            my $facet_advert = List::Util::first { $_->{advert_id} eq $facet_option->[0] } @$adverts;
            $facet_option->[0] = $facet_advert->{jobref} . ' - ' . $facet_advert->{jobtitle};
            $option = $facet_option;
        }
        else {

            # $advert (from advert API) was not found in BBCSS. Ergo it
            # has 0 responses. $advert->{responses} is an unreliable count.
            $option->[0] = $advert->{jobref} . ' - ' . $advert->{jobtitle};
            $option->[1] = $advert->{advert_id};
            $option->[2] = 0;
        }

        # display logic: determines which group to show the facet option under
        my $visibility = $advert->{visible} // '';
        $option->[3] = $visibility eq '1' ? 'default' : 'archived'; # default = live

        push @{$sorted_options}, $option;

    }

    # check to see if the company uses requisition ads and fetch that advert if they do
    if ( $self->requisition_ads_enabled() ) {
        my $requisition_ads = $self->get_adverts({ is_requisition_ad => 1 });
        foreach my $advert ( @$requisition_ads ) {
            push @{$sorted_options}, [
                $advert->{jobtitle},
                $advert->{advert_id},
                0,
                'general_submissions'
            ];
        }
    }

    return $sorted_options;
}

=head2 requisition_ads_enabled

Queries the juice api to get vlad settings and checks to see if requisition adverts
are enabled on the company. Determines whether we need to query the adverts api
to get requisition adverts.

=cut

sub requisition_ads_enabled {
    my ($self) = @_;
    my $cached_value = $self->get_account_value('has_requisition_ads');
    unless ($cached_value) {
        my $user = $self->user_object;
        my $configurations_api = $user->stream2->configurations_api;
        my $company_config = $configurations_api->get_company(company => $user->group_identity());

        my ($is_enabled) = grep {
            $_->{code} && $_->{code} eq 'enable_requisition_ads' && $_->{value} == 1
        } @{ $company_config->{vlad_settings} // [] };

        $self->set_account_value('has_requisition_ads', $is_enabled, 86400); # expire in one day
        return $is_enabled;
    }
    return $cached_value;
}

=head2 get_adverts

Returns adcourier adverts as an arrayref of minimal advert hashes
[{advert_id => 12345, jobtitle => 'blah'}, ...]

Note adverts will come through in a sorted order based on most recent,
see https://broadbean.atlassian.net/browse/SEAR-1587 for details.

Includes requisition ads only if specified in $where

Usage:

 my $adverts = $this->get_adverts();
 my $adverts = $this->get_adverts({ is_requisition_ad => 1 } );

=cut

sub get_adverts {
    my ($self, $where) = @_;

    # Maximum number of adverts we'll fetch
    my $max_ad_count = 5000;
    $where //= {};
    # Exclude requisition ads by default
    $where->{is_requisition_ad} //= 0;

    my $user = $self->user_object;
    my $s2 = $user->stream2;

    my %query = (
        company => $user->group_identity(),
        user_id => $user->provider_id(),
        sort    => '-time',
        %$where,
    );

    # Search in chunks of 500
    $query{limit}  = 500;
    $query{offset} = 0;

    # Request a max of 10 (5000 / 500) chunks
    $log->info('Fetching adverts');
    my @adverts;
    while($query{offset} < $max_ad_count) {
        $log->infof(
            'Fetching adverts %d to %d',
            $query{offset},
            $query{offset} + $query{limit}
        );
        #$log->debug("Advert search query:\n" . Data::Dump::pp(\%query));

        # Prevent the advert_api from modifying our original %query
        my @new_adverts = $s2->advert_api->search_adverts({%query});
        $log->debug('Fetched ' . @new_adverts . ' adverts');
        push @adverts, @new_adverts;

        # Exit loop unless there's potential for more
        last unless @new_adverts == $query{limit};
        # Offset for next chunk
        $query{offset} += $query{limit};
    }
    $log->info('Fetched a total of ' . @adverts . ' adverts');

    # Stub the ads down to specific properties
    # Only use (and cache) what we need
    my @desired_keys = qw(
        jobtitle
        advert_id
        jobref
        visible
        time
    );

    my @stubbed_ads;
    foreach my $advert (@adverts) {
        push @stubbed_ads, { map { $_ => $advert->{$_} } @desired_keys };
    }

    # dont do this unless its really necessary
    if ($log->is_debug) {
        $log->debug("Fetched Advert IDs\n" . join(', ', map { $_->{advert_id} } @stubbed_ads));
    }

    return \@stubbed_ads;
}

=head2 resolve_adcadvert_ids

Resolve the given adcourier advert IDs to a hash { advert_id => 'name' }.
Used to display the advert title on the candidate.

Usage:

 my $resolved_advert_ids = $this->resolve_adcadvert_ids( \@advert_ids );

=cut

sub resolve_adcadvert_ids {
    my ($self, $adverts) = @_;

    return { map { $_->{advert_id} => $_->{jobref} . ' - ' . $_->{jobtitle} } @$adverts };
}

sub search_submit {
    my ( $self ) = @_;
    # the second argument ( subroutine ) tells the agent to return the response without any post-processing ( i.e. json decoding )

    my $search_ref = $self->adcresponses_build_search_json();

    # we only want to facet on list type fields
    my %facet_fields = ( %standard_tokens, %{$self->adcresponses_custom_fields()} );

    # CandidateSearch returns only 10 items per facet. But for some facets we
    # want ALL items. It turns out that 500 is the largest number of items we
    # can request.
    my @full_item_facets = qw/
        broadbean_adcadvert_id
        broadbean_adcresponse_rank
        broadbean_adcresponse_usertags
    /;
    $search_ref->{facet} = [
        map {
            my $facet_name = $_->{Name};
            ( List::Util::any { $facet_name eq $_ } @full_item_facets )
                ? { name => $facet_name, count => 500 }
                : { name => $facet_name }
        } grep {
            $_->{Type} =~ m/(?:tag|advert|list)\z/i
        } values %facet_fields
    ];
    my $response = $self->bbcss_client()->search( $search_ref , sub { pop; } );

    unless ( $response->is_success ){
        return $self->determine_error( $response );
    }

    return $response;
}


sub adcresponses_build_search_json {
    my ( $self ) = @_;

    my $user = $self->user_object();
    my $s2 = $user->stream2();

    my %fields_to_return = ( map { $_ => 1 } $self->adcresponses_required_fields(), keys(%{$self->adcresponses_custom_fields()}) );

    my %fields = (
        %{$self->bbcss_build_search_json},
        fields_to_return => [ keys %fields_to_return , 'tsimport_id' ]
    );

    my @field_filters;

    # First and foremost, filter by broadbean_adcidentity_path
    # In the index, identity paths look like that:
    # "@cinzwonderland@APAC@Testing@bambi"
    {
        if( my $overseer = $self->original_user() ){
            $log->debug("Got original USER");
            unless( ( $overseer->contact_details()->{user_role} // '' ) eq 'SUPERADMIN' ){
                my $identity_path_filter = $overseer->oversees_hierarchical_path().'/*';
                #
                # Wundering why we swap '/' with '@'? Have a look at:
                # https://broadbean.atlassian.net/browse/SEAR-1515
                #
                $identity_path_filter =~ s|/|@|g;
                $log->debug("Will filter with broadbean_adcidentity_path:".$identity_path_filter);
                push @field_filters , {
                    field_name => 'broadbean_adcidentity_path',
                    operation => 'AND',
                    values => [{
                        value => $identity_path_filter,
                    }]
                };
            }else{
                $log->info("Overseer IS a superadmin. No identitypath filter was added");
            }
        }
        $log->info("Will use plain user id = ".$self->user_object()->provider_id()." for filtering");
        push @field_filters , {
            field_name => 'broadbean_adcuser_id',
            operation => 'AND',
            values => [{
                value => $self->user_object()->provider_id(),
            }]
        };
    }
    if( my $date_range = $self->adcresponses_cv_updated_range_string() ){
        push @field_filters , {
            field_name => 'broadbean_adcresponse_date',
            operation => 'AND',
            values => [{
                value => '['.$date_range.']'
            }]
        };
    }

    my $tokens_ref = $self->tokens();
    foreach my $field ( keys %$tokens_ref ){
        # all fields starting with adcresponses_ which haven't been accounted for yet
        # should be 'filter' fields

        next if ( exists( $fields{$field} ) );
        next if ( index( $field, 'adcresponses_' ) != 0 );

        my @values = $self->token_value( $field );

        # Only push some filters when there are some values
        if( scalar( @values ) ){
            push @field_filters , {
                field_name =>  substr( $field, length('adcresponses_') ),
                operation => 'OR',
                values => [ map{ +{ value => $_ } } @values ],
            };
        }
    }


    if( @field_filters ){
        $fields{filter_aggregates} //= {
            operation => 'AND',
            filters => [],
        };
        push @{ $fields{filter_aggregates}->{filters} } , @field_filters;
    }


    # Sorting!
    $fields{sort} = [ { sort_field => 'broadbean_adcresponse_date' ,
                        sort_direction => 'DESC',
                    }];

    return \%fields;
}

sub adcresponses_cv_updated_range_string {
    my ( $self ) = @_;
    my $cv_updated_within = $self->token_value( 'cv_updated_within' );

    if ( ! $cv_updated_within || ( $cv_updated_within eq 'ALL' ) ){
        return;
    }
    my $today = DateTime->now()->add( days => 1 );
    my $today_str = $today->iso8601.'.000Z';

    my $from_dt = {
        'TODAY'         => sub{ $today->subtract( days => 1 )->truncate( to => 'day' ) },
        'YESTERDAY'     => sub{ $today->subtract( days => 2 )->truncate( to => 'day' ) },
        '3D'            => sub{ $today->subtract( days => 3 )->truncate( to => 'day' ) },
        '1W'            => sub{ $today->subtract( weeks => 1 )->truncate( to => 'day' ) },
        '2W'            => sub{ $today->subtract( weeks => 2 )->truncate( to => 'day' ) },
        '1M'            => sub{ $today->subtract( months => 1 )->truncate( to => 'day' ) },
        '2M'            => sub{ $today->subtract( months => 2 )->truncate( to => 'day' ) },
        '3M'            => sub{ $today->subtract( months => 3 )->truncate( to => 'day' ) },
        '6M'            => sub{ $today->subtract( months => 6 )->truncate( to => 'day' ) },
        '1Y'            => sub{ $today->subtract( years => 1 )->truncate( to => 'month' ) },
        '2Y'            => sub{ $today->subtract( years => 2 )->truncate( to => 'month' ) },
        '3Y'            => sub{ $today->subtract( years => 3 )->truncate( to => 'month' ) },
    };
    return &{$from_dt->{$cv_updated_within}}()->iso8601().'.000Z'.' TO '.$today_str;
}

=head2 scrape_facets

Returned in the results are facets and also a summary of what we originally
searched for which is handy to prepopulate the SelectedValues field

=cut

my %board_id_to_nice_name;
sub scrape_facets {
    my ( $self ) = @_;
    my $result_ref = $self->response_to_JSON();
    my $user_object = $self->user_object();

    my $facets_ref = { map { $_->{name} => $_ } ( @{$result_ref->{data}->{facets}},
                                                  @{$result_ref->{data}->{queryFacets}} )
                   };
    my @facets = ();

    # build available options
    my $facet_schema_ref = { %standard_tokens, %{$self->adcresponses_custom_fields()} };
    while ( my ( $name, $attr_ref ) = each %{$facet_schema_ref} ){

        if ( $name eq 'onet_17' ){
            my %onet_map = $self->bbcss_onet_lookup( map { $_->{name} } @{$facets_ref->{$name}->{values}} );
            $attr_ref->{Options} = [
                map {
                    $onet_map{$_->{name}} ?
                        [ $onet_map{$_->{name}}, $_->{name}, $_->{count} ] :
                        ()
                } @{$facets_ref->{$name}->{values}}
            ];
        }
        elsif ( $name eq 'naics_code' ){
            my %naics_map = $self->bbcss_naics_lookup( map { $_->{name} } @{$facets_ref->{$name}->{values}} );
            $attr_ref->{Options} = [
                map {
                    $naics_map{$_->{name}} ?
                        [ $naics_map{$_->{name}}, $_->{name}, $_->{count} ] :
                        ()
                } @{$facets_ref->{$name}->{values}}
            ];
        }
        elsif ( $name eq 'country' ){
            $attr_ref->{Options} = [
                map {
                    [ Locale::Country::code2country($_->{name}), $_->{name}, $_->{count} ]
                } @{$facets_ref->{$name}->{values}}
            ];
        }
        elsif ( $name eq 'broadbean_adcboard_id' ) {
            $attr_ref->{Options} = [
                defined( $facets_ref->{$name} ) ?
                    ( map {
                        my $board_id   = $_->{name};
                        my $board_name = $self->bbcss_board_nice_name( $board_id );
                        $board_id_to_nice_name{ $board_id } = $board_name;
                        [ $board_name, $board_id, $_->{count} ];
                    } ( @{$facets_ref->{$name}->{values}} ) )
                : ()
            ];
        }
        elsif ( $name eq 'broadbean_adcresponse_is_read' ) {
            $attr_ref->{Options} = [
                defined( $facets_ref->{$name} ) ?
                    ( map {
                        [ lc $_->{name} eq 'true' ? 'Read' : 'Unread',
                        $_->{name},
                        $_->{count} ]
                    } ( @{$facets_ref->{$name}->{values}} ) )
                : ()
            ];
        }
        elsif ( $name eq 'broadbean_adcresponse_rank' ) {

            my %flag_map = $user_object->stream2->factory('AdcresponsesCustomFlag')->map_available_flags(
                $user_object->group_identity
            );

            $attr_ref->{Options} = [
                map {
                    [ $flag_map{ $_->{name} }->{description}, $_->{name}, $_->{count} ]
                } ( @{$facets_ref->{$name}->{values}} )
            ];
        }
        elsif ( $name eq 'broadbean_adcresponse_source_id' ) {
            $attr_ref->{Options} = [
                defined( $facets_ref->{$name} ) ?
                ( map {
                        [
                            $_->{name} == 1 ? $user_object->stream2->__('Applications')
                                : $user_object->stream2->__('Shortlisted from Search'), # 1 = Aplitrak, 3 = Stream
                            $_->{name},
                            $_->{count}
                        ]
                    } ( @{$facets_ref->{$name}->{values}} ) )
                : ()
            ];
        }
        else {
            $attr_ref->{Options} = [
                defined( $facets_ref->{$name} ) ?
                    ( map {
                        [ $_->{name}, $_->{name}, $_->{count} ]
                    } ( @{$facets_ref->{$name}->{values}} ) )
                : ()
            ];
        }

        # Transfer values for all the facets.
        if ($attr_ref->{'Type'} eq 'multilist' || $attr_ref->{'Type'} eq 'tag' || $attr_ref->{'Type'} eq 'groupedmultilist' || $attr_ref->{'Type'} eq 'advert') {
            # Transfer multiple values
            my @values = $self->get_token_value( $name );
            $attr_ref->{'SelectedValues'} = { map { $_ => 1 } @values };
        }else{
            # Transfer single values
            $attr_ref->{Value} = $self->get_token_value( $name );
        }

        if ( $attr_ref->{Type} eq 'list' && scalar( @{$attr_ref->{Options}} ) ){
            if( $attr_ref->{WithTotal} // 1 ){
                my $total = 0;
                $total += $_->[-1] for ( @{$attr_ref->{Options}} );
                unshift( $attr_ref->{Options}, [ 'Any', '', $total ] );
            } else {
                unshift( $attr_ref->{Options}, [ 'Any', '' ] );
            }
        }
    }

    $self->results()->facets( [ values %$facet_schema_ref ] );
}

sub scrape_candidate_details {
    my ( $self, $result_ref ) = @_;

    my %candidate_details =  (
        $self->SUPER::scrape_candidate_details( $result_ref ),

        name => $result_ref->{broadbean_adcresponse_name},
        broadbean_adcresponse_rank => int( $result_ref->{broadbean_adcresponse_rank} ),

        tags => $result_ref->{broadbean_adcresponse_tags},

        map {
            $_ => $result_ref->{$_}
        } qw/
            broadbean_group_identity
            broadbean_adcresponse_id
            broadbean_adcadvert_id
            broadbean_adcadvert_title
            broadbean_adcboard_id
            broadbean_adcuser_id
            broadbean_adcidentity_path
            broadbean_adcresponse_date
            broadbean_adcresponse_subject
            broadbean_adcresponse_attachement_cv
            broadbean_adcresponse_attachments
            broadbean_adcresponse_email_body
            broadbean_adcresponse_name
            broadbean_adcresponse_progressed
            broadbean_adcresponse_forwarded
            broadbean_adcresponse_source_id
            broadbean_adcresponse_shortlisted
            broadbean_adcresponse_updated
            broadbean_adcresponse_tagged_time
            broadbean_adcresponse_gender
            broadbean_adcresponse_is_read
            broadbean_group_adcresponse_id
            broadbean_adcresponse_usertags
            broadbean_adcadvert_is_requisition_ad
            broadbean_adcresponse_general_submission_sector
            broadbean_adcresponse_general_submission_location
            broadbean_adcresponse_general_submission_jobtype
            tsimport_id
            flagging_history
        /
    );
    # ^^^^^^ Note that broadbean_adcresponse_usertags is only kept for reference.

    {
        # Turn the usertags stored in the BBCSS into actual tags
        # in this installation.
        my $stream2 = $self->user_object()->stream2();

        my $installation_id = $stream2->installation_id();

        my $remote_usertags = $result_ref->{broadbean_adcresponse_usertags} // '';
        $log->debug("Installation ID = ".$installation_id);
        $log->debug("Remote broadbean_adcresponse_usertags = '".$remote_usertags."'");

        my @current_usertags = split(/\|/ , $remote_usertags );
        # Only consider those from this installation.
        @current_usertags = grep { $_ =~ /^$installation_id/i } @current_usertags;
        $log->debug("Local installation usertags = ".join(', ', @current_usertags) );

        my @tag_ids = map{ $_ =~ m/@(\d+)/ } @current_usertags;

        $log->info("Finding local installation tag IDs = ".join(', ', @tag_ids) );

        my @tags = $stream2->factory('Usertag')->search({ id => { -in => \@tag_ids } })->all();

        # Note that this data is for select2 component at the interface level.
        $candidate_details{usertags} =
            [ map{
                +{
                    id => $_->tag_name(),
                    text =>  $_->tag_name(),
                }
            } @tags
           ];
    }
    return %candidate_details;
}

=head2 set_usertags

Sets the 'broadbean_adcresponse_usertags' according to the
given collection of L<Stream2::O::Usertag> objects. ONLY in this
installation.

=cut

sub set_usertags{
    my ($self, $candidate, $usertags) = @_;
    my $installation_id = $self->user_object()->stream2()->installation_id();
    my @current_tags = split(/\|/, $candidate->{broadbean_adcresponse_usertags} // '' );

    # the new tags are the tags from another installation + those ones that we are setting.
    my @new_tags = grep{ $_ !~ /^$installation_id@/ } @current_tags;

    my $new_tags_string = join('|',(  @new_tags, map{ $installation_id.'@'.$_->id() } @$usertags ) );
    $log->info("New tags for BBCSS will be ".$new_tags_string);

    $self->update_candidate( $candidate , { broadbean_adcresponse_usertags => $new_tags_string } );
}



sub scrape_fixup_candidate {
    my ( $self, $candidate ) = @_;

    $self->SUPER::scrape_fixup_candidate( $candidate );

    # ArrayIFY the tags property
    my $tag_str = $candidate->attr('tags') // '';
    my @tags = grep { $_ } split( /\|/, $tag_str );
    $candidate->attr( tags => \@tags );

    $candidate->attr(
        'broadbean_adcboard_nicename',
        $board_id_to_nice_name{ $candidate->attr('broadbean_adcboard_id') } // ''
    );

    my $attachments_string = $candidate->attr('broadbean_adcresponse_attachments' ) // '';
    my @attachments = grep { $_ } split( /\|/ , $attachments_string );
    $candidate->attr('broadbean_adcresponse_attachments' => \@attachments );

    if ( ($candidate->attr('broadbean_adcuser_id') // '') eq $self->user_object->provider_id ){
        $candidate->attr(
            'advert_preview_url',
            sprintf('https://www.adcourier.com/view-vacancy.cgi?advert_id=%s', $candidate->attr('broadbean_adcadvert_id'))
        );
    }
    else {
        $candidate->attr(
            'advert_preview_url',
            sprintf(
                '/api/cheesesandwiches/adc_shortlist/%s/preview/%s',
                $candidate->attr('broadbean_adcadvert_id'),
                $candidate->attr('broadbean_adcboard_id')
            )
        );
    }

    # set the canonical values as the final step.
    $candidate->set_canonical_values(%canonical_attr_mappings);

}

# if the profile has been viewed then update broadbean_adcresponse_is_read as'true'
# then go about the usual business.
sub download_profile {
    my ($self, $candidate) = @_;
    my $results = $self->SUPER::download_profile( $candidate );

    # only send this update if the value of broadbean_adcresponse_is_read is
    # not equal to 'True', and we force correctness too, as true != True.
    my $is_read = $candidate->attr('broadbean_adcresponse_is_read');

    # It is only in the case the attachments are more than 1
    # that we consider to be other attachments.
    if( @{ $candidate->get_attr_arrayref( 'broadbean_adcresponse_attachments' ) } > 1 ){
        $candidate->has_other_attachments( 1 );
    }

    if( $candidate->get_attr('broadbean_adcresponse_email_body') ){
        # Get the email body via a call to the adcourier API
        my $stream2 = $self->user_object()->stream2();
        my $adcourier_api = $stream2->adcourier_api();
        my $response = eval {
            $adcourier_api->request_json(
                GET => $adcourier_api->url(
                    sprintf( '/%s/responses/%s',
                        $candidate->get_attr('broadbean_group_identity'),
                        $candidate->get_attr('broadbean_adcresponse_id')
                    ),
                    { include_files => 1 }
                )
            );
        };

        if ( $@ ) {
            $log->warn("Error while retrieving email body: $@");
            # add a warning, instead of dying, to complete the action.
            $candidate->add_warning(
                'Sorry! This candidate could not be fully processed. This could be because the candidate has not sent an actual CV document, or because the CV document was badly formatted.'
            );
            # return early to avoid marking the candidate as read.
            return $results;
        }
        else {
            if( $response->{email_body} ){
                my $email_body_source = MIME::Base64::decode( $response->{email_body} );
                my $email_body_html = '';
                # Yes you are seeing this right..
                if( $email_body_source =~ /^-txt-/ ){
                    $email_body_source =~ s/^-txt-//;
                    $email_body_html = '<pre>'.HTML::Entities::encode_entities( Encode::decode('UTF-8', $email_body_source ) ).'</pre>';
                }elsif( $email_body_source =~ /^-html-/ ){
                    $email_body_source =~ s/^-html-//;
                    $email_body_html = $self->generate_html_fragment( $email_body_source ) // 'No html could be parsed';
                }
                unless( $email_body_html ){
                    $log->warn("Could not extract or build some html from the candidate email body");
                }else{
                    # See /share/stream2_templates/stream/candidate_profiles/adcresponses.inc
                    # to see where and how this is rendered.
                    $candidate->attr( 'broadbean_adcresponse_email_body_html', $email_body_html );
                }
            }
        }
    }

    $self->update_candidate( $candidate, { broadbean_adcresponse_is_read => 'True' } )
        unless ($is_read && $is_read eq 'True');
    return $results;
}

# in adcresponses, when downloading the CV we consider the
# application as having been read.
sub download_cv {
    my ( $self, $candidate ) = @_;

    my $response = $self->SUPER::download_cv( $candidate );

    my $is_read = $candidate->attr('broadbean_adcresponse_is_read');
    unless ( $is_read && $is_read eq 'True' ){
        $self->update_candidate( $candidate, { broadbean_adcresponse_is_read => 'True' } )
    }

    return $response;
}

=head2 _candidate_attachment

A Stream2::O::File of the candidate's attachment.
Which attachment is specified by $attachment_key

Usage:

    my $file = $self->_candidate_attachment($candidate);

=cut

sub _candidate_attachment {
    my ($self, $candidate, $attachment_key) = @_;

    my $attachment_ref = $self->user_object->identity_provider()
        ->download_attachment( $self->user_object(), $attachment_key );

    unless( $attachment_ref ){
        $log->warn("Could not download binary content for $attachment_key");
        return;
    }

    my $content_type = 'application/octet-stream';

    my $filename = $attachment_ref->{filename};

    my $stream2 = $self->user_object()->stream2();
    my $type = $stream2->mime_types()->mimeTypeOf( $filename );
    if( $type ){
        $type = $type->type;
    } else {
        $type = 'application/octet-stream';
        $log->warnf(
            'Could not figure out MIME Type for %s Falling back to "%s"',
            $filename,
            $type,
        );
    }

    return Stream2::O::File->new({
        name           => $filename,
        mime_type      => $type,
        binary_content => $attachment_ref->{content}
    });
}

=head2

An arrayref of rendered candidate attachmentes for the given candidate

Usage:

    my $attachmets = $feed->rendered_attachments($candidate);

=cut

sub rendered_attachments {
    my ($self, $candidate) = @_;

    my $cvname = $candidate->get_attr('broadbean_adcresponse_attachement_cv');

    my @attachment_keys = @{ $candidate->get_attr_arrayref( 'broadbean_adcresponse_attachments' ) };

    my @rendered;
    foreach my $attachment_key (@attachment_keys) {
        next if $attachment_key eq $cvname;
        my $file = $self->_candidate_attachment($candidate, $attachment_key);

        if($file->mime_type eq 'application/pdf') {
            push @rendered, {
                render_type   => 'pdf',
                key           => $attachment_key,
                filename      => $file->name,
            };
        } else {
            push @rendered, {
                rendered_html => $file->html_render,
                key           => $attachment_key,
                filename      => $file->name,
            };
        }
    }

    return \@rendered;
}

sub download_attachment {
    my ($self, $candidate, $key) = @_;

    my $valid_key = grep {
        $_ eq $key;
    } @{ $candidate->get_attr_arrayref( 'broadbean_adcresponse_attachments' ) };
    
    $self->throw_known_internal('File not found') unless $valid_key;

    return $self->_candidate_attachment($candidate, $key)->to_download_response;
}

sub download_attachments {
    my ($self, $candidate) = @_;
    my $cvname = $candidate->get_attr('broadbean_adcresponse_attachement_cv');

    return [
        map {
            $self->download_attachment($candidate, $_);
        } grep {
            $_ ne $cvname;
        } @{ $candidate->get_attr_arrayref( 'broadbean_adcresponse_attachments' ) }
    ];
}

sub results_per_page {
    my $self = shift;

    #  return a specified number of results per page, if it exists
    return $self->token_value('results_per_page') ?
        $self->token_value('results_per_page') + 0  : 50;
}


1;

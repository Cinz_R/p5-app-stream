package Stream::Token;

use strict;

# has attributes
#  name
#  label
#  type
#  sortmetric
#  helpermetric
#  helper
#  default

# other attributes may be defined by token type
#  text: value, size, minlength?, maxlength?
#  list/multilist/check/radio: options, min select, max select
#  salary: currency,from,to,per
#  location: location, radius
#  date: epoch,day,month,year,days_ago,months_ago
#

use base qw(Class::Accessor::Fast);

__PACKAGE__->mk_accessors(qw(
    name
    label
    sortmetric
    helpermetric
    default
    _parameters
    example
    value
));

my %subclasses;

BEGIN {
    %subclasses = (
        'hidden'      => 'Stream::Token::Hidden',
        'text'        => 'Stream::Token::Text',
        'list'        => 'Stream::Token::List',
        'multilist'   => 'Stream::Token::MultiList',
        'advert'      => 'Stream::Token::MultiList',
        'location'    => 'Stream::Token::Location',
        'distancelist'=> 'Stream::Token::DistanceList',
        'salary'      => 'Stream::Token::Salary',
        'date'        => 'Stream::Token::Date',
        'test'        => 'Stream::Token::SyntaxError',
        'currency'    => 'Stream::Token::Currency',
        'salaryper'   => 'Stream::Token::SalaryPer',
        'salaryvalue' => 'Stream::Token::SalaryValue',
        'salarymultivalue' => 'Stream::Token::SalaryMultiValue',
    );
};

sub subclass_for {
    my $type = lc(shift);

    my $subclass = $subclasses{ $type };

    if ( !$subclass ) {
        die "Unknown token type: $type";
    }

    return $subclass;
}

sub new {
    my ($class, $options_ref) = (shift, shift);

    if ( @_ ) {
        die "too many arguments, expecting ${class}->new( \$hash_ref )";
    }

    if ( ref( $options_ref ) ) {
        # standardise the keys (lowercase them)
        my @lc_keys = map { lc($_) } keys %$options_ref;

        # store them in a new hash (otherwise we will infinite loop)
        my %new_options;
        @new_options{ @lc_keys } = values %$options_ref;

        $options_ref = \%new_options;
    }
    elsif ( $options_ref ) {
        $options_ref = parse_descriptor_line( $options_ref );
    }
    else {
        $options_ref = {};
    }

    $options_ref = standardise_descriptor( $options_ref );

    if ( defined(my $type = delete $options_ref->{ 'type' }) ) {
        # subclassing
        my $subclass = subclass_for( $type );
        unless ( eval "use $subclass; 1" ) {
            die "Unable to create $type token because $@";
        }

        my $self = $subclass->new( $options_ref );

        if ( !$self ) {
            die "$subclass returned undef instead of a $type token";
        }

        return $self;
    }

    my $self = $class->SUPER::new( $options_ref );

    $self->initialise( $options_ref );

    return $self;
}

sub initialise {
    my $self = shift;
    my $options_ref = shift or return $self;

    if ( defined( my $helper = delete $options_ref->{helper} ) ) {
        $self->helper( $helper );
    }

    @$self{ keys %$options_ref } = values %$options_ref;
}

sub type {
    return 'base';
}

sub convert_to {
    my $self = shift;
    my $new_token_type = shift;
    my $new_options_ref = shift;

    my $subclass = subclass_for( $new_token_type );

    unless ( eval "use $subclass; 1" ) {
        die "Unable to create $new_token_type token because $@";
    }

    bless $self, $subclass;

    $self->initialise( $new_options_ref );
}

sub helper {
    return $_[0]->{helper} if @_ == 1;

    my ($helper, @params) = parse_helper_string( $_[1] );
    $_[0]->parameters( @params ) if @params;
    return $_[0]->{helper} = $helper;
}

sub parse_helper_string {
    my $helper_string = shift or return ();
    $helper_string =~ s/^\s+|\s+$//g;
    my ( $helper, $param_string ) = split /\(/, $helper_string;

    $param_string =~ s/\)\s*$//;
    my @params = split /\|/, $param_string;

    return ($helper, @params);
}

sub parameters {
    my $return = shift->_parameters( @_ );

    return @$return if ref($return);
    return $return;
}

sub parse_descriptor_line {
    my $descriptor = shift or return {};

    # descriptor: %startdate%,Name=Start Date,Type=Field(30),SortMetric=60,Example=e.g. 25/12/2002 or ASAP,V4SortMetric=23

    # remove the descriptor tag
    $descriptor =~ s/^\s*descriptor:\s*//;
    my ( $token_name, @params ) = split /\s*,\s*/, $descriptor;

    my %options = map { my ( $type, $value ) = split /=/, $_, 2; lc($type) => $value }
        @params;

    # descriptor label comes from name in the descriptor line
    $options{label} = delete $options{name};

    $token_name =~ s/^\%//; # strip initial %
    $token_name =~ s/\%$//; # strip trailing %
    $options{name} = $token_name;

    return wantarray ? %options : \%options;
}

sub standardise_descriptor {
    my $options_ref = shift;

    if ( exists( $options_ref->{type} ) ) {
        my ($type, $options) = split /\(/, $options_ref->{type};

        if ( $options ) {
            $options =~ s/\)$//;
        }

        $type = lc( $type );

        if ( $type eq 'text' || $type eq 'field' ) {
            $options_ref->{type} = 'text';
            $options_ref->{size} = $options if $options;
        }
    }

    return $options_ref;
}

1;

# vim: expandtab shiftwidth=4

#!/usr/bin/perl
package Stream::Session;

use strict;

# TODO
# This is only used in a few places ( in feeds )
# Move this to Moose/DBIx code and instantiate in the task before passing to the Engine

# Account Variables
sub set {}
sub set_for {}
sub get {}
sub get_for {}

# Session variables are tied to this user's current session only
sub session_set {}
sub session_set_for {}
sub session_get {}
sub session_get_for {}

sub remote_addr {}

# important to keep on top of old data
sub clear_old_data {} 
sub clear_expired_session_data {}
sub clear_expired_account_data {}
sub _clear_expired_data {}

1;

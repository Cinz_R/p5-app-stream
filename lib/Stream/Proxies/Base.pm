#!/usr/bin/env perl

package Stream::Proxies::Base;

use strict;

sub new {
    my $class = shift;
    return bless {}, $class;
}

sub proxy_url {
    # subclass me
    if ( ref($_[0]) ) {
        return $_[0]->{proxy_url} if @_ == 1;
        return $_[0]->{proxy_url};
    }
    return;
}

1;

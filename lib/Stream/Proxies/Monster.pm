package Stream::Proxies::Monster;

use strict;
use base 'Stream::Proxies::Base';

use Carp;
use Log::Any qw/$log/;
use Stream2;

# monster is IP restricted, hopefully our UK proxy has access


sub proxy_url {
    my $self = shift;
    my $url  = shift;

    my $monster_ip_restricted = 0;
    if ( $url ) {
        if ( $url =~ m/rsx.monster.com/ ) {
            $monster_ip_restricted = 1;
        }
    }
    else {
        # assume we are trying to access an ip restricted site because we don't have a URL
        $monster_ip_restricted = 1;
    }

    # the rest of the servers have to go through a proxy
    if ( $monster_ip_restricted ) {
        my $proxy =  Stream2->instance()->feed_proxy('monster');
        $log->info("Will use proxy $proxy for URL $url");
        return $proxy;
    }

    return '';
}

1;

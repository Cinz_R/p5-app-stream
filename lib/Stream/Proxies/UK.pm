package Stream::Proxies::UK;

use strict;
use base qw(Stream::Proxies::Base);

use Carp;
use Stream2;

use Log::Any qw/$log/;

sub proxy_url {
    ## THIS WAS blade11.gs.
    ## It cannot work from the cloud.
    ## See story https://www.pivotaltracker.com/story/show/71306286
    # return 'http://46.254.116.43:8888'; # blade11.gs
    my $proxy = Stream2->instance()->feed_proxy('_general');
    $log->info("Will use proxy $proxy regardless of any URL");
    return $proxy;
}

1;

package Stream::Proxies::Indeed;

use strict;
use base 'Stream::Proxies::Base';

use Carp;
use Log::Any qw/$log/;
use Stream2;

=head2 proxy_url

Indeed retricts IP of CV urls like

http://www.indeed.com/r/29cf89cf08a097f9/pdf

See https://www.pivotaltracker.com/story/show/71828760

=cut

sub proxy_url {
    my $self = shift;
    my $url  = shift;

    if ( $url && ( $url =~ /\/r\// ) ) {
        my $proxy = Stream2->instance()->feed_proxy('indeed');
        $log->info("Will use proxy '$proxy' for '$url'");
        return $proxy;
    }
    return '';
}

1;

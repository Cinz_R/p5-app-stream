#!/usr/bin/perl

# vim: encoding=utf-8:tabstop=4:expandtab

# $Id: SharedUtils.pm 93282 2013-02-08 12:12:26Z andy $

package Stream::SharedUtils;

use strict;

BEGIN {
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

    $VERSION   = '0.01';
    @ISA       = qw(Exporter);
    @EXPORT    = (); # don't export anything by default
    @EXPORT_OK = qw(
        NOW find_username
        remote_addr current_user current_devuser current_lutoc current_session current_locale
        internal_request
        parts_to_username
        username_to_parts
        username_to_name
        username_to_email
        find_password
        sql_search_consultant sql_search_user
        hostname
        user_exists
        from_storable
        to_storable
    );
    %EXPORT_TAGS = ();
};

use Encode;


# For SQL::Abstract
sub NOW {
    my $_now = 'NOW()';
    return \$_now;
}

sub from_storable {
    if ( wantarray ) {
        return map { Encode::decode( 'UTF-8', $_  ) } @_;
    }

    return Encode::decode( 'UTF-8', $_[0] );
}

sub to_storable {
}

sub sql_search_consultant {
    my ($consultant,$team,$office,$company) = @_;

    if ( $consultant && !$company ) {
        # parse username
        ($consultant,$team,$office,$company) = lutoc( $consultant );
    }

    return _sql_search_permissions(
        'consultant',
        $consultant,
        $team,
        $office,
        $company,
    );
}

sub sql_search_user {
    my ($consultant,$team,$office,$company) = @_;

    if ( $consultant && !$company ) {
        # parse username
        ($consultant,$team,$office,$company) = lutoc( $consultant );
    }

    return _sql_search_permissions(
        'user',
        $consultant,
        $team,
        $office,
        $company,
    );
}

sub _sql_search_permissions {
    my $consultant_col = shift; # name of the consultant column in db
    my ($consultant, $team,$office,$company) = @_;

    # superadmin can see entire company
    if ( $office eq 'superadmin' && $consultant eq 'admin' ) {
        return;
    }

    # office admin can see their office
    if ( $consultant eq 'admin' ) {
        return ( office => $office );
    }

    # teamleaders can see their team
    if ( $consultant eq 'teamleader' ) {
        return (
            office => $office,
            team   => $team,
        );
    }

    # consultants can only see their own adverts
    return (
        $consultant_col => $consultant,
        team            => $team,
        office          => $office,
    );
}

sub big_debug {
    print "\n\n\n\n\n\n#######################\n";
    print join("\n", @_);
    print "\n####################\n\n\n\n\n";
}

sub find_username {
    my $hash_ref = shift;

    if ( exists( $hash_ref->{username} ) ) {
        return lutoc( $hash_ref->{username} );
    }

    # allow user as an alias for consultant
    my $consultant;
    if ( exists( $hash_ref->{user} ) ) {
        $consultant = $hash_ref->{user};
    }
    else {
        $consultant = $hash_ref->{consultant};
    }

    return ( $consultant, @$hash_ref{qw(team office company)} );
}

# not really meant to be used but I've duplicated it from
# Bean::UtilsShare because Bean::UtilsShare chdirs when you load it
sub lutoc {
    my ($user, $domain) = split '@', $_[0];
    my ($company, $office, $team ) = reverse split /\./, $domain;
    if ( $user ) {
        # make sure team is not undef
        $team ||= '';
    }
    return ( $user, $team, $office, $company );
}

sub company_walk_as {
    my $class = shift;
    my $options_ref = shift;

    my $callback_ref = delete $options_ref->{callback};
    my $username     = delete $options_ref->{username};

    my ( $consultant, $team, $office, $company ) = lutoc( $username );

    my %options = (
        company => $company,
    );

    if ( !($consultant eq 'admin' && $office eq 'superadmin') ) {
        $options{office} = $office;

        if ( $consultant ne 'admin' ) {
            $options{team} = $team;

            if ( $consultant ne 'teamleader' ) {
                die "expecting a username that is allowed to view other consultants not $consultant!";
            }
        }
    }

    return $class->company_walk({
        %options,
        callback => $callback_ref,
    });
}

sub company_walk {
    my $class = shift;
    my $options_ref = shift;

    my $callback_ref = delete $options_ref->{callback};
    my $company      = delete $options_ref->{company};
    my $only_office  = delete $options_ref->{office};
    my $only_team    = delete $options_ref->{team};

    if ( !$callback_ref ) {
        require Carp;
        Carp::croak( "Callback missing" );
    }

    my @offices;
    if ( $only_office ) {
        push @offices, $only_office;
    }
    else {
        # company level callback
        $callback_ref->(
            $company,
            '',
            '',
            '',
            $company,
        );

        require Bean::XMLDB::OfficeLib::ListOffices;
        @offices = eval {
            ListOffices::xmldb_listoffices( $company );
        };
    }

    require Bean::XMLDB::TeamLib::ListTeams;
    require Bean::XMLDB::UserLib::ListUsers;
    require Bean::XMLDB::UserLib::GetInfoForUser;

    OFFICE:
    foreach my $office ( sort @offices ) {
        if ( !($only_office && $only_team) ) {
            $callback_ref->(
                $company,
                $office,
                '',
                '',
                $office,
            );
        }

        my @teams;
        if ( $only_office && $only_team ) {
            @teams = $only_team;
        }
        else {
            @teams = eval {
                ListTeams::xmldb_listteams( $company, $office );
            };
        }

        if ( $@ ) {
            # ?!?!?!?!
            warn "Error listing teams in $office in $company: $@\n";
            next OFFICE;
        }

        TEAM:
        foreach my $team ( sort @teams ) {
            $callback_ref->(
                $company,
                $office,
                $team,
                '',
                $team,
            );

            my @users = eval {
                ListUsers::xmldb_listusers( $company, $office, $team );
            };

            if ( $@ ) {
                warn "Unable to list users in $team.$office.$company: $@\n";
                next TEAM;
            }

            # need to sort the users on their realname, not their username
            my %realnames;
            USER:
            foreach my $username ( @users ) {
                if ( $username =~ m/\A(?:quotamanager|teamleader)\@/i ) {
                    next USER;
                }

                my $user_details_ref = eval {
                    GetInfoForUser::xmldb_getinfo_user( $username );
                };

                $realnames{ $username } = from_storable($user_details_ref->{realname});
            }

            USER:
            foreach my $username ( sort { $realnames{ $a } cmp $realnames{ $b } } @users ) {
                my ( $user ) = ( split /\@/, $username )[0];

                if ( $user =~ m/\A(?:quotamanager|teamleader)\z/i ) {
                    next USER;
                }

                $callback_ref->(
                    $company,
                    $office,
                    $team,
                    $user,
                    $realnames{ $username },
                );
            }
        }
    }

    return 1;
}

sub allowed_to_manage {
    my $class = shift;
    my $current_username = shift; # person currently logged in
    my $sudo_username = shift;    # username we want to manage

    if ( !$current_username || !$sudo_username ) {
        # default - you cannot manage someone else
        return 0;
    }

    my ($consultant, $team, $office, $company) = lutoc( $current_username );
    my ($sudo_consultant, $sudo_team, $sudo_office, $sudo_company) = lutoc( $sudo_username );

    if ( $company ne $sudo_company ) {
        # no usernames can manage users in a different company
        return 0;
    }

    if ( $consultant eq 'admin' && $office eq 'superadmin' ) {
        # superadmin can view everyone in the company
        return 1;
    }

    if( $sudo_consultant eq 'bbgeneric' ){
#        #bbgeneric are available across the whole company
        return 1;
    }

    if ( $office ne $sudo_office ) {
        # only superadmin's can manage someone in a different office
        return 0;
    }

    if ( $consultant eq 'admin' && !$team ) {
        # admin can manage anyone in their office
        return 1;
    }

    if ( $team ne $sudo_team ) {
        # only admin & superadmin can manage someone in a different team
        return 0;
    }

    if ( $consultant eq 'teamleader' ) {
        # teamleaders can manage anyone in their team
        return 1;
    }

    # consultant logins can only manage themselves
    return $consultant eq $sudo_consultant;
}

sub remote_addr {
    # running under apache
    if ( exists( $ENV{'REMOTE_ADDR'} ) ) {
        return $ENV{'REMOTE_ADDR'} || '';
    }

    # running under Bean::Async
    if ( exists( $ENV{'ADC_REMOTE_ADDR'} ) ) {
        return $ENV{'ADC_REMOTE_ADDR'} || '';
    }
    
    if ( in_dancer() ) {
        local $@; # ignore any errors
        my $request = eval { Dancer::SharedData->request(); };
        if ( $request ) {
            return $request->remote_address();
        }
    }
}

sub internal_request {
    my $devuser = current_devuser();

    if ( $devuser ) {
        return 1;
    }

    my $remote_addr = remote_addr();

    if ( !$remote_addr ) {
        # offline scripts are treated as internal requests
        return 1;
    }

    if ( $remote_addr =~ m/^83\.223\.97\./ ) {
        return 1;
    }

    if ( $remote_addr =~ m/^213\.165\.2\./ ) {
        return 1;
    }

    if ( $remote_addr =~ m/^46\.254\.116\./ ) {
        return 1;
    }

    return 0;
}

sub current_user {
    # running under Bean::Async
    if ( exists( $ENV{'ADC_USERNAME'} ) ) {
        return $ENV{'ADC_USERNAME'} || '';
    }

    # let Bean::Internal do it for us
    if ( exists( $INC{'Bean/Internal.pm'} ) ) {
        return $Bean::Internal::ADC_USERNAME || '';
    }

    # CGI has been loaded
    if ( exists( $INC{'CGI.pm'} ) ) {
        return CGI::param('username') || CGI::cookie('username') || '';
    }

    # try and grab from cookie?
    return $ENV{'HTTP_COOKIE'} || '';
}

sub current_lutoc {
    my $username = current_user();

    if ( !$username ) {
        return ();
    }

    return lutoc( $username );
}

sub current_devuser {
    # running under Bean::Async
    if ( exists( $ENV{'ADC_DEVUSER'} ) ) {
        return $ENV{'ADC_DEVUSER'} || '';
    }

    # move to Bean::Internal
    if ( in_dancer() ) {
        local $@; # ignore any errors
        my $cookie = eval { Dancer::Cookies->cookies()->{devuser}; };
        if ( $cookie ) {
            my $value = $cookie->value; # call this as a scalar
            return $value;
        }
    }

    # let Bean::Internal do it for us
    if ( exists( $INC{'Bean/Internal.pm'} ) ) {
        return $Bean::Internal::DEVUSER || '';
    }
    # CGI has been loaded
    if ( exists( $INC{'CGI.pm'} ) ) {
        return CGI::cookie('devuser') || '';
    }

    # try and grab from cookie?
    return $ENV{'HTTP_COOKIE'} || '';
}

sub current_session {
    if ( exists( $ENV{ADC_SESSION} ) ) {
        return $ENV{ADC_SESSION};
    }

    return '';
}

sub current_locale {
    if ( exists( $INC{'Bean/Locale.pm'} ) ) {
        my $locale = Bean::Locale::lang();
        $locale =~ s/-/_/g;
        return $locale;
    }

    if ( exists( $ENV{ADC_LANG} ) ) {
        return $ENV{ADC_LANG};
    }

    return 'en';

}

# takes and adcourier username or the username components
# and returns the components
sub username_to_parts {
    my ($user,$team,$office,$company) = @_;
    if ( !$company ) {
        # this will probably change the current directory
        #require Bean::UtilsShare;
        ($user,$team,$office,$company) = lutoc( $user );
    }

    if ( $user eq 'admin' ) {
        if ( $office eq 'superadmin' ) {
            return ('','','',$company);
        }
        return ('','',$office,$company);
    }

    if ( $user eq 'teamleader' ) {
        return ('',$team,$office,$company);
    }

    return ($user || '',$team || '',$office || '',$company);
}

sub parts_to_username {
    my ($consultant,$team,$office,$company) = @_;

    if ( $consultant eq 'admin' ) {
        return sprintf('%s@%s.%s', $consultant, $office, $company);
    }

    return sprintf('%s@%s.%s.%s', $consultant, $team, $office, $company);
}

sub user_exists {
    my $username = shift;

    require Bean::XMLDB::UserLib::GetInfoForUser;
    my $user_details_ref = eval {
        GetInfoForUser::xmldb_getinfo_user( $username );
    };

    if ( $@ ) {
        return;
    }

    return 1;
}

sub username_to_name {
    my $username = shift;

    if ( $username !~ m/\@/ ) {
        my $consultant = $username;
        my $team       = shift;
        my $office     = shift;
        my $company    = shift;

        $username = parts_to_username( $consultant, $team, $office, $company );
    }

    require Bean::XMLDB::UserLib::GetInfoForUser;
    my $user_details_ref = eval {
        GetInfoForUser::xmldb_getinfo_user( $username );
    };

    if ( $@ ) {
        warn "$username not found: $@";
        return;
    }

    return from_storable($user_details_ref->{realname});
}

sub username_to_email {
    my $username = shift;

    if ( $username !~ m/\@/ ) {
        my $consultant = $username;
        my $team       = shift;
        my $office     = shift;
        my $company    = shift;

        $username = parts_to_username( $consultant, $team, $office, $company );
    }

    require Bean::XMLDB::UserLib::GetInfoForUser;
    my $user_details_ref = eval {
        GetInfoForUser::xmldb_getinfo_user( $username );
    };

    if ( $@ ) {
        warn "$username not found: $@";
        return;
    }

    return $user_details_ref->{'email'};
}

sub find_password {
    my $username = shift;

    if ( $username =~ m/^admin\@/ ) {
        return _admin_password( $username );
    }

    return _consultant_password( $username );
}

sub _consultant_password {
    my $username = shift;;

    require Bean::XMLDB::UserLib::GetInfoForUser;
    my $user_ref = GetInfoForUser::xmldb_getinfo_user( $username );

    return $user_ref->{password};
}

sub _admin_password {
    my $username = shift;

    # need to get office admin password
    my ($user,$team,$office,$company) = lutoc( $username );

    require Bean::XMLDB::OfficeLib::GetInfoForOffice;
    my $office_ref = GetInfoForOffice::xmldb_getinfo_office(
        $company,
        $office,
    );

    if ( !$office_ref ) {
        return die 'Unable to find office '. $office;
    }

    return $office_ref->{password};
}

my $_SERVER;
sub hostname {
    if ( !$_SERVER ) {
        require Sys::Hostname;
        $_SERVER = standardise_server(Sys::Hostname::hostname());
    }

    return $_SERVER;
}

sub cluster_id {
    my $hostname = Stream::SharedUtils::hostname();

    if ( $hostname =~ m/cheerleader.gs/ ) {
        return 2;
    }

    if ( $hostname =~ m/blade10.gs/ ) {
        return 3;
    }

    return 1;
}

sub standardise_server {
    my $server = lc( shift );
    $server =~ s/\.broadbean\..*$//;
    return $server;
}

sub test_cluster {
    if ( cluster_id() == 2 ) {
        return 1;
    }

    return 0;
}

sub download_link {
    my $class = shift;
    my $username = shift;
    my $link  = shift;

    require Bean::UserAgent;
    my $www = Bean::UserAgent->new();
    $www->login_as( $username );

#    my $session = current_session();
#    if ( $session ) {
#        $www->adc_session( $session );
#    }

    my $resp = $www->get( $link );

    if ( $resp->is_success() ) {
        my $filename = $resp->filename();

        if ( $filename =~ m/\.cgi|\.pl/ ) {
            die "unable to download <$link>: " . $resp->as_string();
        }

        return ( $resp->filename(), $resp->decoded_content() );
    }

    die "Unable to download [$link] - " .$resp->status_line();
}

sub evaluate_template {
    my $class      = shift;
    my $template   = shift;
    my $tokens_ref = shift || {};

    my $body = $template;

    # replace [XXXX] with $tokens_ref->{XXXX}
    $body =~ s/\[(\w+)\]/$tokens_ref->{$1}/g;

    return $body;
}

sub pattern_to_epoch {
    my $self = shift;
    my $pattern = shift;
    my $date_string = shift;

    if ( !$date_string ) {
        return;
    }

    require DateTime::Format::Strptime;
    my $strp = eval {
        DateTime::Format::Strptime->new( pattern => $pattern );
    };

    if ( $@ || !$strp ) {
        return;
    }

    my $dt = eval {
        $strp->parse_datetime( $date_string );
    };

    if ( $@ || !$dt ) {
        return;
    }

    return $dt->epoch();
}

sub in_dancer {
    return exists( $INC{'Dancer.pm'} );
}

sub render_template { #Lets you render templates from ~/templates in dancer
    my $template = shift;
    my %tokens = @_; 

    my @include_paths = ('/projects/andy/adcourier.broadbean/templates', '/data0/www/cvsearch.adcourier/tt', '/data0/www/adcourier.broadbean/templates' );

    require Template;

    my $tpl = Template->new({
        INCLUDE_PATH => \@include_paths,
    });

    require Bean::Locale;
    $tokens{'L'} = \&Bean::Locale::localise;

    my $output;
    if ( !$tpl->process( $template, \%tokens, \$output ) ) {
       return 'template error: '. $tpl->error;
    }

    return $output;
}

1;

__END__
requires "DateTime::Format::Strptime";
requires "Bean::UserAgent";
requires "Bean::XMLDB::OfficeLib::ListOffices";
requires "Bean::XMLDB::TeamLib::ListTeams";
requires "Bean::XMLDB::UserLib::ListUsers";
requires "Bean::XMLDB::OfficeLib::GetInfoForOffice";
requires "Bean::XMLDB::UserLib::GetInfoForUser";

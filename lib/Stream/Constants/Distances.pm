#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: Currencies.pm 30440 2010-02-23 13:54:08Z andy $

package Stream::Constants::Distances;

use utf8;

use strict;

use Stream::SharedUtils;

use base qw(Exporter);

our @EXPORT_OK = qw(&distance_unit &distance_units);
our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
);

my (%DISTANCE_UNIT_PER_LOCALE);
BEGIN {
    %DISTANCE_UNIT_PER_LOCALE = (
        'en_au'  => 'km',
        'da'     => 'km',
        'de'     => 'km',
        'dk'     => 'km',
        'en'     => 'miles',
        'en_us'  => 'miles',
        'en_usa' => 'miles',
        'es'     => 'km',
        'fr'     => 'km',
        'fr_CH'  => 'km',
        'it'     => 'km',
        'nl'     => 'km',
        'sv'     => 'km',
        'zh'     => 'km',
    );
};

sub distance_unit {
    my $locale = Stream::SharedUtils::current_locale();

    my $distance_unit = exists( $DISTANCE_UNIT_PER_LOCALE{ $locale } ) ? $DISTANCE_UNIT_PER_LOCALE{ $locale } : $DISTANCE_UNIT_PER_LOCALE{'en'};

    return $distance_unit;
}

sub distance_units {
    return qw/miles km/;
}

1;

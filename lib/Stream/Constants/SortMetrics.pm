#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: SortMetrics.pm 19466 2009-05-15 16:21:45Z andy $

package Stream::Constants::SortMetrics;

use strict;

use base qw(Exporter);

our @EXPORT_OK = qw(
    $SM_KEYWORDS  $SM_JOBTYPE  $SM_EXPERIENCE
    $SM_LOCATION  $SM_SALARY   $SM_INDUSTRY
    $SM_EDUCATION $SM_LANGUAGE $SM_ELIGIBILITY
    $SM_DATE      $SM_SEARCH   $SM_SORT);
our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
);

our $SM_KEYWORDS    = 1;
our $SM_LOCATION    = 10;
our $SM_SALARY      = 30;
our $SM_INDUSTRY    = 70;
our $SM_JOBTYPE     = 100;
our $SM_EXPERIENCE  = 120;
our $SM_EDUCATION   = 140;
our $SM_LANGUAGE    = 190;
our $SM_ELIGIBILITY = 220;
our $SM_DATE        = 500;
our $SM_SEARCH      = 510;
our $SM_SORT        = 530;

1;

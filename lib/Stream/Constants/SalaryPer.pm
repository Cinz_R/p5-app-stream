#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: SalaryPer.pm 19466 2009-05-15 16:21:45Z andy $

package Stream::Constants::SalaryPer;

use strict;

use base qw(Exporter);

our @EXPORT_OK = qw(&salary_per_hash &salary_per_order);
our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
);

my (%SALARY_PER, @SALARY_PER_ORDER);
BEGIN {
    %SALARY_PER = (
        'annum' => 'annum',
        'month' => 'month',
        'week'  => 'week',
        'day'   => 'day',
        'hour'  => 'hour',
    );

    @SALARY_PER_ORDER = qw(
        annum month week day hour
    );
};

sub salary_per_order {
    return @SALARY_PER_ORDER;
}

sub salary_per_hash {
    return %SALARY_PER;
}

1;
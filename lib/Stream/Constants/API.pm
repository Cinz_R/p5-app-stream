#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: API.pm 28406 2010-01-12 17:57:36Z andy $

package Stream::Constants::API;

use utf8;

use strict;

use base qw(Exporter);

our @EXPORT_OK = qw($MAX_OFFSET $MAX_PER_PAGE $VERSION_STRING $VERSION $REVISION);
our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
);

our ($REVISION)   = ('$Id: API.pm 28406 2010-01-12 17:57:36Z andy $' =~ m/\s(\d+)\s/ );
our ($VERSION)      = '1.0.0';

# http://semver.org/
our ($VERSION_STRING)    = sprintf '%s-r%s', $VERSION, $REVISION;
our $MAX_OFFSET   = 9999;
our $MAX_PER_PAGE = 100;

1;

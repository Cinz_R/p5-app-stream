#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: Currencies.pm 63143 2011-11-03 18:27:57Z andy $

package Stream::Constants::Currencies;

use utf8;

use strict;

use Stream::SharedUtils;
use List::MoreUtils qw(uniq);

use base qw(Exporter);

our @EXPORT_OK = qw(&currency_order &currency_hash);
our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
);

my (%CURRENCIES);
my (%CURRENCIES_PER_LOCALE);
BEGIN {
    # Contains all supported currencies
    %CURRENCIES = (
        'AED' => 'AED',
        'AUD' => 'AUD',
        'CAD' => 'CAD',
        'CHF' => 'CHF',
        'CNY' => 'CNY',
        'DKK' => 'DKK',
        'EUR' => '€',
        'GBP' => '£',
        'HKD' => 'HKD', # hong kong dollars
        'JPY' => 'JPY', # yen
        'MYR' => 'MYR',
        'NOK' => 'NOK',
        'NZD' => 'NZD', # new zealand dollars
        'SEK' => 'SEK',
        'SGD' => 'SGD', # singapore dollars
        'RMB' => 'RMB',
        'USD' => '$',
        'ZAR' => 'ZAR',
    );

    %CURRENCIES_PER_LOCALE = (
        'da'     => [ qw/DKK EUR/ ],
        'de'     => [ qw/EUR GBP USD/ ],
        'dk'     => [ qw/DKK EUR/ ],
        'en'     => [ qw/GBP USD EUR/ ],
        'en_us'  => [ qw/USD GBP EUR/ ],
        'en_usa' => [ qw/USD GBP EUR/ ],
        'en_au'  => [ qw/AUD USD GBP/ ],
        'es'     => [ qw/EUR GBP USD/ ],
        'fr'     => [ qw/EUR GBP USD/ ],
        'fr_ch'  => [ qw/CHF EUR/ ],
        'it'     => [ qw/EUR GBP USD/ ],
        'nl'     => [ qw/EUR GBP USD/ ],
        'sv'     => [ qw/SEK EUR/ ],
        'zh'     => [ qw/CNY/ ],
    );
};

sub currency_order {
    my $options_ref = shift || {};
    my $company = delete $options_ref->{company};
    if ( !$company ) {
        my $username = delete $options_ref->{username} || Stream::SharedUtils::current_user();
        (undef,undef,undef,$company) = Stream::SharedUtils::lutoc( $username );
    }

    my $locale = delete $options_ref->{locale} || Stream::SharedUtils::current_locale();
    $locale = lc($locale);

    my (@locale_currencies) = currencies_for_locale( $locale );
    my (@company_currencies) = currencies_for_company( $company );

    return uniq( @locale_currencies, @company_currencies );
}

sub currencies_for_locale {
    my $locale = shift;

    my $currencies_ref = exists( $CURRENCIES_PER_LOCALE{ $locale } ) ? $CURRENCIES_PER_LOCALE{ $locale } : $CURRENCIES_PER_LOCALE{'en'};

    return @$currencies_ref;
}

sub currencies_for_company {
    my $company = shift or return;

    require Bean::Company;
    my $comp = Bean::Company->new_from_name( $company )
        or return;
    my @currencies = grep { $_ } split /,/, $comp->salary_keys();
    return @currencies;
}

sub currency_hash {
    return %CURRENCIES;
}

1;

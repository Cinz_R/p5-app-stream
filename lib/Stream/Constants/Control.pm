#!/usr/bin/perl

# vim: expandtab:tabstop=4:encoding=utf-8

# $Id: SortMetrics.pm 19466 2009-05-15 16:21:45Z andy $

package Stream::Constants::Control;

use strict;

use base qw(Exporter);

our @EXPORT_OK = qw(
    $SM_OK
    $SM_WARN
    $SM_FAIL
);

our %EXPORT_TAGS = (
    all => \@EXPORT_OK,
);

our $SM_OK          = 1;
our $SM_WARN        = 2;
our $SM_FAIL        = 3;

1;

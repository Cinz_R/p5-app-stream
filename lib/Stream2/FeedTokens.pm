package Stream2::FeedTokens;

use Moose;
use Stream::TemplateLoader;
use List::Util 'first';
use Stream::Constants::SortMetrics qw($SM_SORT);
use Stream2::Translations::Templates;

has 'board'   => ( is => 'ro', isa => 'Str', required => 1 );
has 'locale'  => ( is => 'ro', isa => 'Str', default => 'en' );

has 'tokens' => (
  is            => 'ro',
  isa           => 'ArrayRef',
  lazy_build    => 1
);

has 'auth_tokens' => (
  is            => 'ro',
  isa           => 'ArrayRef',
  lazy_build    => 1
);

has 'translations' => (
  is            => 'ro',
  isa           => 'Stream2::Translations',
  lazy_build    => 1
);

sub _build_tokens {
  my $self = shift;
  return [ $self->_translate_tokens(
    Stream::TemplateLoader->list_tokens($self->board)
  ) ];
}
sub _build_auth_tokens {
  my $self = shift;
  return [ $self->_translate_tokens(
    Stream::TemplateLoader->list_authtokens($self->board)
  ) ];
}

sub _translate_tokens {
  my ( $self, @tokens ) = @_;

  # template/feed translations exist largely in the old translation system for the time being.
  # that will have to change at some point
  if ( $self->locale ne 'en' ){
    $self->translations->in_language( $self->locale, sub {

      foreach my $token ( @tokens ) {
        $token->{'Label'} = $self->translations->__( $token->{'Label'} );
        if ( my $options = $token->{'Options'} ){
          foreach my $option ( @{$options} ){
            ## NOTE: This is NOT implement for options that have sub options for now.
            ## This is a quick fix to carry on the release.
            unless( ( ref($option) // '' ) eq 'ARRAY' ){ next; }
            $option->[0] = $self->translations->__( $option->[0] );
          }
        }
      }

    });
  }

  return @tokens;
}

sub sort_by_token {
  my $self = shift;
  return first { $_->{Type} eq 'sort' || $_->{SortMetric} eq $SM_SORT } @{$self->tokens()};
}

sub merge_criteria {
  my ($self, $criteria) = @_;

  foreach my $token ( @{$self->tokens()} ) {
    my (@values) = $criteria->get_token($token->{Id});

    if ($token->{'Type'} eq 'multilist' || $token->{'Type'} eq 'tag' || $token->{'Type'} eq 'groupedmultilist') {
      my %selected_values = map { $_ => 1 } @values;
      $token->{'SelectedValues'} = \%selected_values;
    }
    elsif ( scalar(@values) ) {
      $token->{'Value'} = $values[0];
    }
  }

  return $self;
}

sub _build_translations {
    return Stream2::Translations::Templates->new();
}

__PACKAGE__->meta->make_immutable;

package Stream2::Lisp::Evaler;

use Moose;
use Log::Any qw/$log/;

use Language::LispPerl::Reader;
use Language::LispPerl::Evaler;

has 'stream2' => ( is => 'ro', isa => 'Stream2', required => 1 );

=head2 parse

Parse the given lisp string and returns the parsed L<Language::LispPerl::Seq>

=cut

sub parse{
    my ($self, $string) = @_;
    my $reader = Language::LispPerl::Reader->new();
    $reader->read_string( $string );
    return $reader->ast();
}

=head2 head2 lookup_ast_by_symbol

Looks up the first AST (abstract syntax tree), which is a L<Language::LispPerl::Seq>
by its first symbol. Use that to extract some data from lisp source code.

Usage:

  if(   my $ast = $this->lookup_ast_by_symbol('( s2#do-stuff "bla")', 's2#do-stuff' ) ){
     $ast->second()->value(); # is 'bla';
  }

Returns undef if none is found.

=cut

sub lookup_ast_by_symbol{
    my ($self, $lisp_source, $symbol) = @_;
    my @asts = ( $self->parse( $lisp_source ) );
    while(my $ast = shift @asts ){
        if( $ast->class() eq 'Seq' ){
            my $first = $ast->first();
            # Does this first symbol match what we are looking for?
            if(  ( $first->type() eq 'symbol' )  &&
                     ( $first->value() eq $symbol ) ){
                return $ast;
            }
            $ast->each(sub{ push @asts , $_[0]; });
        }
    }
    return undef
}


=head2 get_evaler

Returns a pure L<Language::LispPerl::Evaler> (but set with some stream2 defaults).

=cut

sub get_evaler{
    my ($self) = @_;
    my $lisp = Language::LispPerl::Evaler->new();
    # We want the core.
    $lisp->load("core");
    # And the Stream2 native functions.
    # This will load Stream2::Lisp::Core and map its
    # functions to lisp.
    $lisp->load( $self->stream2()->shared_directory() . '/lisp/s2.clp' );
    $lisp->builtins()->apply_role('Stream2::Lisp::SecureBuiltIn');
    return $lisp;
}

sub eval{
    my ($self, $string) = @_;
    $log->warn('This is deprecated. Use my $l =  $self->get_evaler(); $l->eval(...) instead.');
    return $self->get_evaler()->eval( $string );
}

__PACKAGE__->meta->make_immutable();
1;

package Stream2::Lisp::Core;

use strict;
use warnings;

use Stream2;

use Carp;
use Class::Load;
use Language::LispPerl;
use HTTP::Request::Common;
use HTTP::Response;
use URI;
use MIME::Base64 qw();

use Log::Any qw/$log/;

use Data::Dumper;

use Stream2::Action::MessageCandidate;

=head1 NAME

Stream2::Lisp::Core - Native Stream2 functions for lisp

=cut

sub about{
    return "This is ".__PACKAGE__." with built-in pure functions.";
}

=head2 do_later

Do the given code in the given number of minutes.

Return the id of the created schedule longstep. If the action context contains
a candidate, it also executes (and records) a 'future' action against the candidate, so
people can cancel that at any time before it actually happens.

=cut

sub do_later{
    my ($minutes, $code) = @_;
    $minutes *= 1;
    my $stream2 = Stream2->instance();
    my $context = $stream2->context()->{macro_context};
    my $frozen_context = $context->freeze_to_b64();
    my $stuff = sub{
        my $process =
            $stream2->longsteps()->instantiate_process('Stream2::Process::RunLispLater',
                                                       {
                                                           stream2 => $stream2
                                                       },
                                                       {
                                                           lisp_code => $code,
                                                           macro_context => $frozen_context,
                                                           in_minutes => $minutes,
                                                       }
                                                   );
        # Figure out if there is a candidate in the action context.
        my $action_context   = $context->action_context();
        my $container = $context->candidate_container();
        my $candidate = eval{ $container->candidate_object(); };
        if( my $err = $@ ){
            $log->warn("Could not find candidate in action_context: $err");
        }else{
            my $template_ast = $stream2->lisp()->lookup_ast_by_symbol( $code, 's2#send-email-template' );
            my $template_id;
            if( $template_ast ){
                $template_id = $template_ast->second()->value();
            }
            # Ok a candidate is there in the action context. We can
            # perform the action 'CandidateFuture'
            _do_action(
                ( $template_id ? 'Stream2::Action::CandidateFutureMessage' : 'Stream2::Action::CandidateFuture' ),
                {
                    longstep_process_id => $process->id(),
                    lisp_code => $code,
                    at_time => DateTime->now()->add( minutes => $minutes )->iso8601().'Z',
                    ( $template_id ? ( message_template_id => $template_id ) : () )
                }
            );
        }
        return $process;
    };

    # Note this is the longstep process ID that is returned here.
    return $stream2->stream_schema->txn_do( $stuff )->id();
}

=head2 user_in_company

Is the current context user in the given company?

Returns a Raw lisp Atom.

=cut

sub user_in_company{
    my ($company) = @_;
    my $stream2 = Stream2->instance();
    my $user = $stream2->context()->{macro_context}->user();
    unless( $user ){
        $log->warn("No current user. Cannot test for user in company $company");
        return Language::LispPerl->nil()
    }

    if (  $user->group_identity() eq $company ){
        return Language::LispPerl->true();
    }else{
        return Language::LispPerl->false();
    }
}

=head2 user_in_office

Same as user_in_company, but for office.

=cut

sub user_in_office{
    my ($test_office) = @_;
    my $stream2 = Stream2->instance();
    my $user = $stream2->context()->{macro_context}->user();
    unless( $user ){
        $log->warn("No current user. Cannot test for user in office $test_office");
        return Language::LispPerl->nil() ;
    }
    unless( $user->provider() eq 'adcourier' ){
        $log->warn("Use is not an adcourier user. Cannot test for office $test_office");
        return Language::LispPerl->nil();
    }

    my $full_path = $user->hierarchical_path();
    my ( $void , $company, $office , @rest ) = split( '/' , $full_path );

    $log->info("User office: $office. Test office: $test_office");
    if( $office eq $test_office ){
        return Language::LispPerl->true();
    }else{
        return Language::LispPerl->false();
    }
}

sub user_in_team{
    my ($test_team) = @_;
    my $stream2 = Stream2->instance();
    my $user = $stream2->context()->{macro_context}->user();
    unless( $user ){
        $log->warn("No current user. Cannot test for user in team $test_team");
        return Language::LispPerl->nil() ;
    }
    unless( $user->provider() eq 'adcourier' ){
        $log->warn("Use is not an adcourier user. Cannot test for team $test_team");
        return Language::LispPerl->nil();
    }

    my $full_path = $user->hierarchical_path();
    my ( $void , $company, $office , $team, @rest ) = split( '/' , $full_path );

    if( $team eq $test_team ){
        return Language::LispPerl->true();
    }else{
        return Language::LispPerl->false();
    }
}

=head2 candidate_is_updated_with_value

Has the current candidate given property been updated with the given value?

True or False

=cut

sub candidate_is_updated_with_value{
    my ($property, $value) = @_;
    my $stream2   = Stream2->instance();

    my $action_context   = $stream2->context()->{macro_context}->action_context();
    unless( $action_context->{fields} ){
        $log->debug("No 'fields' in action context. Most likely not an UpdateCandidate action");
        return Language::LispPerl->false();
    }
    unless( exists $action_context->{fields}->{$property} ){
        $log->info("Field $property NOT being updated");
        return Language::LispPerl->false();
    }
    if( $action_context->{fields}->{$property} ~~ $value  ){
        $log->info("Field $property is being updated with $value");
        return Language::LispPerl->true();
    }
    $log->info("Field $property is NOT being updated with $value");
    return Language::LispPerl->false();
}

=head2 candidate_in_destination

Is the current candidate in the given destination?

True or False.

=cut

sub candidate_in_destination{
    my ($destination) = @_;
    my $stream2   = Stream2->instance();
    my $action_context   = $stream2->context()->{macro_context}->action_context();
    my $container = $stream2->context()->{macro_context}->candidate_container();
    my $candidate = $container->candidate_object();

    if( $candidate->destination() eq $destination ){
        $log->debug("Candidate IS in destination '$destination'");
        return Language::LispPerl->true();
    }else{
        $log->debug("Candidate IS NOT in destination '$destination'");
        return Language::LispPerl->false();
    }
}

=head2 send_email_template_candidate

Sent the given email template ID to the current candidate.

Returns true on success.

Dies on error

=cut

sub send_email_template_candidate{
    my ($template_id) = @_;
    my $stream2   = Stream2->instance();
    my $action_context   = $stream2->context()->{macro_context}->action_context();
    my $container = $stream2->context()->{macro_context}->candidate_container();
    my $user      = $container->user_object();
    my $candidate = $container->candidate_object();

    my $email_template = $stream2->factory('EmailTemplate')->search({ group_identity => $user->group_identity(),
                                                                      id => $template_id })->first();
    unless( $email_template ){
        die "Cannot find Email Template ID = $template_id\n";
    }

    unless( $candidate->email() ){
        die "Candidate does not have an email address. Cannot message\n";
    }

    unless( $user->settings_values_hash()->{groupsettings}->{ $email_template->setting_mnemonic() } ){
        die "This template $template_id use is restricted for this user\n";
    }

    $log->infof("Will send email template %s to %s", $email_template->id(), $candidate->email());

    my $jobid = $stream2->uuid()->create_str();
    my $action = Stream2::Action::MessageCandidate->new({ jobid => $jobid,
                                                          stream2 => $stream2,
                                                          # If we are already in a log capturing environment,
                                                          # do not manage the log capturing here.
                                                          log_chunk_capture => Log::Log4perl::MDC->get('chunk') ? 0 : 1 ,
                                                          %{ $action_context },
                                                          message_email   => $email_template->mime_entity_object->one_header_value('Reply-To') || $user->contact_email(), ## The sender of the message.
                                                          subject_prefix  => $user->subject_prefix_for( $candidate->destination() ),
                                                          message_subject => $email_template->mime_entity_object->one_header_value('Subject'),
                                                          message_bcc     => $email_template->mime_entity_object()->one_header_value('Bcc') // '',
                                                          message_content => $email_template->one_html_body(),
                                                          email_template_id => $template_id,
                                                      });
    $action->instance_perform();
    return Language::LispPerl->true();
}

=head2 send_candidate

Sends the candidate xml to the given url.

True: Action was successful.

Goes bang in any other case.

=cut

sub send_candidate {
    my ($url)     = @_;
    my $stream2   = Stream2->instance();
    my $action_context = $stream2->context()->{macro_context}->action_context();
    my $container = $stream2->context()->{macro_context}->candidate_container();

    my $user      = $container->user_object();
    my $candidate = $container->candidate_object();
    my $advert_id = $action_context->{advert_id};

    unless ($url) {
        $log->warn("No URL provided. A URL is required");
        die 'No URL provided';
    }
    unless ( URI->new($url)->has_recognized_scheme ) {
        die 'Invalid URL';
    }

    my $advert;
    if ($advert_id) {
        $advert = $user->login_provider->find_advert( $action_context->{original_user} || $user, $advert_id, { list_name => $action_context->{list_name} } );
    }

    my $cv_response = HTTP::Response->parse( 
        MIME::Base64::decode_base64( _do_action('Stream2::Action::DownloadCV') )
    );

    my $cv_hash = {
        cv_filename => $cv_response->filename,
        cv_mimetype => $cv_response->content_type,
        cv_content  => MIME::Base64::encode_base64($cv_response->content),
    };

    my $cv_bin = $cv_response->content();

    # We ALWAYS want to CV parse.
    $log->info("CV Has not been parsed before. Parsing it now");
    my $cv_parsing_res = $user->parse_cv_with_locale($cv_bin);
    unless( $cv_parsing_res ){
        die "Parsing CV failed. Cannot generate candidate XML";
    }

    $log->info('Parsing result for %s, user_id: %s, candidate_id: %s, locale: %s, postcode: %s',
               $user->group_identity,
               $user->id,
               $candidate->id,
               $cv_parsing_res->{locale},
               $cv_parsing_res->{post_code}
           );
    $candidate->enrich_with_cvparsing($cv_parsing_res);
    $log->trace("CV PARSER RES: ".Dumper($cv_parsing_res)) if $log->is_trace();

    my $xml = $candidate->to_xml( $advert, $cv_hash, $user );

    my $request = HTTP::Request::Common::POST(
        $url,
        'Content-Type' => 'text/xml; charset=' . $xml->encoding(),
        'Content'      => $xml->toString()
    );

    $log->trace( "Got request=". substr( $request->as_string, 0, 2000 )) if $log->is_trace();

    my $response = $stream2->user_agent->request($request);
    $log->trace( "Got response=". substr( $response->as_string, 0, 2000)) if $log->is_trace();

    unless ( $response->is_success ) {
        $log->error('Error in response: '.$response->as_string());
        die $response->status_line;
    }

    return Language::LispPerl->true();
}

=head2 forward_candidate

Forwards the candidate to the email address.

True: Action was successful.

Goes bang in any other case.

=cut

sub forward_candidate {
    my ($email_address) = @_;
    my $stream2 = Stream2->instance();
    die ($stream2->__('Missing an email address to forward the candidate to.')."\n") unless $email_address;
    _do_action(
        'Stream2::Action::ForwardCandidate',
        {
            recipients_ids    => [],
            recipients_emails => [$email_address],
            message           => ''
        }
    );
    return Language::LispPerl->true();
}

# does the action passed, returns the result rather than true.
sub _do_action{
    my ($action_class, $action_attributes) = @_;
    $action_attributes //= {};
    unless( $action_class =~ /^Stream2::Action::/ ){
        $action_class = 'Stream2::Action::'.$action_class;
    }

    my $stream2 = Stream2->instance();
    my $action_context = $stream2->context()->{macro_context}->action_context();
    unless( $action_context ){
        confess("No action_context. Cannot do any action");
    }

    $action_class = Class::Load::load_class( $action_class );

    # The context is there. We can build an action
    my $jobid = $stream2->uuid()->create_str();

    my $instance = $action_class->new({ jobid => $jobid,
                                        stream2 => $stream2,
                                        # If we are already in a log capturing environment,
                                        # do not manage the log capturing here.
                                        log_chunk_capture => Log::Log4perl::MDC->get('chunk') ? 0 : 1 ,
                                        %{ $action_context },
                                        %{ $action_attributes }
                                    });
    # Let the perform fail
    my $result = $instance->instance_perform();

    # Result can be a HTTP::Response entity.
    if( ( ( ref( $result ) // '' ) eq 'HASH' ) && $result->{error} ){
        die "Error doing action ".$action_class.": ".$result->{error}->{message}."\n";
    }

    return $result;
}

=head2 do_action

Do the given action in the current $s2->contect()->{action_context}
and return a Raw lisp atom.

This means that this can only be evaluated in the context given
by another action, or in an interface element that acts in a specific
context.

True: Action was successful.

Goes bang in any other case.

=cut

sub do_action{

    _do_action(@_);

    # Returns true in sucess
    return Language::LispPerl->true();
}

sub do_crash{
    die "Something went very wrong in ".__PACKAGE__."\n";
}

1;

package Stream2::Lisp::SecureBuiltIn;

use Moo::Role;

use Log::Any qw/$log/;

=head1 NAME

Stream2::Lisp::SecureBuiltIn - A Role to secure Lisp Builtins

=cut

my $armful_functions = { while => 1,
                         require => 1,
                     };

around 'has_function' => sub {
    my ( $orig, $self, $fname, @rest ) = @_;
    # while is considered not to be a function
    if( $armful_functions->{$fname} ){
        $log->warn("Function $fname is considered harmful. Pretending it doesnt exist");
        return;
    }
    return $self->$orig( $fname, @rest );
};

around '_impl_perlcall' => sub{
    my ($orig, $self, $ast, $symbol, $opts) = @_;

    my $ns = $opts->{namespace} ;
    if( $ns && $ns ne 'Stream2::Lisp::Core' ){
        die "You cannot use this namespace $ns.\n"
    }

    return $self->$orig( $ast,  $symbol, $opts );
};
1;

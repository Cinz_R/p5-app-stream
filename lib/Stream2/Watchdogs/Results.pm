package Stream2::Watchdog::Results;

use Moose;

has 'stream2' => (
    is          => 'ro',
    required    => 1,
);

has 'watchdog_subscription' => (
    is          => 'ro',
    required    => 1,
    isa         => 'Stream2::Schema::Result::WatchdogSubscription'
);

has 'per_page' => (
    is          => 'ro',
    isa         => 'Int',
    default     => 30
);

has 'page' => (
    is          => 'ro',
    isa         => 'Int',
    default     => 1
);

use Stream2::Results::Result;
use Stream2::Results::Store;

sub offset { ( $_[0]->page - 1 ) * $_[0]->per_page; }

=head2 to_results

    Loads watchdog results from the RDS and returns a L<Stream2::Results> object
    This process includes storing the candidates in redis so they are useable by the front end

    needs: L<Stream2::Schema::Result::WatchdogSubscription>

    returns: L<Stream2::Results>

    usage:
        my $wd_sub = $current_user->watchdogs->find( $watchdog_id )->subscriptions->find( $board );
        my $watchdog_results = Stream2::Watchdog::Results->new({
            stream2                 => $self->stream2,
            watchdog_subscription   => $wd_sub,
            per_page                => 30,
            page                    => $page
        });
        my $results_collector = $watchdog_results->to_results();
        my $redis_result_id = $results_collector->id();

=cut

sub to_results {
    my ( $self ) = @_;

    my $result_set = $self->watchdog_subscription->watchdog_results->search( undef, {
        order_by    =>  [ { -desc => [qw/last_seen_datetime/] }, { -asc => [qw/insert_datetime/] } ],
    });

    my $results_collector = Stream2::Results::Store->new({
                                              destination           => $self->watchdog_subscription->subscription->board,
                                              store                 => $self->stream2->redis(),
                                              per_page              => $self->per_page,
                                              total_results         => $result_set->count(),
                                              results_per_scrape    => $self->per_page,
                                              current_page          => $self->page,
                                              expires_after         => $self->stream2->config->{results_expire_after} || '3h',
                                             });

    $results_collector->save();

    map {
        my $candidate = $results_collector->new_candidate();
        $candidate->attributes( %{$_->content} );
        $results_collector->add_candidate( $candidate );
    } $result_set->search( undef , {
        rows    => $self->per_page,
        offset  => $self->offset
    })->all;

    return $results_collector;
}

__PACKAGE__->meta->make_immutable;

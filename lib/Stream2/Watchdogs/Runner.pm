package Stream2::Watchdogs::Runner;

use Moose;

use Log::Any qw/$log/;

use Data::Dumper;
use DateTime;

use Digest::SHA1;

use Encode;

use Log::Log4perl::MDC;

use Scalar::Util;

use Stream2::Criteria;
use Stream2::Results;

use String::Truncate;

use HTML::FormatText;
use Stream2::Action::WatchdogLoad;

=head1 NAME

Stream2::Watchdogs::Runner - A runner for a watchdog.

=cut

# The factory, just in case.
has 'watchdogs' => ( is => 'ro' , isa => 'Stream2::DBICWrapper::Watchdog' , required => 1);

# The watchdog to run
has 'watchdog'  => ( is => 'ro' , isa => 'Stream2::O::Watchdog' , required => 1);

=head2 stream2

Shortcut to the Stream2 instance.

=cut

sub stream2{
    return shift->watchdogs()->stream2();
}


## Factorizes looping through this runner's watchdog's subscriptions.
##
sub _run_through_subscriptions{
    my ($self , $run_subscription_options, $resultcallback) = @_;

    my $stream2 = $self->stream2();
    my $watchdog = $self->watchdog();

    # Run each subscription (in order of nice name)
    my @subscriptions_to_run = $watchdog->watchdog_subscriptions()
      ->search(undef, { order_by => 'subscription.board_nice_name' , prefetch => 'subscription'  })->all();

    while( my $watchdog_subscription = shift @subscriptions_to_run  ){

        my $subscription = $watchdog_subscription->subscription();
        my $mutex_key = 'WD_'.$watchdog->id().'-'.$subscription->board();
        my $user_object = $stream2->find_user( $subscription->user()->id() );

        # Is this CV subscription not runnable concurrently (for the same account)?
        unless( $subscription->allows_acc_sharing() ){
            $log->info("This subscription does not allow concurrent account access. Will expand the mutex key with the auth tokens");
            # We need to mutex on the users credentials signature
            my $subscription_ref = $user_object->subscriptions_ref()->{ $subscription->board() }
              // confess("No subscription details for board ".$subscription->board() );
            my %auth_tokens = %{ $subscription_ref->{auth_tokens} // {} };

            ## Stable mutex key for this board/credentials. Note that
            ## it doesnt include the subscription directly, cause we need to lock at the credential level
            ## for the board.
            $mutex_key = 'WD-board-'.$subscription->board().'-cred-'.Digest::SHA1::sha1_hex( join('-', map{ $_ , $auth_tokens{$_} } sort keys %auth_tokens ) );
        }

        $stream2->monitor->try_mutex(
            $mutex_key,
            sub {

                ## Callback with the subscription and the results.
                my $new_results = Stream2::Action::WatchdogLoad->new({
                    stream2                 => $stream2,
                    jobid                   => $stream2->uuid->create_str(),
                    criteria_id             => $watchdog->criteria_id(),
                    template_name           => $watchdog_subscription->subscription()->board(),
                    options                 => { page => 1, auto_run => 1, %{$run_subscription_options} },
                    user                    => { user_id => $user_object->id },
                    ripple_settings         => $user_object->ripple_settings(),
                    remote_ip               => $user_object->last_ip() // '127.0.0.1',
                    watchdog                => $watchdog,
                    watchdog_id             => $watchdog->id,
                    watchdog_subscription   => $watchdog_subscription,
                    stream2_base_url        => $watchdog->base_url
                })->instance_perform();

                if ( ref( $new_results ) eq 'HASH' && ( my $err = $new_results->{error} ) ){
                    # the output was that of an error, so new_results is empty
                    $log->warn("Watchdog subscription %s failed with error: %s", $watchdog_subscription->id, $err->{message});
                    $new_results = [];
                }

                if ( ref($resultcallback) eq 'CODE' ){
                    $resultcallback->( $watchdog_subscription, $new_results );
                }
            },
            sub{
                $log->warn("Watchdog subscription (on key = '$mutex_key') is already being ran. Pausing a bit and queuing it for a bit later");
                sleep(2);
                push @subscriptions_to_run , $watchdog_subscription;
            }
        );
    }
}


=head2 initialize

Initialize a watchdog. This is supposed to be run when a watchdog is created
to initialize persistant watchdog data (like last seen candidates and stuff like that).

=cut

sub initialize{
    my ($self) = @_;

    $log->info("Initializing watchdog [".$self->watchdog->id()."] ".$self->watchdog()->name());

    $self->_run_through_subscriptions({ init => 1 });

    # Each subscription is initialized. Set the next run time.

    my $timezone = ( $self->watchdog->user()->timezone() || 'Europe/London' );
    my $next_run_time = DateTime->now( time_zone => $timezone )->add( days => 1 )->set_minute(0)->set_hour(2);

    $next_run_time = $self->_get_runtime_without_conflicts($next_run_time);
    $next_run_time = $next_run_time->set_time_zone('UTC');
    # Add a random number of minutes accross one hour.
    # This is to spread the watchdogs load.
    $next_run_time->add( minutes => int(rand(60)) );

    $log->info("Setting next run datetime at UTC ".$next_run_time->iso8601());

    $self->watchdog()->next_run_datetime( $next_run_time );
    $self->watchdog()->update();
}



=head2 run

Similar to initialise, the candidates are stored as unviewed, an email is send
to the watchdog owner.

=cut

sub run {
    my ($self) = @_;

    $log->info("Running watchdog [".$self->watchdog->id()."] ".$self->watchdog()->name());

    my $user = $self->watchdog()->user();

    unless( $user->exists_remotely() ){
        $log->warn("User ".$user->id()." does not exists remotely anymore");
        return;
    }

    my $subscriptions_results = [];
    my $n_new_results = 0;
    my $n_errors = 0;

    ## Note that this will be run for each subscription by the
    ## helper method _run_though_subscriptions

    my $subscription_run_callback = sub {
        my ( $watchdog_subscription, $new_results ) = @_;

        # Only push if new results are there.
        push @{$subscriptions_results}, {
           watchdog_subscription    => $watchdog_subscription,
           new_results              => $new_results
        };
        $n_new_results += scalar(@$new_results);

        if( $watchdog_subscription->last_run_error() ){
            $n_errors++;
        }
    };

    $self->_run_through_subscriptions({} , $subscription_run_callback );

    unless( $n_new_results || $n_errors ){
        # Return immediately. No new results OR no error means nothing to alert on.
        # We do alert on error
        $log->info("No new watchdog result, no error, so nothing to alert about");
        return $n_new_results;
    }

    my $email_address = $user->watchdog_email_address();
    unless( $email_address ){
        $log->warn("no watchdog_email_address for user [".$user->id()."] Will NOT send any email, even though we should");
        return $n_new_results;
    }

    $log->info("Building email");

    my $translations = $self->stream2->translations();

    my $html_content = $translations->in_timezone(
        $user->timezone(),
        sub {
            $translations->in_language(
                $user->locale(),
                sub {
                    $log->info( "'Hello {name}' will be "
                          . $self->stream2->templates()
                          ->i18nx( 'Hello {name}', { name => 'Blablabla' } ) );
                    return $self->stream2->templates()->render(
                        'stream/watchdogs/email_result.tt',
                        {
                            watchdog => $self->watchdog,

                            # No links for non pure adcourier users please.
                            # Note that here we use the last one.
                            # This means that if a user alternates between
                            # using Search directly and integrated search,
                            # he will get watchog links or not alternatively
                            # according to the last time he used search.
                            can_link => scalar(
                                $user->last_login_provider() eq 'adcourier'
                            ),
                            n_new_results         => $n_new_results,
                            subscriptions_results => $subscriptions_results,
                        }
                    );
                }
            );
        }
    );

    my $base_url = $self->watchdog()->base_url();
    my $xdoc = eval{ $self->stream2()->html_parser()->load_html( string => Encode::encode('UTF-8', $html_content),
                                                                 encoding => 'UTF-8' ); };
    if( my $err = $@ ){
        $log->error("Failed to parse generated email HTML for watchdog ".$self->watchdog->id().". Link resolving and scraping will not happen. Error is: ".$err);
    }

    if( $xdoc ){
        my $has_changed = 0;
        #
        # An xdoc was generated. We can fixup all images
        #
        my @image_nodes  = $xdoc->findnodes('//img[string(@src)]');

        foreach my $img_node ( @image_nodes ){
            my $src = $img_node->getAttribute('src');
            if( $src =~ m|^//| ){
                # Protocol less URL. Fallback to https.
                $img_node->setAttribute('src', 'https:'.$src );
                $has_changed = 1;
                next;
            }
        }

        if( $has_changed ){
            $html_content = Encode::decode_utf8($xdoc->toString());
        }
    }


    # If the user does not log in via adcourier directly,
    # scrape off the links from the html content
    if( $user->last_login_provider() ne 'adcourier' ){
        $log->info("Watchog owner does not logged in via adcourier. Scraping off the URLs starting with '$base_url' from the HTML content");
        if( ! $xdoc ){
            $log->warn("No scraping possible. The xdoc has not been generated.");
        } else {
            my @anchor_nodes = $xdoc->findnodes('//a[string(@href)]');
            foreach my $anchor_node ( @anchor_nodes ){
                unless( $anchor_node->getAttribute('href') =~ /^$base_url/ ){
                    # Skip URLs not pointing at the search product directly
                    next;
                }
                $log->debug( "Will scrape ".$anchor_node->toString());
                # Get the children
                my @children = $anchor_node->childNodes();
                # Replace this node with just an attribute-less span
                my $span = $xdoc->createElement('span');
                # All all children to the span
                map{ $span->appendChild( $_ ) } @children;

                # And swap the node for the span!
                $anchor_node->replaceNode($span);
            }
            $xdoc->setEncoding('UTF-8');
            # And replace the html content with the scraped version
            $html_content = Encode::decode_utf8($xdoc->toString());
        }
    } # End of non adcourier user special treatment

    my $watchdog_name = $self->watchdog->name();
    $watchdog_name = String::Truncate::elide( $watchdog_name, 60 );

    my $email = $self->stream2()->factory('Email')->build(
        [
            From    => 'noreply@broadbean.net',
            To      => $email_address,
            Subject => Encode::encode(
                'MIME-Q',
                '[Watchdog] Your watchdog "'
                  . $watchdog_name
                  . '" Found '
                  . $n_new_results
                  . ' new candidates'
            ),
            Type                    => 'multipart/mixed',
            'X-Stream2-Watchdog-ID' => scalar( $self->watchdog->id() ),
            'X-Stream2-Transport' => 'static_transport' , # We want this to go through the static IP transport
        ]
    );

    my $container = $email->attach( Type => 'multipart/alternative' );
    $container->attach(Type => 'text/plain; charset=UTF-8',
                       Encoding => 'quoted-printable',
                       Disposition => 'inline',
                       Data => [ HTML::FormatText->format_string($html_content) ]);
    $container->attach(Type => 'text/html; charset=UTF-8',
                       Encoding => 'quoted-printable',
                       Data => [ $html_content ]);

    $self->stream2->send_email($email);

    $log->info("A watchdog email has been sent to $email_address");

    return $subscriptions_results;
}

# _get_runtime_without_conflicts returns a date time object that is free from
# temporal conflicts between when a watchdog (WD) would like to run, and any problematic
# times, such as maintenance window for a job board.
#
# if the WD run time does not clash it returns the date-time object untouched.
# Else it returns a mutually acceptable date time object.
#
# Currently clash detection is only for iprofile, as their maintenance window is
# 2 till 5, and we want to run our watchdogs in this time.

sub _get_runtime_without_conflicts {
    my ($self,$next_runtime) = @_;

    # something to re-factor if/when more boards report their maintenance to us,
    #  as this code is only set up to deal with a single board.
    my $run_time = {
        board_name => 'iprofile',
        TZ => 'Europe/London',
        avoid_hours => [2,3,4],
        run_at => 5
    };
    my $watchdog = $self->watchdog();
    my $has_board = 0;
    #  I want to use last to speed things up, so I wrap the grep in a block
    {
        grep {
                if ( $_->subscription->board eq $run_time->{board_name}) {
                    $has_board = 1;
                    last;
                }
        } $watchdog->watchdog_subscriptions;
    }

    # return if the watchdog does not have the problematic board
    return $next_runtime unless $has_board;

    $log->infof("%s has a a problematic board.", $watchdog->name);

    # check if there is a time clash.
    $next_runtime = $next_runtime->set_time_zone($run_time->{TZ});
    my $has_clash = 0;

    map { $has_clash = 1 if $_ == $next_runtime->hour }
        @{$run_time->{avoid_hours}};

    # if there is no clash we don't need to do anything.
    return $next_runtime unless $has_clash;

    $log->infof( "%s has a clash %s.", $watchdog->name, $next_runtime->hour );
    $next_runtime->set_hour( $run_time->{run_at} );
    return $next_runtime;
}



__PACKAGE__->meta->make_immutable();

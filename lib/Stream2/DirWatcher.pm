package Stream2::DirWatcher;
use Moose;
use POSIX ":sys_wait_h";
use Log::Any qw/$log/;

use Filesys::Notify::Simple;

=head1 NAME

Stream2::DirWatcher - Watch a directory and restart the given sub

=head1 Properties and methods.

=head2 dir

The directory to watch.

=head2 validate_path

A sub{ my $path = shift; ...}  that should return true if you want to act on
a change for this file path.

Default lets everything go through.

=head2 restart

A code ref to execute and restart when a file changes in the dir

=head2 sig_kill

The signal to use to kill a previous child. Defaults to 15 (SIGTERM).

=head2 watch

Call this and the given 'restart' code will run under this watcher.

=cut

# The stuff to restart in case something changes.
has 'restart' => ( is => 'ro' , isa => 'CodeRef', required => 1);
# The directory to watch
has 'dir' => ( is => 'ro' , isa => 'Str' , required => 1);
# How to validate a path change. Let everything though by default.
has 'validate_path' => ( is => 'ro' , isa => 'CodeRef' , required => 1, default => sub{ return sub{1;} } );
# The signal to send to the child to make it die. Default to SIGTERM
has 'sig_kill' => ( is => 'ro' , isa => 'Int', required => 1 , default => 15 );

# Internal stuff.
has 'notify_watcher' => ( is => 'ro' , isa => 'Filesys::Notify::Simple', lazy_build => 1);
has 'child_pid' => ( is => 'rw' , isa => 'Int' , clearer => 'clear_child_pid');


sub _build_notify_watcher{
    my ($self) = @_;
    $log->info("Watching directory ".$self->dir()." for changes");
    return Filesys::Notify::Simple->new([ $self->dir ]);
}

sub _restart{
    my ($self) = @_;

    # Fork a new child FIRST. This is in the case
    # we lack resources to fork. We don't want to
    # kill the service. Not restarting is better.
    my $pid = fork();
    unless( defined $pid ){
        $log->critical("Cannot fork: $!. Will carry on with current child to avoid interrupting the service");
        return;
    }

    unless( $pid ){
        # We are the child.
        &{$self->restart()}();
        exit(0);
    }

    $log->info("Started and watching after child $pid");

    # Kill the old one.
    if( $self->child_pid() ){
        $log->info("Killing old child ".$self->child_pid());
        kill $self->sig_kill() , $self->child_pid() ; # Kill the whole group
        waitpid( $self->child_pid() , 0 ); # Wait for the child to be proper dead.
        $log->info($self->child_pid()." is dead");
    }
    # Set the new one.
    $self->child_pid($pid);

}

=head2 watch

Watches the given directory (in the constructor) for changes
and restart the 'restart' sub if something filtered by 'validate_path'
have changed.

This is blocking and returning from that should be considered being
the end of the process.

Usage:

  $this->watch();

=cut

sub watch{
    my ($self) = @_;

    $self->_restart();

    while(1){
        my @events = ();
        $self->notify_watcher
          ->wait(sub{
                     @events = @_;
                     @events = grep { &{$self->validate_path()}($_->{path}) } @events;
                 });

        if( @events ){
            foreach my $event ( @events ){
                $log->info("Path ".$event->{path}." have changed");
            }
            $self->_restart();
        }
    }
}

__PACKAGE__->meta->make_immutable();

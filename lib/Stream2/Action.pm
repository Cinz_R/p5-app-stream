package Stream2::Action;

use Moose;

use Log::Any qw/$log/;

=head1 NAME

Stream2::Action - A base class for all asynchronous actions

=cut

has 'jobid' => ( is => 'ro', isa => 'Maybe[Str]' ); # Maybe, cause we dont care about that in testing mode.

has 'stream2' => ( is => 'ro', isa => 'Stream2', required => 1);
has 'stream2_base_url' => ( is => 'ro', isa => 'Str', lazy_build => 1 );
# This is set ONLY in the top action, and only for action coming from the interface.
has 'stream2_interactive' => ( is => 'ro', isa => 'Bool', default => 0 );

has 'options' => ( is => 'ro', isa => 'HashRef', default => sub{ {}; } );

has 'instance_memory_cache' => ( is => 'ro', isa => 'HashRef', default => sub{ {}; } );

sub _build_stream2_base_url{
    my ($self) = @_;
    $log->warn("stream2_base_url was NOT given. Defaulting to http://127.0.0.1/");
    return "http://127.0.0.1/";
}

=head2 perform

CLASS METHOD. Called by the Resque framework to perform a L<Resque::Job> with
an instance of this class.

Will build an instance of this with all job parameters (so you can use Moose
to enforce consistency) and then call 'instance_perform($job)' on it.

=cut

sub perform{
    my ($class,$job) = @_;

    my $parameters = $job->parameters() // {};

    my $instance = $class->new({ jobid => $job->guid(),
                                 stream2 => $job->{stream2} // confess("Missing stream2 in Job object"),
                                 %{ $parameters },
                             });

    # Note that we give a change to the instance to complete the job itself (e.g. in the FastJob role)
    $log->info("Performing Job class=".$class);
    $log->info("stream2_base_url = ".$instance->stream2_base_url());

    return $instance->instance_perform($job);
}


=head2 build_context

Builds a context (a hash of properties) that is required to have in any subsequently build actions.

This is meant to be enriched by any role action consume.

Usage:

  my $context = $this->build_context();

=cut

sub build_context{
    my ($self) = @_;
    return {
        instance_memory_cache => $self->instance_memory_cache(),
        stream2_base_url => $self->stream2_base_url()
    };
}

=head2 instance_perform

INSTANCE METHOD.

Returns a JSONAble data structure. This is the response of this Action. Does not
take any parameter as everything should be set in this instance.

Usage:

 my $response = $this->instance_perform();

=cut

sub instance_perform{
    my ($self) = @_;
    ...
}

sub macro_action_name{
    my ($self) = @_;
    return $self->blessed();
}

__PACKAGE__->meta->make_immutable();

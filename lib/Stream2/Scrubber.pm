package Stream2::Scrubber;

use Moose;

=head1 NAME

Stream2::Scrubber - A Scrubber to scrub data in all sorts of ways.

=cut

use Data::Visitor::Callback;
use Scalar::Util;

has 'scrub_objects_visitor' => ( is => 'ro', isa => 'Data::Visitor::Callback' , lazy_build => 1 );

sub _build_scrub_objects_visitor{
    my ($self) = @_;
    my $v = Data::Visitor::Callback->new(
        array => sub{
            my ($v, $array) = @_;
            return [  grep{ ! Scalar::Util::blessed( $_ ) } @$array ];
        },
        hash => sub{
            my ($v, $hash) = @_;
            return {
                map{ ( Scalar::Util::blessed( $hash->{$_} ) ? () : ( $_ => $hash->{$_} ) ) } keys %$hash
            };
        },
        object => sub{
            my ($v, $o) = @_;
            confess("Cannot have an object $o here. Bug?");
        }
    );
}

=head2 scrub_objects

Returns a deep copy of the given $data (a perl data structure), without all blessed objects removed.

Usage:

 my $object_less = $this->scrub_objects( $data );

=cut

sub scrub_objects{
    my ($self, $data) = @_;
    return $self->scrub_objects_visitor()->visit( $data );
}

__PACKAGE__->meta->make_immutable();
1;

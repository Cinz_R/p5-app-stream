package Stream2::PerfMonitor;

use Moose;

use Log::Any qw/$log/;
use RRD::Editor;

=head1 NAME

Stream2::PerfMonitor - Monitor various measurements into a RRD file.

=head1 SEE

Want to visualize the rrd file?

See L<http://www.famzah.net/rrdwizard/index.php>

Also see L<RRD::Editor> doc.

=head1 ATTRIBUTES

is_on: Is this on? Config file might say nope (or nothing).

rrd_file: The name of the rrd file (From the configuration)

=cut

has 'stream2' => ( is => 'ro', isa => 'Stream2' , weak_ref => 1 , required => 1 );

has 'is_on' => ( is => 'ro', isa => 'Bool', lazy_build => 1 );
has 'rrd_file' => ( is => 'ro', isa => 'Str', lazy_build => 1 );

sub _build_is_on{
    my ($self) = @_;
    return $self->stream2->config()->{perfmonitor}->{is_on} // 0;
}

sub _build_rrd_file{
    my ($self) = @_;
    return $self->stream2->config()->{perfmonitor}->{rrd_file} // confess("Missing rrd_file in config / perfmonitor");
}


=head2 with_editor

If this is_on Run the measure code with an open L<RDD::Editor>, and close this L<RDD::Editor>.

Returns the 'measure' code return value (or undef)

Usage:

  $this->with_editor(sub{
      my ($self, $editor) = @_;
      $self->record_measure( $editor , { oauthRespTime => 1.2,
                              juiceUserTime => 2.2,
                              juiceSubsTime => 0.2,
                              juiceLocaTime => 3.1
                            });
      return 1;
  });

=cut

sub with_editor{
    my ($self, $measure) = @_;

    unless( $self->is_on() ){
        $log->warn("perfmon is not on. Skipping everything");
        return;
    }

    my $rrd_file = $self->rrd_file();

    my $rrd = RRD::Editor->new();

    unless( -e $rrd_file ){
        $log->info("Creating $rrd_file");

        # See http://oss.oetiker.ch/rrdtool/doc/rrdcreate.en.html
        # Resolution is minute, we use portable format,
        # and we want to measure those things:
        # oauthRespTime
        # juiceUserTime
        # juiceSubsTime
        # juiceLocaTime
        $rrd->create('--step 60  '.
                     'DS:oauthRespTime:GAUGE:600:0:U '.
                     'DS:juiceUserTime:GAUGE:600:0:U '.
                     'DS:juiceSubsTime:GAUGE:600:0:U '.
                     'DS:juiceLocaTime:GAUGE:600:0:U '.
                     'RRA:AVERAGE:0.5:1:2880'
                    );
        $rrd->save($rrd_file);
        $rrd = RRD::Editor->new();
    }

    $rrd->open($rrd_file);

    my $ret = eval{
        &$measure($self, $rrd);
    };
    if( my $err = $@ ){
        $rrd->close();
        confess("ERROR in measurement: $err");
    }

    $rrd->close();
    return $ret;
}

=head2 record_measure

Records a measurement in the given RRD::Editor

To be used in conjunction with L<with_editor>

=cut

sub record_measure{
    my ($self, $editor, $measures) = @_;
    $measures //= {};
    $measures = { %$measures };

    my $str = '-t oauthRespTime:juiceUserTime:juiceSubsTime:juiceLocaTime N:'.join(':' ,
                                                                                   ( ( delete $measures->{oauthRespTime} ) // 'U',
                                                                                     ( delete $measures->{juiceUserTime} ) // 'U',
                                                                                     ( delete $measures->{juiceSubsTime} ) // 'U',
                                                                                     ( delete $measures->{juiceLocaTime} ) // 'U',
                                                                                   ) );
    $editor->update( $str );
    $log->info("Recorded measure '$str'");

    if( my @leftovers = keys %{$measures} ){
        confess("KEYS ".join(', ' , @leftovers ).' were not managed by this editor');
    }
    return $measures;
}



__PACKAGE__->meta->make_immutable();

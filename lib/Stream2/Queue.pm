package Stream2::Queue;
{
  $Stream2::Queue::VERSION = '0.002';
}

# ABSTRACT: reliable queue with many hat tips to Resque

use Moose;
use Stream2::Queue::Job;

with 'Stream2::Role::KeyValStore';

has 'q_key' => (
  is      => 'ro',
  isa     => 'Str',
  lazy    => 1,
  default => 'defaultqueue',
);

# TODO, move this method so it belongs to the job
sub add {
  my $self = shift;
  my $task = shift or die "Missing task to add to the queue"; # hash ref

  if ( ref($task) eq 'HASH' ) {
    $task = Stream2::Queue::Job->new($task);
  }
  elsif ( !ref($task) ) {
    $task = Stream2::Queue::Job->new({ action => $task });
  }

  if ( !$task->can('serialise') ) {
    die "Don't know how to serialise '$task' task, add a 'serialise' method";
  }

  if ( !$task->can('id') || !$task->id ) {
    die "This task does not have an id, add an 'id' method";
  }

  $task->audit('queued');

  # Push onto the top of the queue
  # The worker uses RPOPLPUSH to pull jobs from the bottom of the queue
  my $json = $task->serialise();
  $self->r_lpush( $self->q_key, $json );
  
  return $task->id();
}

sub job_status {
  my $self = shift;
  my $job_id = shift;

  require Stream2::Queue::JobStatus;
  return Stream2::Queue::JobStatus->new(
    id => $job_id,
  )->load();
}

sub is_empty     { !$_[0]->queue_length }
sub queue_length { $_[0]->r_llen( $_[0]->q_key ) }
sub clear        { $_[0]->r_del( $_[0]->q_key )  }

__PACKAGE__->meta->make_immutable;

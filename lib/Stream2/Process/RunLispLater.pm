package Stream2::Process::RunLispLater;

use Moose;
extends qw/Stream2::Process/;

=head1 NAME

Stream2::Process::RunLispLater - A Schedule::LongStep process to run lisp code later.

=cut

use DateTime;

use Stream2::O::MacroContext;

use Log::Any qw/$log/;

has 'macro_context' => ( is => 'ro', isa => 'Stream2::O::MacroContext', lazy_build => 1 );
has 'lisp_code' => ( is => 'ro', isa => 'Str', lazy_build => 1 );


sub _build_lisp_code{
    my ($self) = @_;
    return $self->state()->{lisp_code} // confess("Missing lisp_code in state");
}

sub _build_macro_context{
    my ($self) = @_;
    return Stream2::O::MacroContext->new_from_frozen_b64( $self->state()->{macro_context} // confess("Missing macro_context in state")
                                                              , { stream2 => $self->stream2() } );
}

=head2 build_first_step

As required by L<Schedule::LongSteps> framework.

=cut

sub build_first_step{
    my ($self) = @_;
    return $self->new_step({ what => 'do_plan_run' , run_at => DateTime->now() });
}

=head2 do_plan_run

Plan the actual run in state->in_minutes

=cut

sub do_plan_run{
    my ($self) = @_;
    my $at = DateTime->now()->add( minutes => $self->state()->{in_minutes} // confess("Missing in_minutes in state") );
    return $self->new_step({ what => 'do_run_lisp_code' , run_at => $at });
}

=head2 do_run_lisp_code

Run the lisp code in the given context and put the returned value in the final state.

=cut

sub do_run_lisp_code{
    my ($self) = @_;

    my $context = $self->macro_context();
    my $res = $self->stream2->with_context({ macro_context => $context },
                                           sub{
                                               return $context->evaler()->eval( $self->lisp_code() );
                                           }
                                       );
    my $value = $res->value().'';
    return $self->final_step({ state => { %{$self->state()}, result => $value } });
}

__PACKAGE__->meta->make_immutable();
1;

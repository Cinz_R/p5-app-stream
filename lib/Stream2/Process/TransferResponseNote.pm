package Stream2::Process::TransferResponseNote;

use Moose;
extends qw/Stream2::Process/;

use Data::Dumper;

use Stream2::Results::Result;

=head1 NAME

Stream2::Process::TransferResponseNote - A Schedule::LongStep process to transfer a response note to its sibblings in internal candidate DBs.

=head1 SYNOPSIS

  $sc->instantiate_process('Stream2::Process::TransferResponseNote',
                           { stream2 => $self->stream2() },
                           { user_id => $original_action->user_id(),
                             adcresponses_id => ..,
                             tsimport_id => ..,
                             search_record_id => $original_action->search_record_id(),
                             note_data => $original_action->data()
                           }
                           );


=cut

use DateTime;

use Log::Any qw/$log/;

has 'user' => ( is => 'ro', isa => 'Stream2::O::SearchUser' , lazy_build => 1);
has 'response_candidate' => ( is => 'ro', isa => 'Stream2::Results::Result', lazy_build => 1);

sub _build_user{
    my ($self) = @_;
    my $user_id = $self->state()->{user_id} // confess("No user_id in state");
    return $self->stream2()->factory('SearchUser')->find($user_id);
}

sub _build_response_candidate{
    my ($self) = @_;
    my $adcresponses_id = $self->state()->{adcresponses_id} // confess("No adcresponses_id in state");
    my $tsimport_id     = $self->state()->{tsimport_id} // confess("No tsimport_id in state");
    my $result = Stream2::Results::Result->new({ destination => 'adcresponses',
                                                 candidate_id => $adcresponses_id });
    $result->set_attr( 'tsimport_id' => $tsimport_id );
    return $result;
}

=head2 build_first_step

As required by L<Schedule::LongSteps> framework.

=cut

sub build_first_step{
    my ($self) = @_;
    return $self->new_step({ what => 'do_transfer_note' , run_at => DateTime->now()->add( minutes => 1 ) });
}

=head2 do_transfer_note

If transfering a note is possible, then cool.

Otherwise will wait a bit more, or give up if it's not possible at all.

=cut

sub do_transfer_note{
    my ($self) = @_;
    my $n_try = $self->state()->{n_try} // 1;
    if( $n_try > 17 ){
        # Give up after 3 months.
        die "This process cannot succeed after so long. Something is very wrong";
    }
    my $internal_mapping = $self->response_candidate->get_internal_db_ids();
    if( ( $internal_mapping // '' ) eq 'INTERNAL_DBS_PLEASE_WAIT' ){
        $log->info("Still too early. Will try again later.");
        return $self->new_step({ what => 'do_transfer_note',
                                 run_at => DateTime->now()->add( minutes => 2 ** $n_try ),
                                 state => { %{$self->state()} , n_try => ++$n_try  }
                             });
    }elsif( ref( $internal_mapping // '' ) eq 'HASH' ){
        $log->info("Got internal candidate mapping ".Dumper( $internal_mapping ));
        my $s2 = $self->stream2();

        my @siblings = $self->response_candidate->get_internal_siblings( $internal_mapping );
        foreach my $sibling ( @siblings ){
            $s2->factory('CandidateActionLog')->create({
                action => 'note',
                candidate_id => $sibling->id(),
                destination => $sibling->destination(),
                user_id => $self->user()->id(),
                group_identity => $self->user()->group_identity(),
                search_record_id => $self->state()->{search_record_id},
                data => $self->state()->{note_data} // confess("No notes data here"),
            });
            return $self->final_step({ state => { %{$self->state()}, reason => 'Sucessfully transfered note' } });
        }
    }else{
        die "Inconsistent state. This process should not exists";
    }
}

__PACKAGE__->meta->make_immutable();

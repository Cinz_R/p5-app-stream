package Stream2::Process::TransferResponseTag;

use Moose;
extends qw/Stream2::Process/;

use Data::Dumper;
use Carp;
use Stream2::Results::Result;

=head1 NAME

Stream2::Process::TransferResponseTag - A Schedule::LongStep process to transfer a response tag
to its sibblings in internal candidate DBs.

=head1 SYNOPSIS

    $stream2->longsteps()->initiate_process(
        'Stream2::Process::TransferResponseTag',
        {
            stream2 => $stream2,
        },
        {
            user_id             => $self->user_object()->id(),
            adcresponse_id      => $candidate->candidate_id(),
            tsimport_id         => $candidate->attr('tsimport_id'),
            search_record_id    => $candidate->search_record_id(),
            tag_name            => $tag_name,
            internal_mapping    => { BBCSS => 123, Artirix => 432 }
        }
    );

=cut

use DateTime;

use Log::Any qw/$log/;

has 'user' => ( is => 'ro', isa => 'Stream2::O::SearchUser' , lazy_build => 1);
has 'response_candidate' => ( is => 'ro', isa => 'Stream2::Results::Result', lazy_build => 1);

with qw/
    Stream2::Role::Process::CandidateActionLog
/;

sub _build_user{
    my ($self) = @_;
    my $user_id = $self->state()->{user_id} // confess("No user_id in state");
    return $self->stream2()->factory('SearchUser')->find($user_id);
}

sub _build_response_candidate{
    my ($self) = @_;
    my $adcresponses_id = $self->state()->{adcresponses_id} // confess("No adcresponses_id in state");
    my $tsimport_id     = $self->state()->{tsimport_id} // confess("No tsimport_id in state");
    my $result = Stream2::Results::Result->new({ destination => 'adcresponses',
                                                 candidate_id => $adcresponses_id });
    $result->set_attr( 'tsimport_id' => $tsimport_id );
    return $result;
}

=head2 build_first_step

As required by L<Schedule::LongSteps> framework.

=cut

sub build_first_step{
    my ($self) = @_;
    return $self->new_step({ what => 'do_transfer_tag' , run_at => DateTime->now()->add( minutes => 1 ) });
}

=head2 do_transfer_tag

If transfering a tag is possible, then cool.

Otherwise will wait a bit more, or give up if it's not possible at all.

=cut

sub do_transfer_tag {
    my ($self) = @_;
    my $n_try = $self->state()->{n_try} // 1;
    if( $n_try > 17 ){
        # Give up after 3 months.
        Carp::confess( "This process cannot succeed after so long. Something is very wrong" );
    }
    my $internal_mapping = $self->state()->{internal_mapping} // $self->response_candidate->get_internal_db_ids() // '';
    if( $internal_mapping eq 'INTERNAL_DBS_PLEASE_WAIT' ){
        $log->info("Still too early. Will try again later.");
        return $self->new_step({
            what    => 'do_transfer_tag',
            run_at  => DateTime->now()->add( minutes => 2 ** $n_try ),
            state   => { %{$self->state()} , n_try => ++$n_try  }
        });
    }
    elsif( ref( $internal_mapping ) eq 'HASH' ){
        $log->infof("Got internal candidate mapping: %s", Dumper( $internal_mapping ));

        my @siblings = $self->response_candidate->get_internal_siblings( $internal_mapping );
        foreach my $sibling ( @siblings ){
            my $template = $sibling->destination;
            $log->info("Building template '$template'");
            my $feed = $self->stream2()->build_template(
                $template,
                undef,
                {
                    user_object   => $self->user(),
                    company       => $self->user()->group_identity()
                }
            );

            my $tag = $self->_get_candidate_tag($sibling, $self->state()->{tag_name});

            unless ( $tag ) {
                confess('Tag with tag_name "'.$self->state()->{tag_name}.'" does not exist for candidate "'.$sibling->id().'"');
            }

            $feed->tag_candidate($sibling, $tag);
            $self->log_candidate_action('candidate_tag_add', $sibling, { tag_name => $tag->tag_name });

            return $self->final_step({ state => { %{$self->state()}, reason => 'Sucessfully transfered tag' } });
        }
    }
    else {
        Carp::confess( "Inconsistent state. This process should not exists" );
    }
}

sub _get_candidate_tag {
    my ($self, $candidate, $tag_name) = @_;
    my $schema = $self->stream2->stream_schema;
    my $stuff = sub {
        my $candidate_tag = $schema->resultset('CandidateTag')->find_or_create({
            group_identity  => $self->user->group_identity,
            candidate_id    => $candidate->id(),
            tag_name        => $tag_name,
        });
        return $candidate_tag;
    };
    return $schema->txn_do($stuff);
}

__PACKAGE__->meta->make_immutable();

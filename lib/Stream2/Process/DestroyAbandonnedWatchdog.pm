package Stream2::Process::DestroyAbandonnedWatchdog;

use Moose;
extends qw/Stream2::Process/;

=head1 NAME

Stream2::Process::DestroyAbandonnedWatchdog - A Schedule::LongStep process to put abandonned dogs to sleep.

=cut

use DateTime;
use String::Truncate;

use Log::Any qw/$log/;

has 'watchdog' => ( is => 'ro', isa => 'Maybe[Stream2::O::Watchdog]', lazy_build => 1);


sub _build_watchdog{
    my ($self) = @_;
    my $wd_id = $self->state()->{watchdog_id} // confess("No watchdog_id in this state");
    return $self->stream2()->factory('Watchdog')->find( $wd_id );
}

=head2 build_first_step

As required by L<Schedule::LongSteps> framework.

=cut

sub build_first_step{
    my ($self) = @_;
    return $self->new_step({ what => 'do_send_warning_email' , run_at => DateTime->now() });
}

=head2 do_send_warning_email

Send the first warning email and schedule sending a final warning.

=cut

sub do_send_warning_email{
    my ($self) = @_;
    return $self->_with_abandonned_watchdog(
        sub{
            my ( $watchdog ) = @_;
            my $watchdog_name = $self->watchdog->name();

            my $stream2 = $self->stream2();
            my $translations = $stream2->translations();

            my $user = $watchdog->user();
            my $email =
                $translations->in_language(
                    $user->locale() || 'en_GB',
                    sub{
                        Email::Simple->create(
                            header => [
                                From => 'noreply@broadbean.net',
                                To => $user->watchdog_email_address(),
                                Subject => Encode::encode('MIME-Q', $stream2->__('Warning! Your watchdog on Broadbean is about to expire') ),
                                'X-Stream2-Transport' => 'static_transport' , # We want this to go through the static IP transport
                            ],
                            body => $stream2->__x("Dear {contact_name},\n\nWe have noticed that you have not looked at the results of your watchdog '{watchdog_name}'
for three weeks.\n\nPlease be aware that this watchdog will be automatically deleted in 1 week if you do not access its results page in Broadbean.\n\nThe Broadbean Search Team."
                                                      ,
                                                  contact_name => $user->contact_name(),
                                                  watchdog_name => $watchdog_name
                                              )
                        );
                    });
            my $next_run_at = DateTime->now()->add( days => 7 );
            $log->info("Watchdog ".$self->watchdog->id().". Sending first warning email");

            my $warnings_sent = $self->state()->{warnings_sent} // [];
            push @$warnings_sent , DateTime->now().'';

            $stream2->send_email( $email );

            return $self->new_step({ what => 'do_send_final_warning',
                                     run_at => $next_run_at,
                                     state => { %{$self->state()} , warnings_sent => $warnings_sent }
                                 });
        });
}

=head2 do_send_final_warning

Sends a final warning email and schedule deletion for real.

=cut

sub do_send_final_warning{
    my ($self) = @_;
    return $self->_with_abandonned_watchdog(
        sub{
            my ( $watchdog ) = @_;
            my $watchdog_name = $self->watchdog->name();

            # Email Subject is size sensitive
            $watchdog_name = String::Truncate::elide( $watchdog_name, 60 );

            my $stream2 = $self->stream2();
            my $translations = $stream2->translations();

            my $user = $watchdog->user();
            my $email =
                $translations->in_language(
                    $user->locale() || 'en',
                    sub{
                        Email::Simple->create(
                            header => [
                                From => 'noreply@broadbean.net',
                                To => $user->watchdog_email_address(),
                                Subject => Encode::encode('MIME-Q', $stream2->__x('[Final Warning] Deletion of watchdog {watchdog_name} is imminent', watchdog_name => $watchdog_name) ),
                                'X-Stream2-Transport' => 'static_transport' , # We want this to go through the static IP transport
                            ],
                            body => $stream2->__x("Dear {contact_name}\n\nWe warned you last week that we would delete your watchdog '{watchdog_name}'\nif you leaved it unchecked.\n\nAs you haven't accessed it since, it is now scheduled to be deleted in one day.\n\nTo avoid this, you can access this watchdog now.\n\nThe Broadbean Search Team."
                                                      ,
                                                  contact_name => $user->contact_name(),
                                                  watchdog_name => $watchdog_name
                                              )
                        );
                    });
            my $next_run_at = DateTime->now()->add( days => 1 );
            $log->info("Watchdog ".$self->watchdog->id().". Sending last warning email");

            my $warnings_sent = $self->state()->{warnings_sent} // [];
            push @$warnings_sent , DateTime->now().'';

            $self->stream2()->send_email( $email );

            return $self->new_step({ what => 'do_delete_watchdog',
                                     run_at => $next_run_at,
                                     state => { %{$self->state()} , warnings_sent => $warnings_sent }
                                 });

        });
}

=head2 do_delete_watchdog

Actually delete the watchdog and terminates this process.

=cut

sub do_delete_watchdog{
    my ($self) = @_;
    $self->_with_abandonned_watchdog(
        sub{
            my ( $watchdog ) = @_;
            $log->warn("DELETING watchdog ".$watchdog->id());
            $watchdog->delete();
            return $self->final_step({ state => { %{$self->state()}, reason => 'User did not respond to warning emails' } });
        });
}

# Garantees the watchdog is there and abandonned,
# and do what needs to be done if it is not the case.
sub _with_abandonned_watchdog{
    my ($self, $do_stuff) = @_;
    unless( $self->watchdog() && $self->watchdog()->active() ){
        # Watchdog is gone or inactive.
        $log->info("Watchdog ".$self->state()->{watchdog_id}." has been deleted or inactivated. Terminating process.");
        return $self->final_step({ state => { %{$self->state()}, reason => 'Watchdog is gone or inactive' } });
    }

    my $user = $self->watchdog->user;
    my $watchdog_email_address = $user->watchdog_email_address();
    unless( $watchdog_email_address ){
        $log->infof('Users does not have an email address, will deactivate watchdog [%s] for them.', $self->watchdog->id());
        $self->watchdog->active(0);
        $self->watchdog->update();
        return $self->final_step({ state => { %{$self->state()}, reason => 'User does not have an email address' } });
    }

    my $ebl = $self->stream2->factory('EmailBlacklist');
    # check the global first, as its more likely to be on there
    if (
         $ebl->email_blacklisted($watchdog_email_address) ||
         $ebl->email_blacklisted($watchdog_email_address , { group_identity => $user->group_identity() })
        ){
        $log->infof('Users email address [%s] is blacklisted, will deactivate watchdog [%s] for them.', $watchdog_email_address, $self->watchdog->id());
        $self->watchdog->active(0);
        $self->watchdog->update();
        return $self->final_step({ state => { %{$self->state()}, reason => 'Users email address is blacklisted' } });
    }

    my $three_weeks_ago = DateTime->now()->subtract( days => 7 * 3 );
    if( $self->watchdog()->last_viewed_datetime() > $three_weeks_ago ){
        # Watchog has been viewed again, therefore resetting the process.
        # Send the warning email in three weeks.
        my $new_run_at = $self->watchdog()->last_viewed_datetime()->clone()->add( days => 7 * 3 );
        $log->info("Watchdog ".$self->watchdog->id()." has now been viewed less than 3 weeks ago. Sending warning email at $new_run_at");
        return $self->new_step({ what => 'do_send_warning_email' , run_at => $new_run_at });
    }

    return $do_stuff->($self->watchdog());
}

1;

package Stream2::Feeds::FacetList;

=head1 NAME

Stream2::Feeds::FacetList

=head1 DESCRIPTION

A list of facets

=cut

use Moose;

use Stream2::Feeds::Facet::Updated;
use Stream2::Feeds::Facet::Text;



has 'facets' => (
    is => 'rw',
    isa => 'HashRef[Stream2::Feeds::Facet]',
    lazy_build => 1,
);

sub _build_facets {
    my ($self) = @_;
    return {
        cv_updated_within            => Stream2::Feeds::Facet::Updated->new(default => 1),
        default_jobtype              => Stream2::Feeds::Facet::Text->new(default => 1),
        distance_unit                => Stream2::Feeds::Facet::Text->new(default => 1),
        distance                     => Stream2::Feeds::Facet::Text->new(default => 1),
        include_unspecified_salaries => Stream2::Feeds::Facet::Text->new(default => 1),
        keywords                     => Stream2::Feeds::Facet::Text->new(default => 1),
        location_within              => Stream2::Feeds::Facet::Text->new(default => 1),
        location_within_miles        => Stream2::Feeds::Facet::Text->new(default => 1),
        location_within_km          => Stream2::Feeds::Facet::Text->new(default => 1),
        location_id                  => Stream2::Feeds::Facet::Text->new(default => 1),
        salary_cur                   => Stream2::Feeds::Facet::Text->new(default => 1),
        salary_from                  => Stream2::Feeds::Facet::Text->new(default => 1),
        salary_per                   => Stream2::Feeds::Facet::Text->new(default => 1),
        salary_to                    => Stream2::Feeds::Facet::Text->new(default => 1),
        salary_unknown               => Stream2::Feeds::Facet::Text->new(default => 1),
    };
}

=head C<add_facets(%facets)>

Add the given C<%facets> to our hash of facets

    my $facet_list = Stream2::Feeds::FacetList->new(
        facets => {
            name => Stream2::Feeds::Facet::Text->new()
        }
    );
    # $facet_list contains one facet

    $facet_list->add_facets(
        name     => Stream2::Feeds::Facet::Text->new(),
        jobtitle => Stream2::Feeds::Facet::Text->new()
    );
    # $facet_list contains three facets

=cut

sub add_facets {
    my ($self, %facets) = @_;
    $self->facets({
        %{$self->facets},
        %facets
    });
}

=head2 c<get($key)>

Return the facet with key C<$key>

=cut

sub get {
    my ($self, $key) = @_;
    return $self->facets->{$key} || undef;
}

=head2 C<standard_tokens()>

Return a backwards compatible "token_hash" of this facet.

=cut

sub to_standard_tokens {
    my ($self) = @_;

    my $tokens = {};
    while(my ($key, $facet) = each %{$self->facets}) {
        my $token = $facet->to_token;
        next unless $token;
        $token->{Name} = $key;
        $tokens->{$key} = $token;
    }
    return $tokens;
}

=head2 C<value_of($key)>

Returns the value of the facet with key C<$key>

=cut

sub value_of {
    my ($self, $key) = @_;
    my $facet = $self->get($key)
      or return;

    return $facet->value;
}

__PACKAGE__->meta->make_immutable;

1;

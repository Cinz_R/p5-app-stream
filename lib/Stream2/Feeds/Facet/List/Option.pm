package Stream2::Feeds::Facet::List::Option;

=head1 NAME

Stream2::Feeds::Facet::List::Option

=head1 DESCRIPTION

A list facet option

=cut
use Moose;


=head1 PROPERTIES

=head2 label

This options display label

=cut

has 'label' => (
    is => 'rw',
    isa => 'Str',
    default => '',
);

=head2 value

This options value

=cut

has 'value' => (
    is => 'rw',
    isa => 'Str',
    default => '',
);

=head2 count

Indicates how many candidates are found when this option is selected

=cut

has 'count' => (
    is => 'rw',
    isa => 'Maybe[Int]',
    default => '',
);

=head2 selected

Truthy if this option is selected

=cut

has 'selected' => (
    is => 'rw',
    isa => ,'Bool',
    default => 0
);

=head1 METHODS

=head2 to_arrayref

Returns an array ref of [label, value, count]

=cut

sub to_arrayref {
    my ($self) = @_;
    return [
        $self->label,
        $self->value,
        $self->count
    ];
}

=head2 from_arrayref

Returns a new option from an arrayref of [label, value, count]

=cut

sub from_arrayref {
    my ($class, $ref) = @_;

    return $class->new(
        label => $ref->[0],
        value => $ref->[1],
        count => $ref->[2],
    );
}

__PACKAGE__->meta->make_immutable;

1;

package Stream2::Feeds::Facet::Updated;

use Moose;

=head1 NAME

Stream2::Feeds::Facet::Text

=head1 DESCRIPTION

A text facet. Inherits from L<Stream2::Feeds::Facet>

=cut

extends 'Stream2::Feeds::Facet';

use DateTime;

=head1 PROPERTIES

=head2 type

The facet type: "updated".

=cut

has 'type' => (
    is => 'ro',
    isa => 'Str',
    default => 'updated'
);

=head2 value

The current value of this facet

=cut

has 'value' => (
    is => 'rw',
    isa => 'Maybe[Str]',
);


=head2 set_value($value);

Sets $value as the value for this facet

=cut

sub set_value {
    my ($self, $value) = @_;
    if(ref $value eq 'ARRAY') {
        $value = $value->[0];
    }
    $self->value($value);
}

around 'to_token' => sub {
    my ($orig, $self) = @_;
    return if $self->default || $self->credential;
    my $token = $self->$orig;
    $token->{Value} = $self->value;
    return $token;
};

=head2 set_token($value)

Backwards compaitble. Sets the facet value from the value.

=cut

sub set_token {
    my ($self, $value) = @_;
    $value = [$value] unless ref $value;
    $self->value($value->[0]);
}


=head2 datetime

The datetime object representing the updated since date.
Returned undef if duration is 'ALL'

=cut

sub datetime {
    my ($self) = @_;
    my $duration = $self->duration;
    return undef unless defined($duration);

    return DateTime->now - $self->duration;
}

=head2 duration

A duration object representing the updated since duration.
Returns undef if duration is 'ALL'
=cut
sub duration {
    my ($self) = @_;
    my $value = $self->value || '';
    return undef if $value eq 'ALL';

    return DateTime::Duration->new(days => 1) if $value eq 'TODAY';
    return DateTime::Duration->new(days => 2) if $value eq 'YESTERDAY';
    return DateTime::Duration->new(days => 3) if $value eq '3D';
    return DateTime::Duration->new(weeks => 1) if $value eq '1W';
    return DateTime::Duration->new(weeks => 2) if $value eq '2W';
    return DateTime::Duration->new(months => 1) if $value eq '1M';
    return DateTime::Duration->new(months => 2) if $value eq '2M';
    return DateTime::Duration->new(months => 3) if $value eq '3M';
    return DateTime::Duration->new(months => 6) if $value eq '6M';
    return DateTime::Duration->new(years => 1) if $value eq '1Y';
    return DateTime::Duration->new(years => 2) if $value eq '2Y';
    return DateTime::Duration->new(years => 3) if $value eq '3Y';
    die('Illegal value "' . $value . '"');
}

=head2 mapped_value()

Get the mapped value of the current value using the set map. See map()

=cut

sub mapped_value {
    my ($self, %map) = @_;
    return $map{$self->value};
}

__PACKAGE__->meta->make_immutable;

1;

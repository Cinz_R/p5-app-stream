package Stream2::Feeds::Facet::Checkbox;

=head1 NAME

Stream2::Feeds::Facet::Checkbox

=head1 DESCRIPTION

A checkbox facet

=cut

use Moose;

extends 'Stream2::Feeds::Facet';


has 'type' => (
    is => 'ro',
    isa => 'Str',
    default => 'checkbox',
);

=head1 PROPERTIES

=head2 checked_value

What our value should be when we're checked. Defaults to '1'.

=cut

has 'checked_value' => (
    is => 'rw',
    isa => 'Str',
    default => '1',
);

=head2 unchecked_value

What our value shouldbe when we're unchecked. Defaults to ''.

=cut

has 'unchecked_value' => ( 
    is => 'rw',
    isa => 'Str',
    default => ''
);

=head2 checked

True if we're checked

=cut

has 'checked' => (
    is => 'rw',
    isa => 'Bool',
    default => 0
);

around 'to_token' => sub {
    my ($orig, $self) = @_;
    my $token = $self->$orig;
    $token->{Type} = 'list',
    $token->{Options} = [
        ['Yes', $self->checked_value],
        ['No', $self->unchecked_value],
    ];
    $token->{SelectedValues} = { $self->value,  1 };
    return $token;
};

=head1 METHODS

=head2 value

Our current value

=cut

sub value {
    my ($self) = @_;
    return $self->checked ? $self->checked_value : $self->unchecked_value;
}

=head2 set_token

Set the facet value from a token

=cut

sub set_token {
    my ($self, $value) = @_;
    $value = ref $value ? $value->[0] : $value;
    if($self->checked_value eq $value) {
        $self->checked(1);
        return;
    }
    if($self->unchecked_value eq $value) {
        $self->checked(0);
        return;
    }

    # Received an unknown value. Die.
    die( 'Unknown value given in checkbox set_token');
}

__PACKAGE__->meta->make_immutable;

1;

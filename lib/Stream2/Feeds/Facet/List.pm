package Stream2::Feeds::Facet::List;

=head1 NAME

Stream2::Feeds::Facet::List

=head1 DESCRIPTION

A list facet. Inherits from L<Stream2::Feeds::Facet>.

=cut

use Moose;
extends 'Stream2::Feeds::Facet';

use List::Util;
use Stream2::Feeds::Facet::List::Option;

=head1 PROPERTIES

=head2 options

A list of L<Stream2::Feeds::Facet::list::Option> objects
Custom setter inflates option item arrayrefs
into Stream2::Feeds::Facet::List::Option objects.

=cut

has 'options' => (
    is => 'bare',
    isa => 'ArrayRef[Stream2::Feeds::Facet::List::Option]',
    default => sub {[]},
    writer => '_set_options',
    reader => '_get_options'
);

=head2 max_select

The maximun number of options a user can select at once. Defaults to 1. Use -1 for unlimited.

=cut

has 'max_select' => (
    is => 'rw',
    isa => 'Value',
    default => 1,
);

sub type {
    my ($self) = @_;
    return $self->max_select == 1 ? 'list' : 'multilist';
}

around 'BUILDARGS' => sub {
    my ($orig, $self, %args) = @_;
    $args{options} = $self->_expand_options($args{options});
    return $self->$orig(%args);
};

around 'to_token' => sub {
    my ($orig, $self) = @_;
    my $token = $self->$orig;
    $token->{Options} = [
        map {
            $_->to_arrayref;
        } @{$self->options}
    ];

    $token->{SelectedValues} = {map { $_ => 1} $self->selected_values};

    return $token;
};

sub options {
    my ($self, $options) = @_;
    return $self->_get_options unless $options;
    $self->_set_options($self->_expand_options($options));
}

sub _expand_options {
    my ($self, $options) = @_;
    return [
        map {
            if(ref $_ eq 'ARRAY') {        
                Stream2::Feeds::Facet::List::Option->from_arrayref( $_ );
            } else {
                $_;
            }
        } @$options
    ];

}

=head1 selected_options

A list all our selected options.

=cut

sub selected_options {
    my ($self) = @_;
    return grep { $_->selected; } @{$self->options};
}

=head2 selected_values

Returns a list of our the values of the selected options.

=cut

sub selected_values {
    my ($self) = @_;
    return map { $_->value; } $self->selected_options;
}

sub set_token {
    my ($self, $values) = @_;
    $values = ref $values ? $values : [$values];
    foreach my $option (@{$self->options}) {

        my $selected = List::Util::any {
            $_ eq $option->value;
        } @$values;

        $option->selected($selected);
    }
}
1;

package Stream2::Feeds::Facet::Text;

use Moose;

=head1 NAME

Stream2::Feeds::Facet::Text

=head1 DESCRIPTION

A text facet. Inherits from L<Stream2::Feeds::Facet>

=cut

extends 'Stream2::Feeds::Facet';

=head1 PROPERTIES

=head2 type

The facet type: "text".

=cut

has 'type' => (
    is => 'ro',
    isa => 'Str',
    default => 'text'
);

=head2 value

The current value of this facet

=cut

has 'value' => (
    is => 'rw',
    isa => 'Maybe[Str]',
);

=head2 set_value($value);

Sets $value as the value for this facet

=cut

sub set_value {
    my ($self, $value) = @_;
    if(ref $value eq 'ARRAY') {
        $value = $value->[0];
    }
    $self->value($value);
}

around 'to_token' => sub {
    my ($orig, $self) = @_;
    return if $self->default || $self->credential;
    my $token = $self->$orig;
    $token->{Value} = $self->value;
    return $token;
};

=head2 set_token($value)

Backwards compaitble. Sets the facet value from the value.

=cut

sub set_token {
    my ($self, $value) = @_;
    $value = $value->[0] if ref $value;
    $self->value($value);
}

__PACKAGE__->meta->make_immutable;

1;

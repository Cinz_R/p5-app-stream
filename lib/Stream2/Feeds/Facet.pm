package Stream2::Feeds::Facet;

=head1 NAME

Stream2::Feeds::Facet

=head1 DESCRIPTION

Facet base class

=cut

use Moose;

=head1 PROPERTIES

=head2 label

The facet's name

=cut


has 'label' => (
    is => 'rw',
    isa => 'Str',
);

=head2 mandatory

Whether it is mandatory for users to enter a value for this facet

=cut

has 'mandatory' => (
    is => 'rw',
    isa => 'Bool'
);

=head2 sortmetric

=cut

has 'sortmetric' => (
    is => 'rw',
    isa => 'Num',
    default => 999,
);

=head2 credential

Whether the facet is for a subscription

=cut

has 'credential' => (
    is => 'ro',
    isa => 'Bool',
    default => 0,
);

=head2 default

True if the facet is a default search facet

=cut

has 'default' => (
    is => 'ro',
    isa => 'Bool',
    default => 0
);

=head1 METHODS

=head2 type

returns the facet type

=cut
sub type {
    ...
}

=head2 to_token

Returns a backwards compatible "token_ref" of this token

=cut

sub to_token {
    my ($self) = @_;
    return undef if $self->credential || $self->default;
    return {
        Label      => $self->label,
        Type       => $self->type,
        Id         => $self->label,
        SortMetric => $self->sortmetric,
    };
}

=head2 set_token

Backwards compatible. Set the value of this facet

=cut
sub set_token {
    ...
}

__PACKAGE__->meta->make_immutable;

1;

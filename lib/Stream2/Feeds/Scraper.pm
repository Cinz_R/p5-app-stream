package Stream2::Feeds::Scraper;

=head1 Stream2::Feeds::Scraper

An HTML & XML XPath scraper used by L<Stream::Engine::Composition>

=head1 SYNOPSIS

    my $html = <<'EOF';
    <!doctype html>
    <html>
        <head>
            <title>Hello WorldE<lt>/title>
        </head>
        <body>
            <h1>WelcomeE<lt>/h1>
            <nav>
                <a href="/">HomeE<lt>/a>
                <a href="/cv_search">Search CVsE<lt>/a>
                <a href="/login">Log inE<lt>/a>
            </nav>
        </body>
    </html>
    EOF

    my $instructions = {
        nav => {
            _path => './/nav',
            cv_form_href => './a[contains(text(), "Search CVs"]/@href',
            login_form => './a[contains(text(), "Search CLog inVs"]/@href',
        }
    };
    my $scraper = Stream2::Feeds::Scraper->new;
    my $results = $scraper->($self, $html, $instructions, ['Example']) = @_;

=cut

use Moose;
use XML::LibXML;
use Scalar::Util qw();

use Log::Any qw/$log/;

=head2 Methods

=head3 br2nl_coderef

Returns a CODE reference which converts <br /> tags to newlines.

Usage:

    my $cv = $scraper->br2nl_coderef;
    ...TODO

=cut

sub br2nl_coderef {
    my $self = shift;
    return sub {
        my $nodes = shift;

        my $doc = $nodes->get_node(0)->ownerDocument;
        foreach my $node ($nodes->get_nodelist) {
            foreach my $br ($node->find('br')->get_nodelist) {
                $br->parentNode->insertBefore(
                    $doc->createTextNode("\n"),
                    $br
                );
                $br->parentNode->removeChild($br);
            }
        }
    };
}

=head3 scrape_xml($xml, $instructions, $breadcrumbs)

Scrapes the C<$xml> string using C<$instructions> and returns the result. The arrayref C<$breacrumbs> indicaates a nested scrape keys.

=cut

sub scrape_xml {
    my ($self, $xml, $instructions, $breadcrumbs) = @_;
    my $dom = XML::LibXML->load_xml(
        string => $xml,
        recover => 2,
    );
    return $self->scrape_node($dom->documentElement, $instructions, $breadcrumbs);
}

=head3 scrape_html($html, $instructions, $breadcrumbs)

C<scrape_xml> HTML equivalent

=cut

sub scrape_html {
    my ($self, $html, $instructions, $breadcrumbs) = @_;
    my $dom = XML::LibXML->load_html(
        string => $html,
        recover => 2,
    );
    return $self->scrape_node($dom->documentElement, $instructions, $breadcrumbs);
}

=head3 scrape_node($node, $instruction, $breadcrumbs)

Scrapes a L<XML:LibXML::Node> with the given C<$instruction> and $breadcrumbs.

=cut

sub scrape_node {
    my ($self, $node, $instruction, $breadcrumbs) = @_;
    if( ! ref $instruction ) {
        $instruction = { _path => $instruction };
    }
    my %instruction = %$instruction;
    $breadcrumbs ||= [];

    my $path = delete $instruction{_path};
    $path ||= '.';


    my $result_nodes = $node->find($path);

    my $result = 'nothing';
    if($result_nodes->isa('XML::LibXML::NodeList')) {
        $result = sprintf('Found %d nodes', $result_nodes->size);
    } elsif($result_nodes->isa('XML::LibXML::Literal')) {
        $result = sprintf('Found literal "%s"', $result_nodes);
    }
    $log->debugf(
        "XPATH %s => %s\n%s",
        join(' > ', @$breadcrumbs),
        $path,
        $result
    );

    if( my %children = %instruction ) {
        return [
            $result_nodes->map(sub {
                my $subnode = shift;
                my $result = {};
                while( my ($key, $sub_instruction) = each %children) {
                    $result->{$key} = $self->scrape_node(
                        $subnode, $sub_instruction, [@$breadcrumbs, $key]
                    );
                }
                return $result;
            })
        ];
    }

    return $result_nodes;
}

=head3 finalize($result)

Normalizes scrape result HashRef C<$result>. Recursively converts the following items:

=over

=item C<XML::LibXML::Literal> - becomes its plain values

=item C<XML::LibXML::Node> - becomes XPath's 'normalize-space' applied to this node

=item C<XML::LibXML::NodeList> - becomes an ArrayRef of finalized C<XML::LibXML::Node> objects

=item C<HashRef> - C<finalize()> is run against this value

=item C<ArrayRef> - becomes an ArraRef with C<finalize()> applied to each item

=back

=cut

sub finalize {
    my ($self, $result) = @_;
    my $attributes = {};
    while( my ($key, $value) = each %$result) {
        my $output;

        if(Scalar::Util::blessed($value)) {
            if($value->isa('XML::LibXML::NodeList')) {
                $output = [
                    map { $self->_finalize_node($_) } $value->get_nodelist
                ];
            } elsif($value->isa('XML::LibXML::Node')) {
                $self->_finalize_node($_);

            } elsif($value->isa('XML::LibXML::Literal')) {

                $output = $value->value;

            } else {
                $output = "$value";
            }
        } elsif( ref $value eq 'HASH' ) {
            # Finalize unblessed hashrefs
            $output = $self->finalize($value);
        } elsif(ref $value eq 'ARRAY') {
            $output = [
                map {
                    $self->finalize($_);
                } @$value
            ];
        } else {
            $output = $value;
        }
        $attributes->{$key} = $output;
    }
    return $attributes;
}

sub _finalize_node{
    my ($self, $node) = @_;
    return $node->find('normalize-space()')->value;
}

__PACKAGE__->meta->make_immutable;

1;

=head2 Scrape instructions structure

Todo: Explanation for instructions

=over

=item You get what you scraped for

=item Be explicit about what you want. Use stringifying functions if desired.

=back

=head2 Scrape result structure

Todo: Explain the scrape result structure

=over

=item Everything is an XML::LibXML::* object

=item non-literal values are always in a list of sort

=back

=cut
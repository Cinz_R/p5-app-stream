package Stream2::O::UserGroupsetting;

use Moose;
extends qw/Stream2::O::UserSetting/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::UserGroupsetting;

=head1 NAME

 Stream2::Schema::Result::UserGroupsetting - A User Setting

=cut

has '+o' => ( isa => 'Stream2::Schema::Result::UserGroupsetting' );

__PACKAGE__->meta->make_immutable();


package Stream2::O::LongList;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use DateTime;

use Stream2::Schema::Result::LongList;

has '+o' => ( isa => 'Stream2::Schema::Result::LongList' );

=head1 NAME

 Stream2::Schema::Result::LongList - A long list

=cut

around 'user' => sub{
    my ($orig, $self) = @_;
    return $self->stream2->factory('SearchUser')->wrap($self->$orig());
};

=head2 add_candidate

Adds a L<Stream2::Results::Result> to this Longlist. If the candidate
is already there, this will just update its added date and replace the row
with this freshest value, nowing that the full history of a row will
be kept in the candidate_action_log

=cut

sub add_candidate{
    my ($self, $candidate) = @_;
    unless( $candidate->isa('Stream2::Results::Result') ){
        confess("Invalid class for $candidate. Should be a Stream2::Results::Result");
    }

    my $stream2 = $self->stream2();

    my $stuff = sub{
        my $now = DateTime->now();
        if( my $already = $self->candidates()->find({ candidate_id => $candidate->id() }) ){
            $already->candidate_content( $candidate->to_ref() );
            $already->insert_datetime( $now );
            $already->update();
            return $already;
        }
        return $self->candidates->create({ candidate_id => $candidate->id() , candidate_content => $candidate->to_ref() });
    };

    return $stream2->stream_schema->txn_do($stuff);
}


__PACKAGE__->meta->make_immutable();


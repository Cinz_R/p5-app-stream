package Stream2::O::OnlineFile;

use Moose;

use Digest::SHA1 qw/sha1_hex/;
use File::Path;
use File::Slurp;
use File::Spec;
use File::stat qw();

use Log::Any qw/$log/;

=head1 NAME

 Stream2::O::OnlineFile - An online file and its local properties

=cut

has 'factory' => ( is => 'ro' , isa => 'Stream2::Factory::OnlineFile', required => 1);


has 'url'          => ( is => 'ro', isa => 'Str', required   => 1 );
has 'disk_file'    => ( is => 'ro', isa => 'Str', lazy_build => 1 );
has 'content_type' => ( is => 'ro', isa => 'Str', lazy_build => 1 );
has 'mime_type'    => ( is => 'ro', isa => 'Str', lazy_build => 1 );
has 'url_hash'     => ( is => 'ro', isa => 'Str', lazy_build => 1 );
has 'has_error'    => ( is => 'ro', isa => 'Int', lazy_build => 1 );


# holds the file size as reported from a HEAD request.
has '_file_size_from_head_request' => ( is => 'rw', isa => 'Maybe[Num]', predicate => '_has_file_size_from_head_request');

# Where the magic lives.
# something like { disk_file => ... , mime_type => ... }
has '_cached_entry' => ( is => 'ro', isa => 'HashRef[Str]' , lazy_build => 1,  predicate => '_has_cached_entry' );

sub _build_url_hash{
    my ($self) = @_;
    return sha1_hex($self->url());
}


sub _build_disk_file{
    my ($self) = @_;
    return $self->_cached_entry->{disk_file};
}

sub _build_content_type{
    my ($self) = @_;
    return $self->_cached_entry->{content_type};
}

sub _build_mime_type{
    my ($self) = @_;
    my $content_type = $self->content_type();
    my ( $mime ) = ( $content_type =~ /^(\S+)/ );
    return $mime;
}

sub _build_has_error{
    my ($self) = @_;
    return $self->_cached_entry->{has_error};
}


#
# This is where the magic lives.
#
sub _build__cached_entry{
    my ($self) = @_;

    my $stream2 = $self->factory()->stream2();
    my $entry_json = $self->factory()->chi()
      ->compute( $self->url_hash(),
                 undef, # Default options please
                 sub{
                     # Call this just in case.
                     $self->factory->recover_filespace();
                     # The goal is to produce a
                     # { disk_file => ... , mime_type => .. } from the URL

                     # The file on disk is just from the hash of the SHA plus a unique string
                     # This is to avoid expired cache entry to point to the same filename ever again.
                     my $filename = sha1_hex( $self->url_hash().$stream2->uuid->create_str() );

                     my @dir_fragments = ( $filename =~ /^(.{2})(.{2})/ );

                     # Also , we want to clear old (older than 3 hours) files every hour.
                     my $now = DateTime->now();
                     my $filedir = File::Spec->catdir( $self->factory()->cache_dir(), $now->strftime('%Y%m%d%H'), @dir_fragments );

                     # Vivify the directory
                     unless( -d $filedir ){
                         File::Path::make_path( $filedir );
                     }

                     # Put the file in its directory
                     $filename = File::Spec->catfile( $filedir , $filename);

                     $log->debug("File name is $filename");
                     $log->debug("Getting ".$self->url()." to put innit. Innit");

                     my $request = HTTP::Request->new( GET => $self->url() );
                     my $response = $self->factory()->user_agent()->request($request);
                     unless( $response->is_success ){
                         # Response is wrong.
                         # Produce a text file that will explain everything
                         $log->error("Request on ".$self->url()." went wrong. Look into ".$filename." or look at that:".$response->as_string("\n"));
                         File::Slurp::write_file( $filename, { binmode => ':raw' },  $request->as_string("\n")."\n".$response->as_string("\n") );
                         return $stream2->json->encode({ disk_file => $filename, content_type => 'text/plain', file_size => File::stat::stat($filename)->size, has_error => 1 });
                     }else{
                         File::Slurp::write_file( $filename, { binmode => ':raw' }, $response->content());
                         my $content_type = $response->content_type();
                         unless( $content_type ){
                             $log->warn("Cannot find content_type for url ".$self->url()." Falling back to application/octet-stream");
                             $content_type = 'application/octet-stream';
                         }
                         return $stream2->json->encode({ disk_file => $filename , content_type => $content_type, file_size => File::stat::stat($filename)->size, has_error => 0 });
                     }
                 });

    $log->debug("Got entry ".$entry_json." for URL ".$self->url());
    my $entry = $stream2->json->decode($entry_json);

    # Protect against gone files...you never know.
    unless( -f $entry->{disk_file} ){
        # File is not there. Eject the key and call this again.
        $self->factory->chi()->remove($self->url_hash());
        return $self->_build__cached_entry();
    }
    return $entry;
}

=head2 file_size

returns the size of the file in bytes.  The files size is comes from the
content_length via a HEAD request, or from the size of the file after it has
been fetched.

=cut


sub file_size {

    my ($self) = @_;

 # if the a _cached_entry exists we should take the file size of that over the
 # file size reported back from the remote server via HEAD request.
    return $self->_cached_entry->{file_size} if $self->_has_cached_entry;

    return $self->_file_size_from_head_request if $self->_has_file_size_from_head_request;

  # no need to download the file to get its size, we can ask the remote server
  # to tell us how big the file is
    my $request = HTTP::Request->new( HEAD => $self->url() );
    my $response = $self->factory()->user_agent()->request($request);
    return $self->_file_size_from_head_request( $response->headers->content_length )
        if ( $response->is_success );

    return $self->_file_size_from_head_request(undef);
}

__PACKAGE__->meta->make_immutable();

package Stream2::O::SearchUser;

use Moose;

extends qw/Stream2::O::BaseDBIC/;

use Stream2::Schema::Result::CvUser;

use Stream2::Action::UserAdoptData;

# Some legacy stuff to remove when we decide to refuctor subscriptions
use Stream2::SearchUser::Subscriptions;

use Data::Dumper;
use Encode;
use Email::Address;
use List::Util ();

use HTTP::Request::Common;
use MIME::Base64 qw/encode_base64/;

use String::CamelCase;
use Scalar::Util qw//;

use Class::Load;

use Log::Any qw/$log/;

has '+o' => ( isa => 'Stream2::Schema::Result::CvUser' );
has 'subscriptions_ref' => ( is => 'rw' , isa => 'HashRef', lazy_build => 1);

#
# The Ripple settings of this user.
# They usually live in the session, though they can also leave
# in other places when backend code requires them.
#
has 'ripple_settings' => ( is => 'rw', isa => 'HashRef', required => 1 );

has 'hierarchical_path' => ( is => 'ro', isa => 'Str', lazy_build => 1 );
has 'oversees_hierarchical_path' => ( is => 'ro', isa => 'Str', lazy_build => 1 );

# a path like "/Search2/installation_id/company_name/user_id" that identifies
# this user and the installation of Search2 they're running in
has installation_path => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    builder => '_build_installation_path',
);

=head1 PROPERTIES

Here are some properties that are supported by the underlying DBIC object:

=head2 contact_name

Returns this user's contact name according to the in memory contact details
or to what is in the database.

=head2 contact_email

Returns this user's contact email according to what's in the database.

Will crash if it cannot find a database row to fallback to

=head2 contact_details

A Hash of contact_details as returned by providers. Typically contain things like:

       {
         contact_name => .. Full name ..,
         contact_telephone => .. Guess ..,
         contact_email => .. Standard email address ..,
         feedback_email => .. for watchdogs and maybe other stuff .. ,
         contact_devuser => .. the broadbean Juice connected office username.. ,
         currency => .. Preferred currency ..,
         locale => .. Preferred locale (aka language code) ..,
         timezone => .. Preferred timezone standard string .. ,
         distance_unit => .. Preferred distance unit ..,
         company => .. Identifier string of the user's group (understand company in adcourier for instance) .. ,
         company_nice_name => .. The 'nice name' of a users group ..
      };


=head2 locale

Returns this user's locale accorting to whats stored in the database

Locales will be returned as specified under user settings in Juice

E.g:

    zh     => Chinese
    da     => Danish
    nl     => Dutch
    en_AU  => English (AU)
    en     => English (GB)
    en_HK  => English (HK)
    en_JP  => English (JP)
    en_NZ  => English (NZ)
    en_SG  => English (SG)
    en_usa => English (US)
    fr_CA  => French (Canada)
    fr     => French (Custom)
    fr_CH  => French (Standard)
    de     => German
    it     => Italian
    ja     => Japanese
    pl     => Polish
    pt_BR  => Portuguese (Brazil)
    es     => Spanish
    sv     => Swedish

=head2 currency

Returns this user's currency accorting to whats stored in the database

This property is determined based on the users lang/locale. The code will load the corresponding
language class that has the currency code specified by the key '_DEFAULT_SALARY'

These currency values are hard coded in the language classes under: lib/Bean/Locale/{__LANG__}.pm

See Bean::Locale::localise_in() to see how this works.

Will return a string like:
    GBP or EUR or USD


=head2 timezone

Returns this user's timezone accorting to whats stored in the database

Timezones will be returned as specified under the users office settings in Juice

Please see: https://www.adcourier.com/admin/office/office_contact_details.cgi

E.g:

    Europe/Dublin
    Europe/London
    Australia/Sydney
    America/Los_Angeles
    EST
    MET
    WET
    EST5EDT

=head2 distance_unit

Returns this user's distance_unit accorting to whats stored in the database

Distance units are based on the users locale and are defined in the Bean lib:
Bean::Locale::localise_in( $locale, '_TP_DISTANCE_UNIT');

Returns a string, either: 'km' or 'miles'

=cut


# Pure very liberal attributes. For now ..
foreach my $pure_memory_attribute ( 'is_overseer' ,
                                    'navigation',
                                    'is_admin',
                                  ){
    has $pure_memory_attribute => ( is => 'rw' );
}

has 'settings_values_hash' => ( is => 'ro' , isa => 'HashRef' , lazy_build => 1 );

has 'login_provider' => ( is => 'ro' , isa => 'App::Stream2::Plugin::Auth::Provider::Base', lazy_build => 1 );
has 'identity_provider' => ( is => 'ro', isa => 'App::Stream2::Plugin::Auth::Provider::Base', lazy_build => 1);

sub _build_identity_provider{
    my ($self) = @_;
    $log->debug("Building provider for ".$self->provider());
    return $self->stream2()->build_provider( $self->provider() );
}

sub _build_login_provider{
    my ($self) = @_;
    my $last_login_provider = $self->ripple_settings()->{login_provider};
    unless( $last_login_provider ){
        $last_login_provider = $self->last_login_provider();
        # This is to avoid crashing for everyone on rollback of this feature.
        # after every user has logged in again (in the future), we can replace
        # this by some confession.
        $log->info("No login_provider in ripple_settings. Fallback to last_login_provider = ".$last_login_provider);
    }
    return $self->stream2()->build_provider( $last_login_provider );
}

sub _build_subscriptions_ref{
    my ( $self ) = @_ ;
    my $user_id = $self->id();
    my $subscriptions =
      Stream2::SearchUser::Subscriptions->new({ user => $self,
                                                schema => $self->stream2->stream_schema(),
                                                key => $self->subscription_key()
                                              });
    return $subscriptions->ref() // {};
}

sub _build_settings_values_hash {
    my ($self) = @_;
    unless ( $self->id() ) {
        confess("This user does not have a database ID. Cannot fetch settings");
    }
    my $top_settings =
      $self->stream2()->factory('Setting')->build_settings_hash_for_user($self);
    $top_settings->{groupsettings} =
      $self->stream2()
      ->factory( 'Groupsetting', { group_identity => $self->group_identity() } )
      ->build_settings_hash_for_user($self);
    return $top_settings;
}

around 'discard_changes' => sub{
    my ($orig, $self, @args ) = @_;
    $self->clear_settings_values_hash();
    $self->clear_login_provider();
    $self->clear_identity_provider();
    return $self->$orig(@args);
};

around 'longlists' => sub{
    my ($orig, $self, @args) = @_;
    return $self->stream2()->factory('LongList' , { dbic_rs => scalar( $self->$orig(@args) ) });
};


=head2 adopt_data_from

Adopts most of the data from the given user.
It is safe to delete the other user once his method has completed.

Note that the other user should be from the same group_identity.

Usage:

 $this->adopt_data_from($other_user);

=cut

sub adopt_data_from{
    my ($self, $other_user) = @_;
    my $action = Stream2::Action::UserAdoptData->new({ stream2 => $self->stream2,
                                                       user_object => $self,
                                                       ripple_settings => $self->ripple_settings(),
                                                       from_user => $other_user,
                                                   });
    return $action->instance_perform();
}


=head2 conform_to_sibling_settings

Wipe out all this users specific setting and inherit from its sibblings.

Call that in the shell for a given user for testing or support.

=cut

sub conform_to_sibling_settings{
    my ($self) = @_;

    my $stuff = sub{
        $log->debug("Deleting all settings for this user");
        $self->user_settings->delete();
        $log->debug("Inheriting siblings settings");
        $self->inherit_siblings_settings();
        $log->debug("Reloading user ID = ".$self->id());
        $self->discard_changes();
    };
    return $self->stream2->stream_schema->txn_do($stuff);
}

=head2 inherit_siblings_settings

Inherit direct sibbling settings (at product and at group level). This is intended to prevent brand new users to be stuck
with the default values of settings until an admin go through the setting panel and give
them some settings manually.

=cut

sub inherit_siblings_settings{
    my ($self) = @_;
    unless( $self->id() ){
        confess("This user does not have a database ID. Cannot fetch settings");
    }

    my $siblings_ids = [ $self->siblings_ids() ];
    $self->stream2()->factory('Setting')->inherit_siblings_settings_for_user($self , $siblings_ids);
    $self->stream2()->factory('Groupsetting' , { group_identity => $self->group_identity() } )->inherit_siblings_settings_for_user($self, $siblings_ids);
    return;
}


sub user_id{
    my ($self) = @_;
    # confess("Just use id please");

    $log->error("Deprecated user_id. Please use ->id ");
    return $self->id();
}

=head2 is_adcourier

True if this user is an adcourier user.

=cut

sub is_adcourier{
    my ($self) = @_;
    return $self->provider() =~ /^adcourier/;
}


=head2 adcourier_user_id

Returns the ADCOURIER user ID of this user. Barfs if the provider
is not adcourier compatible.

Usage:

 my $adcourier_user_id = $this->adcourier_user_id();

=cut

sub adcourier_user_id{
    my ($self) = @_;
    unless( $self->provider() =~ /^adcourier/ ){
        confess("User provider ".$self->provider()." is not adcourier");
    }
    return $self->provider_id();
}

=head2 exists_remotely

Returns true if this user still exists remotely according to its provider.

=cut

sub exists_remotely{
    my ($self) = @_;
    return $self->identity_provider()->user_exists($self->provider_id());
}


=head2 login_provider

Returns a new instance of a subclass of L<App::Stream2::Plugin::Auth::Provider::Base> matching the
last_login_provider this user has lastly used to connect (Either via an ats integration (ripple) or directly (adcourier).

=cut

=head2 identity_provider

Returns (For now) an instance of a sublass of L<App::Stream2::Plugin::Auth::Provider::Base> based
on the 'provider' property.

=cut

=head2 overseen_hierarchy

 Just redirects to L<App::Stream2::Plugin::Auth::Provider::Base overseen_hierarchy>

=cut

sub overseen_hierarchy{
    my ($self) = @_;
    return $self->identity_provider->user_overseen_hierarchy($self);
}

=head2 siblings_ids

Returns the list of user IDs that are direct siblings on this user.

Usage:

 my @sibling_ids = $this->siblings_ids();

=cut

sub siblings_ids{
    my ($self) = @_;
    return $self->identity_provider->user_siblings_ids($self);
}

=head2 has_adcourier_search

True if this user has got an adcourier search. An adcourier
search is a search internal to a company provided by broadbean
as part of the adcourier product.

At the moment, its only talentsearch or cb_mysupply or candidatesearch

=cut

sub has_adcourier_search{
    my ($self) = @_;
    return List::Util::first {
        $self->has_subscription( $_ );
    } $self->stream2->factory('Board')->list_internal_databases();
}

=head2 subject_prefix_for

Returns what should be a prefix to any emails sent to candidates by this user in the
context of the given destination.

Usage:

 my $prefix = $this->subject_prefix_for( 'adcresponses' );
 my $prefix = $this->subject_prefix_for( 'monsterxml' );

=cut

sub subject_prefix_for{
    my ($self, $destination) = @_;
    my $subject_prefix;

    if ( $destination eq 'adcresponses' ){
        $subject_prefix = $self->settings_values_hash()->{'responses-email_subject_prefix'};
    }
    else {
        $subject_prefix = $self->settings_values_hash()->{'email-subject-prefix'};
    }

    if( $subject_prefix ){
        return $subject_prefix;
    }

    # Get the board info from the user subscription.
    my $board_info = $self->has_subscription($destination);
    if ( $board_info && ( $board_info->{type} eq 'internal' ) ){
        return $self->stream2->__x('Job Opportunity - from {client_name}', client_name => $self->company_nice_name() );
    }

    return $self->stream2->__x('Job Opportunity - {board_nice_name} from {client_name}', board_nice_name => ( $board_info ? $board_info->{nice_name} : '[board_nice_name]' ),
                               client_name => $self->company_nice_name() );
}

=head2 has_subscription

Does this user has a subscription to the given board?

Usage:

 if( $this->has_subscription('talentsearch') ){
    ...
 }

=cut

sub has_subscription{
    my ($self, $subscription_name) = @_;
    return $self->subscriptions_ref()->{$subscription_name};
}

=head2 inject_authtokens

Inject this user auth tokens and group_identity in the given template object.

Dies if this doesn't have such a subscription.

Usage:

 $this->inject_authtokens($template_object);

=cut

sub inject_authtokens{
    my ($self, $template_object) = @_;

    my $subscription = $self->has_subscription($template_object->destination()) // confess("No subscription for $template_object");

    my $auth_tokens = $subscription->{auth_tokens} // {};

    while ( my ($token, $value) = each %$auth_tokens ) {
        $template_object->token_value( $token => $value );
    }

    $template_object->company($self->group_identity());

    return $template_object;
}


=head2 subscription_key

Shortcut to subscription_key in config.

=cut

sub subscription_key{
    my ($self) = @_;
    return $self->factory->subscription_key();
}

=head2 to_hash

Returns a pure Perl hash representation of this user that contains enough
stuff for the interface. This is NOT a full representation of this user, so
do not count on that to 'save the user' anywhere.

This has should NOT contain any blessed object, as the serialization
of it in JSON would mess things up, see
L<https://broadbean.atlassian.net/browse/SEAR-1549>

Feel free to extend that should you need it.

Usage:

 my $structure = $this->to_hash();

=cut

sub to_hash{
    my ($self) = @_;

    # typically these juice settings should be true or false, so they should at the very least be of type numeric!
    my $juice_settings = $self->settings() || {};

    foreach my $juice_setting ( keys %$juice_settings ){
        my $value = $juice_settings->{$juice_setting};
        if( Scalar::Util::blessed( $value ) ){
            $log->debug("Casting $juice_setting to a boolean, as it was a blessed object");
            $juice_settings->{$juice_setting} = $value ? 1  : 0 ;
        }
    }

    # merge local stream settings with juice settings
    my $settings = { %{$self->settings_values_hash}, %$juice_settings };

    return {
            user_id => $self->id(),
            contact_details => $self->contact_details() // {},
            contact_name => $self->contact_name(),
            contact_email => $self->contact_email(),
            locale => $self->locale(),
            currency => $self->currency(),
            timezone => $self->timezone(),
            distance_unit => $self->distance_unit(),
            group_identity => $self->groupclient->identity(),
            group_nice_name => $self->groupclient->nice_name(),
            provider => $self->provider(),
            provider_id => $self->provider_id(),
            is_admin => $self->is_admin(),
            is_overseer => $self->is_overseer(),
            settings => $settings,
            ripple_settings => $self->ripple_settings(),
           };
}

=head2 short_info_hash

A small info hash, suitable for logging context for instance.

=cut

sub short_info_hash{
    my ($self) = @_;
    return {
        id => $self->id(),
        email => scalar( $self->contact_email() ),
        username => $self->group_identity().':'.$self->provider_id().':'.$self->provider()
    };
}


=head2 company_nice_name

Returns the group_nice_name or the group_identity

=cut

sub company_nice_name{
    my ($self) = @_;
    my $group = $self->groupclient();
    return $group->nice_name() || $group->identity();
}


=head2 save

Very clever save and stuff specific to search users.

Will vivify this users group in the Group resultset

=cut

sub save{
    my ($self) = @_;


    # One transaction
    my $txn = sub{

        # Synchronize whats in the specific columns with what could be in the contact_details
        # hash..
        if( my $contact_details = $self->contact_details() ){
            foreach my $contact_key ( qw/contact_name contact_email
                                         locale timezone currency distance_unit/ ){
                unless( $contact_details->{$contact_key} ){ next; }
                $self->$contact_key($contact_details->{$contact_key});
            }
        }

        # Vivify the group in the group factory
        my $group = $self->stream2->factory('Group')->find_or_create({ identity => $self->group_identity(),
                                                                       nice_name => $self->group_nice_name()
                                                                     });

        # Just do an insert or update .. For now.
        $self->update_or_insert();

        # Also save subscriptions.
        Stream2::SearchUser::Subscriptions->new({
                                                 ref     => $self->subscriptions_ref(),
                                                 key     => $self->subscription_key(),
                                                 user => $self,
                                                 schema  => $self->stream2->stream_schema(),
                                                })->save or die "Subscription storage failed";
        return $self->id();
    };

    return $self->stream2->stream_schema->txn_do($txn);
}

=head2 can_add_watchdog

Shortcut to stream2 / watchdogs->user_can_create_watchdog

=cut

sub can_add_watchdog{
    my ($self) = @_;
    return $self->stream2->watchdogs->user_can_create_watchdog( $self );
}

=head2 watchdog_email_address

Returns the email address appropriate for watchdog notifications.

Usage:

 if( my $address = $this->watchdog_email_address() ){
    .. send watchdog notif. to $address ..
 }

=cut

sub watchdog_email_address {
    my ($self) = @_;
    if ( my $contact_details = $self->contact_details ) {
        return $contact_details->{'feedback_email'} || $self->contact_email();
    }
    return $self->contact_email();
}

=head2 has_done_action

True if the candidate has ever done the given action on the given candidate.

Valid actions are:

 candidate_tag_add
 candidate_tag_delete
 delete_note
 download
 forward
 import
 message
 note
 profile
 shortlist_adc_shortlist
 shortlist_beers
 shortlist_cheese_sandwiches
 update

Usage:

 if( $this->has_done_action( 'download' , $candidate ) ){
    ...
 }

=cut

sub has_done_action{
    my ($self , $action , $candidate ) = @_;

    return $self->stream2->factory('CandidateActionLog')
      ->search({ group_identity => $self->group_identity(),
                 candidate_id => $candidate->id(),
                 action => $action, # Up to there. this is indexed
                 user_id => $self->id(),
               }, { rows => 1 })->count();
}


=head2 adcourier_oversees

DEPRECATED: use auth_provider->user_oversees( $user ) instead

Returns a list of adcourier user IDs that this user oversees in the context of adcourier.

Usage:

 my @oversees_adc_ids = $this->adcourier_oversees();

=cut

sub adcourier_oversees{
    my ($self) = @_;

    my @adcourier_ids = ();
    unless( $self->provider() =~ /^adcourier/ ){
        return @adcourier_ids;
    }

    @adcourier_ids = map{ $_->{id} } @{ $self->stream2->user_api->oversees( $self->provider_id(), { active => 1 } ) };
    return @adcourier_ids;
}

=head2 autocomplete_keywords

Find a previously stored keywords for the current user matching the supplied keyword for autocomplete.

Required argument:
   keyword - A string to be used in a prefix match against keywords stored in the database for
             search criteria on the current user

Returns an array reference of matched keywords

=cut

sub autocomplete_keywords {
    my ($self, $kw) = @_;

    my $stream2 = $self->stream2();

    return [
        map { $_->keywords() }
        $stream2->stream_schema->resultset('SearchRecord')->search({ user_id => $self->id() })
        ->search_related( 'criteria',
                          { keywords => { LIKE => "$kw%" } },
                          {
                              columns => [ {'criteria.keywords' => { distinct => 'criteria.keywords' }} ],
                              rows => $stream2->config()->{autocomplete}->{keywords}->{max_results} // 15,
                          },
        )
    ];
}


=head2 ripple_xdoc_custom_fields

Creates a 'CustomFields' element in the given
XML::LibXML::Document with the custom fields from the custom_fields cached data.

=cut

sub ripple_xdoc_custom_fields{
    my ($self, $xdoc) = @_;
    my $xcustom_fields = $xdoc->createElement('CustomFields');

    my $custom_fields = $self->ripple_settings()->{custom_fields} // [];
    foreach my $field ( @$custom_fields ){
        if( $field->{asHttpHeader} ){
            $log->info("Field ".$field->{name}." should be output asHttpHeader");
            next;
        }
        my $xfield = $xcustom_fields->appendChild($xdoc->createElement('CustomField'));
        $xfield->setAttribute('name' , $field->{name});
        $xfield->appendText($field->{content});
    }

    return $xcustom_fields;
}

=head2 ripple_header_custom_fields

Returns a HashRef of { Header-Name => 'Header value' }
according to the current custom fields where asHttpHeader is true.

Note that the values are MIME-Q encoded.

Usage:

 my $custom_headers = $this->

=cut

sub ripple_header_custom_fields{
    my ($self) = @_;
    my $headers = {};
    my $custom_fields = $self->ripple_settings()->{custom_fields} // [];
    foreach my $field ( @$custom_fields ){
        unless( $field->{asHttpHeader} ){
            $log->info("Field ".$field->{name}." should be output in Ripple XML");
            next;
        }

        # don't encode if only contains word characters - in case being used for ids etc and no decoded
        if ( $field->{content} =~/^\w*$/ ){
            $headers->{ $field->{name} } = $field->{content};
        }
        else{
            $headers->{ $field->{name} } = Encode::encode('MIME-Q', $field->{content} );
        }
    }
    return $headers;
}


=head2 custom_stylesheet_url

Returns a custom stylesheet URL if one is set in
the ripple settings (or undef if no such thing exists).

=cut

sub custom_stylesheet_url{
    my ($self) = @_;

    my $stylesheet_url = $self->ripple_settings->{stylesheet_url};
    $log->info("Custom StyleSheet is ".( $stylesheet_url // 'UNDEF' ));
    return $stylesheet_url;
}

=head2 group_branding

Returns the name of the group branding to use based on the current users group identity.
This is usually equal to the group identity value or 'default' if no specific branding should be used
but broadbean branding shouldn't be displayed.

Returns undef when no custom branding is setup for this group identity

When adding new custom branding, please ensure the assets exists under public/static/groupbranding/[group identity]/

The CSS *MUST* be named: public/static/groupbranding/[group identity]/css/s2.css

Please store any images related to the custom branding in public/static/groupbranding/[group identity]/images/

=cut

sub group_branding {
    my ($self) = @_;
    my $group_branding = $self->stream2->stream_schema->resultset('GroupBranding')->find($self->group_identity());

    if($group_branding) {
        return $group_branding->branding();
    }
    return undef;
}


=head2 emit_webhook

Emit a webhook as defined in the search_users account for the given action_name.
Only for users with ripple settings.

Arguments:

C<action_name>              - (String) - The name of the action. e.g. 'candidate/shortlist'

C<candidate_webhook_xmldoc> - (XML::LibXML Object) - Candidate XML generated by Stream2::Results::Result::candidate_xml_to_webhook_xml()

C<advert> (optional) Hashref of the advert.

C<ripple_settings> Required to work. The ripple settings this method is called under.

Returns an arrayref of all enqueued jobs.

=cut

sub emit_webhook {
    my ($self, $action_name, $candidate, $xdoc, $advert ) = @_;

    # Will be calculated only if needed (in the loop).
    my $candidate_webhook_xmldoc = undef;
    my $jobs = [];

    foreach my $webhook (@{ $self->ripple_settings->{webhooks} // [] }) {
        next unless defined $webhook->{url};

        ## Note we calculate this only once.
        $candidate_webhook_xmldoc //= $candidate->candidate_xml_to_webhook_xml("candidate/shortlist", $xdoc, $self, $advert);

        my $candidate_webhook_xml;
        # as well as the CV we can also append a parsed XML documenta
        # why do it in here? As there may be multiple webhooks per candidates and each one may have a different doc type
        # we need to add the tag doc to our XML object, serialise and then remove the tag doc again
        if ( my $tagdoc_type = $webhook->{config}->{tagdoc_type} ){
            my $tag_doc_node = $candidate_webhook_xmldoc->createElement('Doc');
            $tag_doc_node->setAttribute('EncodingType', 'Base64');
            $tag_doc_node->setAttribute('MimeType' , 'application/xml');
            $tag_doc_node->setAttribute('FileName' , $candidate->name() . '_tagged_cv.xml');

            my $xml_string = '';

            if ( $tagdoc_type eq 'hr_xml' ) {
                $tag_doc_node->setAttribute("DocumentType", "HR");
                $xml_string = $candidate->hr_xml();
            }
            else {
                $tag_doc_node->setAttribute("DocumentType", "BGT");
                $xml_string = $candidate->bg_xml();
            }

            my ( $xml_encoding ) = ( $xml_string =~ m/encoding=['"]([\w-]+)/ );
            unless( $xml_encoding ){
                $log->warn("Embedded XML has got no encoding. Falling back to UTF-8");
                $xml_encoding = 'UTF-8';
            }
            $log->info("Will encode embedded XML in '$xml_encoding'");
            # Base64 doesnt like string. It likes bytes. And to comply with the standard,
            # it should not be line broken (empty string eol param)
            $tag_doc_node->appendText( MIME::Base64::encode_base64( Encode::encode( $xml_encoding , $xml_string ) , '' ) );

            $candidate_webhook_xmldoc->findnodes('StreamCandidate/Candidate')->[0]->appendChild( $tag_doc_node );
            $candidate_webhook_xml = $candidate_webhook_xmldoc->toString(1);
            $candidate_webhook_xmldoc->removeChild( $tag_doc_node );
        }
        else {
            $candidate_webhook_xml = $candidate_webhook_xmldoc->toString(1);
        }

        my $job = $self->stream2->qurious->create_job(
            class => "Stream2::Action::EmitWebhook",
            queue => "BackgroundJob",
            parameters => {
                url           => $webhook->{url},
                action_name   => $action_name,
                candidate_xml => encode_base64($candidate_webhook_xml), #serialise
            },
        );

        $log->info(sprintf("Enqueuing Webhook. Action: %s, URL: %s", $action_name, $webhook->{url}));
        {
            my $shortened_xml = $candidate_webhook_xml;
            $shortened_xml =~ s|(<Doc.*?>)(.+?)(</Doc>)|"$1<!-- removed to reduce log size: ".length($2)." bytes -->$3"|esg;
            $log->trace("Enqueuing Webhook Candidate XML: " . $shortened_xml) if $log->is_trace();
        }

        $job->enqueue();
        push(@{$jobs}, $job);
    }

    return $jobs;
}

=head2 set_cached_data

Same as set_cached_value, but for pure Perl data structure. The given
data structure MUST be jsonable.

Usage:

 $this->set_cached_data('some_key' , { some => 'data', structure => [ 1 , 2 ] } );

=cut

sub set_cached_data{
    my ($self, $key , $data_struct, @rest) = @_;
    my $json = $self->stream2->json->encode($data_struct);
    return $self->set_cached_value($key, $json , @rest);
}

=head2 get_cached_data

Same as get_cached_value, but returns a pure Perl data structure (or undef)
if no such thing is found.

Usage:

 my $data_structure = $self->get_cached_data('some_key');

=cut

sub get_cached_data{
    my ($self, $key, @rest) = @_;
    my $data = $self->get_cached_value($key, @rest);
    unless( $data ){ return undef; }
    return $self->stream2->json->decode($data);
}

=head2 set_cached_value

Sets a value against this users specific cache space with the specified expiry.

If the expiry is not specified, the key never expire.

This is just a decoration around L<CHI::set> on the L<Stream2::users_cache>

Usage:

  $this->set_cached_value('somekey' , 'some_data', 60); # Expires in 60 seconds.
  $this->set_cached_value('somekey' , 'some_data'); # Never expires.

=cut

sub set_cached_value{
    my ($self, $key , $data, $expires_in) = @_;
    $key // confess("Missing key");
    $expires_in //= 'never';

    my $user_id = $self->id();
    unless( $user_id ){ confess("No id in this $self. Make sure its loaded from the DB"); }

    return $self->stream2->users_cache->set($user_id.'-'.$key , $data, $expires_in);
}

sub get_cached_value{
    my ($self, $key ) = @_;
    $key // confess("Missing key");
    my $user_id = $self->id();
    unless( $user_id ){ confess("No id in this $self. Make sure its loaded from the DB"); }

    return $self->stream2->users_cache->get($user_id.'-'.$key);
}

sub _build_hierarchical_path {
    my ( $self ) = @_;
    return $self->identity_provider()->get_hierarchical_identifier( $self );
}

sub _build_oversees_hierarchical_path {
    my ( $self ) = @_;
    return $self->identity_provider()->get_oversees_hierarchical_identifier( $self );
}

sub _build_installation_path {
    my ( $self ) = @_;
    return sprintf '/Search2/%s/%s/%s',
        $self->stream2->config->{installation_id},
        $self->group_identity,
        $self->id;
}

=head2 parse_cv_with_locale

Users can hard-set the desired locale they want their CVs to be parsed with in AdCourier.
This method respects that setting, if the setting doesn't exist it falls back to detection.

Usage:

    my $parsed_cv_ref = $this->parse_cv_with_locale( $cv_binary_content );
    my $parsed_cv_ref = $this->parse_cv_with_locale( $cv_binary_content, { conversion => 'hr_xml' } );

=cut

sub parse_cv_with_locale {
    my ( $self, $cv_content, $options ) = @_;
    $options //= {};

    # specify what doc_type we want back
    unless ( $options->{conversion} ) {
        my $tagdoc_type = $self->ripple_settings->{tagged_doc_type} // '';
        if ( $tagdoc_type eq 'BGT' ) {
            $options->{conversion} = 'bg_xml';
        }
        else { # prefer HR XML
            $options->{conversion} = 'hr_xml';
        }
    }

    if ( my $bg_lang = $self->tagger_locale() ) {
        $options->{tagger_locale} = $bg_lang;
    }

    $log->info('Parsing CV with options: ' . Dumper($options));
    return $self->stream2->cvp_api->parse_cv_contents($cv_content, $options);
}

=head2 tagger_locale

Returns the Tagger Locale. AKA BG Language Pack.
in xx_xx format (note that the second part is lower case),
or undef if no such thing is found.

Note that only 5 'pseudo Adcourier locale' are supported:

uk au us fr nl.

When adcourier supports other pseudo locale, we will have to
enrich this thing.

=cut

sub tagger_locale {
    my ( $self ) = @_;

    my $settings = $self->settings()
        or return;
    my $lang = $settings->{responses}->{bg_language_pack}
        or return;

    return {
        uk      => 'en_gb',
        au      => 'en_au',
        us      => 'en_us',
        fr      => 'fr_fr',
        nl      => 'du_nl',
        detect  => '',
    }->{$lang};
}

=head2 tsimport_tagger_locale

Because the tsimport API wants the locale to be different?

Usage:

    my $tsimport_locale = $user->tsimport_tagger_locale();

Out:

    'en_GB'

=cut

sub tsimport_tagger_locale {
    my ( $self ) = @_;

    if ( my $locale = $self->tagger_locale() ){
        $locale =~ s/-/_/;
        if ( $locale =~ m/_/ ) {
            $locale =~ s/([^_]+)$/uc($1)/e;
        }
        return $locale;
    }

    return;
}

=head2 tinymce_images

will be a dbic relation to a table in the future

=cut

sub tinymce_images {
    my ( $self ) = @_;
    return (
        FakeImage->new({
            id => 1,
            filename => 'kitten.png',
            name => 'Kitten'
        })
    );
}

=head2 response_flags

Get a user's available adcresponse flags

Usage:

    my $flags_ref = $this->response_flags();

Out:

    [
        {
            type => 'standard', # or custom
            flag_id => 7,
            setting_mnemonic => '....',
            group_identity => '...',
            description => 'Candidate is good',
            colour_hex_code => '#ff00ee'
        },
        ...
    ]

=cut

sub response_flags {
    my ( $self ) = @_;

    my $flag_factory = $self->stream2->factory('AdcresponsesCustomFlag');
    my @flags = $flag_factory->search({
        group_identity => $self->group_identity,
    })->all();

    return [
        map {
            {
                type => 'standard',
                %$_,
                flag_id => ($_->{flag_id} * 1),
            }
        } $flag_factory->standard_flags, map {
            {
                $_->get_columns,
                setting_mnemonic => $_->setting_mnemonic,
                type => 'custom'
            }
        } @flags
    ];
}

=head2 quota_summary

Gets the available quotas for each chargeable action for the board provided.
Pass nothing if you want quotas for each of the users boards.

=cut

sub quota_summary {
    my ($self, $single_board) = @_;
    my $boards = $single_board ? [ $single_board ] : [ keys %{$self->subscriptions_ref} ];
    my $quota_ref = $self->stream2->quota_object->fetch_quota_summary({
        path => $self->hierarchical_path,
        boards => $boards
    });

    # make sure the quota value is numeric
    foreach my $board ( keys(%$quota_ref) ) {
        foreach my $type ( keys(%{$quota_ref->{$board}}) ) {
            $quota_ref->{$board}->{$type} += 0;
        }
    }

    return $quota_ref;
}

=head2 is_zombie

Returns true if a user is considered to be a zombie
aka, is their office set to 'zombie' (case sensitive)

=cut

sub is_zombie {
    my ($self) = @_;
    my $contact_details = $self->contact_details;
    if ( $contact_details && ( $contact_details->{office} // '' ) eq 'zombie' ) {
       return 1
    }
    return;
}


=head2 parse_email_address_for_forwarding

Returns a valid email

=cut

sub parse_email_address_for_forwarding {
    my ( $self, $email_candidate, $opts ) = @_;

    # Only attempt to build a valid email if the user setting
    # that allows it is on or you are in the responses channel.
    my $settings = $self->settings();

    if (   $settings->{can_forward_to_any_email_address}
        || $opts->{is_responses_channel} )
    {
        # Email::Address->parse returns an array
        my ($well_formed_email) = Email::Address->parse($email_candidate);

        # return early if the email is not well well formed.
        return unless $well_formed_email;

        my $user_settings = $self->settings_values_hash();
        if ( my $domain_restrictions =
            $user_settings->{'forwarding-restrict_recipient_domain'} )
        {
            if (
                List::Util::any { lc $_ eq lc $well_formed_email->host() }
                split( /[,\s]/, $domain_restrictions )
              )
            {
                return $well_formed_email->address;
            }
            # if the email address is not on the white lite
           return
        }
        return $well_formed_email->address;

    }

    return;
}


{
    package FakeImage;
    sub new { bless pop, shift }
    sub id { shift->{id} }
    sub name { shift->{name} }
    sub filename { shift->{filename} }
}

__PACKAGE__->meta->make_immutable();

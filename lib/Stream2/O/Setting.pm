package Stream2::O::Setting;

use Moose;
extends qw/Stream2::O::SettingBase/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::Setting;

use DateTime;

=head1 NAME

 Stream2::Schema::Result::Setting - A Setting

=cut


has '+o' => ( isa => 'Stream2::Schema::Result::Setting' );

with qw/Stream2::Role::O::I18NSettingMeta/;

sub build_usersetting_factory{
    my ($self) = @_;
    $self->stream2->factory('UserSetting');
}



__PACKAGE__->meta->make_immutable();

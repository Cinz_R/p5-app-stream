package Stream2::O::WatchdogResult;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::WatchdogResult;

has '+o' => ( isa => 'Stream2::Schema::Result::WatchdogResult' );

=head1 NAME

Stream2::Schema::Result::WatchdogResult - Wrapper around a pure DBIC watchdog result with extra stuff.

=cut

=head2 earlier

Returns a resultset of earlier results for the same subscription (including myself).

=cut

sub earlier{
    my ($self, $factory) = @_;

    ## Note: If this has been built in a transaction, and
    ## accessed outside a transaction you will have
    ## to give a new factory to earlier for the following search to work.
    ## At least in the test.

    $factory ||= $self->factory();

    my $dtf = $factory->stream2->stream_schema->storage->datetime_parser;

    return $factory->search({watchdog_id => $self->watchdog_id,
                             subscription_id => $self->subscription_id,
                             insert_datetime => { '<= ' => $dtf->format_datetime($self->insert_datetime()) }
                            });
}

=head2 backfill_from_v1

Backfill the content of this from the V1 result.

Errors are recorded in the content.

Usage:

 $this->backfill_from_v1();

 if( my $err = $this->content()->{backfill_error} ){
    ..
 }

=cut

sub backfill_from_v1{
    my ($self) = @_;


    my $id_string = join('-' , $self->id());

    $log->info("Backfilling Watchdog Result $id_string");

    my $content = $self->content() // {};
    my $v1_candidate_id = $content->{v1_candidate_id};

    unless( $v1_candidate_id ){
        $log->warn("This $id_string is already backfilled or does not have any v1_candidate_id");
        return;
    }

    my $new_content = eval{ $self->factory->stream2->stream_api->get_candidate( $v1_candidate_id ); };
    if( my $err = $@ ){
        $self->content({ %$content , v1_backfill_error => $err.'' });
        $self->update();
        $log->warn("Error getting V1 WatchogResult backfill for $id_string : ".$err);
        return;
    }

    $self->content($new_content);
    $self->update();

}

__PACKAGE__->meta->make_immutable();


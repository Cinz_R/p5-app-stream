package Stream2::O::RecurringReport;

use Moose;
extends qw/Stream2::O::BaseDBIC/;
use Stream2::Schema::Result::RecurringReport;

use Log::Any qw/$log/;

use Encode;

has '+o' => ( isa => 'Stream2::Schema::Result::RecurringReport' );

sub BUILD{
    my ($self) = @_;
    $log->info("Recurring Report ".$self->id()." will be a ".$self->class());
    my $class = Class::Load::load_class(  $self->class() );
    $class->meta->apply($self, rebless_params => $self->settings() );
    return $self;
}

=head2 run_this

Run this report,  and calculate the next_run_datetime after it has run successfully

=cut

sub run_this{
    my ($self) = @_;

    $log->info("Running recurring report ".$self->id());

    # Update the next run date whatever happens.
    my $next_month = DateTime->now();
    $next_month->add( months => 1 )->truncate( to => 'month' );
    $self->next_run_datetime($next_month);
    $self->update();

    $log->info("Will run next at ".$self->next_run_datetime());

    my $files = $self->stream2
        ->report_tech([ $self->id() ],
                      sub{
                          my ($s2 , $report_id) = @_;
                          my $report = $s2->factory('RecurringReport')->find($report_id);
                          return $report->generate_files();
                      }
                  );
    unless( $files ){
        $log->error("Recurring report ".$self->id().":Files were not generated");
        return undef;
    }

    my @users = $self->users()->all();

    my @recipients  =  map{ $_->contact_email() } grep { $_->contact_email() } @users;
    if( @recipients ){
        my $recipient_str = join(',', @recipients);
        $log->info("Sending report to $recipient_str");

        my $body = q|Hello,

Here is your monthly report (|.$self->class().q|)

See attached files.

|;

        my $email = $self->stream2()->factory('Email')->build([
            From       => 'noreply@broadbean.net',
            To         => $recipient_str,
            Type       => 'text/plain; charset=UTF-8',
            Encoding   => 'quoted-printable',
            Subject    => Encode::encode('MIME-Q', 'Your monthly report about '.$self->class()),
            'X-Stream2-Transport' => 'static_transport' , # We want this to go through the static IP transport
            Data       => Encode::encode('UTF-8', $body)
        ]);

        # And.. Attach the files!
        foreach my $file ( @$files ){
            $email->attach( Path => $file->disk_file(),
                            Type => $file->mime_type(),
                            Encoding => 'base64',
                            Disposition => 'attachment',
                            Filename    => Encode::encode('MIME-Q', $file->name())
                        );
        }
        $self->stream2()->send_email($email);
    }else{
        $log->error("Recurring report: No valid recipients ID = ".$self->id());
    }
    return $files;
}

__PACKAGE__->meta->make_immutable();

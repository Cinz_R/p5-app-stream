package Stream2::O::GroupMacro;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use DateTime;

use Stream2::O::MacroContext;
use Stream2::Schema::Result::GroupMacro;
use Scalar::Util qw//;

has '+o' => ( isa => 'Stream2::Schema::Result::GroupMacro' );

has 'email_template' => ( is => 'ro', isa => 'Maybe[Stream2::O::EmailTemplate]' , lazy_build => 1);

=head2 email_template

The email_template that is related to executing this macro. This goes through email_template_rule

=cut

sub _build_email_template{
    my ($self) = @_;
    my $template_rule = $self->email_template_rule();
    unless( $template_rule ){
        return undef;
    }
    return $self->stream2()->factory('EmailTemplate')->find( $template_rule->email_template_id() );
}

=head2 execute_this

Executes this macro in Lisp in the given L<Stream2::O::MacroContext> (or arguments to build it).

Returns a L<Language::LispPerl> atom, which is the result of the evaluation of the lisp_source.

Example:

 my $res = $this->execute_this( { user => $user } );

=cut

sub execute_this{
    my ($self, $context) = @_;
    unless( $context ){
        confess("Missing macro context");
    }
    unless( Scalar::Util::blessed( $context ) ){
        $context = Stream2::O::MacroContext->new({
            stream2 => $self->stream2(),
            %$context
        });
    }

    return $self->stream2->with_context({ macro_context => $context },
                                        sub{
                                            return $context->evaler()->eval( $self->lisp_source() );
                                         }
                                     );
}


__PACKAGE__->meta->make_immutable();


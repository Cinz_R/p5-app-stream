package Stream2::O::Usertag;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::Usertag;

=head1 NAME

 Stream2::Schema::Result::Tag - A Usertag

=cut

has '+o' => ( isa => 'Stream2::Schema::Result::Usertag' );

__PACKAGE__->meta->make_immutable();

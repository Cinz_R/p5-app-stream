package Stream2::O::Watchdog;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::Watchdog;

has '+o' => ( isa => 'Stream2::Schema::Result::Watchdog' );

=head1 NAME

 Stream2::Schema::Result::Watchdog - A watchdog

=cut

around 'user' => sub{
    my ($orig, $self) = @_;

    return $self->stream2->factory('SearchUser')->wrap($self->$orig());
};

__PACKAGE__->meta->make_immutable();


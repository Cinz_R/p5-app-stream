package Stream2::O::CandidateActionLog;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::CandidateActionLog;

has '+o' => ( isa => 'Stream2::Schema::Result::CandidateActionLog' );

=head2 user

Returns a Stream2::O::SearchUser

=cut

sub user{
    my ($self) = @_;
    return $self->stream2->factory('SearchUser')->wrap($self->cv_users());
}

=head2 pure_candidate_id

Returns the pure candidate ID as it was returned by the destination (feed aka template)

=cut

sub pure_candidate_id{
    my ($self) = @_;
    my $destination = $self->destination();
    my $pure_id = $self->candidate_id();
    $pure_id =~ s/^$destination//;
    $pure_id =~ s/^_//;
    return $pure_id;
}

=head2 is_adcourier_shortlist

True if this action represents a shortlisting
against an adcourier job.

Usage:

 if ( $this->is_adcourier_shortlist() ){
    ...
 }

=cut

sub is_adcourier_shortlist{
    my ($self) = @_;
    return $self->is_shortlist_type('adc_shortlist');
}

=head2 is_shortlist_type

Returns true if this action represents a shortlisitng of the given type.

Usage:

 if( $this->is_shortlist_type('adc_shortlist') ){
    ...
 }

=cut

sub is_shortlist_type{
    my ($self, $type) = @_;
    return $self->action() eq 'shortlist_'.$type;
}

=head2 adc_advert_id

Returns the Adcourier Advert ID (the advert_id), or
extracted from the advert_link.

=cut

sub adc_advert_id{
    my ($self) = @_;

    unless( $self->is_adcourier_shortlist() ){ return; }

    my $advert_id = $self->data()->{advert_id};
    if( $advert_id ){ return $advert_id; }

    # No advert ID, check the advert_link
    my $advert_link = $self->data()->{advert_link};
    unless( $advert_link ){
        $log->warn("No advert link in data for action ID = ".$self->id());
        return;
    }
    ( $advert_id ) = ( $advert_link =~ m|/api/cheesesandwiches/adc_shortlist/(\d+)/preview| );
    return $advert_id;
}

=head2 cancel

Cancels this action (in case it is a future action).

Usage:

 if( my $cancelled = $this->cancel() ){
    .. action is freshly cancelled ..
 }else{
    .. action was already cancelled in the past.
 }

Note that this goes bang if you try to cancel a action that is not a future.

=cut

sub cancel{
    my ($self) = @_;
    unless( $self->action() =~ /^candidate_future/ ){
        confess("Cannot cancel an action that is not a future");
    }
    unless( $self->data()->{longstep_process_id} ){
        $log->info("No longstep_process_id in this action. Cannot cancel. Already cancelled in the past?");
        return 0;
    }
    my $stuff = sub{
        $self->stream2->longsteps()->find_process( delete $self->data()->{longstep_process_id} )->delete();
        # No longstep_process_id anymore.
        $self->data({ %{$self->data()}, cancelled_on => DateTime->now()->iso8601().'Z' } );
        $self->update();
        return 1;
    };
    return $self->stream2()->stream_schema->txn_do( $stuff );
}

__PACKAGE__->meta->make_immutable();

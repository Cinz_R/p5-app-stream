package Stream2::O::SavedSearch;

use Moose;

extends qw/Stream2::O::BaseDBIC/;

use Stream2::Schema::Result::SavedSearches;

has '+o' => ( isa => 'Stream2::Schema::Result::SavedSearches' );

=head unsave

Funky name for 'deactivate'

Usage:

 $this->unsave();

=cut

sub unsave{
    my ($self) = @_;

    $self->active(0);
    $self->update();
}

__PACKAGE__->meta->make_immutable();

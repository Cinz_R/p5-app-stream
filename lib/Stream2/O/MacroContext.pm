package Stream2::O::MacroContext;

use Moose;

use Stream2::O::MacroContext::CandidateContainer;

with qw/Stream2::Role::Freezable/;

=head1 NAME

Stream2::O::MacroContext - A transient context for macro execution.

=cut

has 'stream2' => ( is => 'ro', isa => 'Stream2' , required => 1);

has 'evaler' => ( is => 'ro', isa => 'Language::LispPerl::Evaler' , required => 1, lazy_build => 1);

# Set at building time.
has 'user' => ( is => 'ro', isa => 'Stream2::O::SearchUser',  required => 1 );
has 'action_context' => ( is => 'ro', isa => 'HashRef' , required => 0);

# Operation attributes
has 'messages' => ( is => 'ro', isa => 'ArrayRef', default => sub{ []; } );

has 'candidate_container' => ( is => 'ro', isa => 'Stream2::O::MacroContext::CandidateContainer', lazy_build => 1 );

sub _build_evaler{
    my ($self) = @_;
    return $self->stream2()->lisp()->get_evaler();
}

sub _build_candidate_container{
    my ($self) = @_;
    return Stream2::O::MacroContext::CandidateContainer->new({
        stream2 => $self->stream2(),
        %{$self->action_context()}
    });
}


=head2 to_hash

Turns this context into a PURE perl data structure.

Usage:

 my $context_hash = $this->to_hash();

=cut

sub to_hash{
    my ($self) = @_;
    return {
        evaler => $self->evaler()->to_hash(),
        user_id => $self->user()->id(),
        # No objects here. This is meant to be frozen.
        action_context => $self->stream2->scrubber()->scrub_objects( $self->action_context ),
        messages => $self->messages(),
    };
}

=head2 from_hash

Given a stored PURE perl hash (given by 'to_hash'), this will return
a new perfectly useable Stream2::O::MacroContext to execute macros with.

Usage:

  my $context = Stream2::O::MacroContext->from_hash( $hash, { stream2 => $stream2 } );

=cut

sub from_hash{
    my ($class, $hash , $attributes) = @_;
    my $new_context = $class->new({
        %{$hash},
        evaler => Language::LispPerl::Evaler->from_hash( $hash->{evaler} ),
        user => $attributes->{stream2}->find_user( $hash->{user_id} ),
        %{$attributes}
    });
    # For the evaler
    # Make sure the Stream2 Lisp Core code is loaded.
    require Stream2::Lisp::Core;
    # Make sure the Secure role is applied to the buildins.
    $new_context->evaler()->builtins()->apply_role('Stream2::Lisp::SecureBuiltIn');
    return $new_context;
}

=head2 add_message

Adds the given message to this context. Usually used to report when macro is done.

Usage:

  $this->add_message("Something went on");

=cut

sub add_message{
    my ($self, $message) = @_;
    push @{$self->messages()}, $message;
}

__PACKAGE__->meta->make_immutable;


package Stream2::O::SettingBase;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use DateTime;

=head1 NAME

 Stream2::Schema::Result::Setting - A base class for Setting and Groupsetting

=cut

# Note that this need to be overriden in the base classes.
has '+o' => ( isa => 'Stream2::Schema::Result::Setting' );

sub build_usersetting_factory{
    confess("Implement me");
}

=head2 default_value_hash

 Returns a value hash like { boolean_value => .. }

=cut

sub default_value_hash{
    my ($self) = @_;
    return {
        boolean_value => $self->default_boolean_value(),
        string_value => $self->default_string_value()
    };
}

=head2 default_value

A single default value, according to this setting_type.

=cut

sub default_value{
    my ($self) = @_;
    return $self->get_column('default_'.$self->setting_type().'_value');
}



=head2 get_group_identity_values

Returns a structure like that for all the users of the given group_identity

    {
        "default": { boolean_value: false },
        "map": [
            {
                value: { boolean_value: true },
                users: [ 3 , 4 , 10 , 11 , ... ]
            }
         ]
    };

=cut

sub get_group_identity_values {
    my ( $self, $group_identity, $provider ) = @_;

    $provider       // confess("Missing provider");
    $group_identity // confess("Missing group_identity");

    # Get the explicit values
    my %values        = ();
    my $user_settings = $self->build_usersetting_factory()->search(
        {
            'me.setting_id'       => $self->id(),
            'user.provider'       => $provider,
            'user.group_identity' => $group_identity,
            'me.'
              . $self->setting_type()
              . '_value' => { '!=', $self->default_value() }
        },
        {
            join     => 'user',
            prefetch => 'setting'
        }
    );
    while ( my $user_setting = $user_settings->next() ) {
        my $value = $user_setting->value();
        $values{$value} //=
          { value => $user_setting->value_hash(), user_ids => [] };
        push @{ $values{$value}->{user_ids} }, $user_setting->user_id();
    }

    return {
        default   => $self->default_value_hash(),
        users_map => [
            map {
                +{
                    value => $values{$_}->{value},
                    users => $values{$_}->{user_ids}
                };
            } sort keys %values
        ]
    };

}

=head2 set_users_value

Have the doer user set the given user IDs with given value. Return the number of DB operations done.

=cut

sub set_users_value {
    my ( $self, $doer, $user_ids, $value ) = @_;
    $doer // confess("Missing doer");
    $user_ids //= [];
    $value // confess("value cannot be undefined");

    unless ( $self->valid_value($value) ) {
        confess( sprintf( "Value is not valid: %s", $value ) );
    }

    my $self_id         = $self->id();
    my $group_identity  = $doer->group_identity();
    my $provider        = $doer->provider();
    my $schema          = $self->stream2->stream_schema();
    my $user_setting_rs = $self->build_usersetting_factory()->dbic_rs();

    ## Type safe comparison
    if ( $self->default_value() . '' eq $value . '' ) {

        # Just remove the user setting rows
        # This is equivalent to setting to the default value
        $self->_go_through_slices(
            $user_ids,
            sub {
                my ($slice) = @_;
                $user_setting_rs->search(
                    {
                        'me.setting_id'       => $self_id,
                        'me.user_id'          => { -in => $slice },
                        'user.group_identity' => $group_identity,
                        'user.provider'       => $provider,
                    },
                    { join => 'user' }
                )->delete();
            }
        );
        return;
    }

    # The value we want to set is NOT a default value.

# A bit of security. ALL the user_ids MUST belong to users of the same group as the doer.
    $self->_go_through_slices(
        $user_ids,
        sub {
            my ($slice) = @_;
            if (
                $schema->resultset('CvUser')->search(
                    {
                        id  => { -in => $slice },
                        -or => [
                            { group_identity => { '!=' => $group_identity } },

                            # { provider => { '!=' => $provider } } # Relax this
                        ]
                    }
                )->count()
              )
            {
                confess("Security issue. One of users in "
                      . join( ',', @$slice )
                      . " does not belong to group_identity = '$group_identity' or to the same provider = '$provider'"
                );
            }
        }
    );

    # Cached stuff
    my $doer_id = $doer->id();
    my $doer_ip = $doer->last_ip() // '127.0.0.1';
    my $now     = DateTime->now();
    my $now_iso = $now->iso8601();

    # Ok, the value is NOT the default.
    # Calculate the set to update and the set to insert.

# We need to update the rows that are already there regardless of their value
# as this is mainly to avoid any uniqueness issue. We will update
# the ip and update datetime in the process only if we are setting a different value
    my @to_update = ();
    $self->_go_through_slices(
        $user_ids,
        sub {
            my ($slice) = @_;
            push @to_update,
              map { $_->user_id() } $user_setting_rs->search(
                {
                    'me.setting_id'       => $self_id,
                    'me.user_id'          => { -in => $slice },
                    'user.group_identity' => $group_identity,
                    'user.provider'       => $provider,
                },
                {
                    join   => 'user',
                    select => ['me.user_id']
                }
              )->all();
        }
    );

    my %in_to_update = map { $_ => 1 } @to_update;
    my @to_insert = grep { !$in_to_update{$_} } @$user_ids;

    # Update the to update rows.
    $self->_go_through_slices(
        \@to_update,
        sub {
            my ($slice) = @_;
            $user_setting_rs->search(
                {
                    'me.setting_id'       => $self_id,
                    'me.user_id'          => { -in => $slice },
                    'user.group_identity' => $group_identity,
                    'user.provider'       => $provider,
                    $self->setting_type . '_value' => { '!=' => $value }
                },
                {
                    join   => 'user',
                    select => ['me.user_id']
                }
              )->update(
                {
                    $self->setting_type . '_value' => $value,
                    admin_user_id                  => $doer_id,
                    update_datetime                => \'NOW()',
                    admin_ip                       => $doer_ip
                }
              );
        }
    );

    # Insert the to insert
    $self->_go_through_slices(
        \@to_insert,
        sub {
            my ($slice) = @_;
            $user_setting_rs->populate(
                [
                    map {
                        +{
                            setting_id                     => $self_id,
                            user_id                        => $_,
                            admin_user_id                  => $doer_id,
                            admin_ip                       => $doer_ip,
                            insert_datetime                => $now_iso,
                            $self->setting_type . '_value' => $value
                          }
                    } @$slice
                ]
            );
        }
    );

}

# Go through a long list in slices of 50
sub _go_through_slices{
    my ($self, $array_ref , $sub) = @_;
    $array_ref //= [];
    $sub //= sub{};
    my @array = @{$array_ref};
    while( my @slice = splice( @array , 0 , 50 ) ){
        &$sub(\@slice);
    }
}


sub valid_value {
    my ( $self, $value ) = @_;

    if ( $self->setting_type eq 'boolean' ){
        if ( $value.'' ne '1' && $value.'' ne '0' ){
            return 0;
        }
    }

    return 1;
}

__PACKAGE__->meta->make_immutable();

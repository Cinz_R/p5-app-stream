package Stream2::O::BaseDBIC;

use Moose;

use DBIx::Class::Row;

# The underlying DBIC Row object
has 'o' => ( is => 'ro' , required => 1 , isa => 'DBIx::Class::Row' , handles => qr/.+/ );

has 'factory' => ( is => 'ro', required => 1 );

=head2 to_raw_hashref

 Returns a raw hashref representing the underlying DBIC object.

=cut

sub to_raw_hashref{
    my ($self) = @_;
    my %hash = $self->o()->get_columns;
    return \%hash;
}

=head2 stream2

Shortcut to stream2 through the factory.

=cut

sub stream2{
    my ($self) = @_;
    return $self->factory->stream2();
}


__PACKAGE__->meta->make_immutable;

package Stream2::O::Groupsetting;

use Moose;
extends qw/Stream2::O::SettingBase/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::Groupsetting;

use DateTime;

=head1 NAME

 Stream2::Schema::Result::Groupsetting - A Setting at group level

=cut


has '+o' => ( isa => 'Stream2::Schema::Result::Groupsetting' );

with qw/Stream2::Role::O::I18NSettingMeta/;

sub build_usersetting_factory{
    my ($self) = @_;
    return $self->stream2->factory('UserGroupsetting');
}

__PACKAGE__->meta->make_immutable();

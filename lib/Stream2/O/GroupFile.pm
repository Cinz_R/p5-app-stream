package Stream2::O::GroupFile;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use DateTime;

use Stream2::Schema::Result::GroupFile;

has '+o' => ( isa => 'Stream2::Schema::Result::GroupFile' );

=head2 uri

An uri for this public file.

Usage:

  print $this->uri();

=cut

sub uri{
    my ($self) = @_;
    my $uri = $self->stream2()->factory('PublicFile')->find($self->public_file_key)->uri();

    # the default scheme is http which gives mixed scheme type errors/warnings on the front end
    # s3 supports https
    $uri->scheme( 'https' );
    return $uri;
}

=head2 around delete

Just mark this as deleted and fix its name for something unique.

=cut

around 'delete' => sub {
    my ( $orig, $self ) = @_;
    $self->name( 'DELETED-'.$self->stream2()->uuid->create_str().'-'.$self->name() );
    $self->deleted_datetime( DateTime->now() );
    $self->update();
    # Note that we do not do the original :)
    $log->infof( 'Marked as deleted image file %s for group %s', $self->name, $self->group_identity );
};

__PACKAGE__->meta->make_immutable();


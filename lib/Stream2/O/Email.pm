package Stream2::O::Email;

use Moose;

use Log::Any qw/$log/;

use Encode;
use MIME::Entity;

use File::Spec;

use Data::Dumper;

=head1 NAME

Stream2::O::Email - A Decorator around a L<MIME::Entity> with more methods

=cut

has 'entity' => (
    is       => 'ro',
    isa      => 'MIME::Entity',
    required => 1,
    handles  => qr/.*/
);

has 'factory' => (
    is       => 'ro',
    isa      => 'Stream2::Factory::Email',
    required => 1
);

=head2 clone

Duplicates this. AKA clone.

Usage:

 my $cloned_entity = $self->clone();

=cut

sub clone {
    my ($self) = @_;
    return $self->factory->create( $self->entity()->dup() );
}

=head2 process_with_stash

Processes this entity with the given stash, replacing the [blabla] variables
in all appropriate parts. Changes this entity IN PLACE

Appropriate parts are:

 text/* Bodies.
 MONO-OCCURENCE Headers.

Usage:

   eval{
      $this->process_with_stash( { foo => 'bar' } );
    }
    if( my $err = $@ ){
     print "Some error has occured processing this entity: ".$error;
   }

Returns this, and dies with a simple string describing the error in case of error.

=cut

sub process_with_stash {
    my ( $self, $stash ) = @_;
    $stash //= {};

    # Our template toolkit toolkit
    my $ttt = $self->factory->stream2()->templates();

    # First change the bodies.
    $self->walk_through_text_bodies(
        sub {
            my ( $mime_entity, $body_string ) = @_;
            my %local_stash = %{$stash};

            if ( $mime_entity->effective_type() =~ /html/ ) {

                # A bit dirty, but will work for flat stash
                # Re explore this if we need a non flat stash
                foreach my $key ( keys %local_stash ) {
                    next if $key eq 'advert_link'; # Encoding would stop this being shown as a link
                    $local_stash{$key} =
                      HTML::Entities::encode_entities( $local_stash{$key} );
                }
            }
            if ( my $err =
                $ttt->validate_simple( $body_string, \%local_stash ) )
            {
                die $err . "\n";    # Avoid line numbers.
            }
            return $ttt->render_simple( $body_string, \%local_stash );
        }
    );

    # Then the headers that can be interpolated and encoded.
    my $head = $self->head();
    foreach my $tag ( 'Subject' ) { # Note we only interpolate and encode the Subject header.
        if ( $head->count($tag) == 1 ) {

            # Mono occurrence tag. Will interpret
            my $value = Encode::decode( 'MIME-Header', $head->get($tag) );
            if ( my $err = $ttt->validate_simple( $value, $stash ) ) {
                die $err . "\n";
            }
            my $new_value = $ttt->render_simple( $value, $stash );
            $head->replace( $tag, Encode::encode( 'MIME-Q', $new_value ) );
        }
    }

    # we don't want to mess about with the encoding of email address bearing headers
    foreach my $tag ( 'Reply-To' ) {
        if ( $head->count( $tag ) == 1 ){
            my $value = $head->get($tag);
            if ( my $err = $ttt->validate_simple( $value, $stash ) ) {
                die $err . "\n";
            }
            $head->replace( $tag, $ttt->render_simple( $value, $stash ) );
        }
    }


    return $self;
}

=head2 one_header_value

Returns the one value of the given header name (or undef if
there is no such thing).

Usage:

 my $reply_to = $this->one_header_value('Reply-To');

=cut

sub one_header_value {
    my ( $self, $header_name ) = @_;

    my $value = $self->entity()->head()->get( $header_name, 0 );
    unless ($value) { return undef; }

    $value = Encode::decode( 'MIME-Header', $value );
    chomp($value);
    return $value;
}

=head2 one_html_body

Returns the first html body it finds in the entity (or undef if no such thing exists)

Usage:

 my $html_body = $this->one_html_body();

=cut

sub one_html_body {
    my ($self) = @_;

    my $html_body;
    $self->walk_through_text_bodies(
        sub {
            my ( $mime_entity, $body_string ) = @_;
            if ( $mime_entity->effective_type() =~ /html/ ) {
                $html_body //= $body_string;    # Only set that once.
            }

            # Note that this is subobtimal as this will walk though
            # all the bodie. We do not expect the number
            # of bodies to be anywhere big, so it should just be fine.
            return $body_string;
        }
    );
    return $html_body;
}

=head2 append_html

Appends the given bit of HTML to all
the HTML Entities in this entity.

Usage:

 $this->append_html('<p>Some common footer</p>');

=cut

sub append_html {
    my ( $self, $html ) = @_;

    $self->walk_through_text_bodies(
        sub {
            my ( $entity, $content ) = @_;
            unless ( $entity->effective_type() =~ /html/ ) {
                return $content;
            }

            # Time to parse some HTML and return some new content.
            my $xdoc = $self->factory->stream2()->html_parser()->load_html(
                string   => Encode::encode( 'UTF-8', $content ),
                encoding => 'UTF-8'
            );

            # This being parsed as HTML will ALWAYS have a body node.
            # Cause that's how XML::LibXML load_html behaves.
            my $body_node = ( $xdoc->findnodes('//body') )[0];

            my $fragment =
              $self->factory->stream2()->html_parser()
              ->parse_balanced_chunk( Encode::encode( 'UTF-8', $html ),
                'UTF-8' );
            $xdoc->adoptNode($fragment);
            $body_node->appendChild($fragment);

            # This callback should return a string always.
            $xdoc->setEncoding('UTF-8');
            return Encode::decode( 'UTF-8', $xdoc->toStringHTML() );
        }
    );
}

=head2 walk_through_text_bodies

Walks through the body(s) of this entity and apply the given body_treatment function
to their content.

Example:

   $this->walk_through_text_bodies(sub{
         my ($body_entity , $content );

         # Body entity is a MIME::Entity
         # Do stuff with $content

         return $new_content;
   });

Returns nothing.

=cut

sub walk_through_text_bodies {
    my ( $self, $body_treatment ) = @_;

    # Default body treatment is to do notin
    $body_treatment //= sub {
        my ( $mime_entity, $body ) = @_;
        return $body;
    };

    my $mime_entity_object = $self->entity();

    my @parts = $mime_entity_object->parts();
    foreach my $mime_part (@parts) {
        my $part = $self->factory->create($mime_part);
        $part->walk_through_text_bodies($body_treatment);
    }

    my $effective_type = $mime_entity_object->effective_type();

    # Only text/* entities are interesting for us here.
    if ( $effective_type !~ /^text\// ) {
        return;
    }

    my $body = $mime_entity_object->bodyhandle();

    unless ($body) {
        $log->warn("No body for this text/* entity. Weird");
        return;
    }

    # We get the string in case we want to change it.
    my $content_type = $mime_entity_object->head->get('Content-Type');
    my ($charset) = ( $content_type =~ m/charset=(\S+)/ );
    $charset //= 'UTF-8';    # Default to UTF-8

    my $string = &{$body_treatment}
      ( $mime_entity_object, Encode::decode( $charset, $body->as_string() ), );

    # Write this string to the body
    my $iobody = $body->open("w");
    $iobody->print( Encode::encode( $charset, $string ) );
    $iobody->close();
    return;
}

=head2 transform_entities

Transform this entity in another one using the given builder sub.

Warning: This method CAN have side effect on the current entity if you do not clone
the given mime_entity in the callback. For efficiency reason, this is what you
probably want to do, but you need to be aware of it if you mix that with some
long term storage issues.

Usage:

 my $other_mime = $this->transform_entities(sub{ my ($mime_entity) = @_; # A L<Stream2::O::Email>
                                              return $another_mime_entity; # Another L<Stream2::O::Email> or the same one.
                                            });

=cut

sub transform_entities {
    my ( $self, $mime_builder ) = @_;

    # Default mime_builder is identity.
    $mime_builder //= sub { return shift; };

    my $body_handle = $self->bodyhandle();
    if ($body_handle) {

        # This is a leaf (not a multipart)
        return &{$mime_builder}($self);
    }

    my @parts     = $self->parts();
    my @new_parts = ();
    foreach my $mime_part (@parts) {

        # Subparts. Call this recursively.
        my $part = $self->factory->create($mime_part);
        push @new_parts, $part->transform_entities($mime_builder)->entity();
    }
    $self->parts( \@new_parts );
    return $self;
}

=head2 resolve_images

Go through the document, resolving images  on the way
and generating correct multipart/related structure.

You just have to provide a function that resolves a URL into a LOCAL file path.
That means that you CAN NOT store a resolved entity in an application wide storage.

So really you want to do this AS LATE AS POSSIBLE before sending such an entity.

You are encourage to implement a caching mechanism of some sort in the provided function.

Note that this WILL HAVE A SIDE EFFECT and change this entity in place. Again,
do not mess with application wide storage after you call this.

Usage:

 my $new_entity = $self->resolve_images(sub{
                                         my ($image_url) = @_;

                                         .. do your local file magic ..

                                         return { local_file => '/path/on/the/local/disk',
                                                  mime_type => 'image/jpeg'
                                                 };
                                       });


=cut

sub resolve_images {
    my ( $self, $image_resolver ) = @_;

    # Default is identity
    $image_resolver //= sub {
        return {
            local_file => File::Spec->devnull(),
            mime_type  => 'image/jpeg'
        };
    };

    my $stream2 = $self->factory->stream2;

    # Note that we need to uuid this. This is because clients like
    # outlook would reuse images named the same way accross different emails.
    # Leading to some confusion.
    my $image_prefix = $stream2->uuid->create_str();
    my $image_id     = 0;
    my %local_srcs   = ();

    return $self->transform_entities(
        sub {
            my ($s2_entity) = @_;
            if ( $s2_entity->effective_type() !~ /text\/html/ ) {

                # Not html leave it just as it is
                return $s2_entity;
            }

            # This is a text HTML entity.
            # Parse and go through the img elements to alter their source
            my $xdoc = $stream2->html_parser()->load_html(
                string =>
                  Encode::encode( 'UTF-8', $s2_entity->one_html_body() ),
                encoding => 'UTF-8'
            );
            my @img_nodes = $xdoc->findnodes('//img[string(@src)]');

            unless (@img_nodes) {

                # Nothing to resolve in this html.
                return $s2_entity;
            }

            my $container = $s2_entity->entity();

            # Use the make_multipart to scrub off the non MIME headers,
            # keeping the To, From etc.. (see MIME::Entity::make_multipart)
            $container->make_multipart('related');

            # The original (well, sort of..) entity is not the first part
            # of this container.
            $s2_entity = ( $container->parts() )[0];

            my @container_parts = ();

            foreach my $img_node (@img_nodes) {
                my $src = $img_node->getAttribute('src');
                unless ( $src =~ /^http/ ) {

                    # Do not mess with non http URLs
                    next;
                }
                $log->debug("Will resolve Image URL=$src");

                # Already resolved?
                if ( my $local_src = $local_srcs{$src} ) {
                    $log->debug("Already resolved $src to $local_src");
                    $img_node->setAttribute( 'src', $local_src );
                    next;
                }

                # Not already resolved.
                my $file_record = &{$image_resolver}($src);
                $log->debug( "Now resolved to " . Dumper($file_record) );

                my $image_part_id = $image_prefix . '-' . $image_id . '';
                # this is about building parts of a email and does not need to
                # be completed with building a s2:O:email object.
                my $image_part = MIME::Entity->build(
                    Path     => $file_record->{local_file},
                    Type     => $file_record->{mime_type},
                    Encoding => 'base64',
                    Id       => $image_part_id,
                );
                push @container_parts, $image_part;

                $local_srcs{$src} = 'cid:' . $image_part_id;
                $img_node->setAttribute( 'src', $local_srcs{$src} );
                $image_id++;
            }

            # The Xdoc now contains right references.
            # Set the body of this main html part.
            $xdoc->setEncoding('UTF-8');
            my $body = $s2_entity->bodyhandle();
            my $io   = $body->open('w');
            $io->print( $xdoc->toStringHTML() );
            $io->close();

            $container->preamble(
                [
                        "HTML Email and "
                      . ( scalar(@container_parts) - 1 )
                      . " embeded images\n"
                ]
            );

            # Inject all part in the container
            map { $container->add_part($_) } @container_parts;
            return $self->factory->create($container);
        }
    );
}

=head2 entity_size

Returns the size of the $self->entity in bytes. If a $email_size_threshold is
passed in, this will die when this is excceded.

  my $entity_size = $s2_entity->entity_size();

or

  my $email_size_threshold = (1024 * 1024); # 1MB

  my $entity_size = eval {
    $s2_entity->entity_size( $email_size_threshold );
  };
  if ($@) {
    warn $@;
  }

=cut

sub entity_size {
    my ( $self, $email_size_threshold ) = @_;

    my $working_size_total = 0;
    my %local_srcs;
    my $stream2 = $self->factory->stream2();

    # multipart/related means we have already resolved the images, and
    # base64'ed it so just grab the size of the string, else lets walk though
    # the entity and estimate the email size
    if ( $self->entity->effective_type() =~ /multipart\/related/ ) {
        $working_size_total +=
          length( Encode::encode_utf8( $self->entity->as_string ) );

        die $stream2->__x("Email exceeds the maximum size.\n")
          if ( $working_size_total
            && $working_size_total > $email_size_threshold );
        return $working_size_total;
    }

    # always return the $string, so not to change the base entity.
    $self->walk_through_text_bodies(
        sub {
            my ( $entity, $string ) = @_;

            # add the byte size of the text
            my $string_length = length( Encode::encode_utf8($string) );

            # protect against empty strings
            unless ($string_length) {
                $working_size_total +=
                  length( Encode::encode_utf8( $entity->as_string ) );
                return $string;
            }
            $working_size_total += $string_length;

            # Now we are only interested in html, for the images sizes.
            return $string
              if ( $entity->effective_type() !~ /text\/html/ );

            my $xdoc = $stream2->html_parser()->load_html(
                string   => $string,
                encoding => 'UTF-8'
            );
            my @img_nodes = $xdoc->findnodes('//img[string(@src)]');

            #  there be no images here lets skip.
            return $string unless (@img_nodes);

            foreach my $img_node (@img_nodes) {
                my $image_url = $img_node->getAttribute('src');

                return $string unless ( $image_url =~ /^http/ );

                # if the images shows up multiple times the email is only a
                # about 20 bytes to the overall email size, so there is no need
                # to add the file size again,
                # HOWEVER, we will add a nominal number of bytes as this stops
                # someone creating a 4GB email with just one image.

                if ( $local_srcs{$image_url} ) {
                    $working_size_total += 20;
                    return $string;
                }

                my $onlineimage =
                  $stream2->factory('OnlineFile')->find($image_url);

                my $file_size = $onlineimage->file_size;

                # should we throw an error here if the image has a size of 0?
                $local_srcs{$image_url} = $file_size ? $file_size : 0;
                $working_size_total += $local_srcs{$image_url};

              # we check the to see if this image has put the template over a
              # given max template size but if on is not given we can skip this.

                die $stream2->__x("Email exceeds the maximum size.\n")
                  if ( $email_size_threshold
                    && $working_size_total > $email_size_threshold );
                return $string;

            }
        }
    );

    # final check incase all the meta info pushes the entity to be over
    # the threshold
    die $stream2->__x("Email exceeds the maximum size.\n")
      if ( $email_size_threshold
        && $working_size_total > $email_size_threshold );

    return $working_size_total;
}

__PACKAGE__->meta->make_immutable();
1;

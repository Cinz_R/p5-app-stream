package Stream2::O::EmailTemplate;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::EmailTemplate;

has '+o' => ( isa => 'Stream2::Schema::Result::EmailTemplate' );

has 'mime_entity_object' => ( is => 'rw', isa => 'Stream2::O::Email', lazy_build => 1, trigger => \&_on_mime_set );

has 'setting_mnemonic' => ( is => 'ro', isa => 'Str', lazy_build => 1 );

has 'permission_setting' => ( is => 'ro', isa => 'Stream2::O::Groupsetting', lazy_build => 1 );

sub _build_mime_entity_object{
    my ($self) = @_;
    my $mime_message = $self->mime_entity();
    my $stream2 = $self->stream2();
    unless( $mime_message ){
        return $stream2->factory('Email')->create();
    }
    return $stream2->factory('Email')->parse($mime_message);
}

sub _on_mime_set{
    my ($self, $new_mime) = @_;
    unless( $new_mime ){ $self->mime_entity( undef ); }
    $self->mime_entity( $new_mime->stringify() );
}

=head2 process_with_stash

Expands this template with the given stash and returns a FRESH

Stream2::O::Email with its body and header parts processed.

Usage:

 $this->process_with_stash({ bla => 1 , foo => 'bar' });

=cut

sub process_with_stash{
    my ($self, $stash) = @_;
    $stash  //= {};

    my $fresh_entity = $self->stream2->factory('Email')->parse( $self->mime_entity() );
    $fresh_entity->process_with_stash($stash);

    return $fresh_entity;
}

=head2 one_html_body

Shortcut to the entity's first HTML body content (as a string)

=cut

sub one_html_body{
    my ($self) = @_;
    return $self->mime_entity_object()->one_html_body();
}

sub _build_setting_mnemonic {
    my ( $self ) = @_;
    return 'email_template-' . $self->id . '-active'
}

sub _build_permission_setting {
    my ( $self ) = @_;

    # Wondering where this is created? Look at triggers.
    return $self->stream2->factory(
        'Groupsetting',
        {
            group_identity => $self->group_identity()
        }
    )->find(
        {
            group_identity   => $self->group_identity(),
            setting_mnemonic => $self->setting_mnemonic()
        }
    );
}

around 'delete' => sub{
    my ($orig, $self) = @_;
    foreach my $rule ( $self->rules()->all() ){
        $rule->delete();
    }
    return $self->$orig();
};


=head1 NAME

 Stream2::Schema::Result::LongList - An EmailTemplate, decorating a Row::EmailTemplate

=cut

=head2 add_rule

Adds the rule of the given role with the given parameters to this template.

=cut

sub add_rule{
    my ($self, $role , $params) = @_;
    $params //= {};
    my $rule = $self->stream2->factory('EmailTemplateRule')->create({
        role_name => $role,
        email_template_id => $self->id(),
        %$params,
    });
    return $rule;
}

around 'rules' => sub{
    my ($orig, $self, @args) = @_;
    return $self->stream2()->factory('EmailTemplateRule' , { dbic_rs => scalar( $self->$orig(@args) ) });
};


__PACKAGE__->meta->make_immutable();


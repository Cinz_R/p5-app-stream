package Stream2::O::AdcresponsesCustomFlag;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;
use Stream2::Schema::Result::AdcresponsesCustomFlag;

=head1 NAME

Stream2::O::AdcresponsesCustomFlag - A Custom flag for use with new manage responses

=cut

has '+o' => ( isa => 'Stream2::Schema::Result::AdcresponsesCustomFlag' );

has 'setting_mnemonic' => ( is => 'ro', isa => 'Str', lazy_build => 1 );
has 'setting_description' => ( is => 'ro', isa => 'Str', lazy_build => 1 );

around 'delete' => sub {
    my ( $orig, $self ) = @_;

    my $stuff = sub {
        $self->$orig();
        return $self->factory->bm->factory(
            'Groupsetting',
            {
                group_identity => $self->group_identity
            }
        )->find({
            group_identity => $self->group_identity,
            setting_mnemonic    => $self->setting_mnemonic
        })->delete();
    };

    return $self->factory->bm->stream_schema->txn_do( $stuff );
};

sub _build_setting_mnemonic {
    my ( $self ) = @_;
    return sprintf( "adcresponses-custom-flag-%s", $self->flag_id );
}

sub _build_setting_description {
    my ( $self ) = @_;
    return sprintf( "Enable custom flag: %s", $self->description );
}

__PACKAGE__->meta->make_immutable();

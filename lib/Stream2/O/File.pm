package Stream2::O::File;

use Moose;

=head1 NAME

Strean2::O::File - A Plain Old Perl class containing a file.

=cut

use File::Temp;
use File::Slurp;
use HTML::Scrubber;

use Log::Any qw/$log/;
use MIME::Base64;
use Stream2;


has 'name' => ( is => 'ro', isa => 'Str', required => 1);
has 'mime_type' => ( is => 'ro', isa => 'Str', required => 1);
has 'disk_file' => ( is => 'ro', isa => 'Str', lazy_build => 1 );
has 'binary_content' => ( is => 'ro', isa => 'Value', lazy_build => 1);

# Shouldn't this file destroy its disk_file when going out of scope?
has 'persistent' => ( is => 'ro', isa => 'Bool', default => 0 );

# An HMTL rendering of this object.
has 'html_render' => ( is => 'ro', isa => 'Str' , lazy_build => 1);


sub _build_binary_content{
    my ($self) = @_;
    unless( $self->has_disk_file() ){
        confess("File ".$self->name()." must have disk_file to dynamically build binary_content");
    }
    return scalar(File::Slurp::read_file( $self->disk_file() , { binmode => ':raw' }));
}


sub _build_disk_file{
    my ($self) = @_;
    unless( $self->has_binary_content() ){
        confess("File ".$self->name()." does not have binary content. Cannot write it on disk");
    }
    my ($fh, $filename) = File::Temp::tempfile();
    File::Slurp::write_file( $filename, {binmode => ':raw'}, $self->binary_content() ) ;
    return $filename;
}

sub _build_html_render{
    my ( $self ) = @_;

    $log->infof("Rendering %s ( %s ) as html", $self->name(), $self->mime_type());

    if ( $self->mime_type =~ /^image\/.+/ ) {
        return $self->_render_image_html();;
    }
    else {
        return $self->_render_document_html();
    }
}

sub _render_document_html{
    my ( $self )    = @_;
    my $stream2     = Stream2->instance();
    my $html;

    if ( $self->mime_type() =~ /pdf$/i ){    # use text_extractor if PDF
        my $htmlization = $stream2->text_extractor()->extract(
            string => $self->binary_content(),
            format => 'html'
        );

        $html = $htmlization->{data};
    }
    else{   # otherwise use parse-normalize
        my $content = $self->binary_content;
        if ( $self->mime_type =~ /html$/ ){ # parse-normalise doesn't accept HTML
            my $scrubber = HTML::Scrubber->new();
            $content = $scrubber->scrub( $content );
        }

        my $b64 = MIME::Base64::encode_base64( $content, '' );
        my $htmlization = $stream2->parse_normalize_api->parse_normalize({ document => $b64, 'desired_enrichments' => [ 'none' ] });

        $html = $htmlization->{data}->{resume_html};
    }

    unless( $html ){
        return $stream2->__('<p>The attachment file could not be rendered.</p>');
    }

    # Extract the body only.
    my $xdoc = $stream2->html_parser->load_html(
        string   => Encode::encode( 'UTF-8', $html ),
        encoding => 'UTF-8'
    );

    my ($xbody) = $xdoc->findnodes('/html/body');
    return join( '', map { $_->toString() } $xbody->childNodes() );
}

sub _render_image_html {
    my ( $self ) = @_;

    return '<img class="other_attachment_image" src="data:'.$self->mime_type.';base64,'.
        MIME::Base64::encode_base64( $self->binary_content, "" )
        .'">';
}

sub to_download_response {
    my ($self) = @_;
    my $disposition = 'attachment; filename="' . quotemeta($self->name) . '";';
    return HTTP::Response->new(
        200,
        'OK',
        [
            'Content-Disposition' => $disposition,
            'Content-Type'        => $self->mime_type,
        ],
        $self->binary_content
    );
}


sub DEMOLISH{
    my ($self) = @_;
    if( $self->has_disk_file()  && ! $self->persistent() ){
        $log->info("Unlinking ".$self->disk_file());
        unlink $self->disk_file();
    }
}

__PACKAGE__->meta->make_immutable();

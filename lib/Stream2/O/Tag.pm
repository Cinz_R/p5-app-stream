package Stream2::O::Tag;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::Tag;

=head1 NAME

 Stream2::Schema::Result::Tag - A Tag

=cut

has '+o' => ( isa => 'Stream2::Schema::Result::Tag' );

=head2 change_flavour

Will update current flavour column and also create a row for the new flavour type if required

Usage:

    my $tag = $s2->factory('Tag')->find({ group_identity => 'dev', tag_name => 'developer' });
    $tag->change_flavour( $new_flavour );

=cut

sub change_flavour {
    my ( $self, $new_flavour ) = @_;
    my $schema = $self->stream2->stream_schema();

    if( $new_flavour ){
        my $flavour = $schema->resultset('TagFlavour')->find_or_create({
            group_identity  => $self->group_identity,
            name            => $new_flavour
        });
        $self->flavour($flavour);
    }else{
        $self->flavour(undef);
    }

    $self->update();

    return 1;
}

=head2 tag_label

returns a formatted version of the tag if needed

=cut

sub tag_label {
    my ( $self ) = @_;
    my $tag_label = $self->tag_name;
    return $tag_label;
}

=head2 flavour_name

shortcut for $tag->flavour->name() but checks if flavour exists first

=cut

sub flavour_name {
    my ( $self ) = @_;
    return $self->flavour ? $self->flavour->name() : undef;
}

__PACKAGE__->meta->make_immutable();

package Stream2::O::UserSetting;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::UserSetting;

=head1 NAME

 Stream2::Schema::Result::UserSetting - A User Setting

=cut


has '+o' => ( isa => 'Stream2::Schema::Result::UserSetting' );

=head2 value_hash

 Returns a value hash like { boolean_value => .. }

=cut

sub value_hash{
    my ($self) = @_;
    return {
        boolean_value => $self->boolean_value(),
        string_value => $self->string_value()
    };
}


=head1 value

Returns the single value of this setting according to its type

=cut

sub value{
    my ( $self ) = @_;
    return $self->get_column( $self->setting->setting_type().'_value' );
}


__PACKAGE__->meta->make_immutable();


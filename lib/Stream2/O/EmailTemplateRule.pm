package Stream2::O::EmailTemplateRule;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Log::Any qw/$log/;

use Stream2::Schema::Result::EmailTemplateRule;

use Moose::Util qw//;

has '+o' => ( isa => 'Stream2::Schema::Result::EmailTemplateRule' );

=head1 NAME

 Stream2::O::EmailTemplateRule - An EmailTemplate, decorating a Row::EmailTemplate

=cut

sub BUILD{
    my ($self, $args_ref) = @_;
    Moose::Util::apply_all_roles( $self,
                                  'Stream2::Role::EmailTemplateRule::'.$args_ref->{o}->role_name() => {
                                      rebless_params => $args_ref
                                  });
    $self->update();
    return $self;
}

sub to_hash{
    my ($self) = @_;
    return {
        id => $self->id().'',
        role_name => $self->role_name()
    };
}

around 'group_macro' => sub{
    my ($orig, $self) = @_;

    my $raw_macro = $self->$orig();
    unless( $raw_macro ){ return undef; }
    return $self->stream2()->factory('GroupMacro')->wrap( $raw_macro );
};


__PACKAGE__->meta->make_immutable();


package Stream2::O::SentEmail;

use Moose;
extends qw/Stream2::O::BaseDBIC/;

use Stream2::Schema::Result::SentEmail;
has '+o' => ( isa => 'Stream2::Schema::Result::SentEmail' );

has 'mime_entity' => ( isa => 'Stream2::O::Email', is => 'ro' , lazy_build => 1);

sub _build_mime_entity {
    my ($self) = @_;
    my $s2     = $self->stream2;
    my $file   = $s2->factory('PrivateFile')->find( $self->aws_s3_key() );
    return $s2->factory('Email')->parse( $file->get_decoded() );
}


=head2 to_hash

A simple property hash mainly designed for display. Dont count on that
to persist any data or anything.

=cut

sub to_hash{
    my ($self) = @_;
    return {
        id => $self->id(),
        data => $self->sent_date()->iso8601().'Z',
        user_id => $self->user_id(),
        action_log_id => $self->action_log_id(),
        subject => $self->subject(),
        recipient => $self->recipient(),
        recipientName => $self->recipient_name(),
    };
}

__PACKAGE__->meta->make_immutable();

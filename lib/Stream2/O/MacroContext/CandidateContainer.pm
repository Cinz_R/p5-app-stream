package Stream2::O::MacroContext::CandidateContainer;

use Moose;

has 'stream2' => ( is => 'ro', isa => 'Stream2' , weak_ref => 1, required => 1);
has 'stream2_interactive' => ( is => 'ro', default => 0 );

with qw/
           Stream2::Role::Action::HasUser
           Stream2::Role::Action::HasCandidate
       /;

sub build_context{
    confess("Do not use that to build a context");
}

sub instance_perform{ confess("This is a fake action. Do not use this"); }

__PACKAGE__->meta()->make_immutable();
1;

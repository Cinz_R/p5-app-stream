package Stream2::Queue::SearchWorker;

use Moose;
extends 'Qurious::Worker';

use Log::Any qw($log);
use Log::Log4perl::MDC;
use Log::Log4perl::NDC;

has 'stream2' => ( is => 'ro' , isa => 'Stream2' , required => 1);

# overwrite to inject the stream2 instance into the job.
sub perform_job {
  my $self = shift;
  my $job = shift;

  $log->info( "Queue: " . $job->queue . ' claimed.' );

  $job->{stream2} = $self->stream2;

  Log::Log4perl::NDC->remove();

  # Set sentry user.
  if( my $user = $job->parameters->{user} ){
      ## Note that just for the sake of timezones, locale and putting which user we are talking
      ## about.
      my $user_object = $self->stream2()->factory('SearchUser')->find($user->{user_id} , { ripple_settings => {} });
      Log::Log4perl::MDC->put('sentry_user' , $user_object->short_info_hash() );
      Log::Log4perl::NDC->push( $user_object->group_identity() );
      Log::Log4perl::NDC->push( $user_object->contact_email() );

      $log->info( "SETTING LANG - " . ( $user_object->locale() // 'UNDEF' ) );
      $self->stream2->translations->set_language( $user_object->locale() || 'en' );
      $log->info( "SETTING TZ - " . ( $user_object->timezone() // 'UNDEF' ) );
      $self->stream2->translations->set_timezone( $user_object->timezone() || 'Europe/London' );
  }

  # Note that perform will catch any exception thrown by
  # the 'perform' method of the Job implementation class.
  # For instance here this perform would call Stream2::Action::Search->perform($job);
  # and catch any error arising in there.
  $job->perform();

  ## Cleanup the Sentry user.
  Log::Log4perl::MDC->put('sentry_user' , undef );

  $log->info( "Queue: " . $job->queue . " " . $job->status );
}

__PACKAGE__->meta->make_immutable;

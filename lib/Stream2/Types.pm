package Stream2::Types;
{
  $Stream2::Types::VERSION = '0.001';
}

use Moose::Util::TypeConstraints;

# Type representing a time in seconds
# allow more readable '1 day' style strings
subtype 'Stream2::Types::Seconds', as 'Int';
  coerce 'Stream2::Types::Seconds',
    from 'Str', via { _hashref_to_secs(_parse_secs_str($_)) };

  coerce 'Stream2::Types::Seconds',
    from 'HashRef', via { _hashref_to_secs($_) };

sub _parse_secs_str {
  my $str = shift;

  my %parsed;
  $parsed{days}    = $1 if $str =~ m/\b(\d+) ?d(?:ays?)?\b/i;
  $parsed{hours}   = $1 if $str =~ m/\b(\d+) ?h(?:ours?)?\b/i;
  $parsed{minutes} = $1 if $str =~ m/\b(\d+) ?m(?:in(?:ute)?s?)?\b/i;
  $parsed{seconds} = $1 if $str =~ m/\b(\d+) ?s(?:ec(?:ond)?s?)?\b/i;

  return \%parsed;
}

sub _hashref_to_secs {
  my $hash_ref = shift or return 0;

  my %units = (
    day    => 24*60*60,
    hour   => 60*60,
    minute => 60,
    second => 1,
  );

  my $total_secs = 0;
  while (my ($unit, $multiplier) = each %units ) {
    foreach my $time_unit ($unit, $unit.'s' ) {
      if ( exists($hash_ref->{$time_unit}) && $hash_ref->{$time_unit} ) {
        $total_secs += $hash_ref->{$time_unit} * $multiplier;
      }
    }
  }

  return $total_secs;
}

1;

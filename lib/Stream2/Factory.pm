package Stream2::Factory;

=head1 NAME

 Stream2::Factory - A base factory class.

=cut

use Moose;

has 'bm' => ( is => 'ro' , isa => 'Stream2' , weak_ref => 1 );

=head2 stream2

alias for ->bm() . Returns the business model 'Stream2'

=cut

has 'stream2' => ( is => 'ro' , isa => 'Stream2' , lazy_build => 1);

sub _build_stream2{
    my ($self) = @_;
    return $self->bm();
}



__PACKAGE__->meta()->make_immutable();


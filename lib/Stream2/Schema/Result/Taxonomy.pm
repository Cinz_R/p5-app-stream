package Stream2::Schema::Result::Taxonomy;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::Taxonomy - Mappings for external board taxonomies

=head1 DESCRIPTION

A key/value table which stores taxonomies (custom list options) of job boards.

=cut

__PACKAGE__->table('taxonomy');
__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->add_columns(
    id => {
        data_type           => 'bigint',
        is_auto_increment   => 1
    },
    field_type => {
        data_type   => 'varchar',
        size        => '50',
    },
    taxonomy_id => {
        data_type   => 'varchar',
        size        => '150',
    },
    label => {
        data_type   => 'varchar',
        size        => '200',
    },
    lang => {
        data_type       => 'varchar',
        size            => '2',
        default_value   => 'en',
    },
    last_updated => {
        data_type       => 'datetime',
        set_on_create   => 1,
        set_on_update   => 1,
    }
);

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint([ qw/field_type taxonomy_id lang/ ]);

1;

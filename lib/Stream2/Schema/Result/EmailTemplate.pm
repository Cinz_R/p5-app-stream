package Stream2::Schema::Result::EmailTemplate;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::CandidateActionLog - Every candidate action should be stored here ( for 30 days )

=cut

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('email_template');

__PACKAGE__->add_columns(
                         id => { data_type => 'int', size => '25', is_auto_increment => 1, is_nullable => 0 },
                         group_identity =>   { data_type => "varchar", is_nullable => 0, size => 50 },
                         name => { data_type => 'varchar', is_nullable => 0, size => 255 },
                         mime_entity => { data_type => 'LONGTEXT', is_nullable => 1 },
                         insert_datetime => { data_type => 'DateTime', set_on_create => 1, is_nullable => 0 }
                        );

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint([ qw/group_identity name/ ]);

__PACKAGE__->belongs_to('groupclient' => 'Stream2::Schema::Result::Group',
                        { 'foreign.identity' => 'self.group_identity' });

__PACKAGE__->has_many('email_template_attachments' => 'Stream2::Schema::Result::EmailTemplateAttachment',
                      { 'foreign.email_template_id' => 'self.id' });

__PACKAGE__->has_many('rules' => 'Stream2::Schema::Result::EmailTemplateRule',
                      { 'foreign.email_template_id' => 'self.id' });

1;


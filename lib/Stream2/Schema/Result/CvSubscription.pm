package Stream2::Schema::Result::CvSubscription;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::CvSubscription

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn", "TimeStamp", "Core");

__PACKAGE__->table("cv_subscriptions");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_nullable: 0

=head2 board

  data_type: 'varchar'
  is_nullable: 0
  size: 40

=head2 token_name

  data_type: 'varchar'
  is_nullable: 0
  size: 50

=head2 token_value

  data_type: 'text'
  is_nullable: 0

=head2 insert_datetime

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_nullable => 0 },
  "board", # This points to the new board table/object
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "board_nice_name", # This is deprecated (for the board table)
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "board_type", # This is deprecated (for the board table)
  {
    data_type => "enum",
    extra => { list => ['internal','external','social','public'] },
    is_nullable => 0,
    default_value => "external"
  },
  "board_id" => # This is deprecated (for the board table)
  {
   data_type => "varchar", size => 100,
   is_nullable => 1
  },
  "eza_number" => # This is company specific and should stay there.
  {
   data_type => "varchar", size => 100,
   is_nullable => 1
  },
  "custom_source_code" => # This is company specific and should stay there.
  {
   data_type => "varchar", size => 255,
   is_nullable => 1
  },
  "allows_acc_sharing" => # This is deprecated (for the board table)
  {
   data_type => 'tinyint',
   is_nullable => 0,
   default_value => 1,
  },
  "auth_data",
  { data_type => "text", is_nullable => 1 },
  "disabled",
  { data_type => "tinyint", size => 1, is_nullable => 0, default_value => 0 },
  "insert_datetime",
  {
    set_on_create => 1,
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
);


__PACKAGE__->set_primary_key("id");

__PACKAGE__->add_unique_constraint('user_subscription_unique' => [ qw/user_id board/  ] );

__PACKAGE__->belongs_to( 'board_object' => 'Stream2::Schema::Result::Board' ,
                         { 'foreign.name' => 'self.board' });

__PACKAGE__->belongs_to( 'user' => 'Stream2::Schema::Result::CvUser' ,
                         { 'foreign.id' => 'self.user_id' });

__PACKAGE__->has_many('watchdog_subscriptions' => 'Stream2::Schema::Result::WatchdogSubscription',
                      { 'foreign.subscription_id' => 'self.id' },
                      { cascade_delete => 1 });
1;

package Stream2::Schema::Result::RecurringReportUser;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::UserSetting - Association table between user and settings.

=cut

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('recurring_report_user');

__PACKAGE__->add_columns(
    "id",
    { data_type => "bigint", is_auto_increment => 1, is_nullable => 0 },
    "user_id",
    { data_type => "integer", is_nullable => 0 },
    "recurring_report_id",
    { data_type => "integer", is_nullable => 0},
);

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint([ qw/user_id recurring_report_id/ ]);

__PACKAGE__->belongs_to('user' => 'Stream2::Schema::Result::CvUser',
						{ 'foreign.id' => 'self.user_id' });

__PACKAGE__->belongs_to('recurring_report' => 'Stream2::Schema::Result::RecurringReport',
						{ 'foreign.id' => 'self.recurring_report_id' });



1;

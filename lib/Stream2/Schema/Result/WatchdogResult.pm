package Stream2::Schema::Result::WatchdogResult;

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

Stream2::Schema::Result::WatchdogResult - A record of each candidate found in a watchdog

=cut


__PACKAGE__->table("watchdog_result");

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

__PACKAGE__->add_columns(
  "candidate_id",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "subscription_id",
  { data_type => "integer", is_nullable => 0 },
  "watchdog_id",
  { data_type => "integer", is_nullable => 0 },
  "viewed",
  { data_type => "tinyint", is_nullable => 0, default_value => 0, size => 1 },
  "content",
  { data_type => "mediumtext", is_nullable => 1 },
  "insert_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
    set_on_create => 1,
  },
  "previously_seen_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "last_seen_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
    set_on_update => 1,
    set_on_create => 1,
  }
);

__PACKAGE__->set_primary_key(qw/watchdog_id subscription_id candidate_id/);

__PACKAGE__->belongs_to( 'watchdog' => 'Stream2::Schema::Result::Watchdog',
                       { 'foreign.id' => 'self.watchdog_id' });

__PACKAGE__->belongs_to('watchdog_subscription' => 'Stream2::Schema::Result::WatchdogSubscription',
                        {
                            'foreign.subscription_id' => 'self.subscription_id',
                            'foreign.watchdog_id' => 'self.watchdog_id'
                        });

__PACKAGE__->inflate_column( 'content', {
    inflate => sub {
        my ( $data, $result ) = @_;
        return JSON::from_json( $data || '{}' );
    },
    deflate => sub {
        my ( $data, $result ) = @_;
        return JSON::to_json( $data || {} , { ascii => 1 } );
    }
});

1;

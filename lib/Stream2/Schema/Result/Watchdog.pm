package Stream2::Schema::Result::Watchdog;

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

Stream2::Schema::Result::Watchdog - A Watchdog in the DB.

=cut


__PACKAGE__->table("watchdog");

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "criteria_id",
  { data_type => "char", is_nullable => 0, size => 27, extra => { 'COLLATE' => 'utf8_bin' } },
  "active",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  'base_url',
  { data_type => 'varchar' , is_nullable => 0, size => 255 , default_value => 'http://localhost/' },
  "insert_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
    set_on_create => 1,
  },
  "update_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
    set_on_create => 1,
    set_on_update => 1,
  },
  "next_run_datetime",
  { data_type => 'datetime',
    is_nullable => 1
  },
  "last_viewed_datetime",
  { data_type => 'datetime',
    is_nullable => 1
  },
  destruction_process_id =>
      { data_type => "integer", is_nullable => 1 },

);


__PACKAGE__->set_primary_key("id");

sub sqlt_deploy_hook {
    my ($self, $sqlt_table) = @_;
    # Speedup next run lookup queries.
    $sqlt_table->add_index( name => 'watchdog_nextrun_index', fields => ['next_run_datetime'] );
}


__PACKAGE__->belongs_to('destruction_process' => 'Stream2::Schema::Result::LongStepProcess',
                        { 'foreign.id' => 'self.destruction_process_id' });

__PACKAGE__->belongs_to('user' => 'Stream2::Schema::Result::CvUser',
                        { 'foreign.id' => 'self.user_id' });

__PACKAGE__->belongs_to('criteria' => 'Stream2::Schema::Result::Criteria',
                        { 'foreign.id' => 'self.criteria_id' });

__PACKAGE__->has_many( 'watchdog_subscriptions' => 'Stream2::Schema::Result::WatchdogSubscription' ,
                       { 'foreign.watchdog_id' => 'self.id' },
                       { cascade_delete => 1 });

__PACKAGE__->has_many( 'watchdog_results' => 'Stream2::Schema::Result::WatchdogResult' ,
                       { 'foreign.watchdog_id' => 'self.id' },
                       { cascade_delete => 1 });

__PACKAGE__->has_many( 'unviewed_results' => 'Stream2::Schema::Result::WatchdogResult',
    sub {
        my $args = shift;
        return {
            "$args->{'foreign_alias'}.watchdog_id" => { -ident => "$args->{self_alias}.id" },
            "$args->{'foreign_alias'}.viewed" => "0"
        }
    }
);

__PACKAGE__->many_to_many( 'subscriptions' => 'watchdog_subscriptions' , 'subscription' );

1;

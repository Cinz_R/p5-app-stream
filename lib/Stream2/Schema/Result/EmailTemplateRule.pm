use utf8;
package Stream2::Schema::Result::EmailTemplateRule;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Stream2::Schema::Result::EmailTemplateRule

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::FilterColumn>

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("FilterColumn", "InflateColumn::DateTime");

=head1 TABLE: C<email_template_rule>

=cut

__PACKAGE__->table("email_template_rule");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 role_name

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 email_template_id

  data_type: 'integer'
  is_nullable: 0

=head2 group_macro_id

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "role_name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "email_template_id",
  { data_type => "integer", is_nullable => 0 },
  "group_macro_id",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2017-02-20 17:22:29
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:p1XFtMGHXXm2NWVTU5GceA


__PACKAGE__->belongs_to( 'email_template' => 'Stream2::Schema::Result::EmailTemplate',
                         { 'foreign.id' => 'self.email_template_id' });

__PACKAGE__->belongs_to( 'group_macro' => 'Stream2::Schema::Result::GroupMacro',
                         { 'foreign.id' => 'self.group_macro_id' });

1;

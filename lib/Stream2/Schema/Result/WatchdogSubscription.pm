package Stream2::Schema::Result::WatchdogSubscription;

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

Stream2::Schema::Result::WatchdogSubscription - A Watchdog in the DB.

=cut


__PACKAGE__->table("watchdog_subscription");

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

__PACKAGE__->add_columns(
  "watchdog_id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "subscription_id",
  { data_type => "integer", is_nullable => 0 },
  "new_result_count",
  { data_type => "integer", is_nullable => 0, default_value => 0, extra => { unsigned => 1 } },
  'last_run_datetime',
  { data_type => 'datetime', datetime_undef_if_invalid => 1 , is_nullable => 1 },
  'last_run_error',
  { data_type => 'text' , is_nullable => 1 },
  "insert_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
    set_on_create => 1,
  }
);


__PACKAGE__->set_primary_key("watchdog_id" => "subscription_id");

__PACKAGE__->belongs_to( 'subscription' => 'Stream2::Schema::Result::CvSubscription',
                         { 'foreign.id' => 'self.subscription_id' }
                       );

__PACKAGE__->belongs_to( 'watchdog' , 'Stream2::Schema::Result::Watchdog' ,
                         { 'foreign.id' => 'self.watchdog_id' } );

__PACKAGE__->has_many( 'watchdog_results' => 'Stream2::Schema::Result::WatchdogResult' ,
                       {
                           'foreign.subscription_id' => 'self.subscription_id',
                           'foreign.watchdog_id' => 'self.watchdog_id'
                       },
                       { cascade_delete => 1 });

__PACKAGE__->has_many( 'unviewed_results' => 'Stream2::Schema::Result::WatchdogResult',
    sub {
        my $args = shift;
        return {
            "$args->{'foreign_alias'}.subscription_id" => { -ident => "$args->{self_alias}.subscription_id" },
            "$args->{'foreign_alias'}.watchdog_id" => { -ident => "$args->{self_alias}.watchdog_id" },
            "$args->{'foreign_alias'}.viewed" => "0"
        }
    }
);


1;

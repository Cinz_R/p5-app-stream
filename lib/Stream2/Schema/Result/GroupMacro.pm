package Stream2::Schema::Result::GroupMacro;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::GroupMacro - A macro for a group

=cut

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('group_macro');

__PACKAGE__->add_columns(
                         id => { data_type => 'int', size => '25', is_auto_increment => 1, is_nullable => 0 },
                         group_identity =>   { data_type => "varchar", is_nullable => 0, size => 50 },
                         name => { data_type => 'varchar', is_nullable => 0, size => 255 },
                         insert_datetime => { data_type => 'DateTime', set_on_create => 1, is_nullable => 0 },
                         on_action => { data_type => 'varchar', is_nullable => 1 , size => 255 },
                         lisp_source => { data_type => 'text' , is_nullable => 0 },
                         blockly_xml => { data_type => 'text' , is_nullable => 1 }
                        );

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint([ qw/group_identity name/ ]);

__PACKAGE__->belongs_to('groupclient' => 'Stream2::Schema::Result::Group',
                        { 'foreign.identity' => 'self.group_identity' });


__PACKAGE__->might_have( 'email_template_rule' => 'Stream2::Schema::Result::EmailTemplateRule',
                         { 'foreign.group_macro_id' => 'self.id' },
                         { cascade_delete => 0  }
                     );

sub sqlt_deploy_hook {
    my ($self, $sqlt_table) = @_;
    $sqlt_table->add_index( name => 'macro_action_index', fields => ['group_identity','on_action'] );
}


1;

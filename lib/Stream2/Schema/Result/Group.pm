package Stream2::Schema::Result::Group;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::Group - A DB Stored Group (AKA Company, AKA Client)

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn", "TimeStamp", "Core");


## group is a reserved word in SQL
__PACKAGE__->table("groupclient");

__PACKAGE__->add_columns(
  "identity",
  { data_type => "varchar", is_nullable => 0 , size => 50 },
  "nice_name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "insert_datetime",
  {
    set_on_create => 1,
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  }
);

__PACKAGE__->set_primary_key("identity");

__PACKAGE__->has_many('users' => 'Stream2::Schema::Result::CvUser',
                      { 'foreign.group_identity' => 'self.identity' });

__PACKAGE__->has_many('email_templates' => 'Stream2::Schema::Result::EmailTemplate',
                      { 'foreign.group_identity' => 'self.identity' });

__PACKAGE__->has_many('macros' => 'Stream2::Schema::Result::GroupMacro',
                      { 'foreign.group_identity' => 'self.identity' });

__PACKAGE__->has_many('settings' => 'Stream2::Schema::Result::Groupsetting',
                      { 'foreign.group_identity' => 'self.identity' });

1;

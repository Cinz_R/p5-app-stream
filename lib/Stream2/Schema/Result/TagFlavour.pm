package Stream2::Schema::Result::TagFlavour;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('tag_flavour');

__PACKAGE__->add_columns(
    id => {
           data_type => 'int',
           size => 25,
           is_nullable => 0,
           is_auto_increment => 1,
    },
    group_identity => {
        data_type => 'varchar',
        size      => '50',
        is_nullable => 0,
    },
    name => {
        data_type => 'varchar',
        size => '100',
        is_nullable => 0,
    },
);

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint( 'group_tag_flavour' => [ 'group_identity' , 'name' ] );

__PACKAGE__->has_many('tags' => 'Stream2::Schema::Result::Tag',
                      { 'foreign.flavour_id' => 'self.id' },
                      { cascade_delete => 0 }
                     );

1;

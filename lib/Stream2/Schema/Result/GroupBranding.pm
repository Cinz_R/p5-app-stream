package Stream2::Schema::Result::GroupBranding;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::GroupBranding - Represents a group_identity/company with custom branding

=cut

__PACKAGE__->load_components(qw/Core InflateColumn::DateTime TimeStamp/);

=head1 TABLE: C<group_branding>

=cut

__PACKAGE__->table('group_branding');

__PACKAGE__->add_columns(
    group_identity => {
        data_type => 'varchar',
        size      => '50',
        is_nullable => 0,
    },
    # This field should contain the 'group identity' in which to brand this as.
    # multiple companies should be allowed to use the same custom branding for instance
    # child companies of AMS will all use 'amscont' as their branding as will users with
    # the group identity 'amscont' should also have 'amscont' specified for their brainding.
    branding => {
        data_type => "varchar",
        is_nullable => 0,
        size => 100,
	default_value => 'default',
    },
    insert_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
        is_nullable => 0,
    }
);

__PACKAGE__->set_primary_key('group_identity');
__PACKAGE__->add_unique_constraint('group_id_unique' => [ 'group_identity', 'branding' ]);


1;

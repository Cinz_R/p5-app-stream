use utf8;
package Stream2::Schema::Result::LoginRecord;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Stream2::Schema::Result::LoginRecord

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::FilterColumn>

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("FilterColumn", "InflateColumn::DateTime");

=head1 TABLE: C<login_record>

=cut

__PACKAGE__->table("login_record");

=head1 ACCESSORS

=head2 id

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0

=head2 insert_datetime

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  default_value: 'CURRENT_TIMESTAMP'
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_nullable: 0

=head2 remote_ip

  data_type: 'varchar'
  is_nullable: 1
  size: 32

=head2 login_provider

  data_type: 'varchar'
  is_nullable: 1
  size: 40

=head2 hostname

  data_type: 'varchar'
  is_nullable: 1
  size: 1024

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "bigint", is_auto_increment => 1, is_nullable => 0 },
  "insert_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    default_value => "CURRENT_TIMESTAMP",
    is_nullable => 0,
  },
  "user_id",
  { data_type => "integer", is_nullable => 0 },
  "remote_ip",
  { data_type => "varchar", is_nullable => 1, size => 32 },
  "login_provider",
  { data_type => "varchar", is_nullable => 1, size => 40 },
  "hostname",
  { data_type => "varchar", is_nullable => 1, size => 1024 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2017-01-23 11:34:45
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:zhTzRHttjZnN/e5ISQSXvg


# You can replace this text with custom code or comments, and it will be preserved on regeneration

__PACKAGE__->belongs_to(
    user => 'Stream2::Schema::Result::CvUser',
    { 'foreign.id' => 'self.user_id' });


1;

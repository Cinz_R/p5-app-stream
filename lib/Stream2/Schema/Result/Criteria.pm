package Stream2::Schema::Result::Criteria;

use strict;
use warnings;

use base 'DBIx::Class::Core';

use Stream2::Data::JSON;

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('cv_criteria');

my $BASE64_SHA1_SIZE = 27;

__PACKAGE__->add_columns(
    id => {
        data_type => 'char',
        size      => $BASE64_SHA1_SIZE,
        extra => { 'COLLATE' => 'utf8_bin' }
    },
    tokens => {
        data_type => 'text', # 64kb
    },
    keywords => {
        data_type => 'varchar',
        size => 2048,
        is_nullable => 1,
    },
    insert_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
    },

    update_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
        set_on_update => 1,
    },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->inflate_column('tokens' => {
    deflate => sub {
      my ($inflated_value, $result_object) = @_;
      return Stream2::Data::JSON->deflate_to_json($inflated_value || {});
    },
    inflate => sub {
      my ($deflated_value, $result_object) = @_;
      return Stream2::Data::JSON->inflate_from_json($deflated_value);
    },
});

1;

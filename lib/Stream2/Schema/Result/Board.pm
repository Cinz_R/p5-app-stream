package Stream2::Schema::Result::Board;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::Board - A Board with all its board attributes. Keyed by name

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn", "TimeStamp", "Core");

__PACKAGE__->table("board");

__PACKAGE__->add_columns(
  "name",
  { data_type => "varchar", size => 50,  is_nullable => 0 },
  "nice_name",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "id",
  { data_type => "varchar", is_nullable => 1, size => 100 },
  "type",
  {
    data_type => "enum",
    extra => { list => ['internal','external','social','public'] },
    is_nullable => 0,
    default_value => "external"
  },
  "allows_acc_sharing" =>
  {
   data_type => 'tinyint',
   is_nullable => 0,
   default_value => 1,
  },
  "disabled",
  { data_type => "tinyint", size => 1, is_nullable => 0, default_value => 0 },
  "insert_datetime",
  {
    set_on_create => 1,
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
);


__PACKAGE__->set_primary_key("name");

__PACKAGE__->has_many('cv_subscriptions' => 'Stream2::Schema::Result::CvSubscription' ,
                      { 'foreign.board'  => 'self.name' } );
__PACKAGE__->has_many('search_records' => 'Stream2::Schema::Result::SearchRecord',
                      { 'foreign.board' => 'self.name' } );
1;

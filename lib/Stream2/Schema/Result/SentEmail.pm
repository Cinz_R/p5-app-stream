package Stream2::Schema::Result::SentEmail;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::SentEmail - an email message sent to a candidate

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

__PACKAGE__->table("sent_email");

__PACKAGE__->add_columns(
    id => {
        data_type => 'int', is_auto_increment => 1, is_nullable => 0,
        extra     => { unsigned => 1 }
    },
    user_id => {
        data_type => "integer", is_foreign_key => 1, is_nullable => 0
    },
    action_log_id => {
        data_type => "integer", is_foreign_key => 1, is_nullable => 0
    },
    sent_date => {
        data_type   => "datetime", datetime_undef_if_invalid => 1,
        is_nullable => 0
    },
    subject => {
        data_type => "varchar", is_nullable => 0, size => 1024
    },
    recipient => {
        data_type => "varchar", is_nullable => 0, size => 1024
    },
    recipient_name => {
        data_type => "varchar", is_nullable => 1, size => 128
    },
    aws_s3_key => {
        data_type => "varchar", is_nullable => 0, size => 2048
    },
    plain_body => {
        data_type => "longtext", is_nullable => 1
    },
);

__PACKAGE__->set_primary_key('id');

# self.action_log_id belongs to candiate_action_log.id
__PACKAGE__->belongs_to(
    "action_log" => "Stream2::Schema::Result::CandidateActionLog",
    { id => "action_log_id" },
    { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

# self.user_id belongs to cv_user.id
__PACKAGE__->belongs_to(
    "user" => "Stream2::Schema::Result::CvUser",
    { id => "user_id" },
    { is_deferrable => 1, on_delete => "RESTRICT", on_update => "RESTRICT" },
);

sub sqlt_deploy_hook {
    my ($self, $sqlt_table) = @_;
    $sqlt_table->add_index( name => 'aws_s3_key_idx', fields => ['aws_s3_key'] );
}

1;

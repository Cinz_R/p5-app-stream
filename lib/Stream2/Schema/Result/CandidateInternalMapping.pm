package Stream2::Schema::Result::CandidateInternalMapping;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stores the relationship between a result on an external board and the candidate once it has been imported into
an internal database

=cut

__PACKAGE__->load_components("InflateColumn::DateTime","TimeStamp");

__PACKAGE__->table('candidate_internal_mapping');

__PACKAGE__->add_columns(
    id => { # primary key
        data_type => 'int',
        size      => '25',
        is_auto_increment => 1,
        is_nullable => 0,
    },
    import_id => { # as given to us by the ts import api ( file_id )
        data_type => "int",
        extra => { unsigned => 1 },
        is_nullable => 0,
    },
    group_identity => {
        data_type => 'varchar',
        size      => '50',
        is_nullable => 0,
    },
    candidate_id => { # the external_id
        data_type => 'varchar',
        size      => '100',
        is_nullable => 0,
    },
    internal_id => { # the id of the candidate within their internal database ( e.g. arterix solr_id )
        data_type => "varchar",
        is_nullable => 1,
        size => '1024'
    },
    completed => {
        data_type => "tinyint",
        size => "1",
        default_value => 0,
        is_nullable => 0
    },
    backend => { # e.g. cb_mysupply or talentsearch
        data_type => "varchar",
        is_nullable => 0,
        size => '100'
    },
    insert_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
        is_nullable => 0,
    },
    update_datetime => {
        data_type => 'DateTime',
        set_on_update => 1,
        is_nullable => 1
    }
);

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint([ qw/import_id backend/ ]);
__PACKAGE__->add_unique_constraint([ qw/group_identity candidate_id/ ]);
1;
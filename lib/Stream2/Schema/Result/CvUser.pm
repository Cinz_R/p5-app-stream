package Stream2::Schema::Result::CvUser;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::CvUser - A DB Stored Search2 user.

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn", "TimeStamp", "Core");

__PACKAGE__->table("cv_users");


__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "provider",
  { data_type => "varchar", is_nullable => 0, size => 40 },
  "provider_id",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "last_login_provider",
  { data_type => "varchar", is_nullable => 0, size => 40, default_value => 'adcourier' },
  "group_identity",
  { data_type => "varchar", is_nullable => 0, size => 50 },
  "group_nice_name",
  { data_type => "varchar", is_nullable => 1, size => 255 },
  "contact_email",
  { data_type => 'varchar' , is_nullable => 1 , size => 255 },
  "contact_name",
  { data_type => 'varchar' , is_nullable => 1 , size => 255 },
  "locale",
  { data_type => 'varchar' , is_nullable => 0 , default_value => 'en', size => 10 },
  "timezone",
  { data_type => 'varchar' , is_nullable => 0 , default_value => 'Europe/London', size => 36 },
  "currency",
  { data_type => 'varchar' , is_nullable => 0 , default_value => 'GBP', size => 5 },
  "distance_unit",
  { data_type => 'varchar' , is_nullable => 0 , default_value => 'miles', size => 5 },
  "settings",
  { data_type => 'text' , is_nullable => 1 },
  "ui_settings",
  { data_type => 'varchar', is_nullable => 1, size => 1024 },
  "contact_details",
  { data_type => 'text' , is_nullable => 1 },
  "insert_datetime",
  {
    set_on_create => 1,
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "last_ip",
  { "data_type" => "varchar", is_nullable => 1, size => 40 }
);

__PACKAGE__->set_primary_key("id");

__PACKAGE__->add_unique_constraint([ qw/provider provider_id/ ]);

__PACKAGE__->belongs_to('groupclient' => 'Stream2::Schema::Result::Group',
                        { 'foreign.identity' => 'self.group_identity' }
                    );


# The has_many relations. All are reciprocal of foreign belongs_to
#
# You can quality check that by doing:
#
# select table_name  from information_schema.referential_constraints  where CONSTRAINT_SCHEMA = 'cvsearch' AND REFERENCED_TABLE_NAME = 'cv_users';
#
#
__PACKAGE__->has_many('candidate_action_logs' => 'Stream2::Schema::Result::CandidateActionLog',
                      { 'foreign.user_id' => 'self.id' });

__PACKAGE__->has_many('candidate_tags' => 'Stream2::Schema::Result::CandidateTag',
                      { 'foreign.user_id' => 'self.id' });

__PACKAGE__->has_many('subscriptions' => 'Stream2::Schema::Result::CvSubscription',
                      { 'foreign.user_id' => 'self.id' },
                      { 'cascade_delete' => 1 });

__PACKAGE__->has_many('longlists' => 'Stream2::Schema::Result::LongList',
                      { 'foreign.user_id' => 'self.id' });

__PACKAGE__->has_many('recurring_report_users' => 'Stream2::Schema::Result::RecurringReportUser',
                      { 'foreign.user_id' => 'self.id' });

__PACKAGE__->has_many('saved_searches' => 'Stream2::Schema::Result::SavedSearches',
                      { 'foreign.user_id' => 'self.id' });

__PACKAGE__->has_many('search_records' => 'Stream2::Schema::Result::SearchRecord',
                      { 'foreign.user_id' => 'self.id' });

__PACKAGE__->has_many('sent_emails' => 'Stream2::Schema::Result::SentEmail',
                      {'foreign.user_id' => 'self.id'});

__PACKAGE__->has_many('user_settings' => 'Stream2::Schema::Result::UserSetting',
                      { 'foreign.user_id' => 'self.id' });

# The settings this user did set as an admin:
__PACKAGE__->has_many('user_set_settings' => 'Stream2::Schema::Result::UserSetting',
                      { 'foreign.admin_user_id' => 'self.id' });

__PACKAGE__->has_many('watchdogs' => 'Stream2::Schema::Result::Watchdog',
                      { 'foreign.user_id' => 'self.id' });

foreach my $json_column ( qw/settings contact_details/ ){
    __PACKAGE__->inflate_column($json_column => {
                                                 deflate => sub {
                                                     my ($inflated_value, $result_object) = @_;
                                                     return Stream2::Data::JSON->deflate_to_json($inflated_value || {});
                                                 },
                                                 inflate => sub {
                                                     my ($deflated_value, $result_object) = @_;
                                                     return Stream2::Data::JSON->inflate_from_json($deflated_value || '{}');
                                                 },
                                                });
}

1;

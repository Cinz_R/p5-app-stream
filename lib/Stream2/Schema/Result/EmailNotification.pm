package Stream2::Schema::Result::EmailNotification;

use strict;
use warnings;

use base 'DBIx::Class::Core';

use Stream2;

=head1 NAME

Stream2::Schema::Result::CandidateActionLog - Every candidate action should be stored here ( for 30 days )

=cut

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('email_notification');

__PACKAGE__->add_columns(
    id => {
        data_type => 'bigint',
        size      => '25',
        is_auto_increment => 1,
        is_nullable => 0,
    },
    insert_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
        is_nullable => 0,
    },
    email => {
        data_type => 'varchar',
        is_nullable => 0,
        size => 255
    },
    notification => {
        data_type => 'varchar',
        is_nullable => 0,
        size => 255,
    },
    notification_body => {
        data_type => 'blob', # 64kb
        is_nullable => 0,
    },
    transport_method => {
        data_type => 'varchar',
        size => 100,
        is_nullable => 1,
    }
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->inflate_column('notification_body' => {
    deflate => sub {
        my ($inflated_value, $result_object) = @_;
        return $inflated_value ? Stream2->instance()->json->encode( $inflated_value ) : '{}';
    },
    inflate => sub {
        my ($deflated_value, $result_object) = @_;
        return $deflated_value ? Stream2->instance()->json->decode($deflated_value) : {} ;
    },
});


1;

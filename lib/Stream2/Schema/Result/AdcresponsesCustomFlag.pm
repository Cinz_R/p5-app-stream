use utf8;
package Stream2::Schema::Result::AdcresponsesCustomFlag;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Stream2::Schema::Result::AdcresponsesCustomFlag

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::FilterColumn>

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("FilterColumn", "InflateColumn::DateTime");

=head1 TABLE: C<adcresponses_custom_flag>

=cut

__PACKAGE__->table("adcresponses_custom_flag");

=head1 ACCESSORS

=head2 flag_id

  data_type: 'integer'
  is_nullable: 0

=head2 group_identity

  data_type: 'varchar'
  default_value: (empty string)
  is_nullable: 0
  size: 50

=head2 description

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 colour_hex_code

  data_type: 'varchar'
  default_value: '#000000'
  is_nullable: 1
  size: 7

=cut

__PACKAGE__->add_columns(
  "flag_id",
  { data_type => "integer", is_nullable => 0 },
  "group_identity",
  { data_type => "varchar", default_value => "", is_nullable => 0, size => 50 },
  "description",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "colour_hex_code",
  {
    data_type => "varchar",
    default_value => "#000000",
    is_nullable => 1,
    size => 7,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</flag_id>

=item * L</group_identity>

=back

=cut

__PACKAGE__->set_primary_key("flag_id", "group_identity");


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2016-08-31 12:21:41
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:X42X9wn4UflmF+RUA9uF9A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

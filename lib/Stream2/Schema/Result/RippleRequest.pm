package Stream2::Schema::Result::RippleRequest;

use strict;
use warnings;

use base 'DBIx::Class::Core';

use Stream2;

__PACKAGE__->table("ripple_request");

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn", "TimeStamp" , "Core");

__PACKAGE__->add_columns(
                         "id" => { data_type => "bigint",
                                   is_auto_increment => 1,
                                   is_nullable => 0
                                 },
                         'uuid' => { data_type => 'varchar',
                                     size => 255,
                                     is_nullable => 0,
                                   },
                         'oauth_access_token' => {
                                                  data_type => 'varchar',
                                                  size => 255,
                                                  is_nullable => 0
                                                 },
                         'oauth_response' => {
                                              data_type => 'blob', # 64kb
                                              is_nullable => 0,
                                             },
                         'ripple_settings' => {
                                               data_type => 'blob', # 64kb
                                               is_nullable => 1,
                                              },
                         'custom_fields' => {
                                             data_type => 'blob', # 64kb
                                             is_nullable => 0,
                                            },
                         'criteria_id' => {
                                           'data_type' => 'char',
                                           size => 27,
                                           is_nullable => 0,
                                           extra => { 'COLLATE' => 'utf8_bin' } 
                                          },
                         "insert_datetime" =>  {
                                                data_type => "datetime",
                                                datetime_undef_if_invalid => 1,
                                                is_nullable => 1,
                                                set_on_create => 1,
                                               },
                         "expiry_datetime" => {
                                               data_type => "datetime",
                                               datetime_undef_if_invalid => 1,
                                               is_nullable => 0,
                                               set_on_create => 0,
                                               set_on_update => 0,
                                              },
                        );

__PACKAGE__->set_primary_key("id");
__PACKAGE__->add_unique_constraint( 'ripple_oneuuid' => [ 'uuid' ] );

__PACKAGE__->inflate_column('oauth_response' => {
                                                 deflate => sub {
                                                     my ($inflated_value, $result_object) = @_;
                                                     return $inflated_value ? Stream2->instance()->json->encode( $inflated_value ) : undef;
                                                 },
                                                 inflate => sub {
                                                     my ($deflated_value, $result_object) = @_;
                                                     return $deflated_value ? Stream2->instance()->json->decode($deflated_value) : undef ;
                                                 },
                                                });

__PACKAGE__->inflate_column('ripple_settings' => {
                                                  deflate => sub {
                                                      my ($inflated_value, $result_object) = @_;
                                                      return $inflated_value ? Stream2->instance()->json->encode( $inflated_value ) : undef;
                                                  },
                                                  inflate => sub {
                                                      my ($deflated_value, $result_object) = @_;
                                                      return $deflated_value ? Stream2->instance()->json->decode($deflated_value) : undef ;
                                                  },
                                                 });

__PACKAGE__->inflate_column('custom_fields' => {
                                                deflate => sub {
                                                    my ($inflated_value, $result_object) = @_;
                                                    return $inflated_value ? Stream2->instance()->json->encode( $inflated_value ) : undef;
                                                },
                                                inflate => sub {
                                                    my ($deflated_value, $result_object) = @_;
                                                    return $deflated_value ? Stream2->instance()->json->decode($deflated_value) : undef ;
                                                },
                                               });

__PACKAGE__->belongs_to('criteria' , 'Stream2::Schema::Result::Criteria',
                        { 'foreign.id' => 'self.criteria_id' });

=head1 NAME

Stream2::Schema::Result::RippleRequest - A Ripple request.

=head2 id

Auto incr. ID.

=cut

1;

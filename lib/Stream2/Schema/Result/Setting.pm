package Stream2::Schema::Result::Setting;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::Setting - A Furniture table, containing all settings search2 supports.

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn", "TimeStamp", "Core");

__PACKAGE__->table('setting');

__PACKAGE__->add_columns(
                         "id",
                         { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
                         "setting_mnemonic", # A unique string. Nicer to use from the code. Something like notes.enabled tags.enabled ,
                                             # or actions.forward.enabled actions.forward.bulk.enabled
                         { data_type => "varchar", size => 255 , is_nullable => 0 },
                         "setting_type",
                         { data_type => "varchar", size => 100 , is_nullable => 0 , default_value => 'boolean'},
                         "setting_description",
                         { data_type => "varchar", size => 512 , is_nullable => 0 },
                         "default_boolean_value",
                         { data_type => "tinyint", is_nullable => 1 },
                         "default_string_value",
                         { data_type => "varchar", size => 1024 , is_nullable => 1 },
                         "setting_meta_data",
                         { data_type => "text", is_nullable => 1 },
                    );

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint([ qw/setting_mnemonic/ ]);

# This relation HAS to be used within $stream2->with_context({ user_id => .. } ), ... .
__PACKAGE__->has_many('user_settings' => 'Stream2::Schema::Result::UserSetting',
                      sub{
                          my $args = shift;
                          my $user_id = $args->{self_resultsource}->schema()->stream2()->context()->{user_id} // confess('Missing user_id in $stream2->context');
                          return {
                                  $args->{foreign_alias}.".setting_id"   => { -ident => $args->{self_alias}.".id" },
                                  $args->{foreign_alias}.".user_id" => $user_id # { -ident => $user_id } ident means another column, not a value
                                 };
                      });


__PACKAGE__->inflate_column('setting_meta_data' => {
                                             deflate => sub {
                                                 my ($inflated_value, $result_object) = @_;
                                                 return Stream2::Data::JSON->deflate_to_json($inflated_value || {});
                                             },
                                             inflate => sub {
                                                 my ($deflated_value, $result_object) = @_;
                                                 return Stream2::Data::JSON->inflate_from_json($deflated_value || '{}');
                                             },
                                            });


1;

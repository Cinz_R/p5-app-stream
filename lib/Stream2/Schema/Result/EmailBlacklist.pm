package Stream2::Schema::Result::EmailBlacklist;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::CandidateActionLog - Every candidate action should be stored here ( for 30 days )

=cut

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('email_blacklist');

__PACKAGE__->add_columns(
    id => {
        data_type => 'int',
        size      => '25',
        is_auto_increment => 1,
        is_nullable => 0,
    },
    email => {
        data_type => 'varchar',
        is_nullable => 0,
        size => 255
    },
    comments => {
        data_type => "varchar",
        is_nullable => 1,
        size => 255
    },
    insert_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
        is_nullable => 0,
    },
    group_identity => {
        data_type => "varchar", is_nullable => 1 , size => 50,
    },
    purpose => {
        data_type => "varchar", is_nullable => 1 , size => 50,
    }
);

__PACKAGE__->belongs_to('group' => 'Stream2::Schema::Result::Group',
                        { 'foreign.identity' => 'self.group_identity' },
                        { join_type => 'left' });

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint([ qw/email group_identity purpose/ ]);

1;
package Stream2::Schema::Result::Tag;

use strict;
use warnings;

use base 'DBIx::Class::Core';

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('tag');

__PACKAGE__->add_columns(
    group_identity => {
        data_type => 'varchar',
        size      => '50',
        is_nullable => 0,
    },
    tag_name => {
        data_type => 'varchar',
        size => '100',
        is_nullable => 0,
    },
    tag_name_ci => {
       data_type => 'varchar',
       size => '100',
       is_nullable => 0,
    },
    flavour_id => {
                   data_type => 'int',
                   size => 25,
                   is_nullable => 1,
                  },
    insert_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
        is_nullable => 0,
    }
);

__PACKAGE__->set_primary_key('group_identity' , 'tag_name');

sub sqlt_deploy_hook {
    my ($self, $sqlt_table) = @_;
    $sqlt_table->add_index( name => 'tag_name_ci_index', fields => ['group_identity','tag_name_ci'] );
}


__PACKAGE__->belongs_to('flavour' => 'Stream2::Schema::Result::TagFlavour',
                        { 'foreign.id' => 'self.flavour_id' },
                        { join_type => 'left'});

__PACKAGE__->has_many('candidate_tags' => 'Stream2::Schema::Result::CandidateTag',
                      { 'foreign.group_identity' => 'self.group_identity',
                        'foreign.tag_name' => 'self.tag_name'
                      },
                      { cascade_delete => 0 });

1;

package Stream2::Schema::Result::RecurringReport;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::RecurringReport - A Recurring report. Sent to a bunch of recipients (SearchUsers

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn", "TimeStamp", "Core");

__PACKAGE__->table('recurring_report');

__PACKAGE__->add_columns(
    "id",
    { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
    "insert_datetime",
    {
        data_type => "datetime",
        datetime_undef_if_invalid => 1,
        is_nullable => 1,
        set_on_create => 1,
    },
    "update_datetime",
    {
        data_type => "datetime",
        datetime_undef_if_invalid => 1,
        is_nullable => 1,
        set_on_create => 1,
        set_on_update => 1,
    },
    "next_run_datetime",
    {
        data_type => 'datetime',
        is_nullable => 1
    },
    "class",
    { data_type => "varchar", is_nullable => 0, size => 255 },
    "settings",
    { data_type => "text", is_nullable => 0 },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->inflate_column('settings' => {
    deflate => sub {
        my ($inflated_value, $result_object) = @_;
        return Stream2::Data::JSON->deflate_to_json($inflated_value || {});
    },
    inflate => sub {
        my ($deflated_value, $result_object) = @_;
        return Stream2::Data::JSON->inflate_from_json($deflated_value || '{}');
    },
});

__PACKAGE__->has_many('recurring_report_users' => 'Stream2::Schema::Result::RecurringReportUser',
                      { 'foreign.recurring_report_id' => 'self.id' });

__PACKAGE__->many_to_many('users' => 'recurring_report_users', 'user' );

1;

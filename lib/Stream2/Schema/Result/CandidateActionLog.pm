package Stream2::Schema::Result::CandidateActionLog;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::CandidateActionLog - Every candidate action should be stored here ( for 30 days )

=cut

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('candidate_action_log');

__PACKAGE__->add_columns(
    id => {
        data_type => 'int',
        size      => '25',
        is_auto_increment => 1,
        is_nullable => 0,
    },
    action => {
        data_type   => 'varchar',
        size        => '255',
        is_nullable => 0,
    },
    candidate_id => {
        data_type => 'varchar',
        size      => '100',
        is_nullable => 0,
    },
    destination => {
        data_type => 'varchar',
        size      => '50',
        is_nullable => 0,
    },
    group_identity => {
        data_type => 'varchar',
        size      => '50',
        is_nullable => 0,
    },
    user_id => {
        data_type   => "integer",
        is_nullable => 0,
    },
    data => {
        data_type   => 'text',
        is_nullable => 1,
    },
    insert_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
        is_nullable => 0,
    },
    search_record_id => {
        data_type => 'int',
        size      => '25',
        is_nullable => 1,
    }
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    candidate_actions => 'Stream2::Schema::Result::CandidateActions',
    { 'foreign.action' => 'self.action' });

__PACKAGE__->belongs_to(
    cv_users => 'Stream2::Schema::Result::CvUser',
    { 'foreign.id' => 'self.user_id' });

__PACKAGE__->belongs_to(
    search_record => 'Stream2::Schema::Result::SearchRecord',
    { 'foreign.id' => 'self.search_record_id' });

__PACKAGE__->has_many(
    'sent_emails' => 'Stream2::Schema::Result::SentEmail',
    {'foreign.action_log_id' => 'self.id'});

__PACKAGE__->inflate_column('data' => {
    deflate => sub {
      my ($inflated_value, $result_object) = @_;
      return Stream2::Data::JSON->deflate_to_json($inflated_value || {});
    },
    inflate => sub {
      my ($deflated_value, $result_object) = @_;
      return Stream2::Data::JSON->inflate_from_json($deflated_value);
    },
});

sub sqlt_deploy_hook {
    my ($self, $sqlt_table) = @_;
    $sqlt_table->add_index( name => 'action_idx', fields => ['group_identity','candidate_id','action'] );
}

1;

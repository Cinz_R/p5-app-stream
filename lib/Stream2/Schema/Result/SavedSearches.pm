use utf8;
package Stream2::Schema::Result::SavedSearches;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::SavedSearches

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 TABLE: C<cv_watchdog>

=cut

__PACKAGE__->table("cv_saved_searches");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'varchar'
  size: 255
  is_nullable: 0

=head2 name

  data_type: 'varchar'
  size: 255
  is_nullable: 0

=head2 criteria_id

  data_type: 'varchar'
  size: 255
  is_nullable: 0

=head2 flavour

  data_type: 'varchar'
  size: 50
  is_nullable: 0

=head2 active

  data_type: 'tinyint'
  default_value: 1
  is_nullable: 0

=head2 insert_datetime

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 update_datetime

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "criteria_id",
  { data_type => "char", is_nullable => 0, size => 27, extra => { 'COLLATE' => 'utf8_bin' } },
  "flavour",
  { data_type => "varchar", is_nullable => 0, size => 50, default_value => 'search', extra => { 'COLLATE' => 'utf8_bin' } },
  "active",
  { data_type => "tinyint", default_value => 1, is_nullable => 0 },
  "insert_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
    set_on_create => 1,
  },
  "update_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
    set_on_create => 1,
    set_on_update => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

# You can replace this text with custom code or comments, and it will be preserved on regeneration

__PACKAGE__->belongs_to('criteria' => 'Stream2::Schema::Result::Criteria',
                        { 'foreign.id' => 'self.criteria_id' });

__PACKAGE__->belongs_to('user' => 'Stream2::Schema::Result::CvUser',
                        { 'foreign.id' => 'self.user_id' });

1;

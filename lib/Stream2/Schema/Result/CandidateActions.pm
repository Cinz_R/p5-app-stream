package Stream2::Schema::Result::CandidateActions;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::CandidateActions - "enumerated" values for the action log

=cut

__PACKAGE__->table('candidate_actions');

__PACKAGE__->add_columns(
    action => {
        data_type   => 'varchar',
        size        => '255',
        is_nullable => 0,
        unique => 1
    }
);

__PACKAGE__->add_unique_constraint([ qw/action/ ]);
1;
package Stream2::Schema::Result::CandidateTag;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::CandidateActionLog - Every candidate action should be stored here ( for 30 days )

=cut

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('candidate_tag');

__PACKAGE__->add_columns(
    id => {
        data_type => 'int',
        size      => '25',
        is_auto_increment => 1,
        is_nullable => 0,
    },
    group_identity => {
        data_type => 'varchar',
        size      => '50',
        is_nullable => 0,
    },
    candidate_id => {
        data_type => 'varchar',
        size      => '100',
        is_nullable => 0,
    },
    tag_name => {
        data_type => 'varchar',
        size => '100',
        is_nullable => 0,
    },
    user_id => {
        data_type   => "integer",
        is_nullable => 1,
    },
    insert_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
        is_nullable => 0,
    }
);

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint('candidate_tag_unique' => [ 'group_identity', 'candidate_id', 'tag_name' ]);


__PACKAGE__->belongs_to(cv_user => 'Stream2::Schema::Result::CvUser',
                        { 'foreign.id' => 'self.user_id' },
                        { join_type => 'left' });

__PACKAGE__->belongs_to(tag => 'Stream2::Schema::Result::Tag',
                        { 'foreign.group_identity' => 'self.group_identity',
                          'foreign.tag_name' => 'self.tag_name'});



## We dont need that for now here.
# sub sqlt_deploy_hook {
#     my ($self, $sqlt_table) = @_;
#     $sqlt_table->add_index( name => 'candidate_tag_idx1', fields => ['group_identity','candidate_id','action'] );
# }

1;

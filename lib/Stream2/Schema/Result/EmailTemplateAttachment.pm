use utf8;
package Stream2::Schema::Result::EmailTemplateAttachment;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Stream2::Schema::Result::EmailTemplateAttachment

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::FilterColumn>

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("FilterColumn", "InflateColumn::DateTime");

=head1 TABLE: C<email_template_attachment>

=cut

__PACKAGE__->table("email_template_attachment");

=head1 ACCESSORS

=head2 email_template_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 group_file_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "email_template_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "group_file_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</email_template_id>

=item * L</group_file_id>

=back

=cut

__PACKAGE__->set_primary_key("email_template_id", "group_file_id");


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2016-08-18 17:00:41
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:FbRJ381tSu7SZE/Fm0Dr7A

__PACKAGE__->belongs_to( 'group_file' => 'Stream2::Schema::Result::GroupFile' ,
                         { 'foreign.id' => 'self.group_file_id' });

__PACKAGE__->belongs_to( 'email_template' => 'Stream2::Schema::Result::EmailTemplate' ,
                         { 'foreign.id' => 'self.email_template_id' });

1;

use utf8;
package Stream2::Schema::Result::Groupsetting;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Stream2::Schema::Result::Groupsetting

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::FilterColumn>

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("FilterColumn", "InflateColumn::DateTime");

=head1 TABLE: C<groupsetting>

=cut

__PACKAGE__->table("groupsetting");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 group_identity

  data_type: 'varchar'
  is_foreign_key: 1
  is_nullable: 0
  size: 50

=head2 setting_mnemonic

  data_type: 'varchar'
  is_nullable: 0
  size: 255

=head2 setting_description

  data_type: 'text'
  is_nullable: 0

=head2 setting_type

  data_type: 'varchar'
  default_value: 'boolean'
  is_nullable: 0
  size: 100

=head2 default_boolean_value

  data_type: 'tinyint'
  is_nullable: 1

=head2 default_string_value

  data_type: 'varchar'
  is_nullable: 1
  size: 1024

=head2 setting_meta_data

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "group_identity",
  { data_type => "varchar", is_foreign_key => 1, is_nullable => 0, size => 50 },
  "setting_mnemonic",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "setting_description",
  { data_type => "text", is_nullable => 0 },
  "setting_type",
  {
    data_type => "varchar",
    default_value => "boolean",
    is_nullable => 0,
    size => 100,
  },
  "default_boolean_value",
  { data_type => "tinyint", is_nullable => 1 },
  "default_string_value",
  { data_type => "varchar", is_nullable => 1, size => 1024 },
  "setting_meta_data",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<groupsetting_mnemonic>

=over 4

=item * L</group_identity>

=item * L</setting_mnemonic>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "groupsetting_mnemonic",
  ["group_identity", "setting_mnemonic"],
);

=head1 RELATIONS

=head2 users_groupsetting

Type: has_many

Related object: L<Stream2::Schema::Result::UserGroupsetting>

=cut

__PACKAGE__->has_many(
  "users_groupsetting",
  "Stream2::Schema::Result::UserGroupsetting",
  { "foreign.setting_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2016-08-26 15:47:54
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:yD6WlRQFNX5kZy6fhIM+AA


# This relation HAS to be used within $stream2->with_context({ user_id => .. } ).
# Note that it is called user_settings like in the product level 'Setting'.
# This is to make sure we can use the same code.
__PACKAGE__->has_many('user_settings' => 'Stream2::Schema::Result::UserGroupsetting',
                      sub{
                          my $args = shift;
                          my $user_id = $args->{self_resultsource}->schema()->stream2()->context()->{user_id} // confess('Missing user_id in $stream2->context');
                          return {
                                  $args->{foreign_alias}.".setting_id"   => { -ident => $args->{self_alias}.".id" },
                                  $args->{foreign_alias}.".user_id" => $user_id # { -ident => $user_id } ident means another column, not a value
                                 };
                      });


__PACKAGE__->inflate_column('setting_meta_data' => {
    deflate => sub {
        my ($inflated_value, $result_object) = @_;
        return Stream2::Data::JSON->deflate_to_json($inflated_value || {});
    },
    inflate => sub {
        my ($deflated_value, $result_object) = @_;
        return Stream2::Data::JSON->inflate_from_json($deflated_value || '{}');
    },
});

__PACKAGE__->belongs_to('groupclient' => 'Stream2::Schema::Result::Group',
                        { 'foreign.identity' => 'self.group_identity' }
                    );

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

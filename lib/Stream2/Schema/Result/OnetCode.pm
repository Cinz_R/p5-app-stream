package Stream2::Schema::Result::OnetCode;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::OnetCode - Mapping for Onet ID to occupation title

=head1 DESCRIPTION

candidatesearch returns the Onet code as its facet so it looks like we have to do our own lookup

=cut

__PACKAGE__->table('onet_code');

__PACKAGE__->add_columns(
    onetsoc_code => {
        data_type   => 'varchar',
        size        => '10',
        is_nullable => 0
    },
    title => {
        data_type   => 'varchar',
        size        => '150',
        is_nullable => 0
    }
);

__PACKAGE__->set_primary_key('onetsoc_code');

1;

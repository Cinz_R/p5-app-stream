package Stream2::Schema::Result::SearchRecord;

use strict;
use warnings;

use Stream2::Data::JSON;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::SearchRecord - Represents a single search performed by a user

=cut

__PACKAGE__->load_components(qw/Core InflateColumn::DateTime TimeStamp InflateColumn/);

=head1 TABLE: C<search_record>

=cut

__PACKAGE__->table('search_record');

__PACKAGE__->add_columns(
    id => {
        data_type => 'int',
        size      => '25',
        is_auto_increment => 1,
        is_nullable => 0,
    },
    criteria_id => {
        data_type => "char",
        is_nullable => 0,
        size => 27,
        extra => {
            'COLLATE' => 'utf8_bin'
        }
    },
    board => {
        data_type => 'varchar',
        is_nullable => 1,
        size => 50,
        extra => {
            COLLATE => 'utf8_bin'
        }
    },
    user_id => {
        data_type   => "integer",
        is_nullable => 0,
    },
    insert_datetime => {
        data_type => 'DateTime',
        set_on_create => 1,
        is_nullable => 0,
    },
    n_results => {
        data_type   => "integer",
        is_nullable => 1,
    },
    remote_ip => {
        data_type => 'varchar',
        is_nullable => 1,
        size => 32,
    },
    data => {
        data_type   => 'text',
        is_nullable => 1,
    }
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to('criteria' => 'Stream2::Schema::Result::Criteria',
                        { 'foreign.id' => 'self.criteria_id' });

__PACKAGE__->belongs_to('user' => 'Stream2::Schema::Result::CvUser',
                        { 'foreign.id' => 'self.user_id' });

__PACKAGE__->belongs_to( board => 'Stream2::Schema::Result::Board',
                         { 'foreign.name' => 'self.board' } );

__PACKAGE__->inflate_column('data' => {
    deflate => sub {
      my ($inflated_value, $result_object) = @_;
      return Stream2::Data::JSON->deflate_to_json($inflated_value || {});
    },
    inflate => sub {
      my ($deflated_value, $result_object) = @_;
      return Stream2::Data::JSON->inflate_from_json($deflated_value);
    },
});



1;

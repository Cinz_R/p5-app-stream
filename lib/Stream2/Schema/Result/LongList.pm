package Stream2::Schema::Result::LongList;

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

Stream2::Schema::Result::LongList - A LongList in the DB

=cut


__PACKAGE__->table("longlist");

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_nullable => 0 },
  "name",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "insert_datetime",
  {
    data_type => "datetime",
    is_nullable => 0,
    set_on_create => 1,
  },
);


__PACKAGE__->set_primary_key("id");

__PACKAGE__->add_unique_constraint([ qw/user_id name/ ]);

__PACKAGE__->belongs_to('user' => 'Stream2::Schema::Result::CvUser',
                        { 'foreign.id' => 'self.user_id' });

__PACKAGE__->has_many('candidates' => 'Stream2::Schema::Result::LongListCandidate',
                      { 'foreign.longlist_id' => 'self.id' });

1;

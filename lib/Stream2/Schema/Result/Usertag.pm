use utf8;
package Stream2::Schema::Result::Usertag;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Stream2::Schema::Result::Usertag

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::FilterColumn>

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("FilterColumn", "InflateColumn::DateTime");

=head1 TABLE: C<usertag>

=cut

__PACKAGE__->table("usertag");

=head1 ACCESSORS

=head2 id

  data_type: 'bigint'
  is_auto_increment: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 tag_name

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=head2 insert_datetime

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  default_value: 'CURRENT_TIMESTAMP'
  is_nullable: 0

=head2 tag_name_ci

  data_type: 'varchar'
  is_nullable: 0
  size: 100

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "bigint", is_auto_increment => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "tag_name",
  { data_type => "varchar", is_nullable => 0, size => 100 },
  "insert_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    default_value => "CURRENT_TIMESTAMP",
    is_nullable => 0,
  },
  "tag_name_ci",
  { data_type => "varchar", is_nullable => 0, size => 100 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<user_id>

=over 4

=item * L</user_id>

=item * L</tag_name>

=back

=cut

__PACKAGE__->add_unique_constraint("user_id", ["user_id", "tag_name"]);


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2016-08-26 10:36:48
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:hyxqC7h7boD6A0rnjvlqGg


__PACKAGE__->belongs_to(
    user => 'Stream2::Schema::Result::CvUser',
    { 'foreign.id' => 'self.user_id' }
);

1;

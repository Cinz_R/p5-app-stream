package Stream2::Schema::Result::GroupFile;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::GroupFile - A file stored at group level

=cut

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('group_file');

__PACKAGE__->add_columns(
                         id => { data_type => 'int', size => '25', is_auto_increment => 1, is_nullable => 0 },
                         group_identity =>   { data_type => "varchar", is_nullable => 0, size => 50 },
                         name => { data_type => 'varchar', is_nullable => 0, size => 255 },
                         mime_type => { data_type => 'varchar' , size => 255, is_nullable => 0 },
                         public_file_key => { data_type => 'varchar' , size => 255 , is_nullable => 0},
                         insert_datetime => { data_type => 'DateTime', set_on_create => 1, is_nullable => 0 },
                         deleted_datetime => { data_type => 'DateTime', is_nullable => 1 } # Delete marker
                        );

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint([ qw/group_identity name/ ]);

__PACKAGE__->belongs_to('groupclient' => 'Stream2::Schema::Result::Group',
                        { 'foreign.identity' => 'self.group_identity' });

__PACKAGE__->has_many('email_template_attachments' => 'Stream2::Schema::Result::EmailTemplateAttachment',
                      { 'foreign.group_file_id' => 'self.id' });

1;


package Stream2::Schema::Result::LongListCandidate;

use strict;
use warnings;

use base 'DBIx::Class::Core';


=head1 NAME

Stream2::Schema::Result::LongListCandidate - A LongListed candidate in the DB

=cut


__PACKAGE__->table("longlist_candidate");

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");

__PACKAGE__->add_columns(
  "longlist_id",
  { data_type => "integer", is_nullable => 0 },
  "candidate_id",
  { data_type => "varchar", is_nullable => 0, size => 255 },
  "insert_datetime",
  {
    data_type => "datetime",
    is_nullable => 0,
    set_on_create => 1,
  },
  "candidate_content",
  { data_type => "mediumtext", is_nullable => 1 }

);


__PACKAGE__->set_primary_key(qw/longlist_id candidate_id/);

__PACKAGE__->belongs_to('longlist' => 'Stream2::Schema::Result::LongList',
                        { 'foreign.id' => 'self.longlist_id' });

__PACKAGE__->inflate_column( 'candidate_content', {
    inflate => sub {
        my ( $data, $result ) = @_;
        return JSON::from_json( $data || '{}' );
    },
    deflate => sub {
        my ( $data, $result ) = @_;
        return JSON::to_json( $data || {} , { ascii => 1 } );
    }
});


1;

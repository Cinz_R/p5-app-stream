use utf8;
package Stream2::Schema::Result::UserGroupsetting;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Stream2::Schema::Result::UserGroupsetting

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::FilterColumn>

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("FilterColumn", "InflateColumn::DateTime");

=head1 TABLE: C<user_groupsetting>

=cut

__PACKAGE__->table("user_groupsetting");

=head1 ACCESSORS

=head2 id

  data_type: 'bigint'
  extra: {unsigned => 1}
  is_auto_increment: 1
  is_nullable: 0

=head2 setting_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 admin_user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 admin_ip

  data_type: 'varchar'
  default_value: '127.0.0.1'
  is_nullable: 0
  size: 40

=head2 insert_datetime

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 update_datetime

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 boolean_value

  data_type: 'tinyint'
  is_nullable: 1

=head2 string_value

  data_type: 'varchar'
  is_nullable: 1
  size: 1024

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type => "bigint",
    extra => { unsigned => 1 },
    is_auto_increment => 1,
    is_nullable => 0,
  },
  "setting_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "admin_user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "admin_ip",
  {
    data_type => "varchar",
    default_value => "127.0.0.1",
    is_nullable => 0,
    size => 40,
  },
  "insert_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "update_datetime",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "boolean_value",
  { data_type => "tinyint", is_nullable => 1 },
  "string_value",
  { data_type => "varchar", is_nullable => 1, size => 1024 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<user_groupsetting_setting_id_user_id>

=over 4

=item * L</setting_id>

=item * L</user_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "user_groupsetting_setting_id_user_id",
  ["setting_id", "user_id"],
);

=head1 RELATIONS

=head2 setting

Type: belongs_to

Related object: L<Stream2::Schema::Result::Groupsetting>

=cut

__PACKAGE__->belongs_to(
  "setting",
  "Stream2::Schema::Result::Groupsetting",
  { id => "setting_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "RESTRICT" },
);


# Created by DBIx::Class::Schema::Loader v0.07045 @ 2016-09-02 14:31:40
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:/TCZWWtCnu/SJA0X/r2MDA

__PACKAGE__->belongs_to(
    user => 'Stream2::Schema::Result::CvUser',
    { id => 'user_id' }
);


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

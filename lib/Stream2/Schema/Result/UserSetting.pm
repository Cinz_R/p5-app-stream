package Stream2::Schema::Result::UserSetting;

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 NAME

Stream2::Schema::Result::UserSetting - Association table between user and settings.

=cut

__PACKAGE__->load_components(qw/InflateColumn::DateTime InflateColumn TimeStamp Core/);

__PACKAGE__->table('user_setting');

__PACKAGE__->add_columns(
                         "id",
                         { data_type => "bigint", is_auto_increment => 1, is_nullable => 0 },

                         # Structural columns
                         "setting_id",
                         { data_type => "integer", is_nullable => 0 },
                         "user_id",
                         { data_type => "integer", is_nullable => 0 },

                         # Audit columns
                         "admin_user_id",
                         { data_type => "integer", is_nullable => 0 },
                         "admin_ip",
                         { data_type => 'varchar', size => 40, is_nullable => 0 , default_value => '127.0.0.1'},
                         "insert_datetime",
                         {
                          set_on_create => 1,
                          data_type => "datetime",
                          datetime_undef_if_invalid => 1,
                          is_nullable => 1,
                         },
                         "update_datetime",
                         {
                          data_type => "datetime",
                          datetime_undef_if_invalid => 1,
                          is_nullable => 1,
                          set_on_create => 1,
                          set_on_update => 1,
                         },

                         ## Values columns (only one can be not null)
                         "boolean_value",
                         { data_type => "tinyint", is_nullable => 1 },
                         "string_value",
                         { data_type => "varchar", size => "1024", is_nullable => 1 }
                        );

__PACKAGE__->set_primary_key('id');
__PACKAGE__->add_unique_constraint([ qw/setting_id user_id/ ]);

__PACKAGE__->belongs_to('user' => 'Stream2::Schema::Result::CvUser',
                        { 'foreign.id' => 'self.user_id' });

__PACKAGE__->belongs_to('admin' => 'Stream2::Schema::Result::CvUser',
                        { 'foreign.id' => 'self.admin_user_id' });

__PACKAGE__->belongs_to('setting' => 'Stream2::Schema::Result::Setting',
                        { 'foreign.id' => 'self.setting_id' });


1;

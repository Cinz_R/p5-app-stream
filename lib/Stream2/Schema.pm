package Stream2::Schema;
use utf8;

use strict;
use warnings;
use Carp;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;

use Log::Any qw/$log/;


# DO NOT BUMP THIS UP EVER
# This is there for legacy reasons, until everyone has transitionned
# away from dbic-class-migration to sqitch.
our $VERSION = 121;

use DBI::Const::GetInfoType;

use DateTime;

use Scalar::Util;

use Digest::SHA qw//;

sub deploy{
    my ($self , @other_args ) = @_;

    $self->SUPER::deploy(@other_args);

    $self->with_sqllite(sub{
                            ## This replicates the Triggers that are there in MySQL
                            ## Useful to make the tests work.
                            $self->storage()->dbh_do(sub{
                                                        my ($storage, $dbh) = @_;
                                                        $dbh->do(q|
CREATE TRIGGER insert_tag AFTER INSERT ON tag
FOR EACH ROW
WHEN ( new.tag_name_ci IS NULL )
BEGIN

 UPDATE tag SET tag_name_ci = NEW.tag_name WHERE tag_name = NEW.tag_name AND group_identity = NEW.group_identity ;

END
|);

                                                        $dbh->do(q{
                                                            CREATE TRIGGER insert_watchdog_result AFTER INSERT ON watchdog_result
                                                            FOR EACH ROW
                                                            WHEN NEW.viewed=0
                                                            BEGIN
                                                                    UPDATE watchdog_subscription SET new_result_count=new_result_count+1
                                                                        WHERE watchdog_subscription.subscription_id = NEW.subscription_id AND watchdog_subscription.watchdog_id = NEW.watchdog_id;
                                                            END;
                                                        });

                                                        $dbh->do(q{
                                                            CREATE TRIGGER update_watchdog_result AFTER UPDATE ON watchdog_result
                                                            FOR EACH ROW
                                                            WHEN ( NEW.viewed = 0 AND OLD.viewed = 1 )
                                                            BEGIN
                                                                UPDATE watchdog_subscription SET new_result_count=new_result_count+1
                                                                    WHERE watchdog_subscription.subscription_id = NEW.subscription_id AND watchdog_subscription.watchdog_id = NEW.watchdog_id;
                                                            END;
                                                        });

                                                        $dbh->do(q{
                                                            CREATE TRIGGER update_watchdog_result2 AFTER UPDATE ON watchdog_result
                                                            FOR EACH ROW
                                                            WHEN ( NEW.viewed = 1 AND OLD.viewed = 0 )
                                                            BEGIN
                                                                UPDATE watchdog_subscription SET new_result_count=new_result_count-1
                                                                    WHERE watchdog_subscription.subscription_id = NEW.subscription_id AND watchdog_subscription.watchdog_id = NEW.watchdog_id;
                                                            END;
                                                        });

                                                        $dbh->do(q{
                                                            CREATE TRIGGER delete_watchdog_result AFTER DELETE ON watchdog_result
                                                            FOR EACH ROW
                                                            WHEN OLD.viewed = 0
                                                            BEGIN
                                                                UPDATE watchdog_subscription SET new_result_count=new_result_count-1
                                                                    WHERE watchdog_subscription.subscription_id = OLD.subscription_id AND watchdog_subscription.watchdog_id = OLD.watchdog_id;
                                                            END;
                                                        });

                                                        $dbh->do(q{
                   INSERT INTO setting(setting_mnemonic, setting_type,  setting_description, default_boolean_value, default_string_value) VALUES
                   ( 'candidate.actions.candidate_tag.enabled', 'boolean', 'Candidate Tagging Enabled', 1, NULL),
                   ( 'candidate.actions.note.enabled', 'boolean', 'Candidate Notes Enabled', 1, NULL),
                   ( 'candidate.actions.download.enabled', 'boolean', 'Candidate Download Enabled', 1, NULL),
                   ( 'candidate.actions.forward.enabled', 'boolean', 'Candidate Forward Enabled', 1 ,NULL),
                   ( 'candidate.actions.import.enabled', 'boolean', 'Candidate Save Enabled', 1, NULL),
                   ( 'candidate.actions.message.enabled', 'boolean', 'Candidate Message Enabled', 1, NULL),
                   ( 'candidate.actions.profile.enabled', 'boolean', 'Candidate View Profile Enabled', 1 , NULL),
                   ( 'candidate.actions.shortlist.enabled', 'boolean', 'Candidate Shortlist Enabled', 1 , NULL),
                   ( 'candidate.actions.update.enabled', 'boolean', 'Candidate Info Updating Enabled', 1 , NULL),
                   ( 'candidate.actions.forward.bulk.enabled', 'boolean', 'Candidate Bulk Forward Enabled', 1 , NULL),
                   ( 'candidate.actions.candidate_tag.bulk.enabled', 'boolean', 'Candidate Bulk Tagging Enabled', 1 , NULL),
                   ( 'candidate.actions.shortlist.bulk.enabled', 'boolean', 'Candidate Bulk Shortlist Enabled', 1 , NULL ),
                   ( 'candidate.actions.message.bulk.enabled', 'boolean', 'Candidate Bulk Message Enabled', 1 , NULL ),
                   ( 'behaviour-downloadcv-use_internal', 'string', 'Use Internal CV when possible', NULL , "0"),
                   ( 'criteria-cv_updated_within-default', 'string', 'Use Internal CV when possible', NULL , "3M");
                                                        });
                                                     })
                        });
}


=head2 escape_like

  Escapes a string to be used in a sql 'like' expression.

  Allows to use special characters like '%' in like expressions.

Usage:

  my $escaped = $this->escape_like($string);

=cut

sub escape_like{
    my ($self, $string) = @_;
    $string //= '';

    my $escape_char = '';
    $self
      ->storage->dbh_do(sub{
                            my ( $storage, $dbh ) = @_;
                            # Note that this does not work for SQLite. Works fine with MySQL though.
                            if( my $new_escape =  $dbh->get_info( $GetInfoType{SQL_SEARCH_PATTERN_ESCAPE} ) ){
                                $escape_char = $new_escape;
                            }
                        });
    $log->debug("SQL search pattern escape char is '".$escape_char."'");

    $string =~ s/([_%])/$escape_char$1/g;
    return $string;
}


=head2 with_sqllite

Executes the code according to the type of storage
of this schema.

This is badly named for historical reasons.

PostgreSQL

Usage:

 my $thing = $this->with_sqllite( sub{ SQLite code .. } , sub{ MySQL Code } , sub{ Postgresql Code } );

=cut

sub with_sqllite{
    my ($self, $sqllite_code , $mysql_code, $postgresql_code ) = @_;
    $sqllite_code    //= sub{};
    $mysql_code      //= sub{};
    $postgresql_code //= sub{};

    my $sqlt_type = $self->storage()->sqlt_type();

    if( $sqlt_type eq 'SQLite' ){
        return &$sqllite_code();
    }elsif( $sqlt_type eq 'PostgreSQL' ){
        return &$postgresql_code();
    }else{
        return &$mysql_code();
    }
}


# Connection parameters.
sub connection{
    my ($class, @args ) = @_;

    my $dsn;

    unless( ( ref $args[0] // '' ) eq 'CODE' ){
        $log->info("Enabling AutoCommit, RaiseError, mysql_enable_utf8 and pg_enable_utf8");
        $args[3] //= {};
        $args[3]->{AutoCommit} //= 1;
        $args[3]->{RaiseError} = 1;
        $args[3]->{mysql_enable_utf8} //= 1;
        $args[3]->{pg_enable_utf8} //= 1; # No pg for now but we want that maybe in the future.
        $args[3]->{sqlite_unicode} //= 1;
        $dsn = $args[0];
        if( $dsn =~ /mysql/i ){
            ## Only for mysql DSNs
            $log->info("Enabling MySQL TRADITIONAL mode");
            $args[3]->{on_connect_do} = ["SET SESSION sql_mode = 'TRADITIONAL'"];
        }
    }
    my $self = $class->next::method(@args);

    if( $dsn && (  $dsn =~ /SQLite/ ) ){
        # Implement missing functions.
        $self->storage->dbh_do(sub{
                                   my ($storage, $dbh, @cols) = @_;
                                   $dbh->func('NOW', 0, sub{
                                                  return DateTime->now()->strftime('%F %T');
                                              },
                                              'create_function')
                               });
    }

    if( $dsn && ( $dsn =~ /:Pg:/ ) ){
        # Switch on quotename, cause some relations are named 'user'
        $self->storage->sql_maker->quote_names(1);
    }


    # Extra properties of our own.
    # MySQL userspace locking depth can only be one per session.
    $self->{_my_lock_depth} = 0;

    return $self;
}

=head2 stream2

Set/Get the stream2 instance against this schema.

=cut

sub stream2{
    my ($self, $stream2) = @_;
    if( $stream2 ){
        $self->{_stream2} = $stream2;
        Scalar::Util::weaken( $self->{_stream2} );
        return $stream2;
    }
    return $self->{_stream2};
}

{

    # Process wide memory lock. Just for tests..
    my $MEMORY_LOCKS = {};

    sub _two_integers_key{
        my ($key) = @_;
        # Calculate a 2 x 32 bits key from the key
        my $twenty_bytes = Digest::SHA::sha1($key);
        my $four_first = substr($twenty_bytes, 0, 4);
        my $four_next = substr($twenty_bytes, 4, 4);
        # Build two 32 bits integers.
        my $ia = unpack('i' , $four_first);
        my $ib = unpack('i' , $four_next);
        return ( $ia, $ib );
    }


=head2 try_acquiring_lock

Small wrapper around DB GET_LOCK (for mysql) logic

=cut

    sub try_acquiring_lock{
        my ($self, $key, $timeout) = @_;

        if( $self->{_my_lock_depth} < 0 ){
            confess("Inconsistent _my_lock_depth");
        }

        if( $self->{_my_lock_depth} ){
            confess("You cannot try acquiring a lock (on $key) unless your released your previous locks. Blame MySQL");
        }

        $timeout //= 1;
        return
          $self->with_sqllite(sub{
                                  # Poorman's implementation
                                  if ( exists $MEMORY_LOCKS->{$key} ) {
                                      return 0;
                                  } else {
                                      $self->{_my_lock_depth}++;
                                      return $MEMORY_LOCKS->{$key} = 1;
                                  }
                              },
                              sub{
                                  $self->storage->dbh_do(sub{
                                                             my ($storage, $dbh) = @_;
                                                             my ($res) = $dbh->selectrow_array('SELECT GET_LOCK(?,?)', {} , $key , $timeout );
                                                             unless( defined $res ){
                                                                 confess("SELECT GET_LOCK on mysql crashed");
                                                             }
                                                             if( $res ){
                                                                 $self->{_my_lock_depth}++;
                                                             }
                                                             return $res;
                                                         });
                              },
                              sub{
                                  $self->storage->dbh_do(sub{
                                                             my ($storage, $dbh) = @_;
                                                             my ($res) = $dbh->selectrow_array('SELECT pg_try_advisory_lock(?,?)', {} , _two_integers_key($key) );
                                                             if( $res ){
                                                                 $self->{_my_lock_depth}++;
                                                             }
                                                             return $res;
                                                         });
                              }
                             );
    }

    sub release_lock{
        my ($self,$key) = @_;

        if( $self->{_my_lock_depth} > 0 ){
            $self->{_my_lock_depth}-- ;
        }

        return
          $self->with_sqllite(sub{ delete  $MEMORY_LOCKS->{$key} ; },
                              sub{
                                  $self->storage->dbh_do(sub{
                                                             my ($storage, $dbh) = @_;
                                                             my ($res) = $dbh->selectrow_array('SELECT RELEASE_LOCK(?)', {} , $key);
                                                             return $res;
                                                         });
                              },
                              sub{
                                  $self->storage->dbh_do(sub{
                                                            my ($storage, $dbh) = @_;
                                                            my ($res) = $dbh->selectrow_array('SELECT pg_advisory_unlock(?,?)', {} , _two_integers_key($key) );
                                                            return $res;
                                                        });
                              }
                             );
    }

    sub is_free_lock {
        my ( $self, $key ) = @_;
        return $self->with_sqllite(
            sub { #sqlite
                if ( exists $MEMORY_LOCKS->{$key} ) {
                    return 0;
                }
                return 1;
            },
            sub { #mysql
                $self->storage->dbh_do( sub {
                    my ($storage, $dbh) = @_;
                    my ( $res ) = $dbh->selectrow_array('SELECT IS_FREE_LOCK( ? )', {}, $key);
                    return $res;
                });
            },
            sub { #postgresql - http://stackoverflow.com/a/25214415
                $self->storage->dbh_do( sub {
                    my ($storage, $dbh) = @_;
                    my ($res) = $dbh->selectrow_array('SELECT 1 FROM pg_locks WHERE locktype = ? AND classid = ? AND objid = ?', {} , 'advisory', _two_integers_key($key) );
                    return $res;
                });
            }
        );
    }

}

1;

package Stream2::Developer;

use Moose;

=head1 NAME

Stream2::Developer - If you read this, this class is you.

=cut

use Log::Any qw/$log/;
use File::Which;
use Config qw//;
use DBIx::Class::Schema::Loader;

use Capture::Tiny;

has 'stream2' => ( is => 'ro', isa => 'Stream2' , required => 1 , weak_ref => 1);

has 'sqitch' => ( is => 'ro', isa => 'Str', lazy_build => 1 );

sub _build_sqitch{
    my ($self) = @_;
    my $sqitch = File::Which::which('sqitch');
    unless( $sqitch ){ confess("Cannot find 'sqitch' in ".$ENV{PATH}); }
    $log->info("Sqitch is at '$sqitch'");
    return $sqitch;
}

=head2 split_dbconfig

Returns something like

 { host => ..,
   database => ...,
   port => ...,
   username => ...,
   password => ...,
 }


=cut

sub split_dbconfig{
    my ($self) = @_;
    my $config = $self->stream2()->config();
    my $dsn = $config->{stream_dsn} // confess("No stream_dsn in ".$self->stream2()->config_place());

    my $res = {};
    foreach my $key ( qw/host database dbname port user mysql_socket/ ){
        my ( $value ) = ( $dsn =~ m/$key=([^;]+)/ );
        if( defined $value ){
            $res->{$key} = $value;
        }
    }
    $res->{database} //= $res->{dbname};

    $res->{username} = $config->{stream_username} if( $config->{stream_username} );
    $res->{password} = $config->{stream_password} // confess("No stream_password in ".$self->stream2()->config_place());

    return $res;
}


=head2 run_sqitch

Runs sqitch in the context of this app/configuration file.
With the given args.

Usage:

 $this->run_sqitch([ 'status' , '....' ]);

Will run something like:

 SQITCH_CONFIG=share/sqitch/sqitch.conf  sqitch --top-dir share/sqitch/ --registry bbcss status ..... db:mysql://apli2:apli2@/bbcss

=cut

sub run_sqitch{
    my ($self, $args) = @_;
    $args //= [];

    my $stream2 =  $self->stream2();
    my $share_dir = $stream2->shared_directory();

    my $sqitch_home = $share_dir.'sqitch/';

    my $dsn_hash = $self->split_dbconfig();

    my $command = 'SQITCH_CONFIG='.$sqitch_home.'/sqitch.conf '.$Config::Config{perlpath}.' -CAS '.$self->sqitch().' --top-dir '.$sqitch_home.' --registry '.$dsn_hash->{database};

    my $sqitch_command = '';
    if( @$args ){
        $sqitch_command = $args->[0];

        if( $sqitch_command eq 'add' ){
            ## Adding require specifying just the engine.
            $command .= ' --engine mysql ';
        }

        $command .= ' '.join(' ' , map{ "'".$_."'" } @$args );
        ## That is the target. Calculated from this config.
        if( $sqitch_command ne 'add' && $sqitch_command ne 'plan' ){
            $command .= sprintf(' -t db:mysql://%s:%s@%s%s/%s%s',
                                $dsn_hash->{username} || '',
                                $dsn_hash->{password} || '',
                                $dsn_hash->{host} || '',
                                ( $dsn_hash->{port} ? ':'.$dsn_hash->{port} : '' ),
                                $dsn_hash->{database},
                                ( $dsn_hash->{mysql_socket} ? '?mysql_socket='.$dsn_hash->{mysql_socket} : '' ) );
        }
    }

    $log->info("Doing Sqitch Command: ".$command);

    if( $sqitch_command ne 'add' ){
        # add can be interactive, which doesnt play well with Capture::Tiny
        my ($stdout, $stderr , $exit) = Capture::Tiny::capture {
            system( $command );
        };
        $log->info($stdout);
        $log->warn($stderr);
        $log->info("Exit code was '$exit'");
        if( $exit ){
            die "Sqitch failed with exit code=$exit\n";
        }
    }else{
        my $exit = system( $command );
        if( $exit ){
            die "Sqitch command exited with $exit\n";
        }
    }

    return 1;
}

=head2 update_quota_schema

Runs the sql patches for the quota library, updating the
quota schema if there are any new patches to apply.

=cut

sub update_quota_schema {
    my ($self) = @_;
    $log->info('Attempting to update quota tables with new patches');
    my $quota_object = $self->stream2->quota_object();
    $quota_object->dbic_schema->deploy_update_schema();
    $log->info('Finished applying quota sql patches');
}

=head2 dump_schema

Dumps the schema in this source code.

=cut

sub dump_schema{
    my ($self, $args) = @_;

    $args //= {};

    my $db_conf = $self->stream2->config();
    my $dump_dir = __FILE__;
    $dump_dir =~ s/Stream2\/Developer\.pm//;

    $log->info("Dumping schema from DB into ".$dump_dir);
    DBIx::Class::Schema::Loader::make_schema_at(
        'Stream2::Schema',
        {
            qualify_objects => 0,
            exclude => qr/\A(?:changes|dependencies|events|projects|releases|tags|chi_cv_users|chi_stream2)\z/,
            # debug => 1,
            dump_directory => $dump_dir,
            components => ["FilterColumn", "InflateColumn::DateTime"],
            filter_generated_code => sub {
                my ( $type, $class, $text ) = @_;

                # we don't want to dump the Stream2::Schema object, this
                # stops that generated code from being written
                if ( $type eq 'schema' ){
                    return;
                }

                return $text;
            },
            %$args,
        },
        [
            $db_conf->{stream_dsn}, $db_conf->{stream_username}, $db_conf->{stream_password}, {}
        ]
    );
}

=head2 dump_tables

Dump a list of tables

Usage:

    $this->dump_tables( [ 'table_1', ... ] );

=cut

sub dump_tables {
    my ( $self, $table_names ) = @_;
    my $table_joined = join( '|', map { quotemeta( $_ ) } @$table_names );
    my $regex = qr/\A(?:$table_joined)\z/;
    return $self->dump_schema({
        constraint => $regex,
    });
}

__PACKAGE__->meta->make_immutable();
1;

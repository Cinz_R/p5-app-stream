package Stream2::Process;

use Moose;

extends qw/Schedule::LongSteps::Process/;

=head1 NAME

Stream2::Process - A base class for all LongStep processes in Stream2

=cut

has 'stream2' => ( is => 'ro', isa => 'Stream2' , required => 1 , weak_ref => 1);

__PACKAGE__->meta()->make_immutable();
1;

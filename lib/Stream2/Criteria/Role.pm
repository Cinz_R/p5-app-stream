package Stream2::Criteria::Role;

use Moose::Role;
use Digest::SHA1;
use MIME::Base64::URLSafe;
use Stream2::Data::JSON;

use Carp qw/confess/;

requires '_id', '_tokens';

has _id_is_dirty => ( is => 'rw', isa => 'Bool', default => 0 );

sub id {
  my $self = shift;

  die "ID is readonly. It is auto generated from the criteria" if @_;

  if ( $self->_id_is_dirty ) {
    $self->recalculate_id();
  }

  return $self->_id()
    || $self->recalculate_id();
}

sub recalculate_id {
  my $self = shift;

  $self->_id_is_dirty(0);

  my $new_id = $self->calculate_id();
  return $self->_id( $new_id );
}

sub calculate_id {
  my $self = shift;

  my $data = Stream2::Data::JSON->deflate_to_json( $self->_tokens() );
  return MIME::Base64::URLSafe::urlsafe_b64encode(Digest::SHA1::sha1($data));
}

=head2 get_token

Returns the value(s) of the given token name.

Usage:

 my @values = $this->get_token('some_token');

=cut

sub get_token {
  my ($self, $token) = @_;

  my $tokens= $self->_tokens
    or return;

  my $value = $tokens->{$token};
  return if !defined($value);
  if ( ref($value) ) {
    return wantarray ? @$value : $value->[0];
  }

  return $value;
}

=head2 add_token

Shortcut to add_tokens

=cut

sub add_token { shift->add_tokens( @_ ) }


=head2 add_tokens

Adds pairs of name , values.

Usage:

 $this->add_tokens( 'just_one' , 'one value' );
 $this->add_tokens( 'just_one' , [ 'first value' , 'second value' ]);

 $this->add_tokens( 'first_token' , 'first value' , 
                    'second_token' , [ 'some' , 'collection' , 'of', 'values' ] );

=cut

sub add_tokens {
  my $self = shift;
  my @new_tokens = @_
    or return;

  my $tokens = $self->_tokens || {};
  while ( @new_tokens ) {
    my ($token, $value) = (shift @new_tokens, shift @new_tokens);

    confess "This token does not belong in the criteria"
        if ( $token eq '_search_terms' );

    my $values = $tokens->{$token} ||= [];
    push @$values, ref($value) eq 'ARRAY' ? @$value : $value;
  }

  $self->_id_is_dirty(1);

  return $self;
}

=head2 remove_token

Shortcut to remove_tokens

=cut

sub remove_token { shift->remove_tokens(@_) }

=head2 remove_tokens

Removes the given token names from this criteria. Returns the collection
of removed multivalues in case you need them.

Usage:

 my $values_removed =  $this->remove_tokens('to_remove');

 my ( $values_removed1 , $values_removed2 ) =  $this->remove_tokens('to_remove1' , 'to_remove2');

=cut

sub remove_tokens {
  my $self = shift;

  my $tokens = $self->_tokens()
    or return ();

  my @deleted;
  foreach my $token ( @_ ) {
    push @deleted, delete $tokens->{$token};
  }

  $self->_id_is_dirty(1) if @deleted;

  return @deleted;
}

=head2 remove_tokens_starting

Same as remove_tokens, but works with token prefixes.

=cut

sub remove_tokens_starting {
  my ($self, $prefix) = @_;

  my @matching_tokens = grep { m/^\Q$prefix\E_/ } $self->token_names;

  if ( !@matching_tokens ) {
    return;
  }

  return $self->remove_tokens(@matching_tokens);
}

sub tokens {
  return %{ $_[0]->_tokens() };
}

sub token_names {
  my ($self) = @_;
  my $tokens = $self->_tokens()
    or return;

  return keys %$tokens;
}

sub keywords {
    return $_[0]->_keywords;
}
1;

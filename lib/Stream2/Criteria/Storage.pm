package Stream2::Criteria::Storage;

use Moose::Role;
use Try::Tiny;

use Log::Any qw/$log/;

requires 'id', 'tokens';

has 'schema' => (
  is  => 'ro',
  isa => 'DBIx::Class::Schema',
);

sub _rs { return $_[0]->schema->resultset('Criteria'); }

sub save {
  my $self = shift;

  # we need to do a bit of cleaning before storing in the DB. i.e. ms characters in the keywords
  $self->tidy_query();

  my $keywords = $self->keywords;
  if ( length( $keywords ) > 2048 ){
      $log->infof( "Truncating keyword string for auto-complete column" );
      $keywords = substr( $keywords, 0, 2048 );
  }

  # we could use find_or_create here
  # but that is not atomic so there is still
  # the chance that two processes will try and create
  # the same row at the same time and error out
  try {
    $self->_rs->update_or_create({
      id     => $self->id,
      tokens => { $self->tokens },
      keywords => $keywords,
    });
  }
  catch {
      ## We need to adapt that to any versions of MySQL/SQLLite(in test mode).
    my $sqlite_already_exists = 'column id is not unique';
    my $new_sqlite_already_exists = 'UNIQUE constraint failed';
    my $mysql_already_exists = 'Duplicate entry';

    if ( $_ =~ m/$sqlite_already_exists|$mysql_already_exists|$new_sqlite_already_exists/o ) {
      # The insert failed because the row already exists
    }
    else {
      die $_;
    }
  };
}

sub tidy_query {
    my $self = shift;

    my $keywords = $self->_tokens->{keywords};
    $keywords = ref($keywords) eq 'ARRAY' ? $keywords->[0] : $keywords // '';
    if ( $keywords ){
        $self->_tokens->{keywords} = [ _filter_keywords( $keywords ) ];
        $self->keywords( $keywords );
    }
}

sub _filter_keywords {
    my $str = shift;
    $str =~ s/\x{2013}/-/g;
    $str =~ s/\x{2014}/-/g;
    $str =~ s/\x{2015}/-/g;
    $str =~ s/\x{2017}/_/g;
    $str =~ s/\x{2018}/'/g;
    $str =~ s/\x{2019}/'/g;
    $str =~ s/\x{201a}/,/g;
    $str =~ s/\x{201b}/'/g;
    $str =~ s/\x{201c}/"/g;
    $str =~ s/\x{201d}/"/g;
    $str =~ s/\x{201e}/"/g;
    $str =~ s/\x{2026}/.../g;
    $str =~ s/\x{2032}/'/g;
    $str =~ s/\x{2033}/"/g;
    return $str;
}

sub load {
  my $self = shift;

  my $criteria_row = $self->_rs->find($self->id)
    or die "No criteria for " . $self->id;

  $self->_tokens( $criteria_row->tokens );

  return $self;
}

1;

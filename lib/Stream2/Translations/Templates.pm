package Stream2::Translations::Templates;
use strict; use warnings;

use Locale::Messages;
use Encode;
use Log::Any qw/$log/;
use Carp;

use base 'Stream2::Translations';

use Locale::TextDomain ( 'stream-templates', __PACKAGE__->get_shared_directory() . 'translations/compiled' );

sub _setup_overrides {
    my $class = shift;

    Locale::Messages::bind_textdomain_filter( 'stream-templates', sub{
        my $text = shift;
        # some of the feeds contain utf-8 characters but already have
        # the use utf8 turned on so this can be perlstring sometimes
        if ( utf8::is_utf8( $text ) ){
            return $text;
        }
        return Encode::decode( 'UTF-8', $text, Encode::FB_WARN );
    });

    local $SIG{__WARN__}=sub{};
    no strict 'refs';
    for my $method ( qw( __ __x __n __nx __xn __p __px __np __npx) ) {
        *{$class.'::'.$method} = sub {
            my $self = shift;
            # if the string is blank return a blank as the empty
            # string in our PO files are the PO header :(
            # we dont check for context, because why should we have context
            # for empty strings.
            return '' if !defined $_[0] || $_[0] eq '';
            return &{'Locale::TextDomain::'.$method}( @_ );
        };
    }
}

1;

package Stream2::Translations::MockStash;

use Moose;

use Log::Any qw/$log/;

use Data::Dumper;

use Locale::PO;

has 'template'    => ( is => 'ro' , isa => 'Str' , required => 1 );
has 'no_location' => ( is => 'ro' , isa => 'Int' , required => 0, default => 0 );
has 'locale_pos'  => ( is => 'ro' , isa => 'ArrayRef[Locale::PO]' , default => sub{ []; } );

=head1 NAME

Stream2::Translations::MockStash - A Template toolkit stash mockery that records a list of Locale::PO objects from ->get('i18n')

=head1 SYNOPSIS

See Stream2::Translations scan_tts_forpos

=cut

sub get{
    my ($self , $args) = @_;

    # Record a Locale::PO from the first args
    unless( ref($args) && ref($args) eq 'ARRAY' ){
        return '';
    }

    unless( $args->[0] =~ /^i18n/ ){
        return '';
    }

    # Args[0] is i18n, the string to translate is the first element of args->[1]
    unless( $args->[1] && (ref($args->[1] // '' ) eq 'ARRAY'  ) ){
        $log->warn("Cannot extract any string from ".Dumper($args));
        return '';
    }

    my $msgid = $args->[1]->[0] // '';

    $log->info("FOUND msgid = '$msgid' in template ".$self->template());

    my %options = (
        -msgid => $msgid,
        -msgstr => ''
    );
    unless( $self->no_location ){
        $options{'-reference'} = $self->template();
    }

    my $po = new Locale::PO( %options );

    push @{$self->locale_pos}, $po;


    return 'matched';

}


__PACKAGE__->meta->make_immutable();

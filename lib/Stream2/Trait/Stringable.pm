package Stream2::Trait::Stringable;
{
  $Stream2::Trait::Stringable::VERSION = '0.001';
}

use Moose::Role;

Moose::Util::meta_attribute_alias('Stringable');

has stringable => (
  is        => 'rw',
  isa       => 'Bool',
  predicate => 'has_stringable',
);

has stringify  => (
  is        => 'rw',
  isa       => 'CodeRef',
  predicate => 'has_stringify',
);

1;

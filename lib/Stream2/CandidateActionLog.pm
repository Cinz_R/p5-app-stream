package Stream2::CandidateActionLog;

use Moose;

## This should be a user ID, not a user object.
has 'schema' => (
    is => 'ro',
    required => 1,
);

=head2 insert

Small abstraction of DBIx create

Note that 'an_action_name' MUST 

usage:

  $this->insert({ action => 'an_action_name' ,
                  action          => $action,
                  candidate_id    => $candidate->id,
                  destination     => $candidate->destination,
                  user_id         => $user->user_id,
                  group_identity  => $user->group_identity,
                  data            => $data || undef,
                });

=cut

sub insert {
    my $self = shift;
    my $options_ref = shift;

    return $self->schema->resultset('CandidateActionLog')
        ->create($options_ref);
}

=head2 get_actions

accepts a user_id, group_identity and a list of candidate_ids

returns:

 {
     candidateid123 => { download => <A random action of this type>, shortlist => <A random action of this type> },
     candidateid321 => { message => <A random action of this type> }
 }

munges a RS into a hashref by candidate ID and action.
Note that the returned action instances are not very useful, because they are drawn randomly from
the set of actions for this user.

=cut

sub get_actions {
    my $self = shift;
    my @candidate_actions_rows = @{ $self->list( @_ ) };
    my $row_by_cand_action = {};
    map {
        # Store the Row itself against candidateId -> Action
        $row_by_cand_action->{$_->candidate_id}->{$_->action} = $_;
    } @candidate_actions_rows;
    return $row_by_cand_action;
}

=head2 get_all_actions

Like get_actions, but instead of having a random action for each action type, you
get the actual list of actions.

Example:

  {
     candidateid123 => { download => [ download , action, instances ] , shortlist => [ shortlist, action, instances ] },
     candidateid321 => { message =>  [ message, action, instances ] }
  }

=cut

sub get_all_actions {
    my $self = shift;
    my @candidate_actions_rows = @{ $self->list( @_ ) };

    my $row_by_cand_action = {};
    foreach my $row (@candidate_actions_rows) {
        $row_by_cand_action->{ $row->candidate_id }->{ $row->action } //= [];
        push @{ $row_by_cand_action->{ $row->candidate_id }->{ $row->action } }, $row;
    }

    return $row_by_cand_action;
}

sub get_notes {
    my $self = shift;
    my ( $user_id, $group_identity, $candidate_ids ) = @_;
    my $rs = $self->schema->resultset('CandidateActionLog')->search(
        {
            action          => 'note',
            candidate_id    => { 'in' => $candidate_ids },
            group_identity  => $group_identity,
        },
        {
            order_by        => { -desc => 'insert_datetime' }
        }
    );

    my $notes_by_cand = {};

    map {
        push @{$notes_by_cand->{$_->candidate_id}}, $_;
    }
      # As of story 79104524 , there is no concept of private notes anymore.
      # grep {
      #     $_->user_id eq $user_id
      #       || $_->data->{privacy} eq 'group'
      #   }
      $rs->all();

    return $notes_by_cand;
}

=head2 list

Accepts user_id, group_identity and candidate ids, returns an arrayref of L<Stream2::O::CandidateActionLog>

Runs a query to retrieve candidate actions to take a user_id and match on group_identity
when the action IS NOT 'profile' or match on user_id when action IS 'profile'

=cut

sub list {
    my ( $self, $user_id, $group_identity, $candidate_ids ) = @_;

    my $rs = $self->schema->resultset('CandidateActionLog')->search({
                                                                     candidate_id    => { 'in' => $candidate_ids },
                                                                     group_identity  => $group_identity,
                                                                    });

    my @list = $rs->all();
    # Filter out the list 'profile' actions where the user ID
    # is not mine.
    @list = grep{ $_->action() ne 'profile' || $_->user_id == $user_id } @list;

    return \@list ;
}

=head2 list_all

accepts group_identity and candidate ids, returns all instances
of L<Stream2::O::CandidateActionLog>
e.g. for notes or audit.

=cut

sub list_all {
    my $self = shift;
    my ( $group_identity, $candidate_ids ) = @_;

    my $rs = $self->schema->resultset('CandidateActionLog')->search(
        {
            'me.candidate_id'    => { 'in' => $candidate_ids },
            'me.group_identity'  => $group_identity
        },
        {
            order_by => [ { -desc => 'me.id' } ],
            prefetch => "cv_users",
        }
    );

    return [ $rs->all() ];
}

__PACKAGE__->meta->make_immutable();

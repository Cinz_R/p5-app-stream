package Stream2::DBICWrapper;

use Moose;
extends  qw/DBIx::Class::Wrapper::Factory/;

=head1 NAME

Stream2::DBICWrapper - Base class for all Stream2 DBIC Wrappers.

=cut

has '+bm' => ( isa => 'Stream2' );

=head2 stream2

alias for ->bm() . Returns the business model 'Stream2'

=cut

has 'stream2' => ( is => 'ro' , isa => 'Stream2' , lazy_build => 1);

sub _build_stream2{
    my ($self) = @_;
    return $self->bm();
}



__PACKAGE__->meta->make_immutable();

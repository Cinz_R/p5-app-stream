package Stream2::Criteria;

use Moose;

use JSON;
use Stream::Keywords;
use Log::Any qw/$log/;

has '_id' => (
  is => 'rw',
  isa => 'Str',
  init_arg => 'id',
);

has '_tokens' => (
  is => 'rw',
  isa => 'HashRef',
  init_arg => 'tokens',
  lazy => 1,
  builder => '_build_tokens',
);

has '_keywords' => (
    is => 'rw',
    isa => 'Str',
    init_arg => 'keywords',
    lazy => 1,
    builder => '_build_keywords',
    );


with 'Stream2::Criteria::Role';
with 'Stream2::Criteria::Storage';


=head2 search_terms

    use Stream::Keywords to extract important words from the keywords
    this will allow highlighting of text etc

    usage:

        my $criteria = Stream2::Criteria->new({
            keyword => 'PHP AND toilet OR Steve Jobs'
        });

        my @terms = $criteria->search_terms();

    out:
        ( 'PHP', 'toilet', 'Steve Jobs' )

=cut

sub search_terms {
    my ( $self ) = @_;
    my $keywords = $self->keywords || $self->_tokens->{keywords};
    if ( $keywords ) {
        if ( ref( $keywords ) eq 'ARRAY' ){
            $keywords = $keywords->[0];
        }
        my $bs = Stream::Keywords->new( $keywords );
        my @words = grep {
            $_
        } ( $bs->and_words(), $bs->or_words(), $bs->exact_words() );
        return @words;
    }
    return ();
}

sub _build_tokens {
  return {};
}

sub _build_keywords {
    return '';
}

=head2 clone

Returns a clone of this criteria object (not saved).

Note that this method relies on this class to consume the roles
S2::C::Role and S2::C::Storage, which is always the case.

Usage:

 # Raw cloning

 my $clone = $this->clone();

 .. fiddle with tokens ..
 my @values = $clone->get_token('whatever');

 $clone->remove_token('whatever');
 $clone->add_token('whatever' , \@values );

 $clone->save();

 # New criteria id:
 print $clone->id();


 # Clone when you know what criteria you want to override:

 $this->clone({ criteria => [ v1 , v2 ] });

=cut

sub clone{
    my ($self, $with) = @_;

    $with //= {};

    my $class = ref($self);

    # Make sure self is loaded
    $self->load();

    my $clone = $class->new({ schema => $self->schema() });
    my %tokens = $self->tokens();

    my $new_tokens = { %tokens , %$with };

    $clone->add_tokens( %$new_tokens );
    return $clone;
}

=head2 is_empty

Returns true if the criteria doesnt have any set token values.

=cut

sub is_empty{
    my ($self) = @_;
    my %tokens = $self->tokens();
    foreach my $key ( keys %tokens ){
        my $first_v = ( $tokens{$key} // [] )->[0];
        if( length( $first_v // '' ) ){ # Empty string doesnt count as a value
            $log->info("KEY $key has got non empty value '$first_v'. Not empty");
            return 0;
        }
    }
    return 1;
}

=head2 display_string

Returns a string suitable to display a quick view of these criteria

=cut

sub display_string{
    my ($self) = @_;

    my $name;
    my %tokens = $self->tokens;

    my @name_fields = qw/keywords location/;
    foreach my $name_field (@name_fields){
        #Sometimes sent through an array
        if ($tokens{$name_field} && ref($tokens{$name_field}) eq 'ARRAY'){
            $tokens{$name_field} = $tokens{$name_field}[0];
        }
        #If it has a value, append it to the name
        if (my $name_value = $tokens{$name_field}){
            if (length($name_value) > 100){ #Limit to 100 chars, mainly for keywords
                $name_value = substr($name_value, 0, 100) . '...';
            }
            $name .= $name ? ', ' . $name_value : $name_value;
        }
    }
    return $name || 'Empty Search';
}

=head2 from_query

Creates, saves and returns a new criteria object based on the given search query paramters

=cut

sub from_query {
    my ($class, $schema, $query) = @_;

    my $criteria = $class->new(
        schema => $schema,
        keywords => ref($query->{keywords}) eq "ARRAY" ? $query->{keywords}->[0] : $query->{keywords} // '',
    );

    foreach my $field ( keys %$query ) {
        my $value = $query->{$field};

        #  boolean values from the front end to `back end` boolean values
        if ( JSON::is_bool( $value ) ){
            $value = ( $value eq JSON::true ) ? 1 : 0;
        }

        $criteria->add_token($field => $value);
    }
    $criteria->save();

    return $criteria;
}


__PACKAGE__->meta->make_immutable;

package Stream2::Report::Usage;

=head1

    Stream2 Usage Report

=head2

    Display breakdown per given company of total/success/error searches

=cut

use Moose;

has 'stream2' => ( is => 'ro' , isa => 'Stream2' , required => 1 );

has 'companies' => ( is => 'ro', 'isa' => 'ArrayRef[Str]', required => 1 );

has 'time_from' => ( is => 'ro', isa => 'Str', required => 1 );
has 'time_to' => ( is => 'ro', isa => 'Str', required => 1 );

=head2 generate

    usage:
        my $report = Stream2::Report::Usage->new({
            stream2 => $s2,
            companies => [qw/manpower empiric khr/],
            time_from => "2014-11-22T00:00:00",
            time_to => "2014-11-22T23:59:59"
        });
        my $out = $report->generate();

        {
            manpower => {
                total => '55',
                success => '54',
                error => '0'
            },
            ...
        }

=cut

sub generate {
    my ( $self, $companies_text, $time_from, $time_to ) = @_;

    my $request = $self->_generate_request( $self->time_from, $self->time_to, $self->companies );

    my $response = $self->stream2()->aws_elastic_search()->search({
            index   => 'threads',
            type    => 'stream2search',
            body    => $request
    });
    my $out = $self->_munge( $response );

    return $out;
}

sub _munge {
    my ( $self, $resp ) = @_;
    my $out_ref = {};
    foreach my $facet ( keys %{$resp->{facets}} ){
        foreach my $term ( @{$resp->{facets}->{$facet}->{terms}} ){
            $out_ref->{$term->{term}}->{$facet} = $term->{count};
        }
    }
    return $out_ref;
}

sub _generate_request {
    my ( $self, $date_from, $date_to, $companies ) = @_;
    my $facet_size = scalar( @$companies );
    return {
        "query"=> {
            "filtered"=> {
                "query"=> {
                    "match_all"=> {}
                },
                "filter"=> {
                    "and"=> [
                       {
                            "range"=> {
                                "time_completed"=> {
                                    "to"=> $date_to,
                                    "from"=> $date_from
                                }
                            }
                        },
                        {
                            "terms"=> {
                                "company"=> $companies
                            }
                        }
                    ]
                }
            }
        },
        "size"=> 0,
        "facets"=> {
            "total"=> {
                "terms"=> {
                    "field"=>"company",
                    "size"=> $facet_size
                }
            },
            "error"=> {
                "terms"=> {
                    "field"=>"company",
                    "size"=> $facet_size
                },
                "facet_filter"=> {
                    "exists" => {
                        "field"=> "error_type"
                    }
                }
            },
            "success"=> {
                "terms"=> {
                    "field"=>"company",
                    "size"=> $facet_size
                },
                "facet_filter"=> {
                    "not"=> {
                        "exists" => {
                            "field"=> "error_type"
                        }
                    }
                }
            }
        }
    };
}

__PACKAGE__->meta->make_immutable;

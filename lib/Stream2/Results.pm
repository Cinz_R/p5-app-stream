package Stream2::Results;

use strict;

use Moose;
use Stream2::Results::Result;

with 'Stream2::Role::AutoId';

has 'results' => (
    is      => 'ro',
    isa     => 'ArrayRef',
    traits  => ['Array'],
    lazy    => 1,
    builder => 'default_results',
    handles => {
      add_candidate => 'push',
    },
);


# Some feeds may support faceting, this means the search criteria
# are only available once the search has been ran and the results are returned.
# facets of the search result. Array of hashrefs like this:
# [
#   {
#     Label   => 'Order By',
#     Type    => 'list',
#     Id      => 'talentsearch_order_by',
#     Name    => 'order_by',
#     Options => [{
#       value => 'application_time',
#       label => 'Application Time',
#       count => 7,
#       ...
#     }]
#   },
#   ...
# ]
# Can be set by the feed.

has 'facets' => ( is => 'rw' , isa => 'ArrayRef[HashRef]', default => sub{ [] } );

has 'current_page' => (
    is      => 'rw',
    default => 1,
);
has 'per_page' => (
    is      => 'rw',
    default => 50,
);

has 'results_per_scrape' => (
    is      => 'rw',
    default => undef,
);

has 'max_pages' => ( # we either get the max_pages or the total_results
    is      => 'rw',
    default => undef,
);

has 'success' => (
    is => 'rw',
    default  => 1,
);

has 'destination' => (
    is => 'rw',
    default => undef,
);

# An array of user notices.
has 'notices' => ( is => 'rw' , isa => 'ArrayRef[HashRef]' , default => sub{ []; } );

## The updated count of the viewed watchdog subscription in case
## these results viewing are the result of a watchdog subscription viewing action
has 'new_wd_subscription_count' => ( is => 'rw' , isa => 'Maybe[Int]' , default => undef );

#has 'start' => (
#    is => 'rw',
#    default => 1,
#);

sub start {
    my $self = shift;
    return 1 + ($self->per_page * ($self->current_page() - 1));
}

sub default_results {
  return [];
}

sub load {
    return ();
}

sub n_results { return $_[0]->new_results(); }
sub new_results { return scalar(@{ $_[0]->results() }); } 

has 'total_results' => (
    is => 'rw',
    isa => 'Maybe[Int]'
);

=head2 add_notice

Pushes the given candidate notice in this result. The default is to add
the given message at 'warning' level.

Usage:

  $this->add_notice('You cannot do this or that');
  $this->add_notice({ level => 'info' , message => 'Chips and gravy' });


Available levels are (For now):

 warning
 info

=cut

sub add_notice{
    my ($self, $message) = @_;
    unless( $message ){ confess("No message given") ; }
    unless( ref($message) ){
        $message = { level => 'warning',
                     message => $message
                 };
    }
    push @{$self->notices()} , $message;
    return $message;
}

=head2 new_candidate

Builds an empty L<Stream2::Results::Result>

=cut

sub new_candidate {
  return Stream2::Results::Result->new();
}

=head2 find

Finds the L<Stream2::Results::Result at the given index in these results.
Will die if no such thing exists.

Usage:

 my $candidate = $this->find(0);

=cut

sub find{
    my ($self, $idx) = @_;

    my $hash = $self->results()->[$idx];
    unless( $hash ){
        confess("Cannot find any candidate at index $idx");
    }

    # Just build an object from the hash
    return Stream2::Results::Result->new($hash);
}

__PACKAGE__->meta->make_immutable;

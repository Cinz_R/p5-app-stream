package Stream2::Factory::PParseLog;

use Moose;

# multiple inheritance: eek: subclassing Stream2::Factory just to get a
# reference to its bm member
extends qw/Stream2::Factory::S3FileBase/;

=head1 NAME

Stream2::Factory::PParseLog - A Factory for private Persistent parse S3 logs

=head1 DESCRIPTION

A factory for persistent parse S3 logs.

=cut

# we're a singleton
my $INSTANCE;
around new => sub {
    my ( $orig, $class, $attrs ) = @_;

    unless ( $INSTANCE ) {
        $attrs //= {};
        # config from conf file
        my $config = $attrs->{bm}->config->{aws};

        $attrs->{config} = {
            aws_access_key_id     => $config->{aws_access_key_id},
            aws_secret_access_key => $config->{aws_secret_access_key},
            %{ $config->{ $class->aws_config_key() } // confess("Missing ".$class->aws_config_key()." in ".$attrs->{bm}->config_place() ) }
        };

        $INSTANCE = $class->$orig( $attrs );
    }
    return $INSTANCE;
};


sub aws_config_key{ return 'pparse_logfiles_s3' }

=head2 get_response_log_url

Returns the given company/response_id log temporary accessible URL
or undef if this log is not in this S3 factory.

=cut

sub get_response_log_url{
    my ($self, $company, $response_id) = @_;
    # Is this log in S3 already?
    # This UUID/key is composed the same way as in Bean::Aplitrak::Parse
    my $object = $self->find( $company.'/pparse-'.$company.'-response-'.$response_id.'.log' );
    unless( $object ){
        return undef;
    }
    return $self->authenticated_uri( $object->key() ); # Note this defaults to 5 minutes.
}

__PACKAGE__->meta->make_immutable( inline_constructor => 0 );

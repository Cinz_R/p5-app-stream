package Stream2::Factory::Email;

use Moose;
extends qw/Stream2::Factory/;

use Log::Any qw/$log/;
use Class::Load;
use Email::Abstract;
use Email::Address;
use Email::Sender::Simple;
use MIME::Entity;
use MIME::Parser;
use Scalar::Util qw();
use Stream::EngineException;
use Stream2::O::Email;

=head1 NAME

Stream2::Factory::Email - A factory for emails.

This Factory creates Stream2::O::Email objects.

=cut

my $singleton;

# Cheap singleton implementation
around 'new' => sub {
    my ( $orig, $class ) = ( shift, shift );
    return $singleton //= $class->$orig(@_);
};


# General email transport.
#
has 'email_transport' => (
    is         => 'ro',
    'does'     => 'Email::Sender::Transport',
    lazy_build => 1
);

# An email transport with a static IP
# in production.
has 'static_email_transport' => (
    is => 'ro',
    does => 'Email::Sender::Transport',
    lazy_build => 1
);

has 'mime_parser' => (
    is         => 'ro',
    isa        => 'MIME::Parser',
    lazy_build => 1
);

sub _build_email_transport {
    my ($self, $transport_name) = @_;

    $transport_name //= 'transport';

    my $conf = $self->stream2->config()->{email};
    my $transport_conf = $conf->{$transport_name};
    unless ( $transport_conf ) {
        confess( "No transport configuration named '".$transport_name."' defined in email configuration in "
              . $self->stream2->config_place() );
    }

    my $transport_class =
      'Email::Sender::Transport::' . $transport_conf->{class};
    my $transport_params = $transport_conf->{params};

    Class::Load::load_class($transport_class);
    return $transport_class->new($transport_params);
}

sub _build_static_email_transport{
    my ($self) = @_;
    return $self->_build_email_transport( 'static_transport' );
}

sub _build_mime_parser {
    my ($self) = @_;
    my $parser = MIME::Parser->new();

    # Do everything in memory (aka core in MIME::Parser parlance)
    $parser->output_to_core(1);
    $parser->tmp_to_core(1);
    return $parser;
}

=head2 create

Creates a Stream2::O::Email from with the provided mime entity email,
if one is note provided it generates a blank Mime::Entity.

    $self->stream2->factory('Email')->create($email)

=cut

sub create {
    my ( $self, $email ) = @_;

    unless ($email) {
        return Stream2::O::Email->new(
            factory => $self,
            entity  => MIME::Entity->new()
        );
    }

    # if the email that is passed in is a Stream2::O::Email, there is no
    # need to recreate it, just return it.
    return $email
      if Scalar::Util::blessed($email) && $email->isa('Stream2::O::Email');

    # we break out the mime entity as casting a to a Mime::Entity from a
    # Mime::Entity via Email::Abstract removed some headers
    if ( $email->isa('MIME::Entity') ) {
        return Stream2::O::Email->new(
            factory => $self,
            entity  => $email
        );
    }
    else {
        # this will confess if the email is not something it can handle
        return Email::Abstract->new($email)->cast('MIME::Entity')

    }
}

=head2 build

Creates a Stream2::O::Email with a mime entity with the given params.

    $stream2->factory('Email')->build(
        [
            From     => 'noreply@broadbean.net',
            To       => 'An.Email@emailme.com',
            Type     => "text/plain; charset=UTF-8",
            Encoding => "quoted-printable",
            Subject  => Encode::encode( 'MIME-Q',  'a subject line' ),
            Data     => [ Encode::encode( 'UTF-8', 'some data here' ) ]
        ]
    )

=cut

sub build {
    my ( $self, $email_parts ) = @_;
    return $self->create( MIME::Entity->build(@$email_parts) );

}

=head2 parse

Creates a Stream2::O::Email from a stringified Mime::Entity.


$self->stream2->factory('Email')->parse($stringified_email)

=cut

sub parse {
    my ( $self, $email ) = @_;
    return $self->create( $self->mime_parser->parse_data($email) );
}



=head2 send

Send any email compatible with Email::Abstract using
the configured transport.

You can specify using the static IP transport by adding the following header
to your email:

  'X-Stream2-Transport' => 'static_transport'


This will return a true value (the sent email) on success and die on failure.


Usage:

  $this->send

=cut

sub send {
    my ( $self, $email ) = @_;

    my $stream2 = $self->stream2;

    # unwrap the Mime::Entity from Stream2::O::Email.
    if ( Scalar::Util::blessed($email)
        && $email->isa('Stream2::O::Email') )
    {
        $email = $email->entity;
    }

    # Wrap the email in an abstract email
    $email = Email::Abstract->new($email);

    # check the email address is valid
    if ( !$self->email_valid($email) ) {
        die Stream::EngineException::MessagingError->new(
            {
                message => $stream2->__('The email address is not valid')
            }
        );
    }

    # check global blacklist
    if ( my $blacklist = $self->email_blacklisted($email) ) {
        my $message;
        my $purpose = $blacklist->purpose() || '';
        my $email_address = $blacklist->email();

        # global blacklist reasons
        if ( $purpose eq 'delivery.bounce' ) {
            $message = $stream2->__x(
'Emails to this recipient cannot be delivered. The email address ({email_address}) has been marked as either invalid or unknown.',
                email_address => $email_address
            );
        }
        elsif ( $purpose eq 'delivery.softbounceblock' ) {
            $message = $stream2->__x(
'Emails to this recipient ({email_address}) have been blocked, as several previous email attempts bounced.',
                email_address => $email_address
            );
        }
        elsif ( $purpose =~ m/\Adelivery\.(?:spam|blocked)\z/ ) {
            $message = $stream2->__x(
'The recipient ({email_address}) has indicated that they no longer wish to receive these emails.',
                email_address => $email_address
            );
        }
        die Stream::EngineException::MessagingError->new(
            {
                message => $message || $stream2->__x(
                    'The email address, {email_address} is blacklisted',
                    email_address => $email_address
                )
            }
        );
    }

    my @bcc_strings = $email->get_header('Bcc');

    # Make the Bcc really blind.
    my @empty = ();
    $email->set_header( 'Bcc' => @empty );

    unless ( $email->get_header('From') ) {
        $email->set_header( 'From', 'noreply@broadbean.net' );
    }

    my $transport_accessor = 'email_transport';
    if( ( $email->get_header('X-Stream2-Transport') || '' ) eq 'static_transport' ){
        $log->info("Sending email using static_transport");
        $transport_accessor = 'static_email_transport';
    }

    # generate a unique identifier for this message
    my $uuid       = $stream2->uuid->create_str;
    my $message_id = sprintf('<%s@stream.broadbean.net>', $uuid);
    $email->set_header( 'Message-ID'    => $message_id );
    $email->set_header( 'X-MJ-CustomID' => $uuid );

    # Only noreply@broadbean.net is a verified from.
    Email::Sender::Simple->send(
        $email,
        {
            transport => $self->$transport_accessor(),
            from      => 'noreply@broadbean.net',
        }
    );

    # Then do the Bccs !

    my @bcc_recipients = ();

    # Parse all the bcc strings for recipients.
    foreach my $bcc_string (@bcc_strings) {
        my @bcc_recpt = Email::Address->parse($bcc_string);
        push @bcc_recipients, @bcc_recpt;
    }

    foreach my $bcc_recipient (@bcc_recipients) {
        $log->info( "BCCing email to " . $bcc_recipient->address() );
        $email->set_header( 'Bcc' => $bcc_recipient->original() );
        my $uuid       = $stream2->uuid->create_str;
        my $message_id = sprintf('<%s@broadbean.net>', $uuid);
        $email->set_header( 'Message-ID'    => $message_id );
        $email->set_header( 'X-MJ-CustomID' => $uuid );

        # Only noreply@broadbean.net is a verified from.
        Email::Sender::Simple->send(
            $email,
            {
                transport => $self->$transport_accessor(),
                to        => $bcc_recipient->address(),
                from      => 'noreply@broadbean.net'
            }
        );
    }

    # restore headers to their state before BCC processing
    $email->set_header( 'Bcc', @bcc_strings );
    $email->set_header( 'Message-ID', $message_id );
    $email->set_header( 'X-MJ-CustomID', $uuid );
    return $email;
}

=head2 email_valid

Check such things as the recipient email address(es) being valid

Usage:

    if ( ! $s2->email_valid( $email ) ){
        die "email is invalid";
    }

=cut

sub email_valid {
    my ( $self, $email ) = @_;
    $email // confess "Email address is required";
    my @addresses         = $self->_extract_email_addresses($email);
    my $blacklist_factory = $self->stream2->factory('EmailBlacklist');

    if ( grep { $blacklist_factory->invalid_email_domain($_) } @addresses ) {
        return 0;
    }

    return 1;
}

=head2 email_blacklisted

    given an email address, return true or false as to whether this email address
    is blacklisted, can also give an Email::Abstract object which will be checked for blacklisted
    emails all over it

    if ( $s2->email_blacklisted( $email ) ){
        die "This candidate does not wish to be contacted"
    }

=cut

sub email_blacklisted {
    my ( $self, $email ) = @_;
    $email // confess "Email address is required";
    my @addresses = $self->_extract_email_addresses($email);

    my $blacklisted_emails =
      $self->stream2->factory('EmailBlacklist')
      ->emails_blacklisted( \@addresses );

    # lets log these blacklisted emails;
    $log->infof( "Blacklisted Emails: %s",
        join ',', map { $_->email } $blacklisted_emails->all );
    return $blacklisted_emails->dbic_rs->first;
}

sub _extract_email_addresses {
    my ( $self, $email ) = @_;

    if ( $email->isa("Email::Abstract") ) {
        my @raw_addresses = ();
        foreach my $raw_email_line ( $email->get_header('To'),
            $email->get_header('Bcc') )
        {
            my @email_objs = Email::Address->parse( Encode::decode( 'MIME-Header', $raw_email_line ));
            push @raw_addresses, map { $_->address } @email_objs;
        }
        return @raw_addresses;
    }

    return ($email);
}

__PACKAGE__->meta->make_immutable( inline_constructor => 0 );
1;


package Stream2::Factory::S3FileBase;

use Moose;

# multiple inheritance: eek: subclassing Stream2::Factory just to get a
# reference to its bm member
extends qw/Bean::AWS::S3FileFactory Stream2::Factory/;

=head1 NAME

Stream2::Factory::S3FileBase - A Base class for all S3 based factories

=head1 DESCRIPTION

This class represents a factory for files stored in Amazon's S3. It's
a singleton (in the domain of the application it participates in) that
takes its configuration from the config key in aws given
by a call to aws_config_key.

=cut

sub aws_config_key{ confess("Please implement me"); }

__PACKAGE__->meta->make_immutable( inline_constructor => 0 );

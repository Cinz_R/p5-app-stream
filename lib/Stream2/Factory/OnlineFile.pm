package Stream2::Factory::OnlineFile;

use Moose;
extends qw/Stream2::Factory/;

use DateTime;
use Filesys::Df;
use File::Spec;
use CHI;
use Data::Dumper;
use Sys::Hostname;

use Stream2::O::OnlineFile;

use Log::Any qw/$log/;
use File::Path;

=head1 NAME

Stream2::Factory::OnlineFile - A factory for files who live online.

=cut

has 'chi' => ( is => 'ro' , isa => 'CHI::Driver' , lazy_build => 1);

has 'user_agent' => ( is => 'ro' , isa => 'LWP::UserAgent' , lazy_build => 1);

has 'cache_dir' => ( is => 'ro', isa => 'Str', lazy_build => 1);

# rw only for tests
has 'filesys_size_threshold' => ( is => 'rw' , isa => 'Int' , default => 500_000 );
has 'filesys_inode_threshold' => ( is => 'rw', isa => 'Int' , default => 2000 );

my $singleton;

# Cheap singleton implementation
around 'new' => sub{
    my ($orig, $class) = ( shift, shift );
    return $singleton //= $class->$orig(@_);
};

sub _build_user_agent{
    my ($self) = @_;
    my $ua = LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 } ); # Lets be tolerant on lack of https
    $ua->timeout(10); # 10 seconds is enough.
    $ua->agent(__PACKAGE__.' http://www.broadbean.com/');
    return $ua;
}


sub _build_chi{
    my ($self) = @_;
    my $root_dir = File::Spec->catdir( File::Spec->tmpdir() , 'chi_fastmmap' ).'';
    $log->info("Online files cache will live at $root_dir");
    return CHI->new( driver => 'FastMmap',
                     root_dir =>  $root_dir ,
                     namespace => 'stream2_factory_onlinefile',
                     expires_variance => 0.10,
                     expires_in => '2 hours'
                   );
}

sub _build_cache_dir {
    my $cache_dir =  File::Spec->catdir( File::Spec->tmpdir(), 'stream2_onlinefiles' );
    # Vivify it.
    File::Path::make_path( $cache_dir );
    return $cache_dir;
}

=head2 recover_filespace

If there is less than 500Mb left on the filesystem designed to hold
the files, start panicing and clear everything, hoping this solved the issue.

Returns: nothing interesting.

=cut

sub recover_filespace{
    my ($self) = @_;

    my $df = Filesys::Df::df($self->cache_dir());
    unless( $df ){
        $log->critical("Cannot access file system size on ".$self->cache_dir());
        return;
    }
    if( $df->{bfree} < $self->filesys_size_threshold() ){ # Less than 500 M available
        my $hostname = Sys::Hostname::hostname();
        my $message = "Less than ".$self->filesys_size_threshold()." K are available on $hostname:".$self->cache_dir().". Will do some agressive clearing (meaning everything), hoping it solves the issue";
        $log->alert($message);
        $self->stream2()->report_tech( [] , sub{
                                           $self->clear_old_cache( { three_hours => -10 } ); # Do the time warp. Forces clear_old_cache to wipe everything.
                                           die $message;
                                       });
    }

    unless( exists ( $df->{files} ) ){
        $log->critical("Cannot access inode stats on ".$self->cache_dir());
        return;
    }
    if( $df->{ffree} < $self->filesys_inode_threshold() ){
        my $hostname = Sys::Hostname::hostname();
        my $message = "Less than ".$self->filesys_inode_threshold()." inodes are available on $hostname:".$self->cache_dir().". Will do some agressive clearing (meaning everything), hoping it solves the issue";
        $log->alert($message);
        $self->stream2()->report_tech( [] , sub{
                                           $self->clear_old_cache( { three_hours => -10 } ); # Do the time warp. Forces clear_old_cache to wipe everything.
                                           die $message;
                                       });
    }
    return;
}

=head2 clear_old_cache

We are only interesting in caching images for n hours. This should be
called regularly to save on disk space

Usage:

    $s2->factory('OnlineFile')->clear_old_cache();

=cut


sub clear_old_cache {
    my ( $self, $opts ) = @_;
    $opts //= {};

    my $filedir = $self->cache_dir();
    $log->info("Will delete old stuff in $filedir");
    my $three_hours_ago = time - ( $opts->{three_hours} // ( 60 * 60 * 3 ) );

    while ( my $dir = glob( sprintf( "%s/*", $filedir ) ) ){
        next if ( $dir =~ m/\A\./ );

        $log->debug("Looking at $dir");
        # check when the directory was last accessed, if it was in the last 3 hours then next
        my @dir_stat = stat( $dir );
        next if ( $dir_stat[9] > $three_hours_ago );

        $log->info("Removing directory $dir");
        File::Path::remove_tree( $dir, {error => \my $err} );

        if ( @$err ) {
            $log->errorf( 'Unable to clear online file cache %s -> %s', $dir, Dumper( $err ) );
        }
        else {
            $log->infof( 'Clearing online file cache %s', $dir );
        }
    }
}

=head2 find

Returns a L<Stream2::O::OnlineFile> by URL

Usage:

 my $o = $this->find('http://www.example.com/');

=cut

sub find{
    my ($self, $url) = @_;
    return Stream2::O::OnlineFile->new({ factory => $self , url => $url });
}

__PACKAGE__->meta->make_immutable(inline_constructor => 0 );

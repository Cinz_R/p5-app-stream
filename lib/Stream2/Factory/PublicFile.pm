package Stream2::Factory::PublicFile;

use Moose;

# multiple inheritance: eek: subclassing Stream2::Factory just to get a
# reference to its bm member
extends qw/Stream2::Factory::S3FileBase/;

=head1 NAME

Stream2::Factory::PublicFile - A Factory for public files stored in S3

=head1 DESCRIPTION

This class represents a factory for public files stored in Amazon's S3. It's
a singleton (in the domain of the application it participates in).

=cut

# we're a singleton
my $INSTANCE;
around new => sub {
    my ( $orig, $class, $attrs ) = @_;

    unless ( $INSTANCE ) {
        $attrs //= {};
        # config from conf file
        my $config = $attrs->{bm}->config->{aws};

        $attrs->{config} = {
            aws_access_key_id     => $config->{aws_access_key_id},
            aws_secret_access_key => $config->{aws_secret_access_key},
            %{ $config->{ $class->aws_config_key() } // confess("Missing ".$class->aws_config_key()." in ".$attrs->{bm}->config_place() ) }
        };

        $INSTANCE = $class->$orig( $attrs );
    }
    return $INSTANCE;
};


sub aws_config_key{ return 'public_file_s3' }

__PACKAGE__->meta->make_immutable( inline_constructor => 0 );

package Stream2::Periodic;

use Moose;

use EV;

use Log::Any qw/$log/;

has 'stream2' => ( is => 'ro', isa => 'Stream2' , required => 1 );

=head1 NAME

Stream2::Periodic - Runs Stream2 pediodic stuff (like firing the watchdogs for instance).

=head2 SYNOPSIS

 # In a separate process:
 $s2->clone->periodic->run();

 # Send the signal TERM to your process to let it finish correctly.

=head2 run

Blocking. Will run Stream2 periodic stuff until the finish_if becomes true

=cut

sub run{
    my ($self, $opts) = @_;
    my $exit_if = $opts->{finish_if} // confess("Missing finish_if code");

    # MOST . IMPORTANT . WATCHER. DO NOT REMOVE THAT (or kitten will die)
    my $heartbeat = EV::periodic 0, 1 , 0, sub{
        if( &$exit_if() ){
            $log->info("Exit condition is now true. Unlooping");
            EV::unloop();
        }
    };


    my $fire_watchdogs = EV::timer 0, 30, sub{
        $self->fire_watchdogs();
        $self->initiate_abandon_watchogs();
    };

    # Fire reports every 5 minutes.
    my $fire_reports = EV::timer 0, 300, sub{
        $self->fire_reports();
    };

    # Clear old login records every minute
    my $clear_old_login_records = EV::timer 0, 60, sub{
        $self->clear_old_login_records();
    };

    my $clear_watchdog_results = EV::timer 0, 30, sub{
        $self->clear_watchdog_results();
    };

    # We run the longsteps at random intervals
    # and in mutual exclusion. This is to avoid
    # dead locks (fatal embraces) that can happen when
    # a worker initialises a run (so updates the process table)
    # whilst another one updates it.
    my $longstep_interval = 30 + int(rand(60));
    my $run_schedule_long_step = EV::timer 0, $longstep_interval, sub{
        $log->info("Running longstep processes (every $longstep_interval seconds)");
        $self->run_longstep_processes();
    };

    my $clear_candidate_internal_mappings = EV::timer 0, 60, sub {
        $self->clear_candidate_internal_mappings();
    };

    # once per hour
    my $hourly = EV::timer 0, 3600, sub {
        $self->clear_old_inactive_watchdogs();
        $self->clear_online_files();
    };

    # once per 4 hours
    my $four_hourly = EV::timer 0, 14400, sub {
        $self->delete_old_soft_bounce_notifications();
    };

    # every 10 minutes
    my $quota_refresh = EV::timer 0, 600, sub {
        $self->refresh_quotas();
    };

    EV::run;
}

sub run_longstep_processes{
    my ($self) = @_;
    my $stuff = sub{
        $self->stream2()->longsteps()->run_due_processes({ stream2 => $self->stream2 });
    };
    $self->stream2->monitor->try_mutex(__PACKAGE__.'::run_longstep_processes',
                                       $stuff,
                                       sub{ $log->warn("Another process is already running longstep processes"); }
                                   );
}

sub clear_old_login_records{
    my ($self) = @_;
    my $stuff = sub{
        $log->info("Clearing old login records");
        $self->stream2()->factory('LoginRecord')->clear_old_records();
    };
    $self->stream2->monitor->try_mutex(__PACKAGE__.'::clear_old_login_record',
                                       $stuff,
                                       sub{ $log->warn("Another process is already clearing old login records"); }
                                   );
}

sub clear_online_files{
    my ($self) = @_;
    my $stuff = sub{
        $log->info("Clearing old online cached files");
        $self->stream2->factory('OnlineFile')->clear_old_cache();
    };
    $self->stream2->monitor->try_mutex(__PACKAGE__.'::clear_online_files',
                                       $stuff,
                                       sub{ $log->warn("Another process is already clearing old online cache files"); }
                                      );

}

sub initiate_abandon_watchogs{
    my ($self) = @_;
    my $stuff = sub{
        $log->info("Initiating clearing abandonned watchdogs");
        $self->stream2()->watchdogs()->initiate_abandonned_destruction();
    };
    $self->stream2->monitor->try_mutex(__PACKAGE__.'::initiate_abandon_watchogs',
                                       $stuff,
                                       sub{ $log->warn("Another process is already initiating abandonned watchdog"); }
                                   );
}


sub clear_watchdog_results{
    my ($self) = @_;
    my $stuff = sub{
        $log->info("Clearing old watchdog results");
        $self->stream2->factory('WatchdogResult')->clean_old_results();
    };

    $self->stream2->monitor->try_mutex(__PACKAGE__.'::clear_watchdog_results',
                                       $stuff,
                                       sub{ $log->warn("Another process is already clearing old watchdog results"); }
                                      );

}

sub fire_reports{
    my ($self) = @_;
    my $stuff = sub{
        $log->trace("Firing due reports");
        $self->stream2()->factory('RecurringReport')->fire_due_reports();
    };
    $self->stream2->monitor->try_mutex(__PACKAGE__.'::fire_reports',
                                       $stuff,
                                       sub{ $log->warn("Another process is already firering reports"); }
                                   );
}

sub fire_watchdogs{
    my ($self) = @_;

    my $stuff = sub{
        $log->debug("Firing due watchdogs");
        $self->stream2->watchdogs->fire_due_watchdogs();
    };

    $self->stream2->monitor->try_mutex(__PACKAGE__.'::fire_watchdogs',
                                       $stuff,
                                       sub{ $log->warn("Another process is already firering watchdogs"); }
                                      );
}

sub clear_candidate_internal_mappings {
    my ( $self ) = @_;
    my $stuff = sub {
        $log->info("Clearing old candidate internal mappings ( > 6 months )");
        $self->stream2->factory('CandidateInternalMapping')->clean_old_shite();
    };
    $self->stream2->monitor->try_mutex(__PACKAGE__.'::clear_old_candidate_internal_mappings',
                                       $stuff,
                                       sub { $log->warn("Another process is already clearing internal mappings"); }
                                      );
}

sub clear_old_inactive_watchdogs {
    my ( $self ) = @_;
    my $stuff = sub {
        $log->info('Clearing old inactive watchdogs ( > 3 months )');
        $self->stream2->watchdogs->clear_old_inactve();
    };

    $self->stream2->monitor->try_mutex(__PACKAGE__.'::clear_old_inactive_watchdogs',
                                       $stuff,
                                       sub{ $log->warn("Another process is already clearing old inactive watchdogs"); }
                                      );
}

sub delete_old_soft_bounce_notifications {
    my $self = shift;
    my $stuff = sub {
        $log->info("Deleting old soft bounce notifications");
        $self->stream2->factory('EmailNotification')->delete_old_soft_bounces;
    };
    $self->stream2->monitor->try_mutex( __PACKAGE__ . '::delete_old_soft_bounces',
                                        $stuff,
                                        sub { $log->warn("Another process is already deleting old soft bounces"); }
                                      );
}

sub refresh_quotas {
    my ($self) = @_;
    # Bean::Quota has its own mutex lock implementation
    $log->info('Refreshing quota');
    $self->stream2->quota_object->refresh_quota('automated');
}

__PACKAGE__->meta->make_immutable();

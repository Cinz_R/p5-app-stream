package Stream2::Role::Stringable;
{
  $Stream2::Role::Stringable::VERSION = '0.001';
}

use Log::Any qw/$log/;

# ABSTRACT: serialise and deserialise your object to JSON

=head1 NAME

Stream2::Role::Stringable - Sugar around JSON::from_json and JSON::to_json and automatic dump (To hash) method.

=cut

use Moose::Role;
use JSON;
use Stream2::Trait::Stringable;

sub new_from_string {
  my $class = shift;
  my $string = shift;

  return $class->new( $class->deserialise($string) );
}

=head2 dump

Dumps this object to a hash recursively taking attributes with trait 'Stream2::Trait::Stringable' into account.

Usage:

  my $hash = $this->dump();

=cut

sub dump {
  my $self = shift;

  my $meta = $self->meta;

  my %dump;
  ATTR:
  foreach my $attribute ( $meta->get_all_attributes ) {
    my $stringable;
    my $stringify;
    if ( $attribute->does('Stream2::Trait::Stringable') ) {
      if ( $attribute->has_stringable ) {
        $stringable = $attribute->stringable;
      }
      if ( $attribute->has_stringify ) {
        $stringify = $attribute->stringify;
      }
    }

    if ( !defined($stringable) ) {
      if ( $stringify ) {
        # attributes that can be stringified are automatically stringable
        $stringable = 1;
      }
      elsif ( $attribute->name =~ m/^_/ ) {
        # skip private methods unless they are explicitly stringable
        $stringable = 0;
      }
      else {
        # unless specified, assume it is stringable
        $stringable = 1;
      }
    }

    next ATTR if ( !$stringable );

    $stringify ||= $attribute->get_read_method;
    $dump{$attribute->name} = $self->$stringify;
  }

  return \%dump;
}

sub serialise {
  my $self = shift;
  return JSON::to_json($self->dump(), { ascii => 1 });
}

sub deserialise {
  my $self = shift;
  my $data_str = shift;
  return JSON::from_json($data_str);
}

1;

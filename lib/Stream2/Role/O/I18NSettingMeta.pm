package Stream2::Role::O::I18NSettingMeta;

use Moose::Role;

requires 'stream2';
requires 'setting_meta_data';

# if the setting is a drop down list, we need to translate the labels
# this is more complicated if the labels contain dynamic segments or are numeric dependent
around "setting_meta_data" => sub {
    my ( $orig, $self, @args ) = @_;
    my $meta_ref = $self->$orig( @args );

    if ( my $options = $meta_ref->{options} ){
        $meta_ref->{options} = [ map {
            my $label = $_->[0];
            if ( ref( $label ) ){
                # label can be scalar or can be in the form { "nx" => [ '{n} cat', '{n} cats', 3, n => 3 ] }
                # or { "x" => [ '{x} amount', x => 3 ] }
                my ( $method, $args ) = each %$label;
                $method = "__" . $method;
                $label = $self->stream2->$method( @$args );
            }
            else {
                $label = $self->stream2->__( $label );
            }
            [ $label, $_->[1] ];
        } @$options ];
    }

    if ( my $information = $meta_ref->{information} ){
        $meta_ref->{information} = $self->stream2->__( $information );
    }

    return $meta_ref;
};


1;

package Stream2::Role::AutoId;
{
  $Stream2::Role::AutoId::VERSION = '0.001';
}

# ABSTRACT: adds an auto generating id attribute to your class

=head1 NAME

Stream2::Role::AutoId - Gives an 'id' automatic property to any Moose class.

=head1 SYNOPSIS

  package My::Class;
  use Moose;
  with qw/Stream2::Role::AutoId/;
  1;

  package main;
  my $o = My::Class->new();
  $o->id(); # Is something!

  # Force ID if you know it already:
  my $o = My::Class->new({ id => $some_id });


=cut

use Moose::Role;
use Data::UUID;

has 'id' => (
  is      => 'ro',
  isa     => 'Str',
  lazy    => 1,
  default => sub { return Data::UUID->new()->create_str(); },
);

1;

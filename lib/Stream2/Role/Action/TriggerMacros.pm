package Stream2::Role::Action::TriggerMacros;

use Moose::Role;

use Log::Any qw/$log/;

use Stream2::O::MacroContext;

requires 'build_context';
requires 'user_object';

=head1 NAME

Stream2::Role::Action::TriggerMacros - Consume that from action that you want to be able to trigger macros from.

=head1 NOTE

Consume this role before the Stream2::Role::Action::FastJob role to keep fastjob working.

=cut

around 'instance_perform'  => sub{
    my ($orig, $self, $job, @rest ) = @_;

    # Do NOT give the qurious job as this would cause early termination
    # of it.
    my $result = $self->$orig( undef , @rest );

    if( ( ( ref( $result ) // '' ) eq 'HASH' ) && $result->{error} ){
      $log->warn("An error has occured in the action. Not executing macros");
      return $result;
    }

    if( my $macro_results = $self->_trigger_macros() ){
        # Some actions (like download CV do not return a HashRef,
        # but a big blob. In this case, we dont want to enrich
        # the results.
        if( ( ref( $result )  // '' ) eq 'HASH' ){
            # Merge macro results into the original result.
            $result->{message} .= ' - '.$macro_results->{message};
            $result->{warnings} ||= [];
            push @{ $result->{warnings} } , @{$macro_results->{warnings}};
        }
    }

    # Keep consistency by returning $original_result anyway.
    return $result;
};

sub _trigger_macros{
    my ($self) = @_;

    my $user = $self->user_object();
    $log->debug( "User object = $user" );

    # There is a current user. Find the macros
    my $macros = $self->stream2()->factory('GroupMacro')
        ->search({ group_identity => $user->groupclient()->identity(),
                   on_action => $self->macro_action_name()
               },{ order_by => 'name' }
           );

    my @macros_names = ();
    my @warnings = ();

    # This will be built only once if there are no macros to execute.
    my $context = undef;

    while( my $macro = $macros->next() ){

        # Restrict macros with associated email_template to respect the template
        # access settings.
        if( my $email_template = $macro->email_template() ){
            $log->debugf("Macro %s has got an email template associated with it.", $macro->name() );
            unless( $user->settings_values_hash()->{groupsettings}->{ $email_template->setting_mnemonic() } ){
                $log->warnf('associated email template %s is not accessible for this user. Skipping', $email_template->id());
                next;
            }
        }

        if( $self->instance_memory_cache()->{macros}->{$macro->name()} ){
            $log->warn("Macro ".$macro->name()." has already been executed as part of this top action. Skipping");
            next;
        }
        # Build a macro context if not in memory already.
        # The functions using that are in Stream2::Lisp::Core
        $context //= Stream2::O::MacroContext->new({
            stream2 => $user->stream2(),
            user => $user,
            action_context =>  $self->build_context()
        });

        $self->instance_memory_cache()->{macros}->{$macro->name()} = 1;

        push @macros_names , $macro->name();
        $log->infof('Performing macro \'%s\' code=\'%s\'', $macro->name(), $macro->lisp_source());
        eval{
            $macro->execute_this( $context );
            $log->info("Macro ".$macro->name()." executed successfully");
        };
        if( my $err = $@ ){
            $log->warn("Error in macro ".$macro->id().": ".$err);
            push @warnings , 'Error in macro "'.$macro->name().'": '.$err;
        }
    }

    # Were any macros executed?
    unless( @macros_names ){
        $log->info("No macros were run");
        return
    };

    $log->info("Macros ".join(', ',  @macros_names)." were run");

    if( @warnings ){
        push @warnings,
            "Note that these macro warnings were run as part of your original action. Please also quote ".$self->jobid()." to support";
    }

    return {
        message => 'With macros '.join(', ' , map{ "\'$_\'"}  @macros_names ),
        warnings => \@warnings
    };
}


1;

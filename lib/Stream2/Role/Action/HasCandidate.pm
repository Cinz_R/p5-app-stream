package Stream2::Role::Action::HasCandidate;

use Moose::Role;
use Bean::Locale;
use HTML::FormatText;
use File::Temp;
use File::Slurp;
use Encode;
use Carp;

use Log::Any qw/$log/;

use Stream2::Results::Result;

=head1 NAME

Stream2::Role::Action::HasCandidate - A has a candidate

=cut

requires 'stream2';
requires 'stream2_interactive';

with qw/
    Stream2::Role::Action::HasUser
    Stream2::Role::Action::HasAdvert
/;

# Candidate Part

# When the results are interactive
has 'results_id' => ( is => 'ro', isa => 'Str', required => 0, predicate => 'has_results_id');
has 'candidate_idx' => ( is => 'ro', isa => 'Str', required => 0, predicate => 'has_candidate_idx');

# When the results are frozen somehow.
has 'candidate_id' => ( is => 'ro', isa => 'Str', required => 0, predicate => 'has_candidate_id' );
has 'candidate_content' => ( is => 'ro', isa => 'HashRef', required => 0, predicate => 'has_candidate_content' );

has 'candidate_object' => ( is => 'ro', isa => 'Stream2::Results::Result', lazy_build => 1);

around 'build_context' => sub{
    my ($orig, $self) = @_;

    return { %{ $self->$orig() },
             candidate_object => $self->candidate_object(),
             # In the context, we inject the plain perl candidate
             # properties in case we need to freeze this context.
             candidate_id => $self->candidate_object()->id(),
             candidate_content => $self->candidate_object()->to_ref()
         };
};

around 'instance_perform' => sub{
    my ($orig, $self, @rest ) = @_;
    my $res = $self->$orig(@rest);
    unless( $self->stream2_interactive() ){
        # Not interactive, no point enriching the result
        # with some candidate data.
        return $res;
    }
    unless( ref($res) && ref($res) eq 'HASH' ){
        # The res is not a hash, but something more exotic.
        return $res;
    }
    my $user = $self->user_object();
    my $candidate = $self->candidate_object();

    $log->info("Enriching result with candidate object ref for reflection after action");
    my $candidate_action_ref = $self->stream2()->action_log()->get_all_actions(
        $user->id(),
        $user->group_identity(),
        [ $candidate->id() ]
    );
    $res->{echo_candidate} = $candidate->to_ref({
        attributes => ['email'],
        candidate_action_ref => $candidate_action_ref->{$candidate->id()}
    });
    return $res;
};

=head2 candidate_relative_url

Returns the relative URL to this candidates API root.

Note that you cannot use that 'as is'. Usually use in combination with
a base URL and further refinements. For instance:

  my $cv_url = $base_url.$this->candidate_relative_url().'/cv';

=cut

sub candidate_relative_url{
    my ($self) = @_;
    return 'results/'.$self->results_id().'/'.$self->candidate_object()->destination().'/candidate/'.$self->candidate_idx();
}

sub _build_candidate_object{
    my ($self) = @_;
    if( $self->has_results_id() && $self->has_candidate_idx() ){
        return $self->stream2()->find_results($self->results_id())->find($self->candidate_idx());
    }
    if( $self->has_candidate_id() && $self->has_candidate_content() ){
        my $candidate = Stream2::Results::Result->new({ candidate_id => $self->candidate_id() });
        $candidate->attributes( %{$self->candidate_content()} );
        return $candidate;
    }
    confess("Missing results_id and candidate_idx OR candidate_id and candidate_content in ".$self);
}

# End of candidate part

#
# Generates an email (Stream2::O::Email)  containing a the stashed candidate and its
# cv (if given) to send to someone.
# Note that this does not set any Subject, To , CC or any other headers, except From
# which should always be noreply@broadbean.net
#
# Options can include:
#
#  - subject_template: A template for subject generating.
#
#  - cv: A Hashref like { content_type => '.. mimetype ..' , content_filename => 'the/cv/filename' , content => 'binary cv content' }
#
#  - advert: A hashref of an AdCourier advert, used to put subject variables on the stash to evaluate templates
#
sub generate_candidate_email{
    my ($self, $options ) = @_;

    $options //= {};

    my $user = $self->user_object();
    my $candidate = $self->candidate_object();

    my $subscription_ref = $self->user_object->has_subscription( $candidate->destination() ) || {};
    my $board_nice_name = $subscription_ref->{nice_name} // 'undefined board name';

    my $candidate_html = $self->stream2->templates->candidate_email(
        {
            L => sub{ Bean::Locale::localise_in( $user->locale(), @_ ); },
            candidate => $candidate->to_ref(),
            board_nice_name => $board_nice_name,
        }
    );

    my $email = $self->stream2->factory('Email')
        ->build( [ Type => 'multipart/mixed',
                   'X-Stream2-Transport' => 'static_transport' , # We want this to go through the static IP transport
               ] );

    # The container for alternative text or html
    my $container = $email->attach( Type => 'multipart/alternative' );

    # The text alternative
    $container->attach(Type => 'text/plain; charset=UTF-8',
                       Encoding => 'quoted-printable',
                       Disposition => 'inline',
                       Data => [ HTML::FormatText->format_string($candidate_html) ]);

    # The HTML alternative
    $container->attach(Type => 'text/html; charset=UTF-8',
                       Encoding => 'quoted-printable',
                       Data => [ $candidate_html ]);

    if( my $cv = $options->{cv} ){
        my $content_type = $cv->{content_type} // confess("Missing content_type");
        my $content_filename = $cv->{content_filename} // confess("Missing content_filename");
        my $content = $cv->{content} // confess("Missing content");

        my $temp_path = do{
            my ($fh,$temp_path) = File::Temp::tempfile();
            $log->debug("Writing CV content to $temp_path");
            File::Slurp::write_file($temp_path, { binmode => ':raw' }, $content);
            $temp_path;
        };

        # Attach the file to the main mixed email
        $email->attach(Type => $content_type,
                       Encoding => 'base64',
                       Path => $temp_path,
                       Disposition => 'attachment',
                       Filename => Encode::encode('MIME-Q', $content_filename)
                      );
    } # End of CV Attaching

    if( my $subject_template = $options->{subject_template} ){
        my $stash = $self->get_template_stash($options->{advert});
        my $subject = $self->stream2()->templates()->render_simple( $subject_template , $stash );
        $log->info("Email Subject=$subject");
        $email->head()->replace('Subject' , Encode::encode('MIME-Q', $subject));
    }

    return $email;
}

=head2 get_template_stash

Creates a HashRef with keys named after the subject variables found at:
L<https://juice.adcourier.com/subject_variables.cgi>.

Can supply an AdCourier advert to add additional subject variables to
the stash.

=cut

sub get_template_stash {
    my ($self, $advert) = @_;

    my $user = $self->user_object();
    my $candidate = $self->candidate_object();

    my $user_details = {};
    if( $user->is_adcourier() && ( my $contact_details = $user->contact_details ) ){
        $user_details = {
            map {
                $_ => $contact_details->{$_} // ''
            } qw/consultant office team company feedback_email/
        };
    }

    my $subscription_ref = $user->has_subscription( $candidate->destination() ) || {};

    my $board_id = $subscription_ref->{board_id};
    my $board_nice_name = $subscription_ref->{nice_name};

    # an adcresponses concession - if the candidate is on adcresponses, always use the board to which
    # they originally applied as the board_id
    if ( $candidate->destination eq 'adcresponses' ) {
        $board_id        = $candidate->attr('broadbean_adcboard_id');
        $board_nice_name = $candidate->attr('broadbean_adcboard_nicename');
        unless ( $advert ) {
            $advert = $self->get_adcresponse_advert($candidate->attr('broadbean_adcadvert_id'));
        }
    }

    my %stash = (
        contactname             => $user->contact_name(),
        consultant_email        => $user->contact_email(),

        feedbackemail           => $user_details->{feedback_email},
        consultant              => $user_details->{consultant},
        team                    => $user_details->{team},
        office                  => $user_details->{office},
        company                 => $user_details->{company},

        candidate_name          => $candidate->name() // '',
        from_email              => $candidate->email(),
        template_name           => $candidate->destination(),

        boardid                 => $board_id,
        eza_number              => $subscription_ref->{eza_number},
        custom_source_code      => $subscription_ref->{custom_source_code},
        upper_nice_board_name   => uc($board_nice_name // ''),
        board_nice_name         => $board_nice_name,
        nice_board_name         => $board_nice_name,

        method_id               => 3,
        method_name             => 'Search',
    );

    if( $advert ){
        %stash = (
            %stash,
            aplitrakid      => $advert->{AplitrakID},
            jobref          => $advert->{JobReference},
            jobtitle        => $advert->{JobTitle},
            jobtype         => $advert->{JobType},
            job             => $advert->{JobType},
            salary_from     => $advert->{SalaryFrom},
            salary_to       => $advert->{SalaryTo},
            currency        => $advert->{SalaryCurrency},
            salary_per      => $advert->{SalaryPer},
            salary_benefits => $advert->{SalaryBenefits},
            location_id     => $advert->{LocationID},
            skills          => $advert->{Skills},
            industry        => $advert->{Industry},
            startdate       => $advert->{StartDate},
            duration        => $advert->{Duration},
            responses       => $advert->{Responses},
            posted_times    => $advert->{CreatedTime},
            advert_title    => $advert->{JobTitle},
        );


        # AplitrakID capable adverts should spawn an aplitrak email
        if ( $advert->{AplitrakID} ) {
            $stash{advert_email} = $self->stream2()->gen_aplitrak_email(
                $advert->{Consultant},
                $advert->{AplitrakID},
                $board_id,
                $user->group_identity()
            );
            $log->info("Advert aplitrak email='".$stash{advert_email}."'");
        }

        ## Take the overriden board_id into account also for the preview URL.
        $stash{advert_link} = $self->_advert_preview_url($advert, { %{ $subscription_ref } , board_id => $board_id  });
    }

    my $original_source = $candidate->get_attr('original_channel_id') || $candidate->get_attr('broadbean_initial_source') || '';
    my $latest_source   = $candidate->get_attr('channel_id') || $candidate->get_attr('broadbean_final_source') || '';

    $stash{original_source} = $original_source;
    $stash{latest_source}   = $latest_source;

    # Inject Iprofile specific stuff. Yes this is horrible.
    if( $candidate->destination() eq 'iprofile' ){
        $stash{iprofile_jobtype}  = $candidate->attr('employment_type');
        $stash{iprofile_jobref}   = 'unknown'; # See site/Stream/Exchange/Email.pm line 207
        $stash{iprofile_jobtitle} = $candidate->attr('job_title');
    }

    return \%stash;
}

sub _advert_preview_url {
    my ($self, $advert, $subscription_ref) = @_;

    my $user = $self->user_object();
    my $candidate = $self->candidate_object();

    my $boards = $self->stream2->factory('Board');
    my $origin_bid;
    my $latest_bid;
    {
        if( my $origin_channel_id = $candidate->get_attr('original_channel_id')  ){
            $origin_bid = $boards->name_to_id( $origin_channel_id );
            $log->info("origin_bid = ".$origin_bid);
        }
        if( my $latest_channel_id = $candidate->get_attr('channel_id') ){
            $latest_bid = $boards->name_to_id( $latest_channel_id );
            $log->info("latest_bid = ".$latest_bid);
        }
    }

    # Login provider is cool here, cause getting the preview URL of an advert
    # depends on Ripple settings (and generally the properties of the advert)
    my $url = $user->login_provider()->advert_preview_url($advert, $self->original_user() || $user,
                                                            { board_id => $subscription_ref->{board_id},
                                                            origin_bid => $origin_bid,
                                                            latest_bid => $latest_bid,
                                                            method => 'search'
                                                        });

    return '' unless $url;
    return sprintf('<a href="%s">%s</a>', $url, $url);
}

1;

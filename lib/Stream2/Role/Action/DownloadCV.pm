package Stream2::Role::Action::DownloadCV;

use Moose::Role;

requires 'stream2';
with qw/Stream2::Role::Action::HasUser
        Stream2::Role::Action::HasCandidate
        Stream2::Role::Action::HasRemoteIP
        Stream2::Role::Action::InLogChunk
       /;

use Data::Dumper;

use Stream::EngineException::CVUnavailable;

use Stream2::Action::DownloadCV;
use Stream2::O::File;

use Log::Any qw/$log/;

=head2 download_cv_file

Download a L<Stream2::O::File> from this object user, candidate, remote Ip, reporting in this log_chunk_id

Side effect:

Will set the candidate email if the Response from the download adds a X-Candidate-Email header.

Usage:

   my $file = $this->download_cv_file();
   my $file = $this->download_cv_file($candidate_object); # pass candidate when performing bulk actions

=cut

sub download_cv_file{
    my ($self, $candidate_object) = @_;
    my $cv_action = Stream2::Action::DownloadCV->new({ stream2 => $self->stream2(),
                                                       user_object => $self->user_object(),
                                                       ripple_settings => $self->ripple_settings(), # From the HasUser role.
                                                       candidate_object => $candidate_object // $self->candidate_object(),
                                                       remote_ip => $self->remote_ip(),
                                                       log_chunk_id => $self->log_chunk_id(),
                                                       log_chunk_capture => 0,
                                                       instance_memory_cache => $self->instance_memory_cache(),
                                                       process_response => sub{ shift; },
                                                       ( $self->user() ? ( user => $self->user() ) : () )
                                                   });
    my $cv_response = $cv_action->instance_perform();
    if ( $cv_response->{error} ) {
        $log->warn('Download CV action failed: ' . Dumper($cv_response));
        die Stream::EngineException::CVUnavailable->new({ message => $cv_response->{error}->{message} });
    }
    return Stream2::O::File->new({ name => $cv_response->filename(),
                                   binary_content => $cv_response->content(),
                                   mime_type => $cv_response->content_type() || 'text/plain'
                               });
}



1;

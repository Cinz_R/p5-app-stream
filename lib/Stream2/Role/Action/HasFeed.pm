package Stream2::Role::Action::HasFeed;

use Moose::Role;

use Log::Any qw/$log/;

=head1 NAME

Stream2::Role::Action::HasFeed - A has a feed. Requires user

=head1 NOTES

Builds $self->feed() with all authentication tokens in place.

=cut

with qw/Stream2::Role::Action::HasUser/;

requires 'template_name';

# Feed part
has 'feed' => ( is => 'ro', isa => 'Stream::Engine', lazy_build => 1 );
has 'feed_subscription_info' => ( is => 'ro', isa => 'HashRef', lazy_build => 1);

around 'build_context' => sub{
    my ($orig, $self) = @_;

    return { %{ $self->$orig() },
             feed => $self->feed(),
             feed_subscription_info => $self->feed_subscription_info()
         };
};


sub _build_feed_subscription_info{
    my ($self) = @_;
    $log->info("Getting subscription Info for ".$self->template_name());
    if( my $subscription = $self->user_object()->has_subscription($self->template_name())){
        return $subscription;
    }
    return {};
}

sub _build_feed{
    my ($self) = @_;
    my $template = $self->template_name();
    $log->info("Building template '$template'");
    my $feed = $self->stream2()->build_template(
        $template,
        undef,
        {
            user_object   => $self->user_object(),
            original_user => $self->original_user(),
            company       => $self->user_object()->group_identity(),
            stream_action        => $self,
        }
    );
    $feed->company( $self->user_object()->group_identity() );
    if( $self->cb_oneiam_auth_code() ) {
        $feed->token_value( cb_user_code => $self->cb_oneiam_auth_code() );
    }
    if( $self->browser_tag() ) {
        $feed->token_value( browser_tag => $self->browser_tag() );
    }
    # There are some tokens for this feed originaly coming from the user.
    if( $self->feed_subscription_info()->{auth_tokens} ){
        $self->user_object()->inject_authtokens( $feed );
    }
    return $feed;
}

1;

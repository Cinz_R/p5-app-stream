package Stream2::Role::Action::HasUser;

use Moose::Role;

=head1 NAME

Stream2::Role::Action::HasUser - A has a user

=cut

requires 'stream2';

use Log::Any qw/$log/;

use Data::Dumper;

# Begin raw data structures.

# Sometimes we dont need ripple settings.
has 'ripple_settings' => ( is => 'ro', isa => 'HashRef', required => 1 );

# User part. Note that for an action, its a hashref of { user_id => 123 }
# Until refactoring action spawning code.
has 'user' => ( is => 'ro', isa => 'HashRef', required => 0 );
# Original user ID. Sometimes useful (like to get an advert for instance)
has 'original_user_id' => ( is => 'ro' , isa => 'Maybe[Int]' );

has 'cb_oneiam_auth_code' => ( is => 'ro', isa => 'Maybe[Str]', lazy_build => 1);

has 'browser_tag' => ( is => 'ro', isa => 'Maybe[Str]', lazy_build => 1);


# Built data:
has 'user_object' => ( is => 'ro', isa => 'Stream2::O::SearchUser', lazy_build => 1);
has 'original_user' => ( is => 'ro', isa => 'Maybe[Stream2::O::SearchUser]', lazy_build => 1);

around 'build_context' => sub{
    my ($orig, $self) = @_;
    return { %{ $self->$orig() },
             ripple_settings => $self->ripple_settings(),
             user_object => $self->user_object(),
             original_user =>  $self->original_user(),
             browser_tag => $self->browser_tag,
             # Also inject original user hash in the context
             ( $self->user() ? ( user => $self->user() ) : () ),
             ( $self->original_user_id() ? ( original_user_id => $self->original_user_id() ) : () ),
         };
};


sub _build_cb_oneiam_auth_code{
    my ($self) = @_;
    return ( $self->user() // {} )->{cb_oneiam_auth_code};
}

sub _build_browser_tag {
    my ($self) = @_;
    return ( $self->user() // {} )->{browser_tag};
}

sub _build_original_user{
    my ($self) = @_;
    unless( $self->original_user_id() ){
        $log->info("No original user ID");
        return undef;
    }
    return $self->stream2()->find_user($self->original_user_id() , $self->ripple_settings() );
}

sub _build_user_object{
    my ($self) = @_;
    unless( $self->user() && $self->user()->{user_id} ){
        confess("Missing user->{user_id} hash in $self");
    }
    $log->info("This is user ID = ".$self->user()->{user_id} );
    $log->trace("This is user hash = ".Dumper( $self->user() ));
    $log->trace("Ripple settings: ".Dumper( $self->ripple_settings() ));
    return $self->stream2()->find_user($self->user()->{user_id} ,  $self->ripple_settings() );
}


1;

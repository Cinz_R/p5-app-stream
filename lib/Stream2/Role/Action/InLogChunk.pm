package Stream2::Role::Action::InLogChunk;

use Moose::Role;

=head1 NAME

Stream2::Role::Action::InLogChunk - Capture a chunk of log on an action

=cut

use Log::Any qw/$log/;

use Log::Log4perl::MDC;
use Sys::Hostname qw//;

requires 'jobid';

has 'log_chunk_id' => ( is => 'ro', isa => 'Str' , lazy_build => 1);
has 'log_chunk_capture' => ( is => 'ro', isa => 'Bool', default => 1);

sub _build_log_chunk_id{
    my ($self) = @_;
    unless( $self->jobid() ){
        confess("No jobid in $self. Please set log_chunk_id");
    }
    return $self->jobid();
}

around 'instance_perform' => sub {
    my ($orig, $self, @rest) = @_;

    unless( $self->log_chunk_capture() ){
        # Sometime we want to control the capturing from the
        # point of view of the object builder.
        return $self->$orig(@rest);
    }

    Log::Log4perl::MDC->put('chunk', $self->log_chunk_id() );

    $log->info("This is hostname = ".Sys::Hostname::hostname());

    my $res = eval{ $self->$orig(@rest); };
    if( my $err = $@ ){
        $log->warn("There was an error in the chunk: $err");
        Log::Log4perl::MDC->put('chunk', undef);
        $log->info("Chunk ".$self->log_chunk_id()." captured");
        # Rethrow as is
        die $err;
    }
    Log::Log4perl::MDC->put('chunk', undef);
    $log->info("Chunk ".$self->log_chunk_id()." captured");
    return $res;
};

1;

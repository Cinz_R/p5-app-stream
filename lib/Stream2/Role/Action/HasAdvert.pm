package Stream2::Role::Action::HasAdvert;

use Moose::Role;

=head1 NAME

Stream2::Role::Action::HasAdvert - A has an advert. Requires HasUser

=cut

requires 'stream2';
with qw/Stream2::Role::Action::HasUser/;

has 'advert_id' => ( is => 'ro', isa => 'Maybe[Str]', required => 0);
has 'list_name' => ( is => 'ro', isa => 'Maybe[Str]', required => 0);

around 'build_context' => sub{
    my ($orig, $self) = @_;

    return { %{ $self->$orig() },
             advert_id => $self->advert_id(),
             list_name => $self->list_name()
         };
};

=head2 get_advert

Returns an advert hash from this advert_id and list_name.
BTW, an advert is just a hash.

=cut

sub get_advert{
    my ($self) = @_;

    unless( $self->advert_id() && $self->list_name() ){
        confess("Missing advert_id or list_name");
    }

    my $advert_user = $self->original_user() || $self->user_object();

    # login_provider is good here, cause finding an advert (aka cheesesandwich)
    # does depends on login type ripple settings.
    my $advert =  $self->user_object()->login_provider
        ->find_advert( $advert_user,
                       $self->advert_id() ,  { list_name => $self->list_name() } );
    $advert->{advert_id} = $self->advert_id();
    $advert->{list_name} = $self->list_name();
    return $advert;
}

=head2 get_adcresponse_advert

Gets the advert associated to the AdCourier Response candidates advert id.

Builds the 'Adcourier' provider and retrieves the adverts details

=cut

sub get_adcresponse_advert {
    my ($self, $advert_id) = @_;
    unless ( $advert_id ) {
        confess('Missing advert_id');
    }

    my $adcourier= $self->stream2->build_provider('adcourier');
    return $adcourier->find_advert(
        $self->original_user() || $self->user_object(),
        $advert_id
    );
}

1;

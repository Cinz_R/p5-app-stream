package Stream2::Role::Action::Email;

use Moose::Role;
use Log::Any qw/$log/;

=head1 NAME

Stream2::Role::Action::Email - Utilities for actions that use emails

=head2 get_mailjet_data

Returns data to facilitate a call to the Mailjet API to get more information
about a particular message.

=cut

sub get_mailjet_data {
    my ($self, $sent_email) = @_;

    my $default_mailjet_account_name = 'transport';
    return $sent_email ?
    (
        mailjet_custom_id =>
            scalar( $sent_email->get_header('X-MJ-CustomID') ),
        mailjet_account   =>
            scalar( $sent_email->get_header('X-Stream2-Transport') )
                || $default_mailjet_account_name
    ) : ();
}

1;
package Stream2::Role::Action::HasRemoteIP;

use Moose::Role;

=head1 NAME

Stream2::Role::Action::HasRemoteIP - A has a remote IP

=cut

use Log::Any qw/$log/;

has 'remote_ip' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_remote_ip{
    my ($self) = @_;
    $log->warn("NO remote_ip given. Falling back to 127.0.0.1");
    return '127.0.0.1';
}

around 'build_context' => sub{
    my ($orig, $self) = @_;
    return { %{ $self->$orig() },
             remote_ip => $self->remote_ip()
         };
};


around 'instance_perform' => sub{
    my ($orig, $self, @rest) = @_;

    local $ENV{REMOTE_ADDR} = $self->remote_ip();
    $log->info("Remote IP is ".$self->remote_ip() );
    return $self->$orig(@rest);
};

1;

package Stream2::Role::Action::InManageError;

use Moose::Role;

use Scalar::Util;

=head1 NAME

Stream2::Role::Action::InManageError - Capture manage error and throw user friendly messages
Returning an action log row from your on_not_managed_error will provide CS with the action ID,
this allows them to track down the log more easily.

=head1 SYNOPSIS

    # in your action class

    has [ '+on_managed_error', '+on_not_managed_error' ] => ( default => sub {
        my $self = shift;
        return sub {
            return $self->stream2()->action_log->insert({
                action          => "forward_fail",
                candidate_id    => $self->candidate_object()->id(),
                destination     => $self->candidate_object()->destination(),
                user_id         => $self->user_object->id,
                group_identity  => $self->user_object->group_identity,
                data            => { s3_log_id => $self->log_chunk_id() },
            });
        }
    });

=cut

use Log::Any qw/$log/;

requires 'jobid'; # For reference
requires 'stream2'; # For translations

has 'on_managed_error' => ( is => 'ro', isa => 'CodeRef', default => sub{ return sub{}; } );
has 'on_not_managed_error' => ( is => 'ro', isa => 'CodeRef', default => sub{ return sub{}; } );

has 'humanize_error' => ( is => 'ro', isa => 'Bool', default => 1);

around 'instance_perform' => sub {
    my ($orig, $self, @rest) = @_;

    my $res = eval{ $self->$orig(@rest); };

    if( my $err = $@ ){
        if( Scalar::Util::blessed($err) && $err->isa('Stream::EngineException') ){
            # This is a managed error (setting or whatever), so from the
            # application's perspective, it's just a warning (or even lower).
            $log->warn("Caught $err: ".$err->message()."\n\n".$err->stack_trace());

            my $action_log = $self->on_managed_error()->( $err );
            # Default to jobid which should be the S3 log id
            my $action_id = ref( $action_log ) eq 'Stream2::Schema::Result::CandidateActionLog' ? $action_log->id : $self->jobid();

            my $properties = $err->error_properties;
            my ($type) = ref($err) =~ m/([^:]+\z)/;
            $properties->{type} = lc $type;

            return { error => { message => $err->human_message()."\n", properties => $properties, log_id => $action_id } };
        }

        $log->error("Unmanaged search engine error (in Job ".( $self->jobid() // 'UNDEF' )."): ".( $err // 'UNDEF' ));

        my $action_log = $self->on_not_managed_error()->( $err );
        my $action_id = ref( $action_log ) eq 'Stream2::Schema::Result::CandidateActionLog' ? $action_log->id : $self->jobid();

        unless( $self->humanize_error() ){
            return { error => { message => $err } };
        }

        unless( ( $ENV{MOJO_MODE} // '' ) eq 'development' ){
            return {
                error => {
                    message => $self->stream2()->__x("An un-managed error has occured. Please quote 'Job {uuid}' to support", uuid => $action_id) . "\n",
                    log_id  => $action_id,
                }
            };
        }else{
            return {
                error => {
                    message => "An un-managed error has occured. Here is some DEV ONLY stuff to think about: ".$err,
                    log_id  => $action_id,
                }
            };
        }
    }

    return $res;
};

1;

package Stream2::Role::Action::FastJob;

use Moose::Role;

use Log::Any qw/$log/;

=head1 NAME

Stream2::Role::Action::FastJob - Returns a successful job's result faster if possible.

=cut

around 'instance_perform'  => sub{
    my ($orig, $self, $job, @rest ) = @_;
    # Do NOT give the job to the original action.
    my $original_result = $self->$orig( undef, @rest );

    if( $job ){
        $log->info("Job object was given. Setting job status before all the other roles");
        if ( $job->should_fail($original_result) ) {
            $log->info('Failing job in fast role');
            $job->fail_job($original_result);
        }
        else {
            $log->info('Completing job in fast role');
            $job->complete_job( $original_result );
        }

    }else{
        $log->info("No job object was given. Carrying on");
    }
    # Keep consistency by returning $original_result anyway.
    return $original_result;
};

1;

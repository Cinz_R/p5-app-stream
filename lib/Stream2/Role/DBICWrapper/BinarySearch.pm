package Stream2::Role::DBICWrapper::BinarySearch;

use Moose::Role;

use Log::Any qw/$log/;

=head1 NAME

Stream2::Role::DBICWrapper::BinarySearch - A role to search rows in an ordered by ID table and other non indexed properties.

=cut


=head2 find_after_date

Binary search the object (by 'id') that is right after the given date (property insert_datetime). This is useful because
actions are not indexed by date and the table is 21 Million rows. What we know is
that their IDs are always increasing with time.

Usage:

 my $action = $this->find_after_date( a DateTime object );

If you give a date that's the future, you will get the last record.
If you give a date that is too much in the past, you will get the first record.

If no records at all are in the database, this returns undef.

=cut

sub find_after_date{
    my ($self, $date) = @_;
    $log->info("Looking up ".ref($self)." record right after ".$date);

    my $lower_limit = $self->search(undef, { order_by => 'me.id', rows => 1 })->first();
    my $upper_limit = $self->search(undef, { order_by => { -desc => 'me.id'} , rows => 1 } )->first();

    # Very rare case where no records are in the database.
    unless( $lower_limit && $upper_limit ){
        return undef;
    }

    my $previous_lower = $lower_limit;
    my $previous_upper = $upper_limit;
    while(1){
        $log->debug("Lower ".$lower_limit->id()." Upper ".$upper_limit->id());
        if( $lower_limit->id() eq $upper_limit->id() ){
            return $lower_limit;
        }
        my $middle_id = int( ( $lower_limit->id() + $upper_limit->id() ) / 2  );
        my $middle = $self->search({ 'me.id' => { '>=' =>  $middle_id } }, { order_by => 'me.id', rows => 1} )->first();
        $log->debug("Middle point ".$middle->id()." at date ".$middle->insert_datetime()." comparing to ".$date);
        if( $middle->insert_datetime() < $date ){
            $lower_limit = $middle;
        }else{
            $upper_limit = $middle;
        }
        # No move? Return upper (we want the one just above the date.
        if( ( $upper_limit->id() == $previous_upper->id() ) &&
                ( $lower_limit->id() == $previous_lower->id() ) ){
            $log->debug("Lower or upper limit have not moved. Returning upper limit at ".$upper_limit->insert_datetime() );
            return $upper_limit;
        }
        $previous_lower = $lower_limit;
        $previous_upper = $upper_limit;
    }
    return undef;
}


1;

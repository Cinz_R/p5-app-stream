package Stream2::Role::Freezable;

use Moose::Role;
use JSON;
use Compress::Zlib qw//;

=head1 NAME

Stream2::Role::Freezable - Consume that if you want an object that can output a hash to be freezable in different ways.

=head1 SYNOPSIS

  package My::Class;
  use Moose;
  with qw/Stream2::Role::Freezable/;
  sub to_hash{ return { pure => perl, hash => [] };
  1;

  package main;
  my $o = My::Class->new();
  my $b64_str = o->freeze_to_b64();


=cut

requires 'to_hash';
requires 'from_hash';

=head2

Turns this object into a compressed b64 json blob of text.

Usage:

 my $b64blob = $this->freeze_to_b64();

Note that if you happen to see such a string outside of perl, you can decode it in unixz using the
following command:

 base64 -D | gzunip | json_pp

=cut

sub freeze_to_b64{
    my ($self) = @_;
    # Get the json, compress and b64 encode as one long string.
    return MIME::Base64::encode_base64( Compress::Zlib::memGzip( JSON::to_json( $self->to_hash(), { ascii => 1 } ) ), '' );
}

=head2 new_from_frozen_b64

Builds a new instance of this using the frozen base64 (from freeze_to_b64)
and the rest of arguments (given to the from_hash method).

Note that it uses the 'from_hash' method of the consuming class.

=cut

sub new_from_frozen_b64{
    my ($class, $b64, @rest) = @_;
    return $class->from_hash( JSON::from_json( Compress::Zlib::memGunzip( MIME::Base64::decode_base64( $b64 ) ) ), @rest );
}

1;

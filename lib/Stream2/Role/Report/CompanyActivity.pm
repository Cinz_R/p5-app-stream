package Stream2::Role::Report::CompanyActivity;

use Moose::Role;

use DateTime::Format::ISO8601;
use Excel::Writer::XLSX;
use Excel::Writer::XLSX::Utility;
use File::Temp;

use Stream2::O::File;

use Scalar::Util;

use Log::Any qw/$log/;

requires 'stream2';
requires 'user_object';

has 'from_str' => ( is => 'ro', isa => 'Maybe[Str]');
has 'to_str'   => ( is => 'ro', isa => 'Maybe[Str]' );

has 'from_datetime' => ( is => 'ro', isa => 'DateTime', lazy_build => 1);
has 'to_datetime'   => ( is => 'ro', isa => 'DateTime', lazy_build => 1);

# actions to report on: an ordered "map" of frontend => backend names
my @ACTION_NAMES = (

    # [ frontend_action_name => backend_action_name ]
    [ Searches        => undef ],
    [ Viewed          => 'profile'],
    [ Downloaded      => 'download' ],
    [ Saved           => 'import' ],
    [ Shortlisted     => 'shortlist_adc_shortlist' ],
    [ Longlisted      => 'longlist_add' ],
    [ '(Tearsheeted)' => 'shortlist_bullhorn_tearsheets' ],
    [ Forwarded       => 'forward' ],
    [ Messaged        => 'message' ],
    [ Tagged          => 'candidate_tag_add' ],
    [ Edited          => 'update']
);

sub _build_from_datetime{
    my ($self) = @_;
    unless( $self->from_str() ){
        my $from = DateTime->now();
        $from->subtract( months => 1)->truncate( to => 'month' );
        return $from;
    }
    return DateTime::Format::ISO8601->parse_datetime($self->from_str());
}

sub _build_to_datetime{
    my ($self) = @_;
    unless( $self->to_str() ){
        my $to = DateTime->now();
        $to->truncate( to => 'month' );
        return $to;
    }
    return DateTime::Format::ISO8601->parse_datetime($self->to_str());
}


=head2 generate_files

Generates a spreadsheet with these columns:

Company Office Team User UserID Searches Viewed Downloaded Saved Longlisted Tearsheeted Forwarded Messages Tagged Edited

Returns the generate files.

=cut


sub generate_files{
    my ($self, $opts ) = @_;
    $opts //= {};
    my $aggregation = $opts->{aggregation} // $self->generate_aggregation();


    $log->info("Generating excel file");
    my ($fh, $filename) = File::Temp::tempfile();
    my $workbook = Excel::Writer::XLSX->new( $filename );
    my $string_format = $workbook->add_format( locked => 1 );
    my $header_format = $workbook->add_format( locked => 1 , bold => 1 );
    my $emphasis_format = $workbook->add_format( bold => 1,
                                                 italic => 1,
                                                 locked => 1
                                             );
    my $integer_format = $workbook->add_format( locked => 1 );
    $integer_format->set_num_format( 0x03 ); # See https://metacpan.org/pod/Excel::Writer::XLSX#CELL-FORMATTING

    my $em_integer_format = $workbook->add_format( locked => 1);
    $em_integer_format->set_num_format( 0x03 ); # See https://metacpan.org/pod/Excel::Writer::XLSX#CELL-FORMATTING
    $em_integer_format->set_italic();
    $em_integer_format->set_bold();


    ## User activity sheet
    {
        my $sheet = $workbook->add_worksheet('UserActivity');

        # Generate counts.
        my $path_length = $aggregation->{_meta}->{path_length};
        {
            # The header
            my ($row, $col) = ( 0, 0 );
            my $hierarchy = $self->user_object()->identity_provider()->user_hierarchy_header();
            foreach my $level ( @{$hierarchy} ){
                $sheet->write( $row, $col++ , $self->stream2()->__($level), $header_format );
            }
            $sheet->write( $row, $col++ , $self->stream2()->__("Database") , $header_format );
            # Freezing panes header, and hierarchy (+ Database).
            $sheet->freeze_panes(1, $col);
            $sheet->autofilter(0, 0, 1, $col - 1);
            foreach my $data_header ( map { $_->[0] } @ACTION_NAMES ){
                $sheet->write( $row, $col++ , $self->stream2()->__( $data_header ) , $header_format );
            }
        }
        {
            # The data body
            my ( $row , $col ) = ( 1 , 0 );
            _traverse( $aggregation,
                       sub{
                           my ($bucket) = @_;
                           foreach my $fragment ( @{$bucket->{_path}} ){
                               $sheet->write( $row, $col++ , $fragment, $string_format );
                           }
                           # Searches
                           $sheet->write( $row, $col++ , ( $bucket->{search} // 0  ) * 1, $integer_format);
                           foreach my $action ( grep { defined } map { $_->[1] } @ACTION_NAMES ){
                               $sheet->write( $row, $col++ , ( $bucket->{'action_'.$action} // 0 ) * 1, $integer_format);
                           }
                           # Row is showed, at outline level of the lenght of the path
                           my $outline_level = scalar( @{$bucket->{_path}} ) - 1;
                           $sheet->set_row( $row, undef, undef, 0, $outline_level , 0 );

                           $row++;
                           $col = 0;
                       },
                       sub{
                           my ($node) = @_;
                           # use Data::Dumper;
                           # local $Data::Dumper::Maxdepth = 1;
                           # warn Dumper( $node );
                           unless( $node->{_path} ){ return; } # No root node please
                           foreach my $fragment ( @{$node->{_path} } ){
                               $sheet->write( $row, $col++ , $fragment , $emphasis_format );
                           }
                           $sheet->write( $row, $col = ( $path_length - 1 ), 'Totals:' , $emphasis_format );
                           $col++;

                           # 11 subtotals (11 actions reported on).
                           foreach my $total ( 1..11 ){
                               $sheet->write( $row, $col, '=SUBTOTAL(9,'.xl_rowcol_to_cell( $row - $node->{_incl_count} + 1, $col ).':'
                                                  .xl_rowcol_to_cell($row - 1,$col).')', $em_integer_format );
                               $col++;
                           }

                           # $sheet->write( $row, $col++, $node->{_count} );
                           # Row is showed, at outline level of the lenght of the path (-1
                           my $outline_level = scalar( @{$node->{_path}} ) - 1;
                           if( $outline_level ){
                               $sheet->set_row( $row, undef, undef, 0, $outline_level , 0 );
                           }

                           $row++;
                           $col = 0;
                       }
                   );
        }
    }

    ## Database activity sheet
    {
        my $sheet = $workbook->add_worksheet('DatabaseActivity');
        my $db_aggregation = {};
        {
            # Aggregate the data by DB
            _traverse( $aggregation,sub{
                           my ($bucket) = @_;
                           my $db_bucket = _target_bucket( $db_aggregation, [ $bucket->{_path}->[-1] ]);
                           $db_bucket->{search} //= 0;
                           $db_bucket->{search} += ( $bucket->{search} // 0 ) * 1;
                           foreach my $action ( grep { defined } map { $_->[1] } @ACTION_NAMES ){
                               $db_bucket->{'action_'.$action} //= 0;
                               $db_bucket->{'action_'.$action} += ( $bucket->{'action_'.$action} // 0 ) * 1;
                           }
                       }, sub{}
                   );
        }
        {
            # Header
            my ($row, $col) = ( 0 , 0 );
            $sheet->write( $row, $col++ , $self->stream2()->__("Database") , $header_format );
            $sheet->freeze_panes(1, $col);
            $sheet->autofilter(0, 0, 1, $col - 1);
            foreach my $data_header ( map { $_->[0] } @ACTION_NAMES ){
                $sheet->write( $row, $col++ , $self->stream2()->__( $data_header ) , $header_format );
            }
        }
        {
            # Body
            my ( $row , $col ) = ( 1 , 0 );
            my $chart = $workbook->add_chart( type => 'column',
                                              name => 'Databases Activity',
                                              embedded => 1
                                          );

            _traverse( $db_aggregation,
                       sub{
                           my ($bucket) = @_;
                           my $db_name = $bucket->{_path}->[-1];
                           $sheet->write( $row, $col++ , $db_name, $string_format );
                           # Searches
                           $sheet->write( $row, $col++ , ( $bucket->{search} // 0  ) * 1, $integer_format);
                           foreach my $action ( grep { defined } map { $_->[1] } @ACTION_NAMES ){
                               $sheet->write( $row, $col++ , ( $bucket->{'action_'.$action} // 0 ) * 1, $integer_format);
                           }

                           my $name = '=DatabaseActivity!'.xl_rowcol_to_cell( $row, 0 , 1 , 1 );
                           my $categories = '=DatabaseActivity!'.xl_rowcol_to_cell( 0, 1, 1, 1).':'.xl_rowcol_to_cell( 0, $col - 1, 1, 1 );
                           my $values = '=DatabaseActivity!'.xl_rowcol_to_cell( $row, 1, 1, 1).':'.xl_rowcol_to_cell( $row, $col - 1, 1, 1 );
                           $chart->add_series(
                               name => $name,
                               categories => $categories,
                               values => $values
                           );

                           $row++;
                           $col = 0;
                       },
                       sub{}
                   );
            $chart->set_title( name => 'Databases Activity' );
            $chart->set_size( width => 800, height => 600 );
            $sheet->insert_chart('$B$2', $chart );
        }
    }


    $workbook->close();
    $log->info("Excel file has been generated");

    my $file = Stream2::O::File->new({ name => 'search_activity.xlsx',
                                       mime_type => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                       disk_file => $filename
                                   });
    return [ $file ];
}

=head2 generate_table

Flattens the aggregation in a displayable table.

A table is:

  {
    head => [ [ { value => .. }, .. ], ..],
    body => [ [ { value => .. }, .. ], ..],
    foot => [ [ { value => .. }, ,, ], ..]
  }

=cut

sub generate_table{
    my ($self, $opts ) = @_;
    $opts //= {};
    my $aggregation = $opts->{aggregation} // $self->generate_aggregation();

    my $head = [ [] ];
    {
        my ( $row , $col ) = ( 0 , 0 );
        my $hierarchy = $self->user_object()->identity_provider()->user_hierarchy_header();
        foreach my $level ( @{$hierarchy} ){
            $head->[$row]->[$col++] = { value =>  $self->stream2()->__($level) };
        }
        $head->[$row]->[$col++] = { value =>  $self->stream2()->__("Database") };
        foreach my $data_header ( map { $_->[0] } @ACTION_NAMES ){
            $head->[$row]->[$col++] = { value =>  $self->stream2()->__($data_header) };
        }
    }

    my $body = [ [] ];
    {
        my ( $row , $col ) = ( 0 , 0 );
        _traverse( $aggregation,
                   sub{
                       my ($bucket) = @_;
                       $body->[$row] //= [];
                       foreach my $fragment ( @{$bucket->{_path}} ){
                           $body->[$row]->[$col++] = { value =>  $fragment };
                       }
                       # Searches
                       $body->[$row]->[$col++] = { value => (  $bucket->{search} // 0  ) * 1 } ;
                       # Viewed
                       my $user_id = $bucket->{_user_id};
                       my $board   = $bucket->{_board};
                       foreach my $header_action_map ( grep { defined $_->[1] } @ACTION_NAMES ){
                            my ($header_name, $action_name) = @{$header_action_map};
                           if( $bucket->{'action_'.$action_name} ){
                               $body->[$row]->[$col] = { user_id => $user_id, action => $action_name,
                                                         header => $header_name,
                                                         board => $board,
                                                         value => $bucket->{'action_'.$action_name} * 1 };
                           }
                           else {
                                $body->[$row]->[$col] = '';
                           }
                           $col++;
                       }
                       $row++;
                       $col = 0;
                   },
                   sub{}
               );
    }
    return { head => $head, body => $body };
}


## The tree traversal. Callback is called on leafs.
# sub _traverse{
#     my ($aggregation, $cb_bucket, $cb_node ) = @_;
#     my @queue;
#     push @queue , $aggregation;
#     while(my $tree = pop @queue ){
#         if( $tree->{_meta_bucket} ){
#             $cb_bucket->( $tree );
#             next;
#         }
#         foreach my $key ( sort grep { $_ !~ /^_/ } keys %$tree ){
#             if( ( ref( $tree->{$key} ) // '' ) eq 'HASH' ){
#                 push @queue , $tree->{$key};
#             }
#         }
#         $cb_node->( $tree );
#     }
# }


# Tree traversal. Leafs then immediate parent
sub _traverse{
    my ($aggregation, $cb_bucket, $cb_node) = @_;
    if( $aggregation->{_meta_bucket} ){
        return $cb_bucket->( $aggregation );
    }
    foreach my $key ( sort grep { $_ !~ /^_/ } keys %$aggregation ){
        if( ( ref( $aggregation->{$key} ) // '' ) eq 'HASH' ){
            _traverse( $aggregation->{$key} , $cb_bucket , $cb_node );
        }
    }
    # We call the callback on the aggregation WHEN we come back from the
    # recursion. This is because we want the subtotals to be after the nodes.
    return $cb_node->( $aggregation );
}


=head2 generate_aggregation

Returns an aggregation of activity count following this structure:

   { <company> => { <office> => { <team> } => { <user> => { data bucket with actions } }}
     _meta => {
        path_length => 4,
        min_action_id => <...>
        max_action_id => <...>
     }
   }

=cut

sub generate_aggregation{
    my ($self) = @_;

    my $identity_provider = $self->user_object()->identity_provider();
    my $overseen_rs = $identity_provider->user_oversees( $self->user_object() );
    my %overseen_by_id = ();

    while( my $overseen_user = $overseen_rs->next() ){
        my $hierarchy_string = eval{ $identity_provider->get_hierarchical_identifier( $overseen_user ); };
        unless( $hierarchy_string ){
            my $err = $@;
            $log->warn("Cannot find hierarchical path of user ".$overseen_user->id()." never logged in? Error was : $err");
        }
        my @hierarchy = split('/', $hierarchy_string );
        shift @hierarchy; # ( '' , 'toplevel' , 'andsoon' ) . We dont want the empty string.
        $log->trace("User ".$overseen_user->id()." hierarchy is ".join(', ', @hierarchy ));
        $overseen_by_id{ $overseen_user->id() } =
            {
                user => $overseen_user,
                hierarchy => \@hierarchy,
            };
    }

    # The aggregated data structure.
    my $aggregate = { _meta => {} };


    # Pupulate data from actions.
    do{
        my $action_logs = $self->stream2()->factory('CandidateActionLog');
        my $first_row = $action_logs->find_after_date($self->from_datetime());
        my $last_row  = $action_logs->find_after_date($self->to_datetime());
        unless( $first_row && $last_row ){
            $log->info("No Action data rows were found. Aborting data generation");
            return;
        }
        $log->info("Found action first_row = ".$first_row->id());
        $log->info("Found action last_row  = ".$last_row->id());

        $aggregate->{_meta}->{min_action_id} = $first_row->id();
        $aggregate->{_meta}->{max_action_id} = $last_row->id();

        my $actions = $action_logs->search({
            'me.group_identity' => $self->user_object()->group_identity(),
            -and => [ 'me.id' => { '>=' => $first_row->id() }, 'me.id' => { '<' => $last_row->id() } ]
        });
        $actions->fast_loop_through(
            sub{
                my ($log) = @_;
                my $user_hash = $overseen_by_id{ $log->user_id() };
                unless( $user_hash ){
                    # No overseen user. Skipping.
                    return;
                }

                $aggregate->{_meta}->{path_length} //= scalar( @{$user_hash->{hierarchy}} ) + 1;
                my $bucket = _target_bucket( $aggregate, [ @{ $user_hash->{hierarchy} } , $log->destination() ] );

                $bucket->{_path} = [ @{ $user_hash->{hierarchy} } , $log->destination() ];
                $bucket->{'_user_id'} = $log->user_id();
                $bucket->{'_board'} = $log->destination();
                # Now is time to count the events in the bucket.
                $bucket->{'action_'.$log->action()} //= 0;
                $bucket->{'action_'.$log->action()}++;
            },
            { rows => 500 }
        );
    };

    ## Populate with searches.
    do{
        my $search_logs = $self->stream2()->factory('SearchRecord');
        my $first_row = $search_logs->find_after_date($self->from_datetime());
        my $last_row  = $search_logs->find_after_date($self->to_datetime());
        unless( $first_row && $last_row ){
            $log->info("No Search data rows were found. Aborting data generation");
            return;
        }
        $log->info("Found search first_row = ".$first_row->id());
        $log->info("Found search last_row  = ".$last_row->id());
        my $searches = $search_logs->search({
            'user.group_identity' => $self->user_object()->group_identity(),
            -and => [ 'me.id' => { '>=' => $first_row->id() }, 'me.id' => { '<' => $last_row->id() } ]
        }, { join => 'user' } );
        $searches->fast_loop_through(
            sub{
                my ($record) = @_;
                my $user_hash = $overseen_by_id{ $record->user_id() };
                unless( $user_hash ){
                    # No overseen user. Skipping.
                    return;
                }
                my $data = $record->data() || {};
                # We are only interested in successful searches here.
                if( $data->{watchdog} || $data->{error_type} ){
                    return;
                }
                my $bucket = _target_bucket( $aggregate, [ @{ $user_hash->{hierarchy} } , $record->get_column('board') ] );
                $bucket->{_path} = [ @{ $user_hash->{hierarchy} } , $record->get_column('board') ];
                $bucket->{'search'} //= 0;
                $bucket->{'search'}++;
            },
            { rows => 500 }
        );
    };

    # Do some counting of leafs and nodes.
    _traverse( $aggregate , sub{
                   my ($bucket) = @_;
                   $bucket->{_count} = 1; # A bucket counts for 1.
                   $bucket->{_incl_count} = 1;
                   $bucket->{_parent}->{_count} //= 0; # The count of leafs
                   $bucket->{_parent}->{_incl_count} //= 1; # The inclusive count, where the node itself counts for 1

                   $bucket->{_parent}->{_count}++;
                   $bucket->{_parent}->{_incl_count}++;

               },
               sub{
                   my ($node) = @_;
                   $node->{_parent}->{_count} //= 0;
                   $node->{_parent}->{_count} += ( $node->{_count} // 0 );
                   $node->{_parent}->{_incl_count} //= 1;
                   $node->{_parent}->{_incl_count} += ( $node->{_incl_count} // 0 );
               });

    return $aggregate;
}


sub _target_bucket{
    my ( $aggregate, $hierarchy ) = @_;
    my $bucket = $aggregate;
    #
    # The granularity of this report is:
    # User Hierarchy , destination (board).
    #
    my @cur_path = ();
    foreach my $level ( @$hierarchy ){
        $level //= '';
        push @cur_path , $level;
        $bucket->{$level} //= { _meta_bucket => 0 , _path => [ @cur_path ] };
        my $weak_bucket = $bucket;
        Scalar::Util::weaken( $weak_bucket );
        $bucket = $bucket->{$level};
        $bucket->{_parent} = $weak_bucket;
    }
    $bucket->{_meta_bucket} = 1;
    return $bucket;
}

1;

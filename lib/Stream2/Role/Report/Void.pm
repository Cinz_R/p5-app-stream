package Stream2::Role::Report::Void;

use Moose::Role;

use Stream2::O::File;

requires 'stream2';

has 'foo' => ( is => 'ro', isa => 'Str' , required => 1);

sub generate_files{
    my ($self) = @_;
    my $file = Stream2::O::File->new({ name => 'doge.txt',
                                       mime_type => 'text/plain',
                                       binary_content => $self->stream2()->doge()
                                   });
    return [ $file ];
}

1;

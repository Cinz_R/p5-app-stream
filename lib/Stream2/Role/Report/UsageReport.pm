package Stream2::Role::Report::UsageReport;

use Moose::Role;

use Action::Retry;
use Carp;
use Bean::Juice::APIClient::Company;
use DateTime::Format::ISO8601;
use Excel::Writer::XLSX;
use Excel::Writer::XLSX::Utility;
use File::Temp;

use Stream2::O::File;

use Log::Any qw/$log/;

requires 'stream2';

has 'from_str' => ( is => 'ro', isa => 'Maybe[Str]');
has 'to_str'   => ( is => 'ro', isa => 'Maybe[Str]' );

has 'from_datetime' => ( is => 'ro', isa => 'DateTime', lazy_build => 1);
has 'to_datetime'   => ( is => 'ro', isa => 'DateTime', lazy_build => 1);

sub _build_from_datetime{
    my ($self) = @_;
    unless( $self->from_str() ){
        my $from = DateTime->now();
        $from->subtract( months => 1)->truncate( to => 'month' );
        return $from;
    }
    return DateTime::Format::ISO8601->parse_datetime($self->from_str());
}

sub _build_to_datetime{
    my ($self) = @_;
    unless( $self->to_str() ){
        my $to = DateTime->now();
        $to->truncate( to => 'month' );
        return $to;
    }
    return DateTime::Format::ISO8601->parse_datetime($self->to_str());
}

=head2 generate_files

Generates a spreadsheet with those columns:

 juicename , nicename, active users, search runs, cv downloaded

=cut

sub generate_files{
    my ($self) = @_;

    my $companies   = {};
    my $all_actions = {};
    # retrive any settings for this report.
    my $settings = $self->settings;
    {
        # First go through all action logs
        my $action_logs = $self->stream2()->factory('CandidateActionLog');

        my $first_row = $action_logs->find_after_date($self->from_datetime());
        my $last_row  = $action_logs->find_after_date($self->to_datetime());
        # protect against there being no records in 'CandidateActionLog'
        next unless $first_row or $last_row;
        my $actions = $action_logs->search({
            'me.id' => { -between => [ $first_row->id() , $last_row->id() ] }
            });


        $actions->fast_loop_through(
            sub{
                my ($log) = @_;
                my $company =  $companies->{$log->group_identity()}
                    //= { actions => {}, users => {}, searches => 0, n_unsub_cands => 0 };
                return if $settings->{ignore_actions}->{$log->action()};
                $company->{actions}->{$log->action()}++;
                $company->{users}->{$log->user_id()}++;
                $all_actions->{$log->action()}++;
            });
    }

    {
        # Now through all search records usage
        my $search_records = $self->stream2()->factory('SearchRecord');

        my $first_row = $search_records->find_after_date($self->from_datetime());
        my $last_row  = $search_records->find_after_date($self->to_datetime());
        # protect against there being no records in 'SearchRecord'
        next unless $first_row or $last_row;
        my $searches = $search_records->search(
            { 'me.id' => { -between => [ $first_row->id(), $last_row->id() ] } },
            { prefetch => 'user' });

        $searches->fast_loop_through(
            sub{
                my ($record) = @_;
                my $company =  $companies->{$record->user()->group_identity()}
                    //= { actions => {}, users => {}, searches => 0, n_unsub_cands => 0 };
                $company->{searches}++;
                $company->{users}->{$record->user_id()}++;
            });
    }

    {
        # Now go through all blacklisted emails to gather the unsubscribers
        my $blacklist_records = $self->stream2()->factory('EmailBlacklist');

        my $first_row = $blacklist_records->find_after_date($self->from_datetime());
        my $last_row  = $blacklist_records->find_after_date($self->to_datetime());
        # protect against there being no records in 'EmailBlacklist'
        next unless $first_row or $last_row;
        my $unsubcried_users = $blacklist_records->search({
                'me.id' => { -between => [ $first_row->id(), $last_row->id() ] }
            },
            { select   => [ 'me.group_identity', { count => 'id' , -as => 'unsub_count' } ],
              group_by => [ 'me.group_identity' ],
              order_by => [ 'unsub_count' ]
        });

        $unsubcried_users->loop_through(
            sub{
                my ($record) = @_;
                $companies->{$record->group_identity}->{n_unsub_cands} =
                    $record->get_column('unsub_count') // 0;
            });
    }

    # Count the number of users and filter the companies here, as there may be
    # no companies to report on once filtered.
    while( my ( $company , $record ) = each %$companies ){
        $record->{company} = $company;
        $record->{n_active_users} = scalar(keys %{$record->{users}});
        $record = $self->_enrich_company_information($record);

        # no we filter the companies
        my $delete_company = 0;
        if ($settings->{filter_company_on}){
            while(my ($filter,$option) = each %{$settings->{filter_company_on}}){
                # skip if the option is blank or set to false or there is no
                # option to filter on.
                next unless $option || $record->{$option};
                $delete_company = 1 if $option ne $record->{$filter};
            }
        }
        delete $companies->{$company} if $delete_company;
    }

    # if there is no data to generate the report, log and return nothing.
    if (0 == keys %{$companies}){

        my $error = "It seems no data usage data was available when generating a usage report.";
        $log->error($error);

        my $json = $self->stream2()->json()->encode({error => $error});
        my $json_file = Stream2::O::File->new({ name => 'search2usage_error.json',
                                                mime_type => 'application/json',
                                                binary_content => $json
                                            });
        my ($fh, $filename) = File::Temp::tempfile();

        my $workbook = Excel::Writer::XLSX->new( $filename );
        my $sheet = $workbook->add_worksheet('Search2 Companies Usage Report');
        $sheet->write(0, 0, $error);
        $workbook->close();

        my $file = Stream2::O::File->new({
            name => 'search2_usage.xlsx',
            mime_type => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            disk_file => $filename
        });

        return [$json_file,$file];
    }
    my @files = ();
    {
        $log->info("Creating JSON File");
        # Create a JSON file with all the data. Just in case.
        my $json = $self->stream2()->json()->encode($companies);
        my $json_file = Stream2::O::File->new({ name => 'search2usage.json',
                                                mime_type => 'application/json',
                                                binary_content => $json
                                            });
        push @files , $json_file;
    }

    # Time to do some excel stuff.
    my @sorted_actions = sort keys %{$all_actions};
    {
        my ($fh, $filename) = File::Temp::tempfile();
        $log->info("Generating Excel file");
        my $workbook = Excel::Writer::XLSX->new( $filename );
        my $sheet = $workbook->add_worksheet('Search2 Companies Usage Report');

        # make a 2 dp percent format.. eg 12.33%
        my $percent_format = $workbook->add_format();
        $percent_format->set_num_format( 0x0a );

        # land marks are a way to keep track of where important places are for further calculations
        my $landmarks = {};
        my ($row, $col) = (0, 0);
        $sheet->write($row, $col, "Search2 Company Usage report. " .
            $self->from_datetime() . ' TO ' . $self->to_datetime());
        $row++; $col = 0;
        $sheet->write($row, $col++, 'Juice Company');
        $sheet->write($row, $col++, 'TZ');
        $sheet->write($row, $col++, 'Account type');
        $sheet->write($row, $col++, 'Billable');
        $sheet->write($row, $col++, 'Active Users');
        $sheet->write($row, $col++, 'Total Users');
        $sheet->write($row, $col++, 'Active / Total Users');
        $sheet->write($row, $col++, 'Users with TS');
        $sheet->write($row, $col++, 'Unsubscribed Candidates');
        $sheet->write($row, $col++, 'Searches');
        for my $acton(@sorted_actions){
            $sheet->write($row, $col++, $acton);
        }
        $row++;
        # set up the totals row.
        $sheet->write($row, 0, 'Totals');
        $landmarks->{totals}->{row} = $row;
        $row++;
        # The totals landmarks will keep track of where we need to so the sums
        $landmarks->{totals}->{s_row} = $row;
        $landmarks->{totals}->{s_col} = 4;
        $landmarks->{totals}->{e_col} = $col -1 ;
        $col = 0;

        # Sort companies by n_active_users
        my @companies = sort{
            $companies->{$b}->{n_active_users} <=> $companies->{$a}->{n_active_users} }
            keys %$companies;
        # spit out all the information.
        foreach my $company_name ( @companies ){
            my $company = $companies->{$company_name};
            my $percent_active_users = 0;
            $percent_active_users = $company->{n_active_users} / $company->{n_total_users}
                if( $company->{n_active_users} && $company->{n_total_users} );
            $sheet->write($row, $col++, $company_name  );
            $sheet->write($row, $col++, $company->{TZ} );
            $sheet->write($row, $col++, $company->{account_type} );
            $sheet->write($row, $col++, $company->{billable} );
            $sheet->write($row, $col++, ( $company->{n_active_users}  // 0 ) );
            $sheet->write($row, $col++, ( $company->{n_total_users}   // 0 ) );
            $sheet->write($row, $col++, $percent_active_users, $percent_format );
            $sheet->write($row, $col++, ( $company->{n_users_with_TS} // 0 ) );
            $sheet->write($row, $col++, ( $company->{n_unsub_cands}   // 0 ) );
            $sheet->write($row, $col++, ( $company->{searches}        // 0 ) );
            map{ $sheet->write($row, $col++, $_ ) }
                map{$company->{actions}->{$_} // 0} @sorted_actions;
            $row++; $col = 0;
        }

        # fill in the totals
        $landmarks->{totals}->{e_row} = $row;
        for my $sum_col($landmarks->{totals}->{s_col}..$landmarks->{totals}->{e_col}){
            my $start_cell  = xl_rowcol_to_cell( $landmarks->{totals}->{s_row}, $sum_col );
            my $end_cell    = xl_rowcol_to_cell( $landmarks->{totals}->{e_row}, $sum_col );
            $sheet->write($landmarks->{totals}->{row}, $sum_col, "=sum($start_cell:$end_cell)" ) ;
        }
        # as the $percent_active_users is not summable we need to fix this.
        {
            my $total_TS_users = xl_rowcol_to_cell(
                $landmarks->{totals}->{s_row}, 4
            );
            my $total_users = xl_rowcol_to_cell(
            $landmarks->{totals}->{s_row}, 5
            );
            $sheet->write($landmarks->{totals}->{s_row} -1, 6,
                "=$total_TS_users/$total_users",
                $percent_format
            )
        }

        # spit out the ranked unsubscribes into its own worksheet for ease of viewing
        my $WS_unsub = $workbook->add_worksheet('Unsubscribed Candidates Report');
        @companies = sort{
            $companies->{$b}->{n_unsub_cands} <=> $companies->{$a}->{n_unsub_cands} }
            keys %$companies;
        ($row,$col) = (0,0);
        $sheet->write($row, $col, "Search2 Unsubscribed Candidates " .
            $self->from_datetime() . ' TO '.$self->to_datetime());
        $WS_unsub->write($row, $col++, 'Juice Name');
        $WS_unsub->write($row, $col++, 'n_unsub_cands');
        $row++;$col=0;
        foreach my $company_name ( @companies ){
            my $company = $companies->{$company_name};
            $WS_unsub->write($row, $col++, $company_name);
            $WS_unsub->write($row, $col++,( $company->{n_unsub_cands} // 0 ));
            $row++;$col=0;
        }


        $workbook->close();

        my $file = Stream2::O::File->new({ name => 'search2_activity.xlsx',
                                           mime_type => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                           disk_file => $filename
                                       });
        push @files, $file;
    }
    return \@files;
}


sub _enrich_company_information {
    my ($self,$company_data) = @_;
    my $stream2  = $self->stream2();

    my @users;
    Action::Retry::retry { @users = $stream2->user_api->company_users($company_data->{company}); }
        strategy => { Fibonacci => { max_retries_number => 5  } };

    $company_data->{$_} = 0 for (qw(zombies n_users_with_TS n_total_users));
    my $tzs = {};
    my $user_factory = $stream2->factory('SearchUser');
    foreach my $user_ref ( @users ) {
        my $id = $user_ref->{id};
        my $user = eval {
            my $stream_user = $user_factory->find({
                provider => 'adcourier',
                provider_id => $id,
            });
            $company_data->{n_total_users}++;
            # Office can be void. Avoid warnings.
            if ( ( $user_ref->{office} // '' ) eq 'zombie' ){
                $company_data->{zombies}++;
            }
            if($stream_user->has_subscription('talentsearch')){
                $company_data->{n_users_with_TS}++;
            }
           $tzs->{$stream_user->timezone()}++;
        }
    }
    #  enrich with some of the juice api data.

    my $company_juice_data;
    Action::Retry::retry
        { $company_juice_data = $stream2->company_api->get_company($company_data->{company}); }
        strategy => { Fibonacci => { max_retries_number => 5  } };

    $company_data->{billable} = $company_juice_data->{billable} // 0;
    $company_data->{account_type} = $company_juice_data->{account_type} // 'UNK';
    $company_data->{TZ} = [reverse sort {$tzs->{$a} <=> $tzs->{$b} } keys %{$tzs}]->[0]  // 'UNK';
    return $company_data;
}

1;

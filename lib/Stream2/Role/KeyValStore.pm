package Stream2::Role::KeyValStore;
{
  $Stream2::Role::KeyValStore::VERSION = '0.001';
}

# ABSTRACT: adds methods to interact with the Redis KV store

use Moose::Role;
use Redis;
use Stream2::Trait::Stringable;

=head1 NAME

Stream2::Role::KeyValStore - Make a Moose object capable of containing a Redis storage and talking to it using '$self->r_' prefixed redis methods.

=head1 SYNOPSIS

  package My::Class;
  use Moose;
  with qw/Stream2::Role::KeyValStore/;

  1;

  package main;

  my $o = My::Class->({ store => .. A Redis instance });

  $o->r_keys() is now equivalent to $o->store->keys();
  # etc.. for all Redis methods.


=cut

has 'store' => (
  is      => 'rw',
  lazy    => 1,
  default => sub {
    return Redis->new(
      encoding => 'UTF-8',
      # debug    => 1,
    );
  },
  traits  => ['Stream2::Trait::Stringable'],
  stringable => 0,
  handles => {
    # Key methods
    r_del        => 'del',
    r_exists     => 'exists',
    r_expire     => 'expire',
    r_keys       => 'keys',
    r_persist    => 'persist',
    r_rename     => 'rename',
    r_renamenx   => 'renamenx',
    r_sort       => 'sort',
    r_ttl        => 'ttl',
    r_type       => 'type',

    # String methods
    r_set        => 'set',
    r_get        => 'get',
    r_setex      => 'setex',

    # Hash methods
    r_hdel       => 'hdel',
    r_hexists    => 'hexists',
    r_hget       => 'hget',
    r_hgetall    => 'hgetall',
    r_hincrby    => 'hincrby',
    r_hincrbyfloat => 'hincrbyfloat',
    r_hkeys      => 'hkeys',
    r_hlen       => 'hlen',
    r_hmget      => 'hmget',
    r_hmset      => 'hmset',
    r_hset       => 'hset',
    r_hsetnx     => 'hsetnx',
    r_hvals      => 'hvals',

    # List methods
    r_blpop      => 'blpop',
    r_brpop      => 'brpop',
    r_brpoplpush => 'brpoplpush',
    r_lindex     => 'lindex',
    r_linsert    => 'linsert',
    r_llen       => 'llen',
    r_lpo        => 'lpop',
    r_lpush      => 'lpush',
    r_lpushx     => 'lpushx',
    r_lrange     => 'lrange',
    r_lrem       => 'lrem',
    r_lset       => 'lset',
    r_ltrim      => 'ltrim',
    r_rpop       => 'rpop',
    r_rpoplpush  => 'rpoplpush',
    r_rpush      => 'rpush',
    r_rpushx     => 'rpushx',
  },
);

1;

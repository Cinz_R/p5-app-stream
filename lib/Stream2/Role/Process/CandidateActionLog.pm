package Stream2::Role::Process::CandidateActionLog;

use Moose::Role;

=head1 NAME

Stream2::Role::Process::CandidateActionLog - Action logger role for longstep processes

=head2 log_candidate_action

Convenience method to insert CandidateActionLog records into the database.

Usage:

    $self->log_candidate_action('some_action_str', $candidate, { some => action_data });

=cut

sub log_candidate_action {
    my ($self, $action, $candidate, $data) = @_;
    $data //= {};

    my $stream2 = $self->stream2();
    my $user = $self->user();

    $stream2->action_log()->insert({
        action              => $action,
        candidate_id        => $candidate->id(),
        destination         => $candidate->destination(),
        user_id             => $user->id(),
        group_identity      => $user->group_identity(),
        search_record_id    => $candidate->search_record_id(),
        data                => {
            %$data,
            $candidate->action_hash(),
        }
    });
}

1;

package Stream2::Role::EmailTemplateRule::ResponseReceived;

use Moose::Role;
use Log::Any qw/$log/;

=head1 NAME

Stream2::Role::EmailTemplateRule::ResponseReceived- A role that turns an EmailTemplateRule into one that sends the template on receiving the application

=cut


around 'update' => sub{
    my ($orig, $self , $args) = @_;
    unless( $self->group_macro_id() ){
        $log->debug("Macro not implemented. Creating it");
        my $new_macro = $self->stream2()->factory('GroupMacro')->create({
            group_identity => $self->email_template()->group_identity(),
            on_action => 'Stream2::Action::NewBBCSSResponse',
            name => 'Template-Rule-'.$self->id(),
            lisp_source => $self->generate_lisp()
        });
        $self->group_macro_id( $new_macro->id() );
    }else{
        $log->debug("Macro is there. Making sure it contains the right code");
        $self->group_macro()->lisp_source( $self->generate_lisp() );
        $self->group_macro()->update();
    }
    $self->$orig( $args );
};


around 'delete' => sub{
    my ($orig, $self) = @_;
    my $macro = $self->group_macro();
    my $ret = $self->$orig();
    $macro->delete();
    return $ret;
};

sub generate_lisp{
    my ($self) = @_;
    return '(s2#send-email-template "'.$self->email_template_id().'")';
}

1;

package Stream2::Role::EmailTemplateRule::ResponseFlagged;

use Moose::Role;
use Log::Any qw/$log/;

=head1 NAME

Stream2::Role::EmailTemplateRule::ResponseFlagged - A role that turns an EmailTemplateRule into one that sends the template on a flag.

=cut

# The generated macro code that will implement self rule.
my $LISP_TEMPLATE = q|(if (s2#response-is-being-flagged "[self.flag_id]")
[- IF self.in_minutes -]
 (s2#do-later [self.in_minutes] (begin (s2#send-email-template "[self.email_template_id]")))
[- ELSE -]
 (begin (s2#send-email-template "[self.email_template_id]"))
[- END -]
)|;


has 'flag_id' => ( is => 'rw', isa => 'Str', lazy_build => 1 );
has 'in_minutes' => ( is => 'rw', isa => 'Int', lazy_build => 1);

requires 'stream2';

sub _build_in_minutes{
    my ($self) = @_;
    unless( $self->group_macro_id() ){
        $log->info("No associated macro ID. Default to 1");
        return 1;
    }
    my $ast = $self->_lookup_ast_by_symbol('s2#do-later');
    unless( $ast ){ return 0; } # do-later not found. Assuming zero.
    my ( $in_minutes ) = $ast->slice(1..1);
    $log->trace("Found in_minutes=".$in_minutes->value() );
    return $in_minutes->value() * 1;

}

sub _build_flag_id{
    my ($self) = @_;
    unless( $self->group_macro_id() ){
        confess("Cannot lazy_build flag_id. No group_macro_id");
    }
    my $ast = $self->_lookup_ast_by_symbol('s2#response-is-being-flagged');
    my ( $flagid ) = $ast->slice(1..1);
    $log->trace("Found flag id=".$flagid->value() );
    return $flagid->value().'';
}

sub _lookup_ast_by_symbol{
    my ($self, $symbol) = @_;
    unless( $self->group_macro_id() ){
        confess("Cannot scan for symbol. No group_macro_id");
    }
    my $lisp_source = $self->group_macro()->lisp_source();
    $log->trace('Parsing '.$lisp_source.' in the hunt for the s2#response-is-being-flagged <flagid>');
    return $self->stream2()->lisp()->lookup_ast_by_symbol( $lisp_source , $symbol );
}

around 'to_hash' => sub{
    my ($orig, $self) = @_;
    return {
        %{$self->$orig()},
        flag_id => $self->flag_id(),
        in_minutes => $self->in_minutes() * 1,
    };
};

around 'update' => sub{
    my ($orig, $self , $args) = @_;
    my $lisp_source = $self->stream2()->templates()->render_simple( $LISP_TEMPLATE , { self => $self });
    unless( $self->group_macro_id() ){
        $log->debug("Macro not implemented. Creating it");
        my $new_macro = $self->stream2()->factory('GroupMacro')->create({
            group_identity => $self->email_template()->group_identity(),
            on_action => 'Stream2::Action::UpdateCandidate',
            name => 'Template-Rule-'.$self->id(),
            lisp_source => $lisp_source
        });
        $self->group_macro_id( $new_macro->id() );
    }else{
        $log->debug("Macro is there. Making sure it contains the right code");
        $self->group_macro()->lisp_source( $lisp_source );
        $self->group_macro()->update();
    }
    $self->$orig( $args );
};


around 'delete' => sub{
    my ($orig, $self) = @_;
    my $macro = $self->group_macro();
    my $ret = $self->$orig();
    $macro->delete();
    return $ret;
};

1;

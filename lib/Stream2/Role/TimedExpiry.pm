package Stream2::Role::TimedExpiry;
{
  $Stream2::Role::TimedExpiry::VERSION = '0.001';
}

# ABSTRACT: store your object in Redis for a limited period of time

use Moose::Role;
use Stream2::Types;

requires qw/id r_prefix serialise deserialise/;

with 'Stream2::Role::KeyValStore';

has 'expires_after' => (
  is      => 'rw',
  isa     => 'Stream2::Types::Seconds',
  coerce  => 1,
  lazy    => 1,
  builder => 'default_expires_after',
);

sub default_expires_after { return '60s'; }

sub r_key {
  my $self = shift;
  return $self->r_prefix() . ':' . $self->id();
}

sub load {
  my $self = shift;

  my $data = $self->r_get($self->r_key)
      or return;
  return $self->new_from_string($data);
}

sub save {
  my $self = shift;

  my $data = $self->serialise();
  return $self->r_set($self->r_key, $data)
    && $self->r_expire($self->r_key, $self->expires_after);
}

1;

package Stream2::SearchUser::Subscriptions;

use Moose;
use Crypt::CBC;
use MIME::Base64;
use JSON;

has 'user' => ( is => 'ro', isa => 'Stream2::O::SearchUser' , required => 1 );

has 'ref' => (
    is => 'ro',
    isa => 'HashRef',
    lazy => 1,
    builder => '_build_ref'
);
has 'key' => (
    is => 'ro',
    isa => 'Str'
);

has 'cipher' => (
    is => 'ro',
    isa => 'Crypt::CBC',
    lazy => 1,
    builder => '_build_cipher'
);

# Shortcut to user id
sub user_id{
    my ($self) = @_;
    return $self->user()->id().'' ;
}

# Shortcut to schema
sub schema{
    my ($self) = @_;
    $self->user()->stream2()->stream_schema();
}

sub _build_ref {
    my $self = shift;
    my $rs = $self->schema->resultset('CvSubscription')->search({
        user_id => $self->user_id
    }, { prefetch => 'board_object' } );
    my @records = $rs->all();

    my $subs_ref = {};
    map {
        my $subs_json = $self->_decrypt( $_->auth_data );
        $subs_ref->{$_->board} = {# Direct subscription properties
                                  auth_tokens => JSON::decode_json( $subs_json ),
                                  eza_number  => $_->eza_number(),
                                  custom_source_code => $_->custom_source_code(),

                                  # Board object properties
                                  nice_name   => $_->board_object->nice_name,
                                  type        => $_->board_object->type,
                                  board_id    => $_->board_object->id(),
                                  disabled    => $_->board_object->disabled(),
                                  allows_acc_sharing => $_->board_object->allows_acc_sharing()
                                 };
    } @records;

    return $subs_ref;
}

sub save {
    my $self = shift;

    my $new_subs_ref = $self->ref
        or die "New subscriptions ref is required for save";

    # # Only keep the active up to date subscriptions in the database
    # my $old_rs = $self->schema->resultset('CvSubscription')->search({
    #     user_id => $self->user_id
    # });
    # $old_rs->delete();

    my $schema = $self->schema();

    my $stuff = sub{

        foreach my $board ( keys %$new_subs_ref ){
            my $imported_board = $new_subs_ref->{$board};

            my $encrypted_auth_data = $self->_encrypt( JSON::encode_json( $imported_board->{auth_tokens} // {}  ));

            my $board_row = $self->schema()->resultset('Board')->find_or_create({
                                                                                 name => $board,
                                                                                 nice_name => $imported_board->{nice_name} // $board,
                                                                                });
            if( $self->user()->provider() =~ /^(?:adcourier|dummy)/ ){
              # Only adcourier (and dummy for testing) users can update board properties.
              # this is because we don't trust other providers to send the right information
              # about boards.
              $board_row->nice_name( $imported_board->{nice_name} // $board );
              $board_row->type( $imported_board->{type} );
              $board_row->id( $imported_board->{board_id} );
              $board_row->allows_acc_sharing( ( $imported_board->{allows_acc_sharing} // 1 ) ? 1 : 0 ); # Defaults to 1 if not specified in the imported_board
              $board_row->disabled( $imported_board->{cvsearch_temp_disabled} ? 1 : 0 );
              $board_row->update();
            }

            my $row = $self->schema->resultset('CvSubscription')
              ->find_or_create({
                     user_id         => $self->user_id,
                     board           => $board,
                     board_nice_name => $imported_board->{nice_name} // $board
                    });            ## Update the subscription properties.
            $row->eza_number( $imported_board->{eza_number} );
            $row->custom_source_code( $imported_board->{custom_code} );
            $row->auth_data( $encrypted_auth_data );

            $row->update();
        }

        ## We did set the existing rows. Now remove the subscriptions
        ## that are not in the injected ones.
        $schema->resultset('CvSubscription')->search({ user_id => $self->user_id(),
                                                       board => { '-not_in' => [ keys %$new_subs_ref  ] }
                                                     })->delete();
    };

    return $schema->txn_do($stuff);

    return 1;
}

sub _decrypt {
    my ( $self, $data ) = @_;
    my $message = $self->cipher->decrypt( MIME::Base64::decode_base64( $data ) );
    return $message;
}

sub _encrypt {
    my ( $self, $message ) = @_;
    my $data = MIME::Base64::encode_base64( $self->cipher->encrypt( $message ), "" );
    return $data;
}

sub _build_cipher {
    my $self = shift;
    my $cipher = Crypt::CBC->new(
        -key    => $self->key,
        -cipher => "Crypt::OpenSSL::AES"
    );
    return $cipher;
}

1;

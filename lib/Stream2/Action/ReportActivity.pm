package Stream2::Action::ReportActivity;

use Moose;
use Log::Any qw/$log/;

extends qw/Stream2::Action/;

use Data::Dumper;
use DateTime;


with qw/
           Stream2::Role::Action::InManageError
           Stream2::Role::Action::HasUser
       /;

with qw/Stream2::Role::Report::CompanyActivity/;


with qw/
           Stream2::Role::Action::FastJob
           Stream2::Role::Action::InLogChunk
       /;

sub instance_perform {
    my ( $self ) = @_;

    my $report_structure = $self->generate_aggregation();
    my $files = $self->generate_files({ aggregation => $report_structure }); # From the Stream2::Role::Report::CompanyActivity

    {
        local $Data::Dumper::Maxdepth = 2;
        $log->info("Got files from report role ", Dumper( $files ) );
    }

    # We need to give the end user
    # access to the files of this report.
    # We do that through private S3 files. Downloading is valid for 1 day.
    my $private_files = $self->stream2()->factory('PrivateFile');
    my @s3_files = ();
    my $download_expiry_date = DateTime->now()->add({ days => 1 })->ymd();
    foreach my $file ( @{$files} ){
        my $s3_object = $private_files
            ->create({ content_type => $file->mime_type(),
                       content => $file->binary_content(),
                       content_disposition => 'attachment; filename='.$file->name(),
                       expires => $download_expiry_date,
                       uuid => 'reports/'.$self->user_object()->group_identity().'/'.$self->stream2()->uuid()->create_str()
                   });
        my $auth_url = $s3_object->query_string_authentication_uri();
        $log->info("Auth URL is: ".$auth_url);
        push @s3_files , {
            content_type => $file->mime_type(),
            file_name => $file->name(),
            file_url =>  $auth_url.'',
        }
    }

    my $table = $self->generate_table({ aggregation => $report_structure });

    return {
        s3_files => \@s3_files ,
        table => $table,
        log_chunk_id => $self->log_chunk_id() ,
        meta => $report_structure->{_meta}
    };
}

__PACKAGE__->meta->make_immutable();

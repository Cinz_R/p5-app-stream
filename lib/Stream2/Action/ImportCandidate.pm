package Stream2::Action::ImportCandidate;

use Moose;
use Data::Dumper;
use MIME::Base64;

use Scope::Guard;

use Stream::EngineException;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;

with qw/
           Stream2::Role::Action::InManageError
           Stream2::Role::Action::HasUser
           Stream2::Role::Action::HasRemoteIP
       /;


with qw/
           Stream2::Role::Action::TriggerMacros

           Stream2::Role::Action::HasCandidate

           Stream2::Role::Action::FastJob
           Stream2::Role::Action::InLogChunk
           Stream2::Role::Action::Email
       /;

with qw/
           Stream2::Role::Action::DownloadCV
       /;

has [ '+on_managed_error', '+on_not_managed_error' ] => ( default => sub {
    my $self = shift;
    return sub {
        $self->stream2()->action_log->insert({
            action          => "import_fail",
            candidate_id    => $self->candidate_object()->id(),
            destination     => $self->candidate_object()->destination(),
            user_id         => $self->user_object->id,
            group_identity  => $self->user_object->group_identity,
            search_record_id => $self->candidate_object->search_record_id(),
            data            => { s3_log_id => $self->log_chunk_id(),
                                 $self->candidate_object()->action_hash()
                             },
        });
    }
});

=head2 instance_perform

AKA 'Save'. This is the 'Save' button action.

Usage:

    Stream2::Action::ImportCandidate->new({
        user            => { user_id => 12345 },
        results_id      => 123,
        candidate_idx   => 17,
        remote_ip       => $ip,
        options         => {
            candidate_attributes => {
                csp_job_id => 12345
            }
        }
    });

=cut

sub instance_perform {
    my ($self) = @_;

    my $user = $self->user_object();
    my $candidate = $self->candidate_object();
    my $cv_file = $self->download_cv_file();

    # All of that relate to the Download CV.
    my $content = $cv_file->binary_content; # That's the CV binary.
    my $content_filename = $cv_file->name();

    my $content_type = $cv_file->mime_type() || 'text/plain';
    my $content_b64 = MIME::Base64::encode_base64( $content );

    my $original_destination = $candidate->destination();
    my $subscription_info = $self->user_object->has_subscription( $original_destination ) || {};

    # Login provider here, cause the hability to import a candidate
    # can depend on ripple settings (like the presence of a candidate URL).
    unless ( $user->login_provider->user_can_import_from( $user, $original_destination ) ){
        $log->errorf( "Candidate cannot be imported from %s", $original_destination );
        die Stream::EngineException::NotSupported->new({ message => "Candidate cannot be imported from $original_destination\n" } );
    }

    my $board_nice_name = $subscription_info->{nice_name} // 'undefined board name';

    unless ( $candidate->email() ) {
        unless ( lc $original_destination eq 'indeed' ) {
            if( my $cv_parsing_res = $user->parse_cv_with_locale( $content ) ) {
                $log->trace( "CV Parser said: ".Dumper( $cv_parsing_res ) );
                $candidate->enrich_with_cvparsing( $cv_parsing_res );
            }
        }
    }

    ## Make up an email address in case nothing is defined.
    unless( $candidate->email() ) {
        $candidate->make_email_up( $user );
    }

    # login provider here, cause this will depend on ripple settings
    $user->login_provider()->user_import_candidate(
        $user,
        {
            cv_mimetype => $content_type,
            cv_content => $content_b64,
            cv_filename => $content_filename
        },
        $candidate,
        {
            board_nice_name => $board_nice_name,
            %{$self->options},
        }
    );

    # TODO:SEAR-511 replace with identity_provider, cause that only depends on the nature
    # of the user.
    my $sent_email;
    if( my $save_email_settings = $user->login_provider()->get_save_email_settings($user) ){
        $log->info("Generating email for ".Dumper($save_email_settings));
        my $email = $self->generate_candidate_email({
            subject_template => $save_email_settings->{subject},
            cv => {
                content_type => $content_type,
                content_filename => $content_filename,
                content => $content
            }
        });
        $email->head()->replace( 'To' , $save_email_settings->{bcc} );
        $log->info("Sending email to '".$save_email_settings->{bcc}."'");
        {
            my $guard = Scope::Guard::scope_guard(
                sub{
                    # Clear all temp files that the email was built on.
                    $log->info("Purging email attached disk files");
                    $email->purge();
                });

            $sent_email = $self->stream2()->send_email($email);
            # The scope guard will run now regardless of any error.
        }
    }

    $self->stream2()->action_log->insert({
        action           => "import",
        candidate_id     => $self->candidate_object()->id,
        destination      => $original_destination,
        user_id          => $user->id,
        group_identity   => $user->group_identity,
        search_record_id => $self->candidate_object->search_record_id(),
        data             => {
            s3_log_id  => $self->log_chunk_id(),
            $self->get_mailjet_data( $sent_email ),
            $self->candidate_object()->action_hash()
        },
    });

    return { message => $self->stream2()->__('success') };
}

__PACKAGE__->meta->make_immutable();

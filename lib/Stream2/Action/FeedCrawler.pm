package Stream2::Action::FeedCrawler;

use Moose;
extends qw/Stream2::Action/;

=head1 Stream2::Action::FeedCrawler

Overview

=cut

use Stream::FeedCrawler::Crawler::Madgex;
use Stream::FeedCrawler::Crawler::Changeboard;

use Log::Any qw/$log/;

=head2 instance_perform

Method does what now?

=cut

sub instance_perform {
    my ( $self )  = @_;

    my $s2 = $self->stream2();

    my $crawler_config = $s2->config->{feed_crawler};

    my $crawler;

    foreach my $madgex_config (values %{$crawler_config->{groups}->{madgex}}) {
        $crawler = Stream::FeedCrawler::Crawler::Madgex->new(
            stream2 => $s2,
            %$madgex_config
        );
        $crawler->verify_remote();
    }

    $crawler = Stream::FeedCrawler::Crawler::Changeboard->new(
        stream2 => $s2,
        %{$crawler_config->{singles}->{changeboard}},
    );
    $crawler->verify_remote();

    return 1;
}

=pod

my @feeds = (


    {
        module   => 'propertyjobs.pm',
        board    => 'propertyjobs',
        base_url => 'https://recruiters.propertyjobs.co.uk',
        username => '',
        password => ''
    },
    {
        module   => 'cima.pm',
        board    => 'cima',
        base_url => 'https://cima-rs.madgexjb.com',
        username => '',
        password => ''
    },
    {
        module   => 'greatinsurancejobs.pm',
        board    => 'greatinsurancejobs',
        base_url => 'https://employers.greatinsurancejobs.com',
        username => '',
        password => ''
    },
    {
        module   => 'flightglobal.pm',
        board    => 'flightglobal',
        base_url => 'https://recruiters.flightglobal.com',
        username => '',
        password => ''
    },
    {
        module   => 'fish4jobs_search.pm',
        board    => 'fish4jobs_search',
        base_url => 'https://recruiter.fish4.co.uk',
        username => '',
        password => ''
    },
    {
        module   => 'taxationjobs.pm',
        board    => 'taxationjobs',
        base_url => 'https://taxation-jobs-rs.madgexjb.com',
        username => '',
        password => ''
    },
    {
        module   => 'newscientist.pm',
        board    => 'newscientist',
        base_url => 'https://recruiters.newscientist.com',
        username => '',
        password => ''
    },
    {
        module   => 'personneltoday.pm',
        board    => 'personneltoday',
        base_url => 'https://recruiters.personneltoday.com',
        username => '',
        password => ''
    },
    {
        module   => 'secsinthecity.pm',
        board    => 'secsinthecity',
        base_url => 'https://recruiter.secsinthecity.co.uk',
        username => '',
        password => ''
    },
    {
        module   => 'brandrepublic_new.pm',
        board    => 'brandrepublic_new',
        base_url => 'https://brandrepublic.haymarketrecruitment.com',
        username => '',
        password => ''
    },
    {
        module   => 'fish4jobs.pm',
        board    => 'fish4jobs',
        base_url => 'https://recruiter.fish4.co.uk',
        username => '',
        password => ''
    },
    {
        module   => 'onlymarketingjobs.pm',
        board    => 'onlymarketingjobs',
        base_url => 'https://onlymarketingjobs-rs.madgexjb.com',
        username => '',
        password => ''
    },
    {
        module   => 'jobstoday.pm',
        board    => 'jobstoday',
        base_url => 'https://recruiters.jobstoday.co.uk',
        username => '',
        password => ''
    },
    {
        module   => 'topengineeringjobs.pm',
        board    => 'topengineeringjobs',
        base_url => 'https://topengineeringjobs-rs.madgexjb.com',
        username => '',
        password => ''
    },
    {
        module   => 'farmersweekly.pm',
        board    => 'farmersweekly',
        base_url => 'https://recruiters.fwi.co.uk',
        username => '',
        password => ''
    },
    {
        module   => 'securityclearedjobs.pm',
        board    => 'securityclearedjobs',
        base_url => 'https://securityclearedjobs-rs.madgexjb.com',
        username => '',
        password => ''
    },
    {
        module   => 'accountancyage_jobs.pm',
        board    => 'accountancyage_jobs',
        base_url => 'https://accountancyagejobs-rs.madgexjb.com',
        username => '',
        password => ''
    },
    {
        module   => 'eventjobsearch.pm',
        board    => 'eventjobsearch',
        base_url => 'https://eventjobsearch.haymarketrecruitment.com',
        username => '',
        password => ''
    },
    {
        module   => 'jobsinrisk.pm',
        board    => 'jobsinrisk',
        base_url => 'https://jobsinrisk-rs.madgexjb.com',
        username => '',
        password => ''
    },
    {
        module   => 'communitycare.pm',
        board    => 'communitycare',
        base_url => 'https://recruiters.communitycare.co.uk',
        username => '',
        password => ''
    },
    {
        module   => 'guardian.pm',
        board    => 'guardian',
        base_url => 'https://www.guardianjobsrecruiter.co.uk',
        username => '',
        password => ''
    },
# Test module?
#    {
#        module   => 'secsinthecity_search.pm',
#        board    => 'secsinthecity_search',
#        base_url => 'http://secsinthecity-rs.madgexjbtest.com',
#        username => '',
#        password => ''
#    },
    {
        module   => 'execappointments_new.pm',
        board    => 'execappointments_new',
        base_url => 'https://recruiter.exec-appointments.com/',
        username => '',
        password => ''
    },
    {
        module   => 'execappointments.pm',
        board    => 'execappointments',
        base_url => 'https://recruiter.exec-appointments.com/',
        username => '',
        password => ''
    },
    {
        module   => 'jobsincredit.pm',
        board    => 'jobsincredit',
        base_url => 'https://jobsincredit-rs.madgexjb.com',
        username => '',
        password => ''
    },
    {
        module   => 'totallylegal.pm',
        board    => 'totallylegal',
        base_url => 'https://recruiter.totallylegal.com',
        username => '',
        password => ''
    },
# No longer a madgex site?
#    {
#        module   => 'theladders.pm',
#        board    => 'theladders',
#        base_url => 'https://recruit.theladders.co.uk',
#        username => '',
#        password => ''
#    },
    {
        module   => 'gaapweb.pm',
        board    => 'gaapweb',
        base_url => 'https://recruiter.gaapweb.com',
        username => '',
        password => ''
    },
    {
        module   => 'drapers.pm',
        board    => 'drapers',
        base_url => 'https://rs.drapersjobs.com',
        username => '',
        password => ''
    },
    {
        module   => 'hairdressersjournal.pm',
        board    => 'hairdressersjournal',
        base_url => 'https://recruiters.hji.co.uk',
        username => '',
        password => ''
    },
    {
        module   => 'prweek.pm',
        board    => 'prweek',
        base_url => 'https://prweekjobs.haymarketrecruitment.com',
        username => '',
        password => ''
    },
    {
        module   => 'careersinconstruction.pm',
        board    => 'careersinconstruction',
        base_url => 'https://rs.careersinconstruction.com',
        username => '',
        password => ''
    },
    {
        module   => 'icaew.pm',
        board    => 'icaew',
        base_url => 'https://recruiters.icaewjobs.com',
        username => '',
        password => ''
    },
    {
        module   => 'supplychainonline.pm',
        board    => 'supplychainonline',
        base_url => 'https://supplychainonline-rs.madgexjb.com',
        username => '',
        password => ''
    },
    {
        module   => 'theengineerjobs.pm',
        board    => 'theengineerjobs',
        base_url => 'https://recruiters.theengineerjobs.co.uk',
        username => '',
        password => ''
    },
    {
        module   => 'legalweek.pm',
        board    => 'legalweek',
        base_url => 'https://legalweekjobs-rs.madgexjb.com',
        username => '',
        password => ''
    },
);

=cut

__PACKAGE__->meta->make_immutable();

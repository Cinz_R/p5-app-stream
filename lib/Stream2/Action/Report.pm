package Stream2::Action::Report;

use Data::Dumper;
use Log::Any qw/ $log /;

sub perform {
    eval {
        my ( $class, $job ) = @_;
        my $stream2 = $job->{stream2} or die 'stream2 not found';

        $log->warn( 'Incrementing with the following data:' );
        $log->warn( Dumper $job->parameters );
        $stream2->stream_api->increment_analytics( %{ $job->parameters } );
        $log->warn( 'Increment complete' );
    };
    $log->error( "Error: $@" ) if $@;
}

1;

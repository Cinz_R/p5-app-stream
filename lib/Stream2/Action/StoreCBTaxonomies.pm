package Stream2::Action::StoreCBTaxonomies;

use Moose;
extends qw/Stream2::Action/;

=head1 Stream2::Action::StoreCBTaxonomies

Downloads taxonomies from Careerbuilders Taxonomy endpoint and inserts/updates them in the taxonomy table.

Docs: https://github.com/careerbuilder/DataScienceAPIDocumentation/blob/master/TaxonomyService.md

=cut

use Bean::OAuth::CareerBuilder;
use JSON;
use Log::Any qw/$log/;

=head2 taxonomy_url

Careerbuilder taxonomy api url

=cut

has taxonomy_url => (
    is => 'ro',
    default => 'https://api.careerbuilder.com/core/taxonomy/'
);

=head2 cb_oauth

An HTTP client to request auth tokens for connecting to careerbuilders api

=cut

has cb_oauth => (
    is => 'ro',
    lazy_build => 1,
);

sub _build_cb_oauth {
    my $self = shift;
    my $config = $self->stream2()->config();
    return Bean::OAuth::CareerBuilder->new({
        client_id => $config->{feeds}->{recruitmentedge}->{oauth}->{client_id},
        client_secret => $config->{feeds}->{recruitmentedge}->{oauth}->{client_secret}
    });
}

=head2 instance_perform

Gets the taxonomies from careerbuilders api and stores in the database. Returns a hashref
of what the results were for each taxonomy storage.

=cut

sub instance_perform {
    my ( $self )  = @_;

    my $s2 = $self->stream2();
    my $factory = $s2->factory('Taxonomy');

    my $taxonomy_results;

    my ($auth_token, $expires) = eval { $self->cb_oauth->get_token(); };
    if ( $@ ) {
        $log->warn("Error receiving auth token to store taxonomies: $@");
        return { error => 'Cannot store taxonomies due to failure receiving auth token' };
    }

    foreach my $taxonomy (qw/skillsv4 carotenev3 onet17/) {
        my $tax_ref = $self->get_taxonomy($taxonomy, $auth_token) or next;
        my $taxonomies = $self->prepare_for_insert($taxonomy, $tax_ref);
        my $result = $factory->store_taxonomies($taxonomies);
        $taxonomy_results->{$taxonomy} = $result if $result;
    }

    return $taxonomy_results;
}

=head2 get_taxonomy(Str $type, Str $auth_token)

Queries careerbuilder api for the given taxonomy and returns the JSON data.

=cut

sub get_taxonomy {
    my ($self, $type, $auth_token) = @_;

    my $s2 = $self->stream2();

    my $url = $self->taxonomy_url . lc $type;
    my $response = $s2->user_agent->get($url, Authorization => 'Bearer ' . $auth_token);

    my $json = JSON->new()->utf8(1);
    my $json_ref = eval { $json->decode($response->content); };
    if ($@) {
        $log->warn("Failed to decode json for CB taxonomy '$type': $@");
        return;
    }

    return $json_ref->{data};
}

=head2 prepare_for_insert(Str $type, ArrayRef $data)

Converts the taxonomy data received into an array of hashes keyed by the taxonomy tables columns
to conveniently insert them into the database.

=cut

sub prepare_for_insert {
    my ($self, $type, $data) = @_;
    my @taxonomies;
    while ( my ($lang, $elements) = each %$data ) {
        foreach my $element ( @$elements ) {
            push @taxonomies, {
                field_type => $type,
                lang => $lang,
                taxonomy_id => $element->{id},
                label => $element->{normalized_name} // $element->{title}
            };
        }
    }
    return \@taxonomies;
}

__PACKAGE__->meta->make_immutable();

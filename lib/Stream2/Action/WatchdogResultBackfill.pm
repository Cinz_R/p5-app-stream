package Stream2::Action::WatchdogResultBackfill;

use strict;
use warnings;

use Log::Any qw/$log/;

=head1 DESCRIPTION

This action will be spawned to backfill some empty watchdog results from V1.

=cut

sub perform {
    my ( $class, $job ) = @_;
    my $s2 = $job->{stream2};

    my $backfill_from = $job->parameters->{'backfill_from'};

    my $wr_factory = $s2->factory('WatchdogResult');

    my $backfill_result = $wr_factory->find({
                                             watchdog_id => $backfill_from->[0],
                                             subscription_id => $backfill_from->[1],
                                             candidate_id => $backfill_from->[2],
                                            });
    $wr_factory->backfill_from($backfill_result);

    return 1;
}

1;

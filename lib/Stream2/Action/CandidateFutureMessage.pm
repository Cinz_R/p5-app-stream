package Stream2::Action::CandidateFutureMessage;

use Moose;
use Log::Any qw/$log/;

extends qw/Stream2::Action::CandidateFuture/;

=head1 NAME

Stream2::Action::CandidateFutureMessage - An action to register a template ID to be sent in the future.

=cut

has 'message_template_id' => ( is => 'ro', isa => 'Str', required => 1 );
has 'message_template' => ( is => 'ro', isa => 'Stream2::O::EmailTemplate', lazy_build => 1 );

sub _build_message_template{
    my ($self) = @_;
    return $self->stream2()->factory('EmailTemplate')->search({ group_identity => $self->user_object()->group_identity(),
                                                                id => $self->message_template_id()
                                                            })->first();
}

sub instance_perform {
    my ( $self ) = @_;

    $log->info("Recording the future message action against this candidate");
    # Log the action
    $self->stream2()->action_log->insert({
        action          => "candidate_future_message",
        candidate_id    => $self->candidate_object()->id,
        destination     => $self->candidate_object()->destination(),
        user_id         => $self->user_object->id,
        group_identity  => $self->user_object->group_identity,
        search_record_id => $self->candidate_object->search_record_id(),
        data            => {
            s3_log_id => $self->log_chunk_id(),
            longstep_process_id => $self->longstep_process_id(),
            at_time => $self->at_time(),
            template_name => $self->message_template()->name(),
        },
    });

    return 1;
}

__PACKAGE__->meta->make_immutable();

package Stream2::Action::DownloadCV;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;
use MIME::Base64 qw/encode_base64/;
use Data::Dumper;

with qw/
           Stream2::Role::Action::InManageError
           Stream2::Role::Action::HasUser
           Stream2::Role::Action::HasRemoteIP
       /;

with qw/
           Stream2::Role::Action::TriggerMacros

           Stream2::Role::Action::HasCandidate

           Stream2::Role::Action::FastJob
           Stream2::Role::Action::InLogChunk
       /;

has [ '+on_managed_error', '+on_not_managed_error' ] => ( default => sub {
    my $self = shift;
    return sub {
        $self->stream2->increment_analytics(
            user_id => $self->user_object->provider_id,
            board => $self->candidate_object->destination,
            cv_attempts => 1,
            cv_failed => 1,
        );
        return $self->stream2()->action_log->insert({
            action          => "download_fail",
            candidate_id    => $self->candidate_object()->id(),
            destination     => $self->candidate_object()->destination(),
            user_id         => $self->user_object->id,
            group_identity  => $self->user_object->group_identity,
            search_record_id => $self->candidate_object->search_record_id(),
            data            => { s3_log_id => $self->log_chunk_id(),
                                 $self->candidate_object()->action_hash(),
                             },
        });
    }
});

has 'process_response' => (
    is      => 'ro',
    isa     => 'CodeRef',
    default => sub {
        sub {
            encode_base64( shift->as_string("\n") );
          }
    }
);

=head2 instance_perform

Entry point for downloading candidate CV

Complete the given job if run as an async task and return 1.

If run in pure functional mode, returns an L<HTTP::Response> containing the CV as binary content,
and various headers.

=cut

sub instance_perform {
    my ( $self, $job ) = @_;

    $log->info("Downloding CV for candidate ".$self->candidate_object()->id()." for user ".$self->user_object()->id());

    my $original_destination = $self->candidate_object()->destination();

    # Check for candidate in internal database
    if ( my $internal_params = $self->_cv_from_internal() ){
        $log->info( sprintf('CV is available via an internal database: %s, ID: %s', $internal_params->{destination}, $internal_params->{candidate_id}) );
        # using the set_attr methods wraps stuff in array refs and stuff so avoid
        map { $self->candidate_object->{$_} = $internal_params->{$_} } keys %$internal_params;
    }

    my $memory_cache_key = __PACKAGE__.'-'.$self->candidate_object()->id().'-'.$self->user_object()->id();
    $log->info("Looking for memory cache key = ".$memory_cache_key );
    my $candidate_response = $self->instance_memory_cache()->{ $memory_cache_key };
    if( $candidate_response ){
        $log->info("Memory Cache hit! Not going to do any more download");
        return $self->process_response()->( $candidate_response );
    }

    $log->debug("Memory Cache miss. Will download the CV");

    $candidate_response = $self->instance_memory_cache()->{ $memory_cache_key }
        = $self->candidate_object->download_cv(
            $self,
            $self->user_object,
            {
                cb_oneiam_auth_code => $self->cb_oneiam_auth_code(),
                browser_tag => $self->browser_tag()
            }
        );

    if( my $candidate_email = $candidate_response->header('X-Candidate-Email') ){
        $log->info("CV download also returned a candidate email = '$candidate_email' . Setting it against the candidate");
        $self->candidate_object()->email($candidate_email);
    }

    $self->stream2->increment_analytics(
        user_id => $self->user_object->provider_id,
        board => $self->candidate_object->destination,
        cv_attempts => 1,
        cv_success => 1,
    );

    # Log the action
    $self->stream2()->action_log->insert({
        action          => "download",
        candidate_id    => $self->candidate_object()->id,
        destination     => $original_destination,
        user_id         => $self->user_object->id,
        group_identity  => $self->user_object->group_identity,
        search_record_id => $self->candidate_object->search_record_id(),
        data            => { s3_log_id => $self->log_chunk_id(),
                             $self->candidate_object()->action_hash()
                         },
    });

    $log->info("Done downloading CV for candidate ".$self->candidate_object()->id());

    return  $self->process_response()->( $candidate_response );
}

=head2 _cv_from_internal

    Given a candidate object, check for any history of importing to an internal database.
    If the feature is switched on and the candidate has been imported within the given timeframe,
    return the corresponding ID for the candidate in that internal database.

    IN:
        my $cand_ref = $me->_cv_from_internal( $job_params, $candidate, $user );
    OUT:
        { candidate_id => xxxx, destination => <internal_database_template_name> }

=cut

sub _cv_from_internal {
    my ( $self ) = @_;

    $log->debug('Checking for use of internal CV');
    my $days_to_serve = $self->user_object->settings_values_hash()->{'behaviour-downloadcv-use_internal'};

    unless( $days_to_serve ) {
        $log->debug("User has not got internal CV feature turned on");
        return;
    }

    $log->debug(sprintf('Internal CVs are eligable for %s days', $days_to_serve));
    my $date = DateTime->now()->subtract( days => $days_to_serve );

    my $schema = $self->stream2->stream_schema;
    my $x_days_limit = $schema->with_sqllite(sub{
                                                       return sprintf("date('now', '-%d day')", $days_to_serve);
                                                   },
                                                   sub{
                                                       return sprintf('DATE_SUB(NOW(), INTERVAL %d DAY )', $days_to_serve);
                                                   });

    my $mapping = $schema->resultset('CandidateInternalMapping')->search({
        group_identity  => $self->user_object->group_identity,
        candidate_id    => $self->candidate_object->id,
        insert_datetime => { '>=' => \$x_days_limit }
    })->first();

    unless ( $mapping ) {
        $log->debug("This Candidate's CV has not been saved before");
        return;
    }

    if ( ! $mapping->completed && ! $self->_fetch_import_status( $mapping ) ){
        # for now this will only involve the tsimport api
        $log->debug("This Candidate's CV has been saved but is not yet ready for download");
        return;
    }

    if ( my $internal_id = $mapping->internal_id ){
        # tsimport API doesn't use the name of the template to differentiate so we have to have this mapping
        my $destination = $self->stream2()->factory('Board')->internal_backend_to_destination( $mapping->backend() );
        $log->info("Candidate is Destination:$destination, candidate_id: $internal_id");
        return { destination => $destination, candidate_id => $internal_id };
    }

    $log->debug( sprintf("There is no internal_id for mapping: %s", $mapping->id) );
    return;
}

sub _fetch_import_status {
    my ( $self, $mapping ) = @_;
    $log->debug('fetching import status from TS import API');
    my $status_ref = $self->stream2->tsimport_api->import_status( $mapping->import_id );
    $log->debug("Got from tsimport API: ".Dumper($status_ref) );
    if ( $status_ref->{status}->{completed} && ( my $internal_id = $status_ref->{status}->{result} ) ){
        $log->debug('TS import has since completed');
        $mapping->update({ internal_id => $internal_id });
        return $internal_id;
    }
    $log->debug('TS import has not completed');
}

__PACKAGE__->meta->make_immutable();

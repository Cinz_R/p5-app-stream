package Stream2::Action::SearchBase;

=head1 NAME

Stream2::Action::SearchBase

=head1 DESCRIPTION

The bedrock of any good search related action, doesn't
just take care of the result collection but also the
reporting of searches being ran

=cut

use Moose;
extends qw/Stream2::Action/;

use Stream2::Criteria;
use Stream2::Results::Store;

use Data::Dumper;
use Log::Any qw/$log/;

has 'template_name'     => ( is => 'ro', isa => 'Str', required => 1);
has 'criteria_id'       => ( is => 'ro', isa => 'Str', required => 1);
has 'common_search_log' => ( is => 'ro', isa => 'HashRef', lazy_build => 1 );

sub _build_common_search_log{
    my ($self) = @_;
    return {
        search_record_id => $self->search_record->id(),
        s3_log_id        => $self->jobid(),
        criteria_id      => $self->criteria_id(),
        stream2_base_url => $self->stream2_base_url(),
        destination      => $self->template_name(),
        company          => $self->user_object->group_identity(),
        user_provider    => $self->user_object->provider(),
        user_provider_id => $self->user_object->provider_id(),
        user_stream2_id  => $self->user_object->id(),
        remote_ip        => $self->remote_ip(),
    };
}

has 'search_record_data' => ( is => 'ro', isa => 'HashRef', lazy_build => 1 );

sub _build_search_record_data {
    my ( $self ) = @_;
    return {
        s3_log_id => $self->log_chunk_id
    };
}

has 'criteria_tokens' => ( is => 'ro', isa => 'HashRef', lazy_build => 1 );

sub _build_criteria_tokens {
    my ( $self ) = @_;
    my %criteria_tokens = $self->criteria->tokens;

    $log->trace("Setting criteria tokens ".Dumper(\%criteria_tokens)) if $log->is_trace();

    # Remove OFCCP field according to user setting
    if ( $criteria_tokens{ofccp_context}
    && ! $self->user_object->settings_values_hash->{'behaviour-search-use_ofccp_field'} ) {
        $log->infof(
            'Removing ofccp_context "%s" from query due to user not having the setting turned on',
            $criteria_tokens{ofccp_context}
        );
        delete $criteria_tokens{ofccp_context};
    }

    return \%criteria_tokens;
}


has 'start_epoch' => ( is => 'ro', isa => 'Int' , default => sub{ scalar(time()); } );

with qw/
    Stream2::Role::Action::HasUser
    Stream2::Role::Action::HasFeed
/;

has 'search_record' => ( is => 'ro', lazy_build => 1 );

sub _build_search_record {
    my ( $self ) = @_;
    my $search_record = $self->stream2->stream_schema()->resultset('SearchRecord')->create({
        criteria_id     => $self->criteria->id,
        user_id         => $self->user_object->id,
        board           => $self->template_name,
        remote_ip       => $self->remote_ip(),
    });
    $log->info("Created search record id=".$search_record->id());
    return $search_record;
}

has 'criteria' => ( is => 'ro', lazy_build => 1 );

sub _build_criteria {
    my ( $self ) = @_;
    my $criteria = Stream2::Criteria->new( id => $self->criteria_id(), schema => $self->stream2()->stream_schema() );
    $criteria->load();
    return $criteria;
}

has 'results_collector' => ( is => 'ro', lazy_build => 1 );

sub _build_results_collector {
    my ( $self ) = @_;
    return Stream2::Results::Store->new({
        id                  => $self->jobid(),
        search_record_id    => $self->search_record->id(),
        destination         => $self->template_name(),
        store               => $self->stream2()->redis(),
        expires_after       => $self->stream2()->config->{results_expire_after} || '3h',
    });
}

=head2 before_search

This method in it's present state simply records
the beginings of a search, override for any further
pre-search requirements

=cut

sub before_search {
    my ( $self ) = @_;
    #########################################################################################
    # PHASE 1 - Record the birth of a new search                                            #
    # Before we do anything dangerous, lets at least record the fact we have begun a search #
    #########################################################################################
    $self->start_epoch();
    $self->search_record();
}

=head2 run_search

Uses the available feed and criteria and will return
a Stream2::Results::Store object containing the
found results

=cut

sub run_search {
    my ( $self ) = @_;

    my $results = $self->results_collector;

    #################################################################################################
    # PHASE 2 - The search proper                                                                   #
    # This is the danger zone, if this fails the rest of this sub will not be completed.            #
    # Instead we will be dropped into one of the above "on_managed_error" or "on_not_managed_error" #
    # routines which *hopefully* will record the failure of the task                                #
    #################################################################################################
    $log->info("Base Stream2 URL is ".$self->stream2_base_url());
    $log->infof("Re-run this search at %ssearch/%s/%s", $self->stream2_base_url, $self->criteria_id, $self->template_name);
    $log->infof("Search runner: %s:%s", $self->user_object->provider(), $self->user_object->provider_id() );

    my $feed = $self->feed();
    $self->_set_feed_tokens();

    my $search_options = {};
    if ( my $page = $self->options->{page} ){
        $search_options->{page} = $page;
        $search_options->{results_per_scrape} = $self->options->{results_per_scrape};
        $results->current_page( $page );
    }

    if ( my $results_per_page = $self->options->{results_per_page} ){
        $log->tracef( 'Setting results_per_page to %s', $results_per_page );
        $feed->token_value( results_per_page => $results_per_page );
    }

    # Give feed the criteria object
    $feed->criteria($self->criteria);

    # Inject the results container in the feed.
    $feed->results($results);

    $feed->run_helpers();

    # This is the search entry point.
    $feed->quota_guard( [ 'search' ], sub{
                           $feed->search($search_options);
                       });

    return $results;
}

=head2 after_search

This should be called/overriden as a method to be
ran after a search has been ran regardless of it's outcome
Currently it will update all of our records ( error calendar, stats etc )
with the outcome of the search

=cut

sub after_search {
    my ( $self, $results ) = @_;
    ####################################################################################
    # PHASE 3 - Record the success                                                     #
    # When a search has completed, results or no, we count it as being a success.      #
    # This fact should be shared with our own DB, our current reporting DB ( MongoDB ) #
    # and with our "error calendar" which records searching stats for internal stats.  #
    ####################################################################################
    $self->report_success( $results );
    $log->info("Search was successful");
}

=head2 report_success

After a search has finished we should update our records of the fact.
This includes incrementing statistics and saving the log record for internal
use.

=cut

sub report_success {
    my ( $self, $results ) = @_;

    my $search_record = $self->search_record();

    # our own internal DB
    {
        $search_record->n_results( $results->total_results() );
        $search_record->data( $self->search_record_data );
        $search_record->update();
    }

    # Our error calendar
    # Log this search in the error calendar.
    $self->stream2()->log_search_calendar({
        n_results           => $results->total_results(),
        duration            => time() - $self->start_epoch,
        %{$self->common_search_log()}
    });
    $log->info( "Have sent success to error calendar" );

    # For use with the "candidate grinder" or candr, this has never been used 28/03/2017
    if ( my $candidate_action = $self->options->{candidate_action} ) {
        # this is for JSON ripple requests to apply follow up actions to any found
        # candidates such as importing them etc
        $self->_follow_up_action( $results, $candidate_action );
    }
}

sub _follow_up_action {
    my ( $self, $results, $candidate_action ) = @_;
    $log->infof( 'Have candidate action %s', Dumper( $candidate_action ) );
    foreach my $result ( $results->fetch_results ) {
        my $job = $self->stream2->qurious->create_job(
            class => $candidate_action->{class},
            queue => "BackgroundJob",
            parameters => {
                results_id      => $results->id,
                candidate_idx   => $result->idx,
                user            => $self->user,
                remote_ip       => $self->remote_ip(),
                options         => $candidate_action->{options},
            }
        );
        $job->enqueue;
        $log->infof( 'Queued action %s for candidate_id: %s, job_id: %s', $candidate_action->{class}, $result->id, $job->guid );
    }
}

sub _set_feed_tokens {
    my ( $self ) = @_;

    my $tokens = $self->criteria_tokens();

    # A token comming from some interactive parameters should NEVER
    # override a subscription token coming from the user.
    # This is a protection against authentication tokens injection.
    my $auth_tokens = $self->feed_subscription_info()->{auth_tokens} // {};

    while ( my ($token, $value) = each %$tokens ) {
        if( $auth_tokens->{$token} ){
            $log->warn("Attempt to inject the authentication token '$token' as a parameter. SKIPPING");
            next;
        }
        if($token eq "jobtype") {
            $log->error("Invalid token 'jobtype' found with value: ". Dumper($value));
        }
        $self->feed()->token_value( $token => $value );
    }

    my @missing_authtokens;
    foreach my $authtoken ( values %{ $self->feed->authtokens } ) {
        my $name = $authtoken->{Name};
        next unless $authtoken->{Mandatory};
        my $value = $self->feed->token_value($name);
        next if $value;
        push @missing_authtokens, $authtoken->{Label};

    }
    if(@missing_authtokens) {
        die Stream::EngineException::LoginError->new({
            message => 'Missing subscription value for ' . join(', ', @missing_authtokens)
        });
    };
}

sub report_error {
    my ($self, $type, $message ) = @_;
    $self->search_record->data({
        %{$self->search_record_data},
        error_type  => $type
    });
    $self->search_record->update();

    $self->stream2->log_search_calendar({
        error_type => $type,
        duration => time() - $self->start_epoch(),
        %{ $self->common_search_log() }
    });
}

__PACKAGE__->meta->make_immutable();

1;

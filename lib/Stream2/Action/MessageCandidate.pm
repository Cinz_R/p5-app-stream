package Stream2::Action::MessageCandidate;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;
use Digest::SHA1;
use Email::Address;

with qw/
           Stream2::Role::Action::InManageError
           Stream2::Role::Action::Email
           Stream2::Role::Action::HasUser
           Stream2::Role::Action::HasAdvert
       /;

has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

with qw/
           Stream2::Role::Action::HasFeed
           Stream2::Role::Action::DownloadCV
       /;

with qw/
           Stream2::Role::Action::TriggerMacros

           Stream2::Role::Action::HasCandidate

           Stream2::Role::Action::FastJob
           Stream2::Role::Action::InLogChunk
       /;

has [ '+on_managed_error', '+on_not_managed_error' ] => (
    default => sub {
        my $self = shift;
        return sub {
            $self->stream2()->action_log->insert(
                {   action           => "message_fail",
                    candidate_id     => $self->candidate_object()->id(),
                    destination      => $self->candidate_object()->destination(),
                    user_id          => $self->user_object->id,
                    group_identity   => $self->user_object->group_identity,
                    search_record_id => $self->candidate_object->search_record_id(),
                    data             => {
                        s3_log_id => $self->log_chunk_id(),
                        $self->email_template_name ? ( email_template_name => $self->email_template_name ) : (),
                        $self->email_template_id   ? ( email_template_id   => $self->email_template_id ) : (),
                        $self->candidate_object()->action_hash(),
                    },
                }
            );
        }
    }
);

use Data::Dumper;
use DateTime;
use Encode;
use HTML::FormatText;
use String::Truncate;
use Stream::EngineException;
use Scalar::Util qw(blessed);
use URI;

has 'message_email' => ( is => 'ro', isa => 'Str', required => 1 );
has 'subject_prefix' => ( is => 'ro', isa => 'Str', required => 1 );
has 'message_subject' => ( is => 'ro', isa => 'Str', required => 1 );
has 'message_bcc' => ( is => 'ro', isa => 'Str', required => 1 );
has 'message_content' => ( is => 'ro', isa => 'Str', required => 1 );
has 'email_template_name' => ( is => 'ro', isa => 'Maybe[Str]', required => 0 );
has 'email_template_id' => ( is => 'ro', isa => 'Maybe[Int]', required => 0 );
has 'message_action_log_name' => ( is => 'ro', isa => 'Str', default => 'message' );

around 'build_context' => sub{
    my ($orig, $self) = @_;
    return {
        %{ $self->$orig() },
        # Context only contains stuff that dont need to be passed by
        # a composing function
        message_email => $self->message_email(),
        subject_prefix => $self->subject_prefix(),
        message_action_log_name => $self->message_action_log_name(),
    };
};


=head2 instance_perform

As the user, message the given candidate, possibly with the given advert

=cut

sub instance_perform {
    my ( $self ) = @_;

    my $s2 = $self->stream2();
    my $candidate = $self->candidate_object();
    my $user = $self->user_object();
    my $stream2 = $self->stream2();

    unless( $candidate->email() ){
        # Candidate has no email. Time to download CV and
        # parse for email. Note that the download would also
        # potentially set the email via the magic X-Candidate-Email header.
        my $cv_parsing_res = $user->parse_cv_with_locale($self->download_cv_file()->binary_content());
        if( $cv_parsing_res ){
            $log->trace("CV Parser said: ".Dumper($cv_parsing_res));
            $candidate->enrich_with_cvparsing($cv_parsing_res);
        }
    }
    unless ( $candidate->email() ) {
        die Stream::EngineException::MessagingError->new( { message => $stream2->__("Failed to extract this candidate email address") } );
    }

    my @previous_candidate_actions = $self->stream2()->factory('CandidateActionLog')->search({
        group_identity  => $user->group_identity(),
        candidate_id    => $candidate->id(),
        '-or'           => [ action   => 'message', action => 'message_auto' ],
    })->all;

    if ( $self->email_template_id &&
             # If this resending is disabled, we need to do the check.
             ! $user->settings_values_hash()->{'candidate-actions-message-template-resend-enabled'}
         ){
        foreach my $action (@previous_candidate_actions) {
            if ( $action->data->{email_template_id} && $action->data->{email_template_id} == $self->email_template_id ) {
                die Stream::EngineException::MessagingError->new(
                    { message => $stream2->__("You have already emailed this template to this candidate") } );
            }
        }
    }

    $log->info("Candidate has got an email address. Building message and sending it");
    my $email = $candidate->build_target_entity();

    # token replacement, only advert_link to begin with
    # We always need the advert, no matter what (to generate the aplitrak FROM)
    my $advert;

    if( $self->advert_id ){
        # Keep in mind the adverts can come from the original user.
        $advert = $self->get_advert();
    }

    $email->head->replace( 'Reply-To' => $self->message_email());
    $email->head->replace( From => 'noreply@broadbean.net'); # Sender is always noreply@broadbean.net. The Reply-To makes the reply work.

    # tokens that will be used to process strings with placeholders
    my $email_stash = $self->get_template_stash($advert);

    my $interactive_subject = $stream2->templates()->render_simple($self->message_subject, $email_stash);
    $log->debug("Interactive subject=$interactive_subject");
    # Truncate interactive subject to 80 characters, to be inline with the interface.
    $interactive_subject = String::Truncate::elide( $interactive_subject, 80 );

    my $email_subject = $self->subject_prefix . ' ' . $interactive_subject;
    $log->info("Will send the following email subject: $email_subject");

    $email->head->replace( Subject => Encode::encode('MIME-Q', $email_subject) );
    $email->head->replace( Bcc => $self->message_bcc );

    $email->attach(
        Type => 'text/html; charset=UTF-8',
        Encoding => 'quoted-printable',
        Disposition => 'inline',
        Data => [ Encode::encode( 'UTF-8', $self->message_content ) ]
    );

    my $s2_mime = $stream2->factory('Email')->create($email);
    eval{
        $s2_mime->process_with_stash( $email_stash );
    };
    if( my $err = $@ ){
        die  Stream::EngineException::MessagingError->new({ message => $stream2->__('Invalid email format: ').$err });
    }

    # check to see if the final reply-to header is a valid email addresss
    unless( scalar( Email::Address->parse( $s2_mime->head->get('Reply-To') ) ) ){
        die  Stream::EngineException::MessagingError->new({ message => $stream2->__("Invalid 'Reply To' email address") });
    }

    # store entire message (headers and body) before image inlining occurs
    my $entire_message = $s2_mime->stringify;

    # resolve images and embed them for maximum viewing pleasure
    $s2_mime = $s2_mime->resolve_images(sub {
                                            my ($url) = @_;
                                            my $onlineimage = $stream2->factory('OnlineFile')->find($url);
                                            $log->info("Local file: ".$onlineimage->disk_file());
                                            return {
                                                local_file => $onlineimage->disk_file(),
                                                mime_type  => $onlineimage->mime_type()
                                            };
                                        });

    my $uuid = $stream2->uuid->create_str;
    my $unsubscribe_url = URI->new($self->stream2_base_url() . 'external/unsubscribe/' . $uuid  )->as_string();
    $email->head()->add('List-unsubscribe' ,  "<$unsubscribe_url/auto>" );

    $log->info("Unsubscribe URL: ".$unsubscribe_url);
    my $unsubscribe_footer = $stream2->templates()->render('stream/emails/unsubscribe.tt',
                                                           { unsubscribe_url => $unsubscribe_url });
    $s2_mime->append_html($unsubscribe_footer);

    my $sent_email = eval {
        $stream2->send_email($s2_mime->entity());
    };

    if ( $@ ){
        my $error = $@;
        if ( $@ =~ m/blacklisted/i ){ #im sorry jesus
            $error = $stream2->__("This candidate does not wish to be contacted.");
        }
        # $error could be either an string, or an Stream::EngineException object
        $error = $error->message if blessed $error && $error->isa('Stream::EngineException');
        die Stream::EngineException::MessagingError->new({
            message => $stream2->__("Unable to email candidate") . ": " . $error
        });
    }

    # This is in case we need it in the future. The potential use case is:
    #     People have a template with just the company branding
    #     They use this template to communicate with candidate, writing a specific 'body' each time.
    # -> In this case, the constraint to forbid reusing the same template is too strong. Because you'll prevent people from having 'brand templates' that they can modify interactively.
    # Storing the signature of the arguments allows to cover the cases where the same template but with no interactive modification is re-used, and to allow re-use of 'brand only' templates.

    my $signature_hash = Digest::SHA1::sha1_base64( Encode::encode_utf8( $self->message_content()),
                                                    Encode::encode_utf8( $self->message_subject()) );

    # Use the template to mark the candidate as contacted (this might not do anything)
    $self->feed->mark_candidate_contacted($candidate);

    my $candidate_action_log = $self->stream2()->action_log->insert(
        {   action           => $self->message_action_log_name(),
            candidate_id     => $self->candidate_object()->id(),
            destination      => $self->candidate_object()->destination(),
            user_id          => $self->user_object()->id(),
            group_identity   => $self->user_object()->group_identity(),
            search_record_id => $self->candidate_object->search_record_id(),
            data             => {
                s3_log_id => $self->log_chunk_id(),
                $self->email_template_name ? ( email_template_name => $self->email_template_name ) : (),
                $self->email_template_id   ? ( email_template_id   => $self->email_template_id )   : (),
                $self->get_mailjet_data( $sent_email ),
                $self->candidate_object()->action_hash(),
                signature => $signature_hash,
            }
        }
    );

    $log->debug(sprintf("Message candidate: %s, by user: %s",$candidate->id,$user->id));

    # upload message body to S3, and store its metadata in DB
    my $private_files = $stream2->factory('PrivateFile');
    my $s3_object;
    eval {
        $s3_object = $private_files->create( {
            content_type => 'text/html',
            content      => $entire_message,
            uuid         => $uuid,
        } ) or confess 'Unable to store message body';

        my $plain_body 
            = HTML::FormatText->format_string( $s2_mime->one_html_body );
        $stream2->factory('SentEmail')->create( {
            user_id        => $user->id,
            action_log_id  => $candidate_action_log->id,
            sent_date      => DateTime->now,
            subject        => $email_subject,
            recipient      => $candidate->email,
            recipient_name => $candidate->name // '',
            aws_s3_key     => $s3_object->key,
            plain_body     => $plain_body,
        } );
    };
    if ( my $error = $@ ) {
        die Stream::EngineException::MessagingError->new({ message =>
                                                               $stream2->__("Unable to store email message")
                                                               . ": $error"
                                                           });
    }
    $log->info("Done sending email");

    return { message => $stream2->__('Success') };
}



__PACKAGE__->meta->make_immutable();

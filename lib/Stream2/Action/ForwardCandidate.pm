package Stream2::Action::ForwardCandidate;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;

with qw/
           Stream2::Role::Action::InManageError
           Stream2::Role::Action::Email
           Stream2::Role::Action::HasUser
           Stream2::Role::Action::HasAdvert
           Stream2::Role::Action::HasRemoteIP
       /;


with qw/
           Stream2::Role::Action::TriggerMacros

           Stream2::Role::Action::HasCandidate

           Stream2::Role::Action::FastJob
           Stream2::Role::Action::InLogChunk
       /;

with qw/Stream2::Role::Action::DownloadCV/;

has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

with qw/Stream2::Role::Action::HasFeed/;


use Bean::Locale;

use Stream::EngineException;
use Stream2::Action::DownloadProfile;

use Stream2::O::File;

use Encode;
use HTML::FormatText;
use HTML::Entities;

use DateTime;

has 'recipients_emails' => ( is => 'ro', isa => 'ArrayRef', required => 1);
has 'recipients_ids'    => ( is => 'ro', isa => 'ArrayRef', required => 1);

has 'message' => ( is => 'ro', isa => 'Maybe[Str]', required => 1);

has [ '+on_managed_error', '+on_not_managed_error' ] => ( default => sub {
    my $self = shift;
    return sub {
        $self->stream2()->action_log->insert({
            action          => "forward_fail",
            candidate_id    => $self->candidate_object()->id(),
            destination     => $self->candidate_object()->destination(),
            user_id         => $self->user_object->id,
            group_identity  => $self->user_object->group_identity,
            search_record_id => $self->candidate_object->search_record_id(),
            data            => { s3_log_id => $self->log_chunk_id(),
                                 $self->candidate_object()->action_hash()
                             },
        });
    }
});

=head2 instance_perform

Downloads the CV or the profile and forward it to the given list of recipients

if the candidate has a CV -> send that

else if the candidate has a profile -> send that
else -> send the result body

=cut

sub instance_perform {
    my ( $self ) = @_;

    my $s2 = $self->stream2();

    my @warnings = ();

    my @recipients =  $s2->factory('SearchUser')->search({id => {-in => $self->recipients_ids()}})->all();

    # Destination can be altered by the download CV
    # but we need to original most of the time
    my $original_destination = $self->candidate_object()->destination();

    # SEAR-1086 A user with a CV-library subscription should only be able to forward a Candidate to another user only if they also
    # have a CV-library subscription.
    if($original_destination eq 'cvlibrary'){
        my @bad_recipients;
        @recipients = grep {$_->has_subscription($original_destination) || ( ( push(@bad_recipients, $_) ) && 0) }  @recipients;
        @warnings = map {$s2->__x("{user_name} doesn't have a {board} user license. Please contact your administrator.",
                            user_name => $_->contact_name, board => 'CV-Library' )} @bad_recipients;
        if(0 == scalar @recipients ){
            my $error_message = $s2->__x("All of the users you are trying to forward this candidate to do not have a {board} user license. Please contact your administrator.",
                                        board => 'CV-Library');
            die Stream::EngineException::ForwardingError->new( {message => $error_message})  ;
        }
    }

    my $advert_html = '';
    if($original_destination eq 'adcresponses') {
        my $advert = $self->get_adcresponse_advert($self->candidate_object()->attr('broadbean_adcadvert_id'));
        $advert_html = $s2->templates->render('adcourier_advert_details.html.tt', $advert);
    }

    # Consolidate recipients built from the recipent IDs, and the ones built from the email addresses.
    my @recipients_emails = ( map {$_->contact_email()} @recipients );
    push @recipients_emails , @{ $self->recipients_emails() };

    $log->info("Forwarding ".$self->candidate_object()->id()." on behalf of ".$self->user_object()->contact_name());
    my $cv_file;
    my $candidate_profile_html;
    if( $self->candidate_object()->has_cv ){
        $cv_file = $self->download_cv_file(); # From the DownloadCV role
    }elsif( $self->candidate_object()->has_profile() ){
        $candidate_profile_html = $self->build_forward_profile();
    }

    # Time to build an email
    my $recipients_string = join(', ' , @recipients_emails  );
    $log->debug("Email recipient: '".$recipients_string.'"');
    my $SUBSCRIPTION_INFO = $self->user_object->has_subscription( $original_destination ) || {};
    my $board_nice_name = $SUBSCRIPTION_INFO->{nice_name} // 'undefined board name';

    # use the profile body if its available, otherwise render the result body
    my $info_html = $candidate_profile_html || $self->stream2->templates->result_body(
            {
                L         => sub { Bean::Locale::localise_in( $self->user_object->locale(), @_ ); },
                candidate => $self->candidate_object->to_ref(),
            }
        );

    my $candidate_html  = q|<html>
<body>
|;
    if( $self->message() ){
        # Consider the message to be preformated.
        $candidate_html .= q|<pre>|.encode_entities($self->message()).q|</pre>
|;
    }


    my $attachment_links_expiry_days = $self->stream2()->config()->{attachment_links_expiry_days} // 30;

    my $private_files = $self->stream2()->factory('PrivateFile');

    my $download_expiry_date = DateTime->now()->add({ days => $attachment_links_expiry_days })->ymd();

    # Go through the other attachments and generate the magic links.
    my $attachments = $self->feed->download_attachments($self->candidate_object);

    my $other_attachments_html = '';
    if(@$attachments) {
        $other_attachments_html ||= '<p><b>'
            .encode_entities( $self->stream2()->__('Other attachments:'))
            .'</b></p>'."\n\n";
    }

    foreach my $download (@$attachments) {
        my $s3_object = $private_files->create({
            content_type        => $download->headers->content_type,
            content             => $download->content,
            content_disposition => $download->headers->header('Content-Disposition'),
            expires             => $download_expiry_date,
        });

        $other_attachments_html .= '<p><a href="'
            . encode_entities( $s3_object->query_string_authentication_uri() )
            . '">'.encode_entities( $download->filename() ).'</a></p>'."\n\n";
    }

    $candidate_html .= q|
<p>From |.encode_entities($board_nice_name).q|:</p>
<h1>|.encode_entities($self->candidate_object()->name()).q|</h1>
<p><a href="mailto:|.encode_entities($self->candidate_object->email()).q|">|.encode_entities($self->candidate_object->email()).q|</a></p>

|.$other_attachments_html.q|

|.$info_html;

    if ( $advert_html ) {
        $candidate_html .= qq|$advert_html|;
    }

    $candidate_html .= q|</body></html>|;

    my $stash = $self->get_template_stash();
    my $subject = $self->stream2()->templates()->render_simple( $self->user_object->settings_values_hash()->{'forwarding-candidate_email_subject'}, $stash );

    $log->info("Building email");
    # Build the email object.
    my $email = $s2->factory('Email')->build([
        From                                => 'noreply@broadbean.net',
        To                                  => $recipients_string,
        'Reply-To'                          => $self->user_object()->contact_email(),
        Type                                => 'multipart/mixed',
        'X-Aplitrak-responding-board'       => $SUBSCRIPTION_INFO->{'board_id'},
        'X-Aplitrak-responding-board-name'  => scalar($original_destination),
        'X-Stream2-Transport' => 'static_transport' , # We want this to go through the static IP transport
        Subject => Encode::encode('MIME-Q', $subject ),
    ]);

    # The container for alternative text or html
    my $container = $email->attach( Type => 'multipart/alternative' );


        # text version of candidate html body
    $container->attach(
        Type        => 'text/plain; charset=UTF-8',
        Encoding    => 'quoted-printable',
        Disposition => 'inline',
        Data        => [ Encode::encode('UTF-8', HTML::FormatText->format_string($candidate_html) ) ]
    );

    # html version of candidate html body
    $container->attach(
        Type        => 'text/html; charset=UTF-8',
        Encoding    => 'quoted-printable',
        Data        => [ Encode::encode('UTF-8',$candidate_html ) ]
    );


    # if there is a CV available
    if ( $cv_file ){
        # Attach the file to the main mixed email
        $email->attach(
            Type        => $cv_file->mime_type(),
            Encoding    => 'base64',
            Path        => $cv_file->disk_file(),
            Disposition => 'attachment',
            Filename    => Encode::encode('MIME-Q', $cv_file->name())
        );
    }

    my $sent_email = $self->stream2()->send_email($email);
    $log->info("Email sent");

    # Log the action
    $self->stream2()->action_log->insert({
        action          => "forward",
        candidate_id    => $self->candidate_object()->id,
        destination     => $original_destination,
        user_id         => $self->user_object->id,
        group_identity  => $self->user_object->group_identity,
        search_record_id => $self->candidate_object->search_record_id(),
        data             => {
            message           => $self->message,
            recipients_emails => \@recipients_emails,
            s3_log_id         => $self->log_chunk_id(),
            $self->get_mailjet_data( $sent_email ),
            $self->candidate_object()->action_hash()
        }
    });

    # Forward sent, now check for juice 'bcc' setting
    ## Potentially Coming from global settings:
    my $bcc_string = '';
    my $custom_subject = '';

    # a little bit of a lapse from the idea of a provider here
    if( $self->user_object->is_adcourier() ){
        if( my $forward_settings = $self->stream2->stream_api()->get_forward_email_settings($self->user_object()->provider_id()) ){
            $bcc_string = $forward_settings->{bcc};
            $custom_subject = $forward_settings->{subject};
        }
    }

    $custom_subject //= '['.$board_nice_name.'] ' . $self->candidate_object()->name();

    if ( $bcc_string ){
        $log->info("Got custom subject '$custom_subject'");
        # Expand the custom subject with a stash
        # See https://juice.adcourier.com/subject_variables.cgi?show=search

        $custom_subject = $self->stream2()->templates()->render_simple( $custom_subject , $stash );
        $log->info("Custom subject is now '".$custom_subject."'");

        # End of custom subject computation
        # my @bcc_email_addrs = split( /,/, $bcc_string );

        my @bcc_email_addrs = Email::Address->parse($bcc_string);

        my $bcc_email = $email->dup;
        $bcc_email->head->replace('Subject', Encode::encode('MIME-Q', $custom_subject ));

        foreach my $bcc_email_addr ( @bcc_email_addrs ){
            $bcc_email->head->replace('To', $bcc_email_addr);
            my $sent_bcc_email = $self->stream2()->send_email($bcc_email);
            $log->infof( "BCC Email sent to %s", $bcc_email_addr );
        }
    }


    return { message => $self->stream2()->__('success') , warnings => \@warnings};
}


sub build_forward_profile{
    my ($self) = @_;
    my $profile_action = Stream2::Action::DownloadProfile->new({ stream2 => $self->stream2(),
                                                                 user_object => $self->user_object(),
                                                                 candidate_object => $self->candidate_object(),
                                                                 remote_ip => $self->remote_ip(),
                                                                 log_chunk_id => $self->log_chunk_id(),
                                                                 log_chunk_capture => 0,
                                                             });
    $profile_action->instance_perform();
    return $self->stream2->templates->profile_body(
        {
            L  => sub { Bean::Locale::localise_in( $self->user_object->locale(), @_ ); },
            candidate => $self->candidate_object()
        }
    );
}


__PACKAGE__->meta->make_immutable();

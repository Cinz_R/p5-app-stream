package Stream2::Action::TagCandidate;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;

with qw/
    Stream2::Role::Action::InManageError
    Stream2::Role::Action::HasUser
/;

with qw/
    Stream2::Role::Action::TriggerMacros
    Stream2::Role::Action::HasCandidate
    Stream2::Role::Action::FastJob
    Stream2::Role::Action::InLogChunk
/;


has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

with qw/Stream2::Role::Action::HasFeed/;

has 'tag_name' => ( is => 'ro', isa => 'Str', required => 1 );

=head2 instance_perform

Tags the candidate with this tag_name

=cut

sub instance_perform {
    my ( $self ) = @_;
    my $stream2 = $self->stream2;
    my $candidate = $self->candidate_object();
    my $user = $self->user_object();
    my $tag_name = $self->tag_name();
    ## HACK, due to https://jira.artirix.com/browse/BRBE-46
    # In short, artirix do not support commas in tags. We shouldn't have any comas anywhere.
    # ALSO, Our careerbuilder friends like to separate stuff on |. Everyone has got different wrong ideas..
    $tag_name =~ s/(?:,|\|)/_/g;

    my $schema = $stream2->stream_schema();

    # We're finding and creating, do stuff in a transaction to avoid race conditions o_O
    my $stuff = sub {
        my $tag = $stream2->factory('Tag')->find_or_create({
            group_identity  => $user->group_identity(),
            tag_name        => $tag_name,
            # Yes this is rididulous,
            # as we have a BEFORE INSERT trigger that calculates
            # this thing for us. So we should not need to give it.
            # However, MySQL being MySQL, it checks that this is NOT NULL,
            # BEFORE the BEFORE trigger has been triggered. Lost 2 hours
            # figuring that out. Thanks MySQL.
            tag_name_ci     => $tag_name,
        });

        my $candidate_tag = $schema->resultset('CandidateTag')->find_or_create({
            group_identity  => $user->group_identity(),
            candidate_id    => $candidate->id(),
            tag_name        => $tag->tag_name,
        });
        $candidate_tag->user_id($user->id);
        $candidate_tag->update();

        $candidate->add_tag($tag_name);
        return $candidate_tag;
    };

    my $new_tag = $schema->txn_do($stuff);

    if ( $new_tag ) {
        # Let the feed do its shenanigans with the tag
        $self->feed->tag_candidate($candidate, $new_tag);
    }

    # tagging an application should percolate through like a wet fart
    if ( $candidate->destination() eq 'adcresponses' && $candidate->attr('tsimport_id') ){
        $self->tag_internal($tag_name);
    }

    $user->stream2->action_log()->insert({
        action              => 'candidate_tag_add',
        candidate_id        => $candidate->id,
        destination         => $candidate->destination,
        user_id             => $user->id,
        group_identity      => $user->group_identity,
        search_record_id    => $candidate->search_record_id(),
        data                => {
            s3_log_id   => $self->log_chunk_id(),
            tag_name    => $tag_name,
            $candidate->action_hash()
        },
    });

    return { message => "Tag '$tag_name' added." };
}

=head2 tag_internal

If this is a response candidate then we might have to push the tag to it's
internal DB counterparts, this method seeks out the association and
creates a longstep process in order to action them.

=cut

sub tag_internal {
    my ( $self, $tag_name ) = @_;

    my $stream2 = $self->stream2();
    my $candidate = $self->candidate_object();
    my $internal_mapping = $candidate->get_internal_db_ids() // '';

    if ( ref ( $internal_mapping ) eq 'HASH' || $internal_mapping eq 'INTERNAL_DBS_PLEASE_WAIT' ){
        # we know there is some sort of internal mapping
        $log->info( "Internal DB candidate exists... initialising longstep tm" );
        return $stream2->longsteps()->instantiate_process(
            'Stream2::Process::TransferResponseTag',
            {
                stream2 => $stream2,
            },
            {
                user_id             => $self->user_object()->id(),
                adcresponses_id     => $candidate->candidate_id(),
                tsimport_id         => $candidate->attr('tsimport_id'),
                search_record_id    => $candidate->search_record_id(),
                tag_name            => $tag_name,
                ( ref ( $internal_mapping ) ? ( internal_mapping => $internal_mapping ) : () )
            }
        );
    }

    $log->info( "This candidate has no internal DB mapping" );
}

__PACKAGE__->meta->make_immutable();

1;

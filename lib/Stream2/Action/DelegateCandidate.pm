package Stream2::Action::DelegateCandidate;

use Moose;
extends qw/Stream2::Action/;

use Log::Any qw/$log/;

use Stream::EngineException;

with qw/
  Stream2::Role::Action::InManageError
  Stream2::Role::Action::HasUser
  Stream2::Role::Action::HasCandidate
  Stream2::Role::Action::HasRemoteIP
/;

has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

with qw/
  Stream2::Role::Action::HasFeed
  Stream2::Role::Action::InLogChunk
/;

has [ '+on_managed_error', '+on_not_managed_error' ] => ( default => sub {
    my $self = shift;
    return sub {
        $self->stream2()->action_log->insert({
            action          => "delegate_fail",
            candidate_id    => $self->candidate_object()->id(),
            destination     => $self->candidate_object()->destination(),
            user_id         => $self->user_object->id,
            group_identity  => $self->user_object->group_identity,
            search_record_id => $self->candidate_object->search_record_id(),
            data            => { s3_log_id => $self->log_chunk_id(),
                                 $self->candidate_object()->action_hash(),
                                 delegate_to_userid => $self->delegate_to_userid,
                             },
        });
    }
});

has delegate_to_userid => (is => 'ro', isa => 'Int', required => 1);

=head2 instance_perform

Takes the candidate object and associates it to the delegated user in
the BBCSS. This action updates both users responses in realtime, but
the indexing of the candidate is not done in realtime, so delays
are expected for the users to see the effects of this action.

=cut

sub instance_perform {
    my ( $self )  = @_;
    my $stream2   = $self->stream2;
    my $user      = $self->user_object();
    my $candidate = $self->candidate_object();

    # The user we are transferring the candidate to.
    my $delegate_user = $stream2->factory('SearchUser')->find($self->delegate_to_userid);

    if ( $delegate_user->id == $user->id ) {
        die Stream::EngineException::Internal->new({
            message => $stream2->__('You cannot delegate the candidate to yourself.')
        });
    }

    # See if the delegate user has a requisition advert too.
    # Uses the "search_adverts" call as it returns AdCourier
    # advert information (single resource equivalent does not).
    my @adverts = $stream2->advert_api->search_adverts({
        company => $delegate_user->group_identity,
        user_id => $delegate_user->provider_id,
        is_requisition_ad => 1,
    });

    unless ( @adverts ) {
        die Stream::EngineException::NotSupported->new({
            message => $stream2->__('The user you are delegating to does not have a requisition advert.')
        });
    }

    my $advert_id = $adverts[0]->{advert_id};

    # Associate the response to the delegate users advert in BBCSS.
    # This will also notify any subscribers to response updates.
    $self->feed->update_candidate(
        $candidate,
        { broadbean_adcidentity_path    => $delegate_user->hierarchical_path,
          broadbean_adcuser_id          => $delegate_user->provider_id,
          broadbean_adcadvert_id        => $advert_id,
        },
        { action => $stream2->whoami(__PACKAGE__) }
    );

    # Lastly, log the action
    # Note that the action is stored against the current user (the delegator) and the user id
    # to which the candidate was delegated to is stored in the data column (useful for
    # knowing the number of bases this candidate has touched).
    $self->stream2()->action_log->insert({
        action              => 'delegate',
        candidate_id        => $self->candidate_object()->id,
        destination         => $self->candidate_object()->destination,
        user_id             => $user->id,
        group_identity      => $user->group_identity,
        search_record_id    => $self->candidate_object->search_record_id(),
        data                => {
            s3_log_id => $self->log_chunk_id(),
            $self->candidate_object()->action_hash(),
            delegate_to_userid          => $self->delegate_to_userid,
            delegate_to_contact_name    => $delegate_user->contact_name,
            delegate_from_contact_name  => $user->contact_name,
        },
    });

    return { message => $stream2->__x('Candidate is being sent to {contact_name}\'s responses', contact_name => $delegate_user->contact_name) };
}

__PACKAGE__->meta->make_immutable();
1;

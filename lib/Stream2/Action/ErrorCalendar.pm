package Stream2::Action::ErrorCalendar;
use strict; use warnings;

use Data::Dumper;
use Log::Any qw/ $log /;

sub perform {
    my ( $class, $job ) = @_;
    my $stream2 = $job->{stream2} or die 'stream2 not found';
    my $opts = $job->parameters;

    # this defo needs to be a number in ES
    if ( defined ( $opts->{n_results} ) ){
        $opts->{n_results} = $opts->{n_results} * 1;
    }

    my $response = eval{
        $stream2->aws_elastic_search()->create({
                index => 'threads',
                type => 'stream2search',
                body => $opts
        });
    };
    if( my $err = $@ ){
        ## This is a transport layer error. Dont break the search product because of that.
        $log->error("Could not send error calendar record ".Dumper($opts).". Error: ".$err);
        die "Could not send calendar record";
    }
    unless( $response->{ok} || $response->{created} ){ # Make that compatible with the new Elastic search.
        my $error_string = "Something went wrong sending ".Dumper($opts)." Response was: ".Dumper($response);
        $log->warn($error_string);
        die $error_string;
    }
    $log->info("Saved search calendar entry. Have a look there: ".$opts->{stream2_base_url}.'searchrecords/'.$response->{_id});
    return 1;
}

1;

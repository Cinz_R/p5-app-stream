package Stream2::Action::PhoneCandidate;

use Moose;
use Log::Any qw/$log/;

extends qw/Stream2::Action/;

with qw/
           Stream2::Role::Action::InManageError
           Stream2::Role::Action::HasUser
       /;


with qw/
           Stream2::Role::Action::TriggerMacros

           Stream2::Role::Action::HasCandidate

           Stream2::Role::Action::FastJob
           Stream2::Role::Action::InLogChunk
       /;

sub instance_perform {
    my ( $self ) = @_;

    $log->info("Phoning candidate ".$self->candidate_object()->id()." for user ".$self->user_object()->id());

    # Log the action
    $self->stream2()->action_log->insert({
        action          => "phonecall",
        candidate_id    => $self->candidate_object()->id,
        destination     => $self->candidate_object()->destination(),
        user_id         => $self->user_object->id,
        group_identity  => $self->user_object->group_identity,
        search_record_id => $self->candidate_object->search_record_id(),
        data            => { 
            s3_log_id => $self->log_chunk_id(),
            $self->candidate_object()->action_hash()
        },
    });

    return 1;
}

__PACKAGE__->meta->make_immutable();

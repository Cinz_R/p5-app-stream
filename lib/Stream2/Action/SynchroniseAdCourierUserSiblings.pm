package Stream2::Action::SynchroniseAdCourierUserSiblings;
use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;

use Data::Dumper;

=head1 Stream2::Actions::SynchroniseAdCourierUserSiblings

    part of the user experience is to have access to thier colleague's contact details
    available within the application.

    The AdCourier user "API" isn't fast enough to make calls to in real time so the alternative
    is to fetch the list ahead of time and keep just the essentials required for auto-complete

    Usage:

        Stream2::Action::SynchroniseAdCourierUserSiblings->perform(
            $qurious->create_job(
                'Stream2::Action::SynchroniseAdCourierUserSiblings',
                'MyQueue',
                {
                    adc_id => xxx
                }
            )
        );

=cut

has 'adc_id'        => ( is => 'ro', isa => 'Str',  required => 0 );
has 'clean_missing' => ( is => 'ro', required => 1, default  => 0 );
has 'company'       => ( is => 'ro', isa => 'Str',  required => 1 );

sub instance_perform {
    my ( $self )  = @_;

    my $company   = $self->company;
    my $mutex_key = 'AdC_user_sync-'.$company;

    # Don't run an import on the same company

    $self->stream2()->monitor->try_mutex( $mutex_key,
        sub {
            $self->run_synchronisation();
        },
        sub {
            $log->debugf( "Synching AdC users already under way for company: %s", $company );
        }
    );

    return 1;
}

sub run_synchronisation {
    my ( $self ) = @_;

    my $stream2  = $self->stream2();
    my $company  = $self->company;
    my $user_id  = $self->adc_id;
    my @users = $stream2->user_api->company_users( $company );

    my %seen = ();
    my @to_sync = grep { ! $seen{$_}++ } map { $_->{id} } @users;

    # any user_ids not in @to_sync should be removed from stream2 user DB
    if ( $self->clean_missing ) {
        $log->info( "deleting users during the sync process is disabled for now #toodangerous" );
        # $self->_remove_not_in_list( $company, $user_id, @to_sync );
    }

    $log->debugf( "importing adc_users: %s - for company %s", join( ", ", @to_sync ), $company );

    my $user_factory = $stream2->factory('SearchUser');
    foreach my $user_ref ( @users ) {
        my $id = $user_ref->{id};
        my $user = eval {
            my $stream_user = $user_factory->find(
                {
                    provider => 'adcourier',
                    provider_id => $id,
                },
                {
                    ripple_settings => {}
                }
            );

            # Office can be void. Avoid warnings.
            if ( ( $user_ref->{office} // '' ) eq 'zombie' ){
                if ( $stream_user ) {
                    $self->_zombie_user( $stream_user );
                }
                die "user is zombie";
            }

            if ( $stream_user ){
                $stream_user->contact_details->{contact_name} = $user_ref->{name};
                $stream_user->contact_details->{contact_email} = $user_ref->{email};
                $stream_user->contact_name( $user_ref->{name} );
                $stream_user->contact_email( $user_ref->{email} );
                $stream_user->contact_details->{$_} = $user_ref->{$_} for qw/consultant team office company username/;

                $stream_user->make_column_dirty( 'contact_details' );
                $stream_user->update();
            }
            else {
                $stream_user = $user_factory->new_result({
                    'contact_details'   => {
                        contact_name    => $user_ref->{name},
                        contact_email   => $user_ref->{email},
                        map { $_ => $user_ref->{$_} } qw/consultant team office company username/
                    },
                    'provider'          => 'adcourier',
                    'last_login_provider' => 'adcourier',
                    'provider_id'       => $id,
                    'group_identity'    => $user_ref->{company},
                    'group_nice_name'   => $user_ref->{company_nice_name},
                    'settings'          => {},
                });
                $stream_user->subscriptions_ref( {} );
                $stream_user->save();
                $stream_user->ripple_settings()->{login_provider} = 'adcourier';
                $stream_user->conform_to_sibling_settings();
            }
            return $stream_user;
        };

        unless ( $@ ) {
            $log->debugf( 'Syncing AdC user: %s, successful', $id );
        }
        else {
            $log->debugf( 'Syncing AdC user: %s, failed - %s', $id, $@ );
        }
    }
}

sub _remove_not_in_list {
    my ( $self , @ids ) = @_;

    my $result_set = $self->stream2()->factory('SearchUser')->search({
        provider_id     => { -not_in => \@ids },
        provider        => 'adcourier',
        group_identity  => $self->company,
    });
    my $deleted = $result_set->delete();

    $log->infof( 'Removed %s users for company %s', $deleted."", $self->company );

    return $deleted;
}

=head2 _zombie_user

a user whom is a zombie should have their watchdogs ( amongst other things ) deactivated
we will be updateing their watchdogs as inactive = 0 at most once per hour ( each time we sync users )
this is not awesome, but it is more awesome than deleting the user altogether because that could cause hassle

Although, in the future I think we should consider deleting users when they are no longer active ( or zombified )

even though we update the watchdogs as active => 0 quite often, the update_datetime should not get updated ( only the first time )
this means that the periodical system should pick up the oldness of the watchdogs after 3 months and delete them ( along with the results )

=cut

sub _zombie_user {
    my ( $self, $user ) = @_;
    $user->watchdogs->update({ active => 0 });
}

__PACKAGE__->meta->make_immutable();

package Stream2::Action::DownloadOtherAttachment;

use Moose;

extends qw/Stream2::Action/;

use MIME::Base64;

use Log::Any qw/$log/;

with qw/
  Stream2::Role::Action::InManageError
  Stream2::Role::Action::HasUser
  Stream2::Role::Action::HasCandidate
  Stream2::Role::Action::HasRemoteIP
  /;

has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}


with qw/
           Stream2::Role::Action::HasFeed
       /;


has 'attachment_key' => ( is => 'ro', isa => 'Str', required => 1 );

=head2 instance_perform

Generates an HTTP::Response containing the attachment targeted by 'attachment_key'

=cut

sub instance_perform {
    my ( $self )  = @_;

    my $user      = $self->user_object();
    my $candidate = $self->candidate_object();
    my $feed      = $self->feed();

    $log->infof(
        "Downloading Attachment '%s' for candidate %s for user %s.",
        $self->attachment_key(),
        $candidate->id(),
        $user->id()
    );

    my $response = $feed->download_attachment($candidate, $self->attachment_key);

    $response->code(200);
    $response->message('Other attachment found');

    return MIME::Base64::encode_base64( $response->as_string("\n") );
}

__PACKAGE__->meta->make_immutable();
1;

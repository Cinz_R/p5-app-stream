package Stream2::Action::ShortlistCandidate;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;
use List::Util ();

with qw/
           Stream2::Role::Action::InManageError
           Stream2::Role::Action::Email
           Stream2::Role::Action::HasUser
           Stream2::Role::Action::HasAdvert
       /;

has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

with qw/Stream2::Role::Action::HasFeed
        Stream2::Role::Action::DownloadCV

        Stream2::Role::Action::TriggerMacros

        Stream2::Role::Action::HasCandidate

        Stream2::Role::Action::FastJob
        Stream2::Role::Action::InLogChunk
       /;

has 'advert_label' => ( is => 'ro', isa => 'Str', required => 1);

has [ '+on_managed_error', '+on_not_managed_error' ] => ( default => sub {
    my $self = shift;
    return sub {
        my $action_name = sprintf( "shortlist_%s_fail", $self->list_name() );
        $self->user_object()->login_provider()->find_action($self->user_object(), $action_name);

        $self->stream2()->action_log->insert({
            action          => $action_name,
            candidate_id    => $self->candidate_object()->id(),
            destination     => $self->candidate_object()->destination(),
            user_id         => $self->user_object->id,
            group_identity  => $self->user_object->group_identity,
            search_record_id => $self->candidate_object->search_record_id(),
            data            => { s3_log_id => $self->log_chunk_id(),
                                 $self->candidate_object()->action_hash()
                             },
        });
    }
});

use Data::Dumper;
use Encode;
use File::Slurp;
use HTML::FormatText;

use Scope::Guard;

use Stream::EngineException;

=head2 instance_perform

As the user, shortlist the given candidate against the given job and
so all sort of other stuff.

=cut

sub instance_perform {
    my ( $self ) = @_;

    my $s2 = $self->stream2();
    my $candidate = $self->candidate_object();
    my $user = $self->user_object();

    ## Downloading the CV is a must have
    my $cv_file = $self->download_cv_file();

    # All of that relate to the Download CV.
    my $content_type = $cv_file->mime_type() || 'text/plain';
    my $content = $cv_file->binary_content(); # That's the CV binary.
    my $content_filename = $cv_file->name();
    my $content_b64 = MIME::Base64::encode_base64( $content );

    my $cv_parsing_res;

    my @warnings = ();

    {   ## CV Parsing section
        # Needed to fixup the candidate
        unless( $candidate->email() && $candidate->name() ){
            $log->info("No email or no name in candidate. Trying to extract from CV with the CVP API");
            $cv_parsing_res = $user->parse_cv_with_locale($content);
        }
        if( $cv_parsing_res ){
            # The CV was parsed for some reason.
            $candidate->enrich_with_cvparsing($cv_parsing_res);

            $log->infof('Parsing result for %s, user_id: %s, candidate_id: %s, locale: %s, postcode: %s',
                $user->group_identity,
                $user->id,
                $candidate->id,
                $cv_parsing_res->{locale},
                $cv_parsing_res->{post_code}
            );
        }
    }


    # Story 71143872 - Email/Name not crucial for shortlisting
    # using same email as Search 1
    unless( $candidate->email() ) {
        $candidate->make_email_up( $user );
        push @warnings, $s2->__("Could not extract email address. Note that this candidate email address was replaced by a temporary one.");
    }

    #
    # Import to Internal DB
    #
    my $in_adcourier_search = List::Util::any {
        $candidate->destination eq $_
    } $self->stream2->factory('Board')->list_internal_databases();

    # if this isn't an adcourier shortlist action then we should import the CV to thier internal database as well
    # AdCourier should push the new response we are registering to the BBCSS and also TSImport for us
    if( $self->list_name ne 'adc_shortlist' && ! $in_adcourier_search && $user->has_adcourier_search() ){
        $self->try_import_internal(
            {
                cv_mimetype => $content_type,
                cv_content => $content_b64,
                cv_filename => $content_filename
            },
            \@warnings
        );
        ## Ok importing to talent search is done.
    } # End of if should import into talent search.

    ## Now is the time to do the shortlist for real.
    $log->info("Shortlisting candidate");

    # From the HasAdvert role
    my $advert = $self->get_advert();
    unless( $advert ){
        die $s2->__("Could not find advert to shortlist against")."\n";
    }
    # Why not. Dont remember why its there.
    $advert->{advert_label} = $self->advert_label;

    $log->debug("Got advert ".Dumper($advert));

    ## Grab the board_nice name from the subscription info.
    my $board_nice_name = $self->feed_subscription_info->{nice_name} // 'undefined board name';
    $log->info("Board nice name is $board_nice_name");

    # Definitely depends on ripple settings.
    my $error = $self->user_object()->login_provider()
        ->advert_shortlist_candidate( $advert , $candidate, { cv_mimetype => $content_type,
                                                              cv_content => $content_b64,
                                                              cv_filename => $content_filename
                                                          },
                                      $self->original_user() || $self->user_object(),
                                      {
                                          board_name        => $self->candidate_object()->destination(),
                                          board_nice_name   => $board_nice_name,
                                          cv_parsing_res    => $cv_parsing_res,
                                      }
                                );
    if( $error ){
        die Stream::EngineException::ShortlistingError->new({ message => ( ref($error) ? $error->{message} : $error ) });
    }

    # Time to build an email if such a thing makes sense.
    # TODO:SEAR-511 replace this with identity_provider as email settings
    # depends only on the nature of the user (not the way they connect)
    my $sent_email;
    if( my $shortlist_email_settings = $self->user_object()->login_provider()
            ->get_shortlist_email_settings($self->user_object()) ){

        $log->info("Generating email for ".Dumper($shortlist_email_settings));

        my $email = $self->generate_candidate_email({ subject_template => $shortlist_email_settings->{subject},
                                                       advert => $advert,
                                                       cv => {
                                                           content_type => $content_type,
                                                           content_filename => $content_filename,
                                                           content => $content
                                                       },
                                                   });
        $email->head()->replace('To' , $shortlist_email_settings->{bcc} );
        $log->info("Sending email to '".$shortlist_email_settings->{bcc}."'");
        {
            my $guard = Scope::Guard::scope_guard(
                sub{
                    # Clear all temp files that the email was built on.
                    $log->info("Purging email attached disk files");
                    $email->purge();
                });

            $sent_email = $s2->send_email($email);
            # The scope guard will run now regardless of any error.
        }
    }


    if( $user->has_adcourier_search() && $in_adcourier_search ){
        # The user has got some adcourier search (TalentSearch, MySupply, etc.. )
        # and the candidate is from one of these boards.
        $self->feed()->add_candidate_advert_identifier( $candidate, $self->list_name.':'.$self->advert_id );
    };
    if( my $err = $@ ){
        $log->error(sprintf("Error adding job reference to candidate: %s", $err));
        push @warnings, $s2->__("Failed to add job reference to candidate");
    }

    $log->info("Incrementing analytics");
    $s2->increment_analytics(
        user_id   => $user->provider_id,
        board     => $candidate->destination(),
        shortlist => 1,
    );

    my $action_name = "shortlist_". $self->list_name();
    $log->info("Recording action '$action_name'");

    # Note that this will vivify dynamically defined action names
    # should the provider be adcourier_ats

    # using login_provider here is cool, because actions against ripple
    # cheese sandwiches will be vivified.
    my $action_o = $self->user_object()
        ->login_provider()->find_action($self->user_object(), $action_name);
    # build the link if the advert comes from adcourier
    my $advert_link = $self->{list_name} =~/^adc/ ? sprintf("/api/cheesesandwiches/%s/%s/preview/999",$self->{list_name},$self->{advert_id}) : undef;
    $self->stream2()->action_log->insert({
        action => $action_name,
        candidate_id => $self->candidate_object()->id(),
        destination  => $self->candidate_object()->destination(),
        user_id      => $self->user_object()->id(),
        group_identity => $self->user_object()->group_identity(),
        search_record_id => $self->candidate_object->search_record_id(),
        data => {
                  advert_link      => $advert_link,
                  advert_label     => $advert->{stream2_jobtitle},
                  advert_list_name => $self->{list_name},
                  advert_id        => $self->{advert_id},
                  board_nice_name  => $board_nice_name,
                  s3_log_id        => $self->log_chunk_id(),
                  $self->get_mailjet_data( $sent_email ),
                  $self->candidate_object()->action_hash(),
        }
    });
    $log->info("All done");
    return { message => $s2->__x('This candidate has been successfully added to {list_item_name}', list_item_name => $self->advert_label), warnings => \@warnings };
}

=head2 try_import internal

If we have a fairly complete candidate record and they are suitable for
importation into available internal databases we will send it onto the tsimportapi.

Usage:

    my @warnings = ();
    $this->try_import_internal(
        {
            cv_mimetype => $content_type,
            cv_content  => $content_b64,
            cv_filename => $content_filename
        },
        \@warnings
    );

=cut

sub try_import_internal {
    my ( $self, $cv_hashref, $warnings ) = @_;

    my $user = $self->user_object();
    my $candidate = $self->candidate_object();

    # The user has got some adcourier search (TalentSearch, MySupply, etc.. ), and the candidate
    # is from a foreign board. Time to import it.
    $log->info("Importing candidate in user talentsearch.");
    eval{
        $self->stream2->build_provider('adcourier')->user_import_candidate(
            $user,
            $cv_hashref,
            $candidate,
            {
                candidate_attributes => {
                    job_requisition_ids => [ $self->list_name.':'.$self->advert_id ]
                }
            }
        );
        $self->stream2()->action_log->insert({
            action => 'import',
            candidate_id => $candidate->id(),
            destination  => $candidate->destination(),
            user_id      => $user->id(),
            group_identity => $user->group_identity(),
            search_record_id => $candidate->search_record_id(),
            data => {
                s3_log_id => $self->log_chunk_id(),
                $candidate->action_hash()
            }
        });

    };
    if( my $err = $@ ){
        $log->errorf("Error importing candidate to talent search: %s", $err);
        push @$warnings, $self->stream2->__("Failed to import Candidate to talent search");
    }

    return;
}

__PACKAGE__->meta->make_immutable();

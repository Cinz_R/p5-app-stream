package Stream2::Action::UntagCandidate;

use Moose;
extends qw/Stream2::Action/;

=head1 NAME

Stream2::Action::UntagCandidate - Handles untagging a candidate

=head1 DESCRIPTION

Usage:

    my $result = Stream2::Action::UntagCandidate({
        stream2             => $stream2,
        candidate_object    => $candidate,
        user_object         => $user,
        jobid               => 'whatever',
        ripple_settings     => $user->ripple_settings,
        tag_name            => 'belieber'
    });

    # success -> $result->{message}
    # failure -> $result->[error}

    Note there are no failure logs being created, this might change
    in the future.

=cut

use Stream::EngineException;
use Log::Any qw/$log/;

with qw/
    Stream2::Role::Action::InManageError
    Stream2::Role::Action::HasUser
/;

with qw/
    Stream2::Role::Action::TriggerMacros
    Stream2::Role::Action::HasCandidate
    Stream2::Role::Action::FastJob
    Stream2::Role::Action::InLogChunk
/;


has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

with qw/Stream2::Role::Action::HasFeed/;

has 'tag_name' => ( is => 'ro', isa => 'Str', required => 1 );

=head2 instance_perform

Untags the candidate with this tag_name

=cut

sub instance_perform {
    my ( $self ) = @_;
    my $stream2 = $self->stream2;
    my $candidate = $self->candidate_object();
    my $user = $self->user_object();
    my $tag_name = $self->tag_name();

    my $candidate_tag = $stream2->stream_schema()->resultset('CandidateTag')->find({
        group_identity  => $user->group_identity(),
        candidate_id    => $candidate->id(),
        tag_name        => $tag_name,
    });

    unless ( $candidate_tag ) {
        die Stream::EngineException::TaggingError->new({
            message => $stream2->__("Tag with name '$tag_name' not found")
        });
    }

    $candidate_tag->delete();
    # Let the feed carry out actions for untagging the candidate too
    $self->feed->untag_candidate($candidate, $candidate_tag);

    $candidate->remove_tag($tag_name);

    $user->stream2->action_log()->insert({
        action              => 'candidate_tag_delete',
        candidate_id        => $candidate->id,
        destination         => $candidate->destination,
        user_id             => $user->id,
        group_identity      => $user->group_identity,
        search_record_id    => $candidate->search_record_id(),
        data                => {
            s3_log_id   => $self->log_chunk_id,
            tag_name    => $tag_name,
            $candidate->action_hash()
        },
    });

    return { message => "Tag '$tag_name' removed." };
}

__PACKAGE__->meta->make_immutable();

1;

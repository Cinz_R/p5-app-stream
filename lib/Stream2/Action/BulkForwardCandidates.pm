package Stream2::Action::BulkForwardCandidates;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;

with qw/
           Stream2::Role::Action::InManageError
           Stream2::Role::Action::HasUser
           Stream2::Role::Action::HasRemoteIP
       /;

with qw/
           Stream2::Role::Action::TriggerMacros
           Stream2::Role::Action::FastJob
           Stream2::Role::Action::InLogChunk
           Stream2::Role::Action::Email
       /;

has 'template_name'         => ( is => 'ro', isa => 'Str', lazy => 1, builder => '_build_destination' );

with qw/
    Stream2::Role::Action::HasFeed
    Stream2::Role::Action::DownloadCV
/;

use Bean::Locale;

use Stream::EngineException;
use Stream2::Action::DownloadProfile;
use Stream2::Action::DownloadCV;

use Stream2::O::File;

use Encode;
use HTML::FormatText;
use HTML::Entities;

use DateTime;

has 'stream2_base_url' => ( is => 'ro', isa => 'Str', required => 1);

has 'recipients_emails'     => ( is => 'ro', isa => 'ArrayRef', required => 1);
has 'recipients_ids'        => ( is => 'ro', isa => 'ArrayRef', required => 1);
has 'results_id'            => ( is => 'ro', isa => 'Str', required => 1 );
has 'candidate_idxs'        => ( is => 'ro', isa => 'ArrayRef', required => 1);
has 'candidates'            => ( is => 'ro', isa => 'ArrayRef', lazy_build => 1 );
has 'message'               => ( is => 'ro', isa => 'Maybe[Str]', required => 1);

has 'destination'           => ( is => 'ro', isa => 'Str', lazy_build => 1 );
has 'warnings'              => ( is => 'ro', isa => 'ArrayRef', default => sub { []; } );
has 'recipients'            => ( is => 'ro', isa => 'ArrayRef', lazy_build => 1 );
has 'all_recipient_emails'  => ( is => 'ro', isa => 'ArrayRef', required => 1, lazy_build => 1 );

has 'forward_settings'      => ( is => 'ro', isa => 'HashRef', lazy_build => 1 );
has 'subscription_info'     => ( is => 'ro', isa => 'HashRef', lazy_build => 1 );


has [ '+on_managed_error', '+on_not_managed_error' ] => ( default => sub {
    my $self = shift;

    return sub {
        foreach my $candidate_object ( @{$self->candidates} ){
            $self->stream2()->action_log->insert({
                action              => "forward_fail",
                candidate_id        => $candidate_object->id(),
                destination         => $candidate_object->destination(),
                user_id             => $self->user_object->id,
                group_identity      => $self->user_object->group_identity,
                search_record_id    => $candidate_object->search_record_id(),
                data                => {
                    s3_log_id => $self->log_chunk_id(),
                    $candidate_object->action_hash()
                },
            });
        }
    }
});

=head2 instance_perform

Downloads the CV or the profile and forward it to the given list of recipients

if the candidate has a CV -> send that

else if the candidate has a profile -> send that
else -> send the result body

=cut

sub instance_perform {
    my ( $self ) = @_;

    my $attachment_links_expiry_days = $self->stream2()->config()->{attachment_links_expiry_days} // 30;
    my $private_files = $self->stream2()->factory('PrivateFile');
    my $download_expiry_date = DateTime->now()->add({ days => $attachment_links_expiry_days })->ymd();

    my @candidate_stash = ();
    foreach my $candidate_object ( @{$self->candidates} ){
        my $candidate_ref = {};
        $candidate_object->fixup();

        # CV
        my $candidate_profile_html = '';
        if( $candidate_object->has_cv ){
            my $file = $self->download_cv_file( $candidate_object ); # From the DownloadCV role
            my $s3_object = $private_files->create({
                content_type            => $file->mime_type(),
                content                 => $file->binary_content(),
                content_disposition     => 'attachment; filename='.$file->name(),
                expires                 => $download_expiry_date
            });

            $candidate_ref->{cv_link} = {
                filename    => $file->name(),
                url         => $s3_object->query_string_authentication_uri()
            };
        }
        # PROFILE
        elsif( $candidate_object->has_profile() ){
            $candidate_profile_html = $self->build_forward_profile( $candidate_object );
        }

        # use the profile body if its available, otherwise render the result body
        $candidate_ref->{profile_body} = $candidate_profile_html || $self->stream2->templates->result_body(
            {
                L         => sub { Bean::Locale::localise_in( $self->user_object->locale(), @_ ); },
                candidate => $candidate_object->to_ref(),
            }
        );

        # OTHER ATTACHMENTS
        my @attachments;
        my $attachment_downloads = $self->feed->download_attachments($candidate_object);
        foreach my $download (@$attachment_downloads) {
            my $s3_object = $private_files->create({
                content_type        => $download->headers->content_type,
                content             => $download->content,
                content_disposition => $download->headers->header('Content-Disposition'),
                expires             => $download_expiry_date,
            });
            push @attachments, {
                filename    => $download->filename,
                url         => $s3_object->query_string_authentication_uri()
            };
        }

        $candidate_ref->{other_attachments} = \@attachments;

        push @candidate_stash, {
            %{$candidate_object->to_ref},
            %{$candidate_ref}
        };
    }

    my $email_body = $self->stream2->templates->render(
        'email_templates/email-bulk-forward.tt',
        {
            candidates      => \@candidate_stash,
            message         => $self->message,
            i18n            => sub { $self->stream2->translations->__(@_) }
        }
    );

    my $email = $self->_generate_email( $email_body );

    my $sent_email = $self->stream2()->send_email($email);
    $log->info("Email sent");

    foreach my $candidate_object ( @{$self->candidates} ){
        # Log the action
        $self->stream2()->action_log->insert({
            action              => "forward",
            candidate_id        => $candidate_object->id,
            destination         => $self->destination,
            user_id             => $self->user_object->id,
            group_identity      => $self->user_object->group_identity,
            search_record_id    => $candidate_object->search_record_id(),
            data                => {
                message            => $self->message,
                recipients_emails  => $self->all_recipient_emails,
                s3_log_id          => $self->log_chunk_id(),
                $self->get_mailjet_data( $sent_email ),
                $candidate_object->action_hash()
            }
        });
    }

    return { message => $self->stream2()->__('success') , warnings => $self->warnings};
}

sub _generate_email {
    my ( $self, $email_body ) = @_;

    # Potentially Coming from global settings:
    my $bcc_string = $self->forward_settings->{bcc} // '';

    $log->info("Building email");
    # Build the email object.
    my $email = $self->stream2->factory('Email')->build([
        'To'                                => join(', ' , @{$self->all_recipient_emails()}),
        'From'                              => 'noreply@broadbean.net',
        'Reply-To'                          => $self->user_object()->contact_email(),
        'Subject'                           => Encode::encode('MIME-Q', $self->_generate_subject() ),
        'Type'                              => 'multipart/mixed',
        'X-Aplitrak-responding-board'       => $self->subscription_info()->{'board_id'},
        'X-Aplitrak-responding-board-name'  => scalar($self->destination),
        'X-Stream2-Transport'               => 'static_transport',

        ( $bcc_string ? ( 'Bcc' => $bcc_string) : () ),
    ]);

    # The container for alternative text or html
    my $container = $email->attach( Type => 'multipart/alternative' );

    # text version of candidate html body
    $container->attach(
        Type        => 'text/plain; charset=UTF-8',
        Encoding    => 'quoted-printable',
        Disposition => 'inline',
        Data        => [ Encode::encode('UTF-8', HTML::FormatText->format_string($email_body) ) ]
    );

    # html version of candidate html body
    $container->attach(
        Type        => 'text/html; charset=UTF-8',
        Encoding    => 'quoted-printable',
        Data        => [ Encode::encode('UTF-8', $email_body) ]
    );

    return $email;
}

sub _generate_subject {
    my ( $self ) = @_;

    if( my $custom_subject = $self->forward_settings()->{subject} ){
        $log->info("Got custom subject '$custom_subject'");
        # Expand the custom subject with a stash
        # See https://juice.adcourier.com/stream_subject_variables.cgi

        my $user_details = $self->user_object->contact_details;
        if( ! $user_details && $self->user_object()->provider() =~ /\Aadcourier/ ){
            $user_details = $self->stream2->user_api->get_user($self->user_object()->provider_id()) // {};
        }

        my $stash = {
            contactname         => $self->user_object()->contact_name(),
            consultant          => $user_details->{consultant},
            consultant_email    => $self->user_object()->contact_email(),
            feedbackemail       => $user_details->{feedback_email},
            team                => $user_details->{team},
            office              => $user_details->{office},
            company             => $user_details->{company},
            boardid             => $self->subscription_info()->{'board_id'},
            nice_board_name     => $self->_board_nice_name,
            template_name       => $self->destination,
            eza_number          => $self->subscription_info()->{'eza_number'},
            custom_source_code  => $self->subscription_info()->{'custom_source_code'},
            method_id           => 3,
            method_name         => 'Search'
        };

        return $self->stream2()->templates()->render_simple( $custom_subject , $stash );
    }

    return sprintf('[%s] %s', $self->_board_nice_name, $self->stream2->__('Bulk Forward'));
}

sub _build_forward_settings {
    my ( $self ) = @_;
    if( $self->user_object->provider() =~ /\Aadcourier/ ){
        return $self->stream2->stream_api()->get_forward_email_settings($self->user_object()->provider_id()) // {};
    }
    return {};
}

sub build_forward_profile{
    my ($self, $candidate_object) = @_;
    my $profile_action = Stream2::Action::DownloadProfile->new({
        stream2             => $self->stream2(),
        stream2_base_url    => $self->stream2_base_url(),
        user_object         => $self->user_object(),
        ripple_settings     => $self->ripple_settings(),
        candidate_object    => $candidate_object,
        remote_ip           => $self->remote_ip(),
        log_chunk_id        => $self->log_chunk_id(),
        log_chunk_capture   => 0,
    });
    $profile_action->instance_perform();
    return $self->stream2->templates->profile_body(
        {
            L  => sub { Bean::Locale::localise_in( $self->user_object->locale(), @_ ); },
            candidate => $candidate_object
        }
    );
}

sub _build_candidates {
    my ( $self ) = @_;
    my $results = $self->stream2()->find_results($self->results_id());
    return [ map {
        $results->find($_);
    } @{$self->candidate_idxs} ];
}

sub _build_recipients {
    my ( $self ) = @_;

    my $s2 = $self->stream2();
    my @recipients =  $s2->factory('SearchUser')->search({id => {-in => $self->recipients_ids()}})->all();

    # SEAR-1086 A user with a CV-library subscription should only be able to forward a Candidate to another user only if they also
    # have a CV-library subscription.
    if( $self->destination eq 'cvlibrary' ){

        my @bad_recipients;
        @recipients = grep {$_->has_subscription($self->destination) || ( ( push(@bad_recipients, $_) ) && 0) }  @recipients;

        push @{$self->warnings}, map { $s2->__x(
            "{user_name} doesn't have a {board} user license. Please contact your administrator.",
            user_name => $_->contact_name,
            board => 'CV-Library'
        )} @bad_recipients;

        unless( scalar(@recipients) ){
            my $error_message = $s2->__x(
                "All of the users you are trying to forward this candidate to do not have a {board} user license. Please contact your administrator.",
                board => 'CV-Library'
            );
            die Stream::EngineException::ForwardingError->new( {message => $error_message})  ;
        }
    }

    return \@recipients;
}

sub _build_all_recipient_emails {
    my ( $self ) = @_;

    my @recipients = @{$self->recipients};
    
    # Consolidate recipients built from the recipent IDs, and the ones built from the email addresses.
    my @recipients_emails = map {$_->contact_email()} @recipients;
    push @recipients_emails , @{ $self->recipients_emails() };
    return [ grep { $_ } @recipients_emails ];
}

sub _build_subscription_info {
    my ( $self ) = @_;
    $self->user_object->has_subscription( $self->destination ) || {};
}

sub _board_nice_name {
    $_[0]->subscription_info()->{nice_name} // 'Unknown board name';
}

sub _build_destination {
    $_[0]->candidates->[0]->destination
}

__PACKAGE__->meta->make_immutable();

1;

package Stream2::Action::UserAdoptData;
use Moose;

extends qw/Stream2::Action/;

with qw/Stream2::Role::Action::HasUser/;

use Log::Any qw/$log/;

use Data::Dumper;

=head1 Stream2::Actions::UserAdoptData

The given user_object adopts ALL the DB data from the
given from_user.

=cut

has 'from_user' => ( is => 'ro', isa => 'Stream2::O::SearchUser', required => 1);

sub instance_perform {
    my ( $self )  = @_;

    my $to_user = $self->user_object();
    my $from_user = $self->from_user();
    my $s2 = $self->stream2();
    my $schema = $s2->stream_schema();

    unless( $to_user->group_identity() eq $from_user->group_identity() ){
        confess("Users ".$from_user->id()." and ".$to_user->id()." do not have the same group_identity");
    }

    $log->info("Transfering data from ".$from_user->id()." to ".$to_user->id());

    $log->info("Transfering candidate_action_log");
    # Nothing complicated here. Simple ownership relation
    $schema->resultset('CandidateActionLog')->search({ user_id => $from_user->id() })->update({ user_id => $to_user->id() });

    $log->info("Transfering candidate_tags");
    # Simple auditing belongs_to relationship
    $schema->resultset('CandidateTag')->search({ user_id => $from_user->id() })->update({ user_id => $to_user->id() });

    $log->info("Transfering user tags");
    my $usertags = $schema->resultset('Usertag');
    {
        # Which of the tags are the same between from and to users?
        my $duplicate_names = $usertags->search({ user_id => [ $from_user->id(), $to_user->id() ] },
                                                {
                                                    select => [ 'tag_name' , { count => 'tag_name', -as => 'count_names' } ],
                                                    group_by => [ 'tag_name' ],
                                                    having => { 'count_names' => 2 }
                                                }
                                            );
        while( my $dup = $duplicate_names->next() ){
            # Find the To user one and replace in the candidate tags.
            my $from_tag = $usertags->find({ user_id => $from_user->id(),
                                             tag_name => $dup->tag_name()
                                         });
            # Get rid of the from_tag
            $from_tag->delete();
        }
        $log->info("Dealt with duplicates. Time to change user tags ownership");
        $schema->resultset('Usertag')->search({ user_id => $from_user->id() })->update({ user_id => $to_user->id() });
    }

    $log->info("Transfering longlists");
    # Simple ownership belongs_to relationship
    $schema->resultset('LongList')->search({ user_id => $from_user->id() })->update({ user_id => $to_user->id() });

    $log->info("Transfering recurring_report_users");
    # Transfer only the reports that the to_user is not already subscribed to
    {
        my $already_subscribed = $schema->resultset('RecurringReportUser')->search({ user_id => $to_user->id()})->get_column('recurring_report_id');
        # Delete the ones that are already subscribed by the destination user.
        $schema->resultset('RecurringReportUser')->search({
            user_id => $from_user->id(),
            recurring_report_id => { -in => $already_subscribed->as_query() }
        })->delete();

        # And transfer ownership of the remaning ones.
        $schema->resultset('RecurringReportUser')->search({
            user_id => $from_user->id(),
        })->update({ user_id => $to_user->id() });

    }

    $log->info("Transferring saved_searches");
    # Simple ownership belongs_to relationship
    $schema->resultset('SavedSearches')->search({ user_id => $from_user->id() })->update({ user_id => $to_user->id() });

    $log->info("Transferring search_records");
    # Simple ownership relation
    $schema->resultset('SearchRecord')->search({ user_id => $from_user->id() })->update({ user_id => $to_user->id() });

    $log->info("Transferring sent_emails");
    # Simple ownership relation. Note that we already transfered the candidate_action_log data.
    $schema->resultset('SentEmail')->search({ user_id => $from_user->id() })->update({ user_id => $to_user->id() });

    $log->info("Transferring watchdogs");

    # For each from_user watchdog, look at the subsription and transfer to
    # an equivalent subscription belonging to the to_user if possible.
    my $wds = $schema->resultset('Watchdog')->search({ user_id => $from_user->id() });
    while( my $wd = $wds->next() ){
        my $wd_subs = $schema->resultset('WatchdogSubscription')
            ->search({ watchdog_id => $wd->id() },
                     { prefetch => 'subscription' });
        while( my $wd_sub = $wd_subs->next() ){
            # Try to find the equivalent subscription belonging to the to_user
            my $equiv = $schema->resultset('CvSubscription')->search({ user_id => $to_user->id(),
                                                                       board => $wd_sub->subscription()->board()
                                                                   })->first();
            unless( $equiv ){
                $log->info("No equivalent subscription belonging to user ".$to_user->id()." for board ".$wd_sub->subscription()->board().". Deleting this WD subscription");
                # Note that this should also delete all the watchdog results
                $wd_sub->delete();
                next;
            }
            # Found an equivalent subscription.
            $log->info("Found equivalent subscription for board ".$wd_sub->subscription()->board()." and user ".$to_user->id());
            $wd_sub->update({ subscription_id => $equiv->id() });
        }
    }
    # Then transfer straight watchdogs ownership
    $schema->resultset('Watchdog')->search({ user_id => $from_user->id() })->update({ user_id => $to_user->id() });

    $log->info("Transferring user_set_settings");
    $schema->resultset('UserSetting')->search({ admin_user_id => $from_user->id() })->update({ admin_user_id => $to_user->id() });

    return 1;
}

__PACKAGE__->meta->make_immutable();

package Stream2::Action::NewBBCSSResponse;

use Moose;
use Moose::Util;

use Stream2::Results::Result;

use Log::Any qw/$log/;

use Log::Log4perl::MDC;
use Log::Log4perl::NDC;

extends qw/Stream2::Action/;

# The api_url (the BBCSS url that sent the notification).
has 'api_url' => ( is => 'ro', isa => 'Str', required => 1 );

# The BBCSS document ID
has 'document_id' => ( is => 'ro', isa => 'Str', required => 1 );

# The BBCSS custome key
has 'customer_key' => ( is => 'ro', isa => 'Str', required => 1);

# The adcourier global user ID holding this application.
has 'broadbean_adcuser_id' => ( is => 'ro', isa => 'Str', required => 1 );

around 'build_context' => sub{
    my ($orig, $self) = @_;
    return {
        %{ $self->$orig() },
        # We want potential template sending actions to
        # have not 'message' as their name in the log,
        # but 'message_auto'.
        message_action_log_name => 'message_auto',
    };
};



# Note we override that, because applying dynamic roles
# causes the default implementation of this method to return the anonymous classes.
sub macro_action_name{ return __PACKAGE__; }



sub BUILD{
    my ($self, $args) = @_;

    $log->infof('Got newresponse notification with api_url=%s, document_id=%s, customer_key=%s, broadbean_adcuser_id=%s',
                $self->api_url(),
                $self->document_id(),
                $self->customer_key(),
                $self->broadbean_adcuser_id() );

    my $stream2 = $self->stream2();
    my $owner = $self->stream2()->factory('SearchUser')->find({ provider => 'adcourier',
                                                                provider_id => $self->broadbean_adcuser_id()
                                                            });
    unless( $owner ){
        $log->info("No local user found for adcourier user id = ".$self->broadbean_adcuser_id());
        return $self;
    }
    Log::Log4perl::MDC->put('sentry_user', $owner->short_info_hash());
    Log::Log4perl::NDC->push( $owner->group_identity() );
    Log::Log4perl::NDC->push( $owner->contact_email() );
    $log->info("Found application owner id=".$owner->id().' '.$owner->contact_email());

    # The whole point of this is to run macros on this action/aka event
    # do a quick check for macros before hitting the BBCSS API
    # Note that this optimisation is only valid because 'instance_perform' does
    # not do anything.
    unless( $self->stream2()->factory('GroupMacro')
                ->search({ group_identity => $owner->group_identity(),
                           on_action => $self->macro_action_name()
                       })->count() ){
        $log->info('No macro defined for this action for group_identity='.$owner->group_identity().' and on_action='.$self->macro_action_name().' No point continuing');
        return $self;
    }
    $log->info("Found macros for group identity = '".$owner->group_identity()."'");


    # Time to build the candidate object.
    # Find the response in the bbcss, according to the given api url.
    my $cs_document = $self->stream2()->build_bbcss_api({ base_url => $self->api_url() })
        ->get_document( $self->customer_key(),
                        $self->document_id(),
                        [ 'document_files' ]
                    );

    unless( $cs_document ){
        confess("Cannot find matching cs_document");
    }
    unless( $cs_document->{broadbean_group_identity} eq $owner->group_identity() ){
        $log->error("Inconsistency. User has got group_identity='".$owner->group_identity()."' but document says:".$cs_document->{broadbean_group_identity});
        return $self;
    }

    # Time for roles applying

    $log->info("Applying role HasUser");
    Moose::Util::apply_all_roles( $self,
                                  'Stream2::Role::Action::HasUser',
                                  {
                                      rebless_params => {
                                          user_object => $owner,
                                          # Needed in case some downstream macros are run later.
                                          user => { user_id => $owner->id() },
                                          ripple_settings => {},
                                      }
                                  }
                              );


    $log->info("Applying role TriggerMacros");
    # The whole point of this action is to trigger macros.
    Moose::Util::apply_all_roles( $self, 'Stream2::Role::Action::TriggerMacros' );

    $log->info("Applying role HasFeed");
    Moose::Util::apply_all_roles( $self,
                                  'Stream2::Role::Action::HasFeed'
                              );

    # Ok got consistent document with user.
    # Lets build a Result.
    my $result = Stream2::Results::Result->new();
    $result->attributes( $self->feed()->scrape_candidate_details( $cs_document ) );
    $result->destination( 'adcresponses' );
    $result->candidate_id( $self->document_id() );

    # use scrape_candidate_details of the FEED on $result.
    # AND scrape_fixup_candidate on it.
    $self->feed()->scrape_fixup_candidate( $result );

    $log->info("Applying role HasCandidate");
    Moose::Util::apply_all_roles( $self,
                                  'Stream2::Role::Action::HasCandidate',
                                  {
                                      rebless_params => {
                                          candidate_object => $result
                                      }
                                  }
                              );


    $log->info("Applying role HasAdvert");
    Moose::Util::apply_all_roles($self,
                                 'Stream2::Role::Action::HasAdvert',
                                 {
                                     rebless_params => {
                                         advert_id => $cs_document->{'broadbean_adcadvert_id'},
                                         list_name => 'adc_shortlist',
                                     }
                                 });


    return $self;
}

sub template_name{
    my ($self) = @_;
    return 'adcresponses';
}

sub instance_perform {
    my ( $self ) = @_;
    $log->info("Performing instance");
    # Note that does not do anything as this action is just there to trigger macros.
    return 1;
}

__PACKAGE__->meta->make_immutable();

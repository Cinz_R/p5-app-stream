package Stream2::Action::Search;

=head1 NAME

Stream2::Action::Search

=head1 DESCRIPTION

For running searches asynchronously or synchronously (rarer)

=head1 SYNOPSIS

    my $search = Stream2::Action::Search->new({
        criteria_id     => 'abcdef123456',
        user_id         => 5555,
        template_name   => 'adcresponses',
    });
    my Stream::Results::Results $results = $search->instance_perform();

=cut

use Moose;
use Log::Any qw/$log/;

extends qw/Stream2::Action::SearchBase/;

with qw/
    Stream2::Role::Action::HasRemoteIP
    Stream2::Role::Action::InManageError
    Stream2::Role::Action::InLogChunk
/;

has '+on_managed_error' => (
    default => sub {
        my ( $self ) = @_;
        return sub {
            my $err = shift;
            $self->report_error( $err->error_calendar_id, $err->human_message() );
        };
    }
);

has '+on_not_managed_error' => (
    default => sub {
        my ( $self ) = @_;
        return sub {
            my $err = shift;
            $self->report_error( 'UNKNOWN INTERNAL', $err );
        };
    }
);


=head2 instance_perform

This method is NOT supposed to die with anything else than a string, whose purpose is to be displayed
as-is to the end users. So this will do all the error handling here.

=cut

sub instance_perform{
    my ($self, $job) = @_;

    $self->before_search();

    my Stream2::Results::Store $results = $self->run_search();

    # Save the results object in its underlying Redis.
    $results->save();

    # Complete the job here. This allows the front end
    # to poll this job result straight away, without waiting
    # for the various reporting things to happen.
    if ( defined( $job ) ) {
        my $RESULT_DUMP = $results->dump();
        $RESULT_DUMP->{total_results} += 0; # Make sure the total_results is a number.
        $job->complete_job( $RESULT_DUMP );
    }

    $self->after_search( $results );

    # Our client facing reports DB
    # only increment adcourier reports if they are an adcourier user
    if ( $self->user_object->provider =~ m/\Aadcourier/i ){
        $log->info( 'Queueing event incrementation...' );
        $self->stream2()->increment_analytics(
            user_id             => $self->user_object->provider_id,
            board               => $self->template_name,
            results             => $results->total_results,
            threads             => 1,
            successful_threads  => 1,
        );
        $log->info( 'Event incrementation queued.' );
    }

    return $results;
}

__PACKAGE__->meta->make_immutable();

1;

package Stream2::Action::DownloadProfile;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;

use URI::Escape qw//;
use HTML::Entities qw//;

with qw/
           Stream2::Role::Action::InManageError
           Stream2::Role::Action::HasUser
           Stream2::Role::Action::HasRemoteIP
       /;

has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

has 'session_token' => ( is => 'ro', isa => 'Str', required => 0 );

with qw/Stream2::Role::Action::HasFeed/;

with qw/Stream2::Role::Action::TriggerMacros

        Stream2::Role::Action::HasCandidate

        Stream2::Role::Action::FastJob
        Stream2::Role::Action::InLogChunk
       /;

has [ '+on_managed_error', '+on_not_managed_error' ] => ( default => sub {
    my $self = shift;
    return sub {
        $self->stream2()->action_log->insert({
            action          => "profile_fail",
            candidate_id    => $self->candidate_object()->id(),
            destination     => $self->candidate_object()->destination(),
            user_id         => $self->user_object->id,
            group_identity  => $self->user_object->group_identity,
            search_record_id => $self->candidate_object->search_record_id(),
            data            => { s3_log_id => $self->log_chunk_id(),
                                 $self->candidate_object()->action_hash()
                             },
        });
    }
});

=head2 instance_perform

Returns an 'Enriched' Stream2::Results::Result from the given parameters
using the Feed's download_profile method.

=cut

sub instance_perform {
    my ( $self ) = @_;
    my $profile;
    my $quota_obj = $self->stream2->quota_object;
    my $candidate_object = $self->candidate_object();
    my $board = $candidate_object->destination();
    my $quota_summary = $quota_obj->fetch_quota_summary(
          {   path => $self->user_object->oversees_hierarchical_path(),
              board => $board,
              type => 'search-profile-or-cv',
          }
      );
    if ($self->user_object->has_done_action( 'profile', $candidate_object )
        || ( $quota_summary->{ $board }->{ 'search-profile-or-cv' }    # user has search-profile-or-cv quota
            && $self->user_object->has_done_action( 'download', $candidate_object )    # and has already downloaded the cv.
            )
        ){
        $log->info(
            "User " . $self->user_object->id() . " has already viewed candidate " . $candidate_object->id() . ". NOT checking quotas" );

        # This modifies 'candidate_object' in place.
        $self->feed->download_profile( $candidate_object );
    }
    else {
        # This too, but under quota guard.
        $self->feed->quota_guard( [ 'profile-or-cv', 'profile' ], sub { $self->feed->download_profile( $candidate_object ); } );
    }

    $self->stream2()->action_log->insert({
        action           => 'profile',
        candidate_id     => $candidate_object->id(),
        destination      => $candidate_object->destination(),
        user_id          => $self->user_object->id,
        group_identity   => $self->user_object->group_identity,
        search_record_id => $candidate_object->search_record_id(),
        data             => { s3_log_id => $self->log_chunk_id(),
                             $candidate_object->action_hash()
                         },
    });

    if(  ( $candidate_object->attr('cv_mimetype' ) || '' ) eq 'application/pdf' ){
        my $candidate_cv_url = '/'.$self->candidate_relative_url().'/cv';
        if( $self->session_token() ){
            $candidate_cv_url .= '?STOK='.$self->session_token();
        }
        my $frame_src = HTML::Entities::encode_entities( $self->stream2_base_url().'static/pdfjs/web/viewer.html?file='.URI::Escape::uri_escape_utf8( $candidate_cv_url ) );
        $log->info("PDFJS frame source = ".$frame_src);
        $candidate_object->attr('cv_html', '<iframe class="pdfjs_viewer" src="'.$frame_src.'"></iframe>');
    }
    return $candidate_object->to_ref();
}




__PACKAGE__->meta->make_immutable();

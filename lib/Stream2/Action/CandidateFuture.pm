package Stream2::Action::CandidateFuture;

use Moose;
use Log::Any qw/$log/;

extends qw/Stream2::Action/;

=head1 NAME

Stream2::Action::CandidateFuture - An action to register something to be done in the future about a candidate

=cut

with qw/
           Stream2::Role::Action::HasUser
           Stream2::Role::Action::HasCandidate
           Stream2::Role::Action::InLogChunk
       /;

has 'longstep_process_id' => ( is => 'ro', isa => 'Int', required => 1 );
has 'at_time' => ( is => 'ro', isa => 'Str', required => 1 );
has 'lisp_code' => ( is => 'ro', isa => 'Maybe[Str]' );


sub instance_perform {
    my ( $self ) = @_;

    $log->info("Recording the future action against this candidate");
    # Log the action
    $self->stream2()->action_log->insert({
        action          => "candidate_future",
        candidate_id    => $self->candidate_object()->id,
        destination     => $self->candidate_object()->destination(),
        user_id         => $self->user_object->id,
        group_identity  => $self->user_object->group_identity,
        search_record_id => $self->candidate_object->search_record_id(),
        data            => {
            s3_log_id => $self->log_chunk_id(),
            longstep_process_id => $self->longstep_process_id(),
            at_time => $self->at_time(),
            lisp_code => $self->lisp_code()
        },
    });

    return 1;
}

__PACKAGE__->meta->make_immutable();

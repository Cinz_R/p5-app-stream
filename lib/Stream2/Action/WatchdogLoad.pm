package Stream2::Action::WatchdogLoad;

use Moose;
extends qw/Stream2::Action::SearchBase/;

use Stream2::Results::Store;
use Log::Any qw/$log/;
use DateTime;
use Scalar::Util ();

=head1 NAME

Stream2::Action::WatchdogLoad;

=head1 DESCRIPTION

Based off the standard Search logic, this module spices things
up by persisting result data in our RDS and adding additional information
to our various analytics.

For a normal watchdog ( auto_run = 1, init = 0 )
- Run a search
- Compare results to those persisted in the RDS
- Add any new results to the RDS, mark them as *unread*
- Update our search_record table with data = { watchdog: 1, s3_log_id: ...,  }
- Post to error calendar with { watchdog: 1, ... }
- Return a dump of each result

For a watchdog initialise ( auto_run = 1, init = 1 )
- Run a search
- Add any new results to the RDS, mark them as *read*
- Update our search_record table with data = { watchdog: 1, s3_log_id: ... }
- Return a dump of each result

For a watchdog view ( auto_run = 0, init = 0 )
- Run a search ( if page 1, otherwise load results from the RDS )
- Compare results to those persisted in the RDS
- Add any new results to the RDS, mark them as *unread*
- Update our search_record table with data = { s3_log_id: ... }
- Post to error calendar as standard search ( no watchdog flag )
- Stash results in Redis for viewing on the front end

=cut

with qw/
    Stream2::Role::Action::HasRemoteIP
    Stream2::Role::Action::InManageError
    Stream2::Role::Action::InLogChunk
/;

has '+on_managed_error' => (
    default => sub {
        my ( $self ) = @_;
        return sub {
            my $err = shift;
            $self->report_error( $err->error_calendar_id, $err->human_message() );
        };
    }
);

has '+on_not_managed_error' => (
    default => sub {
        my ( $self ) = @_;
        return sub {
            my $err = shift;
            $self->report_error( 'UNKNOWN INTERNAL', $err );
        };
    }
);

# params
has 'watchdog_id'           => ( is => 'ro', isa => 'Maybe[Int]' );
has 'watchdog'              => ( is => 'ro', lazy_build => 1 );

sub _build_watchdog {
    my ( $self ) = @_;
    return $self->stream2->factory('Watchdog')->find($self->watchdog_id);
}


has 'watchdog_subscription' => ( is => 'ro', lazy_build => 1 );

sub _build_watchdog_subscription {
    my ( $self ) = @_;
    return $self->watchdog->watchdog_subscriptions->find(
        {
            'subscription.board' => $self->template_name
        },
        {
            join    => 'subscription'
        }
    );
}

has '+criteria_id'          => ( lazy_build => 1 );

sub _build_criteria_id {
    $_[0]->watchdog->criteria_id;
}

# set the last run error for this subscription to this error
override report_error => sub {
    my ( $self, $type, $message ) = @_;
    super();

    # only report this in the subscription table if this is
    # a nightly run of the watchdog
    if ( $self->options->{auto_run} ) {
        $self->watchdog_subscription->last_run_error($message);
        $self->watchdog_subscription->update();
    }
};

# If this is the auto-run watchdog, then mark it as such in our database.
around [qw/_build_search_record_data _build_common_search_log/] => sub {
    my $orig = shift;
    my $self = shift;

    return {
        %{$self->$orig( @_ )},
        ( $self->options->{auto_run} ? ( watchdog => 1 ) : () )
    };
};

override _build_results_collector => sub {
    my ( $self ) = @_;
    if ( $self->options->{auto_run} ) {
        return Stream2::Results->new({
            destination => $self->template_name
        });
    }
    return super();
};

=head2 WatchdogLoad

    an on-demand method to perform a particular search within the context of a watchdoga
    if the first page is requested we shall do a refresh of the watchdog to ensure newest results

    This will ALWAYS be performed in an interactive way, via the interface. This is important
    to inforce, as some feed (CVLibrary, I'm looking at you) will NOT play well being executed
    as watchdog AND as interactive in the same time span.

=cut

sub instance_perform {
    my ( $self, $job ) = @_;

    # ensure we have a page number greater than 0
    $self->options->{page} ||= 1;

    # When they load the results initially for the watchdog, re-run it to get the super freshest results
    if ( $self->options->{page} == 1 ){

        $self->before_search();
        my $results = $self->run_watchdog();
        $self->after_search( $results );

        my $new_results = $self->_filter_new_results( $results );
        if ( $self->options->{auto_run} ){
            return $new_results;
        }

        # calling results->save sends the search result to redis ( not to be confused with the actual results
        # which are added to redis while being scraped by the engine), this isn't necessary if this is an
        # automated search
        $results->save();
    }

    # load watchdog from mysql into a new result array in redis
    my $results_per_page = 30;
    my $results_collector = $self->to_results({
        per_page    => $results_per_page,
        page        => $self->options->{page},
        remote_ip   => $self->remote_ip,
    });

    # Our client facing reports DB
    # only increment adcourier reports if they are an adcourier user
    if ( $self->user_object->provider =~ m/\Aadcourier/i ){
        $log->info( 'Queueing event incrementation...' );
        $self->stream2()->increment_analytics(
            user_id             => $self->user_object->provider_id,
            board               => $self->template_name,
            results             => $results_collector->total_results,
            threads             => 1,
            successful_threads  => 1,
        );
        $log->info( 'Event incrementation queued.' );
    }

    my $dump = $results_collector->dump();

    $dump->{total_results} += 0;
    return $dump;
};

around '_build_criteria_tokens' => sub {
    my $orig = shift;
    my $self = shift;
    my $tokens = $self->$orig( @_ );

    unless ( $self->options->{init} ) {
        $tokens->{cv_updated_within} = 'TODAY';
    }

    return $tokens;
};

sub run_watchdog {
    my ( $self ) = @_;

    my $s2      = $self->stream2;
    my $wd_sub  = $self->watchdog_subscription();

    # We are interactively loading a watchog, set the last_viewed_datetime to now.
    $self->watchdog->last_viewed_datetime( DateTime->now() );
    $self->watchdog->update();

    if ( $self->options->{auto_run} ){
        $self->feed()->is_watchdog( 1 );
    }

    $self->feed()->creator_ip( $self->remote_ip );
    $wd_sub->last_run_datetime( DateTime->now() );

    $log->infof(
        "Running subscription [%s/%s] watchdog: %s %s subscription: %s",
        $wd_sub->watchdog_id,
        $wd_sub->subscription_id(),
        $wd_sub->watchdog->name(),
        $wd_sub->watchdog()->id(),
        $self->template_name()
    );

    my $results = $self->run_search();

    # if this is a nightly run, then record the outcome of the watchdog
    # in the subscription table
    if ( $self->options->{auto_run} ) {
        $wd_sub->last_run_error( undef );
        $wd_sub->update();
    }

    return $results;
}

sub _filter_new_results {
    my ( $self, $results ) = @_;
    my $wd_sub = $self->watchdog_subscription();

    my @new_results;
    # What are the new results?
    foreach my $result ( @{$results->results} ){
        my %data = %$result;
        my $watchdog_result = $wd_sub->find_or_new_related(
            'watchdog_results',
            {
                candidate_id    => $result->id(),
                viewed          => $self->options->{init} // 0
            }
        );
        unless ( $watchdog_result->in_storage ) {
            $log->info("Candidate ".$result->id()." is new for watchdog [".$self->watchdog->id."] subscription : ".$wd_sub->subscription_id);
            $watchdog_result->content( \%data );
            $watchdog_result->insert;
            push @new_results, $result;
        }
        else {
            $log->info(sprintf("Candidate %s has been found again for watchdog [%s] subsription [%s]", $result->id(), $self->watchdog->id, $wd_sub->subscription_id));
            $watchdog_result->content( \%data );
            $watchdog_result->previously_seen_datetime( $watchdog_result->last_seen_datetime );
            $watchdog_result->update;
        }
    }

    $log->info(
        "Done Running subscription [%s/%s] watchdog: %s %s subscription: %s",
        $wd_sub->watchdog_id,
        $wd_sub->subscription_id(),
        $wd_sub->watchdog->name(),
        $wd_sub->watchdog()->id(),
        $wd_sub->subscription()->board()
    );

    return \@new_results;
}


#offset( $page, $per_page );
sub offset { ( $_[0] - 1 ) * $_[1]; }

=head2 to_results

    Loads watchdog results from the RDS and returns a L<Stream2::Results> object
    This process includes storing the candidates in redis so they are useable by the front end

=cut

sub to_results {
    my ( $self, $options_ref ) = @_;

    my $s2                      = $self->stream2;
    my $watchdog_subscription   = $self->watchdog_subscription;
    my $board                   = $self->template_name;
    my $result_set              = $self->fetch_results();
    my $watchdog                = $self->watchdog;
    my $total_results           = $result_set->count() + 0;

    my $results_collector = Stream2::Results::Store->new({
        destination           => $board,
        store                 => $s2->redis(),
        per_page              => $options_ref->{per_page},
        total_results         => $total_results,
        results_per_scrape    => $options_ref->{per_page},
        current_page          => $options_ref->{page},
        search_record_id      => $self->search_record->id(),
        expires_after         => $s2->config->{results_expire_after} || '3h',
    });
    $results_collector->save();

    my $wd_results = $result_set->search_rs(undef , {
        rows    => $options_ref->{per_page},
        offset  => offset( $options_ref->{page}, $options_ref->{per_page} )
    });

    my @viewed_candidate_ids = map {
        my $candidate = $results_collector->new_candidate();
        my $last_seen = $_->last_seen_datetime || $_->insert_datetime;
        $candidate->attributes(
            # changing from viewed to _bb_wd_viewed because totaljobs has a candidate attribute of viewed which
            # collides with ours
            _bb_wd_viewed   => $_->viewed,
            time_found      => $last_seen->iso8601() . 'Z',
            candidate_id    => $_->candidate_id,
            %{$_->content || {}}
        );
        $results_collector->add_candidate( $candidate );
        $_->candidate_id();
    } $wd_results->all;

    $log->infof("There are %s candidates being viewed.", scalar(@viewed_candidate_ids));

    my $viewed_results = $watchdog_subscription->watchdog_results()->search({ candidate_id => \@viewed_candidate_ids });
    my $n_updated = $viewed_results->search({ viewed => 0 })->count();
    $viewed_results->update( { viewed => 1 });

    $log->info("Subscription New Result count: ".$watchdog_subscription->new_result_count());
    $log->info("Changed as viewed: ".$n_updated);

    # Reload the subscription from the DB so the
    # DB level trigger effects are felt
    $watchdog_subscription->discard_changes();
    $results_collector->new_wd_subscription_count( $watchdog_subscription->new_result_count() + 0 ); # Force number.
    $log->info("New susbscription New Result Count: ".$results_collector->new_wd_subscription_count());

    return $results_collector;
}

sub fetch_results {
    my ( $self ) = @_;
    return $self->watchdog_subscription->watchdog_results->search( { content => \'IS NOT NULL' }, {
        order_by    =>  [ \'COALESCE( last_seen_datetime, insert_datetime ) DESC', { -asc => [qw/candidate_id/] } ],
    });
}

__PACKAGE__->meta->make_immutable();

1;

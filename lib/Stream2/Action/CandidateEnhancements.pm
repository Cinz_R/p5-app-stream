package Stream2::Action::CandidateEnhancements;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;
use Data::Dumper;

with qw/
  Stream2::Role::Action::InManageError
  Stream2::Role::Action::InLogChunk
  Stream2::Role::Action::HasUser
  Stream2::Role::Action::HasCandidate
  Stream2::Role::Action::HasRemoteIP
  /;

has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

with qw/
           Stream2::Role::Action::HasFeed
       /;

has [ '+on_managed_error', '+on_not_managed_error' ] => ( default => sub {
    my $self = shift;
    return sub {
        $self->stream2()->action_log->insert({
            action          => "enhancement_fail",
            candidate_id    => $self->candidate_object()->id(),
            destination     => $self->candidate_object()->destination(),
            user_id         => $self->user_object->id,
            group_identity  => $self->user_object->group_identity,
            search_record_id => $self->candidate_object->search_record_id(),
            data            => { s3_log_id => $self->log_chunk_id(),
                                 $self->candidate_object()->action_hash()
                             },
        });
    }
});

=head2 instance_perform

Uses the feed to get additional information for a single candidate
and returns those details. Note the candidate is enriched in the
redis store too so this data is available through the resultset.

Additional information can come through from querying the board,
or any other way. All the feed is expected to do is return a
hash reference of the additional fields to attach to the
candidate.

=cut

sub instance_perform {
    my ( $self, $job )  = @_;
    my $stream2   = $self->stream2;
    my $user      = $self->user_object();
    my $candidate = $self->candidate_object();

    my $section   = $job->parameters->{enhancement_section}
        or die $stream2->__('Section not provided');

    my $method    = $section . '_enhancement_details';

    $log->infof(
        "Enhancing candidate object with id %s for section '%s'.",
        $candidate->id(), $section
    );

    my %enhancements = ();

    eval {
        %enhancements = %{ $self->feed->$method($candidate) };
    };

    if ( my $err = $@ ) {
        $log->warnf(
            "Unable to enhance candidates '%s' section, error was: %s",
            $section, $err
        );
        die $stream2->__('Unable to load additional information, try again later');
    }

    # set something for feeds to know the candidate was updated in a certain part of the app
    $enhancements{ $section . '_enhanced' } = 1;

    # update the result in redis
    my $updated_candidate = $self->stream2->find_results($job->parameters->{results_id})->update($self->candidate_idx(), \%enhancements);

    $log->info('Candidate now has the following attributes: ' . Dumper($updated_candidate->to_ref()) );

    $self->stream2()->action_log->insert({
        action          => 'enhancement',
        candidate_id    => $self->candidate_object()->id,
        destination     => $self->template_name,
        user_id         => $user->id,
        group_identity  => $user->group_identity,
        search_record_id => $self->candidate_object->search_record_id(),
        data            => { s3_log_id => $self->log_chunk_id(),
                             enhancement_section => $section,
                             $self->candidate_object()->action_hash(),
                         },
    });

    # we will pass back the attributes we wanted to add
    return { enhancements => \%enhancements };
}

__PACKAGE__->meta->make_immutable();
1;

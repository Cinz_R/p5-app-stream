package Stream2::Action::ClearWatchdogResults;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;

has 'watchdog_id' => ( is => 'ro', isa => 'Int', required => 1 );
has 'board_name'  => ( is => 'ro', isa => 'Maybe[Str]', required => 0 );

has 'watchdog' => ( is => 'ro', isa => 'Stream2::O::Watchdog', lazy_build => 1 );


sub _build_watchdog{
    my ($self) = @_;
    return $self->stream2()->factory('Watchdog')->find($self->watchdog_id());
}

=head2 instance_perform

Mark watchdog results as viewed. Either for the whole of a watchdog or
just for a board_name.

=cut

sub instance_perform {
  my ( $self ) = @_;

  if( $self->board_name ){
      # Clear results at board level
      my $wd_subscription = $self->watchdog()->watchdog_subscriptions()->search({ 'subscription.board' => $self->board_name() }, { join => 'subscription' })->first();
      unless( $wd_subscription ){
          confess(sprintf("No watchdog subscription found for %s  on watchog %s", $self->board_name(), $self->watchdog->id()));
      }
      $wd_subscription->watchdog_results()->update({ viewed => 1 });
      return { message => $self->stream2()->__("Watchdog results were successfully marked as viewed") };
  }

  # Clear results at watchdog level
  $self->stream2->factory('WatchdogResult')->search({ watchdog_id => $self->watchdog_id() })->dbic_rs()->update({ viewed => 1 });
  return { message => $self->stream2()->__("All Watchdog results were successfully marked as viewed") };
}


__PACKAGE__->meta->make_immutable();

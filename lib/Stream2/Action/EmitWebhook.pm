package Stream2::Action::EmitWebhook;

use strict;
use warnings;

use MIME::Base64 qw/decode_base64/;
use Log::Any qw/$log/;

=head2 EmitWebhook

Will fireoff webhooks to defined urls for given actions with candidate XML

The following job parameters must be defined:

url           - Webhook post URL
action_name   - Webhook action name
candidate_xml - base64 encoded candidate XML string

=cut

sub perform {
    my ( $class, $job ) = @_;

    my %params;

    for (qw/url action_name candidate_xml/) { 
        die "Stream2::Action::EmitWebhook - required param '$_' was not defined"
            unless defined $job->parameters->{$_};
        $params{$_} = $job->parameters->{$_};
    }

    my $candidate_xml = decode_base64($params{candidate_xml});

    $log->info("Stream2::Action::EmitWebhook - JobId(" . $job->guid() . ") - posting candidate XML to: " . $params{url});

    $log->trace("XML= $candidate_xml") if $log->is_trace();

    my $res = $job->{stream2}->user_agent->post(
        $params{url},
        Content_Type => 'text/xml',
        Content => $candidate_xml,
    );

    unless( $res->is_success() ) {
        $log->error(
            "Stream2::Action::EmitWebhook - JobId(" . $job->guid() . ") - Failed to post candidate XML to '" . $params{url}
            . "' for action: '" . $params{action_name} . "' - "
            . $res->status_line() . " - "
            . "Candidate XML:: " . substr($candidate_xml , 0 , 1000)
        );
        return 0;
    }

    $job->complete_job($res->decoded_content);
    return 1;
}

1;

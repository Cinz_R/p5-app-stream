package Stream2::Action::UpdateCandidate;
use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;

with qw/
           Stream2::Role::Action::InManageError

           Stream2::Role::Action::HasUser
       /;

has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

with qw/
        Stream2::Role::Action::HasFeed

        Stream2::Role::Action::TriggerMacros

        Stream2::Role::Action::HasCandidate

        Stream2::Role::Action::FastJob
        Stream2::Role::Action::InLogChunk
       /;

use Data::Dumper;

# fields are a hashref of candidate fields being updated in this action
# Example:
#
# { field1 => 'new value' , field2 => 'new value2' }
#
has 'fields' => ( is => 'ro', isa => 'HashRef', required => 1 );

around 'build_context' => sub{
    my ($orig, $self) = @_;

    return { %{ $self->$orig() },
             fields => $self->fields(),
         };
};

=head2 instance_perform

As the user, this allows for the candidate details to be updated.

=cut

sub instance_perform {
  my ($self) = @_;

  my $candidate = $self->candidate_object;
  my $feed      = $self->feed;

  my @changed_values = map{ { field_name  => $_,
                              new_value   => ref($self->fields->{$_}) eq 'ARRAY' ? join (' ', @{$self->fields->{$_}}) : $self->fields->{$_},
                              old_value   => scalar $candidate->attr($_),
                             }} keys %{$self->fields};

  $log->info("Updating candidate ".$candidate->id()." with ".Dumper(@changed_values));
  $feed->update_candidate( $candidate, $self->fields );

  # Log the action
  $self->stream2()->action_log->insert({
      action => 'update',
      candidate_id => $self->candidate_object()->id(),
      destination  => $self->candidate_object()->destination(),
      user_id      => $self->user_object()->id(),
      group_identity => $self->user_object()->group_identity(),
      search_record_id => $self->candidate_object->search_record_id(),
      data => {
                s3_log_id      => $self->log_chunk_id(),
                updated_fields => \@changed_values,
                $self->candidate_object()->action_hash()
      }
  });

  return { message => $self->stream2()->__('Your change to this candidate has been submitted') };
}



1;

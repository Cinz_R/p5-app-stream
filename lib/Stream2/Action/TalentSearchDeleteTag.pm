package Stream2::Action::TalentSearchDeleteTag;

use strict;
use warnings;

use Log::Any qw/$log/;

=head2 WatchdogInitialise

Some time between a user setting up a watchdog and us running it for the first time
we should record the candidates currently available in the search as already seen

=cut

sub perform {
    my ( $class, $job ) = @_;
    my $s2 = $job->{stream2};

    my $parameters = $job->parameters();

    my $tag_name = $parameters->{tag_name};
    my $user =   $s2->find_user( $parameters->{user_id} );

    $log->info("Action TalentSearchDeleteTag has started");

    if( $user->has_subscription('talentsearch') ){
        my $ts_feed = $s2->build_template('talentsearch',
                                          undef,
                                          {company => $user->group_identity(),
                                           user_object => $user
                                          }
                                         );
        $ts_feed->untag_candidate_bulk($tag_name);
    }

    return 1;
}

1;

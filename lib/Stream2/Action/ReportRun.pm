package Stream2::Action::ReportRun;

use strict;
use warnings;

use Log::Any qw/$log/;

=head2 perform

  Runs a recurring report from the given job. Nothing more.

=cut

sub perform {
    my ( $class, $job ) = @_;
    my $s2 = $job->{stream2};

    my $report_id = $job->parameters->{'report_id'};

    eval{
        my $report = $s2->factory('RecurringReport')->find($report_id);
        $report->run_this();
    };
    if( my $err = $@ ){
        $log->error("Unmanaged error running recurring report ".$report_id." : $err");
    }

    return 1;
}

1;

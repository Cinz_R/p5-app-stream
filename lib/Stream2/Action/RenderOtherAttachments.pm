package Stream2::Action::RenderOtherAttachments;

use Moose;

extends qw/Stream2::Action/;

use Log::Any qw/$log/;

with qw/
  Stream2::Role::Action::InManageError
  Stream2::Role::Action::HasUser
  Stream2::Role::Action::HasCandidate
  Stream2::Role::Action::HasRemoteIP
  /;

has 'template_name' => ( is => 'ro', isa => 'Str', lazy_build => 1);
sub _build_template_name{
    return shift->candidate_object()->destination();
}

with qw/
           Stream2::Role::Action::HasFeed
       /;

=head2 instance_perform

Renders other attachments associated with a candidate.

Other attachments are given by the Feed's rendered_attachments method.

=cut

sub instance_perform {
    my ( $self )  = @_;
    my $user      = $self->user_object();
    my $candidate = $self->candidate_object();
    $log->infof(
        "Processing Other from Attachments for candidate %s for user %s.",
        $candidate->id(), $user->id()
    );

    my $attachments = $self->feed->rendered_attachments($candidate);
    $log->infof(
        "Done Processing Other from Attachments for candidate %s.",
        $candidate->id()
    );

    return { other_attachments => $attachments };
}

__PACKAGE__->meta->make_immutable();
1;

package Stream2::Action::WatchdogRun;

use strict;
use warnings;

use Log::Any qw/$log/;
use Scalar::Util qw();

=head2 WatchdogInitialise

Some time between a user setting up a watchdog and us running it for the first time
we should record the candidates currently available in the search as already seen

=cut

sub perform {
    my ( $class, $job ) = @_;
    my $s2 = $job->{stream2};

    my $watchdog_id = $job->parameters->{'watchdog_id'};
    my $real_run = $job->parameters->{real_run};

    $log->info("Running watchdog (".( $real_run ? 'REAL RUN' : 'INITIALIZATION' ).") [".$watchdog_id."]");

    eval{
        my $watchdog = $s2->factory('Watchdog')->find($watchdog_id);
        my $runner = $s2->watchdogs->runner( $watchdog );

        unless( $real_run ){
            $runner->initialize();
        }else{
            $runner->run();
        }

        $log->info("Done running  watchdog (".( $real_run ? 'REAL RUN' : 'INITIALIZATION' ).") [".$watchdog_id."]");
    };
    if ( my $err = $@ ) {

        if ( Scalar::Util::blessed($err)
            && $err->isa('Stream::EngineException') )
        {
            $err = $err->human_message();
        }
        $log->errorf( "An error occured while running watchdog %s: %s",
            $watchdog_id, $err );
    }

    return 1;
}

1;

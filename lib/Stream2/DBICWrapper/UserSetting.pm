package Stream2::DBICWrapper::UserSetting;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

use Log::Any qw/$log/;

use Stream2::O::UserSetting;


sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::UserSetting->new({ o => $o , factory => $self });
}

__PACKAGE__->meta->make_immutable();

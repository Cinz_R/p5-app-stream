package Stream2::DBICWrapper::LongList;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Stream2::O::LongList;

=head1 NAME

Stream2::DBICWrapper::LongList - A long list wrapper that will check for things.

=cut

has 'longlists_limit' => ( is => 'ro' , isa => 'Int' ,lazy_build => 1);

sub _build_longlists_limit{
    my ($self) = @_;
    return $self->stream2->config()->{longlists_limit} // 10;
}

around 'create' => sub{
    my ($orig, $self, $properties ) = @_;
    $properties //= {};

    my $user_id = $properties->{user_id};
    unless( $user_id ){ confess("Missing user_id in properties"); }

    my $schema = $self->stream2->stream_schema();
    my $stuff = sub{
        my $user = $schema->resultset('CvUser')->find($user_id);
        unless( $self->user_can_create_longlist( $user ) ){
            confess("More than ".$self->longlists_limit()." longlists for user $user_id. Cannot create a new one");
        }
        return $self->$orig($properties);
    };
    return $schema->txn_do($stuff);
};

sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::LongList->new({ o => $o , factory => $self });
}

=head2 user_can_create_longlist

Can the given L<Search2::O::SearchUser> create a longlist?

=cut

sub user_can_create_longlist{
    my ($self , $user ) = @_;
    return $user->longlists()->count() < $self->longlists_limit();
}

__PACKAGE__->meta->make_immutable();

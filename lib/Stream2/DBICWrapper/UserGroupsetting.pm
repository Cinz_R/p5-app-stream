package Stream2::DBICWrapper::UserGroupsetting;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

use Log::Any qw/$log/;

use Stream2::O::UserGroupsetting;

sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::UserGroupsetting->new({ o => $o , factory => $self });
}

__PACKAGE__->meta->make_immutable();

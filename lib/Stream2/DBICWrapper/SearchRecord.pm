package Stream2::DBICWrapper::SearchRecord;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

with qw/Stream2::Role::DBICWrapper::BinarySearch/;

=head1 NAME

Stream2::DBICWrapper::SearchRecord

=cut

__PACKAGE__->meta->make_immutable();

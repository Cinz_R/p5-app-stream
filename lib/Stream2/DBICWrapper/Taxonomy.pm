package Stream2::DBICWrapper::Taxonomy;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Carp;
use Data::Dumper;
use Log::Any qw/$log/;

=head1 NAME

Stream2::DBICWrapper::Taxonomy - A factory around the Taxonomy resultset

=cut

=head2 store_taxonomies(ArrayRef $taxonomies)

 $s2->factory('Taxonomy')->store_taxonomies($taxonomies);

Will take the taxonomies provided and store them in the taxonomy table with the type provided.

=cut

sub store_taxonomies {
    my ($self, $taxonomies) = @_;
    confess("taxonomies are required") unless $taxonomies;

    foreach my $taxonomy ( @$taxonomies ) {
        $log->info('Storing taxonomy: ' . Dumper($taxonomy));
        my $result = $self->dbic_rs()->find_or_new($taxonomy);
        if ( !$result->in_storage ) {
            $result->insert;
        }
        else {
            $result->update($taxonomy);
        }
    }

    return 1;
}

=head2 queue_taxonomy_job

Queues the storing of careerbuilders taxonomies into the taxonomy table

=cut

sub queue_taxonomy_job {
    my ($self) = @_;
    my $job = $self->stream2->qurious->create_job(
        class => 'Stream2::Action::StoreCBTaxonomies',
        queue => 'BackgroundJob',
        parameters => {}
    );
    $job->enqueue();
}

__PACKAGE__->meta->make_immutable();

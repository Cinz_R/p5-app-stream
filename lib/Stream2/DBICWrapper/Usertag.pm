package Stream2::DBICWrapper::Usertag;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;
use Stream2::O::Usertag;
use Text::CSV;

=head1 NAME

Stream2::DBICWrapper::Usertag - A factory around the Usertag resultset

=cut

=head2 wrap

Wraps any result with a Stream2::O::Usertag

=cut

sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::Usertag->new({ o => $o , factory => $self });
}


=head2 autocomplete

Returns a Resultset of tags matching the given stem for the given user, alpha order.

Usage:

 my $tags = $this->autocomplete($group_identity, $stem);

=cut

sub autocomplete{
    my ($self, $user, $stem ) = @_;

    $stem //= '';

    my $schema = $self->stream2()->stream_schema();

    # Escaping the stem (allows for tags to contain '%')
    # See https://metacpan.org/pod/DBI , section 'Further information'

    return $self->search(
        {
            'me.user_id' => $user->id(),
            tag_name_ci => { like => $schema->escape_like($stem).'%' }
        },
        {
            order_by  => [ { -asc => 'tag_name' } ],
        }
    );
}

=head2 enrich_candidates

Enriches the given candidates S2::Results::Result with stored tags (if they haven't got any yet) for the current user.

Usage:

 $this->enrich_candidates( $results , { user => $search_user });

=cut

sub enrich_candidates{
    my ($self, $results, $opts) = @_;
    $opts //= {};

    unless( $opts->{user} ){ confess("Missing user in $opts"); }

    my $user = $opts->{user};
    my $schema = $self->stream2->stream_schema();
    die "NOT IMPLEMENTED YET";
}


__PACKAGE__->meta->make_immutable();

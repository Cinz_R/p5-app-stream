package Stream2::DBICWrapper::SentEmail;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

use Log::Any qw/$log/;

use Stream2::O::SentEmail;

=head1 NAME

Stream2::DBICWrapper::SenEmail - SentEmail factory

=cut

sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::SentEmail->new({ o => $o, factory => $self });
}

# columns to select/appear in resultsets
my @COLUMN_NAMES = qw/me.id me.sent_date me.subject me.recipient
                      me.recipient_name user.contact_name/;

=head2 search_user_emails

Applies a filter to ensure only emails for the given L<Stream2::O::SearchUser>
are returned.

Usage:

    my rs = $this->search_user_emails( $user, 'php developer', { rows => 5, page => 2 } );

=cut

sub search_user_emails {
    my ( $self, $user, $keywords, $options ) = @_;
    my $filter = { user_id => $user->id, group_identity => $user->group_identity };
    return $self->_search_emails( $keywords, $filter, $options );
}

=head2 search_user_emails

Applies a filter to ensure only emails for the given L<Stream2::O::SearchUser> 's group
are returned.

Usage:

    my $rs = $this->search_group_emails( $user, 'php developer', { rows => 5, page => 2 } );

=cut

sub search_group_emails {
    my ( $self, $user, $keywords, $options ) = @_;
    my $filter = { group_identity => $user->group_identity };
    return $self->_search_emails( $keywords, $filter, $options );
}

=for comment

finds email messages by keyword(s). Returns a DBIx::Class::ResultSet
containing matched emails.

=cut

sub _search_emails {
    my ( $self, $keywords, $filter, $options ) = @_;

    my $schema = $self->stream2->stream_schema;

    if ( $keywords ) {
        $keywords        = $schema->escape_like( $keywords );
        my $match_clause = $schema->with_sqllite(
            sub { return "subject LIKE '%$keywords%' OR plain_body LIKE '%$keywords%'" },
            sub { return "MATCH (subject, plain_body) AGAINST ('$keywords')" }
        );
        $filter->{'-or'} = [
            recipient           => { LIKE => "$keywords%" },
            recipient_name      => { LIKE => "$keywords%" },
            'user.contact_name' => { LIKE => "$keywords%" },
            subject             => { LIKE => "$keywords%" },
            \$match_clause
        ];
    }

    return $self->search_rs(
        $filter,
        {
            page     => $options->{page},
            rows     => $options->{rows},
            order_by => 'id',
            order_by => { -desc => ['me.id'] },
            join     => 'user',
            collapse => 1, # newer version of prefetch
            columns  => \@COLUMN_NAMES
        }
    );
}

=head2 get_email

Returns an instance of Result::SentEmail representing the email identified by 
$message_id. If $user is defined, the message_id must also have been sent by
that user.

=cut

sub get_email {
    my ( $self, $message_id, $user ) = @_;

    # we could just find(), based on id alone. But we want to additionally
    # constrain the search to the user ID to prevent retrieval of another
    # another user's message!
    return $self->search_rs(
        {
            id => $message_id,
            $user ? ( user_id => $user->id ) : ()
        },
        { rows => 1, columns => ['aws_s3_key'] }
    )->single;
}

=head2 get_email_by_s3

Returns an instance of Result::SentEmail when searched by the aws-s3 key UUID.
This will return undef if there UUID does not conform to the RFC standard

=cut

sub get_email_by_s3 {
    my ( $self, $s3_id) = @_;

    # do some validation for the UUID
    return undef if $s3_id !~ /\w{8}-\w{4}\-\w{4}\-\w{4}-\w{12}/;
    my $uuid   = $self->stream2->factory('PrivateFile')->compose_key($s3_id);
    my $result = $self->find( {
            aws_s3_key  => $uuid
    });
    return $result;
}

__PACKAGE__->meta->make_immutable();

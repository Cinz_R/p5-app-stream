package Stream2::DBICWrapper::SettingBase;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

=head1 NAME

Stream2::DBICWrapper::SettingBase - A base class for Setting and GroupSetting

=cut

use Log::Any qw/$log/;

use Stream2::O::Setting;
use Stream2::Data::JSON;

use Data::Dumper;

sub wrap{
    confess("Implement this");
}

sub build_usersetting_factory{
    confess("Implement this");
}

=head2 inherit_siblings_settings_for_user

Sets the given user with the SAME setting value as its sibblings
ONLY if all its sibblings have a consistent value.

If you know already of your sibling ids, you can give them as a second parameters.

=cut

sub inherit_siblings_settings_for_user{
    my ($self, $user , $siblings_ids ) = @_;

    # Idea: select everything from the user settings, grouping by setting_id and by setting value
    # Go through all the rows.
    #
    # If we hit a row with a count == the number of sibblings, then set the value of this user to
    # the value of this row.

    my @siblings_ids = $siblings_ids ? @{$siblings_ids} : $user->siblings_ids();
    my $n_siblings = scalar(@siblings_ids);

    $log->info("Got $n_siblings siblings");

    my $stream2 = $self->stream2();
    my $user_settings_f = $self->build_usersetting_factory();

    foreach my $setting_type ( 'boolean' , 'string' ){

        my $user_settings = $user_settings_f->search({
                                                      'setting.setting_type' => $setting_type,
                                                      'me.user_id' => { -in => \@siblings_ids } # We look at all settings, but only for the siblings.
                                                     },
                                                     # We are only interested in:
                                                     { select => [ 'me.setting_id',  'me.'.$setting_type.'_value',
                                                                   { count => 'me.id' } ],
                                                       as =>     [ 'setting_id' , $setting_type.'_value', # This is useful to calculate a valid value hash
                                                                   'number_holders' ],
                                                       group_by => [ 'me.setting_id' , 'me.'.$setting_type.'_value' ],
                                                       join => 'setting',
                                                     }
                                                    );
        while( my $user_setting = $user_settings->next() ){
            unless( $user_setting->get_column('number_holders') == $n_siblings ){
                next;
            }
            my $value_hash = $user_setting->value_hash();
            $log->info("All siblings have the same value ".Dumper($value_hash)." for setting ID = ".$user_setting->setting_id()." setting that against this user");
            $user_settings_f->find_or_create({ setting_id => $user_setting->setting_id(),
                                               user_id => $user->id(),
                                               admin_user_id => $user->id(),
                                               admin_ip => $user->last_ip() || '127.0.0.1',
                                               %$value_hash
                                             });
        }
    }
}

=head2 build_settings_hash_for_user

Builds a hash of { setting mnemonic => value } for all settings for the given L<Stream2::O::SearchUser>

=cut

{

    my %FLATTEN_TYPE = (
                        'boolean' => sub{
                            my ($setting) = @_;
                            return $setting->get_column('boolean_coalesce_value') * 1;
                        },
                        'string' => sub{
                            my ($setting) = @_;
                            my $string_setting = $setting->get_column('string_coalesce_value');
                            my $meta_data = $setting->setting_meta_data();
                            if ($meta_data->{string_type} // '' eq 'json') {
                              return Stream2::Data::JSON->inflate_from_json($string_setting);
                            }
                            return $string_setting . '';
                        }
                       );
    my $FLATTEN_CATCHALL = sub{return;};

    sub build_settings_hash_for_user{
        my ($self, $user) = @_;
        my $settings_hash = {};

        my $stream2 = $self->stream2();
        return $stream2->with_context({ user_id => scalar( $user->id() ) },
                                      sub{
                                          # We use the resultset. No factory magic required, and this should be as fast as
                                          # possible.
                                          my $settings = $self->dbic_rs
                                            ->search(undef,
                                                     {
                                                      'select' => [ 'me.setting_mnemonic',
                                                                    'me.setting_type',
                                                                    'me.setting_meta_data',
                                                                    \'coalesce(user_settings.boolean_value, me.default_boolean_value)',
                                                                    \'coalesce(user_settings.string_value, me.default_string_value)'],
                                                      'as' => [ 'setting_mnemonic',
                                                                'setting_type',
                                                                'setting_meta_data',
                                                                'boolean_coalesce_value',
                                                                'string_coalesce_value' ],
                                                      join => 'user_settings'
                                                     },
                                                    );
                                          # Note the 'next' has to be in there, cause the SQL will only be generated
                                          # when the first 'next' is called.
                                          while ( my $setting = $settings->next() ) {
                                              $settings_hash->{$setting->get_column('setting_mnemonic')} =
                                                ( $FLATTEN_TYPE{$setting->get_column('setting_type')} // $FLATTEN_CATCHALL )->($setting);
                                          }

                                          return $settings_hash;
                                      });
    }

}

__PACKAGE__->meta->make_immutable();

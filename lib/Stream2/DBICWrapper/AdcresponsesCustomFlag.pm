package Stream2::DBICWrapper::AdcresponsesCustomFlag;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;

use Stream2::O::AdcresponsesCustomFlag;

=head1 NAME

Stream2::DBICWrapper::AdcresponsesCustomFlag - A factory around the AdcresponsesCustomFlag

=cut

sub wrap{
     my ($self, $o) = @_;
     return Stream2::O::AdcresponsesCustomFlag->new({ o => $o , factory => $self });
}

# IDs are unique per group_identity and should begin at 20
around 'create' => sub {
    my ( $orig, $self, $columns ) = @_;

    my $group_identity = $columns->{group_identity}
        or confess( "group_identity is required" );

    my $stuff = sub {
        my $max_id = $self->search(
            {
                group_identity => $group_identity
            },
            {
                select => [\'COALESCE(MAX(flag_id)+1,20)'],
                as => ['max_flag_id']
            }
        )->first()->get_column('max_flag_id');

        my $flag = $self->$orig({
            %$columns,
            flag_id => $max_id
        });

        # Create a group setting to control the active state of the flag
        $self->bm->factory(
            'Groupsetting',
            {
                group_identity => $flag->group_identity
            }
        )->create({
            setting_mnemonic        => $flag->setting_mnemonic,
            setting_description     => $flag->setting_description,
            default_boolean_value   => 1,
            setting_meta_data       => { flag_id => $flag->flag_id }
        });

        return $flag;
    };
    return $self->bm->stream_schema->txn_do( $stuff );
};

=head2 list_available_flags

For getting a list of all flags that are/were available for
a company

Usage:

    my @list_of_flags = $this->list_available_flags( $group_identity );

=cut

sub list_available_flags {
    my ( $self, $group_identity ) = @_;

    my @custom_flags = map {
        { $_->get_columns }
    } $self->search({
        group_identity => $group_identity
    })->all();

    return ( $self->standard_flags, @custom_flags );
}

=head2 map_available_flags

Returns a map of flag_id => flag_obj containing
available flags ( see L<list_available_flags> )

Usage:

    my %flag_map = $this->map_available_flags( $group_identity );

=cut

sub map_available_flags {
    my ( $self, $group_identity ) = @_;

    return map {
        $_->{flag_id} => $_
    } $self->list_available_flags( $group_identity );
}

=head2 standard_flags

A hard-coded list of historical flags as defined by the AdCourier code base.
Should be filtered depending on settings

Usage:

    my @standard_flags = $this->standard_flags;

=cut

sub standard_flags {
    my ( $self ) = @_;
    return (
        {
            type => 'standard',
            flag_id => 1,
            description => $self->stream2->__p('feed.adcresponses', 'Red'),
            colour_hex_code => '#ff0000',
            setting_mnemonic => 'responses-flagging-enable_red_flag',
            has_rules => 'true'
        },
        {
            type => 'standard',
            flag_id => 3,
            description => $self->stream2->__p('feed.adcresponses', 'Amber'),
            colour_hex_code => '#ffa500',
            setting_mnemonic => 'responses-flagging-enable_amber_flag',
            has_rules => 'true'
        },
        {
            type => 'standard',
            flag_id => 5,
            description => $self->stream2->__p('feed.adcresponses', 'Green'),
            colour_hex_code => '#008000',
            setting_mnemonic => 'responses-flagging-enable_green_flag'
        },
        {
            type => 'standard',
            flag_id => 6,
            description => $self->stream2->__p('feed.adcresponses', 'Progressed'),
            colour_hex_code => '#00cd00',
            setting_mnemonic => 'responses-flagging-enable_progressed_flag'
        },
        {
            type => 'standard',
            flag_id => 7,
            description => $self->stream2->__p('feed.adcresponses', 'Unranked'),
            colour_hex_code => '#808080'
        },
        {
            type => 'standard',
            flag_id => 9,
            description => $self->stream2->__p('feed.adcresponses', 'Placed'),
            colour_hex_code => '#008000'
        }
    );
}

__PACKAGE__->meta->make_immutable();
1;

package Stream2::DBICWrapper::CandidateInternalMapping;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;


=head2 clean_old_shite

Clearup some old mappings ( older than 6 months )

There is NO guarantee this will clear ALL mappings older than 6 months, as for performance reasons
we only clear a small amount of results. Feel free to call this multiple times.

Usage:

 $this->clean_old_shite();

=cut

sub clean_old_shite{
    my ($self) = @_;

    my $schema = $self->stream2->stream_schema();

    my $six_months_limit = $schema->with_sqllite(sub{
                                                       return "date(NOW() , '-6 month')";
                                                   },
                                                   sub{
                                                       return 'DATE_SUB(NOW() , INTERVAL 6 MONTH ) LIMIT 100'
                                                   });

    my $sql = 'DELETE FROM '.$self->dbic_rs->result_source->name().' WHERE insert_datetime <= '.$six_months_limit;

    $self->stream2->stream_schema->storage()->dbh_do(sub{
                                                         my ($storage, $dbh) = @_;
                                                         $log->debug("Doing '$sql'");
                                                         $dbh->do($sql);
                                                         return 1;
                                                     });
}

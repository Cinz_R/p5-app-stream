package Stream2::DBICWrapper::EmailBlacklist;

use Moose;
extends qw/Stream2::DBICWrapper/;

with qw/Stream2::Role::DBICWrapper::BinarySearch/;

use Log::Any qw/$log/;

has invalid_domains => (
    is => 'ro',
    isa => 'ArrayRef',
    default => sub {
        [
            '.cmo',
            '.ocm',
            '.o.uk',
            '.c.uk',
            'gamil.com',
            'gmai.com',
            'gmail.co',
            'gmail.cm',
            'gmail.con',
            'gmail.om',
            'gmailc.om',
            'gmaill.com',
            'gmal.com',
            'gmeil.com',
            'gmial.com',
            'gnail.com',
            'hotamil.com',
            'hotmai.com',
            'hotmail.co',
            'hotmail.con',
            'hotmail.om',
            'hotmail.cm',
            'hotmailc.om',
            'hotmeil.com',
            'hotmial.com',
            'hotmali.com',
            'hotmsil.com',
            'hoymail@com',
            'hormail.com',
            'hptmail.com',
            'htomail.com',
            'mgail.com',
            'yahoo.om',
            'hotmil.com',
            'hotmal.co.uk',
            'icoud.com',
            'homtail.co.uk',
            'oulook.com',
            'hotnail.com',
            'gmaol.com',
            'goooglemail.com',
            'gtmail.com',
        ];
    }
);

=head1 NAME

Stream2::DBICWrapper::EmailBlacklist - A factory around the EmailBlacklist items

=cut

=head2 email_blacklisted

Returns the EmailBlacklist item in case the given email is blaclisted in the given context.

Usage:

 # In general, outside of any context (support blacklist)
 if( $this->email_blacklisted('pat@pat.com') ){

 }

 # For a given group_identity, with no specific purpose
 if( $this->email_blacklisted('pat@pat.com', { group_identity => 'cinzwonderland' } ){

 }

 # For a given group_identity, for a specific purpose
 if( $this->email_blacklisted('pat@pat.com', { group_identity => 'cinzwonderland' , purpose => 'action:message'} ){

 }

=cut

sub email_blacklisted{
    my ($self, $email , $context) = @_;
    return $self->emails_blacklisted( [ $email ] , $context)->dbic_rs->first();
}

=head2 emails_blacklisted

Returns a resultset of all the EmailBlacklist objects.

Usage:

 my $blacklisted = $this->emails_blacklisted([ 'pat@pat.com' ]);

=cut

sub emails_blacklisted{
    my ($self, $emails , $context) = @_;
    $emails //= [];
    $context //= {};

    return $self->search({ email => { -in => $emails },
                           # Make sure no given context generates  NULL predicates for those properties:
                           group_identity => undef,
                           %$context
                       });
}

=head2 invalid_email_domain

In order to anticipate email failures ( and to improve our standing with any mail providers )
check if there are any obvious mistakes with the provided email address

L<https://github.com/debitoor/email-blacklist/blob/master/index.js>

Usage:

    if ( $self->invalid_email_domain( $email_address ) ){
        die "invalid email address";
    }

=cut

sub invalid_email_domain {
    my ( $self, $email ) = @_;
    return !! grep { $email =~ m/\Q$_\E\z/i } @{$self->invalid_domains};
}

__PACKAGE__->meta->make_immutable();

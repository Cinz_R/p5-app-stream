package Stream2::DBICWrapper::Tag;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;
use Stream2::O::Tag;
use Text::CSV;
use String::Snip;

=head1 NAME

Stream2::DBICWrapper::Tag - A factory around the Tag result set

=cut

=head2 wrap

Wraps any result with a Stream2::O::Tag

=cut

sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::Tag->new({ o => $o , factory => $self });
}


=head2 autocomplete

Returns a Resultset of tags matching the given stem for the given company, alpha order.

Usage:

 my $tags = $this->autocomplete($group_identity, $stem);

=cut

sub autocomplete{
    my ($self, $group_identity, $stem ) = @_;

    $stem //= '';

    my $schema = $self->stream2()->stream_schema();

    # Escaping the stem (allows for tags to contain '%')
    # See https://metacpan.org/pod/DBI , section 'Further information'


    return $self->search(
        {
            'me.group_identity' => $group_identity,
            tag_name_ci => { like => $schema->escape_like($stem).'%' }
        },
        {
            order_by  => [ { -desc => 'flavour_id' } , { -asc => 'tag_name' } ],
            prefetch => 'flavour'
        }
    );
}

=head2 split_hotlist_tags

Split the given tags between hotlist ones and other ones for the given company.

Usage:

  my ($hotlist_tags, $other_ones) = $this->split_hotlist_tags($company , [ 'tag1', 'tag2', .... ]);

Note that it will split the tags REGARDLESS of their Casing. This is to comply with
the fact artirix indexes the tags in a case insensitive way.

=cut

sub split_hotlist_tags{
    my ($self, $company , $tags) = @_;
    $tags //= [];

    # Find the hotlist ones.
    my @hot_ones = $self->search(
        {
            'flavour.name' => 'hotlist',
            'me.group_identity' => $company,
            'tag_name_ci' => { -in => $tags }
        },
        {
            join => 'flavour',
            select => [ 'me.tag_name_ci' ],
            as => [ 'tag_name_ci' ]
        }
    )->all();

    my %hot_hash = map { $_->tag_name_ci() => 1 } @hot_ones;
    return ( [ keys %hot_hash ] , [ grep { ! $hot_hash{lc($_)} } @$tags ] );
}

=head2 enrich_candidates

Enriches the given candidates S2::Results::Result with stored tags (if they haven't got any yet) for the current user.

Usage:

 $this->enrich_candidates( $results , { user => $search_user });

=cut

sub enrich_candidates{
    my ($self, $results, $opts) = @_;
    $opts //= {};

    unless( $opts->{user} ){ confess("Missing user in $opts"); }

    my $user = $opts->{user};
    my $schema = $self->stream2->stream_schema();

    # Holds a marker of all the in memory
    # candidate tags. Will be useful for later
    # vivification of missing ones.
    my $TAGS_FOR_CANDIDATES = {};

    {
        # Consider already tagged candidates (like from talent search for instance.
        # Enrich them with some flavour. Note that we do NOT pull any candidate tagging
        # from the DB for those ones.
        my @tagged = grep { $_->{'tags'} } @$results;


        # Container for missing tags (as flavourless tags and against each candidate).
        my %missing_db_tags = ();

        my %tag_names = ();
        foreach my $tagged ( @tagged ){
            foreach my $tag ( $tagged->tags() ){
                unless( ref($tag) ){
                    $tag_names{$tag} = { value => $tag, flavour => undef };
                }
            }
        }

        # Get all the tag objects that are there in the database.
        {
            my $tags = $self->search(
                {
                    'tag_name' => { -in => [ keys %tag_names ] },
                    'me.group_identity' => $user->group_identity()
                },
                {
                    prefetch => 'flavour'
                }
            );
            ## Reset tag_names so it contains only the tags in the DB
            %tag_names = ();
            while( my $tag = $tags->next() ){
                $tag_names{$tag->tag_name()} = { value => $tag->tag_name() , flavour => $tag->flavour_name(), label => $tag->tag_label() };
            }
        }

        # Enrich the already tagged candidates.
        # and also record the tags we need to vivify as well as the tags
        # that are only in memory at this point.
        $log->debug("Enriching already there tags with tags from the DB");
        foreach my $tagged ( @tagged ){
            my @rich_tags = ();
            # Enrich the poor tags with objects by tag name (unless they are already objects).
            foreach my $poor_tag ( $tagged->tags() ){
                my $tag_name = ref($poor_tag) ? $poor_tag->{value} : $poor_tag;
                my $rich_tag = $tag_names{$tag_name}; # This tag was in the DB

                # Mark this candidate to have this tag.
                $TAGS_FOR_CANDIDATES->{$tag_name}->{$tagged->id()} = 1;

                if( $rich_tag ){
                    $log->trace("Tag '$tag_name' was in the db. Pushing this rich one");
                    push @rich_tags , ( ref($poor_tag) ? $poor_tag  : $rich_tag );
                }else{
                    $log->trace("Tag '$tag_name' is not in the DB. (comming from candidate  ".$tagged->id().")");
                    # Rich tag does NOT exist in our local DB (but it is in the candidate probably coming from the board).
                    # We need to vivify it a bit later with no flavour.
                    $missing_db_tags{$tag_name} = 1;
                    push @rich_tags , { value => $tag_name , flavour => undef };
                }
            }
            $tagged->{tags} = \@rich_tags;
        }

        ## Vivify the missing tags (tags not in the DB but that were there
        ## thanks to the feed or whatever.).
        my @missing_tags = keys %missing_db_tags;
        my $stuff = sub{
            foreach my $missing_tag ( @missing_tags ){
                my $real_tag = $missing_tag;
                $real_tag =~ s/,/_/g;
                if ( length($real_tag) > 100 ){
                    $log->warnf("The tag %s is too big",String::Snip::snip($real_tag, { max_length => 100 }));
                    next;
                }

                $log->info("Adding missing tag to the db '$real_tag'");
                my $tag = $schema->resultset('Tag')->find_or_create({ group_identity => $user->group_identity(),
                                                                      tag_name => $real_tag,
                                                                      tag_name_ci => $real_tag
                                                                  });
            }
        };
        $schema->txn_do($stuff);
    }

    my $candidate_tags_factory = $schema->resultset('CandidateTag');

    # Consider ALL candidates
    my $stuff = sub{
        # Quick access to candidates by ID.
        my %res_hash = map{ $_->id() => $_  } @$results;

        {
            if( keys %res_hash ){
                my $tags = $candidate_tags_factory->search_rs({ 'me.group_identity' => $user->group_identity(),
                                                                'candidate_id' => { -in => [ keys %res_hash ] }
                                                            },
                                                              { order_by => 'candidate_id',
                                                                prefetch => { tag => 'flavour' }
                                                            }
                                                          );
                while( my $cand_tag = $tags->next() ){
                    my $candidate = $res_hash{$cand_tag->candidate_id};
                    $candidate->{_tags_hash} //= { map{ $_->{value} => $_ } @{ $candidate->{tags} // [] } };
                    my $tag = $self->wrap( $cand_tag->tag() );
                    # Unmark the tags_for_candidates, as this one is guaranteed to be in the DB
                    delete $TAGS_FOR_CANDIDATES->{ $tag->tag_name() }->{ $cand_tag->candidate_id() };

                    $candidate->{_tags_hash}->{$tag->tag_name} =  { value => $tag->tag_name, label => $tag->tag_label, flavour => $tag->flavour_name };
                }
            }
        }

        # Vivify all the candidate tags that were in memory but not in the DB.
        {
            foreach my $tag_name ( keys %{ $TAGS_FOR_CANDIDATES } ){
                foreach my $candidate_id ( keys %{ $TAGS_FOR_CANDIDATES->{$tag_name} } ){
                    $log->info("Adding missing candidate tag to the db '$tag_name' for candidate '$candidate_id'");
                    my $candidate_tag = $candidate_tags_factory->find_or_create({ group_identity => $user->group_identity,
                                                                                  tag_name => $tag_name,
                                                                                  candidate_id => $candidate_id });
                    my $tag = $self->wrap( $candidate_tag->tag() );
                    my $candidate = $res_hash{ $candidate_id };
                    $candidate->{_tags_hash}->{$tag->tag_name} =  { value => $tag->tag_name, label => $tag->tag_label, flavour => $tag->flavour_name };
                }
            }
        }
    };
    $schema->txn_do($stuff);


    ## Make sure candidates with no _tags_hash get one from their 'tags' in the case
    ## those tags were not recorded in search's db.
    ## This can happen when a tag exists in the search DB, but is not associated with any candidate.
    ## This is because the vivification of tags only works on tags that were never seen (not at candidate level)
    foreach my $candidate ( @$results ){
        $candidate->{_tags_hash} //= { map{ $_->{value} => $_ } @{ $candidate->{tags} // [] } };
    }

    # Sorting them so the shorter ones are at the top means the display is nicer
    # as you get more tags shown without hovering.
    map {
        $_->{_tags_hash} //= {};
        # we have a hash of unique enriched candidate tags per candidate, flatten it and sort it and delete it
        $_->{tags} = [ sort { length $a->{value} <=> length $b->{value} } values %{delete $_->{_tags_hash}} ];
    } @$results;
}

=head2 import_file

Imports the given filename into the tags of this database.

The file should be a UTF8 encoded CSV with lines in the following format (with the first line as header and tabs delimited):

   board,candidate_id,company,insert_datetime,tag_name
   talentsearch[TAB]12345[TAB]cinzwonderland[TAB]2014-06-27 11:28:25[TAB]blabla

Typical way of getting such a file is:

   www@cheerleader2:~$ echo "SELECT board, candidate_id, company, insert_datetime, tag_name, cts.important from cv_tagging LEFT JOIN cv_tag_suggestions cts ON ( cts.es_index = CONCAT('tags_' , company) AND cts.tag = tag_name )"  | cvsearch > /tmp/candidate_tag.tsv


Options:

    sep_char : The separator character. Defaults  to "\t" (convenient for mysql exports).

    quote_char : The char used to quote a cell. Defaults to undef (convenient for mysql exports).

    escape_char: The char used to escape a quote char in a cell. Defaults to undef (convenient for mysql exports).

    skip_lines: How many lines to skip. Defaults to 3 (convenient for mysql exports).


Returns: Number of imported rows.

=cut

sub import_file{
    my ($self, $filename, $options) = @_;

    unless( $filename ){
        confess(q\Missing filename. Please consider exporting a file:

   www@cheerleader2:~$ echo "SELECT board, candidate_id, company, insert_datetime, tag_name, cts.important from cv_tagging LEFT JOIN cv_tag_suggestions cts ON ( cts.es_index = CONCAT('tags_' , company) AND cts.tag = tag_name )"  | cvsearch > /tmp/candidate_tag.tsv

                \);
    }


    $options //={};
    my $sep_char = $options->{sep_char} // "\t";
    my $skip_lines = $options->{skip_lines} // 3;
    my $quote_char = exists $options->{quote_char} ? $options->{quote_char} : undef;
    my $escape_char = exists $options->{escape_char} ? $options->{escape_char} : undef;

    my $csv = Text::CSV->new({ binary => 1 , sep_char => $sep_char , quote_char => $quote_char , escape_char => $escape_char });
    open my $fh , "<:encoding(utf8)", $filename or confess("Cannot open $filename: $!");


    # Skip the skip_lines lines.
    foreach ( 1..$skip_lines ){
        my $row = $csv->getline( $fh );
        $log->info("Skipping row '".join($sep_char, @$row)."'");
    }

    my $schema = $self->stream2->stream_schema();

    my $count = 0;

    my $stuff = sub{

        my $tags = $schema->resultset('Tag');
        my $candidate_tags = $schema->resultset('CandidateTag');

        my $company_hot = {};

        # Loop though the lines to import.
        while( my $row = $csv->getline($fh) ){
            my ( $board , $candidate_id , $company, $insert_datetime, $tag_name, $important) = @$row;

            # Protect against too long tag names
            if( length($tag_name) > 100 ){
                $tag_name = substr($tag_name, 0, 100);
            }

            $company_hot->{$company} //= $schema->resultset('TagFlavour')->find_or_create({ group_identity => $company,
                                                                                            name => 'hotlist' });

            # $log->trace("Importing '$tag_name'");
            my $tag = $tags->find_or_create({ group_identity => $company,
                                              tag_name => $tag_name,
                                              tag_name_ci => $tag_name
                                            });
            if( ( $important || 'NULL')  ne 'NULL'  &&
                ! $tag->flavour_id()
              ){
                $log->debug("Tag '".$tag->tag_name()."' is now important (hotlist) for company ".$company);
                $tag->flavour($company_hot->{$company});
                $tag->update();
            }

            my $candidate_tag = $candidate_tags->find_or_create({ group_identity => $company,
                                                                  candidate_id => $board.'_'.$candidate_id,
                                                                  tag_name => $tag_name
                                                                });
            if( ( $insert_datetime || 'NULL' ) ne 'NULL' ){
                $candidate_tag->update({ insert_datetime => $insert_datetime });
            }
            $count++;
            if( $count % 1000 == 0 ){
                $log->info("Imported $count rows");
            }
        }
        $csv->eof() or confess("Could not CSV parse '$filename': ".$csv->error_diag());
        close $fh;
        $log->info("Import finished. Imported $count rows");
        return $count;
    };

    return $schema->txn_do($stuff);
}

__PACKAGE__->meta->make_immutable();

package Stream2::DBICWrapper::EmailTemplateRule;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;

use Stream2::O::EmailTemplateRule;

=head1 NAME

Stream2::DBICWrapper::EmailTemplateRule - A factory around the EmailTemplateRule objects

=cut

sub wrap{
    my ($self, $row , $args) = @_;
    $args //= {};
    return Stream2::O::EmailTemplateRule->new({ o => $row,
                                                factory => $self,
                                                %$args });
}

sub create{
    my ($self, $args) = @_;
    $log->info("Creating template rule with role = ".$args->{role_name});
    return $self->wrap(
        $self->dbic_rs->create({
            'role_name' => $args->{role_name},
            'email_template_id' => $args->{email_template_id},
        }) , $args );
}

__PACKAGE__->meta->make_immutable();

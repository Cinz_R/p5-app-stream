package Stream2::DBICWrapper::WatchdogResult;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Stream2::O::WatchdogResult;

use Log::Any qw/$log/;

sub build_dbic_rs{
    my ($self) = @_;
    return $self->stream2->stream_schema->resultset('WatchdogResult');
}

sub wrap{
    my ($self,$o) = @_;
    return Stream2::O::WatchdogResult->new({ o => $o,
                                             factory => $self
                                           });
}

=head2 clean_old_results

Clearup some old results from the watchdog results table

There is NO guarantee this will clear ALL results older than x months, as for performance reasons
we only clear a small amount of results. Feel free to call this multiple times.

Usage:

 $this->clean_old_results();

=cut

sub clean_old_results{
    my ($self) = @_;

    my $schema = $self->stream2->stream_schema();

    my $one_month_ago = $schema->with_sqllite(sub{
                                                       return "date(NOW() , '-1 month')";
                                                   },
                                                   sub{
                                                       return 'DATE_SUB(NOW() , INTERVAL 1 MONTH ) LIMIT 100'
                                                   });

    my $sql = 'DELETE FROM '.$self->dbic_rs->result_source->name().' WHERE last_seen_datetime <= '.$one_month_ago;

    $self->stream2->stream_schema->storage()->dbh_do(sub{
                                                         my ($storage, $dbh) = @_;
                                                         $log->debug("Doing '$sql'");
                                                         $dbh->do($sql);
                                                         return 1;
                                                     });
}

=head2 enqueue_backfill_from

  Enqueues backfilling from V1 from the given result.

  Usage:

    $this->enqueue_backfill_from($result);

=cut

sub enqueue_backfill_from{
    my ($self, $result) = @_;

    $log->info("Enqueuing backfilling of watchdog result  ".join("-",  $result->id() ) );

    my $job = $self->stream2->qurious->create_job(
                                                  class => 'Stream2::Action::WatchdogResultBackfill',
                                                  queue => 'BackgroundJob',
                                                  parameters => {
                                                                 backfill_from => [ $result->id() ],
                                                                }
                                                 );
    $job->enqueue();
    return 1;
}

=head2 backfill_from

Backfills results from the given one using V1 data and sends an email to tech_emails

=cut

sub backfill_from{
    my ($self, $result) = @_;

    my $earlier_results = $result->earlier($self); # See note in Stream2::O::WatchdogResult

    my $subscription = $result->watchdog_subscription();
    my $watchdog = $subscription->watchdog();
    my $user = $watchdog->user();

    my $count = $earlier_results->count();

    $log->info("backfilling [$count] results from V1 for result ".join("-" , $result->id()).
               " Watchdog =".$watchdog->id()." subscription = ".$subscription->subscription->board());


    my @error_results = ();


    while( my $earlier_result = $earlier_results->next() ){
        $earlier_result->backfill_from_v1();
        if( $earlier_result->content()->{v1_backfill_error} ){
            push @error_results , $earlier_result->to_raw_hashref();
        }
    }

    unless( @error_results ){
        return;
    }

    ## There are some errors.
    ## Prepare report for tech people

    my $subject = "[V1 Backfill Error] [".$user->group_identity()."] WD=".$watchdog->id()
      .' - ' .$watchdog->name().'/ '.$subscription->subscription->board().' FOR USER='.$user->id().' '.$user->contact_email();

    my $body = 'WATCHDOG SUBSCRIPTION ID = '.$subscription->id()."  - ".scalar(@error_results)."  errors:\n";
    foreach my $error ( @error_results ){
        $body .= $self->stream2->json->encode($error).",\n";
    }

    my $recipients = $self->stream2->config()->{tech_email_addresses} // [ 'jerome@broadbean.net' ];

    # Got the subject, got the body. Send the email.
    my $email = $self->stream2->factory('Email')->build(
        [
            From => 'noreply@broadbean.net',
            To => $recipients ,
            'X-Stream2-Transport' => 'static_transport' , # We want this to go through the static IP transport
            Subject => Encode::encode( 'MIME-Q', $subject ),
            Data    => $body
        ]
    );

    $self->stream2->send_email($email);


}


__PACKAGE__->meta->make_immutable();

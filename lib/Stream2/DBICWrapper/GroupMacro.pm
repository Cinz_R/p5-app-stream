package Stream2::DBICWrapper::GroupMacro;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;

use Stream2::O::GroupMacro;

=head1 NAME

Stream2::DBICWrapper::GroupMacro - A group macro

=cut

sub wrap{
    my ($self, $row) = @_;
    return Stream2::O::GroupMacro->new({ o => $row , factory => $self });
}



__PACKAGE__->meta->make_immutable();

package Stream2::DBICWrapper::CandidateActionLog;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

with qw/Stream2::Role::DBICWrapper::BinarySearch/;

use Log::Any qw/$log/;

use Stream2::O::CandidateActionLog;

=head1 NAME

Stream2::DBICWrapper::CandidateActionLog

=cut

sub build_dbic_rs{
    my ($self) = @_;
    return $self->stream2->stream_schema()->resultset('CandidateActionLog');
}

sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::CandidateActionLog->new({ o => $o, factory => $self });
}

__PACKAGE__->meta->make_immutable();

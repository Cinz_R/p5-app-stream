package Stream2::DBICWrapper::GroupFile;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;

use Stream2::O::GroupFile;

=head1 NAME

Stream2::DBICWrapper::GroupFile - A group public file.

=cut

=head2 build_dbic_rs

Restrict the vision through this factory as only the NON DELETED (marked as deleted)
items.

=cut

sub build_dbic_rs{
    my ($self) = @_;
    return $self->stream2()->stream_schema->resultset('GroupFile')->search_rs({ deleted_datetime => undef });
}


=head2 create

Examples:

  # 
  my $file = $stream2->factory('GroupFile')->create({ content_type => 'text/plain', content => 'Kittens Beer Pie',
                                                      name => 'kitten.txt',
                                                      group_identity => $memory_user->group_identity()
                                                    });

  # Or if you already know the public key and mime_type:
  my $file = $stream2->factory('GroupFile')->create({ mime_type => 'text/plain', public_file_key => 'B19024D9-377B-43D1-A336-41418EA96F76',
                                                      name => 'kitten.txt',
                                                      group_identity => $memory_user->group_identity()
                                                    });

=cut

around 'create' => sub{
    my ($orig, $self, $args) = @_;

    $args //= {};
    $args = \%{$args}; # Shallow copy

    unless( $args->{mime_type} ){
        # Try extracting the mime_type from the content_type
        ( $args->{mime_type} ) = ( $args->{content_type} =~ /^(\S+)/ );
        unless( $args->{mime_type} ){
            confess("Cannot extract mime_type from content_type (=".$args->{content_type}.")");
        }
    }
    unless( $args->{public_file_key} ){
        # Create a record from those args and clean stuff
        $args->{public_file_key} = $self->bm()->factory('PublicFile')->create($args)->key();
    }

    # Delete extra scruff that could have been added.
    delete $args->{content_type};
    delete $args->{content};
    delete $args->{content_file};

    return $self->$orig( $args );
};

=head1 NAME

Stream2::DBICWrapper::EmailTemplate - A factory around the EmailTemplate

=cut

sub wrap{
    my ($self, $row) = @_;
    return Stream2::O::GroupFile->new({ o => $row , factory => $self });
}



__PACKAGE__->meta->make_immutable();

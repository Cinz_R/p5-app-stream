package Stream2::DBICWrapper::LoginRecord;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

with qw/Stream2::Role::DBICWrapper::BinarySearch/;

use Log::Any qw/$log/;

use Stream2::O::CandidateActionLog;

use DateTime;

=head1 NAME

Stream2::DBICWrapper::LoginRecord

=cut

sub build_dbic_rs{
    my ($self) = @_;
    return $self->stream2->stream_schema()->resultset('LoginRecord');
}

sub wrap{
    my ($self, $o) = @_;
    return $o;
}

sub clear_old_records{
    my ($self) = @_;

    my $three_month = DateTime->now()->subtract( months => 3 );
    my $keep_record = $self->find_after_date( $three_month );
    unless( $keep_record ){
        $log->debug("Nothing to delete");
        return;
    }
    my $sql = 'DELETE FROM '.$self->dbic_rs->result_source->name().' WHERE id < '.$keep_record->id()." LIMIT 100";
    return $self->stream2->stream_schema->storage()->dbh_do(
        sub{
            my ($storage, $dbh) = @_;
            $log->debug("Doing '$sql'");
            $dbh->do($sql);
            return 1;
        });
}

__PACKAGE__->meta->make_immutable();

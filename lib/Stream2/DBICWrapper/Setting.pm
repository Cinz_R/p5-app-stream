package Stream2::DBICWrapper::Setting;

use Moose;
extends  qw/Stream2::DBICWrapper::SettingBase/ ;

use Log::Any qw/$log/;

use Stream2::O::Setting;

use Data::Dumper;

sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::Setting->new({ o => $o , factory => $self });
}

sub build_usersetting_factory{
    my ($self) = @_;
    return $self->stream2()->factory('UserSetting');
}

__PACKAGE__->meta->make_immutable();

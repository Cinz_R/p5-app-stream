package Stream2::DBICWrapper::EmailNotification;

use Moose;
extends qw/Stream2::DBICWrapper/;

# so we can use find_after_date()
with qw/Stream2::Role::DBICWrapper::BinarySearch/;

use Log::Any qw/$log/;

=head2 delete_old_soft_bounces

Deletes soft_bounce notifications that are more than x days old, where x is
the value of config->{email}->{notifications_max_age_in_days}.

=cut

my $HOW_MANY_TO_DELETE = 50;

sub delete_old_soft_bounces {
    my $self = shift;

    # auto-vivify bounce_policy to empty hashref if necessary
    my $bounce_policy   = $self->bm->config->{email}->{bounce_policy} //= {};
    my $max_age_in_days =
        $bounce_policy->{notifications_max_age_in_days} //= 7;

    my $oldest_row_to_keep = $self->find_after_date(
        DateTime->now->subtract( days => $max_age_in_days )
    );
    if ( $oldest_row_to_keep ) {
        $log->info(
            "Deleting $HOW_MANY_TO_DELETE soft bounce email notifications " .
            'before ID ' . $oldest_row_to_keep->id
        );
    }
    else {
        $log->info("No soft bounce notifications older than $max_age_in_days "
            . 'were found: quitting.');
        return;
    }

    # DELETE FROM email_notification
    # WHERE id < 123456 AND notification = 'bounce.soft'
    # LIMIT 50
    $self->search(
        {
            id           => { '<' => $oldest_row_to_keep->id },
            notification => 'bounce.soft',
        },
        {
            page => 1,
            rows => $HOW_MANY_TO_DELETE
        }
    )->delete;

    return 1;
}

1;

package Stream2::DBICWrapper::Groupsetting;

use Moose;
extends  qw/Stream2::DBICWrapper::SettingBase/ ;

use Log::Any qw/$log/;

use Stream2::O::Groupsetting;

has 'group_identity' => ( is => 'ro', isa => 'Str', required => 1 );

around 'create' => sub{
    my ($orig, $self, $params, @rest ) = @_;
    $params //= {};
    $params->{group_identity} = $self->group_identity();
    return $self->$orig( $params , @rest );
};

=head2 search

override of search to transfer 'group_identity' down
the search call chain.

=cut

sub search{
    my ($self , @rest) = @_;
    my $class = ref($self);
    return $class->new({ dbic_rs => $self->dbic_rs->search_rs(@rest),
                         bm => $self->bm(),
                         name => $self->name(),
                         group_identity => $self->group_identity,
                     });
}

=head2 build_dbic_rs

A DBIC resultset here is always filtered by group_identity

=cut

sub build_dbic_rs{
    my ($self) = @_;
    return $self->bm()->dbic_schema()->resultset('Groupsetting')->search({ group_identity => $self->group_identity() });
}

sub wrap{
     my ($self, $o) = @_;
     return Stream2::O::Groupsetting->new({ o => $o , factory => $self });
}

=head2 build_usersetting_factory

See superclass L<Stream2::DBICWrapper::Setting>

=cut

sub build_usersetting_factory{
    my ($self) = @_;
    return $self->stream2()->factory('UserGroupsetting');
}

__PACKAGE__->meta->make_immutable();

package Stream2::DBICWrapper::AdCourierUserCache;

use Moose;
extends  qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;
use Carp;


=head2 autocomplete

Autocomplete AdCourier users on the given stems in the given company

=cut

sub autocomplete{
    my ($self, $company , $stem) = @_;

    my $schema = $self->bm->stream_schema();
    my $escaped_stem = $schema->escape_like($stem // '');

    return $self->search({ company => $company,
                           contact_email => { '!=' => '' },
                           -or => {
                                   contact_email => { like => $escaped_stem.'%' },
                                   contact_name => { like => $escaped_stem.'%' }
                                  }
                         }, { order_by => [ 'contact_name' , 'contact_email' ] } );
}


=head2 build_dbic_rs

The Resultset is in plural..

=cut

sub build_dbic_rs{
    my ($self) = @_;
    return $self->bm->dbic_schema->resultset('AdCourierUserCache');
}

__PACKAGE__->meta->make_immutable();

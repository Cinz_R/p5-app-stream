package Stream2::DBICWrapper::SearchUser;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

use Log::Any qw/$log/;

use Stream2::O::SearchUser;
use Carp;

has 'subscription_key' => ( is => 'ro' , isa => 'Str' , lazy_build => 1);

sub _build_subscription_key{
    my ($self) = @_;
    return $self->stream2->config()->{subscription_key} // confess("Config misses subscription_key");
}

=head2 search

See  L<DBIx::Class::Wrapper>

As an optimization we always want to prefetch the group when we search
through Users.

=cut

sub search{
    my ($self, $search, $opts , @rest ) = @_;
    $opts //= {};
    return $self->SUPER::search($search , { prefetch => 'groupclient' ,
                                            %$opts
                                          }, @rest );
}

=head2 find

Implements find so enrichment of the USer object with ripple_settings can be immediate.

Usage:

 $this->find( 1234 );

Or better:

 $this->find( 1234, { ripple_settings => { .. }} );

=cut

sub find{
    my ($self, $key_params , $opts ) = @_;

    my $dbic_o = $self->dbic_rs->find( $key_params );
    unless( $dbic_o ){ return ;}

    return $self->wrap( $dbic_o , $opts );
}


=head2 wrap

Wraps any result with a Stream2::O::SearchUser

=cut

sub wrap{
    my ($self , $o , $pure_mem_attributes ) = @_;
    $pure_mem_attributes //= {};

    unless( $pure_mem_attributes->{ripple_settings} ){
        $pure_mem_attributes->{ripple_settings} = {};
    }

    return Stream2::O::SearchUser->new({ o => $o , factory => $self,
                                         %$pure_mem_attributes });
}

=head2 autocomplete

Autocomplete users on the given stems in the given group_identity

=cut

sub autocomplete{
    my ($self, $group_identity , $stem) = @_;


    my $schema = $self->bm->stream_schema();

    my $escaped_stem = $schema->escape_like($stem // '');

    return $self->search({ group_identity => $group_identity,
                           contact_email => { '!=' => '' },
                           -or => {
                                   contact_email => { like => $escaped_stem.'%' },
                                   contact_name => { like => $escaped_stem.'%' }
                                  }
                         }, { order_by => [ 'contact_name' , 'contact_email' ] } );
}



foreach my $creation_method ( 'new_result' , 'create' ){

    around $creation_method => sub{
        my ($orig, $self, $args) = @_;

        Carp::cluck( "ARGS IS $args ") unless ( ( ref($args) // '' ) eq 'HASH' );

        ## NO SIDE EFFECT ON ARGS PLEASE.
        $args = { %$args };

        # Capture the pure memory arguments.
        # to put them in the wrapped object.
        my $pure_memory_args = {
                                map{ $_ => ( delete $args->{$_ } // undef ) }
                                ( 'is_overseer' , 'stream2', 'navigation' , 'is_admin' , 'user_id' , 'ripple_settings' )
                               };

        # Stuff that can be not there, but not allowed to be set to undef.
        my $subscriptions_ref = delete $args->{subscriptions_ref};
        if( $subscriptions_ref ){
            $pure_memory_args->{subscriptions_ref} = $subscriptions_ref;
        }

        unless( $args->{last_login_provider} ){
            confess("Missing last_login_provider");
        }

        my $dbic_o = $self->dbic_rs->new_result($args);
        return $self->wrap( $dbic_o , $pure_memory_args );
    };
}



=head2 build_dbic_rs

The Resultset is in plural..

=cut

sub build_dbic_rs{
    my ($self) = @_;
    return $self->bm->dbic_schema->resultset('CvUser');
}

__PACKAGE__->meta->make_immutable();

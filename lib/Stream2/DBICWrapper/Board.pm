package Stream2::DBICWrapper::Board;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;

use Data::Dumper;

=head1 NAME

Stream2::DBICWrapper::Board - A factory around the Board resultset

=cut

has 'cache_time' => ( is => 'ro', default => '1 day' );

=head2 name_to_id

Convenience method. Turns a name into an ID (or undef if this board is not known).
Dies if no board_name is given.

Usage:

  my $board_id = $this->name_to_id($some_name // 'some fallback name');

=cut

sub name_to_id {
    my ($self, $board_name) = @_;
    unless( defined $board_name ){
        confess("Missing board_name");
    }

    if ( my $board = $self->fetch({ name => $board_name }) ){
        return $board->id();
    }

    $log->debug("No board found for name='$board_name'. ID is undef");
    return undef;
}

=head2 fetch

uses given parameters to find/fetch board information from a number of sources

DB: is kept up to date each time a new user logs in
CHI: caches information against the board name for 1 day only
API: can use the board ID or name, will cache result for 1 day

Usage:

  my $board_row = $this->fetch({ name => 'talentsearch' });

=cut

sub fetch {
    my ( $self, $where_ref ) = @_;

    # from board table
    # These are the board that we know of in the search
    # product. They don't need to be cached or anything.
    if ( my $board = $self->find( $where_ref ) ){
        return $board;
    }

    my $board_key = $self->cache_key( $where_ref );

    $log->debug("Board cache key = '$board_key'");

    my $foreign_board_json =
        $self->stream2()->stream2_cache->compute(
            $board_key,
            $self->cache_time(),
            sub{
                $log->debug("Fetching board info for ".Dumper($where_ref));
                my $boards = $self->stream2->board_api->list_boards( $where_ref );
                if ( $boards && scalar( @$boards ) ){
                    return JSON::to_json( $boards->[0] );
                }
                return ''; # Just cache an empty string.
            });

    unless( $foreign_board_json ){
        return;
    }

    my $foreign_board = JSON::from_json( $foreign_board_json );

    my %local_board = map {
        $_ => $foreign_board->{$_}
    } qw/name id nice_name allows_acc_sharing/;

    $local_board{disabled} = $foreign_board->{cvsearch_temp_disabled} || 0;

    $local_board{type} = $foreign_board->{board_type};

    return $self->dbic_rs->new( \%local_board );
}

=head2 cache_key

Given a hash that can contain 'name' OR 'id', both being appropriate unique keys,
return a cache key.

Priority is given to the 'name'.

Usage:

 my $cache_key = $this->cache_key({ name => 'bla' });

 my $cache_key = $this->cache_key({ id => 1234 });

=cut

sub cache_key {
    my ($self, $hash) = @_;
    my $name = $hash->{name};
    my $id   = $hash->{id};
    unless( $name || $id ){
        confess("Missing name or id in given hash");
    }
    join(':',__PACKAGE__, ( $name || ( 'id-'.$id ) ) );
}

=head2 list_internal_boards

Returns a hard coded list of boards which we know to have the internal flavour

Usage:

    my @internal_boards = $this->list_internal_boards();

=cut

sub list_internal_boards {
    return ( 'adcresponses', $_[0]->list_internal_databases );
}

=head2 list_internal_databases

Returns a list of internal databases, specifically concerned
with the aggregation and homogenisation of candidate data

Usage:

    my @internal_databases = $this->list_internal_databases();

=cut

sub list_internal_databases {
    return qw/talentsearch cb_mysupply candidatesearch/;
}

=head2 internal_backend_to_destination

For an internal backend name, return the corresponding destination (template name).

Example : Artirix becomes 'talentsearch'.

Usage:

 my $destination = $this->internal_backend_to_destination( 'Artirix' );

=cut

{
    my $BACKEND_DESTINATION = {
        MySupply => 'cb_mysupply',
        Artirix  => 'talentsearch',
        BBCSS    => 'candidatesearch',
    };
    sub internal_backend_to_destination{
        my ($self, $backend) = @_;
        return $BACKEND_DESTINATION->{$backend};
    }
}


__PACKAGE__->meta->make_immutable();

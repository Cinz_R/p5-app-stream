package Stream2::DBICWrapper::EmailTemplate;

use Moose;
extends qw/Stream2::DBICWrapper/;

use Log::Any qw/$log/;

use Stream2::O::EmailTemplate;

=head1 NAME

Stream2::DBICWrapper::EmailTemplate - A factory around the EmailTemplate

=cut

sub wrap{
    my ($self, $row) = @_;
    return Stream2::O::EmailTemplate->new({ o => $row , factory => $self });
}

__PACKAGE__->meta->make_immutable();

package Stream2::DBICWrapper::SavedSearch;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

use Log::Any qw/$log/;

use Stream2::O::SavedSearch;

=head2 build_dbic_rs

The Resultset is in plural..

=cut

sub build_dbic_rs{
    my ($self) = @_;
    return $self->bm->dbic_schema->resultset('SavedSearches');
}

=head2 wrap

Wraps any result with a Stream2::O::SavedSearch

=cut

sub wrap{
    my ($self , $o ) = @_;
    return Stream2::O::SavedSearch->new({ o => $o , factory => $self });
}

=head2 list

Returns saved searches and a pager object in a hashref

Usage:

    my $saved_search_ref = $this->list(
        $user_id,
        {
            rows => 10, # required
            page => 10,
            prefetch => ['tokens']
        }
     );

Out:

    {
        searches => [ ... ],
        pager => L<Data::Page>
    }

=cut

sub list {
    my ($self, $user_id, $options_ref) = @_;
    unless( $user_id ){ confess("Missing user ID"); }

    defined( $options_ref->{rows} )
        or die "rows is required for paging";

    my $prefetch_crit = $options_ref->{prefetch_criteria} || 0;

    # standard paging/ordering options
    my $search_opt_ref = {
        order_by        => { -asc => 'me.id' },
        rows            => int($options_ref->{rows}),
        ( defined $options_ref->{page} ? ( page => int($options_ref->{page}) ) : ( page => 1 ) )
    };

    # available for prefetch is 'tokens'
    if ( $prefetch_crit ) {
        $search_opt_ref->{prefetch} = [qw/criteria/];
    }

    my $rs = $self->dbic_rs->search(
        {
            active => 1,
            user_id => $user_id
        },
        $search_opt_ref
    );

    return {
        searches => [ map {
            {
                $_->get_columns(),
                ( $prefetch_crit ? ( tokens => $_->criteria->tokens ) : () )
            }
        } $rs->all ],
        pager => $rs->pager
    };
}

around 'create' => sub{
    my ($orig, $self, $params) = @_;

    my $criteria = delete $params->{criteria} || confess("Missing criteria");

    $params->{criteria_id} = $criteria->id() || confess("Criteria has no ID");
    $params->{name} ||= $criteria->display_string(); # No name, default to the criteria display_string

    return $self->$orig( $params );
};

=head2 import_company

Imports the given company in the set of SavedSearches.

Note that the first 3 parameters are positional.

Usage:

 # Simple import of straight adcourier users
 $this->import_company('pat');

 # Filter the adcourier users using a sub see L<Stream2::DBICWrapper::Watchog#import_company> for details
 $this->import_company('pat', sub{ ... });

 # Change the base_url userd:
 $this->import_company('pat', undef, 'http://blabla.blabla.com');

 # Change the vivified user provider to adcourier_ats:
 $this->import_company('pat' , undef, undef , { provider => 'adcourier_ats' });

=cut

sub import_company {
    my ($self, $company, $user_filter, $base_url , $opts ) = @_;
    my $stream2 = $self->bm();

    $user_filter //= sub{ return 1 ; };

    unless( $company ){ confess("Missing company"); }
    unless( $base_url ){
        $log->warn("Missing Base URL. Will default to https://search.adcourier.com/");
        $base_url = 'https://search.adcourier.com/';
    }
    $opts //= {};
    my $user_provider = $opts->{provider} // 'adcourier';

    my $user_api = $stream2->user_api();

    # Prepare the data.
    my $users = [];
    my $users_previous_searches= {}; # Keyed by $user->{id}

    $users = [ $user_api->company_users($company) ];
    $log->info("Got ".scalar(@$users)." users for company $company");

    foreach my $user ( @$users ){
        unless( &{$user_filter}($user) ){
            next;
        }

        ## Get only active watchdogs to save time.
        my @searches = $user_api->get_stream_v1_previous_searches($user->{id} ,{ start => 0, per => 20 });
        $log->info("Got ".scalar(@searches)." previous searches for user ".$user->{id}." ".$user->{username});
        $users_previous_searches->{$user->{id}} = \@searches;
    }


    # Filter out users without  watchdogs
    $users = [ grep{ $users_previous_searches->{$_->{id}} } @$users ];

    # Got all the data needed from APIs.

    my $schema = $stream2->stream_schema();
    my $saved_searches = $stream2->factory('SavedSearch');

    # Ok got all the data just fine. Time to import stuff in one go.
    $log->info("Importing in Stream2 Database");
    foreach my $v1_user ( @$users ){

        my $stuff = sub{
            $log->info("Considering previous searches for V1 user ".$v1_user->{username}." ( ID = ".$v1_user->{id}.")");
            my @v1_searches = @{ $users_previous_searches->{$v1_user->{id}} };

            # =|=|=|=|=|=|=|=|=                   #
            #    CREATE USER And its GroupClient  #
            # =|=|=|=|=|=|=|=|=                   #
            my $group_client = $schema->resultset('Group')->find_or_create({ identity => $company });
            my $v2_user = $schema->resultset('CvUser')->find_or_create({
                provider => 'adcourier', # UNIQ
                last_login_provider => $user_provider,
                provider_id => $v1_user->{id}, # UNIQ
                group_identity => $group_client->identity(),
            });
            $log->info("Vivified V2 user ".$v2_user->id());
            $v2_user->group_identity($company);
            if ( my $user_email = $v1_user->{email} ){
                $v2_user->contact_email($v1_user->{email});
            }
            $v2_user->update();

            # Loop through the previous searches
            use Data::Dumper;
            my $considered = 0;
            my $imported = 0;

            my @to_initialise = ();

            foreach my $v1_search ( @v1_searches ){
                $considered++;

                #+|+|+|+|+|+|+|+|+|+|+|+|+|+|+|+#
                # Normalise and create criteria #
                #+|+|+|+|+|+|+|+|+|+|+|+|+|+|+|+#
                my $v2_criteria = $v1_search->{attributes};
                # Scrub those criteria a little bit.
                foreach my $key ( keys %$v2_criteria ){

                    if( ( grep { $key eq $_ } qw/destination sudo_username rand/  ) ){
                        delete $v2_criteria->{$key}; next;  # Forbidden keys
                    }

                    if( $key =~ /^_/ ){
                        delete $v2_criteria->{$key}; next; # private keys
                    }

                    # Scrub out keys where the arrayref of value only contains empty rubbish
                    my @values = @{ $v2_criteria->{$key} };
                    unless( scalar(@values) ){
                        delete $v2_criteria->{$key}; next; # No values at all
                    }

                    unless( grep{ length( $_ ) } @values ){
                        delete $v2_criteria->{$key}; next; # Only blank values
                    }

                } # End of values scrubbing

                ## Expand location_id to location using the magic api
                if( my $location_id = $v2_criteria->{location_id} ){
                    $location_id = ref($location_id) ? $location_id->[0] : $location_id;
                    if( $location_id ){
                        $log->info("Got location_id = ".$location_id." will make sure there is a location token");
                        if( my $location = $stream2->location_api->find($location_id) ){
                            my $specific_path = $location->specific_path();
                            $log->info("Will set location string as '$specific_path'");
                            $v2_criteria->{location} //= [ $location->specific_path() ];
                        }
                    }
                }

                # Build a criteria object
                my $criteria = Stream2::Criteria->new(
                                                      schema => $schema,
                                                     );
                foreach my $field ( keys %$v2_criteria ) {
                    $criteria->add_token($field => $v2_criteria->{$field});
                }
                $criteria->save();
                my $criteria_id = $criteria->id();
                $log->info("Criteria ID: ".$criteria_id);

                my $name = $v1_search->{name} || $criteria->display_string(); # should default to keywords
                if( length($name) > 255 ){
                    $name = substr($name, 0, 255);
                }

                #+|+|+|+|+|+|+|+|+|+|+|+|+|#
                # Save search against user #
                #+|+|+|+|+|+|+|+|+|+|+|+|+|#
                my $saved_search = $saved_searches->search({
                    user_id     => $v2_user->id(),
                    criteria_id => $criteria->id(),
                    active      => 1
                })->first();
                if ( $saved_search ){
                    $log->info("Save search already exists: ".$criteria->id);
                    if( $name ){
                        $log->info("Renaming to $name");
                        # Update the name
                        $saved_search->name($name);
                        $saved_search->update();
                    }
                }
                else {
                    $saved_searches->create({
                        user_id     => $v2_user->id(),
                        criteria    => $criteria,
                        $name ? ( name => $name ) : (),
                        active => 1 
                    });
                    $imported++;
                }

            }
            $log->info($v1_user->{username}.": considered $considered and imported $imported");
        };

        $schema->txn_do($stuff);
    } # End of user loop

    return 1;

}



__PACKAGE__->meta->make_immutable();

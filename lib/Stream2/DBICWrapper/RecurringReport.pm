package Stream2::DBICWrapper::RecurringReport;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

use Log::Any qw/$log/;

use Data::Dumper;

use DateTime;
use DateTime::Format::MySQL;

use Stream2::O::RecurringReport;

=head1 NAME

Stream2::DBICWrapper::RecurringReport - A recurring report factory

=cut
sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::RecurringReport->new({ o => $o , factory => $self });
}


=head2 fire_due_reports

Fires the due reports in the backgroud job queue.

Return the number of fired reports.

=cut

sub fire_due_reports{
    my ($self) = @_;

    my $n_fired = 0;

    my $stuff = sub{
        my $due_reports = $self->due_reports();
        while( my $report = $due_reports->next() ){
            $log->info("Report ".$report->id()." is due to be run at ".$report->next_run_datetime());
            # Avoid another immediate run to find this report before its due next
            my $new_date = $report->next_run_datetime()->add( hours => 12 );
            $report->next_run_datetime( $new_date );
            $report->update();
            my $job = $self->stream2->qurious->create_job(
                class => 'Stream2::Action::ReportRun',
                queue => 'BackgroundJob',
                parameters => {
                    report_id =>  $report->id(),
                }
            );
            $job->enqueue();
            $n_fired++;
        }
        return $n_fired;
    };
    return $self->stream2->stream_schema->txn_do($stuff);
}

=head2 due_reports

A Resultset of reports due to be run.

Note that the due reports ARE NOT wrapped. This is to avoid crashes
in the process that launches them in case their classes are not consistent.

Usage:

 my @due_reports = $this->due_reports();

=cut

sub due_reports{
    my ($self) = @_;
    return $self->search({ next_run_datetime => { '<=' , \'NOW()' }})->dbic_rs();
}

__PACKAGE__->meta->make_immutable();

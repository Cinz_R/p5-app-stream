package Stream2::DBICWrapper::Watchdog;

use Moose;
extends  qw/Stream2::DBICWrapper/ ;

use Log::Any qw/$log/;

use Data::Dumper;

use DateTime;
use DateTime::Format::MySQL;

use Stream2::Watchdogs::Runner;

use Stream2::O::Watchdog;

=head1 NAME

Stream2::DBICWrapper::Watchdog - Watchdogs factory.

=cut

has 'watchdogs_limit' => ( is => 'ro' , isa => 'Int' ,lazy_build => 1);

sub _build_watchdogs_limit{
    my ($self) = @_;
    return $self->stream2->config()->{watchdogs_limit} // 10;
}

sub wrap{
    my ($self, $o) = @_;
    return Stream2::O::Watchdog->new({ o => $o , factory => $self });
}


sub build_dbic_rs{
    my ($self) = @_;
    return $self->stream2->stream_schema()->resultset('Watchdog');
}

=head2 fire_due_watchdogs

Fires the watchdogs that are due and return the number of fired ones.

Usage:

 my $n_fired = $this->fire_due_watchdogs();

=cut

sub fire_due_watchdogs{
    my ($self) = @_;

    my $stuff = sub{
        my $n_fired = 0;

        my $due_watchdogs = $self->get_due_watchdogs();
        while( my $watchdog = $due_watchdogs->next() ){
            $log->info("Watchdog ".$watchdog->id()." is due to be run at ".$watchdog->next_run_datetime());

            # Set the new date NOW. This will prevent two quickly consecutive runs
            # Set the next run to be tomorrow ASAP
            my $tomorrow = $watchdog->next_run_datetime()->add( days => 1 );
            $watchdog->next_run_datetime($tomorrow);
            $watchdog->update();

            $self->enqueue_watchdog_run($watchdog);

            $n_fired++;
        }

        if( $n_fired ){
            $log->info("Fired $n_fired watchdogs through Qurious");
        }else{
            $log->debug("Fired no watchdogs through Qurious");
        }
        return $n_fired;
    };

    return $self->stream2->stream_schema->txn_do($stuff);
}

=head2 initiate_abandonned_destruction

Finds the watchdogs that have not been viewed for 3 weeks
and initiate a destruction process.

Usage:

 $this->initiate_abandonned_destruction();

=cut

sub initiate_abandonned_destruction{
    my ($self) = @_;

    my $three_weeks_ago = DateTime->now()->subtract( days => 7 * 3 );
    my $dtf = $self->stream2->stream_schema->storage->datetime_parser;

    return
        $self->stream2()->stream_schema->txn_do(
            sub{
                my $to_inititate = $self->search(
                    {
                        last_viewed_datetime => { '<=' => $dtf->format_datetime( $three_weeks_ago ) },
                        destruction_process_id => undef,
                        active => 1,
                    },
                    {
                        for => 'update' ,
                        rows => 10, # Only do 10 at a time why not.
                    }
                );

                my $n_to_destroy = 0;
                while( my $watchdog = $to_inititate->next() ){
                    $log->info("Watchdog ".$watchdog->id()." DESTRUCTION process has started");
                    my $new_p = $self->stream2()->longsteps()
                        ->instantiate_process('Stream2::Process::DestroyAbandonnedWatchdog',
                                              { stream2 => $self->stream2() },
                                              { watchdog_id => $watchdog->id() });
                    $watchdog->update({ destruction_process_id => $new_p->id() });
                    $n_to_destroy++;
                }
                return $n_to_destroy;
            });
}

=head2 clear_old_inactve

If a watchdog is inactive for longer than 3 months, delete it and it's results

=cut

sub clear_old_inactve {
    my ( $self ) = @_;

    my $schema = $self->stream2->stream_schema();

    my $three_months_limit = $schema->with_sqllite(sub{
                                                       return "date(NOW() , '-3 month')";
                                                   },
                                                   sub{
                                                       return 'DATE_SUB(NOW() , INTERVAL 3 MONTH )'
                                                   });

    my $inactive = $self->search({
        update_datetime => { '<=', \$three_months_limit },
        active          => 0
    });

    # delete_all isn't implemented
    my @watchdogs = $inactive->all();
    foreach my $wd ( @watchdogs ) {
        $wd->delete();
        $log->infof("Removing WD %s, name: %s, user: %s", $wd->id, $wd->name, $wd->user->id);
    }
}

=head2 enqueue_watchdog_run

Enqueues a watchdog run though Qurious

Usage:

 # Run the watchdog for real:
 $this->enqueue_watchdog_run($watchdog);

 # Just initialize the watchdog:
 $this->enqueue_watchdog_run($watchdog , { real_run => 0 } );

=cut


sub enqueue_watchdog_run{
    my ( $self, $watchdog , $params ) = @_;

    $params //= {};

    $log->info("Enqueuing run of watchdog ".$watchdog->id());

    my $job = $self->stream2->qurious->create_job(
                                                  class => 'Stream2::Action::WatchdogRun',
                                                  queue => 'BackgroundJob',
                                                  parameters => {
                                                                 watchdog_id =>  $watchdog->id(),
                                                                 real_run => 1,
                                                                 %$params,
                                                                }
                                                 );
    $job->enqueue();
    return $job;
}

=head2 get_due_watchdogs

Gets an Resultset of due watchdogs.

Usage:

 my $due_watchdogs = $this->get_due_watchdogs();

 while( my $watchdog = $due_watchdogs->next() ){
   ...
 }

=cut

sub get_due_watchdogs{
    my ($self) = @_;
    return $self->search({ next_run_datetime => { '<=' => \'NOW()' }, active => 1 });
}

=head2 runner

Gets a new instance of a watchdog runner for the given
watchdog.

Usage:

  $this->runner($watchdog)->initialize();

  $this->runner($watchdog)->run();

=cut

sub runner{
    my ($self, $watchdog) = @_;
    return Stream2::Watchdogs::Runner->new( { watchdogs => $self,
                                              watchdog => $watchdog });
}

=head2 create

Same as DBIx Create, except it check for a watchdog limit (and fails if the limit it hit).

=cut

around 'create' => sub{
    my ($orig, $self, $properties ) = @_;
    $properties //= {};

    my $user_id = $properties->{user_id};
    unless( $user_id ){ confess("Missing user_id in properties"); }

    my $schema = $self->stream2->stream_schema();
    my $stuff = sub{
        my $user = $schema->resultset('CvUser')->find($user_id);
        unless( $self->user_can_create_watchdog( $user ) ){
            confess("More than ".$self->watchdogs_limit()." watchdogs for user $user_id. Cannot create a new one");
        }
        return $self->$orig($properties);
    };
    return $schema->txn_do($stuff);
};

=head2 user_can_create_watchdog

The definitive and final word in user related watchdog creation

    if ( $watchdogs->user_can_create_watchdog( $user ) ){
        // create a watchdog
    }

=cut

sub user_can_create_watchdog {
    my ( $self, $user ) = @_;

    # it begins - client customisations
    if ( $user->group_identity() eq 'khr' ){
        # 78470166 - khr want 10 watchdogs for helping us beta test
        return 1 if ( $user->watchdogs()->count() < 20 );
    }

    # Count is strictly lower than limit. Means we can add at least one.
    return $user->watchdogs()->count() < $self->watchdogs_limit();
}


=head2 fetch_results

given a list of candidate_ids and a watchdog_id - retrieve a list of candidates

=cut

sub fetch_results {
    my ( $self, $watchdog_id, @candidate_ids ) = @_;
}


=head2 export_tov1

Exports the given watchdog to V1. Returns true on success. Dies on failure.

Usage:

 $this->export_tov1($watchdog);

=cut

sub export_tov1{
    my ($self, $watchdog) = @_;

    my $stream2 = $self->stream2();

    unless( $watchdog->user->provider() =~ /^adcourier/ ){
        confess("You can only export adcourier based users watchdogs");
    }

    my $res = $stream2->user_api->post_watchdog_toV1( $watchdog->user()->provider_id(),
                                                      { search => {
                                                                   name => $watchdog->name(),
                                                                   tokens => { $stream2->find_criteria( $watchdog->criteria_id() )->tokens() },
                                                                  },
                                                        destinations => [ map{ $_->board() } $watchdog->subscriptions()->all() ]
                                                      });
    unless( $res->{cv_watchdog_id} ){
        confess("Failed to export watchdog to V1: ".Dumper($res));
    }

    $log->info("Sucessfully exported watchdog to V1. Under number ".$res->{cv_watchdog_id});
    return 1;
}

=head2 import_company

Imports a company's watchdogs in this V2 system from V1 using the Juice
api and all sorts of clever stuff.

Usage:

  # Import ALL the users's watchdogs from a company:
  # With a good base URI (for the email links ). Will default to https://search.adcourier.com/

  $this->import_company('andy');

  $this->import_company('andy', undef , 'https://search.adcourier.com/');

  # Or even specifying the user provider, letting the base_url to be the default
  $this->import_company('andy', undef , undef, { provider => 'adcourier_ats' });


Additionaly, you can filter only the users you want by giving a sub:

  $this->import_company('andy', sub{
                                  my ( $v1_user ) = @_;
                                  # Something like:
                                  # {
                                  #    username => < .. a standard broadbean username .. >
                                  #    id => 123,
                                  #    consultant => "john",
                                  #    team => "A team",
                                  #    office => "The office",
                                  #    company => "Microsoft",
                                  #  },
                                  return $v1_user->{id} == 1234 ; # For instance
                                 }, 'https://search.adcourier.com/');

=cut

sub import_company{
    my ($self, $company, $user_filter, $base_url, $opts ) = @_;

    $user_filter //= sub{ return 1 ; };

    unless( $company ){ confess("Missing company"); }
    unless( $base_url ){
        $log->warn("Missing Base URL. Will default to https://search.adcourier.com/");
        $base_url = 'https://search.adcourier.com/';
    }
    $opts //= {};
    my $users_provider = $opts->{provider} // 'adcourier';

    my $stream2 = $self->stream2();
    my $user_api = $stream2->user_api();

    # Prepare the data.
    my $users = [];
    my $users_watchdogs = {}; # Keyed by $user->{id}
    my $users_subscriptions = {}; # Keyed by $user->{id}

    $users = [ $user_api->company_users($company) ];
    $log->info("Got ".scalar(@$users)." users for company $company");

    foreach my $user ( @$users ){
        unless( &{$user_filter}($user) ){
            next;
        }

        ## Get only active watchdogs to save time.
        my @watchdogs = $user_api->get_watchdogs($user->{id} , { active => 1 });
        $log->info("Got ".scalar(@watchdogs)." watchdog for user ".$user->{id}." ".$user->{username});
        $users_watchdogs->{$user->{id}} = \@watchdogs;
        $users_subscriptions->{$user->{id}} = $stream2->stream_api->get_active_subscription_tokens($user->{id});
    }


    # Filter out users without  watchdogs
    $users = [ grep{ $users_watchdogs->{$_->{id}} } @$users ];

    # Got all the data needed from APIs.

    my $schema = $stream2->stream_schema();

    # Ok got all the data just fine. Time to import stuff in one go.
    $log->info("Importing in Stream2 Database");
    foreach my $v1_user ( @$users ){

        # The sub will fill that in a transaction.
        my @to_initialise = ();
        my @results_to_backfill_from = ();

        my $results_rs = $stream2->factory('WatchdogResult');

        my $stuff = sub{
            $log->info("Considering watchdogs for V1 user ".$v1_user->{username}." ( ID = ".$v1_user->{id}.")");
            my @v1_watchdogs = @{ $users_watchdogs->{$v1_user->{id}} };
            unless( @v1_watchdogs ){
                $log->debug("User ".$v1_user->{username}." Does not have any watchdogs. Skipping");
                return;
            }

            # First the user.
            my $full_v1_details = $stream2->user_api->get_user($v1_user->{id});

            my $group_client = $schema->resultset('Group')->find_or_create({ identity => $company });

            my $v2_user = $schema->resultset('CvUser')->find_or_create({
                provider => 'adcourier', # UNIQ
                last_login_provider => $users_provider,
                provider_id => $v1_user->{id}, # UNIQ
                group_identity => $group_client->identity(), # NOT NULL
                contact_email => $v1_user->{email} || undef,
            });
            $log->info("Vivified V2 user ".$v2_user->id());
            $v2_user->timezone($full_v1_details->{timezone} || 'Europe/London');
            $v2_user->locale($full_v1_details->{locale} || 'en' );
            $v2_user->group_identity($company);
            $v2_user->update();

            my $v2_user_object = $stream2->factory('SearchUser')->wrap($v2_user);

            # Then save the subscriptions of this user.
            Stream2::SearchUser::Subscriptions->new({
                                                     ref     => $users_subscriptions->{$v1_user->{id}},
                                                     key     => $stream2->config()->{subscription_key} // confess("MISSING subscription_key in Config"),
                                                     user    => $v2_user_object,
                                                     schema  => $schema,
                                                    })->save or confess "Subscription storage failed";

            ## We need 2am IN THE TIMEZONE of the user.

            my $tomorrow_at_2am = DateTime->now( time_zone => $v2_user->timezone() )->add( days => 1 )->set_minute(0)->set_hour(2);

            # And back to UTC
            $tomorrow_at_2am = $tomorrow_at_2am->set_time_zone('UTC');
            $log->info("Watchdogs will run at UTC ".$tomorrow_at_2am." (user time zone = ".$v2_user->timezone().")");

            # Loop through the watchdogs
            my $considered = 0;
            my $imported = 0;

            foreach my $v1_watchdog ( @v1_watchdogs ){
                # {
                #     local $Data::Dumper::Maxdepth = 1;
                #     $log->trace("Considering watchdog: ".Dumper($v1_watchdog));
                # };

                $considered++;

                unless( $v1_watchdog->{active} ){
                    $log->debug("Skipping inactive watchdog (V2 doesnt have any notion of active/inactive watchdog)");
                    next;
                }

                my $v1_search = $v1_watchdog->{search};

                my $v2_name = $v1_search->{name} || 'Imported watchdog';
                if( length($v2_name) > 255 ){
                    $v2_name = substr($v2_name, 0, 255);
                }

                my $v2_criteria = $v1_search->{attributes};
                # Scrub those criteria a little bit.
                foreach my $key ( keys %$v2_criteria ){

                    if( ( grep { $key eq $_ } qw/destination sudo_username rand/  ) ){
                        delete $v2_criteria->{$key}; next;  # Forbidden keys
                    }

                    if( $key =~ /^_/ ){
                        delete $v2_criteria->{$key}; next; # private keys
                    }

                    # Scrub out keys where the arrayref of value only contains empty rubbish
                    my @values = @{ $v2_criteria->{$key} };
                    unless( scalar(@values) ){
                        delete $v2_criteria->{$key}; next; # No values at all
                    }

                    unless( grep{ length( $_ ) } @values ){
                        delete $v2_criteria->{$key}; next; # Only blank values
                    }

                } # End of values scrubbing


                ## Expand location_id to location using the magic api
                if( my $location_id = $v2_criteria->{location_id} ){
                    $location_id = ref($location_id) ? $location_id->[0] : $location_id;
                    if( $location_id ){
                        $log->info("Got location_id = ".$location_id." will make sure there is a location token");
                        if( my $location = $self->stream2->location_api->find($location_id) ){
                            my $specific_path = $location->specific_path();
                            $log->info("Will set location string as '$specific_path'");
                            $v2_criteria->{location} //= [ $location->specific_path() ];
                        }
                    }
                }

                # $log->info("V2 Criteria will be: ".Dumper($v2_criteria));

                # Build a criteria object
                my $criteria = Stream2::Criteria->new(
                                                      schema => $schema,
                                                     );
                foreach my $field ( keys %$v2_criteria ) {
                    $criteria->add_token($field => $v2_criteria->{$field});
                }
                $criteria->save();
                my $criteria_id = $criteria->id();
                $log->info("Criteria ID: ".$criteria_id);

                # Time to build a watchdog.
                # Is there a watchdog with this criteria ID?
                my $watchdog = $schema->resultset('Watchdog')->search({ user_id => $v2_user->id(),
                                                                        criteria_id => $criteria_id
                                                                      })->first();


                unless( $watchdog ){
                    $log->info("Watchdog for these criteria ($criteria_id) is not there. Creating it with name '$v2_name'");
                    $watchdog = $schema->resultset('Watchdog')->create({ user_id => $v2_user->id(),
                                                                         criteria_id => $criteria_id,
                                                                         name => $v2_name,
                                                                         base_url => $base_url,
                                                                       });
                    $log->info("Will queue it for initialization");
                    push @to_initialise , $watchdog;
                }else{
                    $log->info("Watchdog for these criteria ($criteria_id) already exists. Will update it");
                }

                $watchdog->next_run_datetime($tomorrow_at_2am);
                $watchdog->update();

                foreach my $destination ( @{ $v1_watchdog->{destinations} // [] } ){
                    $log->info("Setting up destination '$destination' for watchdog");
                    # Find the subscription
                    my $subscription = $v2_user->subscriptions()->find({ board => $destination });
                    unless( $subscription ){
                        $log->warn("No subscription found for board '$destination' , user ".$v2_user->id());
                        next;
                    }

                    my $watchdog_subscription = $watchdog->watchdog_subscriptions()->find({ subscription_id => $subscription->id() });

                    unless( $watchdog_subscription  ){
                        # Add the subscription to the watchdog
                        $watchdog_subscription = $watchdog->add_to_watchdog_subscriptions({ subscription => $subscription });
                        $log->info("Successfully added subscription '".$destination."' to watchdog");
                    }

                    # Alright. Time to import the already viewed candidates.
                    my @candidates = @{ $v1_watchdog->{destinations_candidates}->{$destination} // [] };
                    # Protect against candidates with no ID or no time
                    @candidates = grep { $_->{candidate_id} && $_->{time_received} } @candidates ;
                    # $log->trace("Found candidates from API: ".Dumper(\@candidates));

                    # Foreach candidate, vivify the watchdog result.
                    foreach my $v1_candidate ( @candidates ){
                        my $candidate_id = $destination.'_'.$v1_candidate->{candidate_id};
                        my $insert_dt = DateTime::Format::MySQL->parse_datetime( $v1_candidate->{time_received} );
                        # Set the timezone from Floating to UTC. We know it's UTC. If it's not, blame the API.

                        ## We dont do that here. We will fire something to do this asynchronously.
                        # my $content = $stream2->stream_api->get_candidate( $v1_candidate->{id} );

                        $insert_dt->set_time_zone('UTC');
                        $results_rs->find_or_create({ candidate_id => $candidate_id,
                                                      subscription_id => $subscription->id(),
                                                      watchdog_id => $watchdog->id(),
                                                      viewed => 1,
                                                      insert_datetime => $insert_dt,
                                                      last_seen_datetime => $insert_dt,
                                                      content =>  { v1_candidate_id => $v1_candidate->{id} } # We only store v1 candidate ID for now.
                                                    });
                        $log->debug("Imported candidate $candidate_id , found at ".$insert_dt->iso8601()." (UTC)");
                    }

                    # Find the earliest result we should populate asynchronously.
                    my $latest_result = $results_rs->search({ subscription_id => $subscription->id(),
                                                              watchdog_id => $watchdog->id()
                                                            },
                                                            { order_by => { -desc => 'insert_datetime' },
                                                              rows => 1
                                                            }
                                                           )->first();
                    if( $latest_result ){
                        push @results_to_backfill_from , $latest_result;
                    }

                }

                $imported++;

            } # End of v1 watchdogs loop

            $log->info("Considered $considered watchdogs. Imported only $imported");
        };

        $schema->txn_do($stuff);

        # Transaction is successfull, we can initialize watchdogs.
        $log->info("Will initialize ".scalar(@to_initialise)." watchdogs");
        foreach my $watchdog ( @to_initialise ){
            $self->enqueue_watchdog_run($watchdog, { real_run => 0 });
        }

        # We can also launch backfilling results.
        foreach my $backfill_result ( @results_to_backfill_from ){
            $log->info("Will backfill results from ".join('-', $backfill_result->id()));
            $stream2->factory('WatchdogResult')->enqueue_backfill_from($backfill_result);
        }

    } # End of user loop

    return 1;
}

__PACKAGE__->meta->make_immutable();

package Stream2::Monitor;

use Moose;

use Log::Any qw/$log/;

has 'stream2' => ( is => 'ro' , isa => 'Stream2' , required => 1 );

=head1 NAME

Stream2::Monitor - A mutual exclusion utility object.

=head2 try_mutex

Tries to execute the given code under the given mutual exclusion key.
Does NOT return anything interesting.

If it fails, the given failure code will be executed. Note that the failure
code is optional.

Usage:

 $this->try_mutex('object_id' , sub{ do stuff exclusively },
                                sub{ this object is already busy

=cut

sub try_mutex{
    my ($self, $key, $code, $fail) = @_;

    $key // confess("Missing key");
    $code // confess("Missing code to execute");

    $fail //= sub{};

    # Lock on this key

    $log->debug("Will try mutex on key '$key'");

    if( $self->stream2->stream_schema->try_acquiring_lock($key)  ){
        eval{
            &$code();
        };
        my $err = $@;
        $self->stream2->stream_schema->release_lock($key);
        if( $err ){
            confess("Uncaught error (mutex on $key): $err");
        };
    }else{
        eval{
            &$fail();
        };
        if( my $err = $@ ){
            confess("Uncaught error in alternative: $err");
        }
    }
}

=head2 is_free_lock

If you would like to check if a lock is in place without actually aquiring it

=cut

sub is_free_lock {
    my ( $self, $key ) = @_;

    $key // confess("Missing key");

    if ( $self->stream2->stream_schema->is_free_lock( $key ) ){
        return 1;
    }

    return 0;
}

__PACKAGE__->meta->make_immutable();

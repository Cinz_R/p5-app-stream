package Stream2::ForkManager;
use Moose;
use Log::Any qw/$log/;

my $REFORK_SIGNAL = 10; # This is SIGUSR1

has 'main' => ( is => 'ro' , isa => 'CodeRef' , required => 1);
has 'start_n' => ( is => 'ro' , isa => 'Int' , required => 1 , default => 1);

has 'arguments_array' => ( is => 'ro' , isa => 'ArrayRef[ArrayRef]' , default => sub{ [ [] ]; } );

has 'autostart' => ( is => 'ro' , isa => 'Bool' , required => 1 , default => 1);
has 'refork_on_signals' => ( is => 'ro', isa => 'ArrayRef[Int]' , required => 1, default => sub{ [ 11, 13 ]; } );

# Mutable stuff
# Holds the current children PIDs
has 'children' => ( is => 'rw' , isa => 'ArrayRef[Int]' , lazy_build => 1);

# Holds the mapping of children PID to arguments.
has 'child_arguments' => ( is => 'ro' , isa => 'HashRef[ArrayRef]', default => sub{ {}; });


# Father is set in children only
has 'father' => ( is => 'rw' , isa => 'Maybe[Int]');

# Building the children will fork N processes.
sub _build_children{
    my ($self) = @_;
    my @children = ();

    my @arguments = @{$self->arguments_array()};
    my $size_args = scalar(@arguments);

    for my $i ( 1..$self->start_n ){

        # Choose the arguments based on the index.
        my $arguments = $arguments[ ( $i - 1 ) % $size_args ];

        if( my $pid = $self->_fork_process($arguments) ){
            $self->child_arguments->{$pid} = $arguments;
            push @children , $pid;
        }
    }
    return \@children;
}

sub BUILD{
    my ($self) = @_;
    if( $self->autostart() ){
        # Accessing self children will do some forking.
        $self->children();
    }
}

=head1 NAME

Stream2::ForkManager - Convenient forking manager for multiprocesses stuff.

=head1 SYNOPSYS

   my $fn = Stream2::ForkManager->new({ main => sub{
                                                 my ($fork_manager) = @_;
                                                 my $s2 = $s2->clone();
                                                 work();
                                              },
                                        start_n => 10,
                                       });
$fb->wait_children();

=head1 Properties and Methods

=head2 main

The main CodeRef to execute after forking. Mandatory. It is not advised to potentially
die in your 'main' code.

As a last resort solution, you should wrap everything in an eval and log some errors.

=head2 start_n

The number of processes to start.

=head2 autostart

Boolean. Indicates if child processes should auto start on construction or not.
Default it true.

If false, you can start children processes by either:

 - Accessing children() ( Non-blocking ).

 - Calling wait_children() (Blocking).

=head2 refork_on_signals

ArrayRef of signal IDs according to kill -l (or http://man7.org/linux/man-pages/man7/signal.7.html ).

If any of the children exits with one of those signals, this manager WILL refork one if it's run
under the 'wait_children' mechanism.

Default: 11 and 13 (Seg fault and Sig pipe). Use with care.

=head2 children

Do not SET that yourself EVER. ArrayRef of all children PIDs

Usage:

  my @children = @{$this->children()};

=head2 has_child

Is the given PID a child of this?

Usage:

  if( $this->has_child($pid) ){
    ...
  }

=head2 wait_children

Waits until all children terminate. (Children will be forked if they are not there already).

This is blocking. Will return the number of terminated children processes when
they all exited.

Usage:

   $fm->wait_children();

=cut

sub has_child{
    my ($self, $pid) = @_;
    return scalar(grep { $_ == $pid } @{$self->children()});
}

sub wait_children{
    my ($self) = @_;


    # Make sure children are forked.
    $self->children();

    my $n_terminated = 0;

    # Wait for any running child.
    while( ( my $pid = wait() ) > 0 ){

        unless( $self->has_child($pid) ){
            $log->error("Father process $$ shouldnt have this direct child [$pid]");
            next;
        }

        $n_terminated++;
        my $child_code = ${^CHILD_ERROR_NATIVE};
        my $signal_code = $child_code & 127;
        my $exit_code = $child_code >> 8;
        $log->info("Child [$pid] has exited with ".$child_code." SIGNAL: ".$signal_code." EXIT: ".$exit_code);

        # Get the original arguments so we can reuse them
        my $original_arguments = delete $self->child_arguments->{$pid} // confess("NO ARGUMENTS FOUND FOR PID $pid");

        # Remove from the list.
        $self->children( [ grep{ $_ != $pid } @{$self->children()} ] );


        # Right, what should we do?
        if( $signal_code == $REFORK_SIGNAL ){
            $log->info("Child [$pid] has exited with $signal_code = REFORK_SIGNAL. Forking a new one");
            # Easy, we need to restart a new one.
            if( my $new_pid = $self->_fork_process($original_arguments) ){
                # Remember the arguments against the new pid
                $self->child_arguments()->{$new_pid} = $original_arguments;
                push @{$self->children()} , $new_pid ;
            }
            next;
        }

        # Other managed signals that can have been sent by the OS
        if( grep{ $_ == $signal_code } @{$self->refork_on_signals()}  ){
            $log->info("We are resilient against this signal $signal_code. Reforking");
            if( my $new_pid = $self->_fork_process($original_arguments) ){
                # Remember the arguments against the new pid.
                $self->child_arguments()->{$new_pid} = $original_arguments;
                push @{$self->children()} , $new_pid ;
            }
            next;
        }

    }

    $log->info("No more processes to wait for. $n_terminated returned");
    return $n_terminated;
}


=head2 kill_all

Kill all children. Note that this can only happen in some sort
of signal handlers, because no children exists before you
call the blocking wait_children();

Usage:

 $this->kill_all();

=cut

sub kill_all{
    my ($self, $signal) = @_;
    $signal ||= 9;
    my @children = @{$self->children()};
    $log->info("Sending signal $signal to children ".join(', ' , @children));
    return kill $signal , @children;
}

=head2 refork

* Class method *

To be used in a children process.

Will cause the fork manager to refork the given process.
(Remember this could fail due to lack of resources).

Example of usage:

  my $fm = Stream2::ForkManager
        ->new({ main => sub{
                  ...
                  do some stuff for a while
                  ...
                  Stream2::ForkManager->refork();
               }
              });

=cut

sub refork{
    my ($class) = @_;
    # Commit suicide with the REFORK_SIGNAL
    kill $REFORK_SIGNAL , $$;
}


# WARNING. DEMOLISH WILL ALSO BE CALLED IN THE CHILDREN.
sub DEMOLISH{
    my ($self) = @_;

    if( $self->father() ){
        # We are in a child. Nothing to do.
        return;
    }

    # In the father, wait for children if some are left.
    if( @{$self->children()} ){
        my $message = Carp::longmess(qq/Some left-over children are there in $self. Automatically killing and waiting for them to avoid Zombies

Really you should not count on this module to do that for you. Be nice and call wait_children after you have started your processes.

/);
        warn( $message );
        $log->critical($message);
        $self->kill_all();
        $self->wait_children();
    }
};

# Forks,  and return a new PID. The main will be executed in the new proccess
# with the given $self and arguments.
sub _fork_process{
    my ($self, $arguments) = @_;

    $arguments // confess("Missing arguments");

    my $father = $$;
    my $pid = fork();
    unless( defined $pid ){
        $log->error("Cannot fork: $!");
        return undef;
    }
    if( $pid ){
        $log->info("Forked a new process: $pid");
        return $pid;
    }

    # Child process.
    $self->father($$);
    # Last resort protection against Perl exceptions
    eval{
        $self->main()->($self , @$arguments);
    };
    if( my $err = $@ ){
        $log->critical("Exiting Process $$ with exit status 1 because of this uncaught Perl exception: $err");
        exit(1);
    }

    # All went smoothly
    exit(0);
}


__PACKAGE__->meta->make_immutable();

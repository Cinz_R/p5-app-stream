package Stream2::Results::Result;

use strict;
use base qw(Class::Accessor::Fast);

=head1 NAME

Stream2::Results::Result - A Candidate as a result of a search. Can belong to a L<Stream2::Results> though the link is not kept.

=head2 NOTES

All date attributes are epoch (in Secs) UTC times.

=head1 ATTRIBUTE

Note that using attribute setters is not clear at the moment.

Some attributes are able to be set using vanilla accessors (from Class::Accessor), some of them
seem to be able to handle a  L<XML::XPathEngine::NodeSet> or even a XML::XPathEngine::Node to set their value.

Native attributes include:

=item candidate_id

The string identifier of this candidate IN THE destination. candidate_id/destination form a primary key.

=item destination

The string identifier of the source of the candidate ( talentsearch, careerbuilder, stackoverflow, etc..)

=item idx

Transient property. The index of the candidate in the original search result L<Stream2::Results>.

=item name

The name

=item cv_downloaded

Transient property. The search code has calculated that this candidate has been downloaded already.

=cut

use Carp;

use DateTime;
use DateTime::Format::Strptime;
use Digest::SHA1;

use JSON qw//;

use Encode;
use MIME::Entity;
use MIME::Base64::URLSafe;

use Scalar::Util qw(weaken);
use Stream::SharedUtils qw(NOW);

use XML::LibXML;
use Log::Any qw/$log/;

my $CV_SEARCH_SOURCE_ID = 3;

# Create some standard accessors
__PACKAGE__->mk_accessors(
    'idx' , # The index of this candidate in the Stored Result.
    'destination', # AKA feed name, or template name. You choose.
    'cv_downloaded' , # Has the cv been downloaded?
    'has_other_attachments', # This candiddate has other attachments, not just a CV.
    'has_profile',
    'profile_chargeable',
    'profile_downloaded',
    'shortlisted',
    'ignore_record',
    'cv_link',
    'tag_doc',
    'hr_xml', # An HR-XML standard xml string transformed from Burning Glass xml.
);

# backwards compatibility, because we don't want to have to maintain two versions of a feed
# the flag used to be called has_chargeable.. and has since been changed
*has_chargeable_profile = \&profile_chargeable;

# Some less standard accessors.
# Less standard cause they will be added some XML Node magic in their setter form.
BEGIN {
    no strict 'refs';
    foreach my $accessor ( qw(candidate_id name contact_telephone location_text cv_last_updated purchased) ) {
        *{__PACKAGE__.'::'.$accessor} = sub {
            return $_[0]->{$accessor} if @_ == 1;

            my $value = $_[1];
            return $_[0]->{$accessor} = _decode_node($value);
         };
    }
};

=head2 profile_keys

Arrayref of attribute keys relevant to the candidate profiles

=cut

sub profile_keys {
    return [qw(
        name
        headline
        latitude
        longitude
        address
        telephone
        email
        has_other_attachments_name
        has_other_attachments
        warnings
    )];
};

=head2 email

email address accessor.

Usage:

 my $email = $self->email();
 $self->email('bla@example.com');

=cut

sub email {
    return $_[0]->{email} if @_ == 1;

    my $self = shift;
    my $email = _decode_node(shift);

    $email =~ s/^mailto://;

    $self->{email} = $email;

    return $self->{email};
}

=head2 search_record_id

The search_record_id associated with this candidate.

Might be undef when no such thing exists.

=cut

sub search_record_id{
    my ($self) = @_;
    return scalar( $self->get_attr( 'search_record_id' ) ) || undef;
}

=head2 action_hash

Returns a quick hashmap to include in a CandidateActionLog data.

Usage:

  my %quick_hash = $this->action_hash();

=cut

sub action_hash{
    my ($self) = @_;
    my %hash = ();
    if( my $email = $self->email() ){
        $hash{candidate_email} = $email;
    }
    if( my $name = $self->name() ){
        $hash{candidate_name}  = $name;
    }
    return %hash;
}

=head2 attr

General attribute accessor.

Usage:

 my $value =  $this->attr('blabla');
 $this->attr('blabla' , $value);

Multiple values are set like that:

 $this->attr('blabla' , $value1, $value2 , ...);

Note that there is NO way to make sure an attribute is an array ref with only one element..

DO NOT do:

  $this->set_attr('blabla' , [ $value1 , $value2 ] );

Or kittens will die.

=cut

sub attr {
    if ( @_ == 2 ) {
        # getter
        return $_[0]->get_attr( $_[1] );
    }

    # setter
    my $self = shift;
    return $self->set_attr( @_ );
}

=head2 add_tag

Shortcut to dynamically add tags in the tags attribute.

This doesnt save the tag or anything, it's only for this transient object.

Usage:

 $this->add_tag('a tag');

=cut

sub add_tag{
    my ($self, $new_tag) = @_;
    my $tags = $self->{'tags'} // [];
    my %tags = map { $_ => 1 } @$tags;
    $tags{$new_tag} = 1;
    $self->{'tags'} = [  sort keys %tags ];
}

=head2 remove_tag

Removes the given tag from this in memory candidate result.

Returns the old tag if such a thing was there, does nothing otherwise and returns undef.

Usage:

  $this->remove_tag('blabla');

=cut

sub remove_tag{
    my ($self, $to_remove) = @_;
    my $tags = $self->{'tags'} // [];
    my %tags = map { $_ => 1 } @$tags;
    my $deleted = delete $tags{$to_remove};
    $self->{'tags'}  = [ sort keys %tags ];
    return $deleted;
}

=head2 tags

Returns the Array of tags on this result.

Usage:

 my @tags = $this->tags();

=cut

sub tags{
    my ($self) = @_;
    my $tags =  $self->{'tags'} // [];
    return @$tags;
}

=head2 set_attr

Generic attribute setter.

Set one value:

 $this->set_attr('blabla' , $value1 );

Set multiple values:

 $this->set_attr('blabla', $value1 , $value2 );

Note that there is NO way to make sure the value is an array of one element only.

DO NOT do:

  $this->set_attr('blabla' , [ $value1 , $value2 ] );

You have been warned.

=cut

sub set_attr {
    my $self = shift;
    my $attr = shift;
    my (@values) = @_;

    @values = map {
        _decode_node( $_ )
    } @values;

    if ( @values == 1 ) {
        return $self->{$attr} = $values[0];
    }

    return $self->{$attr} = \@values;
}

=head2 get_attr

Funky Context aware general getter. Context awareness is BAAAAAAD.

The context awareness infects whatever is returning a call to that of course.

Returns an Array if called in array context and if the requested attribute
is an array ref.

Usage:

 my @things = $this->get_attr('things');
 my $things_array_ref = $this->get_attr('things');

 my @stuffs = $this->get_attr('stuff');
 # $stuff[0] is the stuff or not.

 my $stuff = $this->get_attr('stuff');

Be very careful if you use that as hash values or subroutine call parameters..

scalar is your friend.

You have been warned.

=cut

sub get_attr {
    my $return = $_[0]->{$_[1]};
    if ( ref( $return ) ) {
        if ( wantarray ) {
            return @$return;
        }
    }
    return $return;
}

=head2 get_attr_arrayref

As Context awareness is BAAAAAAD. (see attr and get_attr).

As a simplification this will only return an arrayref, and only an arrayref.

Usage:

 my $ArrayRefofthings = $this->get_attr_arrayref('things');

The returned array ref will be empty in case 'things' is undef, thus guaranteing that
this method returns something sensible.

=cut

sub get_attr_arrayref {
    my $return = $_[0]->{$_[1]};

    if ( ref( $return ) eq 'ARRAY' ) {
        return $return;
    }

    unless( defined( $return ) ){
        return [];
    }

    return [$return];
}

=head2 cv_last_updated_from_date

Re-inventing DateTime::Format::* at the wrong place just set the cv_last_updated
as an epoch.

=cut

sub cv_last_updated_from_date {
    my $self = shift;
    my $date_node = shift;

    my $date = _decode_node( $date_node );

    if ( $date ) {
        $date =~ s{-}{/}g;
        my ($d,$m,$y) = split '/', $date;
        if ( $d && $m && $y ) {
            # transparently handle reversed dates (YYYY-MM-DD)
            if ( $d =~ m/^\d{4}$/ ) {
                ($d,$m,$y) = ($y,$m,$d);
            }
            my $dt = eval {
                DateTime->new(
                    day   => $d,
                    month => $m,
                    year  => $y,
                );
            };

            if ( !$@ && $dt ) {
                return $self->cv_last_updated( $dt->epoch() );
            }
        }
    }

    return $self->cv_last_updated( $date );
}

sub cv_last_updated_format {
    my $self = shift;
    my $pattern = shift;
    my $date_node = shift;

    my $date = _decode_node( $date_node );

    my $epoch = $self->pattern_to_epoch( $pattern, $date );

    return $self->cv_last_updated( $epoch || $date );
}

=head id

Returns a globally unique ID for this candidate that is either a concatenation
of the candidates destination and candidate_id OR a BASE64 encoded binary SHA1
of this candidate's cv_link.

Usage:

 my $id = $this->id();

=cut

sub id {
    my $self = shift;

    if ( $self->candidate_id ){
        return $self->destination.'_'.$self->candidate_id;
    }

    return MIME::Base64::URLSafe::urlsafe_b64encode(Digest::SHA1::sha1($self->cv_link // 'UNDEF' ));
}

=head2 pattern_to_epoch

Smoke around DateTime::Format::Strptime.

Hides wrong dates formats under the carpet.

=cut

sub pattern_to_epoch {
    my $self = shift;
    my $pattern = shift;
    my $date_string = shift;

    if ( !$date_string ) {
        return;
    }

    my $strp = eval {
        DateTime::Format::Strptime->new( pattern => $pattern );
    };

    if ( $@ || !$strp ) {
        return;
    }

    my $dt = eval {
        $strp->parse_datetime( $date_string );
    };

    if ( $@ || !$dt ) {
        return;
    }

    return $dt->epoch();
}

# Piece of code that infects this data oriented class with a runtime dependency on XML::XPathEngine
# Refactor this away when it bites us.
#
sub _decode_node {
    my $node = shift;

    if ( !ref( $node ) ) {
        return $node;
    }

    my (@nodes);
    if ( UNIVERSAL::isa( $node, 'XML::XPathEngine::NodeSet' ) ) {
        (@nodes) = $node->get_nodelist();
    }
    else {
        @nodes = ( $node );
    }

    return _nodes_to_text( @nodes );
}

# See above.
sub _nodes_to_text {
    my @nodes = @_;

    my @values;
    foreach my $node ( @nodes ) {
        my $value = $node;
        if ( ref($node) ) {
            if ( UNIVERSAL::can( $node, 'as_trimmed_text' ) ) {
                $value = $node->as_trimmed_text();
            }
            elsif ( UNIVERSAL::can( $node, 'string_value' ) ) {
                $value = $node->string_value();
            }
        }

        push @values, $value;
    }

    return wantarray ? (@values) : shift @values;
}

=head2 attributes

Sets this candidate free form attributes in one go.

Usage:

 $this->attributes( drinks => 2, pie => 'beer' );

=cut

sub attributes {
    return attr( @_ ) if @_ == 2 || @_ == 3;

    my $self = shift;

    while ( @_ ) {
        my $attr = shift;
        my $value = shift;

        $self->set_attr( $attr => $value );
    }
}

sub attrs{
    # Carp::cluck( "'attrs' is deprecated'. Use 'attributes' instead" );
    goto &attributes;
}

=head2 delete_attr

Shortcut for delete $self->{$attr}.

Usage:

 $this->delete_attr('blabla');

=cut

sub delete_attr {
    my $self = shift;
    my $attr = shift;
    return delete $self->{ $attr };
}

=head2 has_cv

Accessor for has_cv.

If has_cv is not explicitely set to a false value, assumes this candidate has a CV and returns true.

Usage:

 $this->has_cv(1);
 $this->has_cv(0);
 if( $this->has_cv() ){
   ...
 }

=cut

sub has_cv {
    my $self = shift;
    if ( @_ ) {
        return $self->attr( 'has_cv' => @_ );
    }

    if ( defined( $self->{has_cv} ) ) {
        return $self->{has_cv};
    }

    # assume they have a cv
    return 1;
}

=head2 can_forward

    Can this candidate be complicate in the forward action
    default: yes ( 1 )

    # usage
    $this->can_forward(0);
    if ( $this->can_forward() ){
        ..
    }

=cut

sub can_forward {
    my $self = shift;
    if ( @_ ){
        return $self->attr( 'can_forward' => @_ );
    }
    if ( defined( $self->{can_forward} ) ) {
        return $self->{can_forward};
    }

    # assume they can be forwarded
    return 1;
}

sub cached_cv {
    # TODO
    return 0;
}

# sets/gets a link to the profile of the result on the board
# if set, clicking the preview button in Stream will open this url
# in lightbox.
# you can also set when the preview link expires.
# nb. this link is not used by the Stream engine to download the profile
sub profile_link {
    my $self = shift;

    if ( @_ ) {
        return $self->set_profile_link( @_ );
    }

    return $self->get_profile_link();
}

sub get_profile_link {
    my $self = shift;

    if ( $self->profile_link_expired() ) {
        return '';
    }

    return $self->get_attr('profile_link');
}

sub set_profile_link {
    my $self = shift;
    return $self->set_attr('profile_link' => @_ );
}

sub profile_link_expired {
    my $self = shift;
    my $link_expires = $self->get_attr('profile_link_expires');
    if ( $link_expires && $link_expires <= time ) {
        return 1; # link has expired
    }

    return 0; # link is still live
}

sub profile_link_expires {
    my $self = shift;
    return $self->attr('profile_link_expires' => @_);
}

=head2 enrich_with_cvparsing

Enriches this with the given CV parsing result

Usage:

 my $res = Bean::CVP::Client parsing result (see Bean::CVP::Client)

 $this->enrich_with_cvparsing($res);

=cut

sub enrich_with_cvparsing{
    my ($self, $parser_res) = @_;
    unless( $parser_res ){ confess("Missing parser_res"); }

    $log->info("Enriching candidate with CV Parser result");

    if( ( ! $self->email() ) && ( my $email = $parser_res->{email} ) ){
        $log->info("Fixing email to be $email in candidate");
        $self->email($email);
    }
    unless( $self->name() ){
        if( my $first_name = $parser_res->{first_name} ){
            $log->info("Fixing firstname to be $first_name in candidate");
            $self->attr('firstname' ,$first_name);
        }
        if( my $last_name = $parser_res->{last_name} ){
            $log->info("Fixing lastname to be $last_name in candidate");
            $self->attr('lastname' , $last_name );
        }
    }

    if ( my $tag_doc = $parser_res->{bg_xml} ) {
        $log->info('Setting tag_doc on candidate with bg_xml');
        $self->tag_doc( $tag_doc );
    }

    if ( my $hr_xml = $parser_res->{hr_xml} ) {
        $log->info('Setting hr_xml on candidate');
        $self->hr_xml( $hr_xml );
        $self->_extract_tag_doc(undef, $parser_res);
    }

    # Build stuff again from basic properties
    # That includes name if needed
    $log->info("Fixing up the name etc");
    $self->fixup();
    $log->info("Fixing up done");

    return $self;
}


# This enriches self with HR-XML attributes we received
# from the CVP API extraction.
sub _extract_tag_doc {
    my ($self, $search_user, $cv_parsing_res) = @_;

    $log->info("Extracting HR XML attributes");

    my %extracted_applicant = %{$cv_parsing_res->{hr_xml_attributes} // {}};

    if ( !%extracted_applicant ) {
        $log->info('No extracted attributes found on candidate.')
        and return;
    }

    # parse the document and convert to HR xml
    my %applicant = ();

    # copy keys that need cleaning up into %applicant
    my ($applicant_email) = split /\s*\(or\)\s*/, $extracted_applicant{from_email} || '';
    $self->email( best_email( $self->email, $applicant_email ) );

    my ($mobile) = split /\s*,\s*/, $extracted_applicant{mobile} || '';
    my ($phone)  = split /\s*,\s*/, $extracted_applicant{phones} || '';
    $applicant{'applicant_mobile'} = $mobile;
    $applicant{'applicant_telephone'} = $phone;

    # copy keys that don't need any processing into %applicant
    my %applicant_mappings = (
        # key in TP2                   => key in $applicant_href
        'applicant_name'               => 'name',
        'applicant_extracted_postcode' => 'postcode',
        'applicant_address'            => 'address',
        'applicant_latitude'           => 'address_position_lat',
        'applicant_longitude'          => 'address_position_lon',
        'applicant_country'            => 'resides'
    );

    @applicant{ keys %applicant_mappings } = @extracted_applicant{ values %applicant_mappings };

    # handle the work experience
    my ($last_employer_ref) = ref($extracted_applicant{experience_ref}) ? $extracted_applicant{experience_ref}->[0] : undef;
    if ( $last_employer_ref ) {
        if ( exists($last_employer_ref->{employerName}) ) {
            $applicant{'employer_org_name'} = $last_employer_ref->{employerName};
            $applicant{employer_org_contact_internet} = $last_employer_ref->{employerDomain};

            my $positions_ref = $last_employer_ref->{positions} || [];
            my $last_position_ref = $positions_ref->[0];
            if ( $last_position_ref ) {
                if( $last_position_ref->{orgName} ){
                    $applicant{employer_org_name} ||= $last_position_ref->{orgName};
                }
                if( $last_position_ref->{positionTitle} ){
                    $applicant{employer_org_position_title} ||= $last_position_ref->{positionTitle};
                }
                $applicant{employer_org_position_industry_code} = $last_position_ref->{orgIndustry};
                $applicant{employer_org_position_start_date}    = $self->to_date(
                    $last_position_ref->{positionStart},
                );
                $applicant{employer_org_position_end_date}    = $self->to_date(
                    $last_position_ref->{positionEnd},
                );
            }
        }
    }

    if ( exists($applicant{applicant_name}) && $applicant{applicant_name} ) {
        $applicant{applicant_name} =~ s/<br>//gi;
    }

    # can we geolocate from the address?
    unless( $applicant{applicant_latitude} ){
        if ( my $address = $applicant{applicant_address} ){
            my $location_ref = eval { $search_user->stream2->geolocation_api->geocode( $address ); };
            unless ( $@ ){
                $applicant{applicant_latitude} = $location_ref->{latitude};
                $applicant{applicant_longitude} = $location_ref->{longitude};
            }
        }
    }

    # remove the empty attributes
    # eg. we don't want to replace an existing address with an empty address
    #     just because we couldn't grab the address from the CV
    foreach my $attribute ( keys %applicant ) {
        if ( !$applicant{$attribute} ) {
            delete $applicant{$attribute};
        }
    }

    # apply the new data to self
    map {
        $self->attr( $_ => $applicant{$_} )
    } keys %applicant;
}

=head2 bg_xml

Another name for $self->tag_doc();


=cut

sub bg_xml {
    my ($self) = @_;
    return $self->tag_doc();
}

=head2 make_email_up

Makes up an email address according to the given user.

Does not do anything if this candidate already have an email address.

=cut

sub make_email_up {
    my ($self, $user) = @_;

    if( $self->email() ){
        $log->warn("Candidate ".$self->id()." already have an email address ( ".$self->email().")");
        return;
    }

    # the domain part of a hostname must not contain underscores. As such, email addresses containing underscores as part
    # of the domain are invalid ( according to wikapedia and Email::Valid which is what is blocking the use in this instance
    my $email_domain = sprintf( '%s-%s', scalar( $self->id() ), scalar( $user->group_identity ) );
    $email_domain =~ s/_/-/g;

    my $email = sprintf( 'unknown_email@%s.com', $email_domain );
    $log->info("Making up fake email address '$email' for this candidate '".$self->id()."'");
    $self->email($email);
    return $email;
}


sub fixup {
    my $self = shift;

    $self->_build_name();
    return;
}

sub _build_name {
    my $self = shift;

    if ( $self->name() ) {
        return;
    }

    my $firstname = $self->attr('firstname') || $self->attr('first_name');
    my $lastname  = $self->attr('lastname' ) || $self->attr('surname') || $self->attr('last_name');

    if ( $firstname && $lastname ) {
        my $fullname = join(' ', $firstname, $lastname );
        $self->name( $fullname );
    }

    return;
}

sub save {
    my $self = shift;

    $self->fixup();

    # TODO
}

=head2 from_json

Builds an instance of myself given a JSON string of BYTES containing plain
attributes, and an index.

Usage:

  my $candidate = Stream2::Results::Result->from_json('{ "name": "blabla", ... }' , 0 );

=cut

sub from_json {
    my $class = shift;
    my $json = shift;
    my $idx = shift;

    my $result_ref = JSON::decode_json($json);
    $result_ref->{idx} = $idx;
    return $class->new($result_ref);
}

=head2 to_ref

Turns this into a pure Perl HashRef structure without any special processing.

Additionally, you can specify which exact attributes you are interested in if
you want to avoid the structure to be too big (see section Attributes).

You can also specify 'candidate_action_ref', a HashRef of this form:

  { <action_type> => [ collection, of, Stream2::O::CandidateActionLog, objects ] }

Usage:

  my $hash = $this->to_ref();

  my $hash = $this->to_ref({ attributes => [ 'name' , 'idx' , 'email' ] });
  my $hash = $this->to_ref({ candidate_action_ref => { message => [ .. ], profile => [ .. ] } });

=cut

sub to_ref {
    my ( $self , $options ) = @_;
    $options //= {};

    my @attributes = @{ $options->{attributes} // [ keys %$self ] };
    my $candidate_action_ref = $options->{candidate_action_ref} // {};

    # Some attributes are calculated properties
    # and do not necessarily exist in the object unless specifically called
    # i.e. has_cv
    my $candidate_ref;
    foreach my $attribute ( @attributes ){
        $candidate_ref->{$attribute} = $self->attr($attribute);
    }

    $candidate_ref->{has_cv} = $self->has_cv();
    $candidate_ref->{can_forward} = $self->can_forward();

    # All the action keys, but button bluifications.
    $candidate_ref->{recent_actions} = [ keys %{$candidate_action_ref} ];

    if( my $future_message_actions = $candidate_action_ref->{candidate_future_message} ){
        my $now_str = DateTime->now()->iso8601().'Z';
        my @active_future_message_actions =
            sort { $a->data()->{at_time} cmp $b->data()->{at_time} } # Only interested in the most upcoming one.
            grep{ ! $_->data()->{cancelled_on}  } # We dont want cancelled futures
            grep{ $_->data()->{at_time} ge $now_str } # We dont want past stuff
            @{$future_message_actions};
        if( my $most_immediate_future_message = $active_future_message_actions[0] ){
            $candidate_ref->{future_message} = {
                action_id => $most_immediate_future_message->id(),
                at_time => $most_immediate_future_message->data()->{at_time},
                template_name => $most_immediate_future_message->data()->{template_name},
            }
        }else{
            # There was some future messages, but they were cancelled or gone in the past.
            # so we need to force this property to be undef.
            $candidate_ref->{future_message} = undef;
        }
    }

    # The sent template IDs, taken from the message actions.
    $candidate_ref->{sent_template_ids} = [
        map { $_->data->{email_template_id} || (); }
          map {
            exists( $candidate_action_ref->{$_} )
              ? @{ $candidate_action_ref->{$_} }
              : ()
          } qw/message_auto message/
      ];

    return $candidate_ref;
}

=head2 candidate_xml_to_webhook_xml

Take XML generated with the to_xml() method and convert it into XML that validates against
share/ripple/ripple_webhook_candidate.dtd

Required arguments:

action_name    - (STRING) - The name of the action that we will eventually emit a webhook for. This also becomes the <type/>. E.g: candidate/shortlist
candidate_xdoc - (XML::LibXML Object) - Candidate XML object generated by the to_xml() method in this class
search_user    - (Stream2::SearchUser Object) - Used to get at the stream2 object to validate the xml against the DTD

Returns the modified candidate XML object which will validate against the ripple_webhook_candidate.dtd.

=cut

sub candidate_xml_to_webhook_xml {
    my ($self, $action_name, $candidate_xdoc, $search_user, $advert) = @_;

    $log->info("Turning the Ripple Candidate into a Webhook candidate XML");

    # rename root node to StreamCandidate
    map { $_->setNodeName('StreamCandidate') } $candidate_xdoc->findnodes('//SearchCandidate');

    my $remove_nodes = { map { $_ => 1 } qw/Version Time CustomList Option/ };

    my $xstream_cand = $candidate_xdoc->documentElement();

    foreach my $child (@{ $xstream_cand->childNodes() }) {
            $xstream_cand->removeChild($child) if $remove_nodes->{$child->nodeName};
    }

    # add these elements as they do not exist in the candidate xml
    my $fired_at_node = $candidate_xdoc->createElement('fired_at');
    $fired_at_node->appendText(DateTime->now()->iso8601().'Z');


    my $type_node = $candidate_xdoc->createElement('type');
    $type_node->appendText($action_name);

    # since there is no prependChild() method, we need to do this
    $xstream_cand->insertBefore($fired_at_node, $xstream_cand->firstChild());
    $xstream_cand->insertAfter($type_node, $fired_at_node);

    my $advert_node = $candidate_xdoc->createElement('Advert');
    $advert_node->appendChild( $candidate_xdoc->createElement('JobReference') )->appendText( $advert->{JobReference} );
    $advert_node->appendChild( $candidate_xdoc->createElement('JobTitle') )->appendText( $advert->{JobTitle} );
    $advert_node->appendChild( $candidate_xdoc->createElement('JobType') )->appendText( $advert->{JobType} );
    $xstream_cand->insertBefore( $advert_node, $candidate_xdoc->findnodes('//Candidate')->[0] );

    # Get rid of the second Doc if such a thing exists.
    my $second_doc = $candidate_xdoc->findnodes('StreamCandidate//Doc')->[1];
    if( $second_doc ){
        $log->info("Clearing up the second doc");
        my $parent = $second_doc->parentNode();
        $parent->removeChild($second_doc);
    }

    $candidate_xdoc->validate( $search_user->stream2->dtd('ripple/ripple_webhook_candidate.dtd') );

    $log->info("Done turning one Ripple candidate XML into a Webhook candidate");

    return $candidate_xdoc;
}

=head2 to_xml

Generate ripple candidate XML that validates against the ripple/candidate DTD.

This code hase been factored out of App::Stream2::Plugin::Auth::Provider::AdcourierAts::advert_shortlist_candidate() for more flexibility.

Required arguments:

advert         - (Hashref) - This can be undef, if defined, must contain keys 'advert_id' and a  'list_name' (CustomList in the candidate XML)
cv_hash        - (Hashref) - These keys must be specified: cv_mimetype, cv_content in b64 and cv_filename
search_user    - (Stream2::SearchUser Object) - Used to get at the stream2 object to validate the xml against the DTD
opts           - (Hashref) - This can be undef, if defined, must contain key 'cv_parsing_res', which is a hashref containing one of the required keys 'hr_xml' or 'bg_xml'

Returns a XML::LibXML object of the candidate xml

=cut

sub to_xml {
    my ($self, $advert, $cv_hash, $search_user, $opts) = @_;

    # apart from the email and name which is handled above, a lot of information is
    # available from the tag doc itself
    $self->_extract_tag_doc( $search_user, $opts->{cv_parsing_res} );

    my $xdoc = XML::LibXML::Document->new('1.0', 'UTF-8');
    my $xsearch_cand = $xdoc->createElement('SearchCandidate');
    $xdoc->setDocumentElement($xsearch_cand);
    $xsearch_cand->appendChild($xdoc->createElement('Version'))->appendText('3.0');
    $xsearch_cand->appendChild($xdoc->createElement('Time'))->appendText(DateTime->now()->iso8601());

    if( $advert ){
        # Its possible the advert is undef. This is in the case
        # we reuse this code to send a 'bare' candidate (not against an advert).
        my $list_name = $advert->{list_name} // confess("Missing list_name in advert");
        my $advert_id = $advert->{advert_id} // confess("Missing advert_id in advert");

        my $xlist = $xsearch_cand->appendChild($xdoc->createElement('CustomList'));
        $xlist->setAttribute('name' , $list_name);
        $xlist->appendChild($xdoc->createElement('Option'))->setAttribute('value' , $advert_id);
    }


    ## Add the usual custom fields.
    $xsearch_cand->appendChild($search_user->ripple_xdoc_custom_fields($xdoc));

    ## Add the candidate name/email and everything.
    my $xcand = $xsearch_cand->appendChild($xdoc->createElement('Candidate'));
    if( my $name = $self->name() ){ $xcand->appendChild($xdoc->createElement('Name'))->appendText($name); }
    if( my $email = $self->email() ){ $xcand->appendChild($xdoc->createElement('Email'))->appendText($email); }
    if( my $telephone = $self->contact_telephone() ){
        $xcand->appendChild($xdoc->createElement('Telephone'))->appendText($telephone);
    }

    # Note that some properties have 'applicant_<blabla>' version
    # This is because we can't be asked to consolidate stuff and prefer
    # to infect client code (like this one) with this kind of logic.
    if( my $mobile = ( $self->attr('mobile') || $self->attr('mobile_phone') || $self->attr('applicant_mobile') ) ){
        $xcand->appendChild($xdoc->createElement('Mobile'))->appendText($mobile);
    }
    if( my $channel = $self->destination() ){
        $xcand->appendChild($xdoc->createElement('ChannelId'))->appendText($channel);
    }
    if( my $candidate_id = $self->candidate_id() ){
        $xcand->appendChild($xdoc->createElement('ChannelCandidateId'))->appendText($candidate_id);
    }
    ## Yes this is horrible.
    if( my $board_nice_name = $opts->{board_nice_name} ){
        $xcand->appendChild($xdoc->createElement('ChannelName'))->appendText($board_nice_name);
    }
    elsif( my $channel = $search_user->stream2->factory("Board")->search({ name => $self->destination() })->first ){ # $opts is optional, ChannelName is not..
        $xcand->appendChild($xdoc->createElement('ChannelName'))->appendText($channel->nice_name);
    }

    if( my $location_text = $self->attr('location_text') ){
        $xcand->appendChild($xdoc->createElement('LocationText'))->appendText($location_text);
    }
    if( my $address = ( $self->attr('address') || $self->attr('applicant_address') ) ){
        $xcand->appendChild($xdoc->createElement('LocationAddress'))->appendText($address);
    }
    if( my $city = $self->attr('city') ){
        $xcand->appendChild($xdoc->createElement('LocationCity'))->appendText($city);
    }
    if( my $county = ( $self->attr('county') || $self->attr('applicant_county') ) ){
        $xcand->appendChild($xdoc->createElement('LocationCounty'))->appendText($county);
    }
    if( my $country = ( $self->attr('country') || $self->attr('applicant_country') ) ){
        $xcand->appendChild($xdoc->createElement('LocationCountry'))->appendText($country);
    }
    if( my $postcode = ( $self->attr('postcode') || $self->attr('zipcode') || $self->attr('applicant_extracted_postcode') )  ){
        $xcand->appendChild($xdoc->createElement('LocationPostCode'))->appendText($postcode);
    }
    if( my $longitude = $self->attr('applicant_longitude') ){
        $xcand->appendChild($xdoc->createElement('LocationLongitude'))->appendText($longitude);
    }
    if( my $latitude = $self->attr('applicant_latitude') ){
        $xcand->appendChild($xdoc->createElement('LocationLatitude'))->appendText($latitude);
    }
    if( my $education = $self->attr('education') ){
        $xcand->appendChild($xdoc->createElement('Education'))->appendText($education);
    }
    if( my $work_permit = $self->attr('work_permit') ){
        $xcand->appendChild($xdoc->createElement('WorkPermit'))->appendText($work_permit);
    }
    if( my $pos_title = $self->attr('employer_org_position_title') ){
        $xcand->appendChild($xdoc->createElement('CurrentJobTitle'))->appendText($pos_title);
    }
    if( my $employer = $self->attr('employer_org_name') ){
        $xcand->appendChild($xdoc->createElement('CurrentJobEmployer'))->appendText($employer);
    }
    if( my $start_date = $self->attr('employer_org_position_start_date') ){
        $xcand->appendChild($xdoc->createElement('CurrentJobStartDate'))->appendText($start_date);
    }
    if( my $end_date = $self->attr('employer_org_position_end_date') ){
        $xcand->appendChild($xdoc->createElement('CurrentJobEndDate'))->appendText($end_date);
    }
    ## Time to add the CV
    {
        my $cv_mimetype = $cv_hash->{cv_mimetype} // confess("Missing cv_mimetype in cv_hash");
        my $cv_b64      = $cv_hash->{cv_content} // confess("Missing cv_content in cv_hash");
        my $cv_filename = $cv_hash->{cv_filename} // confess("Missing cv_filename in cv_hash");

        ## This is a hack to get a filename as UTF8.
        ## Investigate why candidate 'Farzaneh Sabzi' on careerbuilde has got an incorrect filename
        ## later. You will have to dive in the Stream::Engine and Stream::Templates code.
        use utf8;
        utf8::upgrade($cv_filename);

        $log->trace("FILENAME '$cv_filename'");

        my $xcv = $xcand->appendChild($xdoc->createElement('Doc'));
        $xcv->setAttribute('FileName', $cv_filename);
        $xcv->setAttribute('DocumentType', 'Attachment');
        $xcv->setAttribute('EncodingType', 'Base64');
        $xcv->setAttribute('MimeType' , $cv_mimetype);
        $xcv->appendText($cv_b64);
    }
    ## And the CV Parsing Res XML bit
    if ( my $cv_parsing_res = $opts->{cv_parsing_res} ){
        # we need to append tag doc to the candidate XML
        my $tag_doc_node = $xcand->appendChild( $xdoc->createElement('Doc') );
        $tag_doc_node->setAttribute('EncodingType', 'Base64');
        $tag_doc_node->setAttribute('MimeType' , 'application/xml');
        $tag_doc_node->setAttribute('FileName' , $self->name() . '_tagged_cv.xml');
        $tag_doc_node->setAttribute('type' , 'tagged_cv');

        my $xml_string = '';
        if ( my $hr_xml = $cv_parsing_res->{hr_xml} ) {
            $xml_string = $self->hr_xml($hr_xml);
            $tag_doc_node->setAttribute("DocumentType", "HR");
        }
        else {
            $xml_string = $self->bg_xml();
            $tag_doc_node->setAttribute("DocumentType", "BGT");
        }
        my ( $xml_encoding ) = ( $xml_string =~ m/encoding=['"]([\w-]+)/ );
        unless( $xml_encoding ){
            $log->warn("Embedded XML has got no encoding. Falling back to UTF-8");
            $xml_encoding = 'UTF-8';
        }
        $log->info("Will encode embedded XML in '$xml_encoding'");
        # Base64 doesnt like string. It likes bytes. And to comply with the standard,
        # it should not be line broken (empty string eol param)
        $tag_doc_node->appendText( MIME::Base64::encode_base64( Encode::encode( $xml_encoding, $xml_string ) , '' ) );
    }

    ## Validate what we are going to send against our DTD for good measure.
    eval{ $xdoc->validate( $search_user->stream2->dtd('ripple/candidate.dtd') ); };
    if( my $err = $@ ){
        confess("Failed to validate XML: $err. XML IS: ".$xdoc->toString(1));
    }

    ## Declaring the encoding is not enough. This will set the true encoding to
    ## be UTF8 (even if we have pure latin1 strings in this document).
    $xdoc->setEncoding('UTF-8');

    return $xdoc;
}

=head2 build_target_entity

Shortcut to Build a L<MIME::Entity> targeted at this user.

Note you'll still have to fill this entity with other stuff, like the From
address, the content, things you want to attach, etc..

The From default to noreply@broadbean.net

Usage:

  my $entity = $this->build_target_entity();

  # A plain text message.
  $entity->attach( Type => 'text/plain; charset=UTF-8',
                   Encoding => 'quoted-printable',
                   Disposition => 'inline',
                   Data => [ $some_text ]);

  # Set the from:
  $entity->head->replace( From => 'john@doe.com' );

=cut

sub build_target_entity{
    my ($self) = @_;

    my $email_address = $self->email();
    unless( $email_address ){
        confess("This candidate doesnt have an email address");
    }

    # RFC 2822: the "display-name" outside angle brackets must be quoted
    # if it contains certain punctuation chars
    my $recipient_name = $self->name() // '';
       $recipient_name = qq["$recipient_name"] if $recipient_name =~ /[^\w ]/;
    my $recipient      = Encode::encode('MIME-Q', $recipient_name ) . " <$email_address>";

    return MIME::Entity->build(From => 'noreply@broadbean.net',
                               To => $recipient,
                               Type => 'multipart/mixed'
                              );
}

=head2 to_talent_search_properties($user)

Turns this into a hash of properties that is
suitable to be sent to the Talent Search Import API, in the context
of the given user (think tags for instance).

See https://trac.adcourier.com/wiki/TalentSearch/DataImport/FieldDetails for specs.
and https://bitbucket.org/broadbean/p5-bean-tsimport-client for the client's doc.

Usage:

=cut

sub to_talent_search_properties{
    my ($self, $user) = @_;

    unless( $user ){ confess("Missing user in options"); }

    # Make sure this candidate is tag enriched.
    $user->stream2->factory('Tag')->enrich_candidates([ $self ] , { user => $user } );

    # And calculate a tag string (yes this is horrible. See https://jira.artirix.com/browse/BRBE-46
    my $tags_string = join(',', map{ $_ =~ s/,/_/ ; $_  } map{ $_->{value} } @{ $self->{tags} // [] } );
    $log->info("Tags string for candidate ".$self->id()." is '".$tags_string."'");

    my $all_attributes = $self->to_ref();

    # Do the assumed free form attribute to the best of my knowledge
    # as of 14th March 2014. Jerome.
    # This is based on the explicitely defined attributes of this class.
    # We will probably have to add more in ze future.
    my $ts_props = {};

    $ts_props->{source} = $self->destination();
    $ts_props->{applicant_channel_id} = $self->destination();

    $ts_props->{original_id} = $self->id();

    $ts_props->{applicant_email} = $self->email();
    $ts_props->{applicant_name} = $self->name();
    $ts_props->{applicant_telephone} = $self->contact_telephone();
    $ts_props->{applicant_address} = $self->location_text();

    if( $tags_string ){
        $ts_props->{tags} = $tags_string;
    }

    return $ts_props;
}

# utility methods
sub to_date {
    my $self = shift;
    my $datetime = shift or return;

    my $dt;
    if ( $datetime =~ m/^\d+$/ ) {
        # we have been given an epoch
        $dt = DateTime->from_epoch( epoch => $datetime );
    }
    elsif ( $datetime eq 'current' ) {
        $dt = DateTime->now();
    }
    elsif ( $datetime =~ m/^(\d{4})(\D)(\d{1,2})\2(\d{1,2})$/ ) {
        # they have given us a date (YYYY-mm-dd)
        $dt = DateTime->new(
            year => $1,
            month => $3,
            day   => $4,
        );
    }
    else {
        die "Unsupported date(time) format: $datetime (we don't know how to parse it)";
    }

    # YYYY-MM-DDTHH:MM:SS e.g. "2011-08-18T17:09:00"
    return $dt->strftime('%Y-%m-%dT%H:%M:%S');
}

sub best_email {
    my @starting_emails = grep { $_ && valid_candidate_email($_) } @_;

    if ( @starting_emails == 1 ) {
        return $starting_emails[0];
    }

    # prefer the email address from BG over the careerbuilder email address
    my @no_careerbuilder_emails = grep { $_ !~ m/\@resume.cbdr.com/ } @starting_emails;

    if ( @no_careerbuilder_emails ) {
        return $no_careerbuilder_emails[0];
    }

    return $starting_emails[0];
}

sub valid_candidate_email {
    my $email = shift;

    if ( $email =~ m/\.aplitrak\.com$/ ) {
        # hacky stream address so ignore it
        return;
    }

    if ( $email !~ m/\@/ ) {
        # some careerbuilder email addresses are too long
        # and got chopped when they were inserted into aplitrak
        # skip them and prefer the CV
        return;
    }

    if ( $email =~ m/\Aunknown_email\@/ ){
        # stream2 uses this to indicate that we don't know the email address
        return;
    }

    return $email;
}

=head2 download_cv

Returns an HTTP::Response using the Feed's 'download_cv' method.

Usage:

    my $http_response = $candidate->download_cv(
        $stream_action,
        $user_object,
        $options
    );

$options should be:

    {
        cb_oneiam_auth_code => '<code_here>',
        browser_tag         => '<tag_here>',
    }

=cut

sub download_cv {
    my ( $self, $stream_action, $user_object, $options ) = @_;

    my $stream2 = $user_object->stream2;
    my $template = $self->destination;

    my %auth_tokens = %{$user_object->subscriptions_ref->{$template}->{auth_tokens} || {}};
    my $feed = $stream2->build_template(
        $template,
        undef,
        {
            stream_action => $stream_action,
            user_object => $user_object,
        }
    );

    $feed->company( $user_object->group_identity );
    while ( my ($token, $value) = each %auth_tokens ) {
        $feed->token_value( $token => $value );
    }

    if($options->{cb_oneiam_auth_code}) {
        $feed->token_value(  cb_user_code => $options->{cb_oneiam_auth_code} );
    }

    if( $options->{browser_tag} ){
        $feed->token_value(  browser_tag => $options->{browser_tag} );
    }

    my $quota_obj = $stream2->quota_object;
    my $board = $self->destination();
    my $type  = 'search-profile-or-cv';

    my $quota_summary = $quota_obj->fetch_quota_summary(
        {   path => $user_object->oversees_hierarchical_path(),
            board => $board,
            type => $type ,
        }
    );

    if( $user_object->has_done_action('download' , $self )
        || ( $quota_summary->{ $board }->{ $type }    # user has search-profile-or-cv quota
            && $user_object->has_done_action( 'profile', $self ) ) # and has already downloaded the cv.
    ){
        $log->info("User ".$user_object->id()." has already downloaded candidate ".$self->id().". NOT checking quotas");
        return $feed->download_cv( $self );
    }

    return $feed->quota_guard( [ 'profile-or-cv', 'cv' ] , sub{ $feed->download_cv( $self ); });
}

=head2 set_canonical_values

sets canonical values against the candidate

Usage:

    $candidate->set_canonical_values( %canonical_mappings );

=cut

sub set_canonical_values {
    my ($self,%canonical_mappings) = @_;

    die 'You need to provide a mapping of canonical values' unless %canonical_mappings;

    # these are the current fields that need to be within the canonical values
    my @canonical_names  = qw(
        name address city state country postal_code telephone mobile
        education education_level cv_text last_activity employer job_title
        job_description industry
    );
    for my $canonical_name ( @canonical_names ) {
        if ( $canonical_mappings{$canonical_name} ) {
            $self->attr( 'canonical_'.$canonical_name  =>
                $self->attr( $canonical_mappings{$canonical_name} ));
        }
        else {
            $self->set_attr( 'canonical_'.$canonical_name  => undef);
        }
    }
    return;
}

=head2 add_warning

Adds to this candidates 'warnings' array property instead of overwriting it.
Used to display error like messages for the candidate without throwing
an exception.

Usage:

$candidate->add_warning('This isnt something to die for');
$candidate->add_warning('Neither is this');

# $candidate->attr('warnings') = ['This isnt something to die for', 'Neither is this']

=cut

sub add_warning {
    my ($self, $warning) = @_;
    my $warnings = $self->get_attr('warnings') // [];
    push @$warnings, $warning;
    $self->set_attr('warnings', $warnings);
    return;
}

=head2 get_internal_db_ids

If this is an 'adcresponses' candidate, gets the internal DB
ids from the TSImport API if possible.

Returned values:

undef - There is no internal database for this candidate.

INTERNAL_DB_PLEASE_WAIT - There WILL be some internal ID mapping in the near future.

INTERNAL_DB_NO_MAPPING - No internal mapping. You can give up.

{ <backend> => <ID> } - This response exists in this backend as a candidate.

Note that backend names are not template names. Here's a quick mapping:

  Artirix => 'talentsearch'

=cut

sub get_internal_db_ids{
    my ($self) = @_;
    unless( $self->destination() eq 'adcresponses' ){
        confess("Calling this only makes sense for an 'adcresponses' candidate (response)'");
    }
    my $tsimport_id = $self->attr('tsimport_id');
    unless( $tsimport_id ){
        return undef;
    }

    my $status_ref = Stream2->instance()->tsimport_api()->import_status( $tsimport_id );

    unless( $status_ref->{status}->{completed} ){
        return 'INTERNAL_DBS_PLEASE_WAIT';
    }
    my $internal_id = $status_ref->{status}->{result};
    unless( $internal_id ){
        return 'INTERNAL_DBS_NO_MAPPING';
    }

    # Yes parameters is a JSON string in a JSON object. My heart bleeds.
    my $parameters = Stream2->instance()->json->decode( $status_ref->{status}->{parameters} );
    return { $parameters->{backend}->[0] => $internal_id };
}

=head2 get_internal_siblings

From a given internal DB mapping, build the sibling candidates.

Usage:

 my $internal_ids = $this->get_internal_db_ids();
 if( ref( $internal_ids ) eq 'HASH' ){
    my @siblings = $this->get_internal_siblings( $internal_ids );
 }

=cut

sub get_internal_siblings{
    my ($self , $internal_db_ids ) = @_;
    my @siblings = ();
    foreach my $backend ( keys %{$internal_db_ids} ){
        push @siblings, __PACKAGE__->new({
            candidate_id => $internal_db_ids->{$backend},
            destination => Stream2->instance()->factory('Board')->internal_backend_to_destination( $backend ),
        });
    }
    return @siblings;
}

1;

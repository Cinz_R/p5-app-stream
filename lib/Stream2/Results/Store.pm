package Stream2::Results::Store;

use Moose;
use JSON;
use Stream2::Trait::Stringable;
use Stream2::Types;

use Stream2::Results::Result;

use Log::Any qw/$log/;

=head1 NAME

Stream2::Results::Store - A "stored" L<Stream2::Results> factory as well as being a L<Stream2::Results>.

=head1 SYNOPSIS

A results ID is specific to a board. That's why in this context, no board name (or id) is mentionned.

  # Load a stored Result from a Results ID:

  my $results = Stream2::Results::Store->new({ id => $results_id , store => .. The redis instance results are stored in .. });

  # Find the L<Stream2::Results::Result> (candidate) at position 0:
  my $candidate = $results->find(0);

=cut

extends 'Stream2::Results';

with 'Stream2::Role::KeyValStore';
with 'Stream2::Role::Stringable';

has 'search_record_id' => ( is => 'rw', isa => 'Maybe[Int]' );

## Adding Stringable traits to the 'results' property make them serializable.
has '+results' => (
  traits     => ['Stream2::Trait::Stringable'],
  stringable => 0,
  handles    => {}, # remove the delegation, TODO: refactor Stream2::Results
);

has '+facets' => ( traits => [ 'Stream2::Trait::Stringable'],
                );

has '+notices' => ( traits => [ 'Stream2::Trait::Stringable' ] );

has 'expires_after' => (
  is => 'ro',
  isa => 'Stream2::Types::Seconds',
  builder => 'default_expires_after',
  coerce  => 1,
);

sub default_expires_after {
  return '3h';
}

=head2 find

Returns a L<Stream2::Results::Result> (which is a candidate, dont forget) from the given candidate index in
this Results set. Will die if no such a thing exist.

Usage:

  my $candidate = $this->find(0); # The first candidate of this result

=cut

sub find {
    my $self = shift;
    my $position = shift;
    unless( defined $position ){
        confess("Missing position");
    }

    my $results_key = $self->_results_key;

    # TODO: Replace with an exception (maybe Ouch?)
    my $result_json = $self->r_lindex($results_key, $position)
        or confess "Record [$results_key] [$position] not found";

    return $self->to_results($result_json);
}

=head2 update

Updates the result index in the given _results_key list with additional JSON attributes.
Returns a L<Stream2::Results::Result> object with the $fields attributed to it.

Usage:

    my $candidate = $self->update($position, { something => 'new value' });
    $candidate->attr('something') # will give you 'new value'

=cut

sub update {
    my ($self, $position, $fields) = @_;
    $position // confess('Missing position');
    $fields // confess('Missing fields to update result with');

    my $results_key = $self->_results_key;

    my $result = $self->r_lindex($results_key, $position)
        or confess "Record [$results_key] [$position] not found";

    $result = JSON::from_json($result);

    my %all_result_properties = (%$result, %$fields);
    my $new_result = JSON::to_json(\%all_result_properties, { ascii => 1, utf8 => 1 }); # needs to be in bytes

    my $res = $self->r_lset($results_key, $position, $new_result);

    $log->info("Updated index [$position] for record [$results_key]");

    return Stream2::Results::Result->from_json($new_result, $position);
}

sub default_results {
  return [ $_[0]->fetch_results() ];
}

sub fetch_results {
    my $self = shift;

    my $results_key = $self->_results_key;

    # TODO: test if the results key exists
    return $self->to_results(
        $self->r_lrange( $results_key, 0, $self->r_llen($results_key)-1 ),
    );
}

=head2 to_results

Turns a collection of JSON litterals into an array of
L<Stream2::Results::Result>.

=cut

sub to_results {
    my $self = shift;
    my @json = @_;

    my $idx = 0;
    my @results = map { Stream2::Results::Result->from_json($_,$idx++) } @json;

    # make it easier for people by returning in scalar
    # if there is only one result rather than list context
    return @results == 1 ? $results[0] : @results;
}

after 'add_candidate' => sub {
  my $self = shift;
  $self->r_expire($self->_results_key, $self->expires_after);
};

# replace the default add_candidate method
# with one that persists
sub add_candidate {
    my $self      = shift;
    my $candidate = shift;

    $candidate->fixup();

    my %data = %$candidate;
    $self->incr_new_results();
    $data{results_id}       = $self->id();
    $data{rank}             = $self->new_results();
    $data{search_record_id} = $self->search_record_id() ? $self->search_record_id() * 1 : undef;
    return $self->r_rpush( $self->_results_key, JSON::to_json( \%data ) );
}

=head2 save

Saves this to redis for later. Crashes if something goes wrong.

Usage:

 $this->save();

=cut

sub save{
    my ($self) = @_;
    unless( $self->id() ){
        confess("Cannot save something without an ID");
    }
    # British serialise
    my $key = __PACKAGE__.'-'.$self->id();
    $self->store->set($key , $self->serialise());
    $self->store->expire($key, $self->expires_after());
}

=head2 fetch

Fetches values from Redis. Returns false if nothing is in redis.

Expects $this->id() to be set.

Usage:

 unless( $this->fetch() ){
     print "Results ".$this->id()." is gone";
 }

=cut

sub fetch{
    my ($self) = @_;
    unless( $self->id() ){
        confess("Cannot fetch something without an ID");
    }

    my $json_string = $self->store()->get(__PACKAGE__.'-'.$self->id());
    unless( $json_string ){
        return undef;
    }

    my $props = $self->deserialise($json_string);

    # Set some of my properties from the deserialized properties
    $self->destination( $props->{destination} );
    $self->notices( $props->{notices} );
    $self->facets( $props->{facets} );
    $self->search_record_id( $props->{search_record_id} );
    $self->total_results( $props->{total_results} );

    return $self;
}

sub incr_new_results { return $_[0]->new_results( $_[0]->new_results() + 1); }

# A sensible key to put the results in Redis.
sub _results_key { return 'results:'.$_[0]->id() }

1;

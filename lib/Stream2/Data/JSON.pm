package Stream2::Data::JSON;

use strict;
use warnings;

use JSON;

sub deflate_to_json {
  my ($self, $ref) = @_;

  return JSON::to_json( $ref || {}, { canonical => 1, ascii => 1 });
}

sub inflate_from_json {
  my ($self, $json) = @_;

  return $json ? JSON::from_json( $json ) : {};
}

1;

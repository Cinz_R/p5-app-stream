package Stream2::Translations;

use Locale::Messages qw(:libintl_h);
use Log::Any qw($log);
use Exporter::Declare;
use Carp;
use Stream2::Translations::MockStash;
exports qw/t9n t9np/;

=head2 t9n

Pure identity function that just acts as a marker about what to translate in strings that are built at compile or first run time.

Usage:

  package MY::Package;
  use Stream2::Translations qw/t9n/;

  # Compile time
  # Mark the string to be translatable, for the code scanner.
  my $string = t9n('Will translate this');

  sub do_stuff{
       ...
       $s2->__( $string ); # As usual, except now the code scanner found the string to translate.
  };

=cut

sub t9n{
    shift;
}

=head2 t9np

Like t9np , but to use with 'particular' strings (aka context).

Usage:

  my $string = tp9n('my.context', 'Will translate this' );
  sub blabla{
     $s2->__p('my.context', $string);
  }

=cut

sub t9np{
    shift;
    shift;
}

sub new {
    my $class = shift;
    $class->_setup_overrides( $class );

    # The xs version uses the system locale which requires the chosen language to have been installed on the host
    # http://search.cpan.org/~guido/libintl-perl-1.22/lib/Locale/libintlFAQ.pod
    Locale::Messages->select_package('gettext_pp');

    my $translation_dir = get_shared_directory() . 'translations/compiled';
    my @lang_codes = map { substr $_, rindex($_, '/') + 1 } glob "$translation_dir/*";

    return bless { supported_languages => \@lang_codes }, $class;
}

sub supported_languages { $_[0]->{supported_languages}; }

sub get_shared_directory {
    my ( $class ) = @_;

    my $file_based_dir = File::Spec->rel2abs(__FILE__);
    $file_based_dir =~ s|lib/Stream2.+||;
    $file_based_dir .= 'share/';

    if( -d $file_based_dir ){
        my $real_sharedir = Cwd::realpath($file_based_dir)
            or die "no";

        $real_sharedir .= '/';
        return $real_sharedir;
    }

    my $dist_based_dir = Cwd::realpath(dist_dir('Stream2'));
    my $real_sharedir = Cwd::realpath($dist_based_dir)
        or die "no";

    $real_sharedir .= '/';
    return $real_sharedir;
}

sub set_language {
    my ($self, $lang) = @_;

    $log->trace("Setting LANGUAGE to ".( $lang // 'UNDEF' ) );

    Locale::Messages::nl_putenv('LANGUAGE='.$lang);
    Locale::Messages::nl_putenv('LANG='.$lang);
}

sub set_timezone { 
    my ( $self, $timezone ) = @_;
    $log->trace('Setting BB_TIMEZONE to '.( $timezone // 'UNDEF' ));

    $ENV{BB_TIMEZONE} = $timezone;
}

=head2 in_timezone

Executes and return the given code in the given timezone

Usage:

 my $stuff = $this->in_timezone('Europe/Paris', sub{ return "Stuff"; });

=cut

sub in_timezone{
    my ($self, $timezone , $code) = @_;
    $timezone // Carp::confess("Missing timezone");
    $code // Carp::confess("Missing code");

    my $old_timezone = $ENV{BB_TIMEZONE};

    $ENV{BB_TIMEZONE} = $timezone;

    my $ret = eval{ &$code(); };
    my $err = $@;

    $ENV{BB_TIMEZONE} = $old_timezone;

    unless( $err ){ return $ret; }

    Carp::confess( $err );
}


=head2 in_language

Executes and returns the result of the given code
in the given language.

Usage:

 my $stuff = $this->in_language('en' , sub{ return "Stuff"; } );

=cut

sub in_language{
    my ($self, $lang , $code) = @_;
    $lang // Carp::confess("Missing lang");
    $code // Carp::confess("Missing code");

    my $old_LANG = $ENV{LANG};
    $self->set_language($lang);

    my $ret = eval{ &$code(); };
    my $err = $@;

    $self->set_language($old_LANG);
    unless( $err ){ return $ret; }

    Carp::confess( $err );
}


=head2 scan_templates_for_pos

Scans the template toolkit templates for PO entries
and returns an arrayref of Locale::PO objects.

Usage:

  my $pos = $this->scan_tts_for_pos();

=cut

sub scan_tts_for_pos{
    my ($self, $options_ref) = @_;

    my $stream2 = delete( $options_ref->{stream2} )
        or die "stream2 is required";

    my $templates = $stream2->templates();

    my @paths = @{ $templates->paths() };

    ## Build a special purpose template toolkit that will
    ## compile stuff in a temp directory
    my $tmp_dir = File::Temp::tempdir( CLEANUP => 0 );
    my $tt      = Template->new({
        INCLUDE_PATH => join(':' , @paths),
        COMPILE_DIR  => $tmp_dir,
        ABSOLUTE     => 1,
        VARIABLES    => {
            ## WARNING: This L will always translate in English..
            L => sub{ Bean::Locale::localise_in($ENV{LANG}, @_ ); }
        },
    });

    $log->info("Will scan paths ".join(', ' , @paths));

    File::Find::find( sub {
        $_ =~ m/\.([^.]+)$/;
        my $extension = $1 || '';
        return unless $extension eq 'inc' || $extension eq 'tt';
        my $dir_entry = $File::Find::name;

        # skip directories and temporary files (ending with a tilde)
        return if -d $dir_entry || substr( $dir_entry, -1 ) eq '~';

        my $compiled_template = $tt->template( $dir_entry ) || do {
            my $error = $tt->error;
            $log->error(
                "Error compiling template $dir_entry. Error type '" .
                $error->type . "'. Error info: '" . $error->info . \'' 
            );
            return;
        };
    }, @paths );

    $log->info("All templates are compiled in '$tmp_dir'. Will scan that.");

    my @pos = ();
    File::Find::find(sub{
                         my $cpath = $File::Find::name;
        return unless $cpath =~ /email_result\.tt/;
                         # $log->info("Scanning compiled file '$cpath'");
                         # Open the file

                         my $haz_code = 0;

                         my $code =
                           q|
my ( $template ) = @_;
my $output = ''; my $stash = Stream2::Translations::MockStash->new({ template => $template, %$options_ref });
|;

                         open( my $template , '<' , $cpath ) or Carp::confess("Cannot open $cpath: $!");
                         while( my $line = <$template> ){
                             unless( $line =~ /i18n/ ){ next; }
                             $haz_code = 1;
                             # $log->info("$cpath: Found line '$line'");
                             ## Lines will be like 
                             # $output .=  $stash->get(['i18nx', [ ('' . "your Watchdog &quot;{name}&quot; has yielded <a href=\"{watchdog_link}\">{n_new_results} new candidates</a> for you."), { 'name' => $stash->get('watchdog_name'), 'watchdog_link' => $stash->get('watchdog_link'), 'n_new_results' => $stash->get('n_new_results')  } ]]);
                             $code .= $line;
                         }
                         close($template);

                         unless( $haz_code ){ return; }

                         $code .= "\n".'return $stash;'."\n";

                         my $sub = eval "sub{\n".$code."\n}\n";

                         $log->info("Will analyze code $code");

                         my $mock_stash = &$sub($cpath);

                         push @pos , @{ $mock_stash->locale_pos() };

                     }, $tmp_dir);

    my %seen;
    my @uniq_pos = grep { ! $seen{$_->{msgid}}++ } @pos;

    return \@uniq_pos;
}

1;

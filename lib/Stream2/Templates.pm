package Stream2::Templates;

use Moose;

use Log::Any qw/$log/;
use Bean::Locale;
use Template;
use Stream::Templates;
use HTML::Entities;
use HTML::Scrubber;

=head1 NAME

Stream2::Templates - Stream2 Templates management.

=cut

has 'stream2' => ( is => 'ro' , isa => 'Stream2' , weak_ref => 1 , required => 1 );
has 'dir' => ( is => 'ro' , isa => 'Str' , lazy_build => 1);

# Full list of template toolkit paths
has 'paths' => ( is => 'ro' , isa => 'ArrayRef[Str]', lazy_build => 1);

# The template toolkit renderer
has 'tt' => ( is => 'ro' , isa => 'Template', lazy_build => 1);

# A more simple renderer, to render template strings like '[iprofile_jobtype]/[iprofile_jobref]/[iprofile_jobtitle]/[contactname]/[eza_number]~72/[nice_board_name]/'
# So the delimiters are '[' and ']'
has 'simple_tt' => ( is => 'ro' , isa => 'Template' , lazy_build => 1);

# interface to stream templates package
has 'stream_templates' => ( is => 'ro', isa => 'Stream::Templates', lazy_build => 1 );

sub _build_simple_tt{
    my ($self) = @_;

    return Template->new({ START_TAG => quotemeta('['),
                           END_TAG => quotemeta(']')
                         });
}

sub _build_dir{
    my ($self) = @_;
    return $self->stream2()->shared_directory().'stream2_templates';
}

sub _build_paths{
    my ($self) = @_;
    return [ $self->dir() , $self->stream_templates()->share_dir() ];
}

sub _build_stream_templates {
    return Stream::Templates->new();
}

sub _build_tt{
    my ($self) = @_;

    my @paths = @{ $self->paths() };

    return Template->new({
        INCLUDE_PATH => join(':' , @paths),
        FILTERS => {
            'em_highlight' => sub {
                my $text = shift;
                # NON GREEDY substitution of <em></em> with
                # non html escaped <em></em>
                $text =~ s/&lt;em&gt;(.*?)&lt;\/em&gt;/<em>$1<\/em>/g;
                return $text;
            },

            # Puts the HTML into a beautiful iframe where the dirty
            # HTML and styling cannot harm the rest of the site.
            'html_sandbox' => sub {
                my $html = shift;
                my $scrubber = HTML::Scrubber->new();
                $scrubber->rules(
                    '*' => {
                        # No element attributes
                        '*'     => 0,
                    },
                    # Reject by default
                    '*'      => 0,
                    # Whitelist some elements
                    'p'      => 1,
                    'div'    => 1,
                    'ul'     => 1,
                    'ol'     => 1,
                    'li'     => 1,
                    'br'     => 1,
                    'b'      => 1,
                    'u'      => 1,
                    'i'      => 1,
                    'strong' => 1,
                    'em'     => 1,
                    'table'  => 1,
                    'tr'     => 1,
                    'th'     => 1,
                    'td'     => 1,
                );
                $html = $scrubber->scrub($html);
                return $html;

                #= Doesn't work with perfect-scrollbar =#

                #my $src = HTML::Entities::encode_entities($html);
                #my $onload = 'maintainIframeContentHeight(this)';
                #return '<div style="position: relative">
                #    <iframe
                #        frameborder="0"
                #        style="width: 100%"
                #        srcdoc="' . $src . '"
                #        onload="' . $onload . '"
                #    >
                #    </iframe>
                #</div>';
            }
        },
        VARIABLES => {
            ## WARNING: This L will always translate in English..
            L => sub{ Bean::Locale::localise_in($ENV{LANG} , @_ ); },
            # i18nx will translate in whatever language
            i18nx => sub{ return $self->i18nx(@_); },
            i18n  => sub{ return $self->stream2->translations->__(shift) },
            escape_html => sub{
                return HTML::Entities::encode_entities(shift // '' , '<>&"');
            }
        },
    });

}

=head2 validate_simple

Validates the given template_text with the given stash and returns the evaluation error
if there is an issue in the template. Returns false if no problem occurred.

Usage:

 if( my $error = $this->validate_simple('[from_email/ ] bla') ){
     
 }

=cut

sub validate_simple{
    my ($self, $template_text, $stash ) = @_;
    $stash //= {};
    unless( defined($template_text) ){ return 'Undefined template'; }
    my $output = '';
    unless( $self->simple_tt->process(\$template_text , $stash , \$output) ){
        return $self->simple_tt->error();
    }
    return undef;
}

=head2 render_simple

Renders the given simple template string with the given stash

Usage:

 my $result =  $this->render_simple('[from_email]/blabla/[team]' , { from_email => ... , team => ... });

=cut

sub render_simple{
    my ($self, $template_text, $stash) = @_;
    $stash //= {};
    unless( $template_text ){ return undef };

    my $output = '';

    unless( $self->simple_tt->process(\$template_text , $stash , \$output) ){
        $log->error("Error processing simple template '$template_text': ".$self->simple_tt->error());
        confess("Error processing simple template '$template_text': ".$self->simple_tt->error());
    }

    return $output;
}

=head2 render

Renders the given template name with the stashed variables.

=cut

sub render{
    my ($self, $template_name , $stash) = @_;

    $stash //= {};

    my $output = '';
    unless( $self->tt->process($template_name , $stash , \$output) ){
        $log->error("Error processing template $template_name: ".$self->tt->error());
        confess("Error processing template $template_name: ".$self->tt->error());
    }

    return $output;
}

=head2 result_body

    returns HTML of board specific result body
    Always renders the same file, uses the candidate.destination to find correct body

    usage:
    my $result_body = $stream2_templates->result_body({
        candidate => $candidate_ref
    });

=cut

sub result_body {
    my ( $self, $stash ) = @_;
    $stash // confess( 'stash is required' );
    return $self->render( "stream/results/body.html.tt", $stash );
}

=head2 profile_body

    returns HTML of board specific candidate profile
    Always renders the same file, uses the candidate.destination to find correct profile

    usage:
    my $result_body = $stream2_templates->profile_body({
        candidate => $candidate_ref
    });

=cut

sub profile_body {
    my ( $self, $stash ) = @_;
    $stash // confess( 'stash is required' );
    return $self->render( "stream/candidate_profiles/default.inc", $stash );
}

=head2 candidate_email

    returns the result body to be used in an email
    usage:
    my $candidate_email_body = $stream2_templates->candidate_email({
        candidate => $candidate_ref,
        board_nice_name => $board_nice_name
    });

=cut

sub candidate_email {
    my ( $self, $stash ) = @_;
    $stash // confess( 'stash is required' );
    return $self->render( "stream/candidate_email.html.tt", $stash );
}

=head2 build_template

Builds a template using the given name (like 'talentsearch', 'careerbuilder', etc..),
the given tokens (as a hash) and the given more general attributes.

Note that it automatically injects location_api, user_api and other stuff
in the attributes of the template.

Usage:

 my $template = $this->build_template('talentsearch', undef, { company => 'andy'} );

=cut

sub build_template{
    my ($self, $name, $tokens, $attributes) = @_;
    $tokens //= {};
    $attributes //= {};
    return $self->stream_templates()->build_template(
        $name,
        $tokens,
        {
            location_api => $self->stream2->location_api(),
            user_api => $self->stream2->user_api(),
            documents_api => $self->stream2->documents_api(),
            html_parser => $self->stream2->html_parser(),
            %$attributes
        }
    );
}


=head2 i18nx

Slighlty different version of __x that is compatible with the template toolkit style..

Usage (from template toolkit):

  [% s2.i18nx("Hello {name}" , name = watchdog.user.contact_name ) | html %]

Or:

  [% s2.i18nx("Hello {name} {other}" , {  name = watchdog.user.contact_name , other = blabla  } ) | html %]

=cut

sub i18nx{
    my ($self, $string , $stash) = @_;
    $stash //= {};
    my $translated = $self->stream2->translations->__x($string, %$stash );
    return $translated;
}


__PACKAGE__->meta->make_immutable();


package Stream2;

use Moose;

with qw/DBIx::Class::Wrapper/;

use Log::Any qw/$log/;

use B::Deparse;

use Bean::AdcApi::Client;
use Bean::AdvertService::Client;
use Bean::CVP::Client;
use Bean::CareerBuilder::CandidateSearch;
use Bean::CareerBuilder::ParseNormalize;
use Bean::EmailService::Client;
use Bean::GeoLocation::APIClient;
use Bean::Juice::APIClient::Board;
use Bean::Juice::APIClient::Candidate;
use Bean::Juice::APIClient::Company;
use Bean::Juice::APIClient::Configurations;
use Bean::Juice::APIClient::Currencies;
use Bean::Juice::APIClient::Documents;
use Bean::Juice::APIClient::EmailTemplates;
use Bean::Juice::APIClient::Location;
use Bean::Juice::APIClient::Stream;
use Bean::Juice::APIClient::User;
use Bean::OAuth::CareerBuilder;
use Bean::Quota;
use Bean::PubSub::Client;
use Bean::EmailService::Client;
use Bean::MailjetAPI::Client;

use Bean::TSImport::Client;
use Bean::TextExtractor::Client;

use Class::Load;

use DBI;
use DBI::ProfileDumper;

use Data::Dumper;
use Data::UUID;
use DateTime;
use Digest::MD5;

use Encode;

use File::Spec;
use File::Slurp;
use File::Basename;

use Geo::Coder::Google;

use Hash::MD5;

use IO::Interactive;

use JSON;

use LWP::UserAgent;

use MIME::Base64::URLSafe;
use MIME::Parser;
use MIME::Types;

use Module::Pluggable::Object;

use Qurious;

use Redis;

use Scalar::Util qw();

use Schedule::LongSteps;
use Schedule::LongSteps::Storage::DBIxClass;

use Search::Elasticsearch;
use Search::Elasticsearch::Async;

use Stream2::Developer;

# Monitor for interlocking.
use Stream2::Monitor;
# Periodic stuff.
use Stream2::Periodic;

# Template toolkit templates managers:
use Stream2::Templates;

use Stream2::Lisp::Evaler;

use Bean::OAuth::AsyncClient;

use Stream2::PerfMonitor;

use Stream2::Results::Store;
use Stream2::Translations::App;
use Stream2::Translations::Templates;

# Template or 'feed' or 'board' managers:
use Stream::TemplateLoader;
use Stream::EngineException;
use Stream2::CandidateActionLog;
use Stream2::Scrubber;

use String::CamelCase;

use XML::LibXML;
use Hash::Merge;
use CHI;

has 'config' => ( is => 'ro' , isa => 'HashRef', required => 1, lazy_build => 1);
has 'config_file' => ( is => 'ro' , isa => 'Str');

has 'developer' => ( is => 'ro', isa => 'Stream2::Developer', lazy_build => 1);

# Subsystems connections

# DBIC Schema

# This is an 'alias' for stream_scheam (for the DBIX::Class::Wrapper role)
has '+dbic_schema' => ( lazy_build => 1 );
# The DBICWrapper base class for the DBIx::Class::Wrapper role.
sub _build_dbic_factory_baseclass{
    my ($self) = @_;
    return 'Stream2::DBICWrapper';
}

# The real stream schema
has 'stream_schema' => ( is => 'ro' , isa => 'Stream2::Schema' , required => 1, lazy_build => 1);

has 'monitor' => ( is => 'ro', isa => 'Stream2::Monitor' , lazy_build => 1);
has 'periodic' => ( is => 'ro', isa => 'Stream2::Periodic' , lazy_build => 1);

has 'longsteps' => ( is => 'ro', isa => 'Schedule::LongSteps', lazy_build => 1);

# Broadbean APIs
has 'adcourier_api' => ( is => 'ro' , isa => 'Bean::AdcApi::Client', lazy_build => 1);
has 'location_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::Location' , lazy_build => 1);
has 'email_template_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::EmailTemplates' , lazy_build => 1);
has 'user_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::User', lazy_build => 1);
has 'candidate_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::Candidate', lazy_build => 1);
has 'company_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::Company', lazy_build => 1);
has 'configurations_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::Configurations', lazy_build => 1);
has 'stream_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::Stream' , lazy_build => 1);
has 'currency_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::Currencies' , lazy_build => 1);
has 'geolocation_api' => ( is => 'ro' , isa => 'Bean::GeoLocation::APIClient' , lazy_build => 1);
has 'board_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::Board' , lazy_build => 1);
has 'bbcss_api' => ( is => 'ro' , isa => 'Bean::CareerBuilder::CandidateSearch' , lazy_build => 1 );
has 'text_extractor' => ( is => 'ro', isa => 'Bean::TextExtractor::Client', lazy_build => 1);
has 'email_api' => ( is => 'ro', isa => 'Bean::EmailService::Client', lazy_build => 1 );
has 'mailjet_api' => ( is => 'ro', isa => 'Bean::MailjetAPI::Client', lazy_build => 1 );
has 'advert_api' => ( is => 'ro', isa => 'Bean::AdvertService::Client', lazy_build => 1 );
has 'parse_normalize_api' => ( is => 'ro', isa => 'Bean::CareerBuilder::ParseNormalize', lazy_build => 1 );

# Quota management library
has 'quota_object' => ( is => 'ro', isa => 'Bean::Quota', lazy_build => 1 );
# Talent search import API
has 'tsimport_api' => ( is => 'ro' , isa => 'Bean::TSImport::Client' , lazy_build => 1);
# Document HTMLer API
has 'documents_api' => ( is => 'ro' , isa => 'Bean::Juice::APIClient::Documents', lazy_build => 1);
# CV Parser API
has 'cvp_api' => ( is => 'ro' , isa => 'Bean::CVP::Client', lazy_build => 1);

# General services.
has 'redis' => ( is => 'ro' , isa => 'Redis' , required => 1 , lazy_build => 1);
has 'qurious' => ( is => 'ro', isa => 'Qurious', required => 1, lazy_build => 1);
has 'pubsub_client' => ( is => 'ro', isa => 'Bean::PubSub::Client', required => 1, lazy_build => 1);
has 'action_log' => ( is => 'ro', isa => 'Stream2::CandidateActionLog', lazy_build => 1); # The Set of actions logs factory
has 'watchdogs' => ( is => 'ro' , isa => 'Stream2::DBICWrapper::Watchdog' , lazy_build => 1); # Watchdogs factory

has 'perfmonitor' => ( is => 'ro' , isa => 'Stream2::PerfMonitor', lazy_build => 1 );

# A hash of Bean::OAuth::AsyncClient per auth plugin name
has 'oauth_clients' => ( is => 'ro' , isa => 'HashRef[Bean::OAuth::AsyncClient]', default => sub{ {}; } );

# Elastic search to our AWS elastic search instance
has 'aws_elastic_search' => ( is => 'ro', lazy_build => 1);
has 'aws_elastic_search_async' => ( is => 'ro', lazy_build => 1);

# General utilities.
has 'uuid' => ( is => 'ro', isa => 'Data::UUID' , required => 1 , lazy_build => 1);
has 'xml_parser' => ( is => 'ro' , isa => 'XML::LibXML' , lazy_build => 1);
has 'html_parser' => ( is => 'ro' , isa => 'XML::LibXML' , lazy_build => 1);

# A very simple MIME Types utility (Not magic)
has 'mime_types' => ( is => 'ro', isa => 'MIME::Types', lazy_build => 1 );

has 'dtds' => ( is => 'ro', isa => 'HashRef[XML::LibXML::Dtd]', default => sub{ {}; });
has 'perl_deparser' => ( is => 'ro' , isa => 'B::Deparse' , lazy_build => 1 );

# Some nice and safe json encoder/decoder
has 'json' => ( is => 'ro' , isa => 'JSON' , lazy_build => 1);
# A general purpose data scrubber.
has 'scrubber' => ( is => 'ro', isa => 'Stream2::Scrubber', lazy_build => 1);

# A general purpose LWP::UserAgent
has 'user_agent' => ( is => 'ro' , isa => 'LWP::UserAgent' , lazy_build => 1);

# Cache some values globally
has 'stream2_cache' => ( is => 'ro', isa => 'CHI::Driver' , lazy_build => 1);

# Cache some values against users
# Its at model level cause we dont want to build this over and over again.
has 'users_cache' => ( is => 'ro' , isa => 'CHI::Driver' , lazy_build => 1);

has 'startup_time' => ( is => 'ro', isa => 'DateTime', default => sub { DateTime->now(); } );

# The directory holding all sorts of resource files
has 'VERSION'           => ( is => 'ro' , isa => 'Str', lazy_build => 1 );
has 'VERSION_FRONTEND' => ( is => 'ro' , isa => 'Str', lazy_build => 1 );
has 'installation_id' => ( is => 'ro', isa => 'Str', lazy_build => 1);

has 'shared_directory' => ( is => 'ro' , isa => 'Str' , lazy_build => 1 );

# Template managers
has 'templates' => ( is => 'ro' , isa => 'Stream2::Templates' , lazy_build => 1 );

has 'translations' => ( is => 'ro', isa => 'Stream2::Translations', lazy_build => 1 , handles => [ qw( __ __x __n __nx __xn __p __px __np __npx) ] );
has 'template_translations' => ( is => 'ro', isa => 'Stream2::Translations', lazy_build => 1 );

# A flexible context (a bit like a stash) for the context related functions.
has 'context' => ( is => 'rw' , isa => 'HashRef', default => sub{ {} } );

has 'app_pure_factories' => ( is => 'ro' , isa => 'HashRef[Bool]', lazy_build => 1);

has 'interactive' => ( is => 'ro' , isa => 'Bool', lazy_build => 1);

has 'lisp' => ( is => 'ro', isa => 'Stream2::Lisp::Evaler', lazy_build => 1 );

# A web client to retrieve location data - currently retrieved using the Google Maps API
has 'geocoder' => ( is => 'ro', lazy_build => 1 );

{   # Global access stuff.
    my $instance;
    sub BUILD{
        my ($self) = @_;
        $instance = $self;
        $instance->_setup_enviroment();
    }

    sub instance{
        return $instance;
    }
}

BEGIN{
    # The test compatible File::Share
    eval{ require File::Share; File::Share->import('dist_dir'); };
    if( $@ ){
        # The production only File::ShareDir
        require File::ShareDir;
        File::ShareDir->import('dist_dir');
    }
};

sub _build_installation_id{
    my ($self) = @_;
    return $self->config()->{installation_id};
}

sub _build_developer{
    my ($self) = @_;
    return Stream2::Developer->new({ stream2 => $self });
}

sub _build_geocoder {
    my ($self) = @_;
    return Geo::Coder::Google->new(ua => $self->user_agent);
}

sub _build_lisp {
    my ($self) = @_;
    return Stream2::Lisp::Evaler->new( { stream2 => $self } );
}

sub _build_interactive{
    my ($self) = @_;
    return IO::Interactive::is_interactive();
}

sub _build_text_extractor{
    my ($self) = @_;
    return Bean::TextExtractor::Client->new();
}

sub _build_app_pure_factories{
    my ($self) = @_;
    my $baseclass = 'Stream2::Factory';
    my $res = {};
    my $mp = Module::Pluggable::Object->new( search_path => [ $baseclass ]);
    foreach my $candidate ( $mp->plugins() ){
        $log->trace("Loading class $candidate");
        Class::Load::load_class( $candidate );
        unless( $candidate->isa($baseclass) ){
            confess("Class $candidate is not a $baseclass. Please fix that");
        }
        $res->{$candidate} = 1;
    }
    return $res;
}

sub _build_translations { return Stream2::Translations::App->new(); }
sub _build_template_translations { return Stream2::Translations::Templates->new(); }

sub _build_periodic{
    my ($self) = @_;
    return Stream2::Periodic->new({ stream2 => $self });
}

sub _build_longsteps{
    my ($self) = @_;
    my $storage = Schedule::LongSteps::Storage::DBIxClass->new({
        schema => $self->stream_schema(),
        resultset_name => 'LongStepProcess',
        limit_per_tick => 10,
    });
    return Schedule::LongSteps->new(
        {
            storage => $storage,
            on_error => sub{
                my ($stored_process, $original_eror) = @_;
                my $recipients = $self->config()->{tech_email_addresses} // [ 'jerome@broadbean.net' ];

                my $subject = "Error in process ".$stored_process->process_class();
                my $body = 'An error occured with the following process instance:'."\n";
                $body .= Dumper({ $stored_process->get_columns() });

                # Unpack the error
                $body .= "Unwrapped Stream Error".$original_eror->message."\n"
                    if Scalar::Util::blessed( $original_eror )
                        && $original_eror->isa('Stream::EngineException');

                my $email = $self->factory('Email')->build([
                    From => 'noreply@broadbean.net',
                    To   => $recipients,
                    Subject => Encode::encode( 'MIME-Q', $subject ),
                    Data    => $body
                ]);

                $self->send_email($email);
            }
        });
}


sub _build_monitor{
    my ($self) = @_;
    return Stream2::Monitor->new({ stream2 => $self });
}

sub _build_json{
    my $json = JSON->new();
    $json = $json->ascii(1); # Ascii, pretty and canonical :)
    $json = $json->pretty(1);
    $json = $json->canonical(1);
    return $json;
}

sub _build_scrubber{
    return Stream2::Scrubber->new();
}

sub _build_templates{
    my ($self) = @_;
    return Stream2::Templates->new( { stream2 => $self });
}

sub _build_VERSION{
    my ($self) = @_;
    return $self->_slurp_version('/VERSION-FILE');
}

sub _build_VERSION_FRONTEND{
    my ($self) = @_;
    return $self->_slurp_version('/VERSION-FILE-FRONTEND');
}

sub _build_advert_api {
    my ( $self ) = @_;
    return Bean::AdvertService::Client->new( $self->config->{advert_service_api} );
}

sub _build_parse_normalize_api{
    my ( $self ) = @_;

    my $cb_oauth = Bean::OAuth::CareerBuilder->new( $self->config()->{parse_normalize_api}->{auth} );

    return Bean::CareerBuilder::ParseNormalize->new({
        auth_token => $cb_oauth->get_auto_token( $self->stream2_cache ),
        base_url   => $self->config->{parse_normalize_api}->{base_url},
    });
}

sub _slurp_version{
    my ($self, $file_suffix) = @_;
    my $version_file = $self->shared_directory().$file_suffix;
    my $file_version = File::Slurp::read_file( $version_file , err_mode => 'quiet') ;
    if( $file_version ){
        chomp($file_version);
        return $file_version;
    }
    return $self->startup_time()->iso8601();
}

sub _build_shared_directory{
    my ($self) = @_;
    my $file_based_dir = File::Spec->rel2abs(__FILE__);
    $file_based_dir =~ s|lib/Stream2.+||;
    $file_based_dir .= 'share/';
    if( -d $file_based_dir ){

        my $real_sharedir = Cwd::realpath($file_based_dir);
        unless( $real_sharedir ){
            confess("Could not build Cwd::realpath from '$file_based_dir'");
        }
        $real_sharedir .= '/';

        $log->info("Will use file based shared directory '$real_sharedir'");
        return $real_sharedir;
    }

    my $dist_based_dir = Cwd::realpath(dist_dir('Stream2'));

    my $real_sharedir = Cwd::realpath($dist_based_dir);
    unless( $real_sharedir ){
        confess("Could not build Cwd::realpath from '$dist_based_dir'");
    }

    $real_sharedir .= '/';

    $log->info("Will use File::Share based directory ".$real_sharedir);
    return $real_sharedir;
}

sub _build_user_agent{
    my ($self) = @_;
    my $ua = LWP::UserAgent->new();
    $ua->timeout(45); # Let us be generous.
    $ua->agent(__PACKAGE__.' http://www.broadbean.com/');
    return $ua;
}

sub _build_stream2_cache{
    my ($self) = @_;
    return $self->_build_chidb_cache('stream2');
}

sub _build_users_cache{
    my ($self) = @_;
    return $self->_build_chidb_cache('cv_users');
}

# Build a DB Based chi cache for the given namespace
sub _build_chidb_cache{
    my ($self , $namespace) = @_;

    my $chi_error = sub{
        my ($message, $key , $original ) = @_;
        confess("ERROR IN CHI (namespace ='$namespace' key='$key'): $message , $original");
    };


    my $create_table = 0;
    if( $self->stream_schema->storage()->sqlt_type() eq 'SQLite' ){
        # Autovivify the table only in SQLite.
        # for the test suite to run.
        $create_table = 1;
    }

    return CHI->new( driver => 'DBI',
                     namespace => $namespace, # Just a namespace.
                     dbh => sub{ $self->stream_schema->storage->dbh() }, # Long lived object. The storage can get disconnected. We use the sub.
                     # chi uses keys of length 300. Witha a utf8 table this is too large! We have defined the default charset
                     # for this table in the migration script as latin1 which solves this for now
                     # On startup chi tries to create the table if it doesn't exist, it doesn't specify a latin1 charset so it goes bang
                     create_table => $create_table,
                     on_get_error => $chi_error,
                     on_set_error => $chi_error
                   );
}

sub _build_xml_parser{
    my ($self) = @_;
    return XML::LibXML->new( recover => 0 , pedantic_parser => 1);
}

sub _build_html_parser{
    my ($self) = @_;
    # We are gonna be parsing HTML, lets be lax
    # about strictness. recover -> 2
    return XML::LibXML->new( recover => 2 );
}

sub _build_perl_deparser{
    return B::Deparse->new('-sC');
}

sub _build_mime_types{
    my ($self) = @_;
    return MIME::Types->new();
}

sub _build_tsimport_api{
    my ($self) = @_;
    my $conf = $self->config()->{tsimport_api};
    return Bean::TSImport::Client->new({
                                        service_uri => scalar($conf->{service_uri}) // confess("No service_uri in tsimport_api conf"),
                                        api_key => scalar($conf->{api_key}) // confess("No api_key in tsimport_api conf"),
                                        timeout => 20
                                       });
}

sub _build_uuid{
    my ($self) = @_;
    return new Data::UUID();
}

sub _setup_enviroment {
    my ($self) = @_;
    my $conf = $self->config;
    my $env_vars = $conf->{environment_variables};
    map {
        $ENV{$_} = $env_vars->{$_}
    } keys %$env_vars;
}


sub _build_aws_elastic_search{
    my ($self) = @_;
    my $conf = $self->config()->{aws_elastic_search};
    unless( $conf ){
        confess("No aws_elastic_search config in ".$self->config_place());
    }
    my $es =  Search::Elasticsearch->new( %$conf );
    return $es;
}

=head2 deploy_aws_elastic_search

Deploys the right type in our internal error calendar elastic search (index = threads).

You should have to use this once and only once for the lifetime of this product.

Returns true on success. Dies on error.

Usage:

 $this->deploy_aws_elastic_search();

=cut

sub deploy_aws_elastic_search{
    my ($self) = @_;

    my $es = $self->aws_elastic_search();
    # We need to do some autovivifying of the type stream2search
    # We pay this price the first time we use that. Then
    # the es client persists along with this Stream2 instance.
    $log->info("Vivifying type mapping 'stream2search' in 'threads'");
    my $response =
        $es->indices->put_mapping(
            index => 'threads',
            type => 'stream2search',
            body => {
                stream2search => {
                    properties => {
                        search_record_id    => { type => 'long',    store => 1, index => 'not_analyzed'}, # integer max: 2^31 ( 2.1bn ) long max: 2^63
                        company             => { type => 'string',  store => 1 , index => 'not_analyzed'}, # Company string identifier
                        destination         => { type => 'string',  store => 1 , index => 'not_analyzed'}, # Board string identifier
                        duration            => { type => 'integer', store => 1 , index => 'not_analyzed'}, # Duration in seconds
                        n_results           => { type => 'integer', store => 1 , index => 'not_analyzed'}, # Number of results
                        error_type          => { type => 'string',  store => 1 , index => 'not_analyzed'}, # Error type String identifier
                        watchdog            => { type => 'integer', store => 1, index => 'not_analyzed'}, # Was it a watchdog
                        time_completed      => { type => 'date',    store => 1, index => 'not_analyzed' }, # ISO8601 UTC of completion
                        criteria_id         => { type => 'string',  store => 1 , index => 'not_analyzed' }, # string criteria identifier
                        s3_log_id           => { type => 'string',  store => 1 , index => 'not_analyzed' }, # s2 log file ID in the s3 bucket of this installation
                        stream2_base_url    => { type => 'string',  store => 1 , index => 'not_analyzed' }, # Installation base URL. Required for linking back
                        user_string         => { type => 'string',  store => 1 }, # User descriptive string. DO NOT USE THAT. THIS IS VERY NAUGHTY.
                        user_provider       => { type => 'string',  store => 1, index => 'not_analyzed' }, # Provider ID string
                        user_provider_id    => { type => 'string',  store => 1, index => 'not_analyzed' } , # ID of user inside provider
                        user_stream2_id     => { type => 'string',  store => 1, index => 'not_analyzed' } ,  # The user_id in stream2. For reference
                        remote_ip           => { type => 'string',  store => 1, index => 'not_analyzed' } ,  # ip address of user for investigation purposes
                    }
                }
            }
        );
    unless( $response->{acknowledged} ){
        confess("Cannot vivify type 'stream2search' in index 'threads': ".Dumper($response));
    }
    return 1;
}


=head2 elastic_search_async 

    NONE BLOCKING, should work nicely with MOJO

    $self->render_later;
    $async_es->search()->then( sub {
        my $results = shift;
        return $self->render( json => $results );
    }

=cut

sub _build_aws_elastic_search_async{
    my ($self) = @_;
    my $conf = $self->config()->{aws_elastic_search};
    unless( $conf ){
        confess("No aws_elastic_search config in ".$self->config_place());
    }

    return Search::Elasticsearch::Async->new( %$conf );
}

sub _build_qurious{
    my ($self) = @_;
    return Qurious->new( redis => $self->redis() );
}

sub _build_pubsub_client{
    my ($self) = @_;
    my $conf = $self->config()->{bean_pubsub_client};
    unless( $conf ){
        confess("No bean_pubsub_client config in ".$self->config_place());
    }
    return Bean::PubSub::Client->new(config => $conf );
}

=head2 oauth_client

Returns a L<Bean::OAuth::AsyncClient> for the given Auth Plugin name.

Or die when this is not possible to build such a client from this auth plugin name.


Usage:

 my $client = $this->oauth_client('adcourier');

 my $client = $this->oauth_client('adcourier_ats');

=cut

sub oauth_client{
    my ($self, $auth_plugin_name) = @_;

    if( my $client = $self->oauth_clients()->{$auth_plugin_name} ){ return $client; }

    my $config = $self->config()->{plugins}->{auth}->{providers}->{$auth_plugin_name};
    unless( $config ){ confess("Cannot find auth plugin confit for $auth_plugin_name"); }

    my $private_key_file = $config->{private_key_file} // confess("missing private_key_file property to build OAuth Client");
    unless( $private_key_file =~ /^\// ){
        # Relative URL. Relative to shared directory.
        $private_key_file = $self->shared_directory().'/'.$private_key_file;
    }


    my $oauth_scope = $config->{oauth_scope} // [qw/read_contact_details read_stream_subscriptions read_navigation_tabs read_currencies/];

    my $client = Bean::OAuth::AsyncClient->new({
                                            private_key_file    => $private_key_file,
                                            client_id           => $config->{client_id} // confess("Missing client_id to build OAuth Client"),
                                            oauth_endpoint      => $config->{oauthserver} // confess("Missing oauthserver to build OAuth Client"),
                                            scope               => $oauth_scope
                                           });
    return $self->oauth_clients->{$auth_plugin_name} = $client;
}

sub _build_perfmonitor{
    my ($self) = @_;
    return Stream2::PerfMonitor->new({ stream2 => $self });
}

sub _build_watchdogs{
    my ($self) = @_;
    return $self->factory('Watchdog');
}

sub _build_action_log {
    my ($self) = @_;
    return Stream2::CandidateActionLog->new({
        schema => $self->stream_schema
    });
}


sub _build_redis{
    my ($self) = @_;
    my $conf = $self->config();
    return Redis->new(
                      encoding => 'UTF-8',
                      server   => $self->config->{redis},
                      no_auto_connect_on_new => 1,
                      reconnect => 5,
                      every => 1000,
                      cnx_timeout => 10,
                      read_timeout => 10,
                      write_timeout => 10
                     );
}

sub _build_adcourier_api{
    my ($self) = @_;

    my $api_url = $self->config()->{adcourier_api}->{api_url} // confess("No api_url in adcourier_api config");
    my $api_key = $self->config()->{adcourier_api}->{api_key} // confess("No api_key in adcourier_api config");

    return Bean::AdcApi::Client->new({ ua => $self->user_agent(),
                                       api_key => $api_key,
                                       api_url => $api_url
                                     });
}

sub _build_cvp_api{
    my ($self) = @_;
    my $conf = $self->config()->{cvp_api};
    unless( $conf ){
        confess("Missing cvp_api in configuration ".$self->config_place());
    }

    return Bean::CVP::Client->new( %$conf );
}

sub _build_documents_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::Documents');
}

sub _build_user_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::User');
}

sub _build_candidate_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::Candidate');
}

sub _build_company_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::Company');
}

sub _build_configurations_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::Configurations');
}

sub _build_stream_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::Stream');
}

sub _build_location_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::Location');
}

sub _build_email_template_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::EmailTemplates');
}

sub _build_bbcss_api {
    my ( $self ) = @_;
    return $self->build_bbcss_api();
}

=head2 build_bbcss_api

Builts a fresh BBCSS API client with the given properties if needed.

Usage:

 my $fresh_bbcss_client = $this->build_bbcss_api();

 my $fresh_bbcss_client = $this->build_bbcss_api({ base_url => ... });

=cut

sub build_bbcss_api{
    my ($self, $properties) = @_;
    my $bbcss_client = Bean::CareerBuilder::CandidateSearch->new({
        auth_role => 'Bean::CareerBuilder::CandidateSearch::Role::BBCSSAuth',
        base_url  => $self->config->{bbcss_api}->{api_url},
        bbcss_key => $self->config->{bbcss_api}->{api_key},
        %{ $properties // {} },
    });
    unless ( ( $ENV{MOJO_MODE} // '' ) eq 'production' ){
        $bbcss_client->ua->protocols_allowed( [ 'https' , 'http' ] );
        $bbcss_client->ua->ssl_opts( verify_hostname => 0 );
    }
    return $bbcss_client;
}

sub _build_quota_object {
    my ($self) = @_;
    return Bean::Quota->new(
        config => {},
        uuid => $self->uuid,
        dbh_builder => sub {
            # use the same database connection as our own schema
            return $self->stream_schema->storage()->dbh();
        }
    );
}

sub _build_email_api {
    my ($self) = @_;
    my $config = $self->config->{email}->{service};
    return Bean::EmailService::Client->new(
        hostname  => $config->{host},
        port      => $config->{port},
        auth_code => $config->{auth_code},
    );
}

sub _build_mailjet_api {
    my ($self) = @_;

    # extract Mailjet accounts from "email" config section
    my $config   = $self->config->{email};
    my %accounts = map {
        $_ => {
            username => $config->{$_}->{params}->{sasl_username},
            password => $config->{$_}->{params}->{sasl_password}
        }
    } grep { $_ ne 'service' } keys %{$config};

    return Bean::MailjetAPI::Client->new(
        accounts => \%accounts
    );
}

sub _build_board_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::Board');
}

sub _build_currency_api{
    my ($self) = @_;
    return $self->_build_api_client('Bean::Juice::APIClient::Currencies');
}

sub _build_geolocation_api{
    my ($self) = @_;
    my $conf = $self->config->{geolocation_api} or confess "geolocation_api config missing";
    return Bean::GeoLocation::APIClient->new( $conf );
}

# Note: this is not a lazy builder, this is a utility.
sub _build_api_client{
    my ($self, $client_class) = @_;
    my $api_key =  $ENV{'BROADBEAN_JUICE_API_KEY'};
    my $api_url =  $ENV{'BROADBEAN_JUICE_API_URL'};

    unless( $api_key && $api_url ){
        confess("Missing juiceapi_key or juiceapi_url in the config");
    }

    $log->debug("Building juice API client $client_class on url: ".$api_url." key: ".$api_key);
    return $client_class->new({ua => $self->user_agent(),
                               api_key => $api_key,
                               api_url => $api_url
                              });
}


sub _build_dbic_schema{
    my ($self) = @_;
    return $self->stream_schema();
}

sub _build_stream_schema{
    my ($self) = @_;
    my $schema =  $self->_do_schema_connect($self->_schema_config('stream'));

    if( $self->config->{dbi_profile_dumper} ){
        ## This will get dumped to the file when 
        $schema->storage->dbh->{Profile} = DBI::ProfileDumper->new(
                                                                   Path => [ '!Statement' ],
                                                                   File => "/tmp/dbiprofile-$$.prof"
                                                                  );
    }

    return $schema;
}

sub _schema_config {
  my ($self, $namespace) = @_;
  my $conf = $self->config;
  my %schema_conf;
  foreach my $field ( qw/schema dsn username password debug/ ) {
      $schema_conf{$field} = $conf->{$namespace.'_'.$field};
      $log->debug("Schema namespace $namespace $field: ".( $conf->{$namespace.'_'.$field} // 'UNDEF' ));
  }
  return %schema_conf;
}

# Builds the DBIC Schema from the config
# In all logic, this should be a Stream2::Schema
sub _do_schema_connect {
  my ($self, %config) = @_;
  my $schema_class = $config{schema} or die "Missing schema";

  # # Now the schema lives in a submodule. Push that
  # # in the INC
  # my $submodule_lib = $self->shared_directory().'/p5-app-stream-schema/lib/';

  # $log->info("Pushing submodule lib to INC: '$submodule_lib'");

  # push @INC , $submodule_lib;

  eval "require $schema_class;";
  if ( my $e = $@ ) {
      die "Unable to load $schema_class: $e";
  }

  my $schema = $schema_class->connect(
    $config{dsn},
    $config{username},
    $config{password},
  );

  $schema->stream2($self);

  $schema->storage->debug(1) if $config{debug};
  return $schema;
}



sub _build_config {
    my ($self) = @_;
    my $config_file = $self->config_file();

    my $base_dir = File::Basename::dirname( $config_file );

    unless( $config_file && -r $config_file ){
        confess("Cannot read config file $config_file");
    }

    my $conf =  eval File::Slurp::read_file($config_file, { binmode => ':utf8' });
    if( my $err = $@ ){
        confess("Error eval'ing $config_file content");
    }

    # recurse over any included configs so that we may better
    # manage our ever-growing settings
    if ( my $files = $conf->{include_config} ){
        my $merge = Hash::Merge->new( 'LEFT_PRECEDENT' );
        my $default_ref = {};
        foreach my $file ( @$files ){

	    $file = File::Spec->catfile( $base_dir  , $file );

            my $sub_conf = eval File::Slurp::read_file( $file, { binmode => ':utf8' });
            if ( my $err = $@ ) { confess("Error eval'ing sub config $file"); }
            $default_ref = $merge->merge($sub_conf, $default_ref);
        }
        $conf = $merge->merge($conf, $default_ref);
    }

    return $conf;
}

=head2 build_provider

Builds an authentication provider from the given provider name (required).

Usage:

 my $authentication_provider = $this->build_provider( 'adcourier' );

=cut

sub build_provider{
    my ($self, $provider_name ) = @_;
    unless( $provider_name ){
        confess("Missing provider_name");
    }

    my $build_args = $self->config()->{plugins}->{auth}->{providers}->{$provider_name} || {};

    my $class_name = 'App::Stream2::Plugin::Auth::Provider::'.String::CamelCase::camelize($provider_name);

    Class::Load::load_class($class_name);
    return $class_name->new($build_args);
}

=head2 factory

Gets a factory for anything.

This is a shortcut to DBIx::Class::Wrapper's dbic_factory for now.

Usage:

 my $f = $this->factory('SavedSearch');

 my $f = $this->factory('CvUser');

 ...

=cut

sub factory{
    my ($self, $name, $opts ) = @_;

    my $factory_class = 'Stream2::Factory::'.$name;

    if( $self->app_pure_factories()->{$factory_class} ){
        return $factory_class->new({ bm =>  $self, %{ $opts // {} } });
    }

    # Make sure we load the schema as this will load
    # appropriate classes in memory
    $self->stream_schema();

    return $self->dbic_factory($name, $opts);
}

sub increment_analytics {
    my ( $self, %parameters ) = @_;

    $log->info('increment_analytics is deprecated (for now).');
    return 1;
    # $parameters{$_} or confess "$_ is required" for qw/ board user_id/;

    # $self->qurious->create_job(
    #     class => "Stream2::Action::Report",
    #     queue => "BackgroundJob",
    #     parameters => \%parameters,
    # )->enqueue;
}

=head2 feed_proxy

Returns the feed proxy URL for the given feed name, or the _general one, or an empty string if proxy is not defined.

Usage:

 my $feed_proxy = $this->feed_proxy('monster');

=cut

sub feed_proxy{
    my ($self, $feed_name) = @_;
    my $proxy = $self->config()->{feed_proxies}->{$feed_name // '_WILL_NEVER_MATCH_'} ||
      $self->config()->{feed_proxies}->{_general} || '';
	unless ( $proxy ) {
        $log->warn("Missing feed_proxies -> _general (or '$feed_name') in ".$self->config_place());
	}
    return $proxy;
}

=head2 dtd

Returns a L<XML::LibXML::Dtd> singleton according to the given shared file (relative to this shared_directory).

Usage:

 my $dtd = $stream2->dtd('ripple/ripplev3.dtd');

 # Validate an xml doc against that:
 $xdoc->validate($dtd);

=cut

sub dtd{
    my ($self, $relative_file) = @_;
    unless( $relative_file ){ confess("Missing relative DTD filename"); }
    my $dtd = $self->dtds()->{$relative_file};

    if( $dtd ){ return $dtd; }
    return $self->dtds()->{$relative_file} =
      XML::LibXML::Dtd->parse_string(scalar(File::Slurp::read_file($self->shared_directory().$relative_file, { binmode => ':utf8' })));
}


=head2 config_place

Returns a human friendly indication as to where the config lives.

Usage:

 print "Config is in ".$this->config_place();

=cut

sub config_place{
    my ($self) = @_;
    if( $self->config_file() ){
        return "Config file: ".$self->config_file();
    }
    return "Memory Config Hash";
}

=head2 with_feed_template

carry out actions using a pre-built stream2 template with subscriptions etc

Usage:

    my $candidate = get_candidate();
    my $response = $s2->with_feed_template( $user, $board_name, sub {
        my ( $template ) = @_;
        return $template->download_cv( $candidate );
    } );

=cut

sub with_feed_template {
    my ( $self, $user, $board_name, $code ) = @_;

    my $subscription_hash =  $user->has_subscription($board_name) // confess("User ".$user->id()." does not have any subscription for ".$board_name);

    my $feed_template = $self->build_template( $board_name , undef , { user_object => $user, company => $user->group_identity() });

    # Set authentication tokens against the template. We need them for some actions.
    my $auth_tokens = $subscription_hash->{auth_tokens} // {};
    while( my ( $key , $value ) = each %$auth_tokens ){
        $feed_template->token_value( $key => $value );
    }

    return &$code($feed_template);
}

=head2 build_template

Builds a template using the given name (like 'talentsearch', 'careerbuilder', etc..),
the given tokens (as a hash) and the given more general attributes.

Note that it automatically injects location_api, user_api and other stuff
in the attributes of the template.

Usage:

 my $template = $this->build_template('talentsearch', undef, { company => 'andy'} );

=cut

sub build_template{
    my $self = shift;
    return $self->templates->build_template( @_ );
}

=head2 log_search_calendar

Put a search record in the error calendar.

Usage (minimal successful):

 $this->log_search_error({ company => 'companyid', destination => 'board_string_id' , n_results => 1234 });

Returns nothing as this does the slow stuff in an async way (in a double fork if you're interested in the gory details).

=cut

sub log_search_calendar{
    my ($self, $opts) =@_;
    $opts // confess("Missing options");

    ## Enforce a few things.
    unless( $opts->{company} ){
        confess("Missing company in options");
    }
    unless( $opts->{stream2_base_url} ){
        confess("Missing stream2_base_url in options");
    }
    unless( $opts->{criteria_id} ){
        confess("Missing criteria_id");
    }
    unless( $opts->{s3_log_id} ){
        confess("Missing s3_log_id");
    }

    $self->qurious->create_job(
        class => "Stream2::Action::ErrorCalendar",
        queue => "BackgroundJob",
        parameters => {
            watchdog => 0,
            time_completed => DateTime->now()->iso8601(),
            %$opts,
        }
    )->enqueue;
}

=head2 find_results

Finds the L<Stream2::Results::Store> with the given ID.

Note that this never fails.

Usage:

 my $results = $this->find_results($result_id);

=cut

sub find_results{
    my ($self, $id) = @_;
    unless( $id ){ confess("Missing id"); }
    return Stream2::Results::Store->new( id => $id, store => $self->redis() );
}

=head2 find_user

Shortcut for user finding. Returns a L<Stream2::SearchUser>

Usage:

 $s2->find_user(1); # Find the first user.

=cut

sub find_user{
    my ($self, $user_id , $ripple_settings) = @_;
    # Note that this respects's find DBIx::Class signature:
    return $self->factory('SearchUser')->find($user_id , { ripple_settings => $ripple_settings } );
}


=head2 find_criteria

Shortcut to fetch a criteria object from the DB. Returns undef if
no such criteria exists.

Usage:

 my $criteria = $this->find_criteria('jiowjifo809285025');

=cut

sub find_criteria{
    my ($self, $criteria_id) = @_;
    my $criteria = Stream2::Criteria->new({ id => $criteria_id,
                                            schema => $self->stream_schema() });
    eval{
        $criteria->load();  # This dies if no such criteria is found.
    };
    if( $@ ){
        $log->warn("Could not load criteria for ID=$criteria_id : $@");
        return undef;
    }
    return $criteria;
}

=head2 send_email

Send any email compatible with Email::Abstract using
the configured transport.

This will return a true value (the sent email) on success and die on failure.

Usage:

  $this->send_email

=cut

sub send_email{
    my ($self, $email) = @_;
    $self->factory('Email')->send($email);
}

=head2 with_context

Executes the given sub in the given context.

Usage:

  $self->with_context({ blabla => 1 } , sub{
                                           # Access the current stream2 instance in any way you like
                                           # from any depth you like in the call stack
                                           my $blabla = $stream2->context()->{blabla};
                                           ....
                                       });

=cut

sub with_context{
    my ($self, $context, $sub) = @_;
    $context //= {};
    $sub //= sub{};

    my $old_context = $self->context();

    # Doing the stuff
    $self->context($context);
    my $ret = eval{ &$sub(); };
    my $err = $@;
    $self->context($old_context);

    if( $err ){
        confess("Failed to execute code in context :".$err);
    }
    return $ret;
}

=head2 report_tech

Executes the given code and arguments and reports to tech team on failure (by default)

Note that the code CANNOT be a closuse and will be called with a Stream2 object as its first argument.

Also, the arguments HAVE to be jsonable, so no objects there. Ids are fine.

This will return whatever scalar the code returns and undef in case of failure.

As the sub passed to report_tech is eval'ed you, if the sub contains an eval it is your
responsibility to handle that correctly.

Usage:

  $this->report_tech([ $arg1 , $arg2 ] , sub{
     my ($s2 , $arg1 , $arg2 ) = @_;

     $s2->... do stuff.
     return $a_scalar;
   } );

=cut

sub report_tech{
    my ($self, $arguments , $code ) = @_;

    $arguments // confess("Missing arguments");
    $code // confess("Missing code");

    ## Build the report string now. This will die if the args and code
    ## are not compliant (see pod)
    my ($package, $filename, $line) = caller;

    local $Data::Dumper::Maxdepth = 1;
    my $code_string = $self->perl_deparser->coderef2text($code);
    my $arguments_json = $self->json->encode([
        map { UNIVERSAL::can($_,'can') ? { ref($_) => Dumper( $_ ) } : $_ } @$arguments
    ]);

    my $return;
    eval{
        $return = &$code($self, @$arguments);
    };
    my $err = $@ ;
    unless( $err ){ return $return; }

    $log->error($err);

    # An error was captured. Send a report.
    my $flat_json = $arguments_json;
    $flat_json =~ s/\n//g;
    $flat_json =~ s/\s+/ /g;

    ## Build an email
    my $subject = "[ERROR in $package] for args = ".$flat_json;
    my $body = "An error was found executing:\n";
    $body .= $arguments_json."\n\n";
    $body .= $code_string."\n\n";

    $body .= "Error: ".$err."\n\n";
    $body .= "Package : $package, file: $filename, line: $line";
    $body .= "\n\nPlease fix me :)";

    my $recipients = $self->config()->{tech_email_addresses} // [ 'jerome@broadbean.net' ];
    # Got the subject, got the body. Send the email.
    my $email = $self->factory('Email')->build(
        [
            From => 'noreply@broadbean.net',
            To => $recipients,
            Subject => Encode::encode( 'MIME-Q', $subject ),
            Data    => $body
        ]
    );

    $self->send_email($email);
    return;
}

=head2 sign_hash

Signs the given hash weakly in a weak fashion, just to avoid tampering. Don't count on that
to be ultra secure.

Usage:

 my $signature = $self->sign_hash({ a => 1 , b => 'foo' , bar => 'baz' });

=cut

sub sign_hash{
    my ($self, $hash) = @_;
    $hash //= {};
    return substr( Digest::MD5::md5_hex(
        Hash::MD5::sum($hash).$self->config()->{secret_key} // confess("Missing secret_key")
      ) ,0 ,6 );
}

=head2 clone

Returns a fresh new instance of this based on the same configuration.

Usage:

 my $new_s2 = $this->clone();

=cut

sub clone{
    my ($self) = @_;
    return ref($self)->new({ config => $self->config() });

}

=head2 doge

Console art Shibe

=cut

sub doge{
    my ($self) = @_;
    return File::Slurp::read_file($self->shared_directory().'/doge.txt' , { binmode => ':raw' });
}

=head2 stream2_dbh_do

This is a shortcut to the dbh_do of the Stream2::Schema (DBIx Storage dbh_do).

Google DBIx Storage dbh_do for more details.

Usage:

 my $result = $this->stream2_dbh_do(sub{ my ($storage, $dbh, $arg1 ) = @_ ; ... ; return 'something'; } , $arg1, ... );

=cut

sub stream2_dbh_do{
    my ($self , @args ) = @_;
    return $self->stream_schema->storage->dbh_do(@args);
}

=head2 gen_aplitrak_url / gen_aplitrak_email

Given an advert and a board id, return a job preview url for use in messaging

e.g. consultant.aplitrakid.board_id@company.aplitrak.com = http://www.aplitrak.com/?adid=j9fn34iufh934ijhr934ir9ji3jr9i34jr934r

Usage:

 my $aplitrak_url = $this->gen_aplitrak_url( $consultant, $aplitrakid, $board_id, $company );

=cut

sub gen_aplitrak_email {
    my $self = shift;
    return sprintf(
        "%s\.%s\.%s\@%s\.aplitrak\.com", @_
    );
}
sub gen_aplitrak_url_from_parts {
    my $self = shift;
    return $self->gen_aplitrak_url(
        $self->gen_aplitrak_email( @_ )
    );
}
sub gen_aplitrak_url {
    my $self = shift;
    my $aplitrak_email = shift;
    my $adid = MIME::Base64::URLSafe::urlsafe_b64encode( $aplitrak_email );
    return "http://www.aplitrak.com/?adid=" . $adid;
}

=head2 whoami

Returns the last portion of the passed in package name.

Usage:

  # Where action is 'Stream2::Action::DelegateCandidate'
  my $action = $this->whoami(__PACKAGE__);
  # $action -> 'DelegateCandidate'

=cut

sub whoami {
    my ($self, $package) = @_;
    return $package unless ( $package =~ m/::/ );
    my ($last_part) = $package =~ m/::(\w+)$/;
    return $last_part;
}

__PACKAGE__->meta->make_immutable();

__END__

=head1 NAME

Stream2 - The Stream2 core application

=head2 instance

Returns the current instance of Stream2 in use in your process.

Usage:

 my $s2 =  Stream2->instance();
 $s2->stream_schema()->resultset(...)->...;
 ...

=head2 stream_schema

Returns the L<Stream2::Schema> instance.

=head2 location_api

The L<Bean::Juice::APIClient::Location> client.

=head2 redis

The L<Redis> client

=head2 qurious

The L<Qurious> client

=cut

#!./bin/s2-shell

=pod

This is a script to munge data from the cv_limits table, which is the place where old quotas live in Search V1,
and store them in the new quota_library_quota table used by Bean::Quota.

To run this script you'll need to create a cv_limits table in the Search2 database and dump the data from
AdCouriers cv_limits table there before executing.

=cut

use Log::Any qw/$log/;
use Data::Dumper;
use DBIx::Class::Schema::Loader;

my $dbic_config = $s2->config;

my $reset_day_map = {
    monday => 1,
    tuesday => 2,
    wednesday => 3,
    thursday => 4,
    friday => 5,
    saturday => 6,
    sunday => 7
};

DBIx::Class::Schema::Loader::make_schema_at(
    'OldQuota::Schema',
    { naming => 'current', constraint => '\Acv_limits\z' },
    [ $dbic_config->{stream_dsn}, $dbic_config->{stream_username}, $dbic_config->{stream_password} ],
);

my $schema = OldQuota::Schema->connect($dbic_config->{stream_dsn}, $dbic_config->{stream_username}, $dbic_config->{stream_password});

my $old_quota_rs = $schema->resultset('CvLimit')->search({});
while ( my $result = $old_quota_rs->next ) {
    my @user_parts = grep { defined($_) } ($result->company, $result->office, $result->team, $result->consultant);
    my $path = '/' . join('/', @user_parts);
    my $board = $result->board;
    my $type = 'search-' . $result->type;

    my $has_new_quota = eval { $s2->quota_object->fetch_quota({path => $path, board => $board, type => $type}); };
    if (defined($has_new_quota)) {
        $log->warn("'$type' quota already exists for user '$path' on $board, will not set it as it will be overwritten.");
        next;
    }

    my %quota = (
        path        => $path,
        remaining   => $result->remaining
    );

    if ($result->reset_to) {
        $quota{reset_to} = $result->reset_to + 0;
    }

    my $reset_day = $result->reset_day;
    $reset_day = undef if $reset_day eq 'NULL';
    if ($reset_day) {
        if ($reset_day eq '-1') { # the last day of the month
            $quota{reset_dom} = 31;
        }
        elsif (my $reset_dow = $reset_day_map->{$reset_day}) {
            $quota{reset_dow} = $reset_dow;
        }
        elsif ($reset_day eq 'daily') {
            $quota{reset_interval} = 1;
        }
        else {
            $log->infof('The number is not a day of the week, setting reset to be every %d days', $reset_day);
            $quota{reset_interval} = $reset_day; # it should reset every X days
        }
    }

    if ($result->rollover_quota) {
        $quota{rollover} = 1;
    }
    else {
        $quota{rollover} = 0;
    }

    $log->info("About to insert the following quota for $board - $type: " . Dumper(\%quota));

    $s2->quota_object->create(
        {
            board   => $board,
            type    => $type,
            quota   => [\%quota]
        },
        {
            actioned_by => 'quota-import',
            remote_addr => '127.0.0.1'
        }
    );
}

#! perl6

# Ho yes!
#
# You will need: Perl6 , compiling Inline::Perl5 under the right version of perl you want
# to use with S2 code.
#
# The providing you are NOT using local::lib, you can go like:
#
# $ PERL5LIB=./lib perl6  bin/s2-perl6.pl6
#

use v6;
use Inline::Perl5;
use Stream2:from<Perl5>;

my $s2 = Stream2.new( config_file => 'app-stream2.jerome.conf' );

say $s2.VERSION;

say $s2.user_api.get_user(1);

#!/usr/bin/env perl

use strict;

use FindBin;
use lib "$FindBin::RealBin/../lib";

use Stream2::Queue::Worker::Restarter;

my $worker = Stream2::Queue::Worker::Restarter->new(
  debug => 1,
  log_to_stderr => 1,
);
$worker->watch($FindBin::RealBin, "$FindBin::RealBin/../lib");
$worker->work();

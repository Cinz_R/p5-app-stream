#!/usr/bin/env perl
use strict;
use warnings;

# ADC import script, copy this over to your favourite server and run
# perl -Mlocal::lib=/data0/www/adcourier.broadbean/local ADC-demigrate-NMR-part2.pl --company=COMPANY --data_file  JSON_TXT_FILE_GENERATED HERE.
# !! NOTE:
# you have to opt into running the update by using real_run=1.
# this was done like this on purpose as this is a very sensitive thing to do.

use lib '/data0/www/adcourier.broadbean/site';
use Bean::BeanConfig;
use Bean::DB;

use Getopt::Long;
my $company;
my $data_file;
my $run_for_real = 0;
GetOptions(
    "company=s"   => \$company,
    "data_file=s" => \$data_file,
    "real_run=i"  => \$run_for_real,
 )
  or die("Error in command line arguments\n");


unless ($company && $data_file) {
    die "$0 --company <company_name> --data_file <file with json data per line>"
}

# selecting the row
my $select_sql = "SELECT * FROM ${company}_responses WHERE response_id=?";
my $select_sth = Bean::DB->prepare($select_sql);

# updating is_read
my $update_is_read_sql =
  "UPDATE ${company}_responses SET is_read=? WHERE response_id=?";
my $update_is_read_sth = Bean::DB->prepare($update_is_read_sql);

# updating responded_to
my $update_responded_to_sql =
  "UPDATE ${company}_responses SET responded_to=? WHERE response_id=?";
my $update_responded_to_sth = Bean::DB->prepare($update_responded_to_sql);

# updating rank
my $update_rank_sql =
  "UPDATE ${company}_responses SET rank=? WHERE response_id=?";
  my $update_rank_sth = Bean::DB->prepare($update_rank_sql);


# read the file

open my $fh, '<', $data_file || "cannot open $data_file";

while ( my $line = <$fh> ) {
    print "DOING $line";
    chomp $line;

    # skip any commented out or blank lines and tell the user
    if ( $line =~ /^#?\s*$/ ) {
        print "Skipping '$line'\n";
    }

    # deserealise the bbcss data
    my $bbcss_data = JSON::XS::decode_json($line);

    my $aplitrakid        = $bbcss_data->{"adcresponse_id"};
    my $bbcss_rank        = $bbcss_data->{"rank"};
    my $is_read           = $bbcss_data->{"is_read"};
    my $been_responded_to = $bbcss_data->{"responded_to"};

    # get the current state of the candidate in ADC.
    $select_sth->execute($aplitrakid);
    my $select_ref = $select_sth->fetchrow_hashref;
    die "No candidate response record with ID: $aplitrakid"
      unless $select_sth->rows;

    # is_read is 1 if the aplication is 'read'
    if ( $is_read) {
        eval {
            # check to see if the candidate is already read, as there is no
            # point updating it other wise.
            if ( $select_ref->{is_read} ) {
                print "IS_READ: Candidate is already 'read'\n";
            }
            else {
                print '======= IS_READ: Updating IS_READ on ' . $aplitrakid . "\n";
                if (1 == $run_for_real) {
                    $update_is_read_sth->execute( 1, $aplitrakid );
                    die "IS_READ: No candidate response record" unless $update_is_read_sth->rows;
                }
            }
        };
        if ($@) {
            die "IS_READ: Died while updating is_read at $aplitrakid: $@";
        }
    }
    else {
        print "IS_READ: Skipping is_read as the candidate is not read.\n";
    }

    # responded_to holds the rank of when a candidate was sent a message
    # bbcss can be wrong so do not transfer unrank
    if ( $been_responded_to && $bbcss_rank != 7 ) {
        eval {
            # check to see if the candidate is has already been responded to,
            # if so ignore it, 0 means there is
            if ( $select_ref->{responded_to} != $bbcss_rank ) {
                print '======= RESPONDED_TO: Updating responded_to on ' . $aplitrakid . "\n";
                if (1 == $run_for_real) {
                    $update_responded_to_sth->execute( $bbcss_rank, $aplitrakid );
                    die "responded_to: No candidate response record" unless $update_is_read_sth->rows;
                }
            }
        };
        if ($@) {
            die "RESPONDED_TO: Died while updating responded_to $aplitrakid: $@";
        }
    }
    else {
        print "RESPONDED_TO: Candidate data is the same.\n";
    }

    # rank holds the rank of the candidate, bbcss can be wrong so do not transfer unrank
    if ( $bbcss_rank != $select_ref->{rank} && $bbcss_rank != 7 ) {
        eval {

            # there is a rank discrepancy
            print '======= RANK: Updating rank on ' . $aplitrakid . ' FROM ' . $select_ref->{rank} . ' to ' . $bbcss_rank . "\n";
            if (1 == $run_for_real) {
                $update_rank_sth->execute( $bbcss_rank, $aplitrakid );
                die "RANK: No candidate response record" unless $update_rank_sth->rows;
            }
        };
        if ($@) {
            die "RANK: Died while updating rank $aplitrakid: $@";
        }
    } else {
        print "RANK: is the same or BBCSS rank is (7), ADC_RANK " . $select_ref->{rank}  .   " BBCSS_RANK $bbcss_rank\n"
    }


    print "$aplitrakid was updated\n";
}

close $fh;

print "ALL good\n"
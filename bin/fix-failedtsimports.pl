#! bin/s2-shell

use Log::Any qw/$log/;
use Data::Dumper;

use Stream2::Results::Result;
use MIME::Base64;
use Encode;
use MIME::Entity;

my @ERRORS = ();

my %ID_IS_ENOUGH = (
                    monster_xml => 1,
                    cvlibrary => 1
                   );

my $actions = $s2->factory('CandidateActionLog')->search({ action => 'import',
                                                           'me.insert_datetime' => { -between => [ '2015-01-30 12:00' ,  '2015-01-30 14:00' ] },
                                                         } , { prefetch => 'cv_users' });

my $count = $actions->count();
my $done = 0;
my $error_count = 0;
$log->info("Will do again $count import actions");

while( my $action = $actions->next() ){
    eval{
        $log->info("Considering ".Dumper($action->to_raw_hashref() ) );

        my $template = $action->destination();
        my $user = $action->user();

        local $ENV{REMOTE_ADDR} = $user->last_ip() || '127.0.0.1';

        my $pure_candidate_id = $action->pure_candidate_id();

        $log->info("Will download candidate '$pure_candidate_id' from '$template'");

        my $candidate_object =  Stream2::Results::Result->new({ candidate_id => $pure_candidate_id,
                                                                destination => $template });

        my $template_object = $s2->build_template($template, undef,
                                                  { user_object => $user });
        $user->inject_authtokens($template_object);

        # Should we reconstruct the candidate by downloading a profile?
        unless( $ID_IS_ENOUGH{$template} ){
            $template_object->download_profile($candidate_object);
        }

        my $response = $template_object->download_cv($candidate_object);
        if( my $candidate_email = $response->header('X-Candidate-Email') ){
            $log->info("CV download also returned a candidate email = '$candidate_email' . Setting it against the candidate");
            $candidate_object->email($candidate_email);
        }

        unless( $candidate_object->email() ) {
            $candidate_object->make_email_up( $user );
        }

        ## All is dandy and ready for import in talent search.
        ## Hurray!
        {
            my $content = $response->content; # That's the CV binary.
            my $content_filename = $response->filename;
            my $content_type = $response->content_type || 'text/plain';
            my $content_b64 = MIME::Base64::encode_base64( $content );
            my $board_nice_name = $user->has_subscription($template)->{nice_name} // 'undefined board name';

            $user->provider_object->user_import_candidate($user,
                                                          { cv_mimetype => $content_type,
                                                            cv_content => $content_b64,
                                                            cv_filename => $content_filename
                                                          },
                                                          $candidate_object,
                                                          { board_nice_name => $board_nice_name }
                                                         );
            $log->info("Imported again successfully in talentsearch");
            $done++;
            $log->info("Done $done/$count . Error = $error_count");
        }
    };
    if( my $err = $@ ){
        $error_count++;
        if( ref $err ){
            $err = $err."";
        }
        push @ERRORS , { action => $action->to_raw_hashref(),
                         error => $err };
    }
}


if( @ERRORS ){

    $log->warn("Sending an error email");

    my $body = "ERROR REIMPORTING THOSE ACTIONS: \n\n";
    $body .= $s2->json->encode(\@ERRORS);

    my $email = MIME::Entity->build(From => 'noreply@broadbean.net',
                                    To => [ 'jerome@broadbean.com' ],
                                    Subject => Encode::encode('MIME-Q', "Failures in redoing CV imports"),
                                    Data => $body
                                   );
    $s2->send_email($email);
}

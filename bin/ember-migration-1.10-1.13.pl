#!/usr/bin/env perl
use strict; use warnings;

use Path::Class;
use File::Find ();

File::Find::find( { wanted => \&process, follow => 1 },  './app/templates' );
File::Find::find( { wanted => \&process_script, follow => 1 },  './app/scripts/stream2/' );

sub process {
    my $path = $File::Find::fullname;
    return unless $path =~ m/\.hbs\z/;
    my $file = Path::Class::file( $path );
    my $content = $file->slurp();

    $content = _migrate_each( $content );
    $content = _migrate_bindattr( $content );
    $content = _migrate_view( $content );

    $file->spew( $content );
}

sub process_script {
    my $path = $File::Find::fullname;
    return unless $path =~ m/\.js\z/;
    my $file = Path::Class::file( $path );
    my $content = $file->slurp();

    $content =~ s/Stream2\.([^\s]+)View/Stream2.$1Component/g;
    $content =~ s/Ember\.View\.extend/Ember.Component.extend/g;

    $file->spew( $content );
}

sub _migrate_each {
    my $content = shift;
    # {{#each this in that}} -> {{#each that as |this|}}
    $content =~ s/{{#each\s{1,}([^\s]+)\s{1,}in\s{1,}([^}]+)/{{#each $2 as |$1|/g;
    return $content;
}

sub _migrate_bindattr {
    my $content = shift;

    # bind-attr is no more, just do class="{{if stuff 'yes' 'no'}}"
    my @bind_matches = $content =~ m/{{\s*bind-?attr[^}]+}}/gi;
    my %match_map = ();
    foreach my $match ( @bind_matches ) {
        my ( $attr, $value ) = $match =~ m/\s([^\s=]+)=(?:"|')?([^"'}]+)/;
        my @vals = split(/\s/,$value);

        my @parsed_values = ();
        foreach my $val ( @vals ) {
            if ( $val !~ /\w/ ){
                next;
            }
            elsif ( $val =~ m/\A\:/ ){ # she is a static value
                push @parsed_values, substr( $val, 1 );
            }
            else { # she is a dynamic value
                push @parsed_values, '{{' . _evaluate_dynamic_value( $val ) . '}}';
            }
        }

        $match_map{$match} = sprintf('%s="%s"',$attr,join(' ',@parsed_values));
    }

    while ( my ( $from, $to ) = each ( %match_map ) ){
        $content =~ s/\Q$from\E/$to/g;
    }

    return $content;
}

sub _evaluate_dynamic_value {
    my $val = shift;

    if ( $val =~ m/\:/ ){
        my ( $c, $t, $f ) = split(/\:/,$val);
        $t//="";
        $f//="";
        return sprintf( "if %s '%s' '%s'", $c, $t, $f );
    }

    return $val;
}

sub _migrate_view {
    my $content = shift;
    
    # changes "view select" to new select component
    my @select_matches = $content =~ m/{{view (?:"|')select(?:"|')[^}]+}}/gsi;
    my %match_map = ();
    foreach my $match ( @select_matches ) {
        my ( $options ) = $match =~ m/{{view (?:"|')select(?:"|')\s+([^}]+)}}/gsi;
        $options =~ s/\n/ /g;
        $options =~ s/\s+/ /g;

        my %option_map = map { split('=',$_) } split(/\s+/,$options);
        my $value = delete( $option_map{value} );

        my @attr = ();
        while ( my ( $opt, $val ) = each( %option_map ) ){
            $val =~ s/"content\./"/g;
            push( @attr, sprintf('%s=%s', $opt, $val) );
        }

        $match_map{$match} = sprintf(
            '{{select-list %s action=(action (mut %s))}}',
            join( ' ', @attr),
            $value
        );
    }
    while ( my ( $from, $to ) = each ( %match_map ) ){
        $content =~ s/\Q$from\E/$to/g;
    }

    # very specific changes
    $content =~ s/action="facetSearch"/focus-out="facetSearch"/g;
    $content =~ s/action="update" on="focus-out"/focus-out="update"/g;
    $content =~ s/classBinding=":btn view.searchUser.restrictUI:restrictUI"/class="btn {{if searchUser.restrictUI 'restrictUI' ''}}"/g;

    # general changes
    $content =~ s/{{view (?:'|")?([^\s'"]+)(?:'|")?/{{$1/g; # view -> component
    $content =~ s/("|'|{|\s|=)view\./$1/g; # remove reference to view
    $content =~ s/(\w)Binding=(?:"|')([^"']+)(?:"|')/$1=$2/g; # no more "Binding" in templates
    $content =~ s/(\w)Binding=/$1=/g;
    $content =~ s/master=controller/master=model/g; # no more reference to controller
    $content =~ s/controller[^s]//g;
    $content =~ s/{{unbound /{{/g; # no more need to unbound

    return $content;
}

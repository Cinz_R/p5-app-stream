#! /usr/bin/env perl

use local::lib qw/local/;

use FindBin;
BEGIN { unshift @INC, "$FindBin::Bin/../lib" }

use strict;
use warnings;

use Getopt::Long;
use Devel::REPL;
use Stream2;

use Pod::Text;

use Log::Any::Adapter;
use Log::Log4perl qw/:easy/;

Log::Any::Adapter->set('Log4perl');

$| = 1;

use Log::Any qw/$log/;

use Term::ANSIColor;

sub usage{
  print qq|$0 -c <conf file>

|;
  exit(1);
}

my $verbose = 0;
my $conf_file;

Getopt::Long::Configure( "pass_through" );
GetOptions( "c=s" => \$conf_file,
            "v" => \$verbose
          );

unless( $verbose ){
  Log::Log4perl->easy_init($INFO);
}else{
  Log::Log4perl->easy_init($TRACE);
}

unless( $conf_file ){
    foreach my $candidate ( './app-stream2.'.$ENV{USER}.'.conf',
                            './app-stream2.conf',
                            $ENV{HOME}.'/app-stream2.conf',
                            '/etc/stream2/app-stream2.conf'){
        if( -e $candidate ){
            $log->info("Will use $candidate as config file");
            $conf_file = $candidate;
            last;
        }
    }
}

unless( $conf_file ){ usage(); }


$log->info('Building $s2 global object from conf file '.$conf_file);
our $s2 = Stream2->new({ config_file => $conf_file });


## Accessing config makes sure the conf file is ok
$s2->config();

if( $s2->interactive() ){
    print $s2->doge();
    my @prefix = qw(Much Such Very Many So);
    my @suffix = qw(shell interactive code doge);
    my @colours= qw(bright_magenta bright_cyan bright_green bright_red yellow);
    print "\n\n";
    print ' ' x int(rand(30)) , Term::ANSIColor::colored($prefix[rand @prefix].' '.$suffix[rand @suffix], $colours[rand @colours]),"\n\n";
    print ' ' x int(rand(30)) , Term::ANSIColor::colored($prefix[rand @prefix].' '.$suffix[rand @suffix], $colours[rand @colours]),"\n\n";
    print "\n\n"
}



$log->info('OK');

my $repl = Devel::REPL->new;

binmode $repl->out_fh, ':utf8';

my $pod2text = sub{
    my ($o) = @_;
    unless( $o ){
        return 'Try $s2 or help($s2)'."\n";
    }
    my $class = ref($o) || $o;
    my $short_package = $class;
    $short_package =~ s/::/\//g;
    $short_package =~ s/$/.pm/;
    my $package_file = $INC{$short_package};

    unless( $package_file ){
        return "Sorry, cannot file absolute file for $class ( $short_package )\n";
    }

    my $output;
    my $p2txt = Pod::Text->new();
    $p2txt->output_string(\$output);
    $p2txt->parse_file($package_file);
    return $output;
};

## Inject the instance of s2 in the shell lexical environment.
$repl->load_plugin('Commands');
$repl->command_set()->{pod} = sub{ return $pod2text };
$repl->command_set()->{help} = sub{ return $pod2text };

$repl->load_plugin('ReadLineHistory');
$repl->load_plugin('LexEnv');
$repl->lexical_environment->do(q|my $s2 = $main::s2 ;
|);
## Various autocompletion.
$repl->load_plugin('CompletionDriver::LexEnv');
$repl->load_plugin('CompletionDriver::Methods');
$repl->load_plugin('CompletionDriver::INC');

## Some clever prompt
$repl->load_plugin('FancyPrompt');
$repl->fancy_prompt(sub {
 my $self = shift;
 return Term::ANSIColor::colored('Such $s2', 'green').' ['.$s2->config_file().']  ';
});

## Allow multiline statements.
$repl->load_plugin('MultiLine::PPI');

# And run!

if( my $script = shift @ARGV ){
    $log->info("Loading $script file for execution");
    my $code = File::Slurp::read_file($script);
    $repl->lexical_environment->do($code);
}else{
    # This is interactive.
    $repl->run();
}

$log->info("Bye!");

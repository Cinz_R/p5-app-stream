#! bin/s2-shell

# Checks watchdogs and saved searches criteria ids against cv_criteria table
# and creates a new criteria if the tokens contain a 'jobtype' key
# the new criteria will be identical to the original with the exception of the 'jobtype' token being renamed to 'default_jobtype'
# Usage:
#   bin/fix-s2-jobtype-criteria.pl -c <conf file>

use feature 'say';
use Data::Dumper;
use Stream2::Criteria;

my %criteria_ids = ();

my $fix_criteria = sub {

    my $watchdogs  = $s2->stream_schema->resultset('Watchdog');
    while( my $watchdog = $watchdogs->next() ){
        $criteria_ids{$watchdog->criteria_id()}->{watchdog} ||= [];
        push(@{ $criteria_ids{$watchdog->criteria_id()}->{watchdog} }, $watchdog);

        say "found watchdog (id: " . $watchdog->id . ") criteria_id: " . $watchdog->criteria_id();
    }

    my $saved_searches = $s2->stream_schema->resultset('SavedSearches');
    while( my $ss = $saved_searches->next() ){
        $criteria_ids{$ss->criteria_id()}->{saved_search} ||= [];
        push(@{ $criteria_ids{$ss->criteria_id()}->{saved_search} }, $ss);

        say "found saved search (id: " . $ss->id . ") criteria_id: " . $ss->criteria_id();
    }

    say "total criteria found: " . scalar(keys %criteria_ids) . "\n";

    foreach my $criteria_id (keys %criteria_ids) {
        say "\ngetting criteria object for id: " . $criteria_id;

        my $criteria = Stream2::Criteria->new(
            id => $criteria_id,
            schema => $s2->stream_schema,
            )->load;

        my %tokens = $criteria->tokens();

        if(exists($tokens{default_jobtype})) {
            say "default_jobtype already defined under criteria id: " . $criteria_id;
            next;
        } elsif(!exists($tokens{jobtype})) {
            say "neither 'default_jobtype' nor 'jobtype' tokens defined under criteria id: " . $criteria_id;
            next;
        }

        $tokens{default_jobtype} = $tokens{jobtype};
        delete $tokens{jobtype}; # do not put this into the tokens array of the new criteria

        my $new_criteria = Stream2::Criteria->new(schema => $s2->stream_schema());
        $new_criteria->add_tokens(%tokens);
        $new_criteria->save();

        say "new criteria saved, id: " . $new_criteria->id;

        foreach my $key (qw/watchdog saved_search/) {
            next if !scalar(@{$criteria_ids{$criteria_id}->{$key} // []});

            say "updating " . scalar(@{$criteria_ids{$criteria_id}->{$key}}) . " '$key' references to old criteria_id: " . $criteria_id;

            foreach my $obj ( @{ $criteria_ids{$criteria_id}->{$key} } ) {
                say "setting new criteria id on '$key' object (id: " . $obj->id . ") " . ref($obj);
                $obj->update({ criteria_id => $new_criteria->id });
            }
        }
    }
    say "done!";
};

$s2->stream_schema->txn_do($fix_criteria);


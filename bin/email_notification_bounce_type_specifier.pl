#!/usr/bin/env bin/s2-shell

use 5.016;
use warnings;

=head1 SYNOPSIS

    $ bin/email_notification_bounce_type_specifier.pl

=head1 DESCRIPTION

Finds all bounce notifications (where notification = "Bounce"), then
reclassifies them as either "bounce.soft" or "bounce.hard" depending on the
contents of their notification_body field. Hard bounces will have a
"hard_bounce: true" entry. Soft bounces won't.

=cut

my $bounces = $s2->factory('EmailNotification')->search(
    { notification => 'Bounce' },
    {}
);

$bounces->fast_loop_through( sub {

    # There are 2 kinds of Mailjet notification bodies. The older kind have 3
    # key/value pairs. The newer kind have more: typically 9 or 13.
    my $bounce      = shift;
    my $body        = $bounce->notification_body;
    my $num_entries = scalar keys $body;

    my $bounce_type;
    if ( $num_entries == 3 ) {
        $bounce_type = $body->{bounce}->{bounceType} =~ /permanent/i
            ? 'bounce.hard' : 'bounce.soft';
    }
    else {
        $bounce_type = $bounce->notification_body->{hard_bounce}
            ? 'bounce.hard' : 'bounce.soft';
    }
    
    say "Setting notification #", $bounce->id, " to $bounce_type";
    $bounce->update( { notification => $bounce_type } );
} );
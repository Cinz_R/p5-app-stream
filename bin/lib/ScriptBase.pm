package ScriptBase;

use Moose;

use Log::Any qw/$log/;
use Stream2::Action::SynchroniseAdCourierUserSiblings;

has 'company' => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

has 's2' => (
    is => 'ro',
    isa => 'Stream2',
    required => 1
);

has 'user_map' => (
    is => 'ro',
    isa => 'HashRef',
    lazy_build => 1
);

has 'admin_user' => (
    is => 'ro',
    isa => 'Stream2::O::SearchUser',
    lazy_build => 1
);

has 'all_user_ids' => (
    is => 'ro',
    isa => 'ArrayRef[Int]',
    lazy_build => 1
);

# _vivify_user_ids
# if a permission is at office level for example, we need to
# translate this into a list of user level identifiers
# /company5/office32
# -> /company5/office32/team7/user43
# -> /company5/office32/team7/user17
# -> /company5/office32/team44/user17
# -> /company5/office32/team3/user17
# -> etc...

sub _vivify_sub_identifiers {
    my ( $self, $permission ) = @_;

    my ( $company, $office, $team, $consultant ) = map { $permission->{$_} // '' } qw/company office team consultant/;

    my $ident;
    if ( $consultant ) { # only a single user
        $ident = join( '/', '', $company, $office, $team, $consultant ); # /company/office/team/user
    }
    elsif ( $team ) { # all users in the team
        $ident = join( '/', '', $company, $office, $team, '' ); # /company/office/team/
    }
    elsif ( $office ) { # all users in an office
        $ident = join( '/', '', $company, $office, '' ); # /company/office/
    }
    else { # company level restrictions need not apply
        $ident = join( '/', '', $company, '' );
    }
    
    my $regex = qr|\A\Q$ident\E|;
    return grep { $_ =~ $regex } keys $self->user_map;
}

sub _build_admin_user {
    my ( $self ) = @_;
    return $self->s2->factory('SearchUser')->search({ group_identity => $self->company })->first()
        or die "Unable to find first user";
}

sub _build_all_user_ids {
    my ( $self ) = @_;
    return [ values %{$self->user_map} ];
}

# _build_user_map
# get the latest list of adc users for this company and update our user table
# then fetch the hierarchical identifier for each user

sub _build_user_map {
    my ( $self ) = @_;

    $self->perform_user_synchronisation();

    my @users = $self->s2->factory('SearchUser')->search({ group_identity => $self->company })->all();

    my %map = map {
        my $u = $_;
        ( $u->contact_details && $u->contact_details->{consultant} ) ? (
            sprintf( '/%s/%s/%s/%s', map { $u->contact_details->{$_} // '' } qw/company office team consultant/ ),
            $u->id
        ) : ();
    } @users;

    return \%map;
}


sub perform_user_synchronisation {
    my ( $self ) = @_;

    $log->info( 'Syncing adcourier users' );
    my $action = Stream2::Action::SynchroniseAdCourierUserSiblings->new({
        stream2 => $self->s2,
        company => $self->company
    });
    $action->instance_perform();
    $log->info("Synchronizing users done");
}


__PACKAGE__->meta->make_immutable;
 
1;

package ImportQuotas;

use Moose;

use lib q{./bin/lib};
extends q{ScriptBase};

use Log::Any qw/$log/;
use List::Util ();

sub run {
    my ($self) = @_;
    # these create warnings
    {
        local *Log::Any::Proxy::warn = sub {};
        $self->user_map;
        $self->admin_user;
    }

    $log->info('Will import quotas for company ' . $self->company);

    my $user_paths = $self->user_map;
    my @boards = $self->get_all_company_boards($user_paths);
    $log->infof('Will set %s quotas for the following boards: ' . join(', ', @boards), $self->company);

    foreach my $user_path ( keys %$user_paths ) {
        my $user = $user_paths->{$user_path};

        foreach my $board ( @boards ) {
            foreach my $type ( qw/cv profile search/ ) {
                my $remaining_quota = $self->s2->stream_api->check_board_quota($user->provider_id, $board, $type) // 0;
                if ( !$remaining_quota ) {
                    $log->infof('No quota available for user %s - board: %s - type: %s', $user_path, $board, $type);
                    next;
                }
                $log->infof('Received %d quotas from AdCourier for user %s - board: %s - type: %s', $remaining_quota, $user_path, $board, $type);

                $log->infof('Creating quota row for user %s', $user_path);
                $self->s2->quota_object->create({
                    board => $board,
                    type => "search-$type",
                    quota => [{
                        path => $user_path,
                        remaining => $remaining_quota,
                    }]
                },
                {
                    actioned_by => 'quota-import',
                    remote_addr => '127.0.0.1',
                });

                # for team/office/company, we want to increment on the total number of quotas,
                # so get what is currently set and add the $remaining_quota.
                foreach my $level ( qw/team office company/ ) {
                    my $level_quota = eval {
                        $self->s2->quota_object->fetch_quota({
                            path => $self->get_path_for($user_path, $level),
                            board => $board,
                            type => "search-$type",
                        });
                    };
                    $level_quota //= 0;
                    $log->infof('Will update %s for user_path %s to have %d quotas - board: %s - type: %s', $level, $user_path, $level_quota + $remaining_quota, $board, $type);
                    $self->s2->quota_object->create({
                        board => $board,
                        type => "search-$type",
                        quota => [{
                            path => $self->get_path_for($user_path, $level),
                            remaining => $level_quota + $remaining_quota,
                        }]
                    },
                    {
                        actioned_by => 'quota-import',
                        remote_addr => '127.0.0.1',
                    });
                }
            } # end type
            $log->infof('Slept for %d seconds', sleep(10));
        } # end board
    } # end user_path
    # end life
}

# We need to update each level of the users hierarchy individually when
# setting quotas, this will generate strings based on the desired level.
sub get_path_for {
    my ($self, $path, $want) = @_;
    $path =~ s{^/}{};
    my @hierarchy = split('/', $path);

    if ( $want eq 'team' && scalar(@hierarchy) >= 3 ) {
        return sprintf('/%s/%s/%s', $hierarchy[0], $hierarchy[1], $hierarchy[2]);
    }
    elsif ( $want eq 'office' && scalar(@hierarchy) >= 2 ) {
        return sprintf('/%s/%s', $hierarchy[0], $hierarchy[1]);
    }
    elsif ( $want eq 'company' ) {
        return '/' . $hierarchy[0];
    }
    elsif ( $want eq 'user' && scalar(@hierarchy) == 4 ) {
        return $path;
    }
}

sub get_all_company_boards {
    my ($self, $user_paths) = @_;
    my @boards;
    foreach my $user_path ( keys %$user_paths ) {
        my $user = $user_paths->{$user_path};
        my $subscriptions_ref = $user->subscriptions_ref;
        push @boards, keys(%$subscriptions_ref) or next;
    }
    return List::Util::uniq(@boards);
}

sub _build_user_map {
    my ( $self ) = @_;

    $log->info( 'Syncing adcourier users' );
    my $action = Stream2::Action::SynchroniseAdCourierUserSiblings->new({
        stream2 => $self->s2,
        company => $self->company
    });
    $action->instance_perform();

    my @users = $self->s2->factory('SearchUser')->search({ group_identity => $self->company })->all();

    my %map = map {
        my $u = $_;
        ( $u->contact_details && $u->contact_details->{consultant} ) ? (
            sprintf( '/%s/%s/%s/%s', map { $u->contact_details->{$_} // '' } qw/company office team consultant/ ),
            $u
        ) : ();
    } @users;

    return \%map;
}

__PACKAGE__->meta->make_immutable;
1;

package ImportEmailTemplates;

use Moose;

use lib q{./bin/lib};
extends q{ScriptBase};

use MIME::Types;
use Log::Any qw/$log/;
use Encode ();
use Array::Utils ();

has 'type' => (
    is => 'ro',
    isa => 'Str',
    required => 1
);

has 'run_fixup' => (
    is => 'ro',
    isa => 'Bool'
);

=head2 run

This is the main loop which does the following...

1. Fetches and updates all adcourier users for given company
2. Downloads all email templates for the company
3. Creates email_template objects in s2 DB
4. Downloads and associates attachements for the email_template
5. Vivifies and applies restrictions to the email_templates

=cut

sub run {
    my ( $self ) = @_;

    # these create warnings
    {
        local *Log::Any::Proxy::warn = sub {};
        $self->user_map;
        $self->admin_user;
    }

    my $email_factory = $self->s2->factory('EmailTemplate');
    my $templates_ref = $self->s2->email_template_api->get_company_email_templates( $self->company, $self->type );

    TEMPLATE: foreach my $template ( @$templates_ref ) {

        my $subtype = $template->{subtype};
        my $template_name = $template->{name};
        if ( $subtype ) {
            $template_name = sprintf('[%s] - %s', $subtype, $template_name);
        }

        if ( $email_factory->find({ group_identity => $self->company, name => $template_name }) ){
            $log->infof( 'Template "%s" already exists for company %s, skipping...', $template_name, $self->company );
            next TEMPLATE;
        }

        $log->infof( 'Creating template - %s', $template_name );

        if ( $self->run_fixup ) {
            $log->infof( ' - Fixing up template - %s', $template_name );
            $template = $self->fix_up_template( $template );
        }

        my $email_template = eval {
            my $xbody = $self->s2->html_parser()->load_html( string => Encode::decode( 'utf-8', $template->{body} ),  encoding => 'UTF-8' );

            # fetch adc images and save them to our S3, swap the sources
            $self->_resolve_images( $xbody );

            # save email template to DB
            $self->_create_email_template( $self->_to_mime_entity( $template, $xbody ) ,$template_name );
        };
        if( my $err = $@ ){
            $log->errorf( "Template: %s - failed to process entity error: %s", $template_name, $err );
            next TEMPLATE;
        }

        # attachments
        foreach my $attachment ( @{$template->{attachments}} ){
            $log->infof( ' - Adding attachment: %s', $attachment );
            my $file = $self->_download_and_create_file( $attachment );
            $email_template->find_or_create_related( 'email_template_attachments' => {
                group_file_id => $file->id
            });
        }

        my @allowed_ids;
        foreach my $permission ( @{$template->{permissions}} ){
            push( @allowed_ids, grep { $_ } map {
                $self->user_map->{$_}
            } $self->_vivify_sub_identifiers( $permission ) );
        }

        my @restricted_ids = Array::Utils::array_minus( @{$self->all_user_ids}, @allowed_ids );
        $log->infof(" - Adding restriction for %s", $_ ) for @restricted_ids;

        $email_template->permission_setting->set_users_value(
            $self->admin_user, \@restricted_ids, 0
        );
    }
}

sub _to_mime_entity {
    my ( $self, $template, $xbody ) = @_;

    return $self->s2->factory('Email')->build(
        [
            Type     => 'text/html; charset=UTF-8',
            Encoding => 'quoted-printable',
            Subject  => Encode::encode( 'MIME-Q', $template->{subject} ),
            (
                $template->{from_email}
                ? ( 'Reply-To' => Encode::encode( 'MIME-Q', $template->{from_email} ) )
                : ()
            ),
            ( $template->{bcc} ? ( Bcc => Encode::encode( 'MIME-Q', $template->{bcc} ) ) : () ),
            ( $template->{cc} ? ( Cc => Encode::encode( 'MIME-Q', $template->{cc} ) ) : () ),
            Disposition => 'inline',
            Data        => [ $xbody->toStringHTML() ]
        ]
    );
}

sub _download_and_create_file {
    my ( $self, $attachment ) = @_;

    my $response = $self->s2->user_agent->get( 'http://www.adcourier.com/' . $attachment );
    my $content = $response->decoded_content;

    my $mt = MIME::Types->new();
    my ( $filename ) = $attachment =~ m|/([^/]+)\z|;
    my $type = $mt->mimeTypeOf( $filename ) // 'application/octet-stream';

    my $groupfile = $self->s2->factory('GroupFile')->find({ group_identity => $self->company, name => $filename });
    unless ( $groupfile ){
        $groupfile = $self->s2->factory('GroupFile')->create(
            {
                content_type => $type.'',
                content => $content,
                name => $filename,
                group_identity => $self->company
            }
        );
    }
    return $groupfile;
}

sub _resolve_images {
    my ( $self, $xdoc ) = @_;
    my @img_nodes = $xdoc->findnodes('//img[string(@src)]');

    unless (@img_nodes) {
        # Nothing to resolve in this html.
        return $xdoc;
    }

    foreach my $img_node (@img_nodes) {
        my $src = $img_node->getAttribute('src');
        $log->infof( ' - Adding image: %s', $src );
        my $file = $self->_download_and_create_file( $src );
        $img_node->setAttribute('src' => $file->uri);
    }
}


sub _create_email_template {
    my ( $self, $mime_entity, $template_name ) = @_;

    $mime_entity->entity_size(
        $self->s2->config()->{max_email_template_size} // 1024 * 1024 * 2
    );

    # swap some of adcourier's tokens for the tokens we already have while also
    # leaving the tokens s2 doesn't have intact
    my $new_mime_entity = $mime_entity->clone()->process_with_stash({
        boardname => '[board_nice_name]',
        candidatename => '[candidate_name]',
        contactemail => '[consultant_email]',
        contactname => '[contactname]',
        jobtitle => '[advert_title]',
        candidate_firstname => '[candidate_firstname]',
        contactfax => '[contactfax]',
        contactoffice => '[contactoffice]',
        contacttelephone => '[contacttelephone]',
        contacttelephone => '[contacttelephone]',
        dear_candidate => '[dear_candidate]',
        beste_sollicitant => '[beste_sollicitant]',
        geachte_kandidaatnaam => '[geachte_kandidaatnaam]',
        jobref => '[jobref]',
        team_nice_name => '[team_nice_name]',
    });

    my $email = $self->s2->factory('EmailTemplate')->create({
        group_identity => $self->company,
        name => $template_name,
    });

    $email->mime_entity_object( $new_mime_entity );
    $email->update();

    return $email;
}

=head2 fix_up_template
    Fixes up any issues in the template, such as miss naming of the merged
    fields in the template body.
=cut

sub fix_up_template {
    my ( $self, $template ) = @_;

    # resourcinggroup has [ Candidate name ] instead of [candidatename]
    $template->{body} =~ s/\[Candidate\sName\]/[candidatename]/g;

    return $template
}

__PACKAGE__->meta->make_immutable;
1;

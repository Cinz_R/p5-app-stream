package ImportResponseHotlists;

use Moose;

use lib q{./bin/lib};
extends q{ScriptBase};

use Log::Any qw/$log/;

use Data::Dumper;

=head2 run

This is the main loop which does the following...

1. Sync the users
2. Grab the hotlists.
3. Loop through the responses and attach them to the hotlists.

=cut

sub run {

    my ($self)  = @_;
    my $s2      = $self->s2;
    my $company = $self->company;
    
    # Find the customer in the BBCSS.
    my $customer = $s2->bbcss_api()->get_customer( { broadbean_name => $company, flavour => 'ResponsesHolder' } );
    unless ($customer) {
        die "Cannot find ResponsesHolder customer for company = $company\n";
    }
    $log->infof(
        "Found customer FLAVOUR = %s BROADBEAN_NAME = %s KEY = %s",
        $customer->{flavour},
        $customer->{broadbean_name},
        $customer->{careerbuilder_customer_key}
    );


    # Synchronise the companies users and build users_by_path.
    $self->perform_user_synchronisation;
    my $users_by_path = $self->_build_users_by_path();

    my $first_company_user =
      $s2->factory('SearchUser')->search( { provider => 'adcourier', group_identity => $company } )->first();

    my $adcresponses = $s2->build_template(
        'adcresponses',
        undef,
        {
            company        => $company,
            user_object    => $first_company_user,
            bbcss_customer => $customer,
        }
    );

    # Time to loop through all hotlists coming from the juiceapi.

    my $candidate_api = $s2->candidate_api();
    my $user_tags     = $s2->factory('Usertag');

    my $last_hotlist_time;
    my $hotlists_found = 0;
    while (1) {
        my $res = $candidate_api->request_json(
            'GET',
            $candidate_api->url(
                '/candidate/hotlists',
                {
                    company => $company,
                    (
                        $last_hotlist_time ? ( after_hotlist_time => $last_hotlist_time ) : ()
                    )
                }
            )
        );
        my $hotlists = $res->{hotlists};
        unless (@$hotlists) {
            $log->info("No more hotlists");
            last;
        }
        $hotlists_found += scalar(@$hotlists);

        foreach my $hotlist (@$hotlists) {
            unless ( $hotlist->{from_email} ) {
                $log->warnf( "Cannot find email for hotlist: %s",
                    Dumper $hotlist );
                next;
            }

            my $hotlist_identity_path = '/'
              . $hotlist->{company} . '/'
              . $hotlist->{office} . '/'
              . $hotlist->{team} . '/'
              . $hotlist->{consultant};

            $log->infof(
                "HOTLISTING '%s' for latest candidate with email = '%s for user %s ' ..",
                $hotlist->{name},
                $hotlist->{from_email},
                $hotlist_identity_path
            );
            my $hotlist_owner = $users_by_path->{ lc($hotlist_identity_path) };
            unless ($hotlist_owner) {
                $log->warnf( "Cannot find local user for owner_path = %s",
                    $hotlist_identity_path );
                next;
            }
            $log->info("Got hotlist owner in search");

            # Got user.

            my $latest_response      = undef;
            my $latest_response_date = '';
            $s2->bbcss_api()->go_through_responses(
                {
                    broadbean_name => $company,
                    flavour        => 'ResponsesHolder',
                    property_name  => 'applicant_email',
                    property_value => $hotlist->{from_email}
                },
                sub {
                    my ($response) = @_;
                    my $cs_document = $response->{document}->{cs_document};
                    my $document_owner_id = $cs_document->{broadbean_adcuser_id};
                    unless (
                        $hotlist_owner->provider_id() == $document_owner_id )
                    {
                        return;
                    }
                    $log->debugf(
                        "Found document with owner = %s and email = %s",
                        $document_owner_id, $hotlist->{from_email} );

                    if ( $cs_document->{broadbean_adcresponse_date} gt $latest_response_date ) {
                        $latest_response = $response;
                        $latest_response_date = $cs_document->{broadbean_adcresponse_date};
                    }
                }
            );

            unless ($latest_response) {
                $log->warn( "Could not find any response for the given email and given hotlist owner $hotlist_identity_path. Skipping" );
                next;
            }

            my $document_id = $latest_response->{document_id};

            $log->infof( ".. Found document id = %s ", $document_id );

            # So. At this point we have:
            # The user in search to which the hotlist (aka usertag) belongs.
            # The ID of the document in BBCSS.
            #
            # So we just need to
            # 1 - Create the usertag
            # 2 - Assign to the document ID.
            # Hurray!
            my $user_tag = $user_tags->find_or_create(
                {
                    user_id     => $hotlist_owner->id(),
                    tag_name    => $hotlist->{name},
                    tag_name_ci => 'Needs to be there. Trigger will fix this'
                }
            );

            # Extract local usertags from the document
            my %candidate_details = $adcresponses->scrape_candidate_details(
                $latest_response->{document}->{cs_document} );

            $log->info( "Document already has usertags = " . Dumper( $candidate_details{usertags} ) );
            $log->info("Setting the new set of usertags");
            my @there_tags = $user_tags->search(
                {
                    user_id  => $hotlist_owner->id(),
                    tag_name => {
                        -in => [
                            map { $_->{id} } @{ $candidate_details{usertags} }
                        ]
                    }
                }
            )->all();
            @there_tags = grep { $_->id() != $user_tag->id() } @there_tags;
            push @there_tags, $user_tag;

            my $candidate_object = Stream2::Results::Result->new();
            $candidate_object->destination('adcresponses');

            # Injecting scraped user details in the candidate object
            # so the set_usertags works.
            foreach my $key ( keys %candidate_details ) {
                $candidate_object->attr( $key, $candidate_details{$key} );
            }
            $candidate_object->candidate_id($document_id);

            $adcresponses->set_usertags( $candidate_object, \@there_tags );

        }    # End of hotlist loop

        $log->info( "Imported " . $hotlists_found );
        $last_hotlist_time = $hotlists->[-1]->{time};
    }

    $log->infof( "Done importing %s hotlists items", $hotlists_found );
}

sub _build_users_by_path {
    my ($self)  = @_;
    my $s2      = $self->s2;
    my $company = $self->company;

    $log->info("Building a map of all users per path");
    my $all_users = $s2->factory('SearchUser')
      ->search( { group_identity => $company, provider => 'adcourier' } );

    my $users_by_path = {};
    my $good_users    = 0;
    my $bad_users     = 0;
    while ( my $user = $all_users->next() ) {
        my $hierarchical_path = eval { $user->hierarchical_path() };
        unless ($hierarchical_path) {
            $bad_users++;
            next;
        }
        $good_users++;

        # Lower case stuff. Adcourier case can change..
        $hierarchical_path = lc( $user->hierarchical_path() );
        $log->infof( "User %s -> %s", $user->id(), $hierarchical_path );
        $users_by_path->{$hierarchical_path} = $user;
    }
    $log->info("Mapped $good_users users, skipped $bad_users users");

    return $users_by_path;
}

__PACKAGE__->meta->make_immutable;
1;

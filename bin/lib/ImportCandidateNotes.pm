package ImportCandidateNotes;

use Moose;
use lib q{./bin/lib/};
extends q{ScriptBase};

use Log::Any qw/$log/;
use Log::Any::Adapter qw/Stdout log_level info/;
use DateTime::Format::DateParse;
use HTML::Strip;
use DateTime;

=head1 NAME

ImportCandidateNotes

=head1 DESCRIPTION

I import public and private notes into the Search 2 candidate action log
from old manage responses

- I strip HTML from notes
- I set the author to "Unknown" if I can't find the original user who added it

=cut

has 'after_id' => (
    is => 'ro',
    isa => 'Int',
    default => 0
);

has 'oldest_response_first' => (
    is => 'ro',
    isa => 'Bool',
    default => 0
);

has '_notes_cache' => (
    is => 'rw',
    isa => 'HashRef',
    default => sub { {} }
);

sub run {
    my ( $self ) = @_;

    my $MAX_WORKERS = 10;
    my $CHUNK_SIZE = 1;
    my $number_of_tries = 5;
    my $n_responses = 0;
    my $previous_id = $self->after_id;

    my $not_before_time = DateTime->now()->subtract( months => 6 )->epoch();

    $log->infof( "Not importing responses older than %s", $not_before_time );

    my $cal_factory = $self->s2->factory('CandidateActionLog');
    my $user_factory = $self->s2->factory('SearchUser');

    while( 1 ){
        # This can fail randomly because of network issues.
        my $local_n_response = eval{
            $self->s2->candidate_api->loop_through_responses(
                $self->company,
                sub{
                    my $response = shift;

                    if ( $response->{time} < $not_before_time ) {
                        $previous_id = $response->{response_id};
                        return;
                    }

                    $log->infof( "finding notes for candidate: %s", $response->{response_id} );
                    
                    my $user_id = $self->user_map->{sprintf(q{/%s/%s/%s/%s}, @{$response}{qw/company office team consultant/})};
                    unless ( $user_id ) {
                        $log->infof("Can't find user %s", sprintf(q{/%s/%s/%s/%s}, @{$response}{qw/company office team consultant/}) );
                        $previous_id = $response->{response_id};
                        return;
                    }

                    my $user = $user_factory->search({ id => $user_id })->first();
                    my $contact_details = $user->contact_details // {};
                    my $name = $contact_details->{contact_name} || $contact_details->{contact_email} || "Unknown";

                    my $candidate_id = 'adcresponses_' . $response->{response_id};

                    # some notes are stored in the adc response table... if they are "private"
                    my @notes;
                    do {
                        push @notes, {
                            action => 'note',
                            destination => 'adcresponses',
                            user_id => $user_id,
                            group_identity => $self->company,
                            insert_datetime => $_->{datetime},

                            data => {
                                author => $name,
                                candidate_email => $response->{from_email},
                                candidate_name => $response->{from_name},
                                content => $_->{content},
                                privacy => 'user'
                            }
                        };
                    } for $self->_extract_private_notes( $response );

                    do {

                        my $name = "Unknown";
                        my $adding_user = $user_factory->search({ group_identity => $self->company, provider_id => $_->{user_id} })->first();
                        if ( $adding_user && ( my $adding_contact_details = $adding_user->contact_details ) ){
                            $name = $adding_contact_details->{contact_name} || $adding_contact_details->{contact_email} || "Unknown";
                        }

                        push @notes, {
                            action => 'note',
                            candidate_id => $candidate_id,
                            destination => 'adcresponses',
                            user_id => $user_id,
                            group_identity => $self->company,
                            insert_datetime => $_->{datetime},

                            data => {
                                author => $name,
                                candidate_email => $response->{from_email},
                                candidate_name => $response->{from_name},
                                content => $_->{content},
                                privacy => 'group'
                            }
                        };

                    } for $self->_fetch_public_notes( $response );

                    if ( scalar( @notes ) ){

                        my $document_id;
                        $self->s2->bbcss_api()->go_through_responses(
                            {
                                broadbean_name => $self->company,
                                flavour => 'ResponsesHolder',
                                ( $previous_id ? ( after_document_id => $previous_id ) : () ),
                                property_name   => 'broadbean_adcresponse_id',
                                property_value  => $response->{response_id}
                            },
                            sub {
                                my $document = shift;
                                $document_id = $document->{document_id};
                            }
                        );

                        if ( $document_id ) {
                            do {
                                $log->infof("Adding %s note for candidate: %s, user: %s, content: %s",
                                    $_->{data}->{privacy},
                                    'adcresponses_' . $document_id,
                                    $_->{user_id},
                                    $_->{data}->{content}
                                );
                                $cal_factory->create({ %$_, candidate_id => 'adcresponses_' . $document_id });
                            } for @notes;
                        }
                    }

                    $n_responses++;
                    $previous_id = $response->{response_id};
                },
                {
                    previous_id => $previous_id || 0,
                    $self->oldest_response_first ? () : ( reverse_order => 1 ),
                });
        };
        if ( my $err = $@ ) {
            $log->warnf("An error has occured. %s", substr( $err , 0 , 300 ));
            unless ( $number_of_tries-- ){
                $log->warn( "I have tried too many times to import the responses but it has failed" );
                last;
            }
            $log->warnf("Will restart from response ID=%s", $previous_id // 0);
        }
        else {
            unless( $local_n_response ){
                # No error and no local_n_response. This is the end!
                last;
            }

            $log->infof( "Apparently I still have %s responses left, previous_id: %s", $local_n_response, $previous_id );
        }
    }

    $log->info("Went through $n_responses responses");
    $log->info("Bye");
}

sub _extract_private_notes {
    my ( $self, $response ) = @_;
    my $notes = Encode::decode( "UTF-8", $response->{notes} )
        or return ();

    # <span class="date">1474985449</span><p>hgghfgf</p><hr /><span class="date">1474985531</span><p>dgtbttgtggtt</p>
    my @notes;
    my $xdoc = $self->s2->html_parser->load_html(
        string => Encode::encode( "UTF-8", $notes ),
        encoding => 'UTF-8'
    );

    my ( $body ) = $xdoc->findnodes( '/html/body' );

    my $message = "";
    my $time;
    foreach my $child ( $body->childNodes() ){
        if ( $child->nodeName() eq 'span' && ( my $class = $child->getAttributeNode('class') ) ){
            if ( $class->getValue() eq 'date' ){
                ( $time ) = $child->textContent =~ m/(\d{10})/;
            }
        }
        else {
            $message = $message . " " . $child->textContent;
        }

        if ( $child->nodeName() eq 'hr' ){
            if ( $time && $message ) {
                chomp( $message );
                push @notes, { datetime => DateTime->from_epoch( epoch => $time ), content => $message };
            }
            else {
                $log->warnf( "unable to parse: %s", $notes );
            }
            $message = ""; $time = undef;
        }
    }

    if ( $time && $message ) {
        chomp( $message );
        push @notes, { datetime => DateTime->from_epoch( epoch => $time ), content => $message };
    }
    else {
        $log->warnf( "unable to parse: %s", $notes );
    }


    return @notes;
}

sub _fetch_public_notes {
    my ( $self, $response ) = @_;

    my $candidate_id = $self->_public_notes_candidate_id( $response->{from_email} );
    $candidate_id =~ s/\s+//g;

    my $json_ref = $self->_notes_cache()->{$candidate_id};

    unless ( $json_ref ) {
        # the client is AWOL from pinto
        my $res = $self->s2->user_agent->get(
            sprintf('http://candidatenotes.adcourier.com/cws/%s/candidate/%s/note', $self->company, $candidate_id),
            Authorization => q{bGRmamtncG84MXYzNDA4NjFvcmhpcWVwcjBnOTgyMDk0Mzg3dHlvaXFldXJs}
        );
        if ( $res->is_success ) {
            $json_ref = $self->s2->json->decode( $res->content );
            $self->_notes_cache()->{$candidate_id} = $json_ref;
        }
        else {
            $log->warnf( "unable to fetch notes for %s", $candidate_id );
            return;
        }
    }

    my @notes = map {
        my $hs = HTML::Strip->new();
        my $message = $hs->parse( $_->{text} );

        +{
            datetime => DateTime::Format::DateParse->parse_datetime( $_->{insert_datetime} ),
            content => Encode::decode( "UTF-8", $message ),
            user_id => $_->{user_id}
        };
    } @{$json_ref->{notes}};

    return @notes;
}

sub _public_notes_candidate_id {
    my ( $self, $email ) = @_;

    $email =~ s/\@/_at_/g;
    $email =~ s/\./_/g;

    return sprintf(
        "%s|%s",
        $self->company,
        $email,
    );
}


__PACKAGE__->meta->make_immutable();

1;

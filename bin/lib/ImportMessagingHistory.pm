package ImportMessagingHistory;

use Moose;

extends q{ScriptBase};

use Log::Any qw/$log/;

use HTTP::Request::Common ();
use DateTime;

has memory_adverts => (
    is => 'rw',
    isa => 'HashRef',
    default => sub { {} }
);

has memory_bbcss_responses => (
    is => 'rw',
    isa => 'HashRef',
    default => sub { {} }
);

=head2 run

=cut

sub run {
    my ( $self ) = @_;

    # these create warnings
    {
        local *Log::Any::Proxy::warn = sub {};
        $self->user_map;
        $self->admin_user;
    }

    my $candidate_api = $self->s2->candidate_api;

    my $today = DateTime->now();
    my $six_months_ago = $today->subtract( months => 6 )->truncate( to => 'month' );
    my $stop_epoch = $six_months_ago->epoch;

    my $pause_time = 1;
    my $after_response_id = 0;
    my $importing = 1;
    my @messaged_candidates = ();
    while ($importing) {
        my $local_n_response = eval {
            $candidate_api->loop_through_responses(
                $self->company,
                sub {
                    my $adc_response = shift;

                    $after_response_id = $adc_response->{response_id};

                    if ( $adc_response->{time} < $stop_epoch ) {
                        $log->infof('Skipping response_id (older than 6 months): %s %s', $self->company, $after_response_id);
                        $importing = 0;
                        last;
                    }

                    return unless $adc_response->{responded_to};

                    my $bbcss_response = $self->get_bbcss_response($adc_response);

                    unless ($bbcss_response) {
                        $log->warnf('No response found in bbcss for adcresponse_id [%d], skipping...', $adc_response->{response_id})
                        and return;
                    }

                    unless ( $self->advert_is_live($adc_response->{advert_id}) ) {
                        $log->infof('Advert with ID [%d] is not live, will not import candidate messaging history for it.', $adc_response->{advert_id})
                        and return;
                    }

                    my $search_user = $self->s2->factory('SearchUser')->find({ provider_id => $adc_response->{user_id} });
                    unless ( $search_user ) {
                        $log->infof('No search user found for adcourier user_id [%d], skipping...', $adc_response->{user_id})
                        and return;
                    }

                    if ( $adc_response->{responded_to} ) {
                        my $action_log = $self->s2->factory('CandidateActionLog')->search(
                            {
                                action => 'message',
                                candidate_id => 'adcresponses_'.$bbcss_response->{document_id},
                                data => { like => '%imported_from_adcourier%' },
                                user_id => $search_user->id(),
                            }
                        )->first;
                        if ( $action_log ) {
                            $log->infof('Message import record already exists for this candidate, skipping...')
                            and return;
                        }

                        $self->s2()->action_log->insert({
                            action => 'message',
                            candidate_id => 'adcresponses_'.$bbcss_response->{document_id},
                            destination  => 'adcresponses',
                            user_id      => $search_user->id(),
                            group_identity => $self->company,
                            data => {
                                # Might be handy information to store...
                                imported_from_adcourier => 1,
                                responded_to            => $adc_response->{responded_to}
                            }
                        });
                        $log->infof('Updating action history for candidate_id [%d]', $bbcss_response->{document_id});
                        push @messaged_candidates, $bbcss_response->{document_id};
                    }

                },
                {
                    $after_response_id ? (previous_id => $after_response_id) : (),
                    reverse_order => 1
                }
            );
        };
        my $err = $@;
        unless ($err) {
            $pause_time = 1; # reset pause time for next response.
            unless ($local_n_response) {
                # No error and no local_n_response. This is the end!
                last;
            }
        }
        else {    # if there is an error
            if ( $pause_time < 256 ){ # 4 mins, 16 seconds
                $pause_time *= 2;
            }
            $log->warnf( "An error has occured. %s", substr( $err, 0, 300 ) );
            sleep $pause_time; # Juice API regularly goes down for a few seconds - pause for an increasing amount of time, don't stop retrying.
        }
    }

    $log->info('Updated action log for the following candidate IDs: ' . join(', ', @messaged_candidates) );
}

sub get_bbcss_response {
    my ($self, $adc_response) = @_;

    my $advert_id = $adc_response->{advert_id};

    unless ( $self->memory_bbcss_responses->{$advert_id} ) {
        $log->infof('Getting responses from BBCSS for advert_id [%d]', $advert_id);

        my $bbcss = $self->s2->bbcss_api;
        my $request = HTTP::Request::Common::GET(
            $bbcss->_url(
                'responses{?broadbean_name,flavour,property_name,property_value}',
                {   flavour         => 'ResponsesHolder',
                    broadbean_name  => $self->company,
                    property_name   => 'broadbean_adcadvert_id',
                    property_value  => $adc_response->{advert_id},
                }
            ),
            'Content-Type' => 'application/json'
        );

        my $json = $bbcss->_request_with_404($request);
        $self->memory_bbcss_responses->{$advert_id} = $json->{responses};
    }

    my ($response) = grep { $_->{broadbean_adcresponse_id} eq $adc_response->{response_id} } @{$self->memory_bbcss_responses->{$advert_id}};
    unless ( $response ) {
        $log->warnf('No response found matching response_id [%d] on advert [%d]', $adc_response->{response_id}, $advert_id);
        return;
    }
    return $response;
}

sub advert_is_live {
    my ($self, $advert_id) = @_;
    my $advert_api = $self->s2->advert_api;

    # should really only be one advert
    my @adverts = $self->memory_adverts->{$advert_id} // $advert_api->search_adverts({ company => $self->company, advert_id => [$advert_id] });
    my $advert = $self->memory_adverts->{$advert_id} = $adverts[0];

    if ( $advert->{visible} eq '1' ) {
        return 1;
    }
}

__PACKAGE__->meta->make_immutable;
1;

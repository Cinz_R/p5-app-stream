#! bin/s2-shell

use Log::Any qw/$log/;
use DateTime;

my $run_time = {board_name => 'iprofile',  TZ => 'Europe/London' , avoid_hours => [2,3,4], run_at => 5};


my $wds = $s2->factory('Watchdog')->search({},{});



$wds->fast_loop_through(
    sub {
            my ($wd) = @_;
            my $has_board = 0;
            #  I want to use last to speed things up, so I wrap the grep in a block
            {
                grep {
                        if ( $_->subscription->board eq $run_time->{board_name}) {
                            $has_board = 1;
                            last;
                        }
                } $wd->watchdog_subscriptions;
            }

            # return if the watchdog does not have the problematic board
            return unless $has_board;

            # if we do have a problematic board then we channel our inner
            # Lu-Tze and do some time slicing.
            $log->infof("%s has a a problematic board.", $wd->name);

            # the run time really shouldn't be null but we should guard agasint is anyway
            my $next_runtime = $wd->next_run_datetime //
                DateTime->now(time_zone => $run_time->{TZ})->set_hour($run_time->{run_at});

            # check if there is a time clash.
            $next_runtime = $next_runtime->set_time_zone($run_time->{TZ});
            my $has_clash;
            map { $has_clash = 1 if $_ == $next_runtime->hour }
                @{$run_time->{avoid_hours}};

            # if there is no clash we don't need to do anything.
            return unless $has_clash;
            $log->infof( "%s has a clash %s.", $wd->name, $next_runtime->hour );

            $next_runtime->set_hour( $run_time->{run_at} );
            $next_runtime->set_minute( int(rand(60)) );

            # set the TZ to UTC and save.
            $next_runtime = $next_runtime->set_time_zone('UTC');
            $wd->next_run_datetime( $next_runtime );
            $wd->update();
            return 1;
        }
);
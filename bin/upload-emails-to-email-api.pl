#!/usr/bin/env perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long::Descriptive;
use JSON qw/decode_json encode_json/;
use Log::Any qw/$log/;
use Log::Any::Adapter ('Stderr');
use LWP::UserAgent;
use MIME::Base64 qw/encode_base64/;
use Stream2;

=head1 SYNOPSIS

    $ bin/upload-emails-to-email-api.pl \
        --config=app-stream2.mhawkes.conf \
        --from=2015-11-08 \
        --to=2016-01-30 \
        --start-with-id=12345 \
        localhost:3002

    # see usage message and CLI options
    $ bin/upload-emails-to-email-api.pl --help

=head1 DESCRIPTION

Uploads the email message represented by each row of the
C<cvsearch.sent_email> table to the EmailAPI for storage.

=cut

my ( $email_api_url, $conf_file, $from_date, $to_date, $start_with_id )
    = get_command_line_params();

my $s2              = Stream2->new( config_file => $conf_file );
my $installation_id = $s2->config->{installation_id}
    or die "$conf_file has no installation ID entry in it\n";
$log->info(
    "Fetching emails from cvsearch.sent_email from $from_date to $to_date " .
    "from Search2 instance ID $installation_id"
);

my $emails = $s2->factory('SentEmail')->search(
    {
        sent_date => { -between => [ $from_date, $to_date ] },
        id        => { '>=' => $start_with_id // 0 },
    },
    { order_by => 'id', columns => [ qw/id user_id sent_date aws_s3_key/ ] }
);

my $ua = LWP::UserAgent->new(
    timeout         => 20,
    default_headers => HTTP::Headers->new(
        Authorization => 'Y66Tg7wqW9yuXEn4JXw7ey1iHsuiHGG7H2o99fhwjdzy768H' .
                         'ZzO0jdVb44uItsF8uGH9ZlQmMftvBo4j9twa5zHlq19k420l' .
                         'P21rklr3YXyv72dVdD46s7Nbca4QmLt3'
    )
);

$emails->loop_through( sub {
    my $email     = shift;
    my $num_tries = 5;
    while ( $num_tries > 0 ) {
        eval {
            $log->info('Attempting to upload email ID ' . $email->id);
            my $new_email = upload_email( $ua, $email );    # dies on error
            $log->info('Stored ok in email service with ID '
                . $new_email->{id});
        };
        if ( my $err = $@ ) {
            if ( --$num_tries ) {

                # sleep a random amount of seconds (1 to 10) before retrying
                sleep (int rand 10) + 1;
            }
            else {
                die 'Tried multiple times to upload Email ', $email->id,
                    ": giving up: most recent error: $err";
            }
        }
        else { return; }
    }
} );

# uploads an email message to the EmailAPI, or dies with a string on error.
# Returns a hashref of how the EmailAPI has chosen to interpret/store the
# message. E.g.
#
#  {
#    fromAddress  => 'pb@example.co.uk',
#    clientRef    => 'Search2/company123/user47',
#    fromName     => '',
#    date         => '2016-01-06T10:20:28',
#    subject      => 'Foobar',
#    awsS3Key     => 'priv-test9DE26934-C6A2-11E5-855F-7BF942330FC0',
#    id           => '29',
#    recipientsTo => [ 'monkey@broadbean.com' ],
#    recipientsCc => [ 'John <john@example.com>', 'Bill <bill@example.com>' ]
#  }
sub upload_email {
    my ( $ua, $email ) = @_;

    my $id         = $email->id;
    my $client_ref = build_client_ref( $email->user );
       $client_ref = "/$id$client_ref";

    fix_email( $email );
    my $post_body = encode_json( {
        clientRef => $client_ref,
        message   => encode_base64($email->mime_entity->stringify, ''),
    } );
    my $response  = $ua->post(
        "http://$email_api_url/emails",
        'X-Stream2-Email-Id' => $id,
        'Content-Type'       => 'application/json',
        'Content'            => $post_body
    );

    my $status_code   = $response->code;
    my $response_body = $response->decoded_content;
    die "Got non 200 HTTP status code $status_code for email ID $id.\n" .
        "Response body:\n$response_body"
        unless $status_code == 200;

    my $json_response = decode_json( $response_body );
    return $json_response->{response}->[0];
}

# processes command line options: returns a list:
#   ( $api_url, $config_file, $from_date, $to_date, $start_with_id )
# $start_with_id may be undef. In that case, caller should start with first
# available email within the specified date range
# dies if command line options are malformed or if mandatory ones are absent
sub get_command_line_params {
    my $DEFAULT_FROM_DATE = '1000-01-01';    # earliest MySQL supports
    my $DEFAULT_TO_DATE   = '9999-12-31';    # latest MySQL supports
    my $DATE_FORMAT       = qr/^\d{4}-?\d{2}-?\d{2}$/;

    my ($options, $usage) = describe_options(
        "%c %o hostname[:port]",
        [],
        ['POSTs the email message represented by each of the rows in the '],
        ['cvsearch.sent_email table to the EmailAPI at hostname:port. Date '],
        ['range is implicitly $DEFAULT_FROM_DATE to $DEFAULT_TO_DATE but '],
        ['can be overridden if you want to upload fewer messages. You can '],
        ['also begin uploading emails from a specific ID within the date '],
        ['range.'],
        [],
        [
            'config|c=s',
            'Search2 config file to use for DB connection',
            { required => 1, default => 'app-stream2.mhawkes.conf' },
        ],
        [
            'from|f=s',
            'start date range (YYYY-MM-DD format)',
            { default => '1000-01-01', regex => $DATE_FORMAT }
        ],
        [
            'to|t=s',
            'ending date range (YYYY-MM-DD format)',
            { default => '9999-12-31', regex => $DATE_FORMAT }
        ],
        [
            'start-with-id|i=i',
            'ID of email within date range to begin uploading'
        ],
        [ 'help|h', 'Print usage message and exit' ],
        [],
    );

    if ( $options->help ) {
        warn $usage->text;
        return undef;
    }
    else {

        my $email_api_url = shift @ARGV;
        unless ($email_api_url) {
            warn "Mandatory 'hostname:port' of EmailAPI server is absent.\n";
            warn $usage->text;
            return undef;
        }

        # make date ranges inclusive: truncate start date to 00:00:00 (it
        # effectively already is) and the end date to 23:59:59
        my $from_date = $options->from;
        my $to_date   = $options->to;
        $from_date .= index( $from_date, '-' ) >= 0 ? ' 00:00:00' : '000000';
        $to_date   .= index( $to_date, '-' )   >= 0 ? ' 23:59:59' : '235959';

        return ( $email_api_url, $options->config, $from_date, $to_date,
            $options->start_with_id );
    }
}

# build and return a client_ref string of the format
# 'product/installationId/groupIdentity/searchUserId'
sub build_client_ref {
    my $user = shift;

    return sprintf "/%s/%s/%s/%s",
        'Search2',
        $installation_id,
        $user->group_identity,
        $user->id;
}

# Before March 2016 Search generated malformed email messages to candidates.
# Such messages lacked a Date header and failed to quote the display-name in
# "display-name <email@addr>" the values of "To" headers.
sub fix_email {
    my $email = shift;      # a MIME::Entity

    my $mime_entity = $email->mime_entity;

    # quote the display-name part of "display-name <msgbox@domain>"
    # changing $to_value changes $to_header_values[n]: it's an alias
    my $mime_head        = $mime_entity->head;
    my @to_header_values = $mime_head->get('to');
    for my $to_value ( @to_header_values ) {
        my ( $display_name, $addr ) = $to_value =~ /([^"]*?) *(<[^>]+?>)/;
        $to_value = qq["$display_name" $addr]
            if $display_name && $addr && $display_name =~ /[^\w ]/;
    }
    my $to_header_value = join ', ', @to_header_values;
    $mime_head->set('to', $to_header_value);
}

Stream 2 (hopefully)
====================

Do NOT use the master branch.
=============================

Develop in FEATURE BRANCHES
===========================

[![Build Status](https://build.adcourier.com/buildStatus/icon?job=p5-app-stream_build--dev)](https://build.adcourier.com/job/p5-app-stream_build--dev/)

 * Mojolicious based
 * Stores results temporarily instead of permanently


'dev' is the main development branch.

### Quick Setup

** If you are setting up a development environment to submit patches, you may want use the stream2 VM which can be found in the stream2-vagrant repo. **

How-to video: https://www.youtube.com/watch?v=hLLM8L8mJmM

see/run ./dev-bin/build-dev-environment

### Install Compass ( http://compass-style.org/ )

Recommend using `rvm` to install ruby and compass. See http://rvm.io/ fpr rvm installation.

    sudo gem install compass

## Install svn.

Some part of the system need the svn binaries.

   sudo apt-get install svn

### Install Node dependencies

You will need the [node package manager](https://npmjs.org/) first:

    sudo apt-get install node

If you are Debian based: make sure node is actually nodejs:

    sudo update-alternatives --install /usr/bin/node node /usr/bin/nodejs 10

Then:

    npm install

### Install CPAN dependencies

Recommend using `perlbrew`:

    perlbrew install-cpanm
    perlbrew use 5.16.2
    perlbrew lib create s2-web
    perlbrew use 5.16.3@s2-web

Can include the `perlbrew` lib in  your prompt by adding this to your `.bashrc`:

    function __local_perlbrew {
      local perlbrew=$(echo $PERLBREW_PERL | sed -e 's/^perl-//')
      local locallib=$(echo $PERLBREW_LIB)
      [ "$locallib" != "" ] && locallib="@$locallib"

      echo "$perlbrew$locallib"
    }

    # Add perlbrew perl + local::lib version to prompt
    PS1="\$(__local_perlbrew) $PS1"

Point `cpanm` at our [Pinto repo](https://trac.adcourier.com/wiki/Pinto):

     export PERL_CPANM_OPT="--mirror https://$PINTO_USERNAME:$PINTO_PASSWORD\@aws_pinto.adcourier.com --mirror http://backpan.perl.org --mirror-only --cascade-search"

Point `carton` at our [Pinto repo](https://trac.adcourier.com/wiki/Pinto):

     export PERL_CARTON_MIRROR="https://$PINTO_USERNAME:$PINTO_PASSWORD\@aws_pinto.adcourier.com"

and install dependencies using `cpanm` and `carton`:

Note: First you might want to
 
     sudo apt-get install libxslt1-dev

<!-- And install the 1.78 version of libXSLT manually: -->

<!--      wget http://backpan.perl.org/authors/id/S/SH/SHLOMIF/XML-LibXSLT-1.78.tar.gz -->
<!--      cpanm XML-LibXSLT-1.78.tar.gz -->

<!-- Note: Due to https://github.com/ingydotnet/spiffy-pm/pull/2, you might have to -->

<!--      PERL_CPANM_OPT="" cpanm Spiffy@0.31 -->


<!-- Note: Due to Net::Ping tests being broken (as of 18 June 2014) -->
 
<!--      PERL_CPANM_OPT="" cpanm -vn Net::Ping@2.4 -->

<!-- Note: Due to https://github.com/demerphq/Data-Dump-Streamer/issues/8 you might have to -->

<!--      cpanm -n Data::Dump::Streamer -->

Note: Make sure you have libmysqlclient-dev AND mysql-server.
This is because the DBD-mysql distrib won't install with cpanm without having mysql-server.

     sudo apt-get install libmysqlclient-dev mysql-server

required packages for ubuntu:

    sudo apt-get install libxslt1-dev libxml2-dev lib32z1-dev libgdbm-dev libssl-dev libmysqld-dev
    sudo apt-get install libaspell-dev aspell-de aspell-en aspell-fr aspell-nl aspell-it aspell-sv
    cd /usr/include && sudo ln -s libxml2/libxml/ . # might be needed to ensure xml2 header files are available

Install perl level dependencies:

    cpanm local::lib
    cpanm Carton
    cpanm Test::Harness

General dependencies install in the `local` directory using carton:

    carton install --deployment

Also update to the latest Stream::Templates

    cpanm -Llocal Stream::Templates



This will install frozen dependencies versions as defined in cpanfile.snapshot

### Introducing a NEW dependency.

1. Stick it in cpanfile

2. Install it with `cpanm -L local --installdeps .`

3. Run `carton` to update the cpanmfile.snapshot

### MySQL

See `bin/mysql` to setup a standalone version of Mysql

### Redis

Run `bin/redis` to start a standalone version of Redis

After Redis is built and started, you can Ctrl-D it and check you local instance of redis is running:

ps auxf | grep redis

### Rebuild css/js when it changes

Make sure brunch is installed:

    npm install brunch    

And then:

    node_modules/brunch/bin/brunch build # (or `brunch build --optimize` to minify CSS and JS)

### Generate some keys and login.

    mkdir oauth_rsa
    perl gen-keys.pl

## Register your app

    https://oauth.adcourier.com/register

and take note of the app_id you get after registration

## Make yourself a app-stream2.$USER.conf.

touch "app-stream2.$USER.conf"

and edit

app-stream2.pat.conf is a good example. Don't forget to set your mysql port,
your private key location and your application ID (from the above registration in there.

Also, will need to set the database access credentials to msandbox/msandbox 

## Prepare the log files destination

sudo mkdir -p /var/log/broadbean/
sudo chown $USER:$USER /var/log/broadbean/

touch /var/log/broadbean/search.log

## Install or upgrade the schema

Deploy the lastest patches:

    perl -Mlocal::lib=local ./bin/s2-sqitch -c app-stream2.$USER.conf deploy


## Start all the services with the right config

   MOJO_CONFIG="app-stream2.$USER.conf" MOJO_MODE=development ./dev.sh

## Hit

  https://192.168.1.103:3001/login


and login with

  username => 'andy@users.live.andy',
  password => 'demo',


## Troubleshooting

### Installing XML::LibXSLT on a Mac

Apple ship libxml2 which makes this module a bit tricker to install.

    # install LibXSLT with homebrew
    brew install libxslt

    # Pass libxslt and libxml2 path to cpanm
    cpanm --configure-args='LIB="-L/usr/local/Cellar/libxslt/1.1.28/lib" INC="-I/usr/local/Cellar/libxslt/1.1.28/include -I/usr/local/Cellar/libxml2/2.9.1/include/libxml2"' -L local http://backpan.perl.org/authors/id/S/SH/SHLOMIF/XML-LibXSLT-1.78.tar.gz

    # or, to do it manually...
    # get CPAN distro and unpack
    wget http://www.cpan.org/authors/id/S/SH/SHLOMIF/XML-LibXSLT-1.80.tar.gz
    tar -xzf XML-LibXSLT-1.80.tar.gz
    cd XML-LibXSLT-1.80

    #�perl make linking against Apple supplied 'libxml2'
    # and libxslt in the homebrew cellar
    perl Makefile.PL LIB="-L/usr/local/Cellar/libxslt/1.1.26/lib" INC="-I/usr/local/Cellar/libxslt/1.1.26/include -I/usr/include/libxml2"
    make
    make test
    make install

## Demo credentials.

### Login as superadmin for company 'andy'

http://juice.adcourier.com/lazy_hien.cgi?action=login&co=andy


### OR use a demo account:

searchdemo@users.live.andy

stream4life

### PRODUCTION BOX

Use your own username to ssh in there:

prod-aws-search-1.steamcourier.com

prod-aws-search-3.steamcourier.com
   
Then become broadbean:

   sudo su - broadbean

The code and config is in:

   /srv/deploy_aws_search

'current' points to the current version.


## Staging server

Web: https://192.168.1.20:3001/
Emails: http://192.168.1.20:8000/


Username/password: staging/stream4life

## Error calendar (aka search logs but not only)

http://www.adcourier.com/manage/error-calendar/

## Nipple API credential access:

jerome@broadbean.com / f766b05469a8


## Switching a client to search2

You'll need your editor:

www@cheerleader2:~$ vi site/Stream/ProductBundles.pm

## Using Guard to rebuild styles/scripts ##

    # install latest ruby
    apt-get install ruby2.2
    apt-get install ruby2.2-dev

    # make sure most recent ruby is being used
    # on my linux /usr/bin/ruby points to a specific version via symlink
    rm /usr/bin/ruby /usr/bin/gem
    ln -s /usr/bin/ruby2.2 /usr/bin/ruby
    ln -s /usr/bin/gem2.2 /usr/bin/gem

    # install Guard
    gem install --install-dir local-gems/ guard-shell

    # run Search with Guard
    ./local-dev-guard.sh
# Search Team Manifesto #

## Each developer should have a computer ##

## Jenkins build should be stable ##

Last 2 built should be successful.

## Test suites should always pass ##

## Staging box ##

Staging box should always be up-to-date. (do SEAR-1634)

## Tickets follow our standard ticket workflow ##

See docs/workflow.md

## Tickets are written according to our ticket writing guidelines ##

See docs/guidelines_issues.md

## Code should adhere roughly to coding guidelines ##

See docs/guidelines.md

# Regression suite

Follow regression progress at https://docs.google.com/spreadsheets/d/1Ud5czGkJbEvzw-iVKJrO0Vf7B5oiPN37kabwZq6PJbM/edit?usp=sharing

## Staging server.

[New Careerbuilder staging box](https://svrus4srch2stg.cb.careerbuilder.com/)

[Kitten page (contains all emails sent by staging)](http://svrus4srch2stg.cb.careerbuilder.com:5000/)

Note that NO real emails are sent to the outside world. They all go to the kitten page for inspection by us.

## Ripple testing

- cd into your p5-app-stream dir (root of the Search2 project)

- curl -k -X POST https://svrus4srch2stg.cb.careerbuilder.com:3001/ripple -d @sandbox/test_ripple.xml

- Go to the replied URL (there is a URL in the reply normally)

- Test that you can shortlist a candidate against a Cheesesandwich or a Beer or your choice

- Test that in a email (message feature), you can add a Beer Job URL.

- Inspect the sent email and checks it links to http://www.beer.com/ at the bottom of it.

- Search on an external board (totaljobs) with no criteria and click on the save candidate button, it should turn green

- curl -k -X POST https://svrus4srch2stg.cb.careerbuilder.com:3001/ripple -d @sandbox/test_ripple_lead.xml

- Check that you get the 'Search As' page.

- Choose Search as cinzpoland

- List all talent search candidates.

- Use shortlisting features. They should be cheesesandwiches and beers.

### JSON ripple

- run perl -Mlocal::lib=local  sandbox/json_ripple_regression.pl

- go to the preview URL provided, it should open a profile, and the name should be the same as in the script output

- check that the result number in the script output is the same as the expected result number

- Close the profile. The total number of results in Search should be the same as in the script output

- check that the adcourier navigation is present and correct

- run perl -Mlocal::lib=local sandbox/json_ripple_thirdparty.pl

- This scripts logs in using the third party provider and output various URLs.

- Check that the cv, profile, profile_history, profile_url and results_url URLs work.

- tail the log on the staging box: `tail -f /var/log/broadbean/search.log | grep HOYEAHBABY`

- run perl -Mlocal::lib=local sandbox/json_ripple_blocking.pl , while tailing the log
on the staging box.

- This script runs a similar request to a normal jripple search but should output the results ( as well as a session id ).

- Also check the log. It should say 'Removing ofccp_context "["HOYEAHBABY"]" from query due to user not having the setting turned on'

### JSON Ripple + OneIAM

- Go to http://employer.careerbuilder.com/jobposter/mycb.aspx

U: Jerome.Eteve@careerbuilder.com
P: C756ai2592C3

If you have trouble login in, look into docs/careerbuilder.md

- Run: ./sandbox/json_ripple_oneiam.pl

- In your browser, open your JS console and tail the staging box your logs for search log S3 URLs.

- Go to the replied URL

- Check that the JS Console contains: CB OneIAM session is there. status:  Object -> status: logged_in

- Check that your S3 log contains the following:
```
This is user hash = $VAR1 = {
'cb_oneiam_auth_code' => 'A LONG AUTH CODE',
          'auth_tokens' => { ... } }
```

- Check that the S3 log contains the following:
```
Removing ofccp_context "["boudinblanc"]" from query due to user not having the setting turned on
```


## As khr superadmin

- Become the first khr user.

- Come back to admin.

- Become the first khr user again.

- Check talent search is working without any search criteria.

- Check that some results have got 'application flags' and application history (in the preview) against them.

- Check talent search is working with a keyword and a location (N results should go down)

- Check the industry facet is working correctly (try filtering your results by industry)

- Use the keywords 'MARIUSZ BŁASZCZAK' (UTF8 stuff)

- Check talent search is working with More criteria: CVs updated, Job Type. N of results should all change when you change those criteria.

- Try paging through candidate previews. Ensure the details are consistent with the result body

- Repeat those search tests for Reed (except the flags).

- Go to the juice vlad panel of khr, in options. In the drop down find Search - Download CVs on profile view (setting 102),
view the setting and see that it is OFF. If it is ON, tell others as we will have to choose a different company for this test.

- From reed. Try:

- Viewing a profile. This should NOT result in the automatic import of the profile, as no one in KHR has got the vlad setting 102 on.

- Downloading a CV. Also the history icon appears straight away.

- Forwarding a CV. Check the message on http://svrus4srch2stg.cb.careerbuilder.com:5000/ (kitten page).

- Messaging a candidate without a job. Check the message on the kitten page. Hitting the email client 'Reply button' should reply to the sender.

- Try saving a search.

- Clear the criteria and load your saved search (The criteria should echo just fine)

- Delete your saved search.

## As Cinz Wonderland (cinzwonderland) Superadmin

- Check the admin page about Settings (Change settings, expand/collapse tree, set some stuff to on/off back and forth).

- Check the admin watchdog page works

- Check the admin savedsearch page works

- Check you can become cinzia.second.

- From a CV Library search, or totaljob

- Try ALL The candidates actions

- Bulk Messaging a couple of candidate with a job, making sure the message contains some unicode strings (like BŁASZCZAK).

- Check the message(s). Hitting the email client 'Reply' should reply to the sender.

- Preview forward and backward should work.

- For The forward action, this should generate two emails (check kitten page) according to the BCC settings of cinzwonderland.

- Importing a tagged candidate to talent pool should work (button goes green).
Check if the candidate turns up in talent search for real with the tag.
If not check tsimport API product.

- Shorlisting against a job should work. (Hyppo Oculist).

- After shortlisting, check that the candidate appears in talentsearch and that you can download and open its CV correctly

- Bulk Shortlisting should work, check against the job in adcourier (click the job link from the Talent search preview panel should take you there directly).

- From talent search

- You should see some 'application flags'. They should be consistent with the number of application
on the preview page.

- Bulk Messaging a couple of candidate with a job. Check the message(s). Hitting the email client 'Reply' should reply to the sender.

- Search talent search for C++ (note the ++). Open a profile. It doesn't crash.

- Search talentsearch for 'Andreea' with no time limit.

- In 'Andreea Elisabeta Mara' view profile page, applications coming from a shortlist action should be described as 'Candidate Shortlisted',
as opposed to 'Candidate Applied'.

- Try Saving/Loading/Deleting a watchdog. (The criteria should echo just fine).

- Try editing a watchdogs criteria.

- In the watchdog drop down, there should be numbers with total number of new results from your preview WDs.

- Open a watchdog, one that has talent search in it. Try to forward a candidate from the watchodog result (green).

- Open a watchdog, change the search criteria and check you can update the watchdog criteria (and reload it).

- Copy the link to a watchdog and paste it into a new tab. Make sure results load and boards are traversable.

- Try 'tagging' and 'untagging' some candidates.

## New Manage Response

- As cinzwonderland / cinz.second

- Search for prafulla vasa

- Create a folder/add to a folder.

- Green flag a candidate, make sure it shows in the facet after about 5 minutes. Change the flag to something else.

- See profile. Check you can see the Application Message.

- Try adding a note. Check the note icon is displayed in the snippet.

- Check that clicking on the note snippet takes you to the notes section.

- Check the note has been added to the same candidate in talentsearch.

- Deleting the note against the candidate.

- Check the folder added earlier turns up in the folders' list.

## As pat superadmin, USING IE(11)

- Go to the settings, make sure 'Long List' is on.

- log in as dutchiedutch

- load results in talentsearch using a location (like London)

- Make sure you can longlist some of them. Either via a button in the sliding panel, or via
the dynamic button once you've used one.

- Go and see a longlist, try to remove some candidates from it, then return to search.

- view profile of candidate in talentsearch, apply new availability date, make sure it looks dutch.

- Close profile reopen same profile to ensure it still looks good

- reload the results to make sure the availability date has been updated in TS and it is displayed nicely in the results view

## As simonr

- as superadmin

- Make sure ONLY test user has got OFCCP turned on.

- switch to test user.

- From MySupply

- Put something in OFCCP field

- Observe the log. See OFCCPSearchTitle is sent to MySupply (grep log for s3 - click on s3 link created from your search - search in s3 log for ofccp)

- Make sure you can tag/ remove tag from a candidate.

- Make sure you can message a candidate.

- Make sure you can open a profile view.

- Make sure you can download the CV.

- Perform a search. Make sure you can Share the Search criteria with another coworker. It emails the current URL to them. Check the kitten page to ensure it was sent.

- back to superadmin

- become simonr

- check you haven't got OFCCP box.

- Click search on MySupply

- It works. In the log, there is no mention of OFCCP.

- Do a search on Recruitment Edge

- Open a profile, download the CV

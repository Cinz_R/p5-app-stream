## Definition Of Done

### Pre-UAT

- Tests are written (backend and frontend)

- How to UAT is written and tested

- Test it works according to acceptance criteria

- Code is written

- Ensure the test suite passes

- Code review passes

- Embeded doc is written

- If big change, regression passes on your box

- Added to regression (if approriate)

- Carton is rebuilt if needed

- Is on staging

- Vested interests if any are notified

### Post-UAT

- UAT tested

- Vested interests if any are notified

- Released to live

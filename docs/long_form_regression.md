# Regression suite (long form)

## Staging server.

[New Careerbuilder staging box](https://svrus4srch2stg.cb.careerbuilder.com/)

[Kitten page (contains all emails sent by staging)](http://svrus4srch2stg.cb.careerbuilder.com:5000/)

Note that NO real emails are sent to the outside world. They all go to the kitten page for inspection by us.

## Main Interface


### CV preview 

- check for the delete candidate link (must be a dev user and be on the talent search board.)


### Long lists (LL)

#### All open inline actions should be closed on LL deletion

- do a search and create a long list
- refresh the page, and open several line actions on candidates, (shortlist, message)
- delete the previouly created longlist
- expected, all line actions should be closed.

### Unsubscribe

- send a email to yourself (DO NOT USE YOUR BB ADDRESS!!!), go to the unsubscribe page, and unsubscribe, try and send yourself again, which should be blocked.
- delete this email from the DB.



### Watchdogs

- create a watchdog, then try to navigate straight to it
- expected, the watchdog should load.

#### Watchdogs should have unique names

- try to create a watchdog (wd)  with the same name as a previous wd
- expected, ui should compain it has the same name, and wil not save

- try to rename a watchdog with the same name as another wd
- expected, ui should complain, and wil not save



### Settings

Changing the setting you should see/detect a given change, usually these are
set in the front end interface.

####UI BASED

##### quota-show_credit_counter

When the setting is on:

- if a board has a search quota set against it, a counter pill should be
visible on the board selection,
- if an action has a quota set against it, when viewing a board, a
sticky box should be visible.


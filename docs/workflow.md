# Search Tickets Workflow #

## Standard tickets procedure ##

1. A ticket enters the sprint backlog with a priority following the decision of the product manager.

2. When a developer is available,
they pick and assign to themselves the top ticket from the TODO column.

3. Developer works on one or more branche(s) all cut from the freshest dev(for search) or master(other projects),
all named with the preefix SEAR-<ticket number>-..., putting the ticket in 'In Progress'

4. Developer runs the test suite to make sure he hasn't broken any test.

5. Developer create PR in bitbucket and move ticket to Code Review and blow their kazoo using an appropriate pitch according to the ticket or code nature.

6. Fellow developers review the code and if happy approve it.

7. Developer potentially re-work the ticket according to Code Reviews. For small changes, the ticket can stay in Code Review. For big changes, it goes back to in progress.

8. Developer merges PR to dev. And update staging.

9. DOD is enforced. Ticket moves to UAT, making sure the ticket has got UAT section (not for bugs).

10. Someone ELSE UATs ticket and if happy puts it in TBD. If not happy, puts in back in progress with comments.

11. Deployment happens. Ticket is moved to Done by Scrum Master (not by anyone else).

## Hotfixing (aka cherry picking) ##

How does an issue qualify for HOT FIX?

- Testabiliy. Is it covered by at least one test?

- Complexity. Is it just a simple if statement or one missing file? Can
you understand it on a Monday morning?

- Size. Is it a one liner, a 1000 lines one? Obviously increase in size
Reduces hotfixing qualification.

- Risk. What happens it if your fix goes wrong? Does it corrupts data or
just moves a few pixels on the user's screen? Increase in risk reduces
hotfixing qualification.

- Scope. Does that impact the whole of the system? Or just a very specific
low used feature or sub-feature? Increase in scope reduces hotfixing
qualification.

- Business value. Are we going to lose 1 million dollars if we don't
deploy that right now? Or can it wait a few hours/days without damage.
What's the damage to existing clients
If it goes wrong (see risk). Increase in business value increases
hotfixing qualification.

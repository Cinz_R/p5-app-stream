# Contributing to Search 2 #

# Branching and pull requests #

## 'master' is not to be used for normal operation ##

The master branch is what is on live. It should *always* be possible to deploy
the current version of Search 2 to live.

## dev is the development branch ##

If you break dev, you fix dev. 

For any work, branch off dev. (See 'Branch Naming').

Code review is required on all work. Work is completed on branches and merged
in using BitBucket's Pull Request mechanism.

## Branch naming ##

Prefix has to be used to refer to the JIRA ticket containing an appropriate
description of th work being done. You can also put a short mnemonic
after the ticket reference.

For example, for jira ticket SEAR-12345 that deals with richer email template,
acceptable branch names are:

SEAR-12345 or SEAR-12345-richemtmpl

Anything else, and specifically a branch that doesn't refer to a jira ticket
(or any ticket tracking system of the day) is strongly not part of normal operation.

## Pull requests ##

Once your branch is ready to merge, run the entire test suite on your
development box to make sure you didn't break anything obvious.

Then create a BitBucket pull request, inviting at least the core search
developers to review it.

When you get enough approbal, merge as soon as possible.

Pull requests should only contain one feature or one bug fix. The pull request
will only be considered if all the tests pass.

You are encouraged to include tests for bugfixes. Help us build up a valuable
test suite!

## Running the tests ##

 * All the tests: `prove -Mlocal::lib=local -lr -j 4 t/` . Note that his includes frontend tests.

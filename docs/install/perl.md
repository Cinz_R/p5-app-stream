# Perl #

We manage our CPAN dependencies with:
 * perlbrew so we can control the perl version separately to system perl
 * cpanfile to track our dependencies
 * pinto as a private DarkPAN repository

## Using perlbrew ##

```
# install perlbrew
\curl -L http://install.perlbrew.pl | bash

# install cpanm in all perlbrews
perlbrew install-cpanm

# install a perl (we are testing with 5.16.2)
perlbrew install 5.16.2
perlbrew use 5.16.2
perlbrew create lib s2-web
perlbrew use 5.16.2@s2-web
```

Use `perlbrew switch 5.16.2@s2-web` to make this perlbrew lib your default.

## Installing CPAN dependencies ##

Ensure cpanm is pointing at our [Pinto repository](https://trac.adcourier.com/wiki/Pinto).

     export PERL_CPANM_OPT="--mirror https://$PINTO_USERNAME:@PINTO_PASSWORD\@aws_pinto.adcourier.com:443/ --mirror http://backpan.perl.org --mirror-only --cascade-search"

Then install the dependencies using the cpanfile:

```
cpanm --installdeps .
```

### Adding new dependencies ###

Update the `cpanfile` and run `cpanm --installdeps .`

## Tests ##

Our tests are organised under `t/` by module name eg. `App::Stream2` tests will be in `t/app/stream2`.

Run the tests: `prove -l -r t/`

# Node & NPM #

## The build system ##

We use [Brunch](http://brunch.io/) to:

 * lint our JS
 * compile our Ember handlebar templates
 * compile scss to css
 * concat scripts and css into a single file

The brunch config is in `config.js`.

We concat into 3 separate js files:

 * app.js: our client side javascript
 * vendor.js: 3rd party javascript & libraries (separate file as it does not change as frequently as app.js)
 * vendor-test.js: 3rd party javascript required during tests. This is not loaded by the client.

## Client side testing ##

We use [PhantomJS](http://phantomjs.org/), [mocha](http://visionmedia.github.io/mocha/) and
[Sinon.js](http://sinonjs.org/) for client side tests.

To run them: `./node_modules/.bin/mocha-phantomjs test/index.html`

You can also open `test/index.html` in your browser.

## Installing ##

1. system node is usually good enough: `sudo apt-get install nodejs`
2. install npm: `sudo apt-get install npm`
3. install the project dependencies: `npm install`

## Adding new dependencies ##

Edit `package.json` and run `npm install` (or do `npm install --save $PACKAGE`)

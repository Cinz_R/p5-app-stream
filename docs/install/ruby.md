# Installing Ruby & Compass #

  1. Install RVM as a normal user (not root):
    *  With curl: `\curl -L https://get.rvm.io | bash -s stable`
  2. Install a ruby:
    *  `rvm install 1.9.3`
    *  `rvm use 1.9.3`
  3. Install compass:
    * `gem install compass`

## Known issues ##

### rvm errors ###

You may experience errors if you have already tried to install rvm via system packages or as root.

You can uninstall rvm using the script below (pinched from [rvm.io](https://rvm.io/support/troubleshooting)) and then installing rvm as a normal user.

```
#!shell
#!/bin/bash
/usr/bin/sudo rm -rf $HOME/.rvm $HOME/.rvmrc /etc/rvmrc /etc/profile.d/rvm.sh /usr/local/rvm /usr/local/bin/rvm
/usr/bin/sudo /usr/sbin/groupdel rvm
/bin/echo "RVM is removed. Please check all .bashrc|.bash_profile|.profile|.zshrc for RVM source lines and delete
or comment out if this was a Per-User installation."
```

## See also ##

 * http://compass-style.org/
 * http://rvm.io
 * http://thesassway.com/beginner/getting-started-with-sass-and-compass

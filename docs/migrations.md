# MySQL Migrations #

We use [DBIx::Class::Migrations](https://metacpan.org/module/DBIx::Class::Migration) to manage
migrations.

This lets us deploy a new database and upgrade an existing database.


## Deploying a new database ##

When installing, you must start at version 1 and then jump to the latest version or you will not be able to upgrade / downgrade properly as the version in the DB is wiped

WARNING. MAKE SURE YOU DO NOT CONNECT TO THE LIVE DB.

```
./bin/stream-migration install --to_version=1 
./bin/stream-migration upgrade
```

## Upgrading an existing database to the latest version ##

```
./bin/stream-migration upgrade
```

## Creating a migration ##

1. Update the schema version in `lib/Stream2/Schema.pm`
2. Make your schema changes to the DBIC libraries (not to the DB)
3. `./bin/stream-migration prepare`
4. Check the generated files in `schema/stream2`
5. `./bin/stream-migration upgrade`

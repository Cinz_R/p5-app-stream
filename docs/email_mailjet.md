# Sending Emails in Search 2

We use Mailjet, an external email service, to send our emails.

Login here to manage our account...

Login URL:   https://app.mailjet.com/signin
Login Email: mark.hawkes@broadbean.com
Password:    See passpack entry named "Mailjet Testing Account"

Click "Sub-account management", top right, then click the
infrastructure@broadbean.com sub-account. This lets us manage some aspects of
the Search2 and Delayed Email Mailjet sub-sub-accounts.

I say "some aspects" because this login lacks some permissions. Jacek in
DevOps, however, can do everything.

-----

## Sending Emails from a Fixed IP Address

We have some clients who want our emails to come from one fixed IP address so
they can whitelist it. Any emails sent with the following API credentials will
fulfil this.

  Public key: a99df9ec0d3cb8e250a3f703fe49d262
  Secret key: b4c64dd722a46507bc4e112b83460a06

The Mailjet sub-account name given to this account is search2_static.
We think the IP is: 87.253.235.13, please update if you have confirmation
from Mailjet @markhawkes

## Our account manager (though he claims we don't have one):

Neil Pablo

UK Growth
Skype: n.pablo0
Mobile: 077 388 544 98


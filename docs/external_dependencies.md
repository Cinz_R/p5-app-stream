# External Dependencies #

## Perl dependencies ##

Dependencies are managed using a cpanfile. Update the dependencies using `cpanm --installdeps .`

See install/perl.md for more.

 * [Mojolicious: web framework](https://metacpan.org/module/Mojolicious)
 * [Moose: object system](https://metacpan.org/module/Moose]
 * [DBIx::Class: database library](https://metacpan.org/module/DBIx::Class])
 * [DBIx::Class::Migration: database deployment](https://metacpan.org/module/DBIx::Class::Migration])

## Javascript dependencies ##

[Bower](http://bower.io/) manages our javascript dependencies.

Add new components to `bower.json`. You only need the component name and [version range](https://github.com/isaacs/node-semver) if it is in the [Bower registry](http://sindresorhus.com/bower-components/).

```
#!js
  "dependencies": {
    "ember": "~1.0.0"
  },
```

Otherwise include a url to the library source or the git repo:

```
#!js
  "dependencies": {
    "sinon":  "http://sinonjs.org/releases/sinon-1.7.1.js",
    "dropdowns": "git://github.com/claviska/jquery-dropdown.git"
  },
```

Running `bower install` will install the new libraries to `/vendor/`. Then edit the brunch config `config.js` so
the new library is added to `javascripts/vendor.js`

To upgrade an existing javascript library, edit the version range in `bower.json` and run `bower install`.

 * [Ember: javascript web framework](http://emberjs.com/guides/)
 * [Mocha: javascript test framework](http://visionmedia.github.io/mocha/)
 * [Sinon.js: javascript mocking library](http://sinonjs.org/)
 * [Parsley.js: form validation](http://parsleyjs.org/documentation.html)

## Ember ##

We try to run with the latest Ember version. We precompile (handlebars templates)[http://handlebarsjs.com/]
using [a brunch plugin](https://github.com/bartsqueezy/ember-handlebars-brunch).

Brunch must compile the templates using the same version of handlebars that Ember uses.

You must upgrade the brunch plugin `ember-handlebars-brunch` if you upgrade `handlebars`.

## Node dependencies ##

Managed by npm.

 * [Bower: package manager](http://bower.io/)
 * [Brunch: css/js build system](http://brunch.io/)

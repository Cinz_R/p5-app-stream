# Search deployment procedure.

## Stop merging stuff to dev.

## Make sure staging is up-to-date

## Make sure all UAT is done

## Make sure ALL Of the above steps have been done YESTERDAY.

You can NOT start the release day with stuff in the UAT column.
This is to avoid tunnel vision when rushing through regression.

## Make sure staging passes the regression suite. Start at 10.30 on release day.

See regression_testing.md

## DELETE the master branch from bitbucket.

## Cut a NEW master branch from dev.

## Build master on jenkins

Really, Jenkins should already be doing this, or about to begin to. On
(re)creating master, above, BitBucket will have automatically triggered a
build of the deploy_aws_search job.

## Make sure the live DB is up-to-date with the schema in master.

Run this on your laptop:

    $ ssh -v -nNT -L 54321:bbdbsearch.steamcourier.com:3306 \
        prod-aws-searchworker-10.steamcourier.com

It opens a tunnel from port 54321 on your laptop, going via searchworker-10,
to port 3306 (the port MySQL binds to) on bbdbsearch.steamcourier.com. If it
fails, try `jsmith@prod-aws-searchworker-10.steamcourier.com` or whatever
username searchworker-10 knows you as.

Change the stream_dsn setting in app-stream2.conf to:

    stream_dsn => 'dbi:mysql:database=cvsearch;host=bbdbsearch.steamcourier.com;port=54321',

Then run

    $ perl -Mlocal::lib=local bin/s2-sqitch -c app-stream2.conf status
    $ perl -Mlocal::lib=local bin/s2-sqitch -c app-stream2.conf deploy

Close your tunnel and revert changes to app-stream2.conf.

## Alert client services

Tell @here in the App Support HipChat room that Search will be redeployed
about 10-15 minutes from now.

## Deploy the new code using the deployment dashboard

Note that for mysterious reasons,
you will have to restart the frontend services manually until we deploy another way.

## Move all tickets to the done column in JIRA

## Post Release Testing

When the release is complete and pushed to all live boxes, choose a
[Post-Release Testing scenario](https://broadbean.atlassian.net/wiki/pages/editpage.action?pageId=117606747).

# Search2 coding guidelines #

Not code formatting guidelines.

## Tests ##

### Test preamble ###

In your unit test, you want to see any warnings, and not just warnings coming from your
test code. To achieve this, use the following lines:

```perl
 #! perl -w

 use strict;
 use warnings;
```

## SQL Patches ##


### Non blocking alter tables (MySQL) ###

When possible, use ALGORITHM=INPLACE, LOCK=NONE at the end of your ALTER TABLE statements.

This will make sure the table is still useable while the update is going on.

### Grouping table changes (MySQL) ###

Group table changes as much as you can. The reason behind this is that
MySQL will copy/change the table only once.

Not good:

```sql

  ALTER TABLE foo ADD COLUMN blabla;
  ALTER TABLE foo ADD COLUMN beer;

```

Good:

```sql

  ALTER TABLE foo ADD COLUMN blabla, ADD COLUMN beer, ALGORITHM=INPLACE, LOCK=NONE

```

## Logging ##

```perl

  use Log::Any qw/$log/;

  $log->info("Something ".small() );

  $log->warnf("Something %s", small() );

  $log->debug("Something ".long_or_big()) if $log->is_debug(); # Only do the long and big thing if needed.

```

## Caching ##

Use the CHI. Note that by default it does serializing/deserializing itself using
Storable. For caching that is compatible accross versions of Perl, prefer other less
perl tied formats, like JSON.

getting and setting a cache key should be as closed as possible in the code to
reduce confusion (locality principle). The optimum is to use $chi->compute(...).

## Retrying stuff synchronously (short term in code)  ##

Use Action::Retry

## Importing functions ##

It is almost always a bad idea to import functions
in your namespace. This is because as a reader of the code, you want
to see where a function comes from just by looking at it, not by
hunting it at the top of the file, or even worse in another file.

Very very bad:
```perl
  use Foo;

  ...

  somefunction;
```

Here ```use Foo;``` imports the 'somefunction' in the namespace auto magically. This is horrible
and will lead to much confusion and hunting around for the reader.

Very bad:
```perl

  use Foo qw/:common/;
  ...
  somefunction();
```

You can see why...

Bad:
```perl
 use Foo qw/somefunction/
 ...
 somefunction();
```

From the middle of the code, finding where the function is implemented requires some hunting.

Good:
```perl
 use Foo qw//;
 ...
 Foo::somefunction();
```

Looking at the code, it's immediatly obvious the function is implemented in the Foo package.

Editors have been fitted with autocomplete features since the dawn
of programming, so there's no such thing as 'saving key strokes' as
an excuse for not fully qualifying function calls.

## Logic ##

Logic should be Monday morning compatible.

### unless ###

Use unless sparingly. No stuff like unless( ! $thing || $not_something ){..}.
Good: unless( $somethingpositive ){ .. }

### Postfix logic ###

Same rule as unless.

### Dying ###

confess (from Carp) is always advisable instead of just dying.
Except in the case you want to die with something very specific (an end user message with a \n,
or an exception object).

## Asynchronous programming ##

### Promise rejection (aka error) handling ###

In a rejection handler: If you return, the next step will be fullfilled
with the value of your return. Which is probably not what you want.
If you die, the next step will be rejected with the result of your die, which
is probably what you want.

If you want to manage your rejections/errors yourself, it's probably best to
do the following:

```perl
  $promise->then(sub{... fullfill code...},
                sub{
                    my ($error) = @_;
                    .. do stuff with $error ..
                    die $error_or_some_other_one;
                }

```

Or if you do not want to manage your rejections, you can let any error percolate
to the outermost layer by simply not implementing any rejection handler.

## JSON ##

When outputting JSON, make sure your JSON native Booleans
are output properly by your rendering component.

you can try `JSON::PP::Boolean`, `Mojo::JSON->true()`, `JSON::XS::Boolean`,
and any flavour of boolean you want.

If your rendering component does NOT guarantee that these will be turned into the
appropriate JSON tokens `true` or `false`, you have two solutions:

1. Cast them manually as late as possible
to whatever the rendering component manages. For instance for Mojolicious:
`$json->{key} = $json->{key} ? Mojo::JSON->true() : Mojo::JSON->false();`

2. Cast them manually at any layer to a plain 0 or 1. Note that this will work
for Javascript, but not guaranteed to work for other languages:
`$json->{key} = $json->{key} ? 1 : 0;`

## Sending emails ##

### Avoiding attached temp files to stick around ###

If you had generated temp files to attach to your MIME::Entity email
and want to make sure they are cleaned even in the event your email sending
fails, here is a recipe:

```perl
  {
   my $guard = Scope::Guard::scope_guard(
      sub{
        # Clear all temp files that the email was built on.
        $log->info("Purging email attached disk files");
        $email->purge();
      });

     $s2->send_email($email);
  }
```

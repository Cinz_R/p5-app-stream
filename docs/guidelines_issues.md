# Issue writing guidelines #

## Writing a Story ##

Our story writing guidelines roughly follow the industry standard of Scrum stories,
with a couple of additions.

Actors:

Product Owner: The primary person responsible for filing stories. Some stories are more technical and have to be written by developer, but
at the end of the day, they should all get the attention and approval of the product owner.

Developer: The person who implements the story in the affected product. The developer is responsible for: Writing the code, the unit tests,
making sure his stories follows our process smoothly, participating in the acceptance criteria, writing the 'How to UAT' section. And I probably
forget other stuff.

UAT Tester: The person that tests the story works on the staging environment and validates it for release. It CANNOT be the same person as a developer.
It can be the product owner (best) or another developer (more technical stories), or anyone who can understand the story and follow the 'How to UAT'
section.

### Title ###

As consise AND descriptive as Possible. Should be written/vetted by the product owner.

### Description ###

Should be written/vetted by the product owner. Failures to provide this will result in poor comprehension
of the what and why of the story by the developer, and this will most probably lead to a poor implementation.

Try to stick to the following format:

'As a <actor>, I want to <do something>, so I can <benefit>'.

Example:

As a recruiter, I want to be able to Longlist candidates, so I can have an easy way to organise my current candidates.

### Acceptance criteria ###

A detailed bullet point list of what needs to be there so the story is considered done.

Should be written by the product owner and the team. Failure to provide acceptance criteria will
result in: random implementation, lot of tears and misundestanding between all actors.

Example:

- Recruiter can add a candidate to a Longlist.

- Adding to a non existing Longlist simply creates it dynamically.

- Recruiter can load a view a Longlist's candidates.

- Recruiter can remove candidate from a Longlist.

- A Recruiter has got a max of 10 longlists.

### How to UAT ###

For some stories, the Acceptance Criteria are sometimes not enough to allow testing the story.
It can be because a specific set of options should be switched on, or you should be looking at
a specific piece of data. In all those cases, a 'How to UAT' section is necessary to guide the
tester through testing the story actually works.

It's basically a much more detailed walk through of the testing of acceptance criteria. They
are usually written by the developer for the benefit of the UAT tester as they implement the story.

Also, this 'How to UAT' should be written from the perspective of the end user as far as possible, thus guaranteeing
your work really works in reality (not just in the unit tests).

Failure to provide 'How to UAT' will result in UATing slowdown, leading to release delays, and therefore
grumpy product owner and clients.

Example:

- Login as cinzwonderland/cinz.second via adcourier.

- Go to settings and switch option 'blabla' on.

- Go to the board 'Careerbuilder' and search for 'manager'

- Click the Longlist button

- Enter a name of a non existing longlist and click add.

- etc. etc.


## Writing a bug ##

The goal of good bug writing is to minimize the extra communication
needed between at least three actors.

The bug filer: The person actually filing the bug (after finding it, or hearing from a client).

The developer: The person that makes a code change to fix the bug.

The tester: The person who checks the bug is actually fixed before release to live.

Failure to comply with this guidelines will increase communication, increase delays,
increase frustration, and eventually lead to the death of innocent kittens.

### Title ###

As concise AND descriptive as possible. Should be written by the bug filer.

Bad: 'Jobserve doesn't work'

Better: 'Jobserve profile view doesn't work'

Good: 'Jobserve profile view w/ spanish candidate broken'

### Description ###

A Bug description should include

An HTR (How To Replicate) bullet point list. The issue that is the bug should be clearly marked as such and
should be at the end of this list. Should be written by the bug filer.

Failure to write a good HTR will make fixing the bug impossible, as to fix it,
the developer needs to know precisely how to replicate it.

An 'Expected' section that tells what to expect when this bug is fixed.

Failure to write a meaningful 'Expected' section will make testing the bug impossible
or more difficult, as the tester will now know what to expect from the fix.

An example of a good bug description is:

```
HTR:

- Login as cinzwonderland superadmin, then cinz.second

- Search for 'miguel cordoba' on Jobserve (or for any spanish user).

- Open the profile view

- Issue: The CV preview text is all molded (See Screenshot)

Expected: The CV preview text looks clean and correct.
```

Of course, any variations on this theme is ok.
For instance you can see more than one issue whilst running through
the HTR.

# Perl tests #

Run the tests: `./bin/app.pl test`

Run the tests with coverage: `./bin/app.pl cover` (run `cpanm --installdeps --with-recommends .` first to install Devel::Cover)

## Conventions ##

All perl tests are in `t/` and have a `.t` file extension.

The tests are organised by module name. So tests for `Stream::Templates::Default` are in `t/stream/templates/default.t` or `t/stream/templates/default/*.t`.

## Automated testing ##

Master is tested after every commit: http://private.broadbean.net:40801/job/p5-app-stream/

## See Also ##

 * [Test::Most](https://metacpan.org/module/Test::Most) (automatically uses strict & warnings)
 * [Devel::Cover](https://metacpan.org/module/Devel::Cover)

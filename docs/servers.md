Search feed proxies
===================

Live static IP:

54.228.204.154 and
54.228.204.187

Development static IP:

52.18.196.46

See https://broadbean.atlassian.net/browse/SEAR-1456 , comment 13 Jul 2016

search servers
==============

search6 search7 search10 search14

Artirix elastic search for talent seearch:
=========================================

export ARTIRIX=https://broadbean:wor2lahT@es.bb.artirix.com

# Get version
curl -k $ARTIRIX/

# Explore the indices and the 'mappings':
curl -k $ARTIRIX/_mapping | json_pp

# Get a sample of all applicants:
curl -k $ARTIRIX/items/applicant/_search | json_pp
